﻿CREATE TABLE [dbo].[Questionaire] (
    [QuestionaireId]   INT            IDENTITY (1, 1) NOT NULL,
    [WarehouseId]      INT            NOT NULL,
    [QuestionaireType] NVARCHAR (50)  NOT NULL,
    [QuestionaireDesc] NVARCHAR (255) NULL
);

