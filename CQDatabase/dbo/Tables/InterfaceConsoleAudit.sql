﻿CREATE TABLE [dbo].[InterfaceConsoleAudit] (
    [InterfaceConsoleAuditId] INT          IDENTITY (1, 1) NOT NULL,
    [InsertDate]              DATETIME     CONSTRAINT [DF__Interface__Inser__08925646] DEFAULT (getdate()) NULL,
    [ProcName]                [sysname]    NOT NULL,
    [InterfaceTableId]        INT          NULL,
    [DocumentType]            VARCHAR (50) NULL,
    [RecordStatus]            VARCHAR (50) NULL,
    [MessageStatus]           VARCHAR (50) NULL,
    [StartDate]               DATETIME     NULL,
    [EndDate]                 DATETIME     NULL,
    [InterfaceId]             INT          NULL,
    [Reprocess]               VARCHAR (10) NULL,
    [Key]                     INT          NULL,
    PRIMARY KEY CLUSTERED ([InterfaceConsoleAuditId] ASC) WITH (FILLFACTOR = 90)
);

