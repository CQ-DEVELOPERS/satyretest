﻿CREATE TABLE [dbo].[MenuItemCultureHistory] (
    [MenuItemCultureId] INT            NULL,
    [MenuItemId]        INT            NULL,
    [CultureId]         INT            NULL,
    [MenuItemText]      NVARCHAR (50)  NULL,
    [ToolTip]           NVARCHAR (255) NULL,
    [CommandType]       NVARCHAR (10)  NOT NULL,
    [InsertDate]        DATETIME       NOT NULL
);

