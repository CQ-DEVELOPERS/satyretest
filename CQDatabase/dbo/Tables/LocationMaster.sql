﻿CREATE TABLE [dbo].[LocationMaster] (
    [WarehouseId]       INT             NULL,
    [AreaId]            INT             NULL,
    [LocationId]        INT             NULL,
    [StorageUnitId]     INT             NULL,
    [LocationMasterId]  INT             IDENTITY (1, 1) NOT NULL,
    [RecordStatus]      CHAR (1)        DEFAULT ('N') NULL,
    [InsertDate]        DATETIME        CONSTRAINT [DF__LocationM__Inser__1E819765] DEFAULT (getdate()) NULL,
    [ProcessedDate]     DATETIME        NULL,
    [Warehouse]         NVARCHAR (50)   NULL,
    [WarehouseCode]     NVARCHAR (30)   NULL,
    [Area]              NVARCHAR (50)   NULL,
    [AreaCode]          NVARCHAR (10)   NULL,
    [Location]          NVARCHAR (15)   NULL,
    [Ailse]             NVARCHAR (10)   NULL,
    [Column]            NVARCHAR (10)   NULL,
    [Level]             NVARCHAR (10)   NULL,
    [SecurityCode]      INT             NULL,
    [RelativeValue]     FLOAT (53)      NULL,
    [ActiveBinning]     BIT             NULL,
    [ActivePicking]     BIT             NULL,
    [PalletQuantity]    FLOAT (53)      NULL,
    [ProductCode]       NVARCHAR (MAX)  NULL,
    [SKUCode]           NVARCHAR (MAX)  NULL,
    [MinimumQuantity]   FLOAT (53)      NULL,
    [HandlingQuantity]  FLOAT (53)      NULL,
    [MaximumQuantity]   FLOAT (53)      NULL,
    [LocationType]      NVARCHAR (100)  NULL,
    [NumberOfSUB]       INT             NULL,
    [NumberOfSU]        INT             NULL,
    [Width]             NUMERIC (13, 6) NULL,
    [Length]            NUMERIC (13, 6) NULL,
    [Height]            NUMERIC (13, 6) NULL,
    [Volume]            NUMERIC (13, 6) NULL,
    [Weight]            NUMERIC (13, 6) NULL,
    [TrackLPN]          BIT             NULL,
    [FreightForwarding] NVARCHAR (100)  NULL,
    PRIMARY KEY CLUSTERED ([LocationMasterId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Area] ([AreaId])
);


GO
CREATE NONCLUSTERED INDEX [IX_LocationMaster_ProcessedDate]
    ON [dbo].[LocationMaster]([ProcessedDate] ASC) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_LocationMaster_AreaId_ProcessedDate]
    ON [dbo].[LocationMaster]([AreaId] ASC, [ProcessedDate] ASC) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_LocationMaster_ProcessedDate_AreaId_LocationId]
    ON [dbo].[LocationMaster]([ProcessedDate] ASC, [AreaId] ASC, [LocationId] ASC) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_LocationMaster_ProcessedDate_LocationId_StorageUnitId]
    ON [dbo].[LocationMaster]([ProcessedDate] ASC, [LocationId] ASC, [StorageUnitId] ASC) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_LocationMaster_LocationId_ProcessedDate]
    ON [dbo].[LocationMaster]([LocationId] ASC, [ProcessedDate] ASC) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_LocationMaster_WarehouseId_ProcessedDate]
    ON [dbo].[LocationMaster]([WarehouseId] ASC, [ProcessedDate] ASC) WITH (FILLFACTOR = 70);

