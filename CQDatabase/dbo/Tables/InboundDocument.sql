﻿CREATE TABLE [dbo].[InboundDocument] (
    [InboundDocumentId]     INT            IDENTITY (1, 1) NOT NULL,
    [InboundDocumentTypeId] INT            NOT NULL,
    [ExternalCompanyId]     INT            NULL,
    [StatusId]              INT            NOT NULL,
    [WarehouseId]           INT            NOT NULL,
    [OrderNumber]           NVARCHAR (30)  NOT NULL,
    [DeliveryDate]          DATETIME       NOT NULL,
    [CreateDate]            DATETIME       NOT NULL,
    [ModifiedDate]          DATETIME       NULL,
    [ReferenceNumber]       NVARCHAR (30)  NULL,
    [FromLocation]          NVARCHAR (50)  NULL,
    [ToLocation]            NVARCHAR (50)  NULL,
    [ReasonId]              INT            NULL,
    [Remarks]               NVARCHAR (255) NULL,
    [StorageUnitId]         INT            NULL,
    [BatchId]               INT            NULL,
    [DivisionId]            INT            NULL,
    [PrincipalId]           INT            NULL,
    CONSTRAINT [InboundDocument_PK] PRIMARY KEY CLUSTERED ([InboundDocumentId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [ExternalCompany_InboundDocument_FK1] FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    CONSTRAINT [FK__InboundDo__Batch__0DCF0841] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    CONSTRAINT [FK__InboundDo__Divis__004C869F] FOREIGN KEY ([DivisionId]) REFERENCES [dbo].[Division] ([DivisionId]),
    CONSTRAINT [FK__InboundDo__Princ__64BFD57B] FOREIGN KEY ([PrincipalId]) REFERENCES [dbo].[Principal] ([PrincipalId]),
    CONSTRAINT [FK__InboundDo__Reaso__0EC32C7A] FOREIGN KEY ([ReasonId]) REFERENCES [dbo].[Reason] ([ReasonId]),
    CONSTRAINT [FK__InboundDo__Stora__0FB750B3] FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId]),
    CONSTRAINT [FK_InboundDocument_InboundDocumentType] FOREIGN KEY ([InboundDocumentTypeId]) REFERENCES [dbo].[InboundDocumentType] ([InboundDocumentTypeId]),
    CONSTRAINT [Status_InboundDocument_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [Warehouse_InboundDocument_FK1] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocument_ReasonId]
    ON [dbo].[InboundDocument]([ReasonId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocument_InboundDocumentTypeId]
    ON [dbo].[InboundDocument]([InboundDocumentTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocument_StatusId]
    ON [dbo].[InboundDocument]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocument_ExternalCompanyId]
    ON [dbo].[InboundDocument]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocument_StorageUnitId]
    ON [dbo].[InboundDocument]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocument_WarehouseId]
    ON [dbo].[InboundDocument]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocument_BatchId]
    ON [dbo].[InboundDocument]([BatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocument_DivisionId]
    ON [dbo].[InboundDocument]([DivisionId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocument_PrincipalId]
    ON [dbo].[InboundDocument]([PrincipalId] ASC) WITH (FILLFACTOR = 90);

