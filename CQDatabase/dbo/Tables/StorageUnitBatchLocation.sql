﻿CREATE TABLE [dbo].[StorageUnitBatchLocation] (
    [StorageUnitBatchId] INT        NOT NULL,
    [LocationId]         INT        NOT NULL,
    [ActualQuantity]     FLOAT (53) CONSTRAINT [DF_StorageUnitBatchLocation_ActualQuantity] DEFAULT ((0)) NOT NULL,
    [AllocatedQuantity]  FLOAT (53) CONSTRAINT [DF_StorageUnitBatchLocation_AllocatedQuantity] DEFAULT ((0)) NOT NULL,
    [ReservedQuantity]   FLOAT (53) CONSTRAINT [DF_StorageUnitBatchLocation_ReservedQuantity] DEFAULT ((0)) NOT NULL,
    [NettWeight]         FLOAT (53) NULL,
    CONSTRAINT [SUBLocation_PK] PRIMARY KEY CLUSTERED ([StorageUnitBatchId] ASC, [LocationId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Location_SUBLocation_FK1] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [StorageUnitBatch_SUBLocation_FK1] FOREIGN KEY ([StorageUnitBatchId]) REFERENCES [dbo].[StorageUnitBatch] ([StorageUnitBatchId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitBatchLocation_LocationId]
    ON [dbo].[StorageUnitBatchLocation]([LocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitBatchLocation_StorageUnitBatchId]
    ON [dbo].[StorageUnitBatchLocation]([StorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_StorageUnitBatchLocation_ActualQuantity_AllocatedQuantity_ReservedQuantity]
    ON [dbo].[StorageUnitBatchLocation]([ActualQuantity] ASC, [AllocatedQuantity] ASC, [ReservedQuantity] ASC) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_StorageUnitBatchLocation_AllocatedQuantity]
    ON [dbo].[StorageUnitBatchLocation]([AllocatedQuantity] ASC)
    INCLUDE([StorageUnitBatchId], [LocationId]) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_StorageUnitBatchLocation_ReservedQuantity]
    ON [dbo].[StorageUnitBatchLocation]([ReservedQuantity] ASC)
    INCLUDE([StorageUnitBatchId], [LocationId]) WITH (FILLFACTOR = 70);

