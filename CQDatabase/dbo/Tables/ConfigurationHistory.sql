﻿CREATE TABLE [dbo].[ConfigurationHistory] (
    [ConfigurationId] INT            NULL,
    [ModuleId]        INT            NULL,
    [Configuration]   NVARCHAR (255) NOT NULL,
    [Indicator]       BIT            NULL,
    [IntegerValue]    INT            NULL,
    [Value]           SQL_VARIANT    NULL,
    [CommandType]     NVARCHAR (10)  NOT NULL,
    [InsertDate]      DATETIME       NOT NULL,
    [WarehouseId]     INT            NULL
);

