﻿CREATE TABLE [dbo].[OutboundPerformance] (
    [JobId]            INT      NOT NULL,
    [CreateDate]       DATETIME NULL,
    [Release]          DATETIME NULL,
    [StartDate]        DATETIME NULL,
    [EndDate]          DATETIME NULL,
    [Checking]         DATETIME NULL,
    [Despatch]         DATETIME NULL,
    [DespatchChecked]  DATETIME NULL,
    [Complete]         DATETIME NULL,
    [Checked]          DATETIME NULL,
    [PlanningComplete] DATETIME NULL,
    [WarehouseId]      INT      NULL,
    PRIMARY KEY CLUSTERED ([JobId] ASC) WITH (FILLFACTOR = 90)
);

