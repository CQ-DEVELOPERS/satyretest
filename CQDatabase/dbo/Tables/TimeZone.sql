﻿CREATE TABLE [dbo].[TimeZone] (
    [TimeZoneId]   INT            IDENTITY (1, 1) NOT NULL,
    [TimeZoneCode] NVARCHAR (10)  NULL,
    [TimeZone]     NVARCHAR (255) NULL,
    [Location]     NVARCHAR (255) NULL,
    [Conversion]   NVARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([TimeZoneId] ASC) WITH (FILLFACTOR = 90)
);

