﻿CREATE TABLE [dbo].[IssueLineInstruction] (
    [OutboundDocumentTypeId] INT        NULL,
    [OutboundShipmentId]     INT        NULL,
    [OutboundDocumentId]     INT        NULL,
    [IssueId]                INT        NULL,
    [IssueLineId]            INT        NOT NULL,
    [InstructionId]          INT        NOT NULL,
    [DropSequence]           INT        NULL,
    [Quantity]               FLOAT (53) NULL,
    [ConfirmedQuantity]      FLOAT (53) NULL,
    [Weight]                 FLOAT (53) NULL,
    [ConfirmedWeight]        FLOAT (53) NULL,
    CONSTRAINT [IssueLineInstruction_PK] PRIMARY KEY CLUSTERED ([IssueLineId] ASC, [InstructionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK__IssueLine__Instr__2866FB9F] FOREIGN KEY ([InstructionId]) REFERENCES [dbo].[Instruction] ([InstructionId]),
    CONSTRAINT [FK__IssueLine__Issue__2772D766] FOREIGN KEY ([IssueLineId]) REFERENCES [dbo].[IssueLine] ([IssueLineId])
);


GO
CREATE NONCLUSTERED INDEX [nci_IssueLineInstruction_InstructionId]
    ON [dbo].[IssueLineInstruction]([InstructionId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IssueLineInstruction_IssueLineId]
    ON [dbo].[IssueLineInstruction]([IssueLineId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InstructionId]
    ON [dbo].[IssueLineInstruction]([InstructionId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IssueLineInstruction]
    ON [dbo].[IssueLineInstruction]([OutboundDocumentTypeId] ASC, [OutboundShipmentId] ASC, [OutboundDocumentId] ASC, [IssueId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_IssueLineInstruction_IssueId]
    ON [dbo].[IssueLineInstruction]([IssueId] ASC);

