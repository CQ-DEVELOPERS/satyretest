﻿CREATE TABLE [dbo].[DashboardInboundPutawayAchieved] (
    [WarehouseId] INT           NULL,
    [OperatorId]  INT           NULL,
    [CreateDate]  DATETIME      NULL,
    [Legend]      NVARCHAR (50) NULL,
    [Value]       INT           NULL,
    [Pallets]     INT           NULL,
    [KPI]         INT           NULL
);

