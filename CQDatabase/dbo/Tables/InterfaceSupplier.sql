﻿CREATE TABLE [dbo].[InterfaceSupplier] (
    [HostId]        NVARCHAR (30)  NULL,
    [SupplierCode]  NVARCHAR (30)  NULL,
    [SupplierName]  NVARCHAR (255) NULL,
    [Address1]      NVARCHAR (255) NULL,
    [Address2]      NVARCHAR (255) NULL,
    [Address3]      NVARCHAR (255) NULL,
    [Address4]      NVARCHAR (255) NULL,
    [Modified]      NVARCHAR (10)  NULL,
    [ContactPerson] NVARCHAR (100) NULL,
    [Phone]         NVARCHAR (255) NULL,
    [Fax]           NVARCHAR (255) NULL,
    [Email]         NVARCHAR (255) NULL,
    [ProcessedDate] DATETIME       NULL,
    [RecordStatus]  CHAR (1)       DEFAULT ('N') NULL,
    [InsertDate]    DATETIME       CONSTRAINT [DF_InterfaceSupplier_InsertDate] DEFAULT (getdate()) NULL,
    [PrincipalCode] NVARCHAR (30)  NULL
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceSupplier_Status_Dates]
    ON [dbo].[InterfaceSupplier]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceSupplier_ProcessedDate]
    ON [dbo].[InterfaceSupplier]([ProcessedDate] ASC)
    INCLUDE([HostId], [SupplierCode], [SupplierName], [Address1], [Address2], [Address3], [Address4], [Modified], [ContactPerson], [Phone], [Fax], [Email]);

