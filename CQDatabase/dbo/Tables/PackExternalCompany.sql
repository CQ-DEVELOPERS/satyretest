﻿CREATE TABLE [dbo].[PackExternalCompany] (
    [WarehouseId]       INT        NOT NULL,
    [ExternalCompanyId] INT        NOT NULL,
    [StorageUnitId]     INT        NOT NULL,
    [FromPackId]        INT        NOT NULL,
    [FromQuantity]      FLOAT (53) NOT NULL,
    [ToPackId]          INT        NOT NULL,
    [ToQuantity]        FLOAT (53) NOT NULL,
    [Comments]          NTEXT      NULL,
    CONSTRAINT [pk_PackExternalCompany] PRIMARY KEY CLUSTERED ([WarehouseId] ASC, [StorageUnitId] ASC, [ExternalCompanyId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    FOREIGN KEY ([FromPackId]) REFERENCES [dbo].[Pack] ([PackId]),
    FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId]),
    FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_PackExternalCompany_StorageUnitId]
    ON [dbo].[PackExternalCompany]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_PackExternalCompany_ToPackId]
    ON [dbo].[PackExternalCompany]([ToPackId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_PackExternalCompany_ExternalCompanyId]
    ON [dbo].[PackExternalCompany]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_PackExternalCompany_FromPackId]
    ON [dbo].[PackExternalCompany]([FromPackId] ASC) WITH (FILLFACTOR = 90);

