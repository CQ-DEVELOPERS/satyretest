﻿CREATE TABLE [dbo].[DockSchedule] (
    [DockScheduleId]     INT             IDENTITY (1, 1) NOT NULL,
    [OutboundShipmentId] INT             NULL,
    [IssueId]            INT             NULL,
    [InboundShipmentId]  INT             NULL,
    [ReceiptId]          INT             NULL,
    [Subject]            NVARCHAR (510)  NOT NULL,
    [PlannedStart]       DATETIME        NOT NULL,
    [PlannedEnd]         DATETIME        NOT NULL,
    [ActualStart]        DATETIME        NULL,
    [ActualEnd]          DATETIME        NULL,
    [LocationId]         INT             NULL,
    [RecurrenceRule]     NVARCHAR (1024) NULL,
    [RecurrenceParentID] INT             NULL,
    [Description]        NVARCHAR (MAX)  NULL,
    [Version]            INT             NULL,
    [CssClass]           NVARCHAR (50)   NULL,
    PRIMARY KEY CLUSTERED ([DockScheduleId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([InboundShipmentId]) REFERENCES [dbo].[InboundShipment] ([InboundShipmentId]),
    FOREIGN KEY ([IssueId]) REFERENCES [dbo].[Issue] ([IssueId]),
    FOREIGN KEY ([OutboundShipmentId]) REFERENCES [dbo].[OutboundShipment] ([OutboundShipmentId]),
    FOREIGN KEY ([ReceiptId]) REFERENCES [dbo].[Receipt] ([ReceiptId])
);


GO
CREATE NONCLUSTERED INDEX [nci_DockSchedule_OutboundShipmentId]
    ON [dbo].[DockSchedule]([OutboundShipmentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DockSchedule_InboundShipmentId]
    ON [dbo].[DockSchedule]([InboundShipmentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DockSchedule_IssueId]
    ON [dbo].[DockSchedule]([IssueId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DockSchedule_ReceiptId]
    ON [dbo].[DockSchedule]([ReceiptId] ASC) WITH (FILLFACTOR = 90);

