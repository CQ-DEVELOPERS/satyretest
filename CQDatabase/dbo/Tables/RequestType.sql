﻿CREATE TABLE [dbo].[RequestType] (
    [RequestTypeId]   INT           IDENTITY (1, 1) NOT NULL,
    [RequestTypeCode] NVARCHAR (20) NULL,
    [RequestType]     NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([RequestTypeId] ASC) WITH (FILLFACTOR = 90)
);

