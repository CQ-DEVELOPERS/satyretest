﻿CREATE TABLE [dbo].[IntransitLoad] (
    [IntransitLoadId]     INT            IDENTITY (1, 1) NOT NULL,
    [WarehouseId]         INT            NULL,
    [StatusId]            INT            NULL,
    [DeliveryNoteNumber]  NVARCHAR (30)  NULL,
    [DeliveryDate]        DATETIME       NULL,
    [SealNumber]          NVARCHAR (50)  NULL,
    [VehicleRegistration] NVARCHAR (50)  NULL,
    [Route]               NVARCHAR (50)  NULL,
    [DriverId]            INT            NULL,
    [Remarks]             NVARCHAR (MAX) NULL,
    [CreatedById]         INT            NULL,
    [ReceivedById]        INT            NULL,
    [CreateDate]          DATETIME       NULL,
    [LoadClosed]          DATETIME       NULL,
    [ReceivedDate]        DATETIME       NULL,
    [Offloaded]           DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([IntransitLoadId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK__Intransit__Creat__30B0922F] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [FK__Intransit__Drive__2FBC6DF6] FOREIGN KEY ([DriverId]) REFERENCES [dbo].[ContactList] ([ContactListId]),
    CONSTRAINT [FK__Intransit__Statu__2EC849BD] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [FK__Intransit__Wareh__2DD42584] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_IntransitLoad_CreatedById]
    ON [dbo].[IntransitLoad]([CreatedById] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IntransitLoad_ReceivedById]
    ON [dbo].[IntransitLoad]([ReceivedById] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IntransitLoad_DriverId]
    ON [dbo].[IntransitLoad]([DriverId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IntransitLoad_StatusId]
    ON [dbo].[IntransitLoad]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IntransitLoad_WarehouseId]
    ON [dbo].[IntransitLoad]([WarehouseId] ASC) WITH (FILLFACTOR = 90);

