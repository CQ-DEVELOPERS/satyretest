﻿CREATE TABLE [dbo].[InterfaceExportOrderPick] (
    [InterfaceExportOrderPickId] INT           IDENTITY (1, 1) NOT NULL,
    [IssueId]                    INT           NULL,
    [OrderNumber]                NVARCHAR (30) NULL,
    [RecordStatus]               CHAR (1)      DEFAULT ('N') NOT NULL,
    [ProcessedDate]              DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([InterfaceExportOrderPickId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceExportOrderPick_IssueId]
    ON [dbo].[InterfaceExportOrderPick]([IssueId] ASC) WITH (FILLFACTOR = 70);

