﻿CREATE TABLE [dbo].[ContactList] (
    [ContactListId]     INT            IDENTITY (1, 1) NOT NULL,
    [ExternalCompanyId] INT            NULL,
    [OperatorId]        INT            NULL,
    [ContactPerson]     NVARCHAR (255) NULL,
    [Telephone]         NVARCHAR (255) NULL,
    [Fax]               NVARCHAR (255) NULL,
    [EMail]             NVARCHAR (255) NULL,
    [Alias]             VARCHAR (50)   NULL,
    [Type]              NVARCHAR (20)  NULL,
    [ReportTypeId]      INT            NULL,
    CONSTRAINT [ContactList_PK] PRIMARY KEY CLUSTERED ([ContactListId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([ReportTypeId]) REFERENCES [dbo].[ReportType] ([ReportTypeId]),
    CONSTRAINT [ExternalCompany_ContactList_FK1] FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    CONSTRAINT [Operators_ContactList_FK1] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ContactList_ExternalCompanyId]
    ON [dbo].[ContactList]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ContactList_ReportTypeId]
    ON [dbo].[ContactList]([ReportTypeId] ASC) WITH (FILLFACTOR = 90);

