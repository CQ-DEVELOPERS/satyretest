﻿CREATE TABLE [dbo].[OperatorTransactionSummary] (
    [OperatorId]        INT             NULL,
    [InstructionTypeId] INT             NULL,
    [EndDate]           DATETIME        NULL,
    [Units]             INT             NULL,
    [Weight]            NUMERIC (13, 3) NULL,
    [Instructions]      INT             NULL,
    [Jobs]              INT             NULL,
    [ActiveTime]        INT             NULL,
    [DwellTime]         INT             NULL,
    [WarehouseId]       INT             NULL,
    CONSTRAINT [FK__OperatorT__Opera__1EC94E23] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId])
);

