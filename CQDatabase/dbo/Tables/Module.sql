﻿CREATE TABLE [dbo].[Module] (
    [ModuleId] INT           IDENTITY (1, 1) NOT NULL,
    [Module]   NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED ([ModuleId] ASC) WITH (FILLFACTOR = 90)
);

