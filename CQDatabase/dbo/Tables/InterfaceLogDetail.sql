﻿CREATE TABLE [dbo].[InterfaceLogDetail] (
    [InterfaceLogDetailId] INT           IDENTITY (1, 1) NOT NULL,
    [OrderNumber]          NVARCHAR (30) NULL,
    [ReceivedByHost]       DATETIME      NULL,
    [ProcessedDate]        DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([InterfaceLogDetailId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceLogDetail]
    ON [dbo].[InterfaceLogDetail]([OrderNumber] ASC) WITH (FILLFACTOR = 90);

