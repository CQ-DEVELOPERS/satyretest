﻿CREATE TABLE [dbo].[StockTakeLog_Backup] (
    [StockTakeLogId]    INT             IDENTITY (1, 1) NOT NULL,
    [ProductCode]       NVARCHAR (50)   NULL,
    [Product]           NVARCHAR (255)  NULL,
    [SKUCode]           NVARCHAR (50)   NULL,
    [Batch]             NVARCHAR (50)   NULL,
    [Quantity]          NUMERIC (13, 6) NULL,
    [ConfirmedQuantity] NUMERIC (13, 6) NULL,
    [ActualQuantity]    NUMERIC (13, 6) NULL,
    [AllocatedQuantity] NUMERIC (13, 6) NULL,
    [ReservedQuantity]  NUMERIC (13, 6) NULL,
    [InsertDate]        DATETIME        NULL,
    [Location]          NVARCHAR (15)   NULL,
    [Area]              NVARCHAR (50)   NULL,
    [InstructionId]     INT             NULL
);

