﻿CREATE TABLE [dbo].[NotificationMethod] (
    [NotificationMethodId]   INT           IDENTITY (1, 1) NOT NULL,
    [NotificationMethodCode] NVARCHAR (10) NULL,
    [NotificationMethod]     NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([NotificationMethodId] ASC) WITH (FILLFACTOR = 90)
);

