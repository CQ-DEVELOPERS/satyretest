﻿CREATE TABLE [dbo].[InterfaceImportIVHeader] (
    [InterfaceImportIVHeaderId] INT             IDENTITY (1, 1) NOT NULL,
    [PrimaryKey]                NVARCHAR (30)   NULL,
    [OrderNumber]               NVARCHAR (30)   NULL,
    [InvoiceNumber]             NVARCHAR (30)   NULL,
    [RecordType]                NVARCHAR (30)   DEFAULT ('INV') NULL,
    [RecordStatus]              CHAR (1)        DEFAULT ('N') NOT NULL,
    [CustomerCode]              NVARCHAR (30)   NULL,
    [Customer]                  NVARCHAR (255)  NULL,
    [Address]                   NVARCHAR (255)  NULL,
    [FromWarehouseCode]         NVARCHAR (30)   NULL,
    [ToWarehouseCode]           NVARCHAR (30)   NULL,
    [Route]                     NVARCHAR (50)   NULL,
    [DeliveryDate]              NVARCHAR (20)   NULL,
    [Remarks]                   NVARCHAR (255)  NULL,
    [NumberOfLines]             INT             NULL,
    [VatPercentage]             DECIMAL (13, 3) NULL,
    [VatSummary]                DECIMAL (13, 3) NULL,
    [Total]                     DECIMAL (13, 3) NULL,
    [Additional1]               NVARCHAR (255)  NULL,
    [Additional2]               NVARCHAR (255)  NULL,
    [Additional3]               NVARCHAR (255)  NULL,
    [Additional4]               NVARCHAR (255)  NULL,
    [Additional5]               NVARCHAR (255)  NULL,
    [Additional6]               NVARCHAR (255)  NULL,
    [Additional7]               NVARCHAR (255)  NULL,
    [Additional8]               NVARCHAR (255)  NULL,
    [Additional9]               NVARCHAR (255)  NULL,
    [Additional10]              NVARCHAR (255)  NULL,
    [ProcessedDate]             DATETIME        NULL,
    [InsertDate]                DATETIME        CONSTRAINT [DF_InterfaceImportSVHeader_InsertDate] DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([InterfaceImportIVHeaderId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportIVHeader_Status_Dates]
    ON [dbo].[InterfaceImportIVHeader]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

