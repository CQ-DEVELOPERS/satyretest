﻿CREATE TABLE [dbo].[MenuHistory] (
    [MenuId]      INT           NULL,
    [MenuText]    NVARCHAR (50) NULL,
    [CommandType] NVARCHAR (10) NOT NULL,
    [InsertDate]  DATETIME      NOT NULL
);

