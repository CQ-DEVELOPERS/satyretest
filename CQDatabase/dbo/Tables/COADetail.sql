﻿CREATE TABLE [dbo].[COADetail] (
    [COADetailId] INT            IDENTITY (1, 1) NOT NULL,
    [COAId]       INT            NULL,
    [NoteId]      INT            NULL,
    [NoteCode]    NVARCHAR (255) NULL,
    [Note]        NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([COADetailId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([COAId]) REFERENCES [dbo].[COA] ([COAId]),
    FOREIGN KEY ([NoteId]) REFERENCES [dbo].[Note] ([NoteId])
);


GO
CREATE NONCLUSTERED INDEX [nci_COADetail_COAId]
    ON [dbo].[COADetail]([COAId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_COADetail_NoteId]
    ON [dbo].[COADetail]([NoteId] ASC) WITH (FILLFACTOR = 90);

