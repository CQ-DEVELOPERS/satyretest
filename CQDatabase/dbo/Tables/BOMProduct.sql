﻿CREATE TABLE [dbo].[BOMProduct] (
    [BOMLineId]     INT        NOT NULL,
    [LineNumber]    INT        NOT NULL,
    [StorageUnitId] INT        NOT NULL,
    [Quantity]      FLOAT (53) NOT NULL,
    CONSTRAINT [PK_BOMProduct] PRIMARY KEY CLUSTERED ([BOMLineId] ASC, [LineNumber] ASC, [StorageUnitId] ASC) WITH (FILLFACTOR = 90)
);

