﻿CREATE TABLE [dbo].[InterfaceDocumentDesc] (
    [DocId]   INT           IDENTITY (1, 1) NOT NULL,
    [Doc]     NVARCHAR (10) NULL,
    [DocDesc] NVARCHAR (30) NOT NULL,
    CONSTRAINT [InterfaceDocumentDesc_PK] PRIMARY KEY CLUSTERED ([DocId] ASC) WITH (FILLFACTOR = 90)
);

