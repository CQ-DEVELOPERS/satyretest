﻿CREATE TABLE [dbo].[InboundShipment] (
    [InboundShipmentId] INT            IDENTITY (1, 1) NOT NULL,
    [StatusId]          INT            NOT NULL,
    [WarehouseId]       INT            NOT NULL,
    [ShipmentDate]      DATETIME       NOT NULL,
    [Remarks]           NVARCHAR (255) NULL,
    [LocationId]        INT            NULL,
    [VehicleId]         INT            NULL,
    CONSTRAINT [PK_InboundShipment] PRIMARY KEY CLUSTERED ([InboundShipmentId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[Vehicle] ([VehicleId]),
    FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InboundShipment_LocationId]
    ON [dbo].[InboundShipment]([LocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundShipment_VehicleId]
    ON [dbo].[InboundShipment]([VehicleId] ASC) WITH (FILLFACTOR = 90);

