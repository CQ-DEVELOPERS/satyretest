﻿CREATE TABLE [dbo].[AreaOperator] (
    [AreaId]     INT NOT NULL,
    [OperatorId] INT NOT NULL,
    CONSTRAINT [AreaOperator_PK] PRIMARY KEY CLUSTERED ([AreaId] ASC, [OperatorId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Area_AreaOperator_FK1] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Area] ([AreaId]),
    CONSTRAINT [Operators_AreaOperator_FK1] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId])
);


GO
CREATE NONCLUSTERED INDEX [nci_AreaOperator_OperatorId]
    ON [dbo].[AreaOperator]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_AreaOperator_AreaId]
    ON [dbo].[AreaOperator]([AreaId] ASC) WITH (FILLFACTOR = 90);

