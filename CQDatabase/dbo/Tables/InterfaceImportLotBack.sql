﻿CREATE TABLE [dbo].[InterfaceImportLotBack] (
    [InterfaceImportLotId] INT            IDENTITY (1, 1) NOT NULL,
    [RecordStatus]         NVARCHAR (2)   NULL,
    [InsertDate]           DATETIME       NULL,
    [ProcessedDate]        DATETIME       NULL,
    [LotId]                NVARCHAR (50)  NULL,
    [Code]                 NVARCHAR (255) NULL,
    [WhseId]               NVARCHAR (50)  NULL,
    [Whse]                 NVARCHAR (255) NULL,
    [QtyOnHand]            NVARCHAR (50)  NULL,
    [QtyFree]              NVARCHAR (50)  NULL,
    [LotStatusId]          NVARCHAR (50)  NULL,
    [LotStatus]            NVARCHAR (100) NULL,
    [ExpiryDate]           DATETIME       NULL
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportLotBack_Status_Dates]
    ON [dbo].[InterfaceImportLotBack]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

