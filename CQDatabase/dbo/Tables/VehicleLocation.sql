﻿CREATE TABLE [dbo].[VehicleLocation] (
    [VehicleId]     INT NOT NULL,
    [LocationId]    INT NOT NULL,
    [MinutesToLoad] INT NULL,
    FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    FOREIGN KEY ([VehicleId]) REFERENCES [dbo].[Vehicle] ([VehicleId])
);


GO
CREATE NONCLUSTERED INDEX [nci_VehicleLocation_VehicleId]
    ON [dbo].[VehicleLocation]([VehicleId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_VehicleLocation_LocationId]
    ON [dbo].[VehicleLocation]([LocationId] ASC) WITH (FILLFACTOR = 90);

