﻿CREATE TABLE [dbo].[DeliveryAdviceDetail] (
    [DeliveryAdviceDetailId] INT        IDENTITY (1, 1) NOT NULL,
    [DeliveryAdviceHeaderId] INT        NULL,
    [PurchaseOrderHeaderId]  INT        NULL,
    [PurchaseOrderDetailId]  INT        NULL,
    [CallOffHeaderId]        INT        NULL,
    [CallOffDetailId]        INT        NULL,
    [StorageUnitId]          INT        NULL,
    [BatchId]                INT        NULL,
    [RequiredQuantity]       FLOAT (53) NULL,
    [ShipmentQuantity]       FLOAT (53) NULL,
    [ReceivedQuantity]       FLOAT (53) NULL,
    [StatusId]               INT        NULL,
    [ShipmentDetailId]       INT        NULL,
    [UnitPrice]              FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([DeliveryAdviceDetailId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CallOffDetailId]) REFERENCES [dbo].[CallOffDetail] ([CallOffDetailId]),
    FOREIGN KEY ([CallOffHeaderId]) REFERENCES [dbo].[CallOffHeader] ([CallOffHeaderId]),
    FOREIGN KEY ([DeliveryAdviceHeaderId]) REFERENCES [dbo].[DeliveryAdviceHeader] ([DeliveryAdviceHeaderId]),
    FOREIGN KEY ([PurchaseOrderDetailId]) REFERENCES [dbo].[PurchaseOrderDetail] ([PurchaseOrderDetailId]),
    FOREIGN KEY ([PurchaseOrderHeaderId]) REFERENCES [dbo].[PurchaseOrderHeader] ([PurchaseOrderHeaderId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId])
);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceDetail_CallOffDetailId]
    ON [dbo].[DeliveryAdviceDetail]([CallOffDetailId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceDetail_StatusId]
    ON [dbo].[DeliveryAdviceDetail]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceDetail_DeliveryAdviceHeaderId]
    ON [dbo].[DeliveryAdviceDetail]([DeliveryAdviceHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceDetail_PurchaseOrderDetailId]
    ON [dbo].[DeliveryAdviceDetail]([PurchaseOrderDetailId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceDetail_PurchaseOrderHeaderId]
    ON [dbo].[DeliveryAdviceDetail]([PurchaseOrderHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_DeliveryAdviceDetail_CallOffHeaderId]
    ON [dbo].[DeliveryAdviceDetail]([CallOffHeaderId] ASC) WITH (FILLFACTOR = 90);

