﻿CREATE TABLE [dbo].[InterfaceMessage] (
    [InterfaceMessageId]   INT            IDENTITY (1, 1) NOT NULL,
    [InterfaceMessageCode] NVARCHAR (50)  NULL,
    [InterfaceMessage]     NVARCHAR (MAX) NULL,
    [InterfaceId]          INT            NULL,
    [InterfaceTable]       [sysname]      NULL,
    [Status]               NVARCHAR (20)  NULL,
    [OrderNumber]          NVARCHAR (30)  NULL,
    [CreateDate]           DATETIME       NULL,
    [ProcessedDate]        DATETIME       NULL,
    [InsertDate]           DATETIME       CONSTRAINT [DF__Interface__Inser__659EEB1B] DEFAULT (getdate()) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceMessage_InterfaceTable_InterfaceID]
    ON [dbo].[InterfaceMessage]([InterfaceTable] ASC, [InterfaceId] ASC)
    INCLUDE([Status]) WITH (FILLFACTOR = 90);

