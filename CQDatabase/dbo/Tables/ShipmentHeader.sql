﻿CREATE TABLE [dbo].[ShipmentHeader] (
    [ShipmentHeaderId]      INT            IDENTITY (1, 1) NOT NULL,
    [ShipmentNumber]        NVARCHAR (30)  NULL,
    [PurchaseOrderHeaderId] INT            NULL,
    [DocumentTypeId]        INT            NULL,
    [StatusId]              INT            NULL,
    [PriorityId]            INT            NULL,
    [ExternalCompanyId]     INT            NULL,
    [WarehouseId]           INT            NULL,
    [RequiredDate]          DATETIME       NULL,
    [DeliveryNoteNumber]    NVARCHAR (30)  NULL,
    [DeliveryDate]          DATETIME       NULL,
    [TransportModeId]       INT            NULL,
    [NotificationMethodId]  INT            NULL,
    [ConfirmReceipt]        BIT            DEFAULT ((0)) NULL,
    [ConfirmAccpeted]       BIT            DEFAULT ((0)) NULL,
    [NumberOfOrders]        INT            NULL,
    [NumberOfLines]         INT            NULL,
    [Remarks]               NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([ShipmentHeaderId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[InboundDocumentType] ([InboundDocumentTypeId]),
    FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    FOREIGN KEY ([NotificationMethodId]) REFERENCES [dbo].[NotificationMethod] ([NotificationMethodId]),
    FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    FOREIGN KEY ([PurchaseOrderHeaderId]) REFERENCES [dbo].[PurchaseOrderHeader] ([PurchaseOrderHeaderId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    FOREIGN KEY ([TransportModeId]) REFERENCES [dbo].[TransportMode] ([TransportModeId]),
    FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentHeader_PurchaseOrderHeaderId]
    ON [dbo].[ShipmentHeader]([PurchaseOrderHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentHeader_WarehouseId]
    ON [dbo].[ShipmentHeader]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentHeader_ExternalCompanyId]
    ON [dbo].[ShipmentHeader]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentHeader_NotificationMethodId]
    ON [dbo].[ShipmentHeader]([NotificationMethodId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentHeader_PriorityId]
    ON [dbo].[ShipmentHeader]([PriorityId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentHeader_TransportModeId]
    ON [dbo].[ShipmentHeader]([TransportModeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentHeader_DocumentTypeId]
    ON [dbo].[ShipmentHeader]([DocumentTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentHeader_StatusId]
    ON [dbo].[ShipmentHeader]([StatusId] ASC) WITH (FILLFACTOR = 90);

