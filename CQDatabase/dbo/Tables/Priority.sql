﻿CREATE TABLE [dbo].[Priority] (
    [PriorityId]     INT           IDENTITY (1, 1) NOT NULL,
    [Priority]       NVARCHAR (50) NOT NULL,
    [PriorityCode]   NVARCHAR (10) NOT NULL,
    [OrderBy]        INT           NOT NULL,
    [CheckingAreaId] INT           NULL,
    CONSTRAINT [Priority_PK] PRIMARY KEY CLUSTERED ([PriorityId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_Priority] UNIQUE NONCLUSTERED ([Priority] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_PriorityCode] UNIQUE NONCLUSTERED ([PriorityCode] ASC) WITH (FILLFACTOR = 90)
);

