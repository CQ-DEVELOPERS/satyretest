﻿CREATE TABLE [dbo].[InterfaceImportBOMDetail] (
    [InterfaceImportBOMId] INT            NULL,
    [ForeignKey]           NVARCHAR (30)  NOT NULL,
    [CompIndex]            INT            NULL,
    [CompProductCode]      NVARCHAR (30)  NULL,
    [CompProduct]          NVARCHAR (255) NULL,
    [CompQuantity]         FLOAT (53)     NULL,
    [HostId]               NVARCHAR (30)  NULL,
    [InsertDate]           DATETIME       CONSTRAINT [DF_InterfaceImportBOMDetail_InsertDate] DEFAULT (getdate()) NULL,
    [Additional1]          VARCHAR (255)  NULL,
    [Additional2]          NVARCHAR (255) NULL,
    [Additional3]          NVARCHAR (255) NULL,
    [Additional4]          NVARCHAR (255) NULL,
    [Additional5]          NVARCHAR (255) NULL,
    [Additional6]          NVARCHAR (255) NULL,
    [Additional7]          NVARCHAR (255) NULL,
    [Additional8]          NVARCHAR (255) NULL,
    [Additional9]          NVARCHAR (255) NULL
);

