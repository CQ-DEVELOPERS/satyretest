﻿CREATE TABLE [dbo].[LocationTypeHistory] (
    [LocationTypeId]            INT           NULL,
    [LocationType]              NVARCHAR (50) NULL,
    [LocationTypeCode]          NVARCHAR (10) NULL,
    [DeleteIndicator]           BIT           NULL,
    [ReplenishmentIndicator]    BIT           NULL,
    [MultipleProductsIndicator] BIT           NULL,
    [BatchTrackingIndicator]    BIT           NULL,
    [CommandType]               NVARCHAR (10) NOT NULL,
    [InsertDate]                DATETIME      NOT NULL
);

