﻿CREATE TABLE [dbo].[DashboardKPI] (
    [DashboardKPIId]    INT           IDENTITY (1, 1) NOT NULL,
    [FunctionId]        NVARCHAR (50) NULL,
    [OperatorKPI]       INT           NULL,
    [NumberOfOperators] INT           NULL,
    [WarehouseKPI]      INT           NULL,
    [Measure]           NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([DashboardKPIId] ASC) WITH (FILLFACTOR = 90)
);

