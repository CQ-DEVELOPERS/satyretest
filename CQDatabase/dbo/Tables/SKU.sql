﻿CREATE TABLE [dbo].[SKU] (
    [SKUId]            INT             IDENTITY (1, 1) NOT NULL,
    [UOMId]            INT             NOT NULL,
    [SKU]              NVARCHAR (50)   NOT NULL,
    [SKUCode]          NVARCHAR (50)   NULL,
    [Quantity]         FLOAT (53)      NOT NULL,
    [AlternatePallet]  INT             NULL,
    [SubstitutePallet] INT             NULL,
    [Litres]           NUMERIC (13, 3) NULL,
    [ParentSKUCode]    NVARCHAR (50)   NULL,
    CONSTRAINT [SKU_PK] PRIMARY KEY CLUSTERED ([SKUId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UOM_SKU_FK1] FOREIGN KEY ([UOMId]) REFERENCES [dbo].[UOM] ([UOMId]),
    CONSTRAINT [uc_SKUCode] UNIQUE NONCLUSTERED ([SKUCode] ASC) WITH (FILLFACTOR = 90)
);

