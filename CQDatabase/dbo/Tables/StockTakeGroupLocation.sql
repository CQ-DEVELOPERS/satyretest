﻿CREATE TABLE [dbo].[StockTakeGroupLocation] (
    [StockTakeGroupLocId] INT IDENTITY (1, 1) NOT NULL,
    [StockTakeGroupId]    INT NOT NULL,
    [LocationId]          INT NOT NULL,
    FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StockTakeGroupLocation_LocationId]
    ON [dbo].[StockTakeGroupLocation]([LocationId] ASC) WITH (FILLFACTOR = 90);

