﻿CREATE TABLE [dbo].[InterfacePackInfo] (
    [HostId]          NVARCHAR (30)  NULL,
    [ProductCode]     NVARCHAR (30)  NULL,
    [PackCode]        NVARCHAR (30)  NULL,
    [PackDescription] NVARCHAR (255) NULL,
    [UOM]             NVARCHAR (30)  NULL,
    [SaleUOM]         NVARCHAR (30)  NULL,
    [Ingredients]     NVARCHAR (255) NULL,
    [Quantity]        FLOAT (53)     NULL,
    [UnitSize]        INT            NULL,
    [Weight]          FLOAT (53)     NULL,
    [Length]          INT            NULL,
    [Width]           INT            NULL,
    [Height]          INT            NULL
);

