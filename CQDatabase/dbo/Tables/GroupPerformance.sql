﻿CREATE TABLE [dbo].[GroupPerformance] (
    [OperatorGroupId] INT             NULL,
    [PerformanceType] NVARCHAR (1)    NULL,
    [Units]           INT             NULL,
    [Weight]          FLOAT (53)      NULL,
    [Instructions]    NUMERIC (13, 3) NULL,
    [Orders]          NUMERIC (13, 3) NULL,
    [OrderLines]      NUMERIC (13, 3) NULL,
    [Jobs]            NUMERIC (13, 3) NULL,
    [QA]              NUMERIC (13, 3) NULL,
    FOREIGN KEY ([OperatorGroupId]) REFERENCES [dbo].[OperatorGroup] ([OperatorGroupId])
);


GO
CREATE NONCLUSTERED INDEX [nci_GroupPerformance_OperatorGroupId]
    ON [dbo].[GroupPerformance]([OperatorGroupId] ASC) WITH (FILLFACTOR = 90);

