﻿CREATE TABLE [dbo].[ShipmentSequence] (
    [ShipmentSequenceId]   INT            IDENTITY (1, 1) NOT NULL,
    [ShipmentHeaderId]     INT            NULL,
    [ExternalCompanyId]    INT            NULL,
    [TransportModeId]      INT            NULL,
    [NotificationMethodId] INT            NULL,
    [CollectionDate]       DATETIME       NULL,
    [ReceivedDate]         DATETIME       NULL,
    [DespatchDate]         DATETIME       NULL,
    [PlannedDelivery]      DATETIME       NULL,
    [Remarks]              NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([ShipmentSequenceId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId]),
    FOREIGN KEY ([NotificationMethodId]) REFERENCES [dbo].[NotificationMethod] ([NotificationMethodId]),
    FOREIGN KEY ([ShipmentHeaderId]) REFERENCES [dbo].[ShipmentHeader] ([ShipmentHeaderId]),
    FOREIGN KEY ([TransportModeId]) REFERENCES [dbo].[TransportMode] ([TransportModeId])
);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentSequence_ShipmentHeaderId]
    ON [dbo].[ShipmentSequence]([ShipmentHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentSequence_ExternalCompanyId]
    ON [dbo].[ShipmentSequence]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentSequence_NotificationMethodId]
    ON [dbo].[ShipmentSequence]([NotificationMethodId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_ShipmentSequence_TransportModeId]
    ON [dbo].[ShipmentSequence]([TransportModeId] ASC) WITH (FILLFACTOR = 90);

