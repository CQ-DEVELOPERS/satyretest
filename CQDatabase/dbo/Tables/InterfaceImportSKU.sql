﻿CREATE TABLE [dbo].[InterfaceImportSKU] (
    [InterfaceImportSKUId] INT             IDENTITY (1, 1) NOT NULL,
    [RecordStatus]         CHAR (1)        CONSTRAINT [DF_InterfaceImportSKU_RecordStatus] DEFAULT ('N') NULL,
    [SKUCode]              VARCHAR (10)    NULL,
    [SKU]                  VARCHAR (50)    NULL,
    [UnitOfMeasure]        VARCHAR (50)    NULL,
    [Quantity]             FLOAT (53)      NULL,
    [AlternatePallet]      INT             NULL,
    [SubstitutePallet]     INT             NULL,
    [Litres]               NUMERIC (13, 3) NULL,
    [HostId]               VARCHAR (30)    NULL,
    [ProcessedDate]        DATETIME        NULL,
    [Insertdate]           DATETIME        CONSTRAINT [DF_InterfaceImportSKU_Insertdate] DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([InterfaceImportSKUId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportSKU_Status_Dates]
    ON [dbo].[InterfaceImportSKU]([RecordStatus] ASC, [ProcessedDate] ASC, [Insertdate] ASC) WITH (FILLFACTOR = 90);

