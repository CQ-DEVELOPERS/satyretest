﻿CREATE TABLE [dbo].[ProductCategory] (
    [ProductCategoryId]   INT           IDENTITY (1, 1) NOT NULL,
    [ProductCategoryType] NVARCHAR (50) NULL,
    [ProductType]         NVARCHAR (20) NULL,
    [ProductCategory]     CHAR (1)      NULL,
    [PackingCategory]     CHAR (1)      NULL,
    [StackingCategory]    INT           NULL,
    [MovementCategory]    INT           NULL,
    [ValueCategory]       INT           NULL,
    [StoringCategory]     INT           NULL,
    PRIMARY KEY CLUSTERED ([ProductCategoryId] ASC) WITH (FILLFACTOR = 90)
);

