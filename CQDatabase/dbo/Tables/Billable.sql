﻿CREATE TABLE [dbo].[Billable] (
    [BillableId]   INT           IDENTITY (1, 1) NOT NULL,
    [BillableCode] NVARCHAR (20) NULL,
    [Billable]     NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([BillableId] ASC) WITH (FILLFACTOR = 90)
);

