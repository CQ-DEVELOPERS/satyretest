﻿CREATE TABLE [dbo].[CommentCallOffHeader] (
    [CommentCallOffHeaderId] INT IDENTITY (1, 1) NOT NULL,
    [CallOffHeaderId]        INT NULL,
    [CommentId]              INT NULL,
    PRIMARY KEY CLUSTERED ([CommentCallOffHeaderId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CallOffHeaderId]) REFERENCES [dbo].[CallOffHeader] ([CallOffHeaderId]),
    FOREIGN KEY ([CommentId]) REFERENCES [dbo].[Comment] ([CommentId])
);


GO
CREATE NONCLUSTERED INDEX [nci_CommentCallOffHeader_CommentId]
    ON [dbo].[CommentCallOffHeader]([CommentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CommentCallOffHeader_CallOffHeaderId]
    ON [dbo].[CommentCallOffHeader]([CallOffHeaderId] ASC) WITH (FILLFACTOR = 90);

