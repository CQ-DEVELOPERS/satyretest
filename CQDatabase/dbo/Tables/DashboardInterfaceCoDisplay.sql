﻿CREATE TABLE [dbo].[DashboardInterfaceCoDisplay] (
    [CompanyId]          INT           NOT NULL,
    [DisplayCode]        CHAR (5)      NOT NULL,
    [DisplayDesc]        NVARCHAR (50) NULL,
    [Display]            CHAR (1)      NOT NULL,
    [Counter]            NUMERIC (18)  NULL,
    [ReportName]         NVARCHAR (50) NOT NULL,
    [ExtractDescription] NVARCHAR (50) NULL,
    [Show]               CHAR (1)      NULL
);

