﻿CREATE TABLE [dbo].[StorageUnitBatchHistory] (
    [StorageUnitBatchId] INT           NULL,
    [BatchId]            INT           NULL,
    [StorageUnitId]      INT           NULL,
    [CommandType]        NVARCHAR (10) NOT NULL,
    [InsertDate]         DATETIME      NOT NULL
);

