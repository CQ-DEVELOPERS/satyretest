﻿CREATE TABLE [dbo].[MovementSequence] (
    [MovementSequenceId] INT           IDENTITY (1, 1) NOT NULL,
    [PickAreaId]         INT           NOT NULL,
    [StoreAreaId]        INT           NOT NULL,
    [StageAreaId]        INT           NULL,
    [OperatorGroupId]    INT           NOT NULL,
    [StatusCode]         NVARCHAR (10) NOT NULL,
    CONSTRAINT [PK_MovementSequence] PRIMARY KEY CLUSTERED ([MovementSequenceId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([OperatorGroupId]) REFERENCES [dbo].[OperatorGroup] ([OperatorGroupId]),
    CONSTRAINT [FK_MovementSequence_AreaPick] FOREIGN KEY ([PickAreaId]) REFERENCES [dbo].[Area] ([AreaId])
);


GO
CREATE NONCLUSTERED INDEX [nci_MovementSequence_StageAreaId]
    ON [dbo].[MovementSequence]([StageAreaId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_MovementSequence_StoreAreaId]
    ON [dbo].[MovementSequence]([StoreAreaId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_MovementSequence_PickAreaId]
    ON [dbo].[MovementSequence]([PickAreaId] ASC) WITH (FILLFACTOR = 90);

