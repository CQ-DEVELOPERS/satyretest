﻿CREATE TABLE [dbo].[CommentLink] (
    [CommentLinkId] INT       IDENTITY (1, 1) NOT NULL,
    [CommentId]     INT       NULL,
    [TableId]       INT       NULL,
    [TableName]     [sysname] NOT NULL,
    PRIMARY KEY CLUSTERED ([CommentLinkId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CommentId]) REFERENCES [dbo].[Comment] ([CommentId])
);


GO
CREATE NONCLUSTERED INDEX [nci_CommentLink_CommentId]
    ON [dbo].[CommentLink]([CommentId] ASC) WITH (FILLFACTOR = 90);

