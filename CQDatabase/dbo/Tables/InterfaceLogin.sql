﻿CREATE TABLE [dbo].[InterfaceLogin] (
    [InterfaceLoginId]  INT            IDENTITY (1, 1) NOT NULL,
    [DatabaseName]      [sysname]      NULL,
    [PrincipalCode]     NVARCHAR (30)  NULL,
    [InterfaceUsername] NVARCHAR (50)  NULL,
    [InterfacePassword] NVARCHAR (50)  NULL,
    [FTPUsername]       NVARCHAR (50)  NULL,
    [FTPPassword]       NVARCHAR (50)  NULL,
    [InsertDate]        DATETIME       NULL,
    [CreateDate]        DATETIME       NULL,
    [IsSetup]           BIT            NULL,
    [Message]           NVARCHAR (MAX) NULL,
    [Result]            NVARCHAR (MAX) NULL,
    [SentDate]          DATETIME       NULL,
    [Email]             NVARCHAR (50)  NULL,
    [FirstName]         NVARCHAR (50)  NULL,
    [LastName]          NVARCHAR (50)  NULL,
    [IsActive]          BIT            NULL,
    PRIMARY KEY CLUSTERED ([InterfaceLoginId] ASC) WITH (FILLFACTOR = 90)
);

