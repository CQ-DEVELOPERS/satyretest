﻿CREATE TABLE [dbo].[TransportMode] (
    [TransportModeId]   INT           IDENTITY (1, 1) NOT NULL,
    [TransportModeCode] NVARCHAR (10) NULL,
    [TransportMode]     NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([TransportModeId] ASC) WITH (FILLFACTOR = 90)
);

