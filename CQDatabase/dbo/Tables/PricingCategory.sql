﻿CREATE TABLE [dbo].[PricingCategory] (
    [PricingCategoryId]   INT            IDENTITY (1, 1) NOT NULL,
    [PricingCategory]     NVARCHAR (255) NULL,
    [PricingCategoryCode] NVARCHAR (30)  NULL,
    PRIMARY KEY CLUSTERED ([PricingCategoryId] ASC) WITH (FILLFACTOR = 90)
);

