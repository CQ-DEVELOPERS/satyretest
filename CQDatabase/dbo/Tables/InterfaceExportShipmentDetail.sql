﻿CREATE TABLE [dbo].[InterfaceExportShipmentDetail] (
    [InterfaceExportShipmentDeliveryId] INT             NULL,
    [ReferenceNumber]                   NVARCHAR (30)   NULL,
    [JobId]                             INT             NULL,
    [ContainerType]                     NVARCHAR (50)   NULL,
    [Length]                            FLOAT (53)      NULL,
    [Width]                             FLOAT (53)      NULL,
    [Height]                            FLOAT (53)      NULL,
    [NettWeight]                        FLOAT (53)      NULL,
    [TareWeight]                        FLOAT (53)      NULL,
    [DropSequence]                      INT             NULL,
    [LineNumber]                        INT             NULL,
    [ProductCode]                       NVARCHAR (100)  NULL,
    [Product]                           NVARCHAR (255)  NULL,
    [SkuCode]                           NVARCHAR (10)   NULL,
    [Batch]                             NVARCHAR (50)   NULL,
    [ExpiryDate]                        DATETIME        NULL,
    [Boxed]                             CHAR (1)        NULL,
    [Quantity]                          FLOAT (53)      NULL,
    [CheckQuantity]                     FLOAT (53)      NULL,
    [ShortQuantity]                     FLOAT (53)      NULL,
    [Weight]                            FLOAT (53)      NULL,
    [Barcode]                           NVARCHAR (50)   NULL,
    [SerialNo]                          NVARCHAR (50)   NULL,
    [PackType]                          NVARCHAR (30)   NULL,
    [PackItems]                         INT             NULL,
    [PackTare]                          NUMERIC (13, 6) NULL,
    [Trusted]                           CHAR (1)        NULL,
    CONSTRAINT [FK_InterfaceExportShipmentDetail_InterfaceExportShipmentDelivery] FOREIGN KEY ([InterfaceExportShipmentDeliveryId]) REFERENCES [dbo].[InterfaceExportShipmentDelivery] ([InterfaceExportShipmentDeliveryId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceExportShipmentDetail_InterfaceExportShipmentDeliveryId]
    ON [dbo].[InterfaceExportShipmentDetail]([InterfaceExportShipmentDeliveryId] ASC) WITH (FILLFACTOR = 90);

