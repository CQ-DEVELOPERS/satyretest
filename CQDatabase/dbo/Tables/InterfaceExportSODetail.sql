﻿CREATE TABLE [dbo].[InterfaceExportSODetail] (
    [InterfaceExportSOHeaderId] INT            NULL,
    [ForeignKey]                NVARCHAR (30)  NULL,
    [LineNumber]                INT            NULL,
    [ProductCode]               NVARCHAR (30)  NOT NULL,
    [Product]                   NVARCHAR (50)  NULL,
    [SKUCode]                   NVARCHAR (10)  NULL,
    [Batch]                     NVARCHAR (50)  NULL,
    [Quantity]                  FLOAT (53)     NOT NULL,
    [Weight]                    FLOAT (53)     NULL,
    [Additional1]               NVARCHAR (255) NULL,
    [Additional2]               NVARCHAR (255) NULL,
    [Additional3]               NVARCHAR (255) NULL,
    [Additional4]               NVARCHAR (255) NULL,
    [Additional5]               NVARCHAR (255) NULL,
    [IssueLineId]               INT            NULL,
    [JobId]                     INT            NULL,
    [Despatch]                  NVARCHAR (30)  NULL,
    FOREIGN KEY ([InterfaceExportSOHeaderId]) REFERENCES [dbo].[InterfaceExportSOHeader] ([InterfaceExportSOHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceExportSODetail_InterfaceExportSOHeaderId]
    ON [dbo].[InterfaceExportSODetail]([InterfaceExportSOHeaderId] ASC) WITH (FILLFACTOR = 90);

