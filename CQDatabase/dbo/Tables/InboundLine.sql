﻿CREATE TABLE [dbo].[InboundLine] (
    [InboundLineId]      INT             IDENTITY (1, 1) NOT NULL,
    [InboundDocumentId]  INT             NOT NULL,
    [StorageUnitId]      INT             NOT NULL,
    [StatusId]           INT             NOT NULL,
    [LineNumber]         INT             NOT NULL,
    [Quantity]           FLOAT (53)      NOT NULL,
    [BatchId]            INT             NULL,
    [UnitPrice]          NUMERIC (13, 2) NULL,
    [ReasonId]           INT             NULL,
    [ChildStorageUnitId] INT             NULL,
    [BOELineNumber]      NVARCHAR (50)   NULL,
    [TariffCode]         NVARCHAR (50)   NULL,
    [CountryofOrigin]    NVARCHAR (100)  NULL,
    [Reference1]         NVARCHAR (50)   NULL,
    [Reference2]         NVARCHAR (50)   NULL,
    CONSTRAINT [InboundLine_PK] PRIMARY KEY CLUSTERED ([InboundLineId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK__InboundLi__Child__4C8DB6CE] FOREIGN KEY ([ChildStorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId]),
    CONSTRAINT [FK__InboundLi__Reaso__147C05D0] FOREIGN KEY ([ReasonId]) REFERENCES [dbo].[Reason] ([ReasonId]),
    CONSTRAINT [FK_InboundLine_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    CONSTRAINT [InboundDocument_InboundLine_FK1] FOREIGN KEY ([InboundDocumentId]) REFERENCES [dbo].[InboundDocument] ([InboundDocumentId]),
    CONSTRAINT [Status_InboundLine_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InboundLine_InboundDocumentId]
    ON [dbo].[InboundLine]([InboundDocumentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundLine_ReasonId]
    ON [dbo].[InboundLine]([ReasonId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundLine_StorageUnitId]
    ON [dbo].[InboundLine]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundLine_BatchId]
    ON [dbo].[InboundLine]([BatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundLine_ChildStorageUnitId]
    ON [dbo].[InboundLine]([ChildStorageUnitId] ASC) WITH (FILLFACTOR = 90);

