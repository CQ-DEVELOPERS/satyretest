﻿CREATE TABLE [dbo].[InstructionType] (
    [InstructionTypeId]   INT           IDENTITY (1, 1) NOT NULL,
    [PriorityId]          INT           NOT NULL,
    [InstructionType]     NVARCHAR (30) NOT NULL,
    [InstructionTypeCode] NVARCHAR (10) NOT NULL,
    CONSTRAINT [InstructionType_PK] PRIMARY KEY CLUSTERED ([InstructionTypeId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    CONSTRAINT [uc_InstructionType] UNIQUE NONCLUSTERED ([InstructionType] ASC) WITH (FILLFACTOR = 90)
);


GO
create trigger tr_InstructionType_i
on InstructionType
for insert
as
begin
  insert OperatorGroupInstructionType
        (InstructionTypeId,
         OperatorGroupId,
         OrderBy)
  select it.InstructionTypeId,
         og.OperatorGroupId,
         0
    from inserted it,
         OperatorGroup og
end

drop trigger tr_MenuItem_i
