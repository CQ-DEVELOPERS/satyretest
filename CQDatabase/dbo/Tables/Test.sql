﻿CREATE TABLE [dbo].[Test] (
    [TestId]   INT            IDENTITY (1, 1) NOT NULL,
    [TestCode] NVARCHAR (10)  NULL,
    [Test]     NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([TestId] ASC) WITH (FILLFACTOR = 90)
);

