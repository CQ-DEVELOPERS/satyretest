﻿CREATE TABLE [dbo].[PackHistory] (
    [PackId]             INT             NULL,
    [StorageUnitId]      INT             NULL,
    [PackTypeId]         INT             NULL,
    [Quantity]           FLOAT (53)      NULL,
    [Barcode]            NVARCHAR (50)   NULL,
    [Length]             INT             NULL,
    [Width]              INT             NULL,
    [Height]             INT             NULL,
    [Volume]             NUMERIC (13, 3) NULL,
    [Weight]             FLOAT (53)      NULL,
    [WarehouseId]        INT             NULL,
    [CommandType]        NVARCHAR (10)   NOT NULL,
    [InsertDate]         DATETIME        NOT NULL,
    [NettWeight]         FLOAT (53)      NULL,
    [TareWeight]         FLOAT (53)      NULL,
    [ProductionQuantity] FLOAT (53)      NULL,
    [StatusId]           INT             NULL
);

