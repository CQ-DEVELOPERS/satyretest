﻿CREATE TABLE [dbo].[CommentShipmentHeader] (
    [CommentShipmentHeaderId] INT IDENTITY (1, 1) NOT NULL,
    [ShipmentHeaderId]        INT NULL,
    [CommentId]               INT NULL,
    PRIMARY KEY CLUSTERED ([CommentShipmentHeaderId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CommentId]) REFERENCES [dbo].[Comment] ([CommentId]),
    FOREIGN KEY ([ShipmentHeaderId]) REFERENCES [dbo].[ShipmentHeader] ([ShipmentHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_CommentShipmentHeader_CommentId]
    ON [dbo].[CommentShipmentHeader]([CommentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_CommentShipmentHeader_ShipmentHeaderId]
    ON [dbo].[CommentShipmentHeader]([ShipmentHeaderId] ASC) WITH (FILLFACTOR = 90);

