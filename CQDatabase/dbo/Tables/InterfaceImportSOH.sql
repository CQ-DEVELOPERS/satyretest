﻿CREATE TABLE [dbo].[InterfaceImportSOH] (
    [InterfaceImportSOHId] INT             IDENTITY (1, 1) NOT NULL,
    [Location]             NVARCHAR (50)   NULL,
    [ProductCode]          NVARCHAR (30)   NULL,
    [Batch]                NVARCHAR (50)   NULL,
    [Quantity]             FLOAT (53)      NULL,
    [UnitPrice]            NUMERIC (13, 3) NULL,
    [ProcessedDate]        DATETIME        NULL,
    [RecordStatus]         CHAR (1)        DEFAULT ('N') NULL,
    [RecordType]           NVARCHAR (20)   NULL,
    [InsertDate]           DATETIME        CONSTRAINT [DF_InterfaceImportSOH_InsertDate] DEFAULT (getdate()) NULL,
    [SKUCode]              NVARCHAR (10)   NULL,
    [PrincipalCode]        NVARCHAR (30)   NULL,
    PRIMARY KEY CLUSTERED ([InterfaceImportSOHId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportSOH_SKUCode]
    ON [dbo].[InterfaceImportSOH]([SKUCode] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportSOH_Batch]
    ON [dbo].[InterfaceImportSOH]([Batch] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportSOH_InsertDate]
    ON [dbo].[InterfaceImportSOH]([InsertDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportSOH_Status_Dates]
    ON [dbo].[InterfaceImportSOH]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

