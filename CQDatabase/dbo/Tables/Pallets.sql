﻿CREATE TABLE [dbo].[Pallets] (
    [pallet_id] INT           NOT NULL,
    [stock_no]  NVARCHAR (30) NOT NULL,
    [sku_code]  NVARCHAR (15) NOT NULL,
    [lot_code]  NVARCHAR (30) NOT NULL
);

