﻿CREATE TABLE [dbo].[TimeBox] (
    [TimeBoxId]   INT        IDENTITY (1, 1) NOT NULL,
    [TaskListId]  INT        NULL,
    [CreateDate]  DATETIME   CONSTRAINT [DF__TimeBox__CreateD__5A974CC3] DEFAULT (getdate()) NULL,
    [ActualHours] FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([TimeBoxId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([TaskListId]) REFERENCES [dbo].[TaskList] ([TaskListId])
);


GO
CREATE NONCLUSTERED INDEX [nci_TimeBox_TaskListId]
    ON [dbo].[TimeBox]([TaskListId] ASC) WITH (FILLFACTOR = 90);

