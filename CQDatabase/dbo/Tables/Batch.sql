﻿CREATE TABLE [dbo].[Batch] (
    [BatchId]              INT           IDENTITY (1, 1) NOT NULL,
    [StatusId]             INT           NOT NULL,
    [WarehouseId]          INT           NOT NULL,
    [Batch]                NVARCHAR (50) NOT NULL,
    [CreateDate]           DATETIME      NOT NULL,
    [ExpiryDate]           DATETIME      NULL,
    [IncubationDays]       INT           NULL,
    [ShelfLifeDays]        INT           NULL,
    [ExpectedYield]        INT           NULL,
    [ActualYield]          INT           NULL,
    [ECLNumber]            NVARCHAR (10) NULL,
    [BatchReferenceNumber] NVARCHAR (50) NULL,
    [ProductionDateTime]   DATETIME      NULL,
    [FilledYield]          INT           NULL,
    [COACertificate]       BIT           DEFAULT ((0)) NULL,
    [StatusModifiedBy]     INT           NULL,
    [StatusModifiedDate]   DATETIME      NULL,
    [CreatedBy]            INT           NULL,
    [ModifiedBy]           INT           NULL,
    [ModifiedDate]         DATETIME      NULL,
    [Prints]               INT           NULL,
    [ParentProductCode]    NVARCHAR (30) NULL,
    [RI]                   FLOAT (53)    NULL,
    [Density]              FLOAT (53)    NULL,
    [BOELineNumber]        NVARCHAR (50) NULL,
    [BillOfEntry]          NVARCHAR (50) NULL,
    [ReferenceNumber]      NVARCHAR (50) NULL,
    CONSTRAINT [Batch_PK] PRIMARY KEY CLUSTERED ([BatchId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [Status_Batch_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [Warehouse_Batch_FK1] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Batch_CreatedBy]
    ON [dbo].[Batch]([CreatedBy] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Batch_StatusId]
    ON [dbo].[Batch]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Batch_WarehouseId]
    ON [dbo].[Batch]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Batch_ModifiedBy]
    ON [dbo].[Batch]([ModifiedBy] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Batch_ExpiryDate]
    ON [dbo].[Batch]([ExpiryDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Batch_CreateDate]
    ON [dbo].[Batch]([CreateDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Batch_ECLNumber]
    ON [dbo].[Batch]([ECLNumber] ASC) WITH (FILLFACTOR = 90);

