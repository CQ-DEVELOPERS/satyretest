﻿CREATE TABLE [dbo].[InterfaceMappingFile20170310] (
    [InterfaceMappingFileId]  INT            IDENTITY (1, 1) NOT NULL,
    [PrincipalId]             INT            NULL,
    [InterfaceFileTypeId]     INT            NULL,
    [InterfaceDocumentTypeId] INT            NULL,
    [Delimiter]               CHAR (2)       NULL,
    [StyleSheet]              XML            NULL,
    [PrincipalFTPSite]        NVARCHAR (MAX) NULL,
    [FTPUserName]             NVARCHAR (50)  NULL,
    [FTPPassword]             NVARCHAR (50)  NULL,
    [HeaderRow]               INT            NULL,
    [SubDirectory]            NVARCHAR (MAX) NULL,
    [FilePrefix]              NVARCHAR (30)  NULL,
    [FileExtension]           NVARCHAR (30)  NULL
);

