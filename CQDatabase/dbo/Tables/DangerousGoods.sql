﻿CREATE TABLE [dbo].[DangerousGoods] (
    [DangerousGoodsId]   INT           IDENTITY (1, 1) NOT NULL,
    [DangerousGoodsCode] NVARCHAR (10) NOT NULL,
    [DangerousGoods]     NVARCHAR (50) NOT NULL,
    [Class]              FLOAT (53)    NULL,
    [UNNumber]           NVARCHAR (30) NOT NULL,
    [PackClass]          NVARCHAR (30) NULL,
    [DangerFactor]       INT           NULL,
    PRIMARY KEY CLUSTERED ([DangerousGoodsId] ASC) WITH (FILLFACTOR = 90)
);

