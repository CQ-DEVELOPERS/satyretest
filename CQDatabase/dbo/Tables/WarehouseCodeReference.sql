﻿CREATE TABLE [dbo].[WarehouseCodeReference] (
    [WarehouseCodeReferenceId] INT           IDENTITY (1, 1) NOT NULL,
    [WarehouseCode]            NVARCHAR (30) NULL,
    [DownloadType]             NVARCHAR (10) NULL,
    [WarehouseId]              INT           NULL,
    [InboundDocumentTypeId]    INT           NULL,
    [OutboundDocumentTypeId]   INT           NULL,
    [ToWarehouseId]            INT           NULL,
    [ToWarehouseCode]          NVARCHAR (30) NULL,
    [Module]                   NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([WarehouseCodeReferenceId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([InboundDocumentTypeId]) REFERENCES [dbo].[InboundDocumentType] ([InboundDocumentTypeId]),
    FOREIGN KEY ([OutboundDocumentTypeId]) REFERENCES [dbo].[OutboundDocumentType] ([OutboundDocumentTypeId]),
    FOREIGN KEY ([ToWarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);

