﻿CREATE TABLE [dbo].[Calendar] (
    [DWDateKey]      DATE           NULL,
    [DayDate]        INT            NULL,
    [DayOfWeekName]  NVARCHAR (30)  NULL,
    [WeekNumber]     INT            NULL,
    [MonthNumber]    INT            NULL,
    [MonthName]      NVARCHAR (30)  NULL,
    [MonthShortName] NVARCHAR (4)   NULL,
    [Year]           INT            NULL,
    [QuarterNumber]  NVARCHAR (30)  NULL,
    [QuarterName]    NVARCHAR (30)  NULL,
    [DayType]        NVARCHAR (50)  DEFAULT ((0)) NULL,
    [DayDescription] NVARCHAR (100) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_Calendar_DWDateKey]
    ON [dbo].[Calendar]([DWDateKey] ASC)
    INCLUDE([DayType]) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_Calendar_DayType]
    ON [dbo].[Calendar]([DayType] ASC)
    INCLUDE([DWDateKey]) WITH (FILLFACTOR = 70);

