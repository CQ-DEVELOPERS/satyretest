﻿CREATE TABLE [dbo].[BOMInstruction] (
    [BOMInstructionId]   INT           IDENTITY (1, 1) NOT NULL,
    [BOMHeaderId]        INT           NOT NULL,
    [Quantity]           FLOAT (53)    NOT NULL,
    [OrderNumber]        NVARCHAR (30) NULL,
    [InboundDocumentId]  INT           NULL,
    [OutboundDocumentId] INT           NULL,
    [ParentId]           INT           NULL,
    [OutboundShipmentId] INT           NULL,
    [IssueId]            INT           NULL,
    CONSTRAINT [PK_BOMInstruction] PRIMARY KEY CLUSTERED ([BOMInstructionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_BOMInstruction_BOMHeader] FOREIGN KEY ([BOMHeaderId]) REFERENCES [dbo].[BOMHeader] ([BOMHeaderId]),
    CONSTRAINT [FK_BOMInstruction_InboundDocument] FOREIGN KEY ([InboundDocumentId]) REFERENCES [dbo].[InboundDocument] ([InboundDocumentId]),
    CONSTRAINT [FK_BOMInstruction_OutboundDocument] FOREIGN KEY ([OutboundDocumentId]) REFERENCES [dbo].[OutboundDocument] ([OutboundDocumentId])
);


GO
CREATE NONCLUSTERED INDEX [nci_BOMInstruction_OutboundDocumentId]
    ON [dbo].[BOMInstruction]([OutboundDocumentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_BOMInstruction_InboundDocumentId]
    ON [dbo].[BOMInstruction]([InboundDocumentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_BOMInstruction_BOMHeaderId]
    ON [dbo].[BOMInstruction]([BOMHeaderId] ASC) WITH (FILLFACTOR = 90);

