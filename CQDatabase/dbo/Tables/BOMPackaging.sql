﻿CREATE TABLE [dbo].[BOMPackaging] (
    [BOMPackagingId]   INT        IDENTITY (1, 1) NOT NULL,
    [OperatorId]       INT        NULL,
    [CreateDate]       DATETIME   NOT NULL,
    [BOMInstructionId] INT        NOT NULL,
    [StorageUnitId]    INT        NOT NULL,
    [Quantity]         FLOAT (53) NOT NULL,
    PRIMARY KEY CLUSTERED ([BOMPackagingId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([BOMInstructionId]) REFERENCES [dbo].[BOMInstruction] ([BOMInstructionId]),
    FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId]),
    FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [nci_BOMPackaging_StorageUnitId]
    ON [dbo].[BOMPackaging]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_BOMPackaging_OperatorId]
    ON [dbo].[BOMPackaging]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_BOMPackaging_BOMInstructionId]
    ON [dbo].[BOMPackaging]([BOMInstructionId] ASC) WITH (FILLFACTOR = 90);

