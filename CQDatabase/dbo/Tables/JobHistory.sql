﻿CREATE TABLE [dbo].[JobHistory] (
    [JobId]           INT           NULL,
    [PriorityId]      INT           NULL,
    [OperatorId]      INT           NULL,
    [StatusId]        INT           NULL,
    [WarehouseId]     INT           NULL,
    [ReceiptLineId]   INT           NULL,
    [IssueLineId]     INT           NULL,
    [ContainerTypeId] INT           NULL,
    [ReferenceNumber] NVARCHAR (30) NULL,
    [TareWeight]      FLOAT (53)    NULL,
    [Weight]          FLOAT (53)    NULL,
    [CommandType]     NVARCHAR (10) NOT NULL,
    [InsertDate]      DATETIME      NOT NULL,
    [NettWeight]      FLOAT (53)    NULL,
    [CheckedBy]       INT           NULL,
    [CheckedDate]     DATETIME      NULL,
    [DropSequence]    INT           NULL,
    [Pallets]         INT           NULL,
    [BackFlush]       BIT           NULL,
    [Prints]          INT           NULL,
    [CheckingClear]   BIT           NULL,
    [TrackingNumber]  NVARCHAR (30) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_JobHistory_JobId]
    ON [dbo].[JobHistory]([JobId] ASC)
    INCLUDE([StatusId], [InsertDate]) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_JobHistory_StatusId]
    ON [dbo].[JobHistory]([StatusId] ASC)
    INCLUDE([JobId], [InsertDate]) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_JobHistory_JobId1]
    ON [dbo].[JobHistory]([JobId] ASC)
    INCLUDE([StatusId], [InsertDate]) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_JobHistory_StatusId1]
    ON [dbo].[JobHistory]([StatusId] ASC)
    INCLUDE([JobId], [InsertDate]) WITH (FILLFACTOR = 70);

