﻿CREATE TABLE [dbo].[SKUCheck] (
    [JobId]             INT           NULL,
    [SKUCode]           NVARCHAR (10) NULL,
    [Quantity]          FLOAT (53)    NULL,
    [ConfirmedQuantity] FLOAT (53)    NULL,
    FOREIGN KEY ([JobId]) REFERENCES [dbo].[Job] ([JobId])
);


GO
CREATE NONCLUSTERED INDEX [nci_SKUCheck_JobId]
    ON [dbo].[SKUCheck]([JobId] ASC) WITH (FILLFACTOR = 90);

