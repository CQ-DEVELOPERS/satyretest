﻿CREATE TABLE [dbo].[BOMSpecialInstruction] (
    [BOMHeaderId]               INT            NULL,
    [ProcedureLine1]            NVARCHAR (255) NULL,
    [ProcedureLine2]            NVARCHAR (255) NULL,
    [ProcedureLine3]            NVARCHAR (255) NULL,
    [ProcedureLine4]            NVARCHAR (255) NULL,
    [ProcedureLine5]            NVARCHAR (255) NULL,
    [ProcedureLine6]            NVARCHAR (255) NULL,
    [ProcedureLine7]            NVARCHAR (255) NULL,
    [ProcedureLine8]            NVARCHAR (255) NULL,
    [ProcedureLine9]            NVARCHAR (255) NULL,
    [ProcedureLine10]           NVARCHAR (255) NULL,
    [ProcedureLine11]           NVARCHAR (255) NULL,
    [ProcedureLine12]           NVARCHAR (255) NULL,
    [SpecialInstructionsLine1]  NVARCHAR (255) NULL,
    [SpecialInstructionsLine2]  NVARCHAR (255) NULL,
    [SpecialInstructionsLine3]  NVARCHAR (255) NULL,
    [SpecialInstructionsLine4]  NVARCHAR (255) NULL,
    [SpecialInstructionsLine5]  NVARCHAR (255) NULL,
    [SpecialInstructionsLine6]  NVARCHAR (255) NULL,
    [SpecialInstructionsLine7]  NVARCHAR (255) NULL,
    [SpecialInstructionsLine8]  NVARCHAR (255) NULL,
    [SpecialInstructionsLine9]  NVARCHAR (255) NULL,
    [SpecialInstructionsLine10] NVARCHAR (255) NULL,
    [SpecialInstructionsLine11] NVARCHAR (255) NULL,
    [SpecialInstructionsLine12] NVARCHAR (255) NULL,
    FOREIGN KEY ([BOMHeaderId]) REFERENCES [dbo].[BOMHeader] ([BOMHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_BOMSpecialInstruction_BOMHeaderId]
    ON [dbo].[BOMSpecialInstruction]([BOMHeaderId] ASC) WITH (FILLFACTOR = 90);

