﻿CREATE TABLE [dbo].[StockTakeReferenceJob] (
    [StockTakeReferenceJobId,] INT           IDENTITY (1, 1) NOT NULL,
    [StockTakeReferenceId]     INT           NOT NULL,
    [JobId]                    INT           NOT NULL,
    [StatusCode]               NVARCHAR (10) NOT NULL,
    CONSTRAINT [uc_ReferenceJob] UNIQUE NONCLUSTERED ([StockTakeReferenceId] ASC, [JobId] ASC) WITH (FILLFACTOR = 90)
);

