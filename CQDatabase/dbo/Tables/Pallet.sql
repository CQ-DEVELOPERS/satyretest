﻿CREATE TABLE [dbo].[Pallet] (
    [PalletId]               INT           IDENTITY (1, 1) NOT NULL,
    [StorageUnitBatchId]     INT           NULL,
    [LocationId]             INT           NULL,
    [Weight]                 FLOAT (53)    NULL,
    [Tare]                   FLOAT (53)    NULL,
    [Quantity]               FLOAT (53)    NULL,
    [Prints]                 INT           NOT NULL,
    [CreateDate]             DATETIME      CONSTRAINT [DF__Pallet__CreateDa__12CBF4E4] DEFAULT (getdate()) NULL,
    [Nett]                   FLOAT (53)    NULL,
    [ReasonId]               INT           NULL,
    [EmptyWeight]            FLOAT (53)    NULL,
    [DeliveryAdviceDetailId] INT           NULL,
    [ReferenceNumber]        NVARCHAR (30) NULL,
    [StatusId]               INT           NULL,
    CONSTRAINT [PK_Pallet] PRIMARY KEY CLUSTERED ([PalletId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([DeliveryAdviceDetailId]) REFERENCES [dbo].[DeliveryAdviceDetail] ([DeliveryAdviceDetailId]),
    FOREIGN KEY ([ReasonId]) REFERENCES [dbo].[Reason] ([ReasonId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [FK_Pallet_Location] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [FK_Pallet_StorageUnitBatch] FOREIGN KEY ([StorageUnitBatchId]) REFERENCES [dbo].[StorageUnitBatch] ([StorageUnitBatchId])
);

