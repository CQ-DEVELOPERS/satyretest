﻿CREATE TABLE [dbo].[PerformanceLog] (
    [PerformanceLogId] INT            IDENTITY (1, 1) NOT NULL,
    [ProcedureName]    NVARCHAR (128) NULL,
    [Remarks]          NVARCHAR (MAX) NULL,
    [StartDate]        DATETIME       NULL,
    [LogDate]          DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([PerformanceLogId] ASC) WITH (FILLFACTOR = 90)
);

