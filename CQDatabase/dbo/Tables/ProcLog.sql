﻿CREATE TABLE [dbo].[ProcLog] (
    [ProcLogId]          INT           IDENTITY (1, 1) NOT NULL,
    [Type]               INT           NULL,
    [OperatorId]         INT           NULL,
    [JobId]              INT           NULL,
    [Barcode]            NVARCHAR (50) NULL,
    [Batch]              NVARCHAR (50) NULL,
    [LocationId]         INT           NULL,
    [StorageUnitBatchId] INT           NULL,
    [Quantity]           FLOAT (53)    NULL,
    [Weight]             FLOAT (53)    NULL,
    [CreateDate]         DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([ProcLogId] ASC)
);

