﻿CREATE TABLE [dbo].[COATest] (
    [COATestId]  INT            IDENTITY (1, 1) NOT NULL,
    [COAId]      INT            NULL,
    [TestId]     INT            NULL,
    [ResultId]   INT            NULL,
    [MethodId]   INT            NULL,
    [ResultCode] NVARCHAR (10)  NULL,
    [Result]     NVARCHAR (255) NULL,
    [Pass]       BIT            NULL,
    [StartRange] FLOAT (53)     NULL,
    [EndRange]   FLOAT (53)     NULL,
    [Value]      SQL_VARIANT    NULL,
    PRIMARY KEY CLUSTERED ([COATestId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([COAId]) REFERENCES [dbo].[COA] ([COAId]),
    FOREIGN KEY ([MethodId]) REFERENCES [dbo].[Method] ([MethodId]),
    FOREIGN KEY ([ResultId]) REFERENCES [dbo].[Result] ([ResultId]),
    FOREIGN KEY ([TestId]) REFERENCES [dbo].[Test] ([TestId])
);


GO
CREATE NONCLUSTERED INDEX [nci_COATest_ResultId]
    ON [dbo].[COATest]([ResultId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_COATest_COAId]
    ON [dbo].[COATest]([COAId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_COATest_MethodId]
    ON [dbo].[COATest]([MethodId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_COATest_TestId]
    ON [dbo].[COATest]([TestId] ASC) WITH (FILLFACTOR = 90);

