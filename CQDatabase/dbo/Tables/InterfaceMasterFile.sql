﻿CREATE TABLE [dbo].[InterfaceMasterFile] (
    [MasterFileId]          INT           IDENTITY (1, 1) NOT NULL,
    [MasterFile]            VARCHAR (50)  NULL,
    [MasterFileDescription] VARCHAR (255) NULL,
    [LastDate]              DATETIME      NULL,
    [SendFlag]              BIT           NULL,
    CONSTRAINT [InterfaceMasterFile_PK] PRIMARY KEY CLUSTERED ([MasterFileId] ASC) WITH (FILLFACTOR = 90)
);

