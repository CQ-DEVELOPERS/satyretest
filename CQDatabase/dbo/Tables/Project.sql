﻿CREATE TABLE [dbo].[Project] (
    [ProjectId]   INT           IDENTITY (1, 1) NOT NULL,
    [ProjectCode] NVARCHAR (20) NULL,
    [Project]     NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([ProjectId] ASC) WITH (FILLFACTOR = 90)
);

