﻿CREATE TABLE [dbo].[PurchaseOrderDetail] (
    [PurchaseOrderDetailId] INT        IDENTITY (1, 1) NOT NULL,
    [PurchaseOrderHeaderId] INT        NULL,
    [LineNumber]            INT        NULL,
    [StorageUnitId]         INT        NULL,
    [BatchId]               INT        NULL,
    [RequiredQuantity]      FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([PurchaseOrderDetailId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([PurchaseOrderHeaderId]) REFERENCES [dbo].[PurchaseOrderHeader] ([PurchaseOrderHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_PurchaseOrderDetail_PurchaseOrderHeaderId]
    ON [dbo].[PurchaseOrderDetail]([PurchaseOrderHeaderId] ASC) WITH (FILLFACTOR = 90);

