﻿CREATE TABLE [dbo].[InterfaceProduct] (
    [HostId]             NVARCHAR (30)  NULL,
    [ProductCode]        NVARCHAR (30)  NULL,
    [ProductDescription] NVARCHAR (255) NULL,
    [Barcode]            NVARCHAR (255) NULL,
    [OuterCartonBarcode] NVARCHAR (255) NULL,
    [PalletQuantity]     FLOAT (53)     NULL,
    [Class]              NVARCHAR (50)  NULL,
    [Modified]           NVARCHAR (30)  NULL,
    [ProcessedDate]      DATETIME       NULL,
    [RecordStatus]       CHAR (1)       DEFAULT ('N') NULL,
    [CaseQuantity]       FLOAT (53)     NULL,
    [InsertDate]         DATETIME       CONSTRAINT [DF_InterfaceProduct_InsertDate] DEFAULT (getdate()) NULL
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceProduct_Status_Dates]
    ON [dbo].[InterfaceProduct]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

