﻿CREATE TABLE [dbo].[DashboardPutawayPriority] (
    [WarehouseId]       INT        NULL,
    [StorageUnitId]     INT        NULL,
    [RequiredQuantity]  FLOAT (53) NULL,
    [ReceivingQuantity] FLOAT (53) NULL
);

