﻿CREATE TABLE [dbo].[RouteHistory] (
    [RouteId]     INT           NULL,
    [Route]       NVARCHAR (50) NULL,
    [RouteCode]   NVARCHAR (10) NULL,
    [CommandType] NVARCHAR (10) NOT NULL,
    [InsertDate]  DATETIME      NOT NULL
);

