﻿CREATE TABLE [dbo].[OperatorGroupHistory] (
    [OperatorGroupId]         INT           NULL,
    [OperatorGroup]           NVARCHAR (30) NULL,
    [RestrictedAreaIndicator] BIT           NULL,
    [CommandType]             NVARCHAR (10) NOT NULL,
    [InsertDate]              DATETIME      NOT NULL,
    [OperatorGroupCode]       NVARCHAR (10) NULL,
    [NarrowAisle]             BIT           NULL,
    [Interleaving]            BIT           NULL
);

