﻿CREATE TABLE [dbo].[ExternalCompanyType] (
    [ExternalCompanyTypeId]   INT           IDENTITY (1, 1) NOT NULL,
    [ExternalCompanyType]     NVARCHAR (50) NOT NULL,
    [ExternalCompanyTypeCode] NVARCHAR (10) NOT NULL,
    CONSTRAINT [ExternalCompanyType_PK] PRIMARY KEY CLUSTERED ([ExternalCompanyTypeId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_ExternalCompanyType] UNIQUE NONCLUSTERED ([ExternalCompanyType] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_ExternalCompanyTypeCode] UNIQUE NONCLUSTERED ([ExternalCompanyTypeCode] ASC) WITH (FILLFACTOR = 90)
);

