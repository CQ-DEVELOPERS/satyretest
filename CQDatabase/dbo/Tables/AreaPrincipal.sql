﻿CREATE TABLE [dbo].[AreaPrincipal] (
    [AreaId]      INT NOT NULL,
    [PrincipalId] INT NOT NULL,
    FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Area] ([AreaId]),
    FOREIGN KEY ([PrincipalId]) REFERENCES [dbo].[Principal] ([PrincipalId])
);


GO
CREATE NONCLUSTERED INDEX [nci_AreaPrincipal_AreaId]
    ON [dbo].[AreaPrincipal]([AreaId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_AreaPrincipal_PrincipalId]
    ON [dbo].[AreaPrincipal]([PrincipalId] ASC) WITH (FILLFACTOR = 90);

