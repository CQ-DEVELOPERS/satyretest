﻿CREATE TABLE [dbo].[InterfaceExportDetail] (
    [InterfaceExportHeaderId] INT            NULL,
    [ForeignKey]              NVARCHAR (30)  NULL,
    [LineNumber]              INT            NULL,
    [ProductCode]             NVARCHAR (30)  NOT NULL,
    [Product]                 NVARCHAR (255) NULL,
    [SKUCode]                 NVARCHAR (50)  NULL,
    [Batch]                   NVARCHAR (50)  NULL,
    [Quantity]                FLOAT (53)     NOT NULL,
    [Weight]                  FLOAT (53)     NULL,
    [Additional1]             NVARCHAR (255) NULL,
    [Additional2]             NVARCHAR (255) NULL,
    [Additional3]             NVARCHAR (255) NULL,
    [Additional4]             NVARCHAR (255) NULL,
    [Additional5]             NVARCHAR (255) NULL,
    [OrderQuantity]           FLOAT (53)     NULL,
    [RejectQuantity]          FLOAT (53)     NULL,
    [ReceivedQuantity]        FLOAT (53)     NULL,
    [AcceptedQuantity]        FLOAT (53)     NULL,
    [DeliveryNoteQuantity]    FLOAT (53)     NULL,
    [ReasonCode]              NVARCHAR (10)  NULL,
    [ExpiryDate]              DATETIME       NULL,
    FOREIGN KEY ([InterfaceExportHeaderId]) REFERENCES [dbo].[InterfaceExportHeader] ([InterfaceExportHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceExportDetail_InterfaceExportHeaderId]
    ON [dbo].[InterfaceExportDetail]([InterfaceExportHeaderId] ASC) WITH (FILLFACTOR = 90);

