﻿CREATE TABLE [dbo].[IndicatorHistory] (
    [IndicatorId]      INT           NULL,
    [Header]           NVARCHAR (50) NULL,
    [Indicator]        NVARCHAR (50) NULL,
    [IndicatorValue]   SQL_VARIANT   NULL,
    [IndicatorDisplay] SQL_VARIANT   NULL,
    [ThresholdGreen]   INT           NULL,
    [ThresholdYellow]  INT           NULL,
    [ThresholdRed]     INT           NULL,
    [ModifiedDate]     DATETIME      NULL,
    [CommandType]      NVARCHAR (10) NOT NULL,
    [InsertDate]       DATETIME      NOT NULL
);

