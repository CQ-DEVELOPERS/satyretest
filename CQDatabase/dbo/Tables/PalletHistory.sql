﻿CREATE TABLE [dbo].[PalletHistory] (
    [PalletId]               INT           NULL,
    [StorageUnitBatchId]     INT           NULL,
    [LocationId]             INT           NULL,
    [Weight]                 FLOAT (53)    NULL,
    [Tare]                   FLOAT (53)    NULL,
    [Quantity]               FLOAT (53)    NULL,
    [Prints]                 INT           NULL,
    [CommandType]            NVARCHAR (10) NOT NULL,
    [InsertDate]             DATETIME      NOT NULL,
    [CreateDate]             DATETIME      NULL,
    [Nett]                   FLOAT (53)    NULL,
    [ReasonId]               INT           NULL,
    [EmptyWeight]            FLOAT (53)    NULL,
    [DeliveryAdviceDetailId] INT           NULL,
    [ReferenceNumber]        NVARCHAR (30) NULL,
    [StatusId]               INT           NULL
);

