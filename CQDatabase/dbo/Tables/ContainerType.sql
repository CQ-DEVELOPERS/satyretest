﻿CREATE TABLE [dbo].[ContainerType] (
    [ContainerTypeId]   INT             IDENTITY (1, 1) NOT NULL,
    [ContainerType]     NVARCHAR (50)   NULL,
    [ContainerTypeCode] NVARCHAR (10)   NULL,
    [Description]       NVARCHAR (50)   NULL,
    [Length]            INT             NULL,
    [Width]             INT             NULL,
    [Height]            INT             NULL,
    [TareWeight]        NUMERIC (15, 3) NULL,
    PRIMARY KEY CLUSTERED ([ContainerTypeId] ASC) WITH (FILLFACTOR = 90)
);

