﻿CREATE TABLE [dbo].[InterfaceExportShipment] (
    [InterfaceExportShipmentId] INT            IDENTITY (1, 1) NOT NULL,
    [RecordStatus]              CHAR (1)       NULL,
    [ProcessedDate]             DATETIME       NULL,
    [PrimaryKey]                NVARCHAR (40)  NULL,
    [OutboundShipmentId]        INT            NULL,
    [RouteCode]                 NVARCHAR (10)  NULL,
    [DivisionCode]              NVARCHAR (10)  NULL,
    [DriverName]                NVARCHAR (50)  NULL,
    [DriverId]                  NVARCHAR (50)  NULL,
    [VehicleRegistration]       NVARCHAR (10)  NULL,
    [SequenceNumber]            INT            NULL,
    [WarehouseCode]             NVARCHAR (30)  NULL,
    [CompanyCode]               NVARCHAR (10)  NULL,
    [CompanyName]               NVARCHAR (50)  NULL,
    [Street]                    NVARCHAR (100) NULL,
    [Suburb]                    NVARCHAR (100) NULL,
    [Town]                      NVARCHAR (100) NULL,
    [Country]                   NVARCHAR (100) NULL,
    [PostalCode]                NVARCHAR (20)  NULL,
    [ContactPerson]             NVARCHAR (50)  NULL,
    [Telephone]                 NVARCHAR (20)  NULL,
    [EMail]                     NVARCHAR (30)  NULL,
    [InsertDate]                DATETIME       NULL,
    CONSTRAINT [PK_InterfaceExportShipment] PRIMARY KEY CLUSTERED ([InterfaceExportShipmentId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceExportShipment_Status_Dates]
    ON [dbo].[InterfaceExportShipment]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

