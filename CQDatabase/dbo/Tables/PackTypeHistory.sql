﻿CREATE TABLE [dbo].[PackTypeHistory] (
    [PackTypeId]       INT           NULL,
    [PackType]         NVARCHAR (30) NULL,
    [InboundSequence]  SMALLINT      NULL,
    [OutboundSequence] SMALLINT      NULL,
    [CommandType]      NVARCHAR (10) NOT NULL,
    [InsertDate]       DATETIME      NOT NULL,
    [PackTypeCode]     VARCHAR (10)  NULL,
    [Unit]             BIT           NULL,
    [Pallet]           BIT           NULL
);

