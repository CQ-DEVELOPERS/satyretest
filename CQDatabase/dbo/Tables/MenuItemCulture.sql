﻿CREATE TABLE [dbo].[MenuItemCulture] (
    [MenuItemCultureId] INT            IDENTITY (1, 1) NOT NULL,
    [MenuItemId]        INT            NULL,
    [CultureId]         INT            NULL,
    [MenuItem]          NVARCHAR (50)  NULL,
    [ToolTip]           NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([MenuItemCultureId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([CultureId]) REFERENCES [dbo].[Culture] ([CultureId]),
    FOREIGN KEY ([MenuItemId]) REFERENCES [dbo].[MenuItem] ([MenuItemId])
);


GO
CREATE NONCLUSTERED INDEX [IX_MenuItemCulture_CultureId]
    ON [dbo].[MenuItemCulture]([CultureId] ASC)
    INCLUDE([MenuItemId], [MenuItem], [ToolTip]) WITH (FILLFACTOR = 70);

