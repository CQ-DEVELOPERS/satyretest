﻿CREATE TABLE [dbo].[InterfaceImportIPHeader] (
    [InterfaceImportIPHeaderId] INT             IDENTITY (1, 1) NOT NULL,
    [OutboundShipmentId]        INT             NULL,
    [IssueId]                   INT             NULL,
    [InvoiceDate]               DATETIME        NULL,
    [PrimaryKey]                NVARCHAR (30)   NULL,
    [CustomerAddress1]          NVARCHAR (255)  NULL,
    [CustomerAddress2]          NVARCHAR (255)  NULL,
    [CustomerAddress3]          NVARCHAR (255)  NULL,
    [CustomerAddress4]          NVARCHAR (255)  NULL,
    [CustomerAddress5]          NVARCHAR (255)  NULL,
    [CustomerAddress6]          NVARCHAR (255)  NULL,
    [DeliveryAddress1]          NVARCHAR (255)  NULL,
    [DeliveryAddress2]          NVARCHAR (255)  NULL,
    [DeliveryAddress3]          NVARCHAR (255)  NULL,
    [DeliveryAddress4]          NVARCHAR (255)  NULL,
    [DeliveryAddress5]          NVARCHAR (255)  NULL,
    [DeliveryAddress6]          NVARCHAR (255)  NULL,
    [CustomerName]              NVARCHAR (50)   NULL,
    [CustomerCode]              NVARCHAR (50)   NULL,
    [OrderNumber]               NVARCHAR (30)   NULL,
    [ReferenceNumber]           NVARCHAR (50)   NULL,
    [ChargeTax]                 NVARCHAR (10)   NULL,
    [TaxReference]              NVARCHAR (50)   NULL,
    [SalesCode]                 NVARCHAR (50)   NULL,
    [TotalNettValue]            NUMERIC (13, 2) NULL,
    [LogisticsDataFee]          NUMERIC (13, 2) NULL,
    [NettExcLogDataFee]         NUMERIC (13, 2) NULL,
    [Tax]                       NUMERIC (13, 2) NULL,
    [TotalDue]                  NUMERIC (13, 2) NULL,
    [Message1]                  NVARCHAR (255)  NULL,
    [Message2]                  NVARCHAR (255)  NULL,
    [Message3]                  NVARCHAR (255)  NULL,
    [Printed]                   INT             CONSTRAINT [DF_InterfaceImportIPHeader_Printed] DEFAULT ((1)) NULL,
    [ProcessedDate]             DATETIME        NULL,
    [RecordStatus]              NVARCHAR (10)   DEFAULT ('N') NULL,
    [InsertDate]                DATETIME        CONSTRAINT [DF_InterfaceImportIPHeader_InsertDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK__InterfaceImportI__63544DF1] PRIMARY KEY CLUSTERED ([InterfaceImportIPHeaderId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK__Interface__Issue__653C9663] FOREIGN KEY ([IssueId]) REFERENCES [dbo].[Issue] ([IssueId]),
    CONSTRAINT [FK__Interface__Outbo__6448722A] FOREIGN KEY ([OutboundShipmentId]) REFERENCES [dbo].[OutboundShipment] ([OutboundShipmentId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportIPHeader_OutboundShipmentId]
    ON [dbo].[InterfaceImportIPHeader]([OutboundShipmentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportIPHeader_IssueId]
    ON [dbo].[InterfaceImportIPHeader]([IssueId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportIPHeader_OrderNumber]
    ON [dbo].[InterfaceImportIPHeader]([OrderNumber] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportIPHeader_Status_Dates]
    ON [dbo].[InterfaceImportIPHeader]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

