﻿CREATE TABLE [dbo].[DashboardOutboundCheckingAchieved] (
    [WarehouseId] INT           NULL,
    [OperatorId]  INT           NULL,
    [CreateDate]  DATETIME      NULL,
    [Legend]      NVARCHAR (50) NULL,
    [Value]       INT           NULL,
    [KPI]         INT           NULL
);

