﻿CREATE TABLE [dbo].[InterfaceImportTakeOnStock] (
    [InterfaceImportTakeOnStockId] INT             IDENTITY (1, 1) NOT NULL,
    [Location]                     NVARCHAR (50)   NULL,
    [ProductCode]                  NVARCHAR (30)   NULL,
    [Batch]                        NVARCHAR (50)   NULL,
    [ExpiryDate]                   DATETIME        NULL,
    [Quantity]                     FLOAT (53)      NULL,
    [UnitPrice]                    NUMERIC (13, 3) NULL,
    [ProcessedDate]                DATETIME        NULL,
    [RecordStatus]                 CHAR (1)        DEFAULT ('N') NULL,
    [RecordType]                   NVARCHAR (20)   NULL,
    [InsertDate]                   DATETIME        CONSTRAINT [DF_InterfaceImportTakeOnStock_InsertDate] DEFAULT ([dbo].[ufn_Getdate]()) NULL,
    [SKUCode]                      NVARCHAR (10)   NULL,
    [PrincipalCode]                NVARCHAR (30)   NULL,
    PRIMARY KEY CLUSTERED ([InterfaceImportTakeOnStockId] ASC) WITH (FILLFACTOR = 90)
);

