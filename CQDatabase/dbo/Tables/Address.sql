﻿CREATE TABLE [dbo].[Address] (
    [AddressId]         INT            IDENTITY (1, 1) NOT NULL,
    [ExternalCompanyId] INT            NOT NULL,
    [Street]            NVARCHAR (100) NULL,
    [Suburb]            NVARCHAR (100) NULL,
    [Town]              NVARCHAR (100) NULL,
    [Country]           NVARCHAR (100) NULL,
    [Code]              NVARCHAR (20)  NULL,
    CONSTRAINT [Address_PK] PRIMARY KEY CLUSTERED ([AddressId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [ExternalCompany_Address_FK1] FOREIGN KEY ([ExternalCompanyId]) REFERENCES [dbo].[ExternalCompany] ([ExternalCompanyId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Address_ExternalCompanyId]
    ON [dbo].[Address]([ExternalCompanyId] ASC) WITH (FILLFACTOR = 90);

