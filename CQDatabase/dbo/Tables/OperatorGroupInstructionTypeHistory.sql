﻿CREATE TABLE [dbo].[OperatorGroupInstructionTypeHistory] (
    [InstructionTypeId] INT             NULL,
    [OperatorGroupId]   INT             NULL,
    [OrderBy]           INT             NULL,
    [CommandType]       NVARCHAR (10)   NOT NULL,
    [InsertDate]        DATETIME        NOT NULL,
    [HourlyPickRate]    NUMERIC (13, 6) NULL
);

