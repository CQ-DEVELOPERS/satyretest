﻿CREATE TABLE [dbo].[ApplicationException] (
    [ApplicationExceptionId] INT            IDENTITY (1, 1) NOT NULL,
    [PriorityLevel]          INT            NULL,
    [Module]                 NVARCHAR (50)  NULL,
    [Page]                   NVARCHAR (50)  NULL,
    [Method]                 NVARCHAR (100) NULL,
    [ErrorMsg]               NTEXT          NULL,
    [CreateDate]             DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([ApplicationExceptionId] ASC) WITH (FILLFACTOR = 90)
);

