﻿CREATE TABLE [dbo].[OperatorGroup] (
    [OperatorGroupId]         INT           IDENTITY (1, 1) NOT NULL,
    [OperatorGroup]           NVARCHAR (30) NOT NULL,
    [RestrictedAreaIndicator] BIT           CONSTRAINT [DF_OperatorGroup_RestrictedAreaIndicator] DEFAULT ((0)) NULL,
    [OperatorGroupCode]       NVARCHAR (10) NULL,
    [NarrowAisle]             BIT           NULL,
    [Interleaving]            BIT           NULL,
    CONSTRAINT [OperatorGroup_PK] PRIMARY KEY CLUSTERED ([OperatorGroupId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_OperatorGroup] UNIQUE NONCLUSTERED ([OperatorGroup] ASC) WITH (FILLFACTOR = 90)
);


GO
create trigger tr_OperatorGroup_i
on OperatorGroup
for insert
as
begin
  insert AreaOperatorGroup
        (AreaId,
         OperatorGroupId)
  select a.AreaId,
         og.OperatorGroupId
    from Area      a,
         inserted og
  
  insert OperatorGroupInstructionType
        (InstructionTypeId,
         OperatorGroupId,
         OrderBy)
  select it.InstructionTypeId,
         og.OperatorGroupId,
         0
    from InstructionType it,
         inserted        og
  
  insert LocationOperatorGroup
        (LocationId,
         OperatorGroupId,
         OrderBy)
  select l.LocationId,
         og.OperatorGroupId,
         0
    from Location l,
         inserted og
end

GO
create trigger tr_OperatorGroup_d
on OperatorGroup
instead of delete
as
begin
  delete t
    from deleted d
    join GroupPerformance t on d.OperatorGroupId = t.OperatorGroupId
  
  delete t
    from deleted d
    join OperatorGroupInstructionType t on d.OperatorGroupId = t.OperatorGroupId
  
  delete t
    from deleted d
    join LocationOperatorGroup t on d.OperatorGroupId = t.OperatorGroupId
  
  delete t
    from deleted d
    join OperatorGroupMenuItem t on d.OperatorGroupId = t.OperatorGroupId
  
  delete t
    from deleted d
    join AreaOperatorGroup t on d.OperatorGroupId = t.OperatorGroupId
  
  delete t
    from deleted d
    join OperatorGroup t on d.OperatorGroupId = t.OperatorGroupId
end
