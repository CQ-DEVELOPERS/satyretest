﻿CREATE TABLE [dbo].[Location] (
    [LocationId]        INT              IDENTITY (1, 1) NOT NULL,
    [LocationTypeId]    INT              NOT NULL,
    [Location]          NVARCHAR (15)    NOT NULL,
    [Ailse]             NVARCHAR (10)    NULL,
    [Column]            NVARCHAR (10)    NULL,
    [Level]             NVARCHAR (10)    NULL,
    [PalletQuantity]    FLOAT (53)       NULL,
    [SecurityCode]      INT              NOT NULL,
    [RelativeValue]     NUMERIC (13, 3)  NULL,
    [NumberOfSUB]       INT              NOT NULL,
    [StocktakeInd]      BIT              NOT NULL,
    [ActiveBinning]     BIT              NOT NULL,
    [ActivePicking]     BIT              NOT NULL,
    [Used]              BIT              NOT NULL,
    [Latitude]          DECIMAL (16, 13) NULL,
    [Longitude]         DECIMAL (16, 13) NULL,
    [Direction]         NVARCHAR (10)    NULL,
    [Height]            NUMERIC (13, 6)  NULL,
    [Length]            NUMERIC (13, 6)  NULL,
    [Width]             NUMERIC (13, 6)  NULL,
    [Volume]            NUMERIC (13, 6)  NULL,
    [Weight]            NUMERIC (13, 6)  NULL,
    [NumberOfSU]        INT              NULL,
    [TrackLPN]          BIT              NULL,
    [FreightForwarding] NVARCHAR (100)   NULL,
    CONSTRAINT [Location_PK] PRIMARY KEY CLUSTERED ([LocationId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [LocationType_Location_FK1] FOREIGN KEY ([LocationTypeId]) REFERENCES [dbo].[LocationType] ([LocationTypeId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Location_LocationTypeId]
    ON [dbo].[Location]([LocationTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Location_ActiveBinning]
    ON [dbo].[Location]([ActiveBinning] ASC)
    INCLUDE([LocationId]);


GO
CREATE NONCLUSTERED INDEX [IX_Location_ActivePicking]
    ON [dbo].[Location]([ActivePicking] ASC)
    INCLUDE([LocationId]);


GO
CREATE NONCLUSTERED INDEX [IX_Location_StocktakeInd]
    ON [dbo].[Location]([StocktakeInd] ASC)
    INCLUDE([LocationId]);


GO
CREATE NONCLUSTERED INDEX [IX_Location_Location]
    ON [dbo].[Location]([Location] ASC) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_Location_Location1]
    ON [dbo].[Location]([Location] ASC)
    INCLUDE([LocationId], [Level]) WITH (FILLFACTOR = 70);


GO
create trigger tr_Location_i
on Location
for insert
as
begin
  insert LocationOperatorGroup
        (LocationId,
         OperatorGroupId,
         OrderBy)
  select l.LocationId,
         og.OperatorGroupId,
         0
    from inserted       l,
         OperatorGroup og
end
