﻿CREATE TABLE [dbo].[InterfaceImportBOM] (
    [InterfaceImportBOMId] INT            IDENTITY (1, 1) NOT NULL,
    [RecordStatus]         CHAR (1)       DEFAULT ('N') NULL,
    [ProcessedDate]        DATETIME       NULL,
    [ProductCode]          NVARCHAR (30)  NULL,
    [Quantity]             FLOAT (53)     NULL,
    [Product]              NVARCHAR (255) NULL,
    [PrimaryKey]           NVARCHAR (30)  NULL,
    [HostID]               NVARCHAR (30)  NULL,
    [InsertDate]           DATETIME       CONSTRAINT [DF_InterfaceImportBOM_InsertDate] DEFAULT (getdate()) NULL,
    [Additional1]          VARCHAR (255)  NULL,
    [Additional2]          NVARCHAR (255) NULL,
    [Additional3]          NVARCHAR (255) NULL,
    [Additional4]          NVARCHAR (255) NULL,
    [Additional5]          NVARCHAR (255) NULL,
    [Additional6]          NVARCHAR (255) NULL,
    [Additional7]          NVARCHAR (255) NULL,
    [Additional8]          NVARCHAR (255) NULL,
    [Additional9]          NVARCHAR (255) NULL
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportBOM_Status_Dates]
    ON [dbo].[InterfaceImportBOM]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

