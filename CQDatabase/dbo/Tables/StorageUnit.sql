﻿CREATE TABLE [dbo].[StorageUnit] (
    [StorageUnitId]     INT             IDENTITY (1, 1) NOT NULL,
    [SKUId]             INT             NOT NULL,
    [ProductId]         INT             NOT NULL,
    [ProductCategory]   CHAR (1)        NULL,
    [PackingCategory]   CHAR (1)        NULL,
    [PickEmpty]         BIT             NULL,
    [StackingCategory]  INT             NULL,
    [MovementCategory]  INT             NULL,
    [ValueCategory]     INT             NULL,
    [StoringCategory]   INT             NULL,
    [PickPartPallet]    INT             NULL,
    [MinimumQuantity]   FLOAT (53)      NULL,
    [ReorderQuantity]   FLOAT (53)      NULL,
    [MaximumQuantity]   FLOAT (53)      NULL,
    [UnitPrice]         NUMERIC (13, 2) NULL,
    [Size]              NVARCHAR (100)  NULL,
    [Colour]            NVARCHAR (100)  NULL,
    [Style]             NVARCHAR (100)  NULL,
    [PreviousUnitPrice] NUMERIC (13, 2) NULL,
    [StockTakeCounts]   INT             NULL,
    [DefaultQC]         BIT             NULL,
    [StatusId]          INT             NULL,
    [SerialTracked]     BIT             NULL,
    [SizeCode]          NVARCHAR (50)   NULL,
    [ColourCode]        NVARCHAR (50)   NULL,
    [StyleCode]         NVARCHAR (50)   NULL,
    [PutawayRule]       NVARCHAR (100)  NULL,
    [AllocationRule]    NVARCHAR (100)  NULL,
    [BillingRule]       NVARCHAR (100)  NULL,
    [CycleCountRule]    NVARCHAR (100)  NULL,
    [LotAttributeRule]  NVARCHAR (100)  NULL,
    [StorageCondition]  NVARCHAR (100)  NULL,
    [ManualCost]        NVARCHAR (30)   NULL,
    [OffSet]            NVARCHAR (30)   NULL,
    [ProductCategoryId] INT             NULL,
    CONSTRAINT [StorageUnit_PK] PRIMARY KEY CLUSTERED ([StorageUnitId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([ProductCategoryId]) REFERENCES [dbo].[ProductCategory] ([ProductCategoryId]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [Product_StorageUnit_FK1] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductId]),
    CONSTRAINT [SKU_StorageUnit_FK1] FOREIGN KEY ([SKUId]) REFERENCES [dbo].[SKU] ([SKUId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnit_SKUId]
    ON [dbo].[StorageUnit]([SKUId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnit_ProductId]
    ON [dbo].[StorageUnit]([ProductId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnit_ProductCategoryId]
    ON [dbo].[StorageUnit]([ProductCategoryId] ASC) WITH (FILLFACTOR = 90);


GO
create trigger tr_StorageUnit_i
on StorageUnit
for insert
as
begin
  insert StorageUnitBatch
        (StorageUnitId,
         BatchId)
  select i.StorageUnitId,
         b.BatchId
    from inserted           i
    join Batch              b on b.Batch = 'Default'
    left
    join StorageUnitBatch sub on i.StorageUnitId = sub.StorageUnitId
                             and b.BatchId        = b.BatchId
   where sub.StorageUnitBatchId is null
end
