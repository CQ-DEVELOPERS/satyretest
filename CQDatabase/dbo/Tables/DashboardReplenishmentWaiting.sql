﻿CREATE TABLE [dbo].[DashboardReplenishmentWaiting] (
    [WarehouseId] INT           NULL,
    [Area]        NVARCHAR (50) NULL,
    [Value]       INT           NULL,
    [KPI]         INT           NULL
);

