﻿CREATE TABLE [dbo].[OperatorGroupInstructionType] (
    [InstructionTypeId] INT             NOT NULL,
    [OperatorGroupId]   INT             NOT NULL,
    [OrderBy]           INT             NOT NULL,
    [HourlyPickRate]    NUMERIC (13, 6) NULL,
    CONSTRAINT [OperatorGroupInstructionType_PK] PRIMARY KEY CLUSTERED ([InstructionTypeId] ASC, [OperatorGroupId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [InstructionType_OperatorGroupInstructionType_FK1] FOREIGN KEY ([InstructionTypeId]) REFERENCES [dbo].[InstructionType] ([InstructionTypeId]),
    CONSTRAINT [OperatorGroup_OperatorGroupInstructionType_FK1] FOREIGN KEY ([OperatorGroupId]) REFERENCES [dbo].[OperatorGroup] ([OperatorGroupId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorGroupInstructionType_InstructionTypeId]
    ON [dbo].[OperatorGroupInstructionType]([InstructionTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorGroupInstructionType_OperatorGroupId]
    ON [dbo].[OperatorGroupInstructionType]([OperatorGroupId] ASC) WITH (FILLFACTOR = 90);

