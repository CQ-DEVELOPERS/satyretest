﻿CREATE TABLE [dbo].[ContactListHistory] (
    [ContactListId]     INT            NULL,
    [ExternalCompanyId] INT            NULL,
    [OperatorId]        INT            NULL,
    [ContactPerson]     NVARCHAR (255) NULL,
    [Telephone]         NVARCHAR (255) NULL,
    [Fax]               NVARCHAR (255) NULL,
    [EMail]             NVARCHAR (255) NULL,
    [CommandType]       NVARCHAR (10)  NOT NULL,
    [InsertDate]        DATETIME       NOT NULL,
    [Alias]             NVARCHAR (50)  NULL,
    [Type]              NVARCHAR (20)  NULL,
    [ReportTypeId]      INT            NULL
);

