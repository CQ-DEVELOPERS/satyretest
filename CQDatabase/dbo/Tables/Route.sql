﻿CREATE TABLE [dbo].[Route] (
    [RouteId]   INT           IDENTITY (1, 1) NOT NULL,
    [Route]     NVARCHAR (50) NULL,
    [RouteCode] NVARCHAR (10) NULL,
    PRIMARY KEY CLUSTERED ([RouteId] ASC) WITH (FILLFACTOR = 90)
);

