﻿CREATE TABLE [dbo].[InterfaceExportHeader] (
    [InterfaceExportHeaderId] INT            IDENTITY (1, 1) NOT NULL,
    [IssueId]                 INT            NULL,
    [PrimaryKey]              NVARCHAR (30)  NULL,
    [OrderNumber]             NVARCHAR (30)  NULL,
    [RecordType]              NVARCHAR (30)  DEFAULT ('SAL') NULL,
    [RecordStatus]            CHAR (1)       DEFAULT ('N') NOT NULL,
    [CompanyCode]             NVARCHAR (30)  NULL,
    [Company]                 NVARCHAR (255) NULL,
    [Address]                 NVARCHAR (255) NULL,
    [FromWarehouseCode]       NVARCHAR (30)  NULL,
    [ToWarehouseCode]         NVARCHAR (30)  NULL,
    [Route]                   NVARCHAR (50)  NULL,
    [DeliveryNoteNumber]      NVARCHAR (30)  NULL,
    [ContainerNumber]         NVARCHAR (30)  NULL,
    [SealNumber]              NVARCHAR (30)  NULL,
    [DeliveryDate]            DATETIME       NULL,
    [Remarks]                 NVARCHAR (255) NULL,
    [NumberOfLines]           INT            NULL,
    [Additional1]             NVARCHAR (255) NULL,
    [Additional2]             NVARCHAR (255) NULL,
    [Additional3]             NVARCHAR (255) NULL,
    [Additional4]             NVARCHAR (255) NULL,
    [Additional5]             NVARCHAR (255) NULL,
    [Additional6]             NVARCHAR (255) NULL,
    [Additional7]             NVARCHAR (255) NULL,
    [Additional8]             NVARCHAR (255) NULL,
    [Additional9]             NVARCHAR (255) NULL,
    [Additional10]            NVARCHAR (255) NULL,
    [ProcessedDate]           DATETIME       NULL,
    [InsertDate]              DATETIME       CONSTRAINT [DF_InterfaceExportHeader_InsertDate] DEFAULT (getdate()) NULL,
    [WebServiceMsg]           NVARCHAR (MAX) NULL,
    [PrincipalCode]           NVARCHAR (30)  NULL,
    [FinalDelivery]           BIT            CONSTRAINT [DF__Interface__Final__28EAE296] DEFAULT ((0)) NULL,
    [ReceiptId]               INT            NULL,
    [OrderStatus]             VARCHAR (20)   NULL,
    [ReasonCode]              NVARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([InterfaceExportHeaderId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceExportHeader_Status_Dates]
    ON [dbo].[InterfaceExportHeader]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceExportHeader_OrderNumber_RecordType]
    ON [dbo].[InterfaceExportHeader]([OrderNumber] ASC, [RecordType] ASC)
    INCLUDE([InterfaceExportHeaderId]);


GO
CREATE NONCLUSTERED INDEX [IX_InterfaceExportHeader_IssueId_RecordType]
    ON [dbo].[InterfaceExportHeader]([IssueId] ASC, [RecordType] ASC) WITH (FILLFACTOR = 70);

