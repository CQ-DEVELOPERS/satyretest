﻿CREATE TABLE [dbo].[Result] (
    [ResultId]   INT            IDENTITY (1, 1) NOT NULL,
    [ResultCode] NVARCHAR (10)  NULL,
    [Result]     NVARCHAR (255) NULL,
    [Pass]       BIT            CONSTRAINT [DF_Result_Pass] DEFAULT ((0)) NULL,
    [StartRange] FLOAT (53)     NULL,
    [EndRange]   FLOAT (53)     NULL,
    PRIMARY KEY CLUSTERED ([ResultId] ASC) WITH (FILLFACTOR = 90)
);

