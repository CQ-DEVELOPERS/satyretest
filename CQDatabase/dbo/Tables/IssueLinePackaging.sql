﻿CREATE TABLE [dbo].[IssueLinePackaging] (
    [PackageLineId]      INT        IDENTITY (1, 1) NOT NULL,
    [IssueId]            INT        NULL,
    [IssueLineId]        INT        NULL,
    [JobId]              INT        NULL,
    [StorageUnitBatchId] INT        NOT NULL,
    [StorageUnitId]      INT        NULL,
    [StatusId]           INT        NULL,
    [OperatorId]         INT        NULL,
    [Quantity]           FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([PackageLineId] ASC) WITH (FILLFACTOR = 90)
);

