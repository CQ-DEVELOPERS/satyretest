﻿CREATE TABLE [dbo].[StatusHistory] (
    [StatusId]    INT           NULL,
    [Status]      NVARCHAR (50) NULL,
    [StatusCode]  NVARCHAR (10) NULL,
    [Type]        CHAR (2)      NULL,
    [CommandType] NVARCHAR (10) NOT NULL,
    [InsertDate]  DATETIME      NOT NULL,
    [OrderBy]     INT           NULL
);

