﻿CREATE TABLE [dbo].[InterfaceMappingFile] (
    [InterfaceMappingFileId]  INT            IDENTITY (1, 1) NOT NULL,
    [PrincipalId]             INT            NULL,
    [InterfaceFileTypeId]     INT            NULL,
    [InterfaceDocumentTypeId] INT            NULL,
    [Delimiter]               CHAR (2)       NULL,
    [StyleSheet]              XML            NULL,
    [PrincipalFTPSite]        NVARCHAR (MAX) NULL,
    [FTPUserName]             NVARCHAR (50)  NULL,
    [FTPPassword]             NVARCHAR (50)  NULL,
    [HeaderRow]               INT            DEFAULT ((0)) NULL,
    [SubDirectory]            NVARCHAR (MAX) NULL,
    [FilePrefix]              NVARCHAR (30)  NULL,
    [FileExtension]           NVARCHAR (30)  NULL,
    PRIMARY KEY CLUSTERED ([InterfaceMappingFileId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([InterfaceDocumentTypeId]) REFERENCES [dbo].[InterfaceDocumentType] ([InterfaceDocumentTypeId]),
    FOREIGN KEY ([InterfaceFileTypeId]) REFERENCES [dbo].[InterfaceFileType] ([InterfaceFileTypeId]),
    FOREIGN KEY ([PrincipalId]) REFERENCES [dbo].[Principal] ([PrincipalId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceMappingFile_InterfaceDocumentTypeId]
    ON [dbo].[InterfaceMappingFile]([InterfaceDocumentTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceMappingFile_InterfaceFileTypeId]
    ON [dbo].[InterfaceMappingFile]([InterfaceFileTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceMappingFile_PrincipalId]
    ON [dbo].[InterfaceMappingFile]([PrincipalId] ASC) WITH (FILLFACTOR = 90);


GO
create trigger tr_InterfaceMappingFile_d
on InterfaceMappingFile
instead of delete
as
begin
  delete t
    from deleted d
    join InterfaceMappingColumn t on d.InterfaceMappingFileId = t.InterfaceMappingFileId
  
  delete t
    from deleted d
    join InterfaceMappingFile t on d.InterfaceMappingFileId = t.InterfaceMappingFileId
end
