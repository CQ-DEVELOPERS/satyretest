﻿CREATE TABLE [dbo].[Process] (
    [ProcessId]   INT           IDENTITY (1, 1) NOT NULL,
    [ProcessCode] NVARCHAR (30) NULL,
    [Process]     NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([ProcessId] ASC) WITH (FILLFACTOR = 90)
);

