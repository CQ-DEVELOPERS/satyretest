﻿CREATE TABLE [dbo].[Exception] (
    [ExceptionId]   INT            IDENTITY (1, 1) NOT NULL,
    [ReceiptLineId] INT            NULL,
    [InstructionId] INT            NULL,
    [IssueLineId]   INT            NULL,
    [ReasonId]      INT            NULL,
    [Exception]     NVARCHAR (255) NULL,
    [ExceptionCode] NVARCHAR (10)  NULL,
    [CreateDate]    DATETIME       NULL,
    [OperatorId]    INT            NULL,
    [ExceptionDate] DATETIME       NULL,
    [JobId]         INT            NULL,
    [Detail]        SQL_VARIANT    NULL,
    [Quantity]      FLOAT (53)     NULL,
    CONSTRAINT [PK__Exception__4EF38B7F] PRIMARY KEY CLUSTERED ([ExceptionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK__Exception__Instr__50DBD3F1] FOREIGN KEY ([InstructionId]) REFERENCES [dbo].[Instruction] ([InstructionId]),
    CONSTRAINT [FK__Exception__Issue__51CFF82A] FOREIGN KEY ([IssueLineId]) REFERENCES [dbo].[IssueLine] ([IssueLineId]),
    CONSTRAINT [FK__Exception__JobId__27C8A878] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Job] ([JobId]),
    CONSTRAINT [FK__Exception__Opera__29B0F0EA] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [FK__Exception__Reaso__52C41C63] FOREIGN KEY ([ReasonId]) REFERENCES [dbo].[Reason] ([ReasonId]),
    CONSTRAINT [FK__Exception__Recei__4FE7AFB8] FOREIGN KEY ([ReceiptLineId]) REFERENCES [dbo].[ReceiptLine] ([ReceiptLineId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Exception_OperatorId]
    ON [dbo].[Exception]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Exception_IssueLineId]
    ON [dbo].[Exception]([IssueLineId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Exception_InstructionId]
    ON [dbo].[Exception]([InstructionId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Exception_JobId]
    ON [dbo].[Exception]([JobId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Exception_ReceiptLineId]
    ON [dbo].[Exception]([ReceiptLineId] ASC) WITH (FILLFACTOR = 90);

