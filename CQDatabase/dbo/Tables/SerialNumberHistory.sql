﻿CREATE TABLE [dbo].[SerialNumberHistory] (
    [SerialNumberId]     INT            NULL,
    [StorageUnitId]      INT            NULL,
    [LocationId]         INT            NULL,
    [ReceiptId]          INT            NULL,
    [StoreInstructionId] INT            NULL,
    [PickInstructionId]  INT            NULL,
    [SerialNumber]       NVARCHAR (50)  NULL,
    [ReceivedDate]       DATETIME       NULL,
    [DespatchDate]       DATETIME       NULL,
    [CommandType]        NVARCHAR (10)  NOT NULL,
    [InsertDate]         DATETIME       NOT NULL,
    [ReferenceNumber]    NVARCHAR (30)  NULL,
    [BatchId]            INT            NULL,
    [IssueId]            INT            NULL,
    [IssueLineId]        INT            NULL,
    [ReceiptLineId]      INT            NULL,
    [StoreJobId]         INT            NULL,
    [PickJobId]          INT            NULL,
    [Remarks]            NVARCHAR (255) NULL,
    [StatusId]           INT            NULL
);

