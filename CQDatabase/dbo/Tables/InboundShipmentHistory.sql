﻿CREATE TABLE [dbo].[InboundShipmentHistory] (
    [InboundShipmentId] INT            NULL,
    [StatusId]          INT            NULL,
    [WarehouseId]       INT            NULL,
    [ShipmentDate]      DATETIME       NULL,
    [Remarks]           NVARCHAR (255) NULL,
    [CommandType]       NVARCHAR (10)  NOT NULL,
    [InsertDate]        DATETIME       NOT NULL,
    [LocationId]        INT            NULL,
    [VehicleId]         INT            NULL
);

