﻿CREATE TABLE [dbo].[Job] (
    [JobId]            INT           IDENTITY (1, 1) NOT NULL,
    [PriorityId]       INT           NOT NULL,
    [OperatorId]       INT           NULL,
    [StatusId]         INT           NOT NULL,
    [WarehouseId]      INT           NOT NULL,
    [ReceiptLineId]    INT           NULL,
    [IssueLineId]      INT           NULL,
    [ContainerTypeId]  INT           NULL,
    [ReferenceNumber]  NVARCHAR (30) NULL,
    [TareWeight]       FLOAT (53)    NULL,
    [Weight]           FLOAT (53)    NULL,
    [NettWeight]       FLOAT (53)    NULL,
    [CheckedBy]        INT           NULL,
    [CheckedDate]      DATETIME      NULL,
    [DropSequence]     INT           NULL,
    [Pallets]          INT           NULL,
    [BackFlush]        BIT           NULL,
    [Prints]           INT           NULL,
    [CheckingClear]    BIT           NULL,
    [OriginalJobId]    INT           NULL,
    [AuthorisedBy]     INT           NULL,
    [AuthorisedStatus] NVARCHAR (10) NULL,
    [TrackingNumber]   NVARCHAR (30) NULL,
    CONSTRAINT [Job_PK] PRIMARY KEY CLUSTERED ([JobId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK__Job__CheckedBy__3C89F72A] FOREIGN KEY ([CheckedBy]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [FK_Job_ContainerType] FOREIGN KEY ([ContainerTypeId]) REFERENCES [dbo].[ContainerType] ([ContainerTypeId]),
    CONSTRAINT [FK_Job_IssueLine] FOREIGN KEY ([IssueLineId]) REFERENCES [dbo].[IssueLine] ([IssueLineId]),
    CONSTRAINT [FK_Job_ReceiptLine] FOREIGN KEY ([ReceiptLineId]) REFERENCES [dbo].[ReceiptLine] ([ReceiptLineId]),
    CONSTRAINT [Priority_Job_FK1] FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    CONSTRAINT [Status_Job_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [Warehouse_Job_FK1] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Job_StatusId]
    ON [dbo].[Job]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Job_WarehouseId]
    ON [dbo].[Job]([WarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Job_ContainerTypeId]
    ON [dbo].[Job]([ContainerTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Job_OperatorId]
    ON [dbo].[Job]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Job_PriorityId]
    ON [dbo].[Job]([PriorityId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Job_IssueLineId]
    ON [dbo].[Job]([IssueLineId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Job_ReceiptLineId]
    ON [dbo].[Job]([ReceiptLineId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Job_CheckedBy]
    ON [dbo].[Job]([CheckedBy] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Job]
    ON [dbo].[Job]([WarehouseId] ASC, [StatusId] ASC, [PriorityId] ASC, [OperatorId] ASC, [ReferenceNumber] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Job_ReferenceNumber]
    ON [dbo].[Job]([ReferenceNumber] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_Job_WarehouseId_ReferenceNumber]
    ON [dbo].[Job]([WarehouseId] ASC, [ReferenceNumber] ASC) WITH (FILLFACTOR = 70);


GO
CREATE NONCLUSTERED INDEX [IX_Job_StatusId]
    ON [dbo].[Job]([StatusId] ASC)
    INCLUDE([JobId], [WarehouseId]) WITH (FILLFACTOR = 70);

