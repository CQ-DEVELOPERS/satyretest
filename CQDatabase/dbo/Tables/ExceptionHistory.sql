﻿CREATE TABLE [dbo].[ExceptionHistory] (
    [ExceptionId]   INT            NULL,
    [ReceiptLineId] INT            NULL,
    [InstructionId] INT            NULL,
    [IssueLineId]   INT            NULL,
    [ReasonId]      INT            NULL,
    [Exception]     NVARCHAR (255) NULL,
    [ExceptionCode] NVARCHAR (10)  NULL,
    [CreateDate]    DATETIME       NULL,
    [OperatorId]    INT            NULL,
    [ExceptionDate] DATETIME       NULL,
    [JobId]         INT            NULL,
    [Detail]        SQL_VARIANT    NULL,
    [CommandType]   NVARCHAR (10)  NOT NULL,
    [InsertDate]    DATETIME       NOT NULL,
    [Quantity]      FLOAT (53)     NULL
);

