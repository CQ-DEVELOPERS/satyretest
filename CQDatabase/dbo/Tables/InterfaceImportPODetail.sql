﻿CREATE TABLE [dbo].[InterfaceImportPODetail] (
    [InterfaceImportPOHeaderId] INT            NULL,
    [ForeignKey]                NVARCHAR (30)  NULL,
    [LineNumber]                INT            NULL,
    [ProductCode]               NVARCHAR (30)  NOT NULL,
    [Product]                   NVARCHAR (50)  NULL,
    [SKUCode]                   NVARCHAR (10)  NULL,
    [Batch]                     NVARCHAR (50)  NULL,
    [Quantity]                  FLOAT (53)     NOT NULL,
    [Weight]                    FLOAT (53)     NULL,
    [Additional1]               NVARCHAR (255) NULL,
    [Additional2]               NVARCHAR (255) NULL,
    [Additional3]               NVARCHAR (255) NULL,
    [Additional4]               NVARCHAR (255) NULL,
    [Additional5]               NVARCHAR (255) NULL,
    [Additional6]               NVARCHAR (255) NULL,
    [Additional7]               NVARCHAR (255) NULL,
    [Additional8]               NVARCHAR (255) NULL,
    [Additional9]               NVARCHAR (255) NULL,
    [Additional10]              NVARCHAR (255) NULL,
    FOREIGN KEY ([InterfaceImportPOHeaderId]) REFERENCES [dbo].[InterfaceImportPOHeader] ([InterfaceImportPOHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportPODetail_InterfaceImportPOHeaderId]
    ON [dbo].[InterfaceImportPODetail]([InterfaceImportPOHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportPODetail]
    ON [dbo].[InterfaceImportPODetail]([InterfaceImportPOHeaderId] ASC, [ForeignKey] ASC) WITH (FILLFACTOR = 90);

