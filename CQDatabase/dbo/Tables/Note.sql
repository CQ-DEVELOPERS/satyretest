﻿CREATE TABLE [dbo].[Note] (
    [NoteId]   INT            IDENTITY (1, 1) NOT NULL,
    [NoteCode] NVARCHAR (255) NULL,
    [Note]     NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([NoteId] ASC) WITH (FILLFACTOR = 90)
);

