﻿CREATE TABLE [dbo].[StorageUnitLabel] (
    [LabelingCategoryId] INT NULL,
    [StorageUnitId]      INT NULL,
    FOREIGN KEY ([LabelingCategoryId]) REFERENCES [dbo].[LabelingCategory] ([LabelingCategoryId]),
    FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitLabel_LabelingCategoryId]
    ON [dbo].[StorageUnitLabel]([LabelingCategoryId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitLabel_StorageUnitId]
    ON [dbo].[StorageUnitLabel]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);

