﻿CREATE TABLE [dbo].[InboundDocumentContactList] (
    [InboundDocumentId] INT NULL,
    [ContactListId]     INT NULL,
    FOREIGN KEY ([ContactListId]) REFERENCES [dbo].[ContactList] ([ContactListId]),
    FOREIGN KEY ([InboundDocumentId]) REFERENCES [dbo].[InboundDocument] ([InboundDocumentId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocumentContactList_InboundDocumentId]
    ON [dbo].[InboundDocumentContactList]([InboundDocumentId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InboundDocumentContactList_ContactListId]
    ON [dbo].[InboundDocumentContactList]([ContactListId] ASC) WITH (FILLFACTOR = 90);

