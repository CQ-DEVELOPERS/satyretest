﻿CREATE TABLE [dbo].[InterfaceImportOrderNumbers] (
    [InterfaceImportOrderNumbersId] INT           IDENTITY (1, 1) NOT NULL,
    [PrimaryKey]                    NVARCHAR (30) NULL,
    [RecordStatus]                  NVARCHAR (10) DEFAULT ('N') NULL,
    [OrderNumber]                   NVARCHAR (30) NULL,
    [OrderDate]                     DATETIME      NULL,
    [Status]                        NVARCHAR (30) NULL,
    [OutboundDocument]              BIT           NULL,
    [InboundDocument]               BIT           NULL,
    [ProcessedDate]                 DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([InterfaceImportOrderNumbersId] ASC) WITH (FILLFACTOR = 90)
);

