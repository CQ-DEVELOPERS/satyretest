﻿CREATE TABLE [dbo].[OutboundShipmentInstructionType] (
    [OutboundShipmentId] INT             NULL,
    [InstructionTypeId]  INT             NULL,
    [Quantity]           NUMERIC (13, 6) NULL,
    [OperatorGroupId]    INT             NULL
);

