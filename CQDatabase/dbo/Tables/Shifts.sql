﻿CREATE TABLE [dbo].[Shifts] (
    [ShiftId]   INT IDENTITY (1, 1) NOT NULL,
    [Starttime] INT NULL,
    [EndTime]   INT NULL,
    PRIMARY KEY CLUSTERED ([ShiftId] ASC) WITH (FILLFACTOR = 90)
);

