﻿CREATE TABLE [dbo].[InterfaceInvoice] (
    [InterfaceInvoiceId] INT           IDENTITY (1, 1) NOT NULL,
    [OrderNumber]        NVARCHAR (30) NULL,
    [InvoiceNumber]      NVARCHAR (30) NULL,
    [ProcessedDate]      DATETIME      NULL,
    [RecordStatus]       CHAR (1)      DEFAULT ('N') NULL,
    PRIMARY KEY CLUSTERED ([InterfaceInvoiceId] ASC) WITH (FILLFACTOR = 90)
);

