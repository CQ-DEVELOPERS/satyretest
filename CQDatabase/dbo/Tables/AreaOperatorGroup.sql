﻿CREATE TABLE [dbo].[AreaOperatorGroup] (
    [AreaId]          INT NOT NULL,
    [OperatorGroupId] INT NOT NULL,
    CONSTRAINT [AreaOperatorGroup_PK] PRIMARY KEY CLUSTERED ([AreaId] ASC, [OperatorGroupId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Area_AreaOperatorGroup_FK1] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Area] ([AreaId]),
    CONSTRAINT [Operators_AreaOperatorGroup_FK1] FOREIGN KEY ([OperatorGroupId]) REFERENCES [dbo].[OperatorGroup] ([OperatorGroupId])
);


GO
CREATE NONCLUSTERED INDEX [nci_AreaOperatorGroup_OperatorGroupId]
    ON [dbo].[AreaOperatorGroup]([OperatorGroupId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_AreaOperatorGroup_AreaId]
    ON [dbo].[AreaOperatorGroup]([AreaId] ASC) WITH (FILLFACTOR = 90);

