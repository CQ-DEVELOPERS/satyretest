﻿CREATE TABLE [dbo].[LocationBayType] (
    [LocationId] INT NOT NULL,
    [BayTypeId]  INT NOT NULL,
    CONSTRAINT [LocationBayType_PK] PRIMARY KEY CLUSTERED ([LocationId] ASC, [BayTypeId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [BayType_LocationBayType_FK1] FOREIGN KEY ([BayTypeId]) REFERENCES [dbo].[BayType] ([BayTypeId]),
    CONSTRAINT [Location_LocationBayType_FK1] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId])
);

