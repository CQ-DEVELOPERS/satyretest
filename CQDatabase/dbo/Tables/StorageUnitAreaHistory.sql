﻿CREATE TABLE [dbo].[StorageUnitAreaHistory] (
    [StorageUnitId]   INT           NULL,
    [AreaId]          INT           NULL,
    [StoreOrder]      INT           NULL,
    [PickOrder]       INT           NULL,
    [CommandType]     NVARCHAR (10) NOT NULL,
    [InsertDate]      DATETIME      NOT NULL,
    [MinimumQuantity] FLOAT (53)    NULL,
    [ReorderQuantity] FLOAT (53)    NULL,
    [MaximumQuantity] FLOAT (53)    NULL
);

