﻿CREATE TABLE [dbo].[InterfaceImportDetail] (
    [InterfaceImportHeaderId] INT             NULL,
    [ForeignKey]              NVARCHAR (30)   NULL,
    [LineNumber]              INT             NULL,
    [ProductCode]             NVARCHAR (30)   NOT NULL,
    [Product]                 NVARCHAR (50)   NULL,
    [SKUCode]                 NVARCHAR (30)   NULL,
    [Batch]                   NVARCHAR (50)   NULL,
    [Quantity]                FLOAT (53)      NOT NULL,
    [Weight]                  FLOAT (53)      NULL,
    [RetailPrice]             DECIMAL (13, 2) NULL,
    [NetPrice]                DECIMAL (13, 2) NULL,
    [LineTotal]               DECIMAL (13, 2) NULL,
    [Volume]                  DECIMAL (13, 3) NULL,
    [Additional1]             NVARCHAR (255)  NULL,
    [Additional2]             NVARCHAR (255)  NULL,
    [Additional3]             NVARCHAR (255)  NULL,
    [Additional4]             NVARCHAR (255)  NULL,
    [Additional5]             NVARCHAR (255)  NULL,
    [BOELineNumber]           NVARCHAR (50)   NULL,
    [AlternativeSKU]          NVARCHAR (50)   NULL,
    [CountryofOrigin]         NVARCHAR (50)   NULL,
    [Height]                  NUMERIC (13, 6) NULL,
    [Width]                   NUMERIC (13, 6) NULL,
    [Length]                  NUMERIC (13, 6) NULL,
    [TariffCode]              NVARCHAR (50)   NULL,
    [Reference1]              NVARCHAR (50)   NULL,
    [Reference2]              NVARCHAR (50)   NULL,
    [ExpiryDate]              DATETIME        NULL,
    [UnitPrice]               NUMERIC (13, 2) NULL,
    [SKU]                     NVARCHAR (255)  NULL,
    FOREIGN KEY ([InterfaceImportHeaderId]) REFERENCES [dbo].[InterfaceImportHeader] ([InterfaceImportHeaderId])
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportDetail_InterfaceImportHeaderId]
    ON [dbo].[InterfaceImportDetail]([InterfaceImportHeaderId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceImportDetail]
    ON [dbo].[InterfaceImportDetail]([InterfaceImportHeaderId] ASC, [ForeignKey] ASC) WITH (FILLFACTOR = 90);

