﻿CREATE TABLE [dbo].[Menu] (
    [MenuId] INT           IDENTITY (1, 1) NOT NULL,
    [Menu]   NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([MenuId] ASC) WITH (FILLFACTOR = 90)
);

