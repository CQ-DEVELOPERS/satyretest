﻿CREATE TABLE [dbo].[InboundShipmentReceiptHistory] (
    [InboundShipmentId] INT           NULL,
    [ReceiptId]         INT           NULL,
    [CommandType]       NVARCHAR (10) NOT NULL,
    [InsertDate]        DATETIME      NOT NULL
);

