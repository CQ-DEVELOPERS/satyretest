﻿CREATE TABLE [dbo].[Shipment] (
    [ShipmentId]          INT            IDENTITY (1, 1) NOT NULL,
    [StatusId]            INT            NOT NULL,
    [WarehouseId]         INT            NOT NULL,
    [LocationId]          INT            NULL,
    [ShipmentDate]        DATETIME       NOT NULL,
    [Remarks]             NVARCHAR (250) NULL,
    [SealNumber]          NVARCHAR (30)  NULL,
    [VehicleRegistration] NVARCHAR (10)  NULL,
    [Route]               NVARCHAR (10)  NULL,
    CONSTRAINT [Shipment_PK] PRIMARY KEY CLUSTERED ([ShipmentId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Shipment_Location] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [FK_Shipment_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [FK_Shipment_Warehouse] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Shipment_LocationId]
    ON [dbo].[Shipment]([LocationId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Shipment_StatusId]
    ON [dbo].[Shipment]([StatusId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Shipment_WarehouseId]
    ON [dbo].[Shipment]([WarehouseId] ASC) WITH (FILLFACTOR = 90);

