﻿CREATE TABLE [dbo].[Warehouse] (
    [WarehouseId]       INT           IDENTITY (1, 1) NOT NULL,
    [CompanyId]         INT           NULL,
    [Warehouse]         NVARCHAR (50) NOT NULL,
    [WarehouseCode]     NVARCHAR (30) NULL,
    [HostId]            NVARCHAR (30) NULL,
    [ParentWarehouseId] INT           NULL,
    [AddressId]         INT           NULL,
    [DesktopMaximum]    INT           NULL,
    [DesktopWarning]    INT           NULL,
    [MobileWarning]     INT           NULL,
    [MobileMaximum]     INT           NULL,
    [UploadToHost]      BIT           NULL,
    [PostalAddressId]   INT           NULL,
    [ContactListId]     INT           NULL,
    [PrincipalId]       INT           NULL,
    [AllowInbound]      BIT           NULL,
    [AllowOutbound]     BIT           NULL,
    [WarehouseCode2]    NVARCHAR (30) NULL,
    CONSTRAINT [Warehouse_PK] PRIMARY KEY CLUSTERED ([WarehouseId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Address] ([AddressId]),
    FOREIGN KEY ([ContactListId]) REFERENCES [dbo].[ContactList] ([ContactListId]),
    FOREIGN KEY ([ParentWarehouseId]) REFERENCES [dbo].[Warehouse] ([WarehouseId]),
    FOREIGN KEY ([PrincipalId]) REFERENCES [dbo].[Principal] ([PrincipalId]),
    CONSTRAINT [Company_Warehouse_FK1] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [uc_Warehouse_PrincipalId] UNIQUE NONCLUSTERED ([Warehouse] ASC, [PrincipalId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_WarehouseCode_PrincipalId] UNIQUE NONCLUSTERED ([WarehouseCode] ASC, [PrincipalId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [nci_Warehouse_ParentWarehouseId]
    ON [dbo].[Warehouse]([ParentWarehouseId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Warehouse_CompanyId]
    ON [dbo].[Warehouse]([CompanyId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Warehouse_HostId]
    ON [dbo].[Warehouse]([HostId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Warehouse_ContactListId]
    ON [dbo].[Warehouse]([ContactListId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Warehouse_PostalAddressId]
    ON [dbo].[Warehouse]([PostalAddressId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Warehouse_AddressId]
    ON [dbo].[Warehouse]([AddressId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_Warehouse_PrincipalId]
    ON [dbo].[Warehouse]([PrincipalId] ASC) WITH (FILLFACTOR = 90);


GO
create trigger tr_Warehouse_i
on Warehouse
for insert
as
begin
  update Warehouse
     set DesktopMaximum = 10,
         DesktopWarning = 10,
         MobileMaximum = 10,
         MobileWarning = 10,
         ParentWarehouseId = w.WarehouseId
    from inserted  i
    join Warehouse w on i.WarehouseId = w.WarehouseId
end
