﻿CREATE TABLE [dbo].[Setting] (
    [SettingId]   INT            IDENTITY (1, 1) NOT NULL,
    [SettingCode] NVARCHAR (100) NULL,
    [Setting]     NVARCHAR (MAX) NULL,
    [OperatorId]  INT            NULL,
    PRIMARY KEY CLUSTERED ([SettingId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId])
);


GO
CREATE NONCLUSTERED INDEX [nci_Setting_OperatorId]
    ON [dbo].[Setting]([OperatorId] ASC) WITH (FILLFACTOR = 90);

