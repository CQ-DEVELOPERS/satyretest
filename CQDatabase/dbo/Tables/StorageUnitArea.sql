﻿CREATE TABLE [dbo].[StorageUnitArea] (
    [StorageUnitId]   INT        NOT NULL,
    [AreaId]          INT        NOT NULL,
    [StoreOrder]      INT        NULL,
    [PickOrder]       INT        NULL,
    [MinimumQuantity] FLOAT (53) NULL,
    [ReorderQuantity] FLOAT (53) NULL,
    [MaximumQuantity] FLOAT (53) NULL,
    CONSTRAINT [StorageUnitArea_PK] PRIMARY KEY CLUSTERED ([StorageUnitId] ASC, [AreaId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [Area_StorageUnitArea_FK1] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Area] ([AreaId]),
    CONSTRAINT [StorageUnit_StorageUnitArea_FK1] FOREIGN KEY ([StorageUnitId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);


GO
CREATE NONCLUSTERED INDEX [nci_StorageUnitArea_StorageUnitId]
    ON [dbo].[StorageUnitArea]([StorageUnitId] ASC) WITH (FILLFACTOR = 90);

