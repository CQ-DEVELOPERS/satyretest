﻿CREATE TABLE [dbo].[LabelingCategory] (
    [LabelingCategoryId]   INT            IDENTITY (1, 1) NOT NULL,
    [LabelingCategory]     NVARCHAR (255) NULL,
    [LabelingCategoryCode] NVARCHAR (30)  NULL,
    [Additional1]          NVARCHAR (255) NULL,
    [Additional2]          NVARCHAR (255) NULL,
    [Additional3]          NVARCHAR (255) NULL,
    [Additional4]          NVARCHAR (255) NULL,
    [Additional5]          NVARCHAR (255) NULL,
    [ManualLabel]          BIT            DEFAULT ((0)) NULL,
    [ScreenGaurd]          BIT            DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([LabelingCategoryId] ASC) WITH (FILLFACTOR = 90)
);

