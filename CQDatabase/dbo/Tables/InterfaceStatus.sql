﻿CREATE TABLE [dbo].[InterfaceStatus] (
    [StatusId]          INT           IDENTITY (1, 1) NOT NULL,
    [StatusCode]        NVARCHAR (10) NOT NULL,
    [StatusDescription] NVARCHAR (50) NOT NULL
);

