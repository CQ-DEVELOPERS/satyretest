﻿CREATE TABLE [dbo].[RaiserrorCodeHistory] (
    [RaiserrorCodeId]  INT            NULL,
    [RaiserrorCode]    NVARCHAR (10)  NULL,
    [RaiserrorMessage] NVARCHAR (255) NULL,
    [StoredProcedure]  NVARCHAR (256) NULL,
    [CommandType]      NVARCHAR (10)  NOT NULL,
    [InsertDate]       DATETIME       NOT NULL
);

