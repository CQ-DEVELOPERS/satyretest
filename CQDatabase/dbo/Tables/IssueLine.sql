﻿CREATE TABLE [dbo].[IssueLine] (
    [IssueLineId]        INT             IDENTITY (1, 1) NOT NULL,
    [IssueId]            INT             NOT NULL,
    [OutboundLineId]     INT             NOT NULL,
    [StorageUnitBatchId] INT             NOT NULL,
    [StatusId]           INT             NOT NULL,
    [OperatorId]         INT             NULL,
    [Quantity]           FLOAT (53)      NOT NULL,
    [ConfirmedQuatity]   FLOAT (53)      NULL,
    [Weight]             FLOAT (53)      NULL,
    [ConfirmedWeight]    FLOAT (53)      NULL,
    [ParentIssueLineId]  INT             NULL,
    [DeliveredQuantity]  NUMERIC (13, 6) NULL,
    [SOHQuantity]        NUMERIC (13, 6) NULL,
    CONSTRAINT [PK_IssueLine] PRIMARY KEY CLUSTERED ([IssueLineId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_IssueLine_Operator] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId]),
    CONSTRAINT [Issue_IssueLine_FK1] FOREIGN KEY ([IssueId]) REFERENCES [dbo].[Issue] ([IssueId]),
    CONSTRAINT [OutboundLine_IssueLine_FK1] FOREIGN KEY ([OutboundLineId]) REFERENCES [dbo].[OutboundLine] ([OutboundLineId]),
    CONSTRAINT [Status_IssueLine_FK1] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusId]),
    CONSTRAINT [StorageUnitBatch_IssueLine_FK1] FOREIGN KEY ([StorageUnitBatchId]) REFERENCES [dbo].[StorageUnitBatch] ([StorageUnitBatchId])
);


GO
CREATE NONCLUSTERED INDEX [nci_IssueLine_StorageUnitBatchId]
    ON [dbo].[IssueLine]([StorageUnitBatchId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IssueLine_IssueId]
    ON [dbo].[IssueLine]([IssueId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IssueLine_OperatorId]
    ON [dbo].[IssueLine]([OperatorId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IssueLine_OutboundLineId]
    ON [dbo].[IssueLine]([OutboundLineId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_IssueLine_StatusId]
    ON [dbo].[IssueLine]([StatusId] ASC) WITH (FILLFACTOR = 90);

