﻿CREATE TABLE [dbo].[Logo] (
    [LogoId]     INT            IDENTITY (1, 1) NOT NULL,
    [ReportName] NVARCHAR (255) NULL,
    [Logo1]      IMAGE          NULL,
    [Logo2]      IMAGE          NULL,
    [Logo3]      IMAGE          NULL,
    [Logo4]      IMAGE          NULL,
    [Logo5]      IMAGE          NULL,
    PRIMARY KEY CLUSTERED ([LogoId] ASC) WITH (FILLFACTOR = 90)
);

