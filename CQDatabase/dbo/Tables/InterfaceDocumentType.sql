﻿CREATE TABLE [dbo].[InterfaceDocumentType] (
    [InterfaceDocumentTypeId]   INT            IDENTITY (1, 1) NOT NULL,
    [InterfaceDocumentTypeCode] NVARCHAR (30)  NULL,
    [InterfaceDocumentType]     NVARCHAR (50)  NULL,
    [RecordType]                NVARCHAR (30)  NULL,
    [InterfaceType]             NVARCHAR (30)  NULL,
    [InDirectory]               BIT            DEFAULT ((0)) NULL,
    [OutDirectory]              BIT            DEFAULT ((0)) NULL,
    [SubDirectory]              NVARCHAR (MAX) NULL,
    [FilePrefix]                NVARCHAR (30)  NULL,
    [FileExtension]             NVARCHAR (30)  NULL,
    PRIMARY KEY CLUSTERED ([InterfaceDocumentTypeId] ASC) WITH (FILLFACTOR = 90)
);

