﻿CREATE TABLE [dbo].[AreaCheck] (
    [AreaCheckId] INT IDENTITY (1, 1) NOT NULL,
    [AreaId]      INT NOT NULL,
    CONSTRAINT [PK_AreaCheck] PRIMARY KEY CLUSTERED ([AreaCheckId] ASC),
    CONSTRAINT [FK_AreaCheck_AreaCheck] FOREIGN KEY ([AreaCheckId]) REFERENCES [dbo].[Area] ([AreaId])
);

