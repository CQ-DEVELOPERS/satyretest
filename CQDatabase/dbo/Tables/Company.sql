﻿CREATE TABLE [dbo].[Company] (
    [CompanyId]    INT           IDENTITY (1, 1) NOT NULL,
    [Company]      NVARCHAR (50) NOT NULL,
    [CompanyCode]  NVARCHAR (10) NULL,
    [CustomsCode]  NVARCHAR (20) NULL,
    [Registration] NVARCHAR (20) NULL,
    CONSTRAINT [Company_PK] PRIMARY KEY CLUSTERED ([CompanyId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_Company] UNIQUE NONCLUSTERED ([Company] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_CompanyCode] UNIQUE NONCLUSTERED ([CompanyCode] ASC) WITH (FILLFACTOR = 90)
);

