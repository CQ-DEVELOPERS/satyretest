﻿CREATE TABLE [dbo].[MobileLogStockTakeAudit] (
    [AuditId]    INT            IDENTITY (1, 1) NOT NULL,
    [ProcName]   NVARCHAR (MAX) NULL,
    [IPAdd]      NVARCHAR (MAX) NULL,
    [OperatorId] INT            NULL,
    [Old]        NVARCHAR (MAX) NULL,
    [New]        NVARCHAR (MAX) NULL,
    [Params]     NVARCHAR (MAX) NULL,
    [CreateDate] DATETIME       DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([AuditId] ASC)
);

