﻿CREATE TABLE [dbo].[BOMHeader] (
    [BOMHeaderId]   INT            IDENTITY (1, 1) NOT NULL,
    [StorageUnitId] INT            NOT NULL,
    [Description]   NVARCHAR (50)  NULL,
    [Remarks]       NVARCHAR (MAX) NULL,
    [Instruction]   TEXT           NULL,
    [BOMType]       NVARCHAR (50)  NULL,
    [HostId]        INT            NULL,
    [Quantity]      FLOAT (53)     NULL,
    CONSTRAINT [PK_BOMHeader] PRIMARY KEY CLUSTERED ([BOMHeaderId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_BOMHeader_StorageUnit] FOREIGN KEY ([BOMHeaderId]) REFERENCES [dbo].[StorageUnit] ([StorageUnitId])
);

