﻿CREATE TABLE [dbo].[InterfaceLocation] (
    [HostId]              NVARCHAR (30)  NULL,
    [Location]            NVARCHAR (30)  NULL,
    [LocationDescription] NVARCHAR (255) NULL,
    [Address1]            NVARCHAR (255) NULL,
    [Address2]            NVARCHAR (255) NULL,
    [Address3]            NVARCHAR (255) NULL,
    [Address4]            NVARCHAR (255) NULL,
    [City]                NVARCHAR (255) NULL,
    [State]               NVARCHAR (255) NULL,
    [Zip]                 NVARCHAR (255) NULL,
    [ProcessedDate]       DATETIME       NULL,
    [RecordStatus]        CHAR (1)       DEFAULT ('N') NULL,
    [InsertDate]          DATETIME       CONSTRAINT [DF_InterfaceLocation_InsertDate] DEFAULT (getdate()) NULL
);


GO
CREATE NONCLUSTERED INDEX [nci_InterfaceLocation_Status_Dates]
    ON [dbo].[InterfaceLocation]([RecordStatus] ASC, [ProcessedDate] ASC, [InsertDate] ASC) WITH (FILLFACTOR = 90);

