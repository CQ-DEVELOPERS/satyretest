﻿CREATE TABLE [dbo].[InterfaceImportStockTake] (
    [InterfaceImportStockTakeId] INT            IDENTITY (1, 1) NOT NULL,
    [Location]                   NVARCHAR (510) NULL,
    [ProductCode]                NVARCHAR (510) NULL,
    [Description]                NVARCHAR (510) NULL,
    [Batch]                      NVARCHAR (510) NULL,
    [CQQty]                      FLOAT (53)     NULL,
    [Count1]                     NVARCHAR (510) NULL,
    [Count2]                     NVARCHAR (510) NULL,
    [Count3]                     NVARCHAR (510) NULL,
    [NewItemCode]                NVARCHAR (510) NULL,
    [NewDescription]             NVARCHAR (510) NULL,
    [CurrentCount]               FLOAT (53)     NULL,
    [SKUCode]                    NVARCHAR (510) NULL,
    [Area]                       NVARCHAR (510) NULL,
    [RecordStatus]               CHAR (1)       NULL,
    [ProcessedDate]              DATETIME       NULL
);

