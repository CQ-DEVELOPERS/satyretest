﻿CREATE TABLE [dbo].[TaskList] (
    [TaskListId]    INT            IDENTITY (1, 1) NOT NULL,
    [PriorityId]    INT            NULL,
    [ProjectId]     INT            NULL,
    [RequestTypeId] INT            NULL,
    [BillableId]    INT            NULL,
    [TaskList]      NVARCHAR (MAX) NULL,
    [Comments]      NVARCHAR (MAX) NULL,
    [RaisedBy]      INT            NULL,
    [ApprovedBy]    INT            NULL,
    [AllocatedTo]   INT            NULL,
    [Tester]        INT            NULL,
    [EstimateHours] FLOAT (53)     NULL,
    [ActualHours]   FLOAT (53)     NULL,
    [CreateDate]    DATETIME       CONSTRAINT [DF__TaskList__Create__53EA4F34] DEFAULT (getdate()) NULL,
    [TargetDate]    DATETIME       NULL,
    [Percentage]    FLOAT (53)     NULL,
    [CycleId]       INT            NULL,
    [OrderBy]       INT            NULL,
    PRIMARY KEY CLUSTERED ([TaskListId] ASC) WITH (FILLFACTOR = 90),
    FOREIGN KEY ([AllocatedTo]) REFERENCES [dbo].[Operator] ([OperatorId]),
    FOREIGN KEY ([BillableId]) REFERENCES [dbo].[Billable] ([BillableId]),
    FOREIGN KEY ([CycleId]) REFERENCES [dbo].[Cycle] ([CycleId]),
    FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[Priority] ([PriorityId]),
    FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ProjectId]),
    FOREIGN KEY ([RequestTypeId]) REFERENCES [dbo].[RequestType] ([RequestTypeId])
);


GO
CREATE NONCLUSTERED INDEX [nci_TaskList_AllocatedTo]
    ON [dbo].[TaskList]([AllocatedTo] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TaskList_CycleId]
    ON [dbo].[TaskList]([CycleId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TaskList_RequestTypeId]
    ON [dbo].[TaskList]([RequestTypeId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TaskList_ApprovedBy]
    ON [dbo].[TaskList]([ApprovedBy] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TaskList_Tester]
    ON [dbo].[TaskList]([Tester] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TaskList_RaisedBy]
    ON [dbo].[TaskList]([RaisedBy] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TaskList_BillableId]
    ON [dbo].[TaskList]([BillableId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TaskList_PriorityId]
    ON [dbo].[TaskList]([PriorityId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [nci_TaskList_ProjectId]
    ON [dbo].[TaskList]([ProjectId] ASC) WITH (FILLFACTOR = 90);

