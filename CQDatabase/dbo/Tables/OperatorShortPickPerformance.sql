﻿CREATE TABLE [dbo].[OperatorShortPickPerformance] (
    [OperatorId]        INT        NULL,
    [InstructionTypeId] INT        NULL,
    [EndDate]           DATETIME   NULL,
    [ActivityStatus]    CHAR (1)   NULL,
    [PickingRate]       INT        NULL,
    [Units]             INT        NULL,
    [UnitsShort]        INT        NULL,
    [Weight]            FLOAT (53) NULL,
    [WeightShort]       FLOAT (53) NULL,
    [Instructions]      INT        NULL,
    [Orders]            INT        NULL,
    [OrderLines]        INT        NULL,
    [Jobs]              INT        NULL,
    [ActiveTime]        INT        NULL,
    [DwellTime]         INT        NULL,
    [WarehouseId]       INT        NULL,
    CONSTRAINT [FK__OperatorS__Opera__20B19695] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[Operator] ([OperatorId])
);


GO
CREATE NONCLUSTERED INDEX [nci_OperatorShortPickPerformance_OperatorId]
    ON [dbo].[OperatorShortPickPerformance]([OperatorId] ASC) WITH (FILLFACTOR = 90);

