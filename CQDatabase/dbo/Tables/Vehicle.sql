﻿CREATE TABLE [dbo].[Vehicle] (
    [VehicleId]    INT           IDENTITY (1, 1) NOT NULL,
    [Registration] NVARCHAR (20) NOT NULL,
    PRIMARY KEY CLUSTERED ([VehicleId] ASC) WITH (FILLFACTOR = 90)
);

