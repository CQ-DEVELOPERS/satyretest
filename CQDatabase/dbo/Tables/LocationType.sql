﻿CREATE TABLE [dbo].[LocationType] (
    [LocationTypeId]            INT           IDENTITY (1, 1) NOT NULL,
    [LocationType]              NVARCHAR (50) NOT NULL,
    [LocationTypeCode]          NVARCHAR (10) NOT NULL,
    [DeleteIndicator]           BIT           NOT NULL,
    [ReplenishmentIndicator]    BIT           NOT NULL,
    [MultipleProductsIndicator] BIT           NOT NULL,
    [BatchTrackingIndicator]    BIT           NOT NULL,
    CONSTRAINT [LocationType_PK] PRIMARY KEY CLUSTERED ([LocationTypeId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_LocationType] UNIQUE NONCLUSTERED ([LocationType] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [uc_LocationTypeCode] UNIQUE NONCLUSTERED ([LocationTypeCode] ASC) WITH (FILLFACTOR = 90)
);

