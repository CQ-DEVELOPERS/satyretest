﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Pick_Bin_Exception
  ///   Filename       : p_Report_Pick_Bin_Exception.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Pick_Bin_Exception
(
 @ConnectionString nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId      int,
 @LocationId       int = null,
 @FromDate         datetime,
 @ToDate           datetime
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  InstructionTypeId  int,
	  InstructionType    nvarchar(30),
	  CreateDate         datetime,
	  OperatorId         int,
	  Operator           nvarchar(50),
	  StorageUnitBatchId int,
	  ProductCode        nvarchar(30),
	  Product            nvarchar(50),
	  SKUCode            nvarchar(50),
	  Batch              nvarchar(50),
	  Location           nvarchar(15),
   PickLocationId     int null,
   PickLocation       nvarchar(15),
   StoreLocationId    int null,
   StoreLocation      nvarchar(15),
	  Quantity           float,
	  ConfirmedQuantity  float
	 )
	 
	 if @LocationId = -1
	   set @LocationId = null
  
  insert @TableResult
        (InstructionTypeId,
	        CreateDate,
	        OperatorId,
	        StorageUnitBatchId,
         PickLocationId,
         StoreLocationId,
	        Quantity,
	        ConfirmedQuantity)
  select i.InstructionTypeId,
	        i.CreateDate,
	        i.OperatorId,
	        i.StorageUnitBatchId,
         i.PickLocationId,
         i.StoreLocationId,
	        i.Quantity,
	        i.ConfirmedQuantity
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where i.WarehouseId      = @WarehouseId
     and i.CreateDate between @FromDate and @ToDate
     and(isnull(i.StoreLocationId,-1)  = isnull(@LocationId, isnull(i.StoreLocationId,-1))
     or  isnull(i.PickLocationId,-1)   = isnull(@LocationId, isnull(i.PickLocationId,-1)))
     and it.InstructionTypeCode = 'STE'
  
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit          su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product               p (nolock) on su.ProductId          = p.ProductId
    join SKU                 sku (nolock) on su.SKUId              = sku.SKUId
    join Batch                 b (nolock) on sub.BatchId           = b.BatchId
  
  update tr
     set InstructionType = it.InstructionType
    from @TableResult    tr
    join InstructionType it (nolock) on tr.InstructionTypeId = it.InstructionTypeId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId

  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId

  update tr
     set Location      = l.Location,
         StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
  
  select InstructionType
	       ,CreateDate
	       ,Operator
	       ,ProductCode
	       ,Product
	       ,SKUCode
	       ,Batch
	       ,isnull(Location,PickLocation) as 'Location'
	       ,PickLocation
	       ,StoreLocation
	       ,Quantity
	       ,ConfirmedQuantity
    from @TableResult
  order by Location
end
