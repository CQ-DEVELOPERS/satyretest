﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_SOHNotification_Subscription
  ///   Filename       : p_Report_SOHNotification_Subscription.sql
  ///   Create By      : Gareth
  ///   Date Created   : 19 December 2017
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_SOHNotification_Subscription

as
begin
	 set nocount on;

declare @ReceiptId		int,
			 @ReportTypeId	int
	
	select @ReportTypeId = rt.reporttypeid
	from   ReportType    rt (nolock)
	where  rt.reporttypecode = 'SOH'
	
 select (select Value 
			  from Configuration 
			 where ConfigurationId = 200 
			   and WarehouseId = WarehouseId)    as 'ServerName',
			db_name()							 as 'DatabaseName',
			'Auto Email'						 as 'UserName',
			-1									 as ConnectionString,
			cl.EMail							 as 'Email',
			cl.EMail							 as 'TO',
			'SOH Report'			 as 'Subject'
			from ContactList cl
			where ReportTypeId = @ReportTypeId
	  end 
