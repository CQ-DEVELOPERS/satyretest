﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Truck_Checking_Slip
  ///   Filename       : p_Report_Truck_Checking_Slip.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Aug 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Truck_Checking_Slip
(
 @WarehouseId			int,
 @FromDate				date,
 @ToDate				date,
 @ExternalCompanyId		int = null,
 @OrderNumber			nvarchar(50) = null,
 @OutboundShipmentId	int = null
)

as
begin
	 set nocount on;
  
   declare @TableResult as table
  (
   OutboundShipmentId nvarchar(30),
   IssueId            int,
   IssueLineId		  int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   ExternalCompanyCode nvarchar(50),
   DespatchDate       datetime,
   LocationId         int,
   Location           nvarchar(15),
   JobId              int,
   StatusId           int,
   Status             nvarchar(50),
   JobStatusId        int,
   JobStatusCode      nvarchar(10),
   InstructionTypeId  int,
   InstructionTypeCode nvarchar(10),
   InstructionType    nvarchar(50),
   InstructionId      int,
   Pallet             nvarchar(10),
   PalletId           int,
   StorageUnitBatchId int,
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   QuantityOnHand     float,
   DropSequence       int,
   RouteId            int,
   Route              nvarchar(50),
   Loaded             char(1),
   ReferenceNumber	  nvarchar(30),
   ProductQuantity	  numeric(13,6),
   SerialForGeysers   nvarchar(50),
   Product		      nvarchar(50)
  )
  
  if (@OrderNumber = '')
	select @OrderNumber = null
  
  if (@ExternalCompanyId = -1)
	select @ExternalCompanyId = null

  if (@OutboundShipmentId = -1)
	select @OutboundShipmentId = null
 
  insert @TableResult
        (OutboundShipmentId,
         OutboundDocumentId,
         JobId,
         StatusId,
         JobStatusId,
         InstructionTypeId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ReferenceNumber,
         PalletId,
         Product,
         IssueLineId)
         
  select ili.OutboundShipmentId,
         ili.OutboundDocumentId,
         ins.JobId,
         ins.StatusId,
         j.StatusId,
         ins.InstructionTypeId,
         ins.InstructionId,
         ins.StorageUnitBatchId,
         ins.Quantity,
         j.ReferenceNumber,
         ins.PalletId,
         p.Product,
         ili.IssueLineId
    from IssueLineInstruction  ili (nolock)
    join Instruction           ins (nolock) on ili.InstructionId = ins.InstructionId
    join Job                     j (nolock) on ins.JobId         = j.JobId
    join Status                  s (nolock) on j.StatusId        = s.StatusId
    join StorageUnitBatch		sub(nolock) on ins.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit			 su(nolock) on sub.StorageUnitId	  = su.StorageUnitId
    join Product				  p(nolock) on su.ProductId			  = p.ProductId
    join OutboundDocument		 od(nolock) on ili.OutboundDocumentId = od.OutboundDocumentId 
   where ins.CreateDate between @FromDate and dateadd(dd,1,@ToDate)
     and isnull(@ExternalCompanyId, od.ExternalCompanyId) = od.ExternalCompanyId
     and (ili.OutboundShipmentId = @OutboundShipmentId or @OutboundShipmentId is null)
     and isnull(@OrderNumber, od.OrderNumber) = od.OrderNumber
     and isnull(p.Category,'') not in ('GEYSER')
     and j.ReferenceNumber is not null
     
  insert @TableResult
        (OutboundShipmentId,
         OutboundDocumentId,
         JobId,
         StatusId,
         JobStatusId,
         InstructionTypeId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ReferenceNumber,
         PalletId,
         Product,
         IssueLineId)
         
  select ili.OutboundShipmentId,
         ili.OutboundDocumentId,
         insRef.JobId,
         insRef.StatusId,
         j.StatusId,
         insRef.InstructionTypeId,
         insRef.InstructionId,
         insRef.StorageUnitBatchId,
         insRef.Quantity,
         j.ReferenceNumber,
         insRef.PalletId,
         p.Product,
         ili.IssueLineId
    from IssueLineInstruction  ili (nolock)
    join Instruction           ins (nolock) on ili.InstructionId = ins.InstructionId
	join Instruction		insRef (nolock)	on ins.InstructionId = insRef.InstructionRefId									
    join Job                     j (nolock) on ins.JobId         = j.JobId
    join Status                  s (nolock) on j.StatusId        = s.StatusId
    join StorageUnitBatch		sub(nolock) on ins.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit			 su(nolock) on sub.StorageUnitId	  = su.StorageUnitId
    join Product				  p(nolock) on su.ProductId			  = p.ProductId
    join OutboundDocument		 od(nolock) on ili.OutboundDocumentId = od.OutboundDocumentId 
   where ins.CreateDate between @FromDate and dateadd(dd,1,@ToDate)
     and isnull(@ExternalCompanyId, od.ExternalCompanyId) = od.ExternalCompanyId
     and (ili.OutboundShipmentId = @OutboundShipmentId or @OutboundShipmentId is null)
     and isnull(@OrderNumber, od.OrderNumber) = od.OrderNumber
     and isnull(p.Category,'') not in ('GEYSER')
     
  
  insert @TableResult
        (OutboundShipmentId,
         OutboundDocumentId,
         JobId,
         StatusId,
         JobStatusId,
         InstructionTypeId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ReferenceNumber,
         PalletId,
         Product,
         IssueLineId,
         SerialForGeysers)
     
  select ili.OutboundShipmentId,
         ili.OutboundDocumentId,
         ins.JobId,
         ins.StatusId,
         j.StatusId,
         ins.InstructionTypeId,
         ins.InstructionId,
         ins.StorageUnitBatchId,
         1,
         j.ReferenceNumber,
         ins.PalletId,
         p.Product,
         ili.IssueLineId,
         sn.SerialNumber
    from IssueLineInstruction  ili (nolock)
    join Instruction           ins (nolock) on ili.InstructionId = ins.InstructionId
    join Job                     j (nolock) on ins.JobId         = j.JobId
    join Status                  s (nolock) on j.StatusId        = s.StatusId
    join StorageUnitBatch		sub(nolock) on ins.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit			 su(nolock) on sub.StorageUnitId	  = su.StorageUnitId
    join Product				  p(nolock) on su.ProductId			  = p.ProductId
    join OutboundDocument		 od(nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
    join SerialNumber			 sn(nolock) on ili.IssueLineId		  = sn.IssueLineId 
   where ins.CreateDate between @FromDate and dateadd(dd,1,@ToDate)
     and isnull(@ExternalCompanyId, od.ExternalCompanyId) = od.ExternalCompanyId
     and (ili.OutboundShipmentId = @OutboundShipmentId or @OutboundShipmentId is null)
     and isnull(@OrderNumber, od.OrderNumber) = od.OrderNumber
     and isnull(p.Category,'') in ('GEYSER')
  
  update tr
     set DespatchDate = os.ShipmentDate,
         RouteId      = os.RouteId,
         DropSequence = osi.DropSequence,
         LocationId   = os.LocationId
    from @TableResult     tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId
   where tr.OutboundShipmentId is not null
  
  update tr
     set DespatchDate = i.DeliveryDate,
         RouteId      = i.RouteId,
         DropSequence = i.DropSequence,
         LocationId   = i.LocationId
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
   where tr.OutboundShipmentId is null
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set ExternalCompany = ec.ExternalCompany,
		 ExternalCompanyCode = ec.ExternalCompanyCode
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set Location = l.Location
    from @TableResult    tr
    join Location         l (nolock) on tr.LocationId = l.LocationId
    
  declare @InboundSequence smallint,
		 @PackTypeId	int,
		 @PackType   	nvarchar(30),
		 @ShowWeight	bit = null
		 
  select @ShowWeight = Indicator
  from Configuration where ConfigurationId = 206
 
  select @InboundSequence = max(InboundSequence)
  from PackType (nolock) 
  
  select @PackTypeId = pt.PackTypeId
  from PackType pt  where @InboundSequence = pt.InboundSequence
  
    
  update tr
     set Pallet = convert(nvarchar(10), j.DropSequence)
               + ' of '
               + convert(nvarchar(10), j.Pallets)
    from @TableResult tr
    join Job           j (nolock) on tr.JobId = j.JobId

  
  update tr
     set InstructionType     = 'Full',
         InstructionTypeCode = it.InstructionTypeCode
    from @TableResult tr
    join InstructionType it on tr.InstructionTypeId = it.InstructionTypeId
   where it.InstructionTypeCode = 'P'
  
  update @TableResult
     set InstructionType     = 'Mixed'
   where InstructionType is null
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s on tr.StatusId = s.StatusId
  where tr.InstructionTypeCode = 'P'
    and s.StatusCode           = 'NS'
  
  update tr
     set JobStatusCode = s.StatusCode
    from @TableResult tr
    join Status        s on tr.JobStatusId = s.StatusId
  
  update @TableResult
     set Loaded = 'a'
   where JobStatusCode = 'C'
  
  update @TableResult
     set Loaded = 'r',
         Pallet = null
   where JobStatusCode = 'NS'
  
  update tr
     set Loaded = 'r',
         Status = (select Status from Status where StatusCode = 'NS'),
         Pallet = null
    from @TableResult tr
   where not exists(select 1 from @TableResult tr2 where tr.JobId = tr2.JobId and ConfirmedQuantity > 0)
  
  update @TableResult
     set Loaded = 'c'
   where Loaded is null
  
  select OutboundShipmentId,
         DropSequence,
         Route,
         DespatchDate,
         ExternalCompanyCode,
         ExternalCompany,
         Location,
         min(InstructionType) as 'InstructionType',
         JobId,
         Status,
         OrderNumber,
         Pallet,
         PalletId,
         sum(Quantity) as 'Quantity',  
         --sum(Weight) as 'Weight',
         Loaded,
         ReferenceNumber,
         Product,
         SerialForGeysers
    from @TableResult
  group by OutboundShipmentId,
         DropSequence,
         Route,
         DespatchDate,
         ExternalCompanyCode,
         ExternalCompany,
         Location,
         JobId,
         Status,
         OrderNumber,
         Pallet,
         Loaded,
         ReferenceNumber,
         PalletId,
         Quantity,
         Product,
         SerialForGeysers
  order by OutboundShipmentId,
		   ExternalCompany,
		   OrderNumber
		   
           --JobId
end
 
