﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Load_Details
  ///   Filename       : p_Wave_Planning_Load_Details.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Load_Details
(
 @waveId int
)

as
begin
	 set nocount on;
  
  declare @OutboundDocumentId int,
          @LoadWeight         float,
          @LoadVolume         float,
          @NumberOfOrders     int,
          @NumberOfDrops      int,
          @InboundSequence    int,
          @DistinctStorageUnit int
  
  declare @TableResult as table
  (
   WarehouseId        int,
   OutboundDocumentId int,
   ExternalCompanyId  int,
   Weight             float,
   Volume             float
  )
  
  insert @TableResult
        (WarehouseId,
         OutboundDocumentId,
         ExternalCompanyId,
         Weight,
         Volume)
  select i.WarehouseId,
         od.OutboundDocumentId,
         od.ExternalCompanyId,
         i.Weight,
         i.Volume
    from Issue                   i (nolock)
    join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
    join OutboundDocumentType  odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                           and odt.OutboundDocumentTypeCode != 'WAV'
   where i.WaveId = @waveId
  
  select @NumberOfOrders = count(1),
         @NumberOfDrops  = count(distinct(ExternalCompanyId))
    from @TableResult
  
  select @DistinctStorageUnit = count(distinct(StorageUnitId))
    from @TableResult tr
    join OutboundLine ol (nolock) on tr.OutboundDocumentId = ol.OutboundDocumentId
  
  select sum(Weight)         as 'LoadWeight',
         sum(Volume)         as 'LoadVolume',
         @NumberOfOrders     as 'NumberOfOrders',
         @NumberOfDrops      as 'NumberOfDrops',
         @DistinctStorageUnit as 'DistinctStorageUnit'
    from @TableResult
end
