﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Production_Detail
  ///   Filename       : p_Report_Production_Detail.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Jun 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Production_Detail
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId       int,
 @OrderNumber       nvarchar(30),
 @StorageUnitId		int,
 @BatchId           int,
 @StatusId			int,
 @FromDate          datetime,
 @ToDate            datetime,
 @DateSel			nvarchar(1)
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OrderNumber        nvarchar(30),
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(255),
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   ConfirmedQuantity  float,
   StatusId           int,
   Status             nvarchar(50),
   CreateDate         datetime,
   StartDate          datetime,
   Hours              nvarchar(8),
   Minutes            nvarchar(2),
   Seconds            nvarchar(3),
   PalletId           int,
   NettWeight         float,
   InstructionId	  int,
   EndDate			  datetime,
   InsOrderHead		  nvarchar(30),
   CrtAcceptHead	  nvarchar(30),
   TrfCompleteHead	  nvarchar(30),
   TransferNumber	  nvarchar(100)
  )
  
  
declare @PrintPlasonVer 	bit
		
set @PrintPlasonVer = 0
  
 
  select @PrintPlasonVer = Indicator
  from Configuration where ConfigurationId = 308
  
	 
	 if @OrderNumber = '-1'
	   set @OrderNumber = null
	   
	 if @BatchId = -1
	   set @BatchId = null
	 
	 if @StorageUnitId = -1
	   set @StorageUnitId = null
	   
	 if @StatusId = -1
	   set @StatusId = null
	   
  if @DateSel = 'P'	
	--if  @PrintPlasonVer = 0
		insert	@TableResult
				(OrderNumber,
				StorageUnitBatchId,
				ConfirmedQuantity,
				StatusId,
				CreateDate,
				StartDate,
			    PalletId,
			    NettWeight,
			    InstructionId,
			    EndDate)
		 select id.OrderNumber,
			    rl.StorageUnitBatchId,
			    i.ConfirmedQuantity,
			    j.StatusId,
			    i.CreateDate,
		        isnull(j.CheckedDate, i.StartDate),
			    i.PalletId,
			    j.NettWeight,
			    i.InstructionId,
			    i.EndDate
		   from Instruction           i (nolock)
		   join Job                   j (nolock) on i.JobId = j.JobId
		   join ReceiptLine          rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
		   join Receipt               r (nolock) on rl.ReceiptId = r.ReceiptId
		   join InboundDocument      id (nolock) on r.InboundDocumentId = id.InboundDocumentId
		   join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
		   join StorageUnitBatch    sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
		  where id.OrderNumber              = isnull(@OrderNumber, id.OrderNumber)
		    and sub.BatchId                 = isnull(@BatchId, sub.BatchId)
		    and i.CreateDate               between @FromDate and @ToDate
		    and idt.InboundDocumentTypeCode = 'PRV'
		    and i.WarehouseId               = @WarehouseId
		    and sub.StorageUnitId			= isnull(@StorageUnitId, sub.StorageUnitId)
		    and j.StatusId					= ISNULL(@StatusId, j.StatusId)
  if @DateSel = 'W'	
	--if  @PrintPlasonVer = 0
		insert	@TableResult
			    (OrderNumber,
				StorageUnitBatchId,
				ConfirmedQuantity,
				StatusId,
				CreateDate,
				StartDate,
			    PalletId,
			    NettWeight,
			    InstructionId,
			    EndDate)
		 select id.OrderNumber,
			    rl.StorageUnitBatchId,
			    i.ConfirmedQuantity,
			    j.StatusId,
			    i.CreateDate,
		        isnull(j.CheckedDate, i.StartDate),
			    i.PalletId,
			    j.NettWeight,
			    i.InstructionId,
			    i.EndDate
		   from Instruction           i (nolock)
		   join Job                   j (nolock) on i.JobId = j.JobId
		   join ReceiptLine          rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
		   join Receipt               r (nolock) on rl.ReceiptId = r.ReceiptId
		   join InboundDocument      id (nolock) on r.InboundDocumentId = id.InboundDocumentId
		   join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
		   join StorageUnitBatch    sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
		  where id.OrderNumber              = isnull(@OrderNumber, id.OrderNumber)
		    and sub.BatchId                 = isnull(@BatchId, sub.BatchId)
		    and j.CheckedDate        between @FromDate and @ToDate
		    and idt.InboundDocumentTypeCode = 'PRV'
		    and i.WarehouseId               = @WarehouseId
		    and sub.StorageUnitId		    = isnull(@StorageUnitId, sub.StorageUnitId)
		   and j.StatusId					= ISNULL(@StatusId, j.StatusId)
		    
		      
 
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         Batch       = b.Batch
    from @TableResult tr
    join StorageUnitBatch sub on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su on sub.StorageUnitId     = su.StorageUnitId
    join Product            p on su.ProductId = p.ProductId
    join Batch              b on sub.BatchId = b.BatchId
    join SKU              sku on su.SKUId              = sku.SKUId
  
  if @PrintPlasonVer = 0
  update @TableResult
     set Hours   =  DateDiff(ss, CreateDate, StartDate) / 3600,
         Minutes = (DateDiff(ss, CreateDate, StartDate) % 3600) / 60,
         Seconds =  DateDiff(ss, CreateDate, StartDate) % 60
         
  if @PrintPlasonVer = 1
  update @TableResult
     set Hours   =  DateDiff(ss, StartDate, isnull(EndDate, GETDATE())) / 3600,
         Minutes = (DateDiff(ss, StartDate, isnull(EndDate, GETDATE())) % 3600) / 60,
         Seconds =  DateDiff(ss, StartDate, isnull(EndDate, GETDATE())) % 60
  
  update @TableResult
     set Minutes = '0' + Minutes
   where datalength(Minutes) = 1
  
  update @TableResult
     set Seconds = '0' + Seconds
   where datalength(Seconds) = 1
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.StatusId = s.StatusId
  
  update tr
     set NettWeight = pk.Weight * tr.ConfirmedQuantity
    from @TableResult           tr
    join Product                 p (nolock) on tr.ProductCode   = p.ProductCode
    join StorageUnit            su (nolock) on p.ProductId      = su.ProductId
    join Pack                   pk (nolock) on su.StorageUnitId = pk.StorageUnitId
    join PackType               pt (nolock) on pk.PackTypeId    = pt.PackTypeId
   where pt.InboundSequence in (select max(InboundSequence) from PackType)
     and isnull(su.ProductCategory ,'') != 'V'
  
  if @PrintPlasonVer = 0
  select CONVERT(nvarchar, isnull(InstructionId,0)) as OrderNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         ConfirmedQuantity,
         Status,
         CreateDate as Date1,
         StartDate as Date2,
         Hours + ':' + Minutes + ':' + Seconds as 'DwellTime',
         PalletId,
         NettWeight,
         'Order Number' as InsOrderHead,
		 'Date Created' as CrtAcceptHead,
		 'Date Transferred' as TrfCompleteHead
    from @TableResult
  order by CreateDate
 if @PrintPlasonVer = 1
    select CONVERT(nvarchar, isnull(InstructionId,0)) as OrderNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         ConfirmedQuantity,
         Status,
         StartDate as Date1,
         EndDate as Date2,
         Hours + ':' + Minutes + ':' + Seconds as 'DwellTime',
         PalletId,
         NettWeight,
         'Instruction Id' as InsOrderHead,
		 'Accept Date' as CrtAcceptHead,
		 'Complete Date' as TrfCompleteHead,
		 TransferNumber
    from @TableResult
  order by CreateDate
end
 
