﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKUCheck_Search
  ///   Filename       : p_SKUCheck_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:35
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the SKUCheck table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   SKUCheck.JobId,
  ///   SKUCheck.SKUCode,
  ///   SKUCheck.Quantity,
  ///   SKUCheck.ConfirmedQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKUCheck_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         SKUCheck.JobId
        ,SKUCheck.SKUCode
        ,SKUCheck.Quantity
        ,SKUCheck.ConfirmedQuantity
    from SKUCheck
  
end
