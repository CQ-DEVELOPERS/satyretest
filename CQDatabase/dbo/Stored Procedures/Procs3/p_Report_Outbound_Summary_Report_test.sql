﻿ 
/*
/// <summary>
///   Procedure Name : p_Report_Outbound_Summary_Report_test
///   Filename       : p_Report_Outbound_Summary_Report_test.sql
///   Create By      : Karen
///   Date Created   : October 2013
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
CREATE procedure p_Report_Outbound_Summary_Report_test
(
@OrderNumber       nvarchar(30),
@StorageUnitId	   int,
@Batch             nvarchar(50),
@FromDate          datetime,
@ToDate            datetime,
@StatusId		   int,
@PrincipalId	   int
)

as
begin
  set nocount on;
  
  declare @TableSOH as table
  (
   WarehouseId int,
   StorageUnitBatchId  int,
   StockOnHand int
  )
    
  insert @TableSOH
        (WarehouseId,
         StorageUnitBatchId,
         StockOnHand)
  select a.WarehouseId,
         subl.StorageUnitBatchId,
         sum(subl.ActualQuantity)
    from StorageUnitBatchLocation subl (nolock)
    join AreaLocation               al (nolock) on subl.LocationId        = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
  group by a.WarehouseId,
           subl.StorageUnitBatchId

  
  declare @TableDetails as Table
  (
  OrderNumber          nvarchar(30),
  OrderDate            datetime,
  ExpectedDeliveryDate datetime,
  ProductCode          nvarchar(30),
  ProductDesc          nvarchar(255),
  StorageUnitId		   int,
  SKUCode              nvarchar(50),
  SKU                  nvarchar(50),
  Batch                nvarchar(50),
  OrderQty             numeric(13,6),
  IssuedQty            numeric(13,6),
  OutstandingBal       numeric(13,6),
  StockOnHand          numeric(13,6),
  DeliveryNote		   nvarchar(30),
  ExternalCompanyId	   int,
  ExternalCompanyCode  nvarchar(30),
  ExternalCompany	   nvarchar(255),
  DeliveryDate		   datetime,
  IssueId			   int,
  Status			   nvarchar(50),
  PrincipalId		   int,
  PrincipalCode		   nvarchar(30)
  )
  
  if @StorageUnitId = '-1'
     set @StorageUnitId = null
         
  if @OrderNumber = '-1'
     set @OrderNumber = null
         
  if @Batch = '-1'
     set @Batch = null
     
  if @StatusId = -1
	 set @StatusId = null
		
  if @PrincipalId = -1
	 set @PrincipalId = null
     
     
	  insert @TableDetails
			(OrderNumber,
			 OrderDate,
			 ExpectedDeliveryDate,
			 ProductCode,
			 ProductDesc,
			 StorageUnitId,
			 SKUCode,
			 SKU,
			 Batch,
			 OrderQty,
			 IssuedQty,
			 OutstandingBal,
			 StockOnHand,
			 ExternalCompanyId,
			 DeliveryNote,
			 DeliveryDate,
			 IssueId,
			 Status,
			 PrincipalId
			 )
	  select id.OrderNumber,
			 id.CreateDate,
			 id.DeliveryDate,
			 p.ProductCode,
			 p.Product,
			 su.StorageUnitId,
			 sku.SKUCode,
			 sku.SKU,
			 b.Batch,
			 sum(il.Quantity),
			 sum(il.ConfirmedQuatity),
			 sum(il.Quantity - isnull(il.ConfirmedQuatity,0)),
			 sum(StockOnHand) ,
			 id.ExternalCompanyId,
			 i.DeliveryNoteNumber ,
			 i.DeliveryDate,
			 i.IssueId,
			 s.Status,
			 p.PrincipalId       
		from  OutboundDocument     id (nolock)
		 join OutboundLine        ol (nolock) on id.OutboundDocumentId = ol.OutboundDocumentId
		 join IssueLine           il (nolock) on ol.OutboundLineId = il.OutboundLineId
		 join Issue			       i (nolock) on il.IssueId = i.IssueId
		 join status			   s (nolock) on i.StatusId = s.StatusId
		 join StorageUnitBatch   sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
		 join Batch                b (nolock) on sub.BatchId = b.BatchId
		 join StorageUnit         su (nolock) on sub.StorageUnitId = su.StorageUnitId
		 join Product              p (nolock) on su.ProductId = p.ProductId
		 join SKU                sku (nolock) on su.SKUId = sku.SKUId
		 left
		 join @TableSOH         soh on il.StorageUnitBatchId = soh.StorageUnitBatchId
								   and id.WarehouseId = soh.WarehouseId
	   where id.CreateDate between @FromDate and @ToDate
	     and s.StatusId = isnull(@StatusId,s.StatusId)
	     and isnull(id.PrincipalId,-1) = isnull(@PrincipalId,isnull(id.PrincipalId,-1))
	  group by id.OrderNumber,
			   id.CreateDate,
			   id.DeliveryDate,
			   p.ProductCode,
			   p.Product,
			   su.StorageUnitId,
			   sku.SKUCode,
			   sku.SKU,
			   b.Batch,
			   id.ExternalCompanyId,
			   i.DeliveryNoteNumber,
			   i.DeliveryDate,
			   i.IssueId,
			   s.Status,
			   p.PrincipalId   
   update  td
      set td.ExternalCompanyCode = ec.ExternalCompanyCode,
		  td.ExternalCompany = ec.ExternalCompanyCode
     from @TableDetails td
     join ExternalCompany ec (nolock) on ec.ExternalCompanyId = td.ExternalCompanyId
     
   update  td
      set td.PrincipalCode = p.PrincipalCode
     from @TableDetails td
     join Principal p  (nolock) on p.PrincipalId = @PrincipalId
     
  select OrderNumber,
         DeliveryNote,
         OrderDate,
         ExpectedDeliveryDate,
         ProductCode,
         ProductDesc,
         SKUCode,
         SKU,
         Batch,
         OrderQty,
         IssuedQty,
         OutstandingBal,
         StockOnHand,
         ExternalCompanyCode,
         ExternalCompany,
		 deliverydate,
		 Status,
		 PrincipalCode
    from @TableDetails
   where OrderNumber = isnull(@OrderNumber, OrderNumber)
	 and StorageUnitId = ISNULL(@StorageUnitId, StorageUnitId)
     and Batch = isnull(@Batch, Batch)
  order by ProductCode
end
 
