﻿ 
/*
  /// <summary>
  ///   Procedure Name : sp_grants_random
  ///   Filename       : sp_grants_random.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure sp_grants_random
as
begin
  if not exists(select 1 from tempdb..sysobjects where name like '##random%')
  create table ##random
  (
   random int
  )
  if not exists(select 1 from tempdb..sysobjects where name like '##error%')
  create table ##error
  (
   random int
  )
  
  if (select count(1) from ##random) > 98
    truncate table ##random
  
  declare @found bit,
          @random int
  
  set @found = 0
  
  while @found <> 1
  begin
    --select @random = convert(int, rand() * 10000)
    exec GetRandomInteger 10000, 99999, @random out
    
    insert ##random
    select @random
     where not exists(select top 1 1 from ##random where random = @random)
       and @random > 0
    
    if @@rowcount > 0
      set @found = 1
  end
  
  return @random
end
