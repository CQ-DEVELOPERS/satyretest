﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_Take_Count_Sheet
  ///   Filename       : p_Report_Stock_Take_Count_Sheet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Jul 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Stock_Take_Count_Sheet
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @JobId    int = null
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   JobId              int null,
   ReferenceNumber    nvarchar(30) null,
   InstructionId      int,
   PalletId           int null,
   StorageUnitBatchId int,
   ProductCode	       nvarchar(30) null,
   Product	           nvarchar(50) null,
   SKUCode            nvarchar(50) null,
   Batch              nvarchar(50) null,
   ECLNumber          nvarchar(10) null,
   Quantity           float null,
   ConfirmedQuantity  float null,
   PickLocationId     int null,
   PickLocation       nvarchar(15) null,
   CreateDate         datetime null,
   InstructionType	   nvarchar(30),
   OperatorId         int null,
   Operator           nvarchar(50) null,
   StatusId           int null,
   Status             nvarchar(50) null
  )
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @JobId = -1
    set @JobId = null
  
  insert @TableResult
        (JobId,
         ReferenceNumber,
         InstructionId,
         PalletId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         PickLocationId,
         CreateDate,
         InstructionType,
         OperatorId,
         StatusId,
         Status)
  select j.JobId,
         j.ReferenceNumber,
         i.InstructionId,
         i.PalletId,
         i.StorageUnitBatchId,
         i.Quantity,
         i.ConfirmedQuantity,
         i.PickLocationId,
         i.CreateDate,
         it.InstructionType,
         i.OperatorId,
         s.StatusId,
         s.Status
    from Job                   j (nolock)
    join Instruction           i (nolock) on j.JobId             = i.JobId
    join InstructionType      it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status                s (nolock) on i.StatusId          = s.StatusId
   where s.Type             = 'I'
     --and s.StatusCode       = 'W'
     and j.JobId            = isnull(@JobId, j.JobId)
  
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         ECLNumber     = b.ECLNumber
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit         su  (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId          = p.ProductId
    join SKU                 sku (nolock) on su.SKUId              = sku.SKUId
    join Batch               b   (nolock) on sub.BatchId           = b.BatchId
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator     o  (nolock) on tr.OperatorId = o.OperatorId
  
  select JobId,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         ECLNumber,
         Quantity,
         ConfirmedQuantity,
         PickLocation,
         PalletId,
         CreateDate,
         InstructionType,
         Operator,
         Status
    from @TableResult
end
