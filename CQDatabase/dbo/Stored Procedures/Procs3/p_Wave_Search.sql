﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Search
  ///   Filename       : p_Wave_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2013 14:17:31
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Wave table.
  /// </remarks>
  /// <param>
  ///   @WaveId int = null output,
  ///   @Wave nvarchar(100) = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Wave.WaveId,
  ///   Wave.Wave,
  ///   Wave.CreateDate,
  ///   Wave.ReleasedDate,
  ///   Wave.StatusId,
  ///   Status.Status,
  ///   Wave.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   Wave.CompleteDate,
  ///   Wave.ReleasedOrders,
  ///   Wave.PutawayDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Search
(
 @WaveId int = null output,
 @Wave nvarchar(100) = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @WaveId = '-1'
    set @WaveId = null;
  
  if @Wave = '-1'
    set @Wave = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
 
  select
         Wave.WaveId
        ,Wave.Wave
        ,Wave.CreateDate
        ,Wave.ReleasedDate
        ,Wave.StatusId
         ,StatusStatusId.Status as 'Status'
        ,Wave.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,Wave.CompleteDate
        ,Wave.ReleasedOrders
        ,Wave.PutawayDate
    from Wave
    left
    join Status StatusStatusId on StatusStatusId.StatusId = Wave.StatusId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = Wave.WarehouseId
   where isnull(Wave.WaveId,'0')  = isnull(@WaveId, isnull(Wave.WaveId,'0'))
     and isnull(Wave.Wave,'%')  like '%' + isnull(@Wave, isnull(Wave.Wave,'%')) + '%'
     and isnull(Wave.StatusId,'0')  = isnull(@StatusId, isnull(Wave.StatusId,'0'))
     and isnull(Wave.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Wave.WarehouseId,'0'))
  order by Wave
  
end
