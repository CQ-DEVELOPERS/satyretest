﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Test_Update
  ///   Filename       : p_Test_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:00
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Test table.
  /// </remarks>
  /// <param>
  ///   @TestId int = null,
  ///   @TestCode nvarchar(20) = null,
  ///   @Test nvarchar(510) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Test_Update
(
 @TestId int = null,
 @TestCode nvarchar(20) = null,
 @Test nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
  if @TestId = '-1'
    set @TestId = null;
  
  if @TestCode = '-1'
    set @TestCode = null;
  
  if @Test = '-1'
    set @Test = null;
  
	 declare @Error int
 
  update Test
     set TestCode = isnull(@TestCode, TestCode),
         Test = isnull(@Test, Test) 
   where TestId = @TestId
  
  select @Error = @@Error
  
  
  return @Error
  
end
