﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Operator_Transaction_SummarySubR
  ///   Filename       : p_Report_Operator_Transaction_SummarySubR.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Operator_Transaction_SummarySubR
(
	@FromDate			Datetime,
	@ToDate				DateTime
)



as
begin

	 set nocount on;
	
Declare @TempTable2 As Table
(
	Units				int,
	Jobs				int,
	InstructionTypeId	int,
	InstructionType		varchar(30),
	Average				int
)

Declare @TempTable3 As Table
(
	OperatorId			int,
	InstructionTypeId	int
)

Declare @TempTable4 As Table
(
	OperatorCount		int,
	InstructionTypeId	int
)

	Insert Into @TempTable3 (OperatorId,
							InstructionTypeId)

	SELECT  distinct(ots.OperatorId), 
			ots.InstructionTypeId
	FROM    OperatorTransactionSummary ots 
	INNER JOIN Operator O ON ots.OperatorId = O.OperatorId
	where CONVERT(datetime, CONVERT(varchar,ots.EndDate,101)) between  CONVERT(datetime, CONVERT(varchar,@FromDate,101)) and CONVERT(datetime, CONVERT(varchar,@ToDate,101))
	--Group BY	ots.InstructionTypeId


	
	Insert Into @TempTable4 (OperatorCount,
							InstructionTypeId)
	select count(OperatorId) Operators
		, InstructionTypeId 
	from @TempTable3 
	group by InstructionTypeId
	order by InstructionTypeId
	

	
	Insert Into @TempTable2 (Units,
							Jobs,
							InstructionTypeId)
	(
	SELECT  Sum(OP.Units) as Units, 
			Sum(OP.Jobs)  as Jobs,
			op.InstructionTypeId
	FROM    OperatorTransactionSummary OP 
	INNER JOIN Operator O ON OP.OperatorId = O.OperatorId
	where CONVERT(datetime, CONVERT(varchar,OP.EndDate,101)) between  CONVERT(datetime, CONVERT(varchar,@FromDate,101)) and CONVERT(datetime, CONVERT(varchar,@ToDate,101))
	--remove next line to add checked
	--and op.InstructionTypeId <> '9999'
	Group BY	op.InstructionTypeId
	) 
	
	update t2
		set Average = t2.Jobs/t4.OperatorCount
    from @TempTable2 t2
    join @TempTable4 t4 on t2.InstructionTypeId = t4.InstructionTypeId
    
    update t2
		set InstructionType = 'Checked'
    from @TempTable2 t2
    where t2.InstructionTypeId = '9999'
    
    update t2
		set t2.InstructionType = it.InstructionType
    from @TempTable2 t2
    join	InstructionType it (nolock) on it.InstructionTypeId = t2.InstructionTypeId 
      
    				
Select	Average,
		InstructionType 
from	@TempTable2 t2
--join	InstructionType i (nolock) on i.InstructionTypeId = t2.InstructionTypeId 
end
  
  select * from @TempTable2 where InstructionTypeId = '9999' 
   		
