﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Transfer_Line_Update
  ///   Filename       : p_Transfer_Line_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Transfer_Line_Update
(
 @IssueLineId        int,
 @StorageUnitBatchId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_IssueLine_Update
   @IssueLineId        = @IssueLineId,
   @StorageUnitBatchId = @StorageUnitBatchId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Transfer_Line_Update'); 
    rollback transaction
    return @Error
end
