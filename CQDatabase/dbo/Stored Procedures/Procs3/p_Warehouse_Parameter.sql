﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Warehouse_Parameter
  ///   Filename       : p_Warehouse_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 May 2014 13:32:03
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Warehouse table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Warehouse.WarehouseId,
  ///   Warehouse.Warehouse 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Warehouse_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as WarehouseId
        ,'{All}' as Warehouse
  union
  select
         Warehouse.WarehouseId
        ,Warehouse.Warehouse
    from Warehouse
  order by Warehouse
  
end
