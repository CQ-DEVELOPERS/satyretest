﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeRef_Search_Warehouse
  ///   Filename       : p_StockTakeRef_Search_Warehouse.sql
  ///   Create By      : Karen
  ///   Date Created   : Jul 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeRef_Search_Warehouse
(
 @warehouseId	int,
 @FromDate		datetime,
 @ToDate		datetime
)

as
begin
	 set nocount on;
  
  select distinct StockTakeReferenceId,
	WarehouseId,
	StartDate,
	Enddate
    from StockTakeReference str
   where WarehouseId = @warehouseId
   and   str.StartDate between @FromDate and @ToDate
  order by StockTakeReferenceId
end
