﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitExternalCompany_Parameter
  ///   Filename       : p_StorageUnitExternalCompany_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:58
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitExternalCompany table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   StorageUnitExternalCompany.StorageUnitId,
  ///   StorageUnitExternalCompany.ExternalCompanyId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitExternalCompany_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as StorageUnitId
        ,null as 'StorageUnitExternalCompany'
        ,null as ExternalCompanyId
        ,null as 'StorageUnitExternalCompany'
  union
  select
         StorageUnitExternalCompany.StorageUnitId
        ,StorageUnitExternalCompany.StorageUnitId as 'StorageUnitExternalCompany'
        ,StorageUnitExternalCompany.ExternalCompanyId
        ,StorageUnitExternalCompany.ExternalCompanyId as 'StorageUnitExternalCompany'
    from StorageUnitExternalCompany
  
end
