﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnit_Search_Product_SKU
  ///   Filename       : p_StorageUnit_Search_Product_SKU.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007 11:54:26
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null output,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(30) = null,
  ///   @Product nvarchar(50) = null,
  ///   @Barcode nvarchar(50) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   p.ProductId,
  ///   p.StatusId,
  ///   s.Status,
  ///   p.ProductCode,
  ///   p.Product,
  ///   p.Barcode,
  ///   p.MinimumQuantity,
  ///   p.ReorderQuantity,
  ///   p.MaximumQuantity,
  ///   p.CuringPeriodDays,
  ///   p.ShelfLifeDays,
  ///   p.QualityAssuranceIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnit_Search_Product_SKU
(
 @ProductCode nvarchar(30) = null,
 @Product     nvarchar(50) = null,
 @SKUCode     nvarchar(50) = null,
 @SKU         nvarchar(50) = null,
 @PrincipalId int = null
)

as
begin
	 set nocount on;
	 
  declare @TableResult as table
  (
   StorageUnitId                   int              not null,
   ProductCode                     nvarchar(60)     not null,
   Product                         nvarchar(510)    null    ,
   SKUCode                         nvarchar(100)    null    ,
   SKU                             nvarchar(100)    not null,
   SOH                             float            null    ,
   Principal                       nvarchar(510)    null    
  )
  
  if @ProductCode = '-1'
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
  
  if @SKUCode = '-1'
    set @SKUCode = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null
  
  insert @TableResult
        (StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Principal)
  select top 100 su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         pr.Principal
    from StorageUnit su (nolock)
    join SKU        sku (nolock) on su.SKUId = sku.SKUId
    join Product      p (nolock) on su.ProductId = p.ProductId
    left
    join Principal    pr (nolock) on p.PrincipalId = pr.PrincipalId
   where (p.ProductCode like '%' + @ProductCode + '%' or @ProductCode is null)
     and (p.Product like '%' + @Product + '%' or @Product is null)
     and (sku.SKUCode like '%' + @SKUCode + '%' or @SKUCode is null)
     and (sku.SKU like '%' + @SKU + '%' or @SKU is null)
     and (pr.PrincipalId = @PrincipalId or @PrincipalId is null)
  
  update tr
     set SOH = (select SUM(subl.ActualQuantity)
                  from StorageUnitBatchLocation subl (nolock)
                  join StorageUnitBatch sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
                  join AreaLocation al (nolock) on subl.LocationId = al.LocationId
                  join Area a (nolock) on al.AreaId = a.AreaId
                 where sub.StorageUnitId = tr.StorageUnitId
                   and a.StockOnHand = 1)
    from @TableResult tr
  
  select StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         SOH,
         Principal
    from @TableResult
end
