﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Putaway_Job
  ///   Filename       : p_Report_Putaway_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Jul 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Putaway_Job
(
 @JobId    nvarchar(1000) = null
)

as
begin
	 set nocount on;
  declare @TableHeader as table
  (
   OrderNumber       nvarchar(30),
   InboundShipmentId int null,
   ReceiptId         int,
   ReceiptLineId     int
  )
  
  declare @TableResult as table
  (
   InstructionId      int,
   ReceiptLineId      int,
   StorageUnitBatchId int,
   ProductCode	       nvarchar(30) null,
   Product	           nvarchar(50) null,
   SKUCode            nvarchar(50) null,
   Quantity           float null,
   ConfirmedQuantity  float null,
   JobId              int null,
   StatusId           int null,
   Status             nvarchar(50) null,
   PickLocationId     int null,
   PickLocation       nvarchar(15) null,
   StoreLocationId    int null,
   StoreLocation      nvarchar(15) null,
   StoreAreaId		  int null,
   StoreArea		  nvarchar(50) null,
   PalletId           int null,
   CreateDate         datetime null,
   Batch              nvarchar(50) null,
   ECLNumber          nvarchar(10) null,
   InstructionType	   nvarchar(30),
   OperatorId         int null,
   Operator           nvarchar(50) null
  )
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
--  if @JobId = -1
--    set @JobId = null
  
  insert @TableResult
        (InstructionId,
         ReceiptLineId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         JobId,
         StatusId,
         PickLocationId,
         StoreLocationId,
         PalletId,
         CreateDate,
         InstructionType,
         OperatorId)
  select top 1000
         i.InstructionId,
         i.ReceiptLineId,
         i.StorageUnitBatchId,
         i.Quantity,
         i.ConfirmedQuantity,
         i.JobId,
         i.StatusId,
         i.PickLocationId,
         i.StoreLocationId,
         i.PalletId,
         i.CreateDate,
         it.InstructionType,
         i.OperatorId
    from Instruction         i   (nolock)
    join InstructionType     it  (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Status              s   (nolock) on i.StatusId = s.StatusId
   where charindex(',' + convert(nvarchar(10), i.JobId) + ',',',' + @JobId + ',') > 0
     and s.StatusCode      in ('W','A','S')
  
  update tr
     set StoreLocationId = sul.LocationId
    from @TableResult			tr
    join StorageUnitBatch		sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnitLocation	sul (nolock) on sub.StorageUnitId = sul.StorageUnitId
   where tr.StoreLocationId is null
  
  insert @TableHeader
        (OrderNumber,
         InboundShipmentId,
         ReceiptId,
         ReceiptLineId)
  select distinct
         id.OrderNumber,
         isr.InboundShipmentId,
         r.ReceiptId,
         rl.ReceiptLineId
    from InboundDocument         id (nolock)
    join Receipt                  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join ReceiptLine             rl (nolock) on r.ReceiptId          = rl.ReceiptId
    left outer
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId          = isr.ReceiptId
    join @TableResult            tr          on rl.ReceiptLineId     = tr.ReceiptLineId
  
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         ECLNumber     = b.ECLNumber
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit         su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId         = p.ProductId
    join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
    join Batch               b   (nolock) on sub.BatchId          = b.BatchId

  update tr
     set Status = s.Status
    from @TableResult tr
    join Status       s  (nolock) on tr.StatusId = s.StatusId

  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId

  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
    
  update tr
     set StoreAreaId = al.AreaId,
		 StoreArea   = a.Area
    from @TableResult    tr
    join AreaLocation    al (nolock) on tr.StoreLocationId = al.LocationId
    Join Area			  a (nolock) on al.AreaId = a.AreaId

  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator     o  (nolock) on tr.OperatorId = o.OperatorId

  update j
     set StatusId = dbo.ufn_StatusId('R','PR')
    from @TableResult tr
    join Job           j (nolock) on tr.JobId   = j.JobId
    join Status        s (nolocK) on j.StatusId = s.StatusId
   where s.StatusCode in ('RC','LA','P')
  
  select InstructionId,
         ProductCode,
         Product,
         SKUCode,
         Quantity,
         ConfirmedQuantity,
         JobId,
         Status,
         PickLocation,
         StoreLocation,
         PalletId,
         datediff(mi, CreateDate, @GetDate) as DwellTime,
         CreateDate,
         InstructionType,
         OperatorId,
         Operator,
         StoreArea
    from @TableHeader th
    join @TableResult tr on th.ReceiptLineId = tr.ReceiptLineId
end
 
 
 
 
