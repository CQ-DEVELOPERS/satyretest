﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Putaway_Talley
  ///   Filename       : p_Report_Putaway_Talley.sql
  ///   Create By      : Karen
  ///   Date Created   : April 2014
  /// </summary>
  /// <remarks>
  //
/   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///
   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Putaway_Talley
(
 @FromDate    datetime,
 @ToDate      datetime,
 @PrincipalId int
)

as
begin
	 set nocount on;
  declare @TableHeader 
as table
  (
   OrderNumber       nvarchar(30),
   InboundShipmentId int null,
   ReceiptId         int,
   ReceiptLineId     int,
   WarehouseId		 int,
   Warehouse		 nvarchar(50),
   PrincipalId		 int,
   Principal		 nvarchar(100),
   StagingLocationId int,
   StagingLocation   nvarchar(50)
  )
  
  declare @TableResult as table
  (
   InstructionId      int,
   ReceiptLineId      int,
   StorageUnitBatchId int, 
   StorageUnitId      int,
   ProductCode	      nvarchar(30) null,
   Product	          nvarchar(255) null,
   SKUCode            nvarchar(50) null,
   Quantity           float null,
   ConfirmedQuantity  float null,
   JobId              int null,
   StatusId           int null,
   Status             nvarchar(50) null,
   PickLocationId     int null,
   PickLocation       nvarchar(15) null,
   StoreLocationId    int null,
   StoreLocation      nvarchar(15) null,
   StoreArea          nvarchar(50) null,  
   PalletId           int null,
   CreateDate         datetime null,
   Batch              nvarchar(50) null,
   ECLNumber          nvarchar(10) null,
   InstructionType	  nvarchar(30),
   OperatorId         int null,
   Operator           nvarchar(50) null,
   Weight			  float,
   GrossWeight		  float
  )
  
  declare @GetDate           datetime,
          @StatusCode        nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @PrincipalId = -1	
	set @PrincipalId = null
  

  if @StatusCode is null
    insert @TableResult
          (InstructionId,
           ReceiptLineId,
           StorageUnitBatchId,
           Quantity,  
           ConfirmedQuantity,
           JobId,
           StatusId,
           PickLocationId,
           StoreLocationId,
           PalletId,
           CreateDate,
           InstructionType,
           OperatorId,
           Status)
    select i.InstructionId,
           i.ReceiptLineId,
           i.StorageUnitBatchId,
           i.Quantity,
           i.ConfirmedQuantity,
           i.JobId,
           j.StatusId,
           i.PickLocationId,
           i.StoreLocationId,
           i.PalletId,     
		   i.CreateDate,
           it.InstructionType,
           j.OperatorId,
           sj.Status
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Job              j (nolock) on i.JobId             = j.JobId
      join Status          sj (nolock) on j.StatusId          = sj.StatusId
      join Status          si (nolock) on i.StatusId          = si.StatusId
     where i.CreateDate      between @FromDate and @ToDate      
       and si.StatusCode           in ('W','S')
       and((it.InstructionTypeCode  = 'PR'       and sj.StatusCode in ('A','S'))
       or  (it.InstructionTypeCode in ('S','SM') and sj.StatusCode in ('PR','A','S')))
  else
    insert @TableResult
          (InstructionId,
           ReceiptLineId,
           StorageUnitBatchId,
           Quantity,
           ConfirmedQuantity,
           JobId,
           StatusId,
           PickLocationId,         
		   StoreLocationId,
           PalletId,
           CreateDate,
           InstructionType,
           OperatorId,
           Status)
    select i.InstructionId,
           i.ReceiptLineId,
           i.StorageUnitBatchId,
           i.Quantity,          
		   i.ConfirmedQuantity,
           i.JobId,
           j.StatusId,
           i.PickLocationId,
           i.StoreLocationId,
           i.PalletId,
           i.CreateDate,
           it.InstructionType,
           j.OperatorId,
           sj.Status      
from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Job              j (nolock) on i.JobId             = j.JobId
      join Status          sj (nolock) on j.StatusId          = sj.StatusId
      join Status          si (nolock) on i.StatusId          = si.StatusId
     where i.CreateDate      between @FromDate and @ToDate
       and sj.StatusCode            = @StatusCode    
	   and si.StatusCode           in ('W','S')
       and it.InstructionTypeCode in ('S','SM','PR')
  
  insert @TableHeader
        (OrderNumber,
         InboundShipmentId,
         ReceiptId,
         ReceiptLineId,
         WarehouseId,
         PrincipalId,
         StagingLocation)
  select distinct
         id.OrderNumber,
         isr.InboundShipmentId,
         r.ReceiptId,
         rl.ReceiptLineId,
         r.WarehouseId,
         id.PrincipalId,
         id.FromLocation
    from InboundDocument         id (nolock)
    join Receipt                  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join ReceiptLine             rl (nolock) on r.ReceiptId          = rl.ReceiptId
    left outer
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId          = isr.ReceiptId
    join @TableResult            tr          on rl.ReceiptLineId     = tr.ReceiptLineId
  
  update tr
     set StorageUnitId = su.StorageUnitId,
         ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         ECLNumber     = b.ECLNumber
    from @TableResult tr

    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit         su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId         = p.ProductId
    join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
    join Batch               b   (nolock) on sub.BatchId          = b.BatchId

  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l 
 (nolock) on tr.PickLocationId = l.LocationId
 
 update tr
     set Principal = p.Principal
    from @TableHeader tr
    join Principal  p (nolock) on tr.PrincipalId = p.PrincipalId   
    
 update tr
     set StagingLocation = l.Location
    from @TableHeader tr
    join Location l  (nolock) on tr.StagingLocationId = l.LocationId 
    
  update tr
     set Warehouse = w.Warehouse
    from @TableHeader tr
    join Warehouse w  (nolock) on tr.WarehouseId = w.WarehouseId  

  update  tr
     set  tr.Weight = p.Weight
    from @TableResult tr
    join Pack p on p.StorageUnitId = tr.StorageUnitId  
    where p.PackTypeId = (select pt.PackTypeId from PackType pt where p.PackTypeId = pt.PackTypeId
							and pt.PackType = 'Unit')
							
  update  tr
     set  GrossWeight = tr.Weight * tr.ConfirmedQuantity
    from  @TableResult tr
    

  update tr
     set StoreLocation = l.Location,
         StoreArea     = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId = a.AreaId

  update tr
     set StoreArea     = a.Area
    from @TableResult     tr
    join StorageUnitArea sua (nolock) on tr.StorageUnitId = sua.StorageUnitId
    join Area              a (nolock) on sua.AreaId       = a.AreaId
   where sua.StoreOrder = 1
     and a.AreaCode in ('RK','BK')
     and a.AreaType = ''
     --and a.WarehouseId = @warehouseId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr

    join Operator     o  (nolock) on tr.OperatorId = o.OperatorId
  
  select InstructionId,
         ProductCode,
         Product,
         SKUCode,
         Quantity,
         ConfirmedQuantity,
         JobId,
         Status,
         PickLocation,
         StoreLocation,
         StoreArea,
         PalletId,
         datediff(mi, CreateDate, @GetDate) as DwellTime,
         CreateDate,
         InstructionType,
         OperatorId,
         Operator,
         OrderNumber,
         WarehouseId,
         Warehouse,
         Principal,
         InboundShipmentId,
         Batch,
         StagingLocation,
         GrossWeight
    from @TableResult tr 
   left outer
    join @TableHeader th on tr.ReceiptLineId = th.ReceiptLineId
    where th.PrincipalId = isnull(@PrincipalId,th.PrincipalId)

end

 
