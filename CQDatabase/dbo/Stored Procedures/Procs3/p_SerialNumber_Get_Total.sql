﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Get_Total
  ///   Filename       : p_SerialNumber_Get_Total.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jul 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Get_Total
(
 @keyId   int,
 @keyType nvarchar(100),
 @storageUnitId int = null
)

as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   SerialNumberId int,
   SerialNumber   nvarchar(50),
   StorageUnitId  int,
   IssueLineId	  int
  )
  
  declare @Count int,
          @Total int
  
  if @storageUnitId = -1
    set @storageUnitId = null
  
  if @keyType = 'ReceiptId'
  begin
    insert @TableResult
          (SerialNumberId,
           SerialNumber,
           StorageUnitId)
    select SerialNumberId,
           SerialNumber,
           StorageUnitId
      from SerialNumber (nolock)
     where ReceiptId = @keyId
    
    select @Total = SUM(ReceivedQuantity)
      from ReceiptLine a (nolock)
      join StorageUnitBatch sub (nolock) on a.StorageUnitBatchId = sub.StorageUnitBatchId
     where a.ReceiptId = @keyId
       and sub.StorageUnitId = isnull(@storageUnitId, sub.StorageUnitId)
  end
  
  if @keyType = 'ReceiptLineId'
  begin
    insert @TableResult
          (SerialNumberId,
           SerialNumber,
           StorageUnitId)
    select SerialNumberId,
           SerialNumber,
           StorageUnitId
      from SerialNumber (nolock)
     where ReceiptLineId = @keyId
    
    select @Total = SUM(ReceivedQuantity)
      from ReceiptLine a (nolock)
      join StorageUnitBatch sub (nolock) on a.StorageUnitBatchId = sub.StorageUnitBatchId
     where a.ReceiptLineId = @keyId
  end
  
  if @keyType = 'IssueId'
  begin
    insert @TableResult
          (SerialNumberId,
           SerialNumber,
           StorageUnitId)
    select SerialNumberId,
           SerialNumber,
           StorageUnitId
      from SerialNumber (nolock)
     where IssueId = @keyId
    
    select @Total = SUM(ConfirmedQuatity)
      from IssueLine a (nolock)
      join StorageUnitBatch sub (nolock) on a.StorageUnitBatchId = sub.StorageUnitBatchId
     where a.IssueId = @keyId
       and sub.StorageUnitId = isnull(@storageUnitId, sub.StorageUnitId)
  end
  
  if @keyType = 'IssueLineId'
  begin
    insert @TableResult
          (SerialNumberId,
           SerialNumber,
           StorageUnitId,
           IssueLineId)
    select SerialNumberId,
           SerialNumber,
           StorageUnitId,
           IssueLineId
      from SerialNumber (nolock)
     where IssueLineId = @keyId
    
    select @Total = SUM(ConfirmedQuatity)
      from IssueLine a (nolock)
      join StorageUnitBatch sub (nolock) on a.StorageUnitBatchId = sub.StorageUnitBatchId
     where a.IssueLineId = @keyId
       and sub.StorageUnitId = isnull(@storageUnitId, sub.StorageUnitId)
       
    select @Count = COUNT(distinct(SerialNumberId))
      from @TableResult
     where StorageUnitId = isnull(@storageUnitId, StorageUnitId)
       and IssueLineId = @keyId
  end
  
  if @keyType = 'StoreInstructionId'
  begin
    insert @TableResult
          (SerialNumberId,
           SerialNumber,
           StorageUnitId)
    select SerialNumberId,
           SerialNumber,
           StorageUnitId
      from SerialNumber (nolock)
     where StoreInstructionId = @keyId
    
    select @Total = SUM(ConfirmedQuantity)
      from Instruction a (nolock)
      join StorageUnitBatch sub (nolock) on a.StorageUnitBatchId = sub.StorageUnitBatchId
     where a.InstructionId = @keyId
       and sub.StorageUnitId = isnull(@storageUnitId, sub.StorageUnitId)
  end
  
  if @keyType = 'PickInstructionId'
  begin
    insert @TableResult
          (SerialNumberId,
           SerialNumber,
           StorageUnitId)
    select SerialNumberId,
           SerialNumber,
           StorageUnitId
      from SerialNumber (nolock)
     where PickInstructionId = @keyId
    
    select @Total = SUM(ConfirmedQuantity)
      from Instruction a (nolock)
      join StorageUnitBatch sub (nolock) on a.StorageUnitBatchId = sub.StorageUnitBatchId
     where a.InstructionId = @keyId
       and sub.StorageUnitId = isnull(@storageUnitId, sub.StorageUnitId)
  end
  
  if @keyType = 'StoreJobId'
  begin
    insert @TableResult
          (SerialNumberId,
           SerialNumber,
           StorageUnitId)
    select SerialNumberId,
           SerialNumber,
           StorageUnitId
      from SerialNumber (nolock)
     where StoreJobId = @keyId
    
    select @Total = SUM(ConfirmedQuantity)
      from Instruction a (nolock)
      join StorageUnitBatch sub (nolock) on a.StorageUnitBatchId = sub.StorageUnitBatchId
     where JobId = @keyId
       and sub.StorageUnitId = isnull(@storageUnitId, sub.StorageUnitId)
  end
  
  if @keyType = 'PickJobId'
  begin
    insert @TableResult
          (SerialNumberId,
           SerialNumber,
           StorageUnitId)
    select SerialNumberId,
           SerialNumber,
           StorageUnitId
      from SerialNumber (nolock)
     where PickJobId = @keyId
    
    select @Total = SUM(ConfirmedQuantity)
      from Instruction a (nolock)
      join StorageUnitBatch sub (nolock) on a.StorageUnitBatchId = sub.StorageUnitBatchId
     where a.JobId = @keyId
       and sub.StorageUnitId = isnull(@storageUnitId, sub.StorageUnitId)
  end
  
  select @Count = COUNT(distinct(SerialNumberId))
    from @TableResult
   where StorageUnitId = isnull(@storageUnitId, StorageUnitId)
  
  select @Count as 'Count',
         @Total as 'Total'
end
