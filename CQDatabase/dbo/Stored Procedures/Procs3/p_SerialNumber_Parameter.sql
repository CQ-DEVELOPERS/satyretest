﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Parameter
  ///   Filename       : p_SerialNumber_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Aug 2013 14:12:41
  /// </summary>
  /// <remarks>
  ///   Selects rows from the SerialNumber table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   SerialNumber.SerialNumberId,
  ///   SerialNumber.SerialNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as SerialNumberId
        ,'{All}' as SerialNumber
  union
  select
         SerialNumber.SerialNumberId
        ,SerialNumber.SerialNumber
    from SerialNumber
  order by SerialNumber
  
end
