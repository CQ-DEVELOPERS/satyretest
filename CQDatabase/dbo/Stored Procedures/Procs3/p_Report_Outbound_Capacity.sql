﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Outbound_Capacity
  ///   Filename       : p_Report_Outbound_Capacity.sql
  ///   Create By      : Willis
  ///   Date Created   : 28 July 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Outbound_Capacity
(

 @WarehouseId       int
 
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
	  Class	 nvarchar(50),
	  Period	nvarchar(50),
	  SortId	int,		
	  Weight	numeric(13,3)
  )
  
  insert @TableResult
        (Class,
         Period,
		       SortId,
         Weight)
  select 'Required' as Class,
		       case	when	dbo.ufn_Configuration_Value(44, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then dbo.ufn_Configuration_Description(44,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(44, @warehouseId))
                     when	dbo.ufn_Configuration_Value(45, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then dbo.ufn_Configuration_Description(45,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(45, @warehouseId))
                     when	dbo.ufn_Configuration_Value(46, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then dbo.ufn_Configuration_Description(46,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(46, @warehouseId))
                     when dbo.ufn_Configuration_Value(47, @warehouseId) <= Datediff(hh,getdate(),i.deliverydate) then dbo.ufn_Configuration_Description(47,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(47, @warehouseId))
                else 
                     'Unknown'
                end as Period,
		       case	when	dbo.ufn_Configuration_Value(44, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then 1
                     when	dbo.ufn_Configuration_Value(45, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then 2
                     when	dbo.ufn_Configuration_Value(46, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then 3
                     when dbo.ufn_Configuration_Value(47, @warehouseId) <= Datediff(hh,getdate(),i.deliverydate) then 4
                else 
                     5
                end as SortId,
		       sum(p.Weight * il.Quantity) as Weight
    from Issue					              i	(nolock)
    join IssueLine              il (nolock) on i.IssueId             = il.IssueId
		  join	status				            	si	(nolock)	on i.StatusId            = si.StatusId
    join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
		  join	StorageUnit			        	su	(nolock)	on sub.StorageUnitId			  = su.StorageUnitId
    join	Pack				               	p	(nolock)	on su.StorageUnitId			   = p.StorageUnitId
    join	PackType			           	pt	(nolock)	on p.PackTypeId				      = pt.PackTypeId
   where	si.Type        = 'IS'
		   and si.StatusCode  = 'RL' --in ('M','RL','PC','PS','CK','CD','A','WC','QA')
		   and pt.PackType    = 'Units'
 	group by case	when	dbo.ufn_Configuration_Value(44, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then dbo.ufn_Configuration_Description(44,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(44, @warehouseId))
               when	dbo.ufn_Configuration_Value(45, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then dbo.ufn_Configuration_Description(45,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(45, @warehouseId))
               when	dbo.ufn_Configuration_Value(46, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then dbo.ufn_Configuration_Description(46,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(46, @warehouseId))
               when dbo.ufn_Configuration_Value(47, @warehouseId) <= Datediff(hh,getdate(),i.deliverydate) then dbo.ufn_Configuration_Description(47,@WarehouseId) + ' ' + convert(nvarchar(10), dbo.ufn_Configuration_Value(47, @warehouseId))
          else 
               'Unknown'
          end,
			       case	when	dbo.ufn_Configuration_Value(44, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then 1
               when	dbo.ufn_Configuration_Value(45, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then 2
               when	dbo.ufn_Configuration_Value(46, @warehouseId)  > Datediff(hh,getdate(),i.deliverydate) then 3
               when dbo.ufn_Configuration_Value(47, @warehouseId) <= Datediff(hh,getdate(),i.deliverydate) then 4
          else 
               5
          end
  
--  insert @TableResult
--        (Class,
--         Period,
--         Weight)
--  select 'Capacity' as Class,
--		       'Current' as Period,
--		        dbo.ufn_GetCurrentShiftCapacity(1) as Weight
  
  insert @TableResult
        (Class,
         Period,
         Weight)
  select 'Capacity' as Class,
		       'Current' as Period,
		        ((sum(Weight) / count(distinct(EndDate))) * 10 - count(distinct(EndDate))) + sum(Weight)
  from OperatorAverage
 where EndDate > convert(datetime, convert(nvarchar(10), Getdate(), 120) + ' 00:00:00.000')
  
  select Class,
         Period,
		       SortId,
         Weight
    from @TableResult
end
