﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wizard_Location_Import
  ///   Filename       : p_Wizard_Location_Import.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wizard_Location_Import


as
begin
	 set nocount on;
 
Declare		@WarehouseCode                   nvarchar(50),
			@Area                            nvarchar(50),
			@AreaCode                        nvarchar(10),
			@Location                        nvarchar(15),
			@Ailse                           nvarchar(10),
			@Column                          nvarchar(10),
			@Level                           nvarchar(10),
			@PalletQuantity                  float,
			@SecurityCode                    int,
			@RelativeValue                   numeric(13,3),
			@NumberOfSUB                     int,
			@Width                           numeric(13,3),
			@Height                          numeric(13,3),
			@Length                          numeric(13,3)

  declare	@Error							int,
			@Errormsg						nvarchar(500),
			@GetDate						datetime,
			@WarehouseId					int,
			@AreaId							int,
			@StockOnHand					bit,
			@LocationTypeId					int,
			@LocationId						int
  

  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
DECLARE CursorLocatonImport CURSOR FOR

SELECT 
		[WarehouseCode]
      ,[Area]
      ,[AreaCode]
      ,[Location]
      ,[Ailse]
      ,[Column]
      ,[Level]
      ,[PalletQuantity]
      ,[SecurityCode]
      ,[RelativeValue]
      ,[NumberOfSUB]
      ,[Width]
      ,[Height]
      ,[Length]
  FROM [LocationImport]
 
OPEN CursorLocatonImport

FETCH CursorLocatonImport INTO  @WarehouseCode ,  
								@Area ,          
								@AreaCode,       
								@Location ,      
								@Ailse ,         
								@Column ,        
								@Level ,         
								@PalletQuantity, 
								@SecurityCode,   
								@RelativeValue,  
								@NumberOfSUB,    
								@Width,          
								@Height,         
								@Length         
								
WHILE @@Fetch_Status = 0
   BEGIN

  select @WarehouseId = WarehouseId
    from Warehouse
   where WarehouseCode = @WarehouseCode
  
  if @WarehouseId is null
  begin
    if @AreaCode in ('BK','RK','PK')
      set @StockOnHand = 1
    else 
      set @StockOnHand = 0
    
    exec @Error = p_Warehouse_Insert
     @WarehouseId   = @WarehouseId output,
     @Warehouse     = @WarehouseCode,
     @WarehouseCode = @WarehouseCode
    
    if @Error <> 0
      goto error
  end
  
  select @AreaId = AreaId
    from Area
   where WarehouseId = @WarehouseId
     and Area        = @Area
     and AreaCode    = @AreaCode
  
  if @AreaId is null
  begin
    if @AreaCode in ('BK','RK','PK')
      set @StockOnHand = 1
    else 
      set @StockOnHand = 0
    
    exec @Error = p_Area_Insert
     @AreaId      = @AreaId output,
     @WarehouseId = @WarehouseId,
     @Area        = @Area,
     @AreaCode    = @AreaCode,
     @StockOnHand = @StockOnHand
    
    if @Error <> 0
      goto error
  end
  
  select @LocationTypeId = LocationTypeId
    from LocationType
   where LocationTypeCode = @AreaCode
  
  select @LocationId = LocationId
    from Location
   where Location = @Location
  
  if @LocationId is null
  begin
    if @AreaCode in ('BK','RK','PK')
      set @StockOnHand = 1
    else 
      set @StockOnHand = 0
    
    exec @Error = p_Location_Insert
     @LocationId      = @LocationId output,
     @LocationTypeId  = @LocationTypeId,
     @Location        = @Location,
     @Ailse           = @Ailse,
     @Column          = @Column,
     @Level           = @Level,
     @PalletQuantity  = @PalletQuantity,
     @SecurityCode    = @SecurityCode,
     @RelativeValue   = @RelativeValue,
     @NumberOfSUB     = @NumberOfSUB,
     @StocktakeInd    = 0,
     @ActiveBinning   = 1,
     @ActivePicking   = 1,
     @Used            = 0
    
    if @Error <> 0
      goto error
  end
  else
  begin
    exec @Error = p_Location_Update
     @LocationId      = @LocationId,
     @LocationTypeId  = @LocationTypeId,
     @Location        = @Location,
     @Ailse           = @Ailse,
     @Column          = @Column,
     @Level           = @Level,
     @PalletQuantity  = @PalletQuantity,
     @SecurityCode    = @SecurityCode,
     @RelativeValue   = @RelativeValue,
     @NumberOfSUB     = @NumberOfSUB,
     @StocktakeInd    = 0,
     @ActiveBinning   = 1,
     @ActivePicking   = 1,
     @Used            = 0
    
    if @Error <> 0
      goto error
  end
  
  delete AreaLocation
   where LocationId = @LocationId
  
  exec @Error = p_AreaLocation_Insert
   @AreaId     = @AreaId,
   @LocationId = @LocationId
  
  if @Error <> 0
    goto error
  

FETCH CursorLocatonImport INTO  @WarehouseCode ,  
								@Area ,          
								@AreaCode,       
								@Location ,      
								@Ailse ,         
								@Column ,        
								@Level ,         
								@PalletQuantity, 
								@SecurityCode,   
								@RelativeValue,  
								@NumberOfSUB,    
								@Width,          
								@Height,         
								@Length

end

CLOSE CursorLocatonImport

DEALLOCATE CursorLocatonImport

  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Wizard_Location_Import'); 
    rollback transaction
	CLOSE CursorLocatonImport
	DEALLOCATE CursorLocatonImport
    return @Error
end
