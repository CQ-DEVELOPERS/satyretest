﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatchLocation_Search
  ///   Filename       : p_StorageUnitBatchLocation_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:04
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StorageUnitBatchLocation table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null output,
  ///   @LocationId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StorageUnitBatchLocation.StorageUnitBatchId,
  ///   StorageUnitBatchLocation.LocationId,
  ///   StorageUnitBatchLocation.ActualQuantity,
  ///   StorageUnitBatchLocation.AllocatedQuantity,
  ///   StorageUnitBatchLocation.ReservedQuantity,
  ///   StorageUnitBatchLocation.NettWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatchLocation_Search
(
 @StorageUnitBatchId int = null output,
 @LocationId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
 
  select
         StorageUnitBatchLocation.StorageUnitBatchId
        ,StorageUnitBatchLocation.LocationId
        ,StorageUnitBatchLocation.ActualQuantity
        ,StorageUnitBatchLocation.AllocatedQuantity
        ,StorageUnitBatchLocation.ReservedQuantity
        ,StorageUnitBatchLocation.NettWeight
    from StorageUnitBatchLocation
   where isnull(StorageUnitBatchLocation.StorageUnitBatchId,'0')  = isnull(@StorageUnitBatchId, isnull(StorageUnitBatchLocation.StorageUnitBatchId,'0'))
     and isnull(StorageUnitBatchLocation.LocationId,'0')  = isnull(@LocationId, isnull(StorageUnitBatchLocation.LocationId,'0'))
  
end
