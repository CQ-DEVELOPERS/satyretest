﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Route_Delete
  ///   Filename       : p_Route_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:07
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Route table.
  /// </remarks>
  /// <param>
  ///   @RouteId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Route_Delete
(
 @RouteId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Route
     where RouteId = @RouteId
  
  select @Error = @@Error
  
  
  return @Error
  
end
