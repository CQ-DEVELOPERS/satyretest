﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeReference_Insert
  ///   Filename       : p_StockTakeReference_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:52
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StockTakeReference table.
  /// </remarks>
  /// <param>
  ///   @StockTakeReferenceId int = null,
  ///   @WarehouseId int = null,
  ///   @StockTakeType nvarchar(100) = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null,
  ///   @OperatorId int = null 
  /// </param>
  /// <returns>
  ///   StockTakeReference.StockTakeReferenceId,
  ///   StockTakeReference.WarehouseId,
  ///   StockTakeReference.StockTakeType,
  ///   StockTakeReference.StartDate,
  ///   StockTakeReference.EndDate,
  ///   StockTakeReference.OperatorId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeReference_Insert
(
 @StockTakeReferenceId int = null,
 @WarehouseId int = null,
 @StockTakeType nvarchar(100) = null,
 @StartDate datetime = null,
 @EndDate datetime = null,
 @OperatorId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert StockTakeReference
        (StockTakeReferenceId,
         WarehouseId,
         StockTakeType,
         StartDate,
         EndDate,
         OperatorId)
  select @StockTakeReferenceId,
         @WarehouseId,
         @StockTakeType,
         @StartDate,
         @EndDate,
         @OperatorId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
