﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Picker_Performance
  ///   Filename       : p_Report_Picker_Performance.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jul 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Picker_Performance
(
 @WarehouseId int,
 @OperatorGroupCode   nvarchar(10),
 @FromDate    datetime,
 @ToDate      datetime
)

as
begin
	 set nocount on;
  
  declare @EndDate datetime,
          @EndHour int
  
  select @EndDate = max(EndDate)
    from OperatorPerformance (nolock)
  
  select og.OperatorGroup,
         o.Operator,
         op.EndDate,
         gp.Weight,
         op.ActivityStatus,
         op.Units,
         op.Weight,
         op.Instructions,
         op.Orders,
         op.OrderLines,
         convert(nvarchar(2), op.ActiveTime / 3600) + ':' + convert(nvarchar(2), (op.ActiveTime % 3600) / 60) + ':' + convert(nvarchar(2), op.ActiveTime % 60) as 'ActiveTime',
         convert(nvarchar(2), op.DwellTime / 3600) + ':' + convert(nvarchar(2), (op.DwellTime % 3600) / 60) + ':' + convert(nvarchar(2), op.DwellTime % 60) as 'DwellTime'
    from OperatorPerformance op
    join Operator             o on op.OperatorId = o.OperatorId
    join OperatorGroup       og on o.OperatorGroupId = og.OperatorGroupId
    left outer
    join GroupPerformance    gp on o.OperatorGroupId = gp.OperatorGroupId
   where op.EndDate between @FromDate and @ToDate And
   ---og.OperatorGroupCode = @OperatorGroupCode
   og.OperatorGroupCode IN ('A', 'OP')
   and op.WarehouseId = isnull(@WarehouseId,op.WarehouseId)
  union
  select og.OperatorGroup,
         'Average',
         oa.EndDate,
         gp.Weight,
         null,
         oa.Units,
         oa.Weight,
         oa.Instructions,
         oa.Orders,
         oa.OrderLines,
         convert(nvarchar(2), oa.ActiveTime / 3600) + ':' + convert(nvarchar(2), (oa.ActiveTime % 3600) / 60) + ':' + convert(nvarchar(2), oa.ActiveTime % 60) as 'ActiveTime',
         convert(nvarchar(2), oa.DwellTime / 3600) + ':' + convert(nvarchar(2), (oa.DwellTime % 3600) / 60) + ':' + convert(nvarchar(2), oa.DwellTime % 60) as 'DwellTime'
    from OperatorAverage oa
    join OperatorGroup   og on oa.OperatorGroupId = og.OperatorGroupId
    left outer
    join GroupPerformance    gp on oa.OperatorGroupId = gp.OperatorGroupId
   where oa.EndDate between @FromDate and @ToDate And
   ---og.OperatorGroupCode = @OperatorGroupCode
   og.OperatorGroupCode IN ('A', 'OP')
  order by OperatorGroup,
           EndDate
end
