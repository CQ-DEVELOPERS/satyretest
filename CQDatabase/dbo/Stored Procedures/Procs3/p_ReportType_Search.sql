﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportType_Search
  ///   Filename       : p_ReportType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 15:24:50
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ReportType table.
  /// </remarks>
  /// <param>
  ///   @ReportTypeId int = null output,
  ///   @ReportType nvarchar(100) = null,
  ///   @ReportTypeCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ReportType.ReportTypeId,
  ///   ReportType.ReportType,
  ///   ReportType.ReportTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportType_Search
(
 @ReportTypeId int = null output,
 @ReportType nvarchar(100) = null,
 @ReportTypeCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ReportTypeId = '-1'
    set @ReportTypeId = null;
  
  if @ReportType = '-1'
    set @ReportType = null;
  
  if @ReportTypeCode = '-1'
    set @ReportTypeCode = null;
  
 
  select
         ReportType.ReportTypeId
        ,ReportType.ReportType
        ,ReportType.ReportTypeCode
    from ReportType
   where isnull(ReportType.ReportTypeId,'0')  = isnull(@ReportTypeId, isnull(ReportType.ReportTypeId,'0'))
     and isnull(ReportType.ReportType,'%')  like '%' + isnull(@ReportType, isnull(ReportType.ReportType,'%')) + '%'
     and isnull(ReportType.ReportTypeCode,'%')  like '%' + isnull(@ReportTypeCode, isnull(ReportType.ReportTypeCode,'%')) + '%'
  order by ReportType
  
end
 
