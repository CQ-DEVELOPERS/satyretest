﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Job_Pick_Sheet
  ///   Filename       : p_Report_Job_Pick_Sheet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Job_Pick_Sheet
(
 @JobId            int
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int,
   IssueLineId        int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   RouteId            int,
   Route              nvarchar(50),
   JobId              int,
   InstructionId      int,
   Pallet             nvarchar(10),
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   EndDate            datetime,
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   QuantityOnHand     float,
   OperatorId         int,
   Operator           nvarchar(50),
   DropSequence       int,
   PickLocationId     int,
   PickLocation       nvarchar(15),
   PickArea           nvarchar(50),
   StoreLocationId    int,
   StoreLocation      nvarchar(15),
   StoreArea          nvarchar(50)
  )
  
  declare @OutboundShipmentId int,
          @IssueId            int,
          @WarehouseId        int
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         IssueLineId,
         JobId,
         InstructionId,
         StorageUnitBatchId,
         EndDate,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         OperatorId,
         OutboundDocumentId,
         PickLocationId,
         StoreLocationId)
  select ili.OutboundShipmentId,
         ili.IssueId,
         ili.IssueLineId,
         ins.JobId,
         ins.InstructionId,
         ins.StorageUnitBatchId,
         ins.EndDate,
         ili.Quantity,
         isnull(ili.ConfirmedQuantity,0),
         ili.Quantity - isnull(ili.ConfirmedQuantity,0),
         j.OperatorId,
         ili.OutboundDocumentId,
         ins.PickLocationId,
         ins.StoreLocationId
    from IssueLineInstruction ili (nolock)
    join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
    join Job                    j (nolocK) on ins.JobId           = j.JobId
   where ins.JobId = @JobId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set RouteId = os.RouteId
    from @TableResult     tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         Batch       = b.Batch
    from @TableResult    tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
  
  update tr
     set StoreLocation = l.Location,
         StoreArea     = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId       = al.LocationId
    join Area          a (nolock) on al.AreaId          = a.AreaId
  
  update tr
     set PickLocation = l.Location,
         PickArea     = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId       = al.LocationId
    join Area          a (nolock) on al.AreaId          = a.AreaId
    
  update tr
     set Pallet = convert(nvarchar(10), j.DropSequence)
               + ' of '
               + convert(nvarchar(10), j.Pallets)
    from @TableResult tr
    join Job           j (nolock) on tr.JobId = j.JobId
  
  select @WarehouseId = WarehouseId
    from Instruction (nolock)
   where JobId = @JobId
  
  if dbo.ufn_Configuration(71, @warehouseId) = 0
    update @TableResult
       set ExternalCompany = null
  
  select OutboundShipmentId,
         Route,
         OrderNumber,
         isnull(DropSequence,1) as 'DropSequence',
         JobId,
         Pallet,
         ExternalCompany,
         ProductCode,
         Product,
         EndDate,
         SKUCode,
         Batch,
         PickArea,
         PickLocation,
         isnull(StoreArea,'Undefined') as 'StoreArea',
         isnull(StoreLocation,'Undefined') as 'StoreLocation',
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         QuantityOnHand,
         isnull(Operator,'Unknown') as 'Operator'
    from @TableResult
end
