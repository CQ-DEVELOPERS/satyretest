﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Wave_Area_Transactions
  ///   Filename       : p_Report_Wave_Area_Transactions.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Oct 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Wave_Area_Transactions
(
 @WarehouseId int
)

as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   WaveId                          int,
   OrderNumber                     nvarchar(30),
   Operator                        nvarchar(50),
   InstructionType                 nvarchar(30),
   JobId                           int,
   ReferenceNumber                 nvarchar(30),
   InstructionId                   int,
   InstructionRefId                int,
   JobCode                         nvarchar(30),
   InstructionCode                 nvarchar(30),
   PalletId                        int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(255),
   SKUCode                         nvarchar(50),
   SKU                             nvarchar(50),
   Batch                           nvarchar(50),
   Quantity                        float,
   ConfirmedQuantity               float,
   PickLocation                    nvarchar(15),
   StoreLocation                   nvarchar(15),
   CreateDate                      datetime,
   StartDate                       datetime,
   EndDate                         datetime,
   MostRecent                      datetime
  )
  
  insert @TableResult
        (Operator,
         InstructionType,
         JobId,
         ReferenceNumber,
         InstructionId,
         InstructionRefId,
         JobCode,
         InstructionCode,
         PalletId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         Quantity,
         ConfirmedQuantity,
         PickLocation,
         StoreLocation,
         CreateDate,
         StartDate,
         EndDate,
         MostRecent)
  select distinct o.Operator,
         it.InstructionType,
         j.JobId,
         j.ReferenceNumber,
         i.InstructionId,
         i.InstructionRefId,
         s.Status,
         si.Status,
         i.PalletId,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         vs.SKU,
         vs.Batch,
         i.Quantity,
         i.ConfirmedQuantity,
         pick.Location,
         store.Location,
         i.CreateDate,
         i.StartDate,
         i.EndDate,
         isnull(i.EndDate, isnull(i.StartDate, i.CreateDate))
    from Instruction      i (nolock)
    join Status          si (nolock) on i.StatusId           = si.StatusId
    join InstructionType it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job              j (nolock) on i.JobId              = j.JobId
    join Status           s (nolock) on j.StatusId           = s.StatusId
    left
    join viewStock       vs          on i.StorageUnitBatchId = vs.StorageUnitBatchId
    left
    join viewLocation  pick (nolock) on i.PickLocationId = pick.LocationId
    left
    join viewLocation store (nolock) on i.StoreLocationId = store.LocationId
    left
    join Operator         o (nolock) on j.OperatorId = o.OperatorId
   where exists(select top 1 1 from Instruction i2 (nolock) join Status s2 (nolock) on i2.StatusId = s2.StatusId and s2.StatusCode in ('W','S') where i.JobId = i2.JobId)
     and (
           (pick.Area = 'WavePick' and it.InstructionTypeCode in ('PM','PS','FM','P'))
     or   (it.InstructionTypeCode in ('P'))
     or   (store.Area = 'WavePick' and it.InstructionTypeCode in ('M'))
         )
  
  update tr
    set OrderNumber = od.OrderNumber,
        WaveId      = iss.WaveId
   from @TableResult tr
   join IssueLineInstruction ili (nolock) on isnull(tr.InstructionRefId, tr.InstructionId) = ili.InstructionId
   join IssueLine             il (nolock) on ili.IssueLineId = il.IssueLineId
   join Issue                iss (nolock) on il.IssueId = iss.IssueId
   join OutboundLine          ol (nolock) on il.OutboundLineId = ol.OutboundLineId
   join OutboundDocument od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
  
  select WaveId,
         OrderNumber,
         Operator,
         InstructionType,
         JobId,
         ReferenceNumber,
         InstructionId,
         InstructionRefId,
         JobCode,
         InstructionCode,
         PalletId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Quantity,
         ConfirmedQuantity,
         PickLocation,
         StoreLocation,
         CreateDate,
         StartDate,
         EndDate,
         MostRecent
    from @TableResult
  order by JobId, StartDate, EndDate
end
