﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_UserOperatorInsert
  ///   Filename       : p_UserOperatorInsert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_UserOperatorInsert
(
 @userName	nvarchar(512)
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @CQCommonServer     sysname,
          @CQCommonDatabase   sysname,
          @CQSecurityServer   sysname,
          @CQSecurityDatabase sysname,
          @ServerName         sysname,
          @DatabaseName       sysname,
          @UserId             nvarchar(255),
          @Password           nvarchar(50),
          @Command            nvarchar(4000),
          @WarehouseId        int,
          @OperatorId         int,
          @OperatorGroupId    int,
          @OperatorGroup      nvarchar(50) = 'Administrator',
          @OperatorGroupCode  nvarchar(10) = 'A'
  
  select @CQCommonServer = convert(sysname, Value)
    from Configuration
   where ConfigurationId = 200
  
  select @CQCommonDatabase = convert(sysname, Value)
    from Configuration
   where ConfigurationId = 201
  
  select @CQSecurityServer = convert(sysname, Value)
    from Configuration
   where ConfigurationId = 202
  
  select @CQSecurityDatabase = convert(sysname, Value)
    from Configuration
   where ConfigurationId = 203
  
  select @ServerName = @@SERVERNAME
  
  select @DatabaseName = db_name()
  
  select @UserId = UserId
    from CQSecurity.dbo.aspnet_Users
    where UserName = @userName
  
  select @OperatorId  = OperatorId
        ,@WarehouseId = WarehouseId
        ,@Password = [Password]
    from Operator
   where Operator = @UserName
  
  if not exists(select 1 from CQSecurity.dbo.aspnet_users where UserName = @UserName)
   exec CQSecurity.dbo.aspnet_Membership_CreateUser @ApplicationName=N'/',@UserName=@UserName,@Password=N'6So8rR86Gx5As4HLicV6vcf4zDo=',@PasswordSalt=N'RQGfGhBbWg5msi0m0dbz3w==',@Email=N'grants@cquential.com',@PasswordQuestion=N'?',@PasswordAnswer=N'TQAMmX8aIIw/V1pQcCRgEP8NgsQ=',@IsApproved=1,@UniqueEmail=0,@PasswordFormat=1,@CurrentTimeUtc='Dec  3 2009 12:40:21:000PM',@UserId=@UserId output
  
  select @Command = '
  update uo
     set ServerName = ''' + @ServerName + ''',
         DatabaseName = ''' + @DatabaseName + ''',
         ConnectionString = ''Data Source=' + @ServerName + ';Initial Catalog=' + @DatabaseName + ';User ID=sa;Password=password''
    from [' + @CQCommonServer + '].[' + @CQCommonDatabase + '].dbo.UserOperator uo
    where uo.UserId  = ''' + @UserId + ''''
  
  --select @Command
  execute(@Command)
  
  select @Command = '
  insert [' + @CQCommonServer + '].' + @CQCommonDatabase + '.dbo.UserOperator (UserId, OperatorId, ServerName, DatabaseName, ConnectionString)
  select ''' + @UserId + ''', o.OperatorId, ''' + @ServerName + ''', ''' + @DatabaseName + ''', ''Data Source=' + @ServerName + ';Initial Catalog=' + @DatabaseName + ';User ID=sa;Password=password''
    from [' + @DatabaseName + '].dbo.Operator o
   where o.Operator = ''' + @userName + '''
     and not exists(select 1 from [' + @CQCommonServer + '].[' + @CQCommonDatabase + '].dbo.UserOperator uo where uo.UserId  = ''' + @UserId + ''')'
  
  --select @Command
  execute(@Command)
  
  select @OperatorGroupId = OperatorGroupId from OperatorGroup (nolock) where OperatorGroup = 'Administrator'
  
  if @OperatorGroupId is null
  begin
    exec p_OperatorGroup_Insert
     @OperatorGroupId   = @OperatorGroupId output,
     @OperatorGroup     = @OperatorGroup,
     @OperatorGroupCode = @OperatorGroupCode
  end
  
  exec p_Operator_Update @operatorId=@OperatorId,@operatorGroupId=@OperatorGroupId, @WarehouseId = @WarehouseId,@Password = @Password
  exec CQCommon..p_ChangePassword @UserName, @Password
  exec CQCommon..p_UnlockUser @UserName
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_UserOperatorInsert'); 
    return @Error
end
