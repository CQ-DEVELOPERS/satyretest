﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_RFS
  ///   Filename       : p_Report_RFS.sql
  ///   Create By      : Willis
  ///   Date Created   : 28 July 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_RFS
(
 @WarehouseId       int,
 @TaskListId        int = null,
 @FromDate          datetime = null,
 @ToDate            datetime = null
)

as
begin
	 set nocount on;
  
  if @TaskListId = -1
    set @TaskListId = null
  
  select TaskListId,
         p.Priority,
         j.Project,
         rt.RequestType,
         b.Billable,
         tl.TaskList,
         tl.Comments,
         rb.Operator as 'RaisedBy',
         ab.Operator as 'ApprovedBy',
         at.Operator as 'AllocatedTo',
         t.Operator as 'Tester',
         EstimateHours,
         ActualHours,
         CreateDate,
         TargetDate,
         Percentage,
         c.Cycle
    from TaskList tl
    left
    join Priority  p on tl.PriorityId = p.PriorityId
    left
    join Project   j on tl.ProjectId  = j.ProjectId
    left
    join Billable  b on tl.BillableId = b.BillableId
    left
    join RequestType rt on tl.RequestTypeId = rt.RequestTypeId
    left
    join Operator rb on tl.RaisedBy = rb.OperatorId
    left
    join Operator ab on tl.RaisedBy = ab.OperatorId
    left
    join Operator at on tl.RaisedBy = at.OperatorId
    left
    join Operator t on tl.RaisedBy = t.OperatorId
    left
    join Cycle    c on tl.CycleId = c.CycleId
   where tl.TaskListId = isnull(@TaskListId, tl.TaskListId)
     and isnull(tl.TargetDate, tl.CreateDate) between @FromDate and @ToDate
end
