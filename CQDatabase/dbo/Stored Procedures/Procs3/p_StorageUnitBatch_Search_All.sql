﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_Search_All
  ///   Filename       : p_StorageUnitBatch_Search_All.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007 11:54:26
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null output,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(30) = null,
  ///   @Product nvarchar(50) = null,
  ///   @Barcode nvarchar(50) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   p.ProductId,
  ///   p.StatusId,
  ///   s.Status,
  ///   p.ProductCode,
  ///   p.Product,
  ///   p.Barcode,
  ///   p.MinimumQuantity,
  ///   p.ReorderQuantity,
  ///   p.MaximumQuantity,
  ///   p.CuringPeriodDays,
  ///   p.ShelfLifeDays,
  ///   p.QualityAssuranceIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatch_Search_All
(
 @ProductCode nvarchar(30) = null,
 @Product     nvarchar(50) = null,
 @SKUCode     nvarchar(50) = null,
 @SKU         nvarchar(50) = null,
 @Batch       nvarchar(50) = null,
 @ECLNumber   nvarchar(10) = null,
 @WarehouseId int = 1,
 @PrincipalId int = null
)

as
begin
	 set nocount on;
	 
  declare @TableResult as table
  (
   StorageUnitBatchId              int,
   StorageUnitId                   int,
   PrincipalCode                   nvarchar(30),
   Principal                       nvarchar(255),
   ProductCode                     nvarchar(60),
   Product                         nvarchar(100),
   SKUCode                         nvarchar(20),
   SKU                             nvarchar(100),
   Batch                           nvarchar(100),
   ECLNumber                       nvarchar(20),
   Quantity                        int,
   Location                        nvarchar(30),
   Area                            nvarchar(100),
   ExpiryDays                      int,
   StatusId                        int,
   Status                          nvarchar(50),
   Yield                           float
  )
  
  declare @PackTypeId int,
          @StatusId   int
  
  select @StatusId = dbo.ufn_StatusId('P','A')
  
  select top 1 @PackTypeId = PackTypeId
    from PackType (nolock)
  order by InboundSequence
  
  if @ProductCode = '-1'
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
  
  if @SKUCode = '-1'
    set @SKUCode = null;
  
  if @Batch = '-1'
    set @Batch = null;
  
  if @ECLNumber = '-1'
    set @ECLNumber = null;

  if @PrincipalId = '-1'
    set @PrincipalId = null
  
  if @ProductCode is null and
     @Product is null and
     @SKUCode is null and
     @Batch is null and
     @ECLNumber is null
    goto result
  
  if (select dbo.ufn_Configuration(241, 1)) = 1 -- Product Search - Batch - 6 char min
  if @Batch is null or datalength(@batch) < 6
    return
  
  insert @TableResult
        (StorageUnitBatchId,
         StorageUnitId,
         PrincipalCode,
         Principal,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ECLNumber,
         ExpiryDays,
         StatusId,
         Status,
         Yield)
  select sub.StorageUnitBatchId,
         su.StorageUnitId,
         pn.PrincipalCode,
         pn.Principal,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         b.ECLNumber,
         case when datediff(dd, b.ExpiryDate, dateadd(dd, isnull(p.ShelfLifeDays,0), getdate())) < 0
               then datediff(dd, b.ExpiryDate, dateadd(dd, isnull(p.ShelfLifeDays,0) /* * -365*/, getdate())) * -1
               else datediff(dd, b.ExpiryDate, dateadd(dd, isnull(p.ShelfLifeDays,0), getdate()))
               end as 'ExpiryDays',
         p.StatusId, -- This is the product statusId
         s.Status,   -- This is the batch status description
         b.ActualYield
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join SKU              sku (nolock) on su.SKUId          = sku.SKUId
    join Product            p (nolock) on su.ProductId      = p.ProductId
    left
    join Principal         pn (nolock) on p.PrincipalId     = pn.PrincipalId
    join Batch              b (nolock) on sub.BatchId       = b.BatchId
    join Status             s (nolock) on b.StatusId        = s.StatusId
   where p.ProductCode             like '%' + isnull(@ProductCode, isnull(p.ProductCode,'%')) + '%'
     and p.Product                 like '%' + isnull(@Product, isnull(p.Product,'%')) + '%'
     and sku.SKUCode               like '%' + isnull(@SKUCode, isnull(sku.SKUCode,'%')) + '%'
     and sku.SKU                   like '%' + isnull(@SKU, isnull(sku.SKU,'%')) + '%'
     and b.Batch                   like '%' + isnull(@Batch, isnull(b.Batch,'%')) + '%'
     and isnull(b.ECLNumber,'%')   like '%' + isnull(@ECLNumber, isnull(b.ECLNumber,'%')) + '%'
     and (p.PrincipalId = @PrincipalId or @PrincipalId is null)
  order by p.ProductCode,
           sku.SKUCode,
           'ExpiryDays',
           b.Batch,
           Status
  
  update tr
     set Quantity = pk.Quantity,
         StatusId = isnull(pk.StatusId, tr.StatusId)
    from @TableResult tr
    join Pack         pk (nolock) on pk.StorageUnitId = tr.StorageUnitId
                                 and pk.WarehouseId   = @WarehouseId
                                 and pk.PackTypeId    = @PackTypeId
  
  update tr
     set Location = l.Location
    from @TableResult         tr
    join StorageUnitLocation sul (nolock) on tr.StorageUnitId = sul.StorageUnitId
    join Location              l (nolock) on sul.LocationId   = l.LocationId
    join AreaLocation         al (nolock) on l.LocationId     = al.LocationId
    join Area                  a (nolock) on al.AreaId        = a.AreaId
   where a.AreaCode  = 'PK'
     and tr.StatusId = @StatusId
  
  update tr
     set Area = a.Area
    from @TableResult         tr
    join StorageUnitArea     sua (nolock) on tr.StorageUnitId = sua.StorageUnitId
                                         and sua.StoreOrder   = 1
    join Area                  a (nolock) on sua.AreaId       = a.AreaId
   where tr.StatusId = @StatusId
  
  result:
  
  select StorageUnitBatchId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ECLNumber,
         Quantity,
         Location,
         Area,
         ExpiryDays,
         Status,
         Yield
   from @TableResult
  where isnull(StatusId,@StatusId) = @StatusId
end
