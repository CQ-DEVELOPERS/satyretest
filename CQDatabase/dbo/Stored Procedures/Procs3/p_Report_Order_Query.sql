﻿
   
/*  
  /// <summary>  
  ///   Procedure Name : p_Report_Order_Query  
  ///   Filename       : p_Report_Order_Query.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 15 Jul 2008  
  /// </summary>  
    /// <summary>  
  ///   Procedure Name : p_Report_Order_Query  
  ///   Filename       : p_Report_Order_Query.sql  
  ///   Modified By      : Junaid Desai  
  ///   Date Modified   : 18 Jul 2008  
  /// </summary>  
  /// <remarks>  
  ///   Modified so that Percentage may be coorect value. the initial sp will have to be changed.  
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :  Karen 
  ///   Modified Date  :  25 Jan 2011
  ///   Details        :  If the delivery date has a time of zero then default it to 17:00 so that the 
  ///                     report does not indicate "overdue"
  /// </newpara>  
*/  
CREATE procedure p_Report_Order_Query  
(  
 @ExternalCompanyId int,  
 @OrderNumber       nvarchar(30),  
 @FromDate          datetime,  
 @ToDate            datetime,
 @WarehouseId		int = null
 --@StatusId			int
)  
  
as  
begin  
  set nocount on;  
    
  declare @TableResult as table  
  (  
   OrderNumber                     nvarchar(30),  
   InvoiceNumber                   nvarchar(30),  
   OutboundDocumentTypeId          int,  
   OutboundDocumentTypeCode        nvarchar(10),  
   OutboundDocumentType            nvarchar(50),  
   OutboundShipmentId              int,  
   IssueId                         int,  
   ExternalCompanyId               int,  
   ExternalCompany                 nvarchar(255),  
   ExternalCompanyCode             nvarchar(30),  
   CreateDate                      datetime,  
   DeliveryDate                    datetime,  
   PlanningComplete                datetime,  
   Release                         datetime,  
   Checking                        datetime,  
   Checked                         datetime,  
   Despatch                        datetime,  
   DespatchChecked                 datetime,  
   Complete                        datetime,  
   PickComplete                    float,  
   PickTotal                       float,  
   PickPercentage                  float,  
   CheckingComplete                float,  
   CheckingTotal                   float,  
   CheckingPercentage              float,  
   Status                          nvarchar(50), 
   StatusId						   int,	
   Extracted                       datetime,  
   ReceivedFromHost                datetime,  
   Data                            nvarchar(255),  
   TotalQuantity                   float,
   Delivery						   int  ,
   PODCodeDesc					   nvarchar(255),
   PODUrl						   nvarchar(255)
  )  
    
  declare @TableJobs as table  
  (  
   IssueId    int,  
   JobId      int,  
   StatusCode nvarchar(10)  
  )  
    
  if @ExternalCompanyId = -1  
    set @ExternalCompanyId = null  
    
 -- if @StatusId = -1
--	set @StatusId = null
    
  insert @TableResult  
        (ExternalCompany,  
         ExternalCompanyCode,  
         OrderNumber,  
         OutboundShipmentId,  
         IssueId,  
         CreateDate,  
         DeliveryDate,  
         OutboundDocumentTypeId,  
         Status,  
         StatusId,
         TotalQuantity,
         PickTotal,
         PickComplete,
         Delivery,
         PODCodeDesc,
         PODUrl)
  select ec.ExternalCompany,
         ec.ExternalCompanyCode,
         od.OrderNumber,
         null,
         i.IssueId,
         od.CreateDate,
         case when (datepart ("Hour", i.DeliveryDate) = 0)
             then dateadd ("Hour",17, i.DeliveryDate)
             else i.DeliveryDate
         end as DeliveryDate,
         od.OutboundDocumentTypeId,
         s.Status,
         s.StatusId,
         0,
         i.Total,
         i.Complete,
         i.Delivery,
         null,
         'http://logistics.value.co.za/'
    from OutboundDocument        od (nolock)
    join Issue                    i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    join ExternalCompany         ec (nolock) on od.ExternalCompanyId  = ec.ExternalCompanyId
    join Status                   s (nolock) on i.StatusId            = s.StatusId
    --left join InterfaceInvoice   ii (nolock) on ii.OrderNumber = od.OrderNumber
    --left join InterfaceImportPOD pod (nolock) on pod.CustWayNo = ii.InvoiceNumber 
    --left join InterfaceImportPOD pod2 (nolock) on pod2.CustXRef = ii.OrderNumber
   where ec.ExternalCompanyId = isnull(@ExternalCompanyId, ec.ExternalCompanyId)
     and od.OrderNumber    like '%' + @OrderNumber + '%'
     and od.CreateDate  between @FromDate and @ToDate
     and od.WarehouseId = isnull(@WarehouseId,od.WarehouseId)
    -- and i.StatusId = isnull(@StatusId, i.StatusId) 
   order by ec.ExternalCompany,  
            od.OrderNumber
  
  update tr
     set TotalQuantity = (select sum(il.ConfirmedQuatity)
                            from IssueLine il (nolock)
                           where il.IssueId = tr.IssueId)
    from @TableResult tr
  
  update tr
     set ReceivedFromHost = il.ProcessedDate,
         Data             = substring(il.Data, 1, 20)
    from @TableResult tr
    join InterfaceLog il (nolock) on il.Data like '%SO%' + tr.OrderNumber + '%'
  
  update tr
     set ReceivedFromHost = il.ProcessedDate,
         Data             = substring(il.Data, 1, 20)
    from @TableResult tr
    join InterfaceLog il (nolock) on il.Data like '%DL%' + tr.OrderNumber + '%'
   where tr.Data is null
  
  update tr
     set Extracted = h.ProcessedDate
    from @TableResult tr
    join InterfaceExportSOHeader h (nolock) on tr.OrderNumber = h.PrimaryKey
                                            or tr.OrderNumber = h.OrderNumber
   where h.RecordStatus = 'Y'
  
  update tr
     set OutboundDocumentType = odt.OutboundDocumentType,
         OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode
    from @TableResult          tr
    join OutboundDocumentType odt on tr.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  
  update @TableResult
     set DeliveryDate = dateadd(hh, 3, DeliveryDate)
   where OutboundDocumentTypeCode in ('COD','CNC')
  
  insert @TableJobs
        (IssueId,
         JobId)
  select distinct
         ili.IssueId,
         i.JobId
    from @TableResult          tr
    join IssueLineInstruction ili (nolock) on tr.IssueId        = ili.IssueId
    join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
  
  update tj
     set StatusCode = s.StatusCode
    from @TableJobs tj
    join Job         j on tj.JobId = j.JobId
    join Status      s on j.StatusId = s.StatusId

  update tr
     set PlanningComplete = (select max(PlanningComplete)
                               from OutboundPerformance   op (nolock)
                               join @TableJobs            tj on op.JobId = tj.JobId
                              where tr.IssueId = tj.IssueId)
    from @TableResult tr
  
  update tr
     set Release = (select max(Release)
                       from OutboundPerformance   op (nolock)
                       join @TableJobs            tj on op.JobId = tj.JobId
                      where tr.IssueId = tj.IssueId)
    from @TableResult tr
  
  update tr
     set Checking = (select max(Checking)
                       from OutboundPerformance   op (nolock)
                       join @TableJobs            tj on op.JobId = tj.JobId
                      where tr.IssueId = tj.IssueId)
    from @TableResult tr
  
  update tr  
     set Checked = (select max(Checked)  
                      from OutboundPerformance   op (nolock)  
                      join @TableJobs            tj on op.JobId = tj.JobId  
                     where tr.IssueId = tj.IssueId)  
    from @TableResult tr  
    
  update tr  
     set Despatch = (select max(Despatch)  
                       from OutboundPerformance   op (nolock)  
                       join @TableJobs            tj on op.JobId = tj.JobId  
                      where tr.IssueId = tj.IssueId)  
    from @TableResult tr  
    
  update tr  
     set DespatchChecked = (select max(DespatchChecked)  
                              from OutboundPerformance   op (nolock)  
                              join @TableJobs            tj on op.JobId = tj.JobId  
                             where tr.IssueId = tj.IssueId)  
    from @TableResult tr  
    
  update tr  
     set Complete = (select max(Complete)  
                       from OutboundPerformance   op (nolock)  
                       join @TableJobs            tj on op.JobId = tj.JobId  
                      where tr.IssueId = tj.IssueId)  
    from @TableResult tr  
    
  
  update @TableResult  
     set PickPercentage = (PickComplete / PickTotal) * 100  
   where PickComplete > 0  
     and PickTotal    > 0  
    
  update tr  
     set CheckingTotal = (select count(distinct(tj.JobId))  
                            from @TableJobs tj  
                           where tr.IssueId = tj.IssueId)  
    from @TableResult tr  
    
  update tr  
     set CheckingComplete = (select count(distinct(tj.JobId))  
                               from @TableJobs tj  
                              where tr.IssueId     = tj.IssueId  
                                and tj.StatusCode in ('NS','CD','D','DC','C'))  
    from @TableResult tr  
    
  update @TableResult  
     set CheckingPercentage = (CheckingComplete / CheckingTotal) * 100  
   where CheckingComplete > 0  
     and CheckingTotal    > 0  
    
  update tr  
     set InvoiceNumber = ii.InvoiceNumber  
    from @TableResult     tr  
    join InterfaceInvoice ii on tr.OrderNumber = ii.OrderNumber  
    
  if  (select dbo.ufn_Configuration(346, @WarehouseId)) = 0    
  select ExternalCompany,  
         ExternalCompanyCode,  
         OrderNumber + '(' + convert(nvarchar(10),isnull(Delivery,1)) + ')' as OrderNumber,  
         InvoiceNumber,  
         OutboundDocumentType,  
         CreateDate,  
         PlanningComplete,  
         Release,  
         Checking,  
         Checked,  
         Despatch,  
         DespatchChecked,  
         Complete,  
         IssueId,  
         --isnull(OutboundShipmentId,-1) as 'OutboundShipmentId',  
         OutboundShipmentId,  
         PickPercentage,  
         CheckingPercentage,  
         DeliveryDate,  
         Status,  
         TotalQuantity,  
         Extracted,  
         ReceivedFromHost,  
         Data,
         null as URL
    from @TableResult  
  order by ExternalCompany,OrderNumber  
  
  
  if  (select dbo.ufn_Configuration(346, @WarehouseId)) = 1   
  begin
  
      update tr
     set tr.PODCodeDesc = (select max(pod.PODCodeDesc)
                       from InterfaceImportPOD  pod (nolock)
                       join @TableResult            tr on tr.InvoiceNumber = pod.CustWayNo)
    from @TableResult tr
    where tr.PODCodeDesc is null
  
    update tr
     set tr.PODCodeDesc = (select max(pod.PODCodeDesc)
                       from InterfaceImportPOD  pod (nolock)
                       join @TableResult            tr on tr.OrderNumber = pod.CustXRef)
    from @TableResult tr
    where tr.PODCodeDesc is null
    
  select ExternalCompany,  
         ExternalCompanyCode,  
         OrderNumber + '(' + convert(nvarchar(10),isnull(Delivery,1)) + ')' as OrderNumber,  
         InvoiceNumber,  
         OutboundDocumentType,  
         CreateDate,  
         PlanningComplete,  
         Release,  
         Checking,  
         Checked,  
         Despatch,  
         DespatchChecked,  
         Complete,  
         IssueId,  
         --isnull(OutboundShipmentId,-1) as 'OutboundShipmentId',  
         OutboundShipmentId,
         PickPercentage,  
         CheckingPercentage,  
         DeliveryDate,  
         PODCodeDesc as Status,  
         TotalQuantity,  
         Extracted,  
         ReceivedFromHost,  
         Data,
         PODUrl as URL
    from @TableResult  
  order by ExternalCompany,OrderNumber
  end
    

end
 
