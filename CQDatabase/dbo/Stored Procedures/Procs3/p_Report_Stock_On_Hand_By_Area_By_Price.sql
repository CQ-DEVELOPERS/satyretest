﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_On_Hand_By_Area_By_Price
  ///   Filename       : p_Report_Stock_On_Hand_By_Area_By_Price.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 23 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Stock_On_Hand_By_Area_By_Price
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId       int
)

as
begin
	 set nocount on;
  
  declare @TableResult as table 
  (
   LocationId                      int        ,
   RelativeValue                   numeric(13,3),
   ProductCode                     nvarchar(30),
   Product                         nvarchar(255),
   SKUCode                         nvarchar(50),
   Batch                           nvarchar(50),
   BatchID                         int,             --added to get unique price
   StorageUnitID                   int,             --added to get unique price
   Area                            nvarchar(50),
   Location                        nvarchar(15),
   ActualQuantity                  float        ,
   AllocatedQuantity               float        ,
   ReservedQuantity                float        ,
   Price                           numeric(13,2)    --added for unit price
  )
  
  insert @TableResult
        (LocationId,
         RelativeValue,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         BatchID,
         StorageUnitID,
         Area,
         Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity)
  select l.LocationId,
         l.RelativeValue,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         b.BatchId,
         sub.StorageUnitId,
         a.Area,
         l.Location,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId

   where a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
  
  insert @TableResult
        (LocationId,
         l.RelativeValue,
         Location,
         Area)
  select l.LocationId,
         l.RelativeValue,
         l.Location,
         a.Area
    from Location      l
    join AreaLocation al on l.LocationId = al.LocationId
    join Area          a on al.AreaId    = a.AreaId
   where not exists(select top 1 1 from @TableResult tr where tr.LocationId = l.LocationId)
     and a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1

  update t
  set Price = il.UnitPrice * il.Quantity
  from @TableResult t
  inner join InboundLine il on t.BatchID = il.BatchId
                            and t.StorageUnitID = il.StorageUnitId
  
  select ProductCode,
         Product,
         SKUCode,
         Batch,
         Area,
         Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         Price
    from @TableResult
  order by RelativeValue,
           Area,
           Location,
           Product,
           SKUCode
end
