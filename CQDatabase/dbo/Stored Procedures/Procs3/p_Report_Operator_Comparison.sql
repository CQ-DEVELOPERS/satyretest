﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Operator_Comparison
  ///   Filename       : p_Report_Operator_Comparison.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 08 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Junaid Desai	
  ///   Modified Date  : 13 Oct 2008
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Operator_Comparison
(
			@WarehouseId						int,
			@StartDate							DateTime,
			@EndDate							DateTime,
			@UOM								nvarchar(15) = 'Units',
			@OperatorGroupId					int = null,
			@OperatorId							int = null,
			@TimePeriod							int = null,
			@ScalePeriod						nvarchar(10) = null
)

as
begin
	 set nocount on;
	
if (not isnull(@ScalePeriod,' ') = ' ')
--
Begin
		--
		if (@TimePeriod = 1)
			Begin
				Set @StartDate = @ScalePeriod
				Set @EndDate = DateAdd(dd,1,@StartDate)
			End

		if (@TimePeriod = 2)
			Begin

				Declare @NowYearFirst	DateTime,
						@WeekFirstStart DateTime
			
				Set @NowYearFirst = '01/01/' + Convert(nvarchar,Year(@StartDate)) + ' 12:00:00 AM'

				--SET @WeekFirstStart = DATEADD(wk, DATEDIFF(wk, 6, @NowYearFirst), 6)

				--Set @StartDate = DateAdd(wk,Convert(int,@ScalePeriod),@WeekFirstStart)

				Set @StartDate = Dateadd(dd,1-(DatePart(DW,Dateadd(wk,Convert(int,@ScalePeriod)-1,@NowYearFirst))),Dateadd(wk,Convert(int,@ScalePeriod)-1,@NowYearFirst))

				Set @EndDate = DateAdd(d,6,@StartDate)

			end

		if (@TimePeriod = 3)
			Begin
					Set @StartDate = '01 ' + @ScalePeriod  + ' ' + Convert(nvarchar,Year(getDate())) + ' 12:00:00 AM'
				
					Set @EndDate = DateAdd(d,-1,DateAdd(mm,1,@StartDate))
					print @StartDate
					print @EndDate
			end

End
print @StartDate
Print @EndDate



Declare @DateHoursDiff		int,
		@DateDaysDiff		int,
		@DateMonthsDiff		int

Set @DateHoursDiff = DateDiff(hh,@StartDate,@EndDate)
Set @DateDaysDiff = DateDiff(D,@StartDate,@EndDate)
Set @DateMonthsDiff = DateDiff(M,@StartDate,@EndDate) 

Declare @TempTable As Table
(
	OperatorGroupId			int,
	OperatorId				int,
	Operator				nvarchar(50),
	OperatorGroup			nvarchar(50),
	EndDate					dateTime,
	SiteTarget				bigint,
	IndustryTarget			bigint,
	Actual					bigint,
	UOM						nvarchar(10)
)



Declare @TableHours As Table
(
	OperatorId				int,
	EndDate					dateTime,
	HoursWorked				bigint
)


if (@UOM = 'Units')
Begin

Insert Into @TempTable (OperatorGroupId,OperatorId,Operator,OperatorGroup,EndDate,SiteTarget,IndustryTarget,Actual,UOM)
(
Select O.OperatorGroupID,OP.OperatorId,O.Operator,OG.OperatorGroup,Op.EndDate,GPSite.Units,GPIndustry.Units,OP.Units,'Pieces'
	 FROM         OperatorPerformance OP
	Inner Join Operator O				on  O.OperatorId		= OP.OperatorId
	Inner Join OperatorGroup OG			on	O.OperatorGroupId   = OG.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From GroupPerformance Where PerformanceType = 'S' and OperatorGroupId = @OperatorGroupId) GPSite	on  GPSite.OperatorGroupId	= O.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From GroupPerformance Where PerformanceType = 'I' and OperatorGroupId = @OperatorGroupId) GPIndustry	on  GPIndustry.OperatorGroupId	= O.OperatorGroupId
	Where Op.EndDate Between @StartDate And  @EndDate
	And O.OperatorGroupID = isnull(@OperatorGroupId,O.OperatorGroupID)
	And O.OperatorId = isnull(@OperatorId,O.OperatorId )
	and op.WarehouseId = Isnull(@WarehouseId,op.WarehouseId)
	and OP.InstructionTypeId Not In (4, 7, 9, 10, 11, 12, 15, 16)
	--Group BY OP.OperatorId,EndDate
						
)
End

if (@UOM = 'Weight')
Begin

Insert Into @TempTable (OperatorGroupId,OperatorId,Operator,OperatorGroup,EndDate,SiteTarget,IndustryTarget,Actual,UOM)
(
Select O.OperatorGroupID,OP.OperatorId,O.Operator,OG.OperatorGroup,EndDate,GPSite.Weight,GPIndustry.Weight,OP.Weight,'KG'
	 FROM         OperatorPerformance OP
	Inner Join Operator O				on  O.OperatorId		= OP.OperatorId
	Inner Join OperatorGroup OG			on	O.OperatorGroupId   = OG.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From GroupPerformance Where PerformanceType = 'S' and OperatorGroupId = @OperatorGroupId) GPSite	on  GPSite.OperatorGroupId	= O.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From GroupPerformance Where PerformanceType = 'I' and OperatorGroupId = @OperatorGroupId) GPIndustry	on  GPIndustry.OperatorGroupId	= O.OperatorGroupId
	Where Op.EndDate Between @StartDate And  @EndDate	
	And O.OperatorGroupID = isnull(@OperatorGroupId,O.OperatorGroupID)
	And O.OperatorId = isnull(@OperatorId,O.OperatorId )
	and op.WarehouseId = Isnull(@WarehouseId,op.WarehouseId)
	and OP.InstructionTypeId Not In (4, 7, 9, 10, 11, 12, 15, 16)
--Group BY OP.OperatorId,EndDate
						
)
End

if (@UOM = 'Jobs')
Begin

Insert Into @TempTable (OperatorGroupId,OperatorId,Operator,OperatorGroup,EndDate,SiteTarget,IndustryTarget,Actual,UOM)
(
Select O.OperatorGroupID,OP.OperatorId,O.Operator,OG.OperatorGroup,EndDate,GPSite.Jobs,GPIndustry.Jobs,OP.Jobs,'Jobs'
	 FROM         OperatorPerformance OP
	Inner Join Operator O				on  O.OperatorId		= OP.OperatorId
	Inner Join OperatorGroup OG			on	O.OperatorGroupId   = OG.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From GroupPerformance Where PerformanceType = 'S' and OperatorGroupId = @OperatorGroupId) GPSite	on  GPSite.OperatorGroupId	= O.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From GroupPerformance Where PerformanceType = 'I' and OperatorGroupId = @OperatorGroupId) GPIndustry	on  GPIndustry.OperatorGroupId	= O.OperatorGroupId
	Where Op.EndDate Between @StartDate And  @EndDate
	And O.OperatorGroupID = isnull(@OperatorGroupId,O.OperatorGroupID)
	And O.OperatorId = isnull(@OperatorId,O.OperatorId )
	and op.WarehouseId = Isnull(@WarehouseId,op.WarehouseId)
	and OP.InstructionTypeId Not In (4, 7, 9, 10, 11, 12, 15, 16)
	--Group BY OP.OperatorId,EndDate
						
)
End

if (@UOM = 'Instructions')
Begin

Insert Into @TempTable (OperatorGroupId,OperatorId,Operator,OperatorGroup,EndDate,SiteTarget,IndustryTarget,Actual,UOM)
(
Select O.OperatorGroupID,
		OP.OperatorId,
		O.Operator,OG.
		OperatorGroup,
		EndDate,
		GPSite.Instructions,
		GPIndustry.Instructions,
		OP.Instructions,
		'Pick Lines'
	 FROM         OperatorPerformance OP
	Inner Join Operator O				on  O.OperatorId		= OP.OperatorId
	Inner Join OperatorGroup OG			on	O.OperatorGroupId   = OG.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From GroupPerformance Where PerformanceType = 'S' and OperatorGroupId = @OperatorGroupId) GPSite	on  GPSite.OperatorGroupId	= O.OperatorGroupId
	Inner Join (Select OperatorGroupID,Units,Weight,Instructions,Orders,OrderLines,Jobs From GroupPerformance Where PerformanceType = 'I' and OperatorGroupId = @OperatorGroupId) GPIndustry	on  GPIndustry.OperatorGroupId	= O.OperatorGroupId
	Where Op.EndDate Between @StartDate And  @EndDate
	And O.OperatorGroupID = isnull(@OperatorGroupId,O.OperatorGroupID)
	And O.OperatorId = isnull(@OperatorId,O.OperatorId )
	and op.WarehouseId = Isnull(@WarehouseId,op.WarehouseId)
	and OP.InstructionTypeId Not In (4, 7, 9, 10, 11, 12, 15, 16)
	--Group BY OP.OperatorId,EndDate
						
)
End

If (not Isnull(@TimePeriod,'0') = '0')
	Begin 
				if (@TimePeriod = 1)
					Begin
						--Select 'x < 24 Hours' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
 					SELECT		OperatorId,
								Operator, 
								--InstructionTypeId,
								Convert(nvarchar,EndDate,108) as EndDate, 
								Convert(nvarchar,EndDate,108) as ordscale, 
								OperatorGroupId,
								Sum(Distinct(SiteTarget)) As SiteTarget,
								Sum(Distinct(IndustryTarget)) As IndustryTarget,
								Sum(Actual) as Actual ,UOM ,
								'1' as TimePeriod
							From @TempTable
							Group by OperatorId,Operator,OperatorGroupId,Convert(nvarchar,EndDate,108),Uom
							ORDER BY ordscale
						
						-- Select data by this Scale
					End
				if (@TimePeriod = 2)
					Begin
						--Select '24 Hours < x < 1 week' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
					Insert Into @TableHours (OperatorId,EndDate,HoursWorked)
					Select OperatorId,
							Convert(nvarchar,EndDate,101) as EndDate,
							Count(Distinct(EndDate)) as HoursWorked
					From @TempTable
					Group by OperatorId,
							Convert(nvarchar,EndDate,101)

						
						
					
 					SELECT		t.OperatorId, 
								t.Operator,
								h.EndDate as EndDate,
								h.EndDate as ordscale, 
								OperatorGroupId,
								h.HoursWorked * Sum(Distinct(SiteTarget)) As SiteTarget,
								h.HoursWorked * Sum(Distinct(IndustryTarget)) As IndustryTarget,
								Sum(Actual) as Actual ,UOM ,
								'2' as TimePeriod
							From @TempTable t
									Join @TableHours h on h.OperatorId = t.OperatorId and h.EndDate = Convert(nvarchar,t.EndDate,101)
							Group by t.OperatorId,Operator,OperatorGroupId,h.EndDate,Uom,h.HoursWorked
							ORDER BY ordscale						
						-- Select data by this Scale
					End
			if (@TimePeriod = 3)
				Begin
					--Select '1 Week< x < 3 months' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime

					Insert Into @TableHours (OperatorId,EndDate,HoursWorked)
					Select OperatorId,
							dateadd(wk,DatePart(wk,EndDate)-1 ,convert(char(4),datepart(year,enddate)) + '-01-01') as EndDate,
							Count(Distinct(EndDate)) as HoursWorked
					From @TempTable
					Group by OperatorId,
							dateadd(wk,DatePart(wk,EndDate)-1,convert(char(4),datepart(year,enddate)) + '-01-01')
--					Select dateadd(wk,DatePart(wk,EndDate),convert(char(4),datepart(year,enddate)) + '-01-01'),
--					OperatorId from @TempTable

 					SELECT		t.OperatorId,
								t.Operator,
								h.EndDate,
								h.EndDate as ordscale, 
								t.OperatorGroupId,
								h.HoursWorked * Sum(Distinct(SiteTarget)) As SiteTarget,
								h.HoursWorked * Sum(Distinct(IndustryTarget)) As IndustryTarget,
								Sum(Actual) as Actual ,UOM ,
								'3' as TimePeriod
					From @TempTable t
							Join @TableHours h on h.OperatorId = t.OperatorId and h.EndDate = dateadd(wk,DatePart(wk,t.EndDate) -1,convert(char(4),datepart(year,t.enddate)) + '-01-01')
--DatePart(wk,t.EndDate)
							Group by t.OperatorId,Operator,OperatorGroupId,h.EndDate,Uom,h.HoursWorked
							ORDER BY ordscale		
				
					-- Select data by this Scale
				End
if (@TimePeriod = 4)
	Begin
			--Select  '3 months < x' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
					Insert Into @TableHours (OperatorId,EndDate,HoursWorked)
					Select OperatorId,
							DatePart(M,EndDate) as EndDate,
							Count(Distinct(EndDate)) as HoursWorked
					From @TempTable
					Group by OperatorId,
							DatePart(M,EndDate)

					SELECT		t.OperatorId, 
								t.Operator,
								--InstructionTypeId,
								h.EndDate as EndDate,
								h.EndDate as ordscale,
								t.OperatorGroupId,
								h.HoursWorked * Sum(Distinct(SiteTarget)) As SiteTarget,
								h.HoursWorked * Sum(Distinct(IndustryTarget)) As IndustryTarget,
								Sum(Actual) as Actual ,
								UOM ,
								'4' as TimePeriod
					From @TempTable t
						Join @TableHours h on h.OperatorId = t.OperatorId and h.EndDate = DatePart(wk,t.EndDate)
					Group by t.OperatorId,Operator,OperatorGroupId,h.EndDate,--DateName(M,EndDate),
							Uom,h.HoursWorked
					ORDER BY ordscale
	End

	End

Else

	Begin


if (@DateMonthsDiff < 3)
	Begin
		if (@DateDaysDiff < 8)		
			Begin
				if (@DateHoursDiff < 25)
					Begin
						--Select 'x < 24 Hours' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
 					SELECT		OperatorId,
								Operator, 
								OperatorGroup,
								--InstructionTypeId,
								Convert(nvarchar,EndDate,108) as EndDate, 
								Convert(nvarchar,EndDate,108) as ordscale, 
								OperatorGroupId,
								Sum(Distinct(SiteTarget)) As SiteTarget,
								Sum(Distinct(IndustryTarget)) As IndustryTarget,
								Sum(Actual) as Actual ,UOM ,
								'1' as TimePeriod
							From @TempTable 
							Group by OperatorId,Operator,OperatorGroupId,OperatorGroup,Convert(nvarchar,EndDate,108),Uom
							ORDER BY ordscale
						
						-- Select data by this Scale
					End
				Else
					Begin
						--Select '24 Hours < x < 1 week' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime

					Insert Into @TableHours (OperatorId,EndDate,HoursWorked)
					Select OperatorId,
							Convert(nvarchar,EndDate,101) as EndDate,
							Count(Distinct(EndDate)) as HoursWorked
					From @TempTable
					Group by OperatorId,
							Convert(nvarchar,EndDate,101)

 					SELECT		t.OperatorId, 
								t.Operator,
								t.OperatorGroup,
								h.EndDate as EndDate,
								h.EndDate as ordscale, 
								OperatorGroupId,
								h.HoursWorked * Sum(Distinct(SiteTarget)) As SiteTarget,
								h.HoursWorked * Sum(Distinct(IndustryTarget)) As IndustryTarget,
								Sum(Actual) as Actual ,UOM ,
								'2' as TimePeriod
							From @TempTable t
								Join @TableHours h on h.OperatorId = t.OperatorId and h.EndDate = Convert(nvarchar,t.EndDate,101)
							Group by t.OperatorId,Operator,OperatorGroupId,OperatorGroup,h.EndDate,Uom,h.HoursWorked
							ORDER BY ordscale						
						-- Select data by this Scale
					End
			End
			Else
				Begin
					--Select '1 Week< x < 3 months' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime


					Insert Into @TableHours (OperatorId,EndDate,HoursWorked)
					Select OperatorId,
							dateadd(wk,DatePart(wk,EndDate)-1,convert(char(4),datepart(year,enddate)) + '-01-01') as EndDate,
--DatePart(wk,EndDate) as EndDate,
							Count(Distinct(EndDate)) as HoursWorked
					From @TempTable
					Group by OperatorId,
							dateadd(wk,DatePart(wk,EndDate)-1,convert(char(4),datepart(year,enddate)) + '-01-01') 

						SELECT		t.OperatorId,
								t.Operator,
								t.OperatorGroup, 
								h.EndDate as EndDate,
								h.EndDate as ordscale, 
								OperatorGroupId,
								h.HoursWorked * Sum(Distinct(SiteTarget)) As SiteTarget,
								h.HoursWorked *  Sum(Distinct(IndustryTarget)) As IndustryTarget,
								Sum(Actual) as Actual ,UOM ,
								'3' as TimePeriod
							From @TempTable t
								Join @TableHours h on h.OperatorId = t.OperatorId and h.EndDate = dateadd(wk,DatePart(wk,t.EndDate)-1,convert(char(4),datepart(year,t.enddate)) + '-01-01')
--DatePart(wk,t.EndDate)
							Group by t.OperatorId,Operator,OperatorGroupId,OperatorGroup,h.EndDate,Uom,h.HoursWorked
							ORDER BY ordscale		
				
					-- Select data by this Scale
				End
	End
Else
	Begin
			--Select  '3 months < x' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
				Insert Into @TableHours (OperatorId,EndDate,HoursWorked)
					Select OperatorId,
							DatePart(M,EndDate) as EndDate,
							Count(Distinct(EndDate)) as HoursWorked
					From @TempTable
					Group by OperatorId,
							DatePart(M,EndDate)

					SELECT		t.OperatorId, 
								t.Operator,
								t.OperatorGroup,
								--InstructionTypeId,
								h.EndDate as EndDate,
								h.EndDate as ordscale,
								OperatorGroupId,
								h.HoursWorked * Sum(Distinct(SiteTarget)) As SiteTarget,
								h.HoursWorked * Sum(Distinct(IndustryTarget)) As IndustryTarget,
								Sum(Actual) as Actual ,
								UOM ,
								'4' as TimePeriod
							From @TempTable t
								Join @TableHours h on h.OperatorId = t.OperatorId and h.EndDate = DatePart(wk,t.EndDate)
							Group by t.OperatorId,Operator,OperatorGroupId,OperatorGroup,h.EndDate,Uom,h.HoursWorked
							ORDER BY ordscale
	End
End
end

