﻿ 
/*
/// <summary>
///   Procedure Name : p_Report_Shipment_Order_Summary
///   Filename       : p_Report_Shipment_Order_Summary.sql
///   Create By      : Karen
///   Date Created   : February 2014
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
CREATE procedure p_Report_Shipment_Order_Summary
(
@FromDate          datetime,
@ToDate            datetime,
@PrincipalId	   int,
@ExternalCompanyId int
)

as
begin
  set nocount on;
  
  
  if @PrincipalId = '-1'
     set @PrincipalId = null
         
  if @ExternalCompanyId = '-1'
     set @ExternalCompanyId = null  
     
  declare @ExternalCompany	   nvarchar(255),
		  @ExternalCompanyCode nvarchar(30) 		  
    
	if @ExternalCompanyId is not null
	begin
		set @ExternalCompanyCode = (select ec.ExternalCompanyCode 
									  from ExternalCompany ec 
									 where ec.ExternalCompanyId = @ExternalCompanyId)
		set @ExternalCompany = (select ec.ExternalCompany 
								  from ExternalCompany ec 
								 where ec.ExternalCompanyId = @ExternalCompanyId)
	end  
	else
		set @ExternalCompanyCode = 'ALL'    
	
  declare @TableSumm as table
  (
   TotalOrders		int,
   TotalQty			float,
   TotalOrderLines	int,
   TotalCube		float,
   TotalGrossWeight float,
   TotalNetWeight	float
  )
    
  
  declare @TableDetails as Table
  (
  OrderNumber          nvarchar(30),
  OrderDate            datetime,
  ExpectedDeliveryDate datetime,
  ProductCode          nvarchar(30),
  ProductDesc          nvarchar(255),
  StorageUnitId		   int,
  SKUCode              nvarchar(50),
  SKU                  nvarchar(50),
  Batch                nvarchar(50),
  OrderQty             numeric(13,6),
  IssuedQty          numeric(13,6),
  OutstandingBal       numeric(13,6),
  DeliveryNote		   nvarchar(30),
  ExternalCompanyId	   int,
  ExternalCompanyCode  nvarchar(30),
  ExternalCompany	   nvarchar(255),
  DeliveryDate		   datetime,
  ReceiptId			   int,
  Status			   nvarchar(50) ,
  ReceiptLineId		   int,
  Weight			   float 
  )
  
    
	  insert @TableDetails
			(OrderNumber,
			 OrderDate,
			 ExpectedDeliveryDate,
			 ProductCode,
			 ProductDesc,
			 StorageUnitId,
			 SKUCode,
			 SKU,
			 Batch,
			 OrderQty,
			 IssuedQty,
			 OutstandingBal,
			 DeliveryNote,
			 DeliveryDate,
			 ReceiptId,
			 Status,
			 ReceiptLineId,
			 Weight
			 )
	  select id.OrderNumber,
			 id.CreateDate,
			 id.DeliveryDate,
			 p.ProductCode,
			 p.Product,
			 su.StorageUnitId,
			 sku.SKUCode,
			 sku.SKU,
			 b.Batch,
			 rl.RequiredQuantity,
			 rl.AcceptedQuantity,
			 rl.RequiredQuantity - isnull(rl.AcceptedQuantity,0),
			 r.DeliveryNoteNumber ,
			 r.DeliveryDate,
			 r.ReceiptId,
			 s.Status ,
			 rl.ReceiptLineId ,
			 rl.AcceptedWeight     
		from InboundDocument      id (nolock)
		 join Receipt			   r (nolock) on id.InboundDocumentId = r.InboundDocumentId
		 join ReceiptLine         rl (nolock) on r.ReceiptId = rl.ReceiptId		 
		 join status			   s (nolock) on r.StatusId = s.StatusId
		 join StorageUnitBatch   sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
		 join Batch                b (nolock) on sub.BatchId = b.BatchId
		 join StorageUnit         su (nolock) on sub.StorageUnitId = su.StorageUnitId
		 join Product              p (nolock) on su.ProductId = p.ProductId
		 join SKU                sku (nolock) on su.SKUId = sku.SKUId
	   where id.CreateDate between @FromDate and @ToDate
    

      insert @TableSumm
			(TotalOrders,
			 TotalQty,
			 TotalOrderLines,
			 TotalCube,
			 TotalGrossWeight,
			 TotalNetWeight
			 )
	  select count(distinct OrderNumber),
			 SUM(OrderQty) ,
			 COUNT(ReceiptLineId),
			 null,
			 SUM(Weight),
			 null
		 from @TableDetails	 
		 
	Select TotalOrders,
			 TotalQty,
			 TotalOrderLines,
			 TotalCube,
			 TotalGrossWeight,
			 TotalNetWeight,
			 @ExternalCompany as ExternalCompany,
			 @ExternalCompanyCode as ExternalCompanyCode,
			 @FromDate as FromDate,
			 @ToDate as ToDate
	  from @TableSumm
     

end
