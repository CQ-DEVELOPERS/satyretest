﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Product_Search
  ///   Filename       : p_Static_Product_Search.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 19 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@ProductCode> 
  ///       <@Product>	 
  ///       <@Barcode>	 
  /// </param>
  /// <returns>
  ///        <ProductId>
  ///        <StatusId> 
  ///        <Status> 
  ///        <ProductCode> 
  ///        <Product> 
  ///        <Barcode> 
  ///        <MinimumQuantity> 
  ///        <ReorderQuantity> 
  ///        <MaximumQuantity> 
  ///        <CuringPeriodDays>        
  ///        <ShelfLifeDays> 
  ///        <QualityAssuranceIndicator>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Product_Search
(
 @ProductCode           nvarchar(30),
 @Product	            nvarchar(50),
 @SKUCode               nvarchar(10),
 @SKU		            nvarchar(50),
 @Barcode	            nvarchar(50),
 @PrincipalId         int = null
)

as
begin
	 set nocount on;

if @PrincipalId in (0,-1)
  set @PrincipalId = null

SELECT	distinct P.ProductId, 
			P.StatusId, 
			S.Status, 
			P.ProductCode, 
			P.Product, 
			P.Barcode, 
			P.MinimumQuantity, 
			P.ReorderQuantity, 
			P.MaximumQuantity, 
			P.CuringPeriodDays,           
			P.ShelfLifeDays, 
			P.QualityAssuranceIndicator,
			P.ProductType,
			P.OverReceipt,
			P.HostId,
			p.RetentionSamples,
			p.Samples,
			p.AssaySamples,
			p.ParentProductCode,
			p.Category,
			isnull(p.DangerousGoodsId, -1) as DangerousGoodsId,
			isnull(dg.DangerousGoods, '') as DangerousGoods,
			isnull(p.PrincipalId,-1) as PrincipalId,
			isnull(pri.Principal, '') as  Principal
  FROM Product      P (nolock)
  left
  join StorageUnit su (nolock) on p.ProductId = su.ProductId
  left
  join SKU        sku (nolock) on su.SKUId = sku.SKUId
  --left
  --join Pack        pk (nolock) on su.StorageUnitId = pk.StorageUnitId
  JOIN Status       S ON P.StatusId = S.StatusId
  LEFT
  JOIN DangerousGoods AS dg ON dg.DangerousGoodsId = p.DangerousGoodsId
  left join Principal pri on pri.PrincipalId = p.PrincipalId
	where (p.ProductCode like @ProductCode + '%' or @ProductCode is null)
	and (p.Product like @Product + '%' or @Product is null)
	and (sku.SKUCode like '%' + @SKUCode + '%' or @SKUCode is null)
	and (sku.SKU  like '%' + @SKU + '%' or @SKU  is null)
	And (isnull(p.Barcode,'')  like @Barcode + '%'
	or   @Barcode is null)
	--or   isnull(pk.Barcode,'') like @Barcode + '%'
   
   and (p.ProductType != 'FPC' or p.ProductType is null)
   and (p.PrincipalId = @PrincipalId or @PrincipalId is null)
   
end
 
