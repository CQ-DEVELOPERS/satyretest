﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_BOMProduct_Search
  ///   Filename       : p_Static_BOMProduct_Search.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@ProductCode> 
  ///       <@Product>	 
  ///       <@Barcode>	 
  /// </param>
  /// <returns>
  ///        <ProductId>
  ///        <StatusId> 
  ///        <Status> 
  ///        <ProductCode> 
  ///        <Product> 
  ///        <Barcode> 
  ///        <MinimumQuantity> 
  ///        <ReorderQuantity> 
  ///        <MaximumQuantity> 
  ///        <CuringPeriodDays>        
  ///        <ShelfLifeDays> 
  ///        <QualityAssuranceIndicator>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_BOMProduct_Search
(
 @BOMLineId int
)

as
begin
	 set nocount on;

SELECT		P.ProductId, 
			P.StatusId, 
			S.Status, 
			P.ProductCode, 
			P.Product, 
			P.Barcode, 
			P.MinimumQuantity, 
			P.ReorderQuantity, 
			P.MaximumQuantity, 
			P.CuringPeriodDays,           
			P.ShelfLifeDays, 
			P.QualityAssuranceIndicator,
			P.ProductType,
			P.OverReceipt,
			P.HostId,
			p.RetentionSamples,
			su.StorageUnitId,
			bp.BOMLineId,
			bp.LineNumber,
			bp.Quantity
FROM		BOMProduct bp
			Inner join StorageUnit SU ON bp.StorageUnitId = SU.StorageUnitId 
			Inner join Product AS P ON SU.ProductId = P.ProductId
            Inner join Status AS S ON P.StatusId = S.StatusId
WHERE		bp.BOMLineId = @BOMLineId
 
end
