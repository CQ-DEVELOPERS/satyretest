﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Release_Linked
  ///   Filename       : p_Wave_Planning_Release_Linked.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jun 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Release_Linked
(
 @WaveId int
)

as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int
  )
  
  declare @Error              int,
          @Errormsg           varchar(500) = 'Error executing p_Wave_Planning_Release_Linked',
          @GetDate            datetime,
          @Transaction        bit = 0,
          @OutboundShipmentId int,
          @IssueId            int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId)
  select distinct 
         osi.OutboundShipmentId,
         case when osi.OutboundShipmentId is null
              then i.IssueId
              else null
              end
    from Issue                   i (nolock)
    join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
    join OutboundDocumentType  odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                           and odt.OutboundDocumentTypeCode != 'WAV'
    left
    join OutboundShipmentIssue osi on i.IssueId = osi.IssueId
   where i.WaveId = @WaveId
     and i.StatusId = dbo.ufn_StatusId('IS','W')
  
  while exists(select top 1 1 from @TableResult)
  begin
    select top 1
           @OutboundShipmentId = OutboundShipmentId,
           @IssueId            = IssueId
      from @TableResult
    
    delete @TableResult
     where OutboundShipmentId = @OutboundShipmentId
        or IssueId            = @IssueId
    
    exec @Error = p_Outbound_Auto_Release
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId,
     @AutoRelease        = 1
    
    --if @Error <> 0
    --  goto error
    
    if exists (select top 1 1 
                 from Issue                   i
                 left
                 join OutboundShipmentIssue osi on i.IssueId = osi.IssueId
                 join Status                  s on i.StatusId = s.StatusId
                                               and s.StatusCode in ('W')
                where isnull(osi.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, -1)
                  and i.IssueId = isnull(@IssueId, i.IssueId))
    begin
      set @Error = -1
      goto error
    end
    
    if exists(select top 1 1
                from OutboundDocument od
                join OutboundDocumentType odt on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                             and odt.OutboundDocumentTypeCode = 'WAV'
                join Issue                  i on od.OutboundDocumentId = i.OutboundDocumentId
               where i.WaveId = @WaveId)
    begin
      update j
         set StatusId = dbo.ufn_StatusId('IS','PS')
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolocK) on j.StatusId = s.StatusId
                                              and s.StatusCode = 'RL'
        join AreaLocation          al (nolock) on i.PickLocationId = al.LocationId
        join Area                   a (nolock) on al.AreaId = a.AreaId
                                              and a.Area like '%Wave%'
       where ili.OutboundShipmentId = @OutboundShipmentId
          or ili.IssueId            = @IssueId
    end
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (900000,-1,-1, @Errormsg);
        rollback transaction
      end
      
      return @Error
end
