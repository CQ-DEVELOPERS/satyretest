﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Principal_Search
  ///   Filename       : p_Report_Principal_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Mar 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Principal_Search
(
 @PrincipalCode                   nvarchar(30),
 @Principal                       nvarchar(255)
 )

as
begin
  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
  
  set @trancount = @@trancount;
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_Report_Principal_Search;
    
		  select PrincipalId,
           PrincipalCode,
           Principal
      from Principal (nolock)
     where isnull(Principal,'%')  like '%' + isnull(@Principal, isnull(Principal,'%')) + '%'
       and isnull(PrincipalCode,'%')  like '%' + isnull(@PrincipalCode, isnull(PrincipalCode,'%')) + '%'
	   
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_Report_Principal_Search;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
