﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeAuthorise_Search
  ///   Filename       : p_StockTakeAuthorise_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:47
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StockTakeAuthorise table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StockTakeAuthorise.InstructionId,
  ///   StockTakeAuthorise.StatusCode,
  ///   StockTakeAuthorise.OperatorId,
  ///   StockTakeAuthorise.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeAuthorise_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         StockTakeAuthorise.InstructionId
        ,StockTakeAuthorise.StatusCode
        ,StockTakeAuthorise.OperatorId
        ,StockTakeAuthorise.InsertDate
    from StockTakeAuthorise
  
end
