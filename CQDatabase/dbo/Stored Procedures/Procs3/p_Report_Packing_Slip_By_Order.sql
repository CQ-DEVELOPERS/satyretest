﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Packing_Slip_By_Order
  ///   Filename       : p_Report_Packing_Slip_By_Order.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Packing_Slip_By_Order
(
 @OutboundShipmentId int,
 @IssueId            int
)

as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int,
   IssueLineId        int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(255),
   RouteId            int,
   Route              nvarchar(50),
   JobId              int,
   InstructionId      int,
   InstructionRefId   int,
   Pallet             nvarchar(10),
   StorageUnitBatchId int,
   ProductId		  int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   EndDate            datetime,
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   QuantityOnHand     float,
   OperatorId         int,
   Operator           nvarchar(50),
   DropSequence       int,
   PickLocationId     int,
   PickLocation       nvarchar(15),
   PickArea           nvarchar(50),
   StoreLocationId    int,
   StoreLocation      nvarchar(15),
   StoreArea          nvarchar(50),
   CheckQuantity	  float,
   ReferenceNumber    nvarchar(30),
   Address			  nvarchar(255),
   Customer			  nvarchar(255),
   CustomerCode		  nvarchar(30),
   CheckedById		  int,
   CheckedBy		  nvarchar(50),
   ContactPerson	  nvarchar(255),
   Comments			  nvarchar(255),
   DeliveryAdd1       nvarchar(255),
   DeliveryAdd2		  nvarchar(255),
   DeliveryAdd3		  nvarchar(255),
   DeliveryAdd4		  nvarchar(255),
   ExpiryDate		  datetime,
   BarcodeReference   nvarchar(50),
   UnitBarcode		  nvarchar(50),
   ExpiryOrBarcode	  nvarchar(50),
   EOBHeading		  nvarchar(50),
   ActualChecked	  datetime,
   ActualCompleted	  datetime,   
   ActChckdDate		  varchar(10),
   ActChckdTime		  varchar(10),
   ActComplDate		  varchar(10),
   ActComplTime		  varchar(10),
   PickInstrCreatorId int,
   PickInstrCreator	  nvarchar(50),
   Height			  int,
   Length			  int,
   Width			  int,
   Weight			  int,
   StorageUnitId	  int,
   Cube				  float,
   CustomerOrderNumber	nvarchar(100),
   ExternalOrderNumber	nvarchar(100),
   CreateDate			datetime,
   DeliveryDate			datetime			
  )
  
  declare @TableTemp as table
  (
   JobId              int,
   InstructionId      int,
   InstructionRefId   int,
   Pallet             nvarchar(10),
   StorageUnitBatchId int,
   EndDate            datetime,
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   OperatorId         int,
   PickLocationId     int,
   StoreLocationId    int,
   CheckedById		  int,
   CheckQuantity	  float
  )
  
  declare @WarehouseId		int,
          @ReferenceNumber	nvarchar(30),
          @OrderNumber		nvarchar(30),
          @Pallet			nvarchar(10),
          @ExternalCompany	nvarchar(255),
          @BarcodeReference	nvarchar(50),
          @Customer			nvarchar(255),
		  @CustomerCode		nvarchar(30)
		  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
    
    
  if @OutboundShipmentId is null
  begin
    select @WarehouseId = WarehouseId
      from Issue (nolock)
     where IssueId = @IssueId
  
  insert @TableTemp
        (JobId,
         InstructionId,
         InstructionRefId,
         StorageUnitBatchId,
         EndDate,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         Pallet,
         CheckedById,
         CheckQuantity)
  select distinct ins.JobId,
         ins.InstructionId,
         isnull(ins.InstructionRefId, ins.InstructionId),
         ins.StorageUnitBatchId,
         ins.EndDate,
         ins.Quantity,
         isnull(ins.ConfirmedQuantity,0),
         ins.Quantity - isnull(ins.ConfirmedQuantity,0),
         ins.OperatorId,
         ins.PickLocationId,
         ins.StoreLocationId,
         convert(nvarchar(10), j.DropSequence) + ' of ' + convert(nvarchar(10), j.Pallets),
         j.CheckedBy,
         ins.CheckQuantity
    from Instruction ins
    join Job              j (nolock) on ins.JobId              = j.JobId 
    join IssueLineInstruction ili (nolock) on isnull(ins.InstructionRefId, ins.InstructionId) = ili.InstructionId
   where  ili.IssueId              = @IssueId
  end
  else
  begin
   select @WarehouseId = WarehouseId
      from OutboundShipment (nolock)
     where OutboundShipmentId = @OutboundShipmentId
  insert @TableTemp
        (JobId,
         InstructionId,
         InstructionRefId,
         StorageUnitBatchId,
         EndDate,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         Pallet,
         CheckedById,
         CheckQuantity)
  select distinct ins.JobId,
         ins.InstructionId,
         isnull(ins.InstructionRefId, ins.InstructionId),
         ins.StorageUnitBatchId,
         ins.EndDate,
         ins.Quantity,
         isnull(ins.ConfirmedQuantity,0),
         ins.Quantity - isnull(ins.ConfirmedQuantity,0),
         ins.OperatorId,
         ins.PickLocationId,
         ins.StoreLocationId,
         convert(nvarchar(10), j.DropSequence) + ' of ' + convert(nvarchar(10), j.Pallets),
         j.CheckedBy,
         ins.CheckQuantity
    from IssueLineInstruction ili (nolock)
      join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
      join Job                    j (nolocK) on ins.JobId           = j.JobId
   where ili.OutboundShipmentId = @OutboundShipmentId
  end
  
  insert @TableResult
        (JobId,
         InstructionRefId,
         StorageUnitBatchId,
         EndDate,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         Pallet,
         CheckedById,
         CheckQuantity,
         Customer)
  select JobId,
         InstructionRefId,
         StorageUnitBatchId,
         max(EndDate),
         sum(Quantity),
         sum(ConfirmedQuantity),
         sum(ShortQuantity),
         OperatorId,
         PickLocationId,
         max(StoreLocationId),
         Pallet,
         CheckedById,
         CheckQuantity,
         null
    from @TableTemp
   group by JobId,
            InstructionRefId,
            StorageUnitBatchId,
            OperatorId,
            PickLocationId,
            Pallet,
            CheckedById,
            CheckQuantity
            
  update tr
     set OutboundShipmentId = ili.OutboundShipmentId,
         IssueId            = ili.IssueId,
         IssueLineId        = ili.IssueLineId,
         OutboundDocumentId = ili.OutboundDocumentId
    from @TableResult          tr
    join IssueLineInstruction ili (nolock) on tr.InstructionRefId = ili.InstructionId
  
  update tr
     set OrderNumber        = od.OrderNumber,
         ExternalCompanyId  = od.ExternalCompanyId,
         PickInstrCreatorid = od.OperatorId,
         PickInstrCreator	= o.operator,
         CustomerOrderNumber = od.CustomerOrderNumber,
         ExternalOrderNumber = od.ExternalOrderNumber,
         CreateDate		     = od.CreateDate
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
    join Operator		   o (nolock) on isnull(od.OperatorId,o.OperatorId) = o.OperatorId
    
    
    select @OrderNumber       = od.OrderNumber
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set RouteId = os.RouteId
    from @TableResult     tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
  
  update tr
     set RouteId = i.RouteId,
		 DeliveryDate = i.DeliveryDate
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
   where tr.RouteId is null
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
    
  select @ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set CheckedBy = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.CheckedById = o.OperatorId
  
  update tr
     set ProductId     = p.ProductId,
		 ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         ExpiryDate	   = b.ExpiryDate,
         StorageUnitId = su.StorageUnitId
    from @TableResult    tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
  
  update tr
     set StoreLocation = l.Location,
         StoreArea     = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId       = al.LocationId
    join Area          a (nolock) on al.AreaId          = a.AreaId
  
  update tr
     set PickLocation = l.Location,
         PickArea     = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId       = al.LocationId
    join Area          a (nolock) on al.AreaId          = a.AreaId
  
  --select @WarehouseId = WarehouseId
  --  from Instruction (nolock)
  -- where JobId = @JobId
  
  if dbo.ufn_Configuration(71, @warehouseId) = 0
    update @TableResult
       set ExternalCompany = null
       
  update tr
     set ReferenceNumber = jo.ReferenceNumber,
         CheckedById     = jo.CheckedBy,
         ActualChecked   = jo.CheckedDate
    from @TableResult    tr
    join Job jo (nolock) on tr.JobId = jo.JobId
    
  update tr
     set ActualCompleted = (select max(i.EndDate) from Instruction i 
							join @TableResult tr2 on  i.JobId = tr2.JobId)
    from @TableResult    tr
   
 update @TableResult   
    set ReferenceNumber = (Select 'J:' + isNull(Cast(tr.jobid as nvarchar(50)),''))
    from @TableResult    tr
  where ReferenceNumber is null
 
 update @TableResult
    set BarcodeReference = dbo.ConvertTo128(ReferenceNumber)
 
  select @BarcodeReference = dbo.ConvertTo128(ReferenceNumber)  
    from @TableResult
    
 update tr
     set tr.Address = ish.Address,
		       tr.Customer = ic.CustomerName,
		       tr.CustomerCode = ish.CustomerCode,
		       tr.ContactPerson = ish.Additional1,
		       tr.Comments = ish.Additional3,
		       tr.DeliveryAdd1 = ish.Additional4,
		       tr.DeliveryAdd2 = ish.Additional5,
		       tr.DeliveryAdd3 = ish.Additional6,
		       tr.DeliveryAdd4 = ish.Additional7
    from @TableResult             tr
    join InterfaceImportSOHeader ish (nolock) on tr.OrderNumber   = ish.OrderNumber
    join InterfaceCustomer        ic (nolock) on ish.CustomerCode = ic.CustomerCode
    
    update tr
       set     tr.DeliveryAdd1 = a.Street,
		       tr.DeliveryAdd2 = a.Suburb,
		       tr.DeliveryAdd3 = a.Town,
		       tr.DeliveryAdd4 = a.Code
      from @TableResult             tr
      join Address a  (nolock) on tr.ExternalCompanyId   = a.ExternalCompanyId
     where tr.DeliveryAdd1 is null
       and tr.DeliveryAdd2 is null

  
	update tr
     set  tr.Customer = ec.ExternalCompany,
		  tr.CustomerCode = ec.ExternalCompanyCode
    from @TableResult             tr
    join ExternalCompany ec(nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId  
    where tr.Customer is null  
    
   update tr
     set  tr.Customer = (select max(ic.CustomerName) from InterfaceCustomer ic where tr.CustomerCode = ic.CustomerCode)
    from @TableResult             tr
    where tr.Customer is null
    
     
   update tr
     set  tr.Customer = null
    from @TableResult             tr
    where tr.Customer = tr.CustomerCode
     
      select @Customer     = Customer,
		   @CustomerCode = CustomerCode
      from @TableResult    tr
     where CustomerCode is not null
  
  update  tr
     set  tr.CheckedBy = op.Operator
     from @TableResult tr
     join Operator     op (nolock) on tr.CheckedById = op.OperatorId
     
  update  tr
     set  tr.UnitBarcode = isnull(pr.barcode, p.Barcode)
     from @TableResult tr
     join StorageUnit     su (nolock) on su.ProductId = tr.ProductId
     join Pack             p (nolock) on p.StorageUnitId = su.StorageUnitId
     join product         pr (nolock) on pr.productid = su.ProductId
    where p.PackTypeId = 9
    
  update  tr
     set  tr.ActComplDate = CONVERT(varchar(10), ActualCompleted, 103),
          tr.ActComplTime = CONVERT(varchar(10), ActualCompleted, 108),
          tr.ActChckdDate = CONVERT(varchar(10), ActualChecked, 103),
          tr.ActChckdTime = CONVERT(varchar(10), ActualChecked, 108)
    from @TableResult tr
    
  update  tr
     set  tr.Height = p.Height,
          tr.Length = p.Length,
          tr.Width  = p.Width,
          tr.Weight = p.Weight
    from @TableResult tr
    join Pack p on p.StorageUnitId = tr.StorageUnitId 
    where p.PackTypeId = 8  
    
  update  tr
     set  tr.Cube = tr.Height * tr.Width * tr.Length
    from @TableResult tr
    
  --select @WarehouseId = WarehouseId
  --  from Instruction (nolock)
  -- where JobId = @JobId
  
  if dbo.ufn_Configuration(71, @warehouseId) = 0
    update @TableResult
       set ExternalCompany = null 
       
  -- Check if we must print the unit barcode or the expiry date based on config 295  
    
  if dbo.ufn_Configuration(295, @warehouseId) = 1
    update @TableResult
       set ExpiryOrBarcode = UnitBarcode,
           EOBHeading = 'Unit Barcode'
  else
    update @TableResult
       set ExpiryOrBarcode = CONVERT(varchar(50), getdate(), 111),
           EOBHeading = 'Expiry Date'
           
  if dbo.ufn_Configuration(361, @warehouseId) = 0
	update @TableResult
       set PickInstrCreator = 'Hide'
  
  select distinct OutboundShipmentId,
         Route,
         isnull(OrderNumber,@ordernumber) as 'OrderNumber',
         isnull(DropSequence,ROW_NUMBER() OVER (Order By EndDate)) as'DropSequence',
         JobId,
         isnull(Pallet,0) as Pallet,
         ExternalCompanyid,
         isnull(ExternalCompany,@ExternalCompany) as 'ExternalCompany',
         ProductCode,
         Product,
         EndDate,
         SKUCode,
         Batch,
         PickArea as 'PickArea',
         PickLocation,
         null as 'StoreArea',
         StoreLocation as 'StoreLocation',
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         null as 'QuantityOnHand',
         isnull(Operator,'Unknown') as 'Operator',
         CheckQuantity as  'CheckQuantity',
         isnull(ReferenceNumber,@ReferenceNumber) as 'ReferenceNumber',
         --Route,
         Address,
         isnull(Customer,@Customer) as 'Customer',
         isnull(CustomerCode,@CustomerCode) as 'CustomerCode',
         CheckedBy,
         ContactPerson,
         Comments,
         DeliveryAdd1,
         DeliveryAdd2,
         DeliveryAdd3,
         DeliveryAdd4,
         isnull(BarcodeReference,@BarcodeReference) as 'BarcodeReference',
         ExpiryOrBarcode,
         EOBHeading, 
         isnull(ActComplDate,'dd/mm/yyyy') as ActComplDate,
         ISNULL(ActComplTime,'hh:mm') as ActComplTime,
         isnull(ActChckdDate,'dd/mm/yyyy') as ActChckdDate,
         ISNULL(ActChckdTime,'hh:mm') as ActChckdTime,
         isnull(Operator, 'Picker') as PickerSignature,
         isnull(CheckedBy, 'Pick Checker') as CheckerSignature,
         PickInstrCreator,
         Height,
         Length,
         Width,
         Weight,
         Cube,
         CustomerOrderNumber,
         ExternalOrderNumber,
         CreateDate,
         DeliveryDate,
         (select Indicator from Configuration where ConfigurationId = 426 and WarehouseId = @WarehouseId) as 'PrtDIMS'        
    from @TableResult
   --where JobId = @JobId
   order by Product, EndDate 
end
 
