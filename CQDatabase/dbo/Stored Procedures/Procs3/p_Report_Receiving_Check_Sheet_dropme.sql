﻿/*
  /// <summary>
  ///   Procedure Name : p_Report_Receiving_Check_Sheet_dropme
  ///   Filename       : p_Report_Receiving_Check_Sheet_dropme.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jun 2007
  /// </summary>
  /// <remarks>
  ///  
  /// </remarks>
  /// <param>
  ///  
  /// </param>
  /// <returns>
  ///  
  /// </returns>
  /// <newpara>
  ///   Modified by    :
  ///   Modified Date  :
  ///   Details        :
  /// </newpara>
*/
CREATE procedure p_Report_Receiving_Check_Sheet_dropme
(
@ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
@InboundShipmentId int = null,
@ReceiptId         int = null,
@ReceiptLineId     int = null
)

as
begin

  set nocount on;
 
  declare @TableHeader as Table
  (
   InboundShipmentId  int,
   WarehouseId        int,
   ReceiptId          int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   Supplier           nvarchar(255),
   SupplierCode       nvarchar(30),
   DeliveryNoteNumber nvarchar(30),
   BOE				  nvarchar(255)
  );
 
  declare @TableDetails as Table
  (
   ReceiptId          int,
   ReceiptLineId      int,
   LineNumber         int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   --ExpectedQuantity   nvarchar(30),
   ExpectedQuantity   float,
   ReceivedQuantity   float,
   RejectQuantity     float,
   ProductCode        nvarchar(30),
   Product            nvarchar(255),
   ProductCategoryId  int,
   ProductCategory	  nvarchar(30),
   ProductType  	  nvarchar(30),
   SKUCode            nvarchar(50),
   SKU                nvarchar(50),
   PalletQuantity     float,
   PalletCount        int,
   LayerQuantity      float,
   LayerQtyWeight	  nvarchar(30),
   LayerCount         int,
   LayerWeight		  float,
   CasesQuantity      float,
   CasesCount         int,
   CasesQtyWeight	  nvarchar(30),
   CaseWeight		  float,
   UnitsQuantity      float,
   UnitsCount         int,
   UnitPrice          numeric(13,2),
   SampleQuantity     float default 0,
   Boxes              nvarchar(50),
   BoxesQuantity      nvarchar(10),
   SupplierBarcode    nvarchar(50),
   XDocStock          numeric(13,6),
   DIMWeight		  int,
   DIMWidth			  int,
   DIMHeight		  int,
   DIMLength		  int,
   SerialTracked      bit,
   LotAttributeRule	  nvarchar(100),
   BatchRequired      nvarchar(1),
   SerialNumbers	  nvarchar(1),
   BOELineNumber	  nvarchar(50),
   BOEBarcode		  nvarchar(50),
   Batch			  nvarchar(50),
   ExpiryDate		  datetime,
   Controlled		  nvarchar(1)
  );
 
  declare @TableSOH as table
  (
   WarehouseId       int,
   StorageUnitId     int,
   Quantity          numeric(13,6),
   AllocatedQuantity numeric(13,6)
  )
 
 
  declare @StatusId                       int,
          @WarehouseId              int,
          @UnitInboundSequence      smallint,
          @PalletInboundSequence    smallint,
          @LayerInboundSequence     smallint,
          @CaseInboundSequence      smallint
                   
 
  select @UnitInboundSequence = InboundSequence
  from PackType pt (nolock)
  where pt.PackType = 'Unit'
 
  select @PalletInboundSequence = InboundSequence
  from PackType pt (nolock)
  where pt.PackType = 'Pallet'
 
  select @LayerInboundSequence = InboundSequence
  from PackType pt (nolock)
  where pt.PackType = 'Layer' 
  
  select @CaseInboundSequence = InboundSequence
  from PackType pt (nolock)
  where pt.PackType = 'Case'
 
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
 
  if @ReceiptId = -1
    set @ReceiptId = null
  if @ReceiptLineId = -1
    set @ReceiptLineId = null
 
  if @InboundShipmentId is not null
  begin
    insert @TableHeader
          (InboundShipmentId,
           WarehouseId,
           ReceiptId,
           OrderNumber,
           ExternalCompanyId,
           DeliveryNoteNumber,
           BOE)
    select isr.InboundShipmentId,
           id.WarehouseId,
           r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId,
           r.DeliveryNoteNumber,
           r.BOE
      from InboundShipmentReceipt isr (nolock)
      join Receipt                  r (nolock) on isr.ReceiptId = r.ReceiptId
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
     where isr.InboundShipmentId = isnull(@InboundShipmentId, isr.InboundShipmentId)
 
    select @WarehouseId = WarehouseId
      from InboundShipment (nolock)
     where InboundShipmentId = @InboundShipmentId
  end
  else if @ReceiptId is not null
  begin
    insert @TableHeader
          (InboundShipmentId,
           ReceiptId,
           OrderNumber,
           ExternalCompanyId,
           BOE)
    select null,
           r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId,
           r.BOE
      from Receipt                  r (nolock)
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
     where r.ReceiptId = isnull(@ReceiptId, r.ReceiptId)
 
    select @WarehouseId = WarehouseId
      from Receipt (nolock)
     where ReceiptId = @ReceiptId
  end
 
  update th
     set Supplier     = ec.ExternalCompany,
         SupplierCode = ec.ExternalCompanyCode
    from @TableHeader th
    join ExternalCompany ec on th.ExternalCompanyId = ec.ExternalCompanyId
 
  insert @TableDetails
        (ReceiptId,
         ReceiptLineId,
         LineNumber,
         StorageUnitBatchId,
         ExpectedQuantity,
         ReceivedQuantity,
         RejectQuantity,
         BatchRequired,
         SerialNumbers,
         BOELineNumber,
         Controlled)
  select rl.ReceiptId,
         rl.ReceiptLineId,
         il.LineNumber,
         rl.StorageUnitBatchId,
         rl.RequiredQuantity,
         rl.ReceivedQuantity,
         rl.RejectQuantity,
         'N',
         'N',
         il.BOELineNumber,
         'N'
    from @TableHeader th
    join ReceiptLine  rl (nolock) on th.ReceiptId     = rl.ReceiptId
    join Status        s (nolock) on rl.StatusId  = s.StatusId
    join InboundLine  il (nolock) on rl.InboundLineId = il.InboundLineId
   where rl.ReceiptLineId = isnull(@ReceiptLineId, rl.ReceiptLineId)

 
  if dbo.ufn_Configuration(122, @WarehouseId) = 0
  begin
    update @TableDetails
       set ExpectedQuantity = 0
  end
 
  update td
     set ProductCode   = p.ProductCode,
         Product       = p.Product + ' - ' + sku.SKUCode,
         SKUCode       = sku.SKUCode,
         SKU               = sku.SKU,
         StorageUnitId = su.StorageUnitId,
         SerialTracked = isnull(su.SerialTracked,0),
         LotAttributeRule = su.LotAttributeRule,
         BOEBarcode		= b.Batch,
         ExpiryDate	    = b.ExpiryDate,
         Batch		    = b. batch
    from @TableDetails    td
    join StorageUnitBatch sub (nolock) on td.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch				b (nolock) on sub.BatchId			= b.BatchId
  
    insert @TableSOH
          (WarehouseId,
           StorageUnitId,
           Quantity)
    select Iss.WarehouseId,
           sub.StorageUnitId,
           SUM(il.Quantity)
      from Issue                       iss (nolock)
      join Status                        s (nolock) on iss.StatusId          = s.StatusId
      join IssueLine                    il (nolock) on iss.IssueId           = il.IssueId
      join StorageUnitBatch            sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
     where iss.WarehouseId    = @WarehouseId
       and sub.StorageUnitId in (select StorageUnitId from @TableDetails) -- You'll need to get all the Receipt's StorageUnit's here.
       and il.Quantity        > 0
       and s.StatusCode       = 'OH'
    group by Iss.WarehouseId,
             sub.StorageUnitId
   
   update soh
      set AllocatedQuantity = va.ActualQuantity + va.AllocatedQuantity - va.ReservedQuantity
     from @TableSOH soh
     join viewAvailable va on va.WarehouseId        = soh.WarehouseId
                          and va.StorageUnitId      = soh.StorageUnitId
                          and va.AreaType           = 'XDock'
   
   delete from @TableSOH
    where Quantity - isnull(AllocatedQuantity, 0) <= 0
   
   update t
      set XDocStock = tsoh.Quantity
     from @TableDetails t
     join @TableSOH  tsoh on t.StorageUnitId = tsoh.StorageUnitId
   
   update t
     set SupplierBarcode     = p.Barcode
    from @TableDetails t
    join Pack        p (nolock) on t.StorageUnitId      = p.StorageUnitId
    join PackType   pt (nolock) on p.PackTypeId         = pt.PackTypeId
   where pt.PackType = 'Supplier' 
     and warehouseid = @WarehouseId
  
  update td
     set PalletQuantity = pk.Quantity
    from @TableDetails td
    join Pack          pk (nolock) on td.StorageUnitId = pk.StorageUnitId
    join PackType      pt (nolock) on pk.PackTypeId    = pt.PackTypeId
   where pt.InboundSequence = @PalletInboundSequence
     and pt.PackType = 'Pallet'
     and warehouseid = @WarehouseId
 
  update td
     set LayerQuantity = pk.Quantity,
		 LayerWeight = pk.NettWeight
    from @TableDetails td
    join Pack          pk (nolock) on td.StorageUnitId = pk.StorageUnitId
    join PackType      pt (nolock) on pk.PackTypeId    = pt.PackTypeId
   where pt.InboundSequence = @LayerInboundSequence

   --update td
   --  set LayerQtyWeight = (convert(nvarchar,td.LayerQuantity) + '(' + convert(nvarchar,pk.NettWeight) + ')')
   -- from @TableDetails td
   -- join Pack          pk (nolock) on td.StorageUnitId = pk.StorageUnitId
   -- join PackType      pt (nolock) on pk.PackTypeId    = pt.PackTypeId
   --where pt.InboundSequence = @LayerInboundSequence
   
   
 
  update td
     set CasesQuantity = pk.Quantity,
		 CaseWeight = pk.NettWeight
    from @TableDetails td
    join Pack          pk (nolock) on td.StorageUnitId = pk.StorageUnitId
    join PackType      pt (nolock) on pk.PackTypeId    = pt.PackTypeId
   where pt.InboundSequence = @CaseInboundSequence
     and pt.PackType = 'Case'
     and warehouseid = @WarehouseId
     and isnull(LayerQuantity,0) <= 0 -- Changed - cannot have both
 
  update td
     set UnitsQuantity = pk.Quantity
    from @TableDetails td
    join Pack          pk (nolock) on td.StorageUnitId = pk.StorageUnitId
    join PackType      pt (nolock) on pk.PackTypeId    = pt.PackTypeId
   where pt.InboundSequence = @UnitInboundSequence
     and pt.PackType = 'Unit'
     and warehouseid = @WarehouseId
 
  update @TableDetails
     set PalletCount = ExpectedQuantity / PalletQuantity
 
  update @TableDetails
     set LayerCount = (ExpectedQuantity - isnull((PalletQuantity * PalletCount),0)) / LayerQuantity
 
  update @TableDetails
     set CasesCount = (ExpectedQuantity - isnull((PalletQuantity * PalletCount),0)) / CasesQuantity 
     
  update tr
     set LayerCount = CasesCount,
         LayerQuantity = CasesQuantity,
         LayerWeight = CaseWeight
    from @TableDetails     tr
    where (LayerCount is null
      and LayerQuantity is null)
       or (LayerCount = 0
      and LayerQuantity = 0)
      
  
     
   update tr
     set CasesCount = LayerCount,
         CasesQuantity = LayerQuantity,
         CaseWeight = LayerWeight
    from @TableDetails     tr
    where (CasesCount is null
      and CasesQuantity is null)
       or (CasesCount = 0
      and CasesQuantity = 0)
    
  update @TableDetails
     set UnitsCount = (ExpectedQuantity - (isnull((PalletQuantity * PalletCount),0) + isnull((LayerQuantity * LayerCount),0))) / UnitsQuantity
 
--  select @StatusId = dbo.ufn_StatusId('R','S')
-- 
--  update rl
--     set StatusId = @StatusId
--    from @TableDetails td
--    join ReceiptLine   rl (nolock) on td.ReceiptLineId = rl.ReceiptLineId
--    join Status         s (nolock) on rl.StatusId      = s.StatusId
--   where s.Type       = 'R'
--     and s.StatusCode = 'W'
-- 
--  update r
--     set StatusId = @StatusId
--    from @TableDetails td
--    join Receipt        r (nolock) on td.ReceiptId = r.ReceiptId
--    join Status         s (nolock) on r.StatusId   = s.StatusId
--   where s.Type       = 'R'
--     and s.StatusCode = 'W'
 
  update td
     set UnitPrice = Additional2
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
    join InterfaceImportPODetail d (nolock) on th.OrderNumber = d.ForeignKey
  --                                         and td.LineNumber  = d.LineNumber
 
  --update tr
  --   set Boxes = pt.PackType,
  --       BoxesQuantity = p.Quantity
  --  from @TableDetails     tr
  --  join Pack               p (nolock) on p.WarehouseId         = @WarehouseId
  --                                    and tr.StorageUnitId      = p.StorageUnitId
  --  join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId
  --where pt.InboundSequence > 1
  --  and p.Quantity         > 1 
   
  --update @TableDetails
  --   set ExpectedQuantity = ExpectedQuantity + ' ' + isnull(' /' +  convert(nvarchar(30), convert(float, ExpectedQuantity) / convert(float, BoxesQuantity)) + ' ' + Boxes +'(es)','')
  -- where convert(float, BoxesQuantity) >= 1
   
   update @TableDetails 
     set XDocStock = ExpectedQuantity
   where XDocStock > ExpectedQuantity 
 
   update td
     set BatchRequired = 'Y'
    from @TableDetails td
    where td.LotAttributeRule != 'None'
    
  update td
     set SerialNumbers = 'Y'
    from @TableDetails td
    where td.SerialTracked = 1
    
  update  tr
     set  tr.ProductCategoryId = su.ProductCategoryId,
		  tr.ProductCategory = pr.ProductCategory,
		  tr.ProductType = pr.ProductType
     from @TableDetails tr
     join StorageUnit     su (nolock) on su.StorageUnitId = tr.StorageUnitId
     join ProductCategory pr (nolock) on su.ProductCategoryId = pr.ProductCategoryId
     
  --update  tr
  --   set  tr.ExpiryDate = DATEADD(DD,b.ShelfLifeDays,GETDATE())
  --  from @TableDetails tr
  --  join StorageUnitBatch     sub (nolock) on sub.StorageUnitbatchId = tr.StorageUnitbatchId
  --  join Batch					 b (nolock) on b.BatchId = sub.BatchId
  -- where tr.ExpiryDate is null
                                  
    
  --update td
  --   set BOEBarcode = LTRIM(RTRIM(th.BOE)) + LTRIM(RTRIM(td.BOELineNumber))
  --  from @TableDetails td
  --  join @TableHeader th on th.ReceiptId = td.ReceiptId
    
  update td
     set DIMHeight = isnull(p.Height,0),
		 DIMLength = isnull(p.Length,0),
		 DIMWeight = isnull(p.Weight,0),
		 DIMWidth = isnull(p.Width,0)
    from @TableDetails td
    join Pack p (nolock) on p.StorageUnitId = td.StorageUnitId    
    
  update td
     set Controlled = 'Y'
    from @TableDetails td
   where td.ProductCategory = 'CONTROLLED'
   
   --select * from @TableDetails
   update td
     set LayerQtyWeight = (convert(nvarchar,td.LayerQuantity) + '(' + convert(nvarchar,td.LayerWeight) + 'Kg)')
    from @TableDetails td
   
   update td
     set CasesQtyWeight = (convert(nvarchar,td.CasesQuantity) + '(' + convert(nvarchar,td.CaseWeight) + 'Kg)')
    from @TableDetails td
    
  select th.InboundShipmentId,
         dbo.ConvertTo128( th.OrderNumber) as OrderNumberBarcode,
         th.OrderNumber,
         th.Supplier,
         th.SupplierCode,
         th.DeliveryNoteNumber,
         th.BOE,
         td.ProductCode,
         td.Product,
         td.ExpectedQuantity,
         td.ReceivedQuantity,
         td.RejectQuantity,
         td.PalletQuantity,
         td.PalletCount,
         td.LayerQuantity,
         td.LayerCount,
         td.CasesQuantity,
         td.CasesCount,
         td.UnitsQuantity,
         td.UnitsCount,
         td.UnitPrice,
         td.SampleQuantity,
         td.SKUCode,
         td.SKU,
         td.SupplierBarcode,
         (select Indicator from Configuration where ConfigurationId = 396 and WarehouseId = th.WarehouseId) as 'PrtSupplBCode',
         XDocStock,
         BatchRequired,
         SerialNumbers,
         DIMHeight,
         DIMWeight,
         DIMLength,
         DIMWidth,
         td.BOELineNumber,
         BOEBarcode,
         td.ProductCategoryId,
         td.ProductCategory,
         td.ExpiryDate,
         td.Controlled,
         LayerQtyWeight,
         CasesQtyWeight,
         Batch
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
end
 
 
 
 
