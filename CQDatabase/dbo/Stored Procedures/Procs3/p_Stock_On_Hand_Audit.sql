﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Stock_On_Hand_Audit
  ///   Filename       : p_Stock_On_Hand_Audit.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Feb 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Stock_On_Hand_Audit

as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Stock_On_Hand_Audit',
          @GetDate           datetime,
          @Transaction       bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  insert StockOnHandAudit
        (WarehouseId,
         AreaId,
         AreaCode,
         Area,
         AreaType,
         WarehouseCode,
         StockOnHand,
         Location,
         LocationId,
         StorageUnitBatchId,
         StorageUnitId,
         BatchId,
         PrincipalCode,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ExpiryDate,
         Status,
         StatusCode,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         StockTakeInd,
         ActiveBinning,
         ActivePicking,
         Used)
  select a.WarehouseId,
         a.AreaId,
         a.AreaCode,
         a.Area,
         a.AreaType,
         a.WarehouseCode,
         a.StockOnHand,
         l.Location,
         l.LocationId,
         sub.StorageUnitBatchId,
         su.StorageUnitId,
         sub.BatchId,
         pn.PrincipalCode,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         b.ExpiryDate,
         s.Status,
         s.StatusCode,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity,
         l.StockTakeInd,
         l.ActiveBinning,
         l.ActivePicking,
         l.Used
    from StorageUnitBatch          sub (nolock)
    join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId           = p.ProductId
    left
    join Principal                  pn (nolock) on p.PrincipalId          = pn.PrincipalId
    join SKU                       sku (nolock) on su.SKUId               = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId            = b.BatchId
    join Status                      s (nolock) on b.StatusId             = s.StatusId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join Location                    l (nolock) on subl.LocationId        = l.LocationId
    join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
    join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
