﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wizard_Product_Select
  ///   Filename       : p_Wizard_Product_Select.sql
  ///   Create By      : Junaid	Desi
  ///   Date Created   : 25 Sep 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wizard_Product_Select

as
begin
	 set nocount on;
  
  Execute p_Wizard_Product_Check -1
  
 SELECT [ProductImportId]
      ,[ProductCode]
      ,[SKUCode]
      ,[Product]
      ,[ProductBarcode]
      ,[MinimumQuantity]
      ,[ReorderQuantity]
      ,[MaximumQuantity]
      ,[CuringPeriodDays]
      ,[ShelfLifeDays]
      ,[QualityAssuranceIndicator]
      ,[ProductType]
      ,[PackType]
      ,[PackQuantity]
      ,[PackBarcode]
      ,[PackVolume]
      ,[PackWeight]
      ,[HasError]
      ,ErrorLog.value('(Root)[1]','nvarchar(50)') as ErrorLog
    FROM [ProductImport]
  order by HasError desc

end
