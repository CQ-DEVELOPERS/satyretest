﻿ 
/*
/// <summary>
///   Procedure Name : p_Report_Receiving_Summary_Report_test
///   Filename       : p_Report_Receiving_Summary_Report_test.sql
///   Create By      : Karen
///   Date Created   : October 2013
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
CREATE procedure p_Report_Receiving_Summary_Report_test
(
@OrderNumber       nvarchar(30),
@StorageUnitId	   int,
@Batch             nvarchar(50),
@FromDate          datetime,
@ToDate            datetime,
@Received		   nvarchar(1),
@PrincipalId	   int
)

as
begin
  set nocount on;
  
  declare @TableSOH as table
  (
   WarehouseId int,
   StorageUnitBatchId  int,
   StockOnHand int
  )
  
  print @Received
  
  insert @TableSOH
        (WarehouseId,
         StorageUnitBatchId,
         StockOnHand)
  select a.WarehouseId,
         subl.StorageUnitBatchId,
         sum(subl.ActualQuantity)
    from StorageUnitBatchLocation subl (nolock)
    join AreaLocation               al (nolock) on subl.LocationId        = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
  group by a.WarehouseId,
           subl.StorageUnitBatchId

  
  declare @TableDetails as Table
  (
  OrderNumber          nvarchar(30),
  OrderDate            datetime,
  ExpectedDeliveryDate datetime,
  ProductCode          nvarchar(30),
  ProductDesc          nvarchar(255),
  StorageUnitId		   int,
  SKUCode              nvarchar(50),
  SKU                  nvarchar(50),
  Batch                nvarchar(50),
  OrderQty             numeric(13,6),
  ReceivedQty          numeric(13,6),
  OutstandingBal       numeric(13,6),
  StockOnHand          numeric(13,6),
  DeliveryNote		   nvarchar(30),
  ExternalCompanyId	   int,
  ExternalCompanyCode  nvarchar(30),
  ExternalCompany	   nvarchar(255),
  ReceivedDate		   datetime,
  ReceiptId			   int,
  GRN	  			   nvarchar(30),
  PrincipalId		   int,
  PrincipalCode		   nvarchar(30),
  Length			   int,
  Width				   int,
  Height			   int,
  UnitWeight		   int,
  GrossWeight		   int,
  Weight			   nvarchar(50)  
  )
  
  if @StorageUnitId = '-1'
     set @StorageUnitId = null
         
  if @OrderNumber = '-1'
     set @OrderNumber = null
         
  if @Batch = '-1'
     set @Batch = null
     
  if @PrincipalId = -1 
	 set @PrincipalId = null
     
  if @Received = 'A'       
	  insert @TableDetails
			(OrderNumber,
			 OrderDate,
			 ExpectedDeliveryDate,
			 ProductCode,
			 ProductDesc,
			 StorageUnitId,
			 SKUCode,
			 SKU,
			 Batch,
			 OrderQty,
			 ReceivedQty,
			 OutstandingBal,
			 StockOnHand,
			 ExternalCompanyId,
			 DeliveryNote,
			 ReceivedDate,
			 ReceiptId,
			 GRN,
			 PrincipalId
			 )
	  select id.OrderNumber,
			 id.CreateDate,
			 id.DeliveryDate,
			 p.ProductCode,
			 p.Product,
			 su.StorageUnitId,
			 sku.SKUCode,
			 sku.SKU,
			 b.Batch,
			 sum(rl.RequiredQuantity),
			 sum(rl.ReceivedQuantity),
			 sum(rl.RequiredQuantity - isnull(rl.ReceivedQuantity,0)),
			 sum(StockOnHand) ,
			 id.ExternalCompanyId,
			 r.DeliveryNoteNumber ,
			 r.ReceivingCompleteDate,
			 r.ReceiptId,
			 r.GRN,
			 p.PrincipalId     
		from InboundDocument     id
		 join InboundLine        il on id.InboundDocumentId = il.InboundDocumentId
		 join ReceiptLine        rl on il.InboundLineId = rl.InboundLineId
		 join Receipt			  r on rl.ReceiptId = r.ReceiptId
		 join StorageUnitBatch  sub on rl.StorageUnitBatchId = sub.StorageUnitBatchId
		 join Batch               b on sub.BatchId = b.BatchId
		 join StorageUnit        su on sub.StorageUnitId = su.StorageUnitId
		 join Product             p on su.ProductId = p.ProductId
		 join SKU               sku on su.SKUId = sku.SKUId
		 left
		 join @TableSOH         soh on rl.StorageUnitBatchId = soh.StorageUnitBatchId
								   and id.WarehouseId = soh.WarehouseId
	   where id.CreateDate between @FromDate and @ToDate
	     and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
	  group by id.OrderNumber,
			   id.CreateDate,
			   id.DeliveryDate,
			   p.ProductCode,
			   p.Product,
			   su.StorageUnitId,
			   sku.SKUCode,
			   sku.SKU,
			   b.Batch,
			   id.ExternalCompanyId,
			   r.DeliveryNoteNumber,
			   r.ReceivingCompleteDate,
			   r.ReceiptId ,
			   r.GRN,
			   p.PrincipalId
	if @Received = 'R'       
	  insert @TableDetails
			(OrderNumber,
			 OrderDate,
			 ExpectedDeliveryDate,
			 ProductCode,
			 ProductDesc,
			 StorageUnitId,
			 SKUCode,
			 SKU,
			 Batch,
			 OrderQty,
			 ReceivedQty,
			 OutstandingBal,
			 StockOnHand,
			 ExternalCompanyId,
			 DeliveryNote,
			 ReceivedDate,
			 ReceiptId,
			 GRN,
			 PrincipalId
			 )
	  select id.OrderNumber,
			 id.CreateDate,
			 id.DeliveryDate,
			 p.ProductCode,
			 p.Product,
			 su.StorageUnitId,
			 sku.SKUCode,
			 sku.SKU,
			 b.Batch,
			 sum(rl.RequiredQuantity),
			 sum(rl.ReceivedQuantity),
			 sum(rl.RequiredQuantity - isnull(rl.ReceivedQuantity,0)),
			 sum(StockOnHand) ,
			 id.ExternalCompanyId,
			 r.DeliveryNoteNumber ,
			 r.ReceivingCompleteDate,
			 r.ReceiptId,
			 r.GRN ,
			 p.PrincipalId      
		from InboundDocument     id
		 join InboundLine        il on id.InboundDocumentId = il.InboundDocumentId
		 join ReceiptLine        rl on il.InboundLineId = rl.InboundLineId
		 join Receipt			  r on rl.ReceiptId = r.ReceiptId
		 join Status			  s on s.StatusId = r.StatusId
		 join StorageUnitBatch  sub on rl.StorageUnitBatchId = sub.StorageUnitBatchId
		 join Batch               b on sub.BatchId = b.BatchId
		 join StorageUnit        su on sub.StorageUnitId = su.StorageUnitId
		 join Product             p on su.ProductId = p.ProductId
		 join SKU               sku on su.SKUId = sku.SKUId
		 left
		 join @TableSOH         soh on rl.StorageUnitBatchId = soh.StorageUnitBatchId
								   and id.WarehouseId = soh.WarehouseId
	   where id.CreateDate between @FromDate and @ToDate
	     and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
	     and s.Status = 'Receiving Complete'
	  group by id.OrderNumber,
			   id.CreateDate,
			   id.DeliveryDate,
			   p.ProductCode,
			   p.Product,
			   su.StorageUnitId,
			   sku.SKUCode,
			   sku.SKU,
			   b.Batch,
			   id.ExternalCompanyId,
			   r.DeliveryNoteNumber,
			   r.ReceivingCompleteDate,
			   r.ReceiptId,
			   r.GRN,
			   p.PrincipalId 
			   
	if @Received = 'N'       
	  insert @TableDetails
			(OrderNumber,
			 OrderDate,
			 ExpectedDeliveryDate,
			 ProductCode,
			 ProductDesc,
			 StorageUnitId,
			 SKUCode,
			 SKU,
			 Batch,
			 OrderQty,
			 ReceivedQty,
			 OutstandingBal,
			 StockOnHand,
			 ExternalCompanyId,
			 DeliveryNote,
			 ReceivedDate,
			 ReceiptId,
			 GRN,
			 PrincipalId
			 )
	  select id.OrderNumber,
			 id.CreateDate,
			 id.DeliveryDate,
			 p.ProductCode,
			 p.Product,
			 su.StorageUnitId,
			 sku.SKUCode,
			 sku.SKU,
			 b.Batch,
			 sum(rl.RequiredQuantity),
			 sum(rl.ReceivedQuantity),
			 sum(rl.RequiredQuantity - isnull(rl.ReceivedQuantity,0)),
			 sum(StockOnHand) ,
			 id.ExternalCompanyId,
			 r.DeliveryNoteNumber ,
			 r.ReceivingCompleteDate,
			 r.ReceiptId,
			 r.GRN,
			 p.PrincipalId       
		from InboundDocument     id
		 join InboundLine        il on id.InboundDocumentId = il.InboundDocumentId
		 join ReceiptLine        rl on il.InboundLineId = rl.InboundLineId
		 join Receipt			  r on rl.ReceiptId = r.ReceiptId
		 join Status			  s on s.StatusId = r.StatusId
		 join StorageUnitBatch  sub on rl.StorageUnitBatchId = sub.StorageUnitBatchId
		 join Batch               b on sub.BatchId = b.BatchId
		 join StorageUnit        su on sub.StorageUnitId = su.StorageUnitId
		 join Product             p on su.ProductId = p.ProductId
		 join SKU               sku on su.SKUId = sku.SKUId
		 left
		 join @TableSOH         soh on rl.StorageUnitBatchId = soh.StorageUnitBatchId
								   and id.WarehouseId = soh.WarehouseId
	   where id.CreateDate between @FromDate and @ToDate
	     and s.Status != 'Receiving Complete'
	     and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
	  group by id.OrderNumber,
			   id.CreateDate,
			   id.DeliveryDate,
			   p.ProductCode,
			   p.Product,
			   su.StorageUnitId,
			   sku.SKUCode,
			   sku.SKU,
			   b.Batch,
			   id.ExternalCompanyId,
			   r.DeliveryNoteNumber,
			   r.ReceivingCompleteDate,
			   r.ReceiptId,
			   r.GRN ,
			   p.PrincipalId
  
   update  td
      set td.ExternalCompanyCode = ec.ExternalCompanyCode,
		  td.ExternalCompany = ec.ExternalCompanyCode
     from @TableDetails td
     join ExternalCompany ec  on ec.ExternalCompanyId = td.ExternalCompanyId
     
   update  td
      set td.ExternalCompanyCode = ec.ExternalCompanyCode,
		  td.ExternalCompany = ec.ExternalCompanyCode
     from @TableDetails td
     join ExternalCompany ec  on ec.ExternalCompanyId = td.ExternalCompanyId
     
   update  td
      set td.ReceivedDate = rh.InsertDate
     from @TableDetails td
     join ReceiptHistory rh  on td.ReceiptId = rh.ReceiptId
     join Status s on rh.StatusId = s.statusid
     where Status = 'Received'
   
  update  td
      set td.PrincipalCode = p.PrincipalCode
     from @TableDetails td
     join Principal p  on td.PrincipalId = p.PrincipalId 
     
  update  td
      set td.Height = p.Height,
          td.Width = p.Width,
          td.Length = p.Length,
          td.UnitWeight = p.Weight,
          td.GrossWeight = isnull(p.Weight,0) * isnull(td.ReceivedQty,0) 
     from @TableDetails td
     join pack p  on td.storageunitid = p.storageunitid 
     where p.packtypeid = 8   
     
  update  td
      set td.Weight = convert(nvarchar(20),td.UnitWeight) + '/' + convert(nvarchar(20),td.GrossWeight)
     from @TableDetails td
        
  select OrderNumber,
         DeliveryNote,
         OrderDate,
         ExpectedDeliveryDate,
         ProductCode,
         ProductDesc,
         SKUCode,
         SKU,
         Batch,
         OrderQty,
         ReceivedQty,
         OutstandingBal,
         StockOnHand,
         ExternalCompanyCode,
         ExternalCompany,
         ReceivedDate,
         GRN,
         PrincipalCode,
         Height,
         Width,
         Length,
         Weight,
         UnitWeight,
         GrossWeight
    from @TableDetails
   where OrderNumber = isnull(@OrderNumber, OrderNumber)
	 and StorageUnitId = ISNULL(@StorageUnitId, StorageUnitId)
     --and ProductCode = isnull(@ProductCode, ProductCode)
     and Batch = isnull(@Batch, Batch)
  order by ProductCode
end
