﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Transfer_Order_Search
  ///   Filename       : p_Transfer_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Transfer_Order_Search
(
 @WarehouseId int,
 @LocationId  int,
 @ProductCode nvarchar(30),
 @Product     nvarchar(255),
 @FromDate    datetime,
 @ToDate      datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId    int,
   IssueId               int,
   StorageUnitBatchId    int,
   ProductCode           nvarchar(30),
   Product               nvarchar(255),
   SKUCode               nvarchar(10),
   Batch                 nvarchar(50),
   RequiredQuantity      float,
   UOM                   nvarchar(50),
   CreateDate            datetime,
   ManufacturingId       int,
   ManufacturingLocation nvarchar(15),
   Remarks               nvarchar(255),
   ConfirmedQuantity     float,
   JobId                 int,
   RawStagingId          int,
   RawStaging            nvarchar(15),
   ProductionStagingId   int,
   ProductionStaging     nvarchar(15),
   DwellTime             nvarchar(20),
   StatusCode            nvarchar(20),
   Status                nvarchar(50),
   OutboundDocumentType  nvarchar(30)
  )
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         StorageUnitBatchId,
         RequiredQuantity,
         CreateDate,
         ManufacturingId,
         Remarks,
         ConfirmedQuantity,
         JobId,
         RawStagingId,
         ProductionStagingId,
         OutboundDocumentType)
  select ili.OutboundShipmentId,
         ili.IssueId,
         ins.StorageUnitBatchId,
         ins.Quantity,
         ins.CreateDate,
         iss.ManufacturingId,
         iss.Remarks,
         ins.ConfirmedQuantity,
         ins.JobId,
         iss.LocationId,
         iss.DespatchBay,
         odt.OutboundDocumentType
    from Issue                iss (nolock)
    join OutboundDocument      od (nolock) on iss.OutboundDocumentId = od.OutboundDocumentId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                          and odt.OutboundDocumentTypeCode in ('REP','MR')
    join Status                 s (nolocK) on iss.StatusId = s.StatusId
    join IssueLineInstruction ili (nolock) on iss.IssueId = ili.IssueId
    join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
   where iss.WarehouseId = @WarehouseId
     and s.StatusCode in ('W','P','SA','M','RL','PC','PS','A','WC','QA') -- GS 2012-11-02 Removed 'CK'
     and od.CreateDate between @FromDate and @ToDate
  union
  select null as 'OutboundShipmentId',
         iss.IssueId,
         il.StorageUnitBatchId,
         il.Quantity,
         od.CreateDate,
         iss.ManufacturingId,
         iss.Remarks,
         il.ConfirmedQuatity,
         null as 'JobId',
         iss.LocationId,
         iss.DespatchBay,
         odt.OutboundDocumentType
    from Issue                iss (nolock)
    join IssueLine             il (nolock) on iss.IssueId = il.IssueId
    join OutboundDocument      od (nolock) on iss.OutboundDocumentId = od.OutboundDocumentId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                          and odt.OutboundDocumentTypeCode in ('REP','MR')
    join Status                 s (nolocK) on iss.StatusId = s.StatusId
    left
    join IssueLineInstruction ili (nolock) on il.IssueLineId = ili.IssueLineId
   where iss.WarehouseId = @WarehouseId
     and s.StatusCode = 'W'
     and ili.InstructionId is null
     and od.CreateDate between @FromDate and @ToDate
     
  --union
  --select ins.StorageUnitBatchId,
  --       ins.Quantity,
  --       ins.CreateDate,
  --       null,
  --       null,
  --       ins.ConfirmedQuantity,
  --       ins.JobId,
  --       ins.PickLocationId,
  --       ins.PickLocationId
  --  from Instruction          ins (nolock)
  --  join InstructionType       it (nolock) on ins.InstructionTypeId = it.InstructionTypeId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         Batch       = b.Batch,
         UOM         = uom.UOM
    from @TableResult tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join UOM              uom (nolock) on sku.UOMId             = uom.UOMId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
  
  update tr
     set Status = s.Status,
         StatusCode = s.StatusCode
    from @TableResult tr
    join Job           j (nolock) on tr.JobId = j.JobId
    join Status        s (nolock) on j.StatusId = s.StatusId
  
  update tr
     set ManufacturingLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.ManufacturingId = l.LocationId
  
  update tr
     set RawStaging = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.RawStagingId = l.LocationId
  
  update tr
     set ProductionStaging = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.ProductionStagingId = l.LocationId
  
  select isnull(OutboundShipmentId,-1) as 'OutboundShipmentId',
         IssueId,
         OutboundDocumentType,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         RequiredQuantity,
         UOM,
         CreateDate,
         isnull(ManufacturingId,-1) as 'ManufacturingId',
         ManufacturingLocation,
         Remarks,
         ConfirmedQuantity,
         JobId,
         isnull(RawStagingId,-1) as 'RawStagingId',
         RawStaging,
         isnull(ProductionStagingId,-1) as 'ProductionStagingId',
         ProductionStaging,
         DwellTime,
         Status
    from @TableResult
   where isnull(ProductCode,'%') like '%' + isnull(@ProductCode, isnull(ProductCode,'%')) + '%'
     and isnull(Product,'%')     like '%' + isnull(@Product, isnull(Product,'%')) + '%'
     --and StatusCode not in ('CK','D')
end
