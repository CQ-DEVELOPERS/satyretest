﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Stretch_Wrap_Reject
  ///   Filename       : p_Stretch_Wrap_Reject.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
 
 /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Stretch_Wrap_Reject
(
 @jobId      int,
 @reasonId   int = null,
 @operatorId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @Exception         nvarchar(255),       
   @ExceptionCode     nvarchar(10),
          @InstructionId     int,
          @StatusId          int,
          @InstructionTypeCode nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  select @Exception     = Reason,
      
   @ExceptionCode = ReasonCode
    from Reason (nolock)
   where ReasonId = @reasonId
  
  select top 1 @InstructionId       = i.InstructionId,
               @InstructionTypeCode = it.InstructionTypeCode
    from Instruction i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where i.JobId = @JobId
  order by i.InstructionId
  
  exec @Error = p_Exception_Insert 
   @ExceptionId    = null,
   @InstructionId  = @InstructionId,
   @ReasonId       = @ReasonId,  
 @Exception      = @Exception,
   @ExceptionCode  = @ExceptionCode,
   @CreateDate     = @GetDate
  
  if @Error <> 0
    goto error
  
  if @InstructionTypeCode in ('P','PM','PS','FM','PR')
  begin
    select @StatusId = dbo.ufn_StatusId('IS','QA')
    

    exec @Error = p_Job_Update
     @JobId          = @JobId,
     @StatusId       = @StatusId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Status_Rollup
     @JobId          = @JobId
    
    if @Error <> 0
      goto error
  end
  

  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Stretch_Wrap_Reject'); 
    rollback transaction
    return @Error
end

 
