﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Driver_Upliftment_Instructions
  ///   Filename       : p_Report_Driver_Upliftment_Instructions.sql
  ///   Create By      : Karen
  ///   Date Created   : February 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Driver_Upliftment_Instructions
(
 @InboundDocumentId  int
)

as
begin
	 set nocount on;
  
select	id.InboundDocumentId,
		id.CreateDate,
		p.Product,
		il.Quantity 
from	InboundDocument id
join	InboundLine		il on il.InboundDocumentId = id.InboundDocumentId
join	StorageUnit     su on su.StorageUnitId = id.StorageUnitId
join    Product			p  on su.ProductId = p.ProductId
where	id.InboundDocumentId = @InboundDocumentId

end
