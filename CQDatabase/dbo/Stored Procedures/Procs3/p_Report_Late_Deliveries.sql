﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Late_Deliveries
  ///   Filename       : p_Report_Late_Deliveries.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Late_Deliveries
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId       int,
 @ExternalCompanyId int = null,
 @LocationId        int = null,
 @FromDate          datetime,
 @ToDate            datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   DeliveryDate       datetime,
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   LocationId         int,
   Location           nvarchar(15),
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   RequiredQuantity   float
  )
  
  if @ExternalCompanyId = -1
    set @ExternalCompanyId = null
  
  if @LocationId = -1
    set @LocationId = null
  
  insert @TableResult
        (DeliveryDate,
         ExternalCompanyId,
         LocationId,
         StorageUnitBatchId,
         RequiredQuantity)
  select isnull(r.DeliveryDate, id.DeliveryDate),
         id.ExternalCompanyId,
         r.LocationId,
         rl.StorageUnitBatchId,
         rl.RequiredQuantity
    from InboundDocument id (nolock)
    join Receipt          r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join ReceiptLine     rl (nolock) on r.ReceiptId          = rl.ReceiptId
   where id.WarehouseId       = @WarehouseId
     and id.ExternalCompanyId = isnull(@ExternalCompanyId, id.ExternalCompanyId)
     --and r.LocationId         = isnull(@LocationId, r.LocationId)
     and isnull(r.DeliveryDate, id.DeliveryDate)
                        between @FromDate and @ToDate
  
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit          su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product               p (nolock) on su.ProductId          = p.ProductId
    join SKU                 sku (nolock) on su.SKUId              = sku.SKUId
    join Batch                 b (nolock) on sub.BatchId           = b.BatchId
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  select DeliveryDate,
         ExternalCompany,
         Location,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         RequiredQuantity
    from @TableResult
end
