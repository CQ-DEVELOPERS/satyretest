﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Operator_Performance_Capacity_QA
  ///   Filename       : p_Report_Operator_Performance_Capacity_QA.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 14 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Operator_Performance_Capacity_QA
(
	@FromDate				Datetime,
	@ToDate					DateTime,
	@OperatorGroupId		int,
--	@OperatorId				int = null,
	@UOM                   nvarchar(15) = 'Units'
)

as
begin

--if (@OperatorId = -1)
--	Begin	
--Set @OperatorId = Null
--	End

Declare @DateHoursDiff		int,
		@DateDaysDiff		int,
		@DateMonthsDiff		int,
		@UOMSite			numeric(10,3),
		@UOMIndustry		numeric(10,3)
Select 
 @DateHoursDiff = DateDiff(hh,@FromDate,@ToDate)
,@DateDaysDiff = DateDiff(D,@FromDate,@ToDate)
,@DateMonthsDiff = DateDiff(M,@FromDate,@ToDate) 

Declare @TempTable as Table
(
	OperatorId					int,
	OperatorGroupId				int,
	EndDate						DateTime,
	QA							Numeric(10,3),
	Units						Numeric(10,3),
	Weight						Numeric(10,3),
	Instructions				int,
	Orders						int,
	Orderlines					int,
	Jobs						int,
	ActiveTime					int,
	DwellTime					int,
	PercentQA					Numeric(10,3),
	IndustryQA					Numeric(10,3),
	SiteQA						Numeric(10,3),
	UOMValue					Numeric(10,3),
	UOMIndustry					Numeric(10,3),
	UOMSite						Numeric(10,3),
	UOM							nvarchar(15)
)


Insert Into @Temptable

	(	OperatorId,
		OperatorGroupId,
		EndDate,	
		QA,
		Units,
		Weight,
		Instructions,
		Orders,	
		Orderlines,	
		Jobs,	
		ActiveTime,	
		DwellTime,	
		PercentQa)

(Select OperatorId
	 ,OperatorGroupId
      ,[EndDate]
      --,QA.[JobId]
      ,SUM([QA]) as QA
      --,QA.[InstructionTypeId]
      --,QA.[ActivityStatus]
      --,QA.[PickingRate]
      ,SUM([Units]) as [Units]
      ,Convert(Numeric(10,2),SUM([Weight])) as [Weight]
      ,SUM([Instructions])as [Instructions]
      ,SUM([Orders])as [Orders]
      ,SUM([OrderLines])as [OrderLines]
      ,SUM([Jobs])as [Jobs]
      ,SUM([ActiveTime])as [ActiveTime]
      ,SUM([DwellTime])as [DwellTime]
	  ,Convert(numeric(10,2),(Convert(numeric(10,2),SUM([QA]))/SUM([Jobs])) * 100) as PercentQA
	  -- ,Convert(numeric(10,3),217) as val

From (

SELECT 
		QA.OperatorId
	  ,O.OperatorGroupId
      ,QA.[EndDate]
      --,QA.[JobId]
      ,Count(QA.[QA]) as QA
      --,QA.[InstructionTypeId]
      --,QA.[ActivityStatus]
      --,QA.[PickingRate]
      ,SUM(QA.[Units]) as [Units]
      ,SUM(QA.[Weight])as [Weight]
      ,SUM(QA.[Instructions])as [Instructions]
      ,SUM(QA.[Orders])as [Orders]
      ,SUM(QA.[OrderLines])as [OrderLines]
      ,SUM(QA.[Jobs])as [Jobs]
      ,SUM(QA.[ActiveTime])as [ActiveTime]
      ,SUM(QA.[DwellTime])as [DwellTime]
  FROM [OperatorQA] QA
	Join Operator O on QA.OperatorId = O.OperatorId
Where Qa = 1
And EndDate between @FromDate and @ToDate
And O.OperatorGroupId = @OperatorGroupId
Group by QA.[EndDate],O.OperatorGroupId,QA.OperatorId

union

SELECT QA.[OperatorId]
	  ,O.OperatorGroupId
      ,QA.[EndDate]
      --,QA.[JobId]
      ,0 as QA
      --,QA.[InstructionTypeId]
      --,QA.[ActivityStatus]
      --,QA.[PickingRate]
      ,SUM(QA.[Units]) as [Units]
      ,SUM(QA.[Weight])as [Weight]
      ,SUM(QA.[Instructions])as [Instructions]
      ,SUM(QA.[Orders])as [Orders]
      ,SUM(QA.[OrderLines])as [OrderLines]
      ,SUM(QA.[Jobs])as [Jobs]
      ,SUM(QA.[ActiveTime])as [ActiveTime]
      ,SUM(QA.[DwellTime])as [DwellTime]
  FROM [OperatorQA] QA
	Join Operator O on QA.OperatorId = O.OperatorId
Where Qa = 0
And EndDate between @FromDate and @ToDate
And O.OperatorGroupId = @OperatorGroupId
Group by QA.[EndDate],O.OperatorGroupId,QA.OperatorId ) X

Group by [EndDate],OperatorGroupId,OperatorId)

 Update @TempTable Set	
							SiteQA =(Select QA From GroupPerformance Where OperatorGroupId = @OperatorGroupId And PerformanceType = 'S'),
							IndustryQA = (Select QA From GroupPerformance Where OperatorGroupId = @OperatorGroupId And PerformanceType = 'I')


if @UOM = 'Units'
begin

Set @UOMSite		=(Select Units From GroupPerformance Where OperatorGroupId = @OperatorGroupId And PerformanceType = 'S')
Set @UOMIndustry    = (Select Units From GroupPerformance Where OperatorGroupId = @OperatorGroupId And PerformanceType = 'I')
		
   Update @TempTable Set	UOMValue	= Units,
							UOM			= 'Pieces',
							UOMSite		=	@UOMSite,
							UOMIndustry =	@UOMIndustry 
							
							
						
end
else 
	if @UOM = 'Weight'
	begin

Set @UOMSite		=(Select Weight From GroupPerformance Where OperatorGroupId = @OperatorGroupId And PerformanceType = 'S')
Set @UOMIndustry	= (Select Weight From GroupPerformance Where OperatorGroupId = @OperatorGroupId And PerformanceType = 'I')

	 Update @TempTable Set	UOMValue = Weight,
							UOM = 'KG',
							UOMSite		=	@UOMSite,
							UOMIndustry =	@UOMIndustry 
	end
else 
	if @UOM = 'Jobs'
	begin

Set @UOMSite		=(Select Jobs From GroupPerformance Where OperatorGroupId = @OperatorGroupId And PerformanceType = 'S')
Set @UOMIndustry	= (Select Jobs From GroupPerformance Where OperatorGroupId = @OperatorGroupId And PerformanceType = 'I')

	 Update @TempTable Set	UOMValue = Jobs,
							UOM = 'Jobs',
							UOMSite		=	@UOMSite,
							UOMIndustry =	@UOMIndustry 
	end
else 
	if @UOM = 'Instructions'
begin
Set @UOMSite		=(Select Instructions From GroupPerformance Where OperatorGroupId = @OperatorGroupId And PerformanceType = 'S')
Set @UOMIndustry	= (Select Instructions From GroupPerformance Where OperatorGroupId = @OperatorGroupId And PerformanceType = 'I')

	
	 Update @TempTable Set	UOMValue = Instructions,
							UOM = 'Pick Lines',
							UOMSite		=	@UOMSite,
							UOMIndustry =	@UOMIndustry 
	end

if (@DateMonthsDiff < 3)
	Begin
		if (@DateDaysDiff < 8)		
			Begin
				if (@DateHoursDiff < 25)
					Begin
						--Select 'x < 24 Hours' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
Select	T.OperatorGroupId,
		OG.OperatorGroup,	  
		Convert(nvarchar,T.EndDate,108) as EndDate, 
		Convert(nvarchar,T.EndDate,108) as ordscale, 
		T.OperatorId,
		O.Operator,	
		SUM(T.QA) as QA,
		SUM(T.Units) as Units,			
		SUM(T.Weight) as Weight,			
		SUM(T.Instructions) as Instructions,	
		SUM(T.Orders) as Orders,			
		SUM(T.Orderlines) as Orderlines,		
		SUM(T.Jobs) as Jobs,			
		SUM(T.ActiveTime) as ActiveTime,		
		SUM(T.DwellTime) as DwellTime,		
		SUM(T.PercentQA) as PercentQA,		
		SUM(Distinct(T.IndustryQA)) as IndustryQA,		
		SUM(Distinct(T.SiteQA)) as SiteQA,			
		SUM(T.UOMValue) as UOMValue,		
		SUM(Distinct(T.UOMIndustry)) as UOMIndustry,		
		SUM(Distinct(T.UOMSite)) as UOMSite,			
		T.UOM	
From @TempTable T
		--Inner Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		Inner Join Operator O			On O.OperatorId				= T.OperatorId
	Group by T.OperatorGroupId,OG.OperatorGroup,Convert(nvarchar,EndDate,108),T.UOM,T.OperatorId,O.Operator--,IT.InstructionType, T.InstructionTypeId --,Uom
	ORDER BY ordscale	

			-- Select data by this Scale
					End
				Else
					Begin
					--Select '24 Hours < x < 1 week' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
Select	T.OperatorGroupId,
		OG.OperatorGroup,	  
		Convert(nvarchar,T.EndDate,101) as EndDate, 
		Convert(nvarchar,T.EndDate,101) as ordscale ,
		T.OperatorId,
		O.Operator,	
		SUM(T.QA) as QA,
		SUM(T.Units) as Units,			
		SUM(T.Weight) as Weight,			
		SUM(T.Instructions) as Instructions,	
		SUM(T.Orders) as Orders,			
		SUM(T.Orderlines) as Orderlines,		
		SUM(T.Jobs) as Jobs,			
		SUM(T.ActiveTime) as ActiveTime,		
		SUM(T.DwellTime) as DwellTime,		
		SUM(T.PercentQA) as PercentQA,		
		SUM(Distinct(T.IndustryQA)) as IndustryQA,		
		SUM(Distinct(T.SiteQA)) as SiteQA,			
		SUM(T.UOMValue) as UOMValue,		
		SUM(Distinct(T.UOMIndustry)) as UOMIndustry,		
		SUM(Distinct(T.UOMSite)) as UOMSite,			
		T.UOM	
From @TempTable T
		--Inner Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		Inner Join Operator O			On O.OperatorId				= T.OperatorId
	Group by T.OperatorGroupId,OG.OperatorGroup,Convert(nvarchar,EndDate,101),T.UOM,T.OperatorId,O.Operator--,IT.InstructionType, T.InstructionTypeId --,Uom
	ORDER BY ordscale	
					End
			End
		Else
				Begin
			
			Select	T.OperatorGroupId,
		OG.OperatorGroup,	  
		DatePart(wk,T.EndDate) as EndDate, 
		DatePart(wk,T.EndDate) as ordscale , 
		T.OperatorId,
		O.Operator,	
		SUM(T.QA) as QA,
		SUM(T.Units) as Units,			
		SUM(T.Weight) as Weight,			
		SUM(T.Instructions) as Instructions,	
		SUM(T.Orders) as Orders,			
		SUM(T.Orderlines) as Orderlines,		
		SUM(T.Jobs) as Jobs,			
		SUM(T.ActiveTime) as ActiveTime,		
		SUM(T.DwellTime) as DwellTime,		
		SUM(T.PercentQA) as PercentQA,		
		SUM(Distinct(T.IndustryQA)) as IndustryQA,		
		SUM(Distinct(T.SiteQA)) as SiteQA,			
		SUM(T.UOMValue) as UOMValue,		
		SUM(Distinct(T.UOMIndustry)) as UOMIndustry,		
		SUM(Distinct(T.UOMSite)) as UOMSite,			
		T.UOM	
From @TempTable T
		--Inner Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		Inner Join Operator O			On O.OperatorId				= T.OperatorId
	Group by T.OperatorGroupId,OG.OperatorGroup,DatePart(wk,T.EndDate),T.UOM,T.OperatorId,O.Operator--,IT.InstructionType, T.InstructionTypeId --,Uom
	ORDER BY ordscale		
			
			End
	End
Else
	Begin
	
Select	T.OperatorGroupId,
		OG.OperatorGroup,	  
		DateName(M,T.EndDate) as EndDate, 
		DatePart(M,T.EndDate) as ordscale, 
		T.OperatorId,
		O.Operator,	
		SUM(T.QA) as QA,
		SUM(T.Units) as Units,			
		SUM(T.Weight) as Weight,			
		SUM(T.Instructions) as Instructions,	
		SUM(T.Orders) as Orders,			
		SUM(T.Orderlines) as Orderlines,		
		SUM(T.Jobs) as Jobs,			
		SUM(T.ActiveTime) as ActiveTime,		
		SUM(T.DwellTime) as DwellTime,		
		SUM(T.PercentQA) as PercentQA,		
		SUM(Distinct(T.IndustryQA)) as IndustryQA,		
		SUM(Distinct(T.SiteQA)) as SiteQA,			
		SUM(T.UOMValue) as UOMValue,		
		SUM(Distinct(T.UOMIndustry)) as UOMIndustry,		
		SUM(Distinct(T.UOMSite)) as UOMSite,			
		T.UOM	
From @TempTable T
		--Inner Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		Inner Join Operator O			On O.OperatorId				= T.OperatorId
	Group by T.OperatorGroupId,OG.OperatorGroup,DateName(M,T.EndDate),DatePart(M,T.EndDate),T.UOM,T.OperatorId,O.Operator--,IT.InstructionType, T.InstructionTypeId --,Uom
	ORDER BY ordscale	

	End

	 set nocount on;


end	   		
