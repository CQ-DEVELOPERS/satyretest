﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_OTIF_Detail_test
  ///   Filename       : p_Report_OTIF_Detail_test.sql
  ///   Create By      : Karen
  ///   Date Created   : May 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_OTIF_Detail_test
(
 @WarehouseId      int,
 @FromDate         datetime,
 @ToDate           datetime,
 @OrderNumber	   nvarchar(30)
)

as
begin
	 set nocount on;
	 
  declare @WeekEndsAndHolidays as table
  (
   DayOfWeekDate	datetime,
   DayName			char(3)
   )
  
  declare @TableTemp as table
  (
   OutboundDocumentTypeId int,
   OutboundDocumentType   nvarchar(30),
   OutboundDocumentId     int,
   OrderNumber			  nvarchar(30),
   IssueId                int,
   IssueLineId            int,
   JobId                  int,
   InstructionId          int,
   StorageUnitBatchId     int,
   StorageUnitId          int,
   ProductCode            nvarchar(30),
   Product                nvarchar(50),
   SKUCode                nvarchar(50),
   Quantity               int,
   ConfirmedQuantity      int,
   Weight                 numeric(13,2),
   ConfirmedWeight        numeric(13,2),
   Volume                 numeric(13,2),
   ConfirmedVolume        numeric(13,2),
   CreateDate             datetime,
   Release                datetime,
   StartDate              datetime,
   EndDate                datetime,
   Checking               datetime,
   Despatch               datetime,
   DespatchChecked        datetime,
   Complete               datetime,
   DeliveryDate           datetime,
   StatusCode             nvarchar(10),
   PlannedDespatch        int,
   ActualDespatch		  int,
   OnTimeInd			  int,
   OnTime				  nvarchar(10),
   OnTimeHours			  int,
   DateDiffVal			  int
  )
  
  declare @TableResult as table
  (
   CustomerName           nvarchar(255),
   OrderNumber            nvarchar(30),
   OutboundDocumentType   nvarchar(30),
   ProductCode			  nvarchar(30),
   Product				  nvarchar(50),
   PlannedDespatch        int,
   ActualDespatch		  int,
   InFull				  nvarchar(10),
   OnTime				  nvarchar(10),
   OrderLevel			  nvarchar(10),
   OTIFLine				  nvarchar(10)
  )
  
   declare @linecountAll numeric(15,2),
		   @linecountYes numeric(15,2),
		   @PctOTIF		 numeric(5,2),
		   @FirstSat datetime, @x int
SELECT @FirstSat = '1/1/2011', @x = 1 

--Add WeekEnds
WHILE @x < 52
BEGIN
INSERT INTO @WeekEndsAndHolidays(DayOfWeekDate, DayName)
SELECT DATEADD(ww,@x,@FirstSat),   'SAT' UNION ALL
SELECT DATEADD(ww,@x,@FirstSat+1), 'SUN'
SELECT @x = @x + 1
END


  
  if @OrderNumber = ' '
  or @OrderNumber = '-1'
	set @OrderNumber = null
	
  declare @Config int,
		  @StartDate	datetime
  
  select @StartDate = CURRENT_TIMESTAMP
  
  select @StartDate = DATEADD (DD , -15 , @FromDate )
  

  select @Config = convert(int, Value)
    from Configuration (nolock)
   where WarehouseId = @WarehouseId
     and ConfigurationId = 191
  
  if @Config is null
    set @Config = 7
  
  insert @TableTemp
        (OutboundDocumentTypeId,
         OutboundDocumentId, 
         OrderNumber,        
         IssueId,
         IssueLineId,
         JobId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         CreateDate,
         Release,
         StartDate,
         EndDate,
         Checking,
         Despatch,
         DespatchChecked,
         Complete,
         OnTimeInd,
         OnTime,
         DeliveryDate,
         OnTimeHours)
  select ili.OutboundDocumentTypeId,
         ili.OutboundDocumentId,
         od.OrderNumber,
         ili.IssueId,
         ili.IssueLineId,
         op.JobId,
         i.InstructionId,
         i.StorageUnitBatchId,
         ili.Quantity,
         ili.ConfirmedQuantity,
         op.CreateDate,
         op.Release,
         op.StartDate,
         op.EndDate,
         op.Checking,
         op.Despatch,
         op.DespatchChecked,
         op.Complete,
         datediff(hh, op.EndDate, op.Complete),
         'No',
         iss.DeliveryDate,
         3
    from OutboundPerformance   op (nolock)
    join Instruction            i (nolock) on op.JobId = i.JobId
    join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
    join OutboundDocument      od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
    join ExternalCompany       ec (nolock) on ec.ExternalCompanyId = od.ExternalCompanyId
    join Issue                iss (nolock) on ili.IssueId = iss.IssueId 
   where (op.CreateDate between @FromDate and @ToDate
      or  op.Complete        >= @FromDate 
      or  op.Complete        is null)
      and op.CreateDate between dateadd(dd,-@Config, @FromDate) and @ToDate
	  and i.warehouseId = @WarehouseId
	  
 update tr
     set OnTimeHours = 2
    from @TableTemp tr
   where convert(varchar(10), CreateDate, 108) <= '12:00:00'
   
update tr
     set ConfirmedQuantity = 0,
         StatusCode        = s.StatusCode
    from @TableTemp tr
    join Job           j (nolock) on tr.JobId = j.JobId and j.warehouseId = @WarehouseId
    join Status        s (nolock) on j.StatusId = s.StatusId
   where s.StatusCode = 'NS'
  
 
  update tr
     set CreateDate = od.CreateDate
    from @TableTemp     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId and od.warehouseId = @WarehouseId
  
  update tr
     set OutboundDocumentType = odt.OutboundDocumentType
    from @TableTemp          tr
    join OutboundDocumentType odt (nolock) on tr.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  
 
  update tr
     set StorageUnitId = sub.StorageUnitId
    from @TableTemp        tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
  
  update tr
     set ProductCode     = p.ProductCode,
         Product         = p.Product,
         SKUCode         = sku.SKUCode
    from @TableTemp        tr
    join StorageUnit       su (nolock) on tr.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  update tr
     set Weight          = pk.Weight * tr.Quantity,
         ConfirmedWeight = pk.Weight * tr.ConfirmedQuantity,
         Volume          = pk.Volume * tr.Quantity,
         ConfirmedVolume = pk.Volume * tr.ConfirmedQuantity
    from @TableTemp        tr
    join Pack              pk on tr.StorageUnitId = pk.StorageUnitId
   where pk.PackTypeId   = 2
     and pk.WarehouseId  = @WarehouseId
  
  update @TableTemp
     set ConfirmedQuantity = 0
   where ConfirmedQuantity is null
  
  update @TableTemp
     set ConfirmedWeight = 0
   where ConfirmedWeight is null
  
  SET DATEFIRST 1
 
  update @TableTemp
     set DateDiffVal = (SELECT  DATEDIFF(dd,CreateDate,Complete)- COUNT(*) AS WeekDays 
						FROM @WeekEndsAndHolidays 
						WHERE DayOfWeekDate BETWEEN CreateDate AND Complete)
   
  update @TableTemp
     set OnTime = 'Yes'
     where DateDiffVal <= OnTimeHours
     or (DeliveryDate != CreateDate and DeliveryDate >= Complete)

   
  insert @TableResult
        (CustomerName,
		 OrderNumber,
		 OutboundDocumentType,
		 ProductCode,
		 Product,
		 PlannedDespatch,
		 ActualDespatch,
		 InFull,
		 OnTime,
		 OrderLevel,
		 OTIFLine)
  select distinct ec.ExternalCompany,
		 od.OrderNumber,
		 tt.OutboundDocumentType,
		 tt.ProductCode,
		 tt.Product,
		 SUM(tt.Quantity),
		 SUM(tt.ConfirmedQuantity),
		 'No',
		 OnTime,
		 'No',
		 'No'
    from @TableTemp tt
    join OutboundDocument    od (nolock) on tt.OutboundDocumentId = od.OutboundDocumentId
    join ExternalCompany     ec (nolock) on ec.ExternalCompanyId = od.ExternalCompanyId
    where od.OrderNumber = ISNULL(@OrderNumber, od.OrderNumber)
     and tt.Complete between @FromDate and @ToDate
  group by ExternalCompany,
		  od.OrderNumber,
		 OutboundDocumentType,
		 ProductCode,
		 Product,
		 OnTime

        
   update @TableResult
     set InFull = 'Yes'
     from @TableResult
   where PlannedDespatch = ActualDespatch
  
  
   update @TableResult
     set OrderLevel = 'Yes',
         OTIFLine = 'Yes'
   where InFull = 'Yes'
   and   OnTime = 'Yes'
   
   update @TableResult
     set OrderLevel = 'No'
   where OutboundDocumentType in (select OutboundDocumentType from @TableResult where OrderLevel = 'No')
   and   OrderNumber in (select OrderNumber from @TableResult where OrderLevel = 'No')
   
SET ARITHABORT OFF
SET ANSI_WARNINGS OFF
		   
   set @linecountAll = (select COUNT(OrderNumber) from @TableResult)

   set @linecountYes = (select COUNT(OrderNumber) from @TableResult where OTIFLine = 'Yes')

   set @PctOTIF = ((isnull(@linecountYes,0) / isnull(@linecountAll,1)) * 100)
    
  select *, @PctOTIF as OTIFPct  from  @TableResult
  
end
