﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_Search_COA
  ///   Filename       : p_StorageUnitBatch_Search_COA.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007 11:54:26
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null output,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(30) = null,
  ///   @Product nvarchar(50) = null,
  ///   @Barcode nvarchar(50) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   p.ProductId,
  ///   p.StatusId,
  ///   s.Status,
  ///   p.ProductCode,
  ///   p.Product,
  ///   p.Barcode,
  ///   p.MinimumQuantity,
  ///   p.ReorderQuantity,
  ///   p.MaximumQuantity,
  ///   p.CuringPeriodDays,
  ///   p.ShelfLifeDays,
  ///   p.QualityAssuranceIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatch_Search_COA
(
 @ProductCode nvarchar(30) = null,
 @Product     nvarchar(50) = null,
 --@SKUCode     nvarchar(50) = null,
 --@SKU         nvarchar(50) = null,
 @Batch       nvarchar(50) = null
 --@WarehouseId int = 1
)

as
begin
	 set nocount on;
	 
  declare @TableResult as table
  (
   StorageUnitBatchId              int              not null,
   StorageUnitId                   int              not null,
   ProductCode                     nvarchar(60)     not null,
   Product                         nvarchar(100)    not null,
   SKUCode                         nvarchar(20)     not null,
   SKU                             nvarchar(100)    not null,
   Batch                           nvarchar(100)    not null,
   ECLNumber                       nvarchar(20)     null    ,
   Quantity                        int              null    ,
   Location                        nvarchar(30)     null    ,
   Area                            nvarchar(100)    null    ,
   ExpiryDays                      int              null    ,
   StatusId                        int              null
  )
  
  declare @PackTypeId int,
          @StatusId   int,
          @SKUCode     nvarchar(50) = null,
		  @SKU         nvarchar(50) = null,
		  --@Batch       nvarchar(50) = null,
		  @WarehouseId int = 1
  
  select @StatusId = dbo.ufn_StatusId('P','A')
  
  select top 1 @PackTypeId = PackTypeId
    from PackType (nolock)
  order by InboundSequence
  
  if @ProductCode = '-1'
    set @ProductCode = null;
    
  if @ProductCode = ' '
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
    
  if @Product = ' '
    set @Product = null;
  
  if @SKUCode = '-1'
    set @SKUCode = null;
  
  if @Batch = '-1'
    set @Batch = null;
  
  
  if @ProductCode is null and
     @Product is null and
     @SKUCode is null and
     @Batch is null 
    goto result
  
  if (select dbo.ufn_Configuration(241, 1)) = 1 -- Product Search - Batch - 6 char min
  if @Batch is null or datalength(@batch) < 6
    return
  
  insert @TableResult
        (StorageUnitBatchId,
         StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ECLNumber,
         ExpiryDays)
  select sub.StorageUnitBatchId
         ,su.StorageUnitId
         ,p.ProductCode
         ,p.Product
         ,sku.SKUCode
         ,sku.SKU
         ,b.Batch
         ,b.ECLNumber
         ,case when datediff(dd, b.ExpiryDate, dateadd(dd, isnull(p.ShelfLifeDays,0), getdate())) < 0
               then datediff(dd, b.ExpiryDate, dateadd(dd, isnull(p.ShelfLifeDays,0) /* * -365*/, getdate())) * -1
               else datediff(dd, b.ExpiryDate, dateadd(dd, isnull(p.ShelfLifeDays,0), getdate()))
               end as 'ExpiryDays'
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join SKU              sku (nolock) on su.SKUId          = sku.SKUId
    join Product            p (nolock) on su.ProductId      = p.ProductId
    join Batch              b (nolock) on sub.BatchId       = b.BatchId
   where p.ProductCode             like '%' + isnull(null, isnull(p.ProductCode,'%')) + '%'
     and p.Product                 like '%' + isnull(null, isnull(p.Product,'%')) + '%'
     --and sku.SKUCode               like '%' + isnull(@SKUCode, isnull(sku.SKUCode,'%')) + '%'
     --and sku.SKU                   like '%' + isnull(@SKU, isnull(sku.SKU,'%')) + '%'
     and b.Batch                   like '%' + isnull('Default', isnull(b.Batch,'%')) + '%'
  order by p.ProductCode,
           sku.SKUCode,
           'ExpiryDays',
           b.Batch
           
  update tr
     set Quantity = pk.Quantity,
         StatusId = pk.StatusId
    from @TableResult tr
    join Pack         pk (nolock) on pk.StorageUnitId = tr.StorageUnitId
                                 and pk.WarehouseId   = @WarehouseId
                                 and pk.PackTypeId    = @PackTypeId
  
  update tr
     set Location = l.Location
    from @TableResult         tr
    join StorageUnitLocation sul (nolock) on tr.StorageUnitId = sul.StorageUnitId
    join Location              l (nolock) on sul.LocationId   = l.LocationId
    join AreaLocation         al (nolock) on l.LocationId     = al.LocationId
    join Area                  a (nolock) on al.AreaId        = a.AreaId
   where a.AreaCode  = 'PK'
     and tr.StatusId = @StatusId
  
  update tr
     set Area = a.Area
    from @TableResult         tr
    join StorageUnitArea     sua (nolock) on tr.StorageUnitId = sua.StorageUnitId
                                         and sua.StoreOrder   = 1
    join Area                  a (nolock) on sua.AreaId       = a.AreaId
   where tr.StatusId = @StatusId
  
  result:
  print @statusid
  
  select StorageUnitBatchId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ECLNumber,
         Quantity,
         Location,
         Area,
         ExpiryDays
   from @TableResult
  --where StatusId = @StatusId
end
