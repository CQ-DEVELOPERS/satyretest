﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeReference_Search
  ///   Filename       : p_StockTakeReference_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:53
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StockTakeReference table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StockTakeReference.StockTakeReferenceId,
  ///   StockTakeReference.WarehouseId,
  ///   StockTakeReference.StockTakeType,
  ///   StockTakeReference.StartDate,
  ///   StockTakeReference.EndDate,
  ///   StockTakeReference.OperatorId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeReference_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         StockTakeReference.StockTakeReferenceId
        ,StockTakeReference.WarehouseId
        ,StockTakeReference.StockTakeType
        ,StockTakeReference.StartDate
        ,StockTakeReference.EndDate
        ,StockTakeReference.OperatorId
    from StockTakeReference
  
end
