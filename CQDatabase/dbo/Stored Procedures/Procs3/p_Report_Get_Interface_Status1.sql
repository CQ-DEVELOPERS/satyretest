﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Get_Interface_Status1
  ///   Filename       : p_Report_Get_Interface_Status1.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Dec 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 27/12/2010
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Report_Get_Interface_Status1]
(
 @InterfaceTableId	int,
 @DocumentType		Varchar(50),
 @RecordStatus		varchar(50),
 @MessageStatus		varchar(50) = null,
 @StartDate			Datetime,
 @EndDate			datetime,
 @InterfaceId		int = null,
 @Reprocess			varchar(10) = null
)
as

Begin
  insert InterfaceConsoleAudit
        (ProcName,
         InterfaceTableId,
         DocumentType,
         RecordStatus,
         MessageStatus,
         StartDate,
         EndDate,
         InterfaceId,
         Reprocess)
  select object_name(@@procid),
         @InterfaceTableId,
         @DocumentType,
         @RecordStatus,
         @MessageStatus,
         @StartDate,
         @EndDate,
         @InterfaceId,
         @Reprocess


  If (@Reprocess = 'Y' and @InterfaceId is not null)
	  exec p_Interface_Reprocess @InterfaceTableId ,@InterfaceId 

  Create table #InterfaceStatus1 (InterfaceTableId		int,
								  InterfaceId			int,
								  InterfaceTable		varchar(max),
								  Description1			varchar(max),
								  Description2			varchar(max),
								  DocumentType			varchar(max),
								  RecordStatus			varchar(max),
								  MessageStatus			varchar(max),
								  HeaderField1			varchar(max),
								  HeaderField2			varchar(max),
								  HeaderField3			varchar(max),
								  HeaderField4			Varchar(max),
								  HeaderField5			varchar(max),
  								  HeaderField6			varchar(max),
								  HeaderField7			Varchar(max),
								  HeaderField8			varchar(max),
								  InterfaceMessageCode	varchar(max),
								  InterfaceMessage		varchar(max),
								  Createdate			Datetime,
								  Insertdate			datetime,
								  HeaderName1			varchar(max),
								  HeaderName2			varchar(max),
								  HeaderName3			varchar(max),
								  HeaderName4			varchar(max),
								  HeaderName5			varchar(max),
  								  HeaderName6			varchar(max),
								  HeaderName7			varchar(max),
								  HeaderName8			varchar(max),
								  AltGroupByKey			varchar(max))
  	  
  Declare @SqlCommand varchar(max)

  Declare InterfaceTablesCursor cursor for 
  Select InterfaceTableId,
		  HeaderTable,
		  HeaderTableKey,
		  Description1,
		  Description2,
		  StartId,
		  isnull(HeaderField1,'None') as HeaderField1,
		  isnull(HeaderField2,'None') as HeaderField2,
		  isnull(HeaderField3,'None') as HeaderField3,
		  isnull(HeaderField4,'None') as HeaderField4,
		  isnull(HeaderField5,'None') as HeaderField5,		  		  		  
		  isnull(HeaderField6,'None') as HeaderField6,
		  isnull(HeaderField7,'None') as HeaderField7,
		  isnull(HeaderField8,'None') as HeaderField8,
		  AltGroupByKey		  		  		  		  		  
  from InterfaceTables
	  where InterfaceTableId = @InterfaceTableId
  	
  Open InterfaceTablesCursor

  Declare @HeaderTable		varchar(max),
		  @HeaderTableKey	varchar(max),
		  @Description1		varchar(max),
		  @Description2		varchar(max),
		  @StartId			int,
		  @HeaderField1		varchar(max),
		  @HeaderField2		varchar(max),
		  @HeaderField3		varchar(max),
		  @HeaderField4		varchar(max),
		  @HeaderField5		varchar(max),
		  @HeaderField6		varchar(max),
		  @HeaderField7		varchar(max),
		  @HeaderField8		varchar(max),
		  @AltGroupByKey	varchar(max)

  Fetch next from InterfaceTablesCursor into @InterfaceTableId,@HeaderTable ,@HeaderTableKey ,@Description1 ,	@Description2 ,@StartId, @HeaderField1 ,@HeaderField2,@HeaderField3, @HeaderField4, @HeaderField5, @HeaderField6, @HeaderField7, @HeaderField8, @AltGroupByKey

  While @@FETCH_STATUS = 0
  Begin 
		  Set @SqlCommand = 'Insert into #InterfaceStatus1 (InterfaceTableId,InterfaceId,InterfaceTable,Description1,Description2,DocumentType,'
		  Set @SqlCommand = @SqlCommand +  'RecordStatus,MessageStatus,HeaderField1,HeaderField2,HeaderField3,HeaderField4,HeaderField5,HeaderField6,HeaderField7,HeaderField8,InterfaceMessageCode,InterfaceMessage,CreateDate,Insertdate,HeaderName1,HeaderName2,HeaderName3,HeaderName4,HeaderName5,HeaderName6,HeaderName7,HeaderName8) Select '
		  Set @SqlCommand = @SqlCommand +  CONVERT(char(20),@InterfaceTableId) + ' as InterfaceTableId,'
		  Set @SqlCommand = @SqlCommand + ' h.' + @HeaderTableKey + ','
		  Set @SqlCommand = @SqlCommand + '''' + @HeaderTable + '''' + ','
		  Set @SqlCommand = @SqlCommand + '''' + @Description1 + '''' + ','
		  Set @SqlCommand = @SqlCommand + '''' + @Description2 + '''' + ','
		  Set @SqlCommand = @SqlCommand + 'isnull(d.DocDEsc,h.RecordType) as Recordtype,' 								
		  Set @SqlCommand = @SqlCommand + 'h.RecordStatus,'
		  Set @SqlCommand = @SqlCommand + 'm.Status as MessageStatus,'
		  If @HeaderField1 = 'None' 
			  Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as HeaderField1,'
		  Else
			  Set @SqlCommand = @SqlCommand + ' h.' + @HeaderField1 + ','
			  --Set @SqlCommand = @SqlCommand + 'isnull (h.' + @HeaderField1 + ',' + @AltGroupByKey +'),'
		  If @HeaderField2 = 'None' 
			  Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as HeaderField2,'
		  Else
			  Set @SqlCommand = @SqlCommand + ' h.' + @HeaderField2 + ','
		  If @HeaderField3 = 'None' 
			  Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as HeaderField3,'
		  Else
			  Set @SqlCommand = @SqlCommand + ' h.' + @HeaderField3 + ','
		  If @HeaderField4 = 'None' 
			  Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as HeaderField4,'
		  Else
			  Set @SqlCommand = @SqlCommand + ' h.' + @HeaderField4 + ','
		  If @HeaderField5 = 'None' 
			  Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as HeaderField5,'
		  Else
			  Set @SqlCommand = @SqlCommand + ' h.' + @HeaderField5 + ','
		  If @HeaderField6 = 'None' 
			  Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as HeaderField6,'
		  Else
			  Set @SqlCommand = @SqlCommand + ' h.' + @HeaderField6 + ','	
		  If @HeaderField7 = 'None' 
			  Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as HeaderField7,'
		  Else
			  Set @SqlCommand = @SqlCommand + ' h.' + @HeaderField7 + ','
		  If @HeaderField8 = 'None' 
			  Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as HeaderField8,'
		  Else
			  Set @SqlCommand = @SqlCommand + ' h.' + @HeaderField8 + ','
			  			  		  			    			
		  Set @SqlCommand = @SqlCommand + ' m.InterfaceMessageCode, ' 
		  Set @SqlCommand = @SqlCommand + ' m.InterfaceMessage, '
		  Set @SqlCommand = @SqlCommand + ' m.CreateDate,'
		  Set @SqlCommand = @SqlCommand + ' h.InsertDate,'
		  Set @SqlCommand = @SqlCommand + '''' + 'Order Number' + '''' + 'as HeaderName1,'
  		  --Set @SqlCommand = @SqlCommand + ' t.HeaderField1 as HeaderName1,'
  		  Set @SqlCommand = @SqlCommand + ' t.HeaderField2 as HeaderName2,'
  		  Set @SqlCommand = @SqlCommand + ' t.HeaderField3 as HeaderName3,'
  		  Set @SqlCommand = @SqlCommand + ' t.HeaderField4 as HeaderName4,'
  		  Set @SqlCommand = @SqlCommand + ' t.HeaderField5 as HeaderName5,'
  		  Set @SqlCommand = @SqlCommand + ' t.HeaderField6 as HeaderName6,'
  		  Set @SqlCommand = @SqlCommand + ' t.HeaderField7 as HeaderName7,'
  		  Set @SqlCommand = @SqlCommand + ' t.HeaderField8 as HeaderName8'
		  Set @SqlCommand = @SqlCommand + ' From ' + @HeaderTable + ' h (nolock)'
		  set @SqlCommand = @SqlCommand + 'Join InterfaceTables t (nolock) on t.HeaderTable = '''
		  set @SqlCommand = @SqlCommand + @HeaderTable + ''''
		  set @SqlCommand = @SqlCommand + 'left join InterfaceMessage m (nolock) on m.InterfaceId = h.'
		  set @SqlCommand = @SqlCommand + @HeaderTableKey + ' and m.InterfaceTable = '''
		  set @SqlCommand = @SqlCommand + @HeaderTable + ''''
		  set @SqlCommand = @SqlCommand + ' and Isnull(m.Status,''' + 'N' + '''' + ') in (''''  + ''' + 'N' + '''' + ',' + '''' + 'E' + '''' + ')'
		  --set @SqlCommand = @SqlCommand + ' Left Join InterfaceDocumentDesc d (nolock) on d.doc = h.RecordType '
		  set @SqlCommand = @SqlCommand + ' Join InterfaceDocumentDesc d (nolock) on d.doc = h.RecordType '
		  set @SqlCommand = @SqlCommand + ' and  d.DocDesc = ' + '''' + @DocumentType + '''' 
		  if @RecordStatus != 'Unknown'
			  set @SqlCommand = @SqlCommand + ' Join RecordStatus rs (nolock) on rs.StatusCode = h.RecordStatus and rs.StatusDesc = ''' + @RecordStatus + ''''
		  IF @MessageStatus != 'Not Processed'
			  set @SqlCommand = @SqlCommand + ' Join MessageStatus ms (nolock) on ms.StatusCode = m.Status and ms.StatusDesc = ''' + @MessageStatus + ''''
		  Else
			  set @SqlCommand = @SqlCommand + ' and m.Status IS NULL'
		  set @SqlCommand = @SqlCommand + ' left join (select InterfaceId, ''' + 'N' + '''' + ' As Status from InterfaceMessage m1 Where InterfaceTable = '''
		  set @SqlCommand = @SqlCommand + @HeaderTable + '''' 
		  set @SqlCommand = @SqlCommand + ' and Isnull(Status,''' + 'N' + '''' + ') in ('''  + 'N' + '''' + ')'
		  set @SqlCommand = @SqlCommand + ' Group By InterfaceId ) as m1 on m.InterfaceId = m1.InterfaceId '	
		  --set @SqlCommand = @SqlCommand + 'left join InterfaceTables hn on hn.InterfaceTableId = ' + convert(char(20),@InterfaceTableId)
		  set @SqlCommand = @SqlCommand + ' Where ISNULL(h.InsertDate,getdate()) > '''  + CONVERT(CHAR(20),@StartDate) + '''' 
		  set @SqlCommand = @SqlCommand + ' and ISNULL(h.InsertDate,' + '''' + '2000-01-01' + '''' + ') < '''  + CONVERT(CHAR(20),@EndDate) + '''' 
		  set @SqlCommand = @SqlCommand + ' and ' + @HeaderTableKey + ' > ' + CONVERT(char(20), @StartId)
		  set @SqlCommand = @SqlCommand + ' and ISNULL(m1.Status, m.Status) = m.Status '  
		  --set @SqlCommand = @SqlCommand + ' and  d.DocDesc = ' + '''' + @DocumentType + '''' 
		  set @SqlCommand = @SqlCommand + ' Order by '
		  set @SqlCommand = @SqlCommand + 'isnull(d.DocDEsc,h.RecordType),'
		  set @SqlCommand = @SqlCommand + 'h.RecordStatus,'
		  set @SqlCommand = @SqlCommand + 'm.Status'
  		
		  Print @SqlCommand
		  Exec(@SqlCommand)
		  Fetch next from InterfaceTablesCursor into @InterfaceTableId,@HeaderTable ,@HeaderTableKey ,@Description1, @Description2 ,@StartId, @HeaderField1 ,@HeaderField2,@HeaderField3,@HeaderField4,@HeaderField5,@HeaderField6,@HeaderField7,@HeaderField8, @AltGroupByKey
  End
  Close InterfaceTablesCursor
  Deallocate InterfaceTablesCursor
  
  update #InterfaceStatus1
     set RecordStatus = Case when RecordStatus = 'Y' THEN 'Sent' 
			                          when RecordStatus = 'C' THEN 'Imported' 
			                          when RecordStatus = 'E' THEN 'Error' 
			                          when RecordStatus = 'N' THEN 'Waiting' 
			                          when RecordStatus = 'R' THEN 'Reprocessed' 
		                           else 'UNK' end
  
  update #InterfaceStatus1
     set MessageStatus = Case when MessageStatus = 'E' THEN 'Error' 
    		                        when MessageStatus = 'N' THEN 'Successful'
	                             else Case when InsertDate < dateadd(hh,-1,getdate()) Then 'Error' Else '*' end
	                     end
  
  
  update #InterfaceStatus1
     set MessageStatus = '*'
   where RecordStatus = 'Reprocessed'
   

	Select iis.InterfaceTableId,
		iis.InterfaceId,
		iis.Description1,
		iis.Description2,
		iis.DocumentType,
		iis.InterfaceTable,
		iis.RecordStatus,
		iis.MessageStatus,
		iis.HeaderField1,iis.HeaderField2,iis.HeaderField3,iis.HeaderField4,iis.HeaderField5,iis.HeaderField6,iis.HeaderField7,iis.HeaderField8,
		iis.InterfaceMessageCode,
		iis.InterfaceMessage,
		iis.Createdate, 
		iis.HeaderName1,
		iis.HeaderName2,		
		iis.HeaderName3,
		iis.HeaderName4,
		iis.HeaderName5,
		iis.HeaderName6,
		iis.HeaderName7,
		iis.HeaderName8
	from #InterfaceStatus1 iis
end
