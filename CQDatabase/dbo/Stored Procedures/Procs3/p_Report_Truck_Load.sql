﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Truck_Load
  ///   Filename       : p_Report_Truck_Load.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Sep 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Truck_Load
(
 @ConnectionString   nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @OutboundShipmentId int,
 @IssueId            int
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   ExternalCompanyCode	nvarchar(30),
   DespatchDate       datetime,
   LocationId         int,
   Location           nvarchar(15),
   JobId              int,
   StatusId           int,
   Status             nvarchar(50),
   JobStatusId        int,
   JobStatusCode      nvarchar(10),
   InstructionTypeId  int,
   InstructionTypeCode nvarchar(10),
   InstructionType    nvarchar(50),
   InstructionId      int,
   Pallet             nvarchar(10),
   StorageUnitBatchId int,
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   QuantityOnHand     float,
   DropSequence       int,
   RouteId            int,
   Route              nvarchar(50),
   Weight             numeric(13,3),
   Loaded             char(1),
   ReferenceNumber	  nvarchar(30),
   NettWeight	  	  numeric(13,6),
   GrossWeight		  numeric(13,6),
   PrincipalId		  int,
   Principal		  nvarchar(100),
   Cube				  float
  )
  
  declare @isBoxed int = 0
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
	 
	 if @OutboundShipmentId is not null
	   set @IssueId = null
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         OutboundDocumentId,
         JobId,
         StatusId,
         JobStatusId,
         InstructionTypeId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         ReferenceNumber,
         Weight)
  select ili.OutboundShipmentId,
         ili.IssueId,
         ili.OutboundDocumentId,
         ins.JobId,
         ins.StatusId,
         j.StatusId,
         ins.InstructionTypeId,
         ins.InstructionId,
         ins.StorageUnitBatchId,
         ins.Quantity,
         isnull(ili.ConfirmedQuantity, 0),
         ili.Quantity - isnull(ili.ConfirmedQuantity, 0),
         j.ReferenceNumber,
         isnull(ins.ConfirmedWeight,0)
    from IssueLineInstruction  ili (nolock)
    join Instruction           ins (nolock) on ili.InstructionId = isnull(ins.InstructionRefId, ins.InstructionId)
    join Job                     j (nolock) on ins.JobId         = j.JobId
   where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))
     and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))
     --and j.ReferenceNumber is not null
  
  select @isBoxed = count(1)
    from @TableResult
   where ReferenceNumber is not null
   
   if (@isBoxed >0)
   begin
   delete @TableResult
    where ReferenceNumber is null
   end
  
  update tr
     set DespatchDate = os.ShipmentDate,
         RouteId      = os.RouteId,
         DropSequence = osi.DropSequence,
         LocationId   = os.LocationId
    from @TableResult     tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId
   where tr.OutboundShipmentId is not null
  
  update tr
     set DespatchDate = i.DeliveryDate,
         RouteId      = i.RouteId,
         DropSequence = i.DropSequence,
         LocationId   = i.LocationId
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
   where tr.OutboundShipmentId is null
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId,
         PrincipalId	   = od.PrincipalId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set Principal = p.Principal
    from @TableResult    tr
    join Principal        p (nolock) on tr.PrincipalId = p.PrincipalId
    
  update tr
     set ExternalCompany = ec.ExternalCompany,
         ExternalCompanyCode = ec.ExternalCompanyCode
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set Location = l.Location
    from @TableResult    tr
    join Location         l (nolock) on tr.LocationId = l.LocationId
    
  declare @InboundSequence smallint,
		 @PackTypeId	int,
		 @PackType   	nvarchar(30),
		 @ShowWeight	bit = null
		 
  select @ShowWeight = Indicator
  from Configuration where ConfigurationId = 206
 
  select @InboundSequence = max(InboundSequence)
  from PackType (nolock) 
  
  select @PackTypeId = pt.PackTypeId
  from PackType pt  where @InboundSequence = pt.InboundSequence
  
    update tr
     set tr.NettWeight = ((select max(isnull(i.NettWeight,(i.ConfirmedWeight)))
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   --join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   join Instruction				 i (nolock) on i.InstructionId      = tr.InstructionId
					   where pk.PackTypeId = @PackTypeId
                       and sub.StorageUnitBatchId = tr.StorageUnitBatchId
                       and isnull(su.ProductCategory,'') = 'V'))                                             
  from @TableResult tr
  
      update tr
     set tr.GrossWeight = ((select max(isnull(i.ConfirmedWeight,0))
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   --join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   join Instruction				 i (nolock) on i.InstructionId      = tr.InstructionId
					   where pk.PackTypeId = @PackTypeId
                       and sub.StorageUnitBatchId = tr.StorageUnitBatchId
                       and isnull(su.ProductCategory,'') = 'V'))                                             
  from @TableResult tr
  
  
  if  @ShowWeight = 1
      update tr
     set tr.NettWeight = ((select max(pk.NettWeight)
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   --join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   where pk.PackTypeId = @PackTypeId
                       and sub.StorageUnitBatchId = tr.StorageUnitBatchId
                       and isnull(su.ProductCategory,'') != 'V')* tr.ConfirmedQuantity)
    
    from @TableResult tr
    where tr.NettWeight = 0 or tr.NettWeight is null
    
    if  @ShowWeight = 1
      update tr
     set tr.GrossWeight = ((select max(pk.Weight)
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   --join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   where pk.PackTypeId = @PackTypeId
                       and sub.StorageUnitBatchId = tr.StorageUnitBatchId
                       and isnull(su.ProductCategory,'') != 'V')* tr.ConfirmedQuantity)
    
    from @TableResult tr
    where tr.GrossWeight = 0 or tr.GrossWeight is null
    
  update tr
     set Pallet = convert(nvarchar(10), j.DropSequence)
               + ' of '
               + convert(nvarchar(10), j.Pallets)
    from @TableResult tr
    join Job           j (nolock) on tr.JobId = j.JobId
  
  update tr
     set Weight = tr.ConfirmedQuantity * p.Weight,
		 Cube   = p.Length * p.Height * p.Width
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join Pack               p (nolock) on sub.StorageUnitId     = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId = pt.PackTypeId
   where pt.OutboundSequence in (select max(InboundSequence) from PackType)
  
  update tr
     set InstructionType     = 'Full',
         InstructionTypeCode = it.InstructionTypeCode
    from @TableResult tr
    join InstructionType it on tr.InstructionTypeId = it.InstructionTypeId
   where it.InstructionTypeCode = 'P'
  
  update @TableResult
     set InstructionType     = 'Mixed'
   where InstructionType is null
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s on tr.StatusId = s.StatusId
  where tr.InstructionTypeCode = 'P'
    and s.StatusCode           = 'NS'
  
  update tr
     set JobStatusCode = s.StatusCode
    from @TableResult tr
    join Status        s on tr.JobStatusId = s.StatusId
  
  update @TableResult
     set Loaded = 'a'
   where JobStatusCode = 'C'
  
  update @TableResult
     set Loaded = 'r',
         Pallet = null
   where JobStatusCode = 'NS'
  
  update tr
     set Loaded = 'r',
         Status = (select Status from Status where StatusCode = 'NS'),
         Pallet = null
    from @TableResult tr
   where not exists(select 1 from @TableResult tr2 where tr.JobId = tr2.JobId and ConfirmedQuantity > 0)
  
  update @TableResult
     set Loaded = 'c'
   where Loaded is null
  
  select OutboundShipmentId,
         DropSequence,
         Route,
         DespatchDate,
         ExternalCompany,
         ExternalCompanyCode,
         Location,
         min(InstructionType) as 'InstructionType',
         JobId,
         Status,
         OrderNumber,
         Pallet,
         sum(Weight) as 'Weight',
         Loaded,
         ReferenceNumber,
         sum(NettWeight) as NettWeight,
         Principal,
         Cube
    from @TableResult
  group by OutboundShipmentId,
         DropSequence,
         Route,
         DespatchDate,
         ExternalCompany,
         ExternalCompanyCode,
         Location,
         JobId,
         Status,
         OrderNumber,
         Pallet,
         Loaded,
         ReferenceNumber,
         Principal,
         [Cube]
  order by OutboundShipmentId,
           JobId
end
