﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Insert
  ///   Filename       : p_Wave_Planning_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2013 17:32:28
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Wave table.
  /// </remarks>
  /// <param>
  ///   @WaveId int = null output,
  ///   @Wave nvarchar(100) = null,
  ///   @CreateDate datetime = null,
  ///   @ReleasedDate datetime = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  ///   Wave.WaveId,
  ///   Wave.Wave,
  ///   Wave.CreateDate,
  ///   Wave.ReleasedDate,
  ///   Wave.StatusId,
  ///   Wave.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Insert
(
 @WaveId int = null output,
 @Wave nvarchar(100) = null,
 @CreateDate datetime = null,
 @ReleasedDate datetime = null,
 @StatusId int = null,
 @WarehouseId int = null 
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @StatusId = -1
    set @StatusId = null
  
  begin transaction
	 
	 if @Wave is null
	 begin
	   set @Wave = ''
	 --  select @Wave = CONVERT(nvarchar(10), GETDATE(), 112)
    
  --  --select @Wave = '' + @Wave + '_' + CONVERT(nvarchar(10), count(1) + 1)
  --  --  from Wave (nolock)
  --  -- where WarehouseId = @WarehouseId
  --  --   and CreateDate > CONVERT(nvarchar(10), GETDATE(), 120)
	 --  --select @Wave = CONVERT(nvarchar(10), GETDATE(), 112)
    
  --  select @Wave = 'WAV' + CONVERT(nvarchar(10), count(1) + 1)
  --    from Wave (nolock)
  --   --where WarehouseId = @WarehouseId
  end
  
  if @StatusId is null
     set @StatusId = dbo.ufn_StatusId('W','W')
  
  exec @Error = p_Wave_Insert
   @WaveId       = @WaveId output,
   @Wave         = @Wave,
   @CreateDate   = @GetDate,
   @StatusId     = @StatusId,
   @WarehouseId  = @WarehouseId 
  
  select @Error = @@Error
  
  if @Error != 0
    goto error
  
  if @Wave = '' or @Wave is null
    set @Wave = 'WAV' + CONVERT(nvarchar(10), @WaveId)
  
  exec @Error = p_Wave_Update
   @WaveId       = @WaveId,
   @Wave         = @Wave
  
  select @Error = @@Error
  
  if @Error != 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Wave_Planning_Insert'); 
    rollback transaction
    return @Error
  
end
 
