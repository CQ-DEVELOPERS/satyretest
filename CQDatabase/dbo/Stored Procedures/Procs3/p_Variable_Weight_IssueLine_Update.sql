﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Variable_Weight_IssueLine_Update
  ///   Filename       : p_Variable_Weight_IssueLine_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Mar 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Variable_Weight_IssueLine_Update
(
 @IssueId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @Transction        bit
  
  select @GetDate = dbo.ufn_Getdate()
  set @Transction = 0
  
  if @@trancount = 0
  begin
    begin transaction
    set @Transction = 1
  end
  
  declare @Instructions as table
  (
   JobId             int,
   InstructionId     int,
   InstructionRefId  int,
   StorageUnitId     int,
   ConfirmedQuantity float,
   ConfirmedWeight   float,
   PackagingWeight   float,
   PackagingQuantity float,
   NettWeight        float
  )

  declare @Summary as table
  (
   JobId             int,
   InstructionId     int,
   StorageUnitId     int,
   ConfirmedQuantity float,
   ConfirmedWeight   float,
   NettWeight        float
  )

  declare @TableResult as table 
  (
   IssueLineId     int,
   ConfirmedWeight float
  )
  
  insert @Instructions
        (InstructionId)
  select distinct InstructionId
    from IssueLineInstruction (nolock)
   where IssueId = @IssueId
  
  select @Error = @@Error

  if @Error <> 0
    goto error

  insert @Instructions
        (InstructionId,
         InstructionRefId)
  select distinct InstructionId,
                  InstructionRefId
    from Instruction 
   where InstructionRefId in (select InstructionId from @Instructions)
  
  select @Error = @@Error

  if @Error <> 0
    goto error

  update ti
     set JobId             = i.JobId,
         StorageUnitId     = sub.StorageUnitId,
         ConfirmedQuantity = isnull(i.ConfirmedQuantity,0),
         ConfirmedWeight   = isnull(i.ConfirmedWeight,0),
         PackagingWeight   = isnull(i.PackagingWeight,0),
         PackagingQuantity = isnull(i.PackagingQuantity,0)
    from @Instructions ti
    join Instruction    i (nolock) on ti.InstructionId = i.InstructionId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
  
  select @Error = @@Error

  if @Error <> 0
    goto error

  update @Instructions
     set NettWeight = ConfirmedWeight - (PackagingWeight * PackagingQuantity)
  
  select @Error = @@Error

  if @Error <> 0
    goto error

  update @Instructions
     set NettWeight = 0
   where NettWeight < 0
  
  select @Error = @@Error

  if @Error <> 0
    goto error

  insert @Summary
       (JobId,
        InstructionId,
        StorageUnitId,
        ConfirmedQuantity,
        ConfirmedWeight,
        NettWeight)
  select JobId,
         case when InstructionRefId is null
              then InstructionId
              else InstructionRefId
              end,
         StorageUnitId,
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight),
         sum(NettWeight)
    from @Instructions
  group by JobId,
           case when InstructionRefId is null
                then InstructionId
                else InstructionRefId
                end,
           StorageUnitId
  
  select @Error = @@Error

  if @Error <> 0
    goto error

  update @Summary
     set NettWeight = NettWeight / ConfirmedQuantity
   where ConfirmedQuantity > 0
     and NettWeight > 0
  
  select @Error = @@Error

  if @Error <> 0
    goto error

  insert @TableResult
        (IssueLineId,
         ConfirmedWeight)
  select ili.IssueLineId,
         sum(tr.ConfirmedQuantity * tr.NettWeight)
    from @Summary tr
    join IssueLineInstruction ili (nolock) on tr.InstructionId = ili.InstructionId
   where IssueId = @IssueId
  group by ili.IssueLineId
  
  select @Error = @@Error

  if @Error <> 0
    goto error

  update il
     set ConfirmedWeight = tr.ConfirmedWeight
    from @TableResult tr
    join IssueLine    il (nolock) on tr.IssueLineId = il.IssueLineId
  
  select @Error = @@Error

  if @Error <> 0
    goto error
  
  if @Transction = 1
    commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Variable_Weight_IssueLine_Update'); 
    rollback transaction
    return @Error
end
