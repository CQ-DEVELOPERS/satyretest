﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockAdjustment_IBT_Mismatch
  ///   Filename       : p_StockAdjustment_IBT_Mismatch.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Apr 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockAdjustment_IBT_Mismatch
(
 @ReceiptId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @InterfaceExportHeaderId         int,
          @IssueId                         int,
          @PrimaryKey                      nvarchar(30),
          @OrderNumber                     nvarchar(30),
          @RecordType                      nvarchar(30),
          @RecordStatus                    char(1),
          @CompanyCode                     nvarchar(30),
          @Company                         nvarchar(255),
          @Address                         nvarchar(255),
          @FromWarehouseCode               nvarchar(50),
          @ToWarehouseCode                 nvarchar(50),
          @Route                           nvarchar(50),
          @DeliveryNoteNumber              nvarchar(30),
          @ContainerNumber                 nvarchar(30),
          @SealNumber                      nvarchar(30),
          @DeliveryDate                    datetime,
          @Remarks                         nvarchar(255),
          @NumberOfLines                   int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  insert InterfaceExportHeader
        (PrimaryKey,
         OrderNumber,
         RecordType,
         RecordStatus,
         CompanyCode,
         Company,
         FromWarehouseCode,
         ToWarehouseCode,
         DeliveryNoteNumber,
         SealNumber,
         DeliveryDate,
         Remarks,
         NumberOfLines)
  select id.OrderNumber,
         id.OrderNumber,
         'IBT',
         'N',
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         w.WarehouseCode,
         ec.ExternalCompanyCode,
         r.DeliveryNoteNumber,
         r.SealNumber,
         r.DeliveryDate,
         r.Remarks,
         null
    from Receipt               r (nolock)
    join Warehouse             w (nolock) on r.WarehouseId = w.WarehouseId
    join InboundDocument      id (nolock) on r.InboundDocumentId   = id.InboundDocumentId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    left
    join ExternalCompany      ec (nolocK) on id.ExternalCompanyId = ec.ExternalCompanyId
   where r.ReceiptId = @ReceiptId
  
  select @Error = @@error, @InterfaceExportHeaderId = SCOPE_IDENTITY()
  
  if @Error <> 0
    goto error
  
  insert InterfaceExportDetail
        (InterfaceExportHeaderId,
         ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight,
         Additional3)
  select @InterfaceExportHeaderId,
         i.OrderNumber,
         il.LineNumber,
         p.ProductCode,
         p.Product,
         sk.SKUCode,
         b.Batch,
         rl.RequiredQuantity - rl.ReceivedQuantity,
         rl.AcceptedWeight,
         '01'
    from ReceiptLine       rl
    join Receipt            r (nolock) on rl.ReceiptId = r.ReceiptId
    join InboundDocument    i (nolock) on r.InboundDocumentId = i.InboundDocumentId
    join InboundLine       il (nolock) on rl.InboundLineId = il.InboundLineId
    left
    join ExternalCompany   ec (nolock) on i.ExternalCompanyId = ec.ExternalCompanyId
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Sku               sk (nolock) on su.SkuId = sk.SkuId
    join Product            p (nolock) on su.ProductId = p.ProductId
    join Batch              b (nolock) on sub.BatchId = b.BatchId
   where r.ReceiptId = @ReceiptId
     and rl.RequiredQuantity != rl.ReceivedQuantity
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_StockAdjustment_IBT_Mismatch'); 
    rollback transaction
    return @Error
end
 
