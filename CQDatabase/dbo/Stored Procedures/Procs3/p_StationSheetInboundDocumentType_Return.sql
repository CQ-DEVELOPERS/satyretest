﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StationSheetInboundDocumentType_Return
  ///   Filename       : p_StationSheetInboundDocumentType_Return.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2009 17:55:53
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundDocumentType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InboundDocumentType.InboundDocumentTypeId,
  ///   InboundDocumentType.InboundDocumentType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StationSheetInboundDocumentType_Return

as
begin
	 set nocount on;
  
	 select InboundDocumentType.InboundDocumentTypeId,
         InboundDocumentType.InboundDocumentType 
    from InboundDocumentType
   where InboundDocumentTypeCode = 'RET'
end
