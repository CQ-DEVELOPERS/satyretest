﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Despatch_Advice_Shipment_Search_test
  ///   Filename       : p_Report_Despatch_Advice_Shipment_Search_test.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Despatch_Advice_Shipment_Search_test
(
 @WarehouseId				int,
 @OutboundShipmentId	    int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode		nvarchar(30),
 @ExternalCompany			nvarchar(255),
 @FromDate					datetime,
 @ToDate	                datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundDocumentId        int,
   OutboundDocumentType      nvarchar(30),
   IssueId                   int,
   OutboundShipmentId        int,
   CustomerCode              nvarchar(30),
   Customer                  nvarchar(255),
   RouteId                   int,
   Route                     nvarchar(50),
   NumberOfLines             int,
   DeliveryDate              datetime,
   StatusId                  int,
   Status                    nvarchar(50),
   LoadIndicator             bit,
   Orders					 int,
   TotalJobs				 int,
   JobsStarted				 int,
   JobsChecking				 int,
   JobsChecked				 int,
   JobsDespatch				 int,
   JobsComplete				 int,
   DespatchLocationId		 int,
   DespatchLocation			 nvarchar(15)
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @OutboundShipmentId is null
  begin
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           DeliveryDate,
           LoadIndicator,
           NumberOfLines)
    select ili.OutboundShipmentId,
           ili.OutboundDocumentId,
           ili.IssueId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.DeliveryDate,
           i.LoadIndicator,
           i.NumberOfLines
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId     = ins.InstructionId
      join InstructionType        it (nolock) on ins.InstructionTypeId = it.InstructionTypeId
      join Job                     j (nolock) on ins.JobId             = j.JobId
      join OutboundDocument       od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
      join ExternalCompany        ec (nolock) on od.ExternalCompanyId  = ec.ExternalCompanyId
      join Issue                   i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
      join Status                  s (nolock) on i.StatusId            = s.StatusId
     where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
       and od.CreateDate       between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('PC','RL','CK','CD','DC','C')
       and od.WarehouseId            = @WarehouseId
    --select od.OutboundDocumentId,
    --       i.IssueId,
    --       ec.ExternalCompanyCode,
    --       ec.ExternalCompany,
    --       i.RouteId,
    --       i.StatusId,
    --       s.Status,
    --       i.DeliveryDate,
    --       i.LoadIndicator,
    --       i.NumberOfLines
    --  from OutboundDocument     od  (nolock)
    --  join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
    --  join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    --  join Status               s   (nolock) on i.StatusId               = s.StatusId
    --  join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    -- where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
    --   and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
    --   and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
    --   and od.CreateDate       between @FromDate and @ToDate
    --   and s.Type                    = 'IS'
    --   and s.StatusCode             in ('PC','RL','CK','CD','DC','C')
    --   and od.WarehouseId            = @WarehouseId
  end
  else
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           DeliveryDate,
           LoadIndicator,
           NumberOfLines)
           select ili.OutboundShipmentId,
           ili.OutboundDocumentId,
           ili.IssueId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.DeliveryDate,
           i.LoadIndicator,
           i.NumberOfLines
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId     = ins.InstructionId
      join InstructionType        it (nolock) on ins.InstructionTypeId = it.InstructionTypeId
      join Job                     j (nolock) on ins.JobId             = j.JobId
      join OutboundDocument       od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
      join ExternalCompany        ec (nolock) on od.ExternalCompanyId  = ec.ExternalCompanyId
      join Issue                   i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
      join Status                  s (nolock) on i.StatusId            = s.StatusId
    --select osi.OutboundShipmentId,
    --       od.OutboundDocumentId,
    --       i.IssueId,
    --       ec.ExternalCompanyCode,
    --       ec.ExternalCompany,
    --       i.RouteId,
    --       i.StatusId,
    --       s.Status,
    --       i.DeliveryDate,
    --       i.LoadIndicator,
    --       i.NumberOfLines
    --  from OutboundDocument     od  (nolock)
    --  join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
    --  join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    --  join Status               s   (nolock) on i.StatusId               = s.StatusId
    --  join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    --  join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
     where ili.OutboundShipmentId = @OutboundShipmentId
       and s.Type                 = 'IS'
       and s.StatusCode             in ('PC','RL','CK','CD','DC','C')
  
  update tr
     set OutboundShipmentId = si.OutboundShipmentId,
--         DeliveryDate = os.ShipmentDate,
         Route        = os.Route,
         RouteId      = os.RouteId,
         StatusId     = os.StatusId
    from @TableResult  tr
    join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
    join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
  
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
    
    update tr
     set orders = (select COUNT(od.ordernumber) from OutboundDocument od 
					where tr.OutboundDocumentId = od.OutboundDocumentId)
    from @TableResult  tr

    update tr
     set TotalJobs = (select COUNT(j.JobId) 
						from OutboundDocument od
						join Issue i on i.OutboundDocumentId = od.OutboundDocumentId
						join IssueLine il on il.IssueId = i.IssueId 
						join Job j on j.IssueLineId = il.IssueLineId
						where tr.OutboundDocumentId = od.OutboundDocumentId)
    from @TableResult  tr  
    
    update tr
     set JobsStarted = (select COUNT(j.JobId) 
						from OutboundDocument od
						join Issue i on i.OutboundDocumentId = od.OutboundDocumentId
						join IssueLine il on il.IssueId = i.IssueId 
						join Job j on j.IssueLineId = il.IssueLineId
						join Status s on j.StatusId = s.StatusId
						where tr.OutboundDocumentId = od.OutboundDocumentId
						and s.Status = 'Started')
						
    from @TableResult  tr
    
    update tr
     set JobsChecking = (select COUNT(j.JobId) 
						from OutboundDocument od
						join Issue i on i.OutboundDocumentId = od.OutboundDocumentId
						join IssueLine il on il.IssueId = i.IssueId 
						join Job j on j.IssueLineId = il.IssueLineId
						join Status s on j.StatusId = s.StatusId
						where tr.OutboundDocumentId = od.OutboundDocumentId
						and s.Status = 'Checking')
    from @TableResult  tr
    
    update tr
     set JobsChecked = (select COUNT(j.JobId) 
						from OutboundDocument od
						join Issue i on i.OutboundDocumentId = od.OutboundDocumentId
						join IssueLine il on il.IssueId = i.IssueId 
						join Job j on j.IssueLineId = il.IssueLineId
						join Status s on j.StatusId = s.StatusId
						where tr.OutboundDocumentId = od.OutboundDocumentId
						and s.Status = 'Checked')
    from @TableResult  tr
    
    
    update tr
     set JobsDespatch = (select COUNT(j.JobId) 
						from OutboundDocument od
						join Issue i on i.OutboundDocumentId = od.OutboundDocumentId
						join IssueLine il on il.IssueId = i.IssueId 
						join Job j on j.IssueLineId = il.IssueLineId
						join Status s on j.StatusId = s.StatusId
						where tr.OutboundDocumentId = od.OutboundDocumentId
						and s.Status = 'Despatch')
    from @TableResult  tr
    
    update tr
     set JobsComplete = (select COUNT(j.JobId) 
						from OutboundDocument od
						join Issue i on i.OutboundDocumentId = od.OutboundDocumentId
						join IssueLine il on il.IssueId = i.IssueId 
						join Job j on j.IssueLineId = il.IssueLineId
						join Status s on j.StatusId = s.StatusId
						where tr.OutboundDocumentId = od.OutboundDocumentId
						and s.Status = 'Complete')
    from @TableResult  tr
  
  select IssueId,
         isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
         CustomerCode,
         Customer,
         isnull(RouteId,-1) as 'RouteId',
         Route,
         NumberOfLines,
         DeliveryDate,
         Status,
         Orders,
         TotalJobs,
         JobsStarted,
         JobsChecking,
         JobsChecked,
         JobsDespatch,
         JobsComplete,
         DespatchLocationId,
         DespatchLocation
    from @TableResult
  order by Status desc,
           OutboundShipmentId

end
