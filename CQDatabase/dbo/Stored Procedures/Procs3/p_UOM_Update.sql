﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_UOM_Update
  ///   Filename       : p_UOM_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:59
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the UOM table.
  /// </remarks>
  /// <param>
  ///   @UOMId int = null,
  ///   @UOM nvarchar(100) = null,
  ///   @UOMCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_UOM_Update
(
 @UOMId int = null,
 @UOM nvarchar(100) = null,
 @UOMCode nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
  if @UOMId = '-1'
    set @UOMId = null;
  
  if @UOM = '-1'
    set @UOM = null;
  
  if @UOMCode = '-1'
    set @UOMCode = null;
  
	 declare @Error int
 
  update UOM
     set UOM = isnull(@UOM, UOM),
         UOMCode = isnull(@UOMCode, UOMCode) 
   where UOMId = @UOMId
  
  select @Error = @@Error
  
  
  return @Error
  
end
