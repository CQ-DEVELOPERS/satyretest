﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnit_Search
  ///   Filename       : p_StorageUnit_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:02:12
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StorageUnit table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null output,
  ///   @SKUId int = null,
  ///   @ProductId int = null,
  ///   @StatusId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StorageUnit.StorageUnitId,
  ///   StorageUnit.SKUId,
  ///   SKU.SKU,
  ///   StorageUnit.ProductId,
  ///   Product.Product,
  ///   StorageUnit.ProductCategory,
  ///   StorageUnit.PackingCategory,
  ///   StorageUnit.PickEmpty,
  ///   StorageUnit.StackingCategory,
  ///   StorageUnit.MovementCategory,
  ///   StorageUnit.ValueCategory,
  ///   StorageUnit.StoringCategory,
  ///   StorageUnit.PickPartPallet,
  ///   StorageUnit.MinimumQuantity,
  ///   StorageUnit.ReorderQuantity,
  ///   StorageUnit.MaximumQuantity,
  ///   StorageUnit.DefaultQC,
  ///   StorageUnit.UnitPrice,
  ///   StorageUnit.Size,
  ///   StorageUnit.Colour,
  ///   StorageUnit.Style,
  ///   StorageUnit.PreviousUnitPrice,
  ///   StorageUnit.StockTakeCounts,
  ///   StorageUnit.SerialTracked,
  ///   StorageUnit.SizeCode,
  ///   StorageUnit.ColourCode,
  ///   StorageUnit.StyleCode,
  ///   StorageUnit.StatusId,
  ///   Status.Status,
  ///   StorageUnit.PutawayRule,
  ///   StorageUnit.AllocationRule,
  ///   StorageUnit.BillingRule,
  ///   StorageUnit.CycleCountRule,
  ///   StorageUnit.LotAttributeRule,
  ///   StorageUnit.StorageCondition,
  ///   StorageUnit.ManualCost,
  ///   StorageUnit.OffSet 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnit_Search
(
 @StorageUnitId int = null output,
 @SKUId int = null,
 @ProductId int = null,
 @StatusId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @SKUId = '-1'
    set @SKUId = null;
  
  if @ProductId = '-1'
    set @ProductId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
 
  select
         StorageUnit.StorageUnitId
        ,StorageUnit.SKUId
         ,SKUSKUId.SKU as 'SKU'
        ,StorageUnit.ProductId
         ,ProductProductId.Product as 'Product'
        ,StorageUnit.ProductCategory
        ,StorageUnit.PackingCategory
        ,StorageUnit.PickEmpty
        ,StorageUnit.StackingCategory
        ,StorageUnit.MovementCategory
        ,StorageUnit.ValueCategory
        ,StorageUnit.StoringCategory
        ,StorageUnit.PickPartPallet
        ,StorageUnit.MinimumQuantity
        ,StorageUnit.ReorderQuantity
        ,StorageUnit.MaximumQuantity
        ,StorageUnit.DefaultQC
        ,StorageUnit.UnitPrice
        ,StorageUnit.Size
        ,StorageUnit.Colour
        ,StorageUnit.Style
        ,StorageUnit.PreviousUnitPrice
        ,StorageUnit.StockTakeCounts
        ,StorageUnit.SerialTracked
        ,StorageUnit.SizeCode
        ,StorageUnit.ColourCode
        ,StorageUnit.StyleCode
        ,StorageUnit.StatusId
         ,StatusStatusId.Status as 'Status'
        ,StorageUnit.PutawayRule
        ,StorageUnit.AllocationRule
        ,StorageUnit.BillingRule
        ,StorageUnit.CycleCountRule
        ,StorageUnit.LotAttributeRule
        ,StorageUnit.StorageCondition
        ,StorageUnit.ManualCost
        ,StorageUnit.OffSet
    from StorageUnit
    left
    join SKU SKUSKUId on SKUSKUId.SKUId = StorageUnit.SKUId
    left
    join Product ProductProductId on ProductProductId.ProductId = StorageUnit.ProductId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = StorageUnit.StatusId
   where isnull(StorageUnit.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(StorageUnit.StorageUnitId,'0'))
     and isnull(StorageUnit.SKUId,'0')  = isnull(@SKUId, isnull(StorageUnit.SKUId,'0'))
     and isnull(StorageUnit.ProductId,'0')  = isnull(@ProductId, isnull(StorageUnit.ProductId,'0'))
     and isnull(StorageUnit.StatusId,'0')  = isnull(@StatusId, isnull(StorageUnit.StatusId,'0'))
  order by ProductCategory
  
end
