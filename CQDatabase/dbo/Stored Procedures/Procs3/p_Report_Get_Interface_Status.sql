﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Get_Interface_Status
  ///   Filename       : p_Report_Get_Interface_Status.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Dec 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Get_Interface_Status    
(    
 @StartDate datetime,    
 @EndDate   datetime,    
 @RecordStatus varchar(20) = null,    
 @MessageStatus varchar(50) = null    
)    
    
as    
begin
   set nocount on
   
  insert InterfaceConsoleAudit
        (ProcName,
         RecordStatus,
         MessageStatus,
         StartDate,
         EndDate)
  select object_name(@@procid),
         @RecordStatus,
         @MessageStatus,
         @StartDate,
         @EndDate
  
  Create table #InterfaceStatus (InterfaceTableId int,    
          Description1 varchar(max),    
          Description2 varchar(max),    
          DocumentType varchar(max),    
          RecordStatus varchar(max),    
          MessageStatus varchar(max), 
          InterfaceMessageCode varchar(max),   
          IdCount int)    
    
  Declare @SqlCommand varchar(max)    
    
  Declare InterfaceTablesCursor cursor for     
  Select InterfaceTableId,    
			HeaderTable,    
			HeaderTableKey,    
			GroupByKey,    
			Description1,    
			Description2,    
			StartId,
			AltGroupByKey   
       
  from InterfaceTables (nolock)    
    
  Open InterfaceTablesCursor    
    
  Declare @InterfaceTableId int,    
    @HeaderTable Varchar(max),    
    @HeaderTableKey Varchar(max),    
    @GroupByKey varchar(max),    
    @Description1 varchar(max),    
    @Description2 Varchar(max),    
    @StartId int,
    @AltGroupByKey varchar(max)    
      
  if @RecordStatus is null    
    set @RecordStatus = ''    
      
  if @MessageStatus is null    
    set @MessageStatus = ''    
        
  Fetch next from InterfaceTablesCursor into @InterfaceTableId,@HeaderTable ,@HeaderTableKey ,@GroupByKey, @Description1 , @Description2 ,@StartId, @AltGroupByKey    
    
  While @@FETCH_STATUS = 0    
  Begin    
    Set @SqlCommand = 'Insert into #InterfaceStatus (InterfaceTableId,Description1,Description2,DocumentType,RecordStatus,MessageStatus,IdCount) Select '    
    Set @SqlCommand = @SqlCommand +  CONVERT(char(20),@InterfaceTableId) + ' as InterfaceTableId,'    
    Set @SqlCommand = @SqlCommand + '''' + @Description1 + '''' + ','    
    Set @SqlCommand = @SqlCommand + '''' + @Description2 + '''' + ','    
    Set @SqlCommand = @SqlCommand + 'isnull(d.DocDEsc,h.RecordType) as Recordtype,'             
    Set @SqlCommand = @SqlCommand + 'h.RecordStatus,'    
    Set @SqlCommand = @SqlCommand + 'm.Status as MessageStatus,'  
    --Set @SqlCommand = @SqlCommand + 'COUNT(DISTINCT h.'    
    --Set @SqlCommand = @SqlCommand + @GroupByKey + ') as idcount From '  
    Set @SqlCommand = @SqlCommand + 'COUNT(DISTINCT isnull(h.'  
    Set @SqlCommand = @SqlCommand + @GroupByKey + ',h.' + isnull(@AltGroupByKey,@GroupByKey) +')) as idcount From ' 
    Set @SqlCommand = @SqlCommand + @HeaderTable + ' h (nolock) '    
    set @SqlCommand = @SqlCommand + 'Join InterfaceTables t (nolock) on t.HeaderTable = '''    
    set @SqlCommand = @SqlCommand + @HeaderTable + ''''    
    set @SqlCommand = @SqlCommand + 'left join InterfaceMessage m (nolock) on m.InterfaceId = h.'    
    set @SqlCommand = @SqlCommand + @HeaderTableKey + ' and m.InterfaceTable = '''    
    set @SqlCommand = @SqlCommand + @HeaderTable + ''''    
    set @SqlCommand = @SqlCommand + ' Left Join InterfaceDocumentDesc d (nolock) on d.doc = h.RecordType '  
    set @SqlCommand = @SqlCommand + ' left join (select InterfaceId, ''' + 'N' + '''' + ' As Status from InterfaceMessage m1 Where InterfaceTable = '''
    set @SqlCommand = @SqlCommand + @HeaderTable + '''' 
    set @SqlCommand = @SqlCommand + ' and Isnull(Status,''' + 'N' + '''' + ') in ('''  + 'N' + '''' + ')'
    set @SqlCommand = @SqlCommand + ' Group By InterfaceId ) as m1 on m.InterfaceId = m1.InterfaceId '	  
    set @SqlCommand = @SqlCommand + ' where ISNULL(h.InsertDate,' + '''' + '2000-01-01' + '''' + ') > '''  + CONVERT(CHAR(20),@StartDate) + ''''     
    set @SqlCommand = @SqlCommand + ' and ISNULL(h.InsertDate,' + '''' + '2000-01-01' + '''' + ') < '''  + CONVERT(CHAR(20),@EndDate) + ''''     
    set @SqlCommand = @SqlCommand + ' and Isnull(m.Status,''' + 'N' + '''' + ') in (''''  + ''' + 'N' + '''' + ',' + '''' + 'E' + '''' + ')'    
    set @SqlCommand = @SqlCommand + ' and ' + @HeaderTableKey + ' > ' + CONVERT(char(20), @StartId) 
    set @SqlCommand = @SqlCommand + ' and ISNULL(m1.Status, m.Status) = m.Status '   
    set @SqlCommand = @SqlCommand + ' Group by '    
    set @SqlCommand = @SqlCommand + 'isnull(d.DocDesc,h.RecordType),'    
    set @SqlCommand = @SqlCommand + 'h.RecordStatus,'    
    set @SqlCommand = @SqlCommand + 'm.Status'    
    set @SqlCommand = @SqlCommand + ' Order by '    
    set @SqlCommand = @SqlCommand + 'isnull(d.DocDesc,h.RecordType),'    
    set @SqlCommand = @SqlCommand + 'h.RecordStatus,'    
    set @SqlCommand = @SqlCommand + 'm.Status'    
        
        
    Print @SqlCommand    
        
    Exec(@SqlCommand)    
        
        
        
    
    Fetch next from InterfaceTablesCursor into @InterfaceTableId,@HeaderTable ,@HeaderTableKey ,@GroupByKey, @Description1 ,@Description2 ,@StartId, @AltGroupByKey    
  End    
  Insert into #InterfaceStatus     
      (InterfaceTableId,    
       Description1,    
       Description2,    
       DocumentType,    
       RecordStatus,    
       MessageStatus,
       InterfaceMessageCode,  
       IdCount)     
    Select 99 as InterfaceTableId,    
     'From Host',    
     'Receiving',    
     d.DocDEsc as Recordtype,    
     'C',    
     m.Status as MessageStatus,   
     m.InterfaceMessageCode, 
     COUNT(h.InterfaceImportIPHeaderId) as idcount     
    From InterfaceImportIPHeader h (nolock)    
    join InterfaceMessage m (nolock) on m.InterfaceId = h.InterfaceImportIPHeaderId     
    and m.InterfaceTable = 'InterfaceImportIPHeader'     
    Left Join InterfaceDocumentDesc d (nolock) on d.doc = 'INV'      
    where ISNULL(h.InsertDate,'2000-01-01') >  CONVERT(CHAR(20),@StartDate)     
    and ISNULL(h.InsertDate,'2000-01-01') < CONVERT(CHAR(20),@EndDate)     
    and Isnull(m.Status,'N') in (''  + 'N','E')     
                       
    Group by d.DocDesc,m.Status,m.InterfaceMessageCode     
    Order by d.DocDesc,m.Status    
        
  Close InterfaceTablesCursor    
  Deallocate InterfaceTablesCursor    
      
  update #InterfaceStatus    
     set RecordStatus = Case          when RecordStatus = 'Y' THEN 'Sent'     
                             when RecordStatus = 'C' THEN 'Imported'     
                             when RecordStatus = 'E' THEN 'Error'     
                             when RecordStatus = 'N' THEN 'Waiting'     
                             when RecordStatus = 'R' THEN 'Reprocessed'     
                             else 'Unknown' end    
      
  update #InterfaceStatus    
     set MessageStatus = Case when InterfaceMessageCode = 'Failed' Then 'Error'
							  when InterfaceMessageCode = 'Success' Then 'Error'
								When MessageStatus = 'E' THEN 'Error'     
								when MessageStatus = 'N' THEN 'Successful'     
                              else 'Not Processed' end    
      
  update #InterfaceStatus    
     set MessageStatus = '*'    
   where RecordStatus = 'Reprocessed'    
      
      
  if @RecordStatus = '' or @MessageStatus = ''    
    Select InterfaceTableId,    
           Description1,    
           Description2,    
           DocumentType,    
           RecordStatus,    
           MessageStatus,     
           IdCount,    
            CASE RecordStatus            
     WHEN 'Error'              
     THEN            
     CASE MessageStatus                    
     WHEN 'Error'                      
     THEN 'Received' + ' ' + MessageStatus    
     ELSE RecordStatus + ' ' + MessageStatus         
     END      
     ELSE RecordStatus + ' ' + MessageStatus            
     END  AS 'Status'    
--           RecordStatus + ' ' + MessageStatus as 'Status'    
    
      from #InterfaceStatus    
  else    
    Select InterfaceTableId,    
           Description1,    
           Description2,    
           DocumentType,    
           RecordStatus,    
           MessageStatus,     
           IdCount,    
--           RecordStatus + ' ' + MessageStatus as 'Status',    
           CASE RecordStatus            
     WHEN 'Error'              
     THEN            
     CASE MessageStatus                    
     WHEN 'Error'                      
     THEN 'Received' + ' ' + MessageStatus    
     ELSE RecordStatus + ' ' + MessageStatus         
     END       
     ELSE RecordStatus + ' ' + MessageStatus           
     END  AS 'Status'    
      from #InterfaceStatus    
     where RecordStatus = @RecordStatus    
       and MessageStatus = @MessageStatus    
end    
