﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Production_Authorisation
  ///   Filename       : p_Report_Production_Authorisation.sql
  ///   Create By      : Karen
  ///   Date Created   : Jun 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Production_Authorisation
(
 @WarehouseId	int,
 @OrderNumber	nvarchar(30),
 @ProductCode	nvarchar(30),
 @Batch			nvarchar(50),
 @FromDate	    datetime,
 @ToDate	    datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        InboundShipmentId         int,
        InboundDocumentId         int,
        ReceiptId                 int,
        OrderNumber               nvarchar(30),
        DeliveryDate              datetime,
        PlannedDeliveryDate       datetime,
        CreateDate                datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        LocationId                int,
        Location                  nvarchar(15),
        ProductCode				  nvarchar(30),
        Product					  nvarchar(50),
        Batch					  nvarchar(50),
        RequiredQuantity		  float,
        DeliveryNoteQuantity      float,
        AcceptedQuantity          float,
        AdjustmentQuantity        float,
        Dwelltime				  int,
        AvailabilityIndicator     nvarchar(20),
        FilledVariance			  int,
        ConfirmedVariance		  int		  
       );
  
  insert @TableResult
        (InboundDocumentId,
         ReceiptId,
         LocationId,
         OrderNumber,
         StatusId,
         Status,
         DeliveryDate,
         PlannedDeliveryDate,
         CreateDate,
         ProductCode,
         Product,
         Batch)
  select id.InboundDocumentId,
         r.ReceiptId,
         r.LocationId,
         id.OrderNumber,
         r.StatusId,
         s.Status,
         r.DeliveryDate,
         id.DeliveryDate,
         id.CreateDate,
         p.ProductCode,
         p.Product,
         ba.Batch
    from InboundDocument     id  (nolock)
    join Receipt             r   (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join Status              s   (nolock) on r.StatusId           = s.StatusId
    join StorageUnit         su  (nolock) on id.StorageUnitId	    = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId		       = p.ProductId
    join Batch               ba  (nolock) on id.BatchId				       = ba.BatchId
   where id.WarehouseId              = @WarehouseId
     and s.StatusCode               in ('W','C','D','S','P','R','LA') -- Waiting, Confirmed, Delivered, Started, Palletised, Received, Locations Allocated
     and p.ProductCode            like isnull(@ProductCode  + '%', p.ProductCode)
     and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
     and isnull(r.DeliveryDate, id.DeliveryDate) between @FromDate and @ToDate
  
  update r
     set InboundShipmentId = isr.InboundShipmentId
    from @TableResult             r
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
  
  update r
     set Location = l.Location
    from @TableResult r
    join Location     l (nolock) on r.LocationId = l.LocationId
	
  update tr
     set AvailabilityIndicator = 'Red'
    from @TableResult tr
    
  update tr
     set AvailabilityIndicator = 'Red'
    from @TableResult tr
   where (datediff(hh, GETDATE(), PlannedDeliveryDate)) > -24
  
  update tr
     set AvailabilityIndicator = 'Yellow'
    from @TableResult tr
   where datediff(hh, GETDATE(), PlannedDeliveryDate) between -24 and 12
  
  update tr
     set AvailabilityIndicator = 'Green'
    from @TableResult tr
   where datediff(HH, GETDATE(), PlannedDeliveryDate) > 12
  
  update tr
     set RequiredQuantity     = rl.RequiredQuantity,
         DeliveryNoteQuantity = rl.DeliveryNoteQuantity,
         AcceptedQuantity     = rl.AcceptedQuantity,
         AdjustmentQuantity   = rl.RejectQuantity
    from @TableResult tr
    join ReceiptLine  rl (nolock) on tr.ReceiptId = rl.ReceiptId
    
    update tr
     set FilledVariance     = ((isnull(RequiredQuantity,0) - isnull(DeliveryNoteQuantity,0)) / isnull(RequiredQuantity,0) * 100)
    from @TableResult tr
  
	SET ARITHABORT OFF
	SET ANSI_WARNINGS OFF   

	update tr
     set ConfirmedVariance = isnull(((DeliveryNoteQuantity - isnull(AcceptedQuantity,0)) / (isnull(DeliveryNoteQuantity,1) * 100)),0)
    from @TableResult tr
    
   update tr
     set ConfirmedVariance = 100
    from @TableResult tr
    where ConfirmedVariance = 0 and DeliveryNoteQuantity <> AcceptedQuantity
  
  
  select isnull(InboundShipmentId,-1) as 'InboundShipmentId',
         ReceiptId,
         OrderNumber,
         DeliveryDate,
         PlannedDeliveryDate,
         CreateDate,
         Status,
         isnull(LocationId,-1) as LocationId,	
         Location,
         ProductCode,
         Product,
         Batch,
         RequiredQuantity,
         DeliveryNoteQuantity,
         AcceptedQuantity,
         --(DeliveryNoteQuantity - AcceptedQuantity) as 'AdjustmentQuantity',
         AdjustmentQuantity,
         Dwelltime,
         AvailabilityIndicator,
         FilledVariance,
         ConfirmedVariance		  
    from @TableResult
end
