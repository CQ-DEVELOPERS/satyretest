﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Warehouse_Update
  ///   Filename       : p_Warehouse_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 May 2014 13:32:00
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Warehouse table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null,
  ///   @CompanyId int = null,
  ///   @Warehouse nvarchar(100) = null,
  ///   @WarehouseCode nvarchar(60) = null,
  ///   @HostId nvarchar(60) = null,
  ///   @ParentWarehouseId int = null,
  ///   @AddressId int = null,
  ///   @DesktopMaximum int = null,
  ///   @DesktopWarning int = null,
  ///   @MobileWarning int = null,
  ///   @MobileMaximum int = null,
  ///   @UploadToHost bit = null,
  ///   @PostalAddressId int = null,
  ///   @ContactListId int = null,
  ///   @PrincipalId int = null,
  ///   @AllowInbound bit = null,
  ///   @AllowOutbound bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Warehouse_Update
(
 @WarehouseId int = null,
 @CompanyId int = null,
 @Warehouse nvarchar(100) = null,
 @WarehouseCode nvarchar(60) = null,
 @HostId nvarchar(60) = null,
 @ParentWarehouseId int = null,
 @AddressId int = null,
 @DesktopMaximum int = null,
 @DesktopWarning int = null,
 @MobileWarning int = null,
 @MobileMaximum int = null,
 @UploadToHost bit = null,
 @PostalAddressId int = null,
 @ContactListId int = null,
 @PrincipalId int = null,
 @AllowInbound bit = null,
 @AllowOutbound bit = null 
)

as
begin
	 set nocount on;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @CompanyId = '-1'
    set @CompanyId = null;
  
  if @Warehouse = '-1'
    set @Warehouse = null;
  
  if @WarehouseCode = '-1'
    set @WarehouseCode = null;
  
  if @ParentWarehouseId = '-1'
    set @ParentWarehouseId = null;
  
  if @AddressId = '-1'
    set @AddressId = null;
  
  if @PostalAddressId = '-1'
    set @PostalAddressId = null;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
	 declare @Error int
 
  update Warehouse
     set CompanyId = isnull(@CompanyId, CompanyId),
         Warehouse = isnull(@Warehouse, Warehouse),
         WarehouseCode = isnull(@WarehouseCode, WarehouseCode),
         HostId = isnull(@HostId, HostId),
         ParentWarehouseId = isnull(@ParentWarehouseId, ParentWarehouseId),
         AddressId = isnull(@AddressId, AddressId),
         DesktopMaximum = isnull(@DesktopMaximum, DesktopMaximum),
         DesktopWarning = isnull(@DesktopWarning, DesktopWarning),
         MobileWarning = isnull(@MobileWarning, MobileWarning),
         MobileMaximum = isnull(@MobileMaximum, MobileMaximum),
         UploadToHost = isnull(@UploadToHost, UploadToHost),
         PostalAddressId = isnull(@PostalAddressId, PostalAddressId),
         ContactListId = isnull(@ContactListId, ContactListId),
         PrincipalId = isnull(@PrincipalId, PrincipalId),
         AllowInbound = isnull(@AllowInbound, AllowInbound),
         AllowOutbound = isnull(@AllowOutbound, AllowOutbound) 
   where WarehouseId = @WarehouseId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_WarehouseHistory_Insert
         @WarehouseId = @WarehouseId,
         @CompanyId = @CompanyId,
         @Warehouse = @Warehouse,
         @WarehouseCode = @WarehouseCode,
         @HostId = @HostId,
         @ParentWarehouseId = @ParentWarehouseId,
         @AddressId = @AddressId,
         @DesktopMaximum = @DesktopMaximum,
         @DesktopWarning = @DesktopWarning,
         @MobileWarning = @MobileWarning,
         @MobileMaximum = @MobileMaximum,
         @UploadToHost = @UploadToHost,
         @PostalAddressId = @PostalAddressId,
         @ContactListId = @ContactListId,
         @PrincipalId = @PrincipalId,
         @AllowInbound = @AllowInbound,
         @AllowOutbound = @AllowOutbound,
         @CommandType = 'Update'
  
  return @Error
  
end
