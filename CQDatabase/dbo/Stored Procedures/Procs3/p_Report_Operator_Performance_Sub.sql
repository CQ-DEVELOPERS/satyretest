﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Operator_Performance_Sub
  ///   Filename       : p_Report_Operator_Performance_Sub.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 03 Nov 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Operator_Performance_Sub
(
	@OperatorGroupId		int = null,
	@UOM                   nvarchar(15) = 'Units',
	@WarehouseId			int = null
)

as
begin

if (@OperatorGroupId = -1)
Begin
	set @OperatorGroupId = null
End


--Select @OperatorGroupId

Declare @TempTable As Table 
(
		UOMSiteTarget				Numeric(10,3),
		UOMIndustryTarget			Numeric(10,3),
		HoursPerDay					Int,
		DaysPerWeek					int,
		DaysPerMonth				Int,
		OperatorsinGroup			int	
)


Declare	@UOMSiteTarget				Numeric(10,3),
		@UOMIndustryTarget			Numeric(10,3),
		@OperatorsinGroup			int



Select @OperatorsinGroup	= Count(OperatorId) 
From Operator 
where OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)
and WarehouseId = @WarehouseId

if @UOM = 'Units'
begin
	
    SELECT @UOMSiteTarget = SUM(Units) 
    FROM GroupPerformance 
    WHERE (OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)) 
    AND (PerformanceType = 'S')
    
	SELECT @UOMIndustryTarget =  SUM(Units) 
	FROM GroupPerformance 
	WHERE  (OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)) 
	AND (PerformanceType = 'I')
						
end
else 
	if @UOM = 'Weight'
	begin
    
	SELECT @UOMSiteTarget = SUM(Weight) 
	FROM GroupPerformance 
	WHERE (OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)) 
	AND (PerformanceType = 'S')
	
	SELECT @UOMIndustryTarget =  SUM(Weight) 
	FROM GroupPerformance 
	WHERE  (OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)) 
	AND (PerformanceType = 'I')


	end
else 
	if @UOM = 'Jobs'
	begin
 
	SELECT @UOMSiteTarget = SUM(Jobs) 
	FROM GroupPerformance 
	WHERE (OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)) 
	AND (PerformanceType = 'S')
	
	SELECT @UOMIndustryTarget =  SUM(Jobs) 
	FROM GroupPerformance 
	WHERE  (OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)) 
	AND (PerformanceType = 'I')

  
end
else 
	if @UOM = 'Instructions'
	begin
 
	SELECT @UOMSiteTarget =  SUM(Instructions) 
	FROM GroupPerformance 
	WHERE (OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)) 
	AND (PerformanceType = 'S')
	
	SELECT @UOMIndustryTarget = SUM(Instructions) 
	FROM GroupPerformance 
	WHERE  (OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)) 
	AND (PerformanceType = 'I')

  

	end


Insert Into @TempTable (UOMSiteTarget,UOMIndustryTarget,HoursPerDay,DaysPerWeek,DaysPerMonth,OperatorsinGroup) 
					Values 	(@UOMSiteTarget,@UOMIndustryTarget,8,5,22,@OperatorsinGroup)

Select 
UOMSiteTarget,UOMIndustryTarget,
HoursPerDay,DaysPerWeek,DaysPerMonth,OperatorsinGroup	
 From @TempTable

end	   		
