﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportType_Delete
  ///   Filename       : p_ReportType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 15:24:50
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ReportType table.
  /// </remarks>
  /// <param>
  ///   @ReportTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportType_Delete
(
 @ReportTypeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ReportType
     where ReportTypeId = @ReportTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
