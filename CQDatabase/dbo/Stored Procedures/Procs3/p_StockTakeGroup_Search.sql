﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeGroup_Search
  ///   Filename       : p_StockTakeGroup_Search.sql
  ///   Create By      : Karen
  ///   Date Created   : September 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeGroup_Search


as
begin
	 set nocount on;
  
  select *  
    from StockTakeGroup
  order by StockTakeGroup
end
