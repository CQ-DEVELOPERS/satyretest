﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Wave_Area_Status
  ///   Filename       : p_Report_Wave_Area_Status.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Sep 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Wave_Area_Status
(
 @WarehouseId int,
 @LocationId int = null
)

as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   LocationId        int,
   Location          nvarchar(15),
   Area              nvarchar(50),
   StorageUnitId     int,
   ProductCode       nvarchar(50),
   Product           nvarchar(255),
   SKU               nvarchar(50),
   SKUCode           nvarchar(50),
   Batch             nvarchar(50),
   ActualQuantity    numeric(13,6),
   AllocatedQuantity numeric(13,6),
   ReservedQuantity  numeric(13,6),
   PickQuantity      numeric(13,6),
   StockTakeInd      smallint,
   Reserved          smallint default 0,
   Picks             smallint default 0,
   EndDate           datetime,
   DateDifference    int
  )
  
  insert @TableResult
        (LocationId,
         Location,
         Area,
         StorageUnitId,
         ProductCode,
         Product,
         SKU,
         SKUCode,
         Batch,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         StockTakeInd)
  select l.LocationId,
         l.Location,
         a.Area,
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKU,
         sku.SKUCode,
         b.Batch,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity,
         l.StockTakeInd
    from StorageUnitBatch          sub (nolock)
    join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId           = p.ProductId
    join SKU                       sku (nolock) on su.SKUId               = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId            = b.BatchId
    join Status                      s (nolock) on b.StatusId             = s.StatusId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join Location                    l (nolock) on subl.LocationId        = l.LocationId
    join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
    join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
   where a.Area = 'WavePick'
     and (l.LocationId = @LocationId or @LocationId is null)
  
  insert @TableResult
        (LocationId,
         Location,
         Area,
         StockTakeInd)
  select l.LocationId,
         l.Location,
         a.Area,
         l.StockTakeInd
    from Location                    l (nolock)
    left
    join @TableResult               tr          on l.LocationId           = tr.LocationId
    join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
   where a.Area = 'WavePick'
     and tr.LocationId is null
     and (l.LocationId = @LocationId or @LocationId is null)
  
  update tr
     set EndDate = (select max(EndDate)
                      from Instruction      i (nolock)
                      join InstructionType it (nolock) on i.InstructionTypeId= it.InstructionTypeId
                                                      and (it.InstructionTypeCode = 'M' or it.InstructionTypeCode like 'ST%')
                      join Status           s (nolock) on i.StatusId = s.StatusId
                                                      and s.StatusCode not in ('W','S')
                     where i.StoreLocationId = tr.LocationId)
    from @TableResult tr
  
  update tr
     set PickQuantity = (select SUM(Quantity)
                           from Instruction        i (nolock)
                           join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
                           join InstructionType   it (nolock) on i.InstructionTypeId= it.InstructionTypeId
                                                             and it.InstructionTypeCode in ('PM','PS','FM')
                           join Status             s (nolock) on i.StatusId = s.StatusId
                                                             and s.StatusCode in ('W','S')
                          where sub.StorageUnitId = tr.StorageUnitId)
    from @TableResult tr
  
  update @TableResult
     set Reserved = 1
   where (ReservedQuantity > 0 or PickQuantity > 0)
  
  update @TableResult
     set Picks = 1
   where PickQuantity > 0
  
  update tr1
     set Picks = (select max(Picks)
                    from @TableResult tr2
                   where tr1.LocationId = tr2.LocationId)
    from @TableResult tr1
  
  update tr1
     set Reserved = (select max(Reserved)
                    from @TableResult tr2
                   where tr1.LocationId = tr2.LocationId)
    from @TableResult tr1
  
  update @tableResult
     set DateDifference = datediff(mi, EndDate, getdate())
  
  select LocationId,
         Location,
         Area,
         StorageUnitId,
         ProductCode,
         Product,
         SKU,
         SKUCode,
         Batch,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         PickQuantity,
         convert(bit, StockTakeInd) as 'StockTakeInd',
         convert(bit, Reserved) as 'Reserved',
         convert(bit, Picks) as 'Picks',
         LTRIM(STR(DateDifference/60))+':'+REPLACE(STR(DateDifference%60,2),SPACE(1),'0')+':00' [Duration]
    from @TableResult
  order by Area,
           Location
end
