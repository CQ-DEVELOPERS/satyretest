﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatchHistory_Insert
  ///   Filename       : p_StorageUnitBatchHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:00
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnitBatchHistory table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null,
  ///   @BatchId int = null,
  ///   @StorageUnitId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   StorageUnitBatchHistory.StorageUnitBatchId,
  ///   StorageUnitBatchHistory.BatchId,
  ///   StorageUnitBatchHistory.StorageUnitId,
  ///   StorageUnitBatchHistory.CommandType,
  ///   StorageUnitBatchHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatchHistory_Insert
(
 @StorageUnitBatchId int = null,
 @BatchId int = null,
 @StorageUnitId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert StorageUnitBatchHistory
        (StorageUnitBatchId,
         BatchId,
         StorageUnitId,
         CommandType,
         InsertDate)
  select @StorageUnitBatchId,
         @BatchId,
         @StorageUnitId,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
