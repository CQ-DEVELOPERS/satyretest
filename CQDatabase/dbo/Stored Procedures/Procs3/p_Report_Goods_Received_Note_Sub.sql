﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Goods_Received_Note_Sub
  ///   Filename       : p_Report_Goods_Received_Note_Sub.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Goods_Received_Note_Sub

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   ServerName           varchar(max),
   DatabaseName         varchar(max),
   UserName             varchar(max),
   WarehouseId          int,
   InboundShipmentId    int,
   ReceiptId            int,
   [TO]                 varchar(max),
   CC                   varchar(max),
   BCC                  varchar(max),
   ReplyTo              varchar(max),
   IncludeReport        varchar(max),
   RenderFormat         varchar(max),
   Priority             varchar(max),
   Subject              varchar(max),
   Comment              varchar(max),
   IncludeLink          varchar(max),
   SendEmailToUserAlias varchar(max)
  )
         
  insert @TableResult
        (DatabaseName,
         UserName,
         WarehouseId,
         InboundShipmentId,
         ReceiptId,
         [TO],
         CC,
         BCC,
         ReplyTo,
         IncludeReport,
         RenderFormat,
         Priority,
         Subject,
         Comment,
         IncludeLink,
         SendEmailToUserAlias)
  select db_name(),
         'SQL',
         id.WarehouseId,
         isr.InboundShipmentId,
         r.ReceiptId,
         (select email + '; ' As [text()]
		 from contactlist cl
		 where exists
			(select 1
			 from InboundDocumentContactList idcl
			 where cl.contactListId = idcl.contactListId
			 and idcl.InboundDocumentId = id.InboundDocumentId)
		     for XML Path('')),
         '',
         '',
         '',
         1,
         'PDF',
         2,
         'GRN - ' + isnull(id.OrderNumber,''),
         r.Remarks,
         1,
         1
    from InboundDocument         id (nolock)
    join Receipt                  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join Status                   s (nolock) on r.StatusId           = s.StatusId
    left
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
   where r.Interfaced = 1
     and s.StatusCode = 'RC'
  
  update tr
     set ServerName = CONVERT(sysname, Value)
    from @TableResult tr
    join Configuration c (nolock) on tr.WarehouseId = c.WarehouseId
   where c.ConfigurationId = 200
  
  update r
     set Interfaced = 1,
         StatusId   = dbo.ufn_StatusId('R','C')
    from @TableResult tr
    join Receipt       r on tr.ReceiptId = r.ReceiptId
  
  select DatabaseName,
         UserName,
         WarehouseId,
         InboundShipmentId,
         ReceiptId,
         [TO],
         CC,
         BCC,
         ReplyTo,
         IncludeReport,
         RenderFormat,
         Priority,
         Subject,
         Comment,
         IncludeLink,
         SendEmailToUserAlias
    from @TableResult
end
