﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Route_Update
  ///   Filename       : p_Route_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:07
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Route table.
  /// </remarks>
  /// <param>
  ///   @RouteId int = null,
  ///   @Route nvarchar(100) = null,
  ///   @RouteCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Route_Update
(
 @RouteId int = null,
 @Route nvarchar(100) = null,
 @RouteCode nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
  if @RouteId = '-1'
    set @RouteId = null;
  
  if @Route = '-1'
    set @Route = null;
  
  if @RouteCode = '-1'
    set @RouteCode = null;
  
	 declare @Error int
 
  update Route
     set Route = isnull(@Route, Route),
         RouteCode = isnull(@RouteCode, RouteCode) 
   where RouteId = @RouteId
  
  select @Error = @@Error
  
  
  return @Error
  
end
