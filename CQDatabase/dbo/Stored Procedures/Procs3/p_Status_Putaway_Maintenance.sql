﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Putaway_Maintenance
  ///   Filename       : p_Status_Putaway_Maintenance.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Putaway_Maintenance
(
 @WarehouseId int
)

as
begin
	 set nocount on;
  
  if dbo.ufn_Configuration(61, @WarehouseId) = 1
  begin
    select -1      as 'StatusId',
           '{All}' as 'Status'
    union
    select StatusId,
           Status
      from Status (nolock)
     where Type = 'R'
       and StatusCode in ('S','A')
    order by Status
  end
  else
  begin
    select -1      as 'StatusId',
           '{All}' as 'Status'
    union
    select StatusId,
           Status
      from Status (nolock)
     where Type = 'R'
       and StatusCode in ('PR','S')
    order by Status
  end
end
