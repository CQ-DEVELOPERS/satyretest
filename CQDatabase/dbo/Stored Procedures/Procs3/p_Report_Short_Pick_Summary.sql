﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Short_Pick_Summary
  ///   Filename       : p_Report_Short_Pick_Summary.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Short_Pick_Summary
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @FromDate           datetime,
 @ToDate             datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   IssueLineId        int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   EndDate            datetime,
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   QuantityOnHand     float,
   OperatorId         int,
   Operator           nvarchar(50)
  )
  
  insert @TableResult
        (EndDate,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity)
  select convert(nvarchar(10), ins.EndDate, 111),
         sum(ili.Quantity),
         sum(ili.ConfirmedQuantity),
         sum(ili.Quantity - ili.ConfirmedQuantity)
    from IssueLineInstruction ili
    join Instruction     ins (nolock) on ili.InstructionId = ins.InstructionId
   where ins.EndDate between @FromDate and @ToDate
  group by convert(nvarchar(10), ins.EndDate, 111)
  
  select EndDate,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity
    from @TableResult
end
