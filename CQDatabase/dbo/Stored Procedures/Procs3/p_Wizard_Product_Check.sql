﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wizard_Product_Check
  ///   Filename       : p_Wizard_Product_Check.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 25 Sep 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wizard_Product_Check
(
    @ProductImportId int = -1
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if (@ProductImportId = -1)
  
  begin
		

		Update ProductImport set ErrorLog = '<Root></Root>',HasError = 0

		Update ProductImport set Haserror = 1,ErrorLog.modify('insert <error> Product Code \\n</error> into (/Root)[1]')
		WHERE     (ProductCode IN
                          (SELECT     ProductCode
                            FROM          Product))

		Update ProductImport set Haserror = 1,ErrorLog.modify('insert <error> Sku Code \\n</error> into (/Root)[1]')
		WHERE     (SKUCode NOT IN
                          (SELECT     SKUCode
                            FROM          SKU))

		Update ProductImport set Haserror = 1,ErrorLog.modify('insert <error> Pack Type \\n</error> into (/Root)[1]')
		WHERE     (PackType NOT IN
								  (SELECT     PackType
									FROM          PackType))

  if @Error <> 0
    goto error
  end
  
  else
  
  begin
		
		Update ProductImport set ErrorLog = '<Root></Root>',HasError = 0 where (ProductImportId = @ProductImportId)
		
		Update ProductImport set Haserror = 1,ErrorLog.modify('insert <error> Product Code \\n</error> into (/Root)[1]')
		WHERE     (ProductCode IN
                          (SELECT     ProductCode
                            FROM          Product)) And  (ProductImportId = @ProductImportId)	

		Update ProductImport set Haserror = 1,ErrorLog.modify('insert <error> Sku Code \\n</error> into (/Root)[1]')
		WHERE     (SKUCode NOT IN
                          (SELECT     SKUCode
                            FROM          SKU)) And  (ProductImportId = @ProductImportId)	

		Update ProductImport set Haserror = 1,ErrorLog.modify('insert <error> Pack Type \\n</error> into (/Root)[1]')
		WHERE     (PackType NOT IN
								  (SELECT     PackType
									FROM          PackType)) And  (ProductImportId = @ProductImportId)	

  if @Error <> 0
    goto error
  end
  
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Wizard_Product_Check'); 
    rollback transaction
    return @Error
end
