﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_Search_Product_SKU
  ///   Filename       : p_StorageUnitBatch_Search_Product_SKU.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007 11:54:26
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null output,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(30) = null,
  ///   @Product nvarchar(50) = null,
  ///   @Barcode nvarchar(50) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   p.ProductId,
  ///   p.StatusId,
  ///   s.Status,
  ///   p.ProductCode,
  ///   p.Product,
  ///   p.Barcode,
  ///   p.MinimumQuantity,
  ///   p.ReorderQuantity,
  ///   p.MaximumQuantity,
  ///   p.CuringPeriodDays,
  ///   p.ShelfLifeDays,
  ///   p.QualityAssuranceIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatch_Search_Product_SKU
(
 @ProductCode nvarchar(30) = null,
 @Product     nvarchar(50) = null,
 @SKUCode     nvarchar(50) = null,
 @SKU         nvarchar(50) = null,
 @Batch       nvarchar(50) = null,
 @ECLNumber   nvarchar(10) = null
)

as
begin
	 set nocount on;
  
  if @ProductCode = '-1'
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
  
  if @SKUCode = '-1'
    set @SKUCode = null;
  
  if @Batch is null and @ECLNumber is null
  begin
    select sub.StorageUnitBatchId,
           p.ProductCode,
           p.Product,
           sku.SKUCode,
           sku.SKU,
           b.Batch,
           rl.ReceiptLineId,
           isnull(rl.NumberOfPallets, 999) as NumberOfPallets
      from StorageUnitBatch sub (nolock)
      join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
      join SKU              sku (nolock) on su.SKUId          = sku.SKUId
      join Product            p (nolock) on su.ProductId      = p.ProductId
      join Batch              b (nolock) on sub.BatchId       = b.BatchId
      left
      join ReceiptLine       rl (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
     where isnull(p.ProductCode,'%') like '%' + isnull(@ProductCode, isnull(p.ProductCode,'%')) + '%'
       and isnull(p.Product,'%')     like '%' + isnull(@Product, isnull(p.Product,'%')) + '%'
       and isnull(sku.SKUCode,'%')   like '%' + isnull(@SKUCode, isnull(sku.SKUCode,'%')) + '%'
       and isnull(sku.SKU,'%')       like '%' + isnull(@SKU, isnull(sku.SKU,'%')) + '%'
  end
  else
  begin
    select sub.StorageUnitBatchId,
           p.ProductCode,
           p.Product,
           sku.SKUCode,
           sku.SKU,
           b.Batch,
           rl.ReceiptLineId,
           isnull(rl.NumberOfPallets, 999) as NumberOfPallets
      from StorageUnitBatch sub (nolock)
      join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
      join SKU              sku (nolock) on su.SKUId          = sku.SKUId
      join Product            p (nolock) on su.ProductId      = p.ProductId
      join Batch              b (nolock) on sub.BatchId       = b.BatchId
      left
      join ReceiptLine       rl (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
     where isnull(p.ProductCode,'%') like '%' + isnull(@ProductCode, isnull(p.ProductCode,'%')) + '%'
       and isnull(p.Product,'%')     like '%' + isnull(@Product, isnull(p.Product,'%')) + '%'
       and isnull(sku.SKUCode,'%')   like '%' + isnull(@SKUCode, isnull(sku.SKUCode,'%')) + '%'
       and isnull(sku.SKU,'%')       like '%' + isnull(@SKU, isnull(sku.SKU,'%')) + '%'
       and isnull(b.Batch,'%')       like '%' + isnull(@Batch, isnull(b.Batch,'%')) + '%'
       and isnull(b.ECLNumber,'%')   like '%' + isnull(@ECLNumber, isnull(b.ECLNumber,'%')) + '%'
  end
end
