﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shipment_Delete
  ///   Filename       : p_Shipment_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:49
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Shipment table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shipment_Delete
(
 @ShipmentId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Shipment
     where ShipmentId = @ShipmentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
