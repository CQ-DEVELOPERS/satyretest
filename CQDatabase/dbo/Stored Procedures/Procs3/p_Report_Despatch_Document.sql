﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Despatch_Document
  ///   Filename       : p_Report_Despatch_Document.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Despatch_Document
(
 @ConnectionString   nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @OutboundShipmentId int,
 @IssueId            int
)

as
begin
	 set nocount on;

declare @PrintDangerousGoods	nvarchar(1),
		@PrintDispatchDoc	    nvarchar(1)

	 
	 if @OutboundShipmentId is not null and @OutboundShipmentId != -1
	 begin
	   set @IssueId = null
	   
	   exec p_OutboundShipment_Update
	    @OutboundShipmentId = @OutboundShipmentId,
	    @Remarks = 'Despatch Documents Printed'
	 end
	 else
	   exec p_Issue_Update
	    @IssueId = @IssueId,
	    @Remarks = 'Despatch Documents Printed'
	 
--  exec p_Outbound_Rollup
--   @OutboundShipmentId = @OutboundShipmentId,
--   @IssueId            = @IssueId,
--   @StatusCode         = 'DC'

  set @PrintDangerousGoods = '0'
  
  SELECT @PrintDangerousGoods = (CONVERT(nvarchar, Indicator))
  from Configuration where ConfigurationId = 270
  
  set @PrintDispatchDoc = '0'
  
  SELECT @PrintDispatchDoc = (CONVERT(nvarchar, Indicator))
  from Configuration where ConfigurationId = 415
  
  
	 select @ConnectionString as 'ConnectionString',
         @OutboundShipmentId as 'OutboundShipmentId',
         @IssueId as 'IssueId',
         @PrintDangerousGoods as 'PrintDangerousGoods',
         @PrintDispatchDoc as 'PrintDispatchDoc'
end
