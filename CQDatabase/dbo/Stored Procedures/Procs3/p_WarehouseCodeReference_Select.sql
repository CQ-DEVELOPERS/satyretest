﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_WarehouseCodeReference_Select
  ///   Filename       : p_WarehouseCodeReference_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 09:52:57
  /// </summary>
  /// <remarks>
  ///   Selects rows from the WarehouseCodeReference table.
  /// </remarks>
  /// <param>
  ///   @WarehouseCodeReferenceId int = null 
  /// </param>
  /// <returns>
  ///   WarehouseCodeReference.WarehouseCodeReferenceId,
  ///   WarehouseCodeReference.WarehouseCode,
  ///   WarehouseCodeReference.DownloadType,
  ///   WarehouseCodeReference.WarehouseId,
  ///   WarehouseCodeReference.InboundDocumentTypeId,
  ///   WarehouseCodeReference.OutboundDocumentTypeId,
  ///   WarehouseCodeReference.ToWarehouseId,
  ///   WarehouseCodeReference.ToWarehouseCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_WarehouseCodeReference_Select
(
 @WarehouseCodeReferenceId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         WarehouseCodeReference.WarehouseCodeReferenceId
        ,WarehouseCodeReference.WarehouseCode
        ,WarehouseCodeReference.DownloadType
        ,WarehouseCodeReference.WarehouseId
        ,WarehouseCodeReference.InboundDocumentTypeId
        ,WarehouseCodeReference.OutboundDocumentTypeId
        ,WarehouseCodeReference.ToWarehouseId
        ,WarehouseCodeReference.ToWarehouseCode
    from WarehouseCodeReference
   where isnull(WarehouseCodeReference.WarehouseCodeReferenceId,'0')  = isnull(@WarehouseCodeReferenceId, isnull(WarehouseCodeReference.WarehouseCodeReferenceId,'0'))
  
end
