﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeGroupLocation_Update
  ///   Filename       : p_StockTakeGroupLocation_Update.sql
  ///   Create By      : Karen
  ///   Date Created   : September 2011
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StockTakeReference table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeGroupLocation_Update
(
 @StockTakeGroupCode		varchar(20) = null,
 @StockTakeGroup			varchar(50) = null
)

as
begin
	 set nocount on;
  
  declare @Error int
  update StockTakeGroup
     set StockTakeGroup = isnull(@StockTakeGroup, StockTakeGroup)
   where StockTakeGroupCode = @StockTakeGroupCode
   
  select @Error = @@Error
  
end
