﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeGroupLocation_Delete
  ///   Filename       : p_StockTakeGroupLocation_Delete.sql
  ///   Create By      : Karen
  ///   Date Created   : September 2011
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the StockTakeGroup table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_StockTakeGroupLocation_Delete
(
 @StockTakeGroupLocId	int = null
)
as
begin
	 set nocount on
	 declare @Error int


  delete StockTakeGroupLocation
     where  StockTakeGroupLocId = @StockTakeGroupLocId
  
  select @Error = @@Error
   
end
