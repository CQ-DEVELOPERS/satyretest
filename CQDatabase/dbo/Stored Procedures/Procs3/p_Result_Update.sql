﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Result_Update
  ///   Filename       : p_Result_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:07
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Result table.
  /// </remarks>
  /// <param>
  ///   @ResultId int = null,
  ///   @ResultCode nvarchar(20) = null,
  ///   @Result nvarchar(510) = null,
  ///   @Pass bit = null,
  ///   @StartRange float = null,
  ///   @EndRange float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Result_Update
(
 @ResultId int = null,
 @ResultCode nvarchar(20) = null,
 @Result nvarchar(510) = null,
 @Pass bit = null,
 @StartRange float = null,
 @EndRange float = null 
)

as
begin
	 set nocount on;
  
  if @ResultId = '-1'
    set @ResultId = null;
  
  if @ResultCode = '-1'
    set @ResultCode = null;
  
  if @Result = '-1'
    set @Result = null;
  
	 declare @Error int
 
  update Result
     set ResultCode = isnull(@ResultCode, ResultCode),
         Result = isnull(@Result, Result),
         Pass = isnull(@Pass, Pass),
         StartRange = isnull(@StartRange, StartRange),
         EndRange = isnull(@EndRange, EndRange) 
   where ResultId = @ResultId
  
  select @Error = @@Error
  
  
  return @Error
  
end
