﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeGroup_Insert
  ///   Filename       : p_StockTakeGroup_Insert.sql
  ///   Create By      : Karen
  ///   Date Created   : September 2011
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StockTakeGroup table.
  /// </remarks>
  /// <param>

  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeGroup_Insert
(	@StockTakeGroupCode		varchar(20) = null,
	@StockTakeGroup			varchar(50) = null
    )
    
    

as
begin
	 set nocount on;
  
  declare @Error VARCHAR(255)
  
 
	  insert	StockTakeGroup
			   (StockTakeGroupCode,
				StockTakeGroup)
	  select	@StockTakeGroupCode,
				@StockTakeGroup	  
    
  SELECT @Error
end
