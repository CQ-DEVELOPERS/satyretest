﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Test_Search
  ///   Filename       : p_Test_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:01
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Test table.
  /// </remarks>
  /// <param>
  ///   @TestId int = null output,
  ///   @TestCode nvarchar(20) = null,
  ///   @Test nvarchar(510) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Test.TestId,
  ///   Test.TestCode,
  ///   Test.Test 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Test_Search
(
 @TestId int = null output,
 @TestCode nvarchar(20) = null,
 @Test nvarchar(510) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @TestId = '-1'
    set @TestId = null;
  
  if @TestCode = '-1'
    set @TestCode = null;
  
  if @Test = '-1'
    set @Test = null;
  
 
  select
         Test.TestId
        ,Test.TestCode
        ,Test.Test
    from Test
   where isnull(Test.TestId,'0')  = isnull(@TestId, isnull(Test.TestId,'0'))
     and isnull(Test.TestCode,'%')  like '%' + isnull(@TestCode, isnull(Test.TestCode,'%')) + '%'
     and isnull(Test.Test,'%')  like '%' + isnull(@Test, isnull(Test.Test,'%')) + '%'
  order by TestCode
  
end
