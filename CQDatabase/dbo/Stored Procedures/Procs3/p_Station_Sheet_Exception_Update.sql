﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Station_Sheet_Exception_Update
  ///   Filename       : p_Station_Sheet_Exception_Update.sql
  ///   Create By      : Karen
  ///   Date Created   : 16 Mar 2011 15:15:30
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Exception table.
  /// </remarks>
  /// <param>
  ///   @ExceptionId int = null,
  ///   @ReceiptLineId int = null,
  ///   @InstructionId int = null,
  ///   @IssueLineId int = null,
  ///   @ReasonId int = null,
  ///   @Exception nvarchar(255) = null,
  ///   @ExceptionCode nvarchar(10) = null,
  ///   @CreateDate datetime = null,
  ///   @OperatorId int = null,
  ///   @ExceptionDate datetime = null,
  ///   @JobId int = null,
  ///   @Detail sql_variant = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Station_Sheet_Exception_Update
(
 @ExceptionId int = null,
 @Exception nvarchar(255) = null,
 @Quantity float = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  update Exception
     set Exception = isnull(@Exception, Exception),
         Quantity = isnull(@Quantity, Quantity) 
   where ExceptionId = @ExceptionId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_ExceptionHistory_Insert
         @ExceptionId = @ExceptionId,
         @Exception = @Exception,
         @Quantity = @Quantity,
         @CommandType = 'Update'
  
  return @Error
  
end
