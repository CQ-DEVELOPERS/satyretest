﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Update
  ///   Filename       : p_SerialNumber_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Aug 2013 14:12:40
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the SerialNumber table.
  /// </remarks>
  /// <param>
  ///   @SerialNumberId int = null,
  ///   @StorageUnitId int = null,
  ///   @LocationId int = null,
  ///   @ReceiptId int = null,
  ///   @StoreInstructionId int = null,
  ///   @PickInstructionId int = null,
  ///   @SerialNumber varchar(50) = null,
  ///   @ReceivedDate datetime = null,
  ///   @DespatchDate datetime = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @BatchId int = null,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null,
  ///   @ReceiptLineId int = null,
  ///   @StoreJobId int = null,
  ///   @PickJobId int = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @StatusId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Update
(
 @SerialNumberId int = null,
 @StorageUnitId int = null,
 @LocationId int = null,
 @ReceiptId int = null,
 @StoreInstructionId int = null,
 @PickInstructionId int = null,
 @SerialNumber varchar(50) = null,
 @ReceivedDate datetime = null,
 @DespatchDate datetime = null,
 @ReferenceNumber nvarchar(60) = null,
 @BatchId int = null,
 @IssueId int = null,
 @IssueLineId int = null,
 @ReceiptLineId int = null,
 @StoreJobId int = null,
 @PickJobId int = null,
 @Remarks nvarchar(510) = null,
 @StatusId int = null 
)

as
begin
	 set nocount on;
  
  if @SerialNumberId = '-1'
    set @SerialNumberId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @SerialNumber = '-1'
    set @SerialNumber = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
  if @IssueLineId = '-1'
    set @IssueLineId = null;
  
  if @ReceiptLineId = '-1'
    set @ReceiptLineId = null;
  
  if @StoreJobId = '-1'
    set @StoreJobId = null;
  
  if @PickJobId = '-1'
    set @PickJobId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
	 declare @Error int
 
  update SerialNumber
     set StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         LocationId = isnull(@LocationId, LocationId),
         ReceiptId = isnull(@ReceiptId, ReceiptId),
         StoreInstructionId = isnull(@StoreInstructionId, StoreInstructionId),
         PickInstructionId = isnull(@PickInstructionId, PickInstructionId),
         SerialNumber = isnull(@SerialNumber, SerialNumber),
         ReceivedDate = isnull(@ReceivedDate, ReceivedDate),
         DespatchDate = isnull(@DespatchDate, DespatchDate),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         BatchId = isnull(@BatchId, BatchId),
         IssueId = isnull(@IssueId, IssueId),
         IssueLineId = isnull(@IssueLineId, IssueLineId),
         ReceiptLineId = isnull(@ReceiptLineId, ReceiptLineId),
         StoreJobId = isnull(@StoreJobId, StoreJobId),
         PickJobId = isnull(@PickJobId, PickJobId),
         Remarks = isnull(@Remarks, Remarks),
         StatusId = isnull(@StatusId, StatusId) 
   where SerialNumberId = @SerialNumberId
  
  select @Error = @@Error
  
  
  return @Error
  
end
