﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_Take_Summary_ProductOrBatch
  ///   Filename       : p_Report_Stock_Take_Summary_ProductOrBatch.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
	create procedure p_Report_Stock_Take_Summary_ProductOrBatch
		(
		 @StockTakeReferenceId		int = null,
		 @WarehouseId				int = null,
		 @FromDate					datetime,
		 @ToDate					datetime,
		 @AreaId					Int = null,
		 @ProductId				    int = null,
		 --@ProductCode				nvarchar(30) = null,
		 @BatchId					int = null,
		 @PrincipalId				int = null
		)

	as
	begin
		set nocount on; 

	if (@StockTakeReferenceId = -1)
	 set @StockTakeReferenceId = null

	if (@AreaId = -1)
	 set @AreaId = null

	if (@ProductId = -1)
	 set @ProductId = null

	if  (@BatchId = -1)
	 set @BatchId = null 
 
	if @PrincipalId = -1
		set @PrincipalId = null
	
	Declare @TableResult as  Table 
		(StockTakeReferenceId		int,
		 JobId						int,
		 InstructionId				int,
		 InstructionRefId			int,
		 InstructionTypeId			int,
		 InstructionType			nvarchar(50),
		 InstructionTypeCode		nvarchar(25),
		 Area						nvarchar(50),
		 AreaId						int,
		 StorageUnitBatchId			int,
		 PickLocationId				int,
		 PickLocation				nvarchar(15),
         StoreLocationId			Int,
         StoreLocation				nvarchar(15),
		 ProductCode				Int,
		 Product					nvarchar(50),		
		 BatchId					int,
		 SKUId						int,
		 SKU						nvarchar(50),
		 Quantity					int,
		 PreviousCount1				int,
		 PreviousCount2				int,
		 LatestCount				int,
		 Variance					int,
		 CountedById				int,
		 CountedBy					nvarchar(50),
		 AuthorisedById				int,
		 AuthorisedBy				nvarchar(50),
		 CreateDate					datetime,
		 PrincipalId				int,
		 PrincipalCode				nvarchar(50))
		 
  declare @Instructions as table
	  (
	   LocationId         int,
	   StorageUnitBatchId int,
	   InstructionId      int
	  )
   insert @Instructions
        (LocationId,
         StorageUnitBatchId,
         InstructionId)
  select i.PickLocationId,
         i.StorageUnitBatchId,
         max(i.InstructionId)
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId   = it.InstructionTypeId
    left join StockTakeReferenceJob strj (nolock) on strj.StockTakeReferenceId = @StockTakeReferenceId
   where i.CreateDate           >= '2009-07-01'
     and it.InstructionTypeCode in ('STE','STL','STA','STP')
     and i.JobId = isnull(strj.JobId, i.JobId)
  group by i.PickLocationId, i.StorageUnitBatchId 
  
  --select * from @Instructions
  
	insert @TableResult
			(StockTakeReferenceId,
			 JobId,
			 InstructionType,
			 StorageUnitBatchId,
			 PickLocationId,
			 StoreLocationId,
			 Quantity,
			 CountedById,
			 AuthorisedById,
			 CreateDate)
  SELECT    @StockTakeReferenceId,
			i.JobId, 
            it.InstructionType, 
            i.StorageUnitBatchId, 
            i.PickLocationId, 
            i.StoreLocationId, 
            i.Quantity,
            j.OperatorId,
            sta.operatorid,
            i.CreateDate
	FROM        Instruction			i  (nolock) 
	INNER JOIN  InstructionType		it (nolock) ON i.InstructionTypeId = it.InstructionTypeId
	join		Job                 j  (nolock) on j.JobId = i.JobId
	left join	StockTakeReferenceJob strj (nolock) on strj.StockTakeReferenceId = @StockTakeReferenceId
	left join	StockTakeAuthorise sta (nolock) on sta.InstructionId = i.InstructionId
	--join	    viewStock			vs (nolock) on vs.StorageUnitBatchId = i.StorageUnitBatchId
	WHERE (it.InstructionTypeCode in ('STL','STA','STP','STE'))
		 and i.CreateDate   between @FromDate and @ToDate
		 and i.WarehouseId = Isnull(@WarehouseId,i.WarehouseId)
		 and i.JobId = isnull(strj.JobId, i.JobId)
		 --and vs.ProductId = isnull(@ProductId, vs.ProductId)


  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
    
  update tr
     set PrincipalId = p.PrincipalId,
		 PrincipalCode = pr.PrincipalCode
    from @TableResult tr
    join StorageUnitBatch sub  (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product p (nolock) on su.ProductId = p.ProductId
    join Principal pr (nolock) on p.PrincipalId = pr.PrincipalId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
   
    update tr
     set InstructionId  = i.InstructionId
    from @TableResult  tr
    join @Instructions  i on tr.StorageUnitBatchId = i.StorageUnitBatchId
                         and tr.PickLocationId       = i.LocationId
                           
  update tr
     set CountedBy = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.CountedById = o.OperatorId
    
  update tr
     set AuthorisedBy = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.AuthorisedById = o.OperatorId
    
  update tr
     set PreviousCount1   = i.Quantity,
         LatestCount      = i.ConfirmedQuantity,
         InstructionRefId = i.InstructionRefId
    from @TableResult tr
    join Instruction   i (nolock) on tr.InstructionId = i.InstructionId
  
  update tr
     set PreviousCount2 = i.Quantity
    from @TableResult tr
    join Instruction   i (nolock) on tr.InstructionRefId = i.InstructionId
    
  update tr
     set Variance = isnull(tr.Quantity,0) - isnull(tr.LatestCount,0)
    from @TableResult tr
    where tr.AuthorisedById > 0
    
  update tr
     set PrincipalCode = p.PrincipalCode
    from @TableResult tr
    join Principal        p (nolock) on tr.PrincipalId = p.PrincipalId

  select tr.StockTakeReferenceId,
		 tr.CreateDate,
		 vs.ProductCode,
         vs.Product,
         vs.Batch,
         sum(isnull(tr.Quantity,0)) as 'ExpectedQuantity',
         sum(isnull(tr.PreviousCount1,0)) as 'PreviousCount1',
         sum(isnull(tr.PreviousCount2,0)) as 'PreviousCount2',
         sum(isnull(tr.LatestCount,0)) as 'LatestCount',
         sum(isnull(tr.Variance,0)) as 'Variance',
         tr.AuthorisedBy,
         tr.PrincipalCode
    from @TableResult    tr
    join viewStock       vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
    where vs.ProductId = isnull(@ProductId,vs.ProductId)
    and	  vs.BatchId = isnull(@BatchId, vs.BatchId)
	and   isnull(tr.PrincipalId,-1) = isnull(@PrincipalId,isnull(tr.PrincipalId,-1))
  group by tr.StockTakeReferenceId,
		   tr.CreateDate,
		   vs.ProductCode,
           vs.Product,
           vs.Batch,
           tr.AuthorisedBy,
           tr.PrincipalCode
  order by vs.ProductCode,
           vs.Product
  
end
