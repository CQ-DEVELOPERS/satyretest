﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Unlinked_Products
  ///   Filename       : p_Static_Unlinked_Products.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007 11:54:26
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null output,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(30) = null,
  ///   @Product nvarchar(50) = null,
  ///   @Barcode nvarchar(50) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   p.ProductId,
  ///   p.StatusId,
  ///   s.Status,
  ///   p.ProductCode,
  ///   p.Product,
  ///   p.Barcode,
  ///   p.MinimumQuantity,
  ///   p.ReorderQuantity,
  ///   p.MaximumQuantity,
  ///   p.CuringPeriodDays,
  ///   p.ShelfLifeDays,
  ///   p.QualityAssuranceIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Unlinked_Products
(
 @ProductCode nvarchar(30) = null,
 @Product     nvarchar(50) = null,
 @SKUCode     nvarchar(50) = null,
 @SKU         nvarchar(50) = null,
 @BatchId     int          = null
)

as
begin
	 set nocount on;
  
  if @ProductCode = '-1'
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
  
  if @SKUCode = '-1'
    set @SKUCode = null;
  
  if @BatchId = -1
    set @BatchId = null;
  
  if @ProductCode is null and @Product is null and @SKUCode is null and @BatchId is null
    set @BatchId = -1
  
  select distinct su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU
    from StorageUnit       su (nolock)
    join SKU              sku (nolock) on su.SKUId          = sku.SKUId
                                      and isnull(sku.SKUCode,'%')    like '%' + isnull(@SKUCode, isnull(sku.SKUCode,'%')) + '%'
                                      and isnull(sku.SKU,'%')        like '%' + isnull(@SKU, isnull(sku.SKU,'%')) + '%'
    join Product            p (nolock) on su.ProductId      = p.ProductId
                                      and isnull(p.Product,'%')      like '%' + isnull(@Product, isnull(p.Product,'%')) + '%'
    left
    join StorageUnitBatch sub (nolock) on sub.StorageUnitId = su.StorageUnitId
                                      and sub.BatchId       = @BatchId
   where sub.StorageUnitBatchId       is null
end
