﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitExternalCompany_Search
  ///   Filename       : p_StorageUnitExternalCompany_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:57
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StorageUnitExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null output,
  ///   @ExternalCompanyId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StorageUnitExternalCompany.StorageUnitId,
  ///   StorageUnitExternalCompany.ExternalCompanyId,
  ///   StorageUnitExternalCompany.AllocationCategory,
  ///   StorageUnitExternalCompany.MinimumShelfLife,
  ///   StorageUnitExternalCompany.ProductCode,
  ///   StorageUnitExternalCompany.SKUCode,
  ///   StorageUnitExternalCompany.DefaultQC,
  ///   StorageUnitExternalCompany.SendToQC 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitExternalCompany_Search
(
 @StorageUnitId int = null output,
 @ExternalCompanyId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
 
  select
         StorageUnitExternalCompany.StorageUnitId
        ,StorageUnitExternalCompany.ExternalCompanyId
        ,StorageUnitExternalCompany.AllocationCategory
        ,StorageUnitExternalCompany.MinimumShelfLife
        ,StorageUnitExternalCompany.ProductCode
        ,StorageUnitExternalCompany.SKUCode
        ,StorageUnitExternalCompany.DefaultQC
        ,StorageUnitExternalCompany.SendToQC
    from StorageUnitExternalCompany
   where isnull(StorageUnitExternalCompany.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(StorageUnitExternalCompany.StorageUnitId,'0'))
     and isnull(StorageUnitExternalCompany.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(StorageUnitExternalCompany.ExternalCompanyId,'0'))
  
end
