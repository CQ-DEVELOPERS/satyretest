﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Routing_Order_Pending
  ///   Filename       : p_Routing_Order_Pending.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Routing_Order_Pending
(
 @WarehouseId            int
)

as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        OutboundDocumentId        int,
        OutboundDocumentType      nvarchar(30),
        IssueId                   int,
        OrderNumber               nvarchar(30),
        OutboundShipmentId        int,
        CustomerCode              nvarchar(30),
        Customer                  nvarchar(255),
        RouteId                   int,
        Route                     nvarchar(50),
        NumberOfLines             int,
        DeliveryDate              datetime,
        CreateDate                datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        PriorityId                int,
        Priority                  nvarchar(50),
        LocationId                int,
        Location                  nvarchar(15),
        Rating                    int,
        AvailabilityIndicator     nvarchar(20),
        Remarks                   nvarchar(255)
       );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  insert @TableResult
        (OutboundShipmentId,
         OutboundDocumentId,
         IssueId,
         LocationId,
         OrderNumber,
         CustomerCode,
         Customer,
         RouteId,
         StatusId,
         Status,
         PriorityId,
         DeliveryDate,
         CreateDate,
         OutboundDocumentType,
         Rating,
         Remarks)
  select osi.OutboundShipmentId,
         od.OutboundDocumentId,
         i.IssueId,
         i.LocationId,
         od.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         i.RouteId,
         i.StatusId,
         s.Status,
         i.PriorityId,
         i.DeliveryDate,
         od.CreateDate,
         odt.OutboundDocumentType,
         ec.Rating,
         i.Remarks
    from OutboundDocument     od  (nolock)
    join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
    join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join Status               s   (nolock) on i.StatusId               = s.StatusId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    left
    join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
   where i.WarehouseId = @WarehouseId
     and RoutingSystem = 0
  
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from OutboundLine ol
                           where tr.OutboundDocumentId = ol.OutboundDocumentId)
    from @TableResult tr
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update @TableResult
     set AvailabilityIndicator = 'Standard'
   where dbo.ufn_Configuration_Value(47, @warehouseId) <= DateDiff(hh, @GetDate, DeliveryDate)
   
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where dbo.ufn_Configuration_Value(46, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where dbo.ufn_Configuration_Value(45, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where dbo.ufn_Configuration_Value(44, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  select IssueId,
         isnull(OutboundShipmentId, '-1')as 'OutboundShipmentId',
         OrderNumber,
         CustomerCode,
         Customer,
         isnull(RouteId,-1) as 'RouteId',
         Route,
         NumberOfLines,
         DeliveryDate,
         CreateDate,
         Status,
         PriorityId,
         Priority,
         OutboundDocumentType,
         isnull(LocationId,-1) as 'LocationId',
         Location,
         Rating,
         AvailabilityIndicator,
         Remarks
    from @TableResult
  order by DeliveryDate, OutboundShipmentId, OrderNumber
end
