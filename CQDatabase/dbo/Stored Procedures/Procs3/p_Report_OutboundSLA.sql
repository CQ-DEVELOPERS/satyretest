﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_OutboundSLA
  ///   Filename       : p_Report_OutboundSLA.sql
  ///   Create By      : Karen
  ///   Date Created   : February 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_OutboundSLA
(
 @FromDate				datetime,
 @ToDate				datetime,
 @PrincipalId			int,
 @ShipDateOption		nvarchar(1), -- 1=Use Date and Time Value
									 --	2=Use Date Value Only
 @Measure				nvarchar(1), -- 1=Request Ship Date to Pick Complete date  
									 --	2=Request Ship Date to Actual Ship date
 @StatusId				int = null,
 @OrderNumber			nvarchar(30)
)


as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   CreateDate				    datetime,
   PrincipalId					int,
   OrderNumber					nvarchar(30),
   ExpectedDeliveryDate			datetime,
   StatusId						int,
   Status					    nvarchar(50),
   IssueLineId					int,
   DeliveryDate					date,
   PickDate						datetime,
   ActualShipDate				datetime,
   DiffPickShipped				decimal(13,2),
   DiffExpectedShipped			decimal(13,2)
  )
  
  declare @TableDetail as table
  (
   PrincipalId					int,
   PrincipalCode				nvarchar(50),
   DeliveryDate					date,
   TotalOrders					int,
   TONotMeasured				int,
   TOMeasured					int,
   TOLines						int,
   TOSLA						int,
   TOLinesSLA					int,
   PctOrdersSLA					int,
   PctOrderLinesSLA				int
  )
  
  if @PrincipalId = -1
	set @PrincipalId = null
	
  if @OrderNumber = '-1'
  or @OrderNumber = ' '
	set @OrderNumber = null
	
  if @StatusId = -1
	set @StatusId = null
	
  insert @TableHeader
        (CreateDate,
         PrincipalId,
         OrderNumber,
         ExpectedDeliveryDate,
         StatusId,
         IssueLineId,
         DeliveryDate,
         PickDate,
         ActualShipDate
         )
  SELECT od.CreateDate,
		 od.PrincipalId,
		 od.OrderNumber,
		 od.DeliveryDate,
		 i.StatusId,
		 il.IssueLineId,
		 i.DeliveryDate,
		 i.Checking,
		 i.Checked
    FROM OutboundDocument od (nolock)
    join Issue i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    join IssueLine il (nolock) on i.IssueId = il.IssueId
   WHERE od.CreateDate between @FromDate and @ToDate
   and od.PrincipalId = isnull(@PrincipalId,od.PrincipalId)
   and od.OrderNumber = isnull(@OrderNumber,od.OrderNumber)
   and i.StatusId = isnull(@StatusId,i.StatusId)

  update @TableHeader
	  set ActualShipDate = null 
     from @TableHeader th 
    where th.StatusId in (dbo.ufn_StatusId('IS','CD'),dbo.ufn_StatusId('IS','C'),dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC'))
		
  update @TableHeader 
	 set DiffPickShipped = (select dbo.ufn_Hours_Between_Dates(PickDate, ActualShipDate, 0,0)),
	     DiffExpectedShipped = (select dbo.ufn_Hours_Between_Dates(ExpectedDeliveryDate, ActualShipDate, 0,0))
    from @TableHeader th 
        
  insert @TableDetail
        (PrincipalId,
         DeliveryDate)
  SELECT distinct PrincipalId,
		 DeliveryDate
    FROM @TableHeader th
    
    
  update @TableDetail 
	 set TotalOrders = (select COUNT(distinct OrderNumber) 
						  from @TableHeader th 
						 where td.PrincipalId = th.PrincipalId 
						   and th.DeliveryDate = td.DeliveryDate)
    from @TableDetail td 
  
  update @TableDetail 
	 set TONotMeasured = (select COUNT(distinct 0) 
						  from @TableHeader th 
						 where td.PrincipalId = th.PrincipalId 
						   and th.DeliveryDate = td.DeliveryDate
						   and th.StatusId = dbo.ufn_StatusId('IS','W'))
    from @TableDetail td 
    
    
  update @TableDetail 
	 set TOMeasured = (select COUNT(distinct OrderNumber) 
						  from @TableHeader th 
						 where td.PrincipalId = th.PrincipalId 
						   and th.DeliveryDate = td.DeliveryDate
						   and th.StatusId != dbo.ufn_StatusId('IS','W'))
    from @TableDetail td 
    
  update @TableDetail 
	 set TOLines = (select COUNT(th.IssueLineId) 
						  from @TableHeader th 
						 where td.PrincipalId = th.PrincipalId 
						   and th.DeliveryDate = td.DeliveryDate)
    from @TableDetail td
  
  if @Measure = 1  
  begin
  update @TableDetail 
	 set TOSLA = (select COUNT(distinct OrderNumber) 
						  from @TableHeader th 
						 where td.PrincipalId = th.PrincipalId 
						   and th.DeliveryDate = td.DeliveryDate
						   and th.DiffPickShipped = 0)
    from @TableDetail td
    
    update @TableDetail 
	 set TOLinesSLA = (select COUNT(distinct th.IssueLineId) 
						  from @TableHeader th 
						 where td.PrincipalId = th.PrincipalId 
						   and th.DeliveryDate = td.DeliveryDate
						   and th.DiffPickShipped = 0)
    from @TableDetail td
    end
  else
  begin
  update @TableDetail 
	 set TOSLA = (select COUNT(distinct OrderNumber) 
						  from @TableHeader th 
						 where td.PrincipalId = th.PrincipalId 
						   and th.DeliveryDate = td.DeliveryDate
						   and th.DiffExpectedShipped = 0)
    from @TableDetail td
    
    update @TableDetail 
	 set TOLinesSLA = (select COUNT(distinct th.IssueLineId) 
						  from @TableHeader th 
						 where td.PrincipalId = th.PrincipalId 
						   and th.DeliveryDate = td.DeliveryDate
						   and th.DiffExpectedShipped = 0)
    from @TableDetail td
    end
    
  
  
  update @TableHeader 
	 set DiffPickShipped = (select dbo.ufn_Hours_Between_Dates(PickDate, ActualShipDate, 0, 0)),
	     DiffExpectedShipped = (select dbo.ufn_Hours_Between_Dates(ExpectedDeliveryDate, ActualShipDate, 0, 0))	     
    from @TableHeader th 
    
    
  if @Measure = 1  
  update @TableDetail 
	 set TOSLA = (select COUNT(distinct OrderNumber) 
						  from @TableHeader th 
						 where td.PrincipalId = th.PrincipalId 
						   and th.DeliveryDate = td.DeliveryDate
						   and th.DiffPickShipped > 0)
    from @TableDetail td 
  else
  update @TableDetail 
	 set TOSLA = (select COUNT(distinct OrderNumber) 
						  from @TableHeader th 
						 where td.PrincipalId = th.PrincipalId 
						   and th.DeliveryDate = td.DeliveryDate
						   and th.DiffExpectedShipped > 0)
    from @TableDetail td 
    
  	SET ARITHABORT OFF
	SET ANSI_WARNINGS OFF
	
  update @TableDetail 
	 set PctOrdersSLA = (100 * td.TOSLA) / td.TotalOrders
    from @TableDetail td   
   where td.TotalOrders > 0
   
   update @TableDetail 
	 set PctOrderLinesSLA = (100 * td.TOLinesSLA) / td.TOLines
    from @TableDetail td   
   where td.TotalOrders > 0
 
  
  --update @TableDetail 
	 --set PctOrdersSLA = isnull(TOSLA,0) / isnull(TotalOrders,1) * 100
  --  from @TableDetail td
    
  --update @TableDetail 
	 --set PctOrderLinesSLA = isnull(TOLinesSLA,0) / isnull(TOLines,1) * 100
  --  from @TableDetail td   
 
       
  update  th
      set th.PrincipalCode = p.PrincipalCode
     from @TableDetail th
     join Principal p  (nolock) on p.PrincipalId = th.PrincipalId
     
   
  
  select *
    from @TableDetail
    where TotalOrders > 0

  
end
