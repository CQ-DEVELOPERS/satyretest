﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Despatch_Performance
  ///   Filename       : p_Report_Despatch_Performance.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 May 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Despatch_Performance
(
 @WarehouseId      int,
 @FromDate         datetime,
 @ToDate           datetime
)

as
begin
 set nocount on;
  
  declare @TableResult as table
  (
   OutboundDocumentTypeId int,
   OutboundDocumentType   nvarchar(30),
   OutboundDocumentId     int,
   IssueId                int,
   IssueLineId            int,
   JobId                  int,
   InstructionId          int,
   StorageUnitBatchId     int,
   StorageUnitId          int,
   ProductCode            nvarchar(30),
   Product                nvarchar(255),
   SKUCode                nvarchar(50),
   Quantity               float,
   ConfirmedQuantity      float,
   Weight                 float,
   ConfirmedWeight        float,
   Volume                 float,
   ConfirmedVolume        float,
   CreateDate             datetime,
   Release                datetime,
   StartDate              datetime,
   EndDate                datetime,
   Checking               datetime,
   Despatch               datetime,
   DespatchChecked        datetime,
   Complete               datetime,
   DeliveryDate           datetime,
   StatusCode             nvarchar(10)
  )
  
  declare @Config int
  
  select @Config = convert(int, Value)
    from Configuration (nolock)
   where WarehouseId = @WarehouseId
     and ConfigurationId = 191
  
  if @Config is null
    set @Config = 7
  
  insert @TableResult
        (OutboundDocumentTypeId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         JobId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         CreateDate,
         Release,
         StartDate,
         EndDate,
         Checking,
         Despatch,
         DespatchChecked,
         Complete)
  select ili.OutboundDocumentTypeId,
         ili.OutboundDocumentId,
         ili.IssueId,
         ili.IssueLineId,
         op.JobId,
         i.InstructionId,
         i.StorageUnitBatchId,
         ili.Quantity,
         ili.ConfirmedQuantity,
         op.CreateDate,
         op.Release,
         op.StartDate,
         op.EndDate,
         op.Checking,
         op.Despatch,
         op.DespatchChecked,
         op.Complete
    from OutboundPerformance   op (nolock)
    join Instruction            i (nolock) on op.JobId = i.JobId
    join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
   where (
		----op.Checking between @FromDate and @ToDate
      ----or  op.Complete        >= @FromDate -- between @FromDate and @ToDate
      ----or  op.Complete        is null
      op.StartDate between @FromDate and @ToDate
      )
      --and op.CreateDate <= @ToDate -- GS 2011-02-09 Replaced with line below
      ----and op.CreateDate between dateadd(dd,-@Config, @FromDate) and @ToDate
      and op.StartDate between dateadd(dd,-@Config, @FromDate) and @ToDate
	  and i.WarehouseId = @WarehouseId
  
  update tr
     set ConfirmedQuantity = 0,
         StatusCode        = s.StatusCode
    from @TableResult tr
    join Job           j (nolock) on tr.JobId = j.JobId
    join Status        s (nolock) on j.StatusId = s.StatusId
   where s.StatusCode = 'NS'
  
--  update tr
--     set StatusCode        = 'NS'
--    from @TableResult tr
--   where isnull(StatusCode,'') != 'NS'
--     and isnull(ConfirmedQuantity,0) = 0
  
--  delete tr
--    from @TableResult tr
--    join Job           j on tr.JobId = j.JobId
--    join Status        s on j.StatusId = s.StatusId
--   where s.StatusCode = 'NS'
  
--  delete tr
--    from @TableResult tr
--   where isnull(ConfirmedQuantity,0) = 0
  
  update tr
     set CreateDate = od.CreateDate
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set OutboundDocumentType = odt.OutboundDocumentType
    from @TableResult          tr
    join OutboundDocumentType odt (nolock) on tr.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  
  update tr
     set DeliveryDate = i.DeliveryDate
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
  
  update tr
     set StorageUnitId = sub.StorageUnitId
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
  
  update tr
     set ProductCode     = p.ProductCode,
         Product         = p.Product,
         SKUCode         = sku.SKUCode
    from @TableResult      tr
    join StorageUnit       su (nolock) on tr.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  update tr
     set Weight          = pk.Weight * tr.Quantity,
         ConfirmedWeight = pk.Weight * tr.ConfirmedQuantity,
         Volume          = pk.Volume * tr.Quantity,
         ConfirmedVolume = pk.Volume * tr.ConfirmedQuantity
    from @TableResult      tr
    join Pack              pk on tr.StorageUnitId = pk.StorageUnitId
   where pk.PackTypeId   = 2
     and pk.WarehouseId  = @WarehouseId
  
  update @TableResult
     set ConfirmedQuantity = 0
   where ConfirmedQuantity is null
  
  update @TableResult
     set ConfirmedWeight = 0
   where ConfirmedWeight is null
  
  update @TableResult
     set ConfirmedVolume = 0
   where ConfirmedVolume is null
  
  select 'Despatch KPI''s'                   as 'Group',
         '1. Order turnaround less than 3 hours' as 'Header',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         count(distinct(IssueLineId))        as 'Lines',
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableResult
   where datediff(hh, CreateDate, Complete) <= 3
     and Complete between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Despatch KPI''s',
         '2. Order turnaround less than 24 hours',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         count(distinct(IssueLineId))        as 'Lines',
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableResult
   where datediff(hh, CreateDate, Complete) between 4 and 24
     and Complete between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Despatch KPI''s',
         '3. Order turnaround less than 48 hours',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         count(distinct(IssueLineId))        as 'Lines',
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableResult
   where datediff(hh, CreateDate, Complete) between 25 and 48
     and Complete between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Despatch KPI''s',
         '4. Order turnaround greater than 48 hours',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         count(distinct(IssueLineId))        as 'Lines',
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableResult
   where datediff(hh, CreateDate, Complete) > 48
     and Complete between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Despatch KPI''s',
         '5. Summary of Despatches Achieved',
         'Less than 3 hours',
         case when count(distinct(OutboundDocumentId)) = 0 then null else count(distinct(OutboundDocumentId)) end,
         case when count(distinct(IssueLineId)) = 0 then null else count(distinct(IssueLineId)) end,
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableResult
   where datediff(hh, CreateDate, Complete) <= 3
     and Complete between @FromDate and @ToDate
  union
  select 'Despatch KPI''s',
         '5. Summary of Despatches Achieved',
         'Less than 24 hours',
         case when count(distinct(OutboundDocumentId)) = 0 then null else count(distinct(OutboundDocumentId)) end,
         case when count(distinct(IssueLineId)) = 0 then null else count(distinct(IssueLineId)) end,
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableResult
   where datediff(hh, CreateDate, Complete) between 4 and 24
     and Complete between @FromDate and @ToDate
  union
  select 'Despatch KPI''s',
         '5. Summary of Despatches Achieved',
         'Less than 48 hours',
         case when count(distinct(OutboundDocumentId)) = 0 then null else count(distinct(OutboundDocumentId)) end,
         case when count(distinct(IssueLineId)) = 0 then null else count(distinct(IssueLineId)) end,
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableResult
   where datediff(hh, CreateDate, Complete) between 25 and 48
     and Complete between @FromDate and @ToDate
  union
  select 'Despatch KPI''s',
         '5. Summary of Despatches Achieved',
         'Greater than 48 hours',
         case when count(distinct(OutboundDocumentId)) = 0 then null else count(distinct(OutboundDocumentId)) end,
         case when count(distinct(IssueLineId)) = 0 then null else count(distinct(IssueLineId)) end,
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableResult
   where datediff(hh, CreateDate, Complete) > 48
     and Complete between @FromDate and @ToDate
--  select 'Despatch KPI''s',
--         '5. Summary of Despatches Achieved',
--         OutboundDocumentType,
--         count(distinct(OutboundDocumentId)) as 'Orders',
--         count(distinct(IssueLineId))        as 'Lines',
--         sum(ConfirmedQuantity)              as 'Pieces',
--         sum(ConfirmedWeight)                as 'Weight'
--    from @TableResult
--   where Complete between @FromDate and @ToDate
--  group by OutboundDocumentType
  union
  select 'Work Load'                         as 'Group',
         '1. Brought Forward'                as 'Header',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         count(distinct(IssueLineId))        as 'Lines',
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableResult
   where isnull(Complete, @FromDate) >= @FromDate -- isnull(Complete, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and Checking < @FromDate
  group by OutboundDocumentType
  union
  select 'Work Load',
         '2. New Orders In',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         count(distinct(IssueLineId))        as 'Lines',
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableResult
   where Checking between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Work Load',
         '3. Completed',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         count(distinct(IssueLineId))        as 'Lines',
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableResult
   where Complete between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Work Load',
         '4. Carried Forward',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableResult tr1
   where isnull(Complete, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and Checking <= @ToDate
  group by OutboundDocumentType
  union
  select 'Despatch KPI''s',
         '6. Summary of Despatches Outstanding',
         'Due in 3 hrs',
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableResult
   where isnull(Complete, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and Checking <= @ToDate
     and datediff(hh, @FromDate, DeliveryDate) between 0 and 3
  union
  select 'Despatch KPI''s',
         '6. Summary of Despatches Outstanding',
         'Due in 24 hrs',
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableResult
   where isnull(Complete, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and Checking <= @ToDate
     and datediff(hh, @FromDate, DeliveryDate) between 4 and 24
  union
  select 'Despatch KPI''s',
         '6. Summary of Despatches Outstanding',
         'Due in 48 hrs',
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableResult
   where isnull(Complete, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and Checking <= @ToDate
     and datediff(hh, @FromDate, DeliveryDate) between 25 and 48
  union
  select 'Despatch KPI''s',
         '6. Summary of Despatches Outstanding',
         'Due in greater than 48 hrs',
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableResult
   where isnull(Complete, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and Checking <= @ToDate
     and datediff(hh, @FromDate, DeliveryDate) > 48
  union
  select 'Despatch KPI''s',
         '6. Summary of Despatches Outstanding',
         'Overdue',
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableResult
   where isnull(Complete, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and Checking <= @ToDate
     and datediff(hh, @FromDate, DeliveryDate) < 0
     and isnull(StatusCode,'') != 'NS'
end
 
