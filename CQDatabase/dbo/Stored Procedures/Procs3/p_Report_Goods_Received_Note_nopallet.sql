﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Goods_Received_Note_nopallet
  ///   Filename       : p_Report_Goods_Received_Note_nopallet.sql
  ///   Create By      : Karen
  ///   Date Created   : 22 Oct 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Goods_Received_Note_nopallet
(
 @InboundShipmentId int = null,
 @ReceiptId         int = null
)

as
begin
	 set nocount on;
  
  declare @TableHeader as Table
  (
   InboundShipmentId		int,
   InboundDocumentId		int,
   ReceiptId				int,
   OrderNumber				nvarchar(30),
   ExternalCompanyId		int,
   Supplier					nvarchar(255),
   SupplierCode				nvarchar(30),
   Division					nvarchar(50),
   DivisionCode				nvarchar(10),
   DeliveryNoteNumber		nvarchar(30),
   DeliveryDate				datetime,   
   SealNumber				nvarchar(30),
   VehicleRegistration		nvarchar(10),  
   ReceivingCompleteDate	datetime,
   PrintPrice				bit ,
   WarehouseId			    int,
   PrtSupplBCode			bit,
   GRN					    nvarchar(30)
  );
  
  declare @TableDetails as Table
  (
   ReceiptId			int,
   ReceiptLineId		int,
   StorageUnitBatchId	int,
   StorageUnitId		int,
   ProductCode			nvarchar(30),
   Product				nvarchar(255),
   SKUCode				nvarchar(50),
   SKU					nvarchar(50),
   ExpectedQuantity		numeric(13,6),
   AcceptedQuantity		numeric(13,6),
   RejectQuantity		numeric(13,6),
   ReasonId				int,
   Reason				nvarchar(50),
   Exception			nvarchar(255),
   Batch				nvarchar(50),
   ExpiryDate			datetime,
   SampleQty			numeric(13,6),
   OperatorCode			nvarchar(50),
   Operator				nvarchar(50),
   DeliveryNoteQuantity	float,
   Price				float,
   SupplierBarcode      nvarchar(50),
   PalletId				int
  );
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @ReceiptId = -1
    set @ReceiptId = null
  
  if @InboundShipmentId is not null
    insert @TableHeader
          (InboundShipmentId,
           InboundDocumentId,
           ReceiptId,
           OrderNumber,
           ExternalCompanyId,
           DeliveryNoteNumber,
           DeliveryDate,
           SealNumber,
           VehicleRegistration,
           ReceivingCompleteDate,
           WarehouseId,
           GRN)
    select isr.InboundShipmentId,
           id.InboundDocumentId,
           r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId,
           r.DeliveryNoteNumber,
           r.SealNumber,
           r.DeliveryDate,
           r.VehicleRegistration,
           r.ReceivingCompleteDate,
           id.WarehouseId,
           r.GRN
      from InboundShipmentReceipt isr
      join Receipt                  r on isr.ReceiptId = r.ReceiptId
      join InboundDocument         id on r.InboundDocumentId = id.InboundDocumentId
     where isr.InboundShipmentId = isnull(@InboundShipmentId, isr.InboundShipmentId)
  else if @ReceiptId is not null
    insert @TableHeader
          (InboundDocumentId,
           ReceiptId,
           OrderNumber,
           ExternalCompanyId,
           DeliveryNoteNumber,
           DeliveryDate,
           SealNumber,
           VehicleRegistration,
           ReceivingCompleteDate,
           WarehouseId,
           GRN)
    select id.InboundDocumentId,
           r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId,
           r.DeliveryNoteNumber,
           r.DeliveryDate,
           r.SealNumber,
           VehicleRegistration,
           r.ReceivingCompleteDate,
           id.WarehouseId,
           r.GRN
      from Receipt                  r
      join InboundDocument         id on r.InboundDocumentId = id.InboundDocumentId
      where r.ReceiptId = isnull(@ReceiptId, r.ReceiptId)
  
  insert @TableDetails
        (ReceiptId,
         ReceiptLineId,
         StorageUnitBatchId,
         ExpectedQuantity,
         AcceptedQuantity,
         RejectQuantity,
         SampleQty,
         OperatorCode,
         ReasonId,
         DeliveryNoteQuantity)
  select rl.ReceiptId,
         rl.ReceiptLineId,
         rl.StorageUnitBatchId,
         rl.RequiredQuantity,
         rl.AcceptedQuantity,
         rl.RejectQuantity,
         rl.SampleQuantity,
         o.OperatorCode,
         il.ReasonId,
         rl.DeliveryNoteQuantity
    from @TableHeader th
    join ReceiptLine  rl on th.ReceiptId     = rl.ReceiptId
    join InboundLine  il on rl.InboundLineId = il.InboundLineId
    left join Operator     o  on rl.OperatorId    = o.OperatorId
  
  update th
     set Supplier     = ec.ExternalCompany,
         SupplierCode = ec.ExternalCompanyCode
    from @TableHeader th
    join ExternalCompany ec on th.ExternalCompanyId = ec.ExternalCompanyId
  
  update th
     set Division = d.Division
        ,DivisionCode = d.DivisionCode
    from @TableHeader th
    join InboundDocument id on th.InboundDocumentId = id.InboundDocumentId
    join Division d on id.DivisionId = d.DivisionId
  
  update t
     set Exception  = e.Exception
    from @TableDetails t
    join Exception     e (nolock) on t.ReceiptLineId = e.ReceiptLineId

  update t
     set Reason     = r.Reason
    from @TableDetails t
    join Reason        r (nolock) on t.ReasonId      = r.ReasonId
    
  update t
     set Operator     = o.Operator
    from @TableDetails t
    join Operator        o (nolock) on t.OperatorCode      = o.OperatorCode
  
  update td
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         SKU		 = sku.SKU,
         Batch		 = b.Batch,
         ExpiryDate  = b.ExpiryDate,
         Price		 = su.UnitPrice,
         StorageUnitId = su.StorageUnitId
    from @TableDetails    td
    join StorageUnitBatch sub on td.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  on sub.StorageUnitId     = su.StorageUnitId
    join Product          p   on su.ProductId          = p.ProductId
    join SKU              sku on su.SKUId              = sku.SKUId
    join Batch            b   on sub.BatchId		   = b.BatchId
    
   update t
     set SupplierBarcode     = p.Barcode
    from @TableDetails t
    join Pack        p (nolock) on t.StorageUnitId      = p.StorageUnitId
    join PackType   pt (nolock) on p.PackTypeId         = pt.PackTypeId
   where pt.PackType = 'Supplier'  
   
   --update tr
   --  set PalletId = i.PalletId
   -- from @TableDetails tr
   -- join Instruction i on tr.ReceiptLineId = i.ReceiptLineId
     
  select distinct th.InboundShipmentId,
         th.OrderNumber,
         DeliveryNoteNumber,
         DeliveryDate,
         th.Supplier,
         th.SupplierCode,
         th.Division,
         th.GRN,
         th.DivisionCode,
         td.ProductCode,
         td.Product  + ' (' + isnull(td.Reason, '') + ')' AS Product,
         sum(td.ExpectedQuantity) as ExpectedQuantity,
         sum(AcceptedQuantity) as AcceptedQuantity,
         sum(RejectQuantity) as RejectQuantity,
         Exception,
         SealNumber,
         VehicleRegistration,
         td.Batch,
         td.ExpiryDate,
         sum(td.SampleQty) as SampleQty,
         td.Operator  as OperatorCode,
         sum(td.DeliveryNoteQuantity) as DeliveryNoteQuantity,
         ReceivingCompleteDate,
         SKUCode,
         SKU,
         sum(td.Price) as Price,
         (select Indicator from Configuration where ConfigurationId = 388 and WarehouseId = th.WarehouseId) as 'PrintPrice',
         isnull(td.Reason,' ') as 'Reason',
         td.SupplierBarcode,
         (select Indicator from Configuration where ConfigurationId = 396 and WarehouseId = th.WarehouseId) as 'PrtSupplBCode',
         PalletId,
         (select Indicator from Configuration where ConfigurationId = 302 and WarehouseId = th.WarehouseId) as 'PrtPalletId'
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
   where ExpectedQuantity >0
    or    AcceptedQuantity > 0
    or    RejectQuantity > 0
    or    SampleQty > 0
    group by th.InboundShipmentId,
         th.OrderNumber,
         DeliveryNoteNumber,
         DeliveryDate,
         th.Supplier,
         th.SupplierCode,
         th.Division,
         th.DivisionCode,
         th.GRN,
         td.ProductCode,
         td.Product,
         Product,
         Exception,
         SealNumber,
         VehicleRegistration,
         td.Batch,
         td.ExpiryDate,
         td.OperatorCode,
         td.Operator,
         td.Reason,
         th.ReceivingCompleteDate,
         td.skucode,
         td.SKU,
         th.WarehouseId,
         td.SupplierBarcode,
         td.PalletId

end
