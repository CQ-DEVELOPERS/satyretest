﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Route_Select_List
  ///   Filename       : p_Route_Select_List.sql
  ///   Create By      : Karen
  ///   Date Created   : March 2012
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Route table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Route.RouteId,
  ///   Route.Route 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Route_Select_List

as
begin
	 set nocount on;
  
	 declare @Error int
  select
         Route.RouteId,
         Route.Route 
    from Route
  
end
