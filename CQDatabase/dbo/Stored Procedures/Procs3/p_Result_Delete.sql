﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Result_Delete
  ///   Filename       : p_Result_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:08
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Result table.
  /// </remarks>
  /// <param>
  ///   @ResultId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Result_Delete
(
 @ResultId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Result
     where ResultId = @ResultId
  
  select @Error = @@Error
  
  
  return @Error
  
end
