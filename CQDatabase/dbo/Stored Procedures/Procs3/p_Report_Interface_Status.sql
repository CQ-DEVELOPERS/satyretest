﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Interface_Status
  ///   Filename       : p_Report_Interface_Status.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Jul 2008
  /// </summary>
    /// <summary>
  ///   Procedure Name : p_Report_Interface_Status
  ///   Filename       : p_Report_Interface_Status.sql
  ///   Modified By      : Junaid Desai
  ///   Date Modified   : 18 Jul 2008
  /// </summary>
  /// <remarks>
  ///   Modified so that Percentage may be coorect value. the initial sp will have to be changed.
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Interface_Status
(
 @WarehouseId       int,
 @OrderNumber       nvarchar(30),
 @FromDate          datetime,
 @ToDate            datetime
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
  (
   OrderNumber                     nvarchar(30),
   InvoiceNumber                   nvarchar(30),
   OutboundDocumentTypeId          int,
   OutboundDocumentTypeCode        nvarchar(10),
   OutboundDocumentType            nvarchar(50),
   OutboundShipmentId              int,
   IssueId                         int,
   ExternalCompanyId               int,
   ExternalCompany                 nvarchar(255),
   ExternalCompanyCode             nvarchar(30),
   CreateDate                      datetime,
   DeliveryDate                    datetime,
   Release                         datetime,
   Checked                         datetime,
   PickComplete                    float,
   PickTotal                       float,
   PickPercentage                  float,
   CheckingComplete                float,
   CheckingTotal                   float,
   CheckingPercentage              float,
   Status                          nvarchar(255),
   StatusCode                      nvarchar(10),
   StatusOrder                     int,
   StatusColour                    nvarchar(50),
   Extracted                       datetime,
   ReceivedFromHost                datetime,
   ReceivedByHost                  datetime,
   Data                            nvarchar(255),
   TotalQuantity                   float,
   InvoiceReceived                 datetime
  )
  
  declare @TableJobs as table
  (
   IssueId    int,
   JobId      int,
   StatusCode nvarchar(10)
  )
  
  insert @TableResult
        (ExternalCompany,
         ExternalCompanyCode,
         OrderNumber,
         InvoiceNumber,
         OutboundShipmentId,
         IssueId,
         CreateDate,
         DeliveryDate,
         OutboundDocumentTypeId,
         Status,
         StatusCode,
         TotalQuantity)
  select ec.ExternalCompany,
         ec.ExternalCompanyCode,
         od.OrderNumber,
         od.ReferenceNumber,
         null,
         i.IssueId,
         od.CreateDate,
         i.DeliveryDate,
         od.OutboundDocumentTypeId,
         s.Status,
         s.StatusCode,
         sum(il.ConfirmedQuatity)
    from OutboundDocument        od (nolock)
    join Issue                    i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    join IssueLine               il (nolock) on i.IssueId             = il.IssueId
    join ExternalCompany         ec (nolock) on od.ExternalCompanyId  = ec.ExternalCompanyId
    join Status                   s (nolock) on i.StatusId            = s.StatusId
   where od.WarehouseId = @WarehouseId
     and od.OrderNumber like '%' + @OrderNumber + '%'
     and od.CreateDate between @FromDate and @ToDate
     and s.StatusCode in ('SA','P','RL','CK','CD','DC','D','C')
   group by ec.ExternalCompany,
            ec.ExternalCompanyCode,
            od.OrderNumber,
            od.ReferenceNumber,
            i.IssueId,
            od.CreateDate,
            i.DeliveryDate,
            od.OutboundDocumentTypeId,
            s.Status,
            s.StatusCode
   order by ec.ExternalCompany,
            od.OrderNumber
  
--  update tr
--     set ReceivedFromHost = il.ProcessedDate,
--         Data             = substring(il.Data, 1, 20)
--    from @TableResult tr
--    join InterfaceLog il (nolock) on il.Data like '%SO' + tr.OrderNumber + '(%'
  
--  update tr
--     set ReceivedFromHost = il.ProcessedDate,
--         Data             = substring(il.Data, 1, 20)
--    from @TableResult tr
--    join InterfaceLog il (nolock) on il.Data like '%DL%' + tr.OrderNumber + '%'
--   where tr.Data is null
  
--  update @TableResult
--    set Data           = convert(nvarchar(20), convert(datetime, Data),120),
--        ReceivedByHost = convert(nvarchar(20), convert(datetime, Data),120)
--   where isdate(data) = 1
  
  update tr
     set ReceivedByHost = ild.ReceivedByHost
    from @TableResult tr
    join InterfaceLogDetail ild (nolock) on tr.OrderNumber = ild.OrderNumber
  
  update tr
     set InvoiceReceived = ii.ProcessedDate
    from @TableResult tr
    join InterfaceInvoice ii (nolock) on tr.InvoiceNumber = ii.InvoiceNumber
  
  update tr
     set Extracted = h.ProcessedDate
    from @TableResult tr
    join InterfaceExportSOHeader h (nolock) on tr.OrderNumber = h.PrimaryKey
                                            or tr.OrderNumber = h.OrderNumber
   where h.RecordStatus = 'Y'
    
--  update tr
--     set OutboundDocumentType = odt.OutboundDocumentType,
--         OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode
--    from @TableResult tr
--    join OutboundDocumentType odt on tr.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  
  update @TableResult
     set DeliveryDate = dateadd(hh, 3, DeliveryDate)
   where OutboundDocumentTypeCode in ('COD','CNC')
  
  insert @TableJobs
        (IssueId,
         JobId)
  select distinct
         ili.IssueId,
         i.JobId
    from @TableResult tr
    join IssueLineInstruction ili (nolock) on tr.IssueId = ili.IssueId
    join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
  
  update tj
     set StatusCode = s.StatusCode
    from @TableJobs tj
    join Job         j on tj.JobId = j.JobId
    join Status      s on j.StatusId = s.StatusId
--  
--  update tr
--     set Release = (select max(Release)
--                       from OutboundPerformance   op (nolock)
--                       join @TableJobs            tj on op.JobId = tj.JobId
--                      where tr.IssueId = tj.IssueId)
--    from @TableResult tr
--  
--  update tr
--     set Checked = (select max(Checked)
--                      from OutboundPerformance   op (nolock)
--                      join @TableJobs            tj on op.JobId = tj.JobId
--                     where tr.IssueId = tj.IssueId)
--    from @TableResult tr
  
  update tr
     set PickTotal = (select count(distinct(tj.JobId))
                        from @TableJobs tj
                       where tr.IssueId = tj.IssueId)
    from @TableResult tr
  
  update tr
     set PickComplete = (select count(distinct(tj.JobId))
                               from @TableJobs tj
                              where tr.IssueId     = tj.IssueId
                                and tj.StatusCode in ('NS','CK','CD','D','DC','C'))
    from @TableResult tr

  update @TableResult
     set PickPercentage = (PickComplete / PickTotal) * 100
   where PickComplete > 0
     and PickTotal    > 0
  
  update tr
     set CheckingTotal = (select count(distinct(tj.JobId))
                            from @TableJobs tj
                           where tr.IssueId = tj.IssueId)
    from @TableResult tr
  
  update tr
     set CheckingComplete = (select count(distinct(tj.JobId))
                               from @TableJobs tj
                              where tr.IssueId     = tj.IssueId
                                and tj.StatusCode in ('NS','CD','D','DC','C'))
    from @TableResult tr
  
  update @TableResult
     set CheckingPercentage = (CheckingComplete / CheckingTotal) * 100
   where CheckingComplete > 0
     and CheckingTotal    > 0
  
  update @TableResult
     set PickPercentage = 0
   where PickPercentage is null
  
  update @TableResult
     set CheckingPercentage = 0
   where CheckingPercentage is null
  
  update @TableResult
     set StatusColour = 'Red',
         StatusOrder  = '0'
   where StatusCode in ('P','PC','W','SA','OH')
  
  update @TableResult
     set StatusColour = 'Red',
         StatusOrder  = '0',
         Status       = Status + ' (' + convert(nvarchar(10), convert(int,PickPercentage)) + '%)'
   where StatusCode = 'PS'
  
  update @TableResult
     set StatusColour = 'Orange',
         StatusOrder  = '1',
         Status       = Status + ' (' + convert(nvarchar(10), convert(int,PickPercentage)) + '%)'
   where StatusCode in ('RL','CK')
  
  update @TableResult
     set StatusColour = 'LimeGreen',
         StatusOrder  = '3',
         Status       = Status + ' (' + convert(nvarchar(10), convert(int,CheckingPercentage)) + '%)'
   where StatusCode in ('CD','D','DC','C')
  
  update @TableResult
     set StatusColour = 'LimeGreen',
         StatusOrder  = '2',
         Status       = Status + ' (' + convert(nvarchar(10), convert(int,CheckingPercentage)) + '%)',
         InvoiceNumber = 'Picked : ' + convert(nvarchar(10), isnull(TotalQuantity, 0))
   where StatusCode in ('CD','D','DC','C')
     and TotalQuantity = 0
  
   update @TableResult
     set StatusColour = 'Red',
         StatusOrder  = '0',
         Status       = Status + ' (' + convert(nvarchar(10), convert(int,PickPercentage)) + '%)'
   where OrderNumber = InvoiceNumber
  
  
	--update @TableResult 
	--	Set Status = Message1
	--	from @TableResult t
	--			Join InterfaceImportIpHeader (nolock) i on i.PrimaryKey = t.OrderNumber
	--where t.OrderNumber = t.InvoiceNumber
	
   update @TableResult 
		Set Status = InterfaceMessage
		from @TableResult t
				Join InterfaceMessage (nolock) i on i.OrderNumber = t.OrderNumber
	where t.OrderNumber = t.InvoiceNumber
	and i.InterfaceTable = 'InterfaceExportSOHeader'
	
  select ExternalCompany,
         ExternalCompanyCode,
         OrderNumber,
         InvoiceNumber,
         OutboundDocumentType,
         CreateDate,
         Release,
         Checked,
         PickPercentage,
         CheckingPercentage,
         DeliveryDate,
         Status,
         StatusColour,
         TotalQuantity,
         Extracted,
         ReceivedFromHost,
         case when ReceivedByHost is null and InvoiceNumber is not null
              then dateadd(mi, 5, Extracted)
              else ReceivedByHost
              end as 'Data',
         InvoiceReceived
    from @TableResult
  order by InvoiceNumber,
           StatusOrder,
           ReceivedFromHost,
           Data,
           Extracted,
           Checked
           
--  order by DeliveryDate,
--           StatusOrder,
--           InvoiceNumber,
--           Checked,
--           Extracted,
--           Data,
--           ReceivedFromHost
end
