﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Warehouse_Capacity
  ///   Filename       : p_Report_Warehouse_Capacity.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 05 Nov 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Warehouse_Capacity
(
	@StartDate				Datetime,
	@EndDate				DateTime,
	@OperatorGroupId		int,
	@OperatorId				int = null,
	@UOM                    nvarchar(15) = 'Units',
	@WarehouseId			int = null
)

as
begin

if (@OperatorId = -1)
	Begin	
Set @OperatorId = Null
	End

if (@OperatorGroupId = -1)
	Begin	
Set @OperatorGroupId = Null
	End


	 set nocount on;

Declare @DateHoursDiff		int,
		@DateDaysDiff		int,
		@DateMonthsDiff		int,
		@Operators				int	

Select 
 @DateHoursDiff = DateDiff(hh,@StartDate,@EndDate)
,@DateDaysDiff = DateDiff(D,@StartDate,@EndDate)
,@DateMonthsDiff = DateDiff(M,@StartDate,@EndDate) 
	
Declare @TempTable As Table 
(
		OperatorGroupId				int,
		OperatorId					int,
		InstructionTypeId			int,
		EndDate						DateTime,		
		Units						numeric(10,3), 
		Weight						numeric(10,3), 
		Instructions				numeric(10,3), 
		Orders						numeric(10,3), 
		OrderLines					numeric(10,3),
		Jobs						numeric(10,3),
		ActiveTime					numeric(10,3), 
		DwellTime					numeric(10,3),
		SiteUnits					numeric(10,3), 
		SiteWeight					numeric(10,3), 
		SiteInstructions			numeric(10,3), 
		SiteOrders					numeric(10,3),         
		SiteOrderLines				numeric(10,3), 
		SiteJobs					numeric(10,3),
		IndustryUnits				numeric(10,3), 
		IndustryWeight				numeric(10,3), 
		IndustryInstructions		numeric(10,3),
		IndustryOrders				numeric(10,3),     
		IndustryOrderLines			numeric(10,3), 
		IndustryJobs				numeric(10,3),
		UOMValue					numeric(10,3),
		UOMSiteTarget				numeric(10,3),
		UOMIndustryTarget			numeric(10,3)
		
)

Insert Into @TempTable 
(
OperatorGroupId,
OperatorId,
InstructionTypeId,			
EndDate,		
Units,		
Weight,		
Instructions,		
Orders,		
OrderLines,		
Jobs,		
ActiveTime,		
DwellTime,		
SiteUnits,		
SiteWeight,		
SiteInstructions,		
SiteOrders,		
SiteOrderLines,		
SiteJobs,		
IndustryUnits,		
IndustryWeight,		
IndustryInstructions,	
IndustryOrders,		
IndustryOrderLines,		
IndustryJobs
)

(
SELECT     
			O.OperatorGroupId,
			Op.OperatorId,
			Op.InstructionTypeId,						
			Op.EndDate,								
			Convert(numeric(10,3),Sum(Op.Units)),							
			Convert(numeric(10,3),Sum(Op.Weight)),							
			Convert(numeric(10,3),Sum(Op.Instructions)),					
			Convert(numeric(10,3),Sum(Op.Orders)),							
			Convert(numeric(10,3),Sum(Op.OrderLines)),						
            Convert(numeric(10,3),Sum(Op.Jobs)),							
			Convert(numeric(10,3),Sum(Op.ActiveTime)),						
			Convert(numeric(10,3),Sum(Op.DwellTime)),						
			 Convert(numeric(10,3),Sum(Distinct(GP.Units))),
			 Convert(numeric(10,3),Sum(Distinct(GP.Weight))),
			 Convert(numeric(10,3),Sum(Distinct(GP.Instructions))),
			 Convert(numeric(10,3),Sum(Distinct(GP.Orders))),
			 Convert(numeric(10,3),Sum(Distinct(GP.OrderLines))),
			 Convert(numeric(10,3),Sum(Distinct(GP.Jobs))),
			 Convert(numeric(10,3),Sum(Distinct(IP.Units))),
			 Convert(numeric(10,3),Sum(Distinct(IP.Weight))),
			 Convert(numeric(10,3),Sum(Distinct(IP.Instructions))),
			 Convert(numeric(10,3),Sum(Distinct(IP.Orders))),
			 Convert(numeric(10,3),Sum(Distinct(IP.OrderLines))),
			 Convert(numeric(10,3),Sum(Distinct(IP.Jobs))	)		
			
			
FROM        OperatorPerformance Op INNER JOIN
                      Operator O ON Op.OperatorId = O.OperatorId INNER JOIN
                      (Select * From GroupPerformance Where PerformanceType = 'S' And OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)) AS GP ON O.OperatorGroupId = GP.OperatorGroupId 
					INNER JOIN
                      (Select * From GroupPerformance Where PerformanceType = 'I' And OperatorGroupId = isnull(@OperatorGroupId,OperatorGroupId)) AS IP ON O.OperatorGroupId = IP.OperatorGroupId		
Where convert(nvarchar,Op.EndDate,111) Between @StartDate And @EndDate
And O.OperatorGroupId = isnull(@OperatorGroupId,O.OperatorGroupId )
And OP.OperatorId = Isnull(@OperatorId,Op.OperatorId)
Group By EndDate,Op.OperatorId,Op.InstructionTypeId ,O.OperatorGroupId
)

Select @Operators = Count(Distinct(O.OperatorId)) From Operator O 
				Join OperatorGroup OG On O.OperatorGroupId = OG.OperatorGroupId
			Where OG.OperatorGroupId = isnull(@OperatorGroupId,OG.OperatorGroupId) 

if @UOM = 'Units'
begin
   Update @TempTable Set UOMValue = Units,
						UOMSiteTarget = SiteUnits,
						UOMIndustryTarget = IndustryUnits
						
end
else 
	if @UOM = 'Weight'
	begin
	 Update @TempTable Set UOMValue = Weight,
							UOMSiteTarget = SiteWeight,
						UOMIndustryTarget = IndustryWeight
	end
else 
	if @UOM = 'Jobs'
	begin
	 Update @TempTable Set UOMValue = Jobs,
							UOMSiteTarget = SiteJobs,
						UOMIndustryTarget = IndustryJobs
	end
else 
	if @UOM = 'Instructions'
	begin
	 Update @TempTable Set UOMValue = Instructions,
							UOMSiteTarget = SiteInstructions,
						UOMIndustryTarget = IndustryInstructions
	end


if (@DateMonthsDiff < 3)
	Begin
		if (@DateDaysDiff < 8)		
			Begin
				if (@DateHoursDiff < 25)
					Begin
						--Select 'x < 24 Hours' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
 					
Select 
		--OG.OperatorGroup,
		--T.OperatorId,
		--O.Operator,
		--IT.InstructionType, 
		--T.OperatorGroupId,			
		--T.InstructionTypeId,		
		Convert(nvarchar,T.EndDate,108) as EndDate, 
		Convert(nvarchar,T.EndDate,108) as ordscale, 					
		SUM((T.Units)) as Units,					
		SUM((T.Weight)) as Weight,					
		SUM((T.Instructions)) as Instructions,			
		SUM((T.Orders)) as Orders,					
		SUM(T.OrderLines) as OrderLines,				
		SUM((T.Jobs)) as Jobs,					
		SUM((T.ActiveTime)) as ActiveTime,				
		SUM((T.DwellTime)) as DwellTime ,				
		Convert(numeric(10,3),SUM(Distinct(T.SiteUnits))) as SiteUnits,				
		Convert(numeric(10,3),SUM(Distinct(T.SiteWeight))) as SiteWeight,				
		Convert(numeric(10,3),SUM(Distinct(T.SiteInstructions))) as SiteInstructions,		
		Convert(numeric(10,3),SUM(Distinct(T.SiteOrders))) as SiteOrders,				
		Convert(numeric(10,3),SUM(Distinct(T.SiteOrderLines))) as SiteOrderLines,			
		Convert(numeric(10,3),SUM(Distinct(T.SiteJobs))) as SiteJobs,				
		Convert(numeric(10,3),SUM(Distinct(T.IndustryUnits))) as IndustryUnits,			
		Convert(numeric(10,3),SUM(Distinct(T.IndustryWeight))) as IndustryWeight,			
		Convert(numeric(10,3),SUM(Distinct(T.IndustryInstructions))) as IndustryInstructions,	
		Convert(numeric(10,3),SUM(Distinct(T.IndustryOrders))) as IndustryOrders,			
		Convert(numeric(10,3),SUM(Distinct(T.IndustryOrderLines))) as IndustryOrderLines,		
		Convert(numeric(10,3),SUM(Distinct(T.IndustryJobs))) as IndustryJobs,			
		Convert(numeric(10,3),SUM((T.UOMValue))) as UOMValue,
		Convert(numeric(10,3),SUM((T.UOMValue))) as UOMValueperHour,
		@Operators * Convert(numeric(10,3),SUM(Distinct(T.UOMSiteTarget))) as UOMSiteTargetCapacity,
		@Operators * Convert(numeric(10,3),SUM(Distinct(T.UOMIndustryTarget))) as UOMIndustryTargetCapacity,
		Count(Distinct(T.OperatorId)) as ActiveOperators,
		@Operators as OperatorsCapacity								
	From @TempTable T
		--Inner Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		--Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		--Inner Join Operator O			On O.OperatorId				= T.OperatorId
	Group by Convert(nvarchar,EndDate,108)--O.Operator,T.OperatorId,IT.InstructionType, T.InstructionTypeId --,Uom,T.OperatorGroupId,OG.OperatorGroup,
	ORDER BY ordscale

						
						-- Select data by this Scale
					End
				Else
					Begin
					--Select '24 Hours < x < 1 week' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
Select 
		--OG.OperatorGroup,
		--T.OperatorId,
		--O.Operator,
		--IT.InstructionType, 
		--T.OperatorGroupId,			
		--T.InstructionTypeId,		
		Convert(nvarchar,T.EndDate,101) as EndDate, 
		Convert(nvarchar,T.EndDate,101) as ordscale , 					
		SUM(T.Units) as Units,					
		SUM(T.Weight) as Weight,					
		SUM(T.Instructions) as Instructions,			
		SUM(T.Orders) as Orders,					
		SUM(T.OrderLines) as OrderLines,				
		SUM(T.Jobs) as Jobs,					
		SUM(T.ActiveTime) as ActiveTime,				
		SUM(T.DwellTime) as DwellTime,				
		8 * Convert(numeric(10,3),SUM(Distinct(T.SiteUnits))) as SiteUnits,				
		8 * Convert(numeric(10,3),SUM(Distinct(T.SiteWeight))) as SiteWeight,				
		8 * Convert(numeric(10,3),SUM(Distinct(T.SiteInstructions))) as SiteInstructions,		
		8 * Convert(numeric(10,3),SUM(Distinct(T.SiteOrders))) as SiteOrders,				
		8 * Convert(numeric(10,3),SUM(Distinct(T.SiteOrderLines))) as SiteOrderLines,			
		8 * Convert(numeric(10,3),SUM(Distinct(T.SiteJobs))) as SiteJobs,				
		8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryUnits))) as IndustryUnits,			
		8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryWeight))) as IndustryWeight,			
		8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryInstructions))) as IndustryInstructions,	
		8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryOrders))) as IndustryOrders,			
		8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryOrderLines))) as IndustryOrderLines,		
		8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryJobs))) as IndustryJobs,			
		Convert(numeric(10,3),SUM((T.UOMValue))) as UOMValue,
		Convert(numeric(10,3),(SUM(T.UOMValue)/8)) as UOMValueperHour,
		8 * @Operators * Convert(numeric(10,3),SUM(Distinct(T.UOMSiteTarget))) as UOMSiteTargetCapacity,
		8 * @Operators * Convert(numeric(10,3),SUM(Distinct(T.UOMIndustryTarget))) as UOMIndustryTargetCapacity,
		Count(Distinct(T.OperatorId)) as ActiveOperators,
		@Operators as OperatorsCapacity			
	From @TempTable T
		--Inner Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		--Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		--Inner Join Operator O			On O.OperatorId				= T.OperatorId
							Group by Convert(nvarchar,T.EndDate,101)--,O.Operator,T.OperatorId,IT.InstructionType, T.InstructionTypeId,T.OperatorGroupId,OG.OperatorGroup,
							ORDER BY ordscale						
						-- Select data by this Scale
					End
			End
			Else
				Begin
					--Select '1 Week< x < 3 months' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
Select 
		--OG.OperatorGroup,
		--T.OperatorId,
		--O.Operator,
		--IT.InstructionType, 
		--T.OperatorGroupId,			
		--T.InstructionTypeId,	
		DatePart(wk,T.EndDate) as EndDate, 
		DatePart(wk,T.EndDate) as ordscale , 					
		SUM(T.Units) as Units,					
		SUM(T.Weight) as Weight,					
		SUM(T.Instructions) as Instructions,			
		SUM(T.Orders) as Orders,					
		SUM(T.OrderLines) as OrderLines,				
		SUM(T.Jobs) as Jobs,					
		SUM(T.ActiveTime) as ActiveTime,				
		SUM(T.DwellTime) as DwellTime,				
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteUnits)))) as SiteUnits,				
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteWeight)))) as SiteWeight,				
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteInstructions)))) as SiteInstructions,		
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteOrders)))) as SiteOrders,				
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteOrderLines)))) as SiteOrderLines,			
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteJobs)))) as SiteJobs,				
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryUnits)))) as IndustryUnits,			
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryWeight)))) as IndustryWeight,			
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryInstructions)))) as IndustryInstructions,	
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryOrders)))) as IndustryOrders,			
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryOrderLines)))) as IndustryOrderLines,		
		5 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryJobs)))) as IndustryJobs,
		Convert(numeric(10,3),SUM((T.UOMValue))) as UOMValue,		
		Convert(numeric(10,3),((SUM(T.UOMValue)/5)/8)) as UOMValueperHour,
		5 * (8 * @Operators * Convert(numeric(10,3),SUM(Distinct(T.UOMSiteTarget)))) as UOMSiteTargetCapacity,
		5 * (8 * @Operators * Convert(numeric(10,3),SUM(Distinct(T.UOMIndustryTarget)))) as UOMIndustryTargetCapacity,
		Count(Distinct(T.OperatorId)) as ActiveOperators,
		@Operators as OperatorsCapacity						
	From @TempTable T
		--Inner Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		--Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		--Inner Join Operator O			On O.OperatorId				= T.OperatorId
							Group by DatePart(wk,T.EndDate)--,O.Operator,T.OperatorId,IT.InstructionType, T.InstructionTypeId,T.OperatorGroupId,OG.OperatorGroup,
							ORDER BY ordscale		
				
					-- Select data by this Scale
				End
	End
Else
	Begin
			--Select  '3 months < x' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
Select 
		--OG.OperatorGroup,
		--T.OperatorId,
		--O.Operator,
		--IT.InstructionType, 
		--T.OperatorGroupId,			
		--T.InstructionTypeId,	
		DateName(M,T.EndDate) as EndDate, 
		DatePart(M,T.EndDate) as ordscale, 					
		SUM(T.Units) as Units,					
		SUM(T.Weight) as Weight,					
		SUM(T.Instructions) as Instructions,			
		SUM(T.Orders) as Orders,					
		SUM(T.OrderLines) as OrderLines,				
		SUM(T.Jobs) as Jobs,					
		SUM(T.ActiveTime) as ActiveTime,				
		SUM(T.DwellTime) as DwellTime,				
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteUnits)))) as SiteUnits,				
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteWeight)))) as SiteWeight,				
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteInstructions)))) as SiteInstructions,		
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteOrders)))) as SiteOrders,				
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteOrderLines)))) as SiteOrderLines,			
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.SiteJobs)))) as SiteJobs,				
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryUnits)))) as IndustryUnits,			
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryWeight)))) as IndustryWeight,			
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryInstructions)))) as IndustryInstructions,
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryOrders)))) as IndustryOrders,			
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryOrderLines)))) as IndustryOrderLines,	
		22 * (8 * Convert(numeric(10,3),SUM(Distinct(T.IndustryJobs)))) as IndustryJobs,
		Convert(numeric(10,3),SUM((T.UOMValue))) as UOMValue,			
		Convert(numeric(10,3),((SUM(T.UOMValue)/22)/8)) as UOMValueperHour,
		22 * (8 * @Operators * Convert(numeric(10,3),SUM(Distinct(T.UOMSiteTarget)))) as UOMSiteTargetCapacity,
		22 * (8 * @Operators * Convert(numeric(10,3),SUM(Distinct(T.UOMIndustryTarget)))) as UOMIndustryTargetCapacity,
		Count(Distinct(T.OperatorId)) as ActiveOperators,
		@Operators as OperatorsCapacity						
	From @TempTable T
		--Inner Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		--Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		--Inner Join Operator O			On O.OperatorId				= T.OperatorId
							Group by DatePart(M,T.EndDate),DateName(M,T.EndDate)--,O.Operator,T.OperatorId,IT.InstructionType, T.InstructionTypeId,T.OperatorGroupId,OG.OperatorGroup
							ORDER BY ordscale
	End


--Select * From @TempTable

end	   		
