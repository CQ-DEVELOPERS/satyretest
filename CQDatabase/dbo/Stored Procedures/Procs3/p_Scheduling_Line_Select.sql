﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Scheduling_Line_Select
  ///   Filename       : p_Scheduling_Line_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Scheduling_Line_Select
(
 @ReceiptId int
)

as
begin
	 set nocount on;
	 
	 select vs.ProductCode,
         vs.Product,
         vs.Batch,
         vs.SKUCode,
         rl.RequiredQuantity,
         case when (select count(1) from viewSUL sul where vs.StorageUnitId = sul.StorageUnitId and sul.AreaCode != 'SP') > 0
              then 'Green'
              else 'Red'
              end as 'Pickface',
         case when (select count(1) from viewSUA sua where vs.StorageUnitId = sua.StorageUnitId and sua.AreaCode != 'SP') > 0
              then 'Green'
              else 'Red'
              end as 'PutawayRules'
    from ReceiptLine rl
    join viewStock vs on rl.StorageUnitBatchId = vs.StorageUnitBatchId
   where rl.ReceiptId = @ReceiptId
end
