﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReportHeading_Insert
  ///   Filename       : p_ReportHeading_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:24
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ReportHeading table.
  /// </remarks>
  /// <param>
  ///   @ReportHeadingId int = null output,
  ///   @CultureId int = null,
  ///   @ReportHeadingCode nvarchar(200) = null,
  ///   @ReportHeading nvarchar(200) = null 
  /// </param>
  /// <returns>
  ///   ReportHeading.ReportHeadingId,
  ///   ReportHeading.CultureId,
  ///   ReportHeading.ReportHeadingCode,
  ///   ReportHeading.ReportHeading 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReportHeading_Insert
(
 @ReportHeadingId int = null output,
 @CultureId int = null,
 @ReportHeadingCode nvarchar(200) = null,
 @ReportHeading nvarchar(200) = null 
)

as
begin
	 set nocount on;
  
  if @ReportHeadingId = '-1'
    set @ReportHeadingId = null;
  
  if @ReportHeadingCode = '-1'
    set @ReportHeadingCode = null;
  
  if @ReportHeading = '-1'
    set @ReportHeading = null;
  
	 declare @Error int
 
  insert ReportHeading
        (CultureId,
         ReportHeadingCode,
         ReportHeading)
  select @CultureId,
         @ReportHeadingCode,
         @ReportHeading 
  
  select @Error = @@Error, @ReportHeadingId = scope_identity()
  
  
  return @Error
  
end
