﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitHistory_Insert
  ///   Filename       : p_StorageUnitHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:02:10
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnitHistory table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @SKUId int = null,
  ///   @ProductId int = null,
  ///   @CommandType varchar(10) = null,
  ///   @InsertDate datetime = null,
  ///   @ProductCategory char(1) = null,
  ///   @PackingCategory char(1) = null,
  ///   @PickEmpty bit = null,
  ///   @StackingCategory int = null,
  ///   @MovementCategory int = null,
  ///   @ValueCategory int = null,
  ///   @StoringCategory int = null,
  ///   @PickPartPallet int = null,
  ///   @MinimumQuantity int = null,
  ///   @ReorderQuantity int = null,
  ///   @MaximumQuantity int = null,
  ///   @DefaultQC bit = null,
  ///   @UnitPrice numeric(13,2) = null,
  ///   @Size nvarchar(200) = null,
  ///   @Colour nvarchar(200) = null,
  ///   @Style nvarchar(200) = null,
  ///   @PreviousUnitPrice numeric(13,2) = null,
  ///   @StockTakeCounts int = null,
  ///   @SerialTracked bit = null,
  ///   @SizeCode nvarchar(200) = null,
  ///   @ColourCode nvarchar(200) = null,
  ///   @StyleCode nvarchar(200) = null,
  ///   @StatusId int = null,
  ///   @PutawayRule nvarchar(400) = null,
  ///   @AllocationRule nvarchar(400) = null,
  ///   @BillingRule nvarchar(400) = null,
  ///   @CycleCountRule nvarchar(400) = null,
  ///   @LotAttributeRule nvarchar(400) = null,
  ///   @StorageCondition nvarchar(400) = null,
  ///   @ManualCost nvarchar(60) = null,
  ///   @OffSet nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   StorageUnitHistory.StorageUnitId,
  ///   StorageUnitHistory.SKUId,
  ///   StorageUnitHistory.ProductId,
  ///   StorageUnitHistory.CommandType,
  ///   StorageUnitHistory.InsertDate,
  ///   StorageUnitHistory.ProductCategory,
  ///   StorageUnitHistory.PackingCategory,
  ///   StorageUnitHistory.PickEmpty,
  ///   StorageUnitHistory.StackingCategory,
  ///   StorageUnitHistory.MovementCategory,
  ///   StorageUnitHistory.ValueCategory,
  ///   StorageUnitHistory.StoringCategory,
  ///   StorageUnitHistory.PickPartPallet,
  ///   StorageUnitHistory.MinimumQuantity,
  ///   StorageUnitHistory.ReorderQuantity,
  ///   StorageUnitHistory.MaximumQuantity,
  ///   StorageUnitHistory.DefaultQC,
  ///   StorageUnitHistory.UnitPrice,
  ///   StorageUnitHistory.Size,
  ///   StorageUnitHistory.Colour,
  ///   StorageUnitHistory.Style,
  ///   StorageUnitHistory.PreviousUnitPrice,
  ///   StorageUnitHistory.StockTakeCounts,
  ///   StorageUnitHistory.SerialTracked,
  ///   StorageUnitHistory.SizeCode,
  ///   StorageUnitHistory.ColourCode,
  ///   StorageUnitHistory.StyleCode,
  ///   StorageUnitHistory.StatusId,
  ///   StorageUnitHistory.PutawayRule,
  ///   StorageUnitHistory.AllocationRule,
  ///   StorageUnitHistory.BillingRule,
  ///   StorageUnitHistory.CycleCountRule,
  ///   StorageUnitHistory.LotAttributeRule,
  ///   StorageUnitHistory.StorageCondition,
  ///   StorageUnitHistory.ManualCost,
  ///   StorageUnitHistory.OffSet 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitHistory_Insert
(
 @StorageUnitId int = null,
 @SKUId int = null,
 @ProductId int = null,
 @CommandType varchar(10) = null,
 @InsertDate datetime = null,
 @ProductCategory char(1) = null,
 @PackingCategory char(1) = null,
 @PickEmpty bit = null,
 @StackingCategory int = null,
 @MovementCategory int = null,
 @ValueCategory int = null,
 @StoringCategory int = null,
 @PickPartPallet int = null,
 @MinimumQuantity int = null,
 @ReorderQuantity int = null,
 @MaximumQuantity int = null,
 @DefaultQC bit = null,
 @UnitPrice numeric(13,2) = null,
 @Size nvarchar(200) = null,
 @Colour nvarchar(200) = null,
 @Style nvarchar(200) = null,
 @PreviousUnitPrice numeric(13,2) = null,
 @StockTakeCounts int = null,
 @SerialTracked bit = null,
 @SizeCode nvarchar(200) = null,
 @ColourCode nvarchar(200) = null,
 @StyleCode nvarchar(200) = null,
 @StatusId int = null,
 @PutawayRule nvarchar(400) = null,
 @AllocationRule nvarchar(400) = null,
 @BillingRule nvarchar(400) = null,
 @CycleCountRule nvarchar(400) = null,
 @LotAttributeRule nvarchar(400) = null,
 @StorageCondition nvarchar(400) = null,
 @ManualCost nvarchar(60) = null,
 @OffSet nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert StorageUnitHistory
        (StorageUnitId,
         SKUId,
         ProductId,
         CommandType,
         InsertDate,
         ProductCategory,
         PackingCategory,
         PickEmpty,
         StackingCategory,
         MovementCategory,
         ValueCategory,
         StoringCategory,
         PickPartPallet,
         MinimumQuantity,
         ReorderQuantity,
         MaximumQuantity,
         DefaultQC,
         UnitPrice,
         Size,
         Colour,
         Style,
         PreviousUnitPrice,
         StockTakeCounts,
         SerialTracked,
         SizeCode,
         ColourCode,
         StyleCode,
         StatusId,
         PutawayRule,
         AllocationRule,
         BillingRule,
         CycleCountRule,
         LotAttributeRule,
         StorageCondition,
         ManualCost,
         OffSet)
  select @StorageUnitId,
         @SKUId,
         @ProductId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @ProductCategory,
         @PackingCategory,
         @PickEmpty,
         @StackingCategory,
         @MovementCategory,
         @ValueCategory,
         @StoringCategory,
         @PickPartPallet,
         @MinimumQuantity,
         @ReorderQuantity,
         @MaximumQuantity,
         @DefaultQC,
         @UnitPrice,
         @Size,
         @Colour,
         @Style,
         @PreviousUnitPrice,
         @StockTakeCounts,
         @SerialTracked,
         @SizeCode,
         @ColourCode,
         @StyleCode,
         @StatusId,
         @PutawayRule,
         @AllocationRule,
         @BillingRule,
         @CycleCountRule,
         @LotAttributeRule,
         @StorageCondition,
         @ManualCost,
         @OffSet 
  
  select @Error = @@Error
  
  
  return @Error
  
end
