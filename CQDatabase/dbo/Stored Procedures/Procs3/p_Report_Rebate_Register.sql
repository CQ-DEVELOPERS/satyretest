﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Rebate_Register
  ///   Filename       : p_Report_Rebate_Register.sql
  ///   Create By      : Karen
  ///   Date Created   : April 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Rebate_Register
(
 @FromDate          datetime,
 @ToDate            datetime
)

as
begin
	 set nocount on;
	 
	 declare @Weeks int,
	         @Count int,
	         @Date  datetime
	 

  
  declare @TableResult as table
		  (BOEDate				datetime,
		   BOE					nvarchar(255),
		   CreateDate			datetime,
		   ProductCode			nvarchar(50),
		   Product				nvarchar(255),
		   ContainerNumber		nvarchar(50),
		   SupplDeliveryNote	nvarchar(50),
		   AdditionalInfo		nvarchar(100),
		   DeliveryNote			nvarchar(50),
		   ArrivalDate			datetime,
		   ADORIssueNo			nvarchar(50),
		   Received				decimal(11,0),
		   Adjusted				decimal(11,0),
		   Issued				decimal(11,0),
		   Balance				decimal(11,0),
		   StockOnHand			int,
		   WarehouseId			int,
		   StorageUnitId		int,
		   SKUId				int,
		   ProductId			int
  )
  
  declare @TableSumm as table
		  (BOEDate				datetime,
		   BOE					nvarchar(255),
		   CreateDate			datetime,
		   ProductCode			nvarchar(50),
		   Product				nvarchar(255),
		   ContainerNumber		nvarchar(50),
		   SupplDeliveryNote	nvarchar(50),
		   AdditionalInfo		nvarchar(100),
		   DeliveryNote			nvarchar(50),
		   ArrivalDate			datetime,
		   ADORIssueNo			nvarchar(50),
		   Received				int,
		   Adjusted				int,
		   Issued				int,
		   Balance				int,
		   StockOnHand			int,
		   WarehouseId			int,
		   StorageUnitId		int,
		   SKUId				int,
		   ProductId			int
  )
  
 
    
    insert @TableResult
          (BOEDate,
           BOE,
           WarehouseId,
           StorageUnitId,
           SKUId,
           ProductId,
           Received,
           ContainerNumber,
           DeliveryNote,
           CreateDate,
           ArrivalDate,
           ADORIssueNo)
    select r.DeliveryDate,
		   r.BOE,
           r.WarehouseId,
           sub.StorageUnitId,
           su.SKUId,
           su.ProductId,
           rl.AcceptedQuantity,
           r.ContainerNumber,
           r.DeliveryNoteNumber,
           id.CreateDate,
           r.DeliveryDate,
           id.OrderNumber
       from ReceiptLine        rl (nolock)         
	   join Receipt             r (nolock) on rl.ReceiptId = r.ReceiptId  
	   join InboundDocument    id (nolock) on r.InboundDocumentId = id.InboundDocumentId
	   join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
	   join status              s (nolock) on rl.statusid = s.statusid  
	   join StorageUnitBatch  sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId  
	   join StorageUnit        su (nolock) on sub.StorageUnitId = su.StorageUnitId
      where s.StatusId = dbo.ufn_StatusId('R','RC')
	    and CONVERT(datetime, CONVERT(varchar,r.DeliveryDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  
	    and r.BOE is not null
	    and r.BOE != ''
	    and idt.InboundDocumentTypeCode = 'BOND'

	   
	 insert @TableResult
          (BOEDate,
           BOE,
           WarehouseId,
           StorageUnitId,
           SKUId,
           ProductId,
           Adjusted,
           --ContainerNumber,
           DeliveryNote,
           CreateDate)
    select i.DeliveryDate,
		   od.BOE,
           i.WarehouseId,
           sub.StorageUnitId,
           su.SKUId,
           su.ProductId,
           il.ConfirmedQuatity,
           --i.ContainerNumber,
           i.DeliveryNoteNumber,
           od.CreateDate
       from IssueLine          il (nolock)         
	   join Issue               i (nolock) on il.IssueId = i.IssueId  
	   join OutboundDocument   od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
	   join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
	   join status              s (nolock) on il.statusid = s.statusid  
	   join StorageUnitBatch  sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId  
	   join StorageUnit        su (nolock) on sub.StorageUnitId = su.StorageUnitId
      where s.statusid in (dbo.ufn_StatusId('IS','DC'),dbo.ufn_StatusId('IS','D'))
      and CONVERT(datetime, CONVERT(varchar,i.DeliveryDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  
      and od.BOE is not null
      and od.BOE != ''
      and odt.OutboundDocumentTypeCode = 'BOND'
      
      
   insert @TableResult
          (BOEDate,
           BOE,
           WarehouseId,
           StorageUnitId,
           SKUId,
           ProductId,
           Adjusted,
           --ContainerNumber,
           DeliveryNote,
           CreateDate)
  SELECT    i.CreateDate,
		    b.billofentry, 
            i.WarehouseId, 
            sub.StorageUnitId, 
            SKUId,
            su.ProductId,
            CASE 
			WHEN isnull(i.Quantity,0) > isnull(i.ConfirmedQuantity,0) THEN -(isnull(i.Quantity,0) - isnull(i.ConfirmedQuantity,0)) 
			ELSE isnull(i.Quantity,0) - isnull(i.ConfirmedQuantity,0)
			END,
            --isnull(i.Quantity,0) - isnull(i.ConfirmedQuantity,0),
			null,
			i.CreateDate
FROM        Instruction			i (nolock) 
INNER JOIN  InstructionType	   it (nolock) on i.InstructionTypeId = it.InstructionTypeId
	  join	Job                 j (nolock) on j.JobId = i.JobId
	  join  StorageUnitBatch  sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId 
	  join  Batch               b (nolock) on sub.BatchId = b.BatchId
	  join  StorageUnit        su (nolock) on sub.StorageUnitId = su.StorageUnitId
     WHERE  it.InstructionTypeCode in ('STE','STL','STA','STP')
     and i.CreateDate   between @FromDate and @ToDate
     and i.ConfirmedQuantity > 0
     and b.BillOfEntry is not null
     and b.BillOfEntry != ''


	update tr
       set ProductCode = p.ProductCode,
		   Product = p.Product
	  from @TableResult tr
	  join Product p (nolock) on tr.ProductId = p.ProductId
  
	SET ARITHABORT OFF
	SET ANSI_WARNINGS OFF   

  
	 insert @TableSumm
          (BOE,
		   ProductCode,
		   Product,
		   Received,
		   Adjusted,
		   Issued,
		   WarehouseId,
		   StorageUnitId,
		   SKUId,
		   ProductId)
    select distinct 
		   BOE,
		   ProductCode,
		   Product,
		   sum(isnull(Received,0)) as Received,
		   sum(isnull(Adjusted,0)) as Adjusted,
		   sum(isnull(Issued,0)) as Issued,
		   WarehouseId,
		   StorageUnitId,
		   SKUId,
		   ProductId
      from @TableResult        rl 
      group by BOE,
		   ProductCode,
		   Product,
		   WarehouseId,
		   StorageUnitId,
		   SKUId,
		   ProductId
       
    update tr
       set Balance = isnull(tr.Received,0) + isnull(tr.Adjusted,0) - isnull(tr.Issued,0)
	  from @TableSumm tr   
	  
	update ts
       set ts.BOEDate = tr.BOEDate,
		   ts.CreateDate = tr.CreateDate,
		   ts.ContainerNumber = tr.ContainerNumber,
		   ts.SupplDeliveryNote = tr.SupplDeliveryNote,
		   ts.AdditionalInfo = tr.AdditionalInfo,
		   ts.DeliveryNote = tr.DeliveryNote,
		   ts.ArrivalDate = tr.ArrivalDate,
		   ts.ADORIssueNo = tr.ADORIssueNo,
		   ts.StockOnHand = tr.StockOnHand		   
	  from @TableSumm ts
	  join @TableResult   tr on ts.BOE = tr.BOE  
  

	select *		  
      from @TableSumm tr
     order by tr.BOE, tr.BOEDate, tr.ProductCode


end 
