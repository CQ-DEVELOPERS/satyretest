﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_Master
  ///   Filename       : p_Report_Product_Master.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Product_Master

as
begin
	 set nocount on;
  
  select p.ProductCode,
         replace(p.Product,',','') as 'Product',
         sku.SKUCode,
         sku.SKU,
         (select max(plt.Quantity) from Pack plt where plt.StorageUnitId = su.StorageUnitId) as 'PalletQuantity',
         p.Barcode as 'ProductBarcode',
         su.MinimumQuantity,
         su.ReorderQuantity,
         su.MaximumQuantity,
         p.CuringPeriodDays,
         p.ShelfLifeDays,
         p.QualityAssuranceIndicator,
         p.ProductType,
         su.ProductCategory,
         OverReceipt,
         RetentionSamples,
         HostId,
         PackTypeCode,
         PackType,
         pk.Quantity,
         pk.Barcode as 'PackBarcode',
         pk.[Length],
         pk.Width,
         pk.Height,
         pk.Volume,
         pk.NettWeight,
         pk.[Weight] as 'GrossWeight',
         pk.TareWeight,
         su.ProductCategory as 'PackCategory',
         su.PackingCategory,
         su.PickEmpty,
         su.StackingCategory,
         su.MovementCategory,
         su.ValueCategory,
         su.StoringCategory,
         su.PickPartPallet,
         pn.PrincipalCode,
         su.UnitPrice,
         su.StyleCode,
         su.Style,
         su.ColourCode,
         su.Colour,
         su.SizeCode,
         su.Size,
         p.Description2,
         p.Description3,
         p.ProductAlias,
         su.PutawayRule,
         su.AllocationRule,
         su.BillingRule,
         su.CycleCountRule,
         su.LotAttributeRule,
         su.StorageCondition,
         su.SerialTracked
    from StorageUnit su (nolock)
    join Product      p (nolock) on su.ProductId = p.ProductId
    join SKU        sku (nolock) on su.SKUId     = sku.SKUId
    left
    join Pack        pk (nolock) on su.StorageUnitId = pk.StorageUnitId
    left
    join PackType    pt (nolock) on pk.PackTypeId = pt.PackTypeId
    left
    join Principal   pn (nolock) on p.PrincipalId = pn.PrincipalId
    left
    join ProductCategory pc (nolock) on p.ProductCategoryId = pc.ProductCategoryId
end
