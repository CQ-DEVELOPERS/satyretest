﻿
/*----------------------------------------------------------------------*/
/* Name         : sp_grants_space                                       */
/* Filename     : sp_grants_space.sql                                   */
/* Description  :                                                       */
/* Input Par    :                                                       */
/* Output Par   :                                                       */
/* Object Ref   :                                                       */
/* Date Created : 15 Apr 2004                                           */
/* Log          : GS                                                    */
/*----------------------------------------------------------------------*/
/* Date Updated :                                                       */
/* Log          :                                                       */
/* Reason       :                                                       */
/*----------------------------------------------------------------------*/
create procedure sp_grants_space
(
 @Name sysname = null
)
as
create table #temp
(
 name                           sysname null,
 rows                           int           null,
 reserved                       varchar(30)   null,
 data                           varchar(30)   null,
 index_size                     varchar(30)   null,
 unused                         varchar(30)   null
)

begin
  set nocount on
  
  declare @db_name sysname
  
  select @db_name = db_name()
  
  declare @command varchar(255)
  
  if @db_name = 'master'
  begin
    raiserror 900000 'Your current sessions database is master, please change to the required database.'
    return
  end
  
  declare drop_index_cursor cursor for
		select 'exec sp_spaceused ' + name
		  from sysobjects
		 where type = 'U'
		  and name like isnull(@Name, name)
                order by name
  
		open drop_index_cursor
		
		fetch drop_index_cursor into @command
		
		while (@@fetch_status = 0)
		begin
                  insert #temp
				execute (@command)
				fetch drop_index_cursor into @command
		end
		
  close drop_index_cursor
  deallocate drop_index_cursor
  
  select @db_name,
         @db_name as 'name',
         sum(rows) as 'rows',
         sum(convert(numeric(13,3), replace(reserved, 'kb', ''))) 'reserved',
         sum(convert(numeric(13,3), replace(data, 'kb', ''))) 'data',
         sum(convert(numeric(13,3), replace(index_size, 'kb', ''))) 'index_size',
         sum(convert(numeric(13,3), replace(unused, 'kb', ''))) 'unused',
         '' 'drop',
         '' 'delete',
         '' 'truncate'
    from #temp
  union
  select @db_name,
         name,
         rows,
         convert(numeric(13,3), replace(reserved, 'kb', '')) 'reserved',
         convert(numeric(13,3), replace(data, 'kb', '')) 'data',
         convert(numeric(13,3), replace(index_size, 'kb', '')) 'index_size',
         convert(numeric(13,3), replace(unused, 'kb', '')) 'unused',
         'drop table ' + name 'drop',
         'delete ' + name 'delete',
         'truncate table ' + name 'truncate'
    from #temp
  order by 'data' desc
end
