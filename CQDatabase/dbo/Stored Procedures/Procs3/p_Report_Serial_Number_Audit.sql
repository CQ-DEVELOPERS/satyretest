﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Serial_Number_Audit
  ///   Filename       : p_Report_Serial_Number_Audit.sql
  ///   Create By      : Karen
  ///   Date Created   : Jan 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Serial_Number_Audit
(
 @WarehouseId       int,
 @StorageUnitId     int = null,
 @BatchId           int = null,
 @PrincipalId       int = null
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	   ProductCode        nvarchar(30),
	   Product            nvarchar(255),
	   StorageUnitId	  int,
	   Batch              nvarchar(30),
	   BatchId			  int,
	   ReceiptId		  int,
	   POOrderNumber	  nvarchar(30),
	   SOOrderNumber	  nvarchar(30),
	   IssueId			  int,
	   ReceiptQty         numeric(13,6),
	   IssueQty		      numeric(13,6),
	   SOH		          numeric(13,6),
	   PrincipalId		  int,
	   PrincipalCode	  nvarchar(50),
	   SerialNumber		  nvarchar(50)	      
	 )
	 
	 declare @Matrix as Table
	  (
	   ProductCode			nvarchar(30),
	   Product				nvarchar(255),
	   Batch				nvarchar(30),
	   PrincipalCode		nvarchar(50), 
	   POOrderNumber		nvarchar(30),
	   SOOrderNumber		nvarchar(30),  
	   SerialNumber			nvarchar(50),
	   ReceiptQty	        numeric(13,6),
	   IssueQty		        numeric(13,6),
	   SOH					numeric(13,6),
	   Sequence				int,
	   SetNumber			int
	  );
  
	declare @InboundSequence	int,
			@ShowWeight			bit = null,
			@StoreLocation		varchar(15),
			@PickLocation		varchar(15)			

 
  if @StorageUnitId = -1 set @StorageUnitId = null
  if @BatchId = -1 set @BatchId = null
  if @PrincipalId = -1 set @PrincipalId = null  
  
  insert @TableResult
        (ProductCode,
		 Product,
		 StorageUnitId,
		 Batch,
		 BatchId,
		 ReceiptId,
		 IssueId,
		 PrincipalId,
		 SerialNumber,
		 ReceiptQty,
		 IssueQty)
  select distinct  p.ProductCode,
         p.Product,  
         su.StorageUnitId,       
         b.Batch,
         b.BatchId,
         sn.ReceiptId,
         sn.IssueId,
         p.PrincipalId,
         sn.SerialNumber,
         0,
         0
   from SerialNumber			sn 
   join batch b on b.BatchId		       = sn.BatchId
   join StorageUnit su on su.StorageUnitId = sn.StorageUnitId
   join Product p on su.ProductId = p.ProductId
   where sn.StorageUnitId   = isnull(@StorageUnitId, sn.StorageUnitId)
     and b.BatchId          = isnull(@BatchId, b.BatchId)
     and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
   
    
  update tr
     set PrincipalCode = p.PrincipalCode
    from @TableResult tr
    join Principal        p (nolock) on tr.PrincipalId = p.PrincipalId
  
  update tr
     set POOrderNumber = id.OrderNumber
    from @TableResult    tr
    join Receipt          r (nolock) on tr.ReceiptId = r.ReceiptId
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId 
  
  update tr
     set SOOrderNumber = od.OrderNumber
    from @TableResult     tr
    join Issue             i (nolock) on tr.IssueId = i.IssueId
    join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     
  update tr
     set ReceiptQty = (select COUNT(sn.SerialNumber) from SerialNumber  sn
							where tr.BatchId = sn.BatchId
							  and tr.StorageUnitId = sn.StorageUnitId
							  and tr.ReceiptId = sn.ReceiptId
						      and sn.ReceiptId is not null
							  and sn.IssueId is null )
    from @TableResult tr
    
  update tr
     set IssueQty = (select COUNT(sn.SerialNumber) from SerialNumber  sn
							where tr.BatchId = sn.BatchId
							  and tr.StorageUnitId = sn.StorageUnitId
							  and tr.IssueId = sn.IssueId
						      and sn.ReceiptId is null
							  and sn.IssueId is not null )
    from @TableResult tr
    
   
  insert @Matrix
        (ProductCode,
         Product,
         PrincipalCode,
         Batch,
         POOrderNumber,
         SOOrderNumber,
         ReceiptQty,
         IssueQty,
         SOH,
         SerialNumber,
         sequence,
         SetNumber)
  select distinct 
         td.ProductCode,
         td.Product,
         td.PrincipalCode,
         td.Batch,
         td.POOrderNumber,
         td.SOOrderNumber,
         td.ReceiptQty,
         td.IssueQty,
         td.SOH,
         td.SerialNumber,
         ROW_NUMBER() OVER (partition by ProductCode,Batch,POOrderNumber,SOOrderNumber  
							ORDER BY ProductCode,Batch,POOrderNumber,SOOrderNumber ) Rank,
         1
    from @TableResult td 
   where ProductCode is not null
     and SerialNumber is not null   
    
     
    while exists (select sequence from @Matrix
		where Sequence > 4)
	begin
		update mx
		   set Sequence = (Sequence - 4),
			   SetNumber = SetNumber + 1
	      from @Matrix mx
	     where Sequence > 4
	end
    
	select * from @Matrix
	
end
 
