﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_SKU_By_Job_test
  ///   Filename       : p_Report_SKU_By_Job_test.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_SKU_By_Job_test
(
 @JobId int
)

as
begin
	 set nocount on;
	 
	 
	declare @Matrix as Table
		(
		JobId			int,
		SKUCode				nvarchar(50),
		ConfirmedQuantity		float,
		CheckQuantity float,
		Sequence			int,
		SetNumber			int
		);
	 
	 
	 insert @Matrix
        (JobId,
         SKUCode,
         ConfirmedQuantity,
         CheckQuantity,
         sequence,
         SetNumber)
  select i.JobId, 
			sku.SKUCode, 
			sum(isnull(ili.ConfirmedQuantity,0)) as 'ConfirmedQuantity', 
			sc.ConfirmedQuantity as 'CheckQuantity',
         ROW_NUMBER() OVER (partition by i.JobId ORDER BY i.JobId) Rank,
         1
	   from IssueLineInstruction ili (nolock)
	   join Instruction        i (nolock) on ili.InstructionId    = i.InstructionId
	   join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
	   join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
	   join SKU              sku (nolock) on su.SKUId             = sku.SKUId
	   left
	   join SKUCheck          sc (nolock) on i.JobId = sc.JobId
	                                     and sku.SKUCode = sc.SKUCode
	  where i.JobId = @JobId
	 group by i.JobId, sku.SKUCode, sc.ConfirmedQuantity
	 
	 while exists (select sequence from @Matrix
		where Sequence > 15)
	begin
		update mx
		   set Sequence = (Sequence - 15),
			   SetNumber = SetNumber + 1
	      from @Matrix mx
	     where Sequence > 15
	end
    
	select * from @Matrix
	
	
end
