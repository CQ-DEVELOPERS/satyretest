﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_Movement_Summary_dropme
  ///   Filename       : p_Report_Product_Movement_Summary_dropme.sql
  ///   Create By      : Karen
  ///   Date Created   : 14 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Product_Movement_Summary_dropme
(
 @FromDate          datetime,
 @ToDate            datetime,
 @ProductId 		int,
 @WarehouseId		int,
 @PrincipalId		int,
 @BatchId			int
)

as
begin
	 set nocount on;
	 
	 declare @Weeks int,
	         @Count int,
	         @Date  datetime
	 

  
  declare @TableResult as table
  (
   Week				smallint,
   StartDate		datetime,
   EndDate			datetime,
   MovementDate		datetime,
   ProductCode		varchar(30),
   Opening			int,
   Received			int,
   Issued			int,
   Balance			int,
   Variance			int,
   Percentage		int,
   StockOnHand		int,
   WarehouseId		int,
   StorageUnitId	int,
   SKUId			int
  )
  
  declare @TableResult2 as table
  (
   Week				smallint,
   StartDate		datetime,
   EndDate			datetime,
   MovementDate		datetime,
   ProductCode		varchar(30),
   Product			varchar(50),
   Opening			int,
   Received			int,
   Issued			int,
   Balance			int,
   Variance			int,
   Percentage		int,
   StockOnHand		int,
   WarehouseId		int,
   StorageUnitId	int,
   SKU  			varchar(50),
   Weeks			smallint,
   PrincipalCode	nvarchar(50)
  )
  
  if @ProductId = -1 
	set @ProductId = null
	
  if @PrincipalId = -1
	set @PrincipalId = null
	
  declare @ProductCode nvarchar(50)
  
  select @ProductCode = p.ProductCode
  from Product p 
  where p.ProductId = @ProductId
 
  -- how many weeks between from and to date
  
  set @Count = 0
  select @Weeks = round((datediff(dd, @FromDate, @ToDate) / 7.000) + .49, 0)
  
  
  if @Weeks = 0
    set @Weeks = 1
  
  --while @Count < @Weeks
  begin
    select @Date = dateadd(wk, @Count, @FromDate)

    select @Count = @Count + 1
    
    insert @TableResult
          (Week,
           StartDate,
           EndDate,
           MovementDate,
           ProductCode,
           WarehouseId,
           StorageUnitId,
           SKUId)
    select @Count,
           @FromDate,
           @ToDate,
           MovementDate,
           ProductCode,
           WarehouseId,
           StorageUnitId,
           SKUId
      from ProductMovement pm
     where pm.MovementDate between @FromDate and @ToDate
     and   pm.ProductCode = isnull(@ProductCode,pm.ProductCode)
     and   pm.WarehouseId = @WarehouseId
     and   (pm.ReceivedQuantity > 0 
     or   pm.IssuedQuantity > 0)

  end

 --update tr
 --    set Opening = (select min(OpeningBalance) 
	--				  from ProductMovement pm (nolock) 
	--				 where pm.WarehouseId = @WarehouseId 
	--				   and pm.productcode = tr.productcode 
	--				   and pm.MovementDate  between tr.StartDate and tr.EndDate)
 --   from @TableResult tr  
    
 update tr
     set Opening = (select top 1 OpeningBalance
					  from ProductMovement pm (nolock) 
					 where pm.WarehouseId = @WarehouseId 
					   and pm.productcode = tr.productcode 
					   and pm.MovementDate  between tr.StartDate and tr.EndDate
					 order by WarehouseId, ProductCode, MovementDate)
    from @TableResult tr  
    
   
	update tr
		 set Received = (select sum(isnull(ReceivedQuantity,0)) 
						   from ProductMovement pm 
						  where pm.WarehouseId = @WarehouseId 
						    and pm.productcode = tr.productcode 
						    and pm.MovementDate = tr.MovementDate)  
	from @TableResult tr
	--where tr.MovementDate between @FromDate and @ToDate
	where	  tr.ProductCode = isnull(@ProductCode, tr.ProductCode)
	and	  tr.WarehouseId = @WarehouseId


  
  update tr
     set Issued = (select sum(isnull(IssuedQuantity,0)) 
				     from ProductMovement pm 
				    where pm.WarehouseId = @WarehouseId 
				      and pm.productcode = tr.productcode 
				      and pm.MovementDate = tr.MovementDate) 
    from @TableResult tr
    --where tr.MovementDate between @FromDate and @ToDate
    where	  tr.ProductCode = isnull(@ProductCode, tr.ProductCode)
    and	  tr.WarehouseId = @WarehouseId
 

insert @TableResult2
          (Week,
          StartDate,
          EndDate,
          ProductCode,
          Product,
		  Opening,
		  Received,
		  Issued,
		  WarehouseId,
		  SKU)
	select tr.Week,
          tr.StartDate,
          tr.EndDate,
          tr.ProductCode,
          p.Product,
		  min(tr.Opening),
		  sum(isnull(tr.Received,0))as Received,
		  sum(isnull(tr.Issued,0)) as Issued,
		  tr.WarehouseId,
		  s.SKU
    from @TableResult tr
    join Product p on tr.ProductCode = p.ProductCode
    left join Sku     s on tr.SKUId = s.SKUId
    group by Week,
          tr.StartDate,
          tr.EndDate,
          tr.ProductCode,
          p.Product,
		  tr.WarehouseId,
		  p.Product,
		  s.SKU
 
 --select * from @TableResult2 order by ProductCode
       
 update tr
     set Balance = ((isnull(Opening,0) + isnull(Received,0)) - isnull(Issued,0)) 
    from @TableResult2 tr
    
 update tr
     set Variance = (isnull(Balance,0) - isnull(Issued,0)) 
    from @TableResult2 tr
    
  
SET ARITHABORT OFF
SET ANSI_WARNINGS OFF   

 update tr
     set Percentage = isnull((isnull(Variance,0) / isnull(Issued,0)*100),0)
    from @TableResult2 tr
    
  update tr
     set StockOnHand = isnull(((isnull(Received,0) + isnull(Opening,0)) / isnull(Issued,0)*100),0)
    from @TableResult2 tr   
    
  update tr
     set StockOnHand = 100
    from @TableResult2 tr 
    where isnull(tr.Received,0) = 0 and isnull(tr.Issued,0) = 0
    
  update tr
     set PrincipalCode = pri.PrincipalCode
    from @TableResult2 tr 
    join StorageUnit	su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product        pr (nolock) on su.ProductId = pr.ProductId
    join Principal     pri (nolock) on pr.PrincipalId = pri.PrincipalId

	select tr.Week,
          tr.StartDate,
          tr.EndDate,
		  isnull(tr.Opening,0) as Opening,
		  isnull(tr.Received,0) as Received,
		  isnull(tr.Issued,0) as Issued,
		  isnull(tr.Balance,0) as Balance,
		  isnull(tr.Variance,0) as Variance,
		  isnull(tr.Percentage,0) as Percentage,
		  isnull(tr.StockOnHand,0) as StockOnHand,
		  tr.WarehouseId,
		  tr.Product,
		  tr.ProductCode,
		  tr.SKU,
		  tr.weeks,
		  tr.PrincipalCode
    from @TableResult2 tr
    order by tr.ProductCode,
			 tr.week

end
 
