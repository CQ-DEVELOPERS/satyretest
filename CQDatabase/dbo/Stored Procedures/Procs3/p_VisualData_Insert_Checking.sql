﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VisualData_Insert_Checking
  ///   Filename       : p_VisualData_Insert_Checking.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VisualData_Insert_Checking

as
begin
	 set nocount on;
	 
	 declare @Location         nvarchar(15),
          @Latitude         decimal(16,13),
          @Longitude        decimal(16,13),
          @LatitudeAdd      decimal(16,13),
          @LongitudeAdd     decimal(16,13),
          @CurrentLatitude  decimal(16,13),
          @CurrentLongitude decimal(16,13),
          @Direction        nvarchar(10),
          @Height           int,
          @Length           int,
          @HeightCount      int,
          @LengthCount      int,
          @PreviousLocation nvarchar(15),
          @Rowcount         int,
          @Ident            int
  
  declare @TableResult as table
  (
   Ident              int primary key identity,
   LocationId         int,
   Location           nvarchar(50),
   OutboundShipmentId int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(50),
   IssueId            int,
   JobId              int,
   Pallet             nvarchar(30),
   InstructionType    nvarchar(50),
   StatusCode         nvarchar(10),
   Scanned            datetime,
   DespatchDate       datetime,
   Latitude           decimal(16,13),
   Longitude          decimal(16,13),
   Height             int,
   [Length]           int,
   Updated            bit default 0,
   Row                int,
   Colour             int
  )
  
  declare @TableJobs as table
  (
   JobId      int,
   StatusCode nvarchar(10),
   Scanned            datetime
  )
  
  -- Get jobs first - better for performance
  insert @TableJobs
        (JobId,
         StatusCode,
         Scanned)
  select j.JobId,
         s.StatusCode,
         case when s.StatusCode in ('CK','CD')
         then op.Despatch
         else isnull(op.Checked, op.Checking)
         end
    from Job                  j (nolock)
    join Status               s (nolock) on j.StatusId = s.StatusId
    join OutboundPerformance op (nolock) on j.JobId = op.JobId
   where s.StatusCode in ('CK','CD','D','DC')
     and op.Despatch > dateadd(dd, -10, getdate())
  
  -- Get the rest of the details
  insert @TableResult
        (LocationId,
         JobId,
         InstructionType,
         StatusCode,
         Scanned,
         OutboundShipmentId,
         OutboundDocumentId,
         IssueId,
         Colour)
  select distinct
         i.StoreLocationId,
         j.JobId,
         it.InstructionType,
         j.StatusCode,
         j.Scanned,
         ili.OutboundShipmentId,
         ili.OutboundDocumentId,
         ili.IssueId,
         case when datediff(hh, j.Scanned, getdate()) <= 40
         then datediff(hh, j.Scanned, getdate())
         else 40
         end
    from Instruction          i (nolock)
    join InstructionType     it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join @TableJobs           j          on i.JobId = j.JobId
    join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
                                           or i.InstructionRefId = ili.InstructionId
  
  update @TableResult
     set Colour = 1
    where Colour = 0
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set DespatchDate = i.DeliveryDate,
         LocationId   = isnull(i.DespatchBay, tr.LocationId)
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
  
  update tr
     set LocationId = os.DespatchBay
    from @TableResult tr 
    join OutboundShipment os on tr.OutboundShipmentId = os.OutboundShipmentId
   where os.DespatchBay is not null
  
  declare visual_cursor cursor for
   select tr.Ident,
          vl.Location,
          vl.Latitude,
          vl.Longitude,
          vl.Direction,
          vl.Height,
          vl.[Length]
  	  from	@TableResult tr
  	  join viewLocation vl on tr.LocationId = vl.LocationId
--     where vl.Location != 'DESPATCH'
--  	   and vl.Location = 'COLL'
	  order by	vl.Location, tr.Scanned
  
  open visual_cursor
  
  set @HeightCount = 1
  set @LengthCount = 1
  set @PreviousLocation = '-1'
  
  fetch visual_cursor into @Ident,
                           @Location,
                           @Latitude,
                           @Longitude,
                           @Direction,
                           @Height,
                           @Length
  
  while (@@fetch_status = 0)
  begin
    if @Location != isnull(@PreviousLocation, @Location)
    begin
      set @PreviousLocation = @Location
      set @CurrentLatitude  = @Latitude
      set @CurrentLongitude = @Longitude
      set @Rowcount = 0
      set @HeightCount = 1
      set @LengthCount = 1
    end
    
    if @Direction = 'North'
    begin
      set @LatitudeAdd = 0.00003
      set @LongitudeAdd = 0
    end
    if @Direction = 'East'
    begin
      set @LatitudeAdd = 0
      set @LongitudeAdd = 0.00003
    end
    if @Direction = 'South'
    begin
      set @LatitudeAdd = -0.00003
      set @LongitudeAdd = 0
    end
    if @Direction = 'West'
    begin
      set @LatitudeAdd = 0
      set @LongitudeAdd = -0.00003
    end
    
    update @TableResult
       set Latitude  = @CurrentLatitude,
           Longitude = @CurrentLongitude,
           Updated   = 1,
           Row       = isnull(@Rowcount, 0),
           [Length]  = @LengthCount,
           Height    = @HeightCount,
           Location  = @Location
     where Ident = @Ident
    
    if @HeightCount < @Height
    begin
      set @HeightCount = @HeightCount + 1
    end
    else if @LengthCount < @Length
    begin
      set @HeightCount = 1
      set @LengthCount = @LengthCount + 1
      select @CurrentLatitude  = @CurrentLatitude + @LatitudeAdd
      select @CurrentLongitude = @CurrentLongitude + @LongitudeAdd
    end
    else
    begin
      set @Rowcount = isnull(@Rowcount,0) + 1
      select @CurrentLatitude  = @Latitude + 0.00003 * @Rowcount
      select @CurrentLongitude = @Longitude
      set @HeightCount = 1
      set @LengthCount = 1
    end
    
    fetch visual_cursor into @Ident,
                             @Location,
                             @Latitude,
                             @Longitude,
                             @Direction,
                             @Height,
                             @Length
  end

  close visual_cursor
  deallocate visual_cursor
  
  delete VisualData where Key1 = 'Checking'
  
  update @TableResult
     set Location = convert(nvarchar(10), ExternalCompany)
    where Location in ('COLL','DESPATCH')
  
  insert VisualData
        (Key1,
         Sort1,
         Sort2,
         Sort3,
         Colour,
         Latitude,
         Longitude,
         Info)
  select 'Checking',
         Row + 1,
         Location,
         Height,
         Colour,
         Latitude,
         Longitude,
         '<h3>' + ExternalCompany + '</h3>' +
         '____________________________<br>' + 
         --'<b>Job: </b>' + '<a href="../Reports/JobCheckSheet.aspx?JobId=' + convert(nvarchar(10), JobId) + '" target="_blank">' + convert(nvarchar(10), JobId) + '</a>' + 
         '<b>Job: </b>' + '<a href="../Reports/JobCheckSheet.aspx?JobId=77859" target="_blank">77859</a>' + 
         '<br>' + 
         '<br>' + 
         '<b>Pallet: </b>1 of 3' +
         '<br>' + 
         '<br>' + 
         '<b>Pallet Type: </b>' + InstructionType +
         '<br>' + 
         '<br>' + 
         '<b>Scanned Date: </b>' +
         convert(nvarchar(20), Scanned, 120) +
         '<br>' + 
         '<br>' +
         '<b>Despatch Date: </b>' +
         convert(nvarchar(20), DespatchDate, 120)
    from @TableResult
  where Row is not null
end
