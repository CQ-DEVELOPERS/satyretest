﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_ScheduledAndLateDeliveries
  ///   Filename       : p_Report_ScheduledAndLateDeliveries.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_ScheduledAndLateDeliveries
(
 --@WarehouseId       int,
 @FromDate          datetime,
 @ToDate            datetime,
 @OrderNumber		nvarchar(30),
 @StorageUnitId		int,
 @PrincipalId		int,
 @Selection		    nvarchar(2) -- LD=Late delivered SC=Scheduled LN=Late Not delivered DS=delivered on schedule A=All
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   DeliveryDate			datetime,
   ExternalCompanyId	int,
   ExternalCompany		nvarchar(50),
   LocationId			int,
   Location				nvarchar(15),
   StorageUnitBatchId	int,
   StorageUnitId		int,
   ProductCode			nvarchar(30),
   Product				nvarchar(50),
   SKUCode				nvarchar(50),
   Batch				nvarchar(50),
   RequiredQuantity		float,
   InboundShipmentId	int,
   ReceiptId			int,
   ReceiptLineId		int,
   OrderNumber			nvarchar(30),
   BOE					nvarchar(255),
   ShippingAgentId		int,
   ShippingAgent		nvarchar(255),
   NumberOfLines		int,
   NumberOfPieces		int,
   Weight				int,
   DeliveryStatus	    nvarchar(50),
   DeliveryColour       nvarchar(20),
   DeliverySelect		nvarchar(2),
   PlannedDeliveryDate	date,
   ActualDeliveryDate	date,
   StatusId				int,
   Status				nvarchar(50),
   ReceivingBayId		int,
   ReceivingBay			nvarchar(15),
   DIMSAvailable		nvarchar(5),
   PickBinAvailable	    nvarchar(5),
   ZonePrincipalLink	nvarchar(5)				
  )
  
  if @OrderNumber = '-1'
    set @OrderNumber = null
  
  if @StorageUnitId = -1
    set @StorageUnitId = null
    
  if @PrincipalId = -1
    set @PrincipalId = null
  
  insert @TableResult
        (DeliveryDate,
         ExternalCompanyId,
         LocationId,
         StorageUnitBatchId,
         RequiredQuantity,
		 OrderNumber,
		 BOE,
		 ShippingAgentId,
		 StatusId,
		 NumberOfLines,
		 ReceiptId,
		 ReceiptLineId,
		 NumberOfPieces,
		 DIMSAvailable,
		 ZonePrincipalLink,
		 DeliveryStatus,
		 PlannedDeliveryDate,
		 ActualDeliveryDate)
  select isnull(r.DeliveryDate, id.DeliveryDate),
         id.ExternalCompanyId,
         r.LocationId,
         rl.StorageUnitBatchId,
         rl.RequiredQuantity,
         id.OrderNumber,
         r.BOE,
         r.ShippingAgentId,
         r.StatusId,
         r.NumberOfLines,
         r.ReceiptId,
         rl.ReceiptLineId,
         rl.RequiredQuantity,
         'No',
         'No',
         ' ',
         id.DeliveryDate,
         r.DeliveryDate
    from InboundDocument   id (nolock)
    join Receipt            r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join ReceiptLine       rl (nolock) on r.ReceiptId          = rl.ReceiptId
    join StorageUnitBatch sub (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
   where id.OrderNumber = isnull(@OrderNumber, id.OrderNumber)
     and sub.StorageUnitId = isnull(@StorageUnitId, sub.StorageUnitId)
     and id.PrincipalId = ISNULL(@PrincipalId,id.PrincipalId)
     and isnull(r.DeliveryDate, id.DeliveryDate) between @FromDate and @ToDate
  
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         StorageUnitId = su.StorageUnitId
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit          su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product               p (nolock) on su.ProductId          = p.ProductId
    join SKU                 sku (nolock) on su.SKUId              = sku.SKUId
    join Batch                 b (nolock) on sub.BatchId           = b.BatchId
  
  
  update tr
     set InboundShipmentId = isr.InboundShipmentId
    from @TableResult tr
    join InboundShipmentReceipt isr (nolock) on isr.ReceiptId = tr.ReceiptId
    
  update tr
     set tr.ReceivingBayId = i.StoreLocationId,
		 tr.ReceivingBay   = l.Location
    from @TableResult tr
    join Instruction i  (nolock) on i.ReceiptLineId = tr.ReceiptLineId
    join Location l (nolock) on l.LocationId = i.StoreLocationId
    
  update tr
     set ShippingAgent = ec.ExternalCompany
    from @TableResult tr
    join ExternalCompany  ec (nolock) on ec.ExternalCompanyId = tr.ShippingAgentId
    
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status s  (nolock) on tr.StatusId = s.StatusId    
    
  update tr
     set DIMSAvailable = 'Yes'
    from @TableResult tr
    join Pack p  (nolock) on tr.StorageUnitId = p.StorageUnitId
   where p.Height > 0
     and p.Length > 0
     and p.Width > 0
   
  update tr
     set ZonePrincipalLink = 'Yes'
    from @TableResult tr
    join StorageUnit su  (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product p (nolock) on su.ProductId = p.ProductId
   where p.PrincipalId is not null
    
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
    
  update tr
     set DeliveryStatus = 'Late - not delivered',
		 DeliveryColour = 'RED',
		 DeliverySelect = 'LN'
    from @TableResult    tr
   where PlannedDeliveryDate < GETDATE()
     and StatusId = dbo.ufn_StatusId('R','W')
    
  update tr
     set DeliveryStatus = 'Scheduled',
		 DeliveryColour = 'GREEN',
		 DeliverySelect = 'SC'
    from @TableResult    tr
   where PlannedDeliveryDate >= GETDATE()
     and StatusId = dbo.ufn_StatusId('R','W')
     
  update tr
     set DeliveryStatus = 'Late - delivered',
		 DeliveryColour = 'RED',
		 DeliverySelect = 'LD'
    from @TableResult    tr
   where PlannedDeliveryDate < ActualDeliveryDate
     and StatusId not in (dbo.ufn_StatusId('R','W'),dbo.ufn_StatusId('R','OH'),dbo.ufn_StatusId('R','RE'))
     
  update tr
     set DeliveryStatus = 'Delivered - on schedule',
		 DeliveryColour = 'GREEN',
		 DeliverySelect = 'DS'
    from @TableResult    tr
   where PlannedDeliveryDate >= ActualDeliveryDate
     and StatusId not in (dbo.ufn_StatusId('R','W'),dbo.ufn_StatusId('R','OH'),dbo.ufn_StatusId('R','RE'))
  
  if @Selection = 'A'
  select *
		 --DeliveryDate,
   --      ExternalCompany,
   --      Location,
   --      ProductCode,
   --      Product,
   --      SKUCode,
   --      Batch,
   --      RequiredQuantity
    from @TableResult
  else
  select *
		 --DeliveryDate,
   --      ExternalCompany,
   --      Location,
   --      ProductCode,
   --      Product,
   --      SKUCode,
   --      Batch,
   --      RequiredQuantity
    from @TableResult
    where DeliverySelect = @Selection
    
end
