﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Temporary_Pickface_Create
  ///   Filename       : p_Temporary_Pickface_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Temporary_Pickface_Create
(
 @WarehouseId        int,
 @ExternalCompanyId  int,
 @ShelfLife          numeric(13,3),
 @MinimumShelfLife   numeric(13,3),
 @StorageUnitId      int,
 @Quantity           float,
 @FullPallet         int,
 @StorageUnitBatchId int output,
 @AreaType           nvarchar(10) = '',
 @DeliveryDate       datetime = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @LocationId        int,
          @ActualQuantity    float,
          @StoreLocationId   int,
          @InstructionTypeId int,
          @JobId             int,
          @InstructionId     int,
          @StatusId          int,
          @PriorityId        int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @DeliveryDate is null
    set @DeliveryDate = @GetDate
  
  select top 1
         @StoreLocationId = l.LocationId
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.Locationid = al.Locationid
    join Area          a (nolock) on al.AreaId = a.AreaId
   where a.WarehouseId = @WarehouseId
     and a.AreaCode = 'TP'
     and l.Location in (select Location + 'P'
                          from StorageUnitLocation sul (nolock)
                          join Location              l (nolock) on sul.LocationId = l.LocationId
                         where StorageUnitId = @StorageUnitId)
  
  if @@ROWCOUNT = 0
    select top 1
           @StoreLocationId = l.LocationId
      from Location      l (nolock)
      join AreaLocation al (nolock) on l.Locationid = al.Locationid
      join Area          a (nolock) on al.AreaId = a.AreaId
     where a.WarehouseId = @WarehouseId
       and a.AreaCode = 'TP'
  
  begin transaction
  
  if exists(select 1
              from Instruction        i (nolock)
              join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
              join Status             s (nolock) on i.StatusId           = s.StatusId
              join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
             where s.StatusCode in ('W','S')
               and it.InstructionTypeCode = 'R'                -- Replenishment
               and i.WarehouseId          = @WarehouseId       -- Same Warehouse
               and i.StoreLocationId      = @StoreLocationId   -- Same to Location
               and sub.StorageUnitId      = @StorageUnitId     -- Same product
               and it.InstructionTypeCode = 'R')               -- Replenishment
  begin
    set @LocationId = @StoreLocationId
    goto Result
  end
  
  -- Check if Tempoary Replenishment already exists
  select top 1 @LocationId         = subl.LocationId,
               @StorageUnitBatchId = subl.StorageUnitBatchId,
               @Quantity           = subl.ActualQuantity - subl.ReservedQuantity
    from Location                    l (nolock)
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
    join StorageUnitBatchLocation subl          on al.LocationId           = subl.LocationId
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Status                      s (nolock) on b.StatusId              = s.StatusId
   where l.ActivePicking       = 1 -- Yes
     and l.StocktakeInd        = 0 -- No
     and sub.StorageUnitId     = @StorageUnitId
     and subl.ActualQuantity + subl.AllocatedQuantity - subl.ReservedQuantity >= @Quantity
     and s.StatusCode          = 'A'
     and convert(numeric(13,3), datediff(dd, @DeliveryDate, dateadd(dd, @ShelfLife, b.CreateDate))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
     and a.AreaCode = 'TP'
  order by subl.ActualQuantity - subl.ReservedQuantity, sub.StorageUnitBatchId
  
  if @LocationId  is not null
    goto Result
  
  ---- Get part location first then full location near to NearLocationId
  --select top 1 @LocationId         = subl.LocationId,
  --             @StorageUnitBatchId = subl.StorageUnitBatchId,
  --             @Quantity           = subl.ActualQuantity - subl.ReservedQuantity
  --  from Location                    l (nolock)
  --  join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
  --  join StorageUnitBatchLocation subl          on al.LocationId           = subl.LocationId
  --  join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
  --  join StorageUnitArea           sua (nolock) on sub.StorageUnitId       = sua.StorageUnitId
  --                                             and al.AreaId                = sua.AreaId
  --  join Batch                       b (nolock) on sub.BatchId             = b.BatchId
  --  join Status                      s (nolock) on b.StatusId              = s.StatusId
  -- where l.ActivePicking       = 1 -- Yes
  --   and l.StocktakeInd        = 0 -- No
  --   and sub.StorageUnitId     = @StorageUnitId
  --   and subl.ActualQuantity   - subl.ReservedQuantity >= @Quantity
  --   and s.StatusCode          = 'A'
  --   and convert(numeric(13,3), datediff(dd, @DeliveryDate, dateadd(dd, @ShelfLife, b.CreateDate))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
  --   and convert(numeric(13,3), datediff(dd, @DeliveryDate, dateadd(dd, @ShelfLife, b.CreateDate))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
  --order by subl.ActualQuantity - subl.ReservedQuantity, sub.StorageUnitBatchId
  
  select top 1 @LocationId         = subl.LocationId,
               @StorageUnitBatchId = subl.StorageUnitBatchId,
               @Quantity           = subl.ActualQuantity - subl.ReservedQuantity
    from Location                    l (nolock)
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
    join StorageUnitBatchLocation subl          on al.LocationId           = subl.LocationId
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Status                      s (nolock) on b.StatusId              = s.StatusId
    left
    join StorageUnitArea           sua (nolock) on sub.StorageUnitId       = sua.StorageUnitId
                                               and a.AreaId                = sua.AreaId
   where l.ActivePicking      = 1 -- Yes
     and l.StocktakeInd      <= case when a.AreaCode = 'BK'
                                     then 1 -- Yes
                                     else 0 -- No
                                     end
     and a.WarehouseId        = @WarehouseId
     and sub.StorageUnitId    = @StorageUnitId
     and subl.ActualQuantity  - subl.ReservedQuantity >= case when a.AreaCode = 'BK'
                                                              then case when dbo.ufn_Configuration(175, @WarehouseId) = 1
                                                                        then @FullPallet -- Full pallet
                                                                        else @Quantity
                                                                        end
                                                              else @Quantity   -- Quantity Requested
                                                              end
     and s.StatusCode         = 'A'
     and a.StockOnHand        = 1
     and a.AreaCode          in ('BK','RK','SP','OF')
     --and convert(numeric(13,3), datediff(dd, @DeliveryDate, dateadd(dd, @ShelfLife, b.CreateDate))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
     and (convert(float, datediff(dd, @DeliveryDate, ExpiryDate)) / convert(float, @ShelfLife)) * 100 >  @MinimumShelfLife -- Minimum Shelf life check (ExpiryDate)
     and isnull(a.AreaType,'') = @AreaType
  order by datediff(dd, '1900-01-01', isnull(convert(nvarchar(10), b.ExpiryDate, 120), convert(nvarchar(10), b.CreateDate, 120))),
           b.Batch,
           sua.PickOrder,
           subl.ActualQuantity - subl.ReservedQuantity,
           sub.StorageUnitBatchId
  
  if @LocationId is not null
  begin
    select @ActualQuantity = ActualQuantity
      from StorageUnitBatchLocation
     where StorageUnitBatchId = @StorageUnitBatchId
       and LocationId         = @LocationId
    
    if @ActualQuantity > @FullPallet
      set @ActualQuantity = @FullPallet
    
    select @InstructionTypeId = InstructionTypeId,
           @PriorityId        = PriorityId
      from InstructionType (nolock)
     where InstructionTypeCode = 'R'
    
    if @ActualQuantity < 0 or @ActualQuantity is null
    begin
      set @Error = 2
      goto Result
    end
    
    set @StatusId = dbo.ufn_StatusId('IS','RL')
    
    exec @Error = p_Job_Insert
     @JobId       = @JobId output,
     @PriorityId  = @PriorityId,
     @OperatorId  = null,
     @StatusId    = @StatusId,
     @WarehouseId = @WarehouseId
    
    if @Error <> 0 or @JobId is null
    begin
      set @InstructionId = -1
      goto Result
    end
    
    set @StatusId = dbo.ufn_StatusId('I','W')
    
    exec @Error = p_Instruction_Insert
     @InstructionId       = @InstructionId output,
     @InstructionTypeId   = @InstructionTypeId,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @WarehouseId         = @WarehouseId,
     @StatusId            = @StatusId,
     @JobId               = @JobId,
     @OperatorId          = null,
     @PickLocationId      = @LocationId,
     @StoreLocationId     = @StoreLocationId,
     @InstructionRefId    = null,
     @Quantity            = @ActualQuantity,
     @ConfirmedQuantity   = @ActualQuantity,
     @Weight              = null,
     @ConfirmedWeight     = 0,
     @CreateDate          = @GetDate
    
    if @Error <> 0 or @InstructionId is null
    begin
      set @InstructionId = -1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @InstructionId
    
    if @Error <> 0
      goto Result
  end
  
  Result:
    if @LocationId is null or @StoreLocationId is null
      set @StoreLocationId = -1
    
    if @Error <> 0 or @InstructionId = -1
      rollback transaction
    else
      commit transaction
    
    return @StoreLocationId
end
