﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_SerialNumber
  ///   Filename       : p_Report_SerialNumber.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Oct 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_SerialNumber
(
 @WarehouseId   int,
 @OutboundShipmentId int,
 @IssueId            int,
 @FromDate      datetime,
 @ToDate        datetime
)

as
begin
	 set nocount on;
  
  if @IssueId = -1
    set @IssueId = null
    
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  select sn.SerialNumber,
         sn.ReferenceNumber,
         p.ProductCode,
         p.Product,
         id.OrderNumber as 'PurchaseOrder',
         od.OrderNumber as 'SalesOrder',
         sn.ReceivedDate
    from SerialNumber     sn (nolock)
    join StorageUnit      su (nolock) on sn.StorageUnitId = su.StorageUnitId
    join Product           p (nolock) on su.ProductId     = p.ProductId
    left
    join Receipt           r (nolock) on sn.ReceiptId = r.ReceiptId
    left
    join InboundDocument  id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    left
    join Issue             i (nolock) on sn.IssueId = i.IssueId
    left
    join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
   where sn.ReceivedDate between @FromDate and @ToDate
     and sn.IssueId = ISNULL(@IssueId, sn.IssueId)

end
