﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_OperatorSpecific_Performance
  ///   Filename       : p_Report_OperatorSpecific_Performance.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 14 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_OperatorSpecific_Performance
(
	@StartDate				Datetime,
	@EndDate					DateTime,
	@OperatorGroupId		int,
	@OperatorId				int = null,
	@UOM                   nvarchar(15) = 'Units'
)

as
begin

if (@OperatorId = -1)
	Begin	
Set @OperatorId = Null
	End

	 set nocount on;

Declare @DateHoursDiff		int,
		@DateDaysDiff		int,
		@DateMonthsDiff		int

Select 
 @DateHoursDiff = DateDiff(hh,@StartDate,@EndDate)
,@DateDaysDiff = DateDiff(D,@StartDate,@EndDate)
,@DateMonthsDiff = DateDiff(M,@StartDate,@EndDate) 
	
Declare @TempTable As Table 
(
		OperatorGroupId				int,
		OperatorId					int,
		InstructionTypeId			int,
		EndDate						DateTime,		
		Units						float, 
		Weight						float, 
		Instructions				float, 
		Orders						float, 
		OrderLines					float,
		Jobs						int,
		ActiveTime					int, 
		DwellTime					int,
		SiteUnits					float, 
		SiteWeight					float, 
		SiteInstructions			float, 
		SiteOrders					float,         
		SiteOrderLines				float, 
		SiteJobs					float,
		IndustryUnits				float, 
		IndustryWeight				float, 
		IndustryInstructions		float,
		IndustryOrders				float,     
		IndustryOrderLines			float, 
		IndustryJobs				float,
		UOMValue					float,
		UOMSiteTarget				float,
		UOMIndustryTarget			float
		
)

Insert Into @TempTable 
(
OperatorGroupId,
OperatorId,
InstructionTypeId,			
EndDate,		
Units,		
Weight,		
Instructions,		
Orders,		
OrderLines,		
Jobs,		
ActiveTime,		
DwellTime,		
SiteUnits,		
SiteWeight,		
SiteInstructions,		
SiteOrders,		
SiteOrderLines,		
SiteJobs,		
IndustryUnits,		
IndustryWeight,		
IndustryInstructions,	
IndustryOrders,		
IndustryOrderLines,		
IndustryJobs
)

(
SELECT     
			O.OperatorGroupId,
			Op.OperatorId,
			Op.InstructionTypeId,						
			Op.EndDate,								
			Sum(Op.Units),							
			Sum(Op.Weight),							
			Sum(Op.Instructions),					
			Sum(Op.Orders),							
			Sum(Op.OrderLines),						
            Sum(Op.Jobs),							
			Sum(Op.ActiveTime),						
			Sum(Op.DwellTime),						
			 Sum(Distinct(GP.Units)),
			 Sum(Distinct(GP.Weight)),
			 Sum(Distinct(GP.Instructions)),
			 Sum(Distinct(GP.Orders)),
			 Sum(Distinct(GP.OrderLines)),
			 Sum(Distinct(GP.Jobs)),
			 Sum(Distinct(IP.Units)),
			 Sum(Distinct(IP.Weight)),
			 Sum(Distinct(IP.Instructions)),
			 Sum(Distinct(IP.Orders)),
			 Sum(Distinct(IP.OrderLines)),
			 Sum(Distinct(IP.Jobs))			
			
			
FROM        OperatorPerformance Op INNER JOIN
                      Operator O ON Op.OperatorId = O.OperatorId INNER JOIN
                      (Select * From GroupPerformance Where PerformanceType = 'S' And OperatorGroupId = @OperatorGroupId) AS GP ON O.OperatorGroupId = GP.OperatorGroupId 
					INNER JOIN
                      (Select * From GroupPerformance Where PerformanceType = 'I' And OperatorGroupId = @OperatorGroupId) AS IP ON O.OperatorGroupId = IP.OperatorGroupId		
Where convert(nvarchar,Op.EndDate,111) Between @StartDate And @EndDate
And O.OperatorGroupId = @OperatorGroupId
And OP.OperatorId = Isnull(@OperatorId,Op.OperatorId)
Group By EndDate,O.OperatorGroupId,Op.OperatorId,Op.InstructionTypeId
)


if @UOM = 'Units'
begin
   Update @TempTable Set UOMValue = Units,
						UOMSiteTarget = SiteUnits,
						UOMIndustryTarget = IndustryUnits
						
end
else 
	if @UOM = 'Weight'
	begin
	 Update @TempTable Set UOMValue = Weight,
							UOMSiteTarget = SiteWeight,
						UOMIndustryTarget = IndustryWeight
	end
else 
	if @UOM = 'Jobs'
	begin
	 Update @TempTable Set UOMValue = Jobs,
							UOMSiteTarget = SiteJobs,
						UOMIndustryTarget = IndustryJobs
	end
else 
	if @UOM = 'Instructions'
	begin
	 Update @TempTable Set UOMValue = Instructions,
							UOMSiteTarget = SiteInstructions,
						UOMIndustryTarget = IndustryInstructions
	end


if (@DateMonthsDiff < 3)
	Begin
		if (@DateDaysDiff < 8)		
			Begin
				if (@DateHoursDiff < 25)
					Begin
						--Select 'x < 24 Hours' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
 					
Select 
		OG.OperatorGroup,
		--T.OperatorId,
		O.Operator,
		IT.InstructionType, 
		T.OperatorGroupId,			
		T.InstructionTypeId,		
		Convert(nvarchar,T.EndDate,108) as EndDate, 
		Convert(nvarchar,T.EndDate,108) as ordscale, 					
		SUM((T.Units)) as Units,					
		SUM((T.Weight)) as Weight,					
		SUM((T.Instructions)) as Instructions,			
		SUM((T.Orders)) as Orders,					
		SUM(T.OrderLines) as OrderLines,				
		SUM((T.Jobs)) as Jobs,					
		SUM((T.ActiveTime)) as ActiveTime,				
		SUM((T.DwellTime)) as DwellTime ,				
		SUM(Distinct(T.SiteUnits)) as SiteUnits,				
		SUM(Distinct(T.SiteWeight)) as SiteWeight,				
		SUM(Distinct(T.SiteInstructions)) as SiteInstructions,		
		SUM(Distinct(T.SiteOrders)) as SiteOrders,				
		SUM(Distinct(T.SiteOrderLines)) as SiteOrderLines,			
		SUM(Distinct(T.SiteJobs)) as SiteJobs,				
		SUM(Distinct(T.IndustryUnits)) as IndustryUnits,			
		SUM(Distinct(T.IndustryWeight)) as IndustryWeight,			
		SUM(Distinct(T.IndustryInstructions)) as IndustryInstructions,	
		SUM(Distinct(T.IndustryOrders)) as IndustryOrders,			
		SUM(Distinct(T.IndustryOrderLines)) as IndustryOrderLines,		
		SUM(Distinct(T.IndustryJobs)) as IndustryJobs,			
		SUM(Distinct(T.UOMValue)) as UOMValue,
		SUM(Distinct(T.UOMSiteTarget)) as UOMSiteTarget,
		SUM(Distinct(T.UOMIndustryTarget)) as UOMIndustryTarget,
		Count(Distinct(T.OperatorId)) as ActiveOperators					
	From @TempTable T
		Left Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		Inner Join Operator O			On O.OperatorId				= T.OperatorId
	Group by T.OperatorGroupId,OG.OperatorGroup,O.Operator,Convert(nvarchar,EndDate,108),IT.InstructionType, T.InstructionTypeId --,Uom
	ORDER BY ordscale

						
						-- Select data by this Scale
					End
				Else
					Begin
					--Select '24 Hours < x < 1 week' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
Select 
		OG.OperatorGroup,
		--T.OperatorId,
		O.Operator,
		IT.InstructionType, 
		T.OperatorGroupId,			
		T.InstructionTypeId,		
		Convert(nvarchar,T.EndDate,101) as EndDate, 
		Convert(nvarchar,T.EndDate,101) as ordscale , 					
		SUM(T.Units) as Units,					
		SUM(T.Weight) as Weight,					
		SUM(T.Instructions) as Instructions,			
		SUM(T.Orders) as Orders,					
		SUM(T.OrderLines) as OrderLines,				
		SUM(T.Jobs) as Jobs,					
		SUM(T.ActiveTime) as ActiveTime,				
		SUM(T.DwellTime) as DwellTime,				
		8 * Convert(bigint,SUM(Distinct(T.SiteUnits))) as SiteUnits,				
		8 * Convert(bigint,SUM(Distinct(T.SiteWeight))) as SiteWeight,				
		8 * Convert(bigint,SUM(Distinct(T.SiteInstructions))) as SiteInstructions,		
		8 * Convert(bigint,SUM(Distinct(T.SiteOrders))) as SiteOrders,				
		8 * Convert(bigint,SUM(Distinct(T.SiteOrderLines))) as SiteOrderLines,			
		8 * Convert(bigint,SUM(Distinct(T.SiteJobs))) as SiteJobs,				
		8 * Convert(bigint,SUM(Distinct(T.IndustryUnits))) as IndustryUnits,			
		8 * Convert(bigint,SUM(Distinct(T.IndustryWeight))) as IndustryWeight,			
		8 * Convert(bigint,SUM(Distinct(T.IndustryInstructions))) as IndustryInstructions,	
		8 * Convert(bigint,SUM(Distinct(T.IndustryOrders))) as IndustryOrders,			
		8 * Convert(bigint,SUM(Distinct(T.IndustryOrderLines))) as IndustryOrderLines,		
		8 * Convert(bigint,SUM(Distinct(T.IndustryJobs))) as IndustryJobs,			
		8 * Convert(bigint,SUM(Distinct(T.UOMValue))) as UOMValue,
		8 * Convert(bigint,SUM(Distinct(T.UOMSiteTarget))) as UOMSiteTarget,
		8 * Convert(bigint,SUM(Distinct(T.UOMIndustryTarget))) as UOMIndustryTarget,
		Count(Distinct(T.OperatorId)) as ActiveOperators			
	From @TempTable T
	Left Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		Inner Join Operator O			On O.OperatorId				= T.OperatorId
							Group by T.OperatorGroupId,OG.OperatorGroup,O.Operator,Convert(nvarchar,EndDate,101),IT.InstructionType, T.InstructionTypeId
							ORDER BY ordscale						
						-- Select data by this Scale
					End
			End
			Else
				Begin
					--Select '1 Week< x < 3 months' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
Select 
		OG.OperatorGroup,
		--T.OperatorId,
		O.Operator,
		IT.InstructionType, 
		T.OperatorGroupId,			
		T.InstructionTypeId,		
		DatePart(wk,T.EndDate) as EndDate, 
		DatePart(wk,T.EndDate) as ordscale , 					
		SUM(T.Units) as Units,					
		SUM(T.Weight) as Weight,					
		SUM(T.Instructions) as Instructions,			
		SUM(T.Orders) as Orders,					
		SUM(T.OrderLines) as OrderLines,				
		SUM(T.Jobs) as Jobs,					
		SUM(T.ActiveTime) as ActiveTime,				
		SUM(T.DwellTime) as DwellTime,				
		5 * (8 * Convert(bigint,SUM(Distinct(T.SiteUnits)))) as SiteUnits,				
		5 * (8 * Convert(bigint,SUM(Distinct(T.SiteWeight)))) as SiteWeight,				
		5 * (8 * Convert(bigint,SUM(Distinct(T.SiteInstructions)))) as SiteInstructions,		
		5 * (8 * Convert(bigint,SUM(Distinct(T.SiteOrders)))) as SiteOrders,				
		5 * (8 * Convert(bigint,SUM(Distinct(T.SiteOrderLines)))) as SiteOrderLines,			
		5 * (8 * Convert(bigint,SUM(Distinct(T.SiteJobs)))) as SiteJobs,				
		5 * (8 * Convert(bigint,SUM(Distinct(T.IndustryUnits)))) as IndustryUnits,			
		5 * (8 * Convert(bigint,SUM(Distinct(T.IndustryWeight)))) as IndustryWeight,			
		5 * (8 * Convert(bigint,SUM(Distinct(T.IndustryInstructions)))) as IndustryInstructions,	
		5 * (8 * Convert(bigint,SUM(Distinct(T.IndustryOrders)))) as IndustryOrders,			
		5 * (8 * Convert(bigint,SUM(Distinct(T.IndustryOrderLines)))) as IndustryOrderLines,		
		5 * (8 * Convert(bigint,SUM(Distinct(T.IndustryJobs)))) as IndustryJobs,			
		5 * (8 * Convert(bigint,SUM(Distinct(T.UOMValue)))) as UOMValue,
		5 * (8 * Convert(bigint,SUM(Distinct(T.UOMSiteTarget)))) as UOMSiteTarget,
		5 * (8 * Convert(bigint,SUM(Distinct(T.UOMIndustryTarget)))) as UOMIndustryTarget,
		Count(Distinct(T.OperatorId)) as ActiveOperators			
	From @TempTable T
	Left Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		Inner Join Operator O			On O.OperatorId				= T.OperatorId
							Group by T.OperatorGroupId,OG.OperatorGroup,O.Operator,DatePart(wk,EndDate),IT.InstructionType, T.InstructionTypeId
							ORDER BY ordscale		
				
					-- Select data by this Scale
				End
	End
Else
	Begin
			--Select  '3 months < x' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
Select 
		OG.OperatorGroup,
		--T.OperatorId,
		O.Operator,
		IT.InstructionType, 
		T.OperatorGroupId,			
		T.InstructionTypeId,		
		DateName(M,T.EndDate) as EndDate, 
		DatePart(M,T.EndDate) as ordscale, 					
		SUM(T.Units) as Units,					
		SUM(T.Weight) as Weight,					
		SUM(T.Instructions) as Instructions,			
		SUM(T.Orders) as Orders,					
		SUM(T.OrderLines) as OrderLines,				
		SUM(T.Jobs) as Jobs,					
		SUM(T.ActiveTime) as ActiveTime,				
		SUM(T.DwellTime) as DwellTime,				
		22 * (8 * Convert(bigint,SUM(Distinct(T.SiteUnits)))) as SiteUnits,				
		22 * (8 * Convert(bigint,SUM(Distinct(T.SiteWeight)))) as SiteWeight,				
		22 * (8 * Convert(bigint,SUM(Distinct(T.SiteInstructions)))) as SiteInstructions,		
		22 * (8 * Convert(bigint,SUM(Distinct(T.SiteOrders)))) as SiteOrders,				
		22 * (8 * Convert(bigint,SUM(Distinct(T.SiteOrderLines)))) as SiteOrderLines,			
		22 * (8 * Convert(bigint,SUM(Distinct(T.SiteJobs)))) as SiteJobs,				
		22 * (8 * Convert(bigint,SUM(Distinct(T.IndustryUnits)))) as IndustryUnits,			
		22 * (8 * Convert(bigint,SUM(Distinct(T.IndustryWeight)))) as IndustryWeight,			
		22 * (8 * Convert(bigint,SUM(Distinct(T.IndustryInstructions)))) as IndustryInstructions,
		22 * (8 * Convert(bigint,SUM(Distinct(T.IndustryOrders)))) as IndustryOrders,			
		22 * (8 * Convert(bigint,SUM(Distinct(T.IndustryOrderLines)))) as IndustryOrderLines,	
		22 * (8 * Convert(bigint,SUM(Distinct(T.IndustryJobs)))) as IndustryJobs,			
		22 * (8 * Convert(bigint,SUM(Distinct(T.UOMValue)))) as UOMValue,
		22 * (8 * Convert(bigint,SUM(Distinct(T.UOMSiteTarget)))) as UOMSiteTarget,
		22 * (8 * Convert(bigint,SUM(Distinct(T.UOMIndustryTarget)))) as UOMIndustryTarget,
		Count(Distinct(T.OperatorId)) as ActiveOperators			
	From @TempTable T
	Left Join InstructionType IT	On IT.InstructionTypeId		= T.InstructionTypeId
		Inner Join OperatorGroup OG		On OG.OperatorGroupId		= T.OperatorGroupId
		Inner Join Operator O			On O.OperatorId				= T.OperatorId
							Group by T.OperatorGroupId,OG.OperatorGroup,O.Operator,DatePart(M,T.EndDate),DateName(M,T.EndDate),IT.InstructionType, T.InstructionTypeId
							ORDER BY ordscale
	End


--Select * From @TempTable

end	   		
