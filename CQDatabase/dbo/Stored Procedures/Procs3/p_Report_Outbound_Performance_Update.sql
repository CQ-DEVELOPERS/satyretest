﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Outbound_Performance_Update
  ///   Filename       : p_Report_Outbound_Performance_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 May 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Outbound_Performance_Update

as
begin
	 set nocount on;
  
  insert OutboundPerformance
        (JobId,warehouseId)
  select distinct j.JobId,i.warehouseId
    from Job                    j (nolock)
    join Instruction            i (nolock) on j.JobId = i.JobId
    join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
   where not exists(select 1 from OutboundPerformance op where j.JobId = op.JobId)

  update op
     set CreateDate = jh.InsertDate
    from OutboundPerformance op
    join JobHistory          jh (nolock) on op.JobId = jh.JobId
   where op.CreateDate is null
     and jh.CommandType = 'Insert'

  update op
     set PlanningComplete = jh.InsertDate
    from OutboundPerformance op
    join JobHistory          jh (nolock) on op.JobId    = jh.JobId
    join Status               s (nolock) on jh.StatusId = s.StatusId
   where op.PlanningComplete is null
     and s.StatusCode = 'SA'

  update op
     set Release = jh.InsertDate
    from OutboundPerformance op
    join JobHistory          jh (nolock) on op.JobId    = jh.JobId
    join Status               s (nolock) on jh.StatusId = s.StatusId
   where op.Release is null
     and s.Type = 'IS'
     and s.StatusCode = 'RL'

  update op
     set Checking = jh.InsertDate
    from OutboundPerformance op
    join JobHistory          jh (nolock) on op.JobId    = jh.JobId
    join Status               s (nolock) on jh.StatusId = s.StatusId
   where op.Checking is null
     and s.Type = 'IS'
     and s.StatusCode = 'CK'
  

  update op
     set Checked = j.CheckedDate
    from OutboundPerformance op
    join Job                  j (nolock) on op.JobId    = j.JobId
   where op.Checked is null
     and j.CheckedDate is not null
  
  update op
     set Checked = jh.InsertDate
 
   from OutboundPerformance op
    join JobHistory          jh (nolock) on op.JobId    = jh.JobId
    join Status               s (nolock) on jh.StatusId = s.StatusId
   where op.Checked is null
     and s.Type = 'IS'
     and s.StatusCode = 'CD'
  
  update op
     set StartDate = (select min(i.StartDate)
                        from Instruction i (nolock)
                       where op.JobId = i.JobId  and i.warehouseId = op.warehouseId)
    from OutboundPerformance op
   where op.StartDate is null

  
update op
     set EndDate = (select max(i.EndDate)
                      from Instruction i (nolock)
                     where op.JobId = i.JobId  and i.warehouseId = op.warehouseId)
    from OutboundPerformance op
   where op.EndDate is null
     and op.Checking is not null -- Must not update end date if not finished

  update op
     set Despatch = jh.InsertDate
    from OutboundPerformance op
    join JobHistory          jh (nolock) on op.JobId    = jh.JobId
    join Status               s (nolock) on jh.StatusId = s.StatusId
   where op.Despatch is null
     and s.Type = 'IS'
     and s.StatusCode = 'D'

  update op
     set DespatchChecked = jh.InsertDate
    from OutboundPerformance op
    join JobHistory          jh (nolock) on op.JobId    = jh.JobId
    join Status               s (nolock) on jh.StatusId = s.StatusId
   where op.DespatchChecked is null
     and s.Type = 'IS'
     and s.StatusCode = 'DC'
 
  if dbo.ufn_Configuration(405, 1) = 0 -- Moresport complete is in Checked status
	  update op
		 set Complete = jh.InsertDate
		from OutboundPerformance op
		join JobHistory          jh (nolock) on op.JobId    = jh.JobId
		join Status               s (nolock) on jh.StatusId = s.StatusId
	   where op.Complete is null
		 and s.Type = 'IS'
		 and s.StatusCode = 'C'
  else
	  update op
		 set Complete = jh.InsertDate
		from OutboundPerformance op
		join JobHistory          jh (nolock) on op.JobId    = jh.JobId
		join Status               s (nolock) on jh.StatusId = s.StatusId
	   where op.Complete is null
		 and s.Type = 'IS'
		 and s.StatusCode in ('C','CD')
     
     
	Declare @FromDate Datetime,
			@ToDate DateTime
	
		
	Set @FromDate = DATEADD(dd,-2,getdate())
	Set @ToDate = GETDATE()		

	--exec p_Extract_Operator_Transaction_Summary @FromDate		= @FromDate, @ToDate			= @ToDate

end

 
