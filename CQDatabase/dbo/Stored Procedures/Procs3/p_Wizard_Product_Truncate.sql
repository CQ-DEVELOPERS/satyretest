﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wizard_Product_Truncate
  ///   Filename       : p_Wizard_Product_Truncate.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 25 Sep 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 	
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wizard_Product_Truncate

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
    TRUNCATE TABLE ProductImport
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Wizard_Product_Truncate'); 
    rollback transaction
    return @Error
end
