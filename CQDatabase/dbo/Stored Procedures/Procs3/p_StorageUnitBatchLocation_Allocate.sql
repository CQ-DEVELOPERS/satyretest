﻿/*
      <summary>
        Procedure Name : p_StorageUnitBatchLocation_Allocate
        Filename       : p_StorageUnitBatchLocation_Allocate.sql
        Create By      : Grant Schultz
        Date Created   : 23 May 2007 14:10:21
      </summary>
      <remarks>
        Removes Quantity on StorageUnitBatchLocation
      </remarks>
      <param>
        @InstructionId int
      </param>
      <returns>
        @error
      </returns>
      <newpara>
        Modified by    : 
        Modified Date  : 
        Details        : 
      </newpara>
*/
CREATE procedure p_StorageUnitBatchLocation_Allocate
(
 @InstructionId       int
,@Pick          bit = 1 -- (0 = false, 1 = true)
,@Store         bit = 1 -- (0 = false, 1 = true)
,@Confirmed     bit = 1 -- (0 = false, 1 = true)
)

as
begin
	 set nocount on
  
  declare @error              int,
          @errormsg           nvarchar(500),
          @StorageUnitBatchId int,
          @PickLocationId     int,
          @StoreLocationId    int,
          @Quantity           numeric(13,6),
          @ActualQuantity     numeric(13,6),
          @ReservedQuantity   numeric(13,6),
          @AllocatedQuantity  numeric(13,6),
          @Used               bit
   
   if @Confirmed = 0
     select @PickLocationId      = PickLocationId,
            @StoreLocationId     = StoreLocationId,
            @StorageUnitBatchId  = StorageUnitBatchId,
            @Quantity            = Quantity
       from Instruction
      where InstructionId = @InstructionId
   else
     select @PickLocationId      = PickLocationId,
            @StoreLocationId     = StoreLocationId,
            @StorageUnitBatchId  = StorageUnitBatchId,
            @Quantity            = ConfirmedQuantity
       from Instruction
      where InstructionId = @InstructionId
  
--  -- Pick Location cannot = store location 
--  if @PickLocationId = @StoreLocationId
--  begin
--    set @error = -1
--    goto error
--  end
  
  if @PickLocationId is not null and @Pick = 1
  begin
    if (select Picked
          from Instruction
         where InstructionId = @InstructionId) = 0
    begin
      select @ActualQuantity    = ActualQuantity,
             @AllocatedQuantity = AllocatedQuantity,
             @ReservedQuantity  = ReservedQuantity
        from StorageUnitBatchLocation
       where StorageUnitBatchId = @StorageUnitBatchId
         and LocationId = @PickLocationId
      
      set @ActualQuantity   = @ActualQuantity - @Quantity
      set @ReservedQuantity = @ReservedQuantity - @Quantity
      
      if @ActualQuantity <= 0 and @AllocatedQuantity = 0 and @ReservedQuantity = 0
      begin
        exec @error = p_StorageUnitBatchLocation_Delete
         @StorageUnitBatchId = @StorageUnitBatchId,
         @LocationId         = @PickLocationId
        
        if @error <> 0
          goto error
        
        if exists(select top 1 1
                    from StorageUnitBatchLocation
                   where LocationId = @PickLocationId)
          set @Used = 1
        else
          set @Used = 0
        
        -- Increment the Location.Used column
        exec @error = p_Location_Update
         @LocationId = @PickLocationId,
         @Used       = @Used
        
        if @error <> 0
          goto error
      end
      else
      begin
        if @ActualQuantity < 0
          set @ActualQuantity = 0
        
        exec @error = p_StorageUnitBatchLocation_Update
         @StorageUnitBatchId = @StorageUnitBatchId,
         @LocationId         = @PickLocationId,
         @ActualQuantity     = @ActualQuantity,
         @AllocatedQuantity  = null, -- Don't update
         @ReservedQuantity   = @ReservedQuantity
        
        if @error <> 0
          goto error
      end
      
      exec @Error = p_Instruction_Update
       @InstructionId = @InstructionId,
       @Picked        = 1
      
      if @error <> 0
        goto error
     end
  end
  
  set @Used = 1
  
  if @StoreLocationId is not null and @Store = 1
  begin
    if (select Stored
          from Instruction
         where InstructionId = @InstructionId) = 0
    begin
      if @Quantity is null
        set @Quantity = 0
      
      select @ActualQuantity    = ActualQuantity,
             @AllocatedQuantity = AllocatedQuantity,
             @ReservedQuantity  = ReservedQuantity
        from StorageUnitBatchLocation
       where StorageUnitBatchId = @StorageUnitBatchId
         and LocationId = @StoreLocationId
     
      if @AllocatedQuantity is null -- No row found
      begin
        if @Quantity > 0
        begin
          exec p_StorageUnitBatchLocation_Insert
           @StorageUnitBatchId = @StorageUnitBatchId,
           @LocationId         = @StoreLocationId,
           @ActualQuantity     = @Quantity,
           @AllocatedQuantity  = 0,
           @ReservedQuantity   = 0
          
          if @error <> 0
            goto error
        end
      end
      else
      begin
        set @ActualQuantity    = @ActualQuantity + @Quantity
        set @AllocatedQuantity = @AllocatedQuantity - @Quantity
        
        
        if @ActualQuantity <= 0 and @AllocatedQuantity = 0 and @ReservedQuantity = 0
        begin
          exec @error = p_StorageUnitBatchLocation_Delete
           @StorageUnitBatchId = @StorageUnitBatchId,
           @LocationId         = @StoreLocationId
          
          if @error <> 0
            goto error
          
          if exists(select top 1 1
                      from StorageUnitBatchLocation
                     where LocationId = @StoreLocationId)
            set @Used = 1
          else
            set @Used = 0
          
          -- Increment the Location.Used column
          exec @error = p_Location_Update
           @LocationId = @StoreLocationId,
           @Used       = 0
          
          if @error <> 0
            goto error
        end
        else
        begin
          if @ActualQuantity < 0
            set @ActualQuantity = 0
          
          if @AllocatedQuantity < 0
            set @AllocatedQuantity = 0
          
          exec p_StorageUnitBatchLocation_Update
           @StorageUnitBatchId = @StorageUnitBatchId,
           @LocationId         = @StoreLocationId,
           @ActualQuantity     = @ActualQuantity,
           @AllocatedQuantity  = @AllocatedQuantity,
           @ReservedQuantity   = null  -- Don't update
          
          print @StorageUnitBatchId
          print @StoreLocationId
          print @ActualQuantity
          print @AllocatedQuantity
		  --print @ReservedQuantity
          
          if @error <> 0
            goto error
        end
      end
      
      exec @Error = p_Instruction_Update
       @InstructionId = @InstructionId,
       @Stored        = 1
      
      if @error <> 0
        goto error
     end
  end
  
  return
  
  error:
    return @error
end
