﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VehicleType_Parameter
  ///   Filename       : p_VehicleType_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2014 14:12:06
  /// </summary>
  /// <remarks>
  ///   Selects rows from the VehicleType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   VehicleType.Id 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VehicleType_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as Id
        ,-1 as 'VehicleTypeId'
        ,'{NONE}' as 'VehicleType'
  union
  select
         VehicleType.Id
        ,VehicleType.Id as 'VehicleTypeId'
        ,VehicleType.Name as 'VehicleType'
    from VehicleType
  
end
 
 
