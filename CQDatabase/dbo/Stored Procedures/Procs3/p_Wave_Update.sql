﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Update
  ///   Filename       : p_Wave_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2013 14:17:30
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Wave table.
  /// </remarks>
  /// <param>
  ///   @WaveId int = null,
  ///   @Wave nvarchar(100) = null,
  ///   @CreateDate datetime = null,
  ///   @ReleasedDate datetime = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @CompleteDate datetime = null,
  ///   @ReleasedOrders datetime = null,
  ///   @PutawayDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Update
(
 @WaveId int = null,
 @Wave nvarchar(100) = null,
 @CreateDate datetime = null,
 @ReleasedDate datetime = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @CompleteDate datetime = null,
 @ReleasedOrders datetime = null,
 @PutawayDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @WaveId = '-1'
    set @WaveId = null;
  
  if @Wave = '-1'
    set @Wave = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
	 declare @Error int
 
  update Wave
     set Wave = isnull(@Wave, Wave),
         CreateDate = isnull(@CreateDate, CreateDate),
         ReleasedDate = isnull(@ReleasedDate, ReleasedDate),
         StatusId = isnull(@StatusId, StatusId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         CompleteDate = isnull(@CompleteDate, CompleteDate),
         ReleasedOrders = isnull(@ReleasedOrders, ReleasedOrders),
         PutawayDate = isnull(@PutawayDate, PutawayDate) 
   where WaveId = @WaveId
  
  select @Error = @@Error
  
  
  return @Error
  
end
