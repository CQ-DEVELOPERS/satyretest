﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnit_Update
  ///   Filename       : p_StorageUnit_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:02:11
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StorageUnit table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @SKUId int = null,
  ///   @ProductId int = null,
  ///   @ProductCategory char(1) = null,
  ///   @PackingCategory char(1) = null,
  ///   @PickEmpty bit = null,
  ///   @StackingCategory int = null,
  ///   @MovementCategory int = null,
  ///   @ValueCategory int = null,
  ///   @StoringCategory int = null,
  ///   @PickPartPallet int = null,
  ///   @MinimumQuantity int = null,
  ///   @ReorderQuantity int = null,
  ///   @MaximumQuantity int = null,
  ///   @DefaultQC bit = null,
  ///   @UnitPrice numeric(13,2) = null,
  ///   @Size nvarchar(200) = null,
  ///   @Colour nvarchar(200) = null,
  ///   @Style nvarchar(200) = null,
  ///   @PreviousUnitPrice numeric(13,2) = null,
  ///   @StockTakeCounts int = null,
  ///   @SerialTracked bit = null,
  ///   @SizeCode nvarchar(200) = null,
  ///   @ColourCode nvarchar(200) = null,
  ///   @StyleCode nvarchar(200) = null,
  ///   @StatusId int = null,
  ///   @PutawayRule nvarchar(400) = null,
  ///   @AllocationRule nvarchar(400) = null,
  ///   @BillingRule nvarchar(400) = null,
  ///   @CycleCountRule nvarchar(400) = null,
  ///   @LotAttributeRule nvarchar(400) = null,
  ///   @StorageCondition nvarchar(400) = null,
  ///   @ManualCost nvarchar(60) = null,
  ///   @OffSet nvarchar(60) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnit_Update
(
 @StorageUnitId int = null,
 @SKUId int = null,
 @ProductId int = null,
 @ProductCategory char(1) = null,
 @PackingCategory char(1) = null,
 @PickEmpty bit = null,
 @StackingCategory int = null,
 @MovementCategory int = null,
 @ValueCategory int = null,
 @StoringCategory int = null,
 @PickPartPallet int = null,
 @MinimumQuantity int = null,
 @ReorderQuantity int = null,
 @MaximumQuantity int = null,
 @DefaultQC bit = null,
 @UnitPrice numeric(13,2) = null,
 @Size nvarchar(200) = null,
 @Colour nvarchar(200) = null,
 @Style nvarchar(200) = null,
 @PreviousUnitPrice numeric(13,2) = null,
 @StockTakeCounts int = null,
 @SerialTracked bit = null,
 @SizeCode nvarchar(200) = null,
 @ColourCode nvarchar(200) = null,
 @StyleCode nvarchar(200) = null,
 @StatusId int = null,
 @PutawayRule nvarchar(400) = null,
 @AllocationRule nvarchar(400) = null,
 @BillingRule nvarchar(400) = null,
 @CycleCountRule nvarchar(400) = null,
 @LotAttributeRule nvarchar(400) = null,
 @StorageCondition nvarchar(400) = null,
 @ManualCost nvarchar(60) = null,
 @OffSet nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @SKUId = '-1'
    set @SKUId = null;
  
  if @ProductId = '-1'
    set @ProductId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
	 declare @Error int
 
  update StorageUnit
     set SKUId = isnull(@SKUId, SKUId),
         ProductId = isnull(@ProductId, ProductId),
         ProductCategory = isnull(@ProductCategory, ProductCategory),
         PackingCategory = isnull(@PackingCategory, PackingCategory),
         PickEmpty = isnull(@PickEmpty, PickEmpty),
         StackingCategory = isnull(@StackingCategory, StackingCategory),
         MovementCategory = isnull(@MovementCategory, MovementCategory),
         ValueCategory = isnull(@ValueCategory, ValueCategory),
         StoringCategory = isnull(@StoringCategory, StoringCategory),
         PickPartPallet = isnull(@PickPartPallet, PickPartPallet),
         MinimumQuantity = isnull(@MinimumQuantity, MinimumQuantity),
         ReorderQuantity = isnull(@ReorderQuantity, ReorderQuantity),
         MaximumQuantity = isnull(@MaximumQuantity, MaximumQuantity),
         DefaultQC = isnull(@DefaultQC, DefaultQC),
         UnitPrice = isnull(@UnitPrice, UnitPrice),
         Size = isnull(@Size, Size),
         Colour = isnull(@Colour, Colour),
         Style = isnull(@Style, Style),
         PreviousUnitPrice = isnull(@PreviousUnitPrice, PreviousUnitPrice),
         StockTakeCounts = isnull(@StockTakeCounts, StockTakeCounts),
         SerialTracked = isnull(@SerialTracked, SerialTracked),
         SizeCode = isnull(@SizeCode, SizeCode),
         ColourCode = isnull(@ColourCode, ColourCode),
         StyleCode = isnull(@StyleCode, StyleCode),
         StatusId = isnull(@StatusId, StatusId),
         PutawayRule = isnull(@PutawayRule, PutawayRule),
         AllocationRule = isnull(@AllocationRule, AllocationRule),
         BillingRule = isnull(@BillingRule, BillingRule),
         CycleCountRule = isnull(@CycleCountRule, CycleCountRule),
         LotAttributeRule = isnull(@LotAttributeRule, LotAttributeRule),
         StorageCondition = isnull(@StorageCondition, StorageCondition),
         ManualCost = isnull(@ManualCost, ManualCost),
         OffSet = isnull(@OffSet, OffSet) 
   where StorageUnitId = @StorageUnitId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_StorageUnitHistory_Insert
         @StorageUnitId = @StorageUnitId,
         @SKUId = @SKUId,
         @ProductId = @ProductId,
         @ProductCategory = @ProductCategory,
         @PackingCategory = @PackingCategory,
         @PickEmpty = @PickEmpty,
         @StackingCategory = @StackingCategory,
         @MovementCategory = @MovementCategory,
         @ValueCategory = @ValueCategory,
         @StoringCategory = @StoringCategory,
         @PickPartPallet = @PickPartPallet,
         @MinimumQuantity = @MinimumQuantity,
         @ReorderQuantity = @ReorderQuantity,
         @MaximumQuantity = @MaximumQuantity,
         @DefaultQC = @DefaultQC,
         @UnitPrice = @UnitPrice,
         @Size = @Size,
         @Colour = @Colour,
         @Style = @Style,
         @PreviousUnitPrice = @PreviousUnitPrice,
         @StockTakeCounts = @StockTakeCounts,
         @SerialTracked = @SerialTracked,
         @SizeCode = @SizeCode,
         @ColourCode = @ColourCode,
         @StyleCode = @StyleCode,
         @StatusId = @StatusId,
         @PutawayRule = @PutawayRule,
         @AllocationRule = @AllocationRule,
         @BillingRule = @BillingRule,
         @CycleCountRule = @CycleCountRule,
         @LotAttributeRule = @LotAttributeRule,
         @StorageCondition = @StorageCondition,
         @ManualCost = @ManualCost,
         @OffSet = @OffSet,
         @CommandType = 'Update'
  
  return @Error
  
end
