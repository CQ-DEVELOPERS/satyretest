﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_Search
  ///   Filename       : p_StorageUnitBatch_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:01
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StorageUnitBatch table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StorageUnitBatch.StorageUnitBatchId,
  ///   StorageUnitBatch.BatchId,
  ///   StorageUnitBatch.StorageUnitId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatch_Search
(
 @StorageUnitBatchId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
 
  select
         StorageUnitBatch.StorageUnitBatchId
        ,StorageUnitBatch.BatchId
        ,StorageUnitBatch.StorageUnitId
    from StorageUnitBatch
   where isnull(StorageUnitBatch.StorageUnitBatchId,'0')  = isnull(@StorageUnitBatchId, isnull(StorageUnitBatch.StorageUnitBatchId,'0'))
  
end
