﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatchLocation_List
  ///   Filename       : p_StorageUnitBatchLocation_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:04
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitBatchLocation table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   StorageUnitBatchLocation.StorageUnitBatchId,
  ///   StorageUnitBatchLocation.LocationId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatchLocation_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as StorageUnitBatchId
        ,null as 'StorageUnitBatchLocation'
        ,'-1' as LocationId
        ,null as 'StorageUnitBatchLocation'
  union
  select
         StorageUnitBatchLocation.StorageUnitBatchId
        ,StorageUnitBatchLocation.StorageUnitBatchId as 'StorageUnitBatchLocation'
        ,StorageUnitBatchLocation.LocationId
        ,StorageUnitBatchLocation.LocationId as 'StorageUnitBatchLocation'
    from StorageUnitBatchLocation
  
end
