﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Station_Sheet_Receipt_Receive_Update
  ///   Filename       : p_Station_Sheet_Receipt_Receive_Update.sql
  ///   Create By      : Karen
  ///   Date Created   : Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Station_Sheet_Receipt_Receive_Update
(
 @LocationId			int,
 @ReceiptId				int,
 @AllowPalletise		int,
 @OrderNumber			nvarchar(30)
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime

  if @LocationId = -1
    set @LocationId = null  
  
--  if (select count(distinct(StatusId)) from ReceiptLine where ReceiptId = @ReceiptId) = 1
--  begin
    begin transaction
		
	UPDATE    Receipt
	SET              
		LocationId = @LocationId
    
	Where ReceiptId = @ReceiptId


    if @Error <> 0
      goto error
    
    commit transaction
--  end
  
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Station_Sheet_Receipt_Receive_Update'); 
    rollback transaction
    return @Error
end
