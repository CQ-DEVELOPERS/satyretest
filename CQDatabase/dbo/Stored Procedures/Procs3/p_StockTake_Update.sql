﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTake_Update
  ///   Filename       : p_StockTake_Update.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2011
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StockTakeReference table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTake_Update
(
 @connectionStringName		varchar(255) = null,
 @WarehouseId int = null,
 @StockTakeType varchar(50) = null,
 @StartDate datetime = null,
 @EndDate datetime = null,
 @OperatorId int = null,
 @StockTakeReferenceId int = null
)

as
begin
	 set nocount on;
  
  declare @Error int
  update StockTakeReference
     set WarehouseId = isnull(@WarehouseId, WarehouseId),
         StockTakeType = isnull(@StockTakeType, StockTakeType),
         StartDate = isnull(@StartDate, StartDate),
         EndDate = isnull(@EndDate, EndDate),
         OperatorId = isnull(@OperatorId, OperatorId)
   where StockTakeReferenceId = @StockTakeReferenceId
   
  select @Error = @@Error
  
end
