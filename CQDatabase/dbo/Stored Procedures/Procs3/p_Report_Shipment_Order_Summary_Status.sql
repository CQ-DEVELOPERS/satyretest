﻿ 
/*
/// <summary>
///   Procedure Name : p_Report_Shipment_Order_Summary_Status
///   Filename       : p_Report_Shipment_Order_Summary_Status.sql
///   Create By      : Karen
///   Date Created   : February 2014
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
CREATE procedure p_Report_Shipment_Order_Summary_Status
(
@FromDate          datetime,
@ToDate            datetime,
@PrincipalId	   int,
@ExternalCompanyId int
)

as
begin
  set nocount on;
  
  
  if @PrincipalId = '-1'
     set @PrincipalId = null
         
  if @ExternalCompanyId = '-1'
     set @ExternalCompanyId = null  
     
  declare @ExternalCompany	   nvarchar(255),
		  @ExternalCompanyCode nvarchar(30) 		  
    
	if @ExternalCompanyId is not null
	begin
		set @ExternalCompanyCode = (select ec.ExternalCompanyCode 
									  from ExternalCompany ec 
									 where ec.ExternalCompanyId = @ExternalCompanyId)
		set @ExternalCompany = (select ec.ExternalCompany 
								  from ExternalCompany ec 
								 where ec.ExternalCompanyId = @ExternalCompanyId)
	end  
	else
		set @ExternalCompanyCode = 'ALL'    
	
  declare @TableSumm as table
  (
   Status  			nvarchar(50),
   StatusCount		float
  )
    
  
  declare @TableDetails as Table
  (
  StatusId			int,
  Status			nvarchar(50),
  OrderNumber		nvarchar(30)
  )
    
	  insert @TableDetails
			(StatusId,
			 Status,
			 OrderNumber
			 )
	  select distinct r.StatusId,
			 s.Status,
			 id.OrderNumber
		from InboundDocument     id (nolock)
		join Receipt			  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
		join status			  s (nolock) on r.StatusId = s.StatusId
	   where id.CreateDate between @FromDate and @ToDate
    

	insert	@TableSumm
			(Status,
			StatusCount
			)
	select	distinct Status,
			COUNT(OrderNumber)
	  from @TableDetails	
	 group by status 
		 
	Select *
	  from @TableSumm
     

end
