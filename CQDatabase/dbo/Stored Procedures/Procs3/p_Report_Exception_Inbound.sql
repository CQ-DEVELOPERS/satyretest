﻿/*
  /// <summary>
  ///   Procedure Name : p_Report_Exception_Inbound
  ///   Filename       : p_Report_Exception_Inbound.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Exception_Inbound 
(@WarehouseId		int,
 @FromDate          datetime = null,
 @ToDate            datetime = null,
 @ExceptionCode		nvarchar(50) = null)
 

as
begin
  set nocount on;

  If (@ToDate = '1900-01-01')  set @ToDate = '2080-01-01'
  If (Len(@ExceptionCode) = 0) set @ExceptionCode = null

  if @ExceptionCode = '{All}' or @ExceptionCode = '-1'
    set @ExceptionCode = null

  Select Isnull(e.Exception,'Unknown') as Exception,
		 Isnull(e.ExceptionDate,e.CreateDate) as ExceptionDate,
		 rl.OrderNumber,
		 rl.SupplierCode as 'CompanyCode',
		 e.ExceptionCode,
         rl.SupplierName as 'Company',
         rl.ProductCode,
         rl.Product,
         rl.SKUCode,
         rl.RequiredQuantity,
         rl.Batch,
         rl.ExpiryDate,
         isnull(rl.OperatorId, i.OperatorId) as OperatorId,
         o.Operator,
         i.PalletId
    from Exception e
		  Left Join viewInstruction i on i.InstructionId = e.InstructionId
		  Left Join viewReceiptLine rl on rl.ReceiptLineId = Isnull(i.ReceiptLineId,e.ReceiptLineId)
		  left join Operator o (nolock) on o.OperatorId = isnull(rl.OperatorId, i.OperatorId)
   where rl.warehouseId = @WarehouseId and Isnull(exceptionDate,e.createDate) between Isnull(@FromDate,'1900-01-01') and isnull(@ToDate,'2100-01-01')
     and e.ExceptionCode = isnull(@ExceptionCode,e.ExceptionCode)
end

