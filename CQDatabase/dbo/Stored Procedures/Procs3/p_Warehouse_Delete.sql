﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Warehouse_Delete
  ///   Filename       : p_Warehouse_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 May 2014 13:32:01
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Warehouse table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Warehouse_Delete
(
 @WarehouseId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Warehouse
     where WarehouseId = @WarehouseId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_WarehouseHistory_Insert
         @WarehouseId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
