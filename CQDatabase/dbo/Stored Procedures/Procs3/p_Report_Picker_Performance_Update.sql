﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Picker_Performance_Update
  ///   Filename       : p_Report_Picker_Performance_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jul 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Picker_Performance_Update

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StartDate		 datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @StartDate = DATEADD(DD,-15,@GetDate)
  
  -- Make the Date to the hour by excluding minutes and seconds
  select @GetDate = convert(nvarchar(14), @GetDate, 120) + '00:00'
  
  begin transaction
  
  select count(1) as 'OperatorPerformance' from OperatorPerformance (nolock)
  
  insert OperatorPerformance
        (WarehouseId,
		 OperatorId,	
         InstructionTypeId,
         EndDate,
         Units,
         Weight,
         Instructions,
         Orders,
         OrderLines,
         Jobs,
         ActiveTime,
         DwellTime)
  select	i.WarehouseId,
			i.OperatorId,
         i.InstructionTypeId,
         convert(nvarchar(14), i.EndDate, 120) + '00:00',
         sum(i.ConfirmedQuantity),
         sum(i.ConfirmedWeight),
         count(distinct(i.InstructionId)),
         0,
         0,
         count(distinct(i.JobId)),
         sum(datediff(ss, i.StartDate, i.EndDate)),
         3600 - sum(datediff(ss, i.StartDate, i.EndDate))
    from Instruction            i (nolock)
    --join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
      --                        or i.InstructionRefId = ili.InstructionId
   where i.EndDate <= @GetDate
     and i.EndDate > @StartDate
     and not exists(select 1 from OperatorPerformance op where op.EndDate = convert(nvarchar(14), i.EndDate, 120) + '00:00')
  group by i.OperatorId, i.InstructionTypeId,i.WarehouseId,
           convert(nvarchar(14), i.EndDate, 120) + '00:00'
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  update op
     set Orders = (select count(distinct(ili.OutboundDocumentId))
                    from Instruction i (nolock)
						Join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
                              or i.InstructionRefId = ili.InstructionId
                   where i.InstructionTypeId = op.InstructionTypeId
                     and i.EndDate between op.EndDate and dateadd(hh, 1, op.EndDate)
                     and i.OperatorId = op.OperatorId
					 and i.WarehouseId = op.WarehouseId)
    from OperatorPerformance op (nolock)
    where  op.EndDate > @StartDate
  
  
  update op
     set OrderLines = (select count(distinct(ili.IssueLineId))
                    from Instruction i (nolock)
						Join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
                              or i.InstructionRefId = ili.InstructionId
                   where i.InstructionTypeId = op.InstructionTypeId
                     and i.EndDate between op.EndDate and dateadd(hh, 1, op.EndDate)
                     and i.OperatorId = op.OperatorId
					 and i.WarehouseId = op.WarehouseId)
    from OperatorPerformance op (nolock)
    where  op.EndDate > @StartDate
  
  update op
     set Units = (select sum(ConfirmedQuantity)
                    from Instruction i (nolock)
                   where i.InstructionTypeId = op.InstructionTypeId
                     and i.EndDate between op.EndDate and dateadd(hh, 1, op.EndDate)
                     and i.OperatorId = op.OperatorId
					 and i.WarehouseId = op.WarehouseId)
    from OperatorPerformance op (nolock)
    where  op.EndDate > @StartDate
  
  update op
     set Weight = (select sum(i.ConfirmedWeight)
                     from Instruction i (nolock)
                    where i.InstructionTypeId = op.InstructionTypeId
                      and i.EndDate between op.EndDate and dateadd(hh, 1, op.EndDate)
                      and i.OperatorId = op.OperatorId
						and i.WarehouseId = op.WarehouseId)
    from OperatorPerformance op (nolock)
    where  op.EndDate > @StartDate
  
  select count(1) as 'OperatorPerformance' from OperatorPerformance
  
  select count(1) as 'OperatorShortPickPerformance' from OperatorShortPickPerformance
  
  insert OperatorShortPickPerformance
        (WarehouseId,
		 OperatorId,
         InstructionTypeId,
         EndDate,
         Units,
         Weight,
         UnitsShort,
         WeightShort,
         Instructions,
         Orders,
         OrderLines,
		       Jobs,
         ActiveTime,
         DwellTime)
  select i.WarehouseId,
		 i.OperatorId,
         i.InstructionTypeId,
         convert(nvarchar(14), i.EndDate, 120) + '00:00',
         sum(i.ConfirmedQuantity),
         sum(i.ConfirmedWeight),
         sum(i.Quantity - i.ConfirmedQuantity),
         sum(i.Weight - i.ConfirmedWeight),
         count(distinct(i.InstructionId)),
         0,
         0,
         count(distinct(i.JobId)),
         sum(datediff(ss, i.StartDate, i.EndDate)),
         3600 - sum(datediff(ss, i.StartDate, i.EndDate))
    from Instruction            i (nolock)
    --join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
      --                        or i.InstructionRefId = ili.InstructionId
   where i.EndDate <= @GetDate
     and i.EndDate > @StartDate
     and not exists(select 1 from OperatorShortPickPerformance op (nolock) where op.EndDate = convert(nvarchar(14), i.EndDate, 120) + '00:00')
     and i.Quantity > i.ConfirmedQuantity
  group by i.OperatorId, i.InstructionTypeId,i.WarehouseId,
           convert(nvarchar(14), i.EndDate, 120) + '00:00'
  --Select top 10 * from Instruction
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  update op
     set Orders = (select count(distinct(ili.OutboundDocumentId))
                    from Instruction i
                    Join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
                              or i.InstructionRefId = ili.InstructionId
                   where i.InstructionTypeId = op.InstructionTypeId
                     and i.EndDate between op.EndDate and dateadd(hh, 1, op.EndDate)
                     and i.OperatorId = op.OperatorId
						and i.WarehouseId = op.WarehouseId)
    from OperatorShortPickPerformance op
    where  op.EndDate > @StartDate
  
  
   update op
     set OrderLines = (select count(distinct(ili.IssueLineId))
                    from Instruction i
                    Join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
                              or i.InstructionRefId = ili.InstructionId
                   where i.InstructionTypeId = op.InstructionTypeId
                     and i.EndDate between op.EndDate and dateadd(hh, 1, op.EndDate)
                     and i.OperatorId = op.OperatorId
						and i.WarehouseId = op.WarehouseId)
    from OperatorShortPickPerformance op
    where  op.EndDate > @StartDate
  
  update op
     set Units = (select sum(ConfirmedQuantity)
                    from Instruction i
                   where i.InstructionTypeId = op.InstructionTypeId
                     and i.EndDate between op.EndDate and dateadd(hh, 1, op.EndDate)
                     and i.OperatorId = op.OperatorId
						and i.WarehouseId = op.WarehouseId)
    from OperatorShortPickPerformance op
    where  op.EndDate > @StartDate
  
  update op
     set Weight = (select sum(i.ConfirmedWeight)
                     from Instruction i (nolock)
                    where i.InstructionTypeId = op.InstructionTypeId
                      and i.EndDate between op.EndDate and dateadd(hh, 1, op.EndDate)
                      and i.OperatorId = op.OperatorId
						and i.WarehouseId = op.WarehouseId)
    from OperatorShortPickPerformance op (nolock)
    where  op.EndDate > @StartDate
  
  select count(1) as 'OperatorShortPickPerformance' from OperatorShortPickPerformance (nolock)
  
  select count(1) as 'OperatorAverage' from OperatorAverage (nolock)
  
  insert OperatorAverage
        (WarehouseId,
		 OperatorGroupId,
         InstructionTypeId,
         EndDate,
         Units,
         Weight,
         Instructions,
         Orders,
         OrderLines,
         Jobs,	 
         ActiveTime,
         DwellTime)
  select op.WarehouseId,
		 o.OperatorGroupId,
         op.InstructionTypeId,         
         op.EndDate,
         sum(op.Units) / count(distinct(op.OperatorId)),
         sum(op.Weight) / count(distinct(op.OperatorId)),
         sum(op.Instructions) / count(distinct(op.OperatorId)),
         sum(op.Orders) / count(distinct(op.OperatorId)),
         sum(op.OrderLines) / count(distinct(op.OperatorId)),
		       sum(op.Jobs) / count (distinct(op.Jobs)),
         sum(op.ActiveTime) / count(distinct(op.OperatorId)),
         sum(op.DwellTime) / count(distinct(op.OperatorId))
    from OperatorPerformance op (nolock)
    join Operator             o (nolock) on op.OperatorId = o.OperatorId
   where op.EndDate > @StartDate
   and not exists(select 1 from OperatorAverage oa (nolock) where op.EndDate = oa.EndDate)
  group by o.OperatorGroupId,op.InstructionTypeId,op.WarehouseId,
           op.EndDate
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  select count(1) as 'OperatorAverage' from OperatorAverage (nolock)
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Report_Picker_Performance_Update'); 
    rollback transaction
    return @Error
end
 
