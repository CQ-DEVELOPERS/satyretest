﻿ 
/*
  /// <summary>
  ///   Procedure Name : 'dbo.p_Station_Sheet_Exception_Delete
  ///   Filename       : 'dbo.p_Station_Sheet_Exception_Delete.sql
  ///   Create By      : Karen
  ///   Date Created   : 16 Mar 2011 15:15:30
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Exception table.
  /// </remarks>
  /// <param>
  ///   @ExceptionId int = null,
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Station_Sheet_Exception_Delete
(
 @ExceptionId int = null
)

as
begin
	 set nocount on;
  
	 declare @Error int
delete from Exception
   where ExceptionId = @ExceptionId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_ExceptionHistory_Insert
         @ExceptionId = @ExceptionId,
         @CommandType = 'Delete'
  
  return @Error
  
end
