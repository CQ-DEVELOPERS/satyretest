﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wizard_Location_Check
  ///   Filename       : p_Wizard_Location_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Junaid Desai	
  ///   Modified Date  : 19 Sept 2008
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wizard_Location_Check
(
    @LocationImportId int = -1
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if (@LocationImportId = -1)
  
  begin
		

		Update LocationImport set ErrorLog = '<Root></Root>',HasError = 0

		Update LocationImport set Haserror = 1,ErrorLog.modify('insert <error> Area Code \\n</error> into (/Root)[1]') where
		AreaCode not in('BK', 'D', 'J', 'JJ', 'PK', 'R', 'RK', 'S')
	
		Update LocationImport set Haserror = 1,ErrorLog.modify('insert <error> Warehouse Code \\n</error> into (/Root)[1]') where
		WarehouseCode not in(select WarehouseCode from Warehouse)

		Update LocationImport Set HasError = 1,ErrorLog.modify('insert <error> Ailse,Column,Level \\n</error> into (/Root)[1]') where 
		LocationImportId in (Select X.LocationImportID  From 
					(Select LocationImportId,Ailse,[Column],[Level] From LocationImport) X 
					inner join 
					(Select LocationId,Ailse,[Column],[Level] From Location )Y 
					on	x.ailse = y.ailse and 
						x.[Column] = y.[Column] and
						x.[Level] = y.[Level])

		Update LocationImport Set HasError = 1,ErrorLog.modify('insert <error> Location \\n</error> into (/Root)[1]') where Location in 
		(Select Location from Location)	
		


  if @Error <> 0
    goto error
  end
  
  else
  
  begin
		
		Update LocationImport set ErrorLog = '<Root></Root>',HasError = 0 where (LocationImportId = @LocationImportId)

		Update LocationImport set Haserror = 1,ErrorLog.modify('insert <error> Area Code \\n</error> into (/Root)[1]') where
		AreaCode not in('BK', 'D', 'J', 'JJ', 'PK', 'R', 'RK', 'S') And (LocationImportId = @LocationImportId)	
		
		Update LocationImport set Haserror = 1,ErrorLog.modify('insert <error> Warehouse Code \\n</error> into (/Root)[1]')  where
		WarehouseCode not in(select WarehouseCode from Warehouse) And (LocationImportId = @LocationImportId)
  
				Update LocationImport Set HasError = 1,ErrorLog.modify('insert <error> Ailse,Column,Level \\n</error> into (/Root)[1]') where 
					LocationImportId in (Select X.LocationImportID  From 
									(Select LocationImportId,Ailse,[Column],[Level] From LocationImport) X 
									inner join 
									(Select LocationId,Ailse,[Column],[Level] From Location )Y 
									on	x.ailse = y.ailse and 
										x.[Column] = y.[Column] and
										x.[Level] = y.[Level])
					And (LocationImportId = @LocationImportId)

		Update LocationImport Set HasError = 1,ErrorLog.modify('insert <error> Location  \\n</error> into (/Root)[1]') where Location in 
		(Select Location from Location)	And (LocationImportId = @LocationImportId)

  if @Error <> 0
    goto error
  end
  
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Wizard_Location_Check'); 
    rollback transaction
    return @Error
end
