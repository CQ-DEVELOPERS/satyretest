﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Route_Parameter
  ///   Filename       : p_Route_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:08
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Route table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Route.RouteId,
  ///   Route.Route 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Route_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as RouteId
        ,'{All}' as Route
  union
  select
         Route.RouteId
        ,Route.Route
    from Route
  
end
