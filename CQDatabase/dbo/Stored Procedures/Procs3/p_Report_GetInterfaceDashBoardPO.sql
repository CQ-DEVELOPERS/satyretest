﻿


-- =============================================

-- Author:        <Karen Sucksmith>

--Create date:    <18 August 2010>

--Description:    <Count all Purchase Orders depending on parameters passed from level 2 dashboard drilldown>

--DROP PROCEDURE [dbo].[p_Report_GetInterfaceDashBoardPO]

-- =============================================

CREATE PROCEDURE [dbo].[p_Report_GetInterfaceDashBoardPO]
(
@FromDate datetime,
@ToDate datetime,
@fromtohost char(1),
@extracttype char(3),
@ExtractDescription nvarchar(30),
@Show char(1)
)

AS

begin

--drop table @temp

declare @temp as  table 
(
 InterfaceReprocessId	int,
 PrimaryKey				nvarchar(30),
 OrderNumber			nvarchar(30),
 OtherKey				nvarchar(30),
 TableType				nvarchar(30),
 RepCount				int
)

insert @temp (PrimaryKey, OrderNumber, OtherKey, TableType, RepCount) 
select PrimaryKey, OrderNumber, OtherKey, TableType, count(InterfaceReprocessId)
from InterfaceReprocessCount
group by PrimaryKey, OrderNumber, OtherKey, TableType

end

IF @extracttype = 'EXT'

BEGIN 

SET NOCOUNT ON;
       

DECLARE @OrderNumber nvarchar(30),
        @RecordType CHAR(3),
        @RecordStatus CHAR(1),
        @DeliveryNoteNumber nvarchar(30),
        @DeliveryDate DATETIME,
        @ProcessedDate DATETIME,
        @ErrorDescription nvarchar(255),
        @InterfaceMessage nvarchar(255),
        @ReprocessCount int
     
                 
        
SELECT t1.InterfaceExportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount
		
FROM InterfaceExportPOHeader t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportPOHeaderId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportPOHeaderId 
left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 
WHERE t1.RecordStatus <> 'N' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate
		

SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]  
       

       
        
END
 
IF @extracttype = 'NEX'

BEGIN 

SET NOCOUNT ON;

SELECT t1.InterfaceExportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount
 
FROM InterfaceExportPOHeader t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportPOHeaderId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportPOHeaderId 
left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 
WHERE t1.RecordStatus = 'N' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate
		
       

SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]     
        
END

IF @extracttype = 'TSU'

BEGIN 

SET NOCOUNT ON;
   
       
SELECT t1.InterfaceExportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount

FROM InterfaceExportPOHeader t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportPOHeaderId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportPOHeaderId 
left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 
WHERE t1.RecordStatus = 'S' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate
		
SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]       
        
END

IF @extracttype = 'TFA'

BEGIN 

SET NOCOUNT ON;
   
       
SELECT t1.InterfaceExportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount

FROM InterfaceExportPOHeader t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportPOHeaderId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportPOHeaderId 
left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 
WHERE t1.RecordStatus = 'F' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate
		
		

SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]        
        
END

IF @extracttype = 'TDW'

BEGIN 

SET NOCOUNT ON;
   
       
SELECT t1.InterfaceExportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount
		
FROM InterfaceExportPOHeader t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportPOHeaderId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportPOHeaderId 
left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 
WHERE t1.RecordStatus = 'D' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate
	

SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]        
        
END

IF @extracttype = 'UNP'

BEGIN 

SET NOCOUNT ON;
   
       
SELECT t1.InterfaceImportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount

FROM InterfaceImportPOHeader t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceImportPOHeaderId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceImportPOHeaderId 
left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 
WHERE t1.RecordStatus = 'N' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate
	
SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]         
        
END

IF @extracttype = 'SUC'

BEGIN 

SET NOCOUNT ON;
   
       
SELECT t1.InterfaceImportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount

FROM InterfaceImportPOHeader t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceImportPOHeaderId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceImportPOHeaderId 
left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 
WHERE t1.RecordStatus = 'C' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate
	

SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]         
        
END

IF @extracttype = 'FAI'

BEGIN 

SET NOCOUNT ON;
   
       
SELECT t1.InterfaceImportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount
		
FROM InterfaceImportPOHeader t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceImportPOHeaderId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceImportPOHeaderId  
left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 
WHERE t1.RecordStatus = 'E' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate
		
SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]  
     
       
END

IF @extracttype = 'DEW'

BEGIN 

SET NOCOUNT ON;
   
       
SELECT t1.InterfaceImportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount

FROM InterfaceImportPOHeader t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceImportPOHeaderId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceImportPOHeaderId 
left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 
WHERE t1.RecordStatus = 'D' and t1.RecordType= 'PUR' and t1.ProcessedDate BETWEEN  @FromDate and @ToDate
	
		

SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]       
        
END

IF @extracttype = 'TWA'

BEGIN 

SET NOCOUNT ON;
       
            
SELECT t1.InterfaceExportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       t3.ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount

FROM InterfaceExportPOHeader t1
left JOIN InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportPOHeaderId
left JOIN InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportPOHeaderId 
left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 

WHERE	t1.RecordStatus = 'Y' 
		and t1.RecordType = 'PUR' 
		and t1.ProcessedDate BETWEEN  @FromDate and @ToDate
	
SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]  
       
        
END

IF @extracttype = 'SSU'

BEGIN 

SET NOCOUNT ON;
       
            
SELECT tx1.InterfaceExportPOHeaderId,
	   tx1.OrderNumber,
	   tx1.RecordType,
	   tx1.RecordStatus,
	   tx1.DeliveryNoteNumber,
	   tx1.DeliveryDate,
	   tx1.ProcessedDate,
       '' as ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount

				
FROM InterfaceExportPOHeader tx1
	Join  InterfaceMessage t2 on t2.InterfaceId = tx1.InterfaceExportPOHeaderId
	--JOin  InterfaceDashboardBulkLog t3 on t3.FileKeyId = tx1.InterfaceExportPOHeaderId	
	Left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey =  tx1.PrimaryKey 
					and t4.OrderNumber = tx1.OrderNumber 
					and t4.OtherKey = tx1.DeliveryNoteNumber 
WHERE t2.Status <> 'N' and t2.ProcessedDate BETWEEN  @FromDate and @ToDate 


SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]  
      
        
END

IF @extracttype = 'SER'

BEGIN 

SET NOCOUNT ON;
       
            
SELECT t1.InterfaceExportPOHeaderId,
	   t1.OrderNumber,
	   t1.RecordType,
	   t1.RecordStatus,
	   t1.DeliveryNoteNumber,
	   t1.DeliveryDate,
	   t1.ProcessedDate,
       '' as ErrorDescription,
       t2.InterfaceMessage,
       t4.RepCount

	
FROM InterfaceExportPOHeader t1
	Join InterfaceMessage t2 on t2.InterfaceId = t1.InterfaceExportPOHeaderId
	--Join InterfaceDashboardBulkLog t3 on t3.FileKeyId = t1.InterfaceExportPOHeaderId	
	left join @temp t4	on	TableType = 'PO' 
					and t4.PrimaryKey = t1.PrimaryKey 
					and t4.OrderNumber = t1.OrderNumber 
					and t4.OtherKey = t1.DeliveryNoteNumber 
WHERE t2.Status = 'E' and t2.ProcessedDate BETWEEN  @FromDate and @ToDate 

SELECT @OrderNumber [Order Number],
	   @RecordType [Record Type],
	   @RecordStatus [Record Status],
	   @DeliveryNoteNumber [Delivery Note Number],
	   @DeliveryDate [Delivery Date],
	   @ProcessedDate [Processed Date],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ErrorDescription [CQ Extract Error],
       @InterfaceMessage [Host Processing Error],
       @ReprocessCount [Reprocess Count]  
      
END

--exec [p_Report_GetInterfaceDashBoardPO] '2010-01-10', '2010-10-10', 'T', 'EXT', 'vvvv', 'Y'
--exec [p_Report_GetInterfaceDashBoardPO] '2010-01-10', '2010-08-10', 'T', 'NEX'
--exec [p_Report_GetInterfaceDashBoardPO] '2010-01-10', '2010-08-10', 'F', 'UNP'
--exec [p_Report_GetInterfaceDashBoardPO] '2010-01-27', '2010-08-24', 'F', 'SUC'
--exec [p_Report_GetInterfaceDashBoardPO] '2010-01-10', '2010-08-10', 'F', 'FAI', 'ccc'
--exec [p_Report_GetInterfaceDashBoardPO] '2010-01-10', '2010-08-10', 'F', 'DEW'
--exec [p_Report_GetInterfaceDashBoardPO] '2010-01-10', '2010-08-10', 'F', 'DWA'
--exec [p_Report_GetInterfaceDashBoardPO] '2010-01-10', '2010-08-10', 'F', 'SSU'
--exec [p_Report_GetInterfaceDashBoardPO] '2010-01-10', '2010-08-10', 'F', 'SER'



