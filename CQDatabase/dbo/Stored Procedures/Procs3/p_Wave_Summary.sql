﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Summary
  ///   Filename       : p_Wave_Summary.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Oct 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Summary

as
begin
  /*
  drop table WaveAvailablity
  go

  create table WaveAvailablity
  (
   OutboundShipmentId    int,
   IssueId               int,
   OrderNumber           nvarchar(30),
   PercentageAvailable   numeric(13,2),
   PercentageDrawDown    numeric(13,2),
   BulkPallets           int,
   SimilarSKUDI          nvarchar(max),
   NumberOfLines         int
  )
  */
  truncate table WaveAvailablity
  
  declare @TableResult as table
  (
   OutboundShipmentId    int,
   IssueId               int,
   OrderNumber           nvarchar(30),
   SimilarSKUDI          nvarchar(max),
   WarehouseId           int,
   StorageUnitId         int,
   AllocatedQuantity     numeric(13,6) default 0,
   RequiredQuantity      numeric(13,6) default 0,
   WavePickQuantity      numeric(13,6) default 0,
   NarrowAisle           numeric(13,6) default 0,
   ReplenishmentQuantity numeric(13,6) default 0,
   AvailableQuantity     numeric(13,6) default 0,
   AvailablePercentage   numeric(13,2) default 0,
   DrawDownPercentage    numeric(13,2) default 0,
   DrawDownQuantity      numeric(13,2) default 0,
   DrawDownPallets       numeric(13,2),
   PalletQuantity        numeric(13,2),
   SKUCount              int
  )

  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         WarehouseId,
         StorageUnitId,
         RequiredQuantity)
  select osi.OutboundShipmentId,
         case when osi.OutboundShipmentId is null then i.IssueId else null end,
         i.WarehouseId,
         sub.StorageUnitId,
         SUM(il.Quantity)
    from Issue      i (nolock)
    left
    join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
    join IssueLine il (nolock) on i.IssueId = il.IssueId
    join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
    join Status     s (nolock) on i.StatusId = s.StatusId
   where s.StatusCode = 'W'
  group by osi.OutboundShipmentId,
           case when osi.OutboundShipmentId is null then i.IssueId else null end,
           i.WarehouseId,
           sub.StorageUnitId

  update tr
     set AllocatedQuantity = a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId

  update tr
     set WavePickQuantity = case when a.ActualQuantity - a.ReservedQuantity < 0
                                      then 0
                                      else a.ActualQuantity - a.ReservedQuantity
                                      end
    from @TableResult tr
    join viewAvailablePickfaces a on tr.WarehouseId   = a.WarehouseId -- CHanged from viewAvailable to viewAvailablePickfaces for Moresport
                                 and a.AreaType       = ''
                                 and tr.StorageUnitId = a.StorageUnitId

  update tr
     set NarrowAisle = a.ActualQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = 'backup'
                        and tr.StorageUnitId = a.StorageUnitId

  update tr
     set ReplenishmentQuantity = a.AllocatedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId

  update tr
     set AvailableQuantity = WavePickQuantity + NarrowAisle
    from @TableResult tr

  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null

  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), (WavePickQuantity - AllocatedQuantity) + isnull(ReplenishmentQuantity,0)) / convert(numeric(13,3), RequiredQuantity)) * 100
   where RequiredQuantity > 0

  update @TableResult
     set AvailablePercentage = 100
   where AvailablePercentage > 100

  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0

  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0

  --update @TableResult
  --   set NarrowAisle = 

  update @TableResult
     set DrawDownPercentage = (convert(numeric(13,3), (NarrowAisle)) / convert(numeric(13,3), RequiredQuantity)) * 100
   where RequiredQuantity > 0
     and AvailablePercentage < 100

  update @TableResult
     set DrawDownPercentage = 100
   where DrawDownPercentage > 100

  update @TableResult
     set DrawDownPercentage = 0
   where DrawDownPercentage < 0

  update @TableResult
     set DrawDownPercentage = DrawDownPercentage - AvailablePercentage
   where DrawDownPercentage = 100
  
  declare @TablePallets as table
  (
   StorageUnitId  int,
   PalletQuantity numeric(13,6)
  )
  
  insert @TablePallets 
        (StorageUnitId,
         PalletQuantity)
  select sub.StorageUnitId, AVG(ActualQuantity) as 'PalletQuantity'
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join AreaLocation               al (nolock) on subl.LocationId = al.Locationid
    join Area                        a (nolock) on al.AreaId = a.AreaId
                                               and a.AreaType = 'Backup'
  group by StorageUnitId
  
  update tr
     set PalletQuantity = tp.PalletQuantity
   from @TableResult tr
   join @TablePallets tp on tr.StorageUnitId = tp.StorageUnitId
  
  update @TableResult
     set DrawDownQuantity = case when RequiredQuantity > WavePickQuantity
                                 then RequiredQuantity - WavePickQuantity
                                 else 0
                                 end
  
  update @TableResult
     set DrawDownPallets = FLOOR(DrawDownQuantity/PalletQuantity)
   where DrawDownQuantity > 0 and PalletQuantity > 0
  
  update @TableResult set DrawDownPallets = 1 where DrawDownQuantity > 0 and DrawDownPallets = 0
  
  update @TableResult set DrawDownPallets = 0 where DrawDownPallets is null
  
  -- Update SimilarSKUDI
  update wa
     set OrderNumber = os.ReferenceNumber
    from @TableResult     wa
    join OutboundShipment os (nolock) on wa.OutboundShipmentId = os.OutboundShipmentId
  
  update wa
     set OrderNumber = od.OrderNumber
    from @TableResult     wa
    join Issue             i (nolock) on wa.IssueId = i.IssueId
    join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
  
  declare @StorageUnitId int = 0,
          @OrderNumber   nvarchar(30) = ''
  
  update tr
     set SKUCount = (select COUNT(1)
                       from @TableResult tr2
                      where tr.StorageUnitId = tr2.StorageUnitId 
                     group by StorageUnitId)
    from @TableResult tr
  
  while @StorageUnitId is not null
  begin
    select @StorageUnitId = min(StorageUnitId)
      from @TableResult
     where StorageUnitId > ISNULL(@StorageUnitId,0)
       and SKUCount > 1
    
    if @@ROWCOUNT = 0
      set @StorageUnitId = null
      
    print @StorageUnitId
    set @OrderNumber = ''
    
    while @OrderNumber is not null and @StorageUnitId is not null
    begin
      select @OrderNumber = min(OrderNumber)
        from @TableResult
       where StorageUnitId = @StorageUnitId
         and OrderNumber > @OrderNumber
      
      if @@ROWCOUNT = 0
        set @OrderNumber = null
      else
        update @TableResult
           set SimilarSKUDI = isnull(SimilarSKUDI,'') + @OrderNumber + ','
         where StorageUnitId = @StorageUnitId
           and OrderNumber != @OrderNumber
     end
  end

  insert WaveAvailablity
        (OutboundShipmentId,
         IssueId,
         NumberOfLines,
         PercentageAvailable,
         PercentageDrawDown,
         BulkPallets,
         SimilarSKUDI)
  select OutboundShipmentId,
         IssueId,
         count(distinct(StorageUnitId)),
         sum(AvailablePercentage) / COUNT(1),
         sum(DrawDownPercentage) / COUNT(1),
         sum(DrawDownPallets),
         SimilarSKUDI
    from @TableResult
  group by OutboundShipmentId,
           IssueId,
           SimilarSKUDI
  
  update WaveAvailablity
     set PercentageDrawDown = PercentageDrawDown - ((PercentageAvailable + PercentageDrawDown) - 100)
   where PercentageAvailable + PercentageDrawDown > 100
end
