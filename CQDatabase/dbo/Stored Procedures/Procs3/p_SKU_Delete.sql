﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKU_Delete
  ///   Filename       : p_SKU_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:06
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the SKU table.
  /// </remarks>
  /// <param>
  ///   @SKUId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKU_Delete
(
 @SKUId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete SKU
     where SKUId = @SKUId
  
  select @Error = @@Error
  
  
  return @Error
  
end
