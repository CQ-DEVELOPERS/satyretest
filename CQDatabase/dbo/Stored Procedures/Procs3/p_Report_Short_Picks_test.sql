﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Short_Picks_test
  ///   Filename       : p_Report_Short_Picks_test.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Short_Picks_test
(
 @WarehouseId		     	int = -1,
 @ExternalCompanyId  int = -1,
 @OrderNumber        nvarchar(30),
 @StorageUnitId      int,
 @BatchId            int,
 @FromDate           datetime,
 @ToDate             datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
	  WarehouseId        int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(255),
   StorageUnitId      int,
   BatchId            int,
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(255),
   EndDate            datetime,
   SKUCode            nvarchar(10),
   Batch              nvarchar(50),
   Quantity           int,
   ConfirmedQuantity  int,
   ShortQuantity      int,
   QuantityOnHand     int,
   OperatorId         int,
   Operator           nvarchar(50)
  )
  
  if @WarehouseId = -1
    set @WarehouseId = null
  
  if @ExternalCompanyId = -1
    set @ExternalCompanyId = null
  
  if @StorageUnitId = -1
    set @StorageUnitId = null
  
  if @BatchId = -1
    set @BatchId = null
  
  if @OrderNumber = ''
    set @OrderNumber = null
  
  insert @TableResult
        (WarehouseId,
         StorageUnitBatchId,
         EndDate,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         OperatorId,
         OrderNumber,
         ExternalCompanyId)
  select ins.WarehouseId,
         ins.StorageUnitBatchId,
         isnull(ins.EndDate, ins.CreateDate),
         ili.Quantity,
         isnull(ili.ConfirmedQuantity,0),
         ili.Quantity - isnull(ili.ConfirmedQuantity,0),
         isnull(ins.OperatorId, j.OperatorId),
         od.OrderNumber,
         od.ExternalCompanyId
    from IssueLineInstruction ili (nolock)
    join Instruction          ins (nolock) on ili.InstructionId      = ins.InstructionId
    join Job                    j (nolock) on ins.JobId              = j.JobId
    join Status                 s (nolock) on ins.StatusId           = s.StatusId
    join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
   where ins.WarehouseId      = @WarehouseId
     and od.OrderNumber    like isnull(@OrderNumber,od.OrderNumber)
     and od.ExternalCompanyId = isnull(@ExternalCompanyId, od.ExternalCompanyId)
     and isnull(ins.EndDate, ins.CreateDate) between @FromDate and @ToDate
     and (ili.Quantity - isnull(ili.ConfirmedQuantity,0)) > 0
     and s.StatusCode in ('F','NS')
  
  update tr
     set ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set StorageUnitId = su.StorageUnitId,
         BatchId       = b.BatchId,
         ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch
    from @TableResult    tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
  
  --update tr
  --   set QuantityOnHand = (select sum(ActualQuantity)
  --                           from viewSOH     soh where soh.StorageUnitBatchId = tr.StorageUnitBatchId
  --                                                  and soh.WarehouseId = tr.WarehouseId
  --                                                  and soh.AreaCode in ('PK','RK','BK','SP'))
  --  from @TableResult tr
  
  
  select OrderNumber,
         ExternalCompany,
         ProductCode,
         Product,
         EndDate,
         SKUCode,
         Batch,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         QuantityOnHand,
         Operator
    from @TableResult
   where StorageUnitId = isnull(@StorageUnitId, StorageUnitId)
     and BatchId       = isnull(@BatchId, BatchId)
  order by ProductCode,
           OrderNumber
end
