﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_By_Type
  ///   Filename       : p_Status_By_Type.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_By_Type
(
 @Type       char(2),
 @StatusCode nvarchar(10) = null
)

as
begin
	 set nocount on;
  
  if @Type in ('PR','W') and @StatusCode is null
  begin
    select '-1'    as 'StatusId',
           '{All}' as 'Status'
    union
    select StatusId,
           Status
      from Status (nolock)
     where Type            = @Type
       and StatusCode      = isnull(@StatusCode, StatusCode)
       and StatusCode not in ('D','NS')
    order by Status
  end
  else
  begin
    select StatusId,
           Status
      from Status (nolock)
     where Type            = @Type
       and StatusCode      = isnull(@StatusCode, StatusCode)
       and StatusCode not in ('D','NS')
    order by Status
  end
end
