﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_OperatorSpecific_Performance_Total
  ///   Filename       : p_Report_OperatorSpecific_Performance_Total.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 14 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/

CREATE procedure p_Report_OperatorSpecific_Performance_Total
(
	@StartDate				DateTime,
	@Enddate				DateTime,
	@OperatorGroupId		int,
	@OperatorId				int = null,
	@UOM					nvarchar(15) = 'Units'
)

as
begin

if (@OperatorId = -1)
	Begin	
Set @OperatorId = Null
	End

	 set nocount on;
	
	Declare @TempTable As Table
(
	OperatorGroupId int,
	OperatorId		int,
	EndDate			dateTime,
	Actual			int,
	SiteTar			int,
	Industry		int,
	Measure			nvarchar(10)	
)

Declare @DateHoursDiff		int,
		@DateDaysDiff		int,
		@DateMonthsDiff		int

Select 
 @DateHoursDiff = DateDiff(hh,@StartDate,@EndDate)
,@DateDaysDiff = DateDiff(D,@StartDate,@EndDate)
,@DateMonthsDiff = DateDiff(M,@StartDate,@EndDate)

if @UOM = 'Units'
  begin

					Insert Into @TempTable (Actual,Enddate,OperatorGroupId,OperatorId)
					(
					SELECT  Sum(OP.Units) as Units, OP.EndDate ,O.OperatorGroupId, O.OperatorId 
					FROM         OperatorPerformance OP INNER JOIN
										  Operator O ON OP.OperatorId = O.OperatorId
					WHERE OP.EndDate between @StartDate and @EndDate
					And O.OperatorGroupId = @OperatorGroupId
					And O.OperatorId = isnull(@OperatorID,O.OperatorId)
					Group BY  O.OperatorGroupId,O.OperatorId,EndDate
					) 

					Update @TempTable Set Industry = (Select Distinct(Units) 
					From GroupPerformance
					WHERE GroupPerformance.PerformanceType = 'I' and OperatorGroupId = @OperatorGroupId),
					SiteTar = (Select Distinct(Units) From GroupPerformance
					Where GroupPerformance.PerformanceType = 'S' and OperatorGroupId = @OperatorGroupId),
					Measure = 'Pieces'
					

					--Where OperatorGroupId = @OperatorGroupId
    
  end
   else if @UOM = 'Weight'
  
	begin
					Insert Into @TempTable (Actual,Enddate,OperatorGroupId,OperatorId)
					(
					SELECT  Sum(OP.Weight) as Weight, OP.EndDate ,O.OperatorGroupId, O.OperatorId 
					FROM         OperatorPerformance OP INNER JOIN
										  Operator O ON OP.OperatorId = O.OperatorId
					WHERE OP.EndDate between @StartDate and @EndDate
					And O.OperatorGroupId = @OperatorGroupId
					And O.OperatorId = isnull(@OperatorID,O.OperatorId)
					Group BY  O.OperatorGroupId,O.OperatorId,EndDate
					) 

					Update @TempTable Set Industry = (Select Distinct(Weight) 
					From GroupPerformance
					WHERE GroupPerformance.PerformanceType = 'I' and OperatorGroupId = @OperatorGroupId),
					SiteTar = (Select Distinct(Weight) From GroupPerformance
					Where GroupPerformance.PerformanceType = 'S' and OperatorGroupId = @OperatorGroupId),
					Measure = 'KG'
					


	end
 else if @UOM = 'Jobs'
  
	begin

					Insert Into @TempTable (Actual,Enddate,OperatorGroupId,OperatorId)
					(
					SELECT  Sum(OP.Jobs) as Jobs, OP.EndDate ,O.OperatorGroupId, O.OperatorId 
					FROM         OperatorPerformance OP INNER JOIN
										  Operator O ON OP.OperatorId = O.OperatorId
					WHERE OP.EndDate between @StartDate and @EndDate
					And O.OperatorGroupId = @OperatorGroupId
					And O.OperatorId = isnull(@OperatorID,O.OperatorId)
					Group BY  O.OperatorGroupId,O.OperatorId,EndDate
					)
					Update @TempTable Set Industry = (Select Distinct(Jobs) 
					From GroupPerformance
					WHERE GroupPerformance.PerformanceType = 'I' and OperatorGroupId = @OperatorGroupId),
					SiteTar = (Select Distinct(Jobs) From GroupPerformance
					Where GroupPerformance.PerformanceType = 'S' and OperatorGroupId = @OperatorGroupId),
					Measure = 'Jobs'
      

	end

else if @UOM = 'Instructions'
  
	begin

					Insert Into @TempTable (Actual,Enddate,OperatorGroupId,OperatorId)
					(
					SELECT  Sum(OP.Instructions) as Instructions, OP.EndDate ,O.OperatorGroupId, O.OperatorId 
					FROM         OperatorPerformance OP INNER JOIN
										  Operator O ON OP.OperatorId = O.OperatorId
					WHERE OP.EndDate between @StartDate and @EndDate
					And O.OperatorGroupId = @OperatorGroupId
					And O.OperatorId = isnull(@OperatorID,O.OperatorId)
					Group BY  O.OperatorGroupId,O.OperatorId,EndDate
					)
					Update @TempTable Set Industry = (Select Distinct(Instructions) 
					From GroupPerformance
					WHERE GroupPerformance.PerformanceType = 'I' and OperatorGroupId = @OperatorGroupId),
					SiteTar = (Select Distinct(Instructions) From GroupPerformance
					Where GroupPerformance.PerformanceType = 'S' and OperatorGroupId = @OperatorGroupId),
					Measure = 'Pick Lines'
      

	end


if (@DateMonthsDiff < 3)
	Begin
		if (@DateDaysDiff < 8)		
			Begin
				if (@DateHoursDiff < 25)
					Begin
						--Select 'x < 24 Hours' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
 					
Select	T.OperatorGroupId, 
		T.OperatorId,
		O.Operator,
		Count(Distinct(T.OperatorId)) as ActiveOperators,
		Convert(nvarchar,T.EndDate,108) as EndDate, 
		Convert(nvarchar,T.EndDate,108) as ordscale, 
		Sum(T.Actual) As Actual,			
		Sum(T.SiteTar) As SiteTar,			
		Sum(T.Industry) As Industry,
		T.Measure
		From @TempTable T	
		Inner Join Operator O ON O.OperatorId = T.OperatorId 
	Group by T.OperatorGroupID,T.OperatorID,O.Operator,Convert(nvarchar,T.EndDate,108),T.Measure
	ORDER BY ordscale

						
						-- Select data by this Scale
					End
				Else
					Begin
					--Select '24 Hours < x < 1 week' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
Select  T.OperatorGroupId, 
		T.OperatorId,
		O.Operator,
		Count(Distinct(T.OperatorId)) as ActiveOperators,
		Convert(nvarchar,T.EndDate,101) as EndDate, 
		Convert(nvarchar,T.EndDate,101) as ordscale, 
		Sum(T.Actual) As Actual,			
		(8 * Sum(Distinct(T.SiteTar))) As SiteTar,			
		(8 * Sum(Distinct(T.Industry))) As Industry,
		Measure
		From @TempTable T
		Inner Join Operator O ON O.OperatorId = T.OperatorId 
		Group by T.OperatorGroupID,T.OperatorID,O.Operator,Convert(nvarchar,T.EndDate,101),T.Measure
		ORDER BY ordscale						
						-- Select data by this Scale
					End
			End
			Else
				Begin
					--Select '1 Week< x < 3 months' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
Select	T.OperatorGroupId, 
		T.OperatorId,
		O.Operator,
		Count(Distinct(T.OperatorId)) as ActiveOperators,
		DatePart(wk,T.EndDate) as EndDate, 
		DatePart(wk,T.EndDate) as ordscale, 
		Sum(T.Actual) As Actual,			
		5 * (8 * Sum(Distinct(T.SiteTar))) As SiteTar,			
		5 * (8 * Sum(Distinct(T.Industry))) As Industry,
		T.Measure
		From @TempTable T
		Inner Join Operator O ON O.OperatorId = T.OperatorId 
							Group by T.OperatorGroupID,T.OperatorID,O.Operator,DatePart(wk,T.EndDate),T.Measure
							ORDER BY ordscale		
				
					-- Select data by this Scale
				End
	End
Else
	Begin
			--Select  '3 months < x' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
				
Select	T.OperatorGroupId, 
		T.OperatorId,
		O.Operator,
		Count(Distinct(T.OperatorId)) as ActiveOperators,
		DateName(M,T.EndDate) as EndDate, 
		DatePart(M,T.EndDate) as ordscale, 
		Sum((T.Actual)) As Actual,			
		22 * (8 * Sum(Distinct(T.SiteTar))) As SiteTar,			
		22 * (8 * Sum(Distinct(T.Industry))) As Industry,
		T.Measure
		From @TempTable	T
		Inner Join Operator O ON O.OperatorId = T.OperatorId 
		Group by T.OperatorGroupId,T.OperatorID,O.Operator,DatePart(M,T.EndDate),DateName(M,T.EndDate),T.Measure
		ORDER BY ordscale

	End


end	   		
