﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTake_Insert
  ///   Filename       : p_StockTake_Insert.sql
  ///   Create By      : Karen
  ///   Date Created   : Jul 2011
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StockTakeReference table.
  /// </remarks>
  /// <param>

  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTake_Insert
(
 @connectionStringName		nvarchar(255) = null,
 @WarehouseId				       int = null,
 @StockTakeType				     nvarchar(50) = null,
 @StartDate					        datetime = null,
 @EndDate					          datetime = null,
 @OperatorId					       int = null,
 @StockTakeReferenceId		int = null Output,
 @Reference				     nvarchar(50) = null
)

as
begin
	 set nocount on;
  
  declare @Error nvarchar(255)
  
  select @StockTakeReferenceId = StockTakeReferenceId
    from StockTakeReference (nolock)
   where WarehouseId = @WarehouseId
     and EndDate    is null
  
  IF @StockTakeReferenceId IS NULL
  begin
	  insert	StockTakeReference
			      (WarehouseId,
				      StockTakeType,
				      StartDate,
				      --EndDate,
				      OperatorId,
				      Reference)
	  select	@WarehouseId,
				      @StockTakeType,
				      @StartDate,
				      --@EndDate,
				      @OperatorId,
				      @Reference
	  
	  select @Error = ERROR_MESSAGE(), @StockTakeReferenceId = scope_identity()
  end
  else
  begin
	   select @Error = 'Stock take already open for warehouse'
  end
  
  select @Error
end

 
