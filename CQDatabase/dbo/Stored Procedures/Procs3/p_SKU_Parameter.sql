﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKU_Parameter
  ///   Filename       : p_SKU_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the SKU table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   SKU.SKUId,
  ///   SKU.SKU 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKU_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as SKUId
        ,'{All}' as SKU
  union
  select
         SKU.SKUId
        ,SKU.SKU
    from SKU
  
end
