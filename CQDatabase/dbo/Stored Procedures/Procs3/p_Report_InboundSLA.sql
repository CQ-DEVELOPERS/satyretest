﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_InboundSLA
  ///   Filename       : p_Report_InboundSLA.sql
  ///   Create By      : Karen
  ///   Date Created   : March 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_InboundSLA
(
 @FromDate					datetime,
 @ToDate					datetime,
 @PrincipalId				int,
 @SLAStartDate				nvarchar(1),	-- 1=Trailer check in date
											-- 2=First Receipt Date
 @WeekendType				int,			-- 1=Friday
											-- 2=Friday & Saturday
											-- 3=Saturday Only
											-- 4=Saturday & Sunday
											-- 5=Sunday Only
 @ReceivingThreshold		int = null,
 @PutAwayThreshold			int = null,
 @ASNReceiptCloseThreshold	int = null
)


as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   CreateDate				    date,
   PrincipalId					int,
   OrderNumber					nvarchar(30),
   ExpectedDeliveryDate			datetime,
   StatusId						int,
   Status					    nvarchar(50),
   ReceiptLineId				int,
   DeliveryDate					date,
   HoursToReceive				decimal(11,2),
   HoursToPutaway				decimal(11,2),
   PutawayStarted				datetime,
   PutawayEnded					datetime,
   ReceivedDate					datetime
  )
  
  declare @TableDetail as table
  (
   PrincipalId					int,
   PrincipalCode				nvarchar(50),
   CreateDate					date,
   TotalReceipts				int,
   TRMeasured					int,
   TRLines						int,
   TRLinesReceivedSLA			int,
   TRPutawaysSLA				int,
   AVGHoursToReceive			decimal(11,2),
   AVGHoursToPutaway			decimal(11,2),
   PctLinesReceivedInSLA		decimal(11,2),
   PctLinesPutawayInSLA			decimal(11,2)
  )
  
  if @PrincipalId = -1
	set @PrincipalId = null
  
  insert @TableHeader
        (CreateDate,
         PrincipalId,
         OrderNumber,
         ExpectedDeliveryDate,
         StatusId,
         ReceiptLineId,
         DeliveryDate,
         PutawayStarted,
         PutawayEnded,
         ReceivedDate
         )
  SELECT CreateDate,
		 id.PrincipalId,
		 id.OrderNumber,
		 id.DeliveryDate,
		 r.StatusId,
		 rl.ReceiptLineId,
		 r.DeliveryDate,
		 rl.PutawayStarted,
		 rl.PutawayEnded,
		 rl.ReceivedDate
    FROM inboundDocument id (nolock)
    join Receipt r (nolock) on id.inboundDocumentId = r.inboundDocumentId
    join ReceiptLine rl (nolock) on r.ReceiptId = rl.ReceiptId
   WHERE id.CreateDate between @FromDate and @ToDate
   and id.PrincipalId = isnull(@PrincipalId,id.PrincipalId)
  -- group by 
		--CreateDate,
		--PrincipalId,
		--OrderNumber,
		--id.DeliveryDate,
		--r.StatusId,
		--r.DeliveryDate,
		--rl.ReceiptLineId
		
  update @TableHeader
	 set HoursToReceive = (select dbo.ufn_Hours_Between_Dates(CreateDate, ReceivedDate, 0,0)),
		 HoursToPutaway = (select dbo.ufn_Hours_Between_Dates(CreateDate, PutawayEnded, 0,0))
    from @TableHeader th 
        
  insert @TableDetail
        (PrincipalId,
         CreateDate)
  SELECT distinct PrincipalId,
		 CreateDate
    FROM @TableHeader th
    
    
  update @TableDetail 
	 set TotalReceipts = (select COUNT(distinct OrderNumber) 
						  from @TableHeader th
						 where td.PrincipalId = th.PrincipalId )
						 --  and th.CreateDate = td.CreateDate)
    from @TableDetail td 
  
   
  update @TableDetail 
	 set TRMeasured = (select COUNT(distinct OrderNumber) 
						  from @TableHeader th 
						   where  th.StatusId != dbo.ufn_StatusId('R','W')
						   and td.PrincipalId = th.PrincipalId )
    from @TableDetail td 
    
  update @TableDetail 
	 set TRLines = (select COUNT(th.ReceiptLineId) 
						  from @TableHeader th
						  where td.PrincipalId = th.PrincipalId  )
    from @TableDetail td
    
   
  update @TableDetail 
	 set AVGHoursToReceive = (select sum(HoursToReceive) 
								 from @TableHeader th
								 where td.PrincipalId = th.PrincipalId) / 
						      (td.TRLines)
    from @TableDetail td
   
   	SET ARITHABORT OFF
	SET ANSI_WARNINGS OFF 
	 
   update @TableDetail 
	 set AVGHoursToReceive = (select sum(HoursToPutaway) 
								 from @TableHeader th
								 where td.PrincipalId = th.PrincipalId ) / 
						      (td.TRLines)
    from @TableDetail td
  
  update @TableDetail 
	 set TRLinesReceivedSLA = (select COUNT(ReceiptLineId) 
							     from @TableHeader th
							    where th.ReceivedDate is not null
							    and td.PrincipalId = th.PrincipalId 
							      and HoursToReceive <= @ReceivingThreshold)
    from @TableDetail td
  
  update @TableDetail 
	 set TRPutawaysSLA = (select COUNT(ReceiptLineId) 
							     from @TableHeader th
							    where th.PutawayStarted is not null
							    and td.PrincipalId = th.PrincipalId 
							      and HoursToPutaway <= @PutAwayThreshold)
    from @TableDetail td  
   
  update @TableDetail 
	 set PctLinesReceivedInSLA = ((td.TRLinesReceivedSLA * 100) / td.TRLines)
    from @TableDetail td  
    
  update @TableDetail 
	 set PctLinesPutawayInSLA = ((td.TRPutawaysSLA * 100) / td.TRLines)
    from @TableDetail td 
             
  update  th
      set th.PrincipalCode = p.PrincipalCode
     from @TableDetail th
     join Principal p  (nolock) on p.PrincipalId = th.PrincipalId
  
  select *
    from @TableDetail
    --where TotalReceipts > 0

  
end
