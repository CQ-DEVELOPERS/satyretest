﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Result_Parameter
  ///   Filename       : p_Result_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:09
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Result table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Result.ResultId,
  ///   Result.Result 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Result_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as ResultId
        ,'{All}' as Result
  union
  select
         Result.ResultId
        ,Result.Result
    from Result
  order by Result
  
end
