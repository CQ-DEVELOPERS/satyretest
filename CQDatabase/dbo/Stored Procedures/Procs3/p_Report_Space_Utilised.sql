﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Space_Utilised
  ///   Filename       : p_Report_Space_Utilised.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Space_Utilised
(
 @ConnectionString nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId      int,
 @AreaId           int
)

as
begin
	 set nocount on;
  
  if @AreaId = -1
    set @AreaId = null
  
  select a.Area,
         case when l.Used = 0
              then 'Free Space'
              else 'Used'
              end as 'Type',
         count(1) as 'Value'
    from Area          a (nolock)
    join AreaLocation al (nolock) on a.AreaId = al.AreaId
    join Location      l (nolock) on al.LocationId = l.LocationId
    where a.WarehouseId = @WarehouseId
      and a.AreaId      = isnull(@AreaId, a.AreaId)
      and a.StockOnHand = 1
  group by a.Area,
           case when l.Used = 0
                then 'Free Space'
                else 'Used'
                end
  order by a.Area,
           case when l.Used = 0
                then 'Free Space'
                else 'Used'
                end desc
end
