﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_OTIF
  ///   Filename       : p_Report_OTIF.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_OTIF
(
 @WarehouseId      int,
 @FromDate         datetime,
 @ToDate           datetime
)

as
begin
	 set nocount on;
  
  declare @TableTemp as table
  (
   OutboundDocumentTypeId int,
   OutboundDocumentType   nvarchar(30),
   OutboundDocumentId     int,
   IssueId                int,
   IssueLineId            int,
   JobId                  int,
   InstructionId          int,
   StorageUnitBatchId     int,
   StorageUnitId          int,
   ProductCode            nvarchar(30),
   Product                nvarchar(50),
   SKUCode                nvarchar(50),
   Quantity               float,
   ConfirmedQuantity      float,
   Weight                 numeric(13,2),
   ConfirmedWeight        numeric(13,2),
   Volume                 numeric(13,2),
   ConfirmedVolume        numeric(13,2),
   CreateDate             datetime,
   Release                datetime,
   StartDate              datetime,
   EndDate                datetime,
   Checking               datetime,
   Despatch               datetime,
   DespatchChecked        datetime,
   Complete               datetime,
   DeliveryDate           datetime,
   StatusCode             nvarchar(10)
  )
  
  declare @TableResult as table
  (
   [Group]                nvarchar(255),
   Header                 nvarchar(255),
   OutboundDocumentType   nvarchar(30),
   Orders                 numeric(13,2),
   Lines                  numeric(13,2),
   Pieces                 numeric(13,2),
   Weight                 numeric(13,2)
  )
  
  declare @Config int,
		  @StartDate	datetime
  
  select @StartDate = CURRENT_TIMESTAMP
  
  select @StartDate = DATEADD (DD , -15 , @FromDate )
  

  select @Config = convert(int, Value)
    from Configuration (nolock)
   where WarehouseId = @WarehouseId
     and ConfigurationId = 191
  
  if @Config is null
    set @Config = 7
  
  insert @TableTemp
        (OutboundDocumentTypeId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         JobId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         CreateDate,
         Release,
         StartDate,
         EndDate,
         Checking,
         Despatch,
         DespatchChecked,
         Complete)
  select ili.OutboundDocumentTypeId,
         ili.OutboundDocumentId,
         ili.IssueId,
         ili.IssueLineId,
         op.JobId,
         i.InstructionId,
         i.StorageUnitBatchId,
         ili.Quantity,
         ili.ConfirmedQuantity,
         op.CreateDate,
         op.Release,
         op.StartDate,
         op.EndDate,
         op.Checking,
         op.Despatch,
         op.DespatchChecked,
         op.Complete
    from OutboundPerformance   op (nolock)
    join Instruction            i (nolock) on op.JobId = i.JobId
    join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
   where (
		----op.CreateDate between @FromDate and @ToDate
      ----or  op.Complete        >= @FromDate -- between @FromDate and @ToDate
      ----or  op.Complete        is null
      op.StartDate between @FromDate and @ToDate
      )
      --and op.CreateDate <= @ToDate -- GS 2011-02-09 Replaced with line below
      ----and op.CreateDate between dateadd(dd,-@Config, @FromDate) and @ToDate
      and op.StartDate between dateadd(dd,-@Config, @FromDate) and @ToDate
	  and i.warehouseId = @WarehouseId
  
update tr
     set ConfirmedQuantity = 0,
         StatusCode        = s.StatusCode
    from @TableTemp tr
    join Job           j (nolock) on tr.JobId = j.JobId and j.warehouseId = @WarehouseId
    join Status        s (nolock) on j.StatusId = s.StatusId
   where s.StatusCode = 'NS'
  
--  update tr
--     set StatusCode        = 'NS'
--    from @TableTemp tr
--   where isnull(StatusCode,'') != 'NS'
--     and isnull(ConfirmedQuantity,0) = 0
  
  update tr
     set CreateDate = od.CreateDate
    from @TableTemp     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId and od.warehouseId = @WarehouseId
  
  update tr
     set OutboundDocumentType = odt.OutboundDocumentType
    from @TableTemp          tr
    join OutboundDocumentType odt (nolock) on tr.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  
  update tr
     set DeliveryDate = i.DeliveryDate
    from @TableTemp tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId and i.warehouseId =  @WarehouseId
  
  update tr
     set StorageUnitId = sub.StorageUnitId
    from @TableTemp        tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
  
  update tr
     set ProductCode     = p.ProductCode,
         Product         = p.Product,
         SKUCode         = sku.SKUCode
    from @TableTemp        tr
    join StorageUnit       su (nolock) on tr.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  update tr
     set Weight          = pk.Weight * tr.Quantity,
         ConfirmedWeight = pk.Weight * tr.ConfirmedQuantity,
         Volume          = pk.Volume * tr.Quantity,
         ConfirmedVolume = pk.Volume * tr.ConfirmedQuantity
    from @TableTemp        tr
    join Pack              pk on tr.StorageUnitId = pk.StorageUnitId
   where pk.PackTypeId   = 2
     and pk.WarehouseId  = @WarehouseId
  
  update @TableTemp
     set ConfirmedQuantity = 0
   where ConfirmedQuantity is null
  
  update @TableTemp
     set ConfirmedWeight = 0
   where ConfirmedWeight is null
  
  update @TableTemp
     set ConfirmedVolume = 0
   where ConfirmedVolume is null
  
  insert @TableResult
        ([Group],
         Header,
         OutboundDocumentType,
         Orders,
         Lines,
         Pieces,
         Weight)
  select 'Left',
         '1. OTIF (On time in full)',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableTemp
   where datediff(hh, DeliveryDate, Complete) <= 0
     and Complete between @FromDate and @ToDate
     and Quantity = ConfirmedQuantity
     and CreateDate > @StartDate
  group by OutboundDocumentType
  union
  select 'Left',
         '2. Late - Despatched',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableTemp
   where datediff(hh, DeliveryDate, Complete) > 0
     and Complete between @FromDate and @ToDate
     and CreateDate > @StartDate
  group by OutboundDocumentType
  union
  select 'Left',
         '3. Short Picks Summary',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(Quantity - ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableTemp
   where Complete between @FromDate and @ToDate
     and Quantity > ConfirmedQuantity
     and CreateDate > @StartDate
  group by OutboundDocumentType
  union
  select 'Right',
         '2. Late - Not Despatched'          as 'Header',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableTemp
   where DeliveryDate <= @ToDate
     and isnull(Complete, @ToDate) >= @ToDate -- Complete is null
     and CreateDate > @StartDate
  group by OutboundDocumentType
  union
  select 'Right',
         '3. On time (Short Picks)',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)),
         count(distinct(IssueLineId)),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableTemp
   where datediff(hh, DeliveryDate, Complete) <= 0
     and Complete between @FromDate and @ToDate
     and Quantity > ConfirmedQuantity
     and CreateDate > @StartDate
  group by OutboundDocumentType
  
  insert @TableResult
        ([Group],
         Header,
         OutboundDocumentType,
         Orders,
         Lines,
         Pieces,
         Weight)
  select 'Right',
         '1. Total workload (summary)',
         OutboundDocumentType,
         sum(Orders),
         sum(Lines),
         sum(Pieces),
         sum(Weight)
    from @TableResult
   where Header in ('1. OTIF (On time in full)','2. Late - Despatched','2. Late - Not Despatched','3. On time (Short Picks)')
  group by OutboundDocumentType
  
  insert @TableResult
        ([Group],
         Header,
         OutboundDocumentType,
         Orders,
         Lines,
         Pieces,
         Weight)
  select 'Left',
         '4. % Order fulfillment',
         'A. Total',
         sum(Orders),
         sum(Lines),
         sum(Pieces),
         sum(Weight)
    from @TableResult
   where Header = '1. Total workload (summary)'
  
  insert @TableResult
        ([Group],
         Header,
         OutboundDocumentType,
         Orders,
         Lines,
         Pieces,
         Weight)
  select 'Left',
         '4. % Order fulfillment',
         'B. Short',
         (select sum(Orders) from @TableResult where Header = '3. Short Picks Summary') / (select sum(Orders) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total') * 100,
         (select sum(Lines) from @TableResult where Header = '3. Short Picks Summary') / (select sum(Lines) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total') * 100,
         (select sum(Pieces) from @TableResult where Header = '3. Short Picks Summary') / (select sum(Pieces) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total') * 100,
         (select sum(Weight) from @TableResult where Header = '3. Short Picks Summary') / (select sum(Weight) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total') * 100
  
  insert @TableResult
        ([Group],
         Header,
         OutboundDocumentType,
         Orders,
         Lines,
         Pieces,
         Weight)
  select 'Left',
         '4. % Order fulfillment',
         'C. Full',
         (((select sum(Orders) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total') - (select sum(Orders) from @TableResult where Header = '3. Short Picks Summary')) / (select sum(Orders) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total')) * 100,
         (((select sum(Lines) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total') - (select sum(Lines) from @TableResult where Header = '3. Short Picks Summary')) / (select sum(Lines) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total')) * 100,
         (((select sum(Pieces) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total') - (select sum(Pieces) from @TableResult where Header = '3. Short Picks Summary')) / (select sum(Pieces) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total')) * 100,
         (((select sum(Weight) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total') - (select sum(Weight) from @TableResult where Header = '3. Short Picks Summary')) / (select sum(Weight) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'A. Total')) * 100
  
--  insert @TableResult
--        ([Group],
--         Header,
--         OutboundDocumentType,
--         Orders,
--         Lines,
--         Pieces,
--         Weight)
--  select 'Left',
--         '4. % Order fulfillment',
--         'D. Summary',
--         (select sum(Orders) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'B. Short') + (select sum(Orders) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'C. Full'),
--         (select sum(Lines) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'B. Short') + (select sum(Lines) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'C. Full'),
--         (select sum(Pieces) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'B. Short') + (select sum(Pieces) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'C. Full'),
--         (select sum(Weight) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'B. Short') + (select sum(Weight) from @TableResult where Header = '4. % Order fulfillment' and OutboundDocumentType = 'C. Full')
  
  insert @TableResult
        ([Group],
         Header,
         OutboundDocumentType,
         Orders,
         Lines,
         Pieces,
         Weight)
  select 'Right',
         '4. % Summary',
         t.OutboundDocumentType,
         (select sum(Orders) from @TableResult tr where tr.Header = '1. OTIF (On time in full)' and tr.OutboundDocumentType = t.OutboundDocumentType) / sum(Orders) * 100.000,
         (select sum(Orders) from @TableResult tr where tr.Header = '3. On time (Short Picks)' and tr.OutboundDocumentType = t.OutboundDocumentType) / sum(Orders) * 100.000,
         (select sum(Orders) from @TableResult tr where tr.Header = '2. Late - Despatched' and tr.OutboundDocumentType = t.OutboundDocumentType) / sum(Orders) * 100.000,
         (select sum(Orders) from @TableResult tr where tr.Header = '2. Late - Not Despatched' and tr.OutboundDocumentType = t.OutboundDocumentType) / sum(Orders) * 100.000
    from @TableResult t
   where t.Header = '1. Total workload (summary)'
  group by t.OutboundDocumentType
  
  insert @TableResult
        ([Group],
         Header,
         OutboundDocumentType,
         Orders,
         Lines,
         Pieces,
         Weight)
  select 'Right',
         '4. % Summary',
         'Total',
         (select sum(Orders) from @TableResult tr where tr.Header = '1. OTIF (On time in full)') / sum(Orders) * 100.000,
         (select sum(Orders) from @TableResult tr where tr.Header = '3. On time (Short Picks)') / sum(Orders) * 100.000,
         (select sum(Orders) from @TableResult tr where tr.Header = '2. Late - Despatched') / sum(Orders) * 100.000,
         (select sum(Orders) from @TableResult tr where tr.Header = '2. Late - Not Despatched') / sum(Orders) * 100.000
    from @TableResult t
   where t.Header = '1. Total workload (summary)'
  
  select [Group],
         Header,
         OutboundDocumentType,
         Orders,
         Lines,
         Pieces,
         Weight
    from @TableResult
  order by [Group], Header, OutboundDocumentType
end
