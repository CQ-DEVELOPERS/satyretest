﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitLocation_Update
  ///   Filename       : p_StorageUnitLocation_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:07
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StorageUnitLocation table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @LocationId int = null,
  ///   @MinimumQuantity float = null,
  ///   @HandlingQuantity float = null,
  ///   @MaximumQuantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitLocation_Update
(
 @StorageUnitId int = null,
 @LocationId int = null,
 @MinimumQuantity float = null,
 @HandlingQuantity float = null,
 @MaximumQuantity float = null 
)

as
begin
	 set nocount on;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
	 declare @Error int
 
  update StorageUnitLocation
     set MinimumQuantity = isnull(@MinimumQuantity, MinimumQuantity),
         HandlingQuantity = isnull(@HandlingQuantity, HandlingQuantity),
         MaximumQuantity = isnull(@MaximumQuantity, MaximumQuantity) 
   where StorageUnitId = @StorageUnitId
     and LocationId = @LocationId
  
  select @Error = @@Error
  
  
  return @Error
  
end
