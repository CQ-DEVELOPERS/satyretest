﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_WIP_Line_Update
  ///   Filename       : p_WIP_Line_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_WIP_Line_Update
(
 @IssueLineId        int,
 @StorageUnitBatchId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_IssueLine_Update
   @IssueLineId        = @IssueLineId,
   @StorageUnitBatchId = @StorageUnitBatchId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_WIP_Line_Update'); 
    rollback transaction
    return @Error
end
