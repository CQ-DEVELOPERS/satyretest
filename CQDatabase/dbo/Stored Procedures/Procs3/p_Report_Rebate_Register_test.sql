﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Rebate_Register_test
  ///   Filename       : p_Report_Rebate_Register_test.sql
  ///   Create By      : Karen
  ///   Date Created   : April 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Rebate_Register_test
(
 @FromDate          datetime,
 @ToDate            datetime
)

as
begin
	 set nocount on;
	 
	 declare @Weeks int,
	         @Count int,
	         @Date  datetime
	 

  
  declare @TableResult as table
		  (BOEDate				datetime,
		   BOE					nvarchar(255),
		   CreateDate			datetime,
		   ProductCode			nvarchar(50),
		   Product				nvarchar(255),
		   ContainerNumber		nvarchar(50),
		   SupplDeliveryNote	nvarchar(50),
		   AdditionalInfo		nvarchar(100),
		   DeliveryNote			nvarchar(50),
		   ArrivalDate			datetime,
		   ADORIssueNo			nvarchar(50),
		   Received				int,
		   Adjusted				int,
		   Issued				int,
		   Balance				int,
		   StockOnHand			int,
		   WarehouseId			int,
		   StorageUnitId		int,
		   SKUId				int,
		   ProductId			int
  )
  
  declare @TableResult2 as table
		  (
		   StartDate		date,
		   EndDate			date,
		   MovementDate		date,
		   ProductCode		varchar(30),
		   Product			varchar(50),
		   ContainerNumber  nvarchar(50),
		   Opening			int,
		   Received			int,
		   Issued			int,
		   Balance			int,
		   Variance			int,
		   Percentage		int,
		   StockOnHand		int,
		   WarehouseId		int,
		   StorageUnitId	int,
		   SKU  			varchar(50)
		  )
  
	declare @TableContainer as table
			(
			MovementDate		date,
			StorageUnitBatchId	int,
			StorageUnitId		int,
			WarehouseId			int,
			ContainerNumber		nvarchar(50),
			Received			float
			)
	  
	--if @ProductCode = '-1' 
	-- set @ProductCode = null
	 
	 
	--insert	@TableContainer
	--		(MovementDate,
	--		StorageUnitBatchId,
	--		StorageUnitId,
	--		WarehouseId,
	--		ContainerNumber,
	--		Received)
	--select	r.DeliveryDate,
	--		rl.StorageUnitBatchId,
	--		sub.StorageUnitId,
	--		r.WarehouseId,
	--		r.ContainerNumber,
	--		sum(rl.AcceptedQuantity)		   
	--from ReceiptLine        rl (nolock)  
	--join Receipt             r (nolock) on rl.ReceiptId = r.ReceiptId  
	--join status              s (nolock) on rl.statusid = s.statusid  
	--join StorageUnitBatch  sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId  
	--join StorageUnit        su (nolock) on sub.StorageUnitId = su.StorageUnitId
	--join Product			 p (nolock) on su.ProductId = p.ProductId
	--where s.statuscode = 'RC'  
	--and CONVERT(datetime, CONVERT(varchar,DeliveryDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  
	--and p.ProductCode = isnull(@ProductCode, p.ProductCode)  
	--and @WarehouseId   = r.WarehouseId
	--group by
	--		r.DeliveryDate,
	--		rl.StorageUnitBatchId,
	--		sub.StorageUnitId,
	--		r.WarehouseId,
	--		r.ContainerNumber

    
    insert @TableResult
          (BOEDate,
           BOE,
           WarehouseId,
           StorageUnitId,
           SKUId,
           ProductId,
           Received,
           ContainerNumber,
           DeliveryNote,
           CreateDate,
           ArrivalDate,
           ADORIssueNo)
    select r.DeliveryDate,
		   r.BOE,
           r.WarehouseId,
           sub.StorageUnitId,
           su.SKUId,
           su.ProductId,
           rl.AcceptedQuantity,
           r.ContainerNumber,
           r.DeliveryNoteNumber,
           id.CreateDate,
           r.DeliveryDate,
           id.OrderNumber
       from ReceiptLine        rl (nolock)         
	   join Receipt             r (nolock) on rl.ReceiptId = r.ReceiptId  
	   join InboundDocument    id (nolock) on r.InboundDocumentId = id.InboundDocumentId
	   join status              s (nolock) on rl.statusid = s.statusid  
	   join StorageUnitBatch  sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId  
	   join StorageUnit        su (nolock) on sub.StorageUnitId = su.StorageUnitId
      where s.StatusId = dbo.ufn_StatusId('R','RC')
	    and CONVERT(datetime, CONVERT(varchar,r.DeliveryDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  

	   
	 insert @TableResult
          (BOEDate,
           BOE,
           WarehouseId,
           StorageUnitId,
           SKUId,
           ProductId,
           Adjusted,
           --ContainerNumber,
           DeliveryNote,
           CreateDate)
    select od.CreateDate,
		   od.BOE,
           i.WarehouseId,
           sub.StorageUnitId,
           su.SKUId,
           su.ProductId,
           il.ConfirmedQuatity,
           --i.ContainerNumber,
           i.DeliveryNoteNumber,
           od.CreateDate
       from IssueLine          il (nolock)         
	   join Issue               i (nolock) on il.IssueId = i.IssueId  
	   join OutboundDocument   od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
	   join status              s (nolock) on il.statusid = s.statusid  
	   join StorageUnitBatch  sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId  
	   join StorageUnit        su (nolock) on sub.StorageUnitId = su.StorageUnitId
      where s.statusid in (dbo.ufn_StatusId('IS','DC'),dbo.ufn_StatusId('IS','D'))
      and CONVERT(datetime, CONVERT(varchar,i.DeliveryDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  
      
      
   insert @TableResult
          (BOEDate,
           BOE,
           WarehouseId,
           StorageUnitId,
           SKUId,
           ProductId,
           Received,
           --ContainerNumber,
           DeliveryNote,
           CreateDate)
  SELECT    null,
		    null, 
            i.WarehouseId, 
            sub.StorageUnitId, 
            SKUId,
            su.ProductId,
            i.ConfirmedQuantity,
			null,
			i.CreateDate
FROM        Instruction AS i WITH (nolock) 
INNER JOIN  InstructionType AS it WITH (nolock) ON i.InstructionTypeId = it.InstructionTypeId
	  join	Job                 j (nolock) on j.JobId = i.JobId
	  join  StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId 
	  join  StorageUnit su (nolock) on sub.StorageUnitId = su.StorageUnitId
     WHERE  it.InstructionTypeCode in ('STE','STL','STA','STP')
     and i.CreateDate   between @FromDate and @ToDate
     and i.ConfirmedQuantity > 0

	update tr
       set ProductCode = p.ProductCode,
		   Product = p.Product
	  from @TableResult tr
	  join Product p (nolock) on tr.ProductId = p.ProductId
	  
	update tr
       set Balance = isnull(tr.Received,0) + isnull(tr.Adjusted,0) - isnull(tr.Issued,0)
	  from @TableResult tr

	--update tr
 --      set Opening = (select sum(OpeningBalance) 
	--				  from ProductMovement pm 
	--				 where pm.WarehouseId = @WarehouseId 
	--				   and pm.productcode = tr.productcode 
	--				   and pm.MovementDate = tr.StartDate)
	--				   --and pm.MovementDate between tr.StartDate and tr.EndDate)
 --     from @TableResult tr  
    
   
	--update tr
 --      set Received = tc.Received
	--  from @TableResult tr
	--  join @TableContainer tc on tc.StorageUnitId = tr.StorageUnitId
	--  where tr.WarehouseId = tc.WarehouseId
	--    and tc.MovementDate = tr.MovementDate

         
	--update tr  
 --      set Received2 = (select sum(i.ConfirmedQuantity)  
 --                         from Instruction        i (nolock)  
 --                         join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId  
 --                        where it.InstructionTypeCode in ('S','SM','PR') -- Store, Store mixed, Production receipt  
	--					   and CONVERT(datetime, CONVERT(varchar,i.EndDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  
	--					   and tr.StorageUnitBatchId = i.StorageUnitBatchId
	--					   and tr.WarehouseId   = i.WarehouseId)  
 --     from @TableResult tr  
         
   
      
	--update tr
	--   set Issued = (select sum(isnull(IssuedQuantity,0)) 
	--				   from ProductMovement pm (nolock) 
	--				  where pm.WarehouseId = @WarehouseId 
	--				    and pm.productcode = tr.productcode 
	--				    and pm.MovementDate = tr.MovementDate) 
	--  from @TableResult tr
	-- where tr.ProductCode = isnull(@ProductCode, tr.ProductCode)
	--   and tr.WarehouseId = @WarehouseId
 

	--insert	@TableResult2
	--		(StartDate,
	--		EndDate,
	--		ProductCode,
	--		Product,
	--		ContainerNumber,
	--		Opening,
	--		Received,
	--		Issued,
	--		WarehouseId,
	--		SKU)
	-- select tr.StartDate,
	--		tr.EndDate,
	--		tr.ProductCode,
	--		p.Product,

  
  
SET ARITHABORT OFF
SET ANSI_WARNINGS OFF   

  

	select *		  
      from @TableResult tr
     order by tr.BOE, tr.BOEDate, tr.ProductCode


end 
