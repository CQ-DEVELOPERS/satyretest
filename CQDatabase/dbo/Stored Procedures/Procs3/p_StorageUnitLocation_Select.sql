﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitLocation_Select
  ///   Filename       : p_StorageUnitLocation_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:09
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitLocation table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @LocationId int = null 
  /// </param>
  /// <returns>
  ///   StorageUnitLocation.StorageUnitId,
  ///   StorageUnitLocation.LocationId,
  ///   StorageUnitLocation.MinimumQuantity,
  ///   StorageUnitLocation.HandlingQuantity,
  ///   StorageUnitLocation.MaximumQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitLocation_Select
(
 @StorageUnitId int = null,
 @LocationId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         StorageUnitLocation.StorageUnitId
        ,StorageUnitLocation.LocationId
        ,StorageUnitLocation.MinimumQuantity
        ,StorageUnitLocation.HandlingQuantity
        ,StorageUnitLocation.MaximumQuantity
    from StorageUnitLocation
   where isnull(StorageUnitLocation.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(StorageUnitLocation.StorageUnitId,'0'))
     and isnull(StorageUnitLocation.LocationId,'0')  = isnull(@LocationId, isnull(StorageUnitLocation.LocationId,'0'))
  
end
