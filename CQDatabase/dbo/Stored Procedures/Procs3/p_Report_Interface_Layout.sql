﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Interface_Layout
  ///   Filename       : p_Report_Interface_Layout.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Apr 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Interface_Layout
(
 @principalId            int,
 @interfaceMappingFileId int
)

as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   InterfaceFileType          nvarchar(50),
   InterfaceFileTypeCode      nvarchar(30),
   InterfaceDocumentType      nvarchar(50),
   HeaderRow                  int,
   FilePrefix                 nvarchar(30),
   InterfaceMappingColumnId   int,
   InterfaceMappingColumnCode nvarchar(100),
   InterfaceMappingColumn     nvarchar(100),
   StartPosition              int,
   EndPosition                int,
   FixedWidth                 bit,
   Format                     nvarchar(50),
   ColumnNumber               int,
   Datatype                   nvarchar(50),
   Mandatory                  bit,
   Delimiter                  nvarchar(10),
   SubDirectory               nvarchar(max)
  )
  
  if @principalId = -1
    set @principalId = null
  
  if @interfaceMappingFileId = -1
    set @interfaceMappingFileId = null
  
  insert @TableResult
        (InterfaceFileType,
         InterfaceFileTypeCode,
         InterfaceDocumentType,
         InterfaceMappingColumnId,
         InterfaceMappingColumnCode,
         InterfaceMappingColumn,
         StartPosition,
         EndPosition,
         FixedWidth,
         Format,
         ColumnNumber,
         Datatype,
         Mandatory,
         HeaderRow,
         FilePrefix,
         Delimiter,
         SubDirectory)
  select ift.InterfaceFileType,
         ift.InterfaceFileTypeCode,
         idt.InterfaceDocumentType,
         imc.InterfaceMappingColumnId,
         REPLACE(REPLACE(REPLACE(imc.InterfaceMappingColumnCode, 'Interface',''), 'Import',''), 'Export',''),
         imc.InterfaceMappingColumn,
         imc.StartPosition,
         imc.EndPostion,
         CONVERT(bit, case when ift.InterfaceFileTypeCode = 'FIX'
                           then 1
                           else 0
                           end),
         imc.Format,
         imc.ColumnNumber,
         ifd.Datatype,
         ifd.Mandatory,
         imf.HeaderRow,
         isnull(imf.FilePrefix, idt.FilePrefix),
         imf.Delimiter,
         imf.SubDirectory
    from InterfaceMappingColumn imc (nolock)
    join InterfaceField         ifd (nolock) on imc.InterfaceFieldId = ifd.InterfaceFieldId
    join InterfaceMappingFile   imf (nolock) on imc.InterfaceMappingFileId = imf.InterfaceMappingFileId
    join InterfaceFileType      ift (nolock) on imf.InterfaceFileTypeId = ift.InterfaceFileTypeId
    join InterfaceDocumentType  idt (nolock) on imf.InterfaceDocumentTypeId = idt.InterfaceDocumentTypeId
   where (imf.PrincipalId = @principalId or @principalId is null)
     and (imc.InterfaceMappingFileId = @interfaceMappingFileId or @interfaceMappingFileId is null)
  
  if (select COUNT(distinct(InterfaceMappingColumnCode)) from @TableResult where InterfaceMappingColumnCode != 'Place Holder') > 1
    update @TableResult
       set InterfaceMappingColumn = InterfaceMappingColumnCode + ' - ' + InterfaceMappingColumn
  
  select InterfaceMappingColumnId,
         InterfaceFileType,
         InterfaceFileTypeCode,
         InterfaceDocumentType,
         HeaderRow,
         FilePrefix,
         Delimiter,
         SubDirectory,
         ColumnNumber,
         InterfaceMappingColumn,
         StartPosition,
         EndPosition,
         FixedWidth,
         Format,
         Datatype,
         Mandatory
    from @TableResult
  order by InterfaceDocumentType, ColumnNumber
end
