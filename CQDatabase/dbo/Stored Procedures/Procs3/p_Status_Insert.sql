﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Insert
  ///   Filename       : p_Status_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:54
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Status table.
  /// </remarks>
  /// <param>
  ///   @StatusId int = null output,
  ///   @Status nvarchar(100) = null,
  ///   @StatusCode nvarchar(20) = null,
  ///   @Type char(2) = null,
  ///   @OrderBy int = null 
  /// </param>
  /// <returns>
  ///   Status.StatusId,
  ///   Status.Status,
  ///   Status.StatusCode,
  ///   Status.Type,
  ///   Status.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Insert
(
 @StatusId int = null output,
 @Status nvarchar(100) = null,
 @StatusCode nvarchar(20) = null,
 @Type char(2) = null,
 @OrderBy int = null 
)

as
begin
	 set nocount on;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @Status = '-1'
    set @Status = null;
  
  if @StatusCode = '-1'
    set @StatusCode = null;
  
	 declare @Error int
 
  insert Status
        (Status,
         StatusCode,
         Type,
         OrderBy)
  select @Status,
         @StatusCode,
         @Type,
         @OrderBy 
  
  select @Error = @@Error, @StatusId = scope_identity()
  
  
  return @Error
  
end
