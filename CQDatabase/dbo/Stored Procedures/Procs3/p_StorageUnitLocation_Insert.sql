﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitLocation_Insert
  ///   Filename       : p_StorageUnitLocation_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:07
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnitLocation table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null output,
  ///   @LocationId int = null output,
  ///   @MinimumQuantity float = null,
  ///   @HandlingQuantity float = null,
  ///   @MaximumQuantity float = null 
  /// </param>
  /// <returns>
  ///   StorageUnitLocation.StorageUnitId,
  ///   StorageUnitLocation.LocationId,
  ///   StorageUnitLocation.MinimumQuantity,
  ///   StorageUnitLocation.HandlingQuantity,
  ///   StorageUnitLocation.MaximumQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitLocation_Insert
(
 @StorageUnitId int = null output,
 @LocationId int = null output,
 @MinimumQuantity float = null,
 @HandlingQuantity float = null,
 @MaximumQuantity float = null 
)

as
begin
	 set nocount on;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
	 declare @Error int
 
  insert StorageUnitLocation
        (StorageUnitId,
         LocationId,
         MinimumQuantity,
         HandlingQuantity,
         MaximumQuantity)
  select @StorageUnitId,
         @LocationId,
         @MinimumQuantity,
         @HandlingQuantity,
         @MaximumQuantity 
  
  select @Error = @@Error, @LocationId = scope_identity()
  
  
  return @Error
  
end
