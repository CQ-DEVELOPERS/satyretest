﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_KPI
  ///   Filename       : p_Report_Product_KPI.sql
  ///   Create By      : Karen
  ///   Date Created   : Jul 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Product_KPI
(
@ProductId		int,
@FromDate		datetime,
@ToDate			datetime,
@WarehouseId	int
)



as
begin
	 set nocount on;
	 
		 
Declare @Error				int,
        @Errormsg			varchar(500),
        @CheckedFrom		datetime,
        @CheckedTo			datetime
  
  --select @GetDate = dbo.ufn_Getdate()


  set @FromDate = DATEADD(d,DATEDIFF(d,0,@FromDate),0)
  set @FromDate = DATEADD(HH, -12 ,@FromDate)
  set @ToDate = DATEADD(d,DATEDIFF(d,0,@ToDate),0)
  set @ToDate = DATEADD(HH, 12 ,@ToDate)
  
  --** USE this to select a date range
  --set @FromDate = '2012-07-24 00:00:00'
  --set @ToDate = '2012-07-24 23:59:00'

  If @WarehouseId = -1
		set @WarehouseId = null
		
  --If @ProductId = -1
		--set @ProductId = null
  
  declare @TableDetail as Table
  (
    StorageUnitId	int NULL,
    ProductCode		nvarchar(30),
    Product			nvarchar(255),
	PackType		nvarchar(30) NULL,
	Quantity		int NULL,
	WarehouseId		int NULL,
	Received		int NULL,
	Damages			int NULL,
	Samples			int NULL,
	PutAway			int NULL,
	Movements		int NULL,
	Replenishments	int NULL,
	Picking			int NULL,
	Checking		int NULL,
	MoveToDespatch	int NULL,
	Despatch		int NULL
  );
  

	If @ProductId = -1
	 
  	insert	@TableDetail
			(StorageUnitId,
			PackType,
			Quantity,
			Received,
			Damages,
			Samples,
			PutAway,
			Movements	,
			Replenishments	,
			Picking		,
			Checking	,
			MoveToDespatch	,
			Despatch)
	select	StorageUnitId,
			PackType,
			Quantity,
			sum(Received) as Received,
			sum(Damages) as Damages,
			sum(Samples) as Samples,
			sum(PutAway) as PutAway,
			sum(Movements) as Movements,
			sum(Replenishments)	as Replenishments,
			sum(Picking) as	Picking,
			sum(Checking) as Checking,
			sum(MoveToDespatch)	as MoveToDespatch,
			sum(Despatch) as Despatch
	from ProductKPI pk
	where EndDate between @FromDate and @ToDate
	and pk.WarehouseId = isnull(@WarehouseId, pk.WarehouseId)

	group by	StorageUnitId,
				PackType,
				Quantity
	order by	StorageUnitId,
				PackType,
				Quantity
				
				
    If @ProductId != -1
	 
  	insert	@TableDetail
			(StorageUnitId,
			PackType,
			Quantity,
			Received,
			Damages,
			Samples,
			PutAway,
			Movements	,
			Replenishments	,
			Picking		,
			Checking	,
			MoveToDespatch	,
			Despatch)
	select	StorageUnitId,
			PackType,
			Quantity,
			sum(Received) as Received,
			sum(Damages) as Damages,
			sum(Samples) as Samples,
			sum(PutAway) as PutAway,
			sum(Movements) as Movements,
			sum(Replenishments)	as Replenishments,
			sum(Picking) as	Picking,
			sum(Checking) as Checking,
			sum(MoveToDespatch)	as MoveToDespatch,
			sum(Despatch) as Despatch
	from ProductKPI pk
	where EndDate between @FromDate and @ToDate
	and pk.WarehouseId = isnull(@WarehouseId, pk.WarehouseId)
	and pk.StorageUnitId = (select StorageUnitId from StorageUnit su
							where su.ProductId = @ProductId)
	group by	StorageUnitId,
				PackType,
				Quantity
	order by	StorageUnitId,
				PackType,
				Quantity
 
  
  	update @TableDetail
     set Product =	p.Product,
		 ProductCode = p.ProductCode
    from @TableDetail td
    join StorageUnit su (nolock) on su.StorageUnitId = td.StorageUnitId
    join product p (nolock) on p.ProductId = su.ProductId
    
    
    Select * from @TableDetail     
    
end
   		
