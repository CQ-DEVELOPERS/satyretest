﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Despatch_Document_Search
  ///   Filename       : p_Report_Despatch_Document_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Despatch_Document_Search
(
 @WarehouseId            int,
 @OutboundShipmentId	    int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @OrderNumber	           nvarchar(30),
 @FromDate	              datetime,
 @ToDate	                datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundDocumentId        int,
   OutboundDocumentType      nvarchar(30),
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   CustomerCode              nvarchar(30),
   Customer                  nvarchar(255),
   RouteId                   int,
   Route                     nvarchar(50),
   NumberOfLines             int,
   DeliveryDate              datetime,
   StatusId                  int,
   Status                    nvarchar(50),
   LoadIndicator             bit,
   Remarks                   nvarchar(250)
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @OutboundShipmentId is null
  begin
    insert @TableResult
          (OutboundDocumentId,
           IssueId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           DeliveryDate,
           LoadIndicator,
           Remarks,
           NumberOfLines)
    select od.OutboundDocumentId,
           i.IssueId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.DeliveryDate,
           i.LoadIndicator,
           i.Remarks,
           i.NumberOfLines
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId                = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
     where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
       and od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
       and i.DeliveryDate      between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('CD','D','DC','C')
       and od.WarehouseId            = @WarehouseId
  end
  else
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           DeliveryDate,
           LoadIndicator,
           Remarks,
           NumberOfLines)
    select osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.DeliveryDate,
           i.LoadIndicator,
           os.Remarks,
           i.NumberOfLines
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId                = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
      join OutboundShipment       os (nolock) on osi.OutboundShipmentId = os.OutboundShipmentId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and s.Type                 = 'IS'
       and s.StatusCode          in ('CD','D','DC','C')
  
  update tr
     set OutboundShipmentId = si.OutboundShipmentId,
         Route        = os.Route,
         RouteId      = os.RouteId,
         StatusId     = os.StatusId
    from @TableResult  tr
    join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
    join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
  
  delete tr
    from @TableResult tr
    join OutboundShipmentIssue osi (nolock) on tr.OutboundShipmentId = osi.OutboundShipmentId
    join Issue                   i (nolock) on osi.IssueId           = i.IssueId
    join Status                  s (nolock) on i.StatusId            = s.StatusId
   where s.StatusCode not in ('CD','D','DC','C')
  
  update tr
     set Remarks = os.Remarks
    from @TableResult tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
   where os.remarks = 'Despatch Documents Printed'
  
  update tr
     set Remarks = i.Remarks
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
   where i.remarks = 'Despatch Documents Printed'
  
  update @TableResult
     set Remarks = 'Not Printed'
   where isnull(Remarks,'') != 'Despatch Documents Printed'
  
  update @TableResult
     set Remarks = 'Printed'
   where Remarks = 'Despatch Documents Printed'
  
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
--  update @TableResult
--     set NumberOfLines = (select count(1)
--                            from OutboundLine ol (nolock) 
--                           where tr.OutboundDocumentId = ol.OutboundDocumentId)
--    from @TableResult tr
  
  select IssueId,
         isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
         OrderNumber,
         CustomerCode,
         Customer,
         isnull(RouteId,-1) as 'RouteId',
         Route,
         NumberOfLines,
         DeliveryDate,
         Remarks as 'Status'
    from @TableResult
  order by 'Status',
           OutboundShipmentId,
           OrderNumber
--   where LoadIndicator != 1
--  union
--  select -1,
--         isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
--         null,
--         null,
--         null,
--         RouteId,
--         Route,
--         sum(NumberOfLines),
--         DeliveryDate,
--         Status
--    from @TableResult
--   where LoadIndicator = 1
--  group by isnull(OutboundShipmentId, -1),
--           RouteId,
--           Route,
--           DeliveryDate,
--           Status
end


