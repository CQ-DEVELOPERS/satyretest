﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SalesRep_by_ContactListId
  ///   Filename       : p_SalesRep_by_ContactListId.sql
  ///   Create By      : Karen
  ///   Date Created   : August 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SalesRep_by_ContactListId
(
 @ContactListId int = null
)

as
begin
	 set nocount on;
  
  select -1     as 'ContactListId',
         'None' as 'ContactPerson'
  union
  select ContactListId,
		 ContactPerson
    from ContactList
   --where ContactPerson like @ContactPerson + '%'
   order by ContactPerson
end
