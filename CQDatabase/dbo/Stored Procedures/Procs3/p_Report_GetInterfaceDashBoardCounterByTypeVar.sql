﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_GetInterfaceDashBoardCounterByTypeVar
  ///   Filename       : p_Report_GetInterfaceDashBoardCounterByTypeVar.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Report_GetInterfaceDashBoardCounterByTypeVar
(
@FromDate datetime,
@ToDate datetime,
@fromtohost char(1),
@extracttype char(3)
)
AS
Begin
IF @fromtohost = 'T' and @extracttype = 'NEX'

BEGIN 

SET NOCOUNT ON;

DECLARE @FailedPOCount INT,
        @FailedCRCount INT,
        @FailedTRCount INT,
        @FailedSOCount INT,
        @FailedTOCount INT,
        @FailedINCount INT,
        @FailedDNCount INT,
        @FailedSACount INT,
        @FailedSMCount INT,
        @ExtractDescription nvarchar(50),
        @show char(1)      

SET @ExtractDescription = 'Not Extracted - To Host' 
SET @show = 'Y'       

SELECT @FailedPOCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
	and RecordType = 'PUR' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'PO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Not Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'PO'


SELECT @FailedSOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
	and RecordType = 'SAL' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'SO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Not Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
	and RecordType = 'RET' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'CR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Not Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'CR'
 
SELECT @FailedTOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Not Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Not Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TR'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Not Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'IN'

SELECT @FailedSACount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
	and RecordType = 'SOH' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'AD'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSACount, ExtractDescription = 'Not Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'AD'

SELECT @FailedSMCount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
	and RecordType = 'ADJ' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'SM'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSMCount, ExtractDescription = 'Not Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SM'


        
END

IF @fromtohost = 'T' and @extracttype = 'EXT'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Extracted - To Host'   
SET @show = 'N'     
       
SELECT @FailedPOCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus <> 'N' 
	and RecordType = 'PUR' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'PO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus <> 'N' 
	and RecordType = 'SAL' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'SO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus <> 'N' 
	and RecordType = 'RET' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'CR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'CR'
 
SELECT @FailedTOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus <> 'N' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus <> 'N' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TR'

SELECT @FailedINCount = COUNT(InterfaceImportIPHeaderId) 
FROM InterfaceImportIPHeader, DashboardInterfaceCoDisplay
WHERE ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'IN'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedINCount, ExtractDescription = 'Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'IN'

SELECT @FailedSACount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus <> 'N' 
	and RecordType = 'SOH' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'AD'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSACount, ExtractDescription = 'Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'AD'

SELECT @FailedSMCount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus<> 'N' 
	and RecordType = 'ADJ' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'SM'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSMCount, ExtractDescription = 'Extracted - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SM'



        
END

IF @fromtohost = 'T' and @extracttype = 'TSU'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Successful - To Host'  
SET @show = 'N'      

SELECT @FailedPOCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'S' 
	and RecordType = 'PUR'  
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'PO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Successful - To Host', Show = @show    
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'S' 
	and RecordType = 'SAL' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'SO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Successful - To Host', Show = @show    
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'S' 
	and RecordType = 'RET' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'CR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Successful - To Host', Show = @show    
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'CR'

SELECT @FailedSACount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'S' 
	and RecordType = 'SOH' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'AD'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSACount, ExtractDescription = 'Successful - To Host', Show = @show    
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'AD'

SELECT @FailedSMCount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'S' 
	and RecordType = 'ADJ' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'SM'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSMCount, ExtractDescription = 'Successful - To Host', Show = @show    
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SM'

SELECT @FailedTOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'S' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Successful - To Host', Show = @show    
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'S' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Successful - To Host', Show = @show    
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TR'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Successful - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'IN'

        
END

IF @fromtohost = 'T' and @extracttype = 'TFA'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Failed - To Host' 
SET @show = 'Y'       

SELECT @FailedPOCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'F' 
	and RecordType = 'PUR' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'PO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Failed - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'F' 
	and RecordType = 'SAL' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'SO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Failed - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'F' 
	and RecordType = 'RET' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'CR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Failed - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'CR'
 
SELECT @FailedSACount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'F' 
	and RecordType = 'SOH' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'AD'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSACount, ExtractDescription = 'Failed - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'AD'

SELECT @FailedSMCount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'F' 
	and RecordType = 'ADJ' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'SM'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSMCount, ExtractDescription = 'Failed - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SM'

SELECT @FailedTOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'F' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Failed - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'F' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Failed - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TR'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Failed - To Host'
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'IN'


        
END

IF @fromtohost = 'T' and @extracttype = 'TDW'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Dealt With - To Host' 
SET @show = 'N'     

SELECT @FailedPOCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
	and RecordType = 'PUR' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'PO'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Dealt With - To Host', Show = @show 
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
	and RecordType = 'SAL' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'SO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Dealt With - To Host', Show = @show 
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
	and RecordType = 'RET' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'CR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Dealt With - To Host', Show = @show 
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'CR'
 
SELECT @FailedSACount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
	and RecordType = 'SOH' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'AD'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSACount, ExtractDescription = 'Dealt With - To Host', Show = @show 
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'AD'

SELECT @FailedSMCount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
	and RecordType = 'ADJ' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'SM'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSMCount, ExtractDescription = 'Dealt With - To Host', Show = @show 
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SM'

SELECT @FailedTOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Dealt With - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Dealt With - To Host', Show = @show 
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TR'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Dealt With - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'IN'

        
END

IF @fromtohost = 'T' and @extracttype = 'TWA'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Waiting - To Host'  
SET @show = 'N'      
       
SELECT @FailedPOCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'Y' 
	and RecordType = 'PUR' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'PO'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Waiting - To Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'Y' 
and RecordType = 'SAL' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Waiting - To Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'Y' 
and RecordType = 'RET' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'CR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Waiting - To Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'CR'
 
SELECT @FailedTOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'Y' 
and RecordType = 'IBT' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Waiting - To Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'Y' 
and RecordType = 'IBT' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Waiting - To Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TR'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Waiting - To Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'IN'

SELECT @FailedSACount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'Y' 
and RecordType = 'SOH' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'AD'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSACount, ExtractDescription = 'Waiting - To Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'AD'

SELECT @FailedSMCount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'Y' 
and RecordType = 'ADJ' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SM'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSMCount, ExtractDescription = 'Waiting - To Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SM'

SELECT @FailedSMCount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'Y' 
and RecordType = 'XFR' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SM'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSMCount, ExtractDescription = 'Waiting - To Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SM'
        
END
        
IF @fromtohost = 'T' and @extracttype = 'SSU'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Sent Successfully - To Host'   
SET @show = 'N'     
       
SELECT @FailedPOCount = COUNT(InterfaceMessageId) 
FROM InterfaceMessage, DashboardInterfaceCoDisplay, InterfaceExportPOHeader
WHERE Status <> 'N' 
and InterfaceId = InterfaceExportPOHeaderId
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'PO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Sent Successfully - To Host', Show = @show    
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceMessage, InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE Status <> 'N' 
and InterfaceId = InterfaceExportSOHeaderId
and RecordType = 'SAL' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Sent Successfully - To Host', Show = @show    
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceMessage, InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE Status <> 'N' 
and InterfaceId = InterfaceExportSOHeaderId
and RecordType = 'RET' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'CR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Sent Successfully - To Host', Show = @show    
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'CR'
 
SELECT @FailedTOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceMessage, InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE Status <> 'N' 
and InterfaceId = InterfaceExportSOHeaderId
and RecordType = 'IBT' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Sent Successfully - To Host', Show = @show    
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceMessage, InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE Status <> 'N' 
and InterfaceId = InterfaceExportPOHeaderId
and RecordType = 'IBT' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Sent Successfully - To Host', Show = @show    
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TR'

SELECT @FailedSACount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceMessage, InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE Status <> 'N' 
and InterfaceId = InterfaceExportStockAdjustmentId
and RecordType = 'SOH' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'AD'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSACount, ExtractDescription = 'Sent Successfully - To Host', Show = @show    
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'AD'

SELECT @FailedSMCount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceMessage, InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE Status <> 'N' 
and InterfaceId = InterfaceExportStockAdjustmentId
and RecordType = 'ADJ' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SM'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSMCount, ExtractDescription = 'Sent Successfully - To Host', Show = @show    
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SM'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Sent Successfully - To Host', Show = @show    
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'IN'

        
END

IF @fromtohost = 'T' and @extracttype = 'SER'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Sent With Errors - To Host'  
SET @show = 'Y'      
  


SELECT @FailedPOCount = COUNT(InterfaceMessageId) 
FROM InterfaceMessage, DashboardInterfaceCoDisplay, InterfaceExportPOHeader
WHERE Status = 'E' 
and InterfaceId = InterfaceExportPOHeaderId
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'PO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Sent With Errors - To Host', Show = @show   
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceMessage, InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE Status = 'E' 
and InterfaceId = InterfaceExportSOHeaderId
and RecordType = 'SAL' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Sent With Errors - To Host', Show = @show   
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceMessage, InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE Status = 'E' 
and InterfaceId = InterfaceExportSOHeaderId
and RecordType = 'RET' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'CR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Sent With Errors - To Host', Show = @show   
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'CR'
 
SELECT @FailedTOCount = COUNT(InterfaceExportSOHeaderId) 
FROM InterfaceMessage, InterfaceExportSOHeader, DashboardInterfaceCoDisplay
WHERE Status = 'E' 
and InterfaceId = InterfaceExportSOHeaderId
and RecordType = 'IBT' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Sent With Errors - To Host', Show = @show   
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceExportPOHeaderId) 
FROM InterfaceMessage, InterfaceExportPOHeader, DashboardInterfaceCoDisplay
WHERE Status = 'E' 
and InterfaceId = InterfaceExportPOHeaderId
and RecordType = 'IBT' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Sent With Errors - To Host', Show = @show   
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TR'

SELECT @FailedSACount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceMessage, InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE Status = 'E' 
and InterfaceId = InterfaceExportStockAdjustmentId
and RecordType = 'SOH' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'AD'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSACount, ExtractDescription = 'Sent With Errors - To Host', Show = @show   
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'AD'

SELECT @FailedSMCount = COUNT(InterfaceExportStockAdjustmentId) 
FROM InterfaceMessage, InterfaceExportStockAdjustment, DashboardInterfaceCoDisplay
WHERE Status = 'E' 
and InterfaceId = InterfaceExportStockAdjustmentId
and RecordType = 'ADJ' 
and InterfaceMessage.ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SM'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSMCount, ExtractDescription = 'Sent With Errors - To Host', Show = @show   
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SM'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Sent With Errors - To Host', Show = @show   
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'IN'

        
END


IF @fromtohost = 'F' and @extracttype = 'UNP'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Unprocessed - From Host'
SET @show = 'N'
       
SELECT @FailedPOCount = COUNT(InterfaceImportPOHeaderId) 
FROM InterfaceImportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
and RecordType = 'PUR' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'PO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Unprocessed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
and RecordType = 'SAL' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Unprocessed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
and RecordType = 'RET' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'CR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Unprocessed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'CR'

SELECT @FailedTOCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
and RecordType = 'IBT' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Unprocessed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceImportPOHeaderId) 
FROM InterfaceImportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'N' 
and RecordType = 'IBT' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Unprocessed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TR'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Unprocessed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'IN'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Unprocessed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'AD'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Unprocessed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SM'
        
END

IF @fromtohost = 'F' and @extracttype = 'SUC'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Successful - From Host'
SET @show = 'N'
       
SELECT @FailedPOCount = COUNT(InterfaceImportPOHeaderId) 
FROM InterfaceImportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'C' 
and RecordType = 'PUR' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'PO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Successful - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'C' 
and RecordType = 'SAL' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Successful - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'C' 
and RecordType = 'RET' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'CR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Successful - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'CR'

SELECT @FailedTOCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'C' 
and RecordType = 'IBT' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Successful - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceImportPOHeaderId) 
FROM InterfaceImportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'C' 
and RecordType = 'IBT' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Successful - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TR'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Successful - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'IN'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Successful - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'AD'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Successful - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SM'

      
END

IF @fromtohost = 'F' and @extracttype = 'FAI'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Failed - From Host'
SET @show = 'Y'
       
SELECT @FailedPOCount = COUNT(InterfaceImportPOHeaderId) 
FROM InterfaceImportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'E' 
and RecordType = 'PUR' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'PO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Failed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'E' 
and RecordType = 'SAL' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Failed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'E' 
and RecordType = 'RET' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'CR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Failed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'CR'
 
SELECT @FailedTOCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'E' 
and RecordType = 'IBT' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Failed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceImportPOHeaderId) 
FROM InterfaceImportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'E' 
and RecordType = 'IBT' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Failed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'TR'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Failed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'IN'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Failed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'AD'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Failed - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SM'


        
END

IF @fromtohost = 'F' and @extracttype = 'DEW'

BEGIN 

SET NOCOUNT ON;

SET @ExtractDescription = 'Dealt With - From Host'
SET @show = 'N'
       
SELECT @FailedPOCount = COUNT(InterfaceImportPOHeaderId) 
FROM InterfaceImportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
and RecordType = 'PUR' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'PO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedPOCount, ExtractDescription = 'Dealt With - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'PO'

SELECT @FailedSOCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
and RecordType = 'SAL' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'SO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedSOCount, ExtractDescription = 'Dealt With - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'SO'

SELECT @FailedCRCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
and RecordType = 'RET' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'CR'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedCRCount, ExtractDescription = 'Dealt With - From Host', Show = @show
WHERE CompanyId = '2'
and Display = 'Y'
and DisplayCode = 'CR'
 
SELECT @FailedTOCount = COUNT(InterfaceImportSOHeaderId) 
FROM InterfaceImportSOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
and RecordType = 'IBT' 
and ProcessedDate BETWEEN  @FromDate and @ToDate
and CompanyId = '2'
and DisplayCode = 'TO'
and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTOCount, ExtractDescription = 'Dealt With - From Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TO'

SELECT @FailedTRCount = COUNT(InterfaceImportPOHeaderId) 
FROM InterfaceImportPOHeader, DashboardInterfaceCoDisplay
WHERE RecordStatus = 'D' 
	and RecordType = 'IBT' 
	and ProcessedDate BETWEEN  @FromDate and @ToDate
	and CompanyId = '2'
	and DisplayCode = 'TR'
	and Display = 'Y'

UPDATE DashboardInterfaceCoDisplay
SET Counter = @FailedTRCount, ExtractDescription = 'Dealt With - From Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'TR'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Dealt With - From Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'IN'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Dealt With - From Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'AD'

UPDATE DashboardInterfaceCoDisplay
SET Counter = 0, ExtractDescription = 'Dealt With - From Host', Show = @show
WHERE CompanyId = '2'
	and Display = 'Y'
	and DisplayCode = 'SM'


END
     
Declare @DisplayDesc nvarchar(50),
	    @Counter NUMERIC(18,0),
	    @ReportName nvarchar(50)           
        
SELECT *
		
FROM DashboardInterfaceCoDisplay
WHERE CompanyID = '2'

SELECT @DisplayDesc [Display Description],
	   @Counter [Counter],
	   @FromDate [From Date],
       @ToDate [To Date],
       @fromtohost [From To Host],
       @extracttype [Extract Type],
       @ExtractDescription [Extract Description],
       @ReportName [Report Name],
       @show [Show]
end
