﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Serial_Numbers_By_Job
  ///   Filename       : p_Serial_Numbers_By_Job.sql
  ///   Create By      : Karen
  ///   Date Created   : January 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Serial_Numbers_By_Job
(
 @JobId int = null
)

as
begin
	 set nocount on;
  
  declare @TableHeader as Table
  (
   InboundShipmentId		int,
   InboundDocumentId		int,
   ReceiptId				int,
   WarehouseId				int
  );
  
  declare @TableDetails as Table
  (
   JobId				int,
   StorageUnitBatchId	int,
   StorageUnitId		int,
   ProductCode			nvarchar(30),
   Product				nvarchar(255),
   SKUCode				nvarchar(50),
   SerialNumber			nvarchar(50)
  );
  
  declare @Matrix as Table
  (
   ProductCode			nvarchar(30),
   Product				nvarchar(255),
   SerialNumber			nvarchar(50),
   Sequence				int,
   SetNumber			int
  );
  
  declare @count int
  
  set @JobId = replace(@JobId, 'J:', '')
   
  if @JobId like 'R:%'
  	 select @JobId = convert(nvarchar(30), JobId)
  	   from Job (nolock)
  	  where ReferenceNumber = @JobId	  
				
											
  insert @TableDetails
        (JobId,
         StorageUnitBatchId,
         ProductCode,
         Product,
         StorageUnitId,
         SerialNumber)
  select ins.JobId,
         ins.StorageUnitBatchId,
         p.ProductCode,
         p.Product,
         su.StorageUnitId,
         sn.SerialNumber
    --from Instruction ins (nolock)
    --join Job				j (nolock) on ins.JobId = j.JobId
    --join StorageUnitBatch sub (nolock) on ins.StorageUnitBatchId = sub.StorageUnitBatchId
    --join StorageUnit       su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    --join ReceiptLine       rl (nolock) on ins.ReceiptLineId      = rl.ReceiptLineId
    --join Product            p (nolock) on su.ProductId           = p.ProductId
    --left join SerialNumber sn (nolock) on rl.ReceiptId		     = sn.ReceiptId
        from Instruction ins (nolock)
    join Job				j (nolock) on ins.JobId = j.JobId
    join StorageUnitBatch sub (nolock) on ins.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    join IssueLine       il (nolock) on ins.IssueLineId      = il.IssueLineId
									 
    join Product            p (nolock) on su.ProductId           = p.ProductId
    left join SerialNumber sn (nolock) on il.IssueId		     = sn.IssueId
										and su.StorageUnitId = sn.StorageUnitId
   where ins.JobId = @JobId
 
  insert @Matrix
        (ProductCode,
         Product,
         SerialNumber,
         sequence,
         SetNumber)
  select distinct 
         td.ProductCode,
         td.Product,
         td.SerialNumber,
         ROW_NUMBER() OVER (partition by ProductCode ORDER BY ProductCode) Rank,
         1
    from @TableDetails td 
   where ProductCode is not null
     and SerialNumber is not null 
 

	while exists (select sequence from @Matrix
		where Sequence > 8)
	begin
		update mx
		   set Sequence = (Sequence - 8),
			   SetNumber = SetNumber + 1
	      from @Matrix mx
	     where Sequence > 8
	end
    
	select * from @Matrix
  
end

 
