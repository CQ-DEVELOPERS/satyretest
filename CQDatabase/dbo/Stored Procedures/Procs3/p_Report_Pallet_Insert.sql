﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Pallet_Insert
  ///   Filename       : p_Report_Pallet_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Pallet_Insert
(
 @StorageUnitBatchId int,
 @LocationId int,
 @PalletId int output
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StorageUnitId     int,
          @Quantity          float,
          @Weight            numeric(13,3)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @Quantity = ActualQuantity
    from StorageUnitBatchLocation (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
     and LocationId = @LocationId
  
  select @StorageUnitId = StorageUnitId
    from StorageUnitBatch (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
  
  select @Weight = min(Weight)
    from Pack (nolock)
   where StorageUnitId = @StorageUnitId
  
  if @Weight is null
    set @Weight = 1
  
  select @Weight = convert(numeric(13,3), @Quantity) * @Weight
  
  begin transaction
  
  exec @Error = p_Pallet_Insert
   @PalletId           = @PalletId output,
   @StorageUnitBatchId = @StorageUnitBatchId,
   @LocationId         = @LocationId,
   @Weight             = @Weight,
   @Tare               = null,
   @Quantity           = @Quantity,
   @Prints             = 1
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Report_Pallet_Insert'); 
    rollback transaction
    return @Error
end
