﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_Pack
  ///   Filename       : p_Report_Product_Pack.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 15 Jan 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Product_Pack
(
 @WarehouseId     int,
 @ProductCode        nvarchar(50),
 @Product           nvarchar(50)
)

as
begin

if (isnull(@ProductCode,'All') = 'All')
Begin 
Set @ProductCode = ''
End

if (isnull(@Product,'All') = 'All')
Begin 
set @Product = ''
End

	 set nocount on;
Select 
				X.ProductCode, 
				X.Product, 
				--P.PackId, 
				X.StorageUnitId, 
				X.UnitQuantity, 
				X.UnitWeight, 
				X.UnitHeight, 
				X.UnitLength,
				Y.PalletQuantity, 
				Y.PalletWeight, 
				Y.PalletHeight, 
				Y.PalletLength
 From

(SELECT			Pr.ProductCode, 
				Pr.Product, 
				--P.PackId, 
				SU.StorageUnitId, 
				isnull(P.Quantity,0) as UnitQuantity, 
				convert(numeric(20,4),P.NettWeight) as UnitWeight, 
				Convert(Numeric(20,3),isnull(P.Height,0)) as UnitHeight, 
				Convert(Numeric(20,3),isnull(P.Length,0)) as UnitLength
	FROM         PackType AS PT INNER JOIN
                      Pack AS P ON PT.PackTypeId = P.PackTypeId INNER JOIN
                      StorageUnit AS SU ON P.StorageUnitId = SU.StorageUnitId INNER JOIN
                      Product AS Pr ON SU.ProductId = Pr.ProductId
	Where PT.PackTypeId = 2	
		AND P.WarehouseId = @WarehouseId
And (Pr.ProductCode Like @ProductCode + '%' 
AND Pr.Product LIKE @Product + '%')
) As X	
Inner Join 
(
SELECT			Pr.ProductCode, 
				Pr.Product, 
				--P.PackId, 
				SU.StorageUnitId, 
				isnull(P.Quantity,0) as PalletQuantity, 
				convert(numeric(20,4),P.Weight) as 
PalletWeight, 
				Convert(Numeric(20,3),isnull(P.Height,0)) as PalletHeight, 
				Convert(Numeric(20,3),isnull(P.Length,0)) as PalletLength
	FROM         PackType AS PT INNER JOIN
                      Pack AS P ON PT.PackTypeId = P.PackTypeId INNER JOIN
                      StorageUnit AS SU ON P.StorageUnitId = SU.StorageUnitId INNER JOIN
                      Product AS Pr ON SU.ProductId = Pr.ProductId
	Where PT.PackTypeId = 1		
AND P.WarehouseId = @WarehouseId
And (Pr.ProductCode Like @ProductCode + '%' 
AND Pr.Product LIKE @Product + '%')
 ) as Y 
On X.ProductCode = Y.ProductCode


end
