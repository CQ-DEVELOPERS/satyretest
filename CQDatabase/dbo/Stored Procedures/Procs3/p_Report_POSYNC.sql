﻿
Create procedure [dbo].[p_Report_POSYNC]
as
Begin

Create table #Results (Ordernumber varchar(50),
                                    ProductCode varchar(50),
                                    Product varchar(255),
                                    PastelQuantity float,
                                    CquentialQuantity float,
                                    POStatus varchar(50))
                              

Insert into #Results
Select h.Ordernumber,p.ProductCode,p.Product,d.Quantity as PastelQuantity,l.quantity as CquentialQty,
            Case when l.ProductCode is null then 'Missing on CQ'
                   when d.Quantity <> l.Quantity then 'Quantity MisMatch'
            Else 'Matched' end as POStatus
from InterfaceImportPONHeader h
       Join InterfaceImportPONDetail d on d.InterfaceImportPONHeaderId = h.InterfaceImportPONHeaderId
       Join Product p on p.HostId = d.ProductCode
       Left Join viewReceipt r on r.OrderNumber = h.Ordernumber and ISNULL(r.Interfaced,0) <> 1
       Left Join viewReceiptLineQty l on l.ReceiptId = r.Receiptid and l.ProductCode = p.ProductCode

Insert Into #Results
Select r.Ordernumber,
         rl.ProductCode,
         rl.Product,
         0 as PastelQuantity,
         Sum(rl.RequiredQuantity) as CquentialQTY,
         'Closed on Pastel' as POStatus 
from viewReceiptLine rl
      Join viewReceipt r on r.ReceiptId = rl.ReceiptId
      Left Join InterfaceImportPONHeader h on h.Ordernumber = r.Ordernumber 
where r.statuscode = 'W' and h.Ordernumber is null
Group by r.Ordernumber,
         rl.ProductCode,
         rl.Product
oRDER BY r.Ordernumber desc

Select * from #Results where Ordernumber like 'PO%' and POStatus != 'Matched'  Order by OrderNumber DESC

return
End


