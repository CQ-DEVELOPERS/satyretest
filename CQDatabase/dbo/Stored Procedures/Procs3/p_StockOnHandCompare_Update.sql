﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockOnHandCompare_Update
  ///   Filename       : p_StockOnHandCompare_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:44
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StockOnHandCompare table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null,
  ///   @ComparisonDate datetime = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @Quantity float = null,
  ///   @Sent bit = null,
  ///   @UnitPrice numeric(13,3) = null,
  ///   @WarehouseCode varchar(50) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockOnHandCompare_Update
(
 @WarehouseId int = null,
 @ComparisonDate datetime = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @Quantity float = null,
 @Sent bit = null,
 @UnitPrice numeric(13,3) = null,
 @WarehouseCode varchar(50) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update StockOnHandCompare
     set WarehouseId = isnull(@WarehouseId, WarehouseId),
         ComparisonDate = isnull(@ComparisonDate, ComparisonDate),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         BatchId = isnull(@BatchId, BatchId),
         Quantity = isnull(@Quantity, Quantity),
         Sent = isnull(@Sent, Sent),
         UnitPrice = isnull(@UnitPrice, UnitPrice),
         WarehouseCode = isnull(@WarehouseCode, WarehouseCode) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
