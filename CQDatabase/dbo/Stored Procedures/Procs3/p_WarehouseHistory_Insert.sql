﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_WarehouseHistory_Insert
  ///   Filename       : p_WarehouseHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 May 2014 13:31:59
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the WarehouseHistory table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null,
  ///   @CompanyId int = null,
  ///   @Warehouse nvarchar(100) = null,
  ///   @WarehouseCode nvarchar(20) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @HostId nvarchar(60) = null,
  ///   @ParentWarehouseId int = null,
  ///   @AddressId int = null,
  ///   @DesktopMaximum int = null,
  ///   @DesktopWarning int = null,
  ///   @MobileWarning int = null,
  ///   @MobileMaximum int = null,
  ///   @UploadToHost bit = null,
  ///   @PostalAddressId int = null,
  ///   @ContactListId int = null,
  ///   @PrincipalId int = null,
  ///   @AllowInbound bit = null,
  ///   @AllowOutbound bit = null 
  /// </param>
  /// <returns>
  ///   WarehouseHistory.WarehouseId,
  ///   WarehouseHistory.CompanyId,
  ///   WarehouseHistory.Warehouse,
  ///   WarehouseHistory.WarehouseCode,
  ///   WarehouseHistory.CommandType,
  ///   WarehouseHistory.InsertDate,
  ///   WarehouseHistory.HostId,
  ///   WarehouseHistory.ParentWarehouseId,
  ///   WarehouseHistory.AddressId,
  ///   WarehouseHistory.DesktopMaximum,
  ///   WarehouseHistory.DesktopWarning,
  ///   WarehouseHistory.MobileWarning,
  ///   WarehouseHistory.MobileMaximum,
  ///   WarehouseHistory.UploadToHost,
  ///   WarehouseHistory.PostalAddressId,
  ///   WarehouseHistory.ContactListId,
  ///   WarehouseHistory.PrincipalId,
  ///   WarehouseHistory.AllowInbound,
  ///   WarehouseHistory.AllowOutbound 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_WarehouseHistory_Insert
(
 @WarehouseId int = null,
 @CompanyId int = null,
 @Warehouse nvarchar(100) = null,
 @WarehouseCode nvarchar(20) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @HostId nvarchar(60) = null,
 @ParentWarehouseId int = null,
 @AddressId int = null,
 @DesktopMaximum int = null,
 @DesktopWarning int = null,
 @MobileWarning int = null,
 @MobileMaximum int = null,
 @UploadToHost bit = null,
 @PostalAddressId int = null,
 @ContactListId int = null,
 @PrincipalId int = null,
 @AllowInbound bit = null,
 @AllowOutbound bit = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert WarehouseHistory
        (WarehouseId,
         CompanyId,
         Warehouse,
         WarehouseCode,
         CommandType,
         InsertDate,
         HostId,
         ParentWarehouseId,
         AddressId,
         DesktopMaximum,
         DesktopWarning,
         MobileWarning,
         MobileMaximum,
         UploadToHost,
         PostalAddressId,
         ContactListId,
         PrincipalId,
         AllowInbound,
         AllowOutbound)
  select @WarehouseId,
         @CompanyId,
         @Warehouse,
         @WarehouseCode,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @HostId,
         @ParentWarehouseId,
         @AddressId,
         @DesktopMaximum,
         @DesktopWarning,
         @MobileWarning,
         @MobileMaximum,
         @UploadToHost,
         @PostalAddressId,
         @ContactListId,
         @PrincipalId,
         @AllowInbound,
         @AllowOutbound 
  
  select @Error = @@Error
  
  
  return @Error
  
end
