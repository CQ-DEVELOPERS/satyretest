﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VisualDataOutbound_Search
  ///   Filename       : p_VisualDataOutbound_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:36:10
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the VisualDataOutbound table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   VisualDataOutbound.Location,
  ///   VisualDataOutbound.OrderNumber,
  ///   VisualDataOutbound.ExternalCompany,
  ///   VisualDataOutbound.JobId,
  ///   VisualDataOutbound.Pallet,
  ///   VisualDataOutbound.InstructionType,
  ///   VisualDataOutbound.StatusCode,
  ///   VisualDataOutbound.Scanned,
  ///   VisualDataOutbound.DespatchDate,
  ///   VisualDataOutbound.Minutes 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VisualDataOutbound_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         VisualDataOutbound.Location
        ,VisualDataOutbound.OrderNumber
        ,VisualDataOutbound.ExternalCompany
        ,VisualDataOutbound.JobId
        ,VisualDataOutbound.Pallet
        ,VisualDataOutbound.InstructionType
        ,VisualDataOutbound.StatusCode
        ,VisualDataOutbound.Scanned
        ,VisualDataOutbound.DespatchDate
        ,VisualDataOutbound.Minutes
    from VisualDataOutbound
  
end
