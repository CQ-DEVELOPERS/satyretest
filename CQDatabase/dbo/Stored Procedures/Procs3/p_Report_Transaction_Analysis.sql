﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Transaction_Analysis
  ///   Filename       : p_Report_Transaction_Analysis.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Transaction_Analysis
(
 @ConnectionString nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId      int,
 @FromLocationId   int = null,
 @ToLocationId     int = null,
 @FromDate         datetime,
 @ToDate           datetime
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  InstructionTypeId  int,
	  InstructionType    nvarchar(30),
	  CreateDate         datetime,
	  Location           nvarchar(15),
      PickLocationId     int null,
      PickLocation       nvarchar(15),
      StoreLocationId    int null,
      StoreLocation      nvarchar(15),
	  Quantity           float,
	  ConfirmedQuantity  float
	 )
	 
	 declare @FromLocation nvarchar(15),
	         @ToLocation   nvarchar(15)
	 
	 if @FromLocationId = -1
	   set @FromLocation = '0'
	 else
	   select @FromLocation = Location
	     from Location
	    where LocationId = @FromLocationId
	 
	 if @ToLocationId = -1
	   set @ToLocation = 'Z'
	 else
	   select @ToLocation = Location
	     from Location
	    where LocationId = @ToLocationId
  
  insert @TableResult
        (InstructionTypeId,
	        CreateDate,
         PickLocationId,
         StoreLocationId,
	        Quantity,
	        ConfirmedQuantity)
  select InstructionTypeId,
	        CreateDate,
         PickLocationId,
         StoreLocationId,
	        Quantity,
	        ConfirmedQuantity
    from Instruction  i (nolock)
    join Location  pick (nolock) on i.PickLocationId  = pick.LocationId
    join Location store (nolock) on i.StoreLocationId = store.LocationId
   where WarehouseId                                 = @WarehouseId
     and CreateDate                            between @FromDate and @ToDate
     and(isnull(store.Location, @FromLocation) between @FromLocation and @ToLocation
     or  isnull(pick.Location,  @ToLocation)   between @FromLocation and @ToLocation)
  
  update tr
     set InstructionType = it.InstructionType
    from @TableResult    tr
    join InstructionType it (nolock) on tr.InstructionTypeId = it.InstructionTypeId
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId

  update tr
     set Location      = l.Location,
         StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
  
  if isnull(@FromLocationId,0) <> isnull(@ToLocationId,0)
    insert @TableResult
          (InstructionType
	         ,CreateDate
	         ,Location
	         ,PickLocation
	         ,StoreLocation
	         ,Quantity
	         ,ConfirmedQuantity)
    select InstructionType
	         ,CreateDate
	         ,StoreLocation
	         ,PickLocation
	         ,StoreLocation
	         ,Quantity
	         ,ConfirmedQuantity
      from @TableResult
  
  delete @TableResult where Location is null
  
  select InstructionType
	       ,convert(nvarchar(20), @FromDate, 111) as 'FromDate'
	       ,convert(nvarchar(20), @ToDate, 111)   as 'ToDate'
	       ,Location
	       ,ConfirmedQuantity
    from @TableResult
  order by convert(nvarchar(20), CreateDate, 111)
end
