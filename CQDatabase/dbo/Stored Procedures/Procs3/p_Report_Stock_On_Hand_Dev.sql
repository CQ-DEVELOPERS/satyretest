﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Stock_On_Hand_Dev
  ///   Filename       : p_Report_Stock_On_Hand_Dev.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Stock_On_Hand_Dev
(
 @PickLocationId int = null,
 @StoreLocationId int = null
)

as
begin
	 set nocount on;
  
  select subl.StorageUnitBatchId,
         subl.LocationId,
         p.ProductCode,
         sku.SKUCode,
         b.Batch,
         p.Barcode,
         l.Location,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
   where l.LocationId in (@PickLocationId, @StoreLocationId)
  order by p.ProductCode,
           sku.SKUCode,
           b.Batch
end
