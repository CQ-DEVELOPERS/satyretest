﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_Insert
  ///   Filename       : p_StorageUnitBatch_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:00
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnitBatch table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null output,
  ///   @BatchId int = null,
  ///   @StorageUnitId int = null 
  /// </param>
  /// <returns>
  ///   StorageUnitBatch.StorageUnitBatchId,
  ///   StorageUnitBatch.BatchId,
  ///   StorageUnitBatch.StorageUnitId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatch_Insert
(
 @StorageUnitBatchId int = null output,
 @BatchId int = null,
 @StorageUnitId int = null 
)

as
begin
	 set nocount on;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
	 declare @Error int
 
  insert StorageUnitBatch
        (BatchId,
         StorageUnitId)
  select @BatchId,
         @StorageUnitId 
  
  select @Error = @@Error, @StorageUnitBatchId = scope_identity()
  
  
  return @Error
  
end
