﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Location_Search
  ///   Filename       : p_Static_Location_Search.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 10 September 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Location_Search
(
    @WarehouseId int,
    @AreaId int
)

as
begin
	 set nocount on;
	 
  
SELECT     L.LocationId, L.Location, L.Ailse, L.[Column], L.[Level], L.PalletQuantity, L.SecurityCode, L.RelativeValue, L.StocktakeInd, L.ActiveBinning, L.ActivePicking, L.Used, 
                      L.NumberOfSUB, AL.AreaId, A.WarehouseId,LT.LocationType, LT.LocationTypeCode, LT.LocationTypeId,A.Area,A.AreaID
FROM         Location AS L INNER JOIN
                      AreaLocation AS AL ON L.LocationId = AL.LocationId INNER JOIN
                      Area AS A ON AL.AreaId = A.AreaId  INNER JOIN
                      LocationType LT ON L.LocationTypeId = LT.LocationTypeId
                      
where A.WarehouseId = @WarehouseId 
and AL.AreaId  = isnull(@AreaId,Al.AreaId)

end
