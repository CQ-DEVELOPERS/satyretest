﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTake_Delete
  ///   Filename       : p_StockTake_Delete.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2011
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the StockTakeReference table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_StockTake_Delete
(
 @connectionStringName	varchar(255) = null,
 @StockTakeReferenceId int = null 
)
as
begin
	 set nocount on
	 declare @Error int
	 
  delete StockTakeReferencejob
     where StockTakeReferenceId = @StockTakeReferenceId
  
  delete StockTakeReference
     where StockTakeReferenceId = @StockTakeReferenceId
  
  select @Error = @@Error
  
  
end
