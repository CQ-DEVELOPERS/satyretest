﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Routing_Order_Update
  ///   Filename       : p_Routing_Order_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Routing_Order_Update
(
 @outboundShipmentId int,
 @issueId            int,
 @locationId         int,
 @priorityId         int,
 @routeId            int,
 @deliveryDate       datetime,
 @remarks            nvarchar(250)
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @locationId = -1
    set @locationId = null
  
  if @priorityId = -1
    set @priorityId = null
  
  if @routeId = -1
    set @routeId = null
  
  begin transaction
  
  exec @Error = p_Issue_Update
   @IssueId      = @issueId,
   @LocationId   = @locationId,
   @PriorityId   = @priorityId,
   @RouteId      = @routeId,
   @DeliveryDate = @deliveryDate,
   @Remarks      = @remarks
  
  if @Error <> 0
    goto error
  
  exec @Error = p_OutboundShipment_Update
   @OutboundShipmentId = @outboundShipmentId,
   @ShipmentDate       = @deliveryDate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Routing_Order_Update'); 
    rollback transaction
    return @Error
end
