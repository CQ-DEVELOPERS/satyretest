﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VisualData_Update
  ///   Filename       : p_VisualData_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:36:07
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the VisualData table.
  /// </remarks>
  /// <param>
  ///   @Key1 varchar(50) = null,
  ///   @Sort1 varchar(50) = null,
  ///   @Sort2 varchar(50) = null,
  ///   @Sort3 varchar(50) = null,
  ///   @Colour int = null,
  ///   @Latitude decimal(16,13) = null,
  ///   @Longitude decimal(16,13) = null,
  ///   @Info varchar(1000) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VisualData_Update
(
 @Key1 varchar(50) = null,
 @Sort1 varchar(50) = null,
 @Sort2 varchar(50) = null,
 @Sort3 varchar(50) = null,
 @Colour int = null,
 @Latitude decimal(16,13) = null,
 @Longitude decimal(16,13) = null,
 @Info varchar(1000) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update VisualData
     set Key1 = isnull(@Key1, Key1),
         Sort1 = isnull(@Sort1, Sort1),
         Sort2 = isnull(@Sort2, Sort2),
         Sort3 = isnull(@Sort3, Sort3),
         Colour = isnull(@Colour, Colour),
         Latitude = isnull(@Latitude, Latitude),
         Longitude = isnull(@Longitude, Longitude),
         Info = isnull(@Info, Info) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
