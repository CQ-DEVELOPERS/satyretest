﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockAdjustment_Update
  ///   Filename       : p_StockAdjustment_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Jan 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_StockAdjustment_Update
(
 @InterfaceExportStockAdjustmentId int
,@operatorId int
,@statusCode nvarchar(10)
)
as
begin
  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
  
  set @trancount = @@trancount;
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_StockAdjustment_Update;
    
		  -- Do the actual work here
	   update InterfaceExportStockAdjustment
	      set AuthorisedBy = @operatorId
	         ,AuthorisedDate = dbo.ufn_Getdate()
	         ,RecordStatus = @statusCode
	    where InterfaceExportStockAdjustmentId = @InterfaceExportStockAdjustmentId
	   
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_StockAdjustment_Update;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
