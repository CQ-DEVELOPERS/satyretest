﻿   
/*  
  /// <summary>  
  ///   Procedure Name : p_Report_Load_Sheet  
  ///   Filename       : p_Report_Load_Sheet.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 23 Oct 2007  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Report_Load_Sheet  
(  
 @OutboundShipmentId int,  
 @IssueId            int  
)  
  
as  
begin  
  set nocount on;  
    
  declare @TableResult as table  
  (  
   OutboundShipmentId int,  
   IssueId            int,  
   OutboundDocumentId int,  
   OrderNumber        nvarchar(30),  
   ExternalCompanyId  int,  
   ExternalCompany    nvarchar(50),  
   DespatchDate       datetime,  
   LocationId         int,  
   Location           nvarchar(15),  
   JobId              int,  
   StatusId           int,  
   Status             nvarchar(50),  
   JobStatusId        int,  
   JobStatusCode      nvarchar(10),  
   InstructionTypeId  int,  
   InstructionTypeCode nvarchar(10),  
   InstructionType    nvarchar(50),  
   InstructionId      int,  
   Pallet             nvarchar(10),  
   StorageUnitBatchId int,  
   Quantity           float,  
   ConfirmedQuantity  float,  
   ShortQuantity      float,  
   QuantityOnHand     float,  
   DropSequence       int,  
   RouteId            int,  
   Route              nvarchar(50),  
   Weight             numeric(13,3),  
   Loaded             char(1),
   NumberOfPallets	  int  
  )
    
  if @OutboundShipmentId = -1  
    set @OutboundShipmentId = null  
    
  if @IssueId = -1  
    set @IssueId = null  
    
  if @IssueId is not null  
  begin  
    declare @OutboundDocumentId int  
      
    select @OutboundDocumentId = od.OutboundDocumentId   
      from Issue i  
      join OutboundDocument od on i.OutboundDocumentId = od.OutboundDocumentId  
     where i.IssueId = @IssueId  
      
    select top 1  
           @OutboundShipmentId = osi.OutboundShipmentId,  
           @IssueId            = i.IssueId  
      from OutboundDocument       od (nolock)  
      join Issue                   i (nolock) on i.OutboundDocumentId = od.OutboundDocumentId  
      left  
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId  
     where od.OutboundDocumentId = @OutboundDocumentId  
    order by i.IssueId desc  
  end  
    
  if @OutboundShipmentId is not null  
    set @IssueId = null  
    
  insert @TableResult  
        (OutboundShipmentId,  
         OutboundDocumentId,  
         JobId,  
         StatusId,  
         JobStatusId,  
         InstructionTypeId,  
         InstructionId,  
         StorageUnitBatchId,  
         Quantity,  
         ConfirmedQuantity,  
         ShortQuantity)  
  select ili.OutboundShipmentId,  
         ili.OutboundDocumentId,  
         ins.JobId,  
         ins.StatusId,  
         j.StatusId,  
         ins.InstructionTypeId,  
         ins.InstructionId,  
         ins.StorageUnitBatchId,  
         ins.Quantity,  
         isnull(ili.ConfirmedQuantity, 0),  
         ili.Quantity - isnull(ili.ConfirmedQuantity, 0)  
    from IssueLineInstruction  ili (nolock)  
    join Instruction           ins (nolock) on ili.InstructionId = ins.InstructionId  
    join Job                     j (nolock) on ins.JobId         = j.JobId  
   where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))  
     and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))  
    
  update tr  
     set DespatchDate = os.ShipmentDate,  
         RouteId      = os.RouteId,  
         DropSequence = osi.DropSequence,  
         LocationId   = os.LocationId  
    from @TableResult     tr  
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId  
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId  
   where tr.OutboundShipmentId is not null  
    
  update tr  
     set DespatchDate = i.DeliveryDate,  
         RouteId      = i.RouteId,  
         DropSequence = i.DropSequence,  
         LocationId   = i.LocationId  
    from @TableResult tr  
    join Issue         i (nolock) on tr.IssueId = i.IssueId  
   where tr.OutboundShipmentId is null  
    
  update tr  
     set Route = r.Route  
    from @TableResult tr  
    join Route         r (nolock) on tr.RouteId = r.RouteId  
    
  update tr  
     set OrderNumber       = od.OrderNumber,  
         ExternalCompanyId = od.ExternalCompanyId  
    from @TableResult     tr  
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId  
    
  update tr  
     set ExternalCompany = ec.ExternalCompany  
    from @TableResult    tr  
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId  
    
  update tr  
     set Location = l.Location  
    from @TableResult    tr  
    join Location         l (nolock) on tr.LocationId = l.LocationId  
  
  update tr
     set Pallet = convert(nvarchar(10), j.DropSequence)
               + ' of '
               + convert(nvarchar(10), j.Pallets)
    from @TableResult tr
    join Job           j (nolock) on tr.JobId = j.JobId
    
  update tr  
     set Weight = tr.ConfirmedQuantity * p.Weight  
    from @TableResult      tr  
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId  
    join Pack               p (nolock) on sub.StorageUnitId     = p.StorageUnitId  
   where p.Quantity = 1  
    
  update tr  
     set InstructionType     = 'Full',  
         InstructionTypeCode = it.InstructionTypeCode  
    from @TableResult tr  
    join InstructionType it on tr.InstructionTypeId = it.InstructionTypeId  
   where it.InstructionTypeCode = 'P'  
    
  update @TableResult  
     set InstructionType     = 'Mixed'  
   where InstructionType is null  
    
  update tr  
     set Status = s.Status  
    from @TableResult tr  
    join Status        s on tr.StatusId = s.StatusId  
  where tr.InstructionTypeCode = 'P'  
    and s.StatusCode           = 'NS'  
    
  update tr  
     set JobStatusCode = s.StatusCode  
    from @TableResult tr  
    join Status        s on tr.JobStatusId = s.StatusId  
    
  update tr
     set NumberOfPallets = (select count(distinct(tr2.JobId))
                      from @TableResult tr2
                     where tr.OrderNumber = tr2.OrderNumber)
    from @TableResult tr
    
  update @TableResult  
     set Loaded = 'a'  
   where JobStatusCode = 'C'  
    
  update @TableResult  
     set Loaded = 'r'  
   where JobStatusCode = 'NS'  
    
  update @TableResult  
     set Loaded = 'c'  
   where Loaded is null  
    
  select OutboundShipmentId,  
         DropSequence,  
         Route,  
         DespatchDate,  
         ExternalCompany,  
         Location,  
         min(InstructionType) as 'InstructionType',  
         JobId,  
         Status,  
         OrderNumber,  
         Pallet,  
         sum(Weight) as 'Weight',  
         Loaded,
         NumberOfPallets  
    from @TableResult  
  group by OutboundShipmentId,  
         DropSequence,  
         Route,  
         DespatchDate,  
         ExternalCompany,  
         Location,  
         JobId,  
         Status,  
         OrderNumber,  
         Pallet,  
         Loaded ,
         NumberOfPallets 
  order by OutboundShipmentId,  
           JobId  
end
