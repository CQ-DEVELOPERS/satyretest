﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Result_Select
  ///   Filename       : p_Result_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:09
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Result table.
  /// </remarks>
  /// <param>
  ///   @ResultId int = null 
  /// </param>
  /// <returns>
  ///   Result.ResultId,
  ///   Result.ResultCode,
  ///   Result.Result,
  ///   Result.Pass,
  ///   Result.StartRange,
  ///   Result.EndRange 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Result_Select
(
 @ResultId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Result.ResultId
        ,Result.ResultCode
        ,Result.Result
        ,Result.Pass
        ,Result.StartRange
        ,Result.EndRange
    from Result
   where isnull(Result.ResultId,'0')  = isnull(@ResultId, isnull(Result.ResultId,'0'))
  order by ResultCode
  
end
