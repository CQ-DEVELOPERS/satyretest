﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Invoice_CiplaMedpro
  ///   Filename       : p_Report_Invoice_CiplaMedpro.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Invoice_CiplaMedpro

as
begin
  set nocount on;
  
  declare @AutoIndex   int,
          @InvTotExcl  nvarchar(50),
          @InvDiscAmnt nvarchar(50)
  
  --select top 1 @AutoIndex = AutoIndex
  --  from invNum (nolock)
  -- where DocType = 4
  --   and DocState = 4
  --   and invNumber != ''
  --   and isnull(ubIDInvcqprinted,-1) = 0
  
  --update invNum
  --   set ubIDInvcqprinted = 1
  --  from invNum
  -- where AutoIndex = @AutoIndex
  
  --select @InvTotExcl   = sum(convert(numeric(13,2),fQtyLastProcessLineTotExclNoDisc)),
  --       @InvDiscAmnt   = sum(convert(numeric(13,2),fQtyLastProcessLineTotExclNoDisc)) - sum(convert(numeric(13,2),fQtyLastProcessLineTotExcl))
  -- from _btblInvoiceLines (nolock)
  -- where iInvoiceId = @AutoIndex
  
  select i.AutoIndex,
         i.Ordernum                              as 'OrderNumber',
         i.invNumber                             as 'InvoiceNumber',
         i.invDate                               as 'InvoiceDate',
         replace(i.Address1,'&','&amp;')         as 'CustomerAddress1',
         replace(i.Address2,'&','&amp;')         as 'CustomerAddress2',
         replace(i.address3,'&','&amp;')         as 'CustomerAddress3',
         replace(i.Address4,'&','&amp;')         as 'CustomerAddress4',
         replace(i.Address5,'&','&amp;')         as 'CustomerAddress5',
         replace(i.Address6,'&','&amp;')         as 'CustomerAddress6',
         replace(i.Paddress1,'&','&amp;')        as 'DeliveryAddress1',
         replace(i.Paddress2,'&','&amp;')        as 'DeliveryAddress2',
         replace(i.Paddress3,'&','&amp;')        as 'DeliveryAddress3',
         replace(i.Paddress4,'&','&amp;')        as 'DeliveryAddress4',
         replace(i.Paddress5,'&','&amp;')        as 'DeliveryAddress5',
         replace(i.Paddress6,'&','&amp;')        as 'DeliveryAddress6',
         c.Account                               as 'AccountNumber',
         replace(c.Name,'&','&amp;')             as 'CustomerName',
         replace(i.ExtOrderNum,'&','&amp;')      as 'ReferenceNumber',
         c.CT                                    as 'ChargeTax',
         c.Tax_Number                            as 'TaxReference', 
         r.Code                                  as 'SalesCode',
         convert(numeric(13,2), i.invTotTax)     as 'Tax',
         convert(numeric(13,2), i.InvTotExcl)    as 'NettExcLogDataFee',
         convert(numeric(13,2), i.InvTotIncl)    as 'TotalDue',
         i.Message1,
         i.Message2,
         i.Message3,
         s.Code                                  as 'ProductCode',
         s.Description_1                         as 'Product',
         l.clotNumber                            as 'Batch',
         l.fqtyProcessed                         as 'Quantity',
         convert(numeric(13,2),l.fUnitPriceExcl) as 'Excluding',
         convert(numeric(13,2),l.fUnitPriceIncl) as 'Including',
         convert(numeric(13,2),l.fQtyLastProcessLineTotExclNoDisc) as 'NettValue',
         @InvTotExcl                             as 'TotalNettValue',
         @InvDiscAmnt                            as 'LogisticsDataFee'
    from invNum            i (nolock)
    join _btblInvoiceLines l (nolock) on l.iInvoiceId = i.AutoIndex
    join StkItem           s (nolock) on s.StockLink  = iStockCodeId
    join client            c (nolock) on c.dclink     = i.AccountId
    join SalesRep          r (nolock) on r.idSalesRep = i.DocRepId
   where i.AutoIndex = @AutoIndex
end
