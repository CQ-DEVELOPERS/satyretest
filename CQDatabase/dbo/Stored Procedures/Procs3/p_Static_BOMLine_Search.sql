﻿
CREATE procedure p_Static_BOMLine_Search
(
	@BOMHeaderId INT
)

as
begin
	 set nocount on;
	 
	SELECT		
			P.ProductId, 
			P.StatusId, 
			S.Status, 
			P.ProductCode, 
			P.Product, 
			P.Barcode, 
			P.MinimumQuantity, 
			P.ReorderQuantity, 
			P.MaximumQuantity, 
			P.CuringPeriodDays,           
			P.ShelfLifeDays, 
			P.QualityAssuranceIndicator,
			P.ProductType,
			P.OverReceipt,
			P.HostId,
			p.RetentionSamples,
			Bl.BOMLineId,
			Bp.Quantity
    FROM    BOMLine Bl 
		    Inner join BOMProduct Bp on Bl.BOMLineId = Bp.BOMLineId		    
			Inner join StorageUnit SU ON Bp.StorageUnitId = SU.StorageUnitId 
			Inner join Product AS P ON SU.ProductId = P.ProductId
            Inner join Status AS S ON P.StatusId = S.StatusId
    WHERE   Bp.LineNumber = 1
      AND   Bl.BOMHeaderId = @BOMHeaderId
end
