﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_StockTake_Search
  ///   Filename       : p_Static_StockTake_Search.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2011  
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_StockTake_Search
(	@connectionStringName	nvarchar(255) = null
	)

as
begin
	 set nocount on;
	 
  
SELECT		StockTakeReferenceId, WarehouseId, StockTakeType, StartDate, EndDate, Reference
FROM        StockTakeReference
where		EndDate is null
Order By	StockTakeReferenceId desc


end
