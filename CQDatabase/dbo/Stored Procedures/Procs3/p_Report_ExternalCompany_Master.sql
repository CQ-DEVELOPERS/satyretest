﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_ExternalCompany_Master
  ///   Filename       : p_Report_ExternalCompany_Master.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2012
  /// </summary>
  /// <remarks>
  ///  
 
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_ExternalCompany_Master

as
begin
	 set nocount on;
	 
  select ec.ExternalCompany,
         ec.ExternalCompanyCode,
         ect.ExternalCompanyTypeCode,
         pc.PricingCategoryCode,
         lc.LabelingCategoryCode,
         ct.ContainerTypeCode,
         r.RouteCode,
         ec.HostId,
         ec.Rating,
         ec.Backorder,
         ec.RedeliveryIndicator,
         ec.QualityAssuranceIndicator,
         ec.OrderLineSequence,
         pn.PrincipalCode,
         pn.Address1,
         pn.Address2,
         pn.Address3,
         pn.Address4
    from ExternalCompany ec (nolock) 
    join ExternalCompanyType ect (nolock) on ec.ExternalCompanyTypeId = ect.ExternalCompanyTypeId
    left
    join Route                 r (nolock) on ec.RouteId = r.RouteId
    left
    join ContainerType        ct (nolock) on ec.ContainerTypeId = ct.ContainerTypeId
    left
    join LabelingCategory     lc (nolock) on ec.LabelingCategoryId = lc.LabelingCategoryId
    left 
    join PricingCategory      pc (nolock) on ec.PricingCategoryId = pc.PricingCategoryId
    left
    join Principal            pn (nolock) on ec.PrincipalId = pn.PrincipalId
end

 
