﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ShipmentIssue_List
  ///   Filename       : p_ShipmentIssue_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:52
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ShipmentIssue table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ShipmentIssue.ShipmentId,
  ///   ShipmentIssue.IssueId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ShipmentIssue_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ShipmentId
        ,null as 'ShipmentIssue'
        ,'-1' as IssueId
        ,null as 'ShipmentIssue'
  union
  select
         ShipmentIssue.ShipmentId
        ,ShipmentIssue.ShipmentId as 'ShipmentIssue'
        ,ShipmentIssue.IssueId
        ,ShipmentIssue.IssueId as 'ShipmentIssue'
    from ShipmentIssue
  
end
