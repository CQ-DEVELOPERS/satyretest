﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockOnHandCompare_Insert
  ///   Filename       : p_StockOnHandCompare_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:43
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StockOnHandCompare table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null,
  ///   @ComparisonDate datetime = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @Quantity float = null,
  ///   @Sent bit = null,
  ///   @UnitPrice numeric(13,3) = null,
  ///   @WarehouseCode varchar(50) = null 
  /// </param>
  /// <returns>
  ///   StockOnHandCompare.WarehouseId,
  ///   StockOnHandCompare.ComparisonDate,
  ///   StockOnHandCompare.StorageUnitId,
  ///   StockOnHandCompare.BatchId,
  ///   StockOnHandCompare.Quantity,
  ///   StockOnHandCompare.Sent,
  ///   StockOnHandCompare.UnitPrice,
  ///   StockOnHandCompare.WarehouseCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockOnHandCompare_Insert
(
 @WarehouseId int = null,
 @ComparisonDate datetime = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @Quantity float = null,
 @Sent bit = null,
 @UnitPrice numeric(13,3) = null,
 @WarehouseCode varchar(50) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert StockOnHandCompare
        (WarehouseId,
         ComparisonDate,
         StorageUnitId,
         BatchId,
         Quantity,
         Sent,
         UnitPrice,
         WarehouseCode)
  select @WarehouseId,
         @ComparisonDate,
         @StorageUnitId,
         @BatchId,
         @Quantity,
         @Sent,
         @UnitPrice,
         @WarehouseCode 
  
  select @Error = @@Error
  
  
  return @Error
  
end
