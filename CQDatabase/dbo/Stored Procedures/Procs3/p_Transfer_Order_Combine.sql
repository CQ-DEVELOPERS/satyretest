﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Transfer_Order_Combine
  ///   Filename       : p_Transfer_Order_Combine.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Transfer_Order_Combine
(
 @outboundShipmentId int = null,
 @issueId            int,
 @firstIssueId       int
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
  (
   OutboundShipmentId              int,
   IssueId                         int,
   IssueLineId                     int,
   InstructionId                   int,
   JobId                           int
  )
  
  declare @Error             int = 0,
          @Errormsg          nvarchar(500) = 'Error executing p_Transfer_Order_Combine',
          @JobId             int
  
  if @outboundShipmentId = -1
    set @outboundShipmentId = null
  
  if @issueId = -1
    set @issueId = null
  
  if @firstIssueId = -1
    set @firstIssueId = null
  
  if @issueId = @firstIssueId
    return
  
  select top 1 @JobId = ins.JobId
    from IssueLineInstruction ili (nolock)
    join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
   where ili.IssueId = @firstIssueId
  
  begin transaction
  
  update ins
     set JobId = @JobId
    from IssueLineInstruction ili (nolock)
    join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
   where ili.IssueId = @issueId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
