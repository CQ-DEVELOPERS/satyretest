﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_viewSUL_Search
  ///   Filename       : p_viewSUL_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 May 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_viewSUL_Search
(
 @WarehouseId  int,
 @ProductCode  nvarchar(30) = null,
 @Product      nvarchar(50) = null,
 @SKUCode      nvarchar(50) = null,
 @SKU          nvarchar(50) = null,
 @FromLocation nvarchar(15) = null,
 @ToLocation   nvarchar(15) = null
)

as
begin
	 set nocount on;
  
  if @ProductCode in ('-1','')
    set @ProductCode = null;
  
  if @Product in ('-1','')
    set @Product = null;
  
  if @SKUCode in ('-1','')
    set @SKUCode = null;
  
  if @SKU in ('-1','')
    set @SKU = null;
  
  if @FromLocation in ('-1','')
    set @FromLocation = null;
  
  if @ToLocation in ('-1','')
    set @ToLocation = null;
  
  select su.StorageUnitId,
         sul.LocationId,
         a.Area,
         l.Location,
         p.Product,
         p.ProductCode,
         p.Barcode,
         sku.SKUCode,
         sku.SKU
    from StorageUnit          su (nolock)
    join Product               p (nolock) on su.ProductId     = p.ProductId
    join SKU                 sku (nolock) on su.SKUId         = sku.SKUId
    left
    join StorageUnitLocation sul (nolock) on su.StorageUnitId = sul.StorageUnitId
    left
    join Location              l (nolock) on sul.LocationId   = l.LocationId
                                         and l.Location between isnull(@FromLocation, Location) and isnull(@ToLocation, Location)
    left
    join AreaLocation         al (nolock) on l.LocationId     = al.LocationId
    left
    join Area                  a (nolock) on al.AreaId        = a.AreaId
                                         and a.AreaCode in ('RK','BK','SP','R','PK','SP')
                                         and a.WarehouseId = @WarehouseId
   where isnull(ProductCode,'%') like '%' + isnull(@ProductCode, ProductCode) + '%'
     and isnull(Product,'%')     like '%' + isnull(@Product, Product) + '%'
     and isnull(SKUCode,'%')   like '%' + isnull(@SKUCode, SKUCode) + '%'
     and isnull(SKU,'%')       like '%' + isnull(@SKU, SKU) + '%'
   order by Location, Product
end
