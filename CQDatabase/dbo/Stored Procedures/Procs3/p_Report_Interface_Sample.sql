﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Interface_Sample
  ///   Filename       : p_Report_Interface_Sample.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Apr 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Interface_Sample
(
 @interfaceMappingFileId int
)

as
begin
  set nocount on;
  
  create table #TableSample
  (
   Id        int identity
  ,Col0      nvarchar(max)
  )
  
  declare @Max                    int,
          @ColumnNumber           int = 0,
          @Header                 nvarchar(max),
          @Detail                 nvarchar(max),
          @InterfaceMappingColumn nvarchar(50),
          @Delimiter              nvarchar(2),
          @InterfaceFileTypeCode  nvarchar(30),
          @Command                nvarchar(800),
          @StartPosition          int,
          @Length                 int,
          @InterfaceType nvarchar(30)
  
  select @InterfaceFileTypeCode = InterfaceFileTypeCode
    from InterfaceMappingFile imf
    join InterfaceFileType ift on imf.InterfaceFileTypeId = ift.InterfaceFileTypeId
   where imf.InterfaceMappingFileId = @interfaceMappingFileId
  
  if @InterfaceFileTypeCode = 'TXT'
  begin
    insert #TableSample
          (Col0)
    select null
    union all
    select null
    
    -- minus one because it's zero based
    select @Max = MAX(ColumnNumber) from InterfaceMappingColumn (nolock) where InterfaceMappingFileId = @interfaceMappingFileId
    select @Delimiter = Delimiter from InterfaceMappingFile (nolock) where InterfaceMappingFileId = @interfaceMappingFileId
    
    if @Max > 69
      set @Max = 69
    
    while @ColumnNumber < @Max
    begin
      select @InterfaceMappingColumn = InterfaceMappingColumn
        from InterfaceMappingColumn (nolock)
       where InterfaceMappingFileId = @interfaceMappingFileId
         and ColumnNumber = @ColumnNumber
      
      if @ColumnNumber > 0
      begin
        select @Command = 'alter table #TableSample add Col' + CONVERT(nvarchar(10), @ColumnNumber) + ' nvarchar(255) null'
        exec (@Command)
      end
      
      if @ColumnNumber <= @Max
      begin
        if @ColumnNumber + 1 = @Max
        begin
          select @Command = 'update #TableSample set Col' + CONVERT(nvarchar(10), @ColumnNumber) + ' = ''' + @InterfaceMappingColumn + ''' where Id = 1'
        end
        else
        begin
          select @Command = 'update #TableSample set Col' + CONVERT(nvarchar(10), @ColumnNumber) + ' = ''' + @InterfaceMappingColumn + ','' where Id = 1'
        end
        --select @Command = 'insert #TableSample (Col0) select ''' + @InterfaceMappingColumn + ''''
        exec (@Command)
        
        select @Command = 'update #TableSample set Col' + CONVERT(nvarchar(10), @ColumnNumber) + ' = '','' where Id = 2'
        --select @Command = 'insert #TableSample (Col0) select ''' + @InterfaceMappingColumn + ''''
        exec (@Command)
        
        if @ColumnNumber = 0
        begin
          select @Header = @InterfaceMappingColumn
          select @Detail = ''
        end
        else
        begin
          select @Header = @Header + @Delimiter + @InterfaceMappingColumn
          select @Detail = @Detail + @Delimiter
        end
      end
      
      select @ColumnNumber = @ColumnNumber + 1
    end
  end
  
  if @InterfaceFileTypeCode = 'FIX'
  begin
    insert #TableSample
          (Col0)
    select null
    
    -- minus one because it's zero based
    select @Max = MAX(ColumnNumber) -1 from InterfaceMappingColumn (nolock) where InterfaceMappingFileId = @interfaceMappingFileId
    select @Delimiter = Delimiter from InterfaceMappingFile (nolock) where InterfaceMappingFileId = @interfaceMappingFileId
    
    if @Max > 69
      set @Max = 69
    
    while @ColumnNumber < @Max
    begin
      select @InterfaceMappingColumn = InterfaceMappingColumn,
             @StartPosition = StartPosition,
             @Length        = EndPostion
        from InterfaceMappingColumn (nolock)
       where InterfaceMappingFileId = @interfaceMappingFileId
         and ColumnNumber = @ColumnNumber
      
      if @ColumnNumber > 0
      begin
        select @Command = 'alter table #TableSample add Col' + CONVERT(nvarchar(10), @ColumnNumber) + ' nvarchar(255) null'
        exec (@Command)
      end
      
      if @ColumnNumber <= @Max
      begin
        --select @Command = 'update #TableSample set Col' + CONVERT(nvarchar(10), @ColumnNumber) + ' = ''' + @InterfaceMappingColumn + ''' where Id = 1'
        ----select @Command = 'insert #TableSample (Col0) select ''' + @InterfaceMappingColumn + ''''
        --exec (@Command)
        
        select @Command = 'update #TableSample set Col' + CONVERT(nvarchar(10), @ColumnNumber) + ' = ''' + REPLICATE(CONVERT(nvarchar(10), @ColumnNumber),@length) + ''' where Id = 2'
        --select @Command = 'insert #TableSample (Col0) select ''' + @InterfaceMappingColumn + ''''
        exec (@Command)
        
        if @ColumnNumber = 0
        begin
          select @Header = @InterfaceMappingColumn
          select @Detail = ''
        end
        else
        begin
          select @Header = @Header + @Delimiter + @InterfaceMappingColumn
          select @Detail = @Detail + @Delimiter
        end
      end
      
      select @ColumnNumber = @ColumnNumber + 1
    end
  end
  
  if @InterfaceFileTypeCode = 'XML'
  begin
    
    select @InterfaceType = InterfaceType
      from InterfaceMappingFile imf
      join InterfaceDocumentType idt on imf.InterfaceDocumentTypeId = idt.InterfaceDocumentTypeId
     where InterfaceMappingFileId = @interfaceMappingFileId
    
    if @InterfaceType = 'PRD'
      insert #TableSample
            (Col0)
      select '<?xml version="1.0" encoding="utf-16"?>
<root>
	<Header>
		<Source>CustomerName</Source>
		<Target>Cquential</Target>
		<CreateDate>2011-12-31 23:59:59</CreateDate>
		<FileType>ProductMaster</FileType>
	</Header>
	<Body>
		<Item>
			<ItemType>PRD</ItemType>
			<ItemStatus>N</ItemStatus>
			<InterfaceTable>InterfaceImportProduct</InterfaceTable>
			<InterfaceTableId>0</InterfaceTableId>
			<PrimaryKey>10-6404-206</PrimaryKey>
			<ProductCode>10-6404-206</ProductCode>
			<Product>MILK SHAKE MIX 5L</Product>
			<SKUCode>EU</SKUCode>
			<SKU>1</SKU>
			<PalletQuantity>100</PalletQuantity>
			<Barcode>96001310006604</Barcode>
			<MinimumQuantity>100</MinimumQuantity>
			<ReorderQuantity>10</ReorderQuantity>
			<MaximumQuantity>0</MaximumQuantity>
			<CuringPeriodDays>100</CuringPeriodDays>
			<ShelfLifeDays>30</ShelfLifeDays>
			<QualityAssuranceIndicator>TRUE</QualityAssuranceIndicator>
			<ProductType>L</ProductType>
			<ProductCategory>Milk Shake</ProductCategory>
			<OverReceipt>10</OverReceipt>
			<RetentionSamples>1</RetentionSamples>
			<ItemLine>
				<InterfaceTable>InterfaceImportPack</InterfaceTable>
				<InterfaceTableId>0</InterfaceTableId>
				<ForeignKey>10-6404-206</ForeignKey>
				<PackCode/>
				<PackDescription>5L</PackDescription>
				<Quantity>1</Quantity>
				<Barcode>96001310006604</Barcode>
				<Length>1.000</Length>
				<Height>1.000</Height>
				<Volume>1.000</Volume>
				<NettWeight>1.000</NettWeight>
				<GrossWeight>1.000</GrossWeight>
				<TareWeight>0.000</TareWeight>
				<ProductCategory/>
				<PackingCategory/>
				<PickEmpty/>
				<StackingCategory/>
				<MovementCategory/>
				<ValueCategory/>
				<StoringCategory/>
				<PickPartPallet/>
			</ItemLine>
		</Item>
	</Body>
</root>'
    
    if @InterfaceType = 'SUP'
      insert #TableSample
            (Col0)
      select '<?xml version="1.0" encoding="utf-16"?>
<root>
  <Header>
    <Source>CustomerName</Source>
    <Target>Cquential</Target>
    <CreateDate>2011-12-31 23:59:59</CreateDate>
    <FileType>SupplierMaster</FileType>
  </Header>
  <Body>
    <Item>
      <ItemType>SM</ItemType>
      <ItemStatus>N</ItemStatus>
      <InterfaceTable>InterfaceSupplier</InterfaceTable>
      <InterfaceTableId>0</InterfaceTableId>
      <PrimaryKey>ADT03ADT03</PrimaryKey>
      <SupplierCode>ADT03ADT03</SupplierCode>
      <SupplierName>STEERS GATEWAY ALBERTON D/THRU</SupplierName>
      <Address1>ALBERTON D/THRU</Address1>
      <Address2>C/O DIAMOND STEP PROPERTIES</Address2>
      <Address3>GATEWAY CENTRE</Address3>
      <Address4>RING ROAD EAST  ALBERTON</Address4>
      <ContactPerson>DENNIS/LEANNE/082 925 7987</ContactPerson>
      <Phone>0119074287</Phone>
      <Fax>0126534805</Fax>
      <Email></Email>
      <ItemLine>
        <InterfaceTable>InterfaceSuppli ‘ external source’roduct </InterfaceTable>
        <InterfaceTableId>0</InterfaceTableId>
        <ForeignKey>ADT03ADT03</ForeignKey>
        <ProductCode>10-6404-206</ProductCode>
      </ItemLine>
    </Item>
  </Body>
</root>'
    
    if @InterfaceType = 'CUS'
      insert #TableSample
            (Col0)
      select '<?xml version="1.0" encoding="utf-16"?>
<root>
  <Header>
    <Source>CustomerName</Source>
    <Target>Cquential</Target>
    <CreateDate>2011-12-31 23:59:59</CreateDate>
    <FileType>CustomerMaster</FileType>
  </Header>
  <Body>
    <Item>
      <ItemType>CM</ItemType>
      <ItemStatus>N</ItemStatus>
      <InterfaceTable>InterfaceImportCustomer</InterfaceTable>
      <InterfaceTableId>0</InterfaceTableId>
      <PrimaryKey>ADT03ADT03</PrimaryKey>
      <CustomerCode>ADT03ADT03</CustomerCode>
      <CustomerName>STEERS GATEWAY ALBERTON D/THRU</CustomerName>
      <Class></Class>
      <DeliveryPoint>ADT03ADT03</DeliveryPoint>
      <Address1>ALBERTON D/THRU</Address1>
      <Address2>C/O DIAMOND STEP PROPERTIES</Address2>
      <Address3>GATEWAY CENTRE</Address3>
      <Address4>RING ROAD EAST  ALBERTON</Address4>
      <Modified>Yes</Modified>
      <ContactPerson>DENNIS/LEANNE/082 925 7987</ContactPerson>
      <Phone>0119074287</Phone>
      <Fax>0126534805</Fax>
      <Email></Email>
      <DeliveryGroup>GENERAL</DeliveryGroup>
      <DepotCode>01</DepotCode>
      <VisitFrequency></VisitFrequency>
    </Item>
  </Body>
</root>'
    
    if @InterfaceType = 'LOT'
      insert #TableSample
            (Col0)
      select '<?xml version="1.0" encoding="utf-16"?>
<root>
  <Header>
    <Source>CustomerName</Source>
    <Target>Cquential</Target>
    <CreateDate>2011-12-31 23:59:59</CreateDate>
    <FileType>LotMaster</FileType>
  </Header>
  <Body>
    <Item>
      <ItemType>LOT</ItemType>
      <ItemStatus>N</ItemStatus>
      <InterfaceTable>InterfaceImportLot</InterfaceTable>
      <InterfaceTableId>0</InterfaceTableId>
      <PrimaryKey>ADT03ADT03</PrimaryKey>
      <Lot>ADT03ADT03</Lot>
      <WarehouseCode>WMS1</WarehouseCode>
      <QuantityOnHand>137</QuantityOnHand>
      <QuantityFree>137</QuantityFree>
      <Status>Active</Status>
      <ExpiryDate>2014-03-18</ExpiryDate>
      <ParentProductCode>FXA021350</ParentProductCode>
      <ExpectedYield>980</ExpectedYield>
    </Item>
  </Body>
</root>'
    
    if @InterfaceType = 'ORD'
      insert #TableSample
            (Col0)
      select '<?xml version="1.0" encoding="utf-16"?>
<root>
  <Header>
    <Source>CustomerName</Source>
    <Target>Cquential</Target>
    <CreateDate>2011-12-31 23:59:59</CreateDate>
    <FileType>PurchaseOrder</FileType>
  </Header>
  <Body>
    <Item>
      <ItemType>PO</ItemType>
      <ItemStatus>N</ItemStatus>
      <InterfaceTable>InterfaceImportHeader</InterfaceTable>
      <InterfaceTableId>0</InterfaceTableId>
      <PrimaryKey>PO1234</PrimaryKey>
      <OrderNumber>PO1234</OrderNumber>
      <InvoiceNumber></InvoiceNumber>
      <CompanyCode>CMP01</CompanyCode>
      <Company>Supplier1</Company>
      <Address>1 First Street</Address>
      <FromWarehouseCode>WMS1</FromWarehouseCode>
      <ToWarehouseCode></ToWarehouseCode>
      <Route></Route>
      <DeliveryNoteNumber></DeliveryNoteNumber>
      <ContainerNumber></ContainerNumber>
      <SealNumber></SealNumber>
      <DeliveryDate>2011-12-31</DeliveryDate>
      <Remarks></Remarks>
      <NumberOfLines>1</NumberOfLines>
      <VatPercentage></VatPercentage>
      <VatSummary></VatSummary>
      <Total></Total>
      <Additional1></Additional1>
      <Additional2></Additional2>
      <Additional3></Additional3>
      <Additional4></Additional4>
      <Additional5></Additional5>
      <Additional6></Additional6>
      <Additional7></Additional7>
      <Additional8></Additional8>
      <Additional9></Additional9>
      <Additional10></Additional10>
      <ItemLine>
        <InterfaceTable>InterfaceImportDetail</InterfaceTable>
        <InterfaceTableId>0</InterfaceTableId>
        <ForeignKey>PO1234</ForeignKey>
        <LineNumber>1</LineNumber>
        <ProductCode>10-6404-206</ProductCode>
        <Product>MILK SHAKE MIX 5L</Product>
        <SKUCode>EA</SKUCode>
        <Batch>1010</Batch>
        <Quantity>1</Quantity>
        <Weight>1.000</Weight>
        <RetailPrice>0.00</RetailPrice>
        <NetPrice>0.00</NetPrice>
        <LineTotal>0.00</LineTotal>
        <Volume>0.00</Volume>
        <Additional1></Additional1>
        <Additional2></Additional2>
        <Additional3></Additional3>
        <Additional4></Additional4>
        <Additional5></Additional5>
        <Additional6></Additional6>
        <Additional7></Additional7>
        <Additional8></Additional8>
        <Additional9></Additional9>
        <Additional10></Additional10>
      </ItemLine>
    </Item>
  </Body>
</root>'
    
    if @InterfaceType = 'ORDSEND'
      insert #TableSample
            (Col0)
      select '<?xml version="1.0" encoding="utf-16"?>
<root>
  <Header>
    <Source>Cquential</Source>
    <Target>CustomerName</Target>
    <CreateDate>2011-12-31 23:59:59</CreateDate>
    <FileType>PurchaseOrderConfirmation</FileType>
  </Header>
  <Body>
    <Item>
      <ItemType>PO</ItemType>
      <ItemStatus>N</ItemStatus>
      <InterfaceTable>InterfaceExportHeader</InterfaceTable>
      <InterfaceTableId>0</InterfaceTableId>
      <PrimaryKey>PO1234</PrimaryKey>
      <OrderNumber>PO1234</OrderNumber>
      <InvoiceNumber></InvoiceNumber>
      <CompanyCode>CMP01</CompanyCode>
      <Company>Supplier1</Company>
      <Address>1 First Street</Address>
      <FromWarehouseCode>WMS1</FromWarehouseCode>
      <ToWarehouseCode></ToWarehouseCode>
      <Route></Route>
      <DeliveryNoteNumber></DeliveryNoteNumber>
      <ContainerNumber></ContainerNumber>
      <SealNumber></SealNumber>
      <DeliveryDate>2011-12-31</DeliveryDate>
      <Remarks></Remarks>
      <NumberOfLines>1</NumberOfLines>
      <Additional1></Additional1>
      <Additional2></Additional2>
      <Additional3></Additional3>
      <Additional4></Additional4>
      <Additional5></Additional5>
      <Additional6></Additional6>
      <Additional7></Additional7>
      <Additional8></Additional8>
      <Additional9></Additional9>
      <Additional10></Additional10>
      <ItemLine>
        <InterfaceTable>InterfaceExportDetail</InterfaceTable>
        <InterfaceTableId>0</InterfaceTableId>
        <ForeignKey>PO1234</ForeignKey>
        <LineNumber>1</LineNumber>
        <ProductCode>10-6404-206</ProductCode>
        <Product>MILK SHAKE MIX 5L</Product>
        <SKUCode>EA</SKUCode>
        <Batch>1010</Batch>
        <Quantity>1</Quantity>
        <Weight>1.000</Weight>
        <Volume>0.00</Volume>
        <Additional1></Additional1>
        <Additional2></Additional2>
        <Additional3></Additional3>
        <Additional4></Additional4>
        <Additional5></Additional5>
        <Additional6></Additional6>
        <Additional7></Additional7>
        <Additional8></Additional8>
        <Additional9></Additional9>
        <Additional10></Additional10>
      </ItemLine>
    </Item>
  </Body>
</root>'
    
    if @InterfaceType = 'ADJSEND'
      insert #TableSample
            (Col0)
      select '<?xml version="1.0" encoding="utf-16"?>
<root>
  <Header>
    <Source>Cquential</Source>
    <Target>CustomerName</Target>
    <CreateDate>2011-12-31 23:59:59</CreateDate>
    <FileType>StockAdjustment</FileType>
  </Header>
  <Body>
    <Item>
      <ItemType>SA</ItemType>
      <ItemStatus>N</ItemStatus>
      <ItemLine>
        <InterfaceTable>InterfaceExportStockAdjustment</InterfaceTable>
        <InterfaceTableId>0</InterfaceTableId>
        <FromWarehouseCode>LOC1</FromWarehouseCode>
        <ToWarehouseCode>LOC2</ToWarehouseCode>
        <ProductCode>X1010</ProductCode>
        <Batch>8090889898</Batch>
        <SerialNumber>8090889898</SerialNumber>
        <SKUCode>EA</SKUCode>
        <Quantity>1</Quantity>
        <ReasonCode></ReasonCode>
        <Comments>(WMS Ref: JobId: 420166)</Comments>
      </ItemLine>
    </Item>
  </Body>
</root>
'
    
    if @InterfaceType = 'SOH'
      insert #TableSample
            (Col0)
      select '<?xml version="1.0" encoding="utf-16"?>
<root>
  <Header>
    <Source>CustomerName</Source>
    <Target>Cquential</Target>
    <CreateDate>2011-12-31 23:59:59</CreateDate>
    <FileType>StockOnHand</FileType>
  </Header>
  <Body>
    <Item>
      <ItemType>SOH</ItemType>
      <ItemStatus>N</ItemStatus>
      <ItemLine>
        <InterfaceTable>InterfaceImportSOH</InterfaceTable>
        <InterfaceTableId>0</InterfaceTableId>
        <ProductCode>X1010</ProductCode>
        <Batch>8090889898</Batch>
        <SerialNumber>8090889898</SerialNumber>
        <SKUCode>EA</SKUCode>
        <Warehouse>ZA90M</Warehouse>
        <Quantity>1</Quantity>
        <UnitPrice></UnitPrice>
      </ItemLine>
    </Item>
  </Body>
</root>'
    
    if @InterfaceType = 'CO'
      insert #TableSample
            (Col0)
      select ''
  end
    
    --select @Header as 'Data'
    --union all
    --select @Detail
    
  --create table #TableSample
  --(
  -- Col0         nvarchar(255)
  --,Col1         nvarchar(255)
  --,Col2     nvarchar(255)
  --,Col3     nvarchar(255)
  --,Col4     nvarchar(255)
  --,Col5     nvarchar(255)
  --,Col6     nvarchar(255)
  --,Col7     nvarchar(255)
  --,Col8     nvarchar(255)
  --,Col9     nvarchar(255)
  --,Col10     nvarchar(255)
  --,Col11     nvarchar(255)
  --,Col12     nvarchar(255)
  --,Col13     nvarchar(255)
  --,Col14     nvarchar(255)
  --,Col15     nvarchar(255)
  --,Col16     nvarchar(255)
  --,Col17     nvarchar(255)
  --,Col18     nvarchar(255)
  --,Col19     nvarchar(255)
  --,Col20     nvarchar(255)
  --,Col21     nvarchar(255)
  --,Col22     nvarchar(255)
  --,Col23     nvarchar(255)
  --,Col24     nvarchar(255)
  --,Col25     nvarchar(255)
  --,Col26     nvarchar(255)
  --,Col27     nvarchar(255)
  --,Col28     nvarchar(255)
  --,Col29     nvarchar(255)
  --,Col30     nvarchar(255)
  --,Col31     nvarchar(255)
  --,Col32     nvarchar(255)
  --,Col33     nvarchar(255)
  --,Col34     nvarchar(255)
  --,Col35     nvarchar(255)
  --,Col36     nvarchar(255)
  --,Col37     nvarchar(255)
  --,Col38     nvarchar(255)
  --,Col39     nvarchar(255)
  --,Col40     nvarchar(255)
  --,Col41     nvarchar(255)
  --,Col42     nvarchar(255)
  --,Col43     nvarchar(255)
  --,Col44     nvarchar(255)
  --,Col45     nvarchar(255)
  --,Col46     nvarchar(255)
  --,Col47     nvarchar(255)
  --,Col48     nvarchar(255)
  --,Col49     nvarchar(255)
  --,Col50     nvarchar(255)
  --,Col51     nvarchar(255)
  --,Col52     nvarchar(255)
  --,Col53     nvarchar(255)
  --,Col54     nvarchar(255)
  --,Col55     nvarchar(255)
  --,Col56     nvarchar(255)
  --,Col57     nvarchar(255)
  --,Col58     nvarchar(255)
  --,Col59     nvarchar(255)
  --,Col60     nvarchar(255)
  --,Col61     nvarchar(255)
  --,Col62     nvarchar(255)
  --,Col63     nvarchar(255)
  --,Col64     nvarchar(255)
  --,Col65     nvarchar(255)
  --,Col66     nvarchar(255)
  --,Col67     nvarchar(255)
  --,Col68     nvarchar(255)
  --,Col69     nvarchar(255)
  --,Col70     nvarchar(255)
  --)

--SELECT r.StrVal
--FROM
--(
-- SELECT vl.ValueList as 'StrVal'
-- FROM (SELECT DISTINCT ColumnNumber FROM #TableSample) c1
-- CROSS APPLY(SELECT Col1 + ' ' AS [text()]
--             FROM #TableSample
--             WHERE ColumnNumber=c1.ColumnNumber
--             FOR XML PATH(''))vl(ValueList)
--)r
    
----;With #TableSample(Col1,ColumnNumber)AS
----(SELECT ROW_NUMBER() OVER(PARTITION BY ColumnNumber order by ColumnNumber),
----Col1, ColumnNumber
----FROM #TableSample)
--SELECT r.StrVal
--FROM
--(
-- SELECT dl.DateList AS StrVal,1 as Ord
-- FROM       (SELECT Col1 + ' ' AS [text()]
--             FROM #TableSample
--             WHERE ColumnNumber=1
--             FOR XML PATH(''))dl(DateList)
-- UNION ALL
-- SELECT RTRIM(vl.ValueList),2
-- FROM (SELECT DISTINCT ColumnNumber FROM #TableSample) c1
-- CROSS APPLY(SELECT CAST(Col1 AS varchar(50)) + ' ' AS [text()]
--             FROM #TableSample
--             WHERE ColumnNumber=c1.ColumnNumber
--             FOR XML PATH(''))vl(ValueList)
--)r
--ORDER BY r.Ord
  alter table #TableSample drop column Id
  select * from #TableSample
end
 
