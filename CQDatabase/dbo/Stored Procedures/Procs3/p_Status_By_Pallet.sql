﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_By_Pallet
  ///   Filename       : p_Status_By_Pallet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_By_Pallet

as
begin
	 set nocount on;
  
    select StatusId,
           Status,
           StatusId as 'PalletIdStatusId'
      from Status (nolock)
     where Type            = 'P'
       and StatusCode in ('AP', 'NA')
    order by Status
end
 
