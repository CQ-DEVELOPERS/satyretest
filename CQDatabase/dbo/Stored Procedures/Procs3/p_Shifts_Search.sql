﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shifts_Search
  ///   Filename       : p_Shifts_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:48
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Shifts table.
  /// </remarks>
  /// <param>
  ///   @ShiftId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Shifts.ShiftId,
  ///   Shifts.Starttime,
  ///   Shifts.EndTime 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shifts_Search
(
 @ShiftId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ShiftId = '-1'
    set @ShiftId = null;
  
 
  select
         Shifts.ShiftId
        ,Shifts.Starttime
        ,Shifts.EndTime
    from Shifts
   where isnull(Shifts.ShiftId,'0')  = isnull(@ShiftId, isnull(Shifts.ShiftId,'0'))
  
end
