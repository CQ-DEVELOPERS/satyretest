﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Short_Trend_Picks
  ///   Filename       : p_Report_Short_Trend_Picks.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 9 Sept 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Short_Trend_Picks
(
 @WarehouseId       int,
 @StorageUnitId     int = null,
 @OperatorId        int = null,
 @FromDate          datetime,
 @ToDate            datetime
)

as
begin
	 set nocount on;
  if   @StorageUnitId = -1
    set @StorageUnitId = null
    
  if @OperatorId = -1
    set @OperatorId = null
    
declare @SOH as table
  (
   StorageUnitId		int,	
   WarehouseId			int,
   SOH      			int
  )
 
 
 insert @SOH
       (StorageUnitId,
         WarehouseId,
         SOH) 
  select su.StorageUnitId,
		 a.WarehouseId,
		 sum (subl.ActualQuantity)
   from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Status                      s (nolock) on b.StatusId              = s.StatusId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId 
    where a.StockOnHand = 1
    group by su.StorageUnitId, 
              a.warehouseid
    order by su.StorageUnitId,
              a.warehouseid 
               
SELECT	I.InstructionId, 
		I.OperatorId, 
		I.Quantity, 
		I.ConfirmedQuantity, 
		ILI.Quantity - ILI.ConfirmedQuantity AS Short, 
		IT.InstructionType, 
		O.Operator, 
		vs.ProductCode, 
		vs.Product, 
		vs.SKU, 
		viewLocation.Location, 
		I.JobId,
		vsoh.SOH
FROM	Instruction AS I 
inner join IssueLineInstruction	    AS ILI on I.InstructionId = ILI.InstructionId 
inner join InstructionType			AS IT  on I.InstructionTypeId = IT.InstructionTypeId 
inner join Operator				    AS O   on I.OperatorId = O.OperatorId 
inner join StorageUnitBatch sub            on I.StorageUnitBatchId = sub.StorageUnitBatchId
inner join StorageUnit                 su  on sub.StorageUnitId    = su.StorageUnitId 
inner join viewStock                   vs  on I.StorageUnitBatchId = vs.StorageUnitBatchId 
inner join viewLocation                    on I.PickLocationId = viewLocation.LocationId
inner join @SOH                    vsoh    on vsoh.WarehouseId = @WarehouseId
						                   and vsoh.StorageUnitId = vs.StorageUnitId
WHERE	i.WarehouseId    = @WarehouseId
and		su.StorageUnitId = isnull(@StorageUnitId, su.StorageUnitId)
and		i.OperatorId     = isnull(@OperatorId, i.OperatorId)
and		i.EndDate  between @FromDate and @ToDate
and		ILI.Quantity       > ILI.ConfirmedQuantity
end
