﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Result_Insert
  ///   Filename       : p_Result_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:06
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Result table.
  /// </remarks>
  /// <param>
  ///   @ResultId int = null output,
  ///   @ResultCode nvarchar(20) = null,
  ///   @Result nvarchar(510) = null,
  ///   @Pass bit = null,
  ///   @StartRange float = null,
  ///   @EndRange float = null 
  /// </param>
  /// <returns>
  ///   Result.ResultId,
  ///   Result.ResultCode,
  ///   Result.Result,
  ///   Result.Pass,
  ///   Result.StartRange,
  ///   Result.EndRange 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Result_Insert
(
 @ResultId int = null output,
 @ResultCode nvarchar(20) = null,
 @Result nvarchar(510) = null,
 @Pass bit = null,
 @StartRange float = null,
 @EndRange float = null 
)

as
begin
	 set nocount on;
  
  if @ResultId = '-1'
    set @ResultId = null;
  
  if @ResultCode = '-1'
    set @ResultCode = null;
  
  if @Result = '-1'
    set @Result = null;
  
	 declare @Error int
 
  insert Result
        (ResultCode,
         Result,
         Pass,
         StartRange,
         EndRange)
  select @ResultCode,
         @Result,
         @Pass,
         @StartRange,
         @EndRange 
  
  select @Error = @@Error, @ResultId = scope_identity()
  
  
  return @Error
  
end
