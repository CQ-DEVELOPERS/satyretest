﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ShipmentIssueHistory_Insert
  ///   Filename       : p_ShipmentIssueHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:50
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ShipmentIssueHistory table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null,
  ///   @IssueId int = null,
  ///   @DropSequence smallint = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   ShipmentIssueHistory.ShipmentId,
  ///   ShipmentIssueHistory.IssueId,
  ///   ShipmentIssueHistory.DropSequence,
  ///   ShipmentIssueHistory.CommandType,
  ///   ShipmentIssueHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ShipmentIssueHistory_Insert
(
 @ShipmentId int = null,
 @IssueId int = null,
 @DropSequence smallint = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ShipmentIssueHistory
        (ShipmentId,
         IssueId,
         DropSequence,
         CommandType,
         InsertDate)
  select @ShipmentId,
         @IssueId,
         @DropSequence,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
