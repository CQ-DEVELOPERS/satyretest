﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Status_Rollup
  ///   Filename       : p_Status_Rollup.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Dec 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Status_Rollup
(
 @jobId int,
 @instructionId int = null
)

as
begin
  set nocount on;
  
  declare @TableIssues as table
  (
   AutoSendup         bit default 1,
   AutoCheck          bit default 0,
   OutboundShipmentId int null,
   OutboundDocumentId int,
   IssueId            int,
   IssueLineId        int,
   OutboundDocumentTypeCode nvarchar(10)
  )
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @StatusCode          nvarchar(10),
          @StatusId            int,
          @IssueId             int,
          @IssueLineId         int,
          @IsShipment          bit,
          @ReceiptId           int,
          @ReceiptLineId       int,
          @InstructionTypeCode nvarchar(10),
          @WarehouseId         int,
          @OutboundShipmentId  int,
          @rlStatusCode        nvarchar(10),
          @OutboundDocumentId  int,
          @AutoSendup          bit,
          @InboundDocumentId   int,
		        @OutboundDocumentTypeCode nvarchar(10),
		        @WaveId              int,
		        @TrackingNumber      nvarchar(30),
		        @DropSequence        int,
		        @AutoCheck           bit = 0
  
  set @IsShipment = 0
  
  select @GetDate = dbo.ufn_Getdate()
  set @Error = -1
  
--  select @StatusCode    = s.StatusCode,
--         @IssueLineId   = i.IssueLineId,
--         @ReceiptLineId = i.ReceiptLineId,
--         @InstructionTypeCode = it.InstructionTypeCode
--    from Job              j (nolock)
--    join Instruction      i (nolock) on j.JobId             = i.JobId
--    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
--    join Status           s (nolock) on j.StatusId          = s.StatusId
--   where j.JobId = @jobId
  
  select @StatusCode    = s.StatusCode,
         @DropSequence  = j.DropSequence
    from Job              j (nolock)
    join Status           s (nolock) on j.StatusId          = s.StatusId
   where j.JobId = @jobId
   
 -- if @StatusCode in ('QA')
	--exec p_create_QAAudit
 --        @jobId = @jobId
  
  select top 1
         @WarehouseId   = i.WarehouseId,
         @InstructionTypeCode = it.InstructionTypeCode
    from Instruction i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where i.JobId = @JobId
  
  select @IssueLineId   = IssueLineId,
         @ReceiptLineId = ReceiptLineId
    from Instruction (nolock)
   where InstructionId = @InstructionId
  
  if @IssueLineId is null and @ReceiptLineId is null
  begin
    select @IssueLineId   = IssueLineId,
           @ReceiptLineId = ReceiptLineId
      from Instruction (nolock)
     where JobId = @jobId
  end
  
  set @IssueLineId = null
  
  if @IssueLineId is null and @ReceiptLineId is null
  begin
    set @IsShipment = 1
    
    insert @TableIssues
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           IssueLineId)
    select distinct
           ili.OutboundShipmentId,
           ili.OutboundDocumentId,
           ili.IssueId,
           ili.IssueLineId
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
     where i.JobId = @JobId
    
    if @@rowcount = 0 -- Find using InstructionRefId
      insert @TableIssues
            (OutboundShipmentId,
             OutboundDocumentId,
             IssueId,
             IssueLineId)
      select distinct
             ili.OutboundShipmentId,
             ili.OutboundDocumentId,
             ili.IssueId,
             ili.IssueLineId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionRefId
       where i.JobId = @JobId
    
    insert @TableIssues
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           IssueLineId)
    select ili.OutboundShipmentId,
           ili.OutboundDocumentId,
           ili.IssueId,
           ili.IssueLineId
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
      join Job                    j (nolock) on i.JobId           = j.JobId
      join Status                sj (nolock) on j.StatusId        = sj.StatusId
      join IssueLine             il (nolock) on ili.IssueLineId   = il.IssueLineId
      join Status               sil (nolock) on il.StatusId       = sil.StatusId
      join @TableIssues          ti          on ili.IssueId = ti.IssueId
     where --exists(select 1 from @TableIssues t where ili.IssueId = t.IssueId)
           sj.StatusCode  = 'NS' -- Rollup No Stock job if IssueLine is in the same Status and current job 25/04/2008
       and sil.StatusCode = @StatusCode
    
    select @OutboundShipmentId = OutboundShipmentId,
           @IssueId            = IssueId
      from @TableIssues
  end
  else if @IssueLineId is not null
  begin
    select @IssueId = IssueId
      from IssueLine (nolock)
     where IssueLineId = @IssueLineId
    
    select @OutboundDocumentId = OutboundDocumentId
      from Issue (nolock)
     where IssueId = @IssueId
    
    insert @TableIssues
          (OutboundDocumentId,
           IssueId,
           IssueLineId)
    select @OutboundDocumentId,
           @IssueId,
           @IssueLineId
  end
  
  update @TableIssues
     set AutoSendup = odt.AutoSendup,
         AutoCheck  = isnull(odt.AutoCheck,0),
		       OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode
    from @TableIssues          ti
    join OutboundDocument      od (nolock) on ti.OutboundDocumentId     = od.OutboundDocumentId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  

  if @ReceiptLineId is not null
  begin
    select @StatusId = dbo.ufn_StatusId('R','R')
    
    select @ReceiptId    = ReceiptId,
           @rlStatusCode = s.StatusCode
      from ReceiptLine rl (nolock)
      join Status       s (nolock) on rl.StatusId = s.StatusId
     where rl.ReceiptLineId = @ReceiptLineId
    
    if @rlStatusCode = 'R'
    begin
      exec @Error = p_ReceiptLine_Update
       @ReceiptLineId = @ReceiptLineId,
       @StatusId      = @StatusId
      
      if @Error <> 0
        goto error
    end
  end
  
  if @ReceiptLineId is not null and @StatusCode = 'PR'
  begin
    select @StatusId = dbo.ufn_StatusId('R','A')
    
    exec @Error = p_Job_Update
     @JobId    = @JobId,
     @StatusId = @StatusId
    
    if @Error <> 0

      goto error
  end
  
  -- Can you rollup?
  if exists(select top 1 1
              from Instruction i (nolock)
              join Status      s (nolock) on i.StatusId = s.StatusId
             where i.JobId       = @jobId
               and s.StatusCode in ('W','S'))
  begin
    print 'return'
    set @Error = 0
    return;
  end
  
  if @ReceiptLineId is not null
  begin
    select @InstructionTypeCode = it.InstructionTypeCode
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where i.JobId = @jobId
    
    if @InstructionTypeCode = 'PR'
      select @StatusId = dbo.ufn_StatusId('PR','C')
    else
      select @StatusId = dbo.ufn_StatusId('R','R')
    
    exec @Error = p_Job_Update
     @jobId    = @jobId,
     @StatusId = @StatusId
    
    if @Error <> 0
      goto error
    
    --if not exists(select top 1 1
    --                from ReceiptLine rl (nolock)
    --                join Status       s (nolock) on rl.StatusId = s.StatusId
    --               where rl.ReceiptId = @ReceiptId
    --                 and s.StatusCode not in ('RC'))
    --begin
    --  exec @Error = p_Receipt_Update
    --   @ReceiptId = @ReceiptId,
    --   @StatusId  = @StatusId
      
    --  if @Error <> 0
    --    goto error
    --end
    
    --if not exists(select top 1 1
    --                from ReceiptLine rl (nolock)
    --                join Status       s (nolock) on rl.StatusId = s.StatusId
    --               where rl.ReceiptId = @ReceiptId
    --                 and s.StatusCode not in ('RC'))
    --begin
    --  exec @Error = p_BOM_Receipt_Insert
    --   @OriginalReceiptId = @ReceiptId
      
    --  if @Error <> 0
    --    goto error
    --end
    
    goto result
  end
  
  select top 1 @AutoCheck = 1 
    from Instruction i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnitArea sua (nolock) on sub.StorageUnitId = sua.StorageUnitId
    join Area a (nolock) on sua.AreaId = a.AreaId
   where i.JobId = @JobId
     and a.AutoStore = 1
     and a.AutoCheck = 1
  
  if exists(select top 1 1 from @TableIssues)
  begin
    if (dbo.ufn_Configuration(37, @WarehouseId) = 1 and @InstructionTypeCode = 'P') -- Rollup Full Picks without checking
    or 
       (@StatusCode in ('RL','S') and exists(select top 1 1 from @TableIssues where isnull(AutoCheck,0) = 1))
    or @AutoCheck = 1
    begin
      update Instruction
         set CheckQuantity = ConfirmedQuantity
       where JobId = @jobId
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
      
      set @StatusCode = 'CK'
    end
    
    -- Get the next status
    if dbo.ufn_Configuration(37, @WarehouseId) = 1 and @StatusCode in ('W','RL','M','SA','S')
      select @StatusId = dbo.ufn_StatusId('IS','CK'), @StatusCode = 'CK'
      
    else if dbo.ufn_Configuration(41, @WarehouseId) = 1 and @StatusCode in ('RL','M','SA','CK','QA')
      select @StatusId = dbo.ufn_StatusId('IS','WC'), @StatusCode = 'WC'
      
    else if dbo.ufn_Configuration(84, @WarehouseId) = 1 and @StatusCode in ('RL','M','SA','CK','WC')
      select @StatusId = dbo.ufn_StatusId('IS','CD'), @StatusCode = 'CD'
      
    else if dbo.ufn_Configuration(39, @WarehouseId) = 1 and @StatusCode in ('RL','M','SA','CK','WC','CD')
      select @StatusId = dbo.ufn_StatusId('IS','D'), @StatusCode = 'D'
      
    else if dbo.ufn_Configuration(40, @WarehouseId) = 1 and @StatusCode in ('RL','M','SA','CK','WC','CD','D')
      select @StatusId = dbo.ufn_StatusId('IS','DC'), @StatusCode = 'DC'
      
    else if dbo.ufn_Configuration(38, @WarehouseId) = 1 and @StatusCode in ('RL','M','SA','CK','WC','CD','D','DC')
      select @StatusId = dbo.ufn_StatusId('IS','C'), @StatusCode = 'C'
      
    -- Exception - if Authorising is on
    if @StatusCode = 'CK'
      if dbo.ufn_Configuration(36, @WarehouseId) = 1
        if (select sum(ili.Quantity)
              from IssueLineInstruction ili (nolock)
              join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
             where i.JobId = @jobId)
          !=
           (select sum(ConfirmedQuantity)
              from Instruction (nolock)

             where JobId = @jobId)
        begin
          select @StatusId = dbo.ufn_StatusId('IS','A'), @StatusCode = 'A'
        end
    
    -- Exception - if Quality Assurance is on
    if (dbo.ufn_Configuration(41, @WarehouseId) = 1 and @StatusCode = 'WC')
    or      (dbo.ufn_Configuration(41, @WarehouseId) = 0
         and dbo.ufn_Configuration(42, @WarehouseId) = 1
         and dbo.ufn_Configuration(84, @WarehouseId) = 1
         and @StatusCode in ('CD','D'))
      if dbo.ufn_Configuration(42, @WarehouseId) = 1
        if exists(select top 1 1
                    from Instruction (nolock) i
                    join Status      (nolock) s on i.StatusId = s.StatusId
                    left
                    join JobBOMParent (nolock) p on i.InstructionId = p.InstructionId
                   where i.JobId = @jobId
                     and i.ConfirmedQuantity != isnull(i.CheckQuantity,0)
                     and s.StatusCode        != 'NS'
                     and p.BOMInstructionId is null)

        begin
          select @StatusId = dbo.ufn_StatusId('IS','QA'), @StatusCode = 'QA'
        end
    
    if isnull(@OutboundShipmentId,-1) != -1
      set @TrackingNumber = 'NI' + REPLICATE('0',10 - len(@OutboundShipmentId)) + convert(nvarchar(10), @OutboundShipmentId) + REPLICATE('0', 3 - len(@DropSequence)) + convert(nvarchar(10), @DropSequence)
    else if isnull(@IssueId,-1) != -1
      set @TrackingNumber = 'NI' + REPLICATE('0',10 - len(@IssueId)) + convert(nvarchar(10), @IssueId) + REPLICATE('0', 3 - len(@DropSequence)) + convert(nvarchar(10), @DropSequence)
		
    if @AutoCheck = 1
    Begin
      --if @StatusCode = 'CK'
      --Begin
        set @StatusId = dbo.ufn_StatusId('IS','CD')
      --End
      --else if @StatusCode = 'CD'
      --Begin
      --  set @StatusId = dbo.ufn_StatusId('IS','D')
      --End
      --else
      --Begin
      --  set @StatusId = dbo.ufn_StatusId('IS','C')
      --End
    End
      
    
    exec @Error = p_Job_Update
     @jobId    = @jobId,
     @StatusId = @StatusId,
     @TrackingNumber = @TrackingNumber
    
    if @Error <> 0
      goto error
    
--    if @StatusCode = 'RL'
--      update OutboundPerformance
--         set Release = @Getdate
--       where JobId = @JobId
--         and Release is null
    if @StatusCode in ('NS')
      select @StatusId = dbo.ufn_StatusId('IS','CD'), @StatusCode = 'CD'
    else if @StatusCode = 'CK'
    begin
      update OutboundPerformance
         set Checking = @Getdate
       where JobId = @JobId
         and Checking is null
      
      -- LOUISE INSERT FOR DATA DRIVEN REPORT       
      --exec p_Packaging_Check_Insert @JobId
      

      --exec @Error = p_Wave_Planning_Move_Items_Putaway
      -- @PickJobId = @JobId
      
      --if @Error <> 0
      --  goto error
      
      update OutboundPerformance
         set EndDate = @Getdate
       where JobId = @JobId
         and EndDate is null
    end
    else if @StatusCode = 'CD'
    begin
      update OutboundPerformance
         set Checked = @Getdate
       where JobId = @JobId
         and Checked is null
      
      if dbo.ufn_Configuration(375, @WarehouseId) = 1
      begin

        exec @Error = p_Production_Back_Flush
         @JobId = @jobId
        
        if @Error <> 0
          goto error
      end
    end
    else if @StatusCode = 'D'
      update OutboundPerformance
         set Despatch = @Getdate
       where JobId = @JobId
         and Despatch is null
    else if @StatusCode = 'DC'
      update OutboundPerformance
         set DespatchChecked = @Getdate
       where JobId = @JobId
         and DespatchChecked is null
    else if @StatusCode = 'C'
    begin
      update OutboundPerformance
         set Complete = @Getdate
       where JobId = @JobId
         and Complete is null
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
      
      update i
         set FirstLoaded = @Getdate
        from Issue                  i (nolock)
        join IssueLineInstruction ili (nolock) on i.IssueId         = ili.IssueId
        join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
       where ins.JobId = @JobId

         and i.FirstLoaded is null
    end
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    ------------------------------------------------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------------------------------------------------
    exec @Error = p_Status_Rollup_Issue @jobId = @jobId
    
    if @Error <> 0
      goto error
    
    if @StatusId is not null and @IsShipment = 1
    begin
      while exists(select top 1 1 from @TableIssues)
      begin
        select @IssueId                  = IssueId,
               @IssueLineId              = IssueLineId,
               @AutoSendup               = AutoSendup,
			   @OutboundDocumentTypeCode = OutboundDocumentTypeCode
          from @TableIssues
        
        delete @TableIssues
         where IssueId = @IssueId
           --and IssueLineId = @IssueLineId
      
        if (select s.StatusCode
              from Issue  i (nolock)
              join Status s (nolock) on i.StatusId = s.StatusId
             where i.IssueId = @IssueId) in ('CD','D','DC','C')
        begin
          /**** Kitting change ****/
          select @OutboundDocumentId = OutboundDocumentId,
                 @InboundDocumentId = null
            from Issue (nolock)
           where IssueId = @IssueId
          
          select @InboundDocumentId = id.InboundDocumentId
            from InboundDocument  id (nolock)
            join BOMInstruction   bi (nolock) on id.InboundDocumentId = bi.InboundDocumentId
            join OutboundDocument od (nolock) on bi.OutboundDocumentId = od.OutboundDocumentId
          where od.OutboundDocumentId = @OutboundDocumentId
          
          exec @error = p_BOM_Status_Rollup @IssueId                   
          
          if @InboundDocumentId IS NOT NULL
          begin
            set @ReceiptId = null
            
            select @ReceiptId = ReceiptId,

                   @StatusId  = dbo.ufn_StatusId('R','W')
              from Receipt (nolock)
             where InboundDocumentId = @InboundDocumentId
               and StatusId = dbo.ufn_StatusId('R','OH')
            
            if @ReceiptId is not null
              exec @Error = p_Receipt_Update_Status @ReceiptId, @StatusId
          end                 
          /**** End kitting ****/
          
          exec @Error = p_Bill_Insert_Issue
           @IssueId = @IssueId
          
          if @Error <> 0
            goto error

		  exec @Error = p_Checking_StoreLocation_Clear
			@IssueId = @IssueId
                
		  if @Error <> 0
            goto error
          
    
    if @OutboundDocumentTypeCode = 'COL'
      Begin
      Declare @COLLocationId      int,
			  @COLStorageUnitId   int
		 
		 select @COLLocationId    = i.StoreLocationId,
		        @COLStorageUnitId =  i.StorageUnitBatchId
		 from Instruction i 
		 where JobId = @jobId
      
         update StorageUnitBatchLocation
         set ActualQuantity = 0
         where LocationId = @COLLocationId
         and   StorageUnitBatchId = @COLStorageUnitId
      End
    
          
          if @AutoSendup = 1
          begin
            
            if dbo.ufn_Configuration(403, @WarehouseId) = 1
            begin
              exec @Error = p_Interface_Export_Trip_Insert
               @IssueId = @IssueId
              
              if @Error <> 0
                goto error
            end
            else
            begin
              exec @Error = p_Issue_Update_ili
               @IssueId = @IssueId
              
              if @Error <> 0
                goto error
              
              exec @Error = p_Interface_Export_SO_Insert
               @IssueId = @IssueId
              
              if @Error <> 0
                goto error
            end
            
            if dbo.ufn_Configuration(141, @WarehouseId) = 1
            begin
              select @OutboundShipmentId = OutboundShipmentId
                from OutboundShipmentIssue (nolock)
               where IssueId = @IssueId
              
              exec @Error = p_Receive_By_Label_Insert
               @OutboundShipmentId = @OutboundShipmentId,
               @IssueId            = @IssueId
              
              if @Error <> 0
                goto error
            end
          end
          
		        if @OutboundDocumentTypeCode = 'WAV'
		        begin
		          select @WaveId = WaveId
			           from Issue (nolock)
			          where IssueId = @IssueId
            
		          exec @Error = p_Wave_Update
             @WaveId       = @WaveId,
             @CompleteDate = @GetDate
            
            if @Error <> 0  
              goto error
            
		          --exec @Error = p_Wave_Planning_Release_Linked
            -- @WaveId = @WaveId
            
            --if @Error <> 0  
            --  goto error

		        end
		        --else
		        --begin
		        --  select @WaveId = WaveId
			       --    from Issue (nolock)
			       --   where IssueId = @IssueId
			       --  select @WaveId
			       --  select *
			       --                  from Issue  i (nolock)
			       --                  join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
			       --                  join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
			       --                                                        and odt.OutboundDocumentTypeCode != 'WAV'
			       --                  join Status s (nolock) on i.StatusId = s.StatusId
			       --                 where i.WaveId = @WaveId
			       --                   and s.StatusCode in ('SA','RL','S','CK')
			         
			       --  if @WaveId is not null
			       --  if not exists(select 1 
			       --                  from Issue  i (nolock)
			       --                  join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
			       --                  join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
			       --                                                        and odt.OutboundDocumentTypeCode != 'WAV'
			       --                  join Status s (nolock) on i.StatusId = s.StatusId
			       --                 where i.WaveId = @WaveId
			       --                   and s.StatusCode in ('SA','RL','S','CK'))
			       --  begin
          --    exec @Error = p_Wave_Planning_Move_Items_Putaway
          --     @WaveId = @WaveId
              
          --    if @Error <> 0  
          --      goto error
          --  end
		        --end
          
          --exec @Error = p_InboundDocument_Create_From_Issue
          -- @IssueId            = @IssueId,  
          -- @OutboundShipmentId = -1,  
          -- @IssueLineId        = -1
          
          if @Error <> 0  
            goto error
        end
      end
    end
  end
      
  --  if @StatusId is not null --and @StatusCode not in ('A','QA')
  --  begin
  --    while exists(select top 1 1 from @TableIssues)
  --    begin
  --      select @IssueId     = IssueId,
  --             @IssueLineId = IssueLineId,
  --             @AutoSendup  = AutoSendup
  --        from @TableIssues
        

  --      delete @TableIssues
  --       where IssueId = @IssueId
  --         and IssueLineId = @IssueLineId
        
  --      if @IsShipment = 1
  --      begin
  --        if not exists(select top 1 1
  --                        from IssueLineInstruction ili (nolock)
  --                        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
  --                        join Job                    j (nolock) on i.JobId           = j.JobId
  --                        join Status                 s (nolock) on j.StatusId        = s.StatusId
  --                       where ili.IssueLineId    = @IssueLineId
  --                         and (s.StatusCode     != @StatusCode
  --                         and  s.StatusCode not in ('NS','CD','D','DC','C'))) -- GS 03-07-2008
  --        begin
  --          exec @Error = p_IssueLine_Update
  --           @IssueLineId = @IssueLineId,
  --           @StatusId    = @StatusId
            
  --          if @Error <> 0
  --            goto error
            
  --          if not exists(select top 1 1
  --                          from IssueLine  il (nolock)
  --                          join Status      s (nolock) on il.StatusId = s.StatusId
  --                         where il.IssueId        = @IssueId
  --                           and s.StatusCode not in ('NS','CD','D','DC','C')) -- GS 03-07-2008
  --          begin
  --            if @StatusCode = 'C'
  --            begin
  --              if not exists(select top 1 1
  --                              from IssueLine  il (nolock)
  --                              join Status      s (nolock) on il.StatusId = s.StatusId
  --                             where il.IssueId        = @IssueId
  --                               and s.StatusCode not in ('NS','C')) -- GS 03-07-2008
  --              begin
  --                exec @Error = p_Issue_Update
  --                 @IssueId  = @IssueId,
  --                 @StatusId = @StatusId
                  
  --                if @Error <> 0
  --                  goto error
  --              end
  --            end
  --            else
  --            begin
  --              exec @Error = p_Issue_Update
  --               @IssueId  = @IssueId,
  --               @StatusId = @StatusId
                
  --              if @Error <> 0
  --                goto error
  --            end
              
  --            if @StatusCode in ('CD','D','DC','C') -- GS 03-07-2008
  --            begin
  --              /**** Kitting change ****/
                

  --              select @OutboundDocumentId = OutboundDocumentId
  --                    ,@InboundDocumentId = null
  --                    from Issue (nolock)
  --                   where IssueId = @IssueId
                      
  --              select @InboundDocumentId = id.InboundDocumentId
  --              from InboundDocument id
  --              inner join BOMInstruction bi on id.InboundDocumentId = bi.InboundDocumentId
  --              inner join OutboundDocument od on bi.OutboundDocumentId = od.OutboundDocumentId
  --              where od.OutboundDocumentId = @OutboundDocumentId
                
  --              Exec @error = p_BOM_Status_Rollup @IssueId                   
                
  --              if @InboundDocumentId IS NOT NULL
  --              begin
                
  --                  select @ReceiptId = ReceiptId
  --                        ,@StatusId = dbo.ufn_StatusId('R','W')
  --                  From Receipt
  --                  Where InboundDocumentId = @InboundDocumentId
  --                  and StatusId = dbo.ufn_StatusId('R','OH')
                    
  --                  exec @Error = p_Receipt_Update_Status @ReceiptId, @StatusId
                
  --              end                 
  --              /**** End kitting ****/
                
  --              if @AutoSendup = 1
  --              begin
  --                exec @Error = p_Interface_Export_SO_Insert
  --                 @IssueId = @IssueId
                  
  --                if @Error <> 0

  --                  goto error
                  
  --                if dbo.ufn_Configuration(141, @WarehouseId) = 1
  --                begin
  --                  select @OutboundShipmentId = OutboundShipmentId
  --                    from OutboundShipmentIssue (nolock)
  --                   where IssueId = @IssueId
                    
  --                  exec @Error = p_Receive_By_Label_Insert
  --                   @OutboundShipmentId = @OutboundShipmentId,
  --                   @IssueId            = @IssueId
                    
  --                  if @Error <> 0
  --                    goto error
  --                end
  --              end
  --            end
              
  --            exec @Error = p_InboundDocument_Create_From_Issue
  --             @IssueId            = @IssueId,  
  --             @OutboundShipmentId = -1,  
  --             @IssueLineId        = -1
              
  --            if @Error <> 0  
  --              goto error
  --          end
  --          else if @StatusCode in ('D','WC','QA','A')
  --          begin
  --            if not exists(select top 1 1
  --                            from IssueLineInstruction ili (nolock)
  --                            join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
  --                            join Job                    j (nolock) on ins.JobId         = j.JobId
  --                            join Status                 s (nolock) on j.StatusId        = s.StatusId
  --                           where ili.IssueId     = @IssueId
  --                             and s.StatusCode not in ('WC','QA')) -- 2008-06-30 took out D
  --            begin
  --              if @StatusCode in ('D','WC','QA')
  --                select @StatusId = dbo.ufn_StatusId('IS','QA')
  --              else
  --                select @StatusId = dbo.ufn_StatusId('IS','A')
                
  --              exec @Error = p_Issue_Update
  --               @IssueId  = @IssueId,
  --               @StatusId = @StatusId
                
  --              if @Error <> 0
  --                goto error
  --            end
  --          end
  --        end
  --        else if @StatusCode in ('D','WC','QA')
  --        begin
  --          if not exists(select top 1 1

  --                          from IssueLineInstruction ili (nolock)
  --                          join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
  --                          join Job                    j (nolock) on ins.JobId         = j.JobId
  --                          join Status                 s (nolock) on j.StatusId        = s.StatusId
  --                         where ili.IssueId     = @IssueId
  --                           and s.StatusCode not in ('D','WC','QA'))
  --          begin
  --            select @StatusId = dbo.ufn_StatusId('IS','QA')
              
  --            exec @Error = p_Issue_Update
  --             @IssueId  = @IssueId,
  --             @StatusId = @StatusId
              
  --            if @Error <> 0
  --              goto error
  --          end
  --        end
  --        else if @StatusCode in ('CK','A')
  --        begin
  --          if not exists(select top 1 1
  --                          from IssueLineInstruction ili (nolock)
  --                          join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
  --                          join Job                    j (nolock) on ins.JobId         = j.JobId
  --                          join Status                 s (nolock) on j.StatusId        = s.StatusId
  --                         where ili.IssueId     = @IssueId
  --                           and s.StatusCode not in ('CK','A'))
  --          begin
  --            select @StatusId = dbo.ufn_StatusId('IS','A')
              
  --            exec @Error = p_Issue_Update
  --             @IssueId  = @IssueId,
  --             @StatusId = @StatusId
              
  --            if @Error <> 0
  --              goto error
  --          end
  --        end
  --      end -- IsShipment = 1
  --      else
  --      begin -- IsShipment = 0
  --        if not exists(select top 1 1
  --                        from IssueLine  il (nolock)
  --                        join Instruction i (nolock) on il.IssueLineId = i.IssueLineId
  --                        join Job         j (nolock) on i.JobId        = j.JobId
  --                        join Status      s (nolock) on j.StatusId     = s.StatusId
  --                       where il.IssueLineId = @IssueLineId

  --                         and s.StatusCode  != @StatusCode
  --                         and s.StatusCode  != 'NS')
  --        begin
  --          exec @Error = p_IssueLine_Update
  --           @IssueLineId = @IssueLineId,
  --           @StatusId    = @StatusId
            
  --          if @Error <> 0
  --            goto error
            
  --          if not exists(select top 1 1
  --                          from Issue       i (nolock)
  --                          join IssueLine  il (nolock) on i.IssueId   = il.IssueId
  --                          join Status      s (nolock) on il.StatusId = s.StatusId
  --                         where i.IssueId     = @IssueId
  --                           and s.StatusCode != @StatusCode)
  --          begin

  --            exec @Error = p_Issue_Update
  --             @IssueId  = @IssueId,
  --             @StatusId = @StatusId
              
  --            if @Error <> 0
  --              goto error
  --          end
  --          else if @StatusCode in ('D','WC','QA','A')
  --          begin
  --            if not exists(select top 1 1
  --                            from Issue         i (nolock)
  --                            join IssueLine    il (nolock) on i.IssueId      = il.IssueId
  --                            join Instruction ins (nolock) on il.IssueLineId = ins.IssueLineId
  --                            join Job           j (nolock) on ins.JobId      = j.JobId
  --                            join Status        s (nolock) on j.StatusId     = s.StatusId
  --                           where i.IssueId     = @IssueId
  --                             and s.StatusCode not in ('WC','QA')) -- 2008-06-30 took out D
  --            begin
  --              if @StatusCode in ('D','WC','QA')
  --                select @StatusId = dbo.ufn_StatusId('IS','QA')
  --              else
  --                select @StatusId = dbo.ufn_StatusId('IS','A')
                
  --              exec @Error = p_Issue_Update
  --               @IssueId  = @IssueId,
  --               @StatusId = @StatusId
                
  --              if @Error <> 0
  --                goto error
  --            end
  --          end
  --        end
  --        else if @StatusCode in ('D','WC','QA')
  --        begin
  --          if not exists(select top 1 1
  --                      from Issue         i (nolock)
  --                      join IssueLine    il (nolock) on i.IssueId      = il.IssueId
  --                      join Instruction ins (nolock) on il.IssueLineId = ins.IssueLineId
  --                      join Job           j (nolock) on ins.JobId      = j.JobId
  --                      join Status        s (nolock) on j.StatusId     = s.StatusId
  --                     where i.IssueId     = @IssueId
  --                       and s.StatusCode not in ('D','WC','QA'))
  --          begin
  --            select @StatusId = dbo.ufn_StatusId('IS','QA')
              
  --            exec @Error = p_Issue_Update
  --             @IssueId  = @IssueId,
  --             @StatusId = @StatusId
              
  --            if @Error <> 0
  --              goto error
  --          end
  --        end
  --        else if @StatusCode in ('CK','A')
  --        begin
  --          if not exists(select top 1 1
  --                      from Issue         i (nolock)
  --                      join IssueLine    il (nolock) on i.IssueId      = il.IssueId
  --                      join Instruction ins (nolock) on il.IssueLineId = ins.IssueLineId
  --                      join Job           j (nolock) on ins.JobId      = j.JobId
  --                      join Status        s (nolock) on j.StatusId     = s.StatusId
  --                     where i.IssueId     = @IssueId
  --                       and s.StatusCode not in ('CK','A'))
  --          begin
  --            select @StatusId = dbo.ufn_StatusId('IS','A')
              
  --            exec @Error = p_Issue_Update
  --             @IssueId  = @IssueId,
  --             @StatusId = @StatusId
              
  --            if @Error <> 0

  --              goto error
  --          end
  --        end
  --      end -- IsShipment = 0
  --    end
  --  end
  --end
  result:
  set @Error = 0
  return @Error
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    return @Error
end
