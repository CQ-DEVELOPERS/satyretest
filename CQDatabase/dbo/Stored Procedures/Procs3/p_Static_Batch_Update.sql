﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Batch_Update
  ///   Filename       : dbo.p_Static_Batch_Update.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 11 February 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Batch_Update
(
 @BatchId              int
,@StorageUnitId        int
,@OperatorId           int
,@StatusId             int 
,@WarehouseId          int 
,@Batch                nvarchar(50) 
,@BatchReferenceNumber nvarchar(50) 
,@ProductionDateTime   datetime 
,@ExpiryDate           datetime 
,@IncubationDays       int 
,@ShelfLifeDays        int 
,@ExpectedYield        int 
,@ActualYield          int 
,@FilledYield          int 
,@ECLNumber            nvarchar(10) 
,@COACertificate       bit
,@ReceiptId            int = null
,@RI				   float = null
,@Density			   float = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
	 
	 if @StatusId = 0
	   set @StatusId = 1
	 
--	 if @StorageUnitId = -1
--	   set @StorageUnitId = null
Insert BatchHistory
		(BatchId,
		StatusId,
		WarehouseId,
		Batch,
		CreateDate,
		ExpiryDate,
		IncubationDays,
		ShelfLifeDays,
		ExpectedYield,
		ActualYield,
		EclNumber,
		CommandType,
		InsertDate,
		OperatorId,
		RI,
		Density)
Select @BatchId,
	   StatusId,
	   WarehouseId,
	   Batch,
	   CreateDate,
	   ExpiryDate,
	   IncubationDays,
	   ShelfLifeDays,
	   ExpectedYield,
	   ActualYield,
	   EclNumber,
	   'Update',
	   Getdate(),
	   @OperatorId,
	   @RI,
	   @Density
	From Batch
	where BatchId = @BatchId
	
UPDATE [dbo].[Batch]
   SET [StatusId] = @StatusId
      ,[WarehouseId] = @WarehouseId
	  ,[StatusModifiedBy] = @OperatorId
      ,[StatusModifiedDate] = @GetDate	
      ,[Batch] = @Batch
      ,[BatchReferenceNumber] = @BatchReferenceNumber
      ,[ProductionDateTime] = @ProductionDateTime
      ,[ExpiryDate] = @ExpiryDate
      ,[IncubationDays] = @IncubationDays
      ,[ShelfLifeDays] = @ShelfLifeDays
      ,[ExpectedYield] = @ExpectedYield
      ,[ActualYield] = @ActualYield
      ,[FilledYield] = @FilledYield
      ,[ECLNumber] = @ECLNumber
      ,[COACertificate] = @COACertificate
      ,[ModifiedBy] = @OperatorId
      ,[ModifiedDate] = @GetDate
      ,[RI] = @RI
      ,[Density] = @Density
 WHERE BatchId = @BatchId


end
 
