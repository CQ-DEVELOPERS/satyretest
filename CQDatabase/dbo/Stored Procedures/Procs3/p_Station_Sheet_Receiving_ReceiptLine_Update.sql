﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Station_Sheet_Receiving_ReceiptLine_Update
  ///   Filename       : p_Station_Sheet_Receiving_ReceiptLine_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Station_Sheet_Receiving_ReceiptLine_Update
(
 @ReceiptLineId        int,
 @AcceptedQuantity     numeric(13,6)

)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @ReceivedQuantity  numeric(13,6),
          @RequiredQuantity  numeric(13,6),
          @NewReceiptLineId  int,
          @ReceiptId         int,
          @InboundLineId     int,
          @InboundDocumentId int,
          @StorageUnitId     int,
          @WarehouseId       int,
          @BatchId           int,
          @OverReceipt       numeric(13,3),
          @Exception         nvarchar(255),
          @OldSUB            int,
          @ProductCategory   char(1),
          @InboundSequence   int,
          @StorageUnitBatchId   int,
          @Batch             nvarchar(30),
          @OperatorId        int,
          @AcceptedWeight    numeric(13,6),
          @DeliveryNoteQuantity numeric(13,6),
          @SampleQuantity       numeric(13,6),
          @alternateBatch       bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Error = 0
  
  if @StorageUnitBatchId = -1
    set @StorageUnitBatchId = null
  
  select @ReceivedQuantity   = isnull(@AcceptedQuantity,0) + isnull(rl.RejectQuantity,0),
         @RequiredQuantity   = rl.RequiredQuantity,
         @ReceiptId          = rl.ReceiptId,
         @InboundLineId      = rl.InboundLineId,
         @InboundDocumentId  = r.InboundDocumentId,
         @StorageUnitBatchId = case when @StorageUnitBatchId is null
                                    then rl.StorageUnitBatchId
                                    else @StorageUnitBatchId
                                    end,
         @OldSUB             = rl.StorageUnitBatchId,
         @WarehouseId        = r.WarehouseId,
         @StorageUnitId      = id.StorageUnitId,
         @BatchId            = id.BatchId
    from Receipt      r (nolock)
    join ReceiptLine rl (nolock) on r.ReceiptId = rl.ReceiptId
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
   where rl.ReceiptLineId = @ReceiptLineId
  
  if @StorageUnitId is null or @BatchId is null
    select @StorageUnitId = StorageUnitId,
           @BatchId       = BatchId
      from StorageUnitBatch
     where StorageUnitBatchId = @StorageUnitBatchId
  else
    select @StorageUnitBatchId = StorageUnitBatchId
      from StorageUnitBatch
     where StorageUnitId = @StorageUnitId
       and BatchId       = @BatchId
  
  select @OverReceipt = p.OverReceipt,
         @ProductCategory = su.ProductCategory
    from StorageUnit su (nolock)
    join Product      p (nolock) on su.ProductId = p.ProductId
   where su.StorageUnitId = @StorageUnitId
  
  if @OverReceipt is null
    set @OverReceipt = 0
  
  select @InboundSequence = max(InboundSequence)
    from PackType (nolock)
 
  begin transaction
  
  if exists(select 1
              from Receipt (nolock)
             where InboundDocumentId = @InboundDocumentId
               and ReceiptId > @ReceiptId)
  begin
    set @Error = -3
    goto error
  end
  
  if (select StorageUnitId
        from InboundLine (nolock)
       where InboundLineId = @InboundLineId) != @StorageUnitId
  begin
    set @Error = -1
    goto error
  end
  
  --if ((convert(numeric(13,3),@ReceivedQuantity)/convert(numeric(13,3),@RequiredQuantity))*convert(numeric(13,3),100)) > @OverReceipt + 100
  --begin
    --set @Error = -1
    --goto error
  --end
  
  --if @ReceivedQuantity > @RequiredQuantity
  --begin
   -- set @Exception = 'Quantity over recipted Received:' + convert(nvarchar(10), @ReceivedQuantity) + ' Req:' + convert(nvarchar(10), @ReceivedQuantity)
    
  --exec @Error = p_Exception_insert
    -- @ReceiptLineId = @ReceiptLineId,
    -- @Exception     = @Exception,
    -- @ExceptionCode = 'RLUPD',
    -- @CreateDate    = @Getdate,
    -- @OperatorId    = @OperatorId,
    -- @ExceptionDate = @Getdate
  --end
      
  if (select dbo.ufn_Configuration(205,@WarehouseId)) = 1
  begin
    exec @Error = p_Batch_Update
     @BatchId         = @BatchId,
     @CreateDate      = @getdate
    
    if @Error <> 0
      goto error
  end
  
  if @ProductCategory != 'P'
  begin
    if (select dbo.ufn_Configuration(129,@WarehouseId)) = 0
      if (select Batch from Batch where BatchId = @BatchId) = 'Default'
      begin
        set @Error = -2
        goto error
      end
  end
 
  if exists(select top 1 1
              from InboundDocument id
              join Status           s on id.StatusId = s.StatusId
             where s.Type       = 'ID'
               and s.StatusCode = 'N')
  begin
    select @StatusId = dbo.ufn_StatusId('ID','W')
    
    exec @Error = p_InboundDocument_Update
     @InboundDocumentId = @InboundDocumentId,
     @StatusId          = @StatusId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_InboundLine_Update
     @InboundDocumentId = @InboundDocumentId,
     @StatusId          = @StatusId
    
    if @Error <> 0
      goto error
  end
  
  select @StatusId = dbo.ufn_StatusId('R','R')
  
  update r
     set LocationId = al.LocationId
    from Receipt       r (nolock),
         Area          a (nolock),
         AreaLocation al (nolock)
   where r.ReceiptId   = @ReceiptId
     and a.WarehouseId = @WarehouseId
     and a.AreaId = al.AreaId
     and a.AreaType  = 'QC'
     and r.LocationId is null
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update r
     set LocationId = al.LocationId
    from Receipt       r (nolock),
         Area          a (nolock),
         AreaLocation al (nolock)
   where r.ReceiptId   = @ReceiptId
     and a.WarehouseId = @WarehouseId
     and a.AreaId = al.AreaId
     and a.AreaCode = 'R'
     and r.LocationId is null
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update subl
     set ActualQuantity = case when subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0)) < 0
                               then 0
                               else subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0)  - isnull(rl.SampleQuantity,0))
                               end
    from ReceiptLine                rl
    join Receipt                     r on rl.ReceiptId            = r.ReceiptId
    join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                      and subl.LocationId         = r.LocationId
   where rl.ReceiptLineId = @ReceiptLineId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  select @AcceptedWeight = @AcceptedQuantity * pk.NettWeight
    from ReceiptLine             rl
    join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit             su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product                  p (nolock) on su.ProductId          = p.ProductId
    join Pack                    pk (nolock) on su.StorageUnitId      = pk.StorageUnitId
    join PackType                pt (nolock) on pk.PackTypeId         = pt.PackTypeId
   where rl.ReceiptLineId               = @ReceiptLineId
     and pk.WarehouseId                 = @WarehouseId
     and pt.InboundSequence             = @InboundSequence
     and isnull(su.ProductCategory,'') != 'V'
  
  if @RequiredQuantity > @ReceivedQuantity and @alternateBatch = 1
  begin
    exec @Error = p_ReceiptLine_Update
     @ReceiptLineId        = @ReceiptLineId,
     @StorageUnitBatchId   = @StorageUnitBatchId,
     @StatusId             = @StatusId,
     @OperatorId           = @OperatorId,
     @RequiredQuantity     = @AcceptedQuantity,
     @ReceivedQuantity     = @AcceptedQuantity,
     @AcceptedQuantity     = @AcceptedQuantity,
     @AcceptedWeight       = @AcceptedWeight,
     @DeliveryNoteQuantity = @AcceptedQuantity,
     @SampleQuantity       = @SampleQuantity
    
    if @Error <> 0
      goto error
    
    select @StatusId = dbo.ufn_StatusId('R','W')
    
    set @RequiredQuantity = @RequiredQuantity - @ReceivedQuantity
    
    exec @Error = p_ReceiptLine_Insert
     @ReceiptLineId        = @NewReceiptLineId output,
     @ReceiptId            = @ReceiptId,
     @InboundLineId        = @InboundLineId,
     @StorageUnitBatchId   = @OldSUB,
     @StatusId             = @StatusId,
     @OperatorId           = @OperatorId,
     @RequiredQuantity     = @RequiredQuantity
    
    if @Error <> 0
      goto error
  end
  else
  begin
    exec @Error = p_ReceiptLine_Update
     @ReceiptLineId        = @ReceiptLineId,
     @StorageUnitBatchId   = @StorageUnitBatchId,
     @StatusId             = @StatusId,
     @OperatorId           = @OperatorId,
     @ReceivedQuantity     = @AcceptedQuantity,
     @AcceptedQuantity     = @AcceptedQuantity,
     @AcceptedWeight       = @AcceptedWeight,
     @DeliveryNoteQuantity = @AcceptedQuantity,
     @SampleQuantity       = @SampleQuantity
    
    if @Error <> 0
      goto error
  end
  
  update subl
     set ActualQuantity = subl.ActualQuantity + isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0)
    from ReceiptLine                rl
    join Receipt                     r on rl.ReceiptId            = r.ReceiptId
    join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                      and subl.LocationId         = r.LocationId
   where rl.ReceiptLineId = @ReceiptLineId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  insert StorageUnitBatchLocation
        (StorageUnitBatchId,
         LocationId,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity)
  select rl.StorageUnitBatchId,
         r.LocationId,
         sum(rl.AcceptedQuantity  - isnull(rl.SampleQuantity,0)),
         0,
         0
    from ReceiptLine                rl
    join Receipt                     r on rl.ReceiptId            = r.ReceiptId
    left
    join StorageUnitBatchLocation subl on r.LocationId          = subl.LocationId
                                      and rl.StorageUnitBatchId = subl.StorageUnitBatchId
   where rl.ReceiptLineId = @ReceiptLineId
     and r.LocationId is not null
     and rl.AcceptedQuantity > 0
     and subl.LocationId is null
  group by rl.StorageUnitBatchId,
           r.LocationId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  select @StatusId = dbo.ufn_StatusId('R','R')
  
  exec @Error = p_Receipt_Update_Status
   @ReceiptId = @ReceiptId,
   @StatusId  = @StatusId
  
  commit transaction
  select @Error
  return @Error
  
  error:
--    RAISERROR (900000,-1,-1, 'Error executing p_Station_Sheet_Receiving_ReceiptLine_Update'); 
    rollback transaction
    select @Error
    return @Error
end
