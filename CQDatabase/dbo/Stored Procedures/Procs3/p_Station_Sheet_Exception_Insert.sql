﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Station_Sheet_Exception_Insert
  ///   Filename       : p_Station_Sheet_Exception_Insert.sql
  ///   Create By      : Karen
  ///   Date Created   : 16 Mar 2011 15:15:29
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Exception table.
  /// </remarks>
  /// <param>
  ///   @ExceptionId int = null output,
  ///   @ReceiptLineId int = null,
  ///   @InstructionId int = null,
  ///   @IssueLineId int = null,
  ///   @ReasonId int = null,
  ///   @Exception nvarchar(255) = null,
  ///   @ExceptionCode nvarchar(10) = null,
  ///   @CreateDate datetime = null,
  ///   @OperatorId int = null,
  ///   @ExceptionDate datetime = null,
  ///   @JobId int = null,
  ///   @Detail sql_variant = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  ///   Exception.ExceptionId,
  ///   Exception.ReceiptLineId,
  ///   Exception.InstructionId,
  ///   Exception.IssueLineId,
  ///   Exception.ReasonId,
  ///   Exception.Exception,
  ///   Exception.ExceptionCode,
  ///   Exception.CreateDate,
  ///   Exception.OperatorId,
  ///   Exception.ExceptionDate,
  ///   Exception.JobId,
  ///   Exception.Detail,
  ///   Exception.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Station_Sheet_Exception_Insert
(
 @ExceptionId int = null output,
 @ReceiptLineId int = null,
 @InstructionId int = null,
 @IssueLineId int = null,
 @ReasonId int = null,
 @Exception nvarchar(255) = null,
 @ExceptionCode nvarchar(10) = null,
 @CreateDate datetime = null,
 @OperatorId int = null,
 @ExceptionDate datetime = null,
 @JobId int = null,
 @Detail sql_variant = null,
 @Quantity float = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  insert Exception
        (ReceiptLineId,
         InstructionId,
         IssueLineId,
         ReasonId,
         Exception,
         ExceptionCode,
         CreateDate,
         OperatorId,
         ExceptionDate,
         JobId,
         Detail,
         Quantity)
  select @ReceiptLineId,
         @InstructionId,
         @IssueLineId,
         @ReasonId,
         @Exception,
         @ExceptionCode,
         @CreateDate,
         @OperatorId,
         @ExceptionDate,
         @JobId,
         @Detail,
         @Quantity 
  
  select @Error = @@Error, @ExceptionId = scope_identity()
  
  if @Error = 0
    exec @Error = p_ExceptionHistory_Insert
         @ExceptionId = @ExceptionId,
         @ReceiptLineId = @ReceiptLineId,
         @InstructionId = @InstructionId,
         @IssueLineId = @IssueLineId,
         @ReasonId = @ReasonId,
         @Exception = @Exception,
         @ExceptionCode = @ExceptionCode,
         @CreateDate = @CreateDate,
         @OperatorId = @OperatorId,
         @ExceptionDate = @ExceptionDate,
         @JobId = @JobId,
         @Detail = @Detail,
         @Quantity = @Quantity,
         @CommandType = 'Insert'
  
  return @Error
  
end
