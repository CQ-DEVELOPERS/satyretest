﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_RTT
  ///   Filename       : p_Report_RTT.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_RTT 
(  
 @OutboundShipmentId int,  
 @IssueId            int  
)  
  
as  
begin  
  set nocount on;  
    
  declare @TableResult as table  
  (  
   OutboundShipmentId		int,  
   IssueId					int,  
   OutboundDocumentId		int,  
   OrderNumber				nvarchar(30),  
   ExternalCompanyId		int,  
   ExternalCompany			nvarchar(255),  
   DespatchDate				datetime,  
   LocationId				int,  
   Location					nvarchar(15),  
   JobId					int,  
   StatusId					int,  
   Status					nvarchar(50),  
   InstructionTypeId		int,  
   InstructionTypeCode		nvarchar(10),  
   InstructionType			nvarchar(50),  
   InstructionId			int,  
   Pallet					nvarchar(10),  
   StorageUnitBatchId		int,  
   StorageUnitId			int,
   ProductCode				nvarchar(30),
   Product					nvarchar(50),
   Quantity					float,  
   ConfirmedQuantity		float,  
   ShortQuantity			float,  
   QuantityOnHand			float,  
   DropSequence				int,  
   RouteId					int,  
   Route					nvarchar(50),  
   Weight					float,  
   ReferenceNumber			nvarchar(30),  
   LineLocationId			int,  
   LineLocation				nvarchar(15),  
   PickerId					int,  
   Picker					nvarchar(50),  
   CheckerId				int,  
   Checker					nvarchar(50),  
   Comment					nvarchar(50),  
   StartDate				datetime,  
   CompleteDate				datetime,  
   Jobs						int,  
   OutboundDocumentType		nvarchar(30),  
   JobConfirmedQty			int,
   SKUId					int,
   SKUCode					nvarchar(10),
   Size						nvarchar(30),
   Colour					nvarchar(20)  
  )  
    
  declare	@startdate	datetime,  
			@enddate	datetime      
      
  if @OutboundShipmentId = -1  
    set @OutboundShipmentId = null  
    
  if @IssueId = -1  
    set @IssueId = null  
    
  if @OutboundShipmentId is not null  
    set @IssueId = null  
    
  insert @TableResult  
        (OutboundShipmentId,  
         OutboundDocumentId,  
         JobId,  
         StatusId,  
         InstructionTypeId,  
         InstructionId,  
         StorageUnitBatchId,  
         Quantity,  
         ConfirmedQuantity,  
         ShortQuantity,  
         LineLocationId,  
         PickerId,  
         StartDate,  
         CompleteDate,  
         RouteId)  
  select distinct  i.OutboundShipmentId,  
         ili.OutboundDocumentId,  
         i.JobId,  
         i.StatusId,  
         i.InstructionTypeId,  
         i.InstructionId,  
         i.StorageUnitBatchId,  
         i.Quantity,  
         isnull(ili.ConfirmedQuantity, 0),  
         ili.Quantity - isnull(ili.ConfirmedQuantity, 0),  
         i.StoreLocationId,  
         i.OperatorId,  
         i.StartDate,  
         i.EndDate,  
         iss.RouteId        
     
  from Instruction i
  left
  join Location     store (nolock) on i.StoreLocationId = store.LocationId
  left
  join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
  join IssueLine             il (nolock) on ili.IssueLineId = il.IssueLineId
  join Issue                iss (nolock) on ili.IssueId = iss.IssueId 
  join OutboundDocument      od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
   where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))  
     and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))  
    
   
  update tr  
     set DespatchDate = os.ShipmentDate,  
 
         DropSequence = osi.DropSequence,  
         LocationId   = os.LocationId  
    from @TableResult     tr  
    join OutboundShipment       os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId  
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId  
   where tr.OutboundShipmentId is not null  
     
   update tr  
     set ReferenceNumber = j.ReferenceNumber,  
         CheckerId       = j.CheckedBy  
    from @TableResult     tr  
    join Job j (nolock) on tr.JobId = j.JobId  
      
   update tr  
     set Picker = o.Operator  
    from @TableResult     tr  
    join Operator o (nolock) on tr.PickerId = o.OperatorId  
      
   update tr  
     set Checker = o.Operator  
    from @TableResult     tr  
    join Operator o (nolock) on tr.CheckerId = o.OperatorId  
      
   update tr  
     set LineLocation = l.Location  
    from @TableResult     tr  
    join Location l (nolock) on tr.LineLocationId = l.LocationId  
    
  update tr  
     set DespatchDate = i.DeliveryDate,  
         RouteId      = i.RouteId,  
         DropSequence = i.DropSequence,  
         LocationId   = i.LocationId  
    from @TableResult tr  
    join Issue         i (nolock) on tr.IssueId = i.IssueId  
   where tr.OutboundShipmentId is null  
    
  update tr  
     set Route = r.Route  
    from @TableResult tr  
    join Route         r (nolock) on tr.RouteId = r.RouteId  
    
  update tr  
     set OrderNumber          = od.OrderNumber,  
         ExternalCompanyId    = od.ExternalCompanyId,  
         OutboundDocumentType = odt.OutboundDocumentType  
    from @TableResult               tr  
    join OutboundDocument           od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId  
    left join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId  
    
  update tr  
     set ExternalCompany = ec.ExternalCompany  
    from @TableResult    tr  
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId  
    
  update tr  
     set Location = l.Location  
    from @TableResult    tr  
    join Location         l (nolock) on tr.LocationId = l.LocationId  
      
  
  update tr  
     set Weight = tr.Quantity * p.Weight  
    from @TableResult      tr  
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId  
    join Pack               p (nolock) on sub.StorageUnitId     = p.StorageUnitId  
   where p.Quantity = 1  
   
   update tr  
     set SKUId = sk.SKUId,
		 SKUCode = sk.SKUCode,
		 ProductCode = p.ProductCode,
		 Product = p.Product
    from @TableResult      tr  
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId  
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId = p.ProductId
    join Sku               sk (nolock) on su.SkuId  = sk.SkuId  
    
  update tr  
     set InstructionType     = 'Full',  
         InstructionTypeCode = it.InstructionTypeCode  
    from @TableResult tr  
    join InstructionType it on tr.InstructionTypeId = it.InstructionTypeId  
   where it.InstructionTypeCode = 'P'  
    
  update @TableResult  
     set InstructionType     = 'Mixed'  
   where InstructionType is null  
  
      
   update tr  
     set Jobs = (select count(distinct(tr2.JobId))  
                      from @TableResult tr2  
                     where tr.OrderNumber = tr2.OrderNumber)  
    from @TableResult tr  
    
    update tr  
     set Pallet = convert(nvarchar(10), j.DropSequence)  
               + ' of '  
               + convert(nvarchar(10), tr.Jobs),  
         DropSequence = j.DropSequence  
    from @TableResult tr  
    join Job           j (nolock) on tr.JobId = j.JobId  
  
   SET @startdate = (select MIN(StartDate) from @TableResult)  
   set @enddate   = (select MAX(CompleteDate) from @TableResult)  
     
     
   update tr  
     set JobConfirmedQty = (select sum(ConfirmedQuantity)  
                      from @TableResult tr2  
                     where tr.jobid = tr2.jobid)  
    from @TableResult tr  
      
    update tr  
     set status = s.status  
    from @TableResult tr  
    join Status s on s.StatusId = tr.StatusId  
      
    update tr  
     set status = 'No Stock'  
    from @TableResult tr  
 where JobConfirmedQty = 0  
      
   
  select OutboundShipmentId,  
         DropSequence,  
         Route,  
         DespatchDate,  
         ExternalCompany,  
         Location,  
         min(InstructionType) as 'InstructionType',  
         JobId,  
         Status,  
         OrderNumber,  
         Pallet,  
         ReferenceNumber,  
         LineLocation,  
         Picker,  
         Checker,  
         @startdate as StartDate,  
         @enddate as CompleteDate,  
         Jobs,  
         OutboundDocumentType,
         SKUCode ,
         Weight,
         ProductCode,
         Product
    from @TableResult tr  
    --join status s on tr.StatusId = s.StatusId  
  group by OutboundShipmentId,  
         DropSequence,  
         Route,  
         DespatchDate,  
         ExternalCompany,  
         Location,  
         JobId,  
         Status,  
         OrderNumber,  
         Pallet,  
         ReferenceNumber,  
         LineLocation,  
         Picker,  
         Checker,  
         Jobs,  
         OutboundDocumentType,
         SKUCode,
         Weight,
         ProductCode,
         Product
  order by OutboundShipmentId,  
           DropSequence,  
           JobId  
end  
