﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_OutboundSLA_Detail
  ///   Filename       : p_Report_OutboundSLA_Detail.sql
  ///   Create By      : Karen
  ///   Date Created   : March 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_OutboundSLA_Detail
(
 @FromDate				datetime,
 @ToDate				datetime,
 @PrincipalId			int,
 @ShipDateOption		nvarchar(1), -- 1=Use Date and Time Value
									 --	2=Use Date Value Only
 @Measure				nvarchar(1), -- 1=RequestShipDate to PickCompletedate  
									 --	2=RequestShipDate to ActualShip date
 @StatusId				int = null,
 @OrderNumber			nvarchar(30)
)


as
begin
	 set nocount on;
	 
  declare	@date1	date,
			@date2	date
  
  declare @TableHeader as table
  (
   OrderDate				    datetime,
   PrincipalId					int,
   PrincipalCode			    nvarchar(50),
   OrderNumber					nvarchar(30),
   IssueId						int,
   ExpectedDeliveryDate			datetime,
   ExpectedDeliveryTime			time,
   ExpectedDeliveryDate2		date,
   StatusId						int,
   Status					    nvarchar(50),
   IssueLineId					int,
   DeliveryDate					date,
   ExternalOrderNumber			nvarchar(50),
   NumberOfLines				int,
   ShippedQty					int,
   PickDate						datetime,
   PickDate2					date,
   ActualShipDate				datetime,
   ActualShipDate2				date,
   DiffPickShipped				decimal(13,2),
   DiffPickShipped2				decimal(13,2),
   DiffExpectedShipped			decimal(13,2),
   DiffExpectedShipped2			decimal(13,2),
   OrderMeasuredInd				nvarchar(3),
   OrderInSLA					nvarchar(3)	
  )
  
  if @PrincipalId = -1
	set @PrincipalId = null
	
  if @OrderNumber = '-1'
  or @OrderNumber = ' '
	set @OrderNumber = null
	
  if @StatusId = -1
	set @StatusId = null
 
 
  insert @TableHeader
        (OrderDate,
         PrincipalId,
         OrderNumber,
         ExpectedDeliveryDate,
         ExpectedDeliveryDate2,
         ExpectedDeliveryTime,
         StatusId,
         Status,
         IssueLineId,
         DeliveryDate,
         ExternalOrderNumber,
         IssueId,
         PickDate,
         PickDate2,
         ActualShipDate,
         ActualShipDate2,
         OrderMeasuredInd,
         OrderInSLA
         )
  SELECT od.CreateDate,
		 od.PrincipalId,
		 od.OrderNumber,
		 od.DeliveryDate,
		 od.DeliveryDate,
		 CONVERT(time,od.DeliveryDate,108) AS HourMinute,
		 i.StatusId,
		 s.Status,
		 il.IssueLineId,
		 i.DeliveryDate,
		 od.ExternalOrderNumber,
		 i.IssueId,
		 i.Checking,
		 i.Checking,
		 i.Checked,
		 i.Checked,
		 'No',
		 ' '
    FROM OutboundDocument od (nolock)
    join Issue i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    join IssueLine il (nolock) on i.IssueId = il.IssueId
    join Status s (nolock) on i.StatusId = s.StatusId
   WHERE od.CreateDate between @FromDate and @ToDate
   and od.PrincipalId = isnull(@PrincipalId,od.PrincipalId)
   and od.OrderNumber = isnull(@OrderNumber,od.OrderNumber)
   and i.StatusId = isnull(@StatusId,i.StatusId)
  
	--select * from @TableHeader

   if @ShipDateOption = 1
   begin
    update th
	  set ExpectedDeliveryDate = dateadd(HH, 17, ExpectedDeliveryDate) 
     from @TableHeader th 
     where ExpectedDeliveryTime = '00:00:00.0000000'
     
     update th
	  set ActualShipDate = dateadd(HH, 5, ActualShipDate) 
     from @TableHeader th 
     where ExpectedDeliveryTime = '12:00:00.0000000'
   end
     
     
   update th
	  set ActualShipDate = null,
		  ActualShipDate2 = null 
     from @TableHeader th 
    where th.StatusId in (dbo.ufn_StatusId('IS','W'), dbo.ufn_StatusId('IS','C'))
    
  update @TableHeader 
	 set NumberOfLines = (select COUNT(IssueLineId) 
						  from Issue i 
						  join IssueLine il (nolock) on i.IssueId = il.IssueId
						 where th.IssueId = i.IssueId)
    from @TableHeader th 
    
  update @TableHeader 
	 set ShippedQty = (select SUM(il.ConfirmedQuatity) from IssueLine il (nolock) 
						join issue i (nolock) on il.IssueId = i.IssueId
						where i.IssueId = th.IssueId
						and il.IssueLineId = th.IssueLineId
						and i.StatusId in (dbo.ufn_StatusId('IS','CD'),dbo.ufn_StatusId('IS','C'),dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC')))  
    from @TableHeader th 
    
  if @ShipDateOption = 1 
  update @TableHeader 
	 set DiffPickShipped = (select dbo.ufn_Hours_Between_Dates(ExpectedDeliveryDate, PickDate, 0,0)),
	     DiffExpectedShipped = (select dbo.ufn_Hours_Between_Dates(ExpectedDeliveryDate, ActualShipDate, 0,0))
    from @TableHeader th 
    else
    update @TableHeader 
	 set DiffPickShipped = (select dbo.ufn_Hours_Between_Dates(ExpectedDeliveryDate2, PickDate2, 0,0)),
	     DiffExpectedShipped = (select dbo.ufn_Hours_Between_Dates(ExpectedDeliveryDate2, ActualShipDate2, 0,0))
    from @TableHeader th
    
  update @TableHeader 
	 set OrderMeasuredInd = 'No'
    from @TableHeader th 
  where th.StatusId = dbo.ufn_StatusId('IS','W')
  
  if @ShipDateOption = 1
	  update @TableHeader 
		 set OrderMeasuredInd = 'Yes'
		from @TableHeader th 
	  where th.PickDate is not null 
  else
	  update @TableHeader 
		 set OrderMeasuredInd = 'Yes'
		from @TableHeader th 
	  where th.PickDate2 is not null 
  
	if @Measure = 1
	begin
		if @ShipDateOption = 1
		begin
			update @TableHeader 
			   set OrderInSLA = 'No'
			  from @TableHeader th 
			 where th.PickDate > th.ExpectedDeliveryDate
			    or th.OrderMeasuredInd = 'No'
			   --and ActualShipDate is not null
			
			update @TableHeader 
			   set OrderInSLA = 'Yes'
			  from @TableHeader th 
			 where th.PickDate <= th.ExpectedDeliveryDate
			   --and ActualShipDate is not null
		end
		else
		begin
			update @TableHeader 
			   set OrderInSLA = 'No'
			  from @TableHeader th 
			 where th.PickDate2 > th.ExpectedDeliveryDate2
			    or th.OrderMeasuredInd = 'No'
			   and ActualShipDate2 is not null	
				        
			update @TableHeader 
			   set OrderInSLA = 'Yes'
			  from @TableHeader th 
			 where th.PickDate2 <= th.ExpectedDeliveryDate2
			  and ActualShipDate2 is not null
		end          
	end
	else
	begin
		if @ShipDateOption = 1
		begin
			update @TableHeader 
			   set OrderInSLA = 'No'
			  from @TableHeader th 
			 where th.ActualShipDate > th.ExpectedDeliveryDate
			    or th.OrderMeasuredInd = 'No'
			   and ActualShipDate is not null
			
			update @TableHeader 
			   set OrderInSLA = 'Yes'
			  from @TableHeader th 
			 where th.ActualShipDate <= th.ExpectedDeliveryDate
			   and ActualShipDate is not null
		end
		else
		begin
			update @TableHeader 
			   set OrderInSLA = 'No'
		  	  from @TableHeader th 
			 where th.ActualShipDate2 > th.ExpectedDeliveryDate2
			    or th.OrderMeasuredInd = 'No'
			   and ActualShipDate2 is not null

			update @TableHeader 
			   set OrderInSLA = 'Yes'
			  from @TableHeader th 
			 where th.ActualShipDate2 <= th.ExpectedDeliveryDate2
			   and ActualShipDate2 is not null
		end
	end
    

	--SET ARITHABORT OFF
	--SET ANSI_WARNINGS OFF 
  
       
  update  th
      set th.PrincipalCode = p.PrincipalCode
     from @TableHeader th
     join Principal p  (nolock) on p.PrincipalId = th.PrincipalId
  
  select *
    from @TableHeader
    order by PrincipalCode
    --where TotalOrders > 0
  
end
