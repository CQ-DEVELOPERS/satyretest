﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_InboundSLA_Detail
  ///   Filename       : p_Report_InboundSLA_Detail.sql
  ///   Create By      : Karen
  ///   Date Created   : March 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_InboundSLA_Detail
(
 @FromDate					datetime,
 @ToDate					datetime,
 @PrincipalId				int,
 @SLAStartDate				nvarchar(1),	-- 1=Trailer check in date
											-- 2=First Receipt Date
 @WeekendType				int,			-- 1=Friday
											-- 2=Friday & Saturday
											-- 3=Saturday Only
											-- 4=Saturday & Sunday
											-- 5=Sunday Only
 @ReceivingThreshold		int = null,
 @PutAwayThreshold			int = null,
 @ASNReceiptCloseThreshold	int = null
)


as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   CreateDate				    datetime,
   PrincipalId					int,
   OrderNumber					nvarchar(30),
   ReceiptId					int,
   ReceiptLineId				int,
   StorageUnitId				int,
   StorageUnitBatchId			int,
   ProductId					int,
   ProductCode				    nvarchar(50),
   Product						nvarchar(50),
   LPN							nvarchar(50),
   PrincipalCode				nvarchar(50),
   ReceivingStartDate			datetime,
   LineReceivingDate			datetime,
   SKUPutawayDate				datetime,
   ReceiptCloseDate				datetime,
   ReceiptMeasuredInd			nvarchar(3),
   DiffHoursStartToLineRec		int,
   LineReceivedInSLA			nvarchar(3),
   DiffHoursStartToLinePutaway	decimal(13,2),
   PutawayInSLA					nvarchar(3),
   DiffHoursStartToCloseRec		decimal(13,2),
   ASNClosedInSLA				nvarchar(3),
   StatusId						int,
   StatusIdR					int
  )
  
  if @PrincipalId = -1
	set @PrincipalId = null
   
  insert @TableHeader
        (CreateDate,
         PrincipalId,
         OrderNumber,
         ReceiptId,
         ReceiptLineId,
         StorageUnitBatchId,
         ReceivingStartDate,
         LineReceivingDate,
         SKUPutawayDate,
         ReceiptCloseDate,
         ReceiptMeasuredInd,
         StatusId,
         StatusIdR,
         LineReceivedInSLA,
         PutawayInSLA,
         ASNClosedInSLA
         )
  SELECT CreateDate,
		 id.PrincipalId,
		 id.OrderNumber,
		 r.ReceiptId,
		 rl.ReceiptLineId,
         rl.StorageUnitBatchId,
         r.ReceivingStarted,
         rl.ReceivedDate,
         rl.PutawayStarted,
         r.ReceivingCompleteDate,
         '',
         rl.StatusId,
         r.StatusId,
         '',
         '',
         ''
    FROM InboundDocument id (nolock)
    join Receipt r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join ReceiptLine rl (nolock) on r.ReceiptId = rl.ReceiptId
   WHERE id.CreateDate between @FromDate and @ToDate
   and id.PrincipalId = isnull(@PrincipalId,id.PrincipalId)

   
  --  update @TableHeader 
	 --set ReceiptCloseDate = max(ReceiptCloseDate)
  --  from @TableHeader th 
  --  join ReceiptLine rl on th.ReceiptId = rl.ReceiptId
       
  update @TableHeader 
	 set ReceiptCloseDate = null
    from @TableHeader th 
    where th.StatusIdR != dbo.ufn_StatusId('R','RC')

    
  update @TableHeader 
	 set ProductId = p.ProductId,
		 ProductCode = p.ProductCode,
		 Product = p.Product
    from @TableHeader th 
    join StorageUnitBatch sub on th.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit su on sub.StorageUnitId = su.StorageUnitId
    join Product p on su.ProductId = p.ProductId
    
  update @TableHeader 
	 set ReceiptMeasuredInd = 'No'
    from @TableHeader th 
    where th.StatusId = dbo.ufn_StatusId('R','W')

  update @TableHeader 
	 set ReceiptMeasuredInd = 'Yes'
    from @TableHeader th 
    where th.StatusId != dbo.ufn_StatusId('R','W')
    
  update @TableHeader 
	 set DiffHoursStartToLineRec = (select dbo.ufn_Hours_Between_Dates(ReceivingStartDate, LineReceivingDate, 1,0))
    from @TableHeader th 
    
   update @TableHeader 
	 set LineReceivedInSLA = 'No'
    from @TableHeader th 
    where DiffHoursStartToLineRec > @ReceivingThreshold
    and ReceivingStartDate is not null
    and th.StatusId != dbo.ufn_StatusId('R','W')
    
    
  update @TableHeader 
	 set LineReceivedInSLA = 'Yes'
    from @TableHeader th 
    where DiffHoursStartToLineRec <= @ReceivingThreshold
    and ReceivingStartDate is not null
    and th.StatusId != dbo.ufn_StatusId('R','W')
    
  update @TableHeader 
	 set DiffHoursStartToLinePutaway = (select dbo.ufn_Hours_Between_Dates(ReceivingStartDate, SKUPutawayDate, 1,0))
    from @TableHeader th 
  
  update @TableHeader 
	 set PutawayInSLA = 'No'
    from @TableHeader th 
    where DiffHoursStartToLinePutaway > @PutAwayThreshold
    and th.SKUPutawayDate is not null
    
  update @TableHeader 
	 set PutawayInSLA = 'Yes'
    from @TableHeader th 
    where DiffHoursStartToLinePutaway <= @PutAwayThreshold
    and th.SKUPutawayDate is not null
    
  update @TableHeader 
	 set DiffHoursStartToCloseRec = (select dbo.ufn_Hours_Between_Dates(ReceivingStartDate, ReceiptCloseDate, 1,0))
    from @TableHeader th 
    
  update @TableHeader 
	 set ASNClosedInSLA = 'No'
    from @TableHeader th 
    where DiffHoursStartToCloseRec > @ASNReceiptCloseThreshold
    and th.ReceiptCloseDate is not null
    
  update @TableHeader 
	 set ASNClosedInSLA = 'Yes'
    from @TableHeader th 
    where DiffHoursStartToCloseRec <= @ASNReceiptCloseThreshold
    and th.ReceiptCloseDate is not null

	--SET ARITHABORT OFF
	--SET ANSI_WARNINGS OFF 
  
       
  update  th
      set th.PrincipalCode = p.PrincipalCode
     from @TableHeader th
     join Principal p  (nolock) on p.PrincipalId = th.PrincipalId
  
  select *
    from @TableHeader
  order by PrincipalCode
    --where TotalOrders > 0
  
end
