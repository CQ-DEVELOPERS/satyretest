﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_KPI_Storage_Picking
  ///   Filename       : p_Report_Product_KPI_Storage_Picking.sql
  ///   Create By      : Karen
  ///   Date Created   : Jul 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Product_KPI_Storage_Picking
(
@ProductId		int,
@FromDate		datetime,
@ToDate			datetime,
@WarehouseId	int
)


as
begin
	 set nocount on;
	 
		 
Declare @Error				int,
        @Errormsg			varchar(500),
        @CheckedFrom		datetime,
        @CheckedTo			datetime
  
  --select @GetDate = dbo.ufn_Getdate()


  set @FromDate = DATEADD(d,DATEDIFF(d,0,@FromDate),0)
  set @FromDate = DATEADD(HH, -12 ,@FromDate)
  set @ToDate = DATEADD(d,DATEDIFF(d,0,@ToDate),0)
  set @ToDate = DATEADD(HH, 12 ,@ToDate)
  

  If @WarehouseId = -1
		set @WarehouseId = null
		
  --If @ProductId = -1
		--set @ProductId = null
  
  declare @TableDetail as Table
  (
    StorageUnitId	int NULL,
    ProductCode		nvarchar(30),
    Product			nvarchar(255),
	PackType		nvarchar(30) NULL,
	Quantity		int NULL,
	WarehouseId		int NULL,
	PutAway			int NULL,
	Month			int NULL,
	Year			int NULL
  );
  

	If @ProductId = -1
	 
  	insert	@TableDetail
			(StorageUnitId,
			PackType,
			Quantity,
			PutAway,
			Month,
			Year,
			WarehouseId)
	select	StorageUnitId,
			PackType,
			Quantity,
			sum(PutAway)as PutAway,
			DATEPART (mm, pk.EndDate),
			DATEPART (yy, pk.EndDate),
			pk.WarehouseId
	from ProductKPI pk
	where EndDate between @FromDate and @ToDate
	and pk.WarehouseId = isnull(@WarehouseId, pk.WarehouseId)
	--and pk.PackType = 'Pallet'
	and pk.PackType in ('PG Case',N'PG ყუთი')

	group by	DATEPART (yy, pk.EndDate),
				DATEPART (mm, pk.EndDate),StorageUnitId,
				PackType,
				Quantity,
				WarehouseId
	order by	DATEPART (yy, pk.EndDate),
				DATEPART (mm, pk.EndDate),StorageUnitId,
				PackType,
				Quantity,
				WarehouseId
				
				
    If @ProductId != -1
	 
  	insert	@TableDetail
			(StorageUnitId,
			PackType,
			Quantity,
			PutAway,
			Month,
			Year,
			WarehouseId)
	select	StorageUnitId,
			PackType,
			Quantity,
			sum(PutAway) as PutAway,
			DATEPART (mm, pk.EndDate),
			DATEPART (yy, pk.EndDate),
			pk.WarehouseId
	from ProductKPI pk
	where EndDate between @FromDate and @ToDate
	and pk.WarehouseId = isnull(@WarehouseId, pk.WarehouseId)
	--and pk.PackType = 'Pallet'
	and pk.PackType in ('PG Case',N'PG ყუთი')
	and pk.StorageUnitId = (select StorageUnitId from StorageUnit su
							where su.ProductId = @ProductId)
	group by	DATEPART (yy, pk.EndDate),
				DATEPART (mm, pk.EndDate),StorageUnitId,
				PackType,
				Quantity,
				WarehouseId
	order by	DATEPART (yy, pk.EndDate),
				DATEPART (mm, pk.EndDate),StorageUnitId,
				PackType,
				Quantity,
				WarehouseId
 
  
  	update @TableDetail
     set Product =	p.Product,
		 ProductCode = p.ProductCode
    from @TableDetail td
    join StorageUnit su (nolock) on su.StorageUnitId = td.StorageUnitId
    join product p (nolock) on p.ProductId = su.ProductId
    
    
    Select * from @TableDetail     
    
end
   		
