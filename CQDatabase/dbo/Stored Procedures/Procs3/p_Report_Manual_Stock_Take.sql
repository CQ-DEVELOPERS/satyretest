﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Manual_Stock_Take
  ///   Filename       : p_Report_Manual_Stock_Take.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Jul 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Manual_Stock_Take
(
 @WarehouseId int
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   InstructionId      int,
   InstructionRefId   int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   LocationId         int,
   Location           nvarchar(15),
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   Batch              nvarchar(50),
   SOH                float,
   PreviousCount1     float,
   PreviousCount2     float,
   LatestCount        float,
   NewProductCode     nvarchar(30),
   NewProduct         nvarchar(50),
   CurrentCount       float,
   SKUCode            nvarchar(50),
   Area               nvarchar(50),
   Ailse              nvarchar(10),
   [Column]           nvarchar(10),
   [Level]            nvarchar(10)
  )
  
  declare @TableOrder as table
  (
   OrderBy                         int Identity,
   Location                        nvarchar(15),
   ProductCode                     nvarchar(30),
   Product                         nvarchar(50),
   Batch                           nvarchar(50),
   SOH                             int        ,
   PreviousCount2                  int        ,
   PreviousCount1                  int        ,
   LatestCount                     int        ,
   NewProductCode                  nvarchar(30),
   NewProduct                      nvarchar(50),
   CurrentCount                    int        ,
   SKUCode                         nvarchar(50),
   Area                            nvarchar(50)
  )
  
  declare @Instructions as table
  (
   LocationId         int,
   StorageUnitBatchId int,
   InstructionId      int
  )
  
  insert @TableResult
        (StorageUnitBatchId,
         StorageUnitId,
         LocationId,
         Location,
         ProductCode,
         Product,
         Batch,
         SOH,
         SKUCode,
         Area,
         Ailse,
         [Column],
         [Level])
  select sub.StorageUnitBatchId,
         su.StorageUnitId,
         l.LocationId,
         l.Location,
         p.ProductCode,
         p.Product,
         b.Batch,
         subl.ActualQuantity,
         sku.SKUCode,
         a.Area,
         isnull(l.Ailse,'0'),
         isnull(l.[Column],'0'),
         isnull(l.[Level],'0')
    from StorageUnitBatch          sub (nolock)
    join StorageUnit                su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId          = p.ProductId
    join SKU                       sku (nolock) on su.SKUId              = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId           = b.BatchId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join Location                    l (nolock) on subl.LocationId        = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
   where a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
  
  insert @TableResult
        (LocationId,
         Location,
         Area,
         Ailse,
         [Column],
         [Level])
  select l.LocationId,
         l.Location,
         a.Area,
         isnull(l.Ailse,'0'),
         isnull(l.[Column],'0'),
         isnull(l.[Level],'0')
    from Location                    l (nolock)
    join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
   where a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
     and not exists(select 1 from StorageUnitBatchLocation subl (nolock) where subl.LocationId = l.LocationId)
  
  insert @Instructions
        (LocationId,
         StorageUnitBatchId,
         InstructionId)
  select i.PickLocationId,
         i.StorageUnitBatchId,
         max(i.InstructionId)
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId   = it.InstructionTypeId
   where i.CreateDate           >= '2009-07-01'
     and it.InstructionTypeCode in ('STE','STL','STA','STP')
  group by i.PickLocationId, i.StorageUnitBatchId
  
  update tr
     set InstructionId  = i.InstructionId
    from @TableResult  tr
    join @Instructions  i on tr.StorageUnitBatchId = i.StorageUnitBatchId
                         and tr.LocationId         = i.LocationId
  
  update tr
     set PreviousCount1   = i.Quantity,
         LatestCount      = i.ConfirmedQuantity,
         InstructionRefId = i.InstructionRefId
    from @TableResult tr
    join Instruction   i (nolock) on tr.InstructionId = i.InstructionId
  
  update tr
     set PreviousCount2 = i.Quantity
    from @TableResult tr
    join Instruction   i (nolock) on tr.InstructionRefId = i.InstructionId
  
  insert @TableResult
        (Location,
         ProductCode,
         Product,
         SOH,
         PreviousCount2,
         PreviousCount1,
         LatestCount,
         NewProductCode,
         NewProduct,
         CurrentCount,
         SKUCode,
         Area,
         Ailse,
         [Column],
         [Level])
  select '',
         '',
         'Do not move this row',
         0,
         0,
         0,
         0,
         '',
         '',
         0,
         '',
         '',
         '',
         '',
         ''
  
  --=VLOOKUP(I4,[PalletIds.xls]Sheet1!$A:$C,3,FALSE)
  
  insert @TableOrder
  select Location,
         ProductCode,
         Product,
         Batch,
         SOH,
         PreviousCount2,
         PreviousCount1,
         LatestCount,
         NewProductCode,
         NewProduct,
         CurrentCount,
         SKUCode,
         Area
    from @TableResult
  order by Ailse,
           [Column],
           [Level],
           Location,
           ProductCode
  
  select Location,
         ProductCode,
         Product,
         Batch,
         SOH,
         PreviousCount2,
         PreviousCount1,
         LatestCount,
         NewProductCode,
         '=VLOOKUP(I' + convert(nvarchar(10), OrderBy + 1) + ',[PalletIds.xls]Sheet1!$A:$C,3,FALSE)' as 'NewProduct',
         CurrentCount,
         SKUCode,
         Area
    from @TableOrder
end
