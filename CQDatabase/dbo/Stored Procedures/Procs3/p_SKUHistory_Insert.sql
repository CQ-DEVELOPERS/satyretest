﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKUHistory_Insert
  ///   Filename       : p_SKUHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:53
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the SKUHistory table.
  /// </remarks>
  /// <param>
  ///   @SKUId int = null,
  ///   @UOMId int = null,
  ///   @SKU nvarchar(100) = null,
  ///   @SKUCode nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @AlternatePallet int = null,
  ///   @SubstitutePallet int = null,
  ///   @Litres numeric(13,3) = null,
  ///   @ParentSKUCode nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   SKUHistory.SKUId,
  ///   SKUHistory.UOMId,
  ///   SKUHistory.SKU,
  ///   SKUHistory.SKUCode,
  ///   SKUHistory.Quantity,
  ///   SKUHistory.CommandType,
  ///   SKUHistory.InsertDate,
  ///   SKUHistory.AlternatePallet,
  ///   SKUHistory.SubstitutePallet,
  ///   SKUHistory.Litres,
  ///   SKUHistory.ParentSKUCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKUHistory_Insert
(
 @SKUId int = null,
 @UOMId int = null,
 @SKU nvarchar(100) = null,
 @SKUCode nvarchar(100) = null,
 @Quantity float = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @AlternatePallet int = null,
 @SubstitutePallet int = null,
 @Litres numeric(13,3) = null,
 @ParentSKUCode nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert SKUHistory
        (SKUId,
         UOMId,
         SKU,
         SKUCode,
         Quantity,
         CommandType,
         InsertDate,
         AlternatePallet,
         SubstitutePallet,
         Litres,
         ParentSKUCode)
  select @SKUId,
         @UOMId,
         @SKU,
         @SKUCode,
         @Quantity,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @AlternatePallet,
         @SubstitutePallet,
         @Litres,
         @ParentSKUCode 
  
  select @Error = @@Error
  
  
  return @Error
  
end
