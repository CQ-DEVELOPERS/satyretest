﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Sample_Quantity_Writeoff
  ///   Filename       : p_Sample_Quantity_Writeoff.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Mar 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Sample_Quantity_Writeoff
(
 @ReceiptId int
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @SampleQuatity      numeric(13,6),
          @WarehouseId        int,
          @FromWarehouseCode  nvarchar(30),
          @ToWarehouseCode    nvarchar(30),
          @RecordType         nvarchar(3)
  
  set @Errormsg = 'Error executing p_Sample_Quantity_Writeoff'
  
  select @WarehouseId = WarehouseId
    from Receipt (nolock)
   where ReceiptId = @ReceiptId
  
  select @RecordType = convert(nvarchar(3), Value)
    from Configuration (nolock)
   where ConfigurationId = 353
     and WarehouseId     = @WarehouseId
  
  if isnull(@RecordType,'') = ''
    set @RecordType = 'SOH'
  
  select @FromWarehouseCode = WarehouseCode
    from Warehouse (nolock)
   where WarehouseId = @WarehouseId
  
  if @RecordType = 'XFR'
    select @ToWarehouseCode = WarehouseCode
      from Warehouse (nolock)
     where Warehouse like '%Sam%'
  
  begin transaction
  
  insert InterfaceExportStockAdjustment
        (RecordType,
         RecordStatus,
         ProductCode,
         SKUCode,
         Batch,
         Quantity,
         Additional1,
         Additional2,
         Additional3,
         Additional4)
  select @RecordType,
         'N',
         p.ProductCode,
         sku.SKUCode,
         b.Batch,
         case when rl.SampleQuantity > isnull(p.Samples,0)
              then -(rl.SampleQuantity - isnull(p.RetentionSamples,0))
              else -(isnull(p.Samples,0) - isnull(p.RetentionSamples,0))
              end,
         @FromWarehouseCode,
         @ToWarehouseCode,
         'Writeoff Sample Qty',
         'ReceiptLineId = ' + convert(nvarchar(10), rl.ReceiptLineId)
    from Receipt            r (nolock)
  	 join ReceiptLine       rl (nolock) on r.ReceiptId           = rl.ReceiptId
  	 join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
  	 join Batch              b (nolock) on sub.BatchId           = b.BatchId
  	 join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
  	 join Product            p (nolock) on su.ProductId          = p.ProductId
  	 join SKU              sku (nolock) on su.SKUId              = sku.SKUId
   where r.ReceiptId       = @ReceiptId
	    and rl.SampleQuantity > 0
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
