﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_WarehouseCodeReference_Insert
  ///   Filename       : p_WarehouseCodeReference_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 09:52:55
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the WarehouseCodeReference table.
  /// </remarks>
  /// <param>
  ///   @WarehouseCodeReferenceId int = null output,
  ///   @WarehouseCode nvarchar(20) = null,
  ///   @DownloadType nvarchar(20) = null,
  ///   @WarehouseId int = null,
  ///   @InboundDocumentTypeId int = null,
  ///   @OutboundDocumentTypeId int = null,
  ///   @ToWarehouseId int = null,
  ///   @ToWarehouseCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   WarehouseCodeReference.WarehouseCodeReferenceId,
  ///   WarehouseCodeReference.WarehouseCode,
  ///   WarehouseCodeReference.DownloadType,
  ///   WarehouseCodeReference.WarehouseId,
  ///   WarehouseCodeReference.InboundDocumentTypeId,
  ///   WarehouseCodeReference.OutboundDocumentTypeId,
  ///   WarehouseCodeReference.ToWarehouseId,
  ///   WarehouseCodeReference.ToWarehouseCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_WarehouseCodeReference_Insert
(
 @WarehouseCodeReferenceId int = null output,
 @WarehouseCode nvarchar(20) = null,
 @DownloadType nvarchar(20) = null,
 @WarehouseId int = null,
 @InboundDocumentTypeId int = null,
 @OutboundDocumentTypeId int = null,
 @ToWarehouseId int = null,
 @ToWarehouseCode nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
  if @WarehouseCodeReferenceId = '-1'
    set @WarehouseCodeReferenceId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @InboundDocumentTypeId = '-1'
    set @InboundDocumentTypeId = null;
  
  if @OutboundDocumentTypeId = '-1'
    set @OutboundDocumentTypeId = null;
  
  if @ToWarehouseId = '-1'
    set @ToWarehouseId = null;
  
	 declare @Error int
 
  insert WarehouseCodeReference
        (WarehouseCode,
         DownloadType,
         WarehouseId,
         InboundDocumentTypeId,
         OutboundDocumentTypeId,
         ToWarehouseId,
         ToWarehouseCode)
  select @WarehouseCode,
         @DownloadType,
         @WarehouseId,
         @InboundDocumentTypeId,
         @OutboundDocumentTypeId,
         @ToWarehouseId,
         @ToWarehouseCode 
  
  select @Error = @@Error, @WarehouseCodeReferenceId = scope_identity()
  
  
  return @Error
  
end
