﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shipment_Search
  ///   Filename       : p_Shipment_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:50
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Shipment table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Shipment.ShipmentId,
  ///   Shipment.StatusId,
  ///   Shipment.WarehouseId,
  ///   Shipment.LocationId,
  ///   Shipment.ShipmentDate,
  ///   Shipment.Remarks,
  ///   Shipment.SealNumber,
  ///   Shipment.VehicleRegistration,
  ///   Shipment.Route 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shipment_Search
(
 @ShipmentId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ShipmentId = '-1'
    set @ShipmentId = null;
  
 
  select
         Shipment.ShipmentId
        ,Shipment.StatusId
        ,Shipment.WarehouseId
        ,Shipment.LocationId
        ,Shipment.ShipmentDate
        ,Shipment.Remarks
        ,Shipment.SealNumber
        ,Shipment.VehicleRegistration
        ,Shipment.Route
    from Shipment
   where isnull(Shipment.ShipmentId,'0')  = isnull(@ShipmentId, isnull(Shipment.ShipmentId,'0'))
  
end
