﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Wave_Planning_Linked_Issue
  ///   Filename       : p_Wave_Planning_Linked_Issue.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Wave_Planning_Linked_Issue
(
 @waveId int
)

as
begin
	 set nocount on;
  
  select osi.OutboundShipmentId,
         i.IssueId,
         od.OrderNumber,
         i.Weight as 'OrderWeight',
         i.Volume as 'OrderVolume'
    from OutboundDocument       od  (nolock)
    join Issue                  i   (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
                                            and i.WaveId              = @waveId
    left
    join OutboundShipmentIssue  osi (nolock) on i.IssueId             = osi.IssueId
    left
    join OutboundShipment        os (nolock) on osi.OutboundShipmentId = os.OutboundShipmentId
                                            and osi.OutboundShipmentId = @waveId
end
