﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKUInfo_Insert
  ///   Filename       : p_SKUInfo_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:36
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the SKUInfo table.
  /// </remarks>
  /// <param>
  ///   @SKUId int = null,
  ///   @WarehouseId int = null,
  ///   @Quantity float = null,
  ///   @AlternatePallet int = null,
  ///   @SubstitutePallet int = null 
  /// </param>
  /// <returns>
  ///   SKUInfo.SKUId,
  ///   SKUInfo.WarehouseId,
  ///   SKUInfo.Quantity,
  ///   SKUInfo.AlternatePallet,
  ///   SKUInfo.SubstitutePallet 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKUInfo_Insert
(
 @SKUId int = null,
 @WarehouseId int = null,
 @Quantity float = null,
 @AlternatePallet int = null,
 @SubstitutePallet int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert SKUInfo
        (SKUId,
         WarehouseId,
         Quantity,
         AlternatePallet,
         SubstitutePallet)
  select @SKUId,
         @WarehouseId,
         @Quantity,
         @AlternatePallet,
         @SubstitutePallet 
  
  select @Error = @@Error
  
  
  return @Error
  
end
