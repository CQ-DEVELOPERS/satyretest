﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKUCheck_Update
  ///   Filename       : p_SKUCheck_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:34
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the SKUCheck table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKUCheck_Update
(
 @JobId int = null,
 @SKUCode nvarchar(20) = null,
 @Quantity float = null,
 @ConfirmedQuantity float = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update SKUCheck
     set JobId = isnull(@JobId, JobId),
         SKUCode = isnull(@SKUCode, SKUCode),
         Quantity = isnull(@Quantity, Quantity),
         ConfirmedQuantity = isnull(@ConfirmedQuantity, ConfirmedQuantity) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
