﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Variable_Weights_Job_Select
  ///   Filename       : p_Variable_Weights_Job_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Variable_Weights_Job_Select
(
 @barcode nvarchar(30),
 @WarehouseId int = null
)

as
begin
	 set nocount on;
	 
	 declare @JobId              int,
          @PalletId           int
  
  if isnumeric(replace(@barcode,'J:','')) = 1
    select @JobId   = replace(@barcode,'J:',''),
           @barcode = null
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @PalletId is not null
    select @JobId = j.JobId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Job              j (nolock) on i.JobId             = j.JobId
      join Status           s (nolock) on j.StatusId          = s.StatusId
     where i.PalletId              = @PalletId
       and s.StatusCode           in ('CK','CD','D','QA','DC','C')
       and it.InstructionTypeCode in ('P','PM','PS','FM')
  
  if @barcode is not null
  begin
    select @JobId = JobId
      from Job (nolock)
     where ReferenceNumber = @barcode
  end
  
  select top 1
         i.InstructionId,
         i.JobId,
         s.Status,
         s.StatusCode,
         i.PalletId,
         i.CreateDate,
         j.TareWeight,
         j.Weight as 'GrossWeight',
         j.NettWeight
    from Instruction         i   (nolock)
    join Job                 j   (nolock) on i.JobId             = j.JobId
    join InstructionType     it  (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status              s   (nolock) on j.StatusId          = s.StatusId
   where s.Type                  = 'IS'
     and s.StatusCode           in ('CK','CD','D','QA','DC','C')
     and i.JobId                 = @JobId
     and it.InstructionTypeCode in ('P','PM','PS','FM','PR')
end
