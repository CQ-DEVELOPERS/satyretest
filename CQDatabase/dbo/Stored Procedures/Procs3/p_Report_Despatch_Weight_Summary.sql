﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Despatch_Weight_Summary
  ///   Filename       : p_Report_Despatch_Weight_Summary.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Despatch_Weight_Summary 
(  
 @ManifestNumber		int,  
 @ReferenceNumber       nvarchar(60),
 @ExternalCompanyId		int,
 @FromDate				date,
 @ToDate				date  
)  
  
as  
begin  
  set nocount on;  
    
  declare @TableResult as table  
  (
   OutboundShipmentId              int              null,
   IssueId                         int              null,
   OutboundDocumentId              int              null,
   OrderNumber                     nvarchar(60)     null,
   ExternalCompanyId               int              null,
   ExternalCompany                 nvarchar(510)    null,
   DespatchDate                    datetime         null,
   LocationId                      int              null,
   Location                        nvarchar(30)     null,
   JobId                           int              null,
   StatusId                        int              null,
   Status                          nvarchar(100)    null,
   InstructionId                   int              null,
   StorageUnitBatchId              int              null,
   ProductCode					   nvarchar(50)		null,
   Product						   nvarchar(255)	null,
   SKUCode						   nvarchar(10)		null,
   SKU							   nvarchar(50)		null,
   Quantity                        float            null,
   ConfirmedQuantity               float            null,
   DropSequence                    int              null,
   RouteId                         int              null,
   Route                           nvarchar(100)    null,
   Weight                          float            null,
   ConfirmedWeight				   float			null,
   CheckWeight					   float			null,
   ReferenceNumber                 nvarchar(60)     null,
   PickerId                        int              null,
   Picker                          nvarchar(100)    null,
   CheckerId                       int              null,
   Checker                         nvarchar(100)    null,
   StartDate                       datetime         null,
   CompleteDate                    datetime         null,
   ManifestNumber				   int				null,
   PickLocation					   nvarchar(50)		null,
   PickLocationId				   int				null,
   StoreLocation				   nvarchar(50)		null,
   StoreLocationId				   int				null
  )

  
  if @ManifestNumber in (0,-1) 
    set @ManifestNumber = null  
    
  if @ExternalCompanyId = -1  
    set @ExternalCompanyId = null  
    
  if @ReferenceNumber in ('','-1')   
    set @ReferenceNumber = null  
    
  insert @TableResult  
        (OutboundShipmentId,  
         OutboundDocumentId,
         IssueId, 
         OrderNumber,
         JobId,  
         StatusId,
         Status,  
         InstructionId,  
         StorageUnitBatchId,  
         Quantity,  
         ConfirmedQuantity,  
		 PickLocationId	,
		 StoreLocationId ,
         PickerId,  
         StartDate,  
         CompleteDate,  
         RouteId,
         DropSequence,
         ReferenceNumber,
         Weight,
         ConfirmedWeight,
         CheckWeight,
         ManifestNumber,
         ExternalCompanyId,
         ProductCode,
         Product,
         SKUCode,
         SKU)
 select distinct
		i.OutboundShipmentId, 
		od.OutboundDocumentId,
		iss.IssueId,
		od.OrderNumber,
		i.JobId,
		s.StatusId,
		s.Status,
		i.InstructionId,
		i.StorageUnitBatchId,
		i.Quantity,
		i.ConfirmedQuantity,
        i.PickLocationId,
        i.StoreLocationId,
        j.OperatorId,
        i.StartDate,
        i.EndDate,
        iss.routeid,
        i.DropSequence,
        j.ReferenceNumber,
        i.Weight,
        i.ConfirmedWeight,
        i.CheckWeight,
        iss.WaveId,
        od.ExternalCompanyId,
        vs.ProductCode,
        vs.Product,
        vs.SKUCode,
        vs.SKU
  from Instruction i
  join Status          si (nolock) on i.StatusId           = si.StatusId
  join InstructionType it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
  join Job              j (nolock) on i.JobId              = j.JobId  
  join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
  join IssueLine             il (nolock) on ili.IssueLineId = il.IssueLineId
  join Issue                iss (nolock) on il.IssueId = iss.IssueId
  join Status				  s (nolock) on iss.StatusId           = s.StatusId
  join OutboundLine          ol (nolock) on il.OutboundLineId = ol.OutboundLineId
  join OutboundDocument od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
  left
  join viewStock       vs          on i.StorageUnitBatchId = vs.StorageUnitBatchId
  where s.statusid in (dbo.ufn_StatusId('ISS','CD'),dbo.ufn_StatusId('ISS','D'),dbo.ufn_StatusId('ISS','DC'))
  and i.EndDate between @FromDate and @ToDate
  and iss.WaveId = isnull(@ManifestNumber,iss.WaveId)
  and od.ExternalCompanyId = isnull(@ExternalCompanyId,od.ExternalCompanyId)
  and j.ReferenceNumber = isnull(@ReferenceNumber,j.ReferenceNumber)
  
    
   update tr  
      set PickLocation = pick.Location
     from @TableResult     tr  
     join Location      pick (nolock) on tr.PickLocationId = pick.LocationId
     
  update tr  
      set StoreLocation = store.Location
     from @TableResult     tr  
     join Location      store (nolock) on tr.StoreLocationId = store.LocationId
  
  update tr  
      set Picker = o.Operator
     from @TableResult     tr  
     join Operator         o (nolock) on tr.PickerId = o.OperatorId
     
    
  update tr  
     set ExternalCompany = ec.ExternalCompany  
    from @TableResult    tr  
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId  

  
  select OutboundShipmentId, 
		 OrderNumber,
		 Product,
		 ProductCode,
		 SkuCode,
		 Sku,
         Weight,
         CompleteDate,  
         ExternalCompany,
		 Quantity,
		 ConfirmedQuantity,     
         ReferenceNumber,          
         ManifestNumber,
		 PickLocation,
		 StoreLocation  
    from @TableResult
    order by CompleteDate,
			 OrderNumber
		

end  
