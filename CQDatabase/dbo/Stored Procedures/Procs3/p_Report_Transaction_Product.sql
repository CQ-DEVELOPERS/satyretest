﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Transaction_Product
  ///   Filename       : p_Report_Transaction_Product.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Transaction_Product
(
 @WarehouseId      int,
 @StorageUnitId    int = null,
 @BatchId          int = null,
 @FromDate         datetime,
 @ToDate           datetime,
 @PrincipalId	   int
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  InstructionId       int,
	  InstructionTypeId   int,
	  InstructionType     nvarchar(30),
	  InstructionTypeCode nvarchar(10),
	  StatusId            int,
	  Status              nvarchar(50),
	  CreateDate          datetime,
	  StartDate           datetime,
	  EndDate             datetime,
	  OperatorId          int,
	  Operator            nvarchar(50),
	  StorageUnitBatchId  int,
	  ProductCode         nvarchar(50),
	  Product             nvarchar(255),
	  SKUCode             nvarchar(50),
	  SKU				  nvarchar(50),
	  Batch               nvarchar(50),
	  ExpiryDate		  datetime,
	  Location            nvarchar(15),
      PickLocationId      int null,
      PickLocation        nvarchar(15),
      StoreLocationId     int null,
      StoreLocation       nvarchar(15),
	  Quantity            float,
	  ConfirmedQuantity   float,
	  ReceiptLineId       int,
	  CheckQuantity       float,
	  NettWeight		  float,
	  DRCR				  int,
	  PalletId			  int,
	  OrderNumber		  nvarchar(30),
	  PrincipalId		  int,
	  PrincipalCode		  nvarchar(50)
	 )
	 
	 if @StorageUnitId = -1
	   set @StorageUnitId = null
	 
	 if @BatchId = -1
	   set @BatchId = null
	  
	 if @PrincipalId = -1
		set @PrincipalId = null
  
  insert @TableResult
         (InstructionId,
          InstructionTypeId,
	      StatusId,
	      CreateDate,
	      StartDate,
	      EndDate,
	      OperatorId,
	      StorageUnitBatchId,
          PickLocationId,
          StoreLocationId,
	      Quantity,
	      ConfirmedQuantity,
	      ReceiptLineId,
	      CheckQuantity,
	      PalletId,
	      OrderNumber)
  select  i.InstructionId,
          i.InstructionTypeId,
          i.StatusId,
	      i.CreateDate,
	      i.StartDate,
	      i.EndDate,
	      i.OperatorId,
	      i.StorageUnitBatchId,
          i.PickLocationId,
          i.StoreLocationId,
	      i.Quantity,
	      i.ConfirmedQuantity,
	      i.ReceiptLineId,
	      i.CheckQuantity,
	      i.PalletId,
	      isnull(od.OrderNumber, id.OrderNumber)
    from Instruction            i (nolock)
    join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    left join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
    left join OutboundDocument      od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
    left join ReceiptLine           rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
    left join Inboundline           il (nolock) on il.InboundLineId = rl.InboundLineId
    left join InboundDocument	    id (nolock) on id.InboundDocumentId = il.InboundDocumentId
   where i.WarehouseId = @WarehouseId
     and (i.CreateDate between @FromDate and @ToDate
     or   EndDate      between @FromDate and @ToDate)
     and sub.StorageUnitId  = isnull(@StorageUnitId, sub.StorageUnitId)
     and sub.BatchId        = isnull(@BatchId, sub.BatchId)
     
 
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         ExpiryDate	   = b.ExpiryDate,
         SKU		   = sku.SKU,
         PrincipalId   = p.PrincipalId
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit          su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product               p (nolock) on su.ProductId          = p.ProductId
    join SKU                 sku (nolock) on su.SKUId              = sku.SKUId
    join Batch                 b (nolock) on sub.BatchId           = b.BatchId
  
  update tr
     set Batch         = b.Batch + ' *'
    from @TableResult tr
    join ReceiptLine          rl (nolock) on tr.ReceiptLineId      = rl.ReceiptLineId
    join StorageUnitBatch    sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch                 b (nolock) on sub.BatchId           = b.BatchId
   where tr.Batch = 'Default'
  
  update tr
     set InstructionType = it.InstructionType,
         InstructionTypeCode = it.InstructionTypeCode
    from @TableResult    tr
    join InstructionType it (nolock) on tr.InstructionTypeId = it.InstructionTypeId
  
 
  update @TableResult
     set InstructionType = InstructionType + '*User'
    where CheckQuantity = 1
      and InstructionTypeCode = 'R'
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.StatusId = s.StatusId
    
  update tr
     set PrincipalCode = p.PrincipalCode
    from @TableResult tr
    join Principal        p (nolock) on tr.PrincipalId = p.PrincipalId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId

  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId

  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
    
  update tr
     set DRCR = ConfirmedQuantity 
    from @TableResult tr
  
  update tr
     set DRCR = ConfirmedQuantity * -1
    from @TableResult tr
   where InstructionTypeCode in ('M', 'P', 'O', 'PM', 'PS', 'FM')
  
   update tr
     set DRCR = ConfirmedQuantity 
    from @TableResult tr
   where InstructionTypeCode in ('R', 'S', 'SM', 'PR', 'HS', 'RS')
   
   update tr
     set DRCR = isnull(ConfirmedQuantity,0) - isnull(Quantity,0)
    from @TableResult tr
   where InstructionTypeCode in ('STE', 'STL', 'STA', 'STP')
   
   update tr
     set DRCR = 0
    from @TableResult tr
   where tr.Status in ('Waiting', 'Deleted')
   
  select    InstructionType
	       ,Status
	       ,Operator
	       ,ProductCode
	       ,Product
	       ,SKUCode
	       ,SKU
	       ,Batch
	       ,ExpiryDate
	       ,InstructionId
	       ,PickLocation
	       ,StoreLocation
	       ,Quantity
	       ,ConfirmedQuantity
	       ,CreateDate
	       ,StartDate
	       ,EndDate
	       ,NettWeight
	       ,DRCR
	       ,PalletId
	       ,OrderNumber
	       ,PrincipalCode
    from @TableResult 
    where isnull(PrincipalId, -1) = isnull(@PrincipalId, isnull(PrincipalId, -1))
  order by ProductCode, SKUCode, isnull(EndDate, isnull(StartDate, CreateDate))
end
 
 
