﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Set_By_Type
  ///   Filename       : p_SerialNumber_Set_By_Type.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jul 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Set_By_Type
(
 @warehouseId  int,
 @operatorId   int,
 @keyId        int,
 @keyType      nvarchar(100),
 @serialNumber nvarchar(50)
)

as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_SerialNumber_Set_By_Type',
          @GetDate           datetime,
          @Transaction       bit = 1
  
  select @GetDate = dbo.ufn_Getdate()
  
    declare @ReceiptId          int,
            @ReceiptLineId      int,
            @IssueId            int,
            @IssueLineId        int,
            @StoreInstructionId int,
            @PickInstructionId  int,
            @SerialNumberId     int,
            @StorageUnitBatchId int,
            @StorageUnitId      int,
            @BatchId            int,
            @ReceivedDate       datetime,
            @DespatchDate       datetime,
            @PickJobId          int,
            @StoreJobId         int,
            @Quantity           numeric(13,6),
            @ScannedQuantity    numeric(13,6)
  
  if @keyType = 'ReceiptId'
    set @ReceiptId = @keyId
  
  if @keyType = 'ReceiptLineId'
    set @ReceiptLineId = @keyId
  
  if @keyType = 'IssueId'
    set @IssueId = @keyId
  
  if @keyType = 'IssueLineId'
    set @IssueLineId = @keyId
  
  if @keyType = 'StoreInstructionId'
    set @StoreInstructionId = @keyId
  
  if @keyType = 'PickInstructionId'
    set @PickInstructionId = @keyId
  
  if @keyType = 'PickJobId'
    set @PickJobId = @keyId
  
  if @keyType = 'StoreJobId'
    set @StoreJobId = @keyId
  
  if @StoreInstructionId is not null
  begin
    select @ReceiptLineId = ReceiptLineId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @Quantity           = ConfirmedQuantity
      from Instruction (nolock)
     where InstructionId = @StoreInstructionId
    
    select @ScannedQuantity = COUNT(1)
      from SerialNumber (nolock)
     where StoreInstructionId = @StoreInstructionId
  end
  
  if @ReceiptLineId is not null
  begin
    select @ReceiptId = ReceiptId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @Quantity           = AcceptedQuantity
      from ReceiptLine (nolock)
     where ReceiptLineId = @ReceiptLineId
    
    select @ScannedQuantity = COUNT(1)
      from SerialNumber (nolock)
     where ReceiptLineId = @ReceiptLineId
  end
  
  if @ReceiptId is not null or @ReceivedDate is null
    set @ReceivedDate = @GetDate
  
  if @PickInstructionId is not null
  begin
    select @IssueLineId = ili.IssueLineId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @Quantity           = sum(i.ConfirmedQuantity)
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId in (i.InstructionRefId, i.InstructionId)
     where i.InstructionId = @PickInstructionId
    group by ili.IssueLineId, StorageUnitBatchId
    
    select @ScannedQuantity = COUNT(1)
      from SerialNumber (nolock)
     where PickInstructionId = @PickInstructionId
  end
  
  if @PickJobId is not null
  begin
    select @IssueLineId = ili.IssueLineId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @Quantity           = sum(i.ConfirmedQuantity)
      from IssueLineInstruction ili (nolock)
      join Instruction            i (nolock) on ili.InstructionId in (i.InstructionRefId, i.InstructionId)
     where i.JobId = @PickJobId
    group by ili.IssueLineId, StorageUnitBatchId
    
    select @ScannedQuantity = COUNT(1)
      from SerialNumber (nolock)
     where PickJobId = @PickJobId
  end
  
  if @IssueLineId is not null
  begin
    select @IssueId = il.IssueId,
           @StorageUnitBatchId = il.StorageUnitBatchId,
           @PickJobId = i.JobId
      from IssueLine il (nolock)
      join Instruction i (nolock) on il.IssueLineId = i.IssueLineId
     where il.IssueLineId = @IssueLineId
    
    select @Quantity = SUM(ConfirmedQuatity)
      from IssueLine a (nolock)
      join StorageUnitBatch sub (nolock) on a.StorageUnitBatchId = sub.StorageUnitBatchId
     where a.IssueLineId = @keyId
       and sub.StorageUnitId = isnull(@storageUnitId, sub.StorageUnitId)
     
    select @ScannedQuantity = COUNT(1)
      from SerialNumber (nolock)
     where PickJobId = @PickJobId
       and IssueLineId = @IssueLineId
  end
  
  if @IssueId is not null
    set @DespatchDate = @GetDate
  
  select @StorageUnitId = StorageUnitId,
         @Batchid       = BatchId
    from StorageUnitBatch (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  if @ScannedQuantity >= @Quantity
  begin
    set @Error = -2
    goto Error
  end
  
  select @SerialNumberId = SerialNumberId
    from SerialNumber
   where SerialNumber = @SerialNumber
     and StorageUnitId = @StorageUnitId
  
  if @SerialNumberId is null
  begin
    exec @Error = p_SerialNumber_Insert
     @SerialNumberId      = @SerialNumberId output,
     --@StorageUnitBatchId  = @StorageUnitBatchId,
     @StorageUnitId       = @StorageUnitId,
     @BatchId             = @BatchId,
     
     @LocationId          = null,
     @ReferenceNumber     = null,
     
     @ReceiptId           = @ReceiptId,
     @ReceiptLineId       = @ReceiptLineId,
     @StoreInstructionId  = @StoreInstructionId,
     
     @IssueId             = @IssueId,
     @IssueLineId         = @IssueLineId,
     @PickInstructionId   = @PickInstructionId,
     
     @SerialNumber        = @SerialNumber,
     @ReceivedDate        = @ReceivedDate,
     @DespatchDate        = @DespatchDate,
     
     @PickJobId           = @PickJobId,
     @StoreJobId          = @StoreJobId
    
    if @Error <> 0
      goto error
  end
  else
  begin
    exec @Error = p_SerialNumber_Update
     @SerialNumberId      = @SerialNumberId,
     --@StorageUnitBatchId  = @StorageUnitBatchId,
     
     @LocationId          = null,
     @ReferenceNumber     = null,
     
     @ReceiptId           = @ReceiptId,
     @ReceiptLineId       = @ReceiptLineId,
     @StoreInstructionId  = @StoreInstructionId,
     
     @IssueId             = @IssueId,
     @IssueLineId         = @IssueLineId,
     @PickInstructionId   = @PickInstructionId,
     
     @SerialNumber        = @SerialNumber,
     @ReceivedDate        = @ReceivedDate,
     @DespatchDate        = @DespatchDate,
     
     @PickJobId           = @PickJobId,
     @StoreJobId          = @StoreJobId
    
    if @Error <> 0
      goto error
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
