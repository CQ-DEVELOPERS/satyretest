﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Location_Breakdown
  ///   Filename       : p_Report_Location_Breakdown.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Location_Breakdown
(
 @ConnectionString nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId      int
)

as
begin
	 set nocount on;
  
  select a.Area,
         count(1) as 'Value'
    from Area          a (nolock)
    join AreaLocation al (nolock) on a.AreaId = al.AreaId
    join Location      l (nolock) on al.LocationId = l.LocationId
    where a.WarehouseId = @WarehouseId
      and a.StockOnHand = 1
  group by a.Area
  order by 'Value' desc
end
