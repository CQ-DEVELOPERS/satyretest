﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Take_On_Stock_Import
  ///   Filename       : p_Take_On_Stock_Import.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Take_On_Stock_Import
(
 @xmlstring nvarchar(max)
)

as
begin
  set nocount on;
  
  declare @Error				int,
          @Errormsg				varchar(500) = 'Error executing p_Take_On_Stock_Import',
          @GetDate				datetime,
          @Transaction			bit = 0,
          @DocHandle			int,
          @Xmldoc				xml,
          @Location               nvarchar(50),
          @ProductCode            nvarchar(50),
          @Batch                  nvarchar(50),
          @Quantity			      nvarchar(50),
          @UnitPrice              nvarchar(50),
          @InterfaceImportTakeOnStockId	int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
   
  select @GetDate = dbo.ufn_Getdate()
  
  set @Xmldoc = convert(xml,@xmlstring)
   
  EXEC sp_xml_preparedocument @DocHandle OUTPUT, @Xmldoc
   
  select @Location			= Location,
         @ProductCode		= ProductCode,
         @Batch				= Batch,
         @Quantity			= Quantity,
         @UnitPrice			= case when isnumeric(UnitPrice) = 1
                                         then UnitPrice
                                         end
  
  from OPENXML (@Dochandle, '/root/Line',1)
              WITH (Location nvarchar(50) 'Location',
              ProductCode nvarchar(50) 'ProductCode',
              Batch nvarchar(50) 'Batch',
              Quantity nvarchar(50) 'Quantity',
              UnitPrice nvarchar(50) 'UnitPrice'
              )
  
select @Location			'Location',
       @ProductCode			'ProductCode',
       @Batch               'Batch',
       @Quantity			'Quantity',       
       @UnitPrice			'UnitPrice'
  
  select @InterfaceImportTakeOnStockId = max(InterfaceImportTakeOnStockId)
    from InterfaceImportTakeOnStock
   where Location = @Location
     and ProductCode   = @ProductCode
     and InsertDate  > dateadd(mi, -1, @getdate)
     and RecordType in ('TOS')
     --and isnull(Module, 'Inbound') = 'Inbound'
  
  if @InterfaceImportTakeOnStockId is null
  begin
    insert InterfaceImportTakeOnStock
          (Location,
           ProductCode,
           Batch,
           Quantity,
           UnitPrice,
           RecordType,
           InsertDate)
    select @Location,
           @ProductCode,
           @Batch,
           @Quantity,
           @UnitPrice,
           'TOS' ,
           @GetDate   
    select @Error = @@Error, @InterfaceImportTakeOnStockId = scope_identity()
    
    if @Error <> 0
      goto error
  end
  

  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
 
 
 
