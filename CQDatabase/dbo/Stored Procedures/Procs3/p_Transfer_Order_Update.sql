﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Transfer_Order_Update
  ///   Filename       : p_Transfer_Order_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Transfer_Order_Update
(
 @outboundShipmentId  int,
 @issueId             int,
 @batch               nvarchar(50),
 @manufacturingId     int,
 @rawStagingId        int,
 @productionStagingId int
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
  (
   OutboundShipmentId              int,
   IssueId                         int,
   IssueLineId                     int,
   InstructionId                   int,
   JobId                           int
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @rowcount          int
  
  if @outboundShipmentId = -1
    set @outboundShipmentId = null
  
  if @issueId = -1
    set @issueId = null
  
  if @batch = ''
    set @batch = null
  
  if @manufacturingId = -1
    set @manufacturingId = null
  
  if @rawStagingId = -1
    set @rawStagingId = null
  
  if @productionStagingId = -1
    set @productionStagingId = null
  
  if @outboundShipmentId is not null
  begin
    insert @TableResult
          (OutboundShipmentId,
           IssueId,
           IssueLineId,
           InstructionId,
           JobId)
    select ili.OutboundShipmentId,
           ili.IssueId,
           ili.IssueLineId,
           ili.InstructionId,
           i.JobId
      from IssueLineInstruction ili
      join Instruction            i on ili.InstructionId = i.InstructionId
     where ili.OutboundShipmentId = isnull(@outboundShipmentId, -1)
    
    select @rowcount = @@rowcount
    
    if @rowcount = 0
    begin
      insert @TableResult
            (OutboundShipmentId,
             IssueId,
             IssueLineId,
             InstructionId,
             JobId)
      select osi.OutboundShipmentId,
             il.IssueId,
             il.IssueLineId,
             null,
             null
        from OutboundShipmentIssue osi (nolock)
        join IssueLine              il (nolock) on osi.IssueId = il.IssueId
       where osi.OutboundShipmentId = isnull(@outboundShipmentId, -1)
      
      select @rowcount = @@rowcount
    
      if @rowcount = 0
        return
    end
  end
  else
  begin
    insert @TableResult
          (OutboundShipmentId,
           IssueId,
           IssueLineId,
           InstructionId,
           JobId)
    select ili.OutboundShipmentId,
           ili.IssueId,
           ili.IssueLineId,
           ili.InstructionId,
           i.JobId
      from IssueLineInstruction ili
      join Instruction            i on ili.InstructionId = i.InstructionId
     where ili.IssueId = isnull(@issueId, -1)
    
    select @rowcount = @@rowcount
    
    if @rowcount = 0
    begin
      insert @TableResult
            (OutboundShipmentId,
             IssueId,
             IssueLineId,
             InstructionId,
             JobId)
      select null,
             il.IssueId,
             il.IssueLineId,
             null,
             null
        from IssueLine il (nolock)
       where il.IssueId = isnull(@issueId, -1)
      
      select @rowcount = @@rowcount
    
      if @rowcount = 0
        return
    end
  end
  
  begin transaction
  
  update u
     set ManufacturingId = @manufacturingId,
         LocationId   = @rawStagingId,
         DespatchBay  = @productionStagingId,
         Remarks      = @Batch
    from @TableResult tr
    join Issue         u (nolock) on tr.IssueId = u.IssueId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Transfer_Order_Update'); 
    rollback transaction
    return @Error
end

