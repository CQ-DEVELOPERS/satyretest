﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shifts_Update
  ///   Filename       : p_Shifts_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:47
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Shifts table.
  /// </remarks>
  /// <param>
  ///   @ShiftId int = null,
  ///   @Starttime int = null,
  ///   @EndTime int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shifts_Update
(
 @ShiftId int = null,
 @Starttime int = null,
 @EndTime int = null 
)

as
begin
	 set nocount on;
  
  if @ShiftId = '-1'
    set @ShiftId = null;
  
	 declare @Error int
 
  update Shifts
     set Starttime = isnull(@Starttime, Starttime),
         EndTime = isnull(@EndTime, EndTime) 
   where ShiftId = @ShiftId
  
  select @Error = @@Error
  
  
  return @Error
  
end
