﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Inbound_Query
  ///   Filename       : p_Report_Inbound_Query.sql
  ///   Create By      : Karen
  ///   Date Created   : April 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Inbound_Query
(
 @WarehouseId			int,
 @InboundShipmentId		int = null,
 @PrincipalId			int = null,
 @InboundDocumentTypeId	nvarchar(1000) = null,
 @OrderNumber			nvarchar(30) = null,
 @DeliveryNoteNumber	nvarchar(30) = null,
 @SupplierCode			nvarchar(30) = null,
 @SupplierName			nvarchar(30) = null,
 @FromDate				datetime,
 @ToDate				datetime,
 @DateUsed				nvarchar(30) = null,
 @Status				nvarchar(30) = null
)

as
begin
	 set nocount on;
  
  declare @TableHeader as Table
  (
   InboundShipmentId		int,
   InboundDocumentId		int,
   ReceiptId				int,
   OrderNumber				nvarchar(30),
   ExternalCompanyId		int,
   Supplier					nvarchar(255),
   SupplierCode				nvarchar(30),
   Delivery					nvarchar(1),
   NumberOfLines			int,
   DeliveryNoteNumber		nvarchar(30),
   DeliveryDate				datetime,   
   StatusId					int,
   Status					nvarchar(30),
   InboundDocumentTypeId	int,
   InboundDocumentType		nvarchar(20),		
   ReceivingCompleteDate	datetime,
   SearchDate				datetime,
   CreateDate				datetime,
   PlannedDeliveryDate		datetime
  );
  
  declare @TableDetails as Table
  (
   ReceiptId			int,
   ReceiptLineId		int,
   StorageUnitBatchId	int,
   StorageUnitId		int,
   ProductCode			nvarchar(30),
   Product				nvarchar(255),
   ConfirmedQuantity	float,
   CreateDate			datetime,
   ReceivedDate			datetime,
   ReceiveCompleteDate	datetime,
   PutawayMarshalling	datetime,
   PutawayBulk			datetime,
   StatusId				int,
   Status				nvarchar(30),
   JobId				int,  
   InstructionTypeId	int, 
   InstructionType		nvarchar(20),
   PickLocationId		int,
   PickLocation			nvarchar(20),
   StoreLocationId		int,
   StoreLocation		nvarchar(20),
   ExpectedQuantity		numeric(13,6),
   AcceptedQuantity		numeric(13,6),
   OperatorCode			nvarchar(50),
   Operator				nvarchar(50),
   PalletId				int
  );
  
  declare	@StatusId		int,
			@SearchDate		datetime  
  
  if @WarehouseId = -1
    set @WarehouseId = null
    
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @PrincipalId = -1
    set @PrincipalId = null
    
  if @InboundDocumentTypeId = '-1'
    set @InboundDocumentTypeId = null
    
  if @OrderNumber = ''
    set @OrderNumber = null
    
  if @DeliveryNoteNumber = ''
    set @DeliveryNoteNumber = null
    
  if @SupplierCode = ''
    set @SupplierCode = null
    
  if @SupplierName = ''
    set @SupplierName = null
    
  if @Status = ''
    set @StatusId = null

 
  if @InboundShipmentId is not null
    insert @TableHeader
          (InboundShipmentId,
           InboundDocumentId,
           ReceiptId,
           OrderNumber,
           ExternalCompanyId,
           DeliveryNoteNumber,
           DeliveryDate,
           ReceivingCompleteDate,
           Delivery,
           NumberOfLines,
           StatusId,
           InboundDocumentTypeId,
           PlannedDeliveryDate,
           CreateDate
           )
    select isr.InboundShipmentId,
           id.InboundDocumentId,
           r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId,
           r.DeliveryNoteNumber,
           r.DeliveryDate,
           r.ReceivingCompleteDate,
           r.Delivery,
           r.NumberOfLines,
           r.StatusId,
           id.InboundDocumentTypeId,
           id.DeliveryDate,
           id.CreateDate
      from InboundShipmentReceipt isr
      join Receipt                  r on isr.ReceiptId = r.ReceiptId
      join InboundDocument         id on r.InboundDocumentId = id.InboundDocumentId
      join ExternalCompany		   ec (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId 
      join Status					s (nolock) on r.StatusId = s.StatusId
     where isr.InboundShipmentId = isnull(@InboundShipmentId, isr.InboundShipmentId)
       and id.WarehouseId = @WarehouseId
	   and id.InboundDocumentTypeId = isnull(@InboundDocumentTypeId,id.InboundDocumentTypeId)
	   and id.OrderNumber = isnull(@OrderNumber,id.OrderNumber)
	   and r.DeliveryNoteNumber = isnull(@DeliveryNoteNumber,r.DeliveryNoteNumber)
	   and ec.ExternalCompanyCode = isnull(@SupplierCode,ec.ExternalCompanyCode)
	   and ec.ExternalCompany like '%' + @SupplierName + '%'
	   and s.Status = isnull(@Status,s.Status)
  else 
    insert @TableHeader
          (InboundDocumentId,
           ReceiptId,
           OrderNumber,
           ExternalCompanyId,
           DeliveryNoteNumber,
           DeliveryDate,
           ReceivingCompleteDate,
           Delivery,
           NumberOfLines,
           StatusId,
           InboundDocumentTypeId)
    select id.InboundDocumentId,
           r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId,
           r.DeliveryNoteNumber,
           r.DeliveryDate,
           r.ReceivingCompleteDate,
           r.Delivery,
           r.NumberOfLines,
           r.StatusId,
           id.InboundDocumentTypeId
      from Receipt                  r
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
      join ExternalCompany		   ec (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId 
      join Status					s (nolock) on r.StatusId = s.StatusId
      where id.WarehouseId = @WarehouseId
		and id.InboundDocumentTypeId = isnull(@InboundDocumentTypeId,id.InboundDocumentTypeId)
		and id.OrderNumber = isnull(@OrderNumber,id.OrderNumber)
		and r.DeliveryNoteNumber = isnull(@DeliveryNoteNumber,r.DeliveryNoteNumber)
		and ec.ExternalCompanyCode = isnull(@SupplierCode,ec.ExternalCompanyCode)
		and ec.ExternalCompany like '%' + @SupplierName + '%'
		and s.Status = isnull(@Status,s.Status)
	
   if @DateUsed = 'Create Date'	
	   update th
		 set SearchDate     = th.CreateDate
		from @TableHeader th
    
  if @DateUsed = 'Planned Delivery Date'	
	   update th
		 set SearchDate     = th.PlannedDeliveryDate
		from @TableHeader th
		
  if @DateUsed = 'Delivery Date'	
	   update th
		 set SearchDate     = th.DeliveryDate
		from @TableHeader th
		
  if @DateUsed = 'Received Date'	
	   update th
		 set SearchDate     = th.ReceivingCompleteDate
		from @TableHeader th

  delete from 	@TableHeader
	where SearchDate not between @FromDate and @Todate	
	
  insert @TableDetails
        (ReceiptId,
         ReceiptLineId,
         StorageUnitBatchId,
         ExpectedQuantity,
         AcceptedQuantity,
         OperatorCode,
         ConfirmedQuantity,
         StatusId)
  select rl.ReceiptId,
         rl.ReceiptLineId,
         rl.StorageUnitBatchId,
         rl.RequiredQuantity,
         rl.AcceptedQuantity,
         o.OperatorCode,
         rl.AcceptedQuantity,
         rl.StatusId
    from @TableHeader th
    join ReceiptLine  rl on th.ReceiptId     = rl.ReceiptId
    join InboundLine  il on rl.InboundLineId = il.InboundLineId
    left join Operator     o  on rl.OperatorId    = o.OperatorId
  
  update th
     set Supplier     = ec.ExternalCompany,
         SupplierCode = ec.ExternalCompanyCode
    from @TableHeader th
    join ExternalCompany ec on th.ExternalCompanyId = ec.ExternalCompanyId
    
  update th
     set InboundDocumentType = idt.InboundDocumentType
    from @TableHeader th
    join InboundDocumentType idt on th.InboundDocumentTypeId = idt.InboundDocumentTypeId
    
  update th
     set Status = s.Status
    from @TableHeader th
    join Status s on th.StatusId = s.StatusId 
    
  update t
     set Operator     = o.Operator
    from @TableDetails t
    join Operator        o (nolock) on t.OperatorCode      = o.OperatorCode
    
  update t
     set ReceiveCompleteDate     = (select max(rlh.InsertDate) 
									  from ReceiptLineHistory rlh (nolock)
									 where t.ReceiptLineId      = rlh.ReceiptLineId
									   and rlh.StatusId = dbo.ufn_StatusId('R','RC'))
    from @TableDetails t
    
  update t
     set ReceivedDate     = (select max(rlh.InsertDate) 
									  from ReceiptLineHistory rlh (nolock)
									 where t.ReceiptLineId      = rlh.ReceiptLineId
									   and rlh.StatusId = dbo.ufn_StatusId('R','R'))
    from @TableDetails t
   
  
  update td
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         StorageUnitId = su.StorageUnitId
    from @TableDetails    td
    join StorageUnitBatch sub on td.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  on sub.StorageUnitId     = su.StorageUnitId
    join Product          p   on su.ProductId          = p.ProductId
    
  
   update tr
     set PalletId = i.PalletId,
		 InstructionTypeId = i.InstructionTypeId,
		 CreateDate = i.CreateDate,
		 PickLocationId = i.PickLocationId,
		 StoreLocationId = i.StoreLocationId,
		 JobId = i.JobId
    from @TableDetails tr
    join Instruction i on tr.ReceiptLineId = i.ReceiptLineId

   update t
     set PutawayMarshalling     = (select i.CreateDate
									 from Instruction i (nolock) 
								    where t.JobId = i.JobId
								      and t.StorageUnitBatchId = i.StorageUnitBatchId)
    from @TableDetails t
    
   update t
     set PutawayBulk	        = (select  i.CreateDate
									 from Job j (nolock) 
									 join Instruction i (nolock) on j.JobId = i.jobid
								    where  j.ReferenceNumber  like '%' + convert(varchar,t.JobId) + '%'
								      and t.StorageUnitBatchId = i.StorageUnitBatchId)
    from @TableDetails t
  
     
   update t
     set PickLocation     = l.Location
    from @TableDetails t
    join Location l (nolock) on t.PickLocationId = l.LocationId
    
  update t
     set StoreLocation     = l.Location
    from @TableDetails t
    join Location l (nolock) on t.StoreLocationId = l.LocationId
    
  update t
     set InstructionType     = it.InstructionType
    from @TableDetails t
    join InstructionType it (nolock) on t.InstructionTypeId = it.InstructionTypeId
  
  update t
     set Status = s.Status
    from @TableDetails t
    join Status s on t.StatusId = s.StatusId 
          
  select	InboundShipmentId		as HeaderInboundShipmentId,
		   InboundDocumentId		as HeaderInboundDocumentId,
		   th.ReceiptId				as HeaderReceiptId,
		   OrderNumber				as HeaderOrderNumber,
		   ExternalCompanyId		as HeaderExternalCompanyId,
		   Supplier					as HeaderSupplier,
		   SupplierCode				as HeaderSupplierCode,
		   Delivery					as HeaderDelivery,
		   NumberOfLines			as HeaderNumberOfLines,
		   DeliveryNoteNumber		as HeaderDeliveryNoteNumber,
		   DeliveryDate				as HeaderDeliveryDate,   
		   th.StatusId				as HeaderStatusId,
		   th.Status				as HeaderStatus,
		   InboundDocumentTypeId	as HeaderInboundDocumentTypeId,
		   InboundDocumentType		as HeaderInboundDocumentType,		
		   ReceivingCompleteDate	as HeaderReceivingCompleteDate,
		   td.ReceiptId			,
		   td.ReceiptLineId		,
		   td.StorageUnitBatchId	,
		   td.StorageUnitId		,
		   td.ProductCode		,
		   td.Product		,
		   td.ConfirmedQuantity	,
		   td.CreateDate			,
		   td.ReceivedDate			,
		   td.ReceiveCompleteDate	,
		   td.PutawayMarshalling	,
		   td.PutawayBulk			,
		   td.StatusId				,
		   td.Status				,
		   td.JobId				,  
		   td.InstructionTypeId	, 
		   td.InstructionType		,
		   td.PickLocationId		,
		   td.PickLocation			,
		   td.StoreLocationId		,
		   td.StoreLocation	,
		   td.ExpectedQuantity		,
		   td.AcceptedQuantity	,
		   td.OperatorCode		,
		   td.Operator		,
		   td.PalletId				
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
    --group by th.InboundShipmentId,
    --     th.OrderNumber,
    --     DeliveryNoteNumber,
    --     DeliveryDate,
    --     th.Supplier,
    --     th.SupplierCode,
    --     td.ProductCode,
    --     td.Product,
    --     Product,
    --     td.OperatorCode,
    --     td.Operator,
    --     th.ReceivingCompleteDate,
    --     td.PalletId

end
