﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VisualData_Insert
  ///   Filename       : p_VisualData_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:36:06
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the VisualData table.
  /// </remarks>
  /// <param>
  ///   @Key1 varchar(50) = null,
  ///   @Sort1 varchar(50) = null,
  ///   @Sort2 varchar(50) = null,
  ///   @Sort3 varchar(50) = null,
  ///   @Colour int = null,
  ///   @Latitude decimal(16,13) = null,
  ///   @Longitude decimal(16,13) = null,
  ///   @Info varchar(1000) = null 
  /// </param>
  /// <returns>
  ///   VisualData.Key1,
  ///   VisualData.Sort1,
  ///   VisualData.Sort2,
  ///   VisualData.Sort3,
  ///   VisualData.Colour,
  ///   VisualData.Latitude,
  ///   VisualData.Longitude,
  ///   VisualData.Info 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VisualData_Insert
(
 @Key1 varchar(50) = null,
 @Sort1 varchar(50) = null,
 @Sort2 varchar(50) = null,
 @Sort3 varchar(50) = null,
 @Colour int = null,
 @Latitude decimal(16,13) = null,
 @Longitude decimal(16,13) = null,
 @Info varchar(1000) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert VisualData
        (Key1,
         Sort1,
         Sort2,
         Sort3,
         Colour,
         Latitude,
         Longitude,
         Info)
  select @Key1,
         @Sort1,
         @Sort2,
         @Sort3,
         @Colour,
         @Latitude,
         @Longitude,
         @Info 
  
  select @Error = @@Error
  
  
  return @Error
  
end
