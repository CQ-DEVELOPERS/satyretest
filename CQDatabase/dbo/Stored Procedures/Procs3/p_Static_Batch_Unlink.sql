﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Batch_Unlink
  ///   Filename       : p_Static_Batch_Unlink.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Batch_Unlink
(
 @WarehouseId        int,
 @StorageUnitBatchId int,
 @OperatorId         int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_StorageUnitBatch_Delete
   @StorageUnitBatchId = @StorageUnitBatchId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Static_Batch_Unlink'); 
    rollback transaction
    return @Error
end
