﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Stretch_Wrap_Job_Select_test
  ///   Filename       : p_Stretch_Wrap_Job_Select_test.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Stretch_Wrap_Job_Select_test
(
 @barcode nvarchar(30),
 @WarehouseId int = null
)

as
begin
	 set nocount on;
	 
	 declare @JobId               int,
          @PalletId            int,
          @InstructionRefId    int,
          @OutboundDocumentId  int,
          @OneProductPerPallet bit
  
  if isnumeric(replace(@barcode,'J:','')) = 1
    select @JobId   = replace(@barcode,'J:',''),
           @barcode = null
           

  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @PalletId is not null
    select @JobId = j.JobId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Job              j (nolock) on i.JobId             = j.JobId
      join Status           s (nolock) on j.StatusId          = s.StatusId
     where i.PalletId              = @PalletId
       and s.StatusCode           in ('CK','CD','D','QA','DC','C')
       and it.InstructionTypeCode in ('P','PM','PS','FM')
  
  if @barcode is not null
  begin
    select @JobId = JobId
      from Job (nolock)
     where ReferenceNumber = @barcode
  end
  
  select @InstructionRefId = isnull(InstructionRefId, InstructionId)
    from Instruction (nolock)
   where JobId = @JobId
  
  select @OutboundDocumentId = OutboundDocumentId
    from IssueLineInstruction (nolock)
   where InstructionId = @InstructionRefId
  
  select @OneProductPerPallet = ec.OneProductPerPallet
    from OutboundDocument od (nolock)
    join ExternalCompany  ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
   where od.OutboundDocumentId = @OutboundDocumentId
  print '@OneProductPerPallet'
  print @OneProductPerPallet
  select top 1
         i.InstructionId,
         i.JobId,
         s.Status,
         s.StatusCode,
         i.PalletId,
         i.CreateDate,
         @OneProductPerPallet as 'OneProductPerPallet'
    from Instruction         i   (nolock)
    join Job                 j   (nolock) on i.JobId             = j.JobId
    join InstructionType     it  (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status              s   (nolock) on j.StatusId          = s.StatusId
   where s.Type                  = 'IS'
     and s.StatusCode           in ('CK','CD','D','QA','DC','C')
     and i.JobId                 = @JobId
     and it.InstructionTypeCode in ('P','PM','PS','FM','PR')
end
