﻿
/*
 /// <summary>
 ///   Procedure Name : p_Wave_Planning_Move_Items_Putaway
 ///   Filename       : p_Wave_Planning_Move_Items_Putaway.sql
 ///   Create By      : Grant Schultz
 ///   Date Created   : 15 Mar 2010
 /// </summary>
 /// <remarks>
 ///   
 /// </remarks>
 /// <param>
 ///   
 /// </param>
 /// <returns>
 ///   
 /// </returns>
 /// <newpara>
 ///   Modified by    : 
 ///   Modified Date  : 
 ///   Details        : 
 /// </newpara>
*/
CREATE procedure p_Wave_Planning_Move_Items_Putaway
(
 @WaveId     int = null,
 @PickJobId  int = null,
 @LocationId int = null,
 @Debug      bit = 0
)

as
begin
  
	 set nocount on;
  
  declare @TableProducts as table
  (
   LocationId         int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   Quantity           numeric(13,6)
  )
  
  declare @TableLocations as table
  (
   LocationId         int
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @Transaction       bit = 1,
          @StorageUnitBatchId int,
          @Quantity          numeric(13,6),
          @JobId             int,
          @InstructionId     int,
          @InstructionTypeId int,
          @StatusId          int,
          @PriorityId        int,
          @OperatorId        int,
          @PickLocationId    int,
          @OldPickLocationId int,
          @StoreLocationId   int,
          @WarehouseId       int,
          @PalletId          int,
          @rowcount          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  if @WaveId is not null
  begin
    return 0
    select @WarehouseId = WarehouseId
      from Wave (nolock)
     where WaveId = @WaveId
  end
  else if @PickJobId is not null
  begin
    return 0
    select @WarehouseId = WarehouseId
      from Job (nolock)
     where JobId = @PickJobId
  end
  else if @LocationId is not null
  begin
    select @WarehouseId = a.WarehouseId 
      from AreaLocation al (nolock)
      join Area a (nolock) on al.AreaId = a.AreaId
     where al.LocationId = @LocationId
  end
  
  if @LocationId is not null
  begin
    insert @TableLocations
          (LocationId)
    select @LocationId
  end
  else
  begin
    insert @TableLocations
          (LocationId)
    select distinct PickLocationId
      from Issue                  i (nolock)
      join IssueLineInstruction ili (nolock) on i.IssueId = ili.IssueId
      join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
      join AreaLocation          al (nolock) on ins.PickLocationId = al.LocationId
      join Area                   a (nolocK) on al.AreaId = a.AreaId
     where (i.WaveId = @WaveId
       or   ins.JobId = @PickJobId)
       and ins.PickLocationId is not null
       and a.AreaType = ''
       and a.Area = 'WavePick'
       and (a.Area != 'Xdock')
  end
  
  insert @TableProducts
        (LocationId,
         StorageUnitBatchId,
         StorageUnitId,
         Quantity)
  select subl.LocationId,
         subl.StorageUnitBatchId,
         sub.StorageUnitId,
         subl.ActualQuantity
    from @TableLocations            tl
    join StorageUnitBatchLocation subl (nolock) on tl.LocationId = subl.LocationId
    join StorageUnitBatch sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
   where subl.ActualQuantity > 0
  
  declare @TableInstructions as table
  (
   StorageUnitId int
  )
  
  insert @TableInstructions
        (StorageUnitId)
  select distinct sub.StorageUnitId
    from Instruction        i (nolock)
    join AreaLocation      al (nolock) on i.PickLocationId = al.LocationId
    join Area               a (nolock) on al.AreaId = a.AreaId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Status             s (nolock) on i.StatusId = s.StatusId
                                     and s.StatusCode in ('W','S')
    join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                                      and it.InstructionTypeCode in ('P','FM','PS','PM')
   where sub.StorageUnitId in (select StorageUnitId from @TableProducts)
     and a.Area = 'WavePick'
  
  -- Don't return any locations which are reserving stock anywhere else in WavePick.
  delete @TableProducts
   where LocationId in (select LocationId 
                          from @TableProducts     tp
                          join @TableInstructions ti on tp.StorageUnitId = ti.StorageUnitId)
  
  select @rowcount = @@rowcount
  
  if @Debug = 1
    select @rowcount as 'Has picks'
  
  ---- Don't return any locations which are reserving stock anywhere else in WavePick.
  --delete @TableProducts
  -- where LocationId in (select LocationId 
  --                        from @TableProducts     tp
  --                        join viewAvailableArea vaa on tp.StorageUnitId = vaa.StorageUnitId
  --                       where vaa.Area = 'WavePick'
  --                         and vaa.ReservedQuantity > 0)
  
  -- Don't return any locations which are reserving stock.
  delete tp
    from @TableProducts tp
   where tp.LocationId in (select tp.LocationId
                             from @TableProducts tp
                             join StorageUnitBatchLocation subl (nolock) on tp.LocationId = subl.LocationId
                            where subl.ReservedQuantity > 0
                              and subl.ActualQuantity > 0) -- GS 2013-09-30 need to move location back
  
  select @rowcount = @@rowcount
  
  if @Debug = 1
    select @rowcount as 'Reserved'
  
  if @Debug = 1
    select * from @TableProducts
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @InstructionTypeId = InstructionTypeId,
         @PriorityId        = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = 'M'
  
  while exists(select top 1 1
                 from @TableProducts)
  begin
    select @PickLocationId     = LocationId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @Quantity           = Quantity
      from @TableProducts
    order by LocationId
    
    if @PickLocationId != isnull(@OldPickLocationId,-1)
    begin
      set @StatusId = dbo.ufn_StatusId('J','RL')
      
      exec @Error = p_Job_Insert
       @JobId       = @JobId output,
       @PriorityId  = @PriorityId,
       @OperatorId  = @OperatorId,
       @StatusId    = @StatusId,
       @WarehouseId = @WarehouseId
      
      if @Error <> 0 or @JobId is null
      begin
        set @InstructionId = -1
        goto error
      end
      
      set @PalletId = null
      
      select @PalletId = PalletId
        from Pallet (nolock)
       where LocationId = @PickLocationId
         and StorageUnitBatchId = @StorageUnitBatchId
      
      if @PalletId is null
      begin
        select top 1 @PalletId = PalletId
          from Instruction (nolock)
         where StoreLocationId = @PickLocationId
           and StorageUnitBatchId = @StorageUnitBatchId
        order by EndDate desc
      end
      
      if @PalletId is null
      begin
        select @PalletId = max(PalletId)
          from Pallet (nolock)
         where LocationId = @PickLocationId
      end
      
      --if @PalletId is null
      --begin
      --  exec @Error = p_Pallet_Insert
      --   @PalletId = @PalletId output,
      --   @Prints   = 0
        
      --  if @Error <> 0
      --    goto Error
      --end
    end
    
    select @OldPickLocationId = @PickLocationId
    
    delete @TableProducts
     where LocationId         = @PickLocationId
       and StorageUnitBatchId = @StorageUnitBatchId
       and Quantity           = @Quantity
    
    set @StatusId = dbo.ufn_StatusId('I','W')
    
    exec @Error = p_Instruction_Insert
     @InstructionId       = @InstructionId output,
     @InstructionTypeId   = @InstructionTypeId,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @WarehouseId         = @WarehouseId,
     @StatusId            = @StatusId,
     @JobId               = @JobId,
     @OperatorId          = @OperatorId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = @StoreLocationId,
     @InstructionRefId    = null,
     @Quantity            = @Quantity,
     @ConfirmedQuantity   = @Quantity,
     @CreateDate          = @GetDate,
     @PalletId            = @PalletId,
     @AutoComplete        = 1
    
    if @Error <> 0 or @InstructionId is null
      goto error
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @InstructionId
    
    if @Error <> 0
      goto error
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
 
