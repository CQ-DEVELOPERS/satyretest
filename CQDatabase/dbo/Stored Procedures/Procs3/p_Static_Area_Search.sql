﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Area_Search
  ///   Filename       : p_Static_Area_Search.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 9 September 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Area_Search
(
    @WarehouseId int
)

as
begin
	 set nocount on;
	 
	 SELECT AreaId,
         WarehouseId,
         Area,
         AreaCode,
         StockOnHand,
         AreaSequence,
         AreaType,
         WarehouseCode,
         Replenish,
         MinimumShelfLife
    from Area
   where WarehouseId = @WarehouseId 
end
