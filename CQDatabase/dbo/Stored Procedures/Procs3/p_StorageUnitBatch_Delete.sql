﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatch_Delete
  ///   Filename       : p_StorageUnitBatch_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:01
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the StorageUnitBatch table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatch_Delete
(
 @StorageUnitBatchId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete StorageUnitBatch
     where StorageUnitBatchId = @StorageUnitBatchId
  
  select @Error = @@Error
  
  
  return @Error
  
end
