﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SerialNumber_Delete
  ///   Filename       : p_SerialNumber_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Aug 2013 14:12:40
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the SerialNumber table.
  /// </remarks>
  /// <param>
  ///   @SerialNumberId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SerialNumber_Delete
(
 @SerialNumberId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete SerialNumber
     where SerialNumberId = @SerialNumberId
  
  select @Error = @@Error
  
  
  return @Error
  
end
