﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_VisualData_Insert_Bulk
  ///   Filename       : p_VisualData_Insert_Bulk.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_VisualData_Insert_Bulk

as
begin
	 set nocount on;
	 
	 declare @Location         nvarchar(15),
          @Latitude         decimal(16,13),
          @Longitude        decimal(16,13),
          @LatitudeAdd      decimal(16,13),
          @LongitudeAdd     decimal(16,13),
          @CurrentLatitude  decimal(16,13),
          @CurrentLongitude decimal(16,13),
          @Direction        nvarchar(10),
          @Height           int,
          @Length           int,
          @HeightCount      int,
          @LengthCount      int,
          @PreviousLocation nvarchar(15),
          @Rowcount         int,
          @Ident            int,
          @Quantity         float,
          @String           nvarchar(255)
  
  declare @TableResult as table
  (
   Ident              int primary key identity,
   LocationId         int,
   Location           nvarchar(15),
   StorageUnitBatchId int,
   StorageUnitId int,
   Latitude           decimal(16,13),
   Longitude          decimal(16,13),
   Height             int,
   [Length]           int,
   Updated            bit default 0,
   Row                int,
   Ailse              nvarchar(10),
   [Column]           nvarchar(10),
   [Level]            nvarchar(10),
   Direction          nvarchar(10),
   Colour             int,
   Quantity           float,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Batch              nvarchar(50)
  )
  
  -- Get the rest of the details
  insert @TableResult
        (LocationId,
         StorageUnitBatchId,
         StorageUnitId,
         Location,
         Latitude,
         Longitude,
         Direction,
         Height,
         [Length],
         Ailse,
         [Column],
         [Level],
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity)
  select distinct
         vl.LocationId,
         vs.StorageUnitBatchId,
         vs.StorageUnitId,
         vl.Location,
         vl.Latitude,
         vl.Longitude,
         vl.Direction,
         vl.Height,
         vl.[Length],
         vl.Ailse,
         vl.[Column],
         vl.[Level],
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         vs.Batch,
         subl.ActualQuantity
    from viewLocation vl
    join StorageUnitBatchLocation subl on vl.LocationId = subl.LocationId
    join viewStock vs on subl.StorageUnitBatchId = vs.StorageUnitBatchId
   where vl.AreaCode in ('BK')
     and vl.Latitude is not null
  
  update tr
      set Quantity = tr.Quantity / p.Quantity
     from @TableResult tr
     join Pack          p on tr.StorageUnitId = p.StorageUnitId
     join PackType     pt on p.PackTypeId     = pt.PackTypeId
    where pt.PackType = 'Pallets'
  
  update @TableResult
     set Colour = 11
   where [Column] in ('01','03','05','07','09','11','13','15','17','19')
  
  update @TableResult
     set Colour = 55
   where [Column] in ('02','04','06','08','10','12','14','16','18','20')
  
  --< Insert row per pallet quantity
  declare duplicate_cursor cursor for
   select Ident,
          Quantity
  	  from	@TableResult tr
	  order by	Location
  
  open duplicate_cursor
  
  fetch duplicate_cursor into @Ident,
                              @Quantity
  
  while (@@fetch_status = 0)
  begin
    if @Quantity > 0
    begin
      set rowcount @Quantity
      
      insert @TableResult
            (LocationId,
             Location,
             StorageUnitBatchId,
             StorageUnitId,
             Latitude,
             Longitude,
             Height,
             [Length],
             Updated,
             Row,
             Ailse,
             [Column],
             [Level],
             Direction,
             Colour,
             Quantity,
             ProductCode,
             Product,
             SKUCode,
             Batch)
      select tr.LocationId,
             tr.Location,
             tr.StorageUnitBatchId,
             tr.StorageUnitId,
             tr.Latitude,
             tr.Longitude,
             tr.Height,
             tr.[Length],
             tr.Updated,
             tr.Row,
             tr.Ailse,
             tr.[Column],
             tr.[Level],
             tr.Direction,
             tr.Colour,
             tr.Quantity,
             tr.ProductCode,
             tr.Product,
             tr.SKUCode,
             tr.Batch
        from @TableResult tr,
             Location      l
       where Ident = @Ident
      --select @@rowcount as '@@rowcount' , @Quantity as '@Quantity'
    end
    
    set rowcount 0
    
    fetch duplicate_cursor into @Ident,
                                @Quantity
  end
  
  close duplicate_cursor
  deallocate duplicate_cursor
  --> Insert row per pallet quantity
  
  select * from @TableResult
--  select substring(Location, 1,3), count(1)
--    from @TableResult
--  group by substring(Location, 1,3)
  
  declare visual_cursor cursor for
   select Ident,
          Location,
          Latitude,
          Longitude,
          Direction,
          Height,
          [Length]
  	  from	@TableResult tr
	  order by	Location
  
  open visual_cursor
  
  set @HeightCount = 1
  set @LengthCount = 1
  set @PreviousLocation = '-1'
  
  fetch visual_cursor into @Ident,
                           @Location,
                           @Latitude,
                           @Longitude,
                           @Direction,
                           @Height,
                           @Length
  
  while (@@fetch_status = 0)
  begin
    if @Location != isnull(@PreviousLocation, @Location)
    begin
      set @PreviousLocation = @Location
      set @CurrentLatitude  = @Latitude
      set @CurrentLongitude = @Longitude
      set @Rowcount = 0
      set @HeightCount = 1
      set @LengthCount = 1
    end
    
    if @Direction = 'North'
    begin
      set @LatitudeAdd = 0.00003
      set @LongitudeAdd = 0
    end
    if @Direction = 'East'
    begin
      set @LatitudeAdd = 0
      set @LongitudeAdd = 0.00003
    end
    if @Direction = 'South'
    begin
      set @LatitudeAdd = -0.00003
      set @LongitudeAdd = 0
    end
    if @Direction = 'West'
    begin
      set @LatitudeAdd = 0
      set @LongitudeAdd = -0.00003
    end
    
    update @TableResult
       set Latitude  = @CurrentLatitude,
           Longitude = @CurrentLongitude,
           Updated   = 1,
           Row       = isnull(@Rowcount, 0),
           [Length]  = @LengthCount,
           Height    = @HeightCount,
           Location  = @Location
     where Ident = @Ident
    
    if @HeightCount < @Height
    begin
      set @HeightCount = @HeightCount + 1
    end
    else if @LengthCount < @Length
    begin
      set @HeightCount = 1
      set @LengthCount = @LengthCount + 1
      select @CurrentLatitude  = @CurrentLatitude + @LatitudeAdd
      select @CurrentLongitude = @CurrentLongitude + @LongitudeAdd
    end
    else
    begin
      set @Rowcount = isnull(@Rowcount,0) + 1
      
      select @CurrentLatitude  = @Latitude
      select @CurrentLongitude = @Longitude + 0.00003 * @Rowcount
      set @HeightCount = 1
      set @LengthCount = 1
    end
    
    fetch visual_cursor into @Ident,
                             @Location,
                             @Latitude,
                             @Longitude,
                             @Direction,
                             @Height,
                             @Length
  end

  close visual_cursor
  deallocate visual_cursor
  
  delete VisualData where Key1 = 'Bulk'
  
  insert VisualData
        (Key1,
         Sort1,
         Sort2,
         Sort3,
         Colour,
         Latitude,
         Longitude,
         Info)
  select 'Bulk',
         Ailse,
         Location + ' ' + convert(nvarchar(10), IDent),
         [Level],
         Colour,
         Latitude,
         Longitude,
         isnull(
         '_______________________' + 
         '<h3>' + convert(nvarchar(10), ProductCode) + '</h3>' + 
         '<b>Product Code: </b>' + convert(nvarchar(10), Product) + 
         '<br><b>Sku Code: </b>' + convert(nvarchar(10), SKUCode) + 
         '<br><b>Batch: </b>' + convert(nvarchar(10), Batch),'Empty')
    from @TableResult
  where Row is not null
end
