﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Warehouse_Id
  ///   Filename       : p_Warehouse_Id.sql
  ///   Create By      : Karen
  ///   Date Created   : 07 April 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Warehouse_Id

as
begin
	 set nocount on;
  create table #Warehouse
(
 WarehouseId		int              not null,
 Warehouse			varchar(50)      not null,

)
	Insert into #Warehouse(WarehouseId,Warehouse)
    Select 0,'{All}'

	Insert into #Warehouse(WarehouseId,Warehouse)
    select WarehouseId,
           Warehouse
      from Warehouse (nolock)
	
--     where Type = 'B'
    order by Warehouse
	
	Select * from #Warehouse  
end
