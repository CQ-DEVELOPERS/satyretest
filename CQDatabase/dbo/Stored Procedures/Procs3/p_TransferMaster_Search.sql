﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_TransferMaster_Search
  ///   Filename       : p_TransferMaster_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:36:04
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the TransferMaster table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   TransferMaster.FromWarehouseCode,
  ///   TransferMaster.ProductCode,
  ///   TransferMaster.ToWarehouseCode,
  ///   TransferMaster.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_TransferMaster_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         TransferMaster.FromWarehouseCode
        ,TransferMaster.ProductCode
        ,TransferMaster.ToWarehouseCode
        ,TransferMaster.Quantity
    from TransferMaster
  
end
