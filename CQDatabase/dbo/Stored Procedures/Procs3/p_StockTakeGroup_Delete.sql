﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeGroup_Delete
  ///   Filename       : p_StockTakeGroup_Delete.sql
  ///   Create By      : Karen
  ///   Date Created   : September 2011
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the StockTakeGroup table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_StockTakeGroup_Delete
(
 --@StockTakeGroupCode	varchar(20) = null,
 @StockTakeGroupId		int
)
as
begin
	 set nocount on
	 declare @Error int

	  delete StockTakeGroupLocation
     where  StockTakeGroupId = @StockTakeGroupId
 -- DELETE		stgl
	--FROM		StockTakeGroupLocation stgl
	--INNER JOIN	StockTakeGroup AS stg ON stgl.StockTakeGroupId = stg.StockTakeGroupId
	--WHERE		stg.StockTakeGroupId = @StockTakeGroupId
 
  delete StockTakeGroup
     where StockTakeGroupId = @StockTakeGroupId
  
  
  select @Error = @@Error
   
end
