﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_UOM_Select
  ///   Filename       : p_UOM_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:01
  /// </summary>
  /// <remarks>
  ///   Selects rows from the UOM table.
  /// </remarks>
  /// <param>
  ///   @UOMId int = null 
  /// </param>
  /// <returns>
  ///   UOM.UOMId,
  ///   UOM.UOM,
  ///   UOM.UOMCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_UOM_Select
(
 @UOMId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         UOM.UOMId
        ,UOM.UOM
        ,UOM.UOMCode
    from UOM
   where isnull(UOM.UOMId,'0')  = isnull(@UOMId, isnull(UOM.UOMId,'0'))
  
end
