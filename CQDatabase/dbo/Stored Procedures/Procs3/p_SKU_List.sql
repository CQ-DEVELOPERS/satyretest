﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_SKU_List
  ///   Filename       : p_SKU_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the SKU table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   SKU.SKUId,
  ///   SKU.SKU 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_SKU_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as SKUId
        ,'{All}' as SKU
        ,'{All}' as SKUCode
  union
  select
         SKU.SKUId
        ,SKU.SKU
        ,SKU.SKUCode
    from SKU
  
end
 
