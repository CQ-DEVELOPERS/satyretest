﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_TransferMaster_Update
  ///   Filename       : p_TransferMaster_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:36:04
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the TransferMaster table.
  /// </remarks>
  /// <param>
  ///   @FromWarehouseCode nvarchar(100) = null,
  ///   @ProductCode nvarchar(100) = null,
  ///   @ToWarehouseCode nvarchar(100) = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_TransferMaster_Update
(
 @FromWarehouseCode nvarchar(100) = null,
 @ProductCode nvarchar(100) = null,
 @ToWarehouseCode nvarchar(100) = null,
 @Quantity float = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update TransferMaster
     set FromWarehouseCode = isnull(@FromWarehouseCode, FromWarehouseCode),
         ProductCode = isnull(@ProductCode, ProductCode),
         ToWarehouseCode = isnull(@ToWarehouseCode, ToWarehouseCode),
         Quantity = isnull(@Quantity, Quantity) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
