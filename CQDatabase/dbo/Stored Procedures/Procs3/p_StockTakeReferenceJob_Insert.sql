﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeReferenceJob_Insert
  ///   Filename       : p_StockTakeReferenceJob_Insert.sql
  ///   Create By      : Karen
  ///   Date Created   : Aug 2011
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StockTakeReference table.
  /// </remarks>
  /// <param>

  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeReferenceJob_Insert
(	@StockTakeReferenceId		int = null,
    @JobId						int = null
    )

    

as
begin
	 set nocount on;
  
  declare @Error int
  
  if @StockTakeReferenceId is not null and @JobId is not null
  insert	StockTakeReferenceJob 
		   (StockTakeReferenceId,
			JobId,
			StatusCode
			)
  select	@StockTakeReferenceId,
			@JobId,
			'Active'
 where not exists(select 1 from StockTakeReferenceJob strj 
		where strj.StockTakeReferenceId = @StockTakeReferenceId
		and strj.JobId = @JobId)		  
  select @Error = @@Error, @StockTakeReferenceId = scope_identity()
  
 
end
