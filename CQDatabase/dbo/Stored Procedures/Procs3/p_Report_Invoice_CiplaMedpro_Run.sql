﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Invoice_CiplaMedpro_Run
  ///   Filename       : p_Report_Invoice_CiplaMedpro_Run.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Nov 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Invoice_CiplaMedpro_Run
(
 @WarehouseCode nvarchar(50) = 'RW'
)

as
begin
  if @WarehouseCode = 'RW'
  -- Durbanvillie Invoices
  if exists(select top 1 i.AutoIndex
              from invNum             i (nolock)
              join _btblInvoiceLines il (nolock) on i.AutoIndex = il.iInvoiceID
              join WhseMst            w (nolock) on il.iWarehouseID = w.WhseLink
             where i.DocType = 4
               and i.DocState = 4
               and i.invNumber != ''
               and isnull(i.ubIDInvcqprinted,-1) = 0
               and w.Code in ('RW','RW-I','TW','XBOND','XPOR','XRES','Z-ENT','ZCL','ZCR','ZDQ','ZDR','ZF','ZMGQ','ZMGR','ZMHQ','ZPotR','ZQR','ZQR-I','ZQT','ZS','ZW','ZWBQ','ZWBR','ZWs4','ZWs5','ZX','ZZAC'))
  begin
    print 'RW'
    exec ReportServer.dbo.AddEvent @EventType='TimedSubscription', @EventData='3b563cb4-5534-4313-87d7-6e914c6bcb07'
    return -- don't execute next one yet
  end
  
  if @WarehouseCode = 'RW-A'
  -- Atlas Gardens Invoices
  if exists(select top 1 i.AutoIndex
			           from invNum             i (nolock)
			           join _btblInvoiceLines il (nolock) on i.AutoIndex = il.iInvoiceID
			           join WhseMst            w (nolock) on il.iWarehouseID = w.WhseLink
			          where i.DocType = 4
			            and i.DocState = 4
			            and i.invNumber != ''
			            and isnull(i.ubIDInvcqprinted,-1) = 0
			            and w.Code in ('RW-A','TW-A','TW-I','XBONa','XSCS','ZCR-A','ZENTa','ZF-A','ZQR-A','ZS-A','ZW-A','ZMMR'))
  begin
    print 'RW-A'
    exec ReportServer.dbo.AddEvent @EventType='TimedSubscription', @EventData='f120dad1-1fdd-4783-b1d0-46551e3da770'
    return -- don't execute next one yet
  end
end
