﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatchLocation_Delete
  ///   Filename       : p_StorageUnitBatchLocation_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:03
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the StorageUnitBatchLocation table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitBatchId int = null,
  ///   @LocationId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatchLocation_Delete
(
 @StorageUnitBatchId int = null,
 @LocationId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete StorageUnitBatchLocation
     where StorageUnitBatchId = @StorageUnitBatchId
       and LocationId = @LocationId
  
  select @Error = @@Error
  
  
  return @Error
  
end
