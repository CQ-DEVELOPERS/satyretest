﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeGroupLocation_Search
  ///   Filename       : p_StockTakeGroupLocation_Search.sql
  ///   Create By      : Karen
  ///   Date Created   : September 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeGroupLocation_Search
(	@StockTakeGroupId		int = null   )


as
begin
	 set nocount on;
  
  select StockTakeGroupLocId,
		 stgl.StockTakeGroupId,
		 stgl.LocationId,
		 stg.StockTakeGroup,
		 l.Location
    from StockTakeGroupLocation stgl
    join StockTakeGroup stg (nolock) on stg.StockTakeGroupId = stgl.StockTakeGroupId
    join Location l (nolock) on l.LocationId = stgl.LocationId
    where stgl.StockTakeGroupId = @StockTakeGroupId
  order by StockTakeGroup
end
