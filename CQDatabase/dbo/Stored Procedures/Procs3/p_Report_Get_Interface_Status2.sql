﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Get_Interface_Status2
  ///   Filename       : p_Report_Get_Interface_Status2.sql
  ///   Created By     : Karen
  ///   Date Created   : 06 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Report_Get_Interface_Status2]
(
 @InterfaceTableId int,
 @DocumentType     varchar(50),
 @RecordStatus     varchar(50),
 @MessageStatus    varchar(50),
 @StartDate        datetime,
 @EndDate          datetime,
 @InterfaceId      int = null,
 @Reprocess        varchar(10) = null,
 @Key              int
)
as

DECLARE @DetailName1a VARCHAR(50)

begin
  insert InterfaceConsoleAudit
        (ProcName,
         InterfaceTableId,
         DocumentType,
         RecordStatus,
         MessageStatus,
         StartDate,
         EndDate,
         InterfaceId,
         Reprocess,
         [Key])
  select object_name(@@procid),
         @InterfaceTableId,
         @DocumentType,
         @RecordStatus,
         @MessageStatus,
         @StartDate,
         @EndDate,
         @InterfaceId,
         @Reprocess,
         @Key
  
  If (@Reprocess = 'Y' and @InterfaceId is not null)
	  exec p_Interface_Reprocess @InterfaceTableId ,@InterfaceId 

  Create table #InterfaceStatus1 (InterfaceTableId int,
                        		  InterfaceId int,
                        		  InterfaceTable varchar(max),
                        		  Description1 varchar(max),
                        		  Description2 varchar(max),
                        		  DocumentType varchar(max),
                        		  RecordStatus varchar(max),
                        		  MessageStatus varchar(max),
                        		  DetailField1 varchar(max),
                        		  DetailField2 varchar(max),
                        		  DetailField3 Varchar(max),
                        		  DetailField4 Varchar(max),
                        		  DetailField5 Varchar(max),
                          		  DetailField6 Varchar(max),
                        		  DetailField7 Varchar(max),
                        		  DetailField8 Varchar(max),
                        		  InterfaceMessageCode varchar(max),
                        		  InterfaceMessage varchar(max),
                        		  Createdate Datetime,
                        		  DetailName1 varchar(max),
                        		  DetailName2 varchar(max),
                        		  DetailName3 Varchar(max),
                        		  DetailName4 Varchar(max),
                        		  DetailName5 Varchar(max),
                          		  DetailName6 Varchar(max),
                        		  DetailName7 Varchar(max),
                        		  DetailName8 Varchar(max))
                        		  
	  
	  
  Declare @SqlCommand varchar(max)

  Declare InterfaceTablesCursor cursor for 
  Select InterfaceTableId,
		  DetailTable,
		  HeaderTableKey,
		  Description1,
		  Description2,
		  StartId,
		  isnull(DetailField1,'None') as DetailField1,
		  isnull(DetailField2,'None') as DetailField2,
		  isnull(DetailField3,'None') as DetailField3,
		  isnull(DetailField4,'None') as DetailField4,
		  isnull(DetailField5,'None') as DetailField5,		  		  		  
		  isnull(DetailField6,'None') as DetailField6,
		  isnull(DetailField7,'None') as DetailField7,
		  isnull(DetailField8,'None') as DetailField8		  		  		  		  		  
  from InterfaceTables (nolock)
	  where InterfaceTableId = @InterfaceTableId
  	
  Open InterfaceTablesCursor

  Declare @DetailTable Varchar(max),
		  @HeaderTableKey Varchar(max),
		  @Description1 Varchar(max),
		  @Description2 Varchar(max),
		  @StartId int,
		  @DetailField1 varchar(max),
		  @DetailField2 varchar(max),
		  @DetailField3 varchar(max),
		  @DetailField4 varchar(max),
		  @DetailField5 varchar(max),
		  @DetailField6 varchar(max),
		  @DetailField7 varchar(max),
		  @DetailField8 varchar(max)

  Fetch next from InterfaceTablesCursor into @InterfaceTableId,@DetailTable ,@HeaderTableKey ,@Description1 ,	@Description2 ,@StartId, @DetailField1 ,@DetailField2,@DetailField3, @DetailField4, @DetailField5, @DetailField6, @DetailField7, @DetailField8

  While @@FETCH_STATUS = 0
  Begin 
		  Set @SqlCommand = 'Insert into #InterfaceStatus1 (InterfaceTableId,InterfaceId,InterfaceTable,Description1,Description2,'
		  Set @SqlCommand = @SqlCommand +  'DetailField1,DetailField2,DetailField3,DetailField4,DetailField5,DetailField6,DetailField7,DetailField8) Select '
		  Set @SqlCommand = @SqlCommand +  CONVERT(char(20),@InterfaceTableId) + ' as InterfaceTableId,'
		  Set @SqlCommand = @SqlCommand + ' h.' + @HeaderTableKey + ','
		  Set @SqlCommand = @SqlCommand + '''' + @DetailTable + '''' + ','
		  Set @SqlCommand = @SqlCommand + '''' + @Description1 + '''' + ','
		  Set @SqlCommand = @SqlCommand + '''' + @Description2 + '''' + ','
		  If @DetailField1 = 'None' 
              Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as DetailField1,'
		  Else
              Set @SqlCommand = @SqlCommand + ' h.' + @DetailField1 + ','
		  If @DetailField2 = 'None' 
              Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as DetailField2,'
		  Else
              Set @SqlCommand = @SqlCommand + ' h.' + @DetailField2 + ','
		  If @DetailField3 = 'None' 
              Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as DetailField3,'
		  Else
              Set @SqlCommand = @SqlCommand + ' h.' + @DetailField3 + ','
		  If @DetailField4 = 'None' 
              Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as DetailField4,'
		  Else
              Set @SqlCommand = @SqlCommand + ' h.' + @DetailField4 + ','
		  If @DetailField5 = 'None' 
              Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as DetailField5,'
		  Else
              Set @SqlCommand = @SqlCommand + ' h.' + @DetailField5 + ','
		  If @DetailField6 = 'None' 
              Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as DetailField6,'
		  Else
              Set @SqlCommand = @SqlCommand + ' h.' + @DetailField6 + ','	
		  If @DetailField7 = 'None' 
              Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as DetailField7,'
		  Else
              Set @SqlCommand = @SqlCommand + ' h.' + @DetailField7 + ','
		  If @DetailField8 = 'None' 
              Set @SqlCommand = @SqlCommand + '''' + 'N/A' + '''' + 'as DetailField8'
		  Else
              Set @SqlCommand = @SqlCommand + ' h.' + @DetailField8 + ','
                            		                              	 
		  Set @SqlCommand = @SqlCommand + ' From ' + @DetailTable + ' h (nolock) '
		  set @SqlCommand = @SqlCommand + 'Join InterfaceTables t (nolock)  on t.DetailTable = '''
		  set @SqlCommand = @SqlCommand + @DetailTable + ''''
		  set @SqlCommand = @SqlCommand + ' where ' + @HeaderTableKey + ' = ' + CONVERT(char(20), @Key)

  		--Insert into #InterfaceStatus1 (InterfaceTableId,InterfaceId,InterfaceTable,Description1,Description2,DetailField1,DetailField2,DetailField3,DetailField4,DetailField5,DetailField6,DetailField7,DetailField8) Select 1                    as InterfaceTableId, h.InterfaceExportSOHeaderId,'InterfaceExportSODetail','To Host','Picking', h.LineNumber, h.ProductCode, h.Product, h.SKUCode, h.Batch, h.Quantity, h.Weight,'N/A'as DetailField8 From InterfaceExportSODetail h Join InterfaceTables t on t.DetailTable = 'InterfaceExportSODetail' where InterfaceExportSOHeaderId = 0                   
		  
		  Print @SqlCommand
		  Exec(@SqlCommand)
		  Fetch next from InterfaceTablesCursor into @InterfaceTableId,@DetailTable ,@HeaderTableKey ,@Description1 ,	@Description2 ,@StartId, @DetailField1 ,@DetailField2,@DetailField3,@DetailField4,@DetailField5,@DetailField6,@DetailField7,@DetailField8
  End
  Close InterfaceTablesCursor
  Deallocate InterfaceTablesCursor
  
  update #InterfaceStatus1
     set MessageStatus = ''
   where RecordStatus = 'Reprocessed'
  
  Select	iis.InterfaceTableId,
            iis.InterfaceId,
            iis.Description1,
            iis.Description2,
            iis.InterfaceTable,
            iis.DetailField1,iis.DetailField2,iis.DetailField3,iis.DetailField4,iis.DetailField5,iis.DetailField6,iis.DetailField7,iis.DetailField8,
            isnull(hn.DetailField1,'None') as DetailName1,
            isnull(hn.DetailField2,'None') as DetailName2,
            isnull(hn.DetailField3,'None') as DetailName3,
            isnull(hn.DetailField4,'None') as DetailName4,
            isnull(hn.DetailField5,'None') as DetailName5,
            isnull(hn.DetailField6,'None') as DetailName6,
            isnull(hn.DetailField7,'None') as DetailName7,
            isnull(hn.DetailField8,'None') as DetailName8
		from #InterfaceStatus1 iis
	left join InterfaceTables hn on hn.InterfaceTableId = @InterfaceTableId
end
