﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Receiving_Summary_Report
  ///   Filename       : p_Report_Receiving_Summary_Report.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jul 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Receiving_Summary_Report
(
@OrderNumber       nvarchar(30),
@StorageUnitId	   int,
@Batch             nvarchar(50),
@FromDate          datetime,
@ToDate            datetime,
@Received		   nvarchar(1),
@PrincipalId	   int,
@OperatorId		   int
)

as
begin
  set nocount on;
  
  declare @TableSOH as table
  (
   WarehouseId int,
   StorageUnitBatchId  int,
   StockOnHand int
  )
  
  declare @ExternalCompanyId int,
          @PrincipalId2      int
          
  print @Received
  
  insert @TableSOH
        (WarehouseId,
         StorageUnitBatchId,
         StockOnHand)
  select a.WarehouseId,
         subl.StorageUnitBatchId,
         sum(subl.ActualQuantity)
    from StorageUnitBatchLocation subl (nolock)
    join AreaLocation               al (nolock) on subl.LocationId        = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
  group by a.WarehouseId,
           subl.StorageUnitBatchId

  
  declare @TableDetails as Table
  (
  OrderNumber          nvarchar(30),
  OrderDate            datetime,
  ExpectedDeliveryDate datetime,
  ProductCode          nvarchar(30),
  ProductDesc          nvarchar(255),
  StorageUnitId		   int,
  SKUCode              nvarchar(50),
  SKU                  nvarchar(50),
  Batch                nvarchar(50),
  OrderQty             numeric(13,6),
  ReceivedQty          numeric(13,6),
  OutstandingBal       numeric(13,6),
  StockOnHand          numeric(13,6),
  DeliveryNote		   nvarchar(30),
  ExternalCompanyId	   int,
  ExternalCompanyCode  nvarchar(30),
  ExternalCompany	   nvarchar(255),
  ReceivedDate		   datetime,
  ReceiptId			   int,
  GRN	  			   nvarchar(30),
  PrincipalId		   int,
  PrincipalCode		   nvarchar(30),
  Length			   int,
  Width				   int,
  Height			   int,
  UnitWeight		   int,
  GrossWeight		   int,
  Weight			   nvarchar(50),
  BOE				   nvarchar(255),
  ContainerNumber	   nvarchar(50),
  Reference1		   nvarchar(50),
  Reference2		   nvarchar(50),
  BOELineNumber		   nvarchar(50),
  ExpiryDate		   datetime
  )
  
  if @StorageUnitId = '-1'
     set @StorageUnitId = null
         
  if @OrderNumber = '-1'
     set @OrderNumber = null
         
  if @Batch = '-1'
     set @Batch = null
     
  if @PrincipalId = -1 
	 set @PrincipalId = null
	 
  if @OperatorId = -1
    set @OperatorId = null
    
  select @ExternalCompanyId = ExternalCompanyId,
         @PrincipalId2       = PrincipalId
    from Operator o (nolock)
   where o.OperatorId = @OperatorId
   
   if @PrincipalId2 is not null
	set @PrincipalId = @PrincipalId2
     
  if @Received = 'A'       
	  insert @TableDetails
			(OrderNumber,
			 OrderDate,
			 ExpectedDeliveryDate,
			 ProductCode,
			 ProductDesc,
			 StorageUnitId,
			 SKUCode,
			 SKU,
			 Batch,
			 OrderQty,
			 ReceivedQty,
			 OutstandingBal,
			 StockOnHand,
			 ExternalCompanyId,
			 DeliveryNote,
			 ReceivedDate,
			 ReceiptId,
			 GRN,
			 PrincipalId,
			 BOE,
			 ContainerNumber,
			 Reference1,
			 Reference2,
			 BOELineNumber,
			 ExpiryDate	
			 )
	  select id.OrderNumber,
			 id.CreateDate,
			 id.DeliveryDate,
			 p.ProductCode,
			 p.Product,
			 su.StorageUnitId,
			 sku.SKUCode,
			 sku.SKU,
			 b.Batch,
			 sum(rl.RequiredQuantity),
			 sum(rl.ReceivedQuantity),
			 sum(rl.RequiredQuantity - isnull(rl.ReceivedQuantity,0)),
			 sum(StockOnHand) ,
			 id.ExternalCompanyId,
			 r.DeliveryNoteNumber ,
			 isnull(r.ReceivingCompleteDate,r.DeliveryDate),
			 r.ReceiptId,
			 r.GRN,
			 p.PrincipalId,
			 r.BOE,
			 r.ContainerNumber,
			 r.AdditionalText1,
			 r.AdditionalText2,
			 rl.BOELineNumber,
			 b.ExpiryDate     
		from InboundDocument     id
		 join InboundLine        il on id.InboundDocumentId = il.InboundDocumentId
		 join ReceiptLine        rl on il.InboundLineId = rl.InboundLineId
		 join Receipt			  r on rl.ReceiptId = r.ReceiptId
		 join StorageUnitBatch  sub on rl.StorageUnitBatchId = sub.StorageUnitBatchId
		 join Batch               b on sub.BatchId = b.BatchId
		 join StorageUnit        su on sub.StorageUnitId = su.StorageUnitId
		 join Product             p on su.ProductId = p.ProductId
		 join SKU               sku on su.SKUId = sku.SKUId
		 left
		 join @TableSOH         soh on rl.StorageUnitBatchId = soh.StorageUnitBatchId
								   and id.WarehouseId = soh.WarehouseId
	   where id.CreateDate between @FromDate and @ToDate
	     --and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
	     and (id.PrincipalId = @PrincipalId or @PrincipalId is null)
	     and id.ExternalCompanyId = isnull(@ExternalCompanyId, id.ExternalCompanyId)
	  group by id.OrderNumber,
			   id.CreateDate,
			   id.DeliveryDate,
			   p.ProductCode,
			   p.Product,
			   su.StorageUnitId,
			   sku.SKUCode,
			   sku.SKU,
			   b.Batch,
			   id.ExternalCompanyId,
			   r.DeliveryNoteNumber,
			   r.ReceivingCompleteDate,
			   r.DeliveryDate,
			   r.ReceiptId ,
			   r.GRN,
			   p.PrincipalId,
			   r.BOE,
			 r.ContainerNumber,
			 r.AdditionalText1,
			 r.AdditionalText2,
			 rl.BOELineNumber,
			 b.ExpiryDate
	if @Received = 'R'       
	  insert @TableDetails
			(OrderNumber,
			 OrderDate,
			 ExpectedDeliveryDate,
			 ProductCode,
			 ProductDesc,
			 StorageUnitId,
			 SKUCode,
			 SKU,
			 Batch,
			 OrderQty,
			 ReceivedQty,
			 OutstandingBal,
			 StockOnHand,
			 ExternalCompanyId,
			 DeliveryNote,
			 ReceivedDate,
			 ReceiptId,
			 GRN,
			 PrincipalId,
			 BOE,
			 ContainerNumber,
			 Reference1,
			 Reference2,
			 BOELineNumber,
			 ExpiryDate	
			 )
	  select id.OrderNumber,
			 id.CreateDate,
			 id.DeliveryDate,
			 p.ProductCode,
			 p.Product,
			 su.StorageUnitId,
			 sku.SKUCode,
			 sku.SKU,
			 b.Batch,
			 sum(rl.RequiredQuantity),
			 sum(rl.ReceivedQuantity),
			 sum(rl.RequiredQuantity - isnull(rl.ReceivedQuantity,0)),
			 sum(StockOnHand) ,
			 id.ExternalCompanyId,
			 r.DeliveryNoteNumber ,
			 isnull(r.ReceivingCompleteDate,r.DeliveryDate),
			 r.ReceiptId,
			 r.GRN ,
			 p.PrincipalId,
			 r.BOE,
			 r.ContainerNumber,
			 r.AdditionalText1,
			 r.AdditionalText2,
			 rl.BOELineNumber,
			 b.ExpiryDate          
		from InboundDocument     id
		 join InboundLine        il on id.InboundDocumentId = il.InboundDocumentId
		 join ReceiptLine        rl on il.InboundLineId = rl.InboundLineId
		 join Receipt			  r on rl.ReceiptId = r.ReceiptId
		 join Status			  s on s.StatusId = r.StatusId
		 join StorageUnitBatch  sub on rl.StorageUnitBatchId = sub.StorageUnitBatchId
		 join Batch               b on sub.BatchId = b.BatchId
		 join StorageUnit        su on sub.StorageUnitId = su.StorageUnitId
		 join Product             p on su.ProductId = p.ProductId
		 join SKU               sku on su.SKUId = sku.SKUId
		 left
		 join @TableSOH         soh on rl.StorageUnitBatchId = soh.StorageUnitBatchId
								   and id.WarehouseId = soh.WarehouseId
	   where id.CreateDate between @FromDate and @ToDate
	     --and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
	     and (id.PrincipalId = @PrincipalId or @PrincipalId is null)
	     and id.ExternalCompanyId = isnull(@ExternalCompanyId, id.ExternalCompanyId)
	     and s.Status = 'Receiving Complete'
	  group by id.OrderNumber,
			   id.CreateDate,
			   id.DeliveryDate,
			   p.ProductCode,
			   p.Product,
			   su.StorageUnitId,
			   sku.SKUCode,
			   sku.SKU,
			   b.Batch,
			   id.ExternalCompanyId,
			   r.DeliveryNoteNumber,
			   r.ReceivingCompleteDate,
			   r.DeliveryDate,
			   r.ReceiptId,
			   r.GRN,
			   p.PrincipalId,
			   r.BOE,
			 r.ContainerNumber,
			 r.AdditionalText1,
			 r.AdditionalText2,
			 rl.BOELineNumber,
			 b.ExpiryDate 
			   
	if @Received = 'N'       
	  insert @TableDetails
			(OrderNumber,
			 OrderDate,
			 ExpectedDeliveryDate,
			 ProductCode,
			 ProductDesc,
			 StorageUnitId,
			 SKUCode,
			 SKU,
			 Batch,
			 OrderQty,
			 ReceivedQty,
			 OutstandingBal,
			 StockOnHand,
			 ExternalCompanyId,
			 DeliveryNote,
			 ReceivedDate,
			 ReceiptId,
			 GRN,
			 PrincipalId,
			 BOE,
			 ContainerNumber,
			 Reference1,
			 Reference2,
			 BOELineNumber,
			 ExpiryDate	
			 )
	  select id.OrderNumber,
			 id.CreateDate,
			 id.DeliveryDate,
			 p.ProductCode,
			 p.Product,
			 su.StorageUnitId,
			 sku.SKUCode,
			 sku.SKU,
			 b.Batch,
			 sum(rl.RequiredQuantity),
			 sum(rl.ReceivedQuantity),
			 sum(rl.RequiredQuantity - isnull(rl.ReceivedQuantity,0)),
			 sum(StockOnHand) ,
			 id.ExternalCompanyId,
			 r.DeliveryNoteNumber ,
			 isnull(r.ReceivingCompleteDate,r.DeliveryDate),
			 r.ReceiptId,
			 r.GRN,
			 p.PrincipalId,
			 r.BOE,
			 r.ContainerNumber,
			 r.AdditionalText1,
			 r.AdditionalText2,
			 rl.BOELineNumber,
			 b.ExpiryDate         
		from InboundDocument     id
		 join InboundLine        il on id.InboundDocumentId = il.InboundDocumentId
		 join ReceiptLine        rl on il.InboundLineId = rl.InboundLineId
		 join Receipt			  r on rl.ReceiptId = r.ReceiptId
		 join Status			  s on s.StatusId = r.StatusId
		 join StorageUnitBatch  sub on rl.StorageUnitBatchId = sub.StorageUnitBatchId
		 join Batch               b on sub.BatchId = b.BatchId
		 join StorageUnit        su on sub.StorageUnitId = su.StorageUnitId
		 join Product             p on su.ProductId = p.ProductId
		 join SKU               sku on su.SKUId = sku.SKUId
		 left
		 join @TableSOH         soh on rl.StorageUnitBatchId = soh.StorageUnitBatchId
								   and id.WarehouseId = soh.WarehouseId
	   where id.CreateDate between @FromDate and @ToDate
	     and s.Status != 'Receiving Complete'
	     --and isnull(p.PrincipalId, -1) = isnull(@PrincipalId, isnull(p.PrincipalId, -1))
	     and (id.PrincipalId = @PrincipalId or @PrincipalId is null)
	     and id.ExternalCompanyId = isnull(@ExternalCompanyId, id.ExternalCompanyId)
	  group by id.OrderNumber,
			   id.CreateDate,
			   id.DeliveryDate,
			   p.ProductCode,
			   p.Product,
			   su.StorageUnitId,
			   sku.SKUCode,
			   sku.SKU,
			   b.Batch,
			   id.ExternalCompanyId,
			   r.DeliveryNoteNumber,
			   r.ReceivingCompleteDate,
			   r.DeliveryDate,
			   r.ReceiptId,
			   r.GRN ,
			   p.PrincipalId,
			   r.BOE,
			 r.ContainerNumber,
			 r.AdditionalText1,
			 r.AdditionalText2,
			 rl.BOELineNumber,
			 b.ExpiryDate
  
   update  td
      set td.ExternalCompanyCode = ec.ExternalCompanyCode,
		  td.ExternalCompany = ec.ExternalCompany
     from @TableDetails td
     join ExternalCompany ec  on ec.ExternalCompanyId = td.ExternalCompanyId
     
     
   update  td
      set td.ReceivedDate = rh.InsertDate
     from @TableDetails td
     join ReceiptHistory rh  on td.ReceiptId = rh.ReceiptId
     join Status s on rh.StatusId = s.statusid
     where Status = 'Received'
     and ReceivedDate is null
   
  update  td
      set td.PrincipalCode = p.PrincipalCode
     from @TableDetails td
     join Principal p  on td.PrincipalId = p.PrincipalId 
     
  update  td
      set td.Height = p.Height,
          td.Width = p.Width,
          td.Length = p.Length,
          td.UnitWeight = p.Weight,
          td.GrossWeight = isnull(p.Weight,0) * isnull(td.ReceivedQty,0) 
     from @TableDetails td
     join pack p  on td.storageunitid = p.storageunitid 
     where p.packtypeid = 8   
     
  update  td
      set td.Weight = convert(nvarchar(20),td.UnitWeight) + '/' + convert(nvarchar(20),td.GrossWeight)
     from @TableDetails td
        
  select OrderNumber,
         DeliveryNote,
         OrderDate,
         ExpectedDeliveryDate,
         ProductCode,
         ProductDesc,
         SKUCode,
         SKU,
         Batch,
         OrderQty,
         ReceivedQty,
         OutstandingBal,
         StockOnHand,
         ExternalCompanyCode,
         ExternalCompany,
         ReceivedDate,
         GRN,
         PrincipalCode,
         Height,
         Width,
         Length,
         Weight,
         UnitWeight,
         GrossWeight,
		 BOE,
		 ContainerNumber,
		 Reference1,
		 Reference2,
		 BOELineNumber,
		 ExpiryDate	
    from @TableDetails
   where OrderNumber = isnull(@OrderNumber, OrderNumber)
	 and StorageUnitId = ISNULL(@StorageUnitId, StorageUnitId)
     --and ProductCode = isnull(@ProductCode, ProductCode)
     and Batch = isnull(@Batch, Batch)
  order by ProductCode
end
 
 
 
 
