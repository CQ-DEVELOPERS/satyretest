﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ShipmentIssue_Insert
  ///   Filename       : p_ShipmentIssue_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:51
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ShipmentIssue table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null output,
  ///   @IssueId int = null output,
  ///   @DropSequence smallint = null 
  /// </param>
  /// <returns>
  ///   ShipmentIssue.ShipmentId,
  ///   ShipmentIssue.IssueId,
  ///   ShipmentIssue.DropSequence 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ShipmentIssue_Insert
(
 @ShipmentId int = null output,
 @IssueId int = null output,
 @DropSequence smallint = null 
)

as
begin
	 set nocount on;
  
  if @ShipmentId = '-1'
    set @ShipmentId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
	 declare @Error int
 
  insert ShipmentIssue
        (ShipmentId,
         IssueId,
         DropSequence)
  select @ShipmentId,
         @IssueId,
         @DropSequence 
  
  select @Error = @@Error, @IssueId = scope_identity()
  
  
  return @Error
  
end
