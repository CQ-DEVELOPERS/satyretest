﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Warehouse_Get_Active
  ///   Filename       : p_Warehouse_Get_Active.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Jun 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Warehouse_Get_Active

as
begin
	 set nocount on;
  
  select Warehouse.WarehouseId,
         Warehouse.Warehouse 
    from Warehouse
   where WarehouseId = ParentWarehouseId
   --where WarehouseId = isnull(ParentWarehouseId, WarehouseId)
end
