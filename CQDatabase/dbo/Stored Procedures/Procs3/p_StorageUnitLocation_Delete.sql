﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitLocation_Delete
  ///   Filename       : p_StorageUnitLocation_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:08
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the StorageUnitLocation table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @LocationId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitLocation_Delete
(
 @StorageUnitId int = null,
 @LocationId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete StorageUnitLocation
     where StorageUnitId = @StorageUnitId
       and LocationId = @LocationId
  
  select @Error = @@Error
  
  
  return @Error
  
end
