﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Picking_Performance_test
  ///   Filename       : p_Report_Picking_Performance_test.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 May 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Picking_Performance_test
(
 @WarehouseId      int,
 @FromDate         datetime,
 @ToDate           datetime
)

as
begin
 set nocount on;
  
  declare @TableResult as table
  (
   OutboundDocumentTypeId int,
   OutboundDocumentType   nvarchar(30),
   OutboundDocumentId     int,
   IssueId                int,
   IssueLineId            int,
   JobId                  int,
   InstructionId          int,
   StorageUnitBatchId     int,
   StorageUnitId          int,
   ProductCode            nvarchar(30),
   Product                nvarchar(100),
   SKUCode                nvarchar(50),
   Quantity               float,
   ConfirmedQuantity      float,
   Weight                 numeric(19,2),
   ConfirmedWeight        numeric(19,2),
   Volume                 numeric(19,2),
   ConfirmedVolume        numeric(19,2),
   CreateDate             datetime,
   Release                datetime,
   StartDate              datetime,
   EndDate                datetime,
   Checking               datetime,
   Despatch               datetime,
   DespatchChecked        datetime,
   Complete               datetime,
   DeliveryDate           datetime,
   StatusCode             nvarchar(10)
  )
  
  declare @TableSummary as table
  (
   OutboundDocumentTypeId int,
   OutboundDocumentType   nvarchar(30),
   OutboundDocumentId     int,
   IssueId                int,
   IssueLineId            int,
   JobId                  int,
   InstructionId          int,
   StorageUnitBatchId     int,
   StorageUnitId          int,
   ProductCode            nvarchar(30),
   Product                nvarchar(50),
   SKUCode                nvarchar(50),
   Quantity               float,
   ConfirmedQuantity      float,
   Weight                 numeric(19,2),
   ConfirmedWeight        numeric(19,2),
   Volume                 numeric(19,2),
   ConfirmedVolume        numeric(19,2),
   CreateDate             datetime,
   Release                datetime,
   StartDate              datetime,
   EndDate                datetime,
   Checking               datetime,
   Despatch               datetime,
   DespatchChecked        datetime,
   Complete               datetime,
   DeliveryDate           datetime,
   StatusCode             nvarchar(10)
  )
  
  declare @Config int
  
  select @Config = convert(int, Value)
    from Configuration (nolock)
   where WarehouseId = @WarehouseId
     and ConfigurationId = 191
  
  if @Config is null
    set @Config = 7
  
  update op
     set CreateDate = i.CreateDate
    from OutboundPerformance op
    join Instruction          i on op.JobId = i.JobId
   where op.CreateDate is null
  
  update op
     set CreateDate = i.CreateDate
    from OutboundPerformance op
    join Instruction          i on op.JobId = i.JobId
   where op.CreateDate is null
  
  insert @TableResult
        (OutboundDocumentTypeId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         JobId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         CreateDate,
         Release,
         StartDate,
         EndDate,
         Checking,
         Despatch,
         DespatchChecked,
         Complete)
  select ili.OutboundDocumentTypeId,
         ili.OutboundDocumentId,
         ili.IssueId,
         ili.IssueLineId,
         op.JobId,
         i.InstructionId,
         i.StorageUnitBatchId,
         ili.Quantity,
         ili.ConfirmedQuantity,
         op.CreateDate,
         isnull(op.Release,'2069-12-31'),
         isnull(op.StartDate,'2069-12-31'),
         isnull(op.EndDate,'2069-12-31'),
         isnull(op.Checking,'2069-12-31'),
         isnull(op.Despatch,'2069-12-31'),
         isnull(op.DespatchChecked,'2069-12-31'),
         isnull(op.Complete,'2069-12-31')
    from OutboundPerformance   op (nolock)
    join Instruction            i (nolock) on op.JobId = i.JobId
    join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
   where (op.CreateDate between @FromDate and @ToDate
      or  op.Complete        >= @FromDate -- between @FromDate and @ToDate
      or  op.Complete        is null)
      --and op.CreateDate <= @ToDate -- GS 2011-02-09 Replaced with line below
      and op.CreateDate between dateadd(dd,-@Config, @FromDate) and @ToDate
	  and i.WarehouseId = @WarehouseId
  
  update tr
     set StatusCode        = s.StatusCode
    from @TableResult tr
    join Job           j (nolock) on tr.JobId = j.JobId
    join Status        s (nolock) on j.StatusId = s.StatusId
  
  update @TableResult
     set ConfirmedQuantity = 0
   where StatusCode = 'NS'
  
  update tr
     set CreateDate = od.CreateDate
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set OutboundDocumentType = odt.OutboundDocumentType
    from @TableResult          tr
    join OutboundDocumentType odt (nolock) on tr.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  
  update tr
     set DeliveryDate = i.DeliveryDate
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
  
  update tr
     set StorageUnitId = sub.StorageUnitId
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
  
  update tr
     set ProductCode     = p.ProductCode,
         Product         = p.Product,
         SKUCode         = sku.SKUCode
    from @TableResult      tr
    join StorageUnit       su (nolock) on tr.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  update tr
     set Weight          = pk.Weight * tr.Quantity,
         ConfirmedWeight = pk.Weight * tr.ConfirmedQuantity,
         Volume          = pk.Volume * tr.Quantity,
         ConfirmedVolume = pk.Volume * tr.ConfirmedQuantity
    from @TableResult      tr
    join Pack              pk (nolock) on tr.StorageUnitId = pk.StorageUnitId
    join PackType          pt (nolock) on pk.PackTypeId    = pt.PackTypeId
   where pt.OutboundSequence in (select max(OutboundSequence) from PackType)
     and pk.WarehouseId  = @WarehouseId
  
  update @TableResult
     set ConfirmedQuantity = 0
   where ConfirmedQuantity is null
  
  update @TableResult
     set ConfirmedWeight = 0
   where ConfirmedWeight is null
  
  update @TableResult
     set ConfirmedVolume = 0
   where ConfirmedVolume is null
  
  insert @TableSummary
        (OutboundDocumentType,
         OutboundDocumentTypeId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         DeliveryDate,
         CreateDate,
         Release,
         StartDate,
         EndDate,
         Checking,
         Despatch,
         DespatchChecked,
         Complete,
         Quantity,
         Weight,
         ConfirmedQuantity,
         ConfirmedWeight)
  select OutboundDocumentType,
         OutboundDocumentTypeId,
         OutboundDocumentId,
         IssueId,
         count(distinct(IssueLineId)),
         max(DeliveryDate),
         max(CreateDate),
         max(Release),
         max(StartDate),
         max(EndDate),
         max(Checking),
         max(Despatch),
         max(DespatchChecked),
         max(Complete),
         sum(Quantity),
         sum(Weight),
         sum(ConfirmedQuantity),
         sum(ConfirmedWeight)
    from @TableResult
  group by OutboundDocumentType,
           OutboundDocumentTypeId,
           OutboundDocumentId,
           IssueId
  
  update @TableSummary set Release = null where Release = '2069-12-31'
  update @TableSummary set StartDate = null where StartDate = '2069-12-31'
  update @TableSummary set EndDate = null where EndDate = '2069-12-31'
  update @TableSummary set Checking = null where Checking = '2069-12-31'
  update @TableSummary set Despatch = null where Despatch = '2069-12-31'
  update @TableSummary set DespatchChecked = null where DespatchChecked = '2069-12-31'
  update @TableSummary set Complete = null where Complete = '2069-12-31'
  
  select 'Outbound KPI''s'                   as 'Group',
         '1. Order turnaround less than 3 hours' as 'Header',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         sum(IssueLineId)                    as 'Lines',
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableSummary
   where datediff(hh, CreateDate, Checking) <= 3
     and Checking between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Outbound KPI''s'                   as 'Group',
         '2. Order turnaround less than 24 hours',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         sum(IssueLineId)                    as 'Lines',
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableSummary
   where datediff(hh, CreateDate, Checking) between 4 and 24
     and Checking between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Outbound KPI''s'                   as 'Group',
         '3. Order turnaround less than 48 hours',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         sum(IssueLineId)                    as 'Lines',
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableSummary
   where datediff(hh, CreateDate, Checking) between 25 and 48
     and Checking between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Outbound KPI''s'                   as 'Group',
         '5. Summary of Picking Achieved',
         '1. Less than 3 hours',
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableSummary
   where datediff(hh, CreateDate, Checking) <= 3
     and Checking between @FromDate and @ToDate
  union
  select 'Outbound KPI''s'                   as 'Group',
         '5. Summary of Picking Achieved',
         '2. Less than 24 hours',
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableSummary
   where datediff(hh, CreateDate, Checking) between 4 and 24
     and Checking between @FromDate and @ToDate
  union
  select 'Outbound KPI''s'                   as 'Group',
         '5. Summary of Picking Achieved',
         '3. Less than 48 hours',
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableSummary
   where datediff(hh, CreateDate, Checking) between 25 and 48
     and Checking between @FromDate and @ToDate
  union
  select 'Outbound KPI''s'                   as 'Group',
         '5. Summary of Picking Achieved',
         '4. Greater than 48 hours',
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableSummary
   where datediff(hh, CreateDate, Checking) > 48
     and Checking between @FromDate and @ToDate
  union
  select 'Despatch KPI''s',
         '5. Summary of Picking Achieved',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         sum(IssueLineId)                    as 'Lines',
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableSummary
   where Complete between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Work Load'                         as 'Group',
         '1. Brought Forward'                as 'Header',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         sum(IssueLineId)                    as 'Lines',
         sum(Quantity),
         sum(Weight)
    from @TableSummary
   where isnull(Checking, @FromDate) >= @FromDate -- isnull(Checking, @ToDate) <= @ToDate
     and CreateDate > '2008-05-01'
     and CreateDate < @FromDate
  group by OutboundDocumentType
  union
  select 'Work Load',
         '2. New Orders In',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         sum(IssueLineId)                    as 'Lines',
         sum(Quantity),
         sum(Weight)
    from @TableSummary
   where CreateDate between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Work Load',
         '3. Completed',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(ConfirmedQuantity)              as 'Pieces',
         sum(ConfirmedWeight)                as 'Weight'
    from @TableSummary
   where Checking between @FromDate and @ToDate
  group by OutboundDocumentType
  union
  select 'Work Load',
         '4. Short Picks',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)) as 'Orders',
         count(distinct(IssueLineId))        as 'Lines',
         sum(Quantity - ConfirmedQuantity)   as 'Pieces',
         sum(Weight - ConfirmedWeight)       as 'Weight'
    from @TableResult
   where Checking between @FromDate and @ToDate
     and Quantity > ConfirmedQuantity
  group by OutboundDocumentType
  union
  select 'Work Load',
         '5. Carried Forward',
         OutboundDocumentType,
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(Quantity),
         sum(Weight)
    from @TableSummary tr1
   where isnull(Checking, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and CreateDate <= @ToDate
  group by OutboundDocumentType
  union
  select 'Work Load',
         '6. Summary of picking Outstanding',
         'Due in 3 hrs',
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(Quantity),
         sum(Weight)
    from @TableSummary
   where isnull(Checking, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and CreateDate <= @ToDate
     and datediff(hh, @FromDate, DeliveryDate) between 0 and 3
  union
  select 'Work Load',
         '6. Summary of picking Outstanding',
         'Due in 24 hrs',
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(Quantity),
         sum(Weight)
    from @TableSummary
   where isnull(Checking, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and CreateDate <= @ToDate
     and datediff(hh, @FromDate, DeliveryDate) between 4 and 24
  union
  select 'Work Load',
         '6. Summary of picking Outstanding',
         'Due in 48 hrs',
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(Quantity),
         sum(Weight)
    from @TableSummary
   where isnull(Checking, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and CreateDate <= @ToDate
     and datediff(hh, @FromDate, DeliveryDate) between 25 and 48
  union
  select 'Work Load',
         '6. Summary of picking Outstanding',
         'Due in greater than 48 hrs',
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(Quantity),
         sum(Weight)
    from @TableSummary
   where isnull(Checking, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and CreateDate <= @ToDate
     and datediff(hh, @FromDate, DeliveryDate) > 48
  union
  select 'Work Load',
         '6. Summary of picking Outstanding',
         'Overdue',
         count(distinct(OutboundDocumentId)),
         sum(IssueLineId),
         sum(Quantity),
         sum(Weight)
    from @TableSummary
   where isnull(Checking, @ToDate) >= @ToDate
     and CreateDate > '2008-05-01'
     and CreateDate <= @ToDate
     and datediff(hh, @FromDate, DeliveryDate) < 0
     and isnull(StatusCode,'') != 'NS'
end
