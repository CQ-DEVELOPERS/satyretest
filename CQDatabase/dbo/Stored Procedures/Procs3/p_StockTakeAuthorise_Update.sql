﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockTakeAuthorise_Update
  ///   Filename       : p_StockTakeAuthorise_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:46
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the StockTakeAuthorise table.
  /// </remarks>
  /// <param>
  ///   @InstructionId int = null,
  ///   @StatusCode nvarchar(20) = null,
  ///   @OperatorId int = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockTakeAuthorise_Update
(
 @InstructionId int = null,
 @StatusCode nvarchar(20) = null,
 @OperatorId int = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update StockTakeAuthorise
     set InstructionId = isnull(@InstructionId, InstructionId),
         StatusCode = isnull(@StatusCode, StatusCode),
         OperatorId = isnull(@OperatorId, OperatorId),
         InsertDate = isnull(@InsertDate, InsertDate) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
