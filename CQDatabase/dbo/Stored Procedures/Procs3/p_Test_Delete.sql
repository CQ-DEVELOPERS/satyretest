﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Test_Delete
  ///   Filename       : p_Test_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:01
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Test table.
  /// </remarks>
  /// <param>
  ///   @TestId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Test_Delete
(
 @TestId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Test
     where TestId = @TestId
  
  select @Error = @@Error
  
  
  return @Error
  
end
