﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Shipment_Select
  ///   Filename       : p_Shipment_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:50
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Shipment table.
  /// </remarks>
  /// <param>
  ///   @ShipmentId int = null 
  /// </param>
  /// <returns>
  ///   Shipment.ShipmentId,
  ///   Shipment.StatusId,
  ///   Shipment.WarehouseId,
  ///   Shipment.LocationId,
  ///   Shipment.ShipmentDate,
  ///   Shipment.Remarks,
  ///   Shipment.SealNumber,
  ///   Shipment.VehicleRegistration,
  ///   Shipment.Route 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Shipment_Select
(
 @ShipmentId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Shipment.ShipmentId
        ,Shipment.StatusId
        ,Shipment.WarehouseId
        ,Shipment.LocationId
        ,Shipment.ShipmentDate
        ,Shipment.Remarks
        ,Shipment.SealNumber
        ,Shipment.VehicleRegistration
        ,Shipment.Route
    from Shipment
   where isnull(Shipment.ShipmentId,'0')  = isnull(@ShipmentId, isnull(Shipment.ShipmentId,'0'))
  
end
