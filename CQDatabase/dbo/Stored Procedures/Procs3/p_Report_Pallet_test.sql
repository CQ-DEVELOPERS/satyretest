﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Pallet_test
  ///   Filename       : p_Report_Pallet_test.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jul 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Pallet_test
(
 @PalletId nvarchar(10)
)

as
begin
	 set nocount on;
  
	 declare @TableResult as table
	 (
	  InstructionType    nvarchar(30),
	  Status             nvarchar(50),
	  insStatusCode      nvarchar(10),
	  OperatorId         int,
	  Operator           nvarchar(50),
	  CreateDate         datetime,
	  StartDate          datetime,
	  EndDate            datetime,
	  StorageUnitBatchId int,
	  LocationId		 int,
	  Location			 nvarchar(15),
      PickLocationId     int null,
      PickLocation       nvarchar(15),
      StoreLocationId    int null,
      StoreLocation      nvarchar(15),
	  Quantity           float,
	  ConfirmedQuantity  float,
	  NettWeight		 float,
	  GrossWeight		 float,
	  Pallet1			 int,
	  Pallet2			 int
	 )
  
  insert @TableResult
        (Pallet1,
		 Pallet2,
         InstructionType
	       ,Status
	       ,insStatusCode
	       ,OperatorId
	       ,LocationId
	       ,StorageUnitBatchId
	       ,PickLocationId
	       ,StoreLocationId
	       ,Quantity
	       ,ConfirmedQuantity
	       ,CreateDate
	       ,StartDate
	       ,EndDate
	       ,GrossWeight)
  select i.palletid as 'Pallet instruction',
		 p.palletid as 'Pallet Pallet',
		 it.InstructionType
	       ,s.Status
	       ,si.StatusCode
	       ,i.OperatorId
	       ,p.LocationId
	       ,i.StorageUnitBatchId
	       ,i.PickLocationId
	       ,i.StoreLocationId
	       ,i.Quantity
	       ,i.ConfirmedQuantity
	       ,i.CreateDate
	       ,i.StartDate
	       ,i.EndDate
	       ,j.Weight
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
    join Status           s (nolock) on j.StatusId          = s.StatusId
    join Status          si (nolock) on i.StatusId          = si.StatusId
    join Pallet           p (nolock) on i.StorageUnitBatchId = p.StorageUnitBatchId
									--and p.LocationId in (i.PickLocationId,i.StoreLocationId)
   where p.PalletId = isnull(@PalletId,p.PalletId)
  order by isnull(EndDate, isnull(StartDate, i.CreateDate))
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId

  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
    
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.LocationId = l.LocationId
  
  update @TableResult
     set Status = 'Complete'
   where insStatusCode = 'F'
   
    declare @InboundSequence smallint,
		 @PackTypeId	int,
		 @PackType   	nvarchar(30)
 
  select @InboundSequence = max(InboundSequence)
  from PackType (nolock) 
  
  select @PackTypeId = pt.PackTypeId
  from PackType pt  where @InboundSequence = pt.InboundSequence

      update tr
     set tr.NettWeight = ((select max(pk.NettWeight)
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					     join viewStock    vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
					   where pk.PackTypeId = @PackTypeId
                       and p.ProductCode = vs.ProductCode
                       and isnull(su.ProductCategory,'') != 'V')* tr.ConfirmedQuantity)
    
    from @TableResult tr
  
  select Pallet1 as PalletInstruction,
		 Pallet2 as PalletPallet,
		 tr.InstructionType
	       ,tr.Status
	       ,tr.Operator
	       ,vs.ProductCode
	       ,vs.Product
	       ,vs.SKUCode
	       ,vs.Batch
	       ,Location as LocationPallet
	       ,tr.PickLocation
	       ,tr.StoreLocation
	       ,tr.Quantity
	       ,tr.ConfirmedQuantity
	       ,tr.CreateDate
	       ,tr.StartDate
	       ,tr.EndDate
	       ,tr.NettWeight
	       ,tr.GrossWeight
	    
    from @TableResult tr
    join viewStock    vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
end
