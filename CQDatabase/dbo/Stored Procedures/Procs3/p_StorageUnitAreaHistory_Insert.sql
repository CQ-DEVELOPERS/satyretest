﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitAreaHistory_Insert
  ///   Filename       : p_StorageUnitAreaHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 May 2012 08:41:15
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the StorageUnitAreaHistory table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @AreaId int = null,
  ///   @StoreOrder int = null,
  ///   @PickOrder int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @MinimumQuantity float = null,
  ///   @ReorderQuantity float = null,
  ///   @MaximumQuantity float = null 
  /// </param>
  /// <returns>
  ///   StorageUnitAreaHistory.StorageUnitId,
  ///   StorageUnitAreaHistory.AreaId,
  ///   StorageUnitAreaHistory.StoreOrder,
  ///   StorageUnitAreaHistory.PickOrder,
  ///   StorageUnitAreaHistory.CommandType,
  ///   StorageUnitAreaHistory.InsertDate,
  ///   StorageUnitAreaHistory.MinimumQuantity,
  ///   StorageUnitAreaHistory.ReorderQuantity,
  ///   StorageUnitAreaHistory.MaximumQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitAreaHistory_Insert
(
 @StorageUnitId int = null,
 @AreaId int = null,
 @StoreOrder int = null,
 @PickOrder int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @MinimumQuantity float = null,
 @ReorderQuantity float = null,
 @MaximumQuantity float = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  insert StorageUnitAreaHistory
        (StorageUnitId,
         AreaId,
         StoreOrder,
         PickOrder,
         CommandType,
         InsertDate,
         MinimumQuantity,
         ReorderQuantity,
         MaximumQuantity)
  select @StorageUnitId,
         @AreaId,
         @StoreOrder,
         @PickOrder,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @MinimumQuantity,
         @ReorderQuantity,
         @MaximumQuantity 
  
  select @Error = @@Error
  
  
  return @Error
  
end
