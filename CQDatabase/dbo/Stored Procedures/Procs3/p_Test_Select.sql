﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Test_Select
  ///   Filename       : p_Test_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Test table.
  /// </remarks>
  /// <param>
  ///   @TestId int = null 
  /// </param>
  /// <returns>
  ///   Test.TestId,
  ///   Test.TestCode,
  ///   Test.Test 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Test_Select
(
 @TestId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Test.TestId
        ,Test.TestCode
        ,Test.Test
    from Test
   where isnull(Test.TestId,'0')  = isnull(@TestId, isnull(Test.TestId,'0'))
  order by TestCode
  
end
