﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_GRN_SerialNumbers
  ///   Filename       : p_Report_GRN_SerialNumbers.sql  
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_GRN_SerialNumbers
(
 @InboundShipmentId int = null,
 @ReceiptId         int = null
)

as
begin
	 set nocount on;

 
  select @InboundShipmentId as 'InboundShipmentId',
         @ReceiptId			as 'ReceiptId'
end
 
