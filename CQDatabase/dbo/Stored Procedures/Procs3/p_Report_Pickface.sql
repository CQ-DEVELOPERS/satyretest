﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Pickface
  ///   Filename       : p_Report_Pickface.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Pickface
(
 @ConnectionString nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId      int,
 @LocationId       int = null
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  LocationId       int,
	  Location         nvarchar(15),
	  Area             nvarchar(50),
	  StorageUnitId    int,
	  ProductId        int,
	  Product          nvarchar(50),
	  ProductCode      nvarchar(50),
	  SKUId            int,
	  SKUCode          nvarchar(50),
	  PackType         nvarchar(30),
	  Reserved         bit default 0,
	  MinimumQuantity  float,
   HandlingQuantity float,
   MaximumQuantity  float,
   OnHandQuantity   float
	 )
  
  if @LocationId = -1
    set @LocationId = null
  
--  insert @TableResult
--        (LocationId,
--         Location,
--         Area)
--  select l.LocationId,
--         l.Location,
--         a.Area
--    from Location      l (nolock)
--    join AreaLocation al (nolock) on l.LocationId = al.LocationId
--    join Area          a (nolock) on al.AreaId = a.AreaId
--   where a.WarehouseId = @WarehouseId
--     and l.LocationId = isnull(@LocationId, l.LocationId)
--     and a.AreaCode   = 'PK'
--  
--  update tr
--     set Reserved         = 1,
--         StorageUnitId    = su.StorageUnitId,
--         ProductId        = su.ProductId,
--         SKUId            = su.SKUId,
--         MinimumQuantity  = sul.MinimumQuantity,
--         HandlingQuantity = sul.HandlingQuantity,
--         MaximumQuantity  = sul.MaximumQuantity
--    from @TableResult         tr
--    join StorageUnitLocation sul (nolock) on tr.LocationId = sul.LocationId
--    join StorageUnit          su (nolock) on sul.StorageUnitId = su.StorageUnitId
  
  insert @TableResult
        (LocationId      ,
         Location        ,
         Area            ,
         Reserved        ,
         StorageUnitId   ,
         ProductId       ,
         SKUId           ,
         MinimumQuantity ,
         HandlingQuantity,
         MaximumQuantity ,
         OnHandQuantity  )
  select l.LocationId,
         l.Location,
         a.Area,
         1,
         su.StorageUnitId,
         su.ProductId,
         su.SKUId,
         sul.MinimumQuantity,
         sul.HandlingQuantity,
         sul.MaximumQuantity,
         0
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId = a.AreaId
    join StorageUnitLocation sul (nolock) on al.LocationId = sul.LocationId
    join StorageUnit          su (nolock) on sul.StorageUnitId = su.StorageUnitId
   where a.WarehouseId = @WarehouseId
     and l.LocationId = isnull(@LocationId, l.LocationId)
     and a.AreaCode   = 'PK'
  
  update tr
     set OnHandQuantity = subl.ActualQuantity
    from @TableResult              tr
    join StorageUnitBatch         sub (nolock) on tr.StorageUnitId = sub.StorageUnitId
    join StorageUnitBatchLocation subl (nolock) on tr.LocationId = subl.LocationId
                                              and sub.StorageUnitBatchId = subl.StorageUnitBatchId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product
    from @TableResult tr
    join Product       p (nolock) on tr.ProductId = p.ProductId
  
  update tr
     set SKUCode     = sku.SKUCode
    from @TableResult tr
    join SKU         sku (nolock) on tr.SKUId = sku.SKUId
  
  update tr
     set PackType = pt.PackType
    from @TableResult tr
    join Pack          p (nolock) on tr.StorageUnitId = p.StorageUnitId
    join PackType     pt (nolock) on p.PackTypeId     = pt.PackTypeId
  
  select Area,
         Location,
         Reserved,
         ProductCode,
         Product,
         SKUCode,
         PackType,
	        MinimumQuantity,
         HandlingQuantity,
         MaximumQuantity,
         OnHandQuantity
    from @TableResult tr
  order by Location
end
