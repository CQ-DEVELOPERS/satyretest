﻿

CREATE PROCEDURE p_Report_Despatch_Interface_Comparison
	@FromDate DATETIME
   ,@ToDate DATETIME
   ,@PrincipalId INT = NULL
   ,@WarehouseId INT = NULL
AS

select A.PrincipalCode, A.WaveId, CONVERT(int, LEFT(A.OrderNumber, CASE WHEN CHARINDEX('_',A.OrderNumber, 0) = 0 THEN 0 ELSE CHARINDEX('_',A.OrderNumber, 0)-1 END)) AS DI
,A.OrderNumber, A.OrderStatus,A.JobId, A.ProductCode, A.SKUCode
, A.CheckQuantity, B.CheckQuantity As InterfacedQuantity, A.CompletedDate As PickedDate, B.InsertDate As InterfaceCreated, B.ProcessedDate As InterfaceSent
, B.InterfaceExportShipmentDeliveryId As TransactionNo, C.ResponseDate, C.InterfaceMessage, A.IssueId
from
 (SELECT 	pr.PrincipalCode, iss.WaveId, od.OrderNumber, sis.Status As OrderStatus, iss.IssueId, j.JobId, vs.ProductCode, vs.SKUCode, SUM(i.CheckQuantity) CheckQuantity, MAX(i.EndDate) CompletedDate
	FROM    dbo.Instruction AS i INNER JOIN
			dbo.Status AS si WITH (nolock) ON i.StatusId = si.StatusId INNER JOIN
			dbo.InstructionType AS it WITH (nolock) ON i.InstructionTypeId = it.InstructionTypeId INNER JOIN
			dbo.Job AS j WITH (nolock) ON i.JobId = j.JobId INNER JOIN
			dbo.Status AS s WITH (nolock) ON j.StatusId = s.StatusId LEFT OUTER JOIN
			dbo.viewStock AS vs ON i.StorageUnitBatchId = vs.StorageUnitBatchId LEFT OUTER JOIN
			dbo.IssueLineInstruction AS ili WITH (nolock) ON ISNULL(i.InstructionRefId, i.InstructionId) = ili.InstructionId INNER JOIN
			dbo.IssueLine AS il WITH (nolock) ON ili.IssueLineId = il.IssueLineId INNER JOIN
			dbo.Issue AS iss WITH (nolock) ON il.IssueId = iss.IssueId INNER JOIN
			dbo.Status AS sis WITH(nolock) ON sis.StatusId = iss.StatusId INNER JOIN 
			dbo.OutboundLine AS ol WITH (nolock) ON il.OutboundLineId = ol.OutboundLineId INNER JOIN
			dbo.OutboundDocument AS od WITH (nolock) ON od.OutboundDocumentId = ili.OutboundDocumentId INNER JOIN
			dbo.OutboundDocumentType AS odt WITH (nolock) ON od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId INNER JOIN
			dbo.Principal AS pr WITH (nolock) ON od.PrincipalId = pr.PrincipalId
	WHERE   OutboundDocumentTypeCode = 'DI'
	  AND	ISNULL(CheckQuantity, 0) > 0
	  AND	si.Status = 'Finished'
	  AND	s.Status in ('Checked','Despatch', 'Despatch Checked','Complete')
	  --AND	od.CreateDate > '2013-10-15'
	  AND	i.EndDate BETWEEN @FromDate AND CONVERT(DATE, @ToDate)
	group by pr.PrincipalCode, WaveId, OrderNumber, sis.Status, iss.IssueId, j.JobId, vs.ProductCode, vs.SKUCode) A
left join
(select s.InterfaceExportShipmentId, de.PrincipalCode, de.InterfaceExportShipmentDeliveryId, de.WaybillNo, dl.JobId, ProductCode, SKUCode, SUM(CheckQuantity) CheckQuantity, MAX(InsertDate) InsertDate, MAX(de.ProcessedDate) ProcessedDate
from InterfaceExportShipment s
 inner join InterfaceExportShipmentDelivery de on s.InterfaceExportShipmentId = de.InterfaceExportShipmentId
 inner join InterfaceExportShipmentDetail dl on de.InterfaceExportShipmentDeliveryId = dl.InterfaceExportShipmentDeliveryId
group by s.InterfaceExportShipmentId, de.InterfaceExportShipmentDeliveryId, de.PrincipalCode, WaybillNo, dl.JobId, ProductCode, SKUCode ) B ON A.OrderNumber = B.WaybillNo
																		   AND A.JobId = B.JobId
																		   AND a.ProductCode = B.ProductCode
																		   AND a.SKUCode = B.SkuCode
left join
	(select InterfaceId, REPLACE(REPLACE(InterfaceMessage, '&lt;', '<'), '&gt;','>') InterfaceMessage, OrderNumber, MAX(InsertDate) ResponseDate
	 from InterfaceMessage
	 where InterfaceTable = 'InterfaceExportShipmentDelivery'
	 and InsertDate > '2013-10-15'
	 and InterfaceMessage = 'Success'
	 group by InterfaceId, InterfaceMessage, OrderNumber
	 union
	 select InterfaceId, MAX(InterfaceMessage), OrderNumber, MAX(InsertDate)
	 from InterfaceMessage em
	 where InterfaceTable = 'InterfaceExportShipmentDelivery'
	 and InsertDate > '2013-10-15'
	 and not exists
		 (select 1
		  from InterfaceMessage
		  where InterfaceTable = 'InterfaceExportShipmentDelivery'
		  and InsertDate > '2013-10-15'
		  and InterfaceMessage = 'Success'
		  and InterfaceId = em.InterfaceId
		 )
	 group by InterfaceId, OrderNumber
	) C On B.InterfaceExportShipmentDeliveryId = C.InterfaceId
	   And B.WaybillNo = ISNULL(C.OrderNumber, B.WaybillNo)
--Where 
-- ISNULL(InterfaceMessage,'') <> 'Success'
-- a.CheckQuantity <> ISNULL(b.CheckQuantity, 0)
ORDER BY
	 PrincipalCode
	,WaveId
	,OrderNumber
	,JobId
	,ProductCode
	,SkuCode



