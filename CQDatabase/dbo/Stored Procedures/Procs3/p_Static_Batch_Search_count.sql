﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Batch_Search_count
  ///   Filename       : p_Static_Batch_Search_count.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 3 September 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Batch_Search_count
(
 @StorageUnitId int,
 @Batch         nvarchar(50) = null,
 @ECLNumber     nvarchar(10) = null
)

as
begin
	 set nocount on;
	 
  

select count(sub.StorageUnitBatchId) as rowcnt
    from StorageUnitBatch sub (nolock)
    join Batch            b   (nolock) on sub.BatchId = b.BatchId  
    where sub.StorageUnitId          = isnull(@StorageUnitId, sub.StorageUnitId)
     and isnull(b.Batch,'%')     like '%' + isnull(@Batch, isnull(b.Batch,'%')) + '%'
     and isnull(b.ECLNumber,'%') like '%' + isnull(@ECLNumber, isnull(b.ECLNumber,'%')) + '%'
end
