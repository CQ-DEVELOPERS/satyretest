﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Routing_Add
  ///   Filename       : p_Routing_Add.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Routing_Add
(
 @IssueId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update Issue
     set RoutingSystem = 0
   where IssueId = @IssueId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Routing_Add'); 
    rollback transaction
    return @Error
end
