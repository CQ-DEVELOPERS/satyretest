﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitArea_List
  ///   Filename       : p_StorageUnitArea_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2012 14:20:20
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitArea table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   StorageUnitArea.StorageUnitId,
  ///   StorageUnitArea.AreaId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitArea_List

as
begin
	 set nocount on;
  
	 declare @Error int
  select
         '-1' as StorageUnitId,
         null as 'StorageUnitArea',
         '-1' as AreaId,
         null as 'StorageUnitArea'
  union
  select
         StorageUnitArea.StorageUnitId,
         StorageUnitArea.StorageUnitId as 'StorageUnitArea',
         StorageUnitArea.AreaId,
         StorageUnitArea.AreaId as 'StorageUnitArea' 
    from StorageUnitArea
  
end
