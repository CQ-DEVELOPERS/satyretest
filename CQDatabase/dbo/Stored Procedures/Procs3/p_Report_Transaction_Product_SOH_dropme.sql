﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Transaction_Product_SOH_dropme
  ///   Filename       : p_Report_Transaction_Product_SOH_dropme.sql
  ///   Create By      : Karen
  ///   Date Created   : June 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Transaction_Product_SOH_dropme
(
 @WarehouseId      int,
 @StorageUnitId    int = null,
 @BatchId          int = null,
 @FromDate         date,
 @ToDate           date,
 @PrincipalId	   int
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  InstructionId				int,
	  InstructionTypeId			int,
	  InstructionType			nvarchar(30),
	  InstructionTypeCode		nvarchar(10),
	  InboundDocumentTypeId		int,
	  InboundDocumentType		nvarchar(30),
	  OutboundDocumentTypeId	int,
	  OutboundDocumentType		nvarchar(30),
	  StatusId					int,
	  Status					nvarchar(50),
	  CreateDate				datetime,
	  StartDate					datetime,
	  EndDate					datetime,
	  OperatorId				int,
	  Operator					nvarchar(50),
	  StorageUnitBatchId		int,
	  StorageUnitId				int,
	  ProductCode				nvarchar(30),
	  Product					nvarchar(50),
	  SKUCode					nvarchar(50),
	  Batch						nvarchar(50),
	  Location					nvarchar(15),
      PickLocationId			int null,
      PickLocation				nvarchar(15),
      StoreLocationId			int null,
      StoreLocation				nvarchar(15),
	  Quantity					float,
	  ConfirmedQuantity			float,
	  ReceiptLineId				int,
	  CheckQuantity				float,
	  NettWeight				float,
	  DRCR						int,
	  PalletId					int,
	  OrderNumber				nvarchar(30),
	  OpeningSOH				float,
	  ClosingSOH				float,
	  PrincipalId				int,
	  PrincipalCode				nvarchar(50)
	 )
	 
	 declare @Getdate	datetime = getdate()
	 
	 -- declare @TableRunningTotal1 as table
	 --(
	 -- StorageUnitBatchId		int,
	 -- SOHRunningTotal			float)
	  
	 if @StorageUnitId = -1
	   set @StorageUnitId = null
	 
	 if @BatchId = -1
	   set @BatchId = null
	   
    if @PrincipalId = -1
		set @PrincipalId = null
  
  insert @TableResult
         (InstructionId,
          InstructionTypeId,
          InboundDocumentTypeId,
          OutboundDocumentTypeId,
	      StatusId,
	      CreateDate,
	      StartDate,
	      EndDate,
	      OperatorId,
	      StorageUnitBatchId,
          PickLocationId,
          StoreLocationId,
	      Quantity,
	      ConfirmedQuantity,
	      ReceiptLineId,
	      CheckQuantity,
	      PalletId,
	      OrderNumber)
  select  i.InstructionId,
          i.InstructionTypeId,
          id.InboundDocumentTypeId,
          od.OutboundDocumentTypeId,
          i.StatusId,
	      i.CreateDate,
	      i.StartDate,
	      i.EndDate,
	      i.OperatorId,
	      i.StorageUnitBatchId,
          i.PickLocationId,
          i.StoreLocationId,
          CASE 
            WHEN isnull(i.Quantity,isl.Quantity) <= isl.Quantity 
               THEN isnull(i.Quantity,isl.Quantity)
               ELSE isnull(isl.Quantity,i.Quantity)
		  END,
	      --isnull(i.Quantity,isl.Quantity) as Quantity ,
	      CASE 
            WHEN isnull(i.ConfirmedQuantity,isl.ConfirmedQuatity) <= isl.ConfirmedQuatity 
               THEN isnull(i.ConfirmedQuantity,isl.ConfirmedQuatity)
               ELSE isnull(isl.ConfirmedQuatity,i.ConfirmedQuantity)
		  END,
	      --isnull (i.ConfirmedQuantity,isl.ConfirmedQuatity) as ConfirmedQuantity,
	      i.ReceiptLineId,
	      i.CheckQuantity,
	      i.PalletId,
	      isnull(od.OrderNumber, id.OrderNumber)
    from Instruction            i (nolock)
    join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    left join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
    left join IssueLine            isl (nolock) on isnull(ili.IssueLineId, ili.IssueLineId) = isl.IssueLineId
    left join OutboundDocument      od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
    left join ReceiptLine           rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
    left join Inboundline           il (nolock) on il.InboundLineId = rl.InboundLineId
    left join InboundDocument	    id (nolock) on id.InboundDocumentId = il.InboundDocumentId
   where i.WarehouseId = @WarehouseId
     and (i.CreateDate between @FromDate and @ToDate
     or   EndDate      between @FromDate and @ToDate)
     and sub.StorageUnitId  = isnull(@StorageUnitId, sub.StorageUnitId)
     and sub.BatchId        = isnull(@BatchId, sub.BatchId)
     
 
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         PrincipalId   = p.PrincipalId,
         StorageUnitId = su.StorageUnitId
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit          su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product               p (nolock) on su.ProductId          = p.ProductId
    join SKU                 sku (nolock) on su.SKUId              = sku.SKUId
    join Batch                 b (nolock) on sub.BatchId           = b.BatchId
    
    update tr
     set OpeningSOH       = pm.OpeningBalance
    from @TableResult tr 
    join ProductMovement pm (nolock) on tr.StorageUnitBatchId = pm.StorageUnitBatchId
    where  pm.MovementDate between @FromDate and DATEADD(dd,1,@FromDate) 
    --and  pm.MovementDate between DATEADD(dd,-1,@FromDate) and @FromDate
    
    update tr
     set ClosingSOH		  = pm.Balance
    from @TableResult tr 
    join ProductMovement pm (nolock) on tr.StorageUnitBatchId = pm.StorageUnitBatchId
     where  pm.MovementDate between DATEADD(dd,-1,@ToDate) and @ToDate
   
   print  CAST(@ToDate as date)
   print  cast(@Getdate as date)
   
   if CAST(@ToDate as date) = cast(@Getdate as date)
	 update tr2
        set ClosingSOH	= (select sum(ActualQuantity) 
							from StorageUnitBatch sub (nolock) --on sub.StorageUnitBatchId = tr.StorageUnitBatchId
							join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
						   where sub.StorageUnitId = tr2.StorageUnitId )
       from @TableResult tr2 
	
  update tr
     set Batch         = b.Batch + ' *'
    from @TableResult tr
    join ReceiptLine          rl (nolock) on tr.ReceiptLineId      = rl.ReceiptLineId
    join StorageUnitBatch    sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch                 b (nolock) on sub.BatchId           = b.BatchId
   where tr.Batch = 'Default'
  
  update tr
     set InstructionType = it.InstructionType,
         InstructionTypeCode = it.InstructionTypeCode
    from @TableResult    tr
    join InstructionType it (nolock) on tr.InstructionTypeId = it.InstructionTypeId
    
  update tr
     set OutboundDocumentType = odt.OutboundDocumentType
    from @TableResult    tr
    join OutboundDocumentType odt (nolock) on tr.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    
  update tr
     set PrincipalCode = p.PrincipalCode
    from @TableResult tr
    join Principal        p (nolock) on tr.PrincipalId = p.PrincipalId
    
  update tr
     set InboundDocumentType = idt.inboundDocumentType
    from @TableResult    tr
    join InboundDocumentType idt (nolock) on tr.InboundDocumentTypeId = idt.InboundDocumentTypeId
    
  update tr
     set InboundDocumentType = 'Stock Take'
    from @TableResult    tr
    where tr.InstructionTypeCode in ('STA', 'STE', 'STL', 'STP')
  
  update @TableResult
     set InstructionType = InstructionType + '*User'
    where CheckQuantity = 1
      and InstructionTypeCode = 'R'
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.StatusId = s.StatusId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId

  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId

  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
  
  update tr
     set DRCR = ConfirmedQuantity * -1
    from @TableResult tr
   --where InstructionTypeCode in ('M', 'P', 'O', 'PM', 'PS', 'FM')
   where InstructionTypeCode in ('P', 'O', 'PM', 'PS', 'FM')
   
   update tr
     set DRCR = ConfirmedQuantity 
    from @TableResult tr
   --where InstructionTypeCode in ('R', 'S', 'SM', 'PR', 'HS', 'RS')
   where InstructionTypeCode in ('S', 'SM', 'PR', 'HS', 'RS')
   
   update tr
     set DRCR = isnull(ConfirmedQuantity,0) - isnull(Quantity,0)
    from @TableResult tr
   where InstructionTypeCode in ('STE', 'STL', 'STA', 'STP')
   
   update tr
     set DRCR = 0
    from @TableResult tr
   where tr.Status in ('Waiting', 'Deleted')
   
  --insert @TableRunningTotal1
  --       (StorageUnitBatchId,
  --       SOHRunningTotal)
  --select StorageUnitBatchId
  --from @TableResult
         
          
  select InstructionType  
        ,Status  
        ,Operator  
        ,ProductCode  
        ,Product  
        ,SKUCode  
        ,Batch  
        ,InstructionId  
        ,PickLocation  
        ,StoreLocation  
        ,Quantity  
        ,ConfirmedQuantity  
        ,CreateDate  
        ,StartDate  
        ,EndDate  
        ,NettWeight  
        ,DRCR  
        ,PalletId  
        ,OrderNumber  
        ,OpeningSOH  
        ,ClosingSOH
        ,isnull(InboundDocumentType, isnull(outboundDocumentType,InstructionType)) as DocumentType
        ,PrincipalCode  
    from @TableResult  
   where isnull(PrincipalId, -1) = isnull(@PrincipalId, isnull(PrincipalId, -1))
  order by ProductCode, SKUCode, isnull(EndDate, isnull(StartDate, CreateDate)) 
end
 
 
