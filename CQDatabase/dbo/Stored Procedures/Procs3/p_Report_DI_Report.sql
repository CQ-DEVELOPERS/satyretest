﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_DI_Report
  ///   Filename       : p_Report_DI_Report.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_DI_Report
(
 @WarehouseId	int,
 @FromDate		date,
 @ToDate		date
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OrderNumber			nvarchar(30),
   DINumber				nvarchar(20),
   ExternalCompanyId	int,
   ExternalCompanyCode	nvarchar(10),
   ExternalCompany		nvarchar(255),
   PrincipalCode		nvarchar(30),
   Status				nvarchar(50),
   StorageUnitId		int,
   ProductCode			nvarchar(30),
   Product				nvarchar(255),
   SKUCode				nvarchar(10),
   SKU					nvarchar(50),
   Quantity				float,
   ConfirmedQuantity	float,
   DeliveryDate			datetime,
   CreateDate			datetime,
   OutstandingQuantity	float
  )
  
 
    insert	@TableResult
			(
			OrderNumber,
			DINumber,
			ExternalCompanyId,
			PrincipalCode,
			Status,
			StorageUnitId,
			Quantity,
			ConfirmedQuantity,
			DeliveryDate,
			CreateDate
			)
	select	od.OrderNumber, 
			SUBSTRING(od.OrderNumber, 1, CHARINDEX('_', od.OrderNumber) - 1),
			od.ExternalCompanyId,
			p.PrincipalCode,
			s.Status, 
			ol.StorageUnitId,
			il.Quantity, 
			il.ConfirmedQuatity, 
			i.DeliveryDate,
			od.CreateDate 
	  from OutboundDocument od
	  join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
	  join OutboundLine			 ol (nolock) on od.OutboundDocumentId = ol.OutboundDocumentId
	  join StorageUnit			 su (nolock) on ol.StorageUnitId = su.StorageUnitId
	  join Issue				  i (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
	  join IssueLine			 il (nolock) on i.IssueId = il.IssueId
	  join Status				  s (nolock) on il.StatusId = s.StatusId
	  join Principal			  p (nolock) on od.PrincipalId = p.PrincipalId
	 where od.CreateDate between @FromDate and dateadd(dd,1,@ToDate)
   	   and OutboundDocumentTypeCode = 'DI'
   	   and i.StatusId in (dbo.ufn_StatusId('IS','C'),
   						  dbo.ufn_StatusId('IS','CD'),
   						  dbo.ufn_StatusId('IS','CK'),
   						  dbo.ufn_StatusId('IS','D'),
   						  dbo.ufn_StatusId('IS','DC'))
	   --and od.WarehouseId = @WarehouseId
  
  update tr
     set ProductCode  = p.ProductCode,
		 Product      = p.Product
    from @TableResult   tr
    join StorageUnit	su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product		 p (nolock) on su.ProductId = p.ProductId
    
  update tr
     set SKUCode  = sku.SKUCode,
		 SKU      = sku.SKU
    from @TableResult   tr
    join StorageUnit	su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join SKU		   sku (nolock) on su.SKUId = sku.SKUId 
    
  update tr
     set ExternalCompanyCode = ec.ExternalCompanyCode,
		 ExternalCompany = ec.ExternalCompany
    from @TableResult    tr
    join ExternalCompany ec(nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId  
  
	select	OrderNumber,
			DINumber,
			ExternalCompanyCode,
			ExternalCompany,
			PrincipalCode,
			Status,
			StorageUnitId,
			ProductCode,
			Product,
			SKUCode,
			SKU,
			Quantity,
			ConfirmedQuantity,
			DeliveryDate,
			CreateDate,
			isnull(Quantity,0) - isnull(ConfirmedQuantity,0) as OutstandingQuantity
      from	@TableResult
    
end
