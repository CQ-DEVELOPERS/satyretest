﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Take_On_Stock_Master
  ///   Filename       : p_Report_Take_On_Stock_Master.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Take_On_Stock_Master

as
begin
	 set nocount on;  
 
  select Location,
         ProductCode,
         Batch,
         subl.ActualQuantity,
         UnitPrice    
  from Product p  (nolock)
  join StorageUnit  su (nolock) on p.ProductId = su.ProductId
  join StorageUnitBatch sub (nolock) on su.StorageUnitId = sub.StorageUnitId
  join StorageUnitBatchLocation subl (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
  join Location l on subl.LocationId = l.LocationId
  join Batch b on b.BatchId = sub.BatchId
end

 
 
