﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitExternalCompany_List
  ///   Filename       : p_StorageUnitExternalCompany_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:57
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitExternalCompany table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   StorageUnitExternalCompany.StorageUnitId,
  ///   StorageUnitExternalCompany.ExternalCompanyId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitExternalCompany_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as StorageUnitId
        ,null as 'StorageUnitExternalCompany'
        ,'-1' as ExternalCompanyId
        ,null as 'StorageUnitExternalCompany'
  union
  select
         StorageUnitExternalCompany.StorageUnitId
        ,StorageUnitExternalCompany.StorageUnitId as 'StorageUnitExternalCompany'
        ,StorageUnitExternalCompany.ExternalCompanyId
        ,StorageUnitExternalCompany.ExternalCompanyId as 'StorageUnitExternalCompany'
    from StorageUnitExternalCompany
  
end
