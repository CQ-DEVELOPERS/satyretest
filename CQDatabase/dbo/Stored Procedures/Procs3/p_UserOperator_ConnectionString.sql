﻿
CREATE procedure p_UserOperator_ConnectionString
(
 @UserName nvarchar(512)
)

as
begin
  select uo.ConnectionString
    from Security.dbo.aspnet_Users         u (nolock)
    join CQuentialCommon.dbo.UserOperator uo (nolock) on u.UserId = uo.UserId
   where u.UserName = @UserName
end
