﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_StorageUnitArea_Search
  ///   Filename       : p_Static_StorageUnitArea_Search.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 19 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///       <@StorageUnitId> 
  /// </param>
  /// <returns>

  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_StorageUnitArea_Search
(
 @StorageUnitId            int,
 @WarehouseId				int
)

as
begin
	 set nocount on;

SELECT	sua.StorageUnitId, 
		a.AreaId, 
		a.Area, 
		sua.StoreOrder, 
		sua.PickOrder,
		sua.MinimumQuantity,
		sua.ReorderQuantity,
		sua.MaximumQuantity
FROM    StorageUnitArea AS sua 
INNER JOIN Area AS a ON sua.AreaId = a.AreaId
Where	sua.StorageUnitId = @StorageUnitId  
and		a.WarehouseId = @WarehouseId
 
end
