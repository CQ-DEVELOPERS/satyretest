﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Track_Order
  ///   Filename       : p_Report_Track_Order.sql
  ///   Create By      : Karen
  ///   Date Created   : November 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    
  ///   Modified Date 
  ///   Details       
  /// </newpara>
*/
CREATE procedure p_Report_Track_Order
(
 @OrderNumber	nvarchar(30),
 @Description	nvarchar(255)
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   LineNumber         int,
   OutboundShipmentId int,
   IssueId            int,
   IssueLineId        int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   JobId              int,
   InstructionId      int,
   StorageUnitBatchId int,
   StorageUnitId	  int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Description	      nvarchar(255),
   Statusid			  int,
   Status			  nvarchar(50),
   LineStatusid		  int,
   LineStatus		  nvarchar(50),
   InsertDate		  datetime,
   ProcessDate		  datetime,
   WarehouseCode      nvarchar(10),
   Quantity			  int,
   SortDate			  datetime  	 
  )
  
  if @Description = 'Import PO into Cquential'
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate)
  select OrderNumber,
		 'Import PO into Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate)
  From InterfaceImportPOHeader      h (nolock)
  left join InterfaceImportPODetail d (nolock) on d.InterfaceImportPOHeaderId = h.InterfaceImportPOHeaderId
  join RecordStatus                rs (nolock) on rs.StatusCode = h.RecordStatus
  where OrderNumber = @OrderNumber
  
  if @Description = 'Import into Cquential'
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate)
  select distinct OrderNumber,
		 'Import into Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate)
  From InterfaceImportHeader      h (nolock)
  left join InterfaceImportDetail d (nolock) on d.InterfaceImportHeaderId = h.InterfaceImportHeaderId
  join RecordStatus                rs (nolock) on rs.StatusCode = h.RecordStatus
  where isnull(OrderNumber, primarykey) = @OrderNumber
  
  if @Description = 'Import SO into Cquential'
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate)
  select OrderNumber,
		 'Import SO into Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate)
  From InterfaceImportSOHeader      h (nolock)
  left join InterfaceImportSODetail d (nolock) on d.InterfaceImportSOHeaderId = h.InterfaceImportSOHeaderId
  join RecordStatus                rs (nolock) on rs.StatusCode = h.RecordStatus
  where OrderNumber = @OrderNumber
  
  
  if @Description = (select InboundDocumentType from InboundDocumentType where InboundDocumentType = @Description)
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         StorageUnitBatchId,
         LineNumber,
         WarehouseCode,
         Quantity,
         LineStatus,
         SortDate)
  select OrderNumber,
		 @Description as Description,
		 s1.Status,
		 r.DeliveryDate,
		 null,
		 rl.StorageUnitBatchId,
		 rl.ReceiptLineId,
		 r.WarehouseId,
		 rl.AcceptedQuantity,
		 s2.Status,
		 r.DeliveryDate
  From InboundDocument  id (nolock)
  left join Receipt      r (nolock) on id.InboundDocumentId = r.InboundDocumentId
  left join ReceiptLine rl(nolock)  on r.ReceiptId = rl.ReceiptId 
  join Status           s1 (nolock) on s1.StatusId = r.StatusId
  join Status           s2 (nolock) on s2.StatusId = rl.StatusId
  where OrderNumber = @OrderNumber
  
  
  if @Description = (select OutboundDocumentType from OutboundDocumentType where OutboundDocumentType = @Description)
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         StorageUnitBatchId,
         LineNumber,
         WarehouseCode,
         Quantity,
         LineStatus,
         SortDate)
  select OrderNumber,
		 @Description as Description,
		 s1.Status,
		 od.CreateDate,
		 od.ModifiedDate,
		 il.StorageUnitBatchId,
		 il.IssueLineId,
		 i.WarehouseId,
		 il.ConfirmedQuatity,
		 s2.Status,
		 isnull(i.DeliveryDate, od.createdate) as SortDate
  From OutboundDocument od (nolock)
  left join Issue        i (nolock) on od.OutboundDocumentid = i.OutboundDocumentid
  left join IssueLine   il (nolock) on i.IssueId = il.IssueId 
  join Status           s1 (nolock) on s1.StatusId = i.StatusId
  join Status           s2(nolock)  on s2.StatusId = il.StatusId
  where OrderNumber = @OrderNumber
  
  if @Description = 'Export PO from Cquential'
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate)
  select OrderNumber,
		 'Export PO from Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate)
  From InterfaceExportPOHeader      h (nolock)
  left join InterfaceExportPODetail d (nolock) on d.InterfaceExportPOHeaderId = h.InterfaceExportPOHeaderId
  join RecordStatus                rs (nolock) on rs.StatusCode = h.RecordStatus
  where OrderNumber = @OrderNumber
  
  if @Description = 'Export from Cquential'
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate)
  select distinct OrderNumber,
		 'Export from Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate)
  From InterfaceExportHeader      h (nolock)
  left join InterfaceExportDetail d (nolock) on d.InterfaceExportHeaderId = h.InterfaceExportHeaderId
  join RecordStatus                rs (nolock) on rs.StatusCode = h.RecordStatus
  where isnull(OrderNumber, PrimaryKey) = @OrderNumber
  
  if @Description = 'Export SO from Cquential'
  insert @TableResult
        (OrderNumber,
         Description,
         Status,
         InsertDate,
         ProcessDate,
         ProductCode,
         Product,
         LineNumber,
         WarehouseCode,
         Quantity,
         SortDate)
  select OrderNumber,
		 'Export SO from Cquential',
		 StatusDesc,
		 h.InsertDate,
		 h.ProcessedDate,
		 d.ProductCode,
		 d.Product,
		 d.LineNumber,
		 d.Additional1,
		 d.Quantity,
		 isnull(h.ProcessedDate, h.InsertDate)
  From InterfaceExportSOHeader      h (nolock)
  left join InterfaceExportSODetail d (nolock) on d.InterfaceExportSOHeaderId = h.InterfaceExportSOHeaderId
  join RecordStatus                rs (nolock) on rs.StatusCode = h.RecordStatus
  where OrderNumber = @OrderNumber
    
  update tr
     set ProductCode = p.ProductCode,
		 Product = p.Product
    from @TableResult     tr
    join StorageUnit      su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product           p (nolock) on p.ProductId = su.ProductId
   where tr.StorageUnitId is not null
   and   tr.ProductCode is null
   
   update tr
     set ProductCode = p.ProductCode,
		 Product = p.Product
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on p.ProductId = su.ProductId
   where tr.StorageUnitBatchId is not null
   and   tr.StorageUnitId is null
   and   tr.ProductCode is null
   
     update tr
     set ProductCode = p.ProductCode,
		 Product = p.Product
    from @TableResult tr
    join Product       p (nolock) on p.HostId = tr.ProductCode
   where tr.Product is null
   
  select OrderNumber,
		 Description,
         ProductCode,
		 Product,
	     Status,
	     LineNumber,
         LineStatus,
	     InsertDate,
		 ProcessDate,
		 WarehouseCode,
		 Quantity	 
    from @TableResult
  order by SortDate
end
