﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Throughput_Metrics_test
  ///   Filename       : p_Report_Throughput_Metrics_test.sql
  ///   Create By      : Karen
  ///   Date Created   : February 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Throughput_Metrics_test
(
 @FromDate				date,
 @ToDate				date,
 @PrincipalId			int,
 @FromTime				nvarchar(50),
 @ToTime				nvarchar(50)
)


as
begin
	 set nocount on;
	 
  declare @PrincipalCode		nvarchar(50),
		  @DeliveryDate			date,
		  @AMPM					nvarchar(2),
		  @StartTime  			time,
		  @EndTime				time
  
  if @PrincipalId = -1
	set @PrincipalId = null
  
  if @PrincipalId is not null
	select @PrincipalCode = PrincipalCode
	  from Principal
	 where PrincipalId = @PrincipalId
	
 	 
  SELECT @StartTime = SUBSTRING(@FromTime,12,5)
  SELECT @AMPM = SUBSTRING(@FromTime,21,2)

  if @AMPM = 'PM'
	set @StartTime = dateadd(hh, 12, @StartTime)
	
  SELECT @EndTime = SUBSTRING(@ToTime,12,5)
  SELECT @AMPM = SUBSTRING(@ToTime,21,2)

  if @AMPM = 'PM'
	set @EndTime = dateadd(hh, 12, @EndTime)
  
  declare @TableHeader as table
  (
   DeliveryDate					date,
   QtyShipmentsReceived			int,
   QtyLinesReceived				int,
   QtyPalletsReceived			int,
   QtyCasesReceived				int,
   QtyUnitsReceived				int,
   CCStockQtyExpected			int,
   CCStockQtyActual				int,
   CCStockQtyVariance			int,
   CCStockQtyVariancePct		float,
   StockAccuracyPct				float,
   NoOfLocationsCounted			int,
   NoOfDeviatedLocations		int,
   QtyOrdersPicked				int,
   QtyOrderLinesPicked			int,
   QtyOrdersShipped				int,
   QtyOrderLinesShipped			int,
   QtyPalletsShipped			int,
   QtyCasesShipped				int,
   QtyUnitsShipped				int,
   WeightShipped				int,
   VolumeShipped				int	
  ) 
  
 
	SET @DeliveryDate = @FromDate
		WHILE (@DeliveryDate <= @ToDate)
		BEGIN
			insert @TableHeader
				(DeliveryDate)
			select 
				@DeliveryDate
			SET @DeliveryDate = DATEADD(dd,1,@DeliveryDate)
		END
    
   
    
    SET ARITHABORT OFF
	SET ANSI_WARNINGS OFF 
	
	update @TableHeader 
	   set QtyShipmentsReceived = (select COUNT(distinct InboundDocumentId) 
						  from InboundDocument id 
						 where id.deliverydate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
						 --and cast(id.deliverydate as time) between @FromTime and @ToTime
						 and id.PrincipalId = isnull(@PrincipalId,id.PrincipalId))
      from @TableHeader th
      
   
   
       
    update @TableHeader 
	   set QtyLinesReceived = (select COUNT(distinct InboundLineId) 
						  from InboundDocument id 
						  join InboundLine il on id.InboundDocumentId = il.InboundDocumentId
						 where id.deliverydate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
						 and id.PrincipalId = isnull(@PrincipalId,id.PrincipalId))
      from @TableHeader th
      
       select * from @TableHeader
      
    update @TableHeader 
	   set QtyPalletsReceived = (select sum(NumberOfPallets) 
						  from InboundDocument id 
						  join Receipt r on id.InboundDocumentId = r.InboundDocumentId 
						  join ReceiptLine rl on r.ReceiptId = rl.ReceiptId
						 where id.deliverydate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
						 and cast(id.deliverydate as time) between @FromTime and @ToTime
						 and id.PrincipalId = isnull(@PrincipalId,id.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set QtyCasesReceived = 0
      from @TableHeader th
      
    update @TableHeader 
	   set QtyUnitsReceived = (select sum(AcceptedQuantity) 
						  from InboundDocument id 
						  join Receipt r on id.InboundDocumentId = r.InboundDocumentId 
						  join ReceiptLine rl on r.ReceiptId = rl.ReceiptId
						 where id.deliverydate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
						 and cast(id.deliverydate as time) between @FromTime and @ToTime
						 and id.PrincipalId = isnull(@PrincipalId,id.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set CCStockQtyExpected = (select sum(distinct i.Quantity) 
								   from Instruction      i (nolock)
								   join InstructionType it (nolock) on i.InstructionTypeId   = it.InstructionTypeId
								   join StorageUnitBatch sub on i.StorageUnitBatchId = sub.StorageUnitBatchId
								   join StorageUnit su on sub.StorageUnitId = su.StorageUnitId
								   join Product p on su.ProductId = p.ProductId
								  where i.CreateDate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
								  and cast(i.CreateDate as time) between @FromTime and @ToTime
									and it.InstructionTypeCode in ('STE','STL','STA','STP')
									and p.PrincipalId = isnull(@PrincipalId,p.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set CCStockQtyActual = (select sum(distinct i.ConfirmedQuantity) 
							     from Instruction      i (nolock)
							     join InstructionType it (nolock) on i.InstructionTypeId   = it.InstructionTypeId
							     join StorageUnitBatch sub on i.StorageUnitBatchId = sub.StorageUnitBatchId
								   join StorageUnit su on sub.StorageUnitId = su.StorageUnitId
								   join Product p on su.ProductId = p.ProductId
							    where i.CreateDate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
							    and cast(i.CreateDate as time) between @FromTime and @ToTime
								  and it.InstructionTypeCode in ('STE','STL','STA','STP')
								  and p.PrincipalId = isnull(@PrincipalId,p.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set CCStockQtyVariance = isnull(CCStockQtyExpected,0) - isnull(CCStockQtyActual,0)
      from @TableHeader th
      
    update @TableHeader 
	   set CCStockQtyVariancePct = (100.0 * CCStockQtyVariance) / CCStockQtyExpected 
      from @TableHeader th
      
    update @TableHeader 
	   set StockAccuracyPct = 100 - CCStockQtyVariancePct
      from @TableHeader th
      
    update @TableHeader 
	   set NoOfLocationsCounted = (select count(distinct i.PickLocationId) 
									 from Instruction      i (nolock)
									 join InstructionType it (nolock) on i.InstructionTypeId   = it.InstructionTypeId
									 join StorageUnitBatch sub on i.StorageUnitBatchId = sub.StorageUnitBatchId
								   join StorageUnit su on sub.StorageUnitId = su.StorageUnitId
								   join Product p on su.ProductId = p.ProductId
									where i.CreateDate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
									and cast(i.CreateDate as time) between @FromTime and @ToTime
									  and it.InstructionTypeCode in ('STE','STL','STA','STP')
									  and p.PrincipalId = isnull(@PrincipalId,p.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set NoOfDeviatedLocations = (select count(distinct i.PickLocationId) 
									  from Instruction      i (nolock)
									  join InstructionType it (nolock) on i.InstructionTypeId   = it.InstructionTypeId
									  join StorageUnitBatch sub on i.StorageUnitBatchId = sub.StorageUnitBatchId
								   join StorageUnit su on sub.StorageUnitId = su.StorageUnitId
								   join Product p on su.ProductId = p.ProductId
								  	 where i.CreateDate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
								  	 and cast(i.CreateDate as time) between @FromTime and @ToTime
									   and it.InstructionTypeCode in ('STE','STL','STA','STP')
									   and p.PrincipalId = isnull(@PrincipalId,p.PrincipalId)
									   and i.Quantity != i.ConfirmedQuantity)
      from @TableHeader th
      
    update @TableHeader 
	   set QtyOrdersPicked = (select COUNT(distinct ordernumber) 
							    from OutboundDocument od 
							    join Issue i on od.OutboundDocumentId = i.OutboundDocumentId
							   where od.DeliveryDate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
							   and cast(od.DeliveryDate as time) between @FromTime and @ToTime
							   and i.statusid in (dbo.ufn_StatusId('IS','CD'),dbo.ufn_StatusId('IS','CK'),dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC'),dbo.ufn_StatusId('IS','F'))
							   and od.PrincipalId = isnull(@PrincipalId,od.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set QtyOrderLinesPicked = (select COUNT(il.IssueLineId) 
							    from OutboundDocument od 
							    join Issue i on od.OutboundDocumentId = i.OutboundDocumentId
							    join IssueLine il on i.IssueId = il.IssueId
							   where od.deliverydate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
							   and cast(od.DeliveryDate as time) between @FromTime and @ToTime
							   and il.statusid in (dbo.ufn_StatusId('IS','CD'),dbo.ufn_StatusId('IS','CK'),dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC'),dbo.ufn_StatusId('IS','F'))
							   and od.PrincipalId = isnull(@PrincipalId,od.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set QtyOrdersShipped = (select COUNT(distinct ordernumber) 
							    from OutboundDocument od 
							    join Issue i on od.OutboundDocumentId = i.OutboundDocumentId
							   where od.deliverydate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
							   and cast(od.DeliveryDate as time) between @FromTime and @ToTime
							   and i.statusid in (dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC'))
							   and od.PrincipalId = isnull(@PrincipalId,od.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set QtyOrderLinesShipped = (select COUNT(il.IssueLineId) 
							    from OutboundDocument od 
							    join Issue i on od.OutboundDocumentId = i.OutboundDocumentId
							    join IssueLine il on i.IssueId = il.IssueId
							   where od.deliverydate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
							   and cast(od.DeliveryDate as time) between @FromTime and @ToTime
							   and il.statusid in (dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC'))
							   and od.PrincipalId = isnull(@PrincipalId,od.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set QtyPalletsShipped = (select sum(i.Pallets) 
							      from OutboundDocument od 
							      join Issue i on od.OutboundDocumentId = i.OutboundDocumentId
							     where od.deliverydate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
							     and cast(od.DeliveryDate as time) between @FromTime and @ToTime
							     and i.statusid in (dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC'))
							     and od.PrincipalId = isnull(@PrincipalId,od.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set QtyCasesShipped = 0
      from @TableHeader th
      
    update @TableHeader 
	   set QtyUnitsShipped = (select sum(il.ConfirmedQuatity) 
							    from OutboundDocument od 
							    join Issue i on od.OutboundDocumentId = i.OutboundDocumentId
							    join IssueLine il on i.IssueId = il.IssueId
							   where od.deliverydate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
							   and cast(od.DeliveryDate as time) between @FromTime and @ToTime
							   and il.statusid in (dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC'))
							   and od.PrincipalId = isnull(@PrincipalId,od.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set WeightShipped = (select sum(il.ConfirmedWeight) 
							    from OutboundDocument od 
							    join Issue i on od.OutboundDocumentId = i.OutboundDocumentId
							    join IssueLine il on i.IssueId = il.IssueId
							   where od.deliverydate between th.DeliveryDate and dateadd(dd,1,th.DeliveryDate)
							   and cast(od.DeliveryDate as time) between @FromTime and @ToTime
							   and il.statusid in (dbo.ufn_StatusId('IS','D'),dbo.ufn_StatusId('IS','DC'))
							   and od.PrincipalId = isnull(@PrincipalId,od.PrincipalId))
      from @TableHeader th
      
    update @TableHeader 
	   set VolumeShipped = 0
      from @TableHeader th    
  
  --select *,
		-- @PrincipalCode as PrincipalCode,
		-- @StartTime as FromTime,
		-- @EndTime as ToTime
  --  from @TableHeader


  
end
