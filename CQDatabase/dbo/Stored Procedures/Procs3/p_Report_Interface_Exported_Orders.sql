﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Interface_Exported_Orders
  ///   Filename       : p_Report_Interface_Exported_Orders.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Jun 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Interface_Exported_Orders
(
 @WarehouseId int,
 @OrderNumber nvarchar(30),
 @FromDate    datetime,
 @ToDate      datetime
)

as
begin
	 set nocount on;
	 
	 declare @ProcessedDate datetime,
	         @rowcount      int
	 
  declare @TableResult as table
  (
   InterfaceExportHeaderId         int,
   OrderNumber                     nvarchar(max),
   RecordType                      nvarchar(30),
   RecordStatus                    char(1),
   CustomerCode                    nvarchar(30),
   Customer                        nvarchar(255),
   Address                         nvarchar(255),
   FromWarehouseCode               nvarchar(50),
   ToWarehouseCode                 nvarchar(50),
   DeliveryDate                    nvarchar(20),
   Remarks                         nvarchar(255),
   NumberOfLines                   int,
   ProcessedDate                   datetime,
   Additional1                     nvarchar(255),
   ReceivedFromHost                nvarchar(max),
   Data                            nvarchar(max),
   LineNumber                      int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(50),
   SKUCode                         nvarchar(50),
   Batch                           nvarchar(50),
   Quantity                        decimal,
   Weight                          decimal
  )
  
  if @OrderNumber is null
    set @OrderNumber = ''
  
  insert @TableResult
        (InterfaceExportHeaderId,
         Data,
         ReceivedFromHost,
         OrderNumber,
         RecordType,
         RecordStatus,
         CustomerCode,
         Customer,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         ProcessedDate,
         Additional1,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight)
  select h.InterfaceExportHeaderId,
         substring(h.WebServiceMsg,1,case when charindex('<?xml version="1.0"', h.WebServiceMsg,0) <=0 then datalength(h.WebServiceMsg) else charindex('<?xml version="1.0"', h.WebServiceMsg,0) end),
         h.ProcessedDate,
         isnull(isnull(convert(nvarchar(max), h.OrderNumber), h.PrimaryKey), h.Remarks) as 'OrderNumber',
         h.RecordType,
         h.RecordStatus,
         h.CompanyCode,
         h.Company,
         h.Address,
         h.FromWarehouseCode,
         h.ToWarehouseCode,
         h.DeliveryDate,
         h.Remarks,
         h.NumberOfLines,
         h.ProcessedDate,
         h.Additional1,
         d.LineNumber,
         d.ProductCode,
         d.Product,
         d.SKUCode,
         d.Batch,
         d.Quantity,
         d.Weight
    from InterfaceExportHeader h (nolock)
    join InterfaceExportDetail d (nolock) on h.InterfaceExportHeaderId = d.InterfaceExportHeaderId
   where isnull(h.OrderNumber, isnull(h.PrimaryKey,'')) like '%' + @OrderNumber + '%'
     and isnull(h.ProcessedDate, h.InsertDate) between @FromDate and @ToDate
  
  select @rowcount = @@rowcount
  
  set rowcount 1
  update h
     set RecordStatus  = 'N',
         WebServiceMsg = null,
         ProcessedDate = getdate()
    from @TableResult          tr
    join InterfaceExportHeader  h on tr.InterfaceExportHeaderId = h.InterfaceExportHeaderId
   where h.RecordStatus = 'E'
  set rowcount 0
  
  if @rowcount = 0
  begin
  insert @TableResult
        (OrderNumber,
         RecordType,
         RecordStatus,
         CustomerCode,
         Customer,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         ProcessedDate,
         Additional1,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight)
  select isnull(h.OrderNumber, h.PrimaryKey) as 'OrderNumber',
         h.RecordType,
         h.RecordStatus,
         h.CustomerCode,
         h.Customer,
         h.Address,
         h.FromWarehouseCode,
         h.ToWarehouseCode,
         h.DeliveryDate,
         h.Remarks,
         h.NumberOfLines,
         h.ProcessedDate,
         h.Additional1,
         d.LineNumber,
         d.ProductCode,
         d.Product,
         d.SKUCode,
         d.Batch,
         d.Quantity,
         d.Weight
    from InterfaceExportSOHeader h (nolock)
    join InterfaceExportSODetail d (nolock) on h.InterfaceExportSOHeaderId = d.InterfaceExportSOHeaderId
   where isnull(h.OrderNumber, h.PrimaryKey) like '%' + @OrderNumber + '%'
     and isnull(h.ProcessedDate, h.InsertDate) between @FromDate and @ToDate
  union
  select isnull(h.OrderNumber, h.PrimaryKey) as 'OrderNumber',
         h.RecordType,
         h.RecordStatus,
         h.SupplierCode,
         h.Supplier,
         h.Address,
         h.FromWarehouseCode,
         h.ToWarehouseCode,
         h.DeliveryDate,
         h.Remarks,
         h.NumberOfLines,
         h.ProcessedDate,
         h.Additional1,
         d.LineNumber,
         d.ProductCode,
         d.Product,
         d.SKUCode,
         d.Batch,
         d.Quantity,
         d.Weight
    from InterfaceExportPOHeader h (nolock)
    join InterfaceExportPODetail d (nolock) on h.InterfaceExportPOHeaderId = d.InterfaceExportPOHeaderId
   where isnull(h.OrderNumber, h.PrimaryKey) like '%' + @OrderNumber + '%'
     and isnull(h.ProcessedDate, h.InsertDate) between @FromDate and @ToDate
  
  update tr
     set ReceivedFromHost = il.ProcessedDate,
         Data             = il.Data
    from @TableResult tr
    join InterfaceLog il (nolock) on il.Data like '%' + tr.OrderNumber + '%'
   where il.Data not like '%APOP%'
  
  update tr
     set ReceivedFromHost = il.ProcessedDate,
         Data             = il.Data
    from @TableResult tr
    join InterfaceLog il (nolock) on il.Data like '%' + tr.Additional1 + '%'
   where tr.Additional1 is not null
     and tr.ReceivedFromHost is null
  end
  
  select OrderNumber,
         RecordType,
         RecordStatus,
         CustomerCode,
         Customer,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         ProcessedDate,
         ReceivedFromHost,
         Data,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight
    from @TableResult
end
