﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitExternalCompany_Select
  ///   Filename       : p_StorageUnitExternalCompany_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:58
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @StorageUnitId int = null,
  ///   @ExternalCompanyId int = null 
  /// </param>
  /// <returns>
  ///   StorageUnitExternalCompany.StorageUnitId,
  ///   StorageUnitExternalCompany.ExternalCompanyId,
  ///   StorageUnitExternalCompany.AllocationCategory,
  ///   StorageUnitExternalCompany.MinimumShelfLife,
  ///   StorageUnitExternalCompany.ProductCode,
  ///   StorageUnitExternalCompany.SKUCode,
  ///   StorageUnitExternalCompany.DefaultQC,
  ///   StorageUnitExternalCompany.SendToQC 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitExternalCompany_Select
(
 @StorageUnitId int = null,
 @ExternalCompanyId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         StorageUnitExternalCompany.StorageUnitId
        ,StorageUnitExternalCompany.ExternalCompanyId
        ,StorageUnitExternalCompany.AllocationCategory
        ,StorageUnitExternalCompany.MinimumShelfLife
        ,StorageUnitExternalCompany.ProductCode
        ,StorageUnitExternalCompany.SKUCode
        ,StorageUnitExternalCompany.DefaultQC
        ,StorageUnitExternalCompany.SendToQC
    from StorageUnitExternalCompany
   where isnull(StorageUnitExternalCompany.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(StorageUnitExternalCompany.StorageUnitId,'0'))
     and isnull(StorageUnitExternalCompany.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(StorageUnitExternalCompany.ExternalCompanyId,'0'))
  
end
