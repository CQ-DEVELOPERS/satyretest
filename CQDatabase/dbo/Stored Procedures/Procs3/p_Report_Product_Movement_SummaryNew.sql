﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Product_Movement_SummaryNew
  ///   Filename       : p_Report_Product_Movement_SummaryNew.sql
  ///   Create By      : Karen
  ///   Date Created   : 14 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Product_Movement_SummaryNew
(
 @FromDate          datetime,
 @ToDate            datetime,
 @ProductCode		nvarchar(30),
 @WarehouseId		int
)

as
begin
	 set nocount on;
	 
	 declare @Weeks int,
	         @Count int,
	         @Date  datetime
	 

  
  declare @TableResult as table
  (
   Week				smallint,
   StartDate		datetime,
   EndDate			datetime,
   MovementDate		datetime,
   ProductCode		nvarchar(30),
   Opening       int,
   Received      int,
   Issued        int,
   Balance       int,
   Variance      int,
   Percentage    int,
   StockOnHand   int,
   WarehouseId	 int,
   StorageUnitId	int,
   SKUId			int
  )
  
  if @ProductCode = '-1' 
	set @ProductCode = null
 
  -- how many weeks between from and to date
  
  set @Count = 0
  select @Weeks = round((datediff(dd, @FromDate, @ToDate) / 7.000) + .49, 0)
  

  
  if @Weeks = 0
    set @Weeks = 1
  
  while @Count < @Weeks
  begin
    select @Date = dateadd(wk, @Count, @FromDate)

    select @Count = @Count + 1
    
    insert @TableResult
          (Week,
           StartDate,
           EndDate,
           MovementDate,
           ProductCode,
 --          Opening,
           WarehouseId,
           StorageUnitId,
           SKUId)
    select @Count,
           dateadd(dd, (datepart(dw, @Date) * -1) + 2, @Date),
           dateadd(ss, 86399, dateadd(dd, 6, dateadd(dd, (datepart(dw, @Date) * -1) + 2, @Date))),
           MovementDate,
           ProductCode,
--           OpeningBalance,
           WarehouseId,
           StorageUnitId,
           SKUId
      from ProductMovement pm
     where pm.MovementDate between (dateadd(dd, (datepart(dw, @Date) * -1) + 2, @Date)) and (dateadd(ss, 86399, dateadd(dd, 6, dateadd(dd, (datepart(dw, @Date) * -1) + 2, @Date))))
     and   pm.ProductCode = isnull(@ProductCode,pm.ProductCode)
     and   pm.WarehouseId = @WarehouseId
  
  end
  
 update tr
     set Opening = (select min(OpeningBalance) from ProductMovement pm where pm.WarehouseId = @WarehouseId and pm.productcode = @productcode and pm.MovementDate between tr.StartDate and tr.EndDate)
    from @TableResult tr  
  
update tr
     set Received = (select sum(isnull(ReceivedQuantity,0)) from ProductMovement pm where pm.WarehouseId = @WarehouseId and pm.productcode = @productcode and pm.MovementDate between tr.StartDate and tr.EndDate)  
from @TableResult tr
where tr.MovementDate between @FromDate and @ToDate
and	  tr.ProductCode = @ProductCode
and	  tr.WarehouseId = @WarehouseId

  
  update tr
     set Issued = (select sum(isnull(IssuedQuantity,0)) from ProductMovement pm where pm.WarehouseId = @WarehouseId and pm.productcode = @productcode and pm.MovementDate between tr.StartDate and tr.EndDate) 
    from @TableResult tr
    where tr.MovementDate between @FromDate and @ToDate
    and	  tr.ProductCode = @ProductCode
    and	  tr.WarehouseId = @WarehouseId
    
 update tr
     set Balance = ((isnull(Opening,0) + isnull(Received,0)) - isnull(Issued,0)) 
    from @TableResult tr
    
 update tr
     set Variance = (isnull(Balance,0) - isnull(Issued,0)) 
    from @TableResult tr
    
SET ARITHABORT OFF
SET ANSI_WARNINGS OFF   

 update tr
     set Percentage = isnull((isnull(Variance,0) / isnull(Issued,0)*100),0)
    from @TableResult tr
    
  update tr
     set StockOnHand = isnull(((isnull(Received,0) + isnull(Opening,0)) / isnull(Issued,0)*100),0)
    from @TableResult tr   
    
      update tr
     set StockOnHand = 100
    from @TableResult tr 
    where isnull(tr.Received,0) = 0 and isnull(tr.Issued,0) = 0

	select  distinct tr.Week,
          tr.StartDate,
          tr.EndDate,
          tr.ProductCode,
		  tr.Opening,
		  isnull(tr.Received,0)as Received,
		  isnull(tr.Issued,0) as Issued,
		  tr.Balance,
		  tr.Variance,
		  tr.Percentage,
		  tr.StockOnHand,
		 --  'StockOnHand' = CASE
			--WHEN tr.Received = 0 and tr.Issued = 0 THEN 100
			--ELSE tr.StockOnHand
			--END,
		  tr.WarehouseId,
		  p.Product,
		  s.SKU,
		  @Weeks
    from @TableResult tr
    join Product p on tr.ProductCode = p.ProductCode
    join Sku     s on tr.SKUId = s.SKUId
    order by tr.productcode

end
