﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Variable_Weights_Update
  ///   Filename       : p_Variable_Weights_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Variable_Weights_Update
(
 @JobId              int,
 @StorageUnitId      int,
 @ConfirmedWeight    numeric(13,6),
 @StorageUnitBatchId int,
 @Quantity           float,
 @OperatorId         int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @PackageLineId     int,
          @IssueId           int,
          @IssueLineId       int,
          @StatusId          int,
          @PackagingWeight   numeric(13,6),
          @GrossWeight       float
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('IS','C')
  
  select @ConfirmedWeight = @ConfirmedWeight / SUM(i.ConfirmedQuantity)
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.JobId           = @JobId
     and sub.StorageUnitId = @StorageUnitId
  
  begin transaction
  
  update i
     set ConfirmedWeight = 0
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.JobId           = @JobId
     and sub.StorageUnitId = @StorageUnitId
     and i.ConfirmedQuantity <= 0
  
  update i
     set ConfirmedWeight = (@ConfirmedWeight * i.ConfirmedQuantity)
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.JobId           = @JobId
     and sub.StorageUnitId = @StorageUnitId
     and i.ConfirmedQuantity > 0
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  select @GrossWeight = sum(isnull(i.ConfirmedWeight,0))
    from Instruction i (nolock)
   where i.JobId = @JobId
  
  if @Error <> 0
    goto error
  
  delete IssueLinePackaging
   where JobId         = @jobId
     and StorageUnitId = @StorageUnitId
  select @@rowcount
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  select @PackageLineId = PackageLineId
    from IssueLinePackaging (nolock)
   where JobId              = @jobId
     and StorageUnitBatchId = @StorageUnitBatchId
     and StorageUnitId      = @StorageUnitId
  
  insert IssueLinePackaging
        (IssueId,
         IssueLineId,
         JobId,
         StorageUnitBatchId,
         StorageUnitId,
         StatusId,
         OperatorId,
         Quantity)
  select @IssueId,
         @IssueLineId,
         @JobId,
         @StorageUnitBatchId,
         @StorageUnitId,
         @StatusId,
         @operatorId,
         @quantity
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  declare @TableResult as table
  (
   PackType          nvarchar(50),
   StorageUnitId     int,
   PackagingQuantity numeric(13,6),
   PackagingWeight   numeric(13,6)
  )
  
  insert @TableResult
        (PackType,
         StorageUnitId,
         PackagingQuantity)
  select sku.SKUCode,
         ilp.StorageUnitId,
         sum(ilp.Quantity)
    from IssueLinePackaging ilp (nolock)
    join StorageUnitBatch     sub (nolock) on ilp.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit           su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    join Product                p (nolock) on su.ProductId           = p.ProductId
    join SKU                  sku (nolock) on su.SKUId               = sku.SKUId
   where ilp.JobId             = @JobId
  group by sku.SKUCode,
           ilp.StorageUnitId
  
  update tr
     set PackagingWeight = pk.TareWeight
    from Instruction            i (nolock)
    join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit           su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product                p (nolock) on su.ProductId          = p.ProductId
    join Pack                  pk (nolock) on su.StorageUnitId      = pk.StorageUnitId
    join PackType              pt (nolock) on pk.PackTypeId         = pt.PackTypeId
    join @TableResult          tr          on pt.PackType           = tr.PackType
                                          and su.StorageUnitId      = tr.StorageUnitId
   where i.JobId = @JobId
     and pk.TareWeight is not null
  
  update i
     set PackagingWeight   = tr.PackagingWeight,
         PackagingQuantity = tr.PackagingQuantity
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join @TableResult      tr          on sub.StorageUnitId    = tr.StorageUnitId
   where i.JobId           = @JobId
     and sub.StorageUnitId = @StorageUnitId
  
  select @PackagingWeight = sum(PackagingQuantity * PackagingWeight)
    from @TableResult
  
  update Job
     set Weight     = @GrossWeight,
         NettWeight = isnull(@GrossWeight,0) - isnull(TareWeight,0) - isnull(@PackagingWeight,0)
   where JobId = @JobId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  select @ConfirmedWeight = sum(convert(float, i.ConfirmedWeight)) / sum(convert(float, i.ConfirmedQuantity))
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.JobId           = @JobId
     and sub.StorageUnitId = @StorageUnitId
  
  select @PackagingWeight = sum(PackagingQuantity * PackagingWeight)
    from @TableResult
   where StorageUnitId = @StorageUnitId
  
  update i
     set NettWeight = (@ConfirmedWeight * i.ConfirmedQuantity) - @PackagingWeight
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.JobId           = @JobId
     and sub.StorageUnitId = @StorageUnitId
     and i.ConfirmedQuantity > 0
  
  update i
     set NettWeight = 0
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.JobId           = @JobId
     and sub.StorageUnitId = @StorageUnitId
     and i.ConfirmedQuantity <= 0
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Variable_Weights_Update'); 
    rollback transaction
    return @Error
end
