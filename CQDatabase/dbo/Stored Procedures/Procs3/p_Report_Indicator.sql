﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Indicator
  ///   Filename       : p_Report_Indicator.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Indicator
(
 @WarehouseId int,
 @IndicatorId int = -1
)

as
begin
	 set nocount on;
  
	if (@IndicatorId = -1)
		Begin
			Set @IndicatorId = null
		end

  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if datediff(mi, (select min(ModifiedDate) from Indicator), @Getdate) > 30
    exec p_Report_Indicator_Update
  
  select IndicatorId,
		       Header,
		       Indicator,
		       IndicatorValue,
		       IndicatorDisplay,
		       case when IndicatorValue <= ThresholdGreen then 'LimeGreen'
			           when IndicatorValue <= ThresholdYellow then 'Yellow'
			           when IndicatorValue >= ThresholdYellow then 'Red'
		       else 'Blue' end as Colour,
		            ModifiedDate,
		       convert(int, null) as 'Pos'
		  into #temp
	   from Indicator (nolock)
	Where IndicatorId = isnull(@IndicatorId,IndicatorId)
  order by Header,Indicator desc
  
  declare @count int
  
  set @count = 0
  
  while exists(select top 1 1 from #temp where Pos is null)
  begin
    set @count = @count + 1
    
    update #temp
       set Pos = @count
      where Header = convert(nvarchar(50), (select min(header) from #temp where Pos is null))
    
    if @count = 4
      set @count = 0
  end
  
  select * from #temp order by Header,IndicatorId
end
