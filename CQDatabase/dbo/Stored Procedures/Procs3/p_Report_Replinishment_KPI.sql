﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Replinishment_KPI
  ///   Filename       : p_Report_Replinishment_KPI.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 11 Nov 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Junaid Desai	
  ///   Modified Date  : 13 Oct 2008
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Replinishment_KPI
(
		@FromDate			datetime,
		@ToDate				datetime,
		@OperatorGroupId		int
)

as
begin
	 set nocount on;
	

Declare @DateHoursDiff		int,
		@DateDaysDiff		int,
		@DateMonthsDiff		int

Set @DateHoursDiff = DateDiff(hh,@FromDate,@ToDate)
Set @DateDaysDiff = DateDiff(D,@FromDate,@ToDate)
Set @DateMonthsDiff = DateDiff(M,@FromDate,@ToDate) 

Declare @ReplinishmentTempTable as Table 
(
	Replinishes					int,
	InstructionTypeId			int,
	--InstructionType			nvarchar(30),
	OperatorGroupID				int,
	AverageTimeToAccept			int,
	AverageTimeToComplete		int,
	AcceptToComplete			int,
	EndDate						DateTime,
	ReplinishmentTarget			Decimal(10,3)
)


insert into @ReplinishmentTempTable 
(Replinishes,
InstructionTypeId,
OperatorGroupID,
AverageTimeToAccept,
AverageTimeToComplete,
AcceptToComplete,
EndDate,
ReplinishmentTarget)

(
Select Count(InstructionId) as NumberOfReplinishments,
InstructionTypeID,OperatorGroupId,
--Sum(TimeToAccept)/Count(InstructionId)  as AverageTimeToAccept,
--Sum(TimeToComplete)/Count(InstructionId) as AverageTimeToComplete,
--Sum(AcceptToComplete)/Count(InstructionId) as AcceptToComplete,
Avg(TimeToAccept)  as AverageTimeToAccept,
Avg(TimeToComplete) as AverageTimeToComplete,
Avg(AcceptToComplete) as AcceptToComplete,
convert(nvarchar(14), Enddate, 120) + '00:00',
Sum(Distinct(ReplinishmentTarget))  From 
(
		SELECT     I.InstructionTypeID, I.InstructionId  ,O.OperatorGroupId ,
					DateDiff(ss,I.CreateDate, I.EndDate) As TimeToComplete ,I.Enddate,DateDiff(ss,I.CreateDate, I.StartDate) As TimeToAccept,DateDiff(ss,I.StartDate, I.EndDate) As AcceptToComplete, O.OperatorId
		,KT.KPITargetValue  as  ReplinishmentTarget
		--Instruction.CreateDate, Instruction.StartDate, Instruction.EndDate, 
		                      
		FROM         Instruction I INNER JOIN
							 -- InstructionType IT ON I.InstructionTypeId = IT.InstructionTypeId INNER JOIN
							  Operator O ON I.OperatorId = O.OperatorId
							   join KPITarget KT on O.OperatorGroupId = KT.OperatorGroupID		
		WHERE     (I.InstructionTypeId = 2)
		And I.EndDate Between @FromDate And @ToDate
		And O.OperatorGroupId = @OperatorGroupId 
		and KT.KPITargetCode = 'RT'
) X
Group By OperatorGroupId,EndDate,InstructionTypeID 
)


if (@DateMonthsDiff < 3)
	Begin
		if (@DateDaysDiff < 8)		
			Begin
				if (@DateHoursDiff < 25)
					Begin
						--Select 'x < 24 Hours' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
							Select
							Sum(RT.Replinishes) as Replinishes,
							--RT.InstructionTypeId,IT.InstructionType,
							RT.OperatorGroupId,
							OG.OperatorGroup,
							Convert(nvarchar,RT.EndDate,108) as EndDate, 
							Convert(nvarchar,RT.EndDate,108) as ordscale, 
							Avg(AverageTimeToComplete) as TimeToComplete,
							Avg(AverageTimeToAccept) as TimeToAccept,
							Avg(AcceptToComplete) as TimeComplete,
							Sum(Distinct(ReplinishmentTarget)) as ReplinishmentTarget,
							convert(nvarchar,(Avg(AverageTimeToComplete) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AverageTimeToComplete) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AverageTimeToComplete) % 60))  as AverageTimeToComplete,
							convert(nvarchar,(Avg(AverageTimeToAccept) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AverageTimeToAccept) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AverageTimeToAccept) % 60))  as AverageTimeToAccept,
							convert(nvarchar,(Avg(AcceptToComplete) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AcceptToComplete) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AcceptToComplete) % 60)) as AcceptToComplete

							From @ReplinishmentTempTable RT
							Join InstructionType IT on RT.InstructionTypeId = IT.InstructionTypeId
							join OperatorGroup OG	on RT.OperatorGroupId		= OG.OperatorGroupId
							Group By RT.InstructionTypeId,IT.InstructionType,
							RT.OperatorGroupId,OG.OperatorGroup,
							Convert(nvarchar,RT.EndDate,108)
							Order by ordscale

			-- Select data by this Scale
					End
				Else
					Begin
					--Select '24 Hours < x < 1 week' as scale,@DateHoursDiff as DateDiffHourTime,@DateDaysDiff as DateDiffDayTime,@DateMonthsDiff as DateDiffMonthTime
							Select
							Sum(RT.Replinishes) as Replinishes,
							--RT.InstructionTypeId,IT.InstructionType,
							RT.OperatorGroupId,
							OG.OperatorGroup,
									Convert(nvarchar,RT.EndDate,101) as EndDate, 
									Convert(nvarchar,RT.EndDate,101) as ordscale ,
							Avg(AverageTimeToComplete) as TimeToComplete,
							Avg(AverageTimeToAccept) as TimeToAccept,
							Avg(AcceptToComplete) as TimeComplete,
							8 * Sum(Distinct(ReplinishmentTarget)) as ReplinishmentTarget,
							convert(nvarchar,(Avg(AverageTimeToComplete) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AverageTimeToComplete) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AverageTimeToComplete) % 60))  as AverageTimeToComplete,
							convert(nvarchar,(Avg(AverageTimeToAccept) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AverageTimeToAccept) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AverageTimeToAccept) % 60)) as AverageTimeToAccept,
							convert(nvarchar,(Avg(AcceptToComplete) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AcceptToComplete) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AcceptToComplete) % 60))  as AcceptToComplete

							From @ReplinishmentTempTable RT
							Join InstructionType IT on RT.InstructionTypeId = IT.InstructionTypeId
							join OperatorGroup OG	on RT.OperatorGroupId		= OG.OperatorGroupId
							Group By RT.InstructionTypeId,IT.InstructionType,
							RT.OperatorGroupId,OG.OperatorGroup,
							Convert(nvarchar,RT.EndDate,101)
							Order By ordscale
					End
			End
		Else
				Begin
			
							Select
							Sum(RT.Replinishes) as Replinishes,
							--RT.InstructionTypeId,IT.InstructionType,
							RT.OperatorGroupId,
							OG.OperatorGroup,
									DatePart(wk,RT.EndDate) as EndDate, 
									DatePart(wk,RT.EndDate) as ordscale , 
							Avg(AverageTimeToComplete) as TimeToComplete,
							Avg(AverageTimeToAccept) as TimeToAccept,
							Avg(AcceptToComplete) as TimeComplete,
							5 * 8 * Sum(Distinct(ReplinishmentTarget)) as ReplinishmentTarget,
							convert(nvarchar,(Avg(AverageTimeToComplete) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AverageTimeToComplete) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AverageTimeToComplete) % 60))  as AverageTimeToComplete,
							convert(nvarchar,(Avg(AverageTimeToAccept) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AverageTimeToAccept) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AverageTimeToAccept) % 60))  as AverageTimeToAccept,
							convert(nvarchar,(Avg(AcceptToComplete) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AcceptToComplete) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AcceptToComplete) % 60))  as AcceptToComplete

							From @ReplinishmentTempTable RT
							Join InstructionType IT on RT.InstructionTypeId = IT.InstructionTypeId
							join OperatorGroup OG	on RT.OperatorGroupId		= OG.OperatorGroupId
							Group By RT.InstructionTypeId,IT.InstructionType,
							RT.OperatorGroupId,OG.OperatorGroup,
							DatePart(wk,RT.EndDate) 
							Order By ordscale 	
			
			End
	End
Else
	Begin
	
							Select
							Sum(RT.Replinishes) as Replinishes,
							--RT.InstructionTypeId,IT.InstructionType,
							RT.OperatorGroupId,
							OG.OperatorGroup,
									DateName(M,RT.EndDate) as EndDate, 
									DatePart(M,RT.EndDate) as ordscale, 
							Avg(AverageTimeToComplete) as TimeToComplete,
							Avg(AverageTimeToAccept) as TimeToAccept,
							Avg(AcceptToComplete) as TimeComplete,
							22 * 8 * Sum(Distinct(ReplinishmentTarget)) as ReplinishmentTarget,
							convert(nvarchar,(Avg(AverageTimeToComplete) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AverageTimeToComplete) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AverageTimeToComplete) % 60))  as AverageTimeToComplete,
							convert(nvarchar,(Avg(AverageTimeToAccept) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AverageTimeToAccept) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AverageTimeToAccept) % 60))  as AverageTimeToAccept,
							convert(nvarchar,(Avg(AcceptToComplete) / 3600)) + ' : ' + Convert(nvarchar,((Avg(AcceptToComplete) % 3600)/60)) + ' : ' + Convert(nvarchar,(Avg(AcceptToComplete) % 60))  as AcceptToComplete
							From @ReplinishmentTempTable RT
							Join InstructionType IT on RT.InstructionTypeId = IT.InstructionTypeId
							join OperatorGroup OG	on RT.OperatorGroupId		= OG.OperatorGroupId
							Group By RT.InstructionTypeId,IT.InstructionType,
							RT.OperatorGroupId,OG.OperatorGroup,
							DateName(M,RT.EndDate),DatePart(M,RT.EndDate)
							Order by ordscale

	End




--Select @startDate,@enddate

end
