﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Static_Batch_Insert
  ///   Filename       : dbo.p_Static_Batch_Insert.sql
  ///   Create By      : Junaid Desai
  ///   Date Created   : 11 February 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Static_Batch_Insert
(
 @BatchId              int = null output
,@StorageUnitId        int = null
,@OperatorId           int = null
,@StatusId             int = null
,@WarehouseId          int = null
,@Batch                nvarchar(50) = null
,@BatchReferenceNumber nvarchar(50) = null
,@ProductionDateTime   datetime = null
,@ExpiryDate           datetime = null
,@IncubationDays       int = null
,@ShelfLifeDays        int = null
,@ExpectedYield        int = null
,@ActualYield          int = null
,@FilledYield          int = null
,@ECLNumber            nvarchar(10) = null
,@COACertificate       nvarchar(5) = null
,@StorageUnitBatchId   int = null output
,@ReceiptId            int = null
,@RI                   float = null
,@Density              float = null
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @ExternalCompanyId  int
  
  select @GetDate = dbo.ufn_Getdate()
	 
	 set @BatchId = null;
	 
	 set @StatusId = null
	 
	 if @ReceiptId = -1
	   set @ReceiptId = null
	 
	 select @BatchId = BatchId from Batch where Batch = @Batch
	 
	 if @ReceiptId is not null
	 begin
	   select @ExternalCompanyId = id.ExternalCompanyId,
	          @WarehouseId       = id.WarehouseId
	     from Receipt          r (nolock)
	     join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
	    where r.ReceiptId = @ReceiptId
  	 
	   if (select DefaultQC
	         from StorageUnitExternalCompany (nolock)
	        where StorageUnitId     = @StorageUnitId
	          and ExternalCompanyId = @ExternalCompanyId) = 1
	   begin
	     select @StatusId = StatusId
	       from Status (nolock)
	      where Type       = 'B'
	        and StatusCode = 'QC'
	   end
	 end
	 
	 if @StatusId is null
	 begin
	   select @StatusId = StatusId
	     from Status        s (nolock)
	     join Configuration c (nolock) on s.StatusCode = convert(nvarchar(10), c.Value)
	    where s.Type = 'B'
	      and c.ConfigurationId = 127
	      and c.WarehouseId = @WarehouseId
	 end
	 
	 if datediff(dd, @ExpiryDate, getdate()) = 0
	 begin
	   select @ShelfLifeDays = p.ShelfLifeDays
	     from StorageUnit su
	     join Product      p on su.ProductId = p.ProductId
	    where su.StorageUnitId = @StorageUnitId
	   
	   select @ExpiryDate = dateadd(dd, @ShelfLifeDays, @ExpiryDate)
	 end
	 
	 if @BatchId is null
	 begin
    INSERT [Batch]
           ([StatusId]
           ,[WarehouseId]
           ,[Batch]
           ,[BatchReferenceNumber]
           ,[ProductionDateTime]
           ,[CreateDate]
           ,[ExpiryDate]
           ,[IncubationDays]
           ,[ShelfLifeDays]
           ,[ExpectedYield]
           ,[ActualYield]
           ,[FilledYield]
           ,[ECLNumber]
           --,[IrriadiationDate]
           --,[ProductTemp]
           ,[COACertificate]
           ,RI
           ,Density)
     select @StatusId
           ,@WarehouseId
           ,@Batch
           ,@BatchReferenceNumber
           ,@ProductionDateTime
           ,@GetDate
           ,@ExpiryDate
           ,@IncubationDays
           ,@ShelfLifeDays
           ,@ExpectedYield
           ,@ActualYield
           ,@FilledYield
           ,@ECLNumber
           ,@COACertificate
           ,@RI
           ,@Density
    
    Select @BatchId=@@IDENTITY
  end
  
  select @StorageUnitBatchId = StorageUnitBatchId from StorageUnitBatch where BatchId = @BatchId and StorageUnitId = @StorageUnitId
  
  if @StorageUnitBatchId is null
  begin
     exec @Error = p_StorageUnitBatch_Insert
      @StorageUnitBatchId output
     ,@BatchId
     ,@StorageUnitId
  end
end
