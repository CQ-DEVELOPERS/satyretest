﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Principal_Setup
  ///   Filename       : p_Report_Principal_Setup.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Mar 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Principal_Setup
(
 @WarehouseId int = null,
 @OperatorId int = null
)

as
begin
  set nocount on;
  
  select il.InterfaceLoginId,
         il.DatabaseName,
         il.PrincipalCode,
         p.Principal,
         il.Email,
         il.FirstName,
         il.LastName,
         'http://run.cquential.com/WebserviceCq' as 'WebServiceURL',
         il.InterfaceUsername,
         il.InterfacePassword,
         replace('ftp://run.cquential.com/' + 'Test/ClientIntegration/' + il.DatabaseName + '/' + il.PrincipalCode, ' ','%20') as 'FTPURL',
         il.FTPUsername,
         il.FTPPassword,
         il.InsertDate,
         il.CreateDate,
         il.IsSetup,
         case when IsSetup = 1
              then 'Your instance has succesffully been setup and is ready to use, details are below.'
              else 'Unfortunately there seems to have been an error and we could not create your instance, please contact us to resolve'
              end as 'Message'
    from CQCommon.dbo.InterfaceLogin il (nolock)
    join Principal                    p (nolock) on il.PrincipalCode = p.PrincipalCode
                                                and il.DatabaseName = DB_NAME()
end
