﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StorageUnitBatchLocation_Parameter
  ///   Filename       : p_StorageUnitBatchLocation_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:21:04
  /// </summary>
  /// <remarks>
  ///   Selects rows from the StorageUnitBatchLocation table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   StorageUnitBatchLocation.StorageUnitBatchId,
  ///   StorageUnitBatchLocation.LocationId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StorageUnitBatchLocation_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as StorageUnitBatchId
        ,null as 'StorageUnitBatchLocation'
        ,null as LocationId
        ,null as 'StorageUnitBatchLocation'
  union
  select
         StorageUnitBatchLocation.StorageUnitBatchId
        ,StorageUnitBatchLocation.StorageUnitBatchId as 'StorageUnitBatchLocation'
        ,StorageUnitBatchLocation.LocationId
        ,StorageUnitBatchLocation.LocationId as 'StorageUnitBatchLocation'
    from StorageUnitBatchLocation
  
end
