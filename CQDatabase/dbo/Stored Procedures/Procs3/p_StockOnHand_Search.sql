﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockOnHand_Search
  ///   Filename       : p_StockOnHand_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007 11:54:26
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null output,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(30) = null,
  ///   @Product nvarchar(50) = null,
  ///   @Barcode nvarchar(50) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   p.ProductId,
  ///   p.StatusId,
  ///   s.Status,
  ///   p.ProductCode,
  ///   p.Product,
  ///   p.Barcode,
  ///   p.MinimumQuantity,
  ///   p.ReorderQuantity,
  ///   p.MaximumQuantity,
  ///   p.CuringPeriodDays,
  ///   p.ShelfLifeDays,
  ///   p.QualityAssuranceIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockOnHand_Search
(
 @WarehouseId int,
 @ProductCode nvarchar(30) = null,
 @Product     nvarchar(50) = null,
 @SKUCode     nvarchar(50) = null,
 @SKU         nvarchar(50) = null,
 @Batch       nvarchar(50) = null,
 @FromLocation nvarchar(15) = null,
 @ToLocation nvarchar(15) = null
)

as
begin
	 set nocount on;
  
  if @ProductCode in ('-1','')
    set @ProductCode = null;
  
  if @Product in ('-1','')
    set @Product = null;
  
  if @SKUCode in ('-1','')
    set @SKUCode = null;
  
  if @SKU in ('-1','')
    set @SKU = null;
  
  if @Batch in ('-1','')
    set @Batch = null;
  
  if @FromLocation in ('-1','')
    set @FromLocation = null;
  
  if @ToLocation in ('-1','')
    set @ToLocation = null;
  
  select subl.StorageUnitBatchId,
         vl.LocationId,
         vl.Area,
         vl.Location,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         vs.SKU,
         vs.Batch,
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity
    from viewStock                  vs
    join StorageUnitBatchLocation subl on vs.StorageUnitBatchId = subl.StorageUnitBatchId
    join viewLocation               vl on subl.LocationId       = vl.LocationId
   where vl.WarehouseId = @WarehouseId
     and isnull(vs.ProductCode,'%') like '%' + isnull(@ProductCode, vs.ProductCode) + '%'
     and isnull(vs.Product,'%')     like '%' + isnull(@Product, vs.Product) + '%'
     and isnull(vs.SKUCode,'%')   like '%' + isnull(@SKUCode, vs.SKUCode) + '%'
     and isnull(vs.SKU,'%')       like '%' + isnull(@SKU, vs.SKU) + '%'
     and isnull(vs.Batch,'%')       like '%' + isnull(@Batch, vs.Batch) + '%'
     and vl.Location between isnull(@FromLocation, vl.Location) and isnull(@ToLocation, vl.Location)
--     and vl.AreaCode in ('RK','BK','SP','R','PK')
     and vl.StockOnHand = 1
  order by vl.Location,
           vs.ProductCode,
           vs.SKUCode,
           vs.Batch
end
