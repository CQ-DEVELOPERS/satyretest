﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Picker_Performace_Cumulative
  ///   Filename       : p_Report_Picker_Performace_Cumulative.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jul 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Picker_Performace_Cumulative
(
 @WarehouseId     int,
 @OperatorGroupCode nvarchar(10),
 @FromDate        datetime,
 @ToDate          datetime
)

as
begin
	 set nocount on;
	 
	 declare @OperatorId int,
          @EndDate    datetime
	 
	 declare @TableResult as table
	 (
	  OperatorGroupId   int,
   OperatorGroup     nvarchar(50),
   OperatorGroupCode nvarchar(10),
   OperatorId        int,
   Operator          nvarchar(50),
   EndDate           datetime,
   EndTime           nvarchar(5),
   Units             float,
   CumulativeUnits   float,
   Weight            float,
   CumulativeWeight  float,
	  TargetUnits    float,
      TargetWeight   float,
	  CumulativeUnitsTarget       float,
      CumulativeWeightTarget      float
	 )
	 
	 if @OperatorGroupCode = '-1'	   
	    set @OperatorGroupCode = ''
  
  insert @TableResult
        (OperatorGroupId,
         OperatorGroup,
         OperatorGroupCode,
         OperatorId,
         Operator,
         EndDate,
         EndTime,
         Units,
         Weight,
	        TargetUnits,
         TargetWeight)
  select og.OperatorGroupId,
         og.OperatorGroup,
         og.OperatorGroupCode,   
         o.OperatorId,
         o.Operator,
         convert(nvarchar, op.EndDate, 113),
         convert(nvarchar(5), op.EndDate, 108),
         op.Units,
         op.Weight,
	     gp.Units,
         gp.Weight
    from Operator             o
    join OperatorGroup       og on o.OperatorGroupId = og.OperatorGroupId
    join OperatorPerformance op on o.OperatorId = op.OperatorId
	   join GroupPerformance gp on og.OperatorGroupId = gp.OperatorGroupId
   where og.OperatorGroupCode = isnull(@OperatorGroupCode, og.OperatorGroupCode)
     and op.EndDate between @FromDate and @ToDate
  
  set rowcount 1
  
  while exists (select 1 from @TableResult where CumulativeUnits is null)
  begin
    select top 1
           @OperatorId = OperatorId,
           @EndDate    = EndDate
      from @TableResult
     where CumulativeUnits is null
    order by OperatorId,
             EndDate
    
    update @TableResult
       set CumulativeUnits = isnull(Units,0) + isnull((select max(CumulativeUnits) from @TableResult where OperatorId = @OperatorId),0),
           CumulativeWeight = isnull(Weight,0) + isnull((select max(CumulativeWeight) from @TableResult where OperatorId = @OperatorId),0),
	          CumulativeUnitsTarget = isnull(TargetUnits,0) + isnull((select max(CumulativeUnitsTarget) from @TableResult where OperatorId = @OperatorId),0),
           CumulativeWeightTarget = isnull(TargetWeight,0) + isnull((select max(CumulativeWeightTarget) from @TableResult where OperatorId = @OperatorId),0)
     where OperatorId = @OperatorId
       and EndDate    = @EndDate
       and CumulativeUnits is null
  
  insert @TableResult
        (OperatorGroupId,
         OperatorGroup,
         OperatorGroupCode,
         EndDate,
         EndTime,
         CumulativeUnits,
         CumulativeUnitsTarget,
         CumulativeWeightTarget)
  select og.OperatorGroupId,
         og.OperatorGroup,
         og.OperatorGroupCode,
         convert(nvarchar, @EndDate, 113),
         convert(nvarchar(5), @EndDate, 108),
         0,
         (gp.Units  / isnull((select count(distinct(EndDate)) from @TableResult tr where og.OperatorGroupId = tr.OperatorGroupId and tr.OperatorId is not null),0)) + isnull((select max(CumulativeUnitsTarget) from @TableResult tr where og.OperatorGroupId = tr.OperatorGroupId and tr.OperatorId is null),0),
         (gp.Weight / isnull((select count(distinct(EndDate)) from @TableResult tr where og.OperatorGroupId = tr.OperatorGroupId and tr.OperatorId is not null),0)) + isnull((select max(CumulativeWeightTarget) from @TableResult tr where og.OperatorGroupId = tr.OperatorGroupId and tr.OperatorId is null),0)
    from OperatorGroup    og
    join GroupPerformance gp on og.OperatorGroupId = gp.OperatorGroupId
   where og.OperatorGroupCode = isnull(@OperatorGroupCode, og.OperatorGroupCode)
	 And  (isnull(og.OperatorGroupCode,' ') = ' ')
     and not exists (select 1 from @TableResult where EndDate = @EndDate and OperatorId is null)
  end
  
  set rowcount 0
  
  select tr.OperatorGroup,
	        tr.EndDate,
	        tr.EndTime,
	        tr.Operator,
	        tr.Units,
	        tr.CumulativeUnits,
	        tr.Weight,
	        tr.CumulativeWeight,
	        tr.CumulativeUnitsTarget,
         tr.CumulativeWeightTarget
	   from @TableResult     tr
	  order by OperatorGroup,
            EndDate,
            Operator
end
