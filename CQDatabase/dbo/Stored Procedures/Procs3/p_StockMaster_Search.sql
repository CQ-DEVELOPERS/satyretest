﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_StockMaster_Search
  ///   Filename       : p_StockMaster_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:42
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the StockMaster table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   StockMaster.SKUCode,
  ///   StockMaster.ProductCode,
  ///   StockMaster.ParentSKUCode,
  ///   StockMaster.ParentProductCode,
  ///   StockMaster.Product,
  ///   StockMaster.Barcode,
  ///   StockMaster.WarehouseCode,
  ///   StockMaster.PackTypeId,
  ///   StockMaster.PackType,
  ///   StockMaster.OutboundSequence,
  ///   StockMaster.Quantity,
  ///   StockMaster.Weight,
  ///   StockMaster.Volume 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_StockMaster_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         StockMaster.SKUCode
        ,StockMaster.ProductCode
        ,StockMaster.ParentSKUCode
        ,StockMaster.ParentProductCode
        ,StockMaster.Product
        ,StockMaster.Barcode
        ,StockMaster.WarehouseCode
        ,StockMaster.PackTypeId
        ,StockMaster.PackType
        ,StockMaster.OutboundSequence
        ,StockMaster.Quantity
        ,StockMaster.Weight
        ,StockMaster.Volume
    from StockMaster
  
end
