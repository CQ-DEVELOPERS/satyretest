﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PricingCategory_Delete
  ///   Filename       : p_PricingCategory_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:51:23
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the PricingCategory table.
  /// </remarks>
  /// <param>
  ///   @PricingCategoryId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PricingCategory_Delete
(
 @PricingCategoryId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete PricingCategory
     where PricingCategoryId = @PricingCategoryId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
