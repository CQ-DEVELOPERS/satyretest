﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Store_Mixed
  ///   Filename       : p_Mobile_Store_Mixed.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Mobile_Store_Mixed
(
 @operatorId          int,
 @barcode             nvarchar(30),
 @instructionId       int output
)
as
begin
	 set nocount on
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @Rowcount           int,
          @StatusId           int,
          @JobId              int,
          @PalletId           int,
          @StoreLocationId    int,
          @InstructionTypeCode nvarchar(10),
          @WarehouseId        int,
          @PalletStatusId	  int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if isnumeric(@barcode) = 1
    set @barcode = 'J:' + @barcode
  
  if isnumeric(replace(@barcode,'J:','')) = 1
  begin
    -- First Check if Inter Branch Transfer Putaway
    select @JobId = max(JobId)
      from Job (nolock)
     where ReferenceNumber = @barcode
    
    if @JobId is null
      select @JobId   = replace(@barcode,'J:',''),
             @barcode = null
  end
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @PalletId is not null
  begin
  select @PalletStatusId = p.StatusId
    from Pallet p
   where p.PalletId = @palletId
  
  if @PalletStatusId = dbo.ufn_StatusId('P','NA')
  begin
    begin transaction
	   set @InstructionId = -4
	   goto error
	 end
  end
	
    select @JobId = JobId
      from Instruction      i (nolock)
      join Status           s (nolock) on i.StatusId = s.StatusId
                                      and s.StatusCode in ('W','S')
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where it.InstructionTypeCode in ('S','SM','PR')
       and i.PalletId = @PalletId
  
  if @@ROWCOUNT = 0
    select @JobId = JobId
      from Instruction      i (nolock)
      join Status           s (nolock) on i.StatusId = s.StatusId
                                      and s.StatusCode in ('W','S')
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where it.InstructionTypeCode in ('M')
       and i.PalletId = @PalletId
  
  update Instruction set AutoComplete = 0 where JobId = @JobId
  if @barcode is not null
  begin
    select @JobId = max(JobId)
      from Job    j (nolock)
      join Status s (nolock) on j.StatusId = s.StatusId
     where j.ReferenceNumber = @barcode
       and s.StatusCode not in ('C','F')
    
    select @Rowcount = @@rowcount
    
    if @@rowcount = 0
      select @JobId = max(JobId)
        from Job    j (nolock)
       where j.ReferenceNumber = @barcode
  end
  
  begin transaction
  
  select top 1
         @jobId              = j.JobId,
         @instructionId      = i.InstructionId,
         @StoreLocationId = i.StoreLocationId,
         @InstructionTypeCode = it.InstructionTypeCode,
         @WarehouseId         = i.WarehouseId
    from Job              j (nolock)
    join Status           s (nolock) on j.StatusId          = s.StatusId
    join Instruction      i (nolock) on j.JobId             = i.JobId
    join Status          si (nolock) on i.StatusId          = si.StatusId
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where isnull(j.OperatorId, @operatorId) = @operatorId
     and j.JobId                = @jobId
     and si.StatusCode            in ('W','S')
     and ((it.InstructionTypeCode = 'SM' and s.StatusCode in ('PR','A','S'))
     or   (it.InstructionTypeCode = 'PR' and s.StatusCode in ('PR','A','S'))
     or   (it.InstructionTypeCode = 'M' and s.StatusCode in ('S','W')))
--     and isnull(i.PalletId,0)   = isnull(@PalletId,0)
  
  select @Rowcount = @@rowcount
  
  if @Rowcount = 0
  begin
    set @InstructionId = -1
    set @Error = 2
    goto error
  end
  
  if @StoreLocationId is null and @InstructionTypeCode != 'M' and dbo.ufn_Configuration(468, @WarehouseId) = 0
  begin
    exec @Error = p_Receiving_Manual_Palletisation_Auto_Locations
     @InstructionId = @InstructionId
    
    if @Error <> 0
      goto error
  end
  
  select @StatusId = dbo.ufn_StatusId('I','S')
  
  exec @Error = p_Job_Update
   @JobId      = @JobId,
   @StatusId   = @StatusId,
   @OperatorId = @operatorId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @StatusId      = @StatusId,
   @OperatorId    = @OperatorId,
   @StartDate     = @GetDate
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Operator_Update
   @OperatorId = @OperatorId,
   @LastInstruction      = @GetDate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    rollback transaction
    return @Error
--      select @PickAreaId = AreaId
--        from AreaLocation
--       where LocationId = @PickLocationId
--      
--      select @StoreAreaId = AreaId
--        from AreaLocation
--       where LocationId = @storeLocationId
--      
--      set @OperatorGroupId = dbo.ufn_OperatorGroupId(@operatorId) 
--      
--      select top 1
--             @SecurityCode  = l.SecurityCode,
--             @storeLocation = l.Location,
--             @NewStoreLoactionId = l.LocationId
--        from MovementSequence ms (nolock)
--        join AreaLocation     al (nolock) on ms.StageAreaId = al.AreaId
--        join Location          l (nolock) on al.LocationId  = l.LocationId
--       where PickAreaId      = @PickAreaId
--         and StoreAreaId     = @StoreAreaId
--         and OperatorGroupId = @OperatorGroupId
--      
--      set @StatusId = dbo.ufn_StatusId('I','W')
--      
--      exec @Error = p_Instruction_Insert
--       @InstructionTypeId   = @InstructionTypeId,
--       @StorageUnitBatchId  = @StorageUnitBatchId,
--       @WarehouseId         = @WarehouseId,
--       @StatusId            = @StatusId,
--       @jobId               = @jobId,
--       @operatorId          = @operatorId,
--       @PickLocationId      = @PickLocationId,
--       @storeLocationId     = @storeLocationId,
--       @InstructionRefId    = @InstructionId,
--       @Quantity            = @confirmedQuantity,
--       @confirmedQuantity   = 0,
--       @Weight              = @Weight,
--       @ConfirmedWeight     = 0,
--       @PalletId            = @PalletId,
--       @CreateDate          = @GetDate
--      
--      if @Error <> 0
--      begin
--        set @Error = 1
--        goto Result
--      end
--      
--      exec @Error = p_Instruction_Update
--       @InstructionId = @InstructionId,
--       @storeLocationId = @NewStoreLoactionId
--      
--      if @Error <> 0
--      begin
--        set @Error = 1
--        goto Result
--      end
end
