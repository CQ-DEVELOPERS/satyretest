﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Customer_Returns_by_Reason_SalesRep
  ///   Filename       : p_Report_Customer_Returns_by_Reason_SalesRep.sql
  ///   Create By      : Karen
  ///   Date Created   : Aug 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Customer_Returns_by_Reason_SalesRep
(
 @ReasonId			int = null,
 @ContactListId     int = null,
 @FromDate			datetime,
 @ToDate			datetime
)

as
begin
	 set nocount on;
  
  declare @TableHeader as Table
  (
   InboundShipmentId	int,
   ReceiptId			int,
   OrderNumber			nvarchar(30),
   ExternalCompanyId	int,
   Supplier				nvarchar(255),
   SupplierCode			nvarchar(30),
   DeliveryNoteNumber	nvarchar(30),
   DeliveryDate			datetime,   
   SealNumber			nvarchar(30),
   VehicleRegistration	nvarchar(10),
   HeaderReason			nvarchar(50),
   ContactPerson		nvarchar(50),
   CreateDate			datetime
   
   
  );
  
  declare @TableDetails as Table
  (
   ReceiptId          int,
   ReceiptLineId      int,
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(255),
   SKUCode            nvarchar(50),
   ExpectedQuantity   numeric(13,6),
   AcceptedQuantity   numeric(13,6),
   RejectQuantity     numeric(13,6),
   ReasonId           int,
   LineReason         nvarchar(50),
   Exception          nvarchar(255),
   Batch              nvarchar(50),
   ExpiryDate         datetime,
   SampleQty		  numeric(13,6),
   OperatorCode		  nvarchar(10)
  );
  
  if @ReasonId = -1
    set @ReasonId = null
  
  if @ContactListId = -1
    set @ContactListId = null
  

    insert @TableHeader
          (ReceiptId,
           OrderNumber,
           ExternalCompanyId,
           DeliveryNoteNumber,
           DeliveryDate,
           SealNumber,
           VehicleRegistration,
           HeaderReason,
           ContactPerson,
           CreateDate)
    select r.ReceiptId,
           id.OrderNumber,
           id.ExternalCompanyId,
           r.DeliveryNoteNumber,           
           r.DeliveryDate,
           r.SealNumber,
           r.VehicleRegistration,
           re.Reason,
           cl.ContactPerson,
           id.CreateDate
      --from InboundShipmentReceipt isr
      from Receipt                  r --(nolock) on isr.ReceiptId = r.ReceiptId
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
      join InboundDocumentType	  idt (nolock) on idt.InboundDocumentTypeId = id.InboundDocumentTypeId
      join InboundDocumentContactList idc (nolock) on idc.InboundDocumentId = id.InboundDocumentId
      join Reason				   re (nolock) on re.ReasonId = id.ReasonId
      join ContactList			   cl (nolock) on cl.ContactListId = idc.ContactListId
     where (id.ReasonId = isnull(@ReasonId, id.ReasonId)
     or	   idc.ContactListId = ISNULL(@ContactListId, idc.ContactListId))
     and   idt.InboundDocumentTypeCode = 'RET'	
     and   isnull(id.DeliveryDate, id.CreateDate) between @FromDate and @ToDate   

  
  insert @TableDetails
        (ReceiptId,
         ReceiptLineId,
         StorageUnitBatchId,
         ExpectedQuantity,
         AcceptedQuantity,
         RejectQuantity,
         SampleQty,
         OperatorCode,
         LineReason)
  select rl.ReceiptId,
         rl.ReceiptLineId,
         rl.StorageUnitBatchId,
         rl.RequiredQuantity,
         rl.AcceptedQuantity,
         rl.RejectQuantity,
         rl.SampleQuantity,
         o.OperatorCode,
         re.Reason
    from @TableHeader th
    join ReceiptLine  rl on th.ReceiptId     = rl.ReceiptId
    join InboundLine  il on rl.InboundLineId = il.InboundLineId
    join Reason		  re (nolock) on il.ReasonId = re.ReasonId
    left join Operator     o  on rl.OperatorId    = o.OperatorId
  
  update th
     set Supplier     = ec.ExternalCompany,
         SupplierCode = ec.ExternalCompanyCode
    from @TableHeader th
    join ExternalCompany ec on th.ExternalCompanyId = ec.ExternalCompanyId
  
  update t
     set ReasonId = r.ReasonId,
         --LineReason = r.Reason,
         Exception  = e.Exception
    from @TableDetails t
    join Exception     e (nolock) on t.ReceiptLineId = e.ReceiptLineId
    join Reason        r (nolock) on e.ReasonId      = r.ReasonId
  
  update td
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         Batch		 = b.Batch,
         ExpiryDate  = b.ExpiryDate
    from @TableDetails    td
    join StorageUnitBatch sub on td.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  on sub.StorageUnitId     = su.StorageUnitId
    join Product          p   on su.ProductId          = p.ProductId
    join SKU              sku on su.SKUId              = sku.SKUId
    join Batch            b   on sub.BatchId		   = b.BatchId
  
  select th.InboundShipmentId,
         th.OrderNumber,
         DeliveryNoteNumber,
         DeliveryDate,
         th.Supplier,
         th.SupplierCode,
         th.HeaderReason,
         td.ProductCode,
         td.Product,
         td.ExpectedQuantity,
         AcceptedQuantity,
         RejectQuantity,
         Exception,
         SealNumber,
         VehicleRegistration,
         td.Batch,
         td.ExpiryDate,
         td.SampleQty,
         td.OperatorCode,
         td.LineReason,
         th.ContactPerson,
         th.CreateDate
    from @TableHeader  th
    join @TableDetails td on th.ReceiptId = td.ReceiptId
    order by th.ContactPerson
end
