﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PriorityHistory_Insert
  ///   Filename       : p_PriorityHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:30
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PriorityHistory table.
  /// </remarks>
  /// <param>
  ///   @PriorityId int = null,
  ///   @Priority nvarchar(100) = null,
  ///   @PriorityCode nvarchar(20) = null,
  ///   @OrderBy int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   PriorityHistory.PriorityId,
  ///   PriorityHistory.Priority,
  ///   PriorityHistory.PriorityCode,
  ///   PriorityHistory.OrderBy,
  ///   PriorityHistory.CommandType,
  ///   PriorityHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PriorityHistory_Insert
(
 @PriorityId int = null,
 @Priority nvarchar(100) = null,
 @PriorityCode nvarchar(20) = null,
 @OrderBy int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert PriorityHistory
        (PriorityId,
         Priority,
         PriorityCode,
         OrderBy,
         CommandType,
         InsertDate)
  select @PriorityId,
         @Priority,
         @PriorityCode,
         @OrderBy,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
