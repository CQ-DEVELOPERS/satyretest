﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Parameter
  ///   Filename       : p_Operator_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 13:19:58
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Operator table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Operator.OperatorId,
  ///   Operator.Operator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as OperatorId
        ,'{All}' as Operator
  union
  select
         Operator.OperatorId
        ,Operator.Operator
    from Operator
  order by Operator
  
end
