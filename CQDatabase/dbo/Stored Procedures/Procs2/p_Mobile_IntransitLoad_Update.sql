﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_IntransitLoad_Update
    ///   Filename       : p_Mobile_IntransitLoad_Update.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 2 March 2009 20:04:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
    ///   Details        : 
    /// </newpara>
*/
create procedure p_Mobile_IntransitLoad_Update
(
 @IntransitLoadId     int,
 @OperatorId          int             =  null,
 @DeliveryNoteNumber  nvarchar(30)    =  null,
 @DeliveryDate        nvarchar(30)    =  null,
 @SealNumber          nvarchar(30)    =  null,
 @VehicleRegistration nvarchar(10)    =  null,
 @Remarks             nvarchar(250)   =  null,
 @Route               nvarchar(50)    = null,
 @DriverId            int             = null,
 @LoadClosed          datetime        = null,
 @ReceivedDate        datetime        = null,
 @Offloaded           datetime        = null
)
as
begin
	 set nocount on
  
  declare @Error              int,
          @GetDate            datetime,
          @WarehouseId        int,
          @StatusId           int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if @DeliveryNoteNumber = ''
    set @DeliveryNoteNumber = null
  
  if @DeliveryNoteNumber is not null
  begin
    if @DeliveryNoteNumber !=
      (select DeliveryNoteNumber
         from IntransitLoad (nolock)
        where IntransitLoadId = @IntransitLoadId)
    begin
      set @IntransitLoadId = -2
      goto error
    end
  end
  
  select @StatusId = dbo.ufn_StatusId('R','R')
  
  if @IntransitLoadId is not null
  begin
    exec @Error = p_IntransitLoad_Update
     @IntransitLoadId     = @IntransitLoadId,
     @StatusId            = @StatusId,
     @CreatedById         = @OperatorId,
     @DeliveryNoteNumber  = @DeliveryNoteNumber,
     @DeliveryDate        = @DeliveryDate,
     @SealNumber          = @SealNumber,
     @VehicleRegistration = @VehicleRegistration,
     @Remarks             = @Remarks,
     @Route               = @Route,
     @DriverId            = @DriverId,
     @LoadClosed          = @LoadClosed,
     @ReceivedDate        = @ReceivedDate,
     @Offloaded           = @Offloaded
     
    if @Error <> 0
    begin
      set @IntransitLoadId = -1
      goto error
    end
  end
  
  commit transaction
  select @IntransitLoadId
  return @IntransitLoadId
  
  error:
    rollback transaction
    select @IntransitLoadId
    return @IntransitLoadId
end
