﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Stock_Take_Area
  ///   Filename       : p_Mobile_Stock_Take_Area.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Stock_Take_Area
(
 @operatorId int
)

as
begin
	 set nocount on;
  
  declare @WarehouseId       int
  
  select @WarehouseId = WarehouseId
    from Operator (nolock)
   where OperatorId = @operatorId
  
  select a.AreaId, a.Area, count(1) as 'Count'
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
    join Status          sj (nolock) on j.StatusId          = sj.StatusId
    join Status          si (nolock) on i.StatusId          = si.StatusId
    join AreaLocation    al (nolock) on i.PickLocationId    = al.LocationId
    join Area             a (nolock) on al.AreaId           = a.AreaId
   where i.WarehouseId           = @WarehouseId
     and it.InstructionTypeCode in ('STE','STL','STA','STP')
     and sj.StatusCode                    in ('RL','S')
     and si.StatusCode                    in ('W','S')
     and isnull(j.OperatorId, @operatorId) = @operatorId
  group by a.AreaId, a.Area
end
