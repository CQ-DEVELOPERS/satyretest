﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Order_Search
  ///   Filename       : p_Outbound_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Order_Search
(
 @WarehouseId             int = null,
 @OutboundDocumentTypeId	 int = null,
 @ExternalCompanyCode	    nvarchar(30) = null,
 @ExternalCompany	        nvarchar(255) = null,
 @OrderNumber	            nvarchar(30) = null,
 @FromDate	               datetime = null,
 @ToDate	                 datetime = null
)

as
begin
	 set nocount on;
	 
	 if @OutboundDocumentTypeId = -1
	   set @OutboundDocumentTypeId = null
	 
	 if @ExternalCompanyCode is null
	   set @ExternalCompanyCode = ''
	 
	 if @ExternalCompany is null
	   set @ExternalCompany = ''
	 
  select i.IssueId,
         od.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         s.Status,
         odt.OutboundDocumentType,
         od.DeliveryDate,
         od.CreateDate
    from Issue                  i (nolock)
    join Status                 s (nolock) on i.StatusId                = s.StatusId
    join OutboundDocument      od (nolock) on i.OutboundDocumentId      = od.OutboundDocumentId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    join ExternalCompany       ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
   where od.OutboundDocumentTypeId    = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
     and ec.ExternalCompanyCode    like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany        like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and od.OrderNumber            like isnull(@OrderNumber  + '%', od.OrderNumber)
     and od.DeliveryDate        between isnull(@FromDate, od.DeliveryDate) and isnull(@ToDate, od.DeliveryDate)
     and od.WarehouseId               = isnull(@WarehouseId, od.WarehouseId)
     and s.StatusCode in ('W','P','OH','Z')
     --and not exists(select 1 from OutboundShipmentIssue osi where i.IssueId = osi.IssueId)
  order by od.OrderNumber
end
