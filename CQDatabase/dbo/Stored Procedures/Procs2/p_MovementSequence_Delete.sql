﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MovementSequence_Delete
  ///   Filename       : p_MovementSequence_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:02
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the MovementSequence table.
  /// </remarks>
  /// <param>
  ///   @MovementSequenceId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MovementSequence_Delete
(
 @MovementSequenceId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete MovementSequence
     where MovementSequenceId = @MovementSequenceId
  
  select @Error = @@Error
  
  
  return @Error
  
end
