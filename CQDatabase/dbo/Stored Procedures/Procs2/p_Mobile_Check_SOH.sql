﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_SOH
  ///   Filename       : p_Mobile_Check_SOH.sql
  ///   Create By      : Karen
  ///   Date Created   : May 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Check_SOH
(
 @location		nvarchar(30),
 @barcode			nvarchar(30),
 @batch					nvarchar(30),
 @warehouseId int = 1
)

as
begin
	 set nocount on;
  
  declare @bool       bit,
          @SOH        float
 
  select @batch = replace(@batch,'A:','')
  select @batch = replace(@batch,'Q:','')
  select @batch = replace(@batch,'QC:','')
  
  if @batch = ''
    set @batch = null
  
  select @SOH = subl.ActualQuantity - subl.ReservedQuantity
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join Batch                       b (nolock) on sub.BatchId             = b.BatchId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
    left join SKU                  sku (nolock) on su.SKUId                = sku.SKUId
   where a.WarehouseId = @warehouseId
     and (p.ProductCode = @barcode
      or sku.SKUCode = @barcode)
     and l.Location    = @location
     and b.Batch		     = isnull(@batch,b.Batch)
     and a.StockOnHand = 1
  
  if @SOH > 0
      set @bool = 1
    else
      set @bool = 0
   
  select @bool
end
 
