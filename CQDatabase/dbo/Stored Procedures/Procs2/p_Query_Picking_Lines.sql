﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Query_Picking_Lines
  ///   Filename       : p_Query_Picking_Lines.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Query_Picking_Lines
(
 @JobId int
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
   OutboundShipmentId int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompany    nvarchar(255),
   InstructionId      int,
   InstructionRefId   int,
   PalletId           int,
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   SKU                nvarchar(50),
   Batch              nvarchar(50),
   LocationId         int,
   Location           nvarchar(15),
   Quantity           float,
   ConfirmedQuantity  float,
   OperatorId         int,
   Operator           nvarchar(50),
   StatusId           int,
   Status             nvarchar(50),
   CreateDate         datetime,
   StartDate          datetime,
   EndDate            datetime
	 )
	 
  insert @TableResult
        (InstructionId,
         InstructionRefId,
         PalletId,
         OperatorId,
         StatusId,
         Status,
         OutboundShipmentId,
         StorageUnitBatchId,
         LocationId,
         Quantity,
         ConfirmedQuantity,
         CreateDate,
         StartDate,
         EndDate)
  select i.InstructionId,
         i.InstructionRefId,
         i.PalletId,
         i.OperatorId,
         i.StatusId,
         si.Status,
         i.OutboundShipmentId,
         i.StorageUnitBatchId,
         i.PickLocationId,
         i.Quantity,
         i.ConfirmedQuantity,
         i.CreateDate,
         i.StartDate,
         i.EndDate
    from Instruction            i (nolock)
    join Job                    j (nolock) on i.JobId = j.JobId
    join Status                si (nolock) on i.StatusId = si.StatusId
   where i.JobId = @JobId
  
  update tr
     set OutboundDocumentId = ili.OutboundDocumentId
    from @TableResult tr
    join IssueLineInstruction ili on tr.InstructionId = ili.InstructionId
  
  update tr
     set OutboundDocumentId = ili.OutboundDocumentId
    from @TableResult tr
    join IssueLineInstruction ili on tr.InstructionRefId = ili.InstructionId
   where tr.OutboundDocumentId is null
  
  update tr
     set OrderNumber     = od.OrderNumber,
         ExternalCompany = ec.ExternalCompany
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
    join ExternalCompany  ec (nolock) on od.ExternalCompanyId  = ec.ExternalCompanyId
  
--  update tr
--     set JobStatus = s.Status
--    from @TableResult tr
--    join Status        s (nolock) on tr.JobStatusId = s.StatusId
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set ProductCode = vs.ProductCode,
         Product     = vs.Product,
         SKUCode     = vs.SKUCode,
         SKU         = vs.SKU,
         Batch       = vs.Batch
    from @TableResult tr
    join viewStock    vs on tr.StorageUnitBatchId = vs.StorageUnitBatchId
  
  select OutboundShipmentId,
         OrderNumber,
         ExternalCompany,
         InstructionId,
         PalletId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         Location,
         Quantity,
         ConfirmedQuantity,
         OperatorId,
         Operator,
         StatusId,
         Status,
         CreateDate,
         StartDate,
         EndDate
    from @TableResult
  order by Location, ProductCode
end
