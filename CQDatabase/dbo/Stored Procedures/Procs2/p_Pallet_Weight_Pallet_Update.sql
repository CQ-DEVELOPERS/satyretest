﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Weight_Pallet_Update
  ///   Filename       : p_Pallet_Weight_Pallet_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Weight_Pallet_Update
(
 @palletId    int,
 @tareWeight  numeric(13,6) = null,
 @nettWeight  numeric(13,6) = null,
 @grossWeight numeric(13,6) = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @tareWeight < 0
    set @tareWeight = null
  
  if @grossWeight < 0
    set @grossWeight = null
  
  begin transaction
  
  exec @Error = p_Pallet_Update
   @PalletId   = @palletId,
   @Tare       = @tareWeight,
   @nett       = @nettWeight,
   @Weight     = @grossWeight
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Pallet_Weight_Pallet_Update'); 
    rollback transaction
    return @Error
end
