﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MovementSequence_Update
  ///   Filename       : p_MovementSequence_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:01
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the MovementSequence table.
  /// </remarks>
  /// <param>
  ///   @MovementSequenceId int = null,
  ///   @PickAreaId int = null,
  ///   @StoreAreaId int = null,
  ///   @StageAreaId int = null,
  ///   @OperatorGroupId int = null,
  ///   @StatusCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MovementSequence_Update
(
 @MovementSequenceId int = null,
 @PickAreaId int = null,
 @StoreAreaId int = null,
 @StageAreaId int = null,
 @OperatorGroupId int = null,
 @StatusCode nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
  if @MovementSequenceId = '-1'
    set @MovementSequenceId = null;
  
	 declare @Error int
 
  update MovementSequence
     set PickAreaId = isnull(@PickAreaId, PickAreaId),
         StoreAreaId = isnull(@StoreAreaId, StoreAreaId),
         StageAreaId = isnull(@StageAreaId, StageAreaId),
         OperatorGroupId = isnull(@OperatorGroupId, OperatorGroupId),
         StatusCode = isnull(@StatusCode, StatusCode) 
   where MovementSequenceId = @MovementSequenceId
  
  select @Error = @@Error
  
  
  return @Error
  
end
