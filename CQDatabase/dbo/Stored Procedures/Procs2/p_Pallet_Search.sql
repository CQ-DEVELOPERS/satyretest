﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Search
  ///   Filename       : p_Pallet_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:30
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Pallet table.
  /// </remarks>
  /// <param>
  ///   @PalletId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Pallet.PalletId,
  ///   Pallet.StorageUnitBatchId,
  ///   Pallet.LocationId,
  ///   Pallet.Weight,
  ///   Pallet.Tare,
  ///   Pallet.Quantity,
  ///   Pallet.Prints,
  ///   Pallet.CreateDate,
  ///   Pallet.Nett,
  ///   Pallet.ReasonId,
  ///   Pallet.EmptyWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Search
(
 @PalletId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @PalletId = '-1'
    set @PalletId = null;
  
 
  select
         Pallet.PalletId
        ,Pallet.StorageUnitBatchId
        ,Pallet.LocationId
        ,Pallet.Weight
        ,Pallet.Tare
        ,Pallet.Quantity
        ,Pallet.Prints
        ,Pallet.CreateDate
        ,Pallet.Nett
        ,Pallet.ReasonId
        ,Pallet.EmptyWeight
    from Pallet
   where isnull(Pallet.PalletId,'0')  = isnull(@PalletId, isnull(Pallet.PalletId,'0'))
  
end
