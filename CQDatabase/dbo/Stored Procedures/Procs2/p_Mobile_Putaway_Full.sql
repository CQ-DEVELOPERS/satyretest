﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Putaway_Full
    ///   Filename       : p_Mobile_Putaway_Full.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 11 June 2007 14:15:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    ///  @Type                int,
    ///  @OperatorId          int,
    ///  @PalletId            int,
    ///  @StoreLocation       nvarchar(15),
    ///  @ConfirmedQuantity   float
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
    ///   Details        : 
    /// </newpara>
*/
create procedure p_Mobile_Putaway_Full
(
 @Type                int,
 @OperatorId          int,
 @PalletId            int,
 @StoreLocation       nvarchar(15),
 @ConfirmedQuantity   float
)
as
begin
	 set nocount on
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @Rowcount          int,
          @JobId             int,
          @InstructionId     int,
          @StorageUnitBatchId int,
          @PickLocationId    int,
          @ProductCode       nvarchar(30),
          @SKUCode           nvarchar(50),
          @Batch             nvarchar(50),
          @StoreLocationId   int,
          @SecurityCode      int,
          @Location          nvarchar(15),
          @StatusId          int,
          @DwellTime         int,
          @Quantity          float
  
  declare @StoreAreaId        int,
          @PickAreaId         int,
          @OperatorGroupId    int,
          @NewStoreLoactionId int,
          @InstructionTypeId  int,
          @WarehouseId        int,
          @Weight             numeric(13,3),
          @ReceiptLineId      int
  
  select @GetDate = dbo.ufn_Getdate()
  
  exec @DwellTime = p_Mobile_Dwell_Time
   @Type       = 2,
   @OperatorId = @OperatorId
  
  begin transaction
  
  select top 1
         @InstructionId      = i.InstructionId,
         @StorageUnitBatchId = i.StorageUnitBatchId,
         @PickLocationId     = i.PickLocationId,
         @StoreLocationId    = i.StoreLocationId,
         @StatusId           = i.StatusId,
         @Quantity           = i.Quantity,
         @InstructionTypeId  = i.InstructionTypeId,
         @WarehouseId        = i.WarehouseId,
         @Weight             = i.Weight,
         @JobId              = i.JobId,
         @ReceiptLineId      = i.ReceiptLineId
    from Instruction i
    join Status      s on i.StatusId = s.StatusId
   where i.PalletId  = @PalletId
     and s.Type      = 'I'
     and StatusCode in ('W','S')
  order by InstructionId
  
  select @Rowcount = @@rowcount
  
  if @Rowcount = 0
  begin
    set @Error = 2
    goto result
  end
  
  if (select StatusCode
        from Status (nolock)
       where StatusId = @StatusId) not in ('W','S')
  begin
    set @Error = 4
    goto result
  end
  
  if @Type = 0 -- Get Pallet Info
  begin
    
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'S'
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @OperatorId    = @OperatorId,
     @StartDate     = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    if @Error <> 0
      goto result
    
    exec @Error = p_Operator_Update
     @OperatorId = @OperatorId,
     @LastInstruction      = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    if @Error <> 0
      goto result
    
    if @StoreLocationId is not null
    begin
--      select @PickAreaId = AreaId
--        from AreaLocation
--       where LocationId = @PickLocationId
--      
--      select @StoreAreaId = AreaId
--        from AreaLocation
--       where LocationId = @StoreLocationId
--      
--      set @OperatorGroupId = dbo.ufn_OperatorGroupId(@OperatorId) 
--      
--      select top 1
--             @SecurityCode  = l.SecurityCode,
--             @StoreLocation = l.Location,
--             @NewStoreLoactionId = l.LocationId
--        from MovementSequence ms (nolock)
--        join AreaLocation     al (nolock) on ms.StageAreaId = al.AreaId
--        join Location          l (nolock) on al.LocationId  = l.LocationId
--       where PickAreaId      = @PickAreaId
--         and StoreAreaId     = @StoreAreaId
--         and OperatorGroupId = @OperatorGroupId
--      
--      set @StatusId = dbo.ufn_StatusId('I','W')
--      
--      exec @Error = p_Instruction_Insert
--       @InstructionTypeId   = @InstructionTypeId,
--       @StorageUnitBatchId  = @StorageUnitBatchId,
--       @WarehouseId         = @WarehouseId,
--       @StatusId            = @StatusId,
--       @JobId               = @JobId,
--       @OperatorId          = @OperatorId,
--       @PickLocationId      = @PickLocationId,
--       @StoreLocationId     = @StoreLocationId,
--       @InstructionRefId    = @InstructionId,
--       @Quantity            = @Quantity,
--       @ConfirmedQuantity   = 0,
--       @Weight              = @Weight,
--       @ConfirmedWeight     = 0,
--       @PalletId            = @PalletId,
--       @CreateDate          = @GetDate
--      
--      if @Error <> 0
--      begin
--        set @Error = 1
--        goto Result
--      end
--      
--      exec @Error = p_Instruction_Update
--       @InstructionId = @InstructionId,
--       @StoreLocationId = @NewStoreLoactionId
--      
--      if @Error <> 0
--      begin
--        set @Error = 1
--        goto Result
--      end
      
      select @SecurityCode  = SecurityCode,
             @StoreLocation = Location
        from Location (nolock)
       where LocationId = @StoreLocationId
    end
    else
    begin
      select 'Must get Location???'
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId       = @InstructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 1 -- Cancel Putaway
  begin
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'W'
    
    update Instruction
       set StatusId      = @StatusId,
           OperatorId    = null,
           StartDate     = null
     where InstructionId = @InstructionId
    
    select @Error = @@error
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_InstructionHistory_Insert
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @OperatorId    = null,
     @StartDate     = null,
     @CommandType   = 'Update',
     @InsertDate    = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId       = @InstructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 2 -- Product Picked
  begin
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId       = @InstructionId,
     @Store               = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 3 -- Validate Store Location
  begin
    select @SecurityCode  = SecurityCode,
           @Location = Location
      from Location (nolock)
     where LocationId = @StoreLocationId
    
    if @StoreLocation != @Location
    and @StoreLocation != convert(nvarchar(15), @SecurityCode)
    begin
      set @Error = 6
      goto Result
    end
    
    set @Error = 0
    goto Result
  end
  else if @Type = 4 -- Product Binned
  begin
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId       = @InstructionId,
     @Pick                = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'F' 
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @EndDate       = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
  end
  else if @Type = 5 -- Loation Error
  begin
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId       = @InstructionId,
     @Pick                = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec p_Location_Get
     @WarehouseId        = @WarehouseId,
     @LocationId         = @StoreLocationId output,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @Quantity           = @Quantity
    
    exec @Error = p_Instruction_Update
     @InstructionId      = @InstructionId,
     @StoreLocationId    = @StoreLocationId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId       = @InstructionId,
     @Pick                = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    select @SecurityCode  = SecurityCode,
           @StoreLocation = Location
      from Location (nolock)
     where LocationId = @StoreLocationId
    
--    exec @Error = p_Mobile_Exception_Stock_Take
--     @OperatorId          = @OperatorId,
--     @StorageUnitBatchId  = 1,
--     @PickLocationId      = @PickLocationId,
--     @Quantity            = @Quantity
--    
--    if @Error <> 0
--    begin
--      set @Error = 1
--      goto Result
--    end
    
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 6 -- Confirm Quantity
  begin
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    if @ConfirmedQuantity > @Quantity
    begin
      set @Error = 7
      goto Result
    end
    else if @ConfirmedQuantity = @Quantity
    begin
      set @Error = 0
      goto Result
    end
    else
    begin
      exec @Error = p_Instruction_Update
       @InstructionId       = @InstructionId,
       @ConfirmedQuantity   = @ConfirmedQuantity
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      set @StatusId = dbo.ufn_StatusId('I','W')
      
      set @Quantity = @Quantity - @ConfirmedQuantity
      
      exec @Error = p_Instruction_Insert
       @InstructionId       = @InstructionId output,
       @InstructionTypeId   = @InstructionTypeId,
       @StorageUnitBatchId  = @StorageUnitBatchId,
       @WarehouseId         = @WarehouseId,
       @StatusId            = @StatusId,
       @JobId               = @JobId,
       @OperatorId          = @OperatorId,
       @PickLocationId      = @PickLocationId,
       @StoreLocationId     = @StoreLocationId,
       @InstructionRefId    = @InstructionId,
       @Quantity            = @Quantity,
       @ConfirmedQuantity   = 0,
       @Weight              = @Weight,
       @ConfirmedWeight     = 0,
       @PalletId            = @PalletId,
       @CreateDate          = @GetDate,
       @ReceiptLineId       = @ReceiptLineId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      if (select dbo.ufn_Configuration(21, @WarehouseId)) = 1
      begin
        exec p_Location_Get
         @WarehouseId        = @WarehouseId,
         @LocationId         = @StoreLocationId output,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @Quantity           = @Quantity
        
        exec @Error = p_Instruction_Update
         @InstructionId      = @InstructionId,
         @StoreLocationId    = @StoreLocationId
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
        
        exec @Error = p_StorageUnitBatchLocation_Reserve
         @InstructionId       = @InstructionId,
         @Pick                = 0 -- false
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
      end
    end
    
    set @Error = 0
    goto result
  end
  
  Result:
    if @Error <> 0
      rollback transaction
    else
      commit transaction
    
    select @Error             as 'Result',
           @DwellTime         as 'DwellTime',
           @PalletId          as 'PalletId',
           @ProductCode       as 'ProductCode',
           @SKUCode           as 'SKUCode',
           @Batch             as 'Batch',
           @StoreLocation     as 'StoreLocation',
           @SecurityCode      as 'SecurityCode',
           @Quantity          as 'Quantity'
end
