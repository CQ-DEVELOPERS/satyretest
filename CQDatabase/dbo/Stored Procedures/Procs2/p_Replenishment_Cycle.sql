﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Replenishment_Cycle
  ///   Filename       : p_Replenishment_Cycle.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Mar 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Replenishment_Cycle
(
 @WarehouseId int
)

as
begin
	 set nocount on;
  
  declare @TableProducts as table
  (
   StorageUnitId     int,
   ProductCategory    nvarchar(1),
   MovementCategory  int,
   NumberOfLocations int
  )
  
  declare @TableLocations as table
  (
   LocationId int
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @EmptyLocations    int,
          @StorageUnitId     int,
          @StorageUnitBatchId int,
          @StoreLocationId   int,
          @Quantity          numeric(13,6)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  set @EmptyLocations = 0
  
  exec p_Location_Update_Used
  
  insert @TableLocations
        (LocationId)
  select LocationId
    from viewLocation
   where WarehouseId = @WarehouseId
     and AreaCode    = 'RK'
     and AreaType    = ''
     and Used        = 0
  
  select @EmptyLocations = @@rowcount
  
  select @EmptyLocations as '@EmptyLocations'
  
  set rowcount @EmptyLocations
  insert @TableProducts
        (StorageUnitId,
         ProductCategory,
         MovementCategory)
  select su.StorageUnitId,
         su.ProductCategory,
         su.MovementCategory
         --count(distinct(subl.LocationId)) as 'NumberOfLocations'
    from StorageUnit                su (nolock)
    join StorageUnitArea           sua (nolock) on su.StorageUnitId       = sua.StorageUnitId
    join Area                        a (nolock) on sua.AreaId             = a.AreaId
--    left
--    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
   where a.WarehouseId = @WarehouseId
     and a.StockOnHand = 1
     and a.AreaCode = 'RK'
     and exists(select 1 from viewSOH soh where su.StorageUnitId = soh.StorageUnitId and soh.AreaCode = 'BK')
  group by su.StorageUnitId,
           su.ProductCategory,
           su.MovementCategory
  order by su.ProductCategory,
           su.MovementCategory desc
--  select su.StorageUnitId,
--         su.ProductCategory,
--         su.MovementCategory,
--         count(distinct(subl.LocationId)) as 'NumberOfLocations'
--    from StorageUnitBatch          sub (nolock)
--    join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
--    join Batch                       b (nolock) on sub.BatchId            = b.BatchId
--    join Status                      s (nolock) on b.StatusId             = s.StatusId
--    join StorageUnitArea           sua (nolock) on su.StorageUnitId       = sua.StorageUnitId
--    join Area                        a (nolock) on sua.AreaId             = a.AreaId
--    join AreaLocation               al (nolock) on a.AreaId               = al.AreaId
--    join Location                    l (nolock) on al.LocationId          = l.LocationId
--    left
--    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
--   where a.WarehouseId = 1
--     and a.StockOnHand = 1
--     and a.AreaCode = 'RK'
--     and s.StatusCode = 'A'
--     and exists(select 1 from viewSOH soh where su.StorageUnitId = soh.StorageUnitId and soh.AreaCode = 'BK')
--  group by su.StorageUnitId,
--           su.ProductCategory,
--           su.MovementCategory
--  order by su.ProductCategory,
--           su.MovementCategory desc,
--           count(distinct(subl.LocationId))
  set rowcount 0
  
  update tp
     set NumberOfLocations = (select count(1)
                                from viewSOH soh
                               where tp.StorageUnitId = soh.StorageUnitId
                                 and soh.AreaCode = 'RK')
    from @TableProducts tp
  
  update @TableProducts set MovementCategory = dbo.ufn_Configuration_Value(165, @WarehouseId) where ProductCategory = 'A'
  update @TableProducts set MovementCategory = dbo.ufn_Configuration_Value(166, @WarehouseId) where ProductCategory = 'B'
  update @TableProducts set MovementCategory = dbo.ufn_Configuration_Value(167, @WarehouseId) where ProductCategory = 'C'
  update @TableProducts set MovementCategory = dbo.ufn_Configuration_Value(168, @WarehouseId) where ProductCategory = 'D'
  
  while @EmptyLocations > 0
  begin
    select @EmptyLocations = @EmptyLocations - 1
    
    set @StorageUnitId = null
    
    select top 1 @StorageUnitId = StorageUnitId
      from @TableProducts
     where NumberOfLocations < isnull(MovementCategory,1)
        
    select top 1 @Quantity = Quantity
      from Pack      p (nolock)
      join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
     where StorageUnitId = @StorageUnitId
       and WarehouseId = @WarehouseId
    order by pt.OutboundSequence
    
    if @StorageUnitId is null
    begin
      set @EmptyLocations = 0
    end
    else
    begin
      delete @TableProducts where StorageUnitId = @StorageUnitId
      
      set @StoreLocationId = null
      
      select top 1 @StoreLocationId = LocationId
        from @TableLocations
      
      delete @TableLocations where LocationId = @StoreLocationId
      
      select @StorageUnitBatchId = StorageUnitBatchId
        from StorageUnitBatch sub (nolock)
        join Batch              b (nolock) on sub.BatchId = b.BatchId
       where sub.StorageUnitId = @StorageUnitId
         and b.Batch = 'Default'
      
      exec @Error = p_Mobile_Replenishment_Request
       @storeLocationId    = @StoreLocationId,
       @storageUnitBatchId = @StorageUnitBatchId,
       @operatorId         = null,
       @ShowMsg            = 0,
       @ManualReq          = 0,
       @AnyLocation        = 1
      
      if @Error <> 0
        select @Error as '@Error', @StorageUnitId as '@StorageUnitId', @StoreLocationId as '@StoreLocationId'
    end
  end
  
  declare @PriorityId int

  select top 1 @PriorityId = PriorityId
    from Priority
  order by OrderBy desc

  update j
     set PriorityId = @PriorityId
    from Instruction i
    join Status            si (nolock) on i.StatusId           = si.StatusId
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    left
    join viewLocation    pick (nolock) on i.PickLocationId     = pick.LocationId
    left
    join viewLocation   store (nolock) on i.StoreLocationId    = store.LocationId
   where si.StatusCode = 'W'
     and it.InstructionTypeCode = 'R'
     and pick.AreaCode = 'BK'
     and store.AreaCode = 'RK'
     and j.PriorityId != @PriorityId
  
  declare	@Command nvarchar(max)

  declare replen_cursor cursor for
  select 'exec p_Housekeeping_Instruction_Delete @InstructionId = ' + convert(nvarchar(10), InstructionId)
    from Instruction i
    join Status            si (nolock) on i.StatusId           = si.StatusId
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    left
    join viewLocation    pick (nolock) on i.PickLocationId     = pick.LocationId
    left
    join viewLocation   store (nolock) on i.StoreLocationId    = store.LocationId
   where si.StatusCode = 'W'
     and it.InstructionTypeCode = 'R'
     and pick.AreaCode in ('RK','SP')
     and store.AreaCode = 'RK'

  open replen_cursor

  fetch replen_cursor into @Command

  while (@@fetch_status = 0)
  begin
    execute(@Command)
    
    fetch replen_cursor into @Command
  end
  
  close replen_cursor
  deallocate replen_cursor
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Replenishment_Cycle'); 
    rollback transaction
    return @Error
end
