﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallets_Update
  ///   Filename       : p_Pallets_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:54
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Pallets table.
  /// </remarks>
  /// <param>
  ///   @pallet_id int = null,
  ///   @stock_no nvarchar(60) = null,
  ///   @sku_code nvarchar(30) = null,
  ///   @lot_code nvarchar(60) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallets_Update
(
 @pallet_id int = null,
 @stock_no nvarchar(60) = null,
 @sku_code nvarchar(30) = null,
 @lot_code nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update Pallets
     set pallet_id = isnull(@pallet_id, pallet_id),
         stock_no = isnull(@stock_no, stock_no),
         sku_code = isnull(@sku_code, sku_code),
         lot_code = isnull(@lot_code, lot_code) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
