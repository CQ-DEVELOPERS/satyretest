﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipmentIssue_Update
  ///   Filename       : p_OutboundShipmentIssue_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jun 2013 12:36:34
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OutboundShipmentIssue table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null,
  ///   @IssueId int = null,
  ///   @DropSequence int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipmentIssue_Update
(
 @OutboundShipmentId int = null,
 @IssueId int = null,
 @DropSequence int = null 
)

as
begin
	 set nocount on;
  
  if @OutboundShipmentId = '-1'
    set @OutboundShipmentId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
	 declare @Error int
 
  update OutboundShipmentIssue
     set DropSequence = isnull(@DropSequence, DropSequence) 
   where OutboundShipmentId = @OutboundShipmentId
     and IssueId = @IssueId
  
  select @Error = @@Error
  
  
  return @Error
  
end
