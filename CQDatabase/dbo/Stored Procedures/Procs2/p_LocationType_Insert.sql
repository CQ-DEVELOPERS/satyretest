﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationType_Insert
  ///   Filename       : p_LocationType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:58
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the LocationType table.
  /// </remarks>
  /// <param>
  ///   @LocationTypeId int = null output,
  ///   @LocationType nvarchar(100) = null,
  ///   @LocationTypeCode nvarchar(20) = null,
  ///   @DeleteIndicator bit = null,
  ///   @ReplenishmentIndicator bit = null,
  ///   @MultipleProductsIndicator bit = null,
  ///   @BatchTrackingIndicator bit = null 
  /// </param>
  /// <returns>
  ///   LocationType.LocationTypeId,
  ///   LocationType.LocationType,
  ///   LocationType.LocationTypeCode,
  ///   LocationType.DeleteIndicator,
  ///   LocationType.ReplenishmentIndicator,
  ///   LocationType.MultipleProductsIndicator,
  ///   LocationType.BatchTrackingIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationType_Insert
(
 @LocationTypeId int = null output,
 @LocationType nvarchar(100) = null,
 @LocationTypeCode nvarchar(20) = null,
 @DeleteIndicator bit = null,
 @ReplenishmentIndicator bit = null,
 @MultipleProductsIndicator bit = null,
 @BatchTrackingIndicator bit = null 
)

as
begin
	 set nocount on;
  
  if @LocationTypeId = '-1'
    set @LocationTypeId = null;
  
  if @LocationType = '-1'
    set @LocationType = null;
  
  if @LocationTypeCode = '-1'
    set @LocationTypeCode = null;
  
	 declare @Error int
 
  insert LocationType
        (LocationType,
         LocationTypeCode,
         DeleteIndicator,
         ReplenishmentIndicator,
         MultipleProductsIndicator,
         BatchTrackingIndicator)
  select @LocationType,
         @LocationTypeCode,
         @DeleteIndicator,
         @ReplenishmentIndicator,
         @MultipleProductsIndicator,
         @BatchTrackingIndicator 
  
  select @Error = @@Error, @LocationTypeId = scope_identity()
  
  
  return @Error
  
end
