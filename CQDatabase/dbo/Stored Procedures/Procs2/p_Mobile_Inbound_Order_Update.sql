﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Inbound_Order_Update
    ///   Filename       : p_Mobile_Inbound_Order_Update.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 2 March 2009 20:04:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
    ///   Details 

       : 
    /// </newpara>
*/
create procedure p_Mobile_Inbound_Order_Update
(
 @receiptId           int,
 @OperatorId          int            =  null,
 @DeliveryNoteNumber  nvarchar(30)    =  null,
 @DeliveryDate        nvarchar(30)    =  null,
 @SealNumber          nvarchar(30)    =  null,
 @VehicleRegistration nvarchar(10)    =  null,
 @Remarks             nvarchar(250)   =  null
)
as
begin
	 set nocount on
  
  declare @Error              int,
          @GetDate            datetime,
          @WarehouseId        int,
          @StatusId           int,
          @ContainerNumber	  nvarchar(50),
          @ReceivingStarted   datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @WarehouseId = WarehouseId
    from Receipt (nolock)
   where ReceiptId = @receiptId
  
  begin transaction
  
  if @DeliveryNoteNumber = ''
    set @DeliveryNoteNumber = null
  
  if @DeliveryNoteNumber is not null
  begin
    if @DeliveryNoteNumber !=
      (select DeliveryNoteNumber
         from Receipt
        where ReceiptId = @receiptId)
    begin
    
  set @ReceiptId = -2
      goto error
    
end
  end
  
  select @StatusId = dbo.ufn_StatusId('R','R')
  
  set @ReceivingStarted = (select ReceivingStarted from Receipt where ReceiptId = @receiptId)
  if  @ReceivingStarted is null
	set @ReceivingStarted = getdate()  

  if @ReceiptId is not null
  begin
    exec @Error = p_Receipt_Update
     @ReceiptId           = @ReceiptId,
     @StatusId            = @StatusId,
     @OperatorId          = @OperatorId, 
     @DeliveryNoteNumber  = @DeliveryNoteNumber,
     @DeliveryDate        = @DeliveryDate,
     @SealNumber          = @SealNumber,
     @VehicleRegistration = @VehicleRegistration,
     @Remarks             = @Remarks,
     @ContainerNumber	  = @ContainerNumber,
     @ReceivingStarted    = @ReceivingStarted
    
    if @Error <> 0
    begin
  
    set @ReceiptId = -1
      goto error
    end
  end
  
  commit transaction
  select @ReceiptId
  return @ReceiptId
  
  error:
    rollback transaction
    select @ReceiptId
    return @ReceiptId
end

