﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Despatch_Advice_Ctrl
  ///   Filename       : p_Report_Despatch_Advice_Ctrl.sql
  ///   Create By      : Karen
  ///   Date Created   : March 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Despatch_Advice_Ctrl
(
 @ConnectionString   nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @OutboundShipmentId int,
 @IssueId            int
)

as
begin
	 set nocount on;

declare @PrintDangerousGoods	nvarchar(1)
	   
set @PrintDangerousGoods = '0'
  
  SELECT @PrintDangerousGoods = (CONVERT(nvarchar, Indicator))
  from Configuration where ConfigurationId = 270
  
  
 select @ConnectionString as 'ConnectionString',
         @OutboundShipmentId as 'OutboundShipmentId',
         @IssueId as 'IssueId',
         @PrintDangerousGoods as 'PrintDangerousGoods'
end
