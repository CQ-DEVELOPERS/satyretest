﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_JobHistory_Insert
  ///   Filename       : p_JobHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Aug 2014 12:07:49
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the JobHistory table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null,
  ///   @PriorityId int = null,
  ///   @OperatorId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @ReceiptLineId int = null,
  ///   @IssueLineId int = null,
  ///   @ContainerTypeId int = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @TareWeight float = null,
  ///   @Weight float = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @NettWeight float = null,
  ///   @CheckedBy int = null,
  ///   @CheckedDate datetime = null,
  ///   @DropSequence int = null,
  ///   @Pallets int = null,
  ///   @BackFlush bit = null,
  ///   @Prints int = null,
  ///   @CheckingClear bit = null,
  ///   @TrackingNumber nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   JobHistory.JobId,
  ///   JobHistory.PriorityId,
  ///   JobHistory.OperatorId,
  ///   JobHistory.StatusId,
  ///   JobHistory.WarehouseId,
  ///   JobHistory.ReceiptLineId,
  ///   JobHistory.IssueLineId,
  ///   JobHistory.ContainerTypeId,
  ///   JobHistory.ReferenceNumber,
  ///   JobHistory.TareWeight,
  ///   JobHistory.Weight,
  ///   JobHistory.CommandType,
  ///   JobHistory.InsertDate,
  ///   JobHistory.NettWeight,
  ///   JobHistory.CheckedBy,
  ///   JobHistory.CheckedDate,
  ///   JobHistory.DropSequence,
  ///   JobHistory.Pallets,
  ///   JobHistory.BackFlush,
  ///   JobHistory.Prints,
  ///   JobHistory.CheckingClear,
  ///   JobHistory.TrackingNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_JobHistory_Insert
(
 @JobId int = null,
 @PriorityId int = null,
 @OperatorId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @ReceiptLineId int = null,
 @IssueLineId int = null,
 @ContainerTypeId int = null,
 @ReferenceNumber nvarchar(60) = null,
 @TareWeight float = null,
 @Weight float = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @NettWeight float = null,
 @CheckedBy int = null,
 @CheckedDate datetime = null,
 @DropSequence int = null,
 @Pallets int = null,
 @BackFlush bit = null,
 @Prints int = null,
 @CheckingClear bit = null,
 @TrackingNumber nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert JobHistory
        (JobId,
         PriorityId,
         OperatorId,
         StatusId,
         WarehouseId,
         ReceiptLineId,
         IssueLineId,
         ContainerTypeId,
         ReferenceNumber,
         TareWeight,
         Weight,
         CommandType,
         InsertDate,
         NettWeight,
         CheckedBy,
         CheckedDate,
         DropSequence,
         Pallets,
         BackFlush,
         Prints,
         CheckingClear,
         TrackingNumber)
  select @JobId,
         @PriorityId,
         @OperatorId,
         @StatusId,
         @WarehouseId,
         @ReceiptLineId,
         @IssueLineId,
         @ContainerTypeId,
         @ReferenceNumber,
         @TareWeight,
         @Weight,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @NettWeight,
         @CheckedBy,
         @CheckedDate,
         @DropSequence,
         @Pallets,
         @BackFlush,
         @Prints,
         @CheckingClear,
         @TrackingNumber 
  
  select @Error = @@Error
  
  
  return @Error
  
end
