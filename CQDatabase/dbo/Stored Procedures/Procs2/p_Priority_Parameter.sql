﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Priority_Parameter
  ///   Filename       : p_Priority_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:14
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Priority table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Priority.PriorityId,
  ///   Priority.Priority 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Priority_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as PriorityId
        ,'{All}' as Priority
  union
  select
         Priority.PriorityId
        ,Priority.Priority
    from Priority
  
end
 
