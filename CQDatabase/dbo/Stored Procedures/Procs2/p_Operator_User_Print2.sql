﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_User_Print2
  ///   Filename       : p_Operator_User_Print2.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 May 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_User_Print2
(
	@warehouseId int,
	@StartOperator Varchar(50) = null,
	@EndOperator	Varchar(50) = null
)

as
begin

if @StartOperator = '{All}'
	set @StartOperator = null

if @EndOperator = '{All}'
	set @EndOperator = null
		
	
	Select Operator,
			(Name + ' '+ Surname) as Name,
			dbo.ConvertTo128(rtrim(Operator)) as Operator128,
			dbo.ConvertTo128(rtrim(Password)) as Password128
	From Operator
	where Operator >= isnull(@StartOperator,'') and Operator <= Isnull(@EndOperator,Char(255))
			and warehouseId = @warehouseId
		order by operator

End
