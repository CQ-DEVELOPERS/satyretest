﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Inbound_Instruction_Select
  ///   Filename       : p_Pallet_Rebuild_Inbound_Instruction_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Mar 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Inbound_Instruction_Select
(
 @JobId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  select top 1
         ExceptionId,
         Exception as 'Comments',
         Detail    as 'Detail'
    from Exception  (nolock)
    where JobId = @JobId
  order by ExceptionId desc
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Pallet_Rebuild_Inbound_Instruction_Select'); 
    rollback transaction
    return @Error
end
