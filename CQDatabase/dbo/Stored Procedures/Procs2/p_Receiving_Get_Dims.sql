﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Get_Dims
  ///   Filename       : p_Receiving_Get_Dims.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Get_Dims
(
 @receiptLineId int
)

as
begin
	 set nocount on;
	 
	 declare @StorageUnitId int,
	         @WarehouseId   int
  
  select @StorageUnitId = sub.StorageUnitId,
	        @WarehouseId   = r.WarehouseId
	   from ReceiptLine       rl (nolock)
	   join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
	   join Receipt            r (nolock) on rl.ReceiptId = r.ReceiptId
	  where rl.ReceiptLineId = @ReceiptLineId
  
  select p.Length,
         p.Width,
         p.Height,
         p.Weight,
         p.Quantity,
         (select max(Quantity) from Pack pallet where pallet.StorageUnitId = @StorageUnitId and pallet.WarehouseId = @WarehouseId and Pallet.PackTypeId in (select PackTypeId from PackType where InboundSequence = 1)) as 'PalletQuantity'
    from Pack               p (nolock)
    join PackType          pt (nolock) on p.PackTypeId = pt.PackTypeId
   where pt.PackTypeCode = 'Unit'
     and p.StorageUnitId = @StorageUnitId
     and p.WarehouseId   = @WarehouseId
end
 
