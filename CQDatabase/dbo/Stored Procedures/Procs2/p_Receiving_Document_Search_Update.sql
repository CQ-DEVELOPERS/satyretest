﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Document_Search_Update
  ///   Filename       : p_Receiving_Document_Search_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   Searches for documents for receiving 
  /// </remarks>
  /// <param>
  ///   @InboundDocumentTypeId int = null,
  ///   @OrderNumber           nvarchar(30) = null,
  ///   @ExternalCompanyId     int = null,
  ///   @FromDate              datetime,
  ///   @ToDate                datetime
  /// </param>
  /// <returns>
  ///   ReceiptId,
  ///   OrderNumber,
  ///   InboundShipmentId,
  ///   SupplierCode,
  ///   Supplier,
  ///   Status,
  ///   Location,
  ///   DeliveryDate,
  ///   InboundDocumentType,
  ///   QuantityToFollowIndicator
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Document_Search_Update
(
 @ReceiptId         int = null
)

as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        ReceiptId                 int,
        LocationId                int,
        OrderNumber               nvarchar(30),
        InboundShipmentId         int,
        SupplierCode              nvarchar(30),
        Supplier                  nvarchar(255),
        Status                    nvarchar(50),
        Location                  nvarchar(15),
        DeliveryDate              datetime,
        InboundDocumentType       nvarchar(30),
        QuantityToFollowIndicator bit
       );
  
  insert @TableResult
        (ReceiptId,
         LocationId,
         OrderNumber,
         SupplierCode,
         Supplier,
         Status,
         DeliveryDate,
         InboundDocumentType,
         QuantityToFollowIndicator)
  select r.ReceiptId,
         r.LocationId,
         id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         s.Status,
         r.DeliveryDate,
         idt.InboundDocumentType,
         idt.QuantityToFollowIndicator
    from InboundDocument id
    join ExternalCompany     ec  on id.ExternalCompanyId = ec.ExternalCompanyId
    join Receipt             r   on id.InboundDocumentId = r.InboundDocumentId
    join Status              s   on r.StatusId          = s.StatusId
    join InboundDocumentType idt on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
   where r.ReceiptId = @ReceiptId
     and s.Type                      = 'R'
     and s.StatusCode               in ('W','C','D') -- Waiting, Confirmed, Delivered
  
  update r
     set InboundShipmentId = isr.InboundShipmentId
    from @TableResult             r
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
  
  update r
     set Location = l.Location
    from @TableResult r
    join Location     l (nolock) on r.LocationId = l.LocationId
  
  select ReceiptId,
         OrderNumber,
         InboundShipmentId,
         SupplierCode,
         Supplier,
         Status,
         Location,
         DeliveryDate,
         InboundDocumentType,
         QuantityToFollowIndicator
    from @TableResult
end
