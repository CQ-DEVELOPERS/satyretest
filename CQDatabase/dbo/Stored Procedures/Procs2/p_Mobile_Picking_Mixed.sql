﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Picking_Mixed
    ///   Filename       : p_Mobile_Picking_Mixed.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 11 June 2007 14:15:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    ///   @operatorId          int,
    ///   @referenceNumber     nvarchar(30),
    ///   @instructionId       int output
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
    ///   Details        : 
    /// </newpara>
*/
create procedure p_Mobile_Picking_Mixed
(
 @operatorId          int,
 @referenceNumber     nvarchar(30) = null,
 @instructionId       int output,
 @Next                bit = 1
)
as
begin
	 set nocount on
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @WarehouseId        int,
          @PickLocationId     int,
          @ProductCode        nvarchar(30),
          @SKUCode            nvarchar(50),
          @Batch              nvarchar(50),
          @StorageUnitBatchId int,
          @Quantity           int,
          @Weight             numeric(13,3),
          @StatusId           int,
          @InstructionTypeId  int,
          @StoreLocationId    int,
          @PickLocation       nvarchar(15),
          @PickSecurityCode   int,
          @StoreLocation      nvarchar(15),
          @StoreSecurityCode  int,
          @CurrentLine        int,
          @TotalLines         int,
          @DwellTime          int,
          @JobId              int,
          @PalletId           int,
          @StorageUnitId      int,
          @ProductId          int,
          @ProductBarcode     nvarchar(50),
          @PackBarcode        nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @referenceNumber = '' or substring(@referenceNumber, 1, 2) not in ('R:','P:','J:') -- or isnumeric(substring(@referenceNumber, 3, 27)) = 0
  begin
    --set @referenceNumber = null
    set @instructionId = -1
    return 0
  end
  
  if isnumeric(replace(@referenceNumber,'J:','')) = 1
    select @JobId           = replace(@referenceNumber,'J:',''),
           @referenceNumber = null
  
  if isnumeric(replace(@referenceNumber,'P:','')) = 1
    select @PalletId        = replace(@referenceNumber,'P:',''),
           @referenceNumber = null
  
  if @PalletId is not null
    select @JobId = JobId
      from Instruction (nolock)
     where PalletId = @PalletId
  
  if @ReferenceNumber is not null
  begin
    select @JobId = JobId
      from Job (nolock)
     where ReferenceNumber = @referenceNumber
  end
  
  exec @DwellTime = p_Mobile_Dwell_Time
   @Type       = 2,
   @OperatorId = @OperatorId
  
  begin transaction
  
  exec @Error = p_Mobile_Next_Job
   @operatorId          = @operatorId,
   @jobId               = @JobId output,
   @instructionTypeCode = 'PM', -- Pick Mixed
   @referenceNumber     = @referenceNumber
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Mobile_Next_Line
   @JobId         = @JobId,
   @InstructionId = @InstructionId output,
   @OperatorId    = @operatorId,
   @Next          = @Next
  
  if @Error = 99
    set @instructionId = -1
  else
  if @Error <> 0
    goto error
  
  if @instructionId is null
    set @instructionId = -1
  
  commit transaction
  return
  
  error:
    if @instructionId is null
      set @instructionId = -1
    
    rollback transaction
    return @Error
end
