﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Line_Select
  ///   Filename       : p_Planning_Line_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Line_Select
(
 @OutboundShipmentId int = null,
 @IssueId int = null
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   WarehouseId                     int,
   IssueLineId                     int,
   IssueId                         int,
   OrderNumber                     nvarchar(30),
   StorageUnitBatchId              int,
   StorageUnitId                   int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(50),
   BatchId                         int,
   Batch                           nvarchar(50),
   SKUCode                         nvarchar(50),
   SKU                             nvarchar(50),
   OrderQuantity                   float,
   AllocatedQuantity               float,
   Status                          nvarchar(50),
   AvailableQuantity               float,
   OtherAreaTypeQuantity           numeric(13,6),
   AvailabilityIndicator           nvarchar(20),
   AvailablePercentage             numeric(13,2),
   LocationsAllocated              bit default 1,
   PalletQuantity                  float,
   AreaType                        nvarchar(10)
  )
  
  declare @WarehouseId int
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null;
  else
    select @WarehouseId = WarehouseId from OutboundShipment (nolock) where OutboundShipmentId = @OutboundShipmentId
  
  if @IssueId = -1
    set @IssueId = null;
  else
    select @WarehouseId = WarehouseId from Issue (nolock) where IssueId = @IssueId
  
  if @IssueId is not null
  begin
    insert @TableResult
          (WarehouseId,
           IssueLineId,
           IssueId,
           OrderNumber,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           OrderQuantity,
           Status,
           AreaType)
    select i.WarehouseId,
           il.IssueLineId,
           il.IssueId,
           od.OrderNumber,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           ol.Quantity,
           s.Status,
           isnull(i.AreaType,'')
      from IssueLine        il
      join Issue            i   (nolock) on il.IssueId            = i.IssueId
      join OutboundLine     ol  (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument od  (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status           s   (nolock) on il.StatusId           = s.StatusId
     where il.IssueId = @IssueId
  end
  else if @OutboundShipmentId is not null
  begin
    insert @TableResult
          (WarehouseId,
           IssueLineId,
           IssueId,
           OrderNumber,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           OrderQuantity,
           Status,
           AreaType)
    select i.WarehouseId,
           il.IssueLineId,
           il.IssueId,
           od.OrderNumber,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           ol.Quantity,
           s.Status,
           isnull(i.AreaType,'')
      from IssueLine        il
      join Issue            i   (nolock) on il.IssueId            = i.IssueId
      join OutboundLine     ol  (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument od  (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status           s   (nolock) on il.StatusId           = s.StatusId
      join OutboundShipmentIssue osi (nolock) on il.IssueId = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
  end
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         Batch       = b.Batch,
         SKUCode     = sku.SKUCode,
         SKU         = sku.SKU
    from @TableResult tr
    join StorageUnit      su  (nolock) on tr.StorageUnitId      = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch            b   (nolock) on tr.BatchId           = b.BatchId
  
  update tr
     set AllocatedQuantity = (select sum(i.Quantity)
                                from IssueLineInstruction ili (nolock)
                                join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
                               where tr.IssueLineId = ili.IssueLineId
                                 and i.PickLocationId is not null)
    from @TableResult tr
  
  update @TableResult
     set AllocatedQuantity = 0
   where AllocatedQuantity is null
  
  update tr
     set LocationsAllocated = 0
    from @TableResult tr
    join IssueLineInstruction ili (nolock) on tr.IssueLineId = ili.IssueLineId
    join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
   where i.PickLocationId is null
  
  update tr
     set LocationsAllocated = 0
    from @TableResult tr
   where not exists(select top 1 1 from IssueLineInstruction ili (nolock) where tr.IssueLineId = ili.IssueLineId)
  
  update tr
     set AvailableQuantity = (select sum(subl.ActualQuantity - subl.ReservedQuantity)
                                from StorageUnitBatchLocation subl (nolock)
                                join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
                                join AreaLocation               al (nolock) on subl.LocationId         = al.LocationId
                                join Area                        a (nolock) on al.AreaId               = a.AreaId
                               where a.StockOnHand     = 1
                                 and sub.StorageUnitId = tr.StorageUnitId
                                 and a.WarehouseId     = @WarehouseId
                                 and a.AreaCode in ('RK','PK','R','SP','BK')
                                 and isnull(a.AreaType,'') = tr.AreaType)
    from @TableResult tr
  
  update tr
     set OtherAreaTypeQuantity = (select sum(subl.ActualQuantity - subl.ReservedQuantity)
                                    from StorageUnitBatchLocation subl (nolock)
                                    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
                                    join AreaLocation               al (nolock) on subl.LocationId         = al.LocationId
                                    join Area                        a (nolock) on al.AreaId               = a.AreaId
                                   where a.StockOnHand     = 1
                                     and sub.StorageUnitId = tr.StorageUnitId
                                     and a.WarehouseId     = @WarehouseId
                                     and isnull(a.AreaType,'') != tr.AreaType)
    from @TableResult tr
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), AllocatedQuantity + AvailableQuantity) / convert(numeric(13,3), OrderQuantity)) * 100
   where AllocatedQuantity + AvailableQuantity != 0
     and OrderQuantity > 0
  
  update @TableResult
     set AvailableQuantity = AvailableQuantity + AllocatedQuantity
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where AvailableQuantity <= 0
  
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where AvailableQuantity > 0
  
  update @TableResult
     set AvailabilityIndicator = 'Green'
   where (AvailableQuantity >= OrderQuantity
      or OrderQuantity = AllocatedQuantity)
  
  update tr
     set PalletQuantity = p.Quantity
    from @TableResult   tr
    join Pack            p (nolock) on tr.StorageUnitId    = p.StorageUnitId
                                   and tr.WarehouseId      = p.WarehouseId
    join PackType       pt (nolock) on p.PackTypeId        = pt.PackTypeId
                                   and pt.OutboundSequence = 1
  
  select IssueLineId,
         IssueId,
         OrderNumber,
         StorageUnitBatchId,
         StorageUnitId,
         ProductCode,
         Product,
         Batch,
         SKUCode,
         SKU,
         OrderQuantity,
         Status,
         AvailabilityIndicator,
         AvailablePercentage,
         AvailableQuantity,
         LocationsAllocated,
         PalletQuantity,
         OtherAreaTypeQuantity,
         null as 'PalletiseError'
    from @TableResult
  order by case when isnull(PalletQuantity,0) = 0
                then 0
                else 1
                end,
           
           ProductCode,
           Product,
           Batch,
           SKUCode,
           OrderQuantity
end
