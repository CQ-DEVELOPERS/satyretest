﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocumentType_Return
  ///   Filename       : p_OutboundDocumentType_Return.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2009 17:56:49
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundDocumentType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OutboundDocumentType.OutboundDocumentTypeId,
  ///   OutboundDocumentType.OutboundDocumentType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocumentType_Return

as
begin
	 set nocount on;
  
  select OutboundDocumentType.OutboundDocumentTypeId,
         OutboundDocumentType.OutboundDocumentType 
    from OutboundDocumentType
   where OutboundDocumentTypeCode = 'RET'  
end
