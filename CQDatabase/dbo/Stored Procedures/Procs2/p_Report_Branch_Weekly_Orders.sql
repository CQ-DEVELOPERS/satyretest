﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Branch_Weekly_Orders
  ///   Filename       : p_Report_Branch_Weekly_Orders.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jun 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Branch_Weekly_Orders
(
 @ConnectionString nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @WarehouseId      int
)

as
begin
	 set nocount on;
  
  select datepart(wk, CreateDate) as 'Week',
         convert(nvarchar(10), dateadd(dd, (datepart(dw, CreateDate) * -1) + 2, CreateDate), 120) as 'Week Start',
         CustomerCode,
         CustomerName,
         ProductCode,
         Product,
         SKUCode,
         sum(Quantity) as 'Ordered',
         sum(ConfirmedQuantity) as 'Invoiced'
    from viewOrder where CustomerCode like 'ZA%'
  group by datepart(wk, CreateDate),
           convert(nvarchar(10), dateadd(dd, (datepart(dw, CreateDate) * -1) + 2, CreateDate), 120),
           CustomerCode,
           CustomerName,
           ProductCode,
           Product,
           SKUCode
end
