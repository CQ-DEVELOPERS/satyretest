﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Order_Number_Search
  ///   Filename       : p_OutboundDocument_Order_Number_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Order_Number_Search
(
 @WarehouseId int
)

as
begin
	 set nocount on;
  
  select OrderNumber
    from OutboundDocument      od
    join OutboundDocumentType odt on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where od.WarehouseId = @WarehouseId
     and odt.OutboundDocumentTypeCode = 'SAL'
end
