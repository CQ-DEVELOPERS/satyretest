﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorTransactionSummary_Insert
  ///   Filename       : p_OperatorTransactionSummary_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:32
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorTransactionSummary table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @InstructionTypeId int = null,
  ///   @EndDate datetime = null,
  ///   @Units int = null,
  ///   @Weight float = null,
  ///   @Instructions int = null,
  ///   @Jobs int = null,
  ///   @ActiveTime int = null,
  ///   @DwellTime int = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  ///   OperatorTransactionSummary.OperatorId,
  ///   OperatorTransactionSummary.InstructionTypeId,
  ///   OperatorTransactionSummary.EndDate,
  ///   OperatorTransactionSummary.Units,
  ///   OperatorTransactionSummary.Weight,
  ///   OperatorTransactionSummary.Instructions,
  ///   OperatorTransactionSummary.Jobs,
  ///   OperatorTransactionSummary.ActiveTime,
  ///   OperatorTransactionSummary.DwellTime,
  ///   OperatorTransactionSummary.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorTransactionSummary_Insert
(
 @OperatorId int = null,
 @InstructionTypeId int = null,
 @EndDate datetime = null,
 @Units int = null,
 @Weight float = null,
 @Instructions int = null,
 @Jobs int = null,
 @ActiveTime int = null,
 @DwellTime int = null,
 @WarehouseId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorTransactionSummary
        (OperatorId,
         InstructionTypeId,
         EndDate,
         Units,
         Weight,
         Instructions,
         Jobs,
         ActiveTime,
         DwellTime,
         WarehouseId)
  select @OperatorId,
         @InstructionTypeId,
         @EndDate,
         @Units,
         @Weight,
         @Instructions,
         @Jobs,
         @ActiveTime,
         @DwellTime,
         @WarehouseId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
