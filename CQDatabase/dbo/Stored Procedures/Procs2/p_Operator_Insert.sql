﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Insert
  ///   Filename       : p_Operator_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 13:19:56
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Operator table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null output,
  ///   @OperatorGroupId int = null,
  ///   @WarehouseId int = null,
  ///   @Operator nvarchar(100) = null,
  ///   @OperatorCode nvarchar(100) = null,
  ///   @Password nvarchar(100) = null,
  ///   @NewPassword nvarchar(100) = null,
  ///   @ActiveIndicator bit = null,
  ///   @ExpiryDate datetime = null,
  ///   @LastInstruction datetime = null,
  ///   @Printer nvarchar(100) = null,
  ///   @Port nvarchar(100) = null,
  ///   @IPAddress nvarchar(100) = null,
  ///   @CultureId int = null,
  ///   @LoginTime datetime = null,
  ///   @LogoutTime datetime = null,
  ///   @LastActivity datetime = null,
  ///   @SiteType nvarchar(40) = null,
  ///   @Name nvarchar(200) = null,
  ///   @Surname nvarchar(200) = null,
  ///   @PrincipalId int = null,
  ///   @PickAisle nvarchar(20) = null,
  ///   @StoreAisle nvarchar(20) = null,
  ///   @MenuId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @Upload bit = null 
  /// </param>
  /// <returns>
  ///   Operator.OperatorId,
  ///   Operator.OperatorGroupId,
  ///   Operator.WarehouseId,
  ///   Operator.Operator,
  ///   Operator.OperatorCode,
  ///   Operator.Password,
  ///   Operator.NewPassword,
  ///   Operator.ActiveIndicator,
  ///   Operator.ExpiryDate,
  ///   Operator.LastInstruction,
  ///   Operator.Printer,
  ///   Operator.Port,
  ///   Operator.IPAddress,
  ///   Operator.CultureId,
  ///   Operator.LoginTime,
  ///   Operator.LogoutTime,
  ///   Operator.LastActivity,
  ///   Operator.SiteType,
  ///   Operator.Name,
  ///   Operator.Surname,
  ///   Operator.PrincipalId,
  ///   Operator.PickAisle,
  ///   Operator.StoreAisle,
  ///   Operator.MenuId,
  ///   Operator.ExternalCompanyId,
  ///   Operator.Upload 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Insert
(
 @OperatorId int = null output,
 @OperatorGroupId int = null,
 @WarehouseId int = null,
 @Operator nvarchar(100) = null,
 @OperatorCode nvarchar(100) = null,
 @Password nvarchar(100) = null,
 @NewPassword nvarchar(100) = null,
 @ActiveIndicator bit = null,
 @ExpiryDate datetime = null,
 @LastInstruction datetime = null,
 @Printer nvarchar(100) = null,
 @Port nvarchar(100) = null,
 @IPAddress nvarchar(100) = null,
 @CultureId int = null,
 @LoginTime datetime = null,
 @LogoutTime datetime = null,
 @LastActivity datetime = null,
 @SiteType nvarchar(40) = null,
 @Name nvarchar(200) = null,
 @Surname nvarchar(200) = null,
 @PrincipalId int = null,
 @PickAisle nvarchar(20) = null,
 @StoreAisle nvarchar(20) = null,
 @MenuId int = null,
 @ExternalCompanyId int = null,
 @Upload bit = null 
)

as
begin
	 set nocount on;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @Operator = '-1'
    set @Operator = null;
  
  if @OperatorCode = '-1'
    set @OperatorCode = null;
  
  if @CultureId = '-1'
    set @CultureId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @MenuId = '-1'
    set @MenuId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
	 declare @Error int
 
  insert Operator
        (OperatorGroupId,
         WarehouseId,
         Operator,
         OperatorCode,
         Password,
         NewPassword,
         ActiveIndicator,
         ExpiryDate,
         LastInstruction,
         Printer,
         Port,
         IPAddress,
         CultureId,
         LoginTime,
         LogoutTime,
         LastActivity,
         SiteType,
         Name,
         Surname,
         PrincipalId,
         PickAisle,
         StoreAisle,
         MenuId,
         ExternalCompanyId,
         Upload)
  select @OperatorGroupId,
         @WarehouseId,
         @Operator,
         @OperatorCode,
         @Password,
         @NewPassword,
         @ActiveIndicator,
         @ExpiryDate,
         @LastInstruction,
         @Printer,
         @Port,
         @IPAddress,
         @CultureId,
         @LoginTime,
         @LogoutTime,
         @LastActivity,
         @SiteType,
         @Name,
         @Surname,
         @PrincipalId,
         @PickAisle,
         @StoreAisle,
         @MenuId,
         @ExternalCompanyId,
         @Upload 
  
  select @Error = @@Error, @OperatorId = scope_identity()
  
  if @Error = 0
    exec @Error = p_OperatorHistory_Insert
         @OperatorId = @OperatorId,
         @OperatorGroupId = @OperatorGroupId,
         @WarehouseId = @WarehouseId,
         @Operator = @Operator,
         @OperatorCode = @OperatorCode,
         @Password = @Password,
         @NewPassword = @NewPassword,
         @ActiveIndicator = @ActiveIndicator,
         @ExpiryDate = @ExpiryDate,
         @LastInstruction = @LastInstruction,
         @Printer = @Printer,
         @Port = @Port,
         @IPAddress = @IPAddress,
         @CultureId = @CultureId,
         @LoginTime = @LoginTime,
         @LogoutTime = @LogoutTime,
         @LastActivity = @LastActivity,
         @SiteType = @SiteType,
         @Name = @Name,
         @Surname = @Surname,
         @PrincipalId = @PrincipalId,
         @PickAisle = @PickAisle,
         @StoreAisle = @StoreAisle,
         @MenuId = @MenuId,
         @ExternalCompanyId = @ExternalCompanyId,
         @Upload = @Upload,
         @CommandType = 'Insert'
  
  return @Error
  
end
