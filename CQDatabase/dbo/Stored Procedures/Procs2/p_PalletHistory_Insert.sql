﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletHistory_Insert
  ///   Filename       : p_PalletHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:28
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PalletHistory table.
  /// </remarks>
  /// <param>
  ///   @PalletId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @LocationId int = null,
  ///   @Weight float = null,
  ///   @Tare float = null,
  ///   @Quantity float = null,
  ///   @Prints int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @Nett float = null,
  ///   @ReasonId int = null,
  ///   @EmptyWeight float = null 
  /// </param>
  /// <returns>
  ///   PalletHistory.PalletId,
  ///   PalletHistory.StorageUnitBatchId,
  ///   PalletHistory.LocationId,
  ///   PalletHistory.Weight,
  ///   PalletHistory.Tare,
  ///   PalletHistory.Quantity,
  ///   PalletHistory.Prints,
  ///   PalletHistory.CommandType,
  ///   PalletHistory.InsertDate,
  ///   PalletHistory.CreateDate,
  ///   PalletHistory.Nett,
  ///   PalletHistory.ReasonId,
  ///   PalletHistory.EmptyWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletHistory_Insert
(
 @PalletId int = null,
 @StorageUnitBatchId int = null,
 @LocationId int = null,
 @Weight float = null,
 @Tare float = null,
 @Quantity float = null,
 @Prints int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @CreateDate datetime = null,
 @Nett float = null,
 @ReasonId int = null,
 @EmptyWeight float = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert PalletHistory
        (PalletId,
         StorageUnitBatchId,
         LocationId,
         Weight,
         Tare,
         Quantity,
         Prints,
         CommandType,
         InsertDate,
         CreateDate,
         Nett,
         ReasonId,
         EmptyWeight)
  select @PalletId,
         @StorageUnitBatchId,
         @LocationId,
         @Weight,
         @Tare,
         @Quantity,
         @Prints,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @CreateDate,
         @Nett,
         @ReasonId,
         @EmptyWeight 
  
  select @Error = @@Error
  
  
  return @Error
  
end
