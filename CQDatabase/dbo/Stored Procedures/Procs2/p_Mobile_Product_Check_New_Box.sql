﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_New_Box
  ///   Filename       : p_Mobile_Product_Check_New_Box.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Aug 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_New_Box
(
 @jobId             int = null,
 @operatorId        int         = null,
 @referenceNumber   varchar(30) = null,
 @Instructionid     int = null
)

as
begin
	 set nocount on;
	 
	 if @jobId is null and @InstructionId is not null
	   select @jobId = JobId
	     from Instruction i (nolock)
	    where InstructionId = @InstructionId
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         JobId,
         OperatorId,
         ReferenceNumber,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @JobId,
         @OperatorId,
         @ReferenceNumber,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @NewJobId          int
  
  set @Errormsg = 'Error executing p_Mobile_Product_Check_New_Box'
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if exists(select top 1 1 from Job where ReferenceNumber = @ReferenceNumber)
  begin
    set @Errormsg = @Errormsg + ' referenceNumber already used.'
    set @Error = -1
    set @NewJobId = -2
    goto error
  end
  
  if not exists(select top 1 1 from Instruction where JobId = @jobId and isnull(CheckQuantity,0) != isnull(ConfirmedQuantity,0))
  begin
    set @Errormsg = @Errormsg + ' Nothing to put into new box.'
    set @Error = -1
    set @NewJobId = -3
    goto error
  end
  
  insert Job
        (PriorityId,
         OperatorId,
         StatusId,
         WarehouseId,
         ReferenceNumber,
         CheckedBy,
         CheckedDate)
  select PriorityId,
         OperatorId,
         case when @InstructionId is null 
              then dbo.ufn_StatusId('IS','CK')
              else dbo.ufn_StatusId('IS','S')
              end,
         WarehouseId,
         @referenceNumber,
         @operatorId,
         @GetDate
    from Job
   where JobId = @JobId
  
  select @Error = @@Error, @NewJobId = scope_identity()
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Job_Renumber
   @JobId = @jobId
  
  if @Error <> 0
    goto error
  
  -- insert remaining lines into new job
  if @InstructionId is null
  begin
    insert Instruction
          (InstructionTypeId,
           StorageUnitBatchId,
           WarehouseId,
           StatusId,
           JobId,
           OperatorId,
           PickLocationId,
           StoreLocationId,
           ReceiptLineId,
           IssueLineId,
           InstructionRefId,
           Quantity,
           ConfirmedQuantity,
           Weight,
           ConfirmedWeight,
           PalletId,
           CreateDate,
           StartDate,
           EndDate,
           Picked,
           Stored,
           CheckQuantity,
           CheckWeight,
           OutboundShipmentId,
           DropSequence)
    select InstructionTypeId,
           StorageUnitBatchId,
           WarehouseId,
           StatusId,
           @NewJobId,
           OperatorId,
           PickLocationId,
           StoreLocationId,
           ReceiptLineId,
           IssueLineId,
           isnull(InstructionRefId, InstructionId),
           Quantity - CheckQuantity,
           ConfirmedQuantity - CheckQuantity,
           null,
           null,
           PalletId,
           CreateDate,
           StartDate,
           EndDate,
           Picked,
           Stored,
           0,
           0,
           OutboundShipmentId,
           DropSequence
      from Instruction
     where JobId = @JobId
       and ConfirmedQuantity > CheckQuantity
       and CheckQuantity > 0 -- Only those which were checked - unchecked will be handled below
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    -- now update the original job and take off extra qty - leave only what was checked
    update Instruction
       set Quantity = CheckQuantity,
           ConfirmedQuantity = CheckQuantity
     where JobId = @jobId
       and ConfirmedQuantity > CheckQuantity
       and CheckQuantity > 0 -- Only those which were checked - unchecked will be handled below
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    -- move any unchecked original jobs to the new job
    update Instruction
       set JobId = @NewJobId
     where JobId = @jobId
       and isnull(CheckQuantity,0) = 0
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    delete CheckingProduct
     where JobId = @jobId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  else
  begin
    --insert Instruction
    --      (InstructionTypeId,
    --       StorageUnitBatchId,
    --       WarehouseId,
    --       StatusId,
    --       JobId,
    --       OperatorId,
    --       PickLocationId,
    --       StoreLocationId,
    --       ReceiptLineId,
    --       IssueLineId,
    --       InstructionRefId,
    --       Quantity,
    --       ConfirmedQuantity,
    --       Weight,
    --       ConfirmedWeight,
    --       PalletId,
    --       CreateDate,
    --       StartDate,
    --       EndDate,
    --       Picked,
    --       Stored,
    --       CheckQuantity,
    --       CheckWeight,
    --       OutboundShipmentId,
    --       DropSequence)
    --select InstructionTypeId,
    --       StorageUnitBatchId,
    --       WarehouseId,
    --       i.StatusId,
    --       @NewJobId,
    --       OperatorId,
    --       PickLocationId,
    --       StoreLocationId,
    --       ReceiptLineId,
    --       IssueLineId,
    --       isnull(InstructionRefId, InstructionId),
    --       Quantity,
    --       ConfirmedQuantity,
    --       null,
    --       null,
    --       PalletId,
    --       CreateDate,
    --       StartDate,
    --       EndDate,
    --       Picked,
    --       Stored,
    --       0,
    --       0,
    --       OutboundShipmentId,
    --       DropSequence
    --  from Instruction i
    --  join Status      s on i.StatusId = s.StatusId
    -- where i.JobId = @JobId
    --   and s.StatusCode in ('W','S')
    
    --select @Error = @@Error
    
    --if @Error <> 0
    --  goto error
    
    -- move any unpicked original jobs to the new job
    update i
       set JobId = @NewJobId
      from Instruction i
      join Status      s on i.StatusId = s.StatusId
     where JobId = @jobId
       and s.StatusCode in ('W','S')
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
--select * from viewINS where JobId = @jobId
--select * from viewINS where ReferenceNumber = @ReferenceNumber
  commit transaction
  
  exec @Error = p_Status_Rollup
   @jobId = @jobId
  
  if @Error <> 0
    goto error
  
  update MobileLog
     set EndDate  = getdate(),
         ErrorMsg = 'Successful'
   where MobileLogId = @MobileLogId
  
  select @NewJobId
  return @NewJobId
  
  error:
    rollback transaction
    
    update MobileLog
       set EndDate  = getdate(),
           ErrorMsg = @Errormsg
     where MobileLogId = @MobileLogId
    
    select @NewJobId
    RAISERROR (900000,-1,-1, @Errormsg);
    return @Error
end
