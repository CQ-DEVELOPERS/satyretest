﻿
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Pallet_Link
  ///   Filename       : p_Mobile_Pallet_Link.sql
  ///   Create By      : Willis
  ///   Date Created   : Sep 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   @operatorId        int,
  ///   @palletId          int,
  ///   @pickLocation      nvarchar(15),
  ///   @confirmedQuantity float,
  ///   @storeLocation     nvarchar(15)
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Pallet_Link
(
 @operatorId          int,
 @storageUnitBatchId  int,
 @PalletId            nvarchar(20),
 @storeLocation       nvarchar(15),
 @confirmedQuantity   float,
 @InstructionTypeCode nvarchar(10)
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @DwellTime          int,
          @ProductCode        nvarchar(30),
          @Product            nvarchar(50),
          @SKUCode            nvarchar(50),
          @Batch              nvarchar(50),
          @PickSecurityCode   int,
          @LocationId    int,
          @StoreSecurityCode  int,
          @StatusId           int,
          @InstructionTypeId  int,
          @Quantity           float,
          @WarehouseId        int,
          @JobId              int,
          @Weight             float,
          @InstructionId      int,
          @PriorityId         int,
          @bool               bit
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @storeLocation = replace(@storeLocation,'L:','')
  select @PalletId = replace(@PalletId, 'P:','')
  
  set @Error = 0
  
  select @WarehouseId = WarehouseId
    from Operator (nolock)
   where OperatorId = @operatorId

  select top 1
         @LocationId = l.LocationId
    from Location      l (nolock)
    join AreaLocation al (nolock) on al.LocationId = l.LocationId
    join Area          a (nolock) on al.AreaId = a.AreaId
   where l.Location = @storeLocation
     and a.WarehouseId = @WarehouseId
  
  begin transaction
  
  If @LocationId is null
  Begin
     Set @Error = 3
     Goto Result
  End
  set @Quantity = @confirmedQuantity  
  
  select @InstructionTypeId = InstructionTypeId,
         @PriorityId        = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = @InstructionTypeCode
  
  if @Quantity < 0 or @Quantity is null
  begin
    set @Error = 2
    goto Result
  end
  
  if @confirmedQuantity is null
    set @confirmedQuantity = 0
 
  if @confirmedQuantity > @Quantity 
  begin
    set @Error = 2
    goto Result
  end
  
  set @StatusId = dbo.ufn_StatusId('I','W')
  
  exec @Error = p_Job_Insert
   @JobId       = @JobId output,
   @PriorityId  = @PriorityId,
   @OperatorId  = @OperatorId,
   @StatusId    = @StatusId,
   @WarehouseId = @WarehouseId
  
  if @Error <> 0 or @JobId is null
  begin
    set @InstructionId = -1
    goto Result
  end
  
  exec @Error = p_Instruction_Insert
   @InstructionId       = @InstructionId output,
   @InstructionTypeId   = @InstructionTypeId,
   @StorageUnitBatchId  = @StorageUnitBatchId,
   @WarehouseId         = @WarehouseId,
   @StatusId            = @StatusId,
   @JobId               = @JobId,
   @OperatorId          = @OperatorId,
   @StoreLocationId     = @LocationId,
   @InstructionRefId    = null,
   @Quantity            = @confirmedQuantity,
   @ConfirmedQuantity   = @confirmedQuantity,
   @Weight              = @Weight,
   @ConfirmedWeight     = 0,
   @PalletId            = @PalletId,
   @CreateDate          = @GetDate
  
  if @Error <> 0 or @InstructionId is null
  begin
    set @InstructionId = -1
    goto Result
  end
  
  update Pallet
    set StorageUnitBatchId = @storageUnitBatchId,
        LocationId         = @LocationId,
        Quantity           = @Quantity
   where PalletId           = @PalletId
    and StorageUnitBatchId is null
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId = @InstructionId
  
  if @Error <> 0
    goto Result
  
  exec @Error = p_Instruction_Started
   @InstructionId = @InstructionId,
   @operatorId    = @operatorId
  
  if @Error <> 0
    goto Result
  
  exec @Error = p_StorageUnitBatchLocation_Allocate
   @InstructionId = @InstructionId
  
  if @Error <> 0
    goto Result
  
  exec @Error = p_Instruction_Finished
   @InstructionId = @InstructionId,
   @operatorId    = @operatorId
  
  if @Error <> 0
    goto Result
  
  Result:
    if @InstructionId is null
      set @InstructionId = -1
    
    if @Error <> 0 or @InstructionId = -1
     Begin
      rollback transaction
      set @bool = 0
     End
    else
     Begin
      commit transaction
      set @bool = 1
     End
    
    Select @bool
    return @bool
end
