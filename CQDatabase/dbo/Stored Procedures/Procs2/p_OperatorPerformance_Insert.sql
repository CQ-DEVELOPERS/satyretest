﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorPerformance_Insert
  ///   Filename       : p_OperatorPerformance_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:26
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorPerformance table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @InstructionTypeId int = null,
  ///   @EndDate datetime = null,
  ///   @ActivityStatus char(1) = null,
  ///   @PickingRate int = null,
  ///   @Units int = null,
  ///   @Weight float = null,
  ///   @Instructions int = null,
  ///   @Orders int = null,
  ///   @OrderLines int = null,
  ///   @Jobs int = null,
  ///   @ActiveTime int = null,
  ///   @DwellTime int = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  ///   OperatorPerformance.OperatorId,
  ///   OperatorPerformance.InstructionTypeId,
  ///   OperatorPerformance.EndDate,
  ///   OperatorPerformance.ActivityStatus,
  ///   OperatorPerformance.PickingRate,
  ///   OperatorPerformance.Units,
  ///   OperatorPerformance.Weight,
  ///   OperatorPerformance.Instructions,
  ///   OperatorPerformance.Orders,
  ///   OperatorPerformance.OrderLines,
  ///   OperatorPerformance.Jobs,
  ///   OperatorPerformance.ActiveTime,
  ///   OperatorPerformance.DwellTime,
  ///   OperatorPerformance.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorPerformance_Insert
(
 @OperatorId int = null,
 @InstructionTypeId int = null,
 @EndDate datetime = null,
 @ActivityStatus char(1) = null,
 @PickingRate int = null,
 @Units int = null,
 @Weight float = null,
 @Instructions int = null,
 @Orders int = null,
 @OrderLines int = null,
 @Jobs int = null,
 @ActiveTime int = null,
 @DwellTime int = null,
 @WarehouseId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorPerformance
        (OperatorId,
         InstructionTypeId,
         EndDate,
         ActivityStatus,
         PickingRate,
         Units,
         Weight,
         Instructions,
         Orders,
         OrderLines,
         Jobs,
         ActiveTime,
         DwellTime,
         WarehouseId)
  select @OperatorId,
         @InstructionTypeId,
         @EndDate,
         @ActivityStatus,
         @PickingRate,
         @Units,
         @Weight,
         @Instructions,
         @Orders,
         @OrderLines,
         @Jobs,
         @ActiveTime,
         @DwellTime,
         @WarehouseId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
