﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReceiptLinePackaging_Insert
  ///   Filename       : p_ReceiptLinePackaging_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:19
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ReceiptLinePackaging table.
  /// </remarks>
  /// <param>
  ///   @PackageLineId int = null,
  ///   @ReceiptId int = null,
  ///   @InboundLineId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @RequiredQuantity float = null,
  ///   @ReceivedQuantity float = null,
  ///   @AcceptedQuantity float = null,
  ///   @RejectQuantity float = null,
  ///   @DeliveryNoteQuantity float = null,
  ///   @jobid int = null,
  ///   @ReceiptLineId int = null 
  /// </param>
  /// <returns>
  ///   ReceiptLinePackaging.PackageLineId,
  ///   ReceiptLinePackaging.ReceiptId,
  ///   ReceiptLinePackaging.InboundLineId,
  ///   ReceiptLinePackaging.StorageUnitBatchId,
  ///   ReceiptLinePackaging.StatusId,
  ///   ReceiptLinePackaging.OperatorId,
  ///   ReceiptLinePackaging.RequiredQuantity,
  ///   ReceiptLinePackaging.ReceivedQuantity,
  ///   ReceiptLinePackaging.AcceptedQuantity,
  ///   ReceiptLinePackaging.RejectQuantity,
  ///   ReceiptLinePackaging.DeliveryNoteQuantity,
  ///   ReceiptLinePackaging.jobid,
  ///   ReceiptLinePackaging.ReceiptLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReceiptLinePackaging_Insert
(
 @PackageLineId int = null,
 @ReceiptId int = null,
 @InboundLineId int = null,
 @StorageUnitBatchId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @RequiredQuantity float = null,
 @ReceivedQuantity float = null,
 @AcceptedQuantity float = null,
 @RejectQuantity float = null,
 @DeliveryNoteQuantity float = null,
 @jobid int = null,
 @ReceiptLineId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ReceiptLinePackaging
        (PackageLineId,
         ReceiptId,
         InboundLineId,
         StorageUnitBatchId,
         StatusId,
         OperatorId,
         RequiredQuantity,
         ReceivedQuantity,
         AcceptedQuantity,
         RejectQuantity,
         DeliveryNoteQuantity,
         jobid,
         ReceiptLineId)
  select @PackageLineId,
         @ReceiptId,
         @InboundLineId,
         @StorageUnitBatchId,
         @StatusId,
         @OperatorId,
         @RequiredQuantity,
         @ReceivedQuantity,
         @AcceptedQuantity,
         @RejectQuantity,
         @DeliveryNoteQuantity,
         @jobid,
         @ReceiptLineId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
