﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PerformanceLog_Delete
  ///   Filename       : p_PerformanceLog_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:57
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the PerformanceLog table.
  /// </remarks>
  /// <param>
  ///   @PerformanceLogId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PerformanceLog_Delete
(
 @PerformanceLogId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete PerformanceLog
     where PerformanceLogId = @PerformanceLogId
  
  select @Error = @@Error
  
  
  return @Error
  
end
