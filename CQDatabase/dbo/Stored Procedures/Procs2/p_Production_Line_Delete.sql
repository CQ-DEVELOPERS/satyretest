﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Line_Delete
  ///   Filename       : p_Production_Line_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Line_Delete
(
 @InstructionId int,
 @JobId         int = null,
 @OperatorId    int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @StatusCode        nvarchar(10),
          @WarehouseId       int,
          @Exception         nvarchar(255),
          @Operator          nvarchar(50),
          @PalletId          nvarchar(15)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('Z','Z')
  
  select @Operator = Operator
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  select @WarehouseId = WarehouseId,
         @PalletId    = convert(nvarchar(15), @PalletId)
    from Instruction
   where InstructionId = @InstructionId
  
  select @StatusCode = s.StatusCode
    from Job    j
    join Status s on j.StatusId = s.StatusId
   where j.JobId = @JobId
  
  begin transaction
  
  if (select dbo.ufn_Configuration(192, @WarehouseId)) = 0
  begin
  if (@StatusCode) not in ('W','RE')
    begin
      set @Error = -1
      goto error
    end
  end
  
  if (select dbo.ufn_Configuration(210, @WarehouseId)) = 1 and @StatusCode = 'PR'
  begin
    exec @Error = p_Production_Update_Accepted_Quantity
     @InstructionId = @InstructionId,
     @Sign          = '-'
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId       = @InstructionId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @StatusId      = @StatusId,
   @OperatorId    = @OperatorId
  
  if @Error <> 0
    goto error
  
  if (select count(1) from Instruction where JobId = @JobId) = 1
  begin
    exec @Error = p_Job_Update
     @JobId = @JobId,
     @StatusId = @StatusId,
     @OperatorId = @OperatorId
    
    if @Error <> 0
      goto error
  end
  
  select @Exception = 'Pallet ' + isnull(@PalletId, 'null') + ' deleted by ' + isnull(@Operator, '')  + '.'
  
  exec @Error = p_Exception_Insert
   @ExceptionId   = null,
   @ExceptionCode = 'PRDEL',
   @Exception     = @Exception,
   @InstructionId = @InstructionId,
   @OperatorId    = @OperatorId,
   @JobId         = @JobId,
   @CreateDate    = @GetDate,
   @ExceptionDate = @Getdate
 
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Production_Line_Delete'); 
    rollback transaction
    return @Error
end
