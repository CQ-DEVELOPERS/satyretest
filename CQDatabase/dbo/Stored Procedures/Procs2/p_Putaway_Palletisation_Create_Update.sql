﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Putaway_Palletisation_Create_Update
  ///   Filename       : p_Putaway_Palletisation_Create_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Putaway_Palletisation_Create_Update
(
 @InstructionId int,
 @Quantity      float
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @JobId             int,
          @SplitTotal        int
  
  begin transaction
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @Quantity      = @Quantity
  
  if @Error <> 0
    goto error
  
  commit transaction
  
  select @JobId = JobId
    from Instruction
   where InstructionId = @InstructionId
  
  select @SplitTotal = sum(Quantity)
    from Instruction i (nolock)
    join Status      s (nolock) on i.StatusId = s.StatusId
   where i.JobId       = @JobId
     and s.StatusCode != 'D'
     and s.Type        = 'I'
  
  select @SplitTotal as 'SplitTotal';
  select @SplitTotal as 'SplitTotal';
  return;
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Putaway_Palletisation_Create_Update'); 
    rollback transaction
    return @Error
end
