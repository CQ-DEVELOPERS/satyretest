﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReceiptLinePackaging_Update
  ///   Filename       : p_ReceiptLinePackaging_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:20
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ReceiptLinePackaging table.
  /// </remarks>
  /// <param>
  ///   @PackageLineId int = null,
  ///   @ReceiptId int = null,
  ///   @InboundLineId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @RequiredQuantity float = null,
  ///   @ReceivedQuantity float = null,
  ///   @AcceptedQuantity float = null,
  ///   @RejectQuantity float = null,
  ///   @DeliveryNoteQuantity float = null,
  ///   @jobid int = null,
  ///   @ReceiptLineId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReceiptLinePackaging_Update
(
 @PackageLineId int = null,
 @ReceiptId int = null,
 @InboundLineId int = null,
 @StorageUnitBatchId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @RequiredQuantity float = null,
 @ReceivedQuantity float = null,
 @AcceptedQuantity float = null,
 @RejectQuantity float = null,
 @DeliveryNoteQuantity float = null,
 @jobid int = null,
 @ReceiptLineId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update ReceiptLinePackaging
     set PackageLineId = isnull(@PackageLineId, PackageLineId),
         ReceiptId = isnull(@ReceiptId, ReceiptId),
         InboundLineId = isnull(@InboundLineId, InboundLineId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         StatusId = isnull(@StatusId, StatusId),
         OperatorId = isnull(@OperatorId, OperatorId),
         RequiredQuantity = isnull(@RequiredQuantity, RequiredQuantity),
         ReceivedQuantity = isnull(@ReceivedQuantity, ReceivedQuantity),
         AcceptedQuantity = isnull(@AcceptedQuantity, AcceptedQuantity),
         RejectQuantity = isnull(@RejectQuantity, RejectQuantity),
         DeliveryNoteQuantity = isnull(@DeliveryNoteQuantity, DeliveryNoteQuantity),
         jobid = isnull(@jobid, jobid),
         ReceiptLineId = isnull(@ReceiptLineId, ReceiptLineId) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
