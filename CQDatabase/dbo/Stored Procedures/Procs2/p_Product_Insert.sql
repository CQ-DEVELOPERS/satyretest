﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Insert
  ///   Filename       : p_Product_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jul 2014 14:16:29
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null output,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(510) = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @MinimumQuantity float = null,
  ///   @ReorderQuantity float = null,
  ///   @MaximumQuantity float = null,
  ///   @CuringPeriodDays int = null,
  ///   @ShelfLifeDays int = null,
  ///   @QualityAssuranceIndicator bit = null,
  ///   @ProductType nvarchar(40) = null,
  ///   @OverReceipt numeric(13,3) = null,
  ///   @HostId nvarchar(60) = null,
  ///   @RetentionSamples int = null,
  ///   @Samples int = null,
  ///   @ParentProductCode nvarchar(60) = null,
  ///   @DangerousGoodsId int = null,
  ///   @Description2 nvarchar(510) = null,
  ///   @Description3 nvarchar(510) = null,
  ///   @PrincipalId int = null,
  ///   @AssaySamples float = null,
  ///   @Category nvarchar(100) = null,
  ///   @ProductCategoryId int = null,
  ///   @ProductAlias nvarchar(100) = null,
  ///   @ProductGroup nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   Product.ProductId,
  ///   Product.StatusId,
  ///   Product.ProductCode,
  ///   Product.Product,
  ///   Product.Barcode,
  ///   Product.MinimumQuantity,
  ///   Product.ReorderQuantity,
  ///   Product.MaximumQuantity,
  ///   Product.CuringPeriodDays,
  ///   Product.ShelfLifeDays,
  ///   Product.QualityAssuranceIndicator,
  ///   Product.ProductType,
  ///   Product.OverReceipt,
  ///   Product.HostId,
  ///   Product.RetentionSamples,
  ///   Product.Samples,
  ///   Product.ParentProductCode,
  ///   Product.DangerousGoodsId,
  ///   Product.Description2,
  ///   Product.Description3,
  ///   Product.PrincipalId,
  ///   Product.AssaySamples,
  ///   Product.Category,
  ///   Product.ProductCategoryId,
  ///   Product.ProductAlias,
  ///   Product.ProductGroup 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Product_Insert
(
 @ProductId int = null output,
 @StatusId int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(510) = null,
 @Barcode nvarchar(100) = null,
 @MinimumQuantity float = null,
 @ReorderQuantity float = null,
 @MaximumQuantity float = null,
 @CuringPeriodDays int = null,
 @ShelfLifeDays int = null,
 @QualityAssuranceIndicator bit = null,
 @ProductType nvarchar(40) = null,
 @OverReceipt numeric(13,3) = null,
 @HostId nvarchar(60) = null,
 @RetentionSamples int = null,
 @Samples int = null,
 @ParentProductCode nvarchar(60) = null,
 @DangerousGoodsId int = null,
 @Description2 nvarchar(510) = null,
 @Description3 nvarchar(510) = null,
 @PrincipalId int = null,
 @AssaySamples float = null,
 @Category nvarchar(100) = null,
 @ProductCategoryId int = null,
 @ProductAlias nvarchar(100) = null,
 @ProductGroup nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
  if @ProductId = '-1'
    set @ProductId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @ProductCode = '-1'
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
  
  if @DangerousGoodsId = '-1'
    set @DangerousGoodsId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @ProductCategoryId = '-1'
    set @ProductCategoryId = null;
  
	 declare @Error int
 
  insert Product
        (StatusId,
         ProductCode,
         Product,
         Barcode,
         MinimumQuantity,
         ReorderQuantity,
         MaximumQuantity,
         CuringPeriodDays,
         ShelfLifeDays,
         QualityAssuranceIndicator,
         ProductType,
         OverReceipt,
         HostId,
         RetentionSamples,
         Samples,
         ParentProductCode,
         DangerousGoodsId,
         Description2,
         Description3,
         PrincipalId,
         AssaySamples,
         Category,
         ProductCategoryId,
         ProductAlias,
         ProductGroup)
  select @StatusId,
         @ProductCode,
         @Product,
         @Barcode,
         @MinimumQuantity,
         @ReorderQuantity,
         @MaximumQuantity,
         @CuringPeriodDays,
         @ShelfLifeDays,
         @QualityAssuranceIndicator,
         @ProductType,
         @OverReceipt,
         @HostId,
         @RetentionSamples,
         @Samples,
         @ParentProductCode,
         @DangerousGoodsId,
         @Description2,
         @Description3,
         @PrincipalId,
         @AssaySamples,
         @Category,
         @ProductCategoryId,
         @ProductAlias,
         @ProductGroup 
  
  select @Error = @@Error, @ProductId = scope_identity()
  
  if @Error = 0
    exec @Error = p_ProductHistory_Insert
         @ProductId = @ProductId,
         @StatusId = @StatusId,
         @ProductCode = @ProductCode,
         @Product = @Product,
         @Barcode = @Barcode,
         @MinimumQuantity = @MinimumQuantity,
         @ReorderQuantity = @ReorderQuantity,
         @MaximumQuantity = @MaximumQuantity,
         @CuringPeriodDays = @CuringPeriodDays,
         @ShelfLifeDays = @ShelfLifeDays,
         @QualityAssuranceIndicator = @QualityAssuranceIndicator,
         @ProductType = @ProductType,
         @OverReceipt = @OverReceipt,
         @HostId = @HostId,
         @RetentionSamples = @RetentionSamples,
         @Samples = @Samples,
         @ParentProductCode = @ParentProductCode,
         @DangerousGoodsId = @DangerousGoodsId,
         @Description2 = @Description2,
         @Description3 = @Description3,
         @PrincipalId = @PrincipalId,
         @AssaySamples = @AssaySamples,
         @Category = @Category,
         @ProductCategoryId = @ProductCategoryId,
         @ProductAlias = @ProductAlias,
         @ProductGroup = @ProductGroup,
         @CommandType = 'Insert'
  
  return @Error
  
end
