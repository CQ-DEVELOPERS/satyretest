﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MovementSequence_Search
  ///   Filename       : p_MovementSequence_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:02
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the MovementSequence table.
  /// </remarks>
  /// <param>
  ///   @MovementSequenceId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   MovementSequence.MovementSequenceId,
  ///   MovementSequence.PickAreaId,
  ///   MovementSequence.StoreAreaId,
  ///   MovementSequence.StageAreaId,
  ///   MovementSequence.OperatorGroupId,
  ///   MovementSequence.StatusCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MovementSequence_Search
(
 @MovementSequenceId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @MovementSequenceId = '-1'
    set @MovementSequenceId = null;
  
 
  select
         MovementSequence.MovementSequenceId
        ,MovementSequence.PickAreaId
        ,MovementSequence.StoreAreaId
        ,MovementSequence.StageAreaId
        ,MovementSequence.OperatorGroupId
        ,MovementSequence.StatusCode
    from MovementSequence
   where isnull(MovementSequence.MovementSequenceId,'0')  = isnull(@MovementSequenceId, isnull(MovementSequence.MovementSequenceId,'0'))
  
end
