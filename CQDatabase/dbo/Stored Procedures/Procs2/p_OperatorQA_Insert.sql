﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorQA_Insert
  ///   Filename       : p_OperatorQA_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:28
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorQA table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @EndDate datetime = null,
  ///   @JobId int = null,
  ///   @QA bit = null,
  ///   @InstructionTypeId int = null,
  ///   @ActivityStatus char(1) = null,
  ///   @PickingRate int = null,
  ///   @Units int = null,
  ///   @Weight float = null,
  ///   @Instructions int = null,
  ///   @Orders int = null,
  ///   @OrderLines int = null,
  ///   @Jobs int = null,
  ///   @ActiveTime int = null,
  ///   @DwellTime int = null 
  /// </param>
  /// <returns>
  ///   OperatorQA.OperatorId,
  ///   OperatorQA.EndDate,
  ///   OperatorQA.JobId,
  ///   OperatorQA.QA,
  ///   OperatorQA.InstructionTypeId,
  ///   OperatorQA.ActivityStatus,
  ///   OperatorQA.PickingRate,
  ///   OperatorQA.Units,
  ///   OperatorQA.Weight,
  ///   OperatorQA.Instructions,
  ///   OperatorQA.Orders,
  ///   OperatorQA.OrderLines,
  ///   OperatorQA.Jobs,
  ///   OperatorQA.ActiveTime,
  ///   OperatorQA.DwellTime 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorQA_Insert
(
 @OperatorId int = null,
 @EndDate datetime = null,
 @JobId int = null,
 @QA bit = null,
 @InstructionTypeId int = null,
 @ActivityStatus char(1) = null,
 @PickingRate int = null,
 @Units int = null,
 @Weight float = null,
 @Instructions int = null,
 @Orders int = null,
 @OrderLines int = null,
 @Jobs int = null,
 @ActiveTime int = null,
 @DwellTime int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorQA
        (OperatorId,
         EndDate,
         JobId,
         QA,
         InstructionTypeId,
         ActivityStatus,
         PickingRate,
         Units,
         Weight,
         Instructions,
         Orders,
         OrderLines,
         Jobs,
         ActiveTime,
         DwellTime)
  select @OperatorId,
         @EndDate,
         @JobId,
         @QA,
         @InstructionTypeId,
         @ActivityStatus,
         @PickingRate,
         @Units,
         @Weight,
         @Instructions,
         @Orders,
         @OrderLines,
         @Jobs,
         @ActiveTime,
         @DwellTime 
  
  select @Error = @@Error
  
  
  return @Error
  
end
