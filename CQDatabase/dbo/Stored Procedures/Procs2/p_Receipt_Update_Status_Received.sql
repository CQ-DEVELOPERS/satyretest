﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receipt_Update_Status_Received
  ///   Filename       : p_Receipt_Update_Status_Received.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receipt_Update_Status_Received
(
 @InboundShipmentId int = null,
 @ReceiptId         int = null
)

as
begin
	 set nocount on;
	 
	 declare @TableReceipt as table
	 (
	  ReceiptId int
	 )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @ReceiptId = -1
    set @ReceiptId = null
  
  select @StatusId = dbo.ufn_StatusId('R','R')
  
  if @InboundShipmentId is not null
  begin
    insert @TableReceipt
          (ReceiptId)
    select ReceiptId
      from InboundShipmentReceipt
     where InboundShipmentId = @InboundShipmentId
  end
  else
  begin
    insert @TableReceipt
          (ReceiptId)
    select @ReceiptId
  end
  
  if (select count(distinct(StatusId))
        from @TableReceipt tr
        join ReceiptLine   rl on tr.ReceiptId = rl.ReceiptId
       where rl.ReceiptId = @ReceiptId) = 1
  begin
    begin transaction
    
    while exists(select 1
                   from @TableReceipt)
    begin
      select top 1 @ReceiptId = ReceiptId
        from @TableReceipt
      
      delete @TableReceipt where ReceiptId = @ReceiptId
      
      exec @Error = p_Receipt_Update
       @ReceiptId = @ReceiptId,
       @StatusId  = @StatusId
      
      if @Error <> 0
        goto error
    end
    commit transaction
  end
  
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Receipt_Update_Status_Received'); 
    rollback transaction
    return @Error
end
