﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundPerformance_Search
  ///   Filename       : p_OutboundPerformance_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:35
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OutboundPerformance table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OutboundPerformance.JobId,
  ///   OutboundPerformance.CreateDate,
  ///   OutboundPerformance.Release,
  ///   OutboundPerformance.StartDate,
  ///   OutboundPerformance.EndDate,
  ///   OutboundPerformance.Checking,
  ///   OutboundPerformance.Despatch,
  ///   OutboundPerformance.DespatchChecked,
  ///   OutboundPerformance.Complete,
  ///   OutboundPerformance.Checked,
  ///   OutboundPerformance.PlanningComplete,
  ///   OutboundPerformance.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundPerformance_Search
(
 @JobId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @JobId = '-1'
    set @JobId = null;
  
 
  select
         OutboundPerformance.JobId
        ,OutboundPerformance.CreateDate
        ,OutboundPerformance.Release
        ,OutboundPerformance.StartDate
        ,OutboundPerformance.EndDate
        ,OutboundPerformance.Checking
        ,OutboundPerformance.Despatch
        ,OutboundPerformance.DespatchChecked
        ,OutboundPerformance.Complete
        ,OutboundPerformance.Checked
        ,OutboundPerformance.PlanningComplete
        ,OutboundPerformance.WarehouseId
    from OutboundPerformance
   where isnull(OutboundPerformance.JobId,'0')  = isnull(@JobId, isnull(OutboundPerformance.JobId,'0'))
  
end
