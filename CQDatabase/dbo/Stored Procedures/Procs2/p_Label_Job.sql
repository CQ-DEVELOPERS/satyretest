﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Label_Job
  ///   Filename       : p_Label_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Label_Job

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   Name          varchar(50),
   Value         varchar(50),
   Length        int,
   FixedLength   bit,
   IsMultiline   bit,
   LineLength    int,
   LineCount     int,
   FormatID      int,
   Prompt        varchar(50),
   ValueRequired bit
  )
  
  insert @TableResult
        (Name,
         Value,
         Length,
         FixedLength,
         IsMultiline,
         LineLength,
         LineCount,
         FormatID,
         Prompt,
         ValueRequired)
  select 'Job Number',
         'J:000000000001',
         14,
         0,
         0,
         14,
         1,
         0,
         'Pallet Id:',
         0
  select Name,
         Value,
         Length,
         FixedLength,
         IsMultiline,
         LineLength,
         LineCount,
         FormatID,
         Prompt,
         ValueRequired
    from @TableResult
end
