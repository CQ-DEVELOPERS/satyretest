﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Module_Insert
  ///   Filename       : p_Module_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:47
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Module table.
  /// </remarks>
  /// <param>
  ///   @ModuleId int = null output,
  ///   @Module nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   Module.ModuleId,
  ///   Module.Module 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Module_Insert
(
 @ModuleId int = null output,
 @Module nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @ModuleId = '-1'
    set @ModuleId = null;
  
  if @Module = '-1'
    set @Module = null;
  
	 declare @Error int
 
  insert Module
        (Module)
  select @Module 
  
  select @Error = @@Error, @ModuleId = scope_identity()
  
  
  return @Error
  
end
