﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Master_Import
  ///   Filename       : p_Product_Master_Import.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Apr 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Product_Master_Import
(
 @xmlstring nvarchar(max)
)

as
begin
	 set nocount on;
  
  declare @DesktopLogId int,
          @InterfaceImportProductId int
  
  insert DesktopLog
        (ProcName,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         getdate()
  
  select @DesktopLogId = scope_identity()
  
  declare @TransferHeader as table
  (
   Id                int identity,
   OrderNumber       nvarchar(30),
   FromWarehouseCode nvarchar(50),
   ToWarehouseCode   nvarchar(50),
   InsertDate        datetime default getdate()
  )
  
  declare @TransferDetail as table
  (
   Id                int identity,
   OrderNumber       nvarchar(30),
   FromWarehouseCode nvarchar(50),
   ProductCode       nvarchar(50),
   ToWarehouseCode   nvarchar(50),
   Quantity          nvarchar(50),
   InsertDate        datetime default getdate()
  )
  
  declare @Error                   int = 0,
          @Errormsg                varchar(max),
          @GetDate                 datetime,
          @command                 varchar(max),
          @InterfaceImportHeaderId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Errormsg = 'p_Product_Master_Import - Error executing procedure'
  
  begin transaction
  
  declare @doc xml

  set @doc = convert(xml,@xmlstring)

  DECLARE @idoc int,
          @InsertDate datetime

  Set @InsertDate = Getdate()

  EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
  
  select @InterfaceImportProductId = MAX(InterfaceImportProductId)
    from InterfaceImportProduct
  
  set @Errormsg = 'p_Product_Master_Import - Error inserting into InterfaceImportProduct'
  
		insert InterfaceImportProduct
								(ProductCode,
         Product,
         SKUCode,
         SKU,
         PalletQuantity,
         Barcode,
         MinimumQuantity,
         ReorderQuantity,
         MaximumQuantity,
         CuringPeriodDays,
         ShelfLifeDays,
         QualityAssuranceIndicator,
         ProductType,
         ProductCategory,
         OverReceipt,
         RetentionSamples,
         HostId,
         PrincipalCode,
         UnitPrice,
         SizeCode,
         Size,
         ColourCode,
         Colour,
         StyleCode,
         Style,
         Description2,
         Description3,
         ProductAlias,
         PutawayRule,
         AllocationRule,
         BillingRule,
         CycleCountRule,
         LotAttributeRule,
         StorageCondition,
         SerialTracked)
	 select left(ProductCode,30),
         left(Product, 255),
         left(SKUCode,50),

         left(SKU,50),
         PalletQuantity,
         left(Barcode,50),
         case when isnumeric(MinimumQuantity) = 1
              then MinimumQuantity
              else null
              end,
         case when isnumeric(ReorderQuantity) = 1
              then ReorderQuantity
              else null
              end,
         case when isnumeric(MaximumQuantity) = 1
              then MaximumQuantity
              else null
              end,
         case when isnumeric(CuringPeriodDays) = 1
              then CuringPeriodDays
              else null
              end,
         case when isnumeric(ShelfLifeDays) = 1
              then ShelfLifeDays
              else null
              end,
         case when QualityAssuranceIndicator in ('1','true')
              then 1
              else 0
              end,
         LEFT(ProductType, 1),
         LEFT(ProductCategory,1),
         case when isnumeric(OverReceipt) = 1
              then OverReceipt
              else null
              end,
         case when isnumeric(RetentionSamples) = 1
              then RetentionSamples
              else null
              end,
         HostId,
         left(PrincipalCode,30),
         case when isnumeric(UnitPrice) = 1
              then UnitPrice
              else null
              end,
         left(SizeCode,50),
         left(Size,100),
         left(ColourCode,50),
         left(Colour,100),
         left(StyleCode,50),
         left(Style,100),
         left(Description2,255),
         left(Description3,255),
         left(ProductAlias,50),
         left(PutawayRule,100),
         left(AllocationRule,100),
         left(BillingRule,100),
         left(CycleCountRule,100),
         left(LotAttributeRule,100),
         left(StorageCondition,100),
         case when SerialTracked in ('1','true')
              then 1
              else 0
              end
		  from OPENXML (@idoc, '/root/Line',1)
            WITH (ProductCode               nvarchar(max) 'ProductCode',
                  Product                   nvarchar(max) 'Product',
                  SKUCode                   nvarchar(max) 'SKUCode',
                  SKU                       nvarchar(max) 'SKU',
                  PalletQuantity            nvarchar(max) 'PalletQuantity',
                  Barcode                   nvarchar(max) 'Barcode',
                  MinimumQuantity           nvarchar(max) 'MinimumQuantity',
                  ReorderQuantity           nvarchar(max) 'ReorderQuantity',
                  MaximumQuantity           nvarchar(max) 'MaximumQuantity',
                  CuringPeriodDays          nvarchar(max) 'CuringPeriodDays',
                  ShelfLifeDays             nvarchar(max) 'ShelfLifeDays',
                  QualityAssuranceIndicator nvarchar(max) 'QualityAssuranceIndicator',
                  ProductType               nvarchar(max) 'ProductType',
                  ProductCategory           nvarchar(max) 'ProductCategory',
                  OverReceipt               nvarchar(max) 'OverReceipt',
                  RetentionSamples          nvarchar(max) 'RetentionSamples',
                  HostId                    nvarchar(max) 'HostId',
                  PrincipalCode             nvarchar(max) 'PrincipalCode',
                  UnitPrice                 nvarchar(max) 'UnitPrice',
                  SizeCode                  nvarchar(max) 'SizeCode',
                  Size                      nvarchar(max) 'Size',
                  ColourCode                nvarchar(max) 'ColourCode',
                  Colour                    nvarchar(max) 'Colour',
                  StyleCode                 nvarchar(max) 'StyleCode',
                  Style                     nvarchar(max) 'Style',
                  Description2              nvarchar(max) 'Description2',
                  Description3              nvarchar(max) 'Description3',
                  ProductAlias              nvarchar(max) 'ProductAlias',
                  PutawayRule               nvarchar(max) 'PutawayRule',
                  AllocationRule            nvarchar(max) 'AllocationRule',
                  BillingRule               nvarchar(max) 'BillingRule',
                  CycleCountRule            nvarchar(max) 'CycleCountRule',
                  LotAttributeRule          nvarchar(max) 'LotAttributeRule',
                  StorageCondition          nvarchar(max) 'StorageCondition',
                  SerialTracked             nvarchar(max) 'SerialTracked'
                 )
    where ProductCode not like '%Product%Code%'
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Product_Master_Import - Error inserting into InterfaceImportPack'
    goto error
  end
  
		insert InterfaceImportPack
		(InterfaceImportProductId,
		 PackCode,
         PackDescription,
         Quantity,
         Barcode,
         Length,
         Width,
         Height,
         Volume,
         NettWeight,
         GrossWeight,
         TareWeight,
         ProductCategory,
         PackingCategory,
         PickEmpty,
         StackingCategory,
         MovementCategory,
         ValueCategory,
         StoringCategory,
         PickPartPallet)
	 select ip.InterfaceImportProductId,
		 left(x.PackCode,50),
         left(x.PackDescription,50),
         case when isnumeric(x.Quantity) = 1
              then x.Quantity
              else null
              end,
         x.Packbarcode,
         case when isnumeric(x.Length) = 1
              then x.Length
              else null
              end,
         case when isnumeric(x.Width) = 1
              then x.Width
              else null
              end,
         case when isnumeric(x.Height) = 1
              then x.Height
              else null
              end,
         case when isnumeric(x.Volume) = 1 and datalength(x.Volume) <= 10
              then x.Volume
              else null
              end,
         case when isnumeric(x.NettWeight) = 1
              then x.NettWeight
              else null
              end,
         case when isnumeric(x.GrossWeight) = 1
              then x.GrossWeight
              else null
              end,
         case when isnumeric(x.TareWeight) = 1
              then x.TareWeight
              else null
              end,
         left(x.ProductCategory,1),
         left(x.PackingCategory,1),
         case when x.PickEmpty in ('1','true')
              then 1
              else 0
              end,
         case when isnumeric(x.StackingCategory) = 1
              then x.StackingCategory
              else null
              end,
         case when isnumeric(x.MovementCategory) = 1
              then x.MovementCategory
              else null
              end,
         case when isnumeric(x.ValueCategory) = 1
              then x.ValueCategory
              else null
              end,
         case when isnumeric(x.StoringCategory) = 1
              then x.StoringCategory
              else null
              end,
         case when isnumeric(x.PickPartPallet) = 1
              then x.PickPartPallet
              else null
              end
		  from OPENXML (@idoc, '/root/Line',1)
            WITH (ProductCode      nvarchar(max) 'ProductCode',
                  PackCode         nvarchar(max) 'PackCode',
                  PackDescription  nvarchar(max) 'PackDescription',
                  Quantity         nvarchar(max) 'Quantity',
                  PackBarcode	   nvarchar(max) 'PackBarcode',
                  Length           nvarchar(max) 'Length',
                  Width            nvarchar(max) 'Width',
                  Height           nvarchar(max) 'Height',
                  Volume           nvarchar(max) 'Volume',
                  NettWeight       nvarchar(max) 'NettWeight',
                  GrossWeight      nvarchar(max) 'GrossWeight',
                  TareWeight       nvarchar(max) 'TareWeight',
                  ProductCategory  nvarchar(max) 'ProductCategory',
                  PackingCategory  nvarchar(max) 'PackingCategory',
                  PickEmpty        nvarchar(max) 'PickEmpty',
                  StackingCategory nvarchar(max) 'StackingCategory',
                  MovementCategory nvarchar(max) 'MovementCategory',
                  ValueCategory    nvarchar(max) 'ValueCategory',
                  StoringCategory  nvarchar(max) 'StoringCategory',
                  PickPartPallet   nvarchar(max) 'PickPartPallet') as x
      join InterfaceImportProduct ip on x.ProductCode = ip.ProductCode
                                    and ip.InterfaceImportProductId > isnull(@InterfaceImportProductId,0)
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Product_Master_Import - Error inserting into InterfaceImportPack'
    goto error
  end
    
  commit transaction
  
  update DesktopLog
     set Errormsg = 'Successful',
         EndDate  = getdate()
   where DesktopLogId = @DesktopLogId
  
  return 0
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    
    update DesktopLog
       set Errormsg = isnull(@xmlstring, 'Error executing ' + OBJECT_NAME(@@PROCID)),
           EndDate  = getdate()
     where DesktopLogId = @DesktopLogId
    
    return @Error
end
