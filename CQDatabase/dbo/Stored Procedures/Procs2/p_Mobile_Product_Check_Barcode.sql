﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Barcode
  ///   Filename       : p_Mobile_Product_Check_Barcode.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Barcode
(
 @WarehouseId     int,
 @jobId           int,
 @barcode         nvarchar(50)
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   StorageUnitId                   int              null    ,
   ProductCode                     nvarchar(60)     null    ,
   Product                         nvarchar(510)    null    ,
   SKUCode                         nvarchar(20)     null    ,
   Lines                           nvarchar(510)    null    ,
   ErrorMsg                        nvarchar(max)     null   ,
   IssueLineId					   int 
  )
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         WarehouseId,
         JobId,
         Barcode,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @WarehouseId,
         @JobId,
         @Barcode,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error             int,
          @Errormsg          nvarchar(500)
  
  if @Barcode like 'P:%'
    select @Barcode = vs.ProductCode
      from Pallet     p (nolock)
      join viewStock vs on p.StorageUnitBatchId = vs.StorageUnitBatchId
     where convert(nvarchar(10), PalletId) = replace(@Barcode, 'P:','')
  
  -- GS 2012-06-29 - For Plascon - always scan pack barcode and get correct storage unit first
  insert @TableResult
        (StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         Lines,
         ErrorMsg,
         IssueLineId)
  select top 1
         cp.StorageUnitId,
         cp.ProductCode,
         cp.Product,
         cp.SKUCode,
         cp.Lines,
         cp.ErrorMsg,
         ili.IssueLineId
    from CheckingProduct cp (nolock)
    join Pack            pk (nolock) on cp.StorageUnitId     = pk.StorageUnitId
    join IssueLineInstruction ili (nolock) on cp.InstructionId = ili.InstructionId
   where pk.WarehouseId      = @WarehouseId
     and cp.JobId            = @jobId
     and pk.Barcode          = @barcode
  
  if @@rowcount = 0 -- GS 2012-06-29 - For Plascon - now find the other barcodes
    insert @TableResult
          (StorageUnitId,
           ProductCode,
           Product,
           SKUCode,
           Lines,
           ErrorMsg,
           IssueLineId)
    select top 1
           cp.StorageUnitId,
           cp.ProductCode,
           cp.Product,
           cp.SKUCode,
           cp.Lines,
           cp.ErrorMsg,
           ili.IssueLineId
      from CheckingProduct cp (nolock)
      left
      join SerialNumber    sn (nolock) on cp.StorageUnitId     = sn.StorageUnitId
      join IssueLineInstruction ili (nolock) on cp.InstructionId = ili.InstructionId
     where cp.JobId            = @jobId
       and (cp.ProductCode     = @barcode
        or  cp.ProductBarcode  = @barcode
        or  cp.SKUCode         = @barcode
        or  sn.SerialNumber    = @barcode
        or  sn.ReferenceNumber = @barcode)
  
  select StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         Lines,
         ErrorMsg,
         IssueLineId
    from @TableResult
  
  declare @SerialNumberId int,
          @IssueId        int,
          @StorageUnitBatchId int
  
  if exists(select 1 from SerialNumber (nolock) where SerialNumber = @barcode)
  begin
    select @SerialNumberId = SerialNumberId
      from SerialNumber (nolock)
     where SerialNumber = @barcode
    
    select @IssueId = ili.IssueId
      from Instruction          ins (nolock)
      join IssueLineInstruction ili (nolock) on ili.InstructionId in (ins.InstructionId, ins.InstructionRefId)
      join StorageUnitBatch     sub (nolock) on ins.StorageUnitBatchId = sub.StorageUnitBatchId
      join SerialNumber          sn (nolock) on sub.StorageUnitId      = sn.StorageUnitId
     where ins.JobId       = @jobId
       and sn.SerialNumber = @barcode
    
    exec @Error = p_SerialNumber_Update
     @SerialNumberId = @SerialNumberId,
     @IssueId        = @IssueId
  end
  
  if exists(select 1 from SerialNumber (nolock) where ReferenceNumber = @barcode)
  begin
    set @SerialNumberId = -1
    while @SerialNumberId is not null
    begin
      set @SerialNumberId = null
      
      select @SerialNumberId = SerialNumberId
        from SerialNumber (nolock)
       where ReferenceNumber = @barcode
         and IssueId        is null
      
      if @SerialNumberId is not null and @IssueId is null
      begin
        select @IssueId            = ili.IssueId,
               @StorageUnitBatchId = ins.StorageUnitBatchId
          from Instruction          ins (nolock)
          join IssueLineInstruction ili (nolock) on ili.InstructionId in (ins.InstructionId, ins.InstructionRefId)
          join StorageUnitBatch     sub (nolock) on ins.StorageUnitBatchId = sub.StorageUnitBatchId
          join SerialNumber          sn (nolock) on sub.StorageUnitId      = sn.StorageUnitId
         where ins.JobId          = @jobId
           and sn.ReferenceNumber = @barcode
      end
      
      if @IssueId is null or @SerialNumberId is null
      begin
        set @SerialNumberId = null
      end
      else
      begin
        exec p_Mobile_Product_Check_Quantity
         @warehouseId        = @WarehouseId,
         @jobId              = @jobId,
         @storageUnitBatchId = @StorageUnitBatchId,
         @quantity           = 1,
         @scanMode           = 1,
         @ShowMsg            = 0
        
        exec @Error = p_SerialNumber_Update
         @SerialNumberId = @SerialNumberId,
         @IssueId        = @IssueId
      end
    end
  end
  
  update MobileLog
     set EndDate = getdate()
   where MobileLogId = @MobileLogId
end
