﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationType_Select
  ///   Filename       : p_LocationType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:01
  /// </summary>
  /// <remarks>
  ///   Selects rows from the LocationType table.
  /// </remarks>
  /// <param>
  ///   @LocationTypeId int = null 
  /// </param>
  /// <returns>
  ///   LocationType.LocationTypeId,
  ///   LocationType.LocationType,
  ///   LocationType.LocationTypeCode,
  ///   LocationType.DeleteIndicator,
  ///   LocationType.ReplenishmentIndicator,
  ///   LocationType.MultipleProductsIndicator,
  ///   LocationType.BatchTrackingIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationType_Select
(
 @LocationTypeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         LocationType.LocationTypeId
        ,LocationType.LocationType
        ,LocationType.LocationTypeCode
        ,LocationType.DeleteIndicator
        ,LocationType.ReplenishmentIndicator
        ,LocationType.MultipleProductsIndicator
        ,LocationType.BatchTrackingIndicator
    from LocationType
   where isnull(LocationType.LocationTypeId,'0')  = isnull(@LocationTypeId, isnull(LocationType.LocationTypeId,'0'))
  
end
