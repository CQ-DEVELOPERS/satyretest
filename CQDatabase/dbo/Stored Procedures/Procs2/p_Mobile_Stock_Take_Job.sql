﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Stock_Take_Job
  ///   Filename       : p_Mobile_Stock_Take_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Stock_Take_Job
(
 @operatorId int,
 @areaId     int
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  JobId           int,
	  ReferenceNumber nvarchar(30),
	  InstructionType nvarchar(30),
	  [Count]         int,
	  FirstLocationId int,
	  FirstLocation   nvarchar(15),
	  LastLocationId  int,
	  LastLocation    nvarchar(15)
	 )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @WarehouseId       int
  
  select @WarehouseId = WarehouseId
    from Operator (nolock)
   where OperatorId = @operatorId
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  insert @TableResult
        (JobId,
         ReferenceNumber,
         InstructionType,
         [Count],
         FirstLocationId,
         LastLocationId)
  select j.JobId,
         j.ReferenceNumber,
         it.InstructionType,
         count(1),
         min(i.PickLocationId),
         max(i.PickLocationId)
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
    join Status          sj (nolock) on j.StatusId          = sj.StatusId
    join Status          si (nolock) on i.StatusId          = si.StatusId
    join AreaLocation    al (nolock) on i.PickLocationId    = al.LocationId
   where i.WarehouseId                     = @WarehouseId
     and it.InstructionTypeCode           in ('STE','STL','STA','STP')
     and al.AreaId                         = @AreaId
     and isnull(j.OperatorId, @operatorId) = @operatorId
     and sj.StatusCode                    in ('RL','S')
     and si.StatusCode                    in ('W','S')
  group by j.JobId,
           j.ReferenceNumber,
           it.InstructionType
  
  update tr
     set FirstLocation = l.Location
    from @TableResult tr
    join Location      l on tr.FirstLocationId = l.LocationId
  
  update tr
     set LastLocation = l.Location
    from @TableResult tr
    join Location      l on tr.LastLocationId = l.LocationId
  
  select JobId,
         ReferenceNumber,
         InstructionType,
         [Count],
         FirstLocation,
         LastLocation
    from @TableResult
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Mobile_Stock_Take_Job'); 
    rollback transaction
    return @Error
end
