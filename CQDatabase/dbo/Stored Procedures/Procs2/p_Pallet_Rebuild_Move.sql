﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Move
  ///   Filename       : p_Pallet_Rebuild_Move.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Move
(
 @oldJobId         int,
 @newJobId         int
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  InstructionId int,
	  IssueLineId   int,
	  Quantity      float
	 )
  
  declare @Error         int,
          @Errormsg      varchar(500),
          @GetDate       datetime,
          @IssueLineId   int,
          @InstructionId int,
          @Quantity      float
	 
	 select @GetDate = dbo.ufn_Getdate()
	 
	 set @Errormsg = 'Error executing p_Pallet_Rebuild_Move'
	 
	 --being transaction
	 
	 insert @TableResult
	       (InstructionId,
	        IssueLineId,
	        Quantity)
  select Distinct i.InstructionId,
         ili.IssueLineId,
         i.ConfirmedQuantity
    from IssueLineInstruction ili (nolock)
    join Instruction            i (nolock) on ili.InstructionId    = isnull(i.InstructionRefId, i.InstructionId)
    join Status                 s (nolock) on i.StatusId           = s.StatusId
   where i.JobId = @oldJobId
     and i.ConfirmedQuantity > 0
  
  while exists(select top 1 1
                 from @TableResult)
  begin
    select @IssueLineId   = IssueLineId,
           @InstructionId = InstructionId,
           @Quantity      = Quantity
      from @TableResult
    
    delete @TableResult
     where IssueLineId   = @IssueLineId
       and InstructionId = @InstructionId
       and Quantity      = @Quantity
    
    exec p_Pallet_Rebuild_Link
     @JobId         = @newJobId,
     @IssueLineId   = @IssueLineId,
     @InstructionId = @InstructionId,
     @Quantity      = @Quantity
    
    if @Error <> 0
      goto error
  end
  
  delete Instruction where JobId = @oldJobId and Quantity = ConfirmedQuantity and Quantity = 0 and InstructionRefId is not null
  update Instruction set JobId = @newJobId where JobId = @oldJobId and Quantity = ConfirmedQuantity and Quantity = 0 and InstructionRefId is null

  select @@rowcount
  
  set @Error = @@Error

  if @Error <> 0
    goto error
  
  --commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    --rollback transaction
    return @Error
end
