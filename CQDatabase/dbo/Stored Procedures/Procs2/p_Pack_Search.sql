﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pack_Search
  ///   Filename       : p_Pack_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Nov 2012 14:41:25
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Pack table.
  /// </remarks>
  /// <param>
  ///   @PackId int = null output,
  ///   @StorageUnitId int = null,
  ///   @PackTypeId int = null,
  ///   @WarehouseId int = null,
  ///   @StatusId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Pack.PackId,
  ///   Pack.StorageUnitId,
  ///   StorageUnit.StorageUnitId,
  ///   Pack.PackTypeId,
  ///   PackType.PackType,
  ///   Pack.Quantity,
  ///   Pack.Barcode,
  ///   Pack.Length,
  ///   Pack.Width,
  ///   Pack.Height,
  ///   Pack.Volume,
  ///   Pack.Weight,
  ///   Pack.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   Pack.NettWeight,
  ///   Pack.TareWeight,
  ///   Pack.ProductionQuantity,
  ///   Pack.StatusId 
  ///   Status.Status 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pack_Search
(
 @PackId int = null output,
 @StorageUnitId int = null,
 @PackTypeId int = null,
 @WarehouseId int = null,
 @StatusId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @PackId = '-1'
    set @PackId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @PackTypeId = '-1'
    set @PackTypeId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
 
  select
         Pack.PackId
        ,Pack.StorageUnitId
        ,Pack.PackTypeId
         ,PackTypePackTypeId.PackType as 'PackType'
        ,Pack.Quantity
        ,Pack.Barcode
        ,Pack.Length
        ,Pack.Width
        ,Pack.Height
        ,Pack.Volume
        ,Pack.Weight
        ,Pack.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,Pack.NettWeight
        ,Pack.TareWeight
        ,Pack.ProductionQuantity
        ,Pack.StatusId
         ,StatusStatusId.Status as 'Status'
    from Pack
    left
    join StorageUnit StorageUnitStorageUnitId on StorageUnitStorageUnitId.StorageUnitId = Pack.StorageUnitId
    left
    join PackType PackTypePackTypeId on PackTypePackTypeId.PackTypeId = Pack.PackTypeId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = Pack.WarehouseId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = Pack.StatusId
   where isnull(Pack.PackId,'0')  = isnull(@PackId, isnull(Pack.PackId,'0'))
     and isnull(Pack.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(Pack.StorageUnitId,'0'))
     and isnull(Pack.PackTypeId,'0')  = isnull(@PackTypeId, isnull(Pack.PackTypeId,'0'))
     and isnull(Pack.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Pack.WarehouseId,'0'))
     and isnull(Pack.StatusId,'0')  = isnull(@StatusId, isnull(Pack.StatusId,'0'))
  order by Barcode
  
end
