﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundLine_Search
  ///   Filename       : p_OutboundLine_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jan 2014 07:56:01
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OutboundLine table.
  /// </remarks>
  /// <param>
  ///   @OutboundLineId int = null output,
  ///   @OutboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OutboundLine.OutboundLineId,
  ///   OutboundLine.OutboundDocumentId,
  ///   OutboundDocument.OutboundDocumentId,
  ///   OutboundLine.StorageUnitId,
  ///   StorageUnit.StorageUnitId,
  ///   OutboundLine.StatusId,
  ///   Status.Status,
  ///   OutboundLine.LineNumber,
  ///   OutboundLine.Quantity,
  ///   OutboundLine.BatchId,
  ///   OutboundLine.UnitPrice,
  ///   OutboundLine.UOM,
  ///   OutboundLine.BOELineNumber,
  ///   OutboundLine.TariffCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundLine_Search
(
 @OutboundLineId int = null output,
 @OutboundDocumentId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @OutboundLineId = '-1'
    set @OutboundLineId = null;
  
  if @OutboundDocumentId = '-1'
    set @OutboundDocumentId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
 
  select
         OutboundLine.OutboundLineId
        ,OutboundLine.OutboundDocumentId
        ,OutboundLine.StorageUnitId
        ,OutboundLine.StatusId
         ,StatusStatusId.Status as 'Status'
        ,OutboundLine.LineNumber
        ,OutboundLine.Quantity
        ,OutboundLine.BatchId
        ,OutboundLine.UnitPrice
        ,OutboundLine.UOM
        ,OutboundLine.BOELineNumber
        ,OutboundLine.TariffCode
    from OutboundLine
    left
    join OutboundDocument OutboundDocumentOutboundDocumentId on OutboundDocumentOutboundDocumentId.OutboundDocumentId = OutboundLine.OutboundDocumentId
    left
    join StorageUnit StorageUnitStorageUnitId on StorageUnitStorageUnitId.StorageUnitId = OutboundLine.StorageUnitId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = OutboundLine.StatusId
   where isnull(OutboundLine.OutboundLineId,'0')  = isnull(@OutboundLineId, isnull(OutboundLine.OutboundLineId,'0'))
     and isnull(OutboundLine.OutboundDocumentId,'0')  = isnull(@OutboundDocumentId, isnull(OutboundLine.OutboundDocumentId,'0'))
     and isnull(OutboundLine.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(OutboundLine.StorageUnitId,'0'))
     and isnull(OutboundLine.StatusId,'0')  = isnull(@StatusId, isnull(OutboundLine.StatusId,'0'))
  order by UOM
  
end
