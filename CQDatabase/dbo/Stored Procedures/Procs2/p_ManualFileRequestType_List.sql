﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequestType_List
  ///   Filename       : p_ManualFileRequestType_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:04
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ManualFileRequestType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ManualFileRequestType.ManualFileRequestTypeId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequestType_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ManualFileRequestTypeId
        ,null as 'ManualFileRequestType'
  union
  select
         ManualFileRequestType.ManualFileRequestTypeId
        ,ManualFileRequestType.ManualFileRequestTypeId as 'ManualFileRequestType'
    from ManualFileRequestType
  
end
