﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLinePackaging_Select
  ///   Filename       : p_IssueLinePackaging_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:56
  /// </summary>
  /// <remarks>
  ///   Selects rows from the IssueLinePackaging table.
  /// </remarks>
  /// <param>
  ///   @PackageLineId int = null 
  /// </param>
  /// <returns>
  ///   IssueLinePackaging.PackageLineId,
  ///   IssueLinePackaging.IssueId,
  ///   IssueLinePackaging.IssueLineId,
  ///   IssueLinePackaging.JobId,
  ///   IssueLinePackaging.StorageUnitBatchId,
  ///   IssueLinePackaging.StorageUnitId,
  ///   IssueLinePackaging.StatusId,
  ///   IssueLinePackaging.OperatorId,
  ///   IssueLinePackaging.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLinePackaging_Select
(
 @PackageLineId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         IssueLinePackaging.PackageLineId
        ,IssueLinePackaging.IssueId
        ,IssueLinePackaging.IssueLineId
        ,IssueLinePackaging.JobId
        ,IssueLinePackaging.StorageUnitBatchId
        ,IssueLinePackaging.StorageUnitId
        ,IssueLinePackaging.StatusId
        ,IssueLinePackaging.OperatorId
        ,IssueLinePackaging.Quantity
    from IssueLinePackaging
   where isnull(IssueLinePackaging.PackageLineId,'0')  = isnull(@PackageLineId, isnull(IssueLinePackaging.PackageLineId,'0'))
  
end
