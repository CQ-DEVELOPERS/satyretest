﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QuestionAnswers_List
  ///   Filename       : p_QuestionAnswers_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:28
  /// </summary>
  /// <remarks>
  ///   Selects rows from the QuestionAnswers table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   QuestionAnswers.QuestionAnswersId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QuestionAnswers_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select '-1' as QuestionAnswersId
        ,null as 'QuestionAnswers'
  union
  select
         QuestionAnswers.QuestionAnswersId
        ,QuestionAnswers.QuestionAnswersId as 'QuestionAnswers'
    from QuestionAnswers
  
end
