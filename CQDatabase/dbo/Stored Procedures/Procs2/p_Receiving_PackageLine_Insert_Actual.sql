﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_PackageLine_Insert_Actual
  ///   Filename       : p_Receiving_PackageLine_Insert_Actual.sql
  ///   Create By      : Willis
  ///   Date Created   : Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_PackageLine_Insert_Actual
(
 @WarehouseId			int,
 @ReceiptId				int,
 @StorageUnitBatchId	int,
 @OperatorId			int,
 @ActualQuantity		numeric(13,6) = null
)

as
begin
		Declare @StatusId int,
				@Return	  int

		Set @Return = -1

		Select @StatusId = StatusId from Status where type = 'R' and StatusCode = 'W'

		If exists (Select top 1 1 from ReceiptLinePackaging where ReceiptId = @ReceiptId and StorageUnitBatchId = @StorageUnitBatchId and StatusId = @StatusId)
		Begin
			Print 'Exists'
			Goto InsertExit
		End

		Insert ReceiptLinePackaging (
						ReceiptId,
						StorageUnitBatchId,
						StatusId,
						OperatorId,
						RequiredQuantity,
						ReceivedQuantity,
						AcceptedQuantity,
						RejectQuantity,		
						DeliveryNoteQuantity)
		Values
					(@ReceiptId,@StorageUnitBatchId,@StatusId,@OperatorId, 0,0,Isnull(@ActualQuantity,0),0,0)

		Select @Return = @@IDENTITY

			

InsertExit:
		Select @Return
end
