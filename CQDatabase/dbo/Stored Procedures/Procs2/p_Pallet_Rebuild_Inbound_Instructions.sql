﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Inbound_Instructions
  ///   Filename       : p_Pallet_Rebuild_Inbound_Instructions.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Inbound_Instructions
(
 @JobId int,
 @Barcode nvarchar(30)
)

as
begin
	 set nocount on;
	 
  select Distinct i.InstructionId,
         rl.ReceiptLineId,
         null as 'OrderNumber',
         null as 'ExternalCompany',
         s.Status,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         i.ConfirmedQuantity
    from ReceiptLine		   rl (nolock)
    join Instruction            i (nolock) on rl.ReceiptLineId     = i.ReceiptLineId
    join Status                 s (nolock) on i.StatusId           = s.StatusId
    join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch                  b (nolock) on sub.BatchId          = b.BatchId
    join StorageUnit           su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product                p (nolock) on su.ProductId         = p.ProductId
    join SKU                  sku (nolock) on su.SKUId             = sku.SKUId
   where i.JobId = @JobId
     and i.ConfirmedQuantity > 0
     and (p.Barcode = @Barcode
     or   p.ProductCode like '%' + @Barcode + '%'
     or exists(select 1 from Pack pk where pk.StorageUnitId = su.StorageUnitId and i.WarehouseId = pk.WarehouseId and pk.Barcode = @Barcode))
end
