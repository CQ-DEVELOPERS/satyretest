﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Insert
  ///   Filename       : p_Location_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2013 08:13:20
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Location table.
  /// </remarks>
  /// <param>
  ///   @LocationId int = null output,
  ///   @LocationTypeId int = null,
  ///   @Location nvarchar(30) = null,
  ///   @Ailse nvarchar(20) = null,
  ///   @Column nvarchar(20) = null,
  ///   @Level nvarchar(20) = null,
  ///   @PalletQuantity float = null,
  ///   @SecurityCode int = null,
  ///   @RelativeValue numeric(13,3) = null,
  ///   @NumberOfSUB int = null,
  ///   @StocktakeInd bit = null,
  ///   @ActiveBinning bit = null,
  ///   @ActivePicking bit = null,
  ///   @Used bit = null,
  ///   @Latitude decimal(16,13) = null,
  ///   @Longitude decimal(16,13) = null,
  ///   @Direction nvarchar(20) = null,
  ///   @Height numeric(13,6) = null,
  ///   @Length numeric(13,6) = null,
  ///   @Width numeric(13,6) = null,
  ///   @Volume numeric(13,6) = null 
  /// </param>
  /// <returns>
  ///   Location.LocationId,
  ///   Location.LocationTypeId,
  ///   Location.Location,
  ///   Location.Ailse,
  ///   Location.[Column],
  ///   Location.[Level],
  ///   Location.PalletQuantity,
  ///   Location.SecurityCode,
  ///   Location.RelativeValue,
  ///   Location.NumberOfSUB,
  ///   Location.StocktakeInd,
  ///   Location.ActiveBinning,
  ///   Location.ActivePicking,
  ///   Location.Used,
  ///   Location.Latitude,
  ///   Location.Longitude,
  ///   Location.Direction,
  ///   Location.Height,
  ///   Location.Length,
  ///   Location.Width,
  ///   Location.Volume 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Insert
(
 @LocationId int = null output,
 @LocationTypeId int = null,
 @Location nvarchar(30) = null,
 @Ailse nvarchar(20) = null,
 @Column nvarchar(20) = null,
 @Level nvarchar(20) = null,
 @PalletQuantity float = null,
 @SecurityCode int = null,
 @RelativeValue numeric(13,3) = null,
 @NumberOfSUB int = null,
 @StocktakeInd bit = null,
 @ActiveBinning bit = null,
 @ActivePicking bit = null,
 @Used bit = null,
 @Latitude decimal(16,13) = null,
 @Longitude decimal(16,13) = null,
 @Direction nvarchar(20) = null,
 @Height numeric(13,6) = null,
 @Length numeric(13,6) = null,
 @Width numeric(13,6) = null,
 @Volume numeric(13,6) = null 
)

as
begin
	 set nocount on;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @LocationTypeId = '-1'
    set @LocationTypeId = null;
  
  if @Location = '-1'
    set @Location = null;
  
	 declare @Error int
 
  insert Location
        (LocationTypeId,
         Location,
         Ailse,
         [Column],
         [Level],
         PalletQuantity,
         SecurityCode,
         RelativeValue,
         NumberOfSUB,
         StocktakeInd,
         ActiveBinning,
         ActivePicking,
         Used,
         Latitude,
         Longitude,
         Direction,
         Height,
         Length,
         Width,
         Volume)
  select @LocationTypeId,
         @Location,
         @Ailse,
         @Column,
         @Level,
         @PalletQuantity,
         @SecurityCode,
         @RelativeValue,
         @NumberOfSUB,
         @StocktakeInd,
         @ActiveBinning,
         @ActivePicking,
         @Used,
         @Latitude,
         @Longitude,
         @Direction,
         @Height,
         @Length,
         @Width,
         @Volume 
  
  select @Error = @@Error, @LocationId = scope_identity()
  
  
  return @Error
  
end
