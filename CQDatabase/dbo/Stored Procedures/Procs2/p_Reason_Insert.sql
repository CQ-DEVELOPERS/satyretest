﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Reason_Insert
  ///   Filename       : p_Reason_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:21
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Reason table.
  /// </remarks>
  /// <param>
  ///   @ReasonId int = null output,
  ///   @Reason nvarchar(100) = null,
  ///   @ReasonCode nvarchar(20) = null,
  ///   @AdjustmentType varchar(20) = null,
  ///   @ToWarehouseCode varchar(10) = null,
  ///   @RecordType varchar(20) = null 
  /// </param>
  /// <returns>
  ///   Reason.ReasonId,
  ///   Reason.Reason,
  ///   Reason.ReasonCode,
  ///   Reason.AdjustmentType,
  ///   Reason.ToWarehouseCode,
  ///   Reason.RecordType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Reason_Insert
(
 @ReasonId int = null output,
 @Reason nvarchar(100) = null,
 @ReasonCode nvarchar(20) = null,
 @AdjustmentType varchar(20) = null,
 @ToWarehouseCode varchar(10) = null,
 @RecordType varchar(20) = null 
)

as
begin
	 set nocount on;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @Reason = '-1'
    set @Reason = null;
  
  if @ReasonCode = '-1'
    set @ReasonCode = null;
  
	 declare @Error int
 
  insert Reason
        (Reason,
         ReasonCode,
         AdjustmentType,
         ToWarehouseCode,
         RecordType)
  select @Reason,
         @ReasonCode,
         @AdjustmentType,
         @ToWarehouseCode,
         @RecordType 
  
  select @Error = @@Error, @ReasonId = scope_identity()
  
  
  return @Error
  
end
