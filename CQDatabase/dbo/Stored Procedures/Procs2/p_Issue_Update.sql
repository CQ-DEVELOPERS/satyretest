﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Issue_Update
  ///   Filename       : p_Issue_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2015 09:30:35
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Issue table.
  /// </remarks>
  /// <param>
  ///   @IssueId int = null,
  ///   @OutboundDocumentId int = null,
  ///   @PriorityId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @RouteId int = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @DropSequence int = null,
  ///   @LoadIndicator bit = null,
  ///   @DespatchBay int = null,
  ///   @RoutingSystem bit = null,
  ///   @Delivery int = null,
  ///   @NumberOfLines int = null,
  ///   @Total int = null,
  ///   @Complete int = null,
  ///   @AreaType nvarchar(20) = null,
  ///   @Units int = null,
  ///   @ShortPicks int = null,
  ///   @Releases int = null,
  ///   @Weight float = null,
  ///   @ConfirmedWeight float = null,
  ///   @FirstLoaded datetime = null,
  ///   @Interfaced bit = null,
  ///   @Loaded int = null,
  ///   @Pallets int = null,
  ///   @Volume float = null,
  ///   @ContactListId int = null,
  ///   @ManufacturingId int = null,
  ///   @ReservedId int = null,
  ///   @ParentIssueId int = null,
  ///   @ASN bit = null,
  ///   @WaveId int = null,
  ///   @ReserveBatch bit = null,
  ///   @InvoiceNumber nvarchar(60) = null,
  ///   @TotalDue numeric(13,2) = null,
  ///   @AddressLine1 nvarchar(510) = null,
  ///   @AddressLine2 nvarchar(510) = null,
  ///   @AddressLine3 nvarchar(510) = null,
  ///   @AddressLine4 nvarchar(510) = null,
  ///   @AddressLine5 nvarchar(510) = null,
  ///   @PlanningComplete datetime = null,
  ///   @PlannedBy int = null,
  ///   @Release datetime = null,
  ///   @ReleasedBy int = null,
  ///   @Checking datetime = null,
  ///   @Checked datetime = null,
  ///   @Despatch datetime = null,
  ///   @DespatchChecked datetime = null,
  ///   @CompleteDate datetime = null,
  ///   @Availability nvarchar(40) = null,
  ///   @CheckingCount int = null,
  ///   @Branding bit = null,
  ///   @Downsizing bit = null,
  ///   @CheckSheetPrinted bit = null,
  ///   @CreateDate datetime = null,
  ///   @VehicleId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Issue_Update
(
 @IssueId int = null,
 @OutboundDocumentId int = null,
 @PriorityId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @RouteId int = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Remarks nvarchar(500) = null,
 @DropSequence int = null,
 @LoadIndicator bit = null,
 @DespatchBay int = null,
 @RoutingSystem bit = null,
 @Delivery int = null,
 @NumberOfLines int = null,
 @Total int = null,
 @Complete int = null,
 @AreaType nvarchar(20) = null,
 @Units int = null,
 @ShortPicks int = null,
 @Releases int = null,
 @Weight float = null,
 @ConfirmedWeight float = null,
 @FirstLoaded datetime = null,
 @Interfaced bit = null,
 @Loaded int = null,
 @Pallets int = null,
 @Volume float = null,
 @ContactListId int = null,
 @ManufacturingId int = null,
 @ReservedId int = null,
 @ParentIssueId int = null,
 @ASN bit = null,
 @WaveId int = null,
 @ReserveBatch bit = null,
 @InvoiceNumber nvarchar(60) = null,
 @TotalDue numeric(13,2) = null,
 @AddressLine1 nvarchar(510) = null,
 @AddressLine2 nvarchar(510) = null,
 @AddressLine3 nvarchar(510) = null,
 @AddressLine4 nvarchar(510) = null,
 @AddressLine5 nvarchar(510) = null,
 @PlanningComplete datetime = null,
 @PlannedBy int = null,
 @Release datetime = null,
 @ReleasedBy int = null,
 @Checking datetime = null,
 @Checked datetime = null,
 @Despatch datetime = null,
 @DespatchChecked datetime = null,
 @CompleteDate datetime = null,
 @Availability nvarchar(40) = null,
 @CheckingCount int = null,
 @Branding bit = null,
 @Downsizing bit = null,
 @CheckSheetPrinted bit = null,
 @CreateDate datetime = null,
 @VehicleId int = null 
)

as
begin
	 set nocount on;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
  if @OutboundDocumentId = '-1'
    set @OutboundDocumentId = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @RouteId = '-1'
    set @RouteId = null;
  
  if @DespatchBay = '-1'
    set @DespatchBay = null;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @ManufacturingId = '-1'
    set @ManufacturingId = null;
  
  if @WaveId = '-1'
    set @WaveId = null;
  
  if @PlannedBy = '-1'
    set @PlannedBy = null;
  
  if @ReleasedBy = '-1'
    set @ReleasedBy = null;
  
  if @VehicleId = '-1'
    set @VehicleId = null;
  
	 declare @Error int
 
  update Issue
     set OutboundDocumentId = isnull(@OutboundDocumentId, OutboundDocumentId),
         PriorityId = isnull(@PriorityId, PriorityId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         LocationId = isnull(@LocationId, LocationId),
         StatusId = isnull(@StatusId, StatusId),
         OperatorId = isnull(@OperatorId, OperatorId),
         RouteId = isnull(@RouteId, RouteId),
         DeliveryNoteNumber = isnull(@DeliveryNoteNumber, DeliveryNoteNumber),
         DeliveryDate = isnull(@DeliveryDate, DeliveryDate),
         SealNumber = isnull(@SealNumber, SealNumber),
         VehicleRegistration = isnull(@VehicleRegistration, VehicleRegistration),
         Remarks = isnull(@Remarks, Remarks),
         DropSequence = isnull(@DropSequence, DropSequence),
         LoadIndicator = isnull(@LoadIndicator, LoadIndicator),
         DespatchBay = isnull(@DespatchBay, DespatchBay),
         RoutingSystem = isnull(@RoutingSystem, RoutingSystem),
         Delivery = isnull(@Delivery, Delivery),
         NumberOfLines = isnull(@NumberOfLines, NumberOfLines),
         Total = isnull(@Total, Total),
         Complete = isnull(@Complete, Complete),
         AreaType = isnull(@AreaType, AreaType),
         Units = isnull(@Units, Units),
         ShortPicks = isnull(@ShortPicks, ShortPicks),
         Releases = isnull(@Releases, Releases),
         Weight = isnull(@Weight, Weight),
         ConfirmedWeight = isnull(@ConfirmedWeight, ConfirmedWeight),
         FirstLoaded = isnull(@FirstLoaded, FirstLoaded),
         Interfaced = isnull(@Interfaced, Interfaced),
         Loaded = isnull(@Loaded, Loaded),
         Pallets = isnull(@Pallets, Pallets),
         Volume = isnull(@Volume, Volume),
         ContactListId = isnull(@ContactListId, ContactListId),
         ManufacturingId = isnull(@ManufacturingId, ManufacturingId),
         ReservedId = isnull(@ReservedId, ReservedId),
         ParentIssueId = isnull(@ParentIssueId, ParentIssueId),
         ASN = isnull(@ASN, ASN),
         WaveId = isnull(@WaveId, WaveId),
         ReserveBatch = isnull(@ReserveBatch, ReserveBatch),
         InvoiceNumber = isnull(@InvoiceNumber, InvoiceNumber),
         TotalDue = isnull(@TotalDue, TotalDue),
         AddressLine1 = isnull(@AddressLine1, AddressLine1),
         AddressLine2 = isnull(@AddressLine2, AddressLine2),
         AddressLine3 = isnull(@AddressLine3, AddressLine3),
         AddressLine4 = isnull(@AddressLine4, AddressLine4),
         AddressLine5 = isnull(@AddressLine5, AddressLine5),
         PlanningComplete = isnull(@PlanningComplete, PlanningComplete),
         PlannedBy = isnull(@PlannedBy, PlannedBy),
         Release = isnull(@Release, Release),
         ReleasedBy = isnull(@ReleasedBy, ReleasedBy),
         Checking = isnull(@Checking, Checking),
         Checked = isnull(@Checked, Checked),
         Despatch = isnull(@Despatch, Despatch),
         DespatchChecked = isnull(@DespatchChecked, DespatchChecked),
         CompleteDate = isnull(@CompleteDate, CompleteDate),
         Availability = isnull(@Availability, Availability),
         CheckingCount = isnull(@CheckingCount, CheckingCount),
         Branding = isnull(@Branding, Branding),
         Downsizing = isnull(@Downsizing, Downsizing),
         CheckSheetPrinted = isnull(@CheckSheetPrinted, CheckSheetPrinted),
         CreateDate = isnull(@CreateDate, CreateDate),
         VehicleId = isnull(@VehicleId, VehicleId) 
   where IssueId = @IssueId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_IssueHistory_Insert
         @IssueId = @IssueId,
         @OutboundDocumentId = @OutboundDocumentId,
         @PriorityId = @PriorityId,
         @WarehouseId = @WarehouseId,
         @LocationId = @LocationId,
         @StatusId = @StatusId,
         @OperatorId = @OperatorId,
         @RouteId = @RouteId,
         @DeliveryNoteNumber = @DeliveryNoteNumber,
         @DeliveryDate = @DeliveryDate,
         @SealNumber = @SealNumber,
         @VehicleRegistration = @VehicleRegistration,
         @Remarks = @Remarks,
         @DropSequence = @DropSequence,
         @LoadIndicator = @LoadIndicator,
         @DespatchBay = @DespatchBay,
         @RoutingSystem = @RoutingSystem,
         @Delivery = @Delivery,
         @NumberOfLines = @NumberOfLines,
         @Total = @Total,
         @Complete = @Complete,
         @AreaType = @AreaType,
         @Units = @Units,
         @ShortPicks = @ShortPicks,
         @Releases = @Releases,
         @Weight = @Weight,
         @ConfirmedWeight = @ConfirmedWeight,
         @FirstLoaded = @FirstLoaded,
         @Interfaced = @Interfaced,
         @Loaded = @Loaded,
         @Pallets = @Pallets,
         @Volume = @Volume,
         @ContactListId = @ContactListId,
         @ManufacturingId = @ManufacturingId,
         @ReservedId = @ReservedId,
         @ParentIssueId = @ParentIssueId,
         @ASN = @ASN,
         @WaveId = @WaveId,
         @ReserveBatch = @ReserveBatch,
         @InvoiceNumber = @InvoiceNumber,
         @TotalDue = @TotalDue,
         @AddressLine1 = @AddressLine1,
         @AddressLine2 = @AddressLine2,
         @AddressLine3 = @AddressLine3,
         @AddressLine4 = @AddressLine4,
         @AddressLine5 = @AddressLine5,
         @PlanningComplete = @PlanningComplete,
         @PlannedBy = @PlannedBy,
         @Release = @Release,
         @ReleasedBy = @ReleasedBy,
         @Checking = @Checking,
         @Checked = @Checked,
         @Despatch = @Despatch,
         @DespatchChecked = @DespatchChecked,
         @CompleteDate = @CompleteDate,
         @Availability = @Availability,
         @CheckingCount = @CheckingCount,
         @Branding = @Branding,
         @Downsizing = @Downsizing,
         @CheckSheetPrinted = @CheckSheetPrinted,
         @CreateDate = @CreateDate,
         @VehicleId = @VehicleId,
         @CommandType = 'Update'
  
  return @Error
  
end
