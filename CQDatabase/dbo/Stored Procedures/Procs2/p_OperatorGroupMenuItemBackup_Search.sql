﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupMenuItemBackup_Search
  ///   Filename       : p_OperatorGroupMenuItemBackup_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:22
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorGroupMenuItemBackup table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorGroupMenuItemBackup.OperatorGroupId,
  ///   OperatorGroupMenuItemBackup.MenuItemId,
  ///   OperatorGroupMenuItemBackup.Access 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupMenuItemBackup_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         OperatorGroupMenuItemBackup.OperatorGroupId
        ,OperatorGroupMenuItemBackup.MenuItemId
        ,OperatorGroupMenuItemBackup.Access
    from OperatorGroupMenuItemBackup
  
end
