﻿/*
  /// <summary>
  ///   Procedure Name : p_Operator_Groups
  ///   Filename       : p_Operator_Groups.sql
  ///   Create By      : William
  ///   Date Created   : 09 July 2008
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OperatorGroups table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Operator.GroupId,
  ///   Operator.OperatorGroup 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Groups

as
begin
	 set nocount on;
  
	 declare @Error int
  select OperatorGroupId,
         OperatorGroup
    from OperatorGroup
  order by OperatorGroup
end
