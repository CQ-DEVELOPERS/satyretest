﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequestType_Parameter
  ///   Filename       : p_ManualFileRequestType_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:04
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ManualFileRequestType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ManualFileRequestType.ManualFileRequestTypeId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequestType_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as ManualFileRequestTypeId
        ,null as 'ManualFileRequestType'
  union
  select
         ManualFileRequestType.ManualFileRequestTypeId
        ,ManualFileRequestType.ManualFileRequestTypeId as 'ManualFileRequestType'
    from ManualFileRequestType
  
end
