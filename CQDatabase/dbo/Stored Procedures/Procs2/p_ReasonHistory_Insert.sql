﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReasonHistory_Insert
  ///   Filename       : p_ReasonHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:36
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ReasonHistory table.
  /// </remarks>
  /// <param>
  ///   @ReasonId int = null,
  ///   @Reason nvarchar(100) = null,
  ///   @ReasonCode nvarchar(20) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   ReasonHistory.ReasonId,
  ///   ReasonHistory.Reason,
  ///   ReasonHistory.ReasonCode,
  ///   ReasonHistory.CommandType,
  ///   ReasonHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReasonHistory_Insert
(
 @ReasonId int = null,
 @Reason nvarchar(100) = null,
 @ReasonCode nvarchar(20) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ReasonHistory
        (ReasonId,
         Reason,
         ReasonCode,
         CommandType,
         InsertDate)
  select @ReasonId,
         @Reason,
         @ReasonCode,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
