﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Store_Mixed_dropme
  ///   Filename       : p_Mobile_Store_Mixed_dropme.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Mar 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Mobile_Store_Mixed_dropme
(
 @operatorId          int,
 @barcode             nvarchar(30),
 @instructionId       int output
)
as
begin
	 set nocount on
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @Rowcount           int,
          @StatusId           int,
          @JobId              int,
          @PalletId           int,
          @StoreLocationId    int,
          @QCStatusCheck	  nvarchar(30)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
   
  if isnumeric(@barcode) = 1
    set @barcode = 'J:' + @barcode
  
  if isnumeric(replace(@barcode,'J:','')) = 1
  begin
    -- First Check if Inter Branch Transfer Putaway
    select @JobId = max(JobId)
      from Job (nolock)
     where ReferenceNumber = @barcode
    
    if @JobId is null
      select @JobId   = replace(@barcode,'J:',''),
             @barcode = null
  end
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @PalletId is not null
  begin
  
	 select @QCStatusCheck = s.StatusCode
		from Status s
		join Batch              b (nolock) on s.StatusId = b.StatusId
		join StorageUnitBatch sub (nolock) on b.BatchId = sub.BatchId
		join Instruction		i (nolock) on sub.StorageUnitBatchId = i.StorageUnitBatchId
		join Status            si (nolock) on i.StatusId = si.StatusId
	   where i.PalletId = @palletId
		 and si.StatusCode in ('W','S')
  
  if (@QCStatusCheck = 'QC')
  begin
	 set @InstructionId = -4
	goto error
    end 
	
    select @JobId = JobId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where it.InstructionTypeCode in ('S','SM','PR')
       and i.PalletId = @PalletId
  end     
  
  if @barcode is not null
  begin
    select @JobId = max(JobId)
      from Job    j (nolock)
      join Status s (nolock) on j.StatusId = s.StatusId
     where j.ReferenceNumber = @barcode
       and s.StatusCode not in ('C','F')
    
    select @Rowcount = @@rowcount
    
    if @@rowcount = 0
      select @JobId = max(JobId)
        from Job    j (nolock)
       where j.ReferenceNumber = @barcode
  end
  
  print '@jobId'
  print @jobId
  
  select top 1
         @jobId              = j.JobId,
         @instructionId      = i.InstructionId,
         @StoreLocationId = i.StoreLocationId
    from Job              j (nolock)
    join Status           s (nolock) on j.StatusId          = s.StatusId
    join Instruction      i (nolock) on j.JobId             = i.JobId
    join Status          si (nolock) on i.StatusId          = si.StatusId
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where isnull(j.OperatorId, @operatorId) = @operatorId
     and j.JobId                = @jobId
     and si.StatusCode            in ('W','S')
     and ((it.InstructionTypeCode = 'SM' and s.StatusCode in ('PR','A','S'))
     or   (it.InstructionTypeCode = 'PR' and s.StatusCode in ('PR','A','S')))
--     and isnull(i.PalletId,0)   = isnull(@PalletId,0)
  
  select @Rowcount = @@rowcount
  
  if @Rowcount = 0
  begin
    set @InstructionId = -1
    set @Error = 2
    goto error
  end
  
  if @StoreLocationId is null
  begin
    exec @Error = p_Receiving_Manual_Palletisation_Auto_Locations
     @InstructionId = @InstructionId
    
    if @Error <> 0
      goto error
  end
  
  select @StatusId = dbo.ufn_StatusId('I','S')
  
  exec @Error = p_Job_Update
   @JobId      = @JobId,
   @StatusId   = @StatusId,
   @OperatorId = @operatorId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @StatusId      = @StatusId,
   @OperatorId    = @OperatorId,
   @StartDate     = @GetDate
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Operator_Update
   @OperatorId = @OperatorId,
   @LastInstruction      = @GetDate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    rollback transaction
    return @Error


end
