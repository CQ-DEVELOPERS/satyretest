﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Job_Details_dropme
  ///   Filename       : p_Mobile_Product_Check_Job_Details_dropme.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Aug 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Job_Details_dropme
(
 @WarehouseId     int = null,
 @OperatorId      int = null,
 @ReferenceNumber nvarchar(30),
 @newJobId        int = null
)

as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   WarehouseId                     int,
   JobId                           int,
   InstructionId                   int,
   InstructionRefId                int,
   StorageUnitId                   int,
   ProductCode                     nvarchar(60),
   Product                         nvarchar(510),
   SKUCode                         nvarchar(100),
   SKU                             nvarchar(100),
   Batch                           nvarchar(100),
   PickedQuantity                  numeric(13,6),
   CheckedQuantity                 numeric(13,6),
   Variance                        int,
   PackQuantity                    int,
   JobQuantity                     int,
   BoxQuantity                     numeric(13,6),
   Boxes                           numeric(13)
  )
  
  declare @TableJobs as table
  (
   JobId                           int,
   InstructionId                   int,
   InstructionRefId                int,
   PickedQuantity                  numeric(13,6),
   CheckedQuantity                 numeric(13,6)
  )
  
  declare @JobId int
  
  if isnumeric(replace(@ReferenceNumber,'J:','')) = 1
  begin
    select @JobId   = replace(@ReferenceNumber,'J:','')
  end
  
  insert @TableResult
        (WarehouseId,
         JobId,
         InstructionId,
         InstructionRefId,
         StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         PickedQuantity,
         CheckedQuantity,
         Variance,
         PackQuantity,
         JobQuantity)
  select i.WarehouseId,
         i.JobId,
         i.InstructionId,
         i.InstructionRefId,
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         i.ConfirmedQuantity as 'PickedQuantity',
         i.CheckQuantity     as 'CheckedQuantity',
         null as 'Variance',
         null as 'PackQuantity',
         null as 'JobQuantity'
    from Instruction        i (nolock)
    join Job                j (nolock) on i.JobId               = j.JobId
    join Status             s (nolock) on j.StatusId            = s.StatusId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId  = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
   where (j.ReferenceNumber = @ReferenceNumber or j.JobId = @JobId)
     and s.StatusCode in ('QA','CK','CD')
  
  --insert @TableJobs
  --      (InstructionId,
  --       PickedQuantity,
  --       CheckedQuantity)
  --select isnull(i.InstructionRefId, i.InstructionId),
  --       sum(i.ConfirmedQuantity),
  --       sum(i.CheckQuantity)
  --  from @TableResult tr
  --  join Instruction   i (nolock) on (tr.InstructionId = i.InstructionId or tr.InstructionId = i.InstructionRefId)
  --group by isnull(i.InstructionRefId, i.InstructionId)
  
  insert @TableJobs
        (JobId,
         InstructionId,
         InstructionRefId,
         PickedQuantity,
         CheckedQuantity)
  select i.JobId,
         i.InstructionId,
         i.InstructionRefId,
         sum(i.ConfirmedQuantity),
         sum(i.CheckQuantity)
    from @TableResult tr
    join Instruction   i (nolock) on ((tr.InstructionId = i.InstructionRefId and i.JobId != tr.JobId))
  group by i.JobId,
           i.InstructionId,
           i.InstructionRefId
  
  update tr
     set PickedQuantity  = isnull(tr.PickedQuantity,0) + (select isnull(sum(tj.PickedQuantity), 0)
                                                  from @TableJobs   tj 
                                                 where tr.InstructionId = tj.InstructionRefId)
    from @TableResult tr
  
  update tr
     set CheckedQuantity  = isnull(tr.CheckedQuantity,0) + (select isnull(sum(tj.CheckedQuantity), 0)
                                                  from @TableJobs   tj 
                                                 where tr.InstructionId = tj.InstructionRefId)
    from @TableResult tr
  
  --update tr
  --   set PickedQuantity  = tr.PickedQuantity + tj.PickedQuantity,
  --       CheckedQuantity = tr.CheckedQuantity + isnull(tj.CheckedQuantity,0)
  --  from @TableResult tr
  --  join @TableJobs   tj on ((tr.InstructionId = tj.InstructionRefId))
  
  update tr
     set JobQuantity = (select sum(i.CheckQuantity)
                          from Instruction i (nolock) 
                         where tr.InstructionId = isnull(i.InstructionRefId, tr.InstructionId)
                           and i.JobId = @newJobId)
   --where i.JobId = @newJobId
    from @TableResult      tr
  
  update @TableResult
     set Variance = PickedQuantity - isnull(CheckedQuantity, 0)
  
  update tr
     set BoxQuantity = p.Quantity
    from @TableResult   tr
    join Pack            p (nolock) on tr.StorageUnitId = p.StorageUnitId
                                   and tr.WarehouseId   = p.WarehouseId
    join PackType       pt (nolock) on p.PackTypeId     = pt.PackTypeId
   where pt.OutboundSequence   between 2 and (select max(OutboundSequence) - 1 from PackType (nolock))
  
  update @TableResult
     set Boxes = floor(PickedQuantity / BoxQuantity)
  
  select JobId,
         InstructionId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         PickedQuantity,
         CheckedQuantity,
         BoxQuantity,
         Boxes,
         Variance,
         PackQuantity,
         JobQuantity
    from @TableResult
end
