﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_List_Dangerous_Goods
  ///   Filename       : p_List_Dangerous_Goods.sql
  ///   Create By      : Karen
  ///   Date Created   : Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_List_Dangerous_Goods


as
begin
	 set nocount on;
  
  begin
    select '-1'    as 'DangerousGoodsId',
           '{None}' as 'DangerousGoodsCode',
           '{None}' as 'DangerousGoods'
    union
    select DangerousGoodsId,
           DangerousGoodsCode,
           DangerousGoods
      from DangerousGoods (nolock)
    order by DangerousGoods
  end
 end
