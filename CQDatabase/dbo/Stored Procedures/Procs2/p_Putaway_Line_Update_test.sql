﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Putaway_Line_Update_test
  ///   Filename       : p_Putaway_Line_Update_test.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Putaway_Line_Update_test
(
 @instructionId     int,
 @confirmedQuantity int,
 @operatorId        int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @WarehouseId       int,
          @StatusCode        nvarchar(10),
          @Quantity          int,
          @PalletQuantity    int,
          @StorageUnitId     int
  
  if @operatorId = 0
    set @operatorId = null
  
  select @WarehouseId   = i.WarehouseId,
         @StatusCode    = s.StatusCode,
         @Quantity      = i.Quantity,
         @StorageUnitId = sub.StorageUnitId
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Job                j (nolock) on i.JobId = j.JobId
    join Status             s (nolock) on j.StatusId = s.StatusId
   where i.InstructionId = @instructionId
  
  select top 1 @PalletQuantity = Quantity
    from Pack     pk (nolock)
    join PackType pt (nolock) on pk.PackTypeId = pt.PackTypeId
   where pk.WarehouseId   = @WarehouseId
     and pk.StorageUnitId = @StorageUnitId
     and pt.InboundSequence = 1
  order by pk.Quantity desc
  
  set @Errormsg = 'Error executing p_Putaway_Line_Update_test'
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if @confirmedQuantity > @PalletQuantity
  begin
    set @ErrorMsg = 'Confirmed quantity cannot be greater than pallet quantity.'
    goto error
  end
  
  if (select dbo.ufn_Configuration(210, @WarehouseId)) = 1 and @StatusCode = 'PR'
  begin
    exec @Error = p_Production_Update_Accepted_Quantity
     @InstructionId = @InstructionId,
     @Sign          = '-'
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_Instruction_Update
   @InstructionId     = @instructionId,
   @Quantity          = @confirmedQuantity,
   @ConfirmedQuantity = @confirmedQuantity,
   @OperatorId        = @operatorId
  
  if @Error <> 0
    goto error
  
  if (select dbo.ufn_Configuration(210, @WarehouseId)) = 1 and @StatusCode = 'PR'
  begin
    exec @Error = p_Production_Update_Accepted_Quantity
     @InstructionId = @InstructionId,
     @Sign          = '+'
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
