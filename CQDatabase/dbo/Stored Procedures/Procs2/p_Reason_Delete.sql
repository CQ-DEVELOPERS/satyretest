﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Reason_Delete
  ///   Filename       : p_Reason_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:22
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Reason table.
  /// </remarks>
  /// <param>
  ///   @ReasonId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Reason_Delete
(
 @ReasonId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Reason
     where ReasonId = @ReasonId
  
  select @Error = @@Error
  
  
  return @Error
  
end
