﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductMovement_Insert
  ///   Filename       : p_ProductMovement_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:03
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ProductMovement table.
  /// </remarks>
  /// <param>
  ///   @ProductMovementId int = null,
  ///   @WarehouseId int = null,
  ///   @StorageUnitId int = null,
  ///   @SKUId int = null,
  ///   @AreaId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @MovementDate datetime = null,
  ///   @OpeningBalance int = null,
  ///   @ReceivedQuantity float = null,
  ///   @IssuedQuantity float = null,
  ///   @Balance int = null 
  /// </param>
  /// <returns>
  ///   ProductMovement.ProductMovementId,
  ///   ProductMovement.WarehouseId,
  ///   ProductMovement.StorageUnitId,
  ///   ProductMovement.SKUId,
  ///   ProductMovement.AreaId,
  ///   ProductMovement.ProductCode,
  ///   ProductMovement.MovementDate,
  ///   ProductMovement.OpeningBalance,
  ///   ProductMovement.ReceivedQuantity,
  ///   ProductMovement.IssuedQuantity,
  ///   ProductMovement.Balance 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductMovement_Insert
(
 @ProductMovementId int = null,
 @WarehouseId int = null,
 @StorageUnitId int = null,
 @SKUId int = null,
 @AreaId int = null,
 @ProductCode nvarchar(60) = null,
 @MovementDate datetime = null,
 @OpeningBalance int = null,
 @ReceivedQuantity float = null,
 @IssuedQuantity float = null,
 @Balance int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ProductMovement
        (ProductMovementId,
         WarehouseId,
         StorageUnitId,
         SKUId,
         AreaId,
         ProductCode,
         MovementDate,
         OpeningBalance,
         ReceivedQuantity,
         IssuedQuantity,
         Balance)
  select @ProductMovementId,
         @WarehouseId,
         @StorageUnitId,
         @SKUId,
         @AreaId,
         @ProductCode,
         @MovementDate,
         @OpeningBalance,
         @ReceivedQuantity,
         @IssuedQuantity,
         @Balance 
  
  select @Error = @@Error
  
  
  return @Error
  
end
