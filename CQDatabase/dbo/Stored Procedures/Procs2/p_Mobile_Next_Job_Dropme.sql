﻿/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Next_Job_Dropme
  ///   Filename       : p_Mobile_Next_Job_Dropme.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Mobile_Next_Job_Dropme
(
 @OperatorId          int,
 @JobId               int = null output,
 @InstructionTypeCode nvarchar(10) = null,
 @ReferenceNumber     nvarchar(30) = null,
 @Debug               bit = 0
)
as
begin
	 set nocount on
  
  declare @MobileLogId int
  
  if @Debug = 0
  begin
    insert MobileLog
          (ProcName,
           OperatorId,
           JobId,
           ErrorMsg,
           ReferenceNumber,
           StartDate)
    select OBJECT_NAME(@@PROCID),
           @OperatorId,
           @JobId,
           'InstructionTypeCode = ' + @InstructionTypeCode,
           @ReferenceNumber,
           getdate()
    
    select @MobileLogId = scope_identity()
  end
  
  declare @TableResult as table
  (
   JobId          int,
   InstructionId  int,
   CreateDate     datetime,
   PickLocationId int,
   DTPriority     int,
   OGPriority     int,
   JobPriority    int,
   LGPriority     int default 0,
   Type           char(2),
   KIT            bit default 0,
   IssueId        int,
   PickAisle      nvarchar(10)
  )
  
  --declare @TableInstructionTypeCode as table
  --(
  -- InstructionTypeCode nvarchar(10)
  --)
  
  declare @error             int,
          @errormsg          nvarchar(500),
          @OperatorGroupId   int,
          @WarehouseId       int,
          @rowcount          int = 0,
          @StatusId          int,
          @PickAisle         nvarchar(10),
          @StoreAisle        nvarchar(10),
          @NarrowAisle       bit = 0,
          @Interleaving      bit = 0
  
  select @StatusId = dbo.ufn_StatusId('IS','S')
  
  if @JobId = -1
    set @JobId = null
  
  select @OperatorGroupId = o.OperatorGroupId,
         @WarehouseId     = o.WarehouseId,
         @PickAisle       = o.PickAisle,
         @StoreAisle      = o.StoreAisle,
         @NarrowAisle     = og.NarrowAisle,
         @Interleaving    = og.Interleaving
    from Operator       o (nolock)
    join OperatorGroup og (nolock) on o.OperatorGroupId = og.OperatorGroupId
   where o.OperatorId = @OperatorId
  
  if @Debug = 1
    select @OperatorGroupId as 'OperatorGroupId',
           @WarehouseId     as 'WarehouseId',
           @PickAisle       as 'PickAisle',
           @StoreAisle      as 'StoreAisle',
           @NarrowAisle     as 'NarrowAisle',
           @Interleaving    as 'Interleaving'
  
  --update Operator
  --   set Aisle = null
  -- where OperatorId = @OperatorId
  
  --select @error = @@ERROR
  
  --if @error <> 0
  --begin
  --  set @JobId = null
  --  set @Error = -1
  --  goto error
  --end
  
  --insert @TableInstructionTypeCode
  --      (InstructionTypeCode)
  --select 'M' union
  --select 'P' union
  --select 'R'
  
  if @InstructionTypeCode in ('P','M','R')
  begin
    if @jobId is null
    begin
      if @NarrowAisle = 1
      begin
        --if @PickAisle is not null -- Not in an Aisle so do movement
        --  delete @TableInstructionTypeCode
        --   where InstructionTypeCode in ('P','R')
        --else
        --  delete @TableInstructionTypeCode
        --   where InstructionTypeCode in ('M')
        
        set @rowcount = 0
        
        if @PickAisle is not null
        begin
          insert @TableResult
                (JobId,
                 JobPriority,
                 InstructionId,
                 OGPriority,
                 Type,
                 LGPriority,
                 CreateDate)
          select distinct j.JobId,
                 j.PriorityId,
                 i.InstructionId,
                 ogit.OrderBy,
                 sj.Type,
                 isnull(lg.OrderBy,999999),
                 i.CreateDate
            from OperatorGroupInstructionType ogit (nolock)
            join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
            -- GS -2013-06-28
            join Location                    store (nolock) on i.StoreLocationId      = store.LocationId
                                                and store.Ailse             = isnull(@PickAisle, store.Ailse)
            join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
            join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
            join Job                          j    (nolock) on i.JobId                = j.JobId
            left
            join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
            left
            join AreaLocation                   al (nolock) on i.StoreLocationId       = al.LocationId
            join AreaOperatorGroup             aog (nolock) on isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                           and ogit.OperatorGroupId   = aog.OperatorGroupId
            
            -- Added for Forklift's not allowed to work in VnA
            join AreaLocation                  pal (nolock) on i.PickLocationId        = pal.LocationId
            join AreaOperatorGroup            paog (nolock) on pal.AreaId              = paog.AreaId
                                                           and ogit.OperatorGroupId    = paog.OperatorGroupId
            
            join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
            join Status                       si   (nolock) on i.StatusId             = si.StatusId
            -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
            join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                           and ogit.OperatorGroupId   = lg.OperatorGroupId
           where ogit.OperatorGroupId             = @OperatorGroupId
             and j.WarehouseId                    = @WarehouseId
             and isnull(j.OperatorId,@OperatorId) = @OperatorId
             and sj.StatusCode                    in ('RL','S')
             and si.StatusCode                    in ('W','S')
             and it.InstructionTypeCode           in ('P','M','R') -- (select InstructionTypeCode from @TableInstructionTypeCode)
          
          select @rowcount = @@ROWCOUNT
          
          if @rowcount = 0
          begin
            insert @TableResult
                  (JobId,
                   JobPriority,
                   InstructionId,
                   OGPriority,
                   Type,
                   LGPriority,
                   CreateDate)
            select distinct j.JobId,
                   j.PriorityId,
                   i.InstructionId,
                   ogit.OrderBy,
                   sj.Type,
                   isnull(lg.OrderBy,999999),
                   i.CreateDate
              from OperatorGroupInstructionType ogit (nolock)

              join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
              join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
              join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
              join Job                          j    (nolock) on i.JobId                = j.JobId
              left
              join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
              left
              join AreaLocation                   al (nolock) on i.StoreLocationId      = al.LocationId
              join AreaOperatorGroup             aog (nolock) on isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                             and ogit.OperatorGroupId   = aog.OperatorGroupId
              
              -- Added for Forklift's not allowed to work in VnA
              join AreaLocation                  pal (nolock) on i.PickLocationId        = pal.LocationId
              join AreaOperatorGroup            paog (nolock) on pal.AreaId              = paog.AreaId
                                                             and ogit.OperatorGroupId    = paog.OperatorGroupId
              
              join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
              join Status                       si   (nolock) on i.StatusId             = si.StatusId
              -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
              join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                             and ogit.OperatorGroupId   = lg.OperatorGroupId
             where ogit.OperatorGroupId             = @OperatorGroupId
               and j.WarehouseId                    = @WarehouseId
               and isnull(j.OperatorId,@OperatorId) = @OperatorId
               and sj.StatusCode                    in ('RL','S')
               and si.StatusCode                    in ('W','S')
               and it.InstructionTypeCode           in ('P','M','R') -- (select InstructionTypeCode from @TableInstructionTypeCode)
            
            select @rowcount = @@ROWCOUNT
          end
        end
        
        if @StoreAisle is not null
        begin
          insert @TableResult
                (JobId,
                 JobPriority,
                 InstructionId,
                 OGPriority,
                 Type,
                 LGPriority,
                 CreateDate)
          select distinct j.JobId,
                 j.PriorityId,
                 i.InstructionId,
                 ogit.OrderBy,
                 sj.Type,
                 isnull(lg.OrderBy,999999),
                 i.CreateDate
            from OperatorGroupInstructionType ogit (nolock)
            join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
  
          -- GS -2013-06-28
            join Location                     pick (nolock) on i.PickLocationId      = pick.LocationId
                                                           and pick.Ailse             = isnull(@StoreAisle, pick.Ailse)
            join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
            join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
            join Job                          j    (nolock) on i.JobId                = j.JobId
            left
            join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
            left
            join AreaLocation                   al (nolock) on i.PickLocationId      = al.LocationId
            join AreaOperatorGroup             aog (nolock) on isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                           and ogit.OperatorGroupId   = aog.OperatorGroupId
            
            -- Added for Forklift's not allowed to work in VnA
            join AreaLocation                  sal (nolock) on i.StoreLocationId       = sal.LocationId
            join AreaOperatorGroup            saog (nolock) on sal.AreaId              = saog.AreaId
                                                           and ogit.OperatorGroupId    = saog.OperatorGroupId
            
            join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
            join Status                       si   (nolock) on i.StatusId             = si.StatusId
            -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
            join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                           and ogit.OperatorGroupId   = lg.OperatorGroupId
           where ogit.OperatorGroupId             = @OperatorGroupId
             and j.WarehouseId   = @WarehouseId
             and isnull(j.OperatorId,@OperatorId) = @OperatorId
             and sj.StatusCode                    in ('RL','S')
             and si.StatusCode                    in ('W','S')
             and it.InstructionTypeCode           in ('P','M','R') -- (select InstructionTypeCode from @TableInstructionTypeCode)
          
          select @rowcount = @@ROWCOUNT
          
          if @rowcount = 0
          begin
            insert @TableResult
                  (JobId,
                   JobPriority,
                   InstructionId,
                   OGPriority,
                   Type,
                   LGPriority,
                   CreateDate)
            select distinct j.JobId,
                   j.PriorityId,
                   i.InstructionId,
                   ogit.OrderBy,
                   sj.Type,
                   isnull(lg.OrderBy,999999),
                   i.CreateDate
              from OperatorGroupInstructionType ogit (nolock)
              join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
              join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
              join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
              join Job                          j    (nolock) on i.JobId                = j.JobId
              left
              join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
              left
              join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
              join AreaOperatorGroup             aog (nolock) on isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                             and ogit.OperatorGroupId   = aog.OperatorGroupId
              
              -- Added for Forklift's not allowed to work in VnA
              join AreaLocation                  sal (nolock) on i.StoreLocationId       = sal.LocationId
              join AreaOperatorGroup            saog (nolock) on sal.AreaId              = saog.AreaId
                                                             and ogit.OperatorGroupId    = saog.OperatorGroupId
               
                                              
              join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
              join Status                       si   (nolock) on i.StatusId             = si.StatusId
    
          -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
              join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                             and ogit.OperatorGroupId   = lg.OperatorGroupId
             where ogit.OperatorGroupId             = @OperatorGroupId
               and j.WarehouseId                    = @WarehouseId
               and isnull(j.OperatorId,@OperatorId) = @OperatorId
               and sj.StatusCode                    in ('RL','S')
               and si.StatusCode                    in ('W','S')
               and it.InstructionTypeCode           in ('P','M','R') -- (select InstructionTypeCode from @TableInstructionTypeCode)
            
            select @rowcount = @@ROWCOUNT
          end
        end
      end
      else if @Interleaving = 1
      begin
        if @StoreAisle is not null
        begin
          insert @TableResult
                (JobId,
                 JobPriority,
                 InstructionId,
                 OGPriority,
                 Type,
                 LGPriority,
                 CreateDate)
          select distinct j.JobId,
                 j.PriorityId,
                 i.InstructionId,
                 ogit.OrderBy,
                 sj.Type,
                 isnull(lg.OrderBy,999999),
                 i.CreateDate
            from OperatorGroupInstructionType ogit (nolock)
            join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
            -- GS -2013-06-28
            join Location                     pick (nolock) on i.PickLocationId      = pick.LocationId
                                                           and pick.Ailse             = isnull(@StoreAisle, pick.Ailse)
            join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
            join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
            join Job                          j    (nolock) on i.JobId                = j.JobId
            left
            join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
            join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
            join Area                            a (nolock) on al.AreaId              = a.AreaId
            join AreaOperatorGroup             aog (nolock) on isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                           and ogit.OperatorGroupId   = aog.OperatorGroupId
            
            ---- Added for Forklift's not allowed to work in VnA
            --join AreaLocation                  sal (nolock) on i.StoreLocationId       = sal.LocationId
            --join AreaOperatorGroup            saog (nolock) on sal.AreaId              = saog.AreaId
            --                                               and ogit.OperatorGroupId    = saog.OperatorGroupId
            
            join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
            join Status                       si   (nolock) on i.StatusId             = si.StatusId
            -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
            join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                           and ogit.OperatorGroupId   = lg.OperatorGroupId
           where ogit.OperatorGroupId             = @OperatorGroupId
             and j.WarehouseId                    = @WarehouseId
             and isnull(j.OperatorId,@OperatorId) = @OperatorId
             and sj.StatusCode                   in ('RL','S')
             and si.StatusCode                   in ('W','S')
             and it.InstructionTypeCode          in ('P','M','R') -- (select InstructionTypeCode from @TableInstructionTypeCode)
             and a.InterLeaving                   = 1
             and i.StoreLocationid is null
           and not exists(select 1
                              from Instruction i2
                              join Status s on i2.StatusId = s.StatusId
                             where i.InstructionRefId = i2.InstructionId
                               and s.StatusCode in ('W','S'))
          
          select @rowcount = @@ROWCOUNT
          if @Debug = 1
            select @rowcount as 'Interleaving Store Count'
        end
        else if @PickAisle is not null
        begin
          insert @TableResult
                (JobId,
                 JobPriority,
                 InstructionId,
                 OGPriority,
                 Type,
                 LGPriority,
                 CreateDate)
          select distinct j.JobId,
                 j.PriorityId,
                 i.InstructionId,
                 ogit.OrderBy,
                 sj.Type,
                 isnull(lg.OrderBy,999999),
                 i.CreateDate
            from OperatorGroupInstructionType ogit (nolock)
            join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
            -- GS -2013-06-28
            join Location                     pick (nolock) on i.PickLocationId      = pick.LocationId
            join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
            join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
            join Job                          j    (nolock) on i.JobId                = j.JobId
            left
            join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
            join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
            join Area                            a (nolock) on al.AreaId              = a.AreaId
            join AreaOperatorGroup             aog (nolock) on isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                           and ogit.OperatorGroupId   = aog.OperatorGroupId
            
            -- Added for Forklift's not allowed to work in VnA
            join AreaLocation                  sal (nolock) on i.StoreLocationId       = sal.LocationId
            join AreaOperatorGroup            saog (nolock) on sal.AreaId              = saog.AreaId
                                                           and ogit.OperatorGroupId    = saog.OperatorGroupId
            join Location                    store (nolock) on sal.LocationId          = store.LocationId
                                                           and store.Ailse             = isnull(@PickAisle, store.Ailse)
            
            join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
            join Status                       si   (nolock) on i.StatusId             = si.StatusId
            -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
            join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                           and ogit.OperatorGroupId   = lg.OperatorGroupId
           where ogit.OperatorGroupId             = @OperatorGroupId
             and j.WarehouseId                    = @WarehouseId
             and isnull(j.OperatorId,@OperatorId) = @OperatorId
             and sj.StatusCode                   in ('RL','S')
             and si.StatusCode                   in ('W','S')
             and it.InstructionTypeCode          in ('P','M','R') -- (select InstructionTypeCode from @TableInstructionTypeCode)
             and a.InterLeaving                   = 1
           and not exists(select 1
                              from Instruction i2
                              join Status s on i2.StatusId = s.StatusId
                             where i.InstructionRefId = i2.InstructionId
                               and s.StatusCode in ('W','S'))
          
          select @rowcount = @@ROWCOUNT
          if @Debug = 1
            select @rowcount as 'Interleaving Pick Count'
        end
      end
      
      if @rowcount = 0
        insert @TableResult
              (JobId,
               JobPriority,
               InstructionId,
               OGPriority,
               Type,
               LGPriority,
               CreateDate)
        select distinct j.JobId,
               j.PriorityId,
               i.InstructionId,
               ogit.OrderBy,
               sj.Type,
               isnull(lg.OrderBy,999999),
               i.CreateDate
          from OperatorGroupInstructionType ogit (nolock)
          join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
          join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
          join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
          join Job                          j    (nolock) on i.JobId                = j.JobId
          left
          join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
          left
          join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
          join AreaOperatorGroup             aog (nolock) on isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                         and ogit.OperatorGroupId   = aog.OperatorGroupId
          
          -- Added for Forklift's not allowed to work in VnA
          join AreaLocation                  sal (nolock) on i.StoreLocationId       = sal.LocationId
          join AreaOperatorGroup            saog (nolock) on sal.AreaId              = saog.AreaId
            
                                             and ogit.OperatorGroupId    = saog.OperatorGroupId
          
          join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
          join Status                       si   (nolock) on i.StatusId             = si.StatusId
          -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
          join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                 
                                        and ogit.OperatorGroupId   = lg.OperatorGroupId
         where ogit.OperatorGroupId             = @OperatorGroupId
           and j.WarehouseId                    = @WarehouseId
           and isnull(j.OperatorId,@OperatorId) = @OperatorId
           and sj.StatusCode                    in ('RL','S')
           and si.StatusCode                    in ('W','S')
           and it.InstructionTypeCode           in ('P','R','M')
           and not exists(select 1
                              from Instruction i2
                              join Status s on i2 .StatusId = s.StatusId
                             where i.InstructionRefId = i2.InstructionId
                               and s.StatusCode in ('W','S'))
      
      set @rowcount = @@rowcount
      if @Debug = 1
            select @rowcount as 'Interleaving Pick or Store Count'
      
      if @rowcount = 0
        insert @TableResult
              (JobId,
               JobPriority,
               InstructionId,
               OGPriority,
               Type,
               LGPriority,
               CreateDate)
        select distinct j.JobId,
               j.PriorityId,
               i.InstructionId,
               ogit.OrderBy,
               sj.Type,
               isnull(lg.OrderBy,999999),
               i.CreateDate
          from OperatorGroupInstructionType ogit (nolock)
          join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
          join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
          join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
          join Job                          j    (nolock) on i.JobId                = j.JobId
          left
          join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
          left
          join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
          join AreaOperatorGroup             aog (nolock) on isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                         and ogit.OperatorGroupId   = aog.OperatorGroupId
          
          -- Added for Forklift's not allowed to work in VnA
          join AreaLocation                  sal (nolock) on i.StoreLocationId       = sal.LocationId
          join MovementSequence               ms (nolock) on ms.PickAreaId = al.AreaId
                                                         and ms.StoreAreaId = sal.AreaId
                                                         and ms.OperatorGroupId = ogit.OperatorGroupId
          
          join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
          join Status                       si   (nolock) on i.StatusId             = si.StatusId
          -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
          join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                         and ogit.OperatorGroupId   = lg.OperatorGroupId
         where ogit.OperatorGroupId             = @OperatorGroupId
           and j.WarehouseId                    = @WarehouseId
           and isnull(j.OperatorId,@OperatorId) = @OperatorId
           and sj.StatusCode                    in ('RL','S')
           and si.StatusCode                    in ('W','S')
           and it.InstructionTypeCode           in ('P','R','M')
           and not exists(select 1
                              from Instruction i2
                              join Status s on i2 .StatusId = s.StatusId
                             where i.InstructionRefId = i2.InstructionId
                               and s.StatusCode in ('W','S'))
      
      set @rowcount = @@rowcount
      if @Debug = 1
            select @rowcount as 'Interleaving Pick or Store Movement Sequence Count'
      
      if @rowcount = 0
        insert @TableResult
              (JobId,
               JobPriority,
               InstructionId,
               OGPriority,
               Type,
               LGPriority,
               CreateDate)
        select distinct j.JobId,
               j.PriorityId,
               i.InstructionId,
               ogit.OrderBy,
               sj.Type,
               isnull(lg.OrderBy,999999),
               i.CreateDate
          from OperatorGroupInstructionType ogit (nolock)
          join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
          join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
          join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
          join Job                          j    (nolock) on i.JobId                = j.JobId
          left
          join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
          left
          join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
          join AreaOperatorGroup             aog (nolock) on isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                         and ogit.OperatorGroupId   = aog.OperatorGroupId
          
          join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
          join Status                       si   (nolock) on i.StatusId             = si.StatusId
          -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
          join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                         and ogit.OperatorGroupId   = lg.OperatorGroupId
         where ogit.OperatorGroupId             = @OperatorGroupId
           and j.WarehouseId                    = @WarehouseId
           and isnull(j.OperatorId,@OperatorId) = @OperatorId
           and sj.StatusCode                    in ('RL','S')
           and si.StatusCode                    in ('W','S')
           and it.InstructionTypeCode           in ('P','R','M')
           and not exists(select 1
                              from Instruction i2
                              join Status s on i2 .StatusId = s.StatusId
                             where i.InstructionRefId = i2.InstructionId
                               and s.StatusCode in ('W','S'))
           and i.StoreLocationId is null
           
           
      set @rowcount = @@rowcount
      if @Debug = 1
        select @rowcount as 'Interleaving Store Location null Count'
    end
    else
    begin
      insert @TableResult
            (JobId,
             JobPriority,
             InstructionId,
             OGPriority,
             Type,
             LGPriority,
             CreateDate)
      select distinct j.JobId,
             j.PriorityId,
             i.InstructionId,
             ogit.OrderBy,
             sj.Type,
             isnull(lg.OrderBy,999999),
             i.CreateDate
        from OperatorGroupInstructionType ogit (nolock)
        join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
        join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
        join StorageUnitBatch              sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
        join Job                          j    (nolock) on i.JobId                = j.JobId
        --left
        --join StorageUnitArea               sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
        --left
        join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
        join AreaOperatorGroup             aog (nolock) on al.AreaId              = aog.AreaId  -- isnull(al.AreaId, sua.AreaId) = aog.AreaId
                                                       and ogit.OperatorGroupId   = aog.OperatorGroupId
       
        join Status                       sj   (nolock) on j.StatusId             = sj.StatusId
        join Status                       si   (nolock) on i.StatusId             = si.StatusId
        -- Added for Reach trucks and Forklifts (Forklifts cannot work in high racking)
        join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                       and ogit.OperatorGroupId   = lg.OperatorGroupId
       where ogit.OperatorGroupId             = @OperatorGroupId
         and j.WarehouseId                    = @WarehouseId
         and isnull(j.OperatorId,@OperatorId) = @OperatorId
         and sj.StatusCode                   in ('RL','S')
         and si.StatusCode                   in ('W','S')
         and it.InstructionTypeCode          in ('P','M','R') -- (select InstructionTypeCode from @TableInstructionTypeCode)
         and j.JobId                          = @jobId
      
      set @rowcount = @@rowcount
    end
  end
  else
  begin
    if @JobId is null
    begin
      insert @TableResult
            (JobId,
             JobPriority,
             InstructionId,
             OGPriority,
             Type,
             CreateDate)
      select distinct j.JobId,
             j.PriorityId,
             i.InstructionId,
             ogit.OrderBy,
             sj.Type,
             i.CreateDate
        from OperatorGroupInstructionType ogit (nolock)
        join Instruction                     i (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
        join InstructionType     it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
        join Job                             j (nolock) on i.JobId                = j.JobId
        join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
        join AreaOperatorGroup             aog (nolock) on al.AreaId              = aog.AreaId
                                                       and ogit.OperatorGroupId   = aog.OperatorGroupId
        join Priority                        p (nolock) on j.PriorityId           = p.PriorityId
        join Status                         sj (nolock) on j.StatusId             = sj.StatusId
        join Status                         si (nolock) on i.StatusId             = si.StatusId
       where ogit.OperatorGroupId             = @OperatorGroupId
         and j.WarehouseId                    = @WarehouseId
         and j.OperatorId                     = @OperatorId -- Try get any jobs allocated to you first
         and sj.StatusCode                    in ('RL','S')
         and si.StatusCode                    in ('W','S')
         and j.ReferenceNumber               is null
         and it.InstructionTypeCode          in ('PM','PS','FM','O')
      
      if @@ROWCOUNT = 0
        insert @TableResult
              (JobId,
               JobPriority,
               InstructionId,
               OGPriority,
               Type,
               CreateDate)
        select distinct j.JobId,
               j.PriorityId,
               i.InstructionId,
               ogit.OrderBy,
               sj.Type,
               i.CreateDate
          from OperatorGroupInstructionType ogit (nolock)
          join Instruction                     i (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
          join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
 
         join Job                             j (nolock) on i.JobId                = j.JobId
          join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
          join AreaOperatorGroup             aog (nolock) on al.AreaId              = aog.AreaId
                                                         and ogit.OperatorGroupId   = aog.OperatorGroupId
          join Priority                        p (nolock) on j.PriorityId           = p.PriorityId
          join Status                         sj (nolock) on j.StatusId             = sj.StatusId
          join Status                         si (nolock) on i.StatusId             = si.StatusId
         where ogit.OperatorGroupId             = @OperatorGroupId
           and j.WarehouseId                    = @WarehouseId
           and isnull(j.OperatorId,@OperatorId) = @OperatorId
           and sj.StatusCode                    in ('RL','S')
           and si.StatusCode                    in ('W','S')
           and j.ReferenceNumber               is null
           and it.InstructionTypeCode          in ('PM','PS','FM','O')
      
      set @rowcount = @@rowcount
    end
    else
    begin
      insert @TableResult
            (JobId,
             JobPriority,
             InstructionId,
             OGPriority,
             Type,
             CreateDate)
      select distinct j.JobId,
             j.PriorityId,
             i.InstructionId,
             ogit.OrderBy,
             sj.Type,
             i.CreateDate
        from OperatorGroupInstructionType ogit (nolock)
        join Instruction                     i (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
        join InstructionType                it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
        join Job                             j (nolock) on i.JobId                = j.JobId
        join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
--        join AreaOperatorGroup             aog (nolock) on al.AreaId              = aog.AreaId
--               and ogit.OperatorGroupId   = aog.OperatorGroupId
        join Priority                        p (nolock) on j.PriorityId           = p.PriorityId
        join Status                         sj (nolock) on j.StatusId             = sj.StatusId
        join Status                         si (nolock) on i.StatusId             = si.StatusId
       where ogit.OperatorGroupId             = @OperatorGroupId
         and j.WarehouseId                    = @WarehouseId
         and isnull(j.OperatorId,@OperatorId) = @OperatorId
         and sj.StatusCode                   in ('RL','S')
         and si.StatusCode                   in ('W','S')
         and it.InstructionTypeCode          in ('PM','PS','FM','O')
         and j.JobId                          = @JobId
         --and j.JobId in (29691,29692,29693,29694,29695)
      
      set @rowcount = @@rowcount
    end
  end
  
  select @rowcount = count(1) from @TableResult
  
  if @rowcount = 0
  begin
  
  set @JobId = null
    set @Error = -1
    goto error
  end
  
  update tr
     set KIT = 1
    from @TableResult tr
    join JobBOMParent  p (nolock) on tr.JobId = p.JobId
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @error <> 0
    goto error
  
  update tr
     set IssueId = ili.IssueId
    from @TableResult          tr
    join IssueLineInstruction ili (nolock) on tr.InstructionId = ili.InstructionId
   where KIT = 1
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @error <> 0
    goto error
  
  --select * from @TableResult
  
  declare @TableJobs as table
  (
   IssueId int
  )
  
  insert @TableJobs
        (IssueId)
  select distinct ili.IssueId
    from @TableResult          tr
    join IssueLineInstruction ili (nolock) on tr.Issueid = ili.IssueId
    join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
    join Job                    j (nolock) on i.JobId = j.JobId
    join Status                 s (nolock) on j.StatusId = s.StatusId
    left

    join JobBOMParent           p (nolock) on j.JobId = p.JobId
   where s.StatusCode       in ('RL','S','CK')
     and p.BOMInstructionId is null
  
  --select * from @TableJobs
  
  --delete tr
  --  from @TableResult          tr
  --  join IssueLineInstruction ili (nolock) on tr.Issueid = ili.IssueId
  --  join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
  --  join Job                    j (nolock) on i.JobId = j.JobId
  --  left
  --  join JobBOMParent           p (nolock) on tr.JobId = p.JobId
  --  join Status                 s (nolock) on j.StatusId = s.StatusId
  -- where s.StatusCode       in ('RL','S','CK')
  --   and tr.KIT              = 1
  
  delete tr
    from @TableResult          tr
    join @TableJobs       
     tj on tr.IssueId = tj.IssueId
   where tr.KIT              = 1
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @error <> 0
    goto error
  
  --select * from @TableResult
  
  update tr
     set JobPriority = p.OrderBy
    from @TableResult tr
    join Priority      p (nolock) on tr.JobPriority = p.PriorityId
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @error <> 0
    goto error
  
  update tr
     set DTPriority = p.OrderBy,
         CreateDate = isnull(i.DeliveryDate, tr.CreateDate)
    from @TableResult tr
    join IssueLineInstruction     ili (nolock) on tr.InstructionId = ili.InstructionId
    join OperatorGroupDocumentType dt (nolock) on ili.OutboundDocumentTypeId = dt.OutboundDocumentTypeId
    join Priority     
              p (nolock) on dt.PriorityId              = p.PriorityId
    join Issue                      i (nolock) on ili.IssueId                = i.IssueId
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @error <> 0
    goto error
  
  update @TableResult
     set DTPriority = 2
   where DTPriority is null
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @error <> 0
    goto error
  
  update tr
     set PickAisle = l.Ailse
    from @TableResult tr
    join Instruction   i (nolock) on tr.InstructionId = i.InstructionId
    join Location      l (nolock) on i.PickLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId = a.AreaId
   where a.NarrowAisle = 1
  
  select @error = @@error
  
  if @error <> 0
  begin
    set @JobId = null
    set @error = -1
    goto error
  end
  
  delete tr
    from @TableResult tr
    join Operator      o (nolock) on tr.PickAisle in(o.PickAisle, o.StoreAisle)
                                 and o.OperatorId != @OperatorId
  
  select @error = @@error
  
  if @error <> 0
  begin
    set @JobId = null
    set @error = -1
    goto error
  end
  
  select top 1 @JobId = tr.JobId
    from @TableResult tr
  order by DTPriority, OGPriority, JobPriority, LGPriority, CreateDate
  
  if @Debug = 0
  begin
    update Job
       set OperatorId      = @OperatorId,
           StatusId        = @StatusId,
           ReferenceNumber = @ReferenceNumber
     where JobId = @JobId
       and (OperatorId is null
       or   OperatorId = @OperatorId) -- Should stop duplicates, if another users get this one first
    
    select @error = @@error, @rowcount = @@rowcount
    
    if @error <> 0 or @rowcount = 0
    begin
      set @JobId = null
      set @error = -1
      goto error
    end
  end
  else
  begin
    select tr.JobId, tr.DTPriority, tr.OGPriority, tr.JobPriority, tr.LGPriority, tr.CreateDate,
           vi.InstructionType, vi.InstructionId, vi.InstructionRefId, vi.JobCode, vi.InstructionCode, vi.PalletId, vi.SKUCode, vi.PickLocation, vi.StoreLocation, vi.MostRecent
      from @TableResult tr
      left
      join viewInstruction vi on tr.InstructionId = vi.InstructionId
    order by DTPriority, OGPriority, JobPriority, LGPriority, CreateDate
  end
  
  set @PickAisle = null
  
  select top 1 @PickAisle = l.Ailse
    from Instruction   i (nolock)
    join Location      l (nolock) on i.PickLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId = a.AreaId
                                 and (a.NarrowAisle = 1 or a.Interleaving = 1)
   where i.JobId = @JobId
  
  set @StoreAisle = null
  
  if @PickAisle is null
    select top 1 @StoreAisle = l.Ailse
      from Instruction   i (nolock)
      join Location      l (nolock) on i.StoreLocationId = l.LocationId
      join AreaLocation al (nolock) on l.LocationId = al.LocationId
      join Area          a (nolock) on al.AreaId = a.AreaId
                                   and (a.NarrowAisle = 1 or a.Interleaving = 1)
     where i.JobId = @JobId
  
  if @Debug = 0
  begin
    update Operator
       set PickAisle = @PickAisle,
           StoreAisle = @StoreAisle
     where OperatorId = @OperatorId
    
    select @error = @@error, @rowcount = @@rowcount
    
    if @error <> 0 or @rowcount = 0
    begin
      set @JobId = null
      set @error = -1
      goto error
    end
  end
  else
  begin
    select @PickAisle as '@PickAisle',
           @StoreAisle as '@StoreAisle'
  end
  
  if @Debug = 0
    if @InstructionTypeCode = 'PM'
    begin
      exec @Error = p_Instruction_DropSequence
       @JobId = @JobId
      
      if @error <> 0
      goto error
    end
  
  if @Debug = 0
    update MobileLog
       set EndDate = getdate(),
           JobId = @JobId
     where MobileLogId = @MobileLogId
  
  return
  
  error:
    if @Debug = 0
      update MobileLog
         set EndDate = getdate(),
             JobId = @JobId,
             StatusCode = convert(varchar(10), @Error)
       where MobileLogId = @MobileLogId

    return @error
end

