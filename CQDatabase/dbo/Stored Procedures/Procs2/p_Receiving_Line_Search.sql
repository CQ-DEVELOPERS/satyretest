﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Line_Search
  ///   Filename       : p_Receiving_Line_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Line_Search
(
 @InboundShipmentId int = null,
 @ReceiptId int = null
)

as
begin
	 set nocount on;
  
  declare @WarehouseId int
  
  if @InboundShipmentId = -1 and @ReceiptId = -1
    goto Result
  
  if @ReceiptId in ('-1','0')
    set @ReceiptId = null;
  
  if @InboundShipmentId in ('-1','0')
    set @InboundShipmentId = null
  
  if @InboundShipmentId is not null
    set @ReceiptId = null
  
  if @InboundShipmentId is not null
    select @WarehouseId = WarehouseId
      from InboundShipment (nolock)
     where InboundShipmentId = @InboundShipmentId
  
  if @ReceiptId is not null
    select @WarehouseId = WarehouseId
      from Receipt (nolock)
     where ReceiptId = @ReceiptId
  
  create table #TableResult
  (
   WarehouseId             int,
   ReceiptLineId           int,
   ReceiptId               int,
   OrderNumber             nvarchar(30),
   InboundDocumentType     nvarchar(30),
   LineNumber              int,
   ProductId			   int,
   ProductCode             nvarchar(30),
   Product                 nvarchar(255),
   BatchId                 int,
   Batch                   nvarchar(50),
   SKUCode                 nvarchar(50),
   SKU					                nvarchar(50),
   RequiredQuantity        float,
   ReceivedQuantity        float,
   AcceptedQuantity        float,
   AcceptedWeight          float,
   DeliveryNoteQuantity    float,
   RejectQuantity          float,
   SampleQuantity          float,
   AssaySamples            float,
   Status                  nvarchar(50),
   StorageUnitId           int,
   StorageUnitBatchId      int,
   CreditAdviceIndicator   bit default 0,
   ProductCategory         char(1),
   BatchButton             bit,
   BatchSelect             bit,
   Boxes                   float,
   BoxesQuantity           nvarchar(10),
   ChildStorageUnitBatchId int,
   ChildProductCode        nvarchar(30),
   ChildProduct            nvarchar(50),
   ReasonId                int,
   Reason                  nvarchar(50),
   NumberOfPallets		       numeric(13,2),
   PalletId                int
  )
  
  if @InboundShipmentId is null
  insert #TableResult
        (WarehouseId,
         ReceiptLineId,
         ReceiptId,
         OrderNumber,
         RequiredQuantity,
         DeliveryNoteQuantity,
         ReceivedQuantity,
         AcceptedQuantity,
         AcceptedWeight,
         RejectQuantity,
         SampleQuantity,
         AssaySamples,
         Status,
         StorageUnitBatchId,
         CreditAdviceIndicator,
         LineNumber,
         BatchButton,
         BatchSelect,
         ChildStorageUnitBatchId,
         ReasonId,
         NumberOfPallets)
  select r.WarehouseId,
         rl.ReceiptLineId,
         rl.ReceiptId,
         id.OrderNumber,
         rl.RequiredQuantity,
         rl.DeliveryNoteQuantity,
         rl.ReceivedQuantity,
         rl.AcceptedQuantity,
         rl.AcceptedWeight,
         rl.RejectQuantity,
         rl.SampleQuantity,
         rl.AssaySamples,
         s.Status,
         rl.StorageUnitBatchId,
         r.CreditAdviceIndicator,
         il.LineNumber,
         idt.BatchButton,
         idt.BatchSelect,
         rl.ChildStorageUnitBatchId,
         -1,
         rl.NumberOfPallets
    from ReceiptLine             rl (nolock)
    join Receipt                  r (nolock) on rl.ReceiptId             = r.ReceiptId
    join InboundLine             il (nolock) on rl.InboundLineId         = il.InboundLineId
    join InboundDocument         id (nolock) on r.InboundDocumentId      = id.InboundDocumentId
    join InboundDocumentType    idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    join Status                   s (nolock) on rl.StatusId              = s.StatusId
   where rl.ReceiptId          = @ReceiptId
     --and s.Type = 'R'
     and s.StatusCode in ('W','R','P','S','LA','RC','QC') -- not sure if QC is supposed to be here or not (added 2011-12-14)
else
  insert #TableResult
        (WarehouseId,
         ReceiptLineId,
         ReceiptId,
         OrderNumber,
         RequiredQuantity,
         DeliveryNoteQuantity,
         ReceivedQuantity,
         AcceptedQuantity,
         AcceptedWeight,
         RejectQuantity,
         SampleQuantity,
         AssaySamples,
         Status,
         StorageUnitBatchId,
         CreditAdviceIndicator,
         LineNumber,
         BatchButton,
         BatchSelect,
         ChildStorageUnitBatchId,
         ReasonId,
         NumberOfPallets)
  select r.WarehouseId,
         rl.ReceiptLineId,
         rl.ReceiptId,
         id.OrderNumber,
         rl.RequiredQuantity,
         rl.DeliveryNoteQuantity,
         rl.ReceivedQuantity,
         rl.AcceptedQuantity,
         rl.AcceptedWeight,
         rl.RejectQuantity,
         rl.SampleQuantity,
         rl.AssaySamples,
         s.Status,
         rl.StorageUnitBatchId,
         r.CreditAdviceIndicator,
         il.LineNumber,
         idt.BatchButton,
         idt.BatchSelect,
         rl.ChildStorageUnitBatchId,
         -1,
         rl.NumberOfPallets
    from ReceiptLine             rl (nolock)
    join Receipt                  r (nolock) on rl.ReceiptId             = r.ReceiptId
    join InboundLine             il (nolock) on rl.InboundLineId         = il.InboundLineId
    join InboundDocument         id (nolock) on r.InboundDocumentId      = id.InboundDocumentId
    join InboundDocumentType    idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    left outer
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId              = isr.ReceiptId
    join Status                   s (nolock) on rl.StatusId              = s.StatusId
   where isr.InboundShipmentId = @InboundShipmentId
     --and s.Type = 'R'
     and s.StatusCode in ('W','R','P','S','LA','RC','QC') -- not sure if QC is supposed to be here or not (added 2011-12-14)
  
  update tr
     set BatchId       = sub.BatchId,
         StorageUnitId = sub.StorageUnitId
    from #TableResult tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
  
  update tr
     set ProductCode     = p.ProductCode,
         Product         = p.Product,
         ProductId		 = p.ProductId,
         Batch           = b.Batch,
         SKUCode         = sku.SKUCode,
         SKU	         = sku.SKU,
         ProductCategory = su.ProductCategory
    from #TableResult tr
    join StorageUnit      su  (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId     = p.ProductId
    join SKU              sku (nolock) on su.SKUId         = sku.SKUId
    join Batch            b   (nolock) on tr.BatchId      = b.BatchId
  
  update tr
     set ChildProductCode     = p.ProductCode,
         ChildProduct         = p.Product
    from #TableResult tr
    join StorageUnitBatch sub (nolock) on tr.ChildStorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId     = p.ProductId
    join SKU              sku (nolock) on su.SKUId         = sku.SKUId
    join Batch            b   (nolock) on sub.BatchId      = b.BatchId
  
  update tr
     set CreditAdviceIndicator = 1
    from #TableResult tr
    join Exception     e (nolock) on tr.ReceiptLineId = e.ReceiptLineId
   where ExceptionCode = 'CAVUPD'
  
  update tr
     set PalletId = i.PalletId
    from #TableResult tr
    join Instruction i on tr.ReceiptLineId = i.ReceiptLineId
  
  --update tr
  --   set ReasonId = ISNULL(r.ReasonId, -1)
  --      ,Reason = r.Reason
  --  from #TableResult tr
  --  join Exception     e (nolock) on tr.ReceiptLineId = e.ReceiptLineId
  --  join Reason r on e.ReasonId = r.ReasonId
  -- where ExceptionCode = 'REJECT'
  
  update tr
     set ReasonId = ISNULL(r.ReasonId, -1)
        ,Reason = r.Reason
    from #TableResult tr
    join ReceiptLine  rl (nolock) on tr.ReceiptLineId = rl.ReceiptLineId
    join InboundLine  il (nolock) on rl.InboundLineId = il.InboundLineId
    join Reason        r (nolock) on il.ReasonId = r.ReasonId
  
  update #TableResult
     set BatchButton = 0,
         BatchSelect = 1
   where ProductCategory = 'P'
  
  update #TableResult
     set BatchButton = dbo.ufn_Configuration(161, @WarehouseId)
   where BatchButton is null
  
  update #TableResult
     set BatchSelect = dbo.ufn_Configuration(162, @WarehouseId)
   where BatchSelect is null
  
  update tr
     set BoxesQuantity = p.Quantity
    from #TableResult      tr
    join Pack               p (nolock) on tr.WarehouseId        = p.WarehouseId
                                      and tr.StorageUnitId      = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId
  where pt.InboundSequence > 1
    and p.Quantity         > 1
  
  update #TableResult
     set Boxes = convert(float, RequiredQuantity) / convert(float, BoxesQuantity)
   where convert(float, BoxesQuantity) >= 1
  
  update #TableResult
     set Boxes = 1
   where Boxes is null
   
  update #TableResult
     set ReasonId = -1,
		       Reason = ''
   where RejectQuantity <= 0
  
  Result:
  select ReceiptLineId,
         ReceiptId,
         OrderNumber,
         LineNumber,
         ProductId,
         ProductCode,
         Product,
         ChildProductCode,
         ChildProduct,
         BatchId,
         Batch,
         SKUCode,
         SKU,
         Boxes,
         RequiredQuantity,
         ReceivedQuantity,
         AcceptedQuantity,
         AcceptedWeight,
         DeliveryNoteQuantity,
         RejectQuantity,
         SampleQuantity,
         AssaySamples,
         Status,
         StorageUnitId,
         StorageUnitBatchId,
         convert(bit, 0) as 'AlternateBatch',
         CreditAdviceIndicator,
         BatchButton,
         BatchSelect,
         ReasonId,
         Reason,
         isnull(NumberOfPallets,999) as NumberOfPallets,
         PalletId
    from #TableResult
  order by OrderNumber,
           LineNumber,
           ProductCode,
           Product,
           SKUCode,
           RequiredQuantity
end
