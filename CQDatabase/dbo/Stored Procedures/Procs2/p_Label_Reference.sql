﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Label_Reference
  ///   Filename       : p_Label_Reference.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Label_Reference
(
 @Value int output,
 @WarehouseId int = null
)

as
begin
	 set nocount on;
	 
  update Configuration
     set Value = convert(int,  isnull(value,0)) + 1
   where ConfigurationId = 68
     and WarehouseId = isnull(@WarehouseId, WarehouseId)
  
  select @Value = convert(int, Value)
    from Configuration
   where ConfigurationId = 68
     and WarehouseId = isnull(@WarehouseId, WarehouseId)
  
  if @@Error <> 0
    return -1
  else
    return @Value
end
