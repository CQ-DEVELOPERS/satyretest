﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_List
  ///   Filename       : p_Pallet_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:30
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Pallet table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Pallet.PalletId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as PalletId
        ,null as 'Pallet'
  union
  select
         Pallet.PalletId
        ,Pallet.PalletId as 'Pallet'
    from Pallet
  
end
