﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_ReceiptLine_Default_Accepted
  ///   Filename       : p_Receiving_ReceiptLine_Default_Accepted.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_ReceiptLine_Default_Accepted
(
 @inboundShipmentId int,
 @receiptId         int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @WarehouseId       int,
          @InboundSequence   int,
          @rowcount          int
  
  if @inboundShipmentId in (-1,0)
    set @inboundShipmentId = null
  
  if @receiptId in (-1,0)
    set @receiptId = null
  
  select @InboundSequence = max(InboundSequence)
    from PackType (nolock)
  
  begin transaction
  
  if @inboundShipmentId is not null
  begin
    select @WarehouseId = WarehouseId
      from InboundShipment (nolock)
     where InboundShipmentId = @inboundShipmentId
    
    update r
       set LocationId = al.LocationId
      from InboundShipmentReceipt isr (nolock),
           Receipt                  r (nolock),
           Area                     a (nolock),
           AreaLocation            al (nolock)
     where isr.InboundShipmentId = @inboundShipmentId
       and isr.ReceiptId         = r.ReceiptId
       and a.WarehouseId         = @WarehouseId
       and a.AreaId              = al.AreaId
       and a.AreaType            = 'QC'
       and r.LocationId          is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    update r
       set LocationId = al.LocationId
      from InboundShipmentReceipt isr (nolock),
           Receipt                  r (nolock),
           Area                     a (nolock),
           AreaLocation            al (nolock)
     where isr.InboundShipmentId = @inboundShipmentId
       and isr.ReceiptId         = r.ReceiptId
       and a.WarehouseId         = @WarehouseId
       and a.AreaId              = al.AreaId
       and a.AreaType            = 'R'
       and r.LocationId          is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update r
       set StatusId = dbo.ufn_StatusId('R','R')
      from InboundShipmentReceipt isr (nolock)
      join Receipt                  r (nolock) on isr.ReceiptId = r.ReceiptId
      join Status                   s (nolock) on r.StatusId    = s.StatusId
     where isr.InboundShipmentId = @inboundShipmentId
       and s.StatusCode          = 'W'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update rl
       set AcceptedQuantity = rl.RequiredQuantity,
           ReceivedQuantity = rl.RequiredQuantity,
           StatusId         = dbo.ufn_StatusId('R','R')
      from InboundShipmentReceipt isr (nolock)
      join ReceiptLine             rl          on isr.ReceiptId = rl.ReceiptId
      join Status                   s (nolock) on rl.StatusId   = s.StatusId
     where isr.InboundShipmentId = @inboundShipmentId
       and StatusCode       not in ('R','P','F')
       and Type                  = 'R'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update subl
       set ActualQuantity = case when subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0)) < 0
                                 then 0
                                 else subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0)  - isnull(rl.SampleQuantity,0))
                                 end
      from ReceiptLine                rl (nolock)
      join Receipt                     r (nolock) on rl.ReceiptId            = r.ReceiptId
      join InboundShipmentReceipt    isr (nolock) on r.ReceiptId             = isr.ReceiptId
      join StorageUnitBatchLocation subl (nolock) on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                                 and subl.LocationId         = r.LocationId
     where isr.InboundShipmentId = @inboundShipmentId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update rl
       set AcceptedWeight = rl.AcceptedQuantity * pk.NettWeight
      from InboundShipmentReceipt isr (nolock)
      join ReceiptLine             rl          on isr.ReceiptId         = rl.ReceiptId
      join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit             su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                  p (nolock) on su.ProductId          = p.ProductId
      join Pack                    pk (nolock) on su.StorageUnitId      = pk.StorageUnitId
      join PackType                pt (nolock) on pk.PackTypeId         = pt.PackTypeId
     where isr.InboundShipmentId = @inboundShipmentId
       and pk.WarehouseId        = @WarehouseId
       and pt.InboundSequence    = @InboundSequence
       and isnull(su.ProductCategory,'') != 'V'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update subl
       set ActualQuantity = subl.ActualQuantity + isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0)
      from ReceiptLine                rl (nolock)
      join Receipt                     r (nolock) on rl.ReceiptId   = r.ReceiptId
      join InboundShipmentReceipt    isr (nolock) on r.ReceiptId    = isr.ReceiptId
      join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                        and subl.LocationId         = r.LocationId
     where isr.InboundShipmentId = @inboundShipmentId
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
    
    if @rowcount = 0
    begin
      insert StorageUnitBatchLocation
            (StorageUnitBatchId,
             LocationId,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
      select rl.StorageUnitBatchId,
             r.LocationId,
             rl.AcceptedQuantity  - isnull(rl.SampleQuantity,0),
             0,
             0
        from ReceiptLine                rl (nolock)
        join Receipt                     r (nolock) on rl.ReceiptId = r.ReceiptId
        join InboundShipmentReceipt    isr (nolock) on r.ReceiptId  = isr.ReceiptId
        left
        join StorageUnitBatchLocation subl (nolock) on rl.StorageUnitBatchId = subl.StorageUnitBatchId
                                                   and r.LocationId          = subl.LocationId
       where isr.InboundShipmentId = @inboundShipmentId
       --rl.ReceiptLineId = @ReceiptLineId
         and r.LocationId is not null
         and rl.AcceptedQuantity > 0
         and subl.LocationId is null -- does not exist
    end
  end
  else
  begin
    select @WarehouseId = WarehouseId
      from Receipt (nolock)
     where ReceiptId = @receiptId
    
    update r
       set LocationId = al.LocationId
      from Receipt       r (nolock),
           Area          a (nolock),
           AreaLocation al (nolock)
     where r.ReceiptId   = @ReceiptId
       and a.WarehouseId = @WarehouseId
       and a.AreaId = al.AreaId
       and a.AreaType  = 'QC'
       and r.LocationId is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update r
       set LocationId = al.LocationId
      from Receipt       r (nolock),
           Area          a (nolock),
           AreaLocation al (nolock)
     where r.ReceiptId   = @ReceiptId
       and a.WarehouseId = @WarehouseId
       and a.AreaId = al.AreaId
       and a.AreaCode = 'R'
       and r.LocationId is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update r
       set StatusId = dbo.ufn_StatusId('R','R')
      from Receipt r (nolock)
      join Status  s (nolock) on r.StatusId = s.StatusId
     where r.ReceiptId   = @ReceiptId
       and s.StatusCode = 'W'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update subl
       set ActualQuantity = case when subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0)) < 0
                                 then 0
                                 else subl.ActualQuantity - (isnull(rl.AcceptedQuantity,0)  - isnull(rl.SampleQuantity,0))
                                 end
      from ReceiptLine                rl (nolock)
      join Receipt                     r (nolock) on rl.ReceiptId            = r.ReceiptId
      join StorageUnitBatchLocation subl (nolock) on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                                 and subl.LocationId         = r.LocationId
     where rl.ReceiptId = @ReceiptId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update rl
       set AcceptedQuantity = rl.RequiredQuantity,
           ReceivedQuantity = rl.RequiredQuantity,
           StatusId         = dbo.ufn_StatusId('R','R')
      from ReceiptLine rl
      join Status       s (nolock) on rl.StatusId         = s.StatusId
     where rl.ReceiptId = @receiptId
       and StatusCode not in ('R','P','F')
       and Type       = 'R'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    select @WarehouseId = WarehouseId
      from Receipt (nolock)
     where ReceiptId = @receiptId
    
    update rl
       set AcceptedWeight = rl.AcceptedQuantity * pk.NettWeight
      from ReceiptLine             rl
      join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit             su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                  p (nolock) on su.ProductId          = p.ProductId
      join Pack                    pk (nolock) on su.StorageUnitId      = pk.StorageUnitId
      join PackType                pt (nolock) on pk.PackTypeId         = pt.PackTypeId
     where rl.ReceiptId          = @receiptId
       and pk.WarehouseId        = @WarehouseId
       and pt.InboundSequence    = @InboundSequence
       and isnull(su.ProductCategory,'') != 'V'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update subl
       set ActualQuantity = subl.ActualQuantity + isnull(rl.AcceptedQuantity,0) - isnull(rl.SampleQuantity,0)
      from ReceiptLine                rl (nolock)
      join Receipt                     r (nolock) on rl.ReceiptId   = r.ReceiptId
      join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = rl.StorageUnitBatchId
                                        and subl.LocationId         = r.LocationId
     where r.ReceiptId = @ReceiptId
    
    select @Error = @@Error, @rowcount = @@rowcount
    
    if @Error <> 0
      goto error
    
    --if @rowcount = 0
    --begin
      insert StorageUnitBatchLocation
            (StorageUnitBatchId,
             LocationId,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
    select rl.StorageUnitBatchId,
           r.LocationId,
           sum(rl.AcceptedQuantity  - isnull(rl.SampleQuantity,0)),
           0,
           0
      from ReceiptLine                rl
      join Receipt                     r on rl.ReceiptId            = r.ReceiptId
      left
      join StorageUnitBatchLocation subl on r.LocationId          = subl.LocationId
                                        and rl.StorageUnitBatchId = subl.StorageUnitBatchId
     where r.ReceiptId = @ReceiptId
       and r.LocationId is not null
       and rl.AcceptedQuantity > 0
       and subl.LocationId is null -- does not exist
    group by rl.StorageUnitBatchId,
             r.LocationId
    --end
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Receiving_ReceiptLine_Default_Accepted'); 
    rollback transaction
    return @Error
end
