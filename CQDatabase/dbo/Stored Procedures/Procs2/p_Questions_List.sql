﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questions_List
  ///   Filename       : p_Questions_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:06
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Questions table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Questions.QuestionId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Questions_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as QuestionId
        ,null as 'Questions'
  union
  select
         Questions.QuestionId
        ,Questions.QuestionId as 'Questions'
    from Questions
  
end
