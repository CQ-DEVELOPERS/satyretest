﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_OperatorGroup_Update
  ///   Filename       : p_MenuItem_OperatorGroup_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Jan 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItem_OperatorGroup_Update
(
 @OperatorGroupId int,
 @MenuItemId      int,
 @Access          bit
)

as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_MenuItem_OperatorGroup_Update',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @ParentMenuItemId  int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  update OperatorGroupMenuItem
     set Access = @Access
   where OperatorGroupId = @OperatorGroupId
     and MenuItemId = @MenuItemId
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  update OperatorGroupMenuItem
     set Access = @Access
   where OperatorGroupId = @OperatorGroupId
     and MenuItemId in (select MenuItemId from MenuItem where ParentMenuItemId = @MenuItemId)
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  select @ParentMenuItemId = ParentMenuItemId
    from MenuItem
   where MenuItemId = @MenuItemId
  select @ParentMenuItemId
  if @ParentMenuItemId is not null
  begin
    update ogmi1
       set Access = @Access
      from OperatorGroupMenuItem ogmi1 (nolock)
     where OperatorGroupId = @OperatorGroupId
       and MenuItemId = @ParentMenuItemId
       and ogmi1.Access = 0
       and exists(select top 1 1
                    from OperatorGroupMenuItem ogmi2 (nolock)
                    join MenuItem                mi2 (nolock) on ogmi2.MenuItemId = mi2.MenuItemId
                   where ogmi1.OperatorGroupId = ogmi2.OperatorGroupId
                     and mi2.ParentMenuItemId = ogmi1.MenuItemId
                     and ogmi2.Access = 1)
    select @@ROWCOUNT
    select @Error = @@ERROR
    if @Error <> 0
      goto error
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
