﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Select
  ///   Filename       : p_Operator_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 13:19:58
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Operator table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null 
  /// </param>
  /// <returns>
  ///   Operator.OperatorId,
  ///   Operator.OperatorGroupId,
  ///   Operator.WarehouseId,
  ///   Operator.Operator,
  ///   Operator.OperatorCode,
  ///   Operator.Password,
  ///   Operator.NewPassword,
  ///   Operator.ActiveIndicator,
  ///   Operator.ExpiryDate,
  ///   Operator.LastInstruction,
  ///   Operator.Printer,
  ///   Operator.Port,
  ///   Operator.IPAddress,
  ///   Operator.CultureId,
  ///   Operator.LoginTime,
  ///   Operator.LogoutTime,
  ///   Operator.LastActivity,
  ///   Operator.SiteType,
  ///   Operator.Name,
  ///   Operator.Surname,
  ///   Operator.PrincipalId,
  ///   Operator.PickAisle,
  ///   Operator.StoreAisle,
  ///   Operator.MenuId,
  ///   Operator.ExternalCompanyId,
  ///   Operator.Upload 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Select
(
 @OperatorId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Operator.OperatorId
        ,Operator.OperatorGroupId
        ,Operator.WarehouseId
        ,Operator.Operator
        ,Operator.OperatorCode
        ,Operator.Password
        ,Operator.NewPassword
        ,Operator.ActiveIndicator
        ,Operator.ExpiryDate
        ,Operator.LastInstruction
        ,Operator.Printer
        ,Operator.Port
        ,Operator.IPAddress
        ,Operator.CultureId
        ,Operator.LoginTime
        ,Operator.LogoutTime
        ,Operator.LastActivity
        ,Operator.SiteType
        ,Operator.Name
        ,Operator.Surname
        ,Operator.PrincipalId
        ,Operator.PickAisle
        ,Operator.StoreAisle
        ,Operator.MenuId
        ,Operator.ExternalCompanyId
        ,Operator.Upload
    from Operator
   where isnull(Operator.OperatorId,'0')  = isnull(@OperatorId, isnull(Operator.OperatorId,'0'))
  order by Operator
  
end
