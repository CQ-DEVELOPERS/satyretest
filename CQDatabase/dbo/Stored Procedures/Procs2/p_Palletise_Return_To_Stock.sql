﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Palletise_Return_To_Stock
  ///   Filename       : p_Palletise_Return_To_Stock.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Palletise_Return_To_Stock
(
 @OperatorId         int = null,
 @OutboundShipmentId int = null,
 @IssueId            int = null,
 @JobId              int = null,
 @InstructionId      int = null,
 @ReturnToStock      numeric(13,6) = null
)

as
begin
  declare @TableJobs as table
  (
   InstructionId      int,
   PalletId           int,
   WarehouseId        int,
   ReceiptId          int,
   ReceiptLineId      int,
   OldJobId           int,
   JobId              int,
   ReferenceNumber    nvarchar(30),
   StorageUnitBatchId int,
   StorageUnitId      int,
   PickLocationId     int,
   StoreLocationId    int,
   ConfirmedQuantity  float
  )

	 set nocount on;
	 
  declare @TableHeader as table
  (
   InstructionId int,
   StatusCode    nvarchar(10)
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @OrderNumer         nvarchar(30),
          @Count              int,
          @OldJobId           int,
          @WarehouseId        int,
          @PickLocationId     int,
          @StoreLocationId    int,
          @ConfirmedQuantity  float,
          @StorageUnitBatchId int,
          @ReceiptLineId      int,
          @PriorityId         int,
          @StatusId           int,
          @ReferenceNumber    nvarchar(30),
          @PalletId           int,
          @StatusIdOldJob     int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundShipmentId = -1
	set @OutboundShipmentId = null
	
  if @IssueId = -1
	set @IssueId = null
	
  if @JobId = -1
	set @JobId = null	
	
  if @InstructionId = -1
	set @InstructionId = null
	
  if @OutboundShipmentId is null and
     @IssueId            is null and
     @JobId              is null and 
     @InstructionId      is null
    return;
  
  if @OutboundShipmentId is not null
  begin
    insert @TableHeader
          (InstructionId,
           StatusCode)
    select InstructionId,
           StatusCode
      from Instruction i
      join Status      s (nolock) on i.StatusId    = s.StatusId
     where i.OutboundShipmentId = @OutboundShipmentId
    union
    select InstructionId,
           StatusCode
      from Instruction i
      join IssueLine  il on i.IssueLineId = il.IssueLineId
      join OutboundShipmentIssue osi on il.IssueId = osi.IssueId
      join Status      s (nolock) on i.StatusId    = s.StatusId
     where osi.OutboundShipmentId = @OutboundShipmentId
  end
  else if @IssueId is not null
  begin
    insert @TableHeader
          (InstructionId,
           StatusCode)
    select InstructionId,
           StatusCode
      from Instruction i
      join Status      s (nolock) on i.StatusId    = s.StatusId
      join IssueLine  il (nolock) on i.IssueLineId = il.IssueLineId
     where IssueId = isnull(@IssueId, IssueId)
  end
  else if @JobId is not null
  begin
    insert @TableHeader
          (InstructionId,
           StatusCode)
    select InstructionId,
           StatusCode
      from Instruction i
      join Status      s (nolock) on i.StatusId = s.StatusId
     where i.JobId = @JobId
  end
  else if @InstructionId is not null
  begin
    insert @TableHeader
          (InstructionId,
           StatusCode)
    select InstructionId,
           StatusCode
      from Instruction i
      join Status      s (nolock) on i.StatusId = s.StatusId
     where i.InstructionId = @InstructionId
  end
  
  if(select count(1) from @TableHeader) = 0
    return;
  
  begin transaction
  
  insert @TableJobs
        (InstructionId,
         PalletId,
         WarehouseId,
         OldJobId,
         ReferenceNumber,
         StorageUnitBatchId,
         StorageUnitId,
         PickLocationId,
         StoreLocationId,
         ConfirmedQuantity)
  select i.InstructionId,
         i.PalletId,
         i.WarehouseId,
         i.JobId,
         j.ReferenceNumber,
         i.StorageUnitBatchId,
         sub.StorageUnitId,
         i.PickLocationId,
         i.StoreLocationId,
         sum(i.ConfirmedQuantity)
    from @TableHeader          th
    join Instruction            i (nolock) on th.InstructionId     = i.InstructionId
    join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Job                    j (nolock) on i.JobId              = j.JobId
   where isnull(i.ConfirmedQuantity,0) > 0
  group by i.InstructionId,
           i.PalletId,
           i.WarehouseId,
           i.JobId,
           j.ReferenceNumber,
           i.StorageUnitBatchId,
           i.PickLocationId,
           i.StoreLocationId,
           sub.StorageUnitId
  
  if @InstructionId is null
  begin
    update i
       set ConfirmedQuantity = 0
      from @TableJobs t
      join Instruction i on t.InstructionId = i.InstructionId
  end
  else
  begin
    update i
       set ConfirmedQuantity = case when isnull(i.ConfirmedQuantity,0) - isnull(@ReturnToStock,0) <= 0
                                    then 0
                                    else isnull(i.ConfirmedQuantity,0) - isnull(@ReturnToStock,0)
                                    end
      from @TableJobs t
      join Instruction i on t.InstructionId = i.InstructionId
  
    select @Error = @@Error

    if @Error <> 0
    begin
      goto Error
    end
  end
  
  select @Error = @@Error

  if @Error <> 0
  begin
    goto Error
  end
  
  update j
     set StatusId = dbo.ufn_StatusId('I','NS'),
         OperatorId = @OperatorId
    from @TableJobs t
    join Job        j on t.OldJobId = j.JobId
   where not exists(Select top 1 1 from @TableJobs t2 where j.JobId = t2.OldJobId and ConfirmedQuantity > 0)
  
  select @Error = @@Error

  if @Error <> 0
  begin
    goto Error
  end

  select @count = count(1)
    from @TableJobs
  
  select @PriorityId  = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = 'SM'
  
  select @StatusId = StatusId
    from Status (nolock)
   where StatusCode = 'PR'
     and Type       = 'PR'
  
  set @StatusIdOldJob = dbo.ufn_StatusId('IS','CA')
  
  while @count > 0
  begin
    select @count = @count -1
    
    select @InstructionId      = InstructionId,
           @WarehouseId        = WarehouseId,
           @OldJobId           = OldJobId,
           @JobId              = JobId,
           @ReferenceNumber    = 'J:' + convert(nvarchar(10), OldJobId),
           @StorageUnitBatchId = StorageUnitBatchId,
           @ConfirmedQuantity  = ConfirmedQuantity,
           @PalletId           = PalletId,
           @PickLocationId     = PickLocationId,
           @StoreLocationId    = StoreLocationId
      from @TableJobs

    if @ReturnToStock is not null and @Instructionid is not null
      select @ConfirmedQuantity  = @ReturnToStock
    
    delete @TableJobs
     where InstructionId = @InstructionId
    
    if @JobId is null
    begin
      exec @Error = p_Job_Insert
       @JobId           = @JobId output,
       @PriorityId      = @PriorityId,
       @OperatorId      = null,
       @StatusId        = @StatusId,
       @WarehouseId     = @WarehouseId,
       @ReferenceNumber = @ReferenceNumber
      
      select @Error = @@Error

      if @Error <> 0
      begin
        goto Error
      end
      
      exec	@Error		= p_Job_Update
			@JobId		= @OldJobId,
			@StatusId	= @StatusIdOldJob
			
	  exec	@Error		= p_Issue_Update
			@IssueId	= @IssueId,
			@StatusId	= @StatusIdOldJob
			
	  exec	@Error		= p_IssueLine_Update
			@IssueId	= @IssueId,
			@StatusId	= @StatusIdOldJob
      
      update @TableJobs
         set JobId = @JobId
       where OldJobId = @OldJobId
    end

    if @PalletId is null
    begin
      select @PalletId = max(PalletId)
        from Pallet
       where StorageUnitBatchId = @StorageUnitBatchId
         and LocationId = @PickLocationId
    end
    
    exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @JobId               = @JobId,
         @InstructionTypeCode = 'SM',
         @OperatorId          = null,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = @StoreLocationId,
         @StoreLocationId     = null,
         @Quantity            = @ConfirmedQuantity,
         @PalletId            = @PalletId
    
    if @error <> 0
      goto error
      
  --exec @Error = p_Interface_Export_SO_Insert
		--	@IssueId = @IssueId,
		--	@ZeroLines = 1
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
