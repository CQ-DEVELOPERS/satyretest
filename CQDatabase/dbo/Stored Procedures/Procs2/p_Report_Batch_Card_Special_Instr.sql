﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Batch_Card_Special_Instr
  ///   Filename       : p_Report_Batch_Card_Special_Instr.sql
  ///   Create By      : Karen
  ///   Date Created   : January 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Batch_Card_Special_Instr
(
 --@BOMHeaderId            int
 @OrderNumber            nvarchar(30)
)

as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   Remarks			nvarchar(max)
  )
  
 
  declare @Indicator		  char(1)
  
	insert @TableHeader
		   (Remarks)		
	SELECT	bh.Remarks      
	FROM	BOMInstruction   bi (nolock)
	join    BOMHeader        bh (nolock) on bi.BOMHeaderId = bh.BOMHeaderId      
	where   bi.OrderNumber = isnull(@OrderNumber, bi.OrderNumber)
    
    select * from @TableHeader
 
	--SELECT	top 1 bsi.SpecialInstructionsLine1,
	--	    bsi.SpecialInstructionsLine2,   
	--	    bsi.SpecialInstructionsLine3,
	--	    bsi.SpecialInstructionsLine4,
	--	    bsi.SpecialInstructionsLine5,
	--	    bsi.SpecialInstructionsLine6,
	--	    bsi.SpecialInstructionsLine7,
	--	    bsi.SpecialInstructionsLine8,
	--	    bsi.SpecialInstructionsLine9,
	--	    bsi.SpecialInstructionsLine10,
	--	    bsi.SpecialInstructionsLine11,
	--	    bsi.SpecialInstructionsLine12 
	--FROM	BOMSpecialInstruction   bsi (nolock)
	--join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	--where   bh.BOMHeaderId = @BOMHeaderId
  
end
