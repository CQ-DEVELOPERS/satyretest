﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Instruction_Create
  ///   Filename       : p_Pallet_Rebuild_Instruction_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Mar 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Instruction_Create
(
 @LinkedJobId   int,
 @UnlinkedJobId int,
 @IssueLineId   int,
 @InstructionId int,
 @Quantity      float,
 @Comments      nvarchar(255),
 @OperatorId    int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @Detail            sql_variant,
          @ProductCode       nvarchar(30)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @ProductCode = p.ProductCode
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product            p (nolock) on su.ProductId         = p.ProductId
   where i.InstructionId = @InstructionId
  
  select @Detail = convert(nvarchar(255), 'Move Qty ' + CONVERT(nvarchar(10), @Quantity) + ' of ' + @ProductCode + ' from Job J:' + convert(nvarchar(10), @LinkedJobId) + ' to J:' + convert(nvarchar(10), @UnlinkedJobId))
  
  begin transaction
  
  exec @Error = p_Exception_Insert
   @InstructionId = @InstructionId,
   @IssueLineId   = @IssueLineId,
   @Exception     = @Comments,
   @ExceptionCode = 'PR001',
   @OperatorId    = @OperatorId,
   @CreateDate    = @GetDate,
   @ExceptionDate = @GetDate,
   @JobId         = @LinkedJobId,
   @Detail        = @Detail
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Pallet_Rebuild_Instruction_Create'); 
    rollback transaction
    return @Error
end
