﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Delete
  ///   Filename       : p_Product_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Jul 2014 14:16:33
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Product_Delete
(
 @ProductId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Product
     where ProductId = @ProductId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_ProductHistory_Insert
         @ProductId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
