﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mixed_Job_Available
  ///   Filename       : p_Mixed_Job_Available.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mixed_Job_Available
(
 @InstructionTypeCode nvarchar(10),
 @JobId               int,
 @ShipmentId          int,
 @OrderNumber         nvarchar(30),
 @FromDate            datetime,
 @ToDate              datetime
)

as
begin
	 set nocount on;
	 
	 declare @TableIssueLines as table
	 (
	  IssueLineId int
	 )
	 
	 declare @TableReceiptLines as table
	 (
	  ReceiptLineId int
	 )
  
  declare @StatusId int,
          @Status   nvarchar(50)
  
  if @JobId = -1
    set @JobId = null
  
  if @ShipmentId = -1
    set @ShipmentId = null
  
  if @OrderNumber = '-1'
    set @OrderNumber = null
  
  select @StatusId = dbo.ufn_StatusId('I','W')
  
  if @ShipmentId is null and @OrderNumber is null
  begin
    if @InstructionTypeCode = 'PM'
      insert @TableIssueLines
            (IssueLineId)
      select IssueLineId
        from OutboundDocument od (nolock)
        join Issue             i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
        join IssueLine        il (nolock) on i.IssueId             = il.IssueId
    
    if @InstructionTypeCode = 'SM'
      insert @TableReceiptLines
            (ReceiptLineId)
      select ReceiptLineId
        from InboundDocument id (nolock)
        join Receipt          r (nolock) on id.InboundDocumentId = r.InboundDocumentId
        join ReceiptLine     rl (nolock) on r.ReceiptId          = rl.ReceiptId
  end
  else if @ShipmentId is not null
  begin
    if @InstructionTypeCode = 'PM'
      insert @TableIssueLines
            (IssueLineId)
      select IssueLineId
        from OutboundShipmentIssue osi (nolock)
        join IssueLine              il (nolock) on osi.IssueId = il.IssueId
       where osi.OutboundShipmentId = @ShipmentId
    
    if @InstructionTypeCode = 'SM'
      insert @TableReceiptLines
            (ReceiptLineId)
      select ReceiptLineId
        from InboundShipmentReceipt isr (nolock)
        join ReceiptLine             rl (nolock) on isr.ReceiptId = rl.ReceiptId
       where isr.InboundShipmentId = @ShipmentId
  end
  else if @OrderNumber is not null
  begin
    if @InstructionTypeCode = 'PM'
      insert @TableIssueLines
            (IssueLineId)
      select IssueLineId
        from OutboundDocument od (nolock)
        join Issue             i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
        join IssueLine        il (nolock) on i.IssueId             = il.IssueId
       where od.OrderNumber = @OrderNumber
    
    if @InstructionTypeCode = 'SM'
      insert @TableReceiptLines
            (ReceiptLineId)
      select ReceiptLineId
        from InboundDocument id (nolock)
        join Receipt          r (nolock) on id.InboundDocumentId = r.InboundDocumentId
        join ReceiptLine     rl (nolock) on r.ReceiptId          = rl.ReceiptId
       where id.OrderNumber = @OrderNumber
  end
  
  select i.JobId,
         min(it.InstructionType) as 'InstructionType'
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
   where i.JobId                = isnull(@JobId, i.JobId)
     and i.CreateDate     between @FromDate and @ToDate
     and i.StatusId             = @StatusId
     and it.InstructionTypeCode in ('PM','PS','FM','PR')
     and (exists(select 1 from @TableIssueLines til where i.IssueLineId = til.IssueLineId)
      or  exists(select 1 from @TableReceiptLines trl where i.ReceiptLineId = trl.ReceiptLineId))
  group by i.JobId
  union
  select i.JobId,
         min(it.InstructionType) as 'InstructionType'
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
   where i.JobId                = isnull(@JobId, i.JobId)
     and i.CreateDate     between @FromDate and @ToDate
     and i.StatusId             = @StatusId
     and i.OutboundShipmentId   = @ShipmentId
     and it.InstructionTypeCode in ('PM','PS','FM','PR')
  group by i.JobId
end
