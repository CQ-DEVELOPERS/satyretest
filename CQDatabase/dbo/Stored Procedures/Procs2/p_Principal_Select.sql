﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Principal_Select
  ///   Filename       : p_Principal_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 08:15:19
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Principal table.
  /// </remarks>
  /// <param>
  ///   @PrincipalId int = null 
  /// </param>
  /// <returns>
  ///   Principal.PrincipalId,
  ///   Principal.PrincipalCode,
  ///   Principal.Principal,
  ///   Principal.Email,
  ///   Principal.FirstName,
  ///   Principal.LastName,
  ///   Principal.Address1,
  ///   Principal.Address2,
  ///   Principal.Address3,
  ///   Principal.Address4,
  ///   Principal.Address5,
  ///   Principal.Address6 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Principal_Select
(
 @PrincipalId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Principal.PrincipalId
        ,Principal.PrincipalCode
        ,Principal.Principal
        ,Principal.Email
        ,Principal.FirstName
        ,Principal.LastName
        ,Principal.Address1
        ,Principal.Address2
        ,Principal.Address3
        ,Principal.Address4
        ,Principal.Address5
        ,Principal.Address6
    from Principal
   where isnull(Principal.PrincipalId,'0')  = isnull(@PrincipalId, isnull(Principal.PrincipalId,'0'))
  order by PrincipalCode
  
end
 
