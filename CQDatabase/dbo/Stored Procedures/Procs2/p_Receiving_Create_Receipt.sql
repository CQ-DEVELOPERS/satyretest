﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Create_Receipt
  ///   Filename       : p_Receiving_Create_Receipt.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure dbo.p_Receiving_Create_Receipt
(
 @InboundDocumentId int,
 @InboundLineId     int,
 @OperatorId        int,
 @ChildStorageunitBatchId int = null
)

as
begin
	 set nocount on;
  
  declare @TableInboundLine as table
  (
   InboundLineId                  int,
   StorageUnitId                  int,
   BatchId                        int,
   StorageUnitBatchId             int,
   Quantity                       float
  );
  
  declare @Error                 int,
          @Errormsg              nvarchar(500),
          @GetDate               datetime,
          @InboundDocumentTypeId int,
          @ReceiptId             int,
          @PriorityId            int,
          @WarehouseId           int,
          @StatusId              int,
          @DeliveryDate          datetime,
          @ReceiptLineId         int,
          @StorageUnitBatchId    int,
          @Quantity              float,
          @Delivery              int,
          @OrderNumber           nvarchar(30),
          @InboundDocumentTypeCode nvarchar(10),
          @DeliveryNoteNumber      nvarchar(30),
          @LocationId              int,
          @BatchStatusId           int,
          @BatchId                 int,
          @StorageUnitId           int,
          @PrincipalId             int
  
  select @GetDate = dbo.ufn_Getdate()
  
--  if exists(select 1 from Receipt where InboundDocumentId = @InboundDocumentId)
--  begin
--    begin transaction
--    set @Errormsg = 'Receipt has already been inserted'
--    set @Error = -1
--    goto error
--  end
  
  select @InboundDocumentTypeId = InboundDocumentTypeId,
         @WarehouseId           = WarehouseId,
         @DeliveryDate          = null,--DeliveryDate
         @OrderNumber           = OrderNumber,
         @PrincipalId           = PrincipalId
    from InboundDocument (nolock)
   where InboundDocumentId = @InboundDocumentId
  
  select @InboundDocumentTypeCode = InboundDocumentTypeCode
    from InboundDocumentType (nolock)
   where InboundDocumentTypeId = @InboundDocumentTypeId
  
  select @PriorityId = PriorityId
    from InboundDocumentType (nolock)
   where InboundDocumentTypeId = @InboundDocumentTypeId
  
  select @StatusId = StatusId
    from Status (nolock)
   where StatusCode = 'W'
     and Type       = 'R'
  
  insert @TableInboundLine
        (InboundLineId,
         StorageUnitId,
         BatchId,
         Quantity)
  select InboundLineId,
         StorageUnitId,
         BatchId,
         Quantity
    from InboundLine
   where InboundDocumentId = @InboundDocumentId
     and InboundLineId     = isnull(@InboundLineId, InboundLineId)
  
  if @InboundDocumentTypeCode = 'RET' and (select dbo.ufn_Configuration(262, @WarehouseId)) = 1
  begin
    select @BatchStatusId = StatusId
      from Status (nolock)
     where StatusCode = 'A'
       and Type       = 'B'
    
    select @BatchId = BatchId
      from Batch (nolock)
     where Batch = @OrderNumber
    
    if @BatchId is null
    begin
      exec @Error = p_Batch_Insert
       @BatchId     = @BatchId out,
       @StatusId    = @BatchStatusId,
       @WarehouseId = @WarehouseId,
       @Batch       = @OrderNumber,
       @CreateDate  = @getdate
      
      if @Error <> 0
      begin
        set @Errormsg = 'Error executing p_Batch_Insert'
        set @Error = -1
        goto error
      end
    end
    
    update @TableInboundLine
      set BatchId = @BatchId
    
    update il
       set StorageUnitBatchId = sub.StorageUnitBatchId
      from @TableInboundLine il
      join StorageUnitBatch  sub (nolock) on il.StorageUnitId = sub.StorageUnitId
                                         and il.BatchId       = sub.BatchId
    
    select @StorageUnitId = count(1)
      from @TableInboundLine
     where StorageUnitBatchId is null
    
    if @StorageUnitId < 1
      set @StorageUnitId = null
    
    while @StorageUnitId is not null
    begin
      select @StorageUnitId = StorageUnitId
        from @TableInboundLine
       where StorageUnitBatchId is null
      
      exec @Error = p_StorageUnitBatch_Insert
       @StorageUnitBatchId = @StorageUnitBatchId out,
       @BatchId            = @BatchId,
       @StorageUnitId      = @StorageUnitId
      
      if @Error <> 0
      begin
        set @Errormsg = 'Error executing p_Batch_Insert'
        set @Error = -1
        goto error
      end
      
      insert StorageUnitBatch
            (BatchId,
             StorageUnitId)
      select BatchId,
             StorageUnitId
        from StorageUnit su,
             Batch b
       where not exists(select 1 from StorageUnitBatch sub where sub.StorageUnitId = su.StorageUnitId and sub.BatchId = b.BatchId)
         and b.Batch = 'Default'
      
      update @TableInboundLine
         set StorageUnitBatchId = @StorageUnitBatchId
        where StorageUnitId = @StorageUnitId
          and BatchId = @BatchId
      
      set @StorageUnitId = null
    end
  end
  else
  begin
    update il
       set StorageUnitBatchId = sub.StorageUnitBatchId
      from @TableInboundLine il
      join StorageUnitBatch  sub (nolock) on il.StorageUnitId = sub.StorageUnitId
                                         and il.BatchId       = sub.BatchId
    
    update il
       set StorageUnitBatchId = sub.StorageUnitBatchId
      from @TableInboundLine il,
           Batch             b   (nolock),
           StorageUnitBatch  sub (nolock)
     where il.StorageUnitId = sub.StorageUnitId
       and il.BatchId      is null
       and b.Batch          = 'Default'
       and b.BatchId        = sub.BatchId
    
    update il
       set StorageUnitBatchId = sub.StorageUnitBatchId
      from @TableInboundLine il,
           Batch             b   (nolock),
           StorageUnitBatch  sub (nolock)
     where il.StorageUnitId = sub.StorageUnitId
       and il.StorageUnitBatchId is null
       and b.Batch          = 'Default'
  end
  
  begin transaction
  
  select @ReceiptId = max(ReceiptId),
         @Delivery  = isnull(max(Delivery),0) + 1
    from Receipt
   where InboundDocumentId = @InboundDocumentId
  
  if @ReceiptId is null
  begin
    select @Delivery = isnull(max(r.Delivery),0) + 1
      from InboundDocument id (nolock)
      join Receipt          r (nolock) on id.InboundDocumentId = r.InboundDocumentId
     where id.OrderNumber = @OrderNumber
       and isnull(id.PrincipalId, -1) = isnull(@PrincipalId, isnull(id.PrincipalId, -1))
    
    if @InboundDocumentTypeCode = 'PRV'
      set @DeliveryNoteNumber = @OrderNumber
    
    exec @LocationId = p_Receiving_Location_Get
       @WarehouseId             = @WarehouseId,
       @InboundDocumentTypeCode = @InboundDocumentTypeCode,
       @PrincipalId             = @PrincipalId
    
    if @LocationId in (0,-1)
      set @LocationId = null
    
    exec @Error = p_Receipt_Insert
     @ReceiptId             = @ReceiptId output,
     @InboundDocumentId     = @InboundDocumentId,
     @PriorityId            = @PriorityId,
     @WarehouseId           = @WarehouseId,
     @LocationId            = @LocationId,
     @StatusId              = @StatusId,
     @OperatorId            = @OperatorId,
     @DeliveryNoteNumber    = @DeliveryNoteNumber,
     @DeliveryDate          = @DeliveryDate,
     @SealNumber            = null,
     @VehicleRegistration   = null,
     @Remarks               = null,
     @CreditAdviceIndicator = 0,
     @Delivery              = @Delivery
    
    if @Error <> 0
    begin
      set @Errormsg = 'Error executing p_Receipt_Insert (1)'
      set @Error = -1
      goto error
    end
  end
  
  update r
       set NumberOfLines = (select count(1)
                              from InboundLine il (nolock)
                             where r.InboundDocumentId = il.InboundDocumentId)
      from Receipt r
     where r.ReceiptId = @ReceiptId
  
  while exists(select top 1 1 from @TableInboundLine)
  begin
    select top 1 @InboundLineId      = InboundLineId,
                 @StorageUnitBatchId = StorageUnitBatchId,
                 @Quantity           = Quantity
      from @TableInboundLine
    
    delete @TableInboundLine where InboundLineId = @InboundLineId
    
    set @ReceiptLineId = null
    
    select @ReceiptLineId = ReceiptLineId
      from ReceiptLine (nolock)
     where InboundLineId      = @InboundLineId
       and StorageUnitBatchId = @StorageUnitBatchId
    
    if @ReceiptLineId is null
    begin
      exec @Error = p_ReceiptLine_Insert
       @ReceiptLineId        = @ReceiptLineId output,
       @ReceiptId            = @ReceiptId,
       @InboundLineId        = @InboundLineId,
       @StorageUnitBatchId   = @StorageUnitBatchId,
       @StatusId             = @StatusId,
       @OperatorId           = @OperatorId,
       @RequiredQuantity     = @Quantity,
       @ReceivedQuantity     = 0,
       @AcceptedQuantity     = 0,
       @RejectQuantity       = 0,
       @DeliveryNoteQuantity = 0,
       @ChildStorageunitBatchId = @ChildStorageunitBatchId
      
      if @Error <> 0
      begin
        set @Errormsg = 'Error executing p_Receipt_Insert - Insert ReceiptLine'
        set @Error = -1
        goto error
      end
    end
    else
    begin
      exec @Error = p_ReceiptLine_Update
       @ReceiptLineId        = @ReceiptLineId,
       @ReceiptId            = @ReceiptId,
       @InboundLineId        = @InboundLineId,
       @StorageUnitBatchId   = @StorageUnitBatchId,
       @StatusId             = @StatusId,
       @OperatorId           = @OperatorId,
       @RequiredQuantity     = @Quantity,
       @ChildStorageunitBatchId = @ChildStorageunitBatchId
      
      if @Error <> 0
      begin
        set @Errormsg = 'Error executing p_Receipt_Insert - Update ReceiptLine'
        set @Error = -1
        goto error
      end
    end
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
