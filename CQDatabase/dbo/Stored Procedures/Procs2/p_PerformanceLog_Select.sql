﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PerformanceLog_Select
  ///   Filename       : p_PerformanceLog_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:57
  /// </summary>
  /// <remarks>
  ///   Selects rows from the PerformanceLog table.
  /// </remarks>
  /// <param>
  ///   @PerformanceLogId int = null 
  /// </param>
  /// <returns>
  ///   PerformanceLog.PerformanceLogId,
  ///   PerformanceLog.ProcedureName,
  ///   PerformanceLog.Remarks,
  ///   PerformanceLog.StartDate,
  ///   PerformanceLog.LogDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PerformanceLog_Select
(
 @PerformanceLogId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         PerformanceLog.PerformanceLogId
        ,PerformanceLog.ProcedureName
        ,PerformanceLog.Remarks
        ,PerformanceLog.StartDate
        ,PerformanceLog.LogDate
    from PerformanceLog
   where isnull(PerformanceLog.PerformanceLogId,'0')  = isnull(@PerformanceLogId, isnull(PerformanceLog.PerformanceLogId,'0'))
  
end
