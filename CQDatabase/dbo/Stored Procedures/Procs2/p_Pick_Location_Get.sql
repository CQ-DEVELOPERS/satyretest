﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pick_Location_Get
  ///   Filename       : p_Pick_Location_Get.sql
  ///   Create By      : GS
  ///   Date Created   : 21 Mar 2007
  /// </summary>
  /// <remarks>
  ///   Gets a Location base on the StorageUnitBatch and Quantity
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyId             int = null
  /// </param>
  /// <returns>
  ///   ExternalCompanyId             int,
  ///   ECTypeId                      int,
  ///   Code                          nvarchar(30),
  ///   Name                          nvarchar(50),
  ///   Rating                        int 
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 2017-08-29
  ///   Details        : SAT01 - Added config 389 - Efficiency is more important than FIFO rules for Mixed picks
  /// </newpara>
*/
CREATE procedure p_Pick_Location_Get
(
 @WarehouseId         int,
 @LocationId          int output,
 @StorageUnitBatchId  int output,
 @Quantity            float output,
 @Replenishment       bit = 0,
 @InstructionId       int = null,
 @LIFO                bit = 0,
 @MinimumShelfLife    numeric(13,3) = null,
 @Temporary           bit = 0,
 @AreaType            nvarchar(10) = '', -- Used for QA Area
 @DeliveryDate        datetime = null,
 @InstructionTypeCode varchar(10) = null,
 @Stop                int = 0,
 @IssueId             int = null
)
as
begin
  if @AreaType is null
    set @AreaType = ''
  
  declare @TableBatches as table
  (
   StorageUnitBatchId int
  )
  
  --select @WarehouseId        as '@WarehouseId       ',
  --       @LocationId         as '@LocationId        ',
  --       @StorageUnitBatchId as '@StorageUnitBatchId',
  --       @Quantity           as '@Quantity          ',
  --       @Replenishment      as '@Replenishment     ',
  --       @InstructionId      as '@InstructionId     ',
  --       @LIFO               as '@LIFO              ',
  --       @MinimumShelfLife   as '@MinimumShelfLife  ',
  --       @Temporary          as '@Temporary         ',
  --       @AreaType           as '@AreaType          ',
  --       @DeliveryDate       as '@DeliveryDate      ',
  --       @InstructionTypeCode as '@InstructionTypeCode',
  --       @Stop               as '@Stop              ',
  --       @IssueId            as '@IssueId           '
  
  declare @Getdate               datetime,
          @OldLocationId         int,
          @OldStorageUnitBatchId int,
          @FullPallet            int,
          @FullPalletInd         bit,
          @StorageUnitId         int,
          @rowcount              int,
          @AreaId                int,
          @AreaCode              nvarchar(10),
          @InsLocationId         int,
          @RelativeValue         numeric(13,3),
          @Error                 int,
          @BulkLocationId        int,
          @PickEmpty             bit,
          @ExternalCompanyId     int,
          @ShelfLife             numeric(13,3),
          @OldQuantity           float,
          @SumQuantity           float,
          @BoxQuantity           float,
          @ReservedId            int,
          @OrderByQty            bit = 0,
          @NearLocationId        int,
          @PickPartPalletFirst   bit = 0,
          @ReserveSUB            int,
          @CheckStockTakeInd     bit = 0,
          @MaxPackQty            numeric(13,6),
          @SmallPick             bit = 0
          
  select @GetDate = dbo.ufn_Getdate()
  
  if @DeliveryDate is null
    set @DeliveryDate = @GetDate
  
  set @OldLocationId = @LocationId
  set @LocationId = null
  
  if dbo.ufn_Configuration(180, @WarehouseId) = 1
    set @OldStorageUnitBatchId = @StorageUnitBatchId
  
  if @LIFO is null
    set @LIFO = 0
  
  select @OrderByQty = dbo.ufn_Configuration(371, @WarehouseId)
  
  select @PickPartPalletFirst = Indicator
    from Configuration
   where ConfigurationId = 26
     and WarehouseId = @WarehouseId
  
  select @CheckStockTakeInd = Indicator
    from Configuration
   where ConfigurationId = 436
     and WarehouseId = @WarehouseId
  
  select @StorageUnitId = sub.StorageUnitId,
         @PickEmpty     = isnull(su.PickEmpty, 0),
         @ShelfLife     = isnull(p.ShelfLifeDays, 720)
    from StorageUnitBatch sub (nolock) 
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId      = p.ProductId
   where StorageUnitBatchId = @StorageUnitBatchId
  
  if @ShelfLife = 0
    set @ShelfLife = 99999
  
  select @FullPallet = p.Quantity
    from Pack      p (nolock)
    join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
   where p.WarehouseId      = @WarehouseId
     and pt.InboundSequence = 1 -- Full Pallet
     and p.StorageUnitId    = @StorageUnitId
  
  if dbo.ufn_Configuration(250, @WarehouseId) = 1
  begin
    select @BoxQuantity = p.Quantity
      from Pack      p (nolock)
      join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
     where p.WarehouseId       = @WarehouseId
       and p.StorageUnitId     = @StorageUnitId
       and pt.OutboundSequence between 2 and (select MAX(OutboundSequence) - 1 from PackType (nolock))
    
    if @Quantity >= @BoxQuantity and @FullPallet > @Quantity
      set @FullPallet = @Quantity
  end
  
  if (select dbo.ufn_Configuration(155, @WarehouseId)) = 1
  begin
    if exists(select top 1 1
                from StorageUnitBatch sub
                join Batch              b on sub.BatchId = b.BatchId
               where sub.StorageUnitBatchId = @StorageUnitBatchId
                 and b.Batch != 'Default')
    begin
      select @IssueId = iss.IssueId,
             @ReserveSUB = case when isnull(iss.ReserveBatch, 0) = 1
                                then @StorageUnitBatchId
                                else null
                                end
        from Issue iss
       where iss.IssueId = @IssueId
    end
  end
  else
  if dbo.ufn_Configuration(133, @WarehouseId) = 1
  begin
    select @ExternalCompanyId = od.ExternalCompanyId,
           @IssueId           = ili.IssueId,
           @ReserveSUB = case when isnull(iss.ReserveBatch, 0) = 1
                              then il.StorageUnitBatchId
                              else null
                              end
      from Instruction            i (nolock)
      join IssueLineInstruction ili (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
      join IssueLine             il (nolock) on ili.IssueLineId = il.IssueLineId
      join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
      join Issue                iss (nolock) on ili.IssueId = iss.IssueId
     where i.InstructionId = @InstructionId
  end
  else
  begin
    select @IssueId = ili.IssueId,
           @ReserveSUB = case when isnull(iss.ReserveBatch, 0) = 1
                              then il.StorageUnitBatchId
                              else null
                              end
      from Instruction            i (nolock)
      join IssueLineInstruction ili (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
      join IssueLine             il (nolock) on ili.IssueLineId = il.IssueLineId
      join OutboundDocument      od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
      join Issue                iss (nolock) on ili.IssueId = iss.IssueId
     where i.InstructionId = @InstructionId
  end
  
  if @ReserveSUB is not null
  begin
  insert @TableBatches
        (StorageUnitBatchId)
  select StorageUnitBatchId
    from Receipt r
    join ReceiptLine rl on r.ReceiptId = rl.ReceiptId
   where r.ParentIssueId = @IssueId
  end
  
  select @rowcount = @@ROWCOUNT
  
  if @rowcount = 0 and @ReserveSUB is not null
  begin
    insert @TableBatches
          (StorageUnitBatchId)
    select @ReserveSUB
    
    select @rowcount = @@ROWCOUNT
  end
  
  if @rowcount > 0 and @ReserveSUB is null
    set @ReserveSUB = -1
  
  select @ReservedId = ReservedId
    from Issue (nolock)
   where IssueId = @ReservedId
  
  if @ReservedId is null
    set @ReservedId = -1
  
  if @MinimumShelfLife is null
    select @MinimumShelfLife = MinimumShelfLife
      from StorageUnitExternalCompany (nolock)
     where StorageUnitId     = @StorageUnitId
       and ExternalCompanyId = @ExternalCompanyId
  
  if @MinimumShelfLife is null
    if dbo.ufn_Configuration(230, @WarehouseId) = 1
      set @MinimumShelfLife = 0
    else
      set @MinimumShelfLife = -99999
  
  if dbo.ufn_Configuration(130, @WarehouseId) = 1
  begin
    select @BulkLocationId = l.LocationId
      from Area              a (nolock)
      join AreaLocation     al (nolock) on a.AreaId = al.AreaId
      join Location          l (nolock) on al.LocationId = l.LocationId
      join StorageUnitArea sua (nolock) on a.AreaId = sua.AreaId
      join StorageUnitBatch sub (nolock) on sua.StorageUnitId = sub.StorageUnitId
      join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                                                 and l.LocationId = subl.LocationId
     where sua.StorageUnitId = @StorageUnitId
       and a.AreaCode = 'BK'
       and l.ActivePicking = 1
       and isnull(a.AreaType,'') = @AreaType
    
    if @BulkLocationId is null
    begin
      select @BulkLocationId = l.LocationId
        from Area              a (nolock)
        join AreaLocation     al (nolock) on a.AreaId = al.AreaId
        join Location          l (nolock) on al.LocationId = l.LocationId
        join StorageUnitArea sua (nolock) on a.AreaId = sua.AreaId
       where sua.StorageUnitId = @StorageUnitId
         and a.AreaCode = 'BK'
         and l.ActivePicking = 1
         and isnull(a.AreaType,'') = @AreaType
    end
    
    exec @Error = p_Location_Bulk_Indicators
     @StorageUnitId = @StorageUnitId,
     @LocationId    = @BulkLocationId
  end
  
  if @InstructionTypeCode in ('PM','FM')--('PM','PS','FM')
    set @FullPalletInd = 0
  
  if @FullPalletInd is null
  begin
    if (select dbo.ufn_Configuration(170, @WarehouseId)) = 1
    begin
      if @Quantity >= @FullPallet
      begin
        set @FullPalletInd = 1
      end
      else
        set @FullPalletInd = 0
    end
    else
    begin
      if @Quantity = @FullPallet
        set @FullPalletInd = 1
      else
        set @FullPalletInd = 0
    end
    
    if @Replenishment = 1
    or (dbo.ufn_Configuration(367, @WarehouseId) = 1 and (@FullPallet >= 999999 or @FullPallet = 99999))
    begin
      set @FullPalletInd = 1
      
      if @FullPallet >= 999999 or @FullPallet = 99999
        set @FullPallet = 1
    end
  end
  
  if @Stop = 1
    select @FullPalletInd = 1,
           @Replenishment = 1
    
  --select @Quantity as '@Quantity', @LocationId as '@LocationId', @FullPallet as '@FullPallet'
--  if (select dbo.ufn_Configuration(155, @WarehouseId)) = 1
--  begin
--    set @FullPalletInd = 1
--    set @FullPallet = 1
--  end
  
  if @FullPalletInd = 0
  begin
    -- Only allocated to bulk
    if (select max(a.AreaCode)
                    from StorageUnitLocation sul (nolock)
                    join AreaLocation         al (nolock) on sul.LocationId = al.LocationId
                    join Area                  a (nolock) on al.AreaId      = a.AreaId
                   where sul.StorageUnitId = @StorageUnitId) = 'BK'
    if (dbo.ufn_Configuration(226, @WarehouseId)) = 1
    begin
      set @FullPalletInd = 1
    end
    
    if (dbo.ufn_Configuration(175, @WarehouseId)) = 0
      set @FullPalletInd = 1 --XXX
    
    if dbo.ufn_Configuration(242, @warehouseId) = 1 -- Generate replenishment on empty pickface
    begin
      select @InsLocationId = PickLocationId
        from Instruction (nolock)
       where InstructionId = @InstructionId
      
      select @SumQuantity = sum(isnull(subl.ActualQuantity,0) + isnull(subl.AllocatedQuantity,0))
        from StorageUnitLocation       sul (nolock)
        join Location                    l (nolock) on sul.LocationId         = l.LocationId
        join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
        join Area                        a (nolock) on al.AreaId              = a.AreaId
        join StorageUnitBatch          sub (nolock) on sul.StorageUnitId      = sub.StorageUnitId
        left
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                                                   and l.LocationId           = subl.LocationId
       where l.ActiveBinning        = 1 -- Yes
         and l.StocktakeInd         = 0
         and a.AreaCode             = 'PK'
         and isnull(a.AreaType,'')  = @AreaType
         and sub.StorageUnitId      = @StorageUnitId
         and a.WarehouseId          = @WarehouseId
         and l.LocationId           = @InsLocationId
      group by l.LocationId
      
      select @rowcount = @@rowcount
      
      if isnull(@SumQuantity,0) = 0 and @rowcount > 0 -- must return at least one row
      begin
        exec @Error = p_Mobile_Request_Replenishment
         @instructionId = @instructionId,
         @ShowMsg       = 0
        
        -- Don't report error
        set @Error = 0
      end
    end
  end
  
  if @FullPalletInd = 1
  begin
    set @OldQuantity = @Quantity
    
    if @Replenishment = 1-- or (select dbo.ufn_Configuration(179, @WarehouseId)) = 1
    begin
      set @Quantity = 1
    end
    
    if @Replenishment = 1 or @OldLocationId is null -- Reset
      set @OldStorageUnitBatchId = null
    
    -- Get part location first then full location near to NearLocationId
    if (dbo.ufn_Configuration(389, @WarehouseId)) = 1 -- Efficiency is more important than FIFO rules
    begin
      select top 1 @AreaId = a.AreaId
        from StorageUnitBatch          sub (nolock)
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
        join AreaLocation               al (nolock) on subl.LocationId = al.LocationId
        join Location                    l (nolock) on al.LocationId = l.LocationId
        join Area                        a (nolock) on al.AreaId = a.AreaId
                                                   and a.AreaType = @AreaType
       where sub.StorageUnitId = @StorageUnitId
         and a.AreaCode in ('RK')
      group by a.AreaId, l.Ailse
      order by COUNT(1) desc
      
      select top 1 @NearLocationId = l.LocationId,
                   @RelativeValue  = RelativeValue
        from StorageUnitBatchLocation subl (nolock)
        join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
        join Location                    l (nolock) on subl.LocationId = l.LocationId
        join AreaLocation               al (nolocK) on l.LocationId   = al.LocationId
        join Area                        a (nolock) on al.AreaId      = a.AreaId
       where sub.StorageUnitId = @StorageUnitId
         and a.AreaId          = @AreaId
    end
    set @Quantity = 1
    select top 1 @LocationId         = subl.LocationId,
                 @StorageUnitBatchId = subl.StorageUnitBatchId,
                 @Quantity           = subl.ActualQuantity - (subl.ReservedQuantity - isnull(i.ConfirmedQuantity,0))
      from Location                    l (nolock)
      join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
      join Area                        a (nolock) on al.AreaId               = a.AreaId
      join StorageUnitBatchLocation subl          on al.LocationId           = subl.LocationId
                                                 and ((subl.StorageUnitBatchId = subl.StorageUnitBatchId and @ReserveSUB is null) -- Pick on batch specified
                                                  or   subl.StorageUnitBatchId in (select StorageUnitBatchId from @TableBatches)) -- Pick batches according to linked receipts
      join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
      join Batch                       b (nolock) on sub.BatchId             = b.BatchId
      join Status                      s (nolock) on b.StatusId              = s.StatusId
      left
      join StorageUnitArea           sua (nolock) on sub.StorageUnitId       = sua.StorageUnitId
                                                 and a.AreaId                = sua.AreaId
      left
      join Instruction                 i (nolock) on i.JobId                 = @ReservedId
                                                 and subl.StorageUnitBatchId = i.StorageUnitBatchId
                                                 and subl.LocationId         = i.PickLocationId
     where l.ActivePicking      = 1 -- Yes
       and l.StocktakeInd      <= case when a.AreaCode = 'BK'
                                       then 1 -- Yes
                                       else 0 -- No
                                       end
       and a.WarehouseId        = @WarehouseId
       and sub.StorageUnitId    = @StorageUnitId
       and subl.ActualQuantity  - subl.ReservedQuantity >= case when a.AreaCode = 'BK'
                                                                then case when dbo.ufn_Configuration(175, @WarehouseId) = 1
                                                                          then @FullPallet -- Full pallet
                                                                          else @Quantity
                                                                          end
                                                                else @Quantity   -- Quantity Requested
                                                                end
       and s.StatusCode         in ('A','QCA')
       and a.StockOnHand        = 1
       and a.AreaCode          in ('BK','RK','SP','OF','POT')
       --and (l.LocationId        != isnull(@OldLocationId, -1) and subl.StorageUnitBatchId != isnull(@OldStorageUnitBatchId,-1))
       --and b.ExpiryDate > @Getdate -- Added for Cipla
       and convert(numeric(13,3), datediff(dd, @DeliveryDate, convert(nvarchar(10), isnull(b.ExpiryDate, dateadd(dd, @ShelfLife, b.CreateDate)), 120))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
       --and convert(numeric(13,3), datediff(dd, @DeliveryDate, isnull(convert(nvarchar(10), b.ExpiryDate, 120), dateadd(dd, @ShelfLife, b.CreateDate)))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
       --and convert(numeric(13,3), datediff(dd, @DeliveryDate, dateadd(dd, @ShelfLife, b.CreateDate))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
       and isnull(a.AreaType,'') = @AreaType
       and isnull(i.JobId, -1)   = @ReservedId
    order by case when @PickPartPalletFirst = 0
                  then 0
                  else subl.ActualQuantity - (subl.ReservedQuantity - isnull(i.ConfirmedQuantity,0))
                  end,
             case when @RelativeValue is not null
                  then case when @RelativeValue > isnull(l.RelativeValue,0)
                            then @RelativeValue - isnull(l.RelativeValue,0)
                            else isnull(l.RelativeValue,0) - @RelativeValue
                            end
                  else 0
                  end,
             case when @LIFO = 1
                  then datediff(dd, isnull(convert(nvarchar(10), b.ExpiryDate, 120), convert(nvarchar(10), dateadd(dd, @ShelfLife, b.CreateDate), 120)), '1900-01-01')
                  else datediff(dd, '1900-01-01', isnull(convert(nvarchar(10), b.ExpiryDate, 120), convert(nvarchar(10), dateadd(dd, @ShelfLife, b.CreateDate), 120)))
                  end asc,
             b.Batch,
             sua.PickOrder,
             --subl.ActualQuantity - (subl.ReservedQuantity - isnull(i.ConfirmedQuantity,0)),
             sub.StorageUnitBatchId
    
    select @rowcount = @@rowcount
    
    --select @LocationId              '@LocationId',
    --       @WarehouseId             '@WarehouseId',
    --       @StorageUnitId           '@StorageUnitId',
    --       @FullPallet              '@FullPallet',
    --       @Quantity                '@Quantity',
    --       @OldQuantity             '@OldQuantity',
    --       @OldLocationId           '@OldLocationId',
    --       @OldStorageUnitBatchId   '@OldStorageUnitBatchId',
    --       @ShelfLife               '@ShelfLife',
    --       @MinimumShelfLife        '@MinimumShelfLife',
    --       @AreaType                '@AreaType',
    --       @LIFO                    '@LIFO'
    
    if @rowcount = 0 and (select dbo.ufn_Configuration(155, @WarehouseId)) = 1
      select top 1 @LocationId         = subl.LocationId,
                   @StorageUnitBatchId = subl.StorageUnitBatchId,
                   @Quantity           = subl.ActualQuantity - (subl.ReservedQuantity - isnull(i.ConfirmedQuantity,0))
        from Location                    l (nolock)
        join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
        join Area                        a (nolock) on al.AreaId               = a.AreaId
        join StorageUnitBatchLocation subl          on al.LocationId           = subl.LocationId
                                                   and ((subl.StorageUnitBatchId = subl.StorageUnitBatchId and @ReserveSUB is null) -- Pick on batch specified
                                                    or   subl.StorageUnitBatchId in (select StorageUnitBatchId from @TableBatches)) -- Pick batches according to linked receipts
        join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
        join Batch                       b (nolock) on sub.BatchId             = b.BatchId
        join Status                      s (nolock) on b.StatusId              = s.StatusId
        left
        join StorageUnitArea           sua (nolock) on sub.StorageUnitId       = sua.StorageUnitId
                                                   and a.AreaId                = sua.AreaId
        left
        join Instruction                 i (nolock) on i.JobId                 = @ReservedId
                                                   and subl.StorageUnitBatchId = i.StorageUnitBatchId
                                                   and subl.LocationId         = i.PickLocationId
       where l.ActivePicking      = 1 -- Yes
         --and l.StocktakeInd      <= case when a.AreaCode = 'BK'
         --                                then 1 -- Yes
         --                                else 0 -- No
         --                                end
         and a.WarehouseId        = @WarehouseId
         and sub.StorageUnitId    = @StorageUnitId
         and subl.ActualQuantity  - subl.ReservedQuantity >= case when a.AreaCode = 'BK'
                                                                then case when dbo.ufn_Configuration(175, @WarehouseId) = 1
                                                                          then @FullPallet -- Full pallet
                                                                          else subl.ActualQuantity  - subl.ReservedQuantity
                                                                          end
                                                                  else 1           -- Quantity Requested
                                                                  end
         and s.StatusCode         in ('A','QCA')
         and a.StockOnHand        = 1
         and a.AreaCode          in ('BK','RK','SP','OF','POT')
         and (l.LocationId       != isnull(@OldLocationId, -1) and subl.StorageUnitBatchId != isnull(@OldStorageUnitBatchId,-1))
         and convert(numeric(13,3), datediff(dd, @DeliveryDate, convert(nvarchar(10), isnull(b.ExpiryDate, dateadd(dd, @ShelfLife, b.CreateDate)), 120))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
         and isnull(a.AreaType,'') = @AreaType
         and isnull(i.JobId, -1)   = @ReservedId
         and (subl.ActualQuantity - subl.ReservedQuantity) > 0
      order by case when @LIFO = 1
                    then datediff(dd, isnull(convert(nvarchar(10), b.ExpiryDate, 120), convert(nvarchar(10), dateadd(dd, @ShelfLife, b.CreateDate), 120)), '1900-01-01')
                    else datediff(dd, '1900-01-01', isnull(convert(nvarchar(10), b.ExpiryDate, 120), convert(nvarchar(10), dateadd(dd, @ShelfLife, b.CreateDate), 120)))
                    end asc,
               b.Batch,
               isnull(sua.PickOrder,1),
               subl.ActualQuantity - (subl.ReservedQuantity - isnull(i.ConfirmedQuantity,0))
    
    select @rowcount = @@rowcount
    
    if @LocationId is null
    begin
      set @Quantity = @OldQuantity
      
      if @AreaType = '' and (select dbo.ufn_Configuration(170, @WarehouseId)) = 1
      begin
        update Instruction
           set InstructionTypeId = (select InstructionTypeId from InstructionType where InstructionTypeCode = 'FM')
         where InstructionId = @InstructionId
        
        goto pickface
      end
    end
    
    --if (select dbo.ufn_Configuration(155, @WarehouseId)) = 0
    --  if @Quantity > @FullPallet
    --    set @Quantity = @FullPallet
    
    if @LocationId is not null
      return
  end
  else
  begin
  -- SAT01  start
      if (dbo.ufn_Configuration(389, @WarehouseId)) = 1 -- Efficiency is more important than FIFO rules
    begin
      select top 1 @AreaId = a.AreaId
        from StorageUnitBatch          sub (nolock)
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
        join AreaLocation               al (nolock) on subl.LocationId = al.LocationId
        join Location                    l (nolock) on al.LocationId = l.LocationId
        join Area                        a (nolock) on al.AreaId = a.AreaId
                                                   and a.AreaType = @AreaType
       where sub.StorageUnitId = @StorageUnitId
         and a.AreaCode in ('PK')
      group by a.AreaId, l.Ailse
      order by COUNT(1) desc
      
      select top 1 @NearLocationId = l.LocationId,
                   @RelativeValue  = RelativeValue
        from StorageUnitBatchLocation subl (nolock)
        join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
        join Location                    l (nolock) on subl.LocationId = l.LocationId
        join AreaLocation               al (nolocK) on l.LocationId   = al.LocationId
        join Area                        a (nolock) on al.AreaId      = a.AreaId
       where sub.StorageUnitId = @StorageUnitId
         and a.AreaId          = @AreaId
    end

	-- SAT01  End

  pickface:
    if @Replenishment = 0
    begin
       set @SmallPick = 0
       
       select top 1 @MaxPackQty = max(Quantity)
         from Pack      k (nolock)
         join PackType pt (nolock) on k.PackTypeId = pt.PackTypeId
        where k.StorageUnitId = @StorageUnitId
          and OutboundSequence != 1
       
       if exists(select top 1 1 from Area a where WarehouseId = @WarehouseId and isnull(a.AreaType,'') = @AreaType and SmallPick = 1)
       and @Quantity < @MaxPackQty--5
         set @SmallPick = 1
      
      if @OldLocationId is null
      begin
        --if @Temporary = 1
        --begin
        --  exec @LocationId = p_Temporary_Pickface_Create
        --   @WarehouseId        = @WarehouseId,
        --   @ExternalCompanyId  = @ExternalCompanyId,
        --   @ShelfLife          = @ShelfLife,
        --   @MinimumShelfLife   = @MinimumShelfLife,
        --   @StorageUnitId      = @StorageUnitId,
        --   @Quantity           = @Quantity,
        --   @FullPallet         = @FullPallet,
        --   @StorageUnitBatchId = @StorageUnitBatchId output
          
        --  if @LocationId = -1
        --    set @LocationId = null
          
        --  if @LocationId is not null -- return tempoary location
        --    return
        --end
        
        if @PickEmpty = 1
        begin
          -- Get location with most quantity
          select top 1 @LocationId         = subl.LocationId,
                       @StorageUnitBatchId = subl.StorageUnitBatchId
            from Location                    l (nolock)
            join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
            join Area                        a (nolock) on al.AreaId               = a.AreaId
            join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
                                                       and ((subl.StorageUnitBatchId = subl.StorageUnitBatchId and @ReserveSUB is null) -- Pick on batch specified
                                                        or   subl.StorageUnitBatchId in (select StorageUnitBatchId from @TableBatches)) -- Pick batches according to linked receipts
            join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
            join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
            join Batch                       b (nolock) on sub.BatchId             = b.BatchId
            join Status                      s (nolock) on b.StatusId              = s.StatusId
            left
            join Instruction                 i (nolock) on i.JobId                 = @ReservedId
                                                       and subl.StorageUnitBatchId = i.StorageUnitBatchId
                                                       and subl.LocationId         = i.PickLocationId
           where l.ActivePicking   = 1 -- Yes
             and sub.StorageUnitId = @StorageUnitId
             and a.WarehouseId     = @WarehouseId
             and a.AreaCode       in ('PK','SP','POT')
             and l.StocktakeInd      <= case when @CheckStockTakeInd = 0
                                             then 1 -- Yes
                                             else 0 -- No
                                             end
             and s.StatusCode      in ('A','QCA')
             and convert(numeric(13,3), datediff(dd, @DeliveryDate, convert(nvarchar(10), isnull(b.ExpiryDate, dateadd(dd, @ShelfLife, b.CreateDate)), 120))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
             and isnull(a.AreaType,'') = @AreaType
             and isnull(i.JobId, -1)   = @ReservedId
          order by case when @SmallPick = 0 then a.SmallPick * -1 else a.SmallPick end desc,
                   ActualQuantity - (subl.ReservedQuantity - isnull(i.ConfirmedQuantity,0)) asc,
                   case when @RelativeValue > isnull(l.RelativeValue,0)
                        then @RelativeValue - isnull(l.RelativeValue,0)
                        else isnull(l.RelativeValue,0) - @RelativeValue
                        end
          
          select @rowcount = @@rowcount
        end
        else
        begin
          -- Get location with enough quantity and oldest batch
          select top 1 @LocationId         = subl.LocationId,
                       @StorageUnitBatchId = subl.StorageUnitBatchId
            from Location                    l (nolock)
            join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
            join Area                        a (nolock) on al.AreaId               = a.AreaId
            join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
                                                       and ((subl.StorageUnitBatchId = subl.StorageUnitBatchId and @ReserveSUB is null) -- Pick on batch specified
                                                       or   subl.StorageUnitBatchId in (select StorageUnitBatchId from @TableBatches)) -- Pick batches according to linked receipts
            join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
            join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
            join Batch                       b (nolock) on sub.BatchId             = b.BatchId
            join Status                      s (nolock) on b.StatusId              = s.StatusId
            left
            join Instruction                 i (nolock) on i.JobId                 = @ReservedId
                                                       and subl.StorageUnitBatchId = i.StorageUnitBatchId
                                                       and subl.LocationId         = i.PickLocationId
           where l.ActivePicking   = 1 -- Yes
             and sub.StorageUnitId = @StorageUnitId
             and a.WarehouseId     = @WarehouseId
             and a.AreaCode       in ('PK','SP','POT')
             and l.StocktakeInd      <= case when @CheckStockTakeInd = 0
                                       then 1 -- Yes
                                       else 0 -- No
                                       end
             and s.StatusCode      in ('A','QCA')
             and convert(numeric(13,3), datediff(dd, @DeliveryDate, convert(nvarchar(10), isnull(b.ExpiryDate, dateadd(dd, @ShelfLife, b.CreateDate)), 120))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
             and (subl.ActualQuantity > 0
              or  subl.AllocatedQuantity > 0) -- Stock in location or is coming - added for Tempoary PF fix for FairFields
             and isnull(a.AreaType,'') = @AreaType
             and isnull(i.JobId, -1)   = @ReservedId
          order by case when @SmallPick = 0 then a.SmallPick * -1 else a.SmallPick end desc,
                   case when subl.ActualQuantity > 0 then 0 else 1 end, -- GS 2012-11-12 - Pick Pickface with stock, then batch
                   case when subl.AllocatedQuantity > 0 then 0 else 1 end, -- GS 2013-11-04 - Pick Pickface with Replenishment, then batch
                   a.Replenish, -- Pick from XDock / Non pickface first
                   a.AreaCode, -- Pick from PK first
                   isnull(convert(nvarchar(10), b.ExpiryDate, 120), convert(nvarchar(10), dateadd(dd, @ShelfLife, b.CreateDate), 120)),
                   case when @OrderByQty = 0 then 0 else subl.ActualQuantity end, -- GS 2013-01-14 - Order by Quantity then Relative Value
                   case when @RelativeValue > isnull(l.RelativeValue,0)
                        then @RelativeValue - isnull(l.RelativeValue,0)
                        else isnull(l.RelativeValue,0) - @RelativeValue
                        end
          
          select @rowcount = @@rowcount
        end
      end
      else
      begin
        -- Min LocationId > OldLocationId
        select top 1 @LocationId         = subl.LocationId,
                     @StorageUnitBatchId = subl.StorageUnitBatchId
          from Location                    l (nolock)
          join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
          join Area                        a (nolock) on al.AreaId               = a.AreaId
          join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
                                                     and ((subl.StorageUnitBatchId = subl.StorageUnitBatchId and @ReserveSUB is null) -- Pick on batch specified
                                                      or   subl.StorageUnitBatchId in (select StorageUnitBatchId from @TableBatches)) -- Pick batches according to linked receipts
          join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
          join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
          join Batch                       b (nolock) on sub.BatchId             = b.BatchId
          join Status                      s (nolock) on b.StatusId              = s.StatusId
          left
          join Instruction                 i (nolock) on i.JobId                 = @ReservedId
                                                     and subl.StorageUnitBatchId = i.StorageUnitBatchId
                                                     and subl.LocationId         = i.PickLocationId
         where l.ActivePicking   = 1 -- Yes
           and sub.StorageUnitId = @StorageUnitId
           and a.WarehouseId     = @WarehouseId
           and a.AreaCode       in ('PK','SP','POT')
           and l.StocktakeInd      <= case when @CheckStockTakeInd = 0
                                       then 1 -- Yes
                                       else 0 -- No
                                       end
           and s.StatusCode      in ('A','QCA')
           and l.LocationId      > @OldLocationId
           and convert(numeric(13,3), datediff(dd, @DeliveryDate, convert(nvarchar(10), isnull(b.ExpiryDate, dateadd(dd, @ShelfLife, b.CreateDate)), 120))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
           and subl.ActualQuantity > 0
           and isnull(a.AreaType,'') = @AreaType
           and isnull(i.JobId, -1)   = @ReservedId
        order by case when @SmallPick = 0 then a.SmallPick * -1 else a.SmallPick end desc,
                 a.AreaCode, -- Pick from PK first
                 l.LocationId asc,
                 subl.ActualQuantity desc
        
        select @rowcount = @@rowcount
        
        if @rowcount = 0
        begin
          -- Min LocationId > OldLocationId
          select top 1 @LocationId         = subl.LocationId,
                       @StorageUnitBatchId = subl.StorageUnitBatchId
            from Location                    l (nolock)
            join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
            join Area                        a (nolock) on al.AreaId               = a.AreaId
            join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
                                                       and ((subl.StorageUnitBatchId = subl.StorageUnitBatchId and @ReserveSUB is null) -- Pick on batch specified
                                                        or   subl.StorageUnitBatchId in (select StorageUnitBatchId from @TableBatches)) -- Pick batches according to linked receipts
            join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
            join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
            join Batch                       b (nolock) on sub.BatchId             = b.BatchId
            join Status                      s (nolock) on b.StatusId              = s.StatusId
            left
            join Instruction                 i (nolock) on i.JobId                 = @ReservedId
                                                       and subl.StorageUnitBatchId = i.StorageUnitBatchId
                                                       and subl.LocationId         = i.PickLocationId
           where l.ActivePicking   = 1 -- Yes
             and sub.StorageUnitId = @StorageUnitId
             and a.WarehouseId     = @WarehouseId
             and a.AreaCode       in ('PK','SP','POT')
             and l.StocktakeInd      <= case when @CheckStockTakeInd = 0
                                       then 1 -- Yes
                                       else 0 -- No
                                       end
             and s.StatusCode      in ('A','QCA')
             and l.LocationId      < @OldLocationId
             and convert(numeric(13,3), datediff(dd, @DeliveryDate, convert(nvarchar(10), isnull(b.ExpiryDate, dateadd(dd, @ShelfLife, b.CreateDate)), 120))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
             and subl.ActualQuantity > 0
             and isnull(a.AreaType,'') = @AreaType
             and isnull(i.JobId, -1)   = @ReservedId
          order by case when @SmallPick = 0 then a.SmallPick * -1 else a.SmallPick end desc,
                   a.AreaCode, -- Pick from PK first
                   l.LocationId asc,
                   subl.ActualQuantity desc
          
          select @rowcount = @@rowcount
          
          if @rowcount = 0
          begin
            -- Min LocationId > OldLocationId
            select top 1 @LocationId         = subl.LocationId,
                         @StorageUnitBatchId = subl.StorageUnitBatchId
              from Location                    l (nolock)
              join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
              join Area                        a (nolock) on al.AreaId               = a.AreaId
              join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
                                                         and ((subl.StorageUnitBatchId = subl.StorageUnitBatchId and @ReserveSUB is null) -- Pick on batch specified
                                                          or   subl.StorageUnitBatchId in (select StorageUnitBatchId from @TableBatches)) -- Pick batches according to linked receipts
              join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
              join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
              join Batch                       b (nolock) on sub.BatchId             = b.BatchId
              join Status                      s (nolock) on b.StatusId              = s.StatusId
              left
              join Instruction                 i (nolock) on i.JobId                 = @ReservedId
                                                         and subl.StorageUnitBatchId = i.StorageUnitBatchId
                                                         and subl.LocationId         = i.PickLocationId
             where l.ActivePicking   = 1 -- Yes
               and sub.StorageUnitId = @StorageUnitId
               and a.WarehouseId     = @WarehouseId
               and a.AreaCode       in ('PK','SP','POT')
               and l.StocktakeInd      <= case when @CheckStockTakeInd = 0
                                       then 1 -- Yes
                                       else 0 -- No
                                       end
               and s.StatusCode      in ('A','QCA')
               and l.LocationId      = @OldLocationId
               and convert(numeric(13,3), datediff(dd, @DeliveryDate, convert(nvarchar(10), isnull(b.ExpiryDate, dateadd(dd, @ShelfLife, b.CreateDate)), 120))) / @ShelfLife * 100.000 > @MinimumShelfLife -- Minimum Shelf life check
               and isnull(a.AreaType,'') = @AreaType
               and isnull(i.JobId, -1)   = @ReservedId
            order by case when @SmallPick = 0 then a.SmallPick * -1 else a.SmallPick end desc,
                     a.AreaCode, -- Pick from PK first
                     l.LocationId asc,
                     subl.ActualQuantity desc
            
            select @rowcount = @@rowcount
          end
        end
      end
      
      if @rowcount = 0 and @Temporary = 1
      begin
        exec @LocationId = p_Temporary_Pickface_Create
         @WarehouseId        = @WarehouseId,
         @ExternalCompanyId  = @ExternalCompanyId,
         @ShelfLife          = @ShelfLife,
         @MinimumShelfLife   = @MinimumShelfLife,
         @StorageUnitId      = @StorageUnitId,
         @Quantity           = @Quantity,
         @FullPallet         = @FullPallet,
         @StorageUnitBatchId = @StorageUnitBatchId output,
         @DeliveryDate       = @DeliveryDate
        
        if @LocationId = -1
          set @LocationId = null
          
          if @LocationId is not null -- return tempoary location
            return
      end
      
      if @ReserveSUB is null
      begin
        if @rowcount = 0 and @OldLocationId is not null and @Temporary = 0
        begin
           -- Get location without quantity
          select top 1 @LocationId         = sul.LocationId
            from StorageUnitLocation       sul (nolock)
            join Location                    l (nolock) on sul.LocationId         = l.LocationId
            join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
            join Area                        a (nolock) on al.AreaId              = a.AreaId
           where l.ActivePicking   = 1 -- Yes
             and sul.StorageUnitId = @StorageUnitId
             and a.WarehouseId     = @WarehouseId
             and a.AreaCode       in ('PK','SP','POT')
             and l.StocktakeInd      <= case when @CheckStockTakeInd = 0
                                       then 1 -- Yes
                                       else 0 -- No
                                       end
             and l.LocationId      > @OldLocationId
             and isnull(a.AreaType,'') = @AreaType
          order by case when @SmallPick = 0 then a.SmallPick * -1 else a.SmallPick end desc,
                   a.AreaCode, -- Pick from PK first
                   l.LocationId asc
          
          select @rowcount = @@rowcount
          
          if @rowcount = 0
          begin
             -- Get location without quantity
            select top 1 @LocationId         = sul.LocationId
              from StorageUnitLocation       sul (nolock)
              join Location                    l (nolock) on sul.LocationId         = l.LocationId
              join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
              join Area                        a (nolock) on al.AreaId              = a.AreaId
             where l.ActivePicking   = 1 -- Yes
               and sul.StorageUnitId = @StorageUnitId
               and a.WarehouseId     = @WarehouseId
               and a.AreaCode       in ('PK','SP','POT')
               and l.StocktakeInd      <= case when @CheckStockTakeInd = 0
                                       then 1 -- Yes
                                       else 0 -- No
                                       end
               and l.LocationId      < @OldLocationId
               and isnull(a.AreaType,'') = @AreaType
            order by case when @SmallPick = 0 then a.SmallPick * -1 else a.SmallPick end desc,
                     a.AreaCode, -- Pick from PK first
                     l.LocationId asc
          
            select @rowcount = @@rowcount
          end
        end
      end
      
      if @rowcount = 0 and @LocationId is null and @Temporary = 0
      begin
         -- Get location without quantity
        select top 1 @LocationId         = sul.LocationId
          from StorageUnitLocation       sul (nolock)
          join Location                    l (nolock) on sul.LocationId         = l.LocationId
          join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
          join Area                        a (nolock) on al.AreaId              = a.AreaId
         where l.ActivePicking   = 1 -- Yes
           and sul.StorageUnitId = @StorageUnitId
           and a.WarehouseId     = @WarehouseId
           and a.AreaCode       in ('PK','SP','POT')
           and isnull(a.AreaType,'') = @AreaType
        order by case when @SmallPick = 0 then a.SmallPick * -1 else a.SmallPick end desc,
                 a.AreaCode, -- Pick from PK first
                 case when @RelativeValue > isnull(l.RelativeValue,0)
                      then @RelativeValue - isnull(l.RelativeValue,0)
                      else isnull(l.RelativeValue,0) - @RelativeValue
                      end
      
        select @rowcount = @@rowcount
        
        if @rowcount = 0 and @LocationId is null and @Temporary = 0
        begin
           -- Get location without quantity
          select top 1 @LocationId         = sul.LocationId
            from StorageUnitLocation       sul (nolock)
            join Location                    l (nolock) on sul.LocationId         = l.LocationId
            join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
            join Area                        a (nolock) on al.AreaId              = a.AreaId
           where l.ActivePicking   = 1 -- Yes
             and sul.StorageUnitId = @StorageUnitId
             and a.WarehouseId     = @WarehouseId
             and a.AreaCode       in ('PK','SP','POT')
             and l.StocktakeInd      <= case when @CheckStockTakeInd = 0
                                       then 1 -- Yes
                             else 0 -- No
                                       end
             and isnull(a.AreaType,'') = @AreaType
          order by case when @SmallPick = 0 then a.SmallPick * -1 else a.SmallPick end desc,
                   a.AreaCode, -- Pick from PK first
                   case when @RelativeValue > isnull(l.RelativeValue,0)
                        then @RelativeValue - isnull(l.RelativeValue,0)
                        else isnull(l.RelativeValue,0) - @RelativeValue
                        end
        
          select @rowcount = @@rowcount
          
          if dbo.ufn_Configuration(433, @WarehouseId) = 1 -- Get specials location if linked to specials area only
          if @rowcount = 0 and @AreaType != 'MassMart'
          begin
            begin
              select top 1 @LocationId         = l.LocationId
                from Location                    l (nolock)
                join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
                join Area                        a (nolock) on al.AreaId              = a.AreaId
                join StorageUnitArea           sua (nolock) on a.AreaId               = sua.AreaId
               where l.ActivePicking   = 1 -- Yes
                 and sua.StorageUnitId = @StorageUnitId
                 and a.WarehouseId     = @WarehouseId
                 and a.AreaCode        = 'SP'
                 and l.StocktakeInd      <= case when @CheckStockTakeInd = 0
                                       then 1 -- Yes
                                       else 0 -- No
                                       end
                 and isnull(a.AreaType,'') = @AreaType
              order by case when @SmallPick = 0 then a.SmallPick * -1 else a.SmallPick end desc,
                       case when @RelativeValue > isnull(l.RelativeValue,0)
                            then @RelativeValue - isnull(l.RelativeValue,0)
                            else isnull(l.RelativeValue,0) - @RelativeValue
                            end 
              
              select @rowcount = @@rowcount
            end
          end
        end
      end
    end
    
    if @LocationId is null and @ReserveSUB is not null and @Stop = 0
    begin
      exec p_Pick_Location_Get
       @WarehouseId         = @WarehouseId,
       @LocationId          = @LocationId output,
       @StorageUnitBatchId  = @StorageUnitBatchId output,
       @Quantity            = @Quantity output,
       @Replenishment       = @Replenishment,
       @InstructionId       = @InstructionId,
       @LIFO                = @LIFO,
       @MinimumShelfLife    = @MinimumShelfLife,
       @Temporary           = @Temporary,
       @AreaType            = @AreaType,
       @DeliveryDate        = @DeliveryDate,
       @InstructionTypeCode = @InstructionTypeCode,
       @Stop                = 1,
       @IssueId             = @IssueId
    end
    
    if @LocationId is not null
      return
  end
end
