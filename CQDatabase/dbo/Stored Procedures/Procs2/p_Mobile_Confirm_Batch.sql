﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Confirm_Batch
  ///   Filename       : p_Mobile_Confirm_Batch.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Confirm_Batch
(
 @instructionId int,
 @barcode       nvarchar(50)
)

as
begin
  set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         InstructionId,
         Barcode,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @InstructionId,
         @barcode,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error               int,
          @StorageUnitBatchId  int,
          @Batch               nvarchar(50),
          @StatusCode          nvarchar(10),
          @Prefix              nvarchar(2),
          @InstructionTypeCode nvarchar(10),
          @WarehouseId         int
  
  if left(@barcode,2) = 'A:'
    set @Prefix = 'A'
  
  if left(@barcode,2) = 'Q:' or left(@barcode,2) = 'QC:'
    set @Prefix = 'QC'
  
  select @barcode = replace(@barcode,'A:','')
  select @barcode = replace(@barcode,'Q:','')
  select @barcode = replace(@barcode,'QC:','')
  
  set @Error = 0
  
  if @Barcode is null
  begin
    set @Error = 4
    goto result
  end
  
  select @StorageUnitBatchId  = i.StorageUnitBatchId,
         @InstructionTypeCode = it.InstructionTypeCode,
         @WarehouseId         = i.WarehouseId
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where InstructionId = @instructionId
  
  if (select dbo.ufn_Configuration(198, @WarehouseId)) = 0
    if @Barcode = 'Default'
    begin
      set @Error = 4
      goto result
    end
  
  select @Batch      = b.Batch,
         @StatusCode = s.StatusCode
    from StorageUnitBatch sub (nolock)
    join Batch              b (nolock) on sub.BatchId = b.BatchId
    join Status             s (nolock) on b.StatusId = s.StatusId
   where sub.StorageUnitBatchId = @StorageUnitBatchId
  
  if @Batch != @barcode
  begin
    set @Error = 4
    goto result
  end
  
  --@InstructionTypeCode
  
  if @Prefix != @StatusCode
  begin
    set @Error = 3
    goto result
  end
  
  result:
    update MobileLog
       set EndDate = getdate()
     where MobileLogId = @MobileLogId
    
    select @Error
    return @Error
end
