﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReceiptLine_Insert
  ///   Filename       : p_ReceiptLine_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2013 07:02:51
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ReceiptLine table.
  /// </remarks>
  /// <param>
  ///   @ReceiptLineId int = null output,
  ///   @ReceiptId int = null,
  ///   @InboundLineId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @RequiredQuantity float = null,
  ///   @ReceivedQuantity float = null,
  ///   @AcceptedQuantity float = null,
  ///   @RejectQuantity float = null,
  ///   @DeliveryNoteQuantity float = null,
  ///   @SampleQuantity numeric(13,6) = null,
  ///   @AcceptedWeight numeric(13,6) = null,
  ///   @ChildStorageUnitBatchId int = null,
  ///   @NumberOfPallets int = null,
  ///   @AssaySamples float = null,
  ///   @BOELineNumber nvarchar(50) = null,
  ///   @CountryofOrigin nvarchar(50) = null,
  ///   @TariffCode nvarchar(50) = null,
  ///   @Reference1 nvarchar(50) = null,
  ///   @Reference2 nvarchar(50) = null,
  ///   @UnitPrice decimal(13,2) = null 
  /// </param>
  /// <returns>
  ///   ReceiptLine.ReceiptLineId,
  ///   ReceiptLine.ReceiptId,
  ///   ReceiptLine.InboundLineId,
  ///   ReceiptLine.StorageUnitBatchId,
  ///   ReceiptLine.StatusId,
  ///   ReceiptLine.OperatorId,
  ///   ReceiptLine.RequiredQuantity,
  ///   ReceiptLine.ReceivedQuantity,
  ///   ReceiptLine.AcceptedQuantity,
  ///   ReceiptLine.RejectQuantity,
  ///   ReceiptLine.DeliveryNoteQuantity,
  ///   ReceiptLine.SampleQuantity,
  ///   ReceiptLine.AcceptedWeight,
  ///   ReceiptLine.ChildStorageUnitBatchId,
  ///   ReceiptLine.NumberOfPallets,
  ///   ReceiptLine.AssaySamples,
  ///   ReceiptLineHistory.BOELineNumber,
  ///   ReceiptLineHistory.CountryofOrigin,
  ///   ReceiptLineHistory.TariffCode,
  ///   ReceiptLineHistory.Reference1,
  ///   ReceiptLineHistory.Reference2,
  ///   ReceiptLineHistory.UnitPrice
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReceiptLine_Insert
(
 @ReceiptLineId int = null output,
 @ReceiptId int = null,
 @InboundLineId int = null,
 @StorageUnitBatchId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @RequiredQuantity float = null,
 @ReceivedQuantity float = null,
 @AcceptedQuantity float = null,
 @RejectQuantity float = null,
 @DeliveryNoteQuantity float = null,
 @SampleQuantity numeric(13,6) = null,
 @AcceptedWeight numeric(13,6) = null,
 @ChildStorageUnitBatchId int = null,
 @NumberOfPallets int = null,
 @AssaySamples float = null,
 @BOELineNumber nvarchar(50) = null,
 @CountryofOrigin nvarchar(50) = null,
 @TariffCode nvarchar(50) = null,
 @Reference1 nvarchar(50) = null,
 @Reference2 nvarchar(50) = null,
 @UnitPrice decimal(13,2) = null,
 @ReceivedDate datetime = null,
 @PutawayStarted datetime = null,
 @PutawayEnded datetime = null 
)

as
begin
	 set nocount on;
  
  if @ReceiptLineId = '-1'
    set @ReceiptLineId = null;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @InboundLineId = '-1'
    set @InboundLineId = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @ChildStorageUnitBatchId = '-1'
    set @ChildStorageUnitBatchId = null;
  
	 declare @Error int
 
  insert ReceiptLine
        (ReceiptId,
         InboundLineId,
         StorageUnitBatchId,
         StatusId,
         OperatorId,
         RequiredQuantity,
         ReceivedQuantity,
         AcceptedQuantity,
         RejectQuantity,
         DeliveryNoteQuantity,
         SampleQuantity,
         AcceptedWeight,
         ChildStorageUnitBatchId,
         NumberOfPallets,
         AssaySamples,
         BOELineNumber,
         CountryofOrigin,
         TariffCode,
         Reference1,
         Reference2,
         UnitPrice,
		 ReceivedDate,
		 PutawayStarted,
		 PutawayEnded)
  select @ReceiptId,
         @InboundLineId,
         @StorageUnitBatchId,
         @StatusId,
         @OperatorId,
         @RequiredQuantity,
         @ReceivedQuantity,
         @AcceptedQuantity,
         @RejectQuantity,
         @DeliveryNoteQuantity,
         @SampleQuantity,
         @AcceptedWeight,
         @ChildStorageUnitBatchId,
         @NumberOfPallets,
         @AssaySamples,
         @BOELineNumber,
         @CountryofOrigin,
         @TariffCode,
         @Reference1,
         @Reference2,
         @UnitPrice,
		 @ReceivedDate,
		 @PutawayStarted,
		 @PutawayEnded 
  
  select @Error = @@Error, @ReceiptLineId = scope_identity()
  
  
  return @Error
  
end
