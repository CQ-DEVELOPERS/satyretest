﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Pickface_Check_Accept
  ///   Filename       : p_Mobile_Pickface_Check_Accept.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Pickface_Check_Accept
(
 @operatorId    int,
 @storageUnitId int,
 @locationId    int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Error = 0
  set @Errormsg = 'Error executing p_Mobile_Pickface_Check_Accept'
  
  begin transaction
  
  if (select distinct AreaCode
        from viewLocation
       where LocationId = @locationId) not in ('PK','BK','SP')
  begin
    set @Error = -1
    goto error
  end
  
  if not exists(select 1
                  from StorageUnitLocation
                 where StorageUnitId = @StorageUnitId
                   and LocationId    = @LocationId)
  begin
    exec @Error = p_StorageUnitLocation_Insert
     @StorageUnitId = @StorageUnitId,
     @LocationId    = @LocationId
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  select @Error
  return @Error
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    select @Error
    return @Error
end
