﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Register_SerialNumber_Search
  ///   Filename       : p_Register_SerialNumber_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Register_SerialNumber_Search
(
 @ReceiptId int
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   Indent                    int identity,
   ReceiptId                 int,
   ReceiptLineId             int,
   LineNumber                int,
   OrderNumber               nvarchar(30),
   AcceptedQuantity          float,
   SupplierCode              nvarchar(30),
   Supplier                  nvarchar(255),
   StorageUnitBatchId        int
  );
  
  insert @TableResult
        (ReceiptId,
         ReceiptLineId,
         OrderNumber,
         AcceptedQuantity,
         SupplierCode,
         Supplier,
         StorageUnitBatchId)
  select r.ReceiptId,
         rl.ReceiptLineId,
         id.OrderNumber,
         rl.AcceptedQuantity,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         rl.StorageUnitBatchId
    from Receipt          r (nolock)
    join ReceiptLine     rl (nolock) on r.ReceiptId          = rl.ReceiptId
    join InboundDocument id (nolock) on r.InboundDocumentId  = id.InboundDocumentId
    join InboundLine     il (nolock) on id.InboundDocumentId = il.InboundDocumentId
                                    and rl.InboundLineId     = il.InboundLineId
    join ExternalCompany ec (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
   where r.ReceiptId = @ReceiptId
  order by il.InboundLineId
  
  select t.ReceiptId,
         t.ReceiptLineId,
         su.StorageUnitId,
         sub.BatchId,
         t.OrderNumber,
         isnull(t.LineNumber,t.Indent),
         t.AcceptedQuantity,
         t.SupplierCode,
         t.Supplier,
         p.ProductCode,
         p.Product
    from @TableResult t
    join StorageUnitBatch sub (nolock) on t.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId         = p.ProductId
   where t.AcceptedQuantity > 0
end
