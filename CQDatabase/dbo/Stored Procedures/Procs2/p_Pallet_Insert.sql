﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Insert
  ///   Filename       : p_Pallet_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Insert
(
 @PalletId int = null output,
 @StorageUnitBatchId int = null,
 @LocationId int = null,
 @Weight float = null,
 @Tare float = null,
 @Quantity float = null,
 @Prints int = null,
 @CreateDate datetime = null,
 @Nett float = null,
 @ReasonId int = null,
 @EmptyWeight float = null 
)

as
begin
	 set nocount on;
  
  if @PalletId = '-1'
    set @PalletId = null;
  
  declare @Error int,
		  @ApprovedStatusId int
 
  select @ApprovedStatusId = dbo.ufn_StatusId('P','AP')
 
  insert Pallet
        (StorageUnitBatchId,
         LocationId,
         Weight,
         Tare,
         Quantity,
         Prints,
         CreateDate,
         Nett,
         ReasonId,
         EmptyWeight,
         StatusId)
  select @StorageUnitBatchId,
         @LocationId,
         @Weight,
         @Tare,
         @Quantity,
         @Prints,
         @CreateDate,
         @Nett,
         @ReasonId,
         @EmptyWeight ,
         @ApprovedStatusId
  
  select @Error = @@Error, @PalletId = scope_identity()
  
  
  return @Error
  
end
