﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Method_Search
  ///   Filename       : p_Method_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:05
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Method table.
  /// </remarks>
  /// <param>
  ///   @MethodId int = null output,
  ///   @MethodCode nvarchar(20) = null,
  ///   @Method nvarchar(510) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Method.MethodId,
  ///   Method.MethodCode,
  ///   Method.Method 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Method_Search
(
 @MethodId int = null output,
 @MethodCode nvarchar(20) = null,
 @Method nvarchar(510) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @MethodId = '-1'
    set @MethodId = null;
  
  if @MethodCode = '-1'
    set @MethodCode = null;
  
  if @Method = '-1'
    set @Method = null;
  
 
  select
         Method.MethodId
        ,Method.MethodCode
        ,Method.Method
    from Method
   where isnull(Method.MethodId,'0')  = isnull(@MethodId, isnull(Method.MethodId,'0'))
     and isnull(Method.MethodCode,'%')  like '%' + isnull(@MethodCode, isnull(Method.MethodCode,'%')) + '%'
     and isnull(Method.Method,'%')  like '%' + isnull(@Method, isnull(Method.Method,'%')) + '%'
  order by MethodCode
  
end
