﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundLine_Create
  ///   Filename       : p_OutboundLine_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Jul 2007 19:59:21
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundLine table.
  /// </remarks>
  /// <param>
  ///   @OutboundLineId int = null output,
  ///   @OutboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null,
  ///   @BatchId int = null 
  /// </param>
  /// <returns>
  ///   @Error
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundLine_Create
(
 @OperatorId         int,
 @OutboundLineId     int = null output,
 @OutboundDocumentId int = null,
 @StorageUnitId      int = null,
 @LineNumber         int = null,
 @Quantity           float = null,
 @BatchId            int = null,
 @IssueId            int = null output,
 @IssueLineId        int = null output,
 @WaveId             int = null
)

as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500) = 'Error executing p_OutboundLine_Create',
          @StatusId          int,
          @Transaction       bit = 0
  
  if @batchId = -1
    set @batchId = null
  
  if @StatusId is null
    select @StatusId = StatusId
      from Status (nolock)
     where StatusCode = 'N'
       and Type       = 'ID'
  
  if @LineNumber is null
    set @LineNumber = isnull((select count(1) from OutboundLine (nolock) where OutboundDocumentId = @OutboundDocumentId),1) + 1
  
  if @@trancount = 0
  begin
    begin transaction
    set @Transaction = 1
  end
  
  if @WaveId is null
  if exists(select 1
              from OutboundLine (nolock)
             where OutboundDocumentId   = @OutboundDocumentId
               and StorageUnitId        = @StorageUnitId
               and isnull(BatchId,'-1') = isnull(@BatchId,'-1'))
  begin
    set @Error = -1
    set @Errormsg = 'Product and batch combination already exists'
    goto error
  end
  
  exec @Error = p_OutboundLine_Insert
   @OutboundLineId     = @OutboundLineId output,
   @OutboundDocumentId = @OutboundDocumentId,
   @StorageUnitId      = @StorageUnitId,
   @StatusId           = @StatusId,
   @LineNumber         = @LineNumber,
   @Quantity           = @Quantity,
   @BatchId            = @BatchId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Despatch_Create_Issue
   @OutboundDocumentId = @OutboundDocumentId,
   @OutboundLineId     = @OutboundLineId,
   @OperatorId         = @OperatorId,
   @IssueId            = @IssueId output,
   @IssueLineId        = @IssueLineId output
  
  if @Error <> 0
    goto error
  
  result:
    if @Transaction = 1
      commit transaction
    return 0
  
  error:
    if @Transaction = 1
    begin
      RAISERROR (900000,-1,-1, @Errormsg);
      rollback transaction
    end
    return @Error
end
