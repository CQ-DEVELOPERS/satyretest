﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Priority_List
  ///   Filename       : p_Priority_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:14
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Priority table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Priority.PriorityId,
  ///   Priority.Priority 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Priority_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as PriorityId
        ,'{All}' as Priority
  union
  select
         Priority.PriorityId
        ,Priority.Priority
    from Priority
  
end
