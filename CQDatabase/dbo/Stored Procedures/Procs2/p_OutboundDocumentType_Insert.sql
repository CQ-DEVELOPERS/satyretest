﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocumentType_Insert
  ///   Filename       : p_OutboundDocumentType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Jan 2014 09:16:04
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundDocumentType table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentTypeId int = null output,
  ///   @OutboundDocumentTypeCode nvarchar(20) = null,
  ///   @OutboundDocumentType nvarchar(60) = null,
  ///   @PriorityId int = null,
  ///   @AlternatePallet bit = null,
  ///   @SubstitutePallet bit = null,
  ///   @AutoRelease bit = null,
  ///   @AddToShipment bit = null,
  ///   @MultipleOnShipment bit = null,
  ///   @LIFO bit = null,
  ///   @MinimumShelfLife numeric(13,3) = null,
  ///   @AreaType nvarchar(20) = null,
  ///   @AutoSendup bit = null,
  ///   @CheckingLane int = null,
  ///   @DespatchBay int = null,
  ///   @PlanningComplete bit = null,
  ///   @AutoInvoice bit = null,
  ///   @Backorder bit = null,
  ///   @AutoCheck bit = null,
  ///   @ReserveBatch bit = null 
  /// </param>
  /// <returns>
  ///   OutboundDocumentType.OutboundDocumentTypeId,
  ///   OutboundDocumentType.OutboundDocumentTypeCode,
  ///   OutboundDocumentType.OutboundDocumentType,
  ///   OutboundDocumentType.PriorityId,
  ///   OutboundDocumentType.AlternatePallet,
  ///   OutboundDocumentType.SubstitutePallet,
  ///   OutboundDocumentType.AutoRelease,
  ///   OutboundDocumentType.AddToShipment,
  ///   OutboundDocumentType.MultipleOnShipment,
  ///   OutboundDocumentType.LIFO,
  ///   OutboundDocumentType.MinimumShelfLife,
  ///   OutboundDocumentType.AreaType,
  ///   OutboundDocumentType.AutoSendup,
  ///   OutboundDocumentType.CheckingLane,
  ///   OutboundDocumentType.DespatchBay,
  ///   OutboundDocumentType.PlanningComplete,
  ///   OutboundDocumentType.AutoInvoice,
  ///   OutboundDocumentType.Backorder,
  ///   OutboundDocumentType.AutoCheck,
  ///   OutboundDocumentType.ReserveBatch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocumentType_Insert
(
 @OutboundDocumentTypeId int = null output,
 @OutboundDocumentTypeCode nvarchar(20) = null,
 @OutboundDocumentType nvarchar(60) = null,
 @PriorityId int = null,
 @AlternatePallet bit = null,
 @SubstitutePallet bit = null,
 @AutoRelease bit = null,
 @AddToShipment bit = null,
 @MultipleOnShipment bit = null,
 @LIFO bit = null,
 @MinimumShelfLife numeric(13,3) = null,
 @AreaType nvarchar(20) = null,
 @AutoSendup bit = null,
 @CheckingLane int = null,
 @DespatchBay int = null,
 @PlanningComplete bit = null,
 @AutoInvoice bit = null,
 @Backorder bit = null,
 @AutoCheck bit = null,
 @ReserveBatch bit = null 
)

as
begin
	 set nocount on;
  
  if @OutboundDocumentTypeId = '-1'
    set @OutboundDocumentTypeId = null;
  
  if @OutboundDocumentTypeCode = '-1'
    set @OutboundDocumentTypeCode = null;
  
  if @OutboundDocumentType = '-1'
    set @OutboundDocumentType = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @CheckingLane = '-1'
    set @CheckingLane = null;
  
  if @DespatchBay = '-1'
    set @DespatchBay = null;
  
	 declare @Error int
 
  insert OutboundDocumentType
        (OutboundDocumentTypeCode,
         OutboundDocumentType,
         PriorityId,
         AlternatePallet,
         SubstitutePallet,
         AutoRelease,
         AddToShipment,
         MultipleOnShipment,
         LIFO,
         MinimumShelfLife,
         AreaType,
         AutoSendup,
         CheckingLane,
         DespatchBay,
         PlanningComplete,
         AutoInvoice,
         Backorder,
         AutoCheck,
         ReserveBatch)
  select @OutboundDocumentTypeCode,
         @OutboundDocumentType,
         @PriorityId,
         @AlternatePallet,
         @SubstitutePallet,
         @AutoRelease,
         @AddToShipment,
         @MultipleOnShipment,
         @LIFO,
         @MinimumShelfLife,
         @AreaType,
         @AutoSendup,
         @CheckingLane,
         @DespatchBay,
         @PlanningComplete,
         @AutoInvoice,
         @Backorder,
         @AutoCheck,
         @ReserveBatch 
  
  select @Error = @@Error, @OutboundDocumentTypeId = scope_identity()
  
  if @Error = 0
    exec @Error = p_OutboundDocumentTypeHistory_Insert
         @OutboundDocumentTypeId = @OutboundDocumentTypeId,
         @OutboundDocumentTypeCode = @OutboundDocumentTypeCode,
         @OutboundDocumentType = @OutboundDocumentType,
         @PriorityId = @PriorityId,
         @AlternatePallet = @AlternatePallet,
         @SubstitutePallet = @SubstitutePallet,
         @AutoRelease = @AutoRelease,
         @AddToShipment = @AddToShipment,
         @MultipleOnShipment = @MultipleOnShipment,
         @LIFO = @LIFO,
         @MinimumShelfLife = @MinimumShelfLife,
         @AreaType = @AreaType,
         @AutoSendup = @AutoSendup,
         @CheckingLane = @CheckingLane,
         @DespatchBay = @DespatchBay,
         @PlanningComplete = @PlanningComplete,
         @AutoInvoice = @AutoInvoice,
         @Backorder = @Backorder,
         @AutoCheck = @AutoCheck,
         @ReserveBatch = @ReserveBatch,
         @CommandType = 'Insert'
  
  return @Error
  
end
