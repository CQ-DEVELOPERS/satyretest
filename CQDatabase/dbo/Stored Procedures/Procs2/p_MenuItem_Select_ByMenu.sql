﻿/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_Select_ByMenu
  ///   Filename       : p_MenuItem_Select_ByMenu.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Jun 2007 13:05:10
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Menu table.
  /// </remarks>
  /// <param>
  ///   @MenuId int = null 
  /// </param>
  /// <returns>
  ///   m.MenuItemId,
  ///   m.MenuItem,
  ///   m.ToolTip,
  ///   m.NavigateTo,
  ///   m.ParentMenuId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_MenuItem_Select_ByMenu
(
 @MenuId     int = null,
 @OperatorId int = null,
 @SiteType   nvarchar(20) = null
)
as
begin
	 set nocount on
	 
	 declare @OperatorGroupId int,
	         @CultureId       int
	 
	 select @OperatorGroupId = OperatorGroupId,
	        @CultureId       = CultureId
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  declare @TableResult as table
  (
   MenuItemId       int,
   MenuItemText     nvarchar(50),
   ToolTip          nvarchar(255),
   NavigateTo       nvarchar(255),
   ParentMenuItemId int,
   ParentOrderBy    int,
   OrderBy          int
  )
  
  insert @TableResult
        (MenuItemId,
         MenuItemText,
         ToolTip,
         NavigateTo,
         ParentMenuItemId,
         OrderBy)
  select mi.MenuItemId,
         mic.MenuItem as 'MenuItemText',
         mic.ToolTip,
         mi.NavigateTo,
         mi.ParentMenuItemId,
         isnull(mi.OrderBy,2)
    from MenuItem             mi (nolock)
    join MenuItemCulture     mic (nolock) on mi.MenuItemId = mic.MenuItemId
                                         and mic.CultureId = isnull(@CultureId, 1)
    join OperatorGroupMenuItem o (nolock) on mi.MenuItemId = o.MenuItemId
                                         and o.OperatorGroupId = @OperatorGroupId
                                         and o.Access = 1
   where mi.MenuId = isnull(@MenuId, mi.MenuId)
  order by mi.ParentMenuItemId, isnull(mi.OrderBy,2), mi.MenuItemId
  
  if @@ROWCOUNT = 0 -- Show menu if has no access to main menu
    insert @TableResult
          (MenuItemId,
           MenuItemText,
           ToolTip,
           NavigateTo,
           ParentMenuItemId,
           OrderBy)
    select mi.MenuItemId,
           mic.MenuItem as 'MenuItemText',
           mic.ToolTip,
           mi.NavigateTo,
           mi.ParentMenuItemId,
           isnull(mi.OrderBy,2)
      from MenuItem             mi (nolock)
      join MenuItemCulture     mic (nolock) on mi.MenuItemId = mic.MenuItemId
                                           and mic.CultureId = isnull(@CultureId, 1)
      join OperatorGroupMenuItem o (nolock) on mi.MenuItemId = o.MenuItemId
                                           and o.OperatorGroupId = @OperatorGroupId
                                           and o.Access = 1
    order by mi.ParentMenuItemId, isnull(mi.OrderBy,2), mi.MenuItemId
  
  delete child
    from @TableResult child
    left
    join @TableResult parent on child.ParentMenuItemId = parent.MenuItemId
   where child.ParentMenuItemId is not null
     and parent.MenuItemId is null
  
  update tr
     set ParentOrderBy = mi.OrderBy
    from @TableResult tr
    join MenuItem     mi (nolock) on tr.ParentMenuItemId = mi.MenuId
  
  select MenuItemId,
         MenuItemText,
         ToolTip,
         NavigateTo,
         ParentMenuItemId,
         OrderBy
    from @TableResult
    order by ISNULL(ParentOrderBy, OrderBy), OrderBy
  --order by ISNULL(ParentMenuItemId, MenuItemId), ParentMenuItemId, OrderBy
  
  exec p_Operator_Active
   @OperatorId = @OperatorId,
   @SiteType   = @SiteType
end
