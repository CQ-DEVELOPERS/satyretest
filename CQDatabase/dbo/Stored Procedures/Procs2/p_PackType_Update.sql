﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackType_Update
  ///   Filename       : p_PackType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:40:53
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the PackType table.
  /// </remarks>
  /// <param>
  ///   @PackTypeId int = null,
  ///   @PackType varchar(30) = null,
  ///   @InboundSequence smallint = null,
  ///   @OutboundSequence smallint = null,
  ///   @PackTypeCode varchar(10) = null,
  ///   @Unit bit = null,
  ///   @Pallet bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackType_Update
(
 @PackTypeId int = null,
 @PackType varchar(30) = null,
 @InboundSequence smallint = null,
 @OutboundSequence smallint = null,
 @PackTypeCode varchar(10) = null,
 @Unit bit = null,
 @Pallet bit = null 
)

as
begin
	 set nocount on;
  
  if @PackTypeId = '-1'
    set @PackTypeId = null;
  
  if @PackType = '-1'
    set @PackType = null;
  
  if @PackTypeCode = '-1'
    set @PackTypeCode = null;
  
	 declare @Error int
 
  update PackType
     set PackType = isnull(@PackType, PackType),
         InboundSequence = isnull(@InboundSequence, InboundSequence),
         OutboundSequence = isnull(@OutboundSequence, OutboundSequence),
         PackTypeCode = isnull(@PackTypeCode, PackTypeCode),
         Unit = isnull(@Unit, Unit),
         Pallet = isnull(@Pallet, Pallet) 
   where PackTypeId = @PackTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
