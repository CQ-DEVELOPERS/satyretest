﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_Type
  ///   Filename       : p_Location_Search_Type.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_Type
(
 @LocationTypeCode nvarchar(10)
)

as
begin
	 set nocount on;
  
  select LocationId,
         Location
    from Location l
    join LocationType lt on l.LocationTypeId = lt.LocationTypeId
   where LocationTypeCode = @LocationTypeCode
end
