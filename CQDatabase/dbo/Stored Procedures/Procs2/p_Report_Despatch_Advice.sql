﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Despatch_Advice
  ///   Filename       : p_Report_Despatch_Advice.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Despatch_Advice
(
 @ConnectionString   nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @OutboundShipmentId int,
 @IssueId            int
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId	int,
   IssueId				int,
   OutboundDocumentId	int,
   OrderNumber			nvarchar(30),
   ExternalCompanyId	int,
   ExternalCompany		nvarchar(255),
   ExternalCompanyCode	nvarchar(30),
   DespatchDate			datetime,
   LocationId			int,
   Location				nvarchar(15),
   JobId				int,
   StatusId				int,
   Status				nvarchar(50),
   InstructionTypeId	int,
   InstructionTypeCode	nvarchar(10),
   InstructionType		nvarchar(50),
   InstructionId		int,
   Pallet				nvarchar(10),
   StorageUnitBatchId	int,
   ProductCode			nvarchar(30),
   Quantity				float,
   ConfirmedQuantity	float,
   ShortQuantity		float,
   QuantityOnHand		float,
   DropSequence			int,
   RouteId				int,
   Route				nvarchar(50),
   Weight				float,
   WarehouseId			int,
   PalletId				int
  )
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
	 
	 if @OutboundShipmentId is not null
	   set @IssueId = null
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         OutboundDocumentId,
         JobId,
         StatusId,
         InstructionTypeId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         WarehouseId,
         DespatchDate)
  select ili.OutboundShipmentId,
         ili.IssueId,
         ili.OutboundDocumentId,
         ins.JobId,
         ins.StatusId,
         ins.InstructionTypeId,
         ins.InstructionId,
         ins.StorageUnitBatchId,
         ins.Quantity,
         isnull(ili.ConfirmedQuantity, 0),
         ili.Quantity - isnull(ili.ConfirmedQuantity, 0),
         ins.WarehouseId,
         i.DeliveryDate
    from IssueLineInstruction  ili (nolock)
    join Instruction           ins (nolock) on ili.InstructionId    = isnull(ins.InstructionRefId, ins.InstructionId)
    join Issue					 i (nolock) on ili.IssueId = i.IssueId
   where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))
     and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))
  
	update tr
	   set ProductCode = vs.ProductCode
	  from @TableResult     tr
	  join viewStock        vs    on tr.StorageUnitBatchId = vs.StorageUnitBatchId
	  
	update tr
	   set PalletId = (select max(pl.PalletId)
                          from StorageUnitBatchLocation subl (nolock)
						                    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
						                    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
						                    join Product                     p (nolock) on su.ProductId            = p.ProductId
						                    left join Pallet				pl (nolock) on pl.StorageUnitBatchId   = subl.StorageUnitBatchId
			where p.ProductCode = tr.ProductCode)
	  from @TableResult     tr
  
  update tr
     --set DespatchDate = os.ShipmentDate,
     set RouteId      = os.RouteId,
         DropSequence = osi.DropSequence,
         LocationId   = os.LocationId
    from @TableResult     tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId
   where tr.OutboundShipmentId is not null
  
  update tr
     set DespatchDate = i.DeliveryDate,
         RouteId      = i.RouteId,
         DropSequence = i.DropSequence,
         LocationId   = i.LocationId
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
   where tr.OutboundShipmentId is null
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set ExternalCompany		= ec.ExternalCompany,
		 ExternalCompanyCode	= ec.ExternalCompanyCode		
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set Location = l.Location
    from @TableResult    tr
    join Location         l (nolock) on tr.LocationId = l.LocationId
    
  update tr
     set Pallet = convert(nvarchar(10), j.DropSequence)
               + ' of '
               + convert(nvarchar(10), isnull(j.Pallets,tr.PalletId))
    from @TableResult tr
    join Job           j (nolock) on tr.JobId = j.JobId
  
  update tr
     set Weight = tr.Quantity * p.Weight
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join Pack               p (nolock) on sub.StorageUnitId     = p.StorageUnitId
   where p.Quantity = 1
  
  update tr
     set InstructionType     = 'Full',
         InstructionTypeCode = it.InstructionTypeCode
    from @TableResult tr
    join InstructionType it on tr.InstructionTypeId = it.InstructionTypeId
   where it.InstructionTypeCode = 'P'
  
  update @TableResult
     set InstructionType     = 'Mixed'
   where InstructionType is null
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s on tr.StatusId = s.StatusId
  where tr.InstructionTypeCode = 'P'
    and s.StatusCode           = 'NS'
  
  select OutboundShipmentId,
         DropSequence,
         Route,
         DespatchDate,
         ExternalCompany,
         ExternalCompanyCode,
         Location,
         min(InstructionType) as 'InstructionType',
         JobId,
         Status,
         OrderNumber,
         Pallet,
         sum(Weight) as 'Weight',
         (select Indicator from Configuration where ConfigurationId = 417 and WarehouseId = tr.WarehouseId) as 'PrtCustomer'
    from @TableResult tr
  group by OutboundShipmentId,
         DropSequence,
         Route,
         DespatchDate,
         ExternalCompany,
         ExternalCompanyCode,
         Location,
         JobId,
         Status,
         OrderNumber,
         Pallet,
         tr.WarehouseId
  order by OutboundShipmentId,
           Pallet,
           JobId
end
