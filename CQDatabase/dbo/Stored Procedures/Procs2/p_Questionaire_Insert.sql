﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questionaire_Insert
  ///   Filename       : p_Questionaire_Insert.sql
  ///   Create By      : Willis	
  ///   Date Created   : 18 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Questionaire_Insert
(
	@WarehouseId		int,
	@QuestionaireType				nvarchar(50),
	@QuestionaireDesc				nvarchar(255),
	@QuestionaireId int = null output 
)

as
begin
	 set nocount on;
  
If not exists(Select * from Questionaire where QuestionaireType = @Questionairetype and QuestionaireDesc = @Questionairedesc and Warehouseid = @Warehouseid) 
Begin

Insert into QuestionAire (Warehouseid,QuestionaireType,QuestionaireDesc)
select @WarehouseId,@QuestionaireType,@QuestionaireDesc
   Select @QuestionaireId = Max(QuestionaireId)  from QuestionAire
end  
end
