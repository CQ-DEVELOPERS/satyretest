﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Reasons
  ///   Filename       : p_Mobile_Reasons.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Reasons
(
 @Type int
)

as
begin
	 set nocount on;
  
  declare @Error    int,
          @Rowcount int
  
  declare @TableResult as table
  (
   ReasonId int,
   Reason   nvarchar(50)
  )
  
  if @Type = 0
    insert @TableResult
          (ReasonId,
           Reason)
    select top 10
           ReasonId,
           Reason
      from Reason
  else if @Type = 1
    insert @TableResult
          (ReasonId,
           Reason)
    select top 10
           ReasonId,
           Reason
      from Reason
  
  select @Error = @@Error,
         @Rowcount = @@Rowcount
  
  if @Error <> 0
  begin
    set @Error = 1;
    goto Result;
  end
  
  if @Rowcount = 0
  begin
    set @Error = 2
    
    insert @TableResult
          (ReasonId,
           Reason)
    select null,
           null
    
    goto Result;
  end
  
  Result:
  select @Error as 'Result',
         ReasonId,
         Reason
    from @TableResult
end
