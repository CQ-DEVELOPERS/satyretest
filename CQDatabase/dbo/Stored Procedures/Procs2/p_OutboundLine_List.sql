﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundLine_List
  ///   Filename       : p_OutboundLine_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jan 2014 07:56:01
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundLine table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OutboundLine.OutboundLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundLine_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as OutboundLineId
        ,null as 'OutboundLine'
  union
  select
         OutboundLine.OutboundLineId
        ,OutboundLine.OutboundLineId as 'OutboundLine'
    from OutboundLine
  
end
