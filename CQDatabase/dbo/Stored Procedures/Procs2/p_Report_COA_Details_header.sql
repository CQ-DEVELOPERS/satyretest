﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_COA_Details_header
  ///   Filename       : p_Report_COA_Details_header.sql
  ///   Create By      : Karen
  ///   Date Created   : October 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_COA_Details_header
(
 @COAId       int
)

as
begin
	 set nocount on;
	 
  select	p.Product,
			p.ProductCode,
			b.Batch,
			b.ProductionDateTime,
			b.ExpiryDate,
			coa.COAId
    from	COA coa
    join	storageunitbatch sub (nolock) on coa.StorageUnitBatchId = sub.StorageUnitBatchId
    join	batch			   b (nolock) on b.BatchId = sub.BatchId
    join	storageunit		  su (nolock) on su.StorageUnitId = sub.StorageUnitId
    join	product			   p (nolock) on p.productid = su.productid
   where	coa.COAId = @COAId

end
