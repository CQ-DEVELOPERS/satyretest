﻿	
/*
  /// <summary>
  ///   Procedure Name : p_Report_Deliveries_Summary
  ///   Filename       : p_Report_Deliveries_Summary.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Nov 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Deliveries_Summary

(
	@WarehouseId		int,
	@FromDate			Datetime,
	@ToDate				DateTime,
	@Supplier			nvarchar(50),
	@Status				nvarchar(50),
	@DocumentType		nvarchar(50),
	@OrderNumber		nvarchar(50)
)

	

as
begin
	 set nocount on;
	
	If (@Status = 'All' or @Status = '-1' or len(@Status) = 0) 
	  Begin
			Set @Status = null
	  End
	If (@OrderNumber = 'All' or @OrderNumber = '-1' or len(@OrderNumber) = 0) 
	  Begin
			Set @OrderNumber = null
	  End
	
	If (@Status = 'All' or @Status = '-1' or len(@Status) = 0) 
	  Begin
			Set @Status = null
	  End

	If (@Supplier = 'All' or @Supplier = '-1' or len(@Supplier) = 0) 
	  Begin
			Set @Supplier = null
	  End

--	if (@FromDate = '-1')
--	 Begin
--		Set @FromDate = null
--	 End
--  
--	if (@ToDate = '-1')
--	 Begin
--		Set @ToDate = null
--	 End
  
	Select	i.DeliveryDate,
			i.OrderNumber,
			e.ExternalCompanyCode as SupplierCode,
			e.ExternalCompany as Supplier,
			--rls.status,
			Count(l.InboundLineId) as Lines,
			sum(case when rls.status ='waiting' then 0 else  1 end ) As LinesReceived,
			sum(case when rls.status ='waiting' then 1 else  0 end ) As Outstanding
			
	from InboundDocument i
		Left Join Receipt r on r.InboundDocumentId = i.InboundDocumentId
		Left Join ReceiptLine rl on rl.ReceiptId = r.ReceiptId 
		Join InboundDocumentType t on t.InboundDocumentTypeId = i.InboundDocumentTypeId
		Join ExternalCompany e on e.ExternalCompanyId = i.ExternalCompanyId
		Join InboundLine l on l.InboundDocumentId = i.InboundDocumentId
		Join Status s on s.StatusId = l.StatusId 
		left Join Status rls on rls.statusid = rl.statusid 
	
  where i.WarehouseId = Isnull(@WarehouseId,i.WarehouseId) and
		t.InboundDocumentTypeCode = 'RCP' and
		i.OrderNumber Like '%' + Isnull(@OrderNumber,'') + '%' and
		i.CreateDate >= Isnull(@FromDate,i.CreateDate) and
		i.CreateDate <= Isnull(@ToDate,i.CreateDate) and
		i.ExternalCompanyID = Isnull(@Supplier,i.ExternalCompanyID ) and
		s.Status = isnull(@Status,s.Status)
	Group by 
			i.DeliveryDate,
			i.OrderNumber,
			e.ExternalCompanyCode ,
			e.ExternalCompany ,
			rls.status

end
