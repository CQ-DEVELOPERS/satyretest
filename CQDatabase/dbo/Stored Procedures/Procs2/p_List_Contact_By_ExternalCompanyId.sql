﻿/*  
  /// <summary>  
  ///   Procedure Name : p_List_Contact_By_ExternalCompanyId  
  ///   Filename       : p_List_Contact_By_ExternalCompanyId.sql  
  ///   Create By      : Karen  
  ///   Date Created   : May 2013  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_List_Contact_By_ExternalCompanyId  
(  
 @ExternalCompanyId int  
)  
  
  
as  
begin  
  set nocount on;  
    
  --select -1 as ContactListId,  
  -- 'None' as ContactPerson,  
  -- null as Telephone,  
  -- null as Fax,  
  -- null as EMail,  
  -- -1 as ReportTypeId  
  --union  
  select ContactListId,  
   ContactPerson,  
   Telephone,  
   Fax,  
   EMail,  
   cl.ReportTypeId,
   rt.ReportType  
    from ContactList cl (nolock)   
    join ReportType rt (nolock) on cl.ReportTypeId = rt.ReportTypeId
    where cl.ExternalCompanyId = @ExternalCompanyId  
end  
