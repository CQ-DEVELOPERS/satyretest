﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Select
  ///   Filename       : p_OutboundDocument_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 07:31:32
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundDocument table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null 
  /// </param>
  /// <returns>
  ///   OutboundDocument.OutboundDocumentId,
  ///   OutboundDocument.OutboundDocumentTypeId,
  ///   OutboundDocument.ExternalCompanyId,
  ///   OutboundDocument.StatusId,
  ///   OutboundDocument.WarehouseId,
  ///   OutboundDocument.OrderNumber,
  ///   OutboundDocument.DeliveryDate,
  ///   OutboundDocument.CreateDate,
  ///   OutboundDocument.ModifiedDate,
  ///   OutboundDocument.ReferenceNumber,
  ///   OutboundDocument.FromLocation,
  ///   OutboundDocument.ToLocation,
  ///   OutboundDocument.StorageUnitId,
  ///   OutboundDocument.BatchId,
  ///   OutboundDocument.Quantity,
  ///   OutboundDocument.PrincipalId,
  ///   OutboundDocument.OperatorId,
  ///   OutboundDocument.ExternalOrderNumber,
  ///   OutboundDocument.DirectDeliveryCustomerId,
  ///   OutboundDocument.EmployeeCode,
  ///   OutboundDocument.Collector,
  ///   OutboundDocument.AdditionalText1,
  ///   OutboundDocument.VendorCode,
  ///   OutboundDocument.CustomerOrderNumber,
  ///   OutboundDocument.AdditionalText2,
  ///   OutboundDocument.BOE,
  ///   OutboundDocument.Address1 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Select
(
 @OutboundDocumentId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         OutboundDocument.OutboundDocumentId
        ,OutboundDocument.OutboundDocumentTypeId
        ,OutboundDocument.ExternalCompanyId
        ,OutboundDocument.StatusId
        ,OutboundDocument.WarehouseId
        ,OutboundDocument.OrderNumber
        ,OutboundDocument.DeliveryDate
        ,OutboundDocument.CreateDate
        ,OutboundDocument.ModifiedDate
        ,OutboundDocument.ReferenceNumber
        ,OutboundDocument.FromLocation
        ,OutboundDocument.ToLocation
        ,OutboundDocument.StorageUnitId
        ,OutboundDocument.BatchId
        ,OutboundDocument.Quantity
        ,OutboundDocument.PrincipalId
        ,OutboundDocument.OperatorId
        ,OutboundDocument.ExternalOrderNumber
        ,OutboundDocument.DirectDeliveryCustomerId
        ,OutboundDocument.EmployeeCode
        ,OutboundDocument.Collector
        ,OutboundDocument.AdditionalText1
        ,OutboundDocument.VendorCode
        ,OutboundDocument.CustomerOrderNumber
        ,OutboundDocument.AdditionalText2
        ,OutboundDocument.BOE
        ,OutboundDocument.Address1
    from OutboundDocument
   where isnull(OutboundDocument.OutboundDocumentId,'0')  = isnull(@OutboundDocumentId, isnull(OutboundDocument.OutboundDocumentId,'0'))
  order by OrderNumber
  
end
