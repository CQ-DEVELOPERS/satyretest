﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_IsActive
  ///   Filename       : p_Operator_IsActive.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Aug 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_IsActive
(
 @OperatorId int
)

as
begin
  set nocount on;
  
  declare @Active            bit = 0
  
  select @Active = isnull(ActiveIndicator,0)
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  select @Active = 0
    from Operator (nolock)
   where OperatorId = @OperatorId
     and ExpiryDate <= dbo.ufn_Getdate();
  
  if @Active = 0
    RAISERROR ('Operator not active',11,1)
end
