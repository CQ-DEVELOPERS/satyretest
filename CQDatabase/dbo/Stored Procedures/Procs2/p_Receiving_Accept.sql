﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Accept
  ///   Filename       : p_Receiving_Accept.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Accept
(
 @jobId      int,
 @operatorId int
)

as
begin
	 set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         JobId,
         OperatorId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @JobId,
         @OperatorId,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @InstructionId       int,
          @StatusId            int,
          @InstructionTypeCode nvarchar(10),
          @StoreLocationId     int,
          @WarehouseId         int,
          @StatusCode          nvarchar(10),
          @ProductCategory     nvarchar(10),
          @ReceiptLineId       int,
          @PickLocationId      int,
          @Exception           nvarchar(255),
          @Operator            nvarchar(50)
  
  select @Operator = Operator
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  set @Errormsg = 'Error executing p_Receiving_Accept'
  
  select @GetDate = dbo.ufn_Getdate()
  
  select top 1 @InstructionId       = i.InstructionId,
               @InstructionTypeCode = it.InstructionTypeCode,
               @StoreLocationId     = i.StoreLocationId,
               @WarehouseId         = i.WarehouseId,
               @StatusCode          = s.StatusCode,
               @ProductCategory     = su.ProductCategory,
               @ReceiptLineId       = i.ReceiptLineId
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Job                j (nolock) on i.JobId              = j.JobId
    join Status             s (nolock) on j.StatusId           = s.StatusId
   where j.JobId = @JobId
  order by i.InstructionId
  
  begin transaction
  
  if dbo.ufn_Configuration(145,@WarehouseId) = 1 or (dbo.ufn_Configuration(224,@WarehouseId) = 1 and @ProductCategory = 'W')
  begin
    if @StoreLocationId is null
    begin
      exec @Error = p_Receiving_Manual_Palletisation_Auto_Locations
       @InstructionId = @InstructionId
      
      if @Error <> 0
        goto error
    end
  end
  
  if dbo.ufn_Configuration(224,@WarehouseId) = 1 and @ProductCategory = 'W'
  begin
    select top 1 @StoreLocationId = StoreLocationId
      from Instruction (nolock)
     where InstructionId = @InstructionId
    
    if @StoreLocationId is not null
    begin
      exec @Error = p_Instruction_Started
       @InstructionId = @InstructionId,
       @operatorId    = @OperatorId
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Instruction_Finished
       @InstructionId = @InstructionId,
       @operatorId    = @OperatorId
      
      if @Error <> 0
        goto error
    end
  end
  
  if @InstructionTypeCode in ('S','SM')
  begin
    select @StatusId = dbo.ufn_StatusId('R','A')
    
    exec @Error = p_Job_Update
     @JobId          = @JobId,
     @StatusId       = @StatusId,
     @CheckedBy      = @operatorId,
     @CheckedDate    = @GetDate
    
    if @Error <> 0
      goto error
  end
  else if @InstructionTypeCode = 'PR'
  begin
    select @StatusId = dbo.ufn_StatusId('PR','A')
    
    exec @Error = p_Job_Update
     @JobId          = @JobId,
     @StatusId       = @StatusId,
     @CheckedBy      = @operatorId,
     @CheckedDate    = @GetDate
    
    if @Error <> 0
      goto error
    
    if (select dbo.ufn_Configuration(197, @WarehouseId)) = 1 and @StatusCode = 'PR'
    begin
      exec @Error = p_Production_Adjustment
       @InstructionId = @InstructionId,
       @Sign          = '+-'
      
      if @Error <> 0
        goto error
    end
    
    if (select dbo.ufn_Configuration(285, @WarehouseId)) = 1 and @StatusCode = 'PR'
    begin
      exec @Error = p_Production_Update_Accepted_Quantity
       @InstructionId = @InstructionId,
       @Sign          = '+'
      
      if @Error <> 0
        goto error
    end
  end
  
  if dbo.ufn_Configuration(251,@WarehouseId) = 1
  begin
    select @PickLocationId = r.LocationId
      from ReceiptLine rl (nolock)
      join Receipt      r (nolock) on rl.ReceiptId = r.ReceiptId
     where rl.ReceiptLineId = @ReceiptLineId
    
    if (@PickLocationId is null)
      select @PickLocationId = LocationId
        from Area          a (nolock)
        join AreaLocation al (nolock) on a.AreaId = al.AreaId
       where a.AreaCode = 'R'
    
    exec @Error = p_Instruction_Update
     @InstructionId  = @InstructionId,
     @PickLocationId = @PickLocationId
    
    if @Error <> 0
      goto error
    
    update subl
       set ActualQuantity = subl.ActualQuantity + isnull(i.ConfirmedQuantity,0)
      from Instruction                 i
      join StorageUnitBatchLocation subl on subl.StorageUnitBatchId  = i.StorageUnitBatchId
				                                    and subl.LocationId          = i.PickLocationId
     where i.InstructionId     = @InstructionId
       and i.ConfirmedQuantity > 0
       and i.Picked          is null
    
    if @@rowcount = 0
    begin
      insert StorageUnitBatchLocation
            (StorageUnitBatchId,
             LocationId,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
      select StorageUnitBatchId,
             PickLocationId,
             ConfirmedQuantity,
             0,
             0
        from Instruction
       where InstructionId = @InstructionId
         and PickLocationId is not null
         and ConfirmedQuantity > 0
         and Picked is null
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @InstructionId,
     @Pick          = 1,-- (0 = false, 1 = true)
     @Store         = 0,-- (0 = false, 1 = true)
     @Confirmed     = 1 -- (0 = false, 1 = true)
    
    if @Error <> 0
      goto error
  end
  
  select @Exception = 'Pallet Accepted by ' + isnull(@Operator, '')  + '.'
  
  exec @Error = p_Exception_Insert
   @ExceptionId   = null,
   @ExceptionCode = 'PALACCEPTED',
   @Exception     = @Exception,
   @InstructionId = @InstructionId,
   @OperatorId    = @OperatorId,
   @JobId         = @JobId,
   @CreateDate    = @GetDate,
   @ExceptionDate = @Getdate
 
  if @Error <> 0
    goto error
    
  update MobileLog
     set EndDate = getdate(),
         ErrorMsg = 'Successful'
   where MobileLogId = @MobileLogId
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    
    update MobileLog
       set EndDate = getdate(),
           ErrorMsg = @Errormsg
     where MobileLogId = @MobileLogId
    
    return @Error
end
