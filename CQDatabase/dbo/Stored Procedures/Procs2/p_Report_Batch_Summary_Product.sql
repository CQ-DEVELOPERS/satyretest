﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Batch_Summary_Product
  ///   Filename       : p_Report_Batch_Summary_Product.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Report_Batch_Summary_Product
(
	
 @WarehouseId       int,
 @BatchId			nvarchar(50) = null,
-- @BatchReferenceNumber nvarchar(50),
 @FromDate			DateTime = null,
 @ToDate			DateTime = null,
 @ManufactureArea	nvarchar(100) = null,
 @ProductCode		nvarchar(30) = null,
 @Product			nvarchar(50) = null
)

as
begin
	 set nocount on;
	 
	 
  
	if @BatchId = '-1' or len(@BatchId) = 0  set @BatchId = null
	--if @BatchReferenceNumber = -1 set @BatchReferenceNumber = null
	if @ManufactureArea = '-1' set @ManufactureArea = null
	if @ProductCode = '-1' set @ProductCode = null
	if @Product = '-1' set @Product = null


Select b.Batch,
		p.ProductCode,
		p.Product,
		sku.SKUCode,
		b.BatchReferenceNumber,
		1 as ProductionLine,
		ProductionDateTime,
		b.ExpiryDate,
		b.ShelfLifeDays,
		s.Status as BatchStatus,
		o.OPerator as StatusModifiedBy,
		StatusModifiedDate,
		ExpectedYield,
		ActualYield,
		FilledYield,
		CreateDate,
		getdate() as IrradiationDate,
		p.ProductCode as ProductCode2,
		p.Product as Product2,
		'200' as Qty_Filled

From Batch b
		Join StorageUnitBatch sub on sub.BatchId = b.BatchId
		JOin StorageUnit su on su.StorageUnitId = sub.StorageUnitId
		Join Product p on p.ProductId = su.ProductId
		Join Status s on s.StatusId = b.StatusId
		Join Operator o on o.OperatorId = b.StatusModifiedBy
		Join SKU sku on sku.SkuId = su.SkuId
where --b.Batch like '%' +  Isnull(@BatchNumber,b.Batch) + '%' and
		b.BatchId = Isnull(@BatchId,b.BatchId) and 
	  --b.BatchReferenceNumber like '%' +  Isnull(@BatchReferenceNumber,b.BatchReferenceNumber) + '%' and
	  CreateDate between Isnull(@FromDate,CreateDate) and Isnull(@ToDate,CreateDate) and
	  p.ProductCode Like '%' + Isnull(@ProductCode,p.ProductCode) + '%' and
	  p.Product Like '%' + Isnull(@Product,p.Product) + '%'

end
