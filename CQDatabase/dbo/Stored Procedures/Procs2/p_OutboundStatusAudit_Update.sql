﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundStatusAudit_Update
  ///   Filename       : p_OutboundStatusAudit_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:37
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OutboundStatusAudit table.
  /// </remarks>
  /// <param>
  ///   @IssueId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @CreateDate datetime = null,
  ///   @PlanningComplete datetime = null,
  ///   @Checking datetime = null,
  ///   @Checked datetime = null,
  ///   @Despatch datetime = null,
  ///   @DespatchChecked datetime = null,
  ///   @Complete datetime = null,
  ///   @PickComplete int = null,
  ///   @PickTotal int = null,
  ///   @PickPercentage numeric(13,3) = null,
  ///   @CheckingComplete int = null,
  ///   @CheckingTotal int = null,
  ///   @CheckingPercentage numeric(13,3) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundStatusAudit_Update
(
 @IssueId int = null,
 @ExternalCompanyId int = null,
 @CreateDate datetime = null,
 @PlanningComplete datetime = null,
 @Checking datetime = null,
 @Checked datetime = null,
 @Despatch datetime = null,
 @DespatchChecked datetime = null,
 @Complete datetime = null,
 @PickComplete int = null,
 @PickTotal int = null,
 @PickPercentage numeric(13,3) = null,
 @CheckingComplete int = null,
 @CheckingTotal int = null,
 @CheckingPercentage numeric(13,3) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update OutboundStatusAudit
     set IssueId = isnull(@IssueId, IssueId),
         ExternalCompanyId = isnull(@ExternalCompanyId, ExternalCompanyId),
         CreateDate = isnull(@CreateDate, CreateDate),
         PlanningComplete = isnull(@PlanningComplete, PlanningComplete),
         Checking = isnull(@Checking, Checking),
         Checked = isnull(@Checked, Checked),
         Despatch = isnull(@Despatch, Despatch),
         DespatchChecked = isnull(@DespatchChecked, DespatchChecked),
         Complete = isnull(@Complete, Complete),
         PickComplete = isnull(@PickComplete, PickComplete),
         PickTotal = isnull(@PickTotal, PickTotal),
         PickPercentage = isnull(@PickPercentage, PickPercentage),
         CheckingComplete = isnull(@CheckingComplete, CheckingComplete),
         CheckingTotal = isnull(@CheckingTotal, CheckingTotal),
         CheckingPercentage = isnull(@CheckingPercentage, CheckingPercentage) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
