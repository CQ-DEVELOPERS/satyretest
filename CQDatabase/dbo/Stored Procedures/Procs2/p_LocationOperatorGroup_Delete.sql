﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationOperatorGroup_Delete
  ///   Filename       : p_LocationOperatorGroup_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Jul 2013 10:59:00
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the LocationOperatorGroup table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationOperatorGroup_Delete
(
 @OperatorGroupId int = null,
 @Locationid int = null
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete LocationOperatorGroup
   where LocationId = @Locationid
     and OperatorGroupId = @OperatorGroupId
  
  select @Error = @@Error
  
  
  return @Error
  
end
