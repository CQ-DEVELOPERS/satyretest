﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletId_Search_InstructionId
  ///   Filename       : p_PalletId_Search_InstructionId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletId_Search_InstructionId
(
 @InstructionId int
)

as
begin
	 set nocount on;
	 
  select 'P:' + convert(nvarchar(10), p.PalletId) as 'PalletId'
    from Instruction                 i (nolock)
    join StorageUnitBatch          ins (nolock) on i.StorageUnitBatchId = ins.StorageUnitBatchId
    join StorageUnitBatch          pal (nolock) on ins.StorageUnitId = pal.StorageUnitId
    join Pallet                      p (nolock) on pal.StorageUnitBatchId = p.StorageUnitBatchId
                                               and i.PickLocationId = p.LocationId
    join StorageUnitBatchLocation subl (nolock) on p.StorageUnitBatchId = subl.StorageUnitBatchId
                                               and i.PickLocationId = subl.LocationId
                                               and subl.ActualQuantity >= i.Quantity
   where i.InstructionId = @InstructionId
     and p.StatusId is null
	 union
  select 'P:' + isnull(convert(nvarchar(10), PalletId),'') as 'PalletId'
    from Instruction (nolock)
   where InstructionId = @InstructionId
end
