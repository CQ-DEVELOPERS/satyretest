﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundPerformance_List
  ///   Filename       : p_OutboundPerformance_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:36
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OutboundPerformance table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OutboundPerformance.JobId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundPerformance_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as JobId
        ,null as 'OutboundPerformance'
  union
  select
         OutboundPerformance.JobId
        ,OutboundPerformance.JobId as 'OutboundPerformance'
    from OutboundPerformance
  
end
