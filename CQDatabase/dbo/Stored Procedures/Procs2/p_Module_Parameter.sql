﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Module_Parameter
  ///   Filename       : p_Module_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:48
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Module table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Module.ModuleId,
  ///   Module.Module 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Module_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as ModuleId
        ,'{All}' as Module
  union
  select
         Module.ModuleId
        ,Module.Module
    from Module
  
end
