﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RawPics_Update
  ///   Filename       : p_RawPics_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:17
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the RawPics table.
  /// </remarks>
  /// <param>
  ///   @Data nvarchar(max) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RawPics_Update
(
 @Data nvarchar(max) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update RawPics
     set Data = isnull(@Data, Data) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
