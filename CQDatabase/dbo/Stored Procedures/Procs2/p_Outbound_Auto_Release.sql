﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Auto_Release
  ///   Filename       : p_Outbound_Auto_Release.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Auto_Release
(
 @OutboundDocumentId int = null,
 @OutboundShipmentId int = null,
 @IssueId            int = null,
 @Remarks            nvarchar(255) = null,
 @AutoRelease        bit = null, -- Override OutboundDocumentType
 @PlanningComplete   bit = null  -- Override OutboundDocumentType
)

as
begin
  set nocount on;
  
  declare @Error                  int,
          @Errormsg               nvarchar(500),
          @GetDate                datetime,
          @WarehouseId            int,
          @OutboundDocumentTypeId int,
          @Transaction            bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentId is not null
    select @OutboundShipmentId = osi.OutboundShipmentId,
           @IssueId            = i.IssueId
      from OutboundDocument       od (nolock)
      join Issue                   i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
      left
      join OutboundShipmentIssue osi (nolock) on i.IssueId             = osi.IssueId
   where od.OutboundDocumentId = @OutboundDocumentId
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @OutboundShipmentId is not null
  begin
    select @WarehouseId = WarehouseId
      from OutboundShipment (nolock)
     where OutboundShipmentId = @OutboundShipmentId
    
    select @OutboundDocumentTypeId = OutboundDocumentTypeId
      from OutboundShipmentIssue osi
      join Issue                   i (nolock) on osi.IssueId = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where osi.OutboundShipmentId = @OutboundShipmentId
  end
  else if @IssueId is not null
  begin
    select @WarehouseId            = od.WarehouseId,
           @OutboundDocumentTypeId = od.OutboundDocumentTypeId
      from Issue             i (nolock)
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where i.IssueId = @IssueId
  end
  
  select @OutboundDocumentTypeId = OutboundDocumentTypeId,
         @AutoRelease            = isnull(@AutoRelease, AutoRelease),
         @PlanningComplete       = isnull(@PlanningComplete, PlanningComplete)
    from OutboundDocumentType (nolock)
   where OutboundDocumentTypeId =  @OutboundDocumentTypeId
  
  if @@trancount = 0
  begin
    begin transaction
    set @Transaction = 1
  end
  
  if (@AutoRelease = 1 or @PlanningComplete = 1) and isnull(@Remarks,'') not like '56%' or @OutboundDocumentId is not null
  begin
    if @OutboundShipmentId is not null
      set @IssueId = null
    
    exec @Error = p_Outbound_Palletise
     @WarehouseId        = @WarehouseId,
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId,
     @OperatorId         = null,
     @Weight             = null,
     @Volume             = null
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Outbound_Palletise'
      goto error
    end
    
    exec @Error = p_Despatch_Manual_Palletisation_Auto_Locations
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Despatch_Manual_Palletisation_Auto_Locations'
      goto error
    end
    
    --exec @Error = p_Planning_Complete
     --@OutboundShipmentId = @OutboundShipmentId,
     --@IssueId            = @IssueId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Planning_Complete'
      goto error
    end
    
    if @AutoRelease = 1
    begin
      exec @Error = p_Outbound_Release
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Outbound_Release'
        goto error
      end
    end
  end
  
  if @Transaction = 1
    commit transaction
  return 0
  
  error:
    if @Transaction = 1
    begin
      RAISERROR (900000,-1,-1, 'Error executing p_Outbound_Auto_Release'); 
      rollback transaction
    end
    
    return @Error
end
 
