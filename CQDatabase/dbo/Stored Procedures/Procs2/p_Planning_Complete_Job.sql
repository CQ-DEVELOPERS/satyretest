﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Complete_Job
  ///   Filename       : p_Planning_Complete_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Complete_Job
(
 @jobId int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Planning_Complete_Job'
  
  select @StatusId = dbo.ufn_StatusId('IS','PC')
  
  begin transaction
  
  update j
     set StatusId = @StatusId
    from Job               j
    join Status            s (nolock) on j.StatusId = s.StatusId
   where s.StatusCode  in ('W','P','SA')
     and j.JobId = @JobId

  select @Error = @@Error

  if @Error <> 0
    goto error

  insert JobHistory
        (JobId,
         StatusId,
         CommandType,
         InsertDate)
  select distinct
         j.JobId,
         @StatusId,
         'Update',
         @GetDate
    from Job               j
    join Status            s (nolock) on j.StatusId = s.StatusId
   where s.StatusCode  in ('W','P','SA')
     and j.JobId = @JobId

  select @Error = @@Error

  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
