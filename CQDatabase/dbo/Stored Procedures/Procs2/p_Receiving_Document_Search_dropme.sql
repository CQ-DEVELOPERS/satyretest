﻿/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Document_Search_dropme
  ///   Filename       : p_Receiving_Document_Search_dropme.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Document_Search_dropme
(
 @WarehouseId           int,
 @InboundDocumentTypeId int,
 @InboundShipmentId     int,
 @ExternalCompanyCode   nvarchar(30),
 @ExternalCompany       nvarchar(255),
 @OrderNumber           nvarchar(30),
 @FromDate              datetime,
 @ToDate                datetime,
 @PrincipalId           int = null,
 @OperatorId			int = null
)

as
begin
  set nocount on;
  declare @TableResult as
  table(
        Original                  bit,
        InboundDocumentId         int,
        ReceiptId                 int,
        OrderNumber               nvarchar(60),
        ReferenceNumber           nvarchar(30),
        InboundShipmentId         int,
        SupplierCode              nvarchar(30),
        Supplier                  nvarchar(255),
        NumberOfLines             int,
        DeliveryDate              datetime,
        PlannedDeliveryDate       datetime,
        CreateDate                datetime,
        StatusId                  int,
        StatusCode                nvarchar(10),
        Status                    nvarchar(50),
        InboundDocumentType       nvarchar(30),
        InboundDocumentTypeCode   nvarchar(10),
        LocationId                int,
        Location                  nvarchar(15),
        Rating                    int,
        Delivery                  int,
        DeliveryNoteNumber        nvarchar(30),
        SealNumber                nvarchar(30),
        PriorityID                int,
        Priority                  nvarchar(50),
        VehicleRegistration       nvarchar(10),
        Remarks                   nvarchar(500),
        AllowPalletise            bit,
        PrincipalId               int,
        PrincipalCode             nvarchar(30),
        ShippingAgentId			        int,
        ShippingAgent			          nvarchar(255),
        ContainerNumber			        nvarchar(50),
        ContainerSize			        nvarchar(50),
        BOE						                 nvarchar(255),
        AdditionalText1			        nvarchar(255),
        AdditionalText2			        nvarchar(255),
        QuantityMismatch          bit default 0,
        Complete                  nvarchar(50),
        WarehouseId               int,
        VehicleTypeId             int,
        VehicleType               nvarchar(40),
        GRN                       nvarchar(30)
       );
  
  declare @InboundDocumentTypeCode	nvarchar(10),
		  @ExternalCompanyId		int
  
  set @ExternalCompanyId = null
  set @ExternalCompanyId = (select ExternalCompanyId 
							  from Operator o 
							 where o.OperatorId = @OperatorId)  
  
  if @InboundDocumentTypeId in (-1,0)
    set @InboundDocumentTypeId = null
  
  if @ExternalCompanyCode is null
    set @ExternalCompanyCode = ''
   
  if @ExternalCompany is null
    set @ExternalCompany = ''
  
  if @PrincipalId = -1
    set @PrincipalId = null
  
  if datalength(@OrderNumber) > 1
    set @InboundShipmentId = null
  
  set @InboundDocumentTypeCode = ''
  
  if (select dbo.ufn_Configuration(211, @WarehouseId)) = 0
    set @InboundDocumentTypeCode = 'PRV'
  
  if (select dbo.ufn_Configuration(260, @WarehouseId)) = 1
    set @WarehouseId = null
  
  insert @TableResult
        (Original,
         InboundShipmentId,
         InboundDocumentId,
         ReceiptId,
         LocationId,
         OrderNumber,
         ReferenceNumber,
         SupplierCode,
         Supplier,
         StatusId,
         StatusCode,
         Status,
         DeliveryDate,
         PlannedDeliveryDate,
         CreateDate,
         InboundDocumentType,
         InboundDocumentTypeCode,
         Rating,
         Delivery,
         DeliveryNoteNumber,
         SealNumber,
         PriorityID,
         VehicleRegistration,
         Remarks,
         AllowPalletise,
         NumberOfLines,
         PrincipalId,
         ShippingAgentId,
         ContainerNumber,
         ContainerSize,
         BOE,
         AdditionalText1,
         AdditionalText2,
         WarehouseId,
         VehicleTypeId,
         GRN)
  select 1,
         isr.InboundShipmentId,
         id.InboundDocumentId,
         r.ReceiptId,
         r.LocationId,
         id.OrderNumber,
         id.ReferenceNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         r.StatusId,
         s.StatusCode,
         s.Status,
         isnull(r.DeliveryDate, convert(nvarchar(10), id.CreateDate,120)),
         isnull(r.PlannedDeliveryDate, id.DeliveryDate),
         id.CreateDate,
         idt.InboundDocumentType,
         idt.InboundDocumentTypeCode,
         ec.Rating,
         r.Delivery,
         r.DeliveryNoteNumber,
         r.SealNumber,
         r.PriorityID,
         r.VehicleRegistration,
         r.Remarks,
         r.AllowPalletise,
         r.NumberOfLines,
         id.PrincipalId,
         r.ShippingAgentId,
         r.ContainerNumber,
         null,
         r.BOE,
         r.AdditionalText1,
         r.AdditionalText2,
         r.WarehouseId,
         r.VehicleTypeId,
         r.GRN
    from InboundDocument     id  (nolock)
    join Receipt             r   (nolock) on id.InboundDocumentId     = r.InboundDocumentId
    left
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId           = isr.ReceiptId
    join Status              s   (nolock) on r.StatusId               = s.StatusId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    left outer
    join ExternalCompany     ec  (nolock) on id.ExternalCompanyId     = ec.ExternalCompanyId
   where isnull(isr.InboundShipmentId,-1)       = isnull(@InboundShipmentId, isnull(isr.InboundShipmentId,-1))
     and id.InboundDocumentTypeId               = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
     and isnull(ec.ExternalCompanyCode,'-1') like isnull(@ExternalCompanyCode + '%', Isnull(ec.ExternalCompanyCode,'-1'))
     and isnull(ec.ExternalCompany,'-1')     like isnull(@ExternalCompany + '%', isnull(ec.ExternalCompany,'-1'))
     and (id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
     or   id.ReferenceNumber       like isnull(@OrderNumber  + '%', id.OrderNumber))
     and isnull(r.DeliveryDate, isnull(r.DeliveryDate, id.CreateDate)) between @FromDate and @ToDate
     and s.Type                                = 'R'
     and s.StatusCode                         in ('W','C','D','S','P','R','LA','OH') -- Waiting, Confirmed, Delivered, Started, Palletised, Received, Locations Allocated, On Hold
     and idt.InboundDocumentTypeCode          != @InboundDocumentTypeCode
     and id.WarehouseId                        = isnull(@WarehouseId, id.WarehouseId)
     and id.OrderNumber                 not like 'PRV2%'
     and idt.InboundDocumentTypeCode          != 'MO'
     and isnull(id.PrincipalId, -1) = isnull(@PrincipalId, isnull(id.PrincipalId, -1))
     and id.ExternalCompanyId = isnull(@ExternalCompanyId,id.ExternalCompanyId)
  
  insert @TableResult
        (Original,
         InboundShipmentId,
         InboundDocumentId,
         ReceiptId,
         LocationId,
         OrderNumber,
         ReferenceNumber,
         SupplierCode,
         Supplier,
         StatusId,
         StatusCode,
         Status,
         DeliveryDate,
         PlannedDeliveryDate,
         CreateDate,
         InboundDocumentType,
         InboundDocumentTypeCode,
         Rating,
         Delivery,
         DeliveryNoteNumber,
         SealNumber,
         PriorityID,
         VehicleRegistration,
         Remarks,
         AllowPalletise,
         NumberOfLines,
         PrincipalId,
         ShippingAgentId,
         ContainerNumber,
         ContainerSize,
         BOE,
         AdditionalText1,
         AdditionalText2,
         WarehouseId,
         VehicleTypeId,
         GRN)
  select 0,
         isnull(isr.InboundShipmentId, id.ReferenceNumber),
         id.InboundDocumentId,
         r.ReceiptId,
         r.LocationId,
         tr.OrderNumber + ' / ' + id.OrderNumber,
         id.ReferenceNumber,
         tr.SupplierCode,
         tr.Supplier,
         r.StatusId,
         s.StatusCode,
         s.Status,
         isnull(r.DeliveryDate, convert(nvarchar(10), id.CreateDate,120)),
         isnull(r.PlannedDeliveryDate, id.DeliveryDate),
         id.CreateDate,
         idt.InboundDocumentType,
         idt.InboundDocumentTypeCode,
         tr.Rating,
         r.Delivery,
         r.DeliveryNoteNumber,
         r.SealNumber,
         r.PriorityID,
         r.VehicleRegistration,
         r.Remarks,
         r.AllowPalletise,
         r.NumberOfLines,
         id.PrincipalId,
         r.ShippingAgentId,
         r.ContainerNumber,
         null,
         r.BOE,
         r.AdditionalText1,
         r.AdditionalText2,
         r.WarehouseId,
         r.VehicleTypeId,
         r.GRN
    from @TableResult           tr
    join InboundDocument        id (nolock) on id.ReferenceNumber        = convert(nvarchar(30), tr.InboundShipmentId)
    join Receipt                 r (nolock) on id.InboundDocumentId      = r.InboundDocumentId
    left
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId              = isr.ReceiptId
    join Status                   s (nolock) on r.StatusId               = s.StatusId
    join InboundDocumentType    idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
   where s.StatusCode in ('W','C','D','S','P','R','LA','OH') -- Waiting, Confirmed, Delivered, Started, Palletised, Received, Locations Allocated, On Hold
  
  delete tr1
    from @TableResult tr1
    join @TableResult tr2 on tr1.InboundShipmentId = tr2.InboundShipmentId
   where tr1.Original = 1
     and tr2.Original = 0
  
  update @TableResult
     set InboundShipmentId = null
   where Original = 0
   
--  update tr
--     set ReceiptId = -2,
--         Status    = 'Unit Price not captured'
--    from @TableResult tr
--    join InboundLine  il (nolock) on tr.InboundDocumentId = il.InboundDocumentId
--   where il.UnitPrice is null
--     and tr.InboundDocumentTypeCode = 'RET'
--     and isnull(tr.ReferenceNumber,'') = ''
  
  update r
     set Location = l.Location
    from @TableResult r
    join Location     l (nolock) on r.LocationId = l.LocationId
  
  update r
     set VehicleType = vt.Name
    from @TableResult r
    join VehicleType vt (nolock) on r.VehicleTypeId = vt.Id
  
  update r
     set Priority = P.Priority
    from @TableResult r
    join Priority    P (nolock) on r.PriorityId = P.PriorityId
  
  update tr
     set PrincipalCode = p.PrincipalCode
    from @TableResult tr
    join Principal     p (nolock) on tr.PrincipalId = p.PrincipalId
  
  update tr
     set ShippingAgent = ec.ExternalCompany
    from @TableResult tr
    join ExternalCompany  ec (nolock) on tr.ShippingAgentId = ec.ExternalCompanyId
  
  if (select dbo.ufn_Configuration(446, @WarehouseId)) = 1 -- Receiving - Only allow active StorageUnits
    update tr
       set Status = 'On Hold'
      from @TableResult      tr
      join ReceiptLine       rl (nolock) on tr.ReceiptId = rl.ReceiptId
      join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
      join Pack              pk (nolock) on sub.StorageUnitId = pk.StorageUnitId
                                        and tr.WarehouseId    = pk.WarehouseId
      join Status             s (nolock) on pk.StatusId = s.StatusId
                                        and s.StatusCode != 'A'
  
  if (select dbo.ufn_Configuration(447, @WarehouseId)) = 1 -- Receiving - Only supervisors send up PO which does not balance
    update tr
       set QuantityMismatch = 1
      from @TableResult      tr
      join ReceiptLine       rl (nolock) on tr.ReceiptId = rl.ReceiptId
     where rl.DeliveryNoteQuantity != rl.ReceivedQuantity
  
  update @TableResult
     set Complete = 'Complete'
   where StatusCode in ('R','P','LA')
  
  update @TableResult
     set Complete = 'Qty Mismatch'
   where Complete = 'Complete'
     and QuantityMismatch = 1
  
  select isnull(InboundShipmentId,-1) as 'InboundShipmentId',
         ReceiptId,
         OrderNumber,
         ReferenceNumber,
         SupplierCode,
         Supplier,
         NumberOfLines,
         DeliveryDate,
         PlannedDeliveryDate,
         Status,
         InboundDocumentType,
         isnull(LocationId,-1) as LocationId,
         Location,
         Rating,
         Delivery,
         DeliveryNoteNumber,
         SealNumber,
         PriorityID,
         Priority,
         VehicleRegistration,
         Remarks,
         isnull(AllowPalletise,1) as 'AllowPalletise',
         PrincipalCode,
         --ReceivingCompleteDate,
         Complete,
         case when StatusCode in ('R','P','LA')
              then 'Redelivery'
              else ''
              end as 'Redelivery',
         PrincipalCode,
         ShippingAgent,
         isnull(ShippingAgentid,-1) as ShippingAgentId,
         ContainerNumber,
         ContainerSize,
         BOE,
         AdditionalText1,
         AdditionalText2,
         isnull(VehicleTypeId, -1) as 'VehicleTypeId',
         VehicleType,
         GRN
    from @TableResult
end
 
 
 
