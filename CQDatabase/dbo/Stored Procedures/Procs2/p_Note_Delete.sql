﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Note_Delete
  ///   Filename       : p_Note_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:46
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Note table.
  /// </remarks>
  /// <param>
  ///   @NoteId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Note_Delete
(
 @NoteId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Note
     where NoteId = @NoteId
  
  select @Error = @@Error
  
  
  return @Error
  
end
