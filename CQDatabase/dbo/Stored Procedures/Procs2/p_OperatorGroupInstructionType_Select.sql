﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupInstructionType_Select
  ///   Filename       : p_OperatorGroupInstructionType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OperatorGroupInstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null,
  ///   @OperatorGroupId int = null 
  /// </param>
  /// <returns>
  ///   OperatorGroupInstructionType.InstructionTypeId,
  ///   OperatorGroupInstructionType.OperatorGroupId,
  ///   OperatorGroupInstructionType.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupInstructionType_Select
(
 @InstructionTypeId int = null,
 @OperatorGroupId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         OperatorGroupInstructionType.InstructionTypeId
        ,OperatorGroupInstructionType.OperatorGroupId
        ,OperatorGroupInstructionType.OrderBy
    from OperatorGroupInstructionType
   where isnull(OperatorGroupInstructionType.InstructionTypeId,'0')  = isnull(@InstructionTypeId, isnull(OperatorGroupInstructionType.InstructionTypeId,'0'))
     and isnull(OperatorGroupInstructionType.OperatorGroupId,'0')  = isnull(@OperatorGroupId, isnull(OperatorGroupInstructionType.OperatorGroupId,'0'))
  
end
