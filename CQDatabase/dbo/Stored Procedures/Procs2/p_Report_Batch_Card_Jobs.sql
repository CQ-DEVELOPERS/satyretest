﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Batch_Card_Jobs
  ///   Filename       : p_Report_Batch_Card_Jobs.sql
  ///   Create By      : Karen
  ///   Date Created   : January 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Batch_Card_Jobs
(
 @OrderNumber	         nvarchar(30)
)

as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   JobId				int
  )
  
  
  --declare @Indicator		  char(1),
		--  @OrderNumber		  nvarchar(30)  

 -- if @OutboundShipmentId = -1
	--set @OutboundShipmentId = null
	
 -- if @IssueId = -1
	--set @IssueId = null
	
  --SELECT @OrderNumber = OrderNumber
  --from OutboundDocument od
		--join Issue i on od.OutboundDocumentId = i.OutboundDocumentId
  --WHERE Issueid = @IssueId
insert @TableHeader
		  (JobId)			
SELECT distinct i.JobId
	FROM      BOMInstruction    bi (nolock)
	Left join OutboundDocument  od (nolock) on bi.OutboundDocumentId = od.OutboundDocumentId
	left join IssueLineInstruction ili (nolock) on bi.OutboundDocumentId = ili.OutboundDocumentId
	left join instruction i (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
	where od.OrderNumber = @OrderNumber
	
	--update th
	--	set Jobid = (select max(i.jobid) from @TableHeader th
	--						left join IssueLineInstruction ili (nolock) on th.OutboundDocumentId = ili.OutboundDocumentId
	--						left join instruction i (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId)
	--from @TableHeader th
	
   Select distinct *
	from   @TableHeader   th
	 
end
