﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Priority_Search
  ///   Filename       : p_Priority_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:14
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Priority table.
  /// </remarks>
  /// <param>
  ///   @PriorityId int = null output,
  ///   @Priority nvarchar(100) = null,
  ///   @PriorityCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Priority.PriorityId,
  ///   Priority.Priority,
  ///   Priority.PriorityCode,
  ///   Priority.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Priority_Search
(
 @PriorityId int = null output,
 @Priority nvarchar(100) = null,
 @PriorityCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @Priority = '-1'
    set @Priority = null;
  
  if @PriorityCode = '-1'
    set @PriorityCode = null;
  
 
  select
         Priority.PriorityId
        ,Priority.Priority
        ,Priority.PriorityCode
        ,Priority.OrderBy
    from Priority
   where isnull(Priority.PriorityId,'0')  = isnull(@PriorityId, isnull(Priority.PriorityId,'0'))
     and isnull(Priority.Priority,'%')  like '%' + isnull(@Priority, isnull(Priority.Priority,'%')) + '%'
     and isnull(Priority.PriorityCode,'%')  like '%' + isnull(@PriorityCode, isnull(Priority.PriorityCode,'%')) + '%'
  
end
