﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Module_Search
  ///   Filename       : p_Module_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:48
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Module table.
  /// </remarks>
  /// <param>
  ///   @ModuleId int = null output,
  ///   @Module nvarchar(100) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Module.ModuleId,
  ///   Module.Module 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Module_Search
(
 @ModuleId int = null output,
 @Module nvarchar(100) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ModuleId = '-1'
    set @ModuleId = null;
  
  if @Module = '-1'
    set @Module = null;
  
 
  select
         Module.ModuleId
        ,Module.Module
    from Module
   where isnull(Module.ModuleId,'0')  = isnull(@ModuleId, isnull(Module.ModuleId,'0'))
     and isnull(Module.Module,'%')  like '%' + isnull(@Module, isnull(Module.Module,'%')) + '%'
  
end
