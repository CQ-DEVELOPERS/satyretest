﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundLineHistory_Insert
  ///   Filename       : p_OutboundLineHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jan 2014 07:55:59
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundLineHistory table.
  /// </remarks>
  /// <param>
  ///   @OutboundLineId int = null,
  ///   @OutboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null,
  ///   @BatchId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @UnitPrice float = null,
  ///   @UOM nvarchar(60) = null,
  ///   @BOELineNumber nvarchar(100) = null,
  ///   @TariffCode nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   OutboundLineHistory.OutboundLineId,
  ///   OutboundLineHistory.OutboundDocumentId,
  ///   OutboundLineHistory.StorageUnitId,
  ///   OutboundLineHistory.StatusId,
  ///   OutboundLineHistory.LineNumber,
  ///   OutboundLineHistory.Quantity,
  ///   OutboundLineHistory.BatchId,
  ///   OutboundLineHistory.CommandType,
  ///   OutboundLineHistory.InsertDate,
  ///   OutboundLineHistory.UnitPrice,
  ///   OutboundLineHistory.UOM,
  ///   OutboundLineHistory.BOELineNumber,
  ///   OutboundLineHistory.TariffCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundLineHistory_Insert
(
 @OutboundLineId int = null,
 @OutboundDocumentId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @LineNumber int = null,
 @Quantity float = null,
 @BatchId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @UnitPrice float = null,
 @UOM nvarchar(60) = null,
 @BOELineNumber nvarchar(100) = null,
 @TariffCode nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OutboundLineHistory
        (OutboundLineId,
         OutboundDocumentId,
         StorageUnitId,
         StatusId,
         LineNumber,
         Quantity,
         BatchId,
         CommandType,
         InsertDate,
         UnitPrice,
         UOM,
         BOELineNumber,
         TariffCode)
  select @OutboundLineId,
         @OutboundDocumentId,
         @StorageUnitId,
         @StatusId,
         @LineNumber,
         @Quantity,
         @BatchId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @UnitPrice,
         @UOM,
         @BOELineNumber,
         @TariffCode 
  
  select @Error = @@Error
  
  
  return @Error
  
end
