﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Inbound_Line_Select
  ///   Filename       : p_Mobile_Inbound_Line_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Inbound_Line_Select
(

 @ReceiptLineId int
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
   WarehouseId        int,
   ReceiptLineId      int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   ProductCode        nvarchar(30),
   Product            nvarchar(255),
   SKUCode            nvarchar(max),
   SKU                nvarchar(50),
   Batch              nvarchar(50),
   ExpiryDate         datetime,
   RequiredQuantity   float,
   ReceivedQuantity   float,
   Boxes              nvarchar(50),
   BoxesQuantity      nvarchar(10),
   ShelfLifeDays	  int,
   ProductCategory	  nchar(1),
   NumberOfPallets	  int
	 )
  
  insert @TableResult
        (WarehouseId,
         ReceiptLineId,
         StorageUnitBatchId,
         StorageUnitId, 
        ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ExpiryDate,
         RequiredQuantity,
         ReceivedQuantity,
         ShelfLifeDays,
         ProductCategory,
         NumberOfPallets)
  select r.WarehouseId,
         rl.ReceiptLineId,
         rl.StorageUnitBatchId,
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         b.ExpiryDate,
         rl.RequiredQuantity,       
  rl.ReceivedQuantity,
         p.ShelfLifeDays,
         pc.ProductCategory,
         rl.NumberOfPallets
    from ReceiptLine       rl (nolock)
    join Receipt            r (nolock) on r.ReceiptId          = rl.ReceiptId
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    left join ProductCategory   pc (nolock) on pc.ProductType		= p.ProductType
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
   where rl.ReceiptLineId = @ReceiptLineId
  
  update tr
   
  set Boxes = pt.PackType,
         BoxesQuantity = p.Quantity
    from @TableResult      tr
    join Pack               p (nolock) on tr.WarehouseId        = p.WarehouseId
                                      and tr.StorageUnitId      = p.StorageUnitId

    join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId
  where pt.InboundSequence > 1
    and p.Quantity         > 1
  
  update tr
     set tr.ExpiryDate = DATEADD (DAY, tr.ShelfLifeDays, GETDATE ( ))
    from @TableResult      tr
  where tr.ProductCategory != 'P'  

  
  update @TableResult
     set SKUCode = SKUCode + isnull(' (' +  Boxes + ' of ' + BoxesQuantity + ' )','')
   where convert(float, BoxesQuantity) >= 1
  
  select ReceiptLineId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         ExpiryDate,
         RequiredQuantity,
         ReceivedQuantity,
         ShelfLifeDays,
         ProductCategory,
         NumberOfPallets
    from @TableResult
end

 
