﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Planning_Order_Search
  ///   Filename       : p_Production_Planning_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Planning_Order_Search
(
 @WarehouseId            int,
 @OutboundShipmentId	    int,
 @LocationId             int,
 @OrderNumber	           nvarchar(30),
 @ProductCode            nvarchar(50),
 @Product                nvarchar(255),
 @Batch                  nvarchar(50),
 @FromDate	              datetime,
 @ToDate	                datetime
)

as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   Id                        int primary key identity,
   WarehouseId               int,
   OutboundDocumentId        int,
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   NumberOfLines             int,
   DeliveryDate              datetime,
   CreateDate                datetime,
   StatusId                  int,
   StatusCode                nvarchar(10),
   Status                    nvarchar(50),
   PriorityId                int,
   Priority                  nvarchar(50),
   LocationId                int,
   Location                  nvarchar(15),
   DespatchBay               int,
   DespatchBayDesc           nvarchar(15),
   AvailabilityIndicator     nvarchar(20),
   Remarks                   nvarchar(255),
   Total                     numeric(13,3),
   Complete                  numeric(13,3),
   PercentageComplete        numeric(13,3),
   StockAvailable            numeric(13,3),
   DropSequence              int,
   StorageUnitId             int,
   BatchId                   int,
   ProductCode               nvarchar(30),
   Product                   nvarchar(255),
   Batch                     nvarchar(50),
   PlannedYield              float,
   AreaType                  nvarchar(10)
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @LocationId = -1
    set @LocationId = null
  
  if @ProductCode = '-1'
    set @ProductCode = null
  
  if @Product = '-1'
    set @Product = null
  
  if @Batch is null
    set @Batch = '%'
	 
  if @OutboundShipmentId is null
  begin
    insert @TableHeader
          (WarehouseId,
           OutboundDocumentId,
           IssueId,
           LocationId,
           DespatchBay,
           OrderNumber,
           StatusId,
           StatusCode,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           Remarks,
           NumberOfLines,
           Total,
           Complete,
           DropSequence,
           StorageUnitId,
           BatchId,
           PlannedYield,
           AreaType)
    select i.WarehouseId,
           od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           i.DespatchBay,
           od.OrderNumber,
           i.StatusId,
           s.StatusCode,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           i.Remarks,
           i.NumberOfLines,
           i.Total,
           i.Complete,
           i.DropSequence,
           od.StorageUnitId,
           od.BatchId,
           od.Quantity,
           i.AreaType
      from OutboundDocument      od (nolock)
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                            and odt.OutboundDocumentTypeCode in ('MO','KIT')
      join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status                 s (nolock) on i.StatusId                = s.StatusId
     where od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
       and od.CreateDate      between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('W')
       and od.WarehouseId            = @WarehouseId
    order by i.DropSequence
    
    update tr
       set OutboundShipmentId = si.OutboundShipmentId,
           DeliveryDate = os.ShipmentDate,
           LocationId   = os.LocationId,
           DespatchBay  = os.DespatchBay,
           StatusId     = os.StatusId
      from @TableHeader  tr
      join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
      join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
  end
  else
    insert @TableHeader
          (WarehouseId,
           OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           LocationId,
           DespatchBay,
           OrderNumber,
           StatusId,
           StatusCode,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           Remarks,
           NumberOfLines,
           Total,
           Complete,
           DropSequence,
           StorageUnitId,
           BatchId,
           PlannedYield,
           AreaType)
    select i.WarehouseId,
           osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           i.DespatchBay,
           od.OrderNumber,
           i.StatusId,
           s.StatusCode,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           i.Remarks,
           i.NumberOfLines,
           i.Total,
           i.Complete,
           i.DropSequence,
           od.StorageUnitId,
           od.BatchId,
           od.Quantity,
           i.AreaType
      from OutboundDocument       od (nolock)
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
                                            and odt.OutboundDocumentTypeCode in ('MO','KIT')
      join Issue                   i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status                  s (nolock) on i.StatusId                = s.StatusId
      join OutboundShipmentIssue osi (nolock) on i.IssueId                 = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and s.Type                 = 'IS'
       and s.StatusCode             in ('W')
  order by i.DropSequence
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product
    from @TableHeader tr
    join StorageUnit  su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product       p (nolock) on su.ProductId     = p.ProductId
  
  update tr
     set Batch = b.Batch
    from @TableHeader tr
    join Batch         b (nolock) on tr.BatchId = b.BatchId
  
  declare @TableResult as table
  (
   DropSequence                    int,
   IssueLineId                     int,
   IssueId                         int,
   StorageUnitId                   int,
   RequiredQuantity                numeric(13,6) default 0,
   ConfirmedQuatity                numeric(13,6) default 0,
   AllocatedQuantity               numeric(13,6) default 0,
   ManufacturingQuantity           numeric(13,6) default 0,
   RawQuantity                     numeric(13,6) default 0,
   ReplenishmentQuantity            numeric(13,6) default 0,
   AvailableQuantity               numeric(13,6) default 0,
   AvailabilityIndicator           nvarchar(20),
   AvailablePercentage             numeric(13,2) default 0,
   AreaType                        nvarchar(10),
   WarehouseId                     int,
   StatusCode                      nvarchar(10)
  )
  
  insert @TableResult
        (DropSequence,
         IssueLineId,
         IssueId,
         StorageUnitId,
         RequiredQuantity,
         ConfirmedQuatity,
         AreaType,
         WarehouseId,
         StatusCode)
  select th.DropSequence,
         il.IssueLineId,
         il.IssueId,
         sub.StorageUnitId,
         il.Quantity,
         il.ConfirmedQuatity,
         isnull(th.AreaType,''),
         th.WarehouseId,
         th.StatusCode
    from @TableHeader th
    join IssueLine              il (nolock) on th.IssueId            = il.IssueId
    join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
    join Status                  s (nolock) on il.StatusId           = s.StatusId
  
  update th
     set Location = l.Location
   from @TableHeader th
   join Location      l (nolock) on th.LocationId = l.LocationId
  
  update th
     set DespatchBayDesc = l.Location
   from @TableHeader th
   join Location      l (nolock) on th.DespatchBay = l.LocationId
  
  --update tr
  --   set AllocatedQuantity = a.ReservedQuantity
  --  from @TableResult tr
  --  join viewAvailable a on tr.WarehouseId   = a.WarehouseId
  --                      and a.AreaType       = ''
  --                      and tr.StorageUnitId = a.StorageUnitId
  
  --update tr
  --   set ManufacturingQuantity = a.ActualQuantity - a.ReservedQuantity
  --  from @TableResult tr
  --  join viewAvailable a on tr.WarehouseId   = a.WarehouseId
  --                      and a.AreaType       = ''
  --                      and tr.StorageUnitId = a.StorageUnitId
  
  --update tr
  --   set RAWQuantity = a.ActualQuantity
  --  from @TableResult tr
  --  join viewAvailable a on tr.WarehouseId   = a.WarehouseId
  --                      and a.AreaType       = 'backup'
  --                      and tr.StorageUnitId = a.StorageUnitId
  
  --update tr
  --   set ReplenishmentQuantity = a.AllocatedQuantity
  --  from @TableResult tr
  --  join viewAvailable a on tr.WarehouseId   = a.WarehouseId
  --                      and a.AreaType       = ''
  --                      and tr.StorageUnitId = a.StorageUnitId
  
  --update tr
  --   set AllocatedQuantity = case when (AllocatedQuantity - RequiredQuantity) >= 0
  --                                then AllocatedQuantity - RequiredQuantity
  --                                else 0
  --                                end
  --  from @TableResult tr
  -- where StatusCode in ('RL','CK')
  
  --update tr
  --   set ManufacturingQuantity = ManufacturingQuantity + RequiredQuantity
  --  from @TableResult tr
  -- where StatusCode in ('RL','CK')
  
  --update tr
  --   set AvailableQuantity = ManufacturingQuantity + RAWQuantity
  --  from @TableResult tr
  
  --update @TableResult
  --   set AvailableQuantity = 0
  -- where AvailableQuantity is null
  
  --update @TableResult
  --   set AvailablePercentage = (convert(numeric(13,3), ManufacturingQuantity) / convert(numeric(13,3), RequiredQuantity)) * 100
  -- where AllocatedQuantity + AvailableQuantity != 0
  
  --update @TableResult
  --   set AvailablePercentage = 100
  -- where AvailablePercentage > 100
  
  --update @TableResult
  --   set AvailablePercentage = 0
  -- where AvailablePercentage < 0
  
  --update @TableResult
  --   set AvailablePercentage = 0
  -- where AvailablePercentage < 0
  
  --update @TableResult
  --   set AvailabilityIndicator = 'Red'
  
  --update @TableResult
  --   set AvailabilityIndicator = 'Red'
  -- where AvailableQuantity <= 0
  
  --update @TableResult
  --   set AvailabilityIndicator = 'Orange'
  -- where AvailableQuantity >= RequiredQuantity
  --   and RequiredQuantity   > ManufacturingQuantity
  --   and RequiredQuantity   > ReplenishmentQuantity
  
  --update @TableResult
  --   set AvailabilityIndicator = 'Yellow'
  -- where AvailableQuantity >= RequiredQuantity
  --   and RequiredQuantity  <= ReplenishmentQuantity
  
  --update @TableResult
  --   set AvailabilityIndicator = 'Green'
  -- where ManufacturingQuantity >= RequiredQuantity
  
  update tr
     set AllocatedQuantity = a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set ManufacturingQuantity = a.ActualQuantity - a.ReservedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  update tr
     set RAWQuantity = a.ActualQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = 'backup'
                        and tr.StorageUnitId = a.StorageUnitId
  
  --update tr
  --   set ReceivingQuantity = a.ActualQuantity
  --  from @TableResult tr
  --  join viewAvailableArea a on tr.WarehouseId   = a.WarehouseId
  --                          and a.AreaType       = 'backup'
  --                          and tr.StorageUnitId = a.StorageUnitId
  --                          and a.AreaCode = 'R'
  
  update tr
     set ReplenishmentQuantity = a.AllocatedQuantity
    from @TableResult tr
    join viewAvailable a on tr.WarehouseId   = a.WarehouseId
                        and a.AreaType       = ''
                        and tr.StorageUnitId = a.StorageUnitId
  
  --update tr
  --   set AllocatedQuantity = case when (AllocatedQuantity - RequiredQuantity) >= 0
  --                                then AllocatedQuantity - RequiredQuantity
  --                                else 0
  --                                end
  --  from @TableResult tr
  -- where StatusCode in ('RL','CK')
  
  --update tr
  --   set ManufacturingQuantity = ManufacturingQuantity + RequiredQuantity
  --  from @TableResult tr
  -- where StatusCode in ('RL','CK')
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  --select * from @TableResult where DropSequence > 1
  
  declare @Count int
  
  select @Count = COUNT(1) from @TableResult
  
  while @Count > 0
  begin
    set @Count = @Count - 1
    update tr
       set AllocatedQuantity = tr.AllocatedQuantity + isnull((select sum(tr2.RequiredQuantity)
                                                                from @TableResult  tr2
                                                               where tr.StorageUnitId = tr2.StorageUnitId
                                                                 and tr2.DropSequence <= tr.DropSequence
                                                              group by tr2.StorageUnitId), tr.RequiredQuantity) - tr.RequiredQuantity
      from @TableResult tr
     where DropSequence = @Count
  end
  
  update tr
     set AvailableQuantity = (ManufacturingQuantity + RAWQuantity) - (AllocatedQuantity)
    from @TableResult tr
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), ManufacturingQuantity - AllocatedQuantity) / convert(numeric(13,3), RequiredQuantity)) * 100
    where convert(numeric(13,3), RequiredQuantity) > 0
  
  update @TableResult
     set AvailablePercentage = 100
   where AvailablePercentage > 100
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where AvailableQuantity <= 0
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where AvailableQuantity >= RequiredQuantity
     --and RequiredQuantity   > ManufacturingQuantity
     and RequiredQuantity   > ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where AvailableQuantity >= RequiredQuantity
     and RequiredQuantity  <= ReplenishmentQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Green'
   where AvailablePercentage >= 100
  
  update th
     set AvailabilityIndicator = tr.AvailabilityIndicator
    from @TableHeader th
    join @TableResult tr on th.IssueId = tr.IssueId
   where tr.AvailabilityIndicator = 'Green'
  
  update th
     set AvailabilityIndicator = tr.AvailabilityIndicator
    from @TableHeader th
    join @TableResult tr on th.IssueId = tr.IssueId
   where tr.AvailabilityIndicator = 'Yellow'
  
  update th
     set AvailabilityIndicator = tr.AvailabilityIndicator
    from @TableHeader th
    join @TableResult tr on th.IssueId = tr.IssueId
   where tr.AvailabilityIndicator = 'Orange'
  
  update th
     set AvailabilityIndicator = tr.AvailabilityIndicator
    from @TableHeader th
    join @TableResult tr on th.IssueId = tr.IssueId
   where tr.AvailabilityIndicator = 'Red'
  
  --select * from @TableResult where DropSequence <= 2 and StorageUnitId in (select StorageUnitId from viewProduct where ProductCode = 'TER 002')
  
  select IssueId,
         isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
         OrderNumber,
         NumberOfLines,
         convert(nvarchar(10), DeliveryDate, 120) as 'DeliveryDate',
         CreateDate,
         ProductCode,
         Product,
         Batch,
         PlannedYield,
         Status,
         PriorityId,
         Priority,
         isnull(LocationId,-1) as 'LocationId',
         Location,
         isnull(DespatchBay,-1) as 'PotId',
         DespatchBayDesc        as 'Pot',
         AvailabilityIndicator,
         Remarks,
         Complete,
         Total,
         convert(int, round(PercentageComplete,0)) as 'PercentageComplete',
         DropSequence,
         --ROW_NUMBER() 
         --OVER (ORDER BY Id) AS 'DropSequence',
         ROW_NUMBER() 
         OVER (ORDER BY Id) AS 'Rank'
    from @TableHeader
   where isnull(ProductCode,'%') like isnull(@ProductCode + '%', isnull(ProductCode,'%'))
     and isnull(Product    ,'%') like isnull(@Product     + '%', isnull(Product    ,'%'))
     and isnull(Batch      ,'%') like isnull(@Batch       + '%', isnull(Batch      ,'%'))
  order by DropSequence
end
