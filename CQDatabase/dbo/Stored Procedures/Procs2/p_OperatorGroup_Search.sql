﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroup_Search
  ///   Filename       : p_OperatorGroup_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2012 07:59:18
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null output,
  ///   @OperatorGroup varchar(30) = null,
  ///   @OperatorGroupCode varchar(10) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorGroup.OperatorGroupId,
  ///   OperatorGroup.OperatorGroup,
  ///   OperatorGroup.RestrictedAreaIndicator,
  ///   OperatorGroup.OperatorGroupCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroup_Search
(
 @OperatorGroupId int = null output,
 @OperatorGroup varchar(30) = null,
 @OperatorGroupCode varchar(10) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
  if @OperatorGroup = '-1'
    set @OperatorGroup = null;
  
  if @OperatorGroupCode = '-1'
    set @OperatorGroupCode = null;
  
  -- Use row_number() function
  with Result as
  (
  select row_number() over(order by OperatorGroup.OperatorGroup) as 'RowNumber',
         OperatorGroup.OperatorGroupId,
         OperatorGroup.OperatorGroup,
         OperatorGroup.RestrictedAreaIndicator,
         OperatorGroup.OperatorGroupCode 
    from OperatorGroup
   where isnull(OperatorGroup.OperatorGroupId,'0')  = isnull(@OperatorGroupId, isnull(OperatorGroup.OperatorGroupId,'0'))
     and isnull(OperatorGroup.OperatorGroup,'%')  like '%' + isnull(@OperatorGroup, isnull(OperatorGroup.OperatorGroup,'%')) + '%'
     and isnull(OperatorGroup.OperatorGroupCode,'%')  like '%' + isnull(@OperatorGroupCode, isnull(OperatorGroup.OperatorGroupCode,'%')) + '%'
  )
  
  select
         OperatorGroupId,
         OperatorGroup,
         RestrictedAreaIndicator,
         OperatorGroupCode 
    from Result
   where RowNumber between @RowStart and @RowEnd
  
end
