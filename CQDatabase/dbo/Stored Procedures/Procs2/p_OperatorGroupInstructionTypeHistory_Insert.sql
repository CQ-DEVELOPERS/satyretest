﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupInstructionTypeHistory_Insert
  ///   Filename       : p_OperatorGroupInstructionTypeHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:05
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorGroupInstructionTypeHistory table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null,
  ///   @OperatorGroupId int = null,
  ///   @OrderBy int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   OperatorGroupInstructionTypeHistory.InstructionTypeId,
  ///   OperatorGroupInstructionTypeHistory.OperatorGroupId,
  ///   OperatorGroupInstructionTypeHistory.OrderBy,
  ///   OperatorGroupInstructionTypeHistory.CommandType,
  ///   OperatorGroupInstructionTypeHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupInstructionTypeHistory_Insert
(
 @InstructionTypeId int = null,
 @OperatorGroupId int = null,
 @OrderBy int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorGroupInstructionTypeHistory
        (InstructionTypeId,
         OperatorGroupId,
         OrderBy,
         CommandType,
         InsertDate)
  select @InstructionTypeId,
         @OperatorGroupId,
         @OrderBy,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
