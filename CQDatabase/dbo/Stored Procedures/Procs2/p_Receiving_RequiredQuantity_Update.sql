﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_RequiredQuantity_Update
  ///   Filename       : p_Receiving_RequiredQuantity_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_RequiredQuantity_Update
(
 @InboundLineId int,
 @ReceiptId int = null
)

as
begin
	 set nocount on;
  
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @RequiredQuantity     numeric(13,6),
          @TempReceiptLineId    int,
          @AcceptedQuantity     numeric(13,6),
          @ReceivedQuantity     numeric(13,6),
          @OriginalRequired     numeric(13,6)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  -- Get the Original Required Quantity for line
  select @OriginalRequired = Quantity
    from InboundLine (nolock)
   where InboundLineId = @InboundLineId
  
  -- Get the Received Quantity for all lines across all receipts (excluding the current one)
  select @ReceivedQuantity = sum(ReceivedQuantity)
    from ReceiptLine rl (nolock)
    join Status s (nolock) on rl.StatusId = s.StatusId
   where InboundLineId = @InboundLineId
     and ReceiptId < @ReceiptId
     and s.StatusCode in ('R','RC')
  
  -- Current Required = Original Required - All other receipts
  set @RequiredQuantity = isnull(@OriginalRequired,0) - isnull(@ReceivedQuantity,0)
  
  --select @RequiredQuantity = case when max(Quantity) > sum(rl.RequiredQuantity)
  --                                then max(Quantity)
  --                                else sum(rl.RequiredQuantity)
  --                                end
  --  from InboundLine il (nolock)
  --  join ReceiptLine rl (nolock) on il.InboundLineId = rl.InboundLineId
  -- where il.InboundLineId = @InboundLineId
  --   and rl.ReceiptId = @ReceiptId
  
  declare @TableReceiptLine as table
  (
   ReceiptLineId    int,
   AcceptedQuantity float
  )
  
  insert @TableReceiptLine
        (ReceiptLineId,
         AcceptedQuantity)
  select ReceiptLineId,
         isnull(AcceptedQuantity,0)
    from ReceiptLine (nolock)
   where InboundLineId = @InboundLineId
     and ReceiptId = @ReceiptId
  
  update rl
     set RequiredQuantity = 0
   from @TableReceiptLine t
   join ReceiptLine      rl on t.ReceiptLineId = rl.ReceiptLineId
  
  while exists(select top 1 1 from @TableReceiptLine)
  begin
    select top 1
           @TempReceiptLineId = ReceiptLineId,
           @AcceptedQuantity  = isnull(AcceptedQuantity,0)
      from @TableReceiptLine
    order by ReceiptLineId
    
    delete @TableReceiptLine where ReceiptLineId = @TempReceiptLineId
    
    if @AcceptedQuantity > @RequiredQuantity
    begin
      exec p_ReceiptLine_Update
       @ReceiptLineId = @TempReceiptLineId,
       @RequiredQuantity = @RequiredQuantity
      
      set @RequiredQuantity = null
      
      delete @TableReceiptLine
    end
    else
    begin
      exec p_ReceiptLine_Update
       @ReceiptLineId = @TempReceiptLineId,
       @RequiredQuantity = @AcceptedQuantity
      
      set @RequiredQuantity = @RequiredQuantity - @AcceptedQuantity
    end
    --select @TempReceiptLineId as '@TempReceiptLineId', @RequiredQuantity as '@RequiredQuantity', @AcceptedQuantity as '@AcceptedQuantity'
  end
  
  if @RequiredQuantity is not null
  begin
    set @RequiredQuantity = @RequiredQuantity + @AcceptedQuantity
    exec p_ReceiptLine_Update
     @ReceiptLineId = @TempReceiptLineId,
     @RequiredQuantity = @RequiredQuantity
  end
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Receiving_RequiredQuantity_Update'); 
    rollback transaction
    return @Error
end
 
