﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Shipment_Linked_Issue
  ///   Filename       : p_Outbound_Shipment_Linked_Issue.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Shipment_Linked_Issue
(
 @OutboundShipmentId int
)

as
begin
	 set nocount on;
  
  select isi.OutboundShipmentId,
         i.IssueId,
         od.OrderNumber,
         i.Weight as 'OrderWeight',
         i.Volume as 'OrderVolume'
    from OutboundDocument       od  (nolock)
    join Issue                  i   (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    join OutboundShipmentIssue  isi (nolock) on i.IssueId             = isi.IssueId
   where isi.OutboundShipmentId = @OutboundShipmentId
end
