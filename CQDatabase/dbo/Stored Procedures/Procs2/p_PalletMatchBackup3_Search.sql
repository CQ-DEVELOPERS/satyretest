﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletMatchBackup3_Search
  ///   Filename       : p_PalletMatchBackup3_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:52
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the PalletMatchBackup3 table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   PalletMatchBackup3.PalletId,
  ///   PalletMatchBackup3.InstructionId,
  ///   PalletMatchBackup3.StorageUnitBatchId,
  ///   PalletMatchBackup3.ProductCode,
  ///   PalletMatchBackup3.SKUCode,
  ///   PalletMatchBackup3.Batch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletMatchBackup3_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         PalletMatchBackup3.PalletId
        ,PalletMatchBackup3.InstructionId
        ,PalletMatchBackup3.StorageUnitBatchId
        ,PalletMatchBackup3.ProductCode
        ,PalletMatchBackup3.SKUCode
        ,PalletMatchBackup3.Batch
    from PalletMatchBackup3
  
end
