﻿--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundPerformance_Insert
  ///   Filename       : p_OutboundPerformance_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Aug 2008 19:50:43
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundPerformance table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null output,
  ///   @CreateDate datetime = null,
  ///   @Release datetime = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null,
  ///   @Checking datetime = null,
  ///   @Despatch datetime = null,
  ///   @DespatchChecked datetime = null,
  ///   @Complete datetime = null 
  /// </param>
  /// <returns>
  ///   OutboundPerformance.JobId,
  ///   OutboundPerformance.CreateDate,
  ///   OutboundPerformance.Release,
  ///   OutboundPerformance.StartDate,
  ///   OutboundPerformance.EndDate,
  ///   OutboundPerformance.Checking,
  ///   OutboundPerformance.Despatch,
  ///   OutboundPerformance.DespatchChecked,
  ///   OutboundPerformance.Complete 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundPerformance_Insert
(
 @JobId int = null output,
 @CreateDate datetime = null,
 @Release datetime = null,
 @StartDate datetime = null,
 @EndDate datetime = null,
 @Checking datetime = null,
 @Despatch datetime = null,
 @DespatchChecked datetime = null,
 @Complete datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  insert OutboundPerformance
        (JobId,
         CreateDate,
         Release,
         StartDate,
         EndDate,
         Checking,
         Despatch,
         DespatchChecked,
         Complete)
  select @JobId,
         @CreateDate,
         @Release,
         @StartDate,
         @EndDate,
         @Checking,
         @Despatch,
         @DespatchChecked,
         @Complete 
  
  select @Error = @@Error
  
  return @Error
  
end

