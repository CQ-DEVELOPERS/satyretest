﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Delete
  ///   Filename       : p_OutboundDocument_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 07:31:31
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the OutboundDocument table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Delete
(
 @OutboundDocumentId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete OutboundDocument
     where OutboundDocumentId = @OutboundDocumentId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_OutboundDocumentHistory_Insert
         @OutboundDocumentId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
