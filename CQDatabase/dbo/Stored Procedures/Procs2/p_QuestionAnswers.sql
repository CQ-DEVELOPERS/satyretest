﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QuestionAnswers
  ///   Filename       : p_QuestionAnswers.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 May 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QuestionAnswers (@Startdate datetime,
								  @EndDate  Datetime,
								  @ExternalCompanyId	nvarchar(20))

as
begin
	 set nocount on;

 If (@Startdate = -10) set @StartDate = null
If (@EndDate = '1900-01-01') set @EndDate = null
If Len(@ExternalCompanyId) = 0 set @ExternalCompanyId = null

  Select c.ExternalCompany,
		c.ExternalCompanyId,
		dbo.DateFormatNoTime(DateAsked) as DateAsked,
		QuestionAireType,
		QuestionText,
		Answer,
		Count(1) as Cnt
from QuestionAnswers qa
	Join Questions q on q.QuestionId = qa.QuestionId
	Join QuestionAire a on a.QuestionaireId = q.QuestionaireId
	--Left Join Receipt r on r.ReceiptId = qa.ReceiptId
	Left Join InboundDocument d on d.OrderNumber = qa.OrderNumber
	Join ExternalCompany c on c.ExternalCompanyId = d.ExternalCompanyId
where q.QuestionType = 'YesNo' 
		and  DateAsked >= Isnull(@StartDate,DateAsked)
		and  dbo.DateFormatNoTime(DateAsked) <= Isnull(@EndDate,DateAsked)
		and c.ExternalCompanyId = Isnull(@ExternalCompanyId,c.ExternalCompanyId)
GRoup by c.ExternalCompany,
		c.ExternalCompanyId,
		dbo.DateFormatNoTime(DateAsked),
		QuestionAireType,
		QuestionText,
		Answer
 
end
