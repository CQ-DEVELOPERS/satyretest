﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PricingCategory_Select
  ///   Filename       : p_PricingCategory_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:51:25
  /// </summary>
  /// <remarks>
  ///   Selects rows from the PricingCategory table.
  /// </remarks>
  /// <param>
  ///   @PricingCategoryId int = null 
  /// </param>
  /// <returns>
  ///   PricingCategory.PricingCategoryId,
  ///   PricingCategory.PricingCategory,
  ///   PricingCategory.PricingCategoryCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PricingCategory_Select
(
 @PricingCategoryId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         PricingCategory.PricingCategoryId
        ,PricingCategory.PricingCategory
        ,PricingCategory.PricingCategoryCode
    from PricingCategory
   where isnull(PricingCategory.PricingCategoryId,'0')  = isnull(@PricingCategoryId, isnull(PricingCategory.PricingCategoryId,'0'))
  order by PricingCategory
  
end
 
