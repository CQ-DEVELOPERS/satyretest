﻿
/*
  /// <summary>
  ///   Procedure Name : p_PickChecking_Update_Line
  ///   Filename       : p_PickChecking_Update_Line.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PickChecking_Update_Line
(
 @instructionId int,
 @CheckQuantity float
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @JobId             int,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @JobId             = JobId
    from Instruction (nolock)
   where InstructionId = @instructionId
  
  select @StatusId = dbo.ufn_StatusId('IS','CK')
  
  begin transaction
  
  exec @Error = p_Job_Update
   @JobId    = @JobId,
   @StatusId = @StatusId
  
  exec @Error = p_Instruction_Update
   @instructionId = @instructionId,
   @CheckQuantity = @CheckQuantity
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Status_Rollup
   @jobId = @JobId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_PickChecking_Update_Line'); 
    rollback transaction
    return @Error
end
