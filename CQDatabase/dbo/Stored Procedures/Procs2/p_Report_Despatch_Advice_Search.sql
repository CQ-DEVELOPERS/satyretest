﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Despatch_Advice_Search
  ///   Filename       : p_Report_Despatch_Advice_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Despatch_Advice_Search
(
 @WarehouseId            int,
 @OutboundShipmentId	    int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @OrderNumber	           nvarchar(30),
 @FromDate	              datetime,
 @ToDate	                datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundDocumentId        int,
   OutboundDocumentType      nvarchar(30),
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   CustomerCode              nvarchar(30),
   Customer                  nvarchar(255),
   RouteId                   int,
   Route                     nvarchar(50),
   NumberOfLines             int,
   DeliveryDate              datetime,
   StatusId                  int,
   Status                    nvarchar(50),
   LoadIndicator             bit
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @OutboundShipmentId is null
  begin
    insert @TableResult
          (OutboundDocumentId,
           IssueId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           DeliveryDate,
           LoadIndicator,
           NumberOfLines)
    select od.OutboundDocumentId,
           i.IssueId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.DeliveryDate,
           i.LoadIndicator,
           i.NumberOfLines
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId               = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
     where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
       and od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
       and od.CreateDate       between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('PC','RL','CK','CD','DC','C')
       and od.WarehouseId            = @WarehouseId
  end
  else
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           DeliveryDate,
           LoadIndicator,
           NumberOfLines)
    select osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.DeliveryDate,
           i.LoadIndicator,
           i.NumberOfLines
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId               = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and s.Type                 = 'IS'
       and s.StatusCode             in ('PC','RL','CK','CD','DC','C')
  
  update tr
     set OutboundShipmentId = si.OutboundShipmentId,
--         DeliveryDate = os.ShipmentDate,
         Route        = os.Route,
         RouteId      = os.RouteId,
         StatusId     = os.StatusId
    from @TableResult  tr
    join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
    join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
  
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
  select IssueId,
         isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
         OrderNumber,
         CustomerCode,
         Customer,
         isnull(RouteId,-1) as 'RouteId',
         Route,
         NumberOfLines,
         DeliveryDate,
         Status
    from @TableResult
  order by Status desc,
           OutboundShipmentId,
           OrderNumber
end
