﻿
/*
  /// <summary>
  ///   Procedure Name : p_Plascon_Export_BOT
  ///   Filename       : p_Plascon_Export_BOT.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 2011-11-04
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Plascon_Export_BOT]
as
begin
	 set nocount on;
  
    declare @Error             int,
            @Errormsg          varchar(500),
            @GetDate           datetime,
            @RecordType        char(3),
            @SequenceNumber    int,
            @Increment         int
  
    select @RecordType = 'BOT'
          ,@GetDate = dbo.ufn_Getdate()
          ,@Errormsg = 'p_Plascon_Export_BOT - Error executing p_Plascon_Export_BOT'
  
    begin transaction
  
    update InterfaceExportHeader
       set RecordStatus  = 'Y',
           ProcessedDate = @getdate
     where RecordStatus = 'N'
       and RecordType   = isnull(@RecordType,RecordType)
       
    set @Increment = @@ROWCOUNT
  
    select @Error = @@Error, @Errormsg = 'p_Plascon_Export_BOT - Error updating InterfaceExportHeader'
  
    if @Error <> 0
        goto error

    exec p_Sequence_Number 
                 @Value = @SequenceNumber output
                ,@Increment = @Increment
    
    insert interfaceextract
           (
            RecordType,
            SequenceNumber,
            OrderNumber,
            LineNumber,
            RecordStatus,
            ExtractedDate,
            Data
           )
    select  @RecordType,
            sequence_no,
            doc_no,
            di_no,
            'N',
            extract_date,            
            replicate('0',9  - datalength(convert(varchar(9),sequence_no))) + convert(varchar(9),sequence_no) + 
            record_type + 
            replicate('0',3  - datalength(convert(varchar(3),branch_code))) + convert(varchar(3),branch_code) + 
            convert(varchar(6),ltrim(doc_no)) +  replicate(' ',6 - datalength(convert(varchar(6),ltrim(doc_no)))) +
            replicate('0',9 - datalength(convert(varchar(9),convert(int,di_no)))) + convert(varchar(9),convert(int,di_no)) +
            replicate('0',8 - datalength(convert(varchar(8),cust_cd))) + convert(varchar(8),cust_cd) +
            extract_date + 
            convert(varchar(8),ltrim(doc_customer)) +  replicate(' ',8 - datalength(convert(varchar(8),ltrim(doc_customer)))) +
            convert(varchar(40),ltrim(remarks)) +  replicate(' ',40 - datalength(convert(varchar(40),ltrim(remarks)))) +
            replicate(' ',9 - datalength(convert(varchar(9),ltrim(stock_no)))) + convert(varchar(9),ltrim(stock_no))     +  
            replicate('0',4  - datalength(convert(varchar(4),sku_code))) + convert(varchar(4),sku_code) + 
            replicate('0',6 - datalength(convert(varchar(6),convert(int,di_qty_check)))) + convert(varchar(6),convert(int,di_qty_check)) +
            --replicate('0',6 - datalength(convert(varchar(6),convert(int,grv_no)))) + convert(varchar(6),convert(int,grv_no)) +
            replicate('0',6 - datalength(convert(varchar(6),ltrim(grv_no)))) + convert(varchar(6),ltrim(grv_no)) +
            convert(varchar(1),di_next_status) +
            replicate('0',9 - datalength(convert(varchar(9),convert(int,line_check)))) + convert(varchar(9),convert(int,line_check))
    From
        (Select  s.sequence_no
                ,@RecordType As record_type
                ,ISNULL(h.FromWarehouseCode,'045') As branch_code
                ,h.OrderNumber As doc_no
                ,d.LineNumber As di_no
                ,h.CompanyCode As cust_cd
                ,CONVERT(Varchar(10), @GetDate, 120) As extract_date
                ,ISNULL(h.Company,'supplier') As doc_customer /*???*/
                ,ISNULL(h.Remarks,'none') As remarks
                ,d.ProductCode As stock_no
                ,d.SKUCode As sku_code
                ,d.Quantity As di_qty_check /*???*/
                 ,ISNULL(h.DeliveryNoteNumber,'') As grv_no /*???*/
                ,'N' As di_next_status /*???*/
                ,COUNT(1) OVER (Partition By h.InterfaceExportHeaderId) As line_check
         From 
            (select InterfaceExportHeaderId, ROW_NUMBER() Over (Order By InterfaceExportHeaderId) + @SequenceNumber As sequence_no 
             from InterfaceExportHeader Where ProcessedDate = @GetDate) s
            Inner Join InterfaceExportHeader h on s.InterfaceExportHeaderId = h.InterfaceExportHeaderId
            Inner Join InterfaceExportDetail d on h.InterfaceExportHeaderId = d.InterfaceExportHeaderId
            where d.Quantity > 0
            ) ExtractTable
        Order By sequence_no, di_no
  
  commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    rollback transaction
    return @Error
end

