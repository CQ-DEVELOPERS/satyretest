﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receipt_Instruction_Update
  ///   Filename       : p_Receipt_Instruction_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receipt_Instruction_Update
(
 @InstructionId     int,
 @ConfirmedQuantity numeric(13,6)
)

as
begin
	 set nocount on;
	 
	 declare @TableReceipt as table
	 (
	  ReceiptId int
	 )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @JobId             int,
          @StatusCode        nvarchar(10),
          @JobCode           nvarchar(10),
          @OperatorId        int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @JobId      = i.JobId,
         @StatusCode = si.StatusCode,
         @JobCode    = sj.StatusCode,
         @OperatorId = i.OperatorId
    from Instruction i (nolock)
    join Status     si (nolock) on i.StatusId = si.StatusId
    join Job         j (nolock) on i.JobId    = j.JobId
    join Status     sj (nolocK) on j.StatusId = sj.StatusId
   where i.InstructionId = @InstructionId
  
  begin transaction
  
  if @StatusCode in ('NS','F')
  begin
    exec @Error = p_Instruction_Reset
     @InstructionId = @InstructionId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Instruction_Started
     @InstructionId = @InstructionId,
     @OperatorId    = @OperatorId
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId       = @instructionId,
   @Confirmed           = 1
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId     = @InstructionId,
   @ConfirmedQuantity = @ConfirmedQuantity
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId       = @instructionId,
   @Confirmed           = 1
  
  if @Error <> 0
    goto error
  
  if @JobCode = 'QA' -- If QA Make Printed (Needs to be checked again)
  begin
    select @StatusId = dbo.ufn_StatusId('R','PR')
    
    exec @Error = p_Job_Update
     @JobId    = @jobId,
     @StatusId = @StatusId
    
    if @Error <> 0
      goto error
    
    delete SKUCheck
     where JobId = @JobId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  else if @JobCode = 'C' -- If Complete Make Accpeted (Already partly putaway cannot check again)
  begin
    select @StatusId = dbo.ufn_StatusId('R','A')
    
    exec @Error = p_Job_Update
     @JobId    = @jobId,
     @StatusId = @StatusId
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Receipt_Instruction_Update'); 
    rollback transaction
    return @Error
end
