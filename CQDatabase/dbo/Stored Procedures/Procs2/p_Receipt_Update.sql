﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receipt_Update
  ///   Filename       : p_Receipt_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2014 13:28:36
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Receipt table.
  /// </remarks>
  /// <param>
  ///   @ReceiptId int = null,
  ///   @InboundDocumentId int = null,
  ///   @PriorityId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @CreditAdviceIndicator bit = null,
  ///   @Delivery int = null,
  ///   @AllowPalletise bit = null,
  ///   @Interfaced bit = null,
  ///   @StagingLocationId int = null,
  ///   @NumberOfLines int = null,
  ///   @ReceivingCompleteDate datetime = null,
  ///   @ParentReceiptId int = null,
  ///   @GRN nvarchar(60) = null,
  ///   @PlannedDeliveryDate datetime = null,
  ///   @ShippingAgentId int = null,
  ///   @ContainerNumber nvarchar(100) = null,
  ///   @AdditionalText1 nvarchar(510) = null,
  ///   @AdditionalText2 nvarchar(510) = null,
  ///   @BOE nvarchar(510) = null,
  ///   @Incoterms nvarchar(100) = null,
  ///   @ReceiptConfirmed datetime = null,
  ///   @ReceivingStarted datetime = null,
  ///   @VehicleId int = null,
  ///   @ParentIssueId int = null,
  ///   @VehicleTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receipt_Update
(
 @ReceiptId int = null,
 @InboundDocumentId int = null,
 @PriorityId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Remarks nvarchar(500) = null,
 @CreditAdviceIndicator bit = null,
 @Delivery int = null,
 @AllowPalletise bit = null,
 @Interfaced bit = null,
 @StagingLocationId int = null,
 @NumberOfLines int = null,
 @ReceivingCompleteDate datetime = null,
 @ParentReceiptId int = null,
 @GRN nvarchar(60) = null,
 @PlannedDeliveryDate datetime = null,
 @ShippingAgentId int = null,
 @ContainerNumber nvarchar(100) = null,
 @AdditionalText1 nvarchar(510) = null,
 @AdditionalText2 nvarchar(510) = null,
 @BOE nvarchar(510) = null,
 @Incoterms nvarchar(100) = null,
 @ReceiptConfirmed datetime = null,
 @ReceivingStarted datetime = null,
 @VehicleId int = null,
 @ParentIssueId int = null,
 @VehicleTypeId int = null 
)

as
begin
	 set nocount on;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @InboundDocumentId = '-1'
    set @InboundDocumentId = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @ShippingAgentId = '-1'
    set @ShippingAgentId = null;
  
  if @VehicleId = '-1'
    set @VehicleId = null;
  
  if @VehicleTypeId = '-1'
    set @VehicleTypeId = null;
  
	 declare @Error int
 
  update Receipt
     set InboundDocumentId = isnull(@InboundDocumentId, InboundDocumentId),
         PriorityId = isnull(@PriorityId, PriorityId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         LocationId = isnull(@LocationId, LocationId),
         StatusId = isnull(@StatusId, StatusId),
         OperatorId = isnull(@OperatorId, OperatorId),
         DeliveryNoteNumber = isnull(@DeliveryNoteNumber, DeliveryNoteNumber),
         DeliveryDate = isnull(@DeliveryDate, DeliveryDate),
         SealNumber = isnull(@SealNumber, SealNumber),
         VehicleRegistration = isnull(@VehicleRegistration, VehicleRegistration),
         Remarks = isnull(@Remarks, Remarks),
         CreditAdviceIndicator = isnull(@CreditAdviceIndicator, CreditAdviceIndicator),
         Delivery = isnull(@Delivery, Delivery),
         AllowPalletise = isnull(@AllowPalletise, AllowPalletise),
         Interfaced = isnull(@Interfaced, Interfaced),
         StagingLocationId = isnull(@StagingLocationId, StagingLocationId),
         NumberOfLines = isnull(@NumberOfLines, NumberOfLines),
         ReceivingCompleteDate = isnull(@ReceivingCompleteDate, ReceivingCompleteDate),
         ParentReceiptId = isnull(@ParentReceiptId, ParentReceiptId),
         GRN = isnull(@GRN, GRN),
         PlannedDeliveryDate = isnull(@PlannedDeliveryDate, PlannedDeliveryDate),
         ShippingAgentId = isnull(@ShippingAgentId, ShippingAgentId),
         ContainerNumber = isnull(@ContainerNumber, ContainerNumber),
         AdditionalText1 = isnull(@AdditionalText1, AdditionalText1),
         AdditionalText2 = isnull(@AdditionalText2, AdditionalText2),
         BOE = isnull(@BOE, BOE),
         Incoterms = isnull(@Incoterms, Incoterms),
         ReceiptConfirmed = isnull(@ReceiptConfirmed, ReceiptConfirmed),
         ReceivingStarted = isnull(@ReceivingStarted, ReceivingStarted),
         VehicleId = isnull(@VehicleId, VehicleId),
         ParentIssueId = isnull(@ParentIssueId, ParentIssueId),
         VehicleTypeId = isnull(@VehicleTypeId, VehicleTypeId) 
   where ReceiptId = @ReceiptId
  
  select @Error = @@Error
  
  
  return @Error
  
end
