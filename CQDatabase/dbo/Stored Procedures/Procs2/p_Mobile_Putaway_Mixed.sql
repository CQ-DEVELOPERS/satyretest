﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Putaway_Mixed
    ///   Filename       : p_Mobile_Putaway_Mixed.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 18 June 2007 22:34:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store mixed pallet Items.
    /// </remarks>
    /// <param>
    ///   @type                int,
    ///   @operatorId          int,
    ///   @jobId               int,
    ///   @barcode             nvarchar(30) = null,
    ///   @storeLocation       nvarchar(15) = null,
    ///   @confirmedQuantity   float = null
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
    ///   Details        : 
    /// </newpara>
*/
create procedure p_Mobile_Putaway_Mixed
(
 @type                int,
 @operatorId          int,
 @jobId               int,
 @barcode             nvarchar(30) = null,
 @storeLocation       nvarchar(15) = null,
 @confirmedQuantity   float = null
)
as
begin
	 set nocount on
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @Rowcount           int,
          @InstructionId      int,
          @StorageUnitBatchId int,
          @PickLocationId     int,
          @ProductCode        nvarchar(30),
          @SKUCode            nvarchar(50),
          @Batch              nvarchar(50),
          @storeLocationId    int,
          @SecurityCode       int,
          @Location           nvarchar(15),
          @StatusId           int,
          @DwellTime          int,
          @Quantity           float,
          @PalletId           int,
          @CurrentLine        int,
          @TotalLines         int,
          @ReceiptLineId      int,
          @StorageUnitId      int,
          @ProductId          int,
          @ProductBarcode     nvarchar(50),
          @PackBarcode        nvarchar(50)
          
  declare @StoreAreaId        int,
          @PickAreaId         int,
          @OperatorGroupId    int,
          @NewStoreLoactionId int,
          @InstructionTypeId  int,
          @WarehouseId        int,
          @Weight             numeric(13,3)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    set @PalletId = replace(@barcode,'P:','')
  
  exec @DwellTime = p_Mobile_Dwell_Time
   @type       = 2,
   @operatorId = @operatorId
  
  begin transaction
  
--  if @type = 0
  begin
    select top 1
           @jobId              = j.JobId,
           @InstructionId      = i.InstructionId,
           @StorageUnitBatchId = i.StorageUnitBatchId,
           @PickLocationId     = i.PickLocationId,
           @storeLocationId    = i.StoreLocationId,
           @StatusId           = i.StatusId,
           @Quantity           = i.Quantity,
           @InstructionTypeId  = i.InstructionTypeId,
           @WarehouseId        = i.WarehouseId,
           @Weight             = i.Weight,
           @PalletId           = i.PalletId,
           @ReceiptLineId      = i.ReceiptLineId
      from Job              j (nolock)
      join Instruction      i (nolock) on j.JobId = i.JobId
      join Status           s (nolock) on i.StatusId = s.StatusId
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where j.JobId       = @jobId
       and isnull(j.OperatorId, @operatorId) = @operatorId
       and s.Type        = 'I'
       and s.StatusCode in ('W','S')
       and it.InstructionTypeCode = 'SM'
    
    select @Rowcount = @@rowcount
  end
--  else
--  begin
--    if @barcode like 'P:%'
--    begin
--      select top 1
--             @InstructionId      = InstructionId,
--             @StorageUnitBatchId = StorageUnitBatchId,
--             @PickLocationId     = PickLocationId,
--             @storeLocationId    = StoreLocationId,
--             @StatusId           = StatusId,
--             @Quantity           = Quantity,
--             @InstructionTypeId  = InstructionTypeId,
--             @WarehouseId        = WarehouseId,
--             @Weight             = Weight
--        from Instruction
--       where PalletId                        = @PalletId
--         and isnull(OperatorId, @operatorId) = @operatorId
--         and JobId                           = @jobId
--      
--      select @Rowcount = @@rowcount
--    end
--    else
--    begin
--      select top 1
--             @InstructionId      = i.InstructionId,
--             @StorageUnitBatchId = i.StorageUnitBatchId,
--             @PickLocationId     = i.PickLocationId,
--             @storeLocationId    = i.StoreLocationId,
--             @StatusId           = i.StatusId,
--             @Quantity           = i.Quantity,
--             @InstructionTypeId  = i.InstructionTypeId,
--             @WarehouseId        = i.WarehouseId,
--             @Weight             = i.Weight,
--             @PalletId           = i.PalletId
--        from Instruction        i (nolock)
--        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
--        join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
--        join Product            p (nolock) on su.ProductId         = p.ProductId
--       where p.Barcode                       = @barcode
--         and isnull(OperatorId, @operatorId) = @operatorId
--         and i.JobId                         = @jobId
--      order by i.InstructionId
--      
--      select @Rowcount = @@rowcount
--    end
    
    if (select StatusCode
          from Status (nolock)
         where Type = 'I'
           and StatusId = @StatusId) not in ('W','S')
    begin
      set @Error = 4
      goto result
    end
    
    if @confirmedQuantity > @Quantity
    begin
      set @Error = 7
      goto result
    end
    
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    select @SecurityCode  = SecurityCode,
           @storeLocation = Location
      from Location (nolock)
     where LocationId = @storeLocationId
--  end
  
  if @type = 0
  begin
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'S'
    
    exec @Error = p_Job_Update
     @jobId      = @jobId,
     @StatusId   = @StatusId,
     @operatorId = @operatorId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  if @type = 0 -- Get Pallet Info
  begin
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'S'
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @operatorId    = @operatorId,
     @StartDate     = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    if @storeLocationId is not null
    begin
--      select @PickAreaId = AreaId
--        from AreaLocation
--       where LocationId = @PickLocationId
--      
--      select @StoreAreaId = AreaId
--        from AreaLocation
--       where LocationId = @storeLocationId
--      
--      set @OperatorGroupId = dbo.ufn_OperatorGroupId(@operatorId) 
--      
--      select top 1
--             @SecurityCode  = l.SecurityCode,
--             @storeLocation = l.Location,
--             @NewStoreLoactionId = l.LocationId
--        from MovementSequence ms (nolock)
--        join AreaLocation     al (nolock) on ms.StageAreaId = al.AreaId
--        join Location          l (nolock) on al.LocationId  = l.LocationId
--       where PickAreaId      = @PickAreaId
--         and StoreAreaId     = @StoreAreaId
--         and OperatorGroupId = @OperatorGroupId
--      
--      set @StatusId = dbo.ufn_StatusId('I','W')
--      
--      exec @Error = p_Instruction_Insert
--       @InstructionTypeId   = @InstructionTypeId,
--       @StorageUnitBatchId  = @StorageUnitBatchId,
--       @WarehouseId         = @WarehouseId,
--       @StatusId            = @StatusId,
--       @jobId               = @jobId,
--       @operatorId          = @operatorId,
--       @PickLocationId      = @PickLocationId,
--       @storeLocationId     = @storeLocationId,
--       @InstructionRefId    = @InstructionId,
--       @Quantity            = @confirmedQuantity,
--       @confirmedQuantity   = 0,
--       @Weight              = @Weight,
--       @ConfirmedWeight     = 0,
--       @PalletId            = @PalletId,
--       @CreateDate          = @GetDate
--      
--      if @Error <> 0
--      begin
--        set @Error = 1
--        goto Result
--      end
--      
--      exec @Error = p_Instruction_Update
--       @InstructionId = @InstructionId,
--       @storeLocationId = @NewStoreLoactionId
--      
--      if @Error <> 0
--      begin
--        set @Error = 1
--        goto Result
--      end
      
      select @SecurityCode  = SecurityCode,
             @storeLocation = Location
        from Location (nolock)
       where LocationId = @storeLocationId
    end
    else
    begin
      select 'Must get Location???'
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId       = @InstructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    -- ALL READY DONE UP TOP!!!
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @type = 1 -- Cancel Putaway
  begin
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'W'
    
    exec @Error = p_Job_Update
     @jobId      = @jobId,
     @StatusId   = @StatusId,
     @operatorId = null
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @operatorId    = null,
     @StartDate     = null
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @InstructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @type = 2 -- Product Picked
  begin
    if @confirmedQuantity < @Quantity
    begin
      exec @Error = p_StorageUnitBatchLocation_Deallocate
       @InstructionId = @InstructionId
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      exec @Error = p_Instruction_Update
       @InstructionId     = @InstructionId,
       @confirmedQuantity = @confirmedQuantity
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      exec @Error = p_StorageUnitBatchLocation_Reserve
       @InstructionId       = @InstructionId,
       @Confirmed           = 1 -- true
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
    end
    
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId = @InstructionId,
     @Store         = 0, -- false
     @Confirmed     = 1  -- true
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @type = 3 -- Validate Store Location
  begin
    if (@storeLocation like 'L:%')
      set @storeLocation = replace(@storeLocation,'L:','')
    
    select @SecurityCode  = SecurityCode,
           @Location = Location
      from Location (nolock)
     where LocationId = @storeLocationId
    
    if @storeLocation != @Location
    and @storeLocation != convert(nvarchar(15), @SecurityCode)
    begin
      set @Error = 6
      goto Result
    end
    
    set @Error = 0
    goto Result
  end
  else if @type = 4 -- Product Binned
  begin
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'F' 
    
    exec @Error = p_Instruction_Update
     @InstructionId     = @InstructionId,
     @StatusId          = @StatusId,
     @EndDate           = @GetDate,
     @ConfirmedQuantity = @confirmedQuantity
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId = @InstructionId,
     @Pick          = 0, -- false
     @Confirmed     = 1  -- true
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
  end
  else if @type = 5 -- Location Error
  begin
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId       = @InstructionId,
     @Pick                = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec p_Location_Get
     @WarehouseId        = @WarehouseId,
     @LocationId         = @StoreLocationId output,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @Quantity           = @Quantity
    
    exec @Error = p_Instruction_Update
     @InstructionId      = @InstructionId,
     @StoreLocationId    = @StoreLocationId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId       = @InstructionId,
     @Pick                = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    select @SecurityCode  = SecurityCode,
           @StoreLocation = Location
      from Location (nolock)
     where LocationId = @StoreLocationId
    
--    exec @Error = p_Mobile_Exception_Stock_Take
--     @OperatorId          = @OperatorId,
--     @StorageUnitBatchId  = 1,
--     @PickLocationId      = @PickLocationId,
--     @Quantity            = @Quantity
--    
--    if @Error <> 0
--    begin
--      set @Error = 1
--      goto Result
--    end
    
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 6 -- Confirm Quantity
  begin
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    if @ConfirmedQuantity > @Quantity
    begin
      set @Error = 7
      goto Result
    end
    else if @ConfirmedQuantity = @Quantity
    begin
      exec @Error = p_Instruction_Update
       @InstructionId       = @InstructionId,
       @ConfirmedQuantity   = @ConfirmedQuantity
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      set @Error = 0
      goto Result
    end
    else
    begin
      exec @Error = p_StorageUnitBatchLocation_Deallocate
       @InstructionId       = @InstructionId,
       @Pick                = 0 -- false
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      exec @Error = p_Instruction_Update
       @InstructionId       = @InstructionId,
       @ConfirmedQuantity   = @ConfirmedQuantity
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      exec @Error = p_StorageUnitBatchLocation_Reserve
       @InstructionId       = @InstructionId,
       @Pick                = 0 -- false
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      set @StatusId = dbo.ufn_StatusId('I','W')
      
      set @Quantity = @Quantity - @ConfirmedQuantity
      
      exec @Error = p_Instruction_Insert
       @InstructionId       = @InstructionId output,
       @InstructionTypeId   = @InstructionTypeId,
       @StorageUnitBatchId  = @StorageUnitBatchId,
       @WarehouseId         = @WarehouseId,
       @StatusId            = @StatusId,
       @JobId               = @JobId,
       @OperatorId          = @OperatorId,
       @PickLocationId      = @PickLocationId,
       @StoreLocationId     = @StoreLocationId,
       @ReceiptLineId       = @ReceiptLineId,
       @InstructionRefId    = @InstructionId,
       @Quantity            = @Quantity,
       @ConfirmedQuantity   = 0,
       @Weight              = @Weight,
       @ConfirmedWeight     = 0,
       @PalletId            = @PalletId,
       @CreateDate          = @GetDate
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      if (select dbo.ufn_Configuration(21, @WarehouseId)) = 1
      begin
        exec p_Location_Get
         @WarehouseId        = @WarehouseId,
         @LocationId         = @StoreLocationId output,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @Quantity           = @Quantity
        
        exec @Error = p_Instruction_Update
         @InstructionId      = @InstructionId,
         @StoreLocationId    = @StoreLocationId
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
        
        exec @Error = p_StorageUnitBatchLocation_Reserve
         @InstructionId       = @InstructionId,
         @Pick                = 0 -- false
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
      end
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 8 -- Confirm Product
  begin
    if @PalletId is not null
    begin
      if @StorageUnitBatchId = (select StorageUnitBatchId
                                  from Pallet
                                 where PalletId = @PalletId)
      begin
        goto Result
      end
      else
      begin
        set @Error = 3
        set @Errormsg = 'Incorrect barcode'
        goto result
      end
    end
    
    if @Barcode is null
    begin
      set @Error = 3
      set @Errormsg = 'Incorrect barcode'
      goto result
    end
    
    select @StorageUnitId = StorageUnitId
      from StorageUnitBatch (nolock)
     where StorageUnitBatchId = @StorageUnitBatchId
    
    select @ProductId = ProductId
      from StorageUnit (nolock)
     where StorageUnitId = @StorageUnitId
    
    select @ProductBarcode = Barcode
      from Product (nolock)
     where ProductId = @ProductId
    
    if @ProductBarcode != @barcode
    begin
      select @PackBarcode = Barcode
        from Pack
       where StorageUnitId = @StorageUnitId
         and Barcode = @barcode
      
      if @PackBarcode is null
      begin
        set @Error = 3
        set @Errormsg = 'Incorrect barcode'
        goto result
      end
    end
  end
  
  Result:
    if @Error <> 0
      rollback transaction
    else
      commit transaction
    
    select @CurrentLine = Count(1)
      from Instruction i (nolock)
      join Status      s (nolock) on i.StatusId = s.StatusId
     where i.JobId = @jobId
       and s.Type = 'I'
       and s.StatusCode = 'F'
    
    if @CurrentLine = 0
      set @CurrentLine = 1
    
    select @TotalLines = Count(1)
      from Instruction (nolock)
     where JobId = @jobId
    
    select @Error              as 'Result',
           @DwellTime          as 'DwellTime',
           @jobId              as 'JobId',
           @ProductCode        as 'ProductCode',
           @SKUCode            as 'SKUCode',
           @Batch              as 'Batch',
           @storeLocation      as 'StoreLocation',
           @SecurityCode       as 'SecurityCode',
           isnull(@Quantity,0) as 'Quantity',
           @CurrentLine        as 'CurrentLine',
           @TotalLines         as 'TotalLines'
end
