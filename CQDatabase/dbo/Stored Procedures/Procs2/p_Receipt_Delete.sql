﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receipt_Delete
  ///   Filename       : p_Receipt_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jul 2014 13:28:37
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Receipt table.
  /// </remarks>
  /// <param>
  ///   @ReceiptId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receipt_Delete
(
 @ReceiptId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Receipt
     where ReceiptId = @ReceiptId
  
  select @Error = @@Error
  
  
  return @Error
  
end
