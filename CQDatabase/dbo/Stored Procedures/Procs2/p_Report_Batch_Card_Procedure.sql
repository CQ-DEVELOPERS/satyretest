﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Batch_Card_Procedure
  ///   Filename       : p_Report_Batch_Card_Procedure.sql
  ///   Create By      : Karen
  ///   Date Created   : January 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Batch_Card_Procedure
(
 @BOMHeaderId            int
)

as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   Remarks			nvarchar(max)
  ) 
  
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine1
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine1 is not null
	and bsi.ProcedureLine1 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine2
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine2 is not null
	and bsi.ProcedureLine2 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine3
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine3 is not null
	and bsi.ProcedureLine3 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine4
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine4 is not null
	and bsi.ProcedureLine4 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine5
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine5 is not null
	and bsi.ProcedureLine5 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine6
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine6 is not null
	and bsi.ProcedureLine6 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine7
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine7 is not null
	and bsi.ProcedureLine7 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine8
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine8 is not null
	and bsi.ProcedureLine8 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine9
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine9 is not null
	and bsi.ProcedureLine9 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine10
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine10 is not null
	and bsi.ProcedureLine10 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine11
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine11 is not null
	and bsi.ProcedureLine11 != ''
	
	insert @TableHeader
		   (Remarks)
	SELECT	top 1 bsi.ProcedureLine12
	FROM	BOMSpecialInstruction   bsi (nolock)
	join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	where   bh.BOMHeaderId = @BOMHeaderId
	and bsi.ProcedureLine12 is not null
	and bsi.ProcedureLine12 != ''
			
	--SELECT	top 1 bsi.ProcedureLine1,
	--	    bsi.ProcedureLine2,   
	--	    bsi.ProcedureLine3,
	--	    bsi.ProcedureLine4,
	--	    bsi.ProcedureLine5,
	--	    bsi.ProcedureLine6,
	--	    bsi.ProcedureLine7,
	--	    bsi.ProcedureLine8,
	--	    bsi.ProcedureLine9,
	--	    bsi.ProcedureLine10,
	--	    bsi.ProcedureLine11,
	--	    bsi.ProcedureLine12 
	--FROM	BOMSpecialInstruction   bsi (nolock)
	--join    BOMHeader				bh (nolock) on bsi.BOMHeaderId = bh.BOMHeaderId      
	--where   bh.BOMHeaderId = @BOMHeaderId
    
    select * from @TableHeader
  
end
