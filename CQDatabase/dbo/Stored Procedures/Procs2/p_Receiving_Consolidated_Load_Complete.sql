﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Consolidated_Load_Complete
  ///   Filename       : p_Receiving_Consolidated_Load_Complete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Mar 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Consolidated_Load_Complete
(
 @ReceiptId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @InboundShipmentId int,
          @RequiredQuantity  float,
          @ReceivedQuantity  float,
          @count             int,
          @Id                int,
          @StorageUnitId     int,
          @ReceiptLineId     int,
          @Quantity          float,
          @Tran              bit = 1,
          @DeliveryNoteNumber nvarchar(30)
  
  declare @TableResult as table
  (
   ReceiptId        int,
   ReceiptLineId    int,
   StorageUnitId    int,
   RequiredQuantity float,
   ReceivedQuantity float
  )
  
  declare @TableSummary as table
  (
   Id               int primary key identity,
   StorageUnitId    int,
   ReceivedQuantity float,
   UsedQuantity float
  )
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Error = 0
  
  if @@trancount > 0
    set @Tran = 0
  
  if @Tran = 1
    begin transaction
  
  select @InboundShipmentId = id.ReferenceNumber,
         @DeliveryNoteNumber = r.DeliveryNoteNumber
    from Receipt r
    join InboundDocument id on r.InboundDocumentId = id.InboundDocumentId
   where r.ReceiptId = @ReceiptId
     and isnumeric(id.ReferenceNumber) = 1
  
  if @InboundShipmentId is null
  begin
    set @InboundShipmentId = -1
    goto error
  end
  
  update r
     set DeliveryNoteNumber = @DeliveryNoteNumber
    from InboundShipmentReceipt isr (nolock)
    join Receipt                  r (nolock) on isr.ReceiptId = r.ReceiptId
   where isr.InboundShipmentId = @InboundShipmentId
  
  --insert @TableResult
  --      (ReceiptLineId,
  --       StorageUnitId,
  --       RequiredQuantity)
  --select rl.ReceiptLineId,
  --       sub.StorageUnitId,
  --       rl.RequiredQuantity
  --  from InboundShipmentReceipt isr (nolock)
  --  join ReceiptLine             rl (nolock) on isr.ReceiptId = rl.ReceiptId
  --  join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId =  sub.StorageUnitBatchId
  -- where isr.InboundShipmentId = @InboundShipmentId
  
  insert @TableResult
        (ReceiptId,
         ReceiptLineId,
         StorageUnitId,
         RequiredQuantity)
  select rl.ReceiptId,
         rl.ReceiptLineId,
         sub.StorageUnitId,
         rl.RequiredQuantity
    from InboundShipmentReceipt isr (nolock)
    join ReceiptLine             rl (nolock) on isr.ReceiptId = rl.ReceiptId
    join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId =  sub.StorageUnitBatchId
   where isr.InboundShipmentId = @InboundShipmentId
  
  select @Error = @@Error
  
  insert @TableSummary
        (StorageUnitId,
         ReceivedQuantity,
         UsedQuantity)
  select sub.StorageUnitId,
         sum(rl.AcceptedQuantity),
         0
    from ReceiptLine             rl (nolock)
    join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId =  sub.StorageUnitBatchId
   where rl.ReceiptId = @ReceiptId
  group by sub.StorageUnitId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  while exists(select top 1 1 
                 from @TableSummary
                where ReceivedQuantity > UsedQuantity)
  begin
    set @Id = null
    
    select @Id = Id,
           @StorageUnitId = StorageUnitId,
           @ReceivedQuantity = ReceivedQuantity
      from @TableSummary
     where ReceivedQuantity > UsedQuantity
    
    --delete @TableSummary where Id = @Id
    
    set @ReceiptLineId = null
    
    select @ReceiptLineId = ReceiptLineId,
           @RequiredQuantity = RequiredQuantity
      from @TableResult
     where StorageUnitId = @StorageUnitId
       and RequiredQuantity > isnull(ReceivedQuantity,0)
    order by RequiredQuantity desc
    
    if @ReceivedQuantity > @RequiredQuantity
      set @Quantity = @RequiredQuantity
    else 
      set @Quantity = @ReceivedQuantity
    
    update @TableResult
       set ReceivedQuantity = isnull(ReceivedQuantity,0) + isnull(@Quantity,0)
     where ReceiptLineId = @ReceiptLineId
    
    update @TableSummary
       set UsedQuantity = isnull(UsedQuantity,0) + isnull(@Quantity,0)
     where Id = @Id
  end
  
  update rl
     set ReceivedQuantity = tr.ReceivedQuantity,
         AcceptedQuantity = tr.ReceivedQuantity,
         StatusId         = dbo.ufn_StatusId('R','R')
    from @TableResult tr
    join ReceiptLine  rl on tr.ReceiptLineId = rl.ReceiptLineId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update r
     set StatusId = dbo.ufn_StatusId('R','R')
    from @TableResult tr
    join Receipt       r on tr.ReceiptId = r.ReceiptId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Inbound_Release @InboundShipmentId = @InboundShipmentId, @StatusCode = 'RC'
  
  if @Error <> 0
    goto error
  
  if @Tran = 1
    commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Receiving_Consolidated_Load_Complete'); 
    if @Tran = 1
      rollback transaction
    return @Error
end
