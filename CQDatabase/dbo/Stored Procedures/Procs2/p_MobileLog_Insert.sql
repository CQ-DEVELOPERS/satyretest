﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MobileLog_Insert
  ///   Filename       : p_MobileLog_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:09
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the MobileLog table.
  /// </remarks>
  /// <param>
  ///   @MobileLogId int = null output,
  ///   @ProcName nvarchar(256) = null,
  ///   @WarehouseId int = null,
  ///   @InstructionId int = null,
  ///   @ReferenceNumber nvarchar(100) = null,
  ///   @JobId int = null,
  ///   @PalletId int = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @Pick nvarchar(100) = null,
  ///   @Store nvarchar(100) = null,
  ///   @StorageUnitId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @PickLocationId int = null,
  ///   @StoreLocationId int = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @OperatorId int = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null,
  ///   @ErrorMsg varchar(max) = null,
  ///   @StatusCode varchar(10) = null 
  /// </param>
  /// <returns>
  ///   MobileLog.MobileLogId,
  ///   MobileLog.ProcName,
  ///   MobileLog.WarehouseId,
  ///   MobileLog.InstructionId,
  ///   MobileLog.ReferenceNumber,
  ///   MobileLog.JobId,
  ///   MobileLog.PalletId,
  ///   MobileLog.Barcode,
  ///   MobileLog.Pick,
  ///   MobileLog.Store,
  ///   MobileLog.StorageUnitId,
  ///   MobileLog.StorageUnitBatchId,
  ///   MobileLog.PickLocationId,
  ///   MobileLog.StoreLocationId,
  ///   MobileLog.Batch,
  ///   MobileLog.Quantity,
  ///   MobileLog.OperatorId,
  ///   MobileLog.StartDate,
  ///   MobileLog.EndDate,
  ///   MobileLog.ErrorMsg,
  ///   MobileLog.StatusCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MobileLog_Insert
(
 @MobileLogId int = null output,
 @ProcName nvarchar(256) = null,
 @WarehouseId int = null,
 @InstructionId int = null,
 @ReferenceNumber nvarchar(100) = null,
 @JobId int = null,
 @PalletId int = null,
 @Barcode nvarchar(100) = null,
 @Pick nvarchar(100) = null,
 @Store nvarchar(100) = null,
 @StorageUnitId int = null,
 @StorageUnitBatchId int = null,
 @PickLocationId int = null,
 @StoreLocationId int = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @OperatorId int = null,
 @StartDate datetime = null,
 @EndDate datetime = null,
 @ErrorMsg varchar(max) = null,
 @StatusCode varchar(10) = null 
)

as
begin
	 set nocount on;
  
  if @MobileLogId = '-1'
    set @MobileLogId = null;
  
	 declare @Error int
 
  insert MobileLog
        (ProcName,
         WarehouseId,
         InstructionId,
         ReferenceNumber,
         JobId,
         PalletId,
         Barcode,
         Pick,
         Store,
         StorageUnitId,
         StorageUnitBatchId,
         PickLocationId,
         StoreLocationId,
         Batch,
         Quantity,
         OperatorId,
         StartDate,
         EndDate,
         ErrorMsg,
         StatusCode)
  select @ProcName,
         @WarehouseId,
         @InstructionId,
         @ReferenceNumber,
         @JobId,
         @PalletId,
         @Barcode,
         @Pick,
         @Store,
         @StorageUnitId,
         @StorageUnitBatchId,
         @PickLocationId,
         @StoreLocationId,
         @Batch,
         @Quantity,
         @OperatorId,
         @StartDate,
         @EndDate,
         @ErrorMsg,
         @StatusCode 
  
  select @Error = @@Error, @MobileLogId = scope_identity()
  
  
  return @Error
  
end
