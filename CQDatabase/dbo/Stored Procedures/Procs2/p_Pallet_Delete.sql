﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Delete
  ///   Filename       : p_Pallet_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:30
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Pallet table.
  /// </remarks>
  /// <param>
  ///   @PalletId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Delete
(
 @PalletId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Pallet
     where PalletId = @PalletId
  
  select @Error = @@Error
  
  
  return @Error
  
end
