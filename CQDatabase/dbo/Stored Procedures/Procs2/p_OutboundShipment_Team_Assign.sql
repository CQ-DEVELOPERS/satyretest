﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipment_Team_Assign
  ///   Filename       : p_OutboundShipment_Team_Assign.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipment_Team_Assign
(
 @outboundShipmentId int,
 @instructionTypeId int,
 @operatorGroupId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @requiredStart     datetime,
          @requiredEnd       datetime,
          @LocationId        int,
          @OperatorGroup     nvarchar(30)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @LocationId = LocationId
    from OutboundShipment (nolock)
   where OutboundShipmentId = @outboundShipmentId
  
  select @OperatorGroup = OperatorGroup
    from OperatorGroup (nolock)
   where OperatorGroupId = @operatorGroupId
  
  begin transaction
  
  exec p_OutboundShipment_Team_Result
   @outboundShipmentId=@outboundShipmentId,
   @instructionTypeId=@instructionTypeId,
   @operatorGroupId=@operatorGroupId,
   @requiredStart = @requiredStart output,
   @requiredEnd = @requiredEnd output
  
  update OutboundShipmentInstructionType
     set OperatorGroupId = null
   where OutboundShipmentId = @OutboundShipmentId
     and InstructionTypeId = @InstructionTypeId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  update OutboundShipmentInstructionType
     set OperatorGroupId = @OperatorGroupId
   where OutboundShipmentId = @OutboundShipmentId
     and InstructionTypeId = @InstructionTypeId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  delete TeamSchedule
   where OutboundShipmentId = @OutboundShipmentId
     and InstructionTypeId = @InstructionTypeId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  insert TeamSchedule
        (OutboundShipmentId,
         OperatorGroupId,
         InstructionTypeId,
         Subject,
         PlannedStart,
         PlannedEnd,
         LocationId,
         Description)
  select @OutboundShipmentId,
         @OperatorGroupId,
         @InstructionTypeId,
         'TEAM: ' + @OperatorGroup + char(13) + ' Shipment: ' + convert(nvarchar(10), @outboundShipmentId),
         @requiredStart,
         @requiredEnd,
         @LocationId,
         null
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_OutboundShipment_Team_Assign'); 
    rollback transaction
    return @Error
end
