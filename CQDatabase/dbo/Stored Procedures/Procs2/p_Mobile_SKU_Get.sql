﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_SKU_Get
  ///   Filename       : p_Mobile_SKU_Get.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_SKU_Get
(
 @WarehouseId int,
 @Barcode     nvarchar(30),
 @SKUCode     nvarchar(50) = null output,
 @Quantity    float = null output
)

as
begin
	 set nocount on;
  
  declare @Error           int,
          @Errormsg        nvarchar(500),
          @GetDate         datetime,
          @JobId           int,
          @StatusCode      nvarchar(10),
          @PalletId        int,
          @ReferenceNumber nvarchar(30),
          @SKUId           int
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Error = 0
  
  if isnumeric(@barcode) = 1
    set @barcode = 'J:' + @barcode
  
  if isnumeric(replace(@barcode,'J:','')) = 1
  begin
    -- First Check if Inter Branch Transfer Putaway
    select @JobId = max(j.JobId)
      from Job         j (nolock)
      join Instruction i (nolock) on j.JobId = i.JobId
     where j.ReferenceNumber = @barcode
       and j.WarehouseId     = @WarehouseId
    
    if @JobId is null
      select @JobId           = replace(@barcode,'J:',''),
             @barcode = null
    
    select top 1
           @JobId      = j.JobId,
           @StatusCode = s.StatusCode
      from Job    j (nolock)
      join Status s (nolock) on j.StatusId = s.StatusId
     where j.JobId = @JobId
       and j.WarehouseId     = @WarehouseId
  end
  else if isnumeric(replace(@barcode,'P:','')) = 1
  begin
    select @PalletId        = replace(@barcode,'P:',''),
           @barcode = null
    
    select top 1
           @JobId      = j.JobId,
           @StatusCode = s.StatusCode
      from Instruction            i (nolock)
      join Job                    j (nolock) on i.JobId = j.JobId
      join Status                 s (nolock) on j.StatusId = s.StatusId
     where i.PalletId = @PalletId
       and s.StatusCode in ('CK','CD')
       and j.WarehouseId     = @WarehouseId
  end
  else if isnumeric(replace(@barcode,'R:','')) = 1
    select @ReferenceNumber = @barcode,
           @barcode         = null
  begin
    select top 1
           @JobId      = j.JobId,
           @StatusCode = s.StatusCode
      from Job    j (nolock)
      join Status s (nolock) on j.StatusId = s.StatusId
     where j.ReferenceNumber = @ReferenceNumber
       and j.WarehouseId     = @WarehouseId
  end
  
  if @JobId is null
  begin
    set @Error = 900006
    set @ErrorMsg = 'Could not find the pallet'
    goto error
  end
  
  if @StatusCode = 'C'
  begin
    set @Error = 900005
    set @ErrorMsg = 'The pallet is already complete'
    goto error
  end
  
  if @StatusCode in ('D','A')
  begin
    set @Error = 900008
    set @ErrorMsg = 'The Pallet has been checked'
    goto error
  end
  
  if @StatusCode = 'QA'
  begin
    set @Error = 900009
    set @ErrorMsg = 'The Pallet is in QA'
    goto error
  end
  
  if @StatusCode not in ('CK','PR')
  begin
    set @Error = 900003
    set @ErrorMsg = 'Incorrect Status'
    goto error
  end
  
  if not exists(select top 1 1 from SKUCheck where JobId = @JobId)
  begin
    insert SKUCheck
          (JobId,
           SKUCode,
           Quantity,
           ConfirmedQuantity)
    select i.JobId,
           'SKU''s',
           count(distinct(isnull(sku.ParentSKUCode, sku.SKUCode))),
           null
      from Instruction        i (nolock)
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join SKU              sku (nolock) on su.SKUId             = sku.SKUId
     where i.JobId = @JobId
       and i.ConfirmedQuantity > 0 
    group by i.JobId
    
    insert SKUCheck
          (JobId,
           SKUCode,
           Quantity,
           ConfirmedQuantity)
    select i.JobId,
           isnull(sku.ParentSKUCode, sku.SKUCode),
           sum(i.ConfirmedQuantity),
           min(sku.Quantity)
      from Instruction        i (nolock)
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join SKU              sku (nolock) on su.SKUId             = sku.SKUId
     where i.JobId = @JobId
       and i.ConfirmedQuantity > 0 
    group by i.JobId,
             isnull(sku.ParentSKUCode, sku.SKUCode)
    order by isnull(sku.ParentSKUCode, sku.SKUCode)
    
    update SKUCheck
       set ConfirmedQuantity = null
      where JobId = @JobId
  end
  
  select top 1
         @SKUCode  = SKUCode,
         @Quantity = Quantity
    from SKUCheck (nolock)
   where JobId = @JobId
     and ConfirmedQuantity is null
  
  if @SKUCode is null
    set @SKUCode = ''
  
  if @Quantity is null
    set @Quantity = 0
  
  select @Error
  return @Error
  
  error:
    if @SKUCode is null
      set @SKUCode = ''
    
    if @Quantity is null
      set @Quantity = 0
    
    select @Error
    return @Error
end
