﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pack_Delete
  ///   Filename       : p_Pack_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Nov 2012 14:41:25
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Pack table.
  /// </remarks>
  /// <param>
  ///   @PackId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pack_Delete
(
 @PackId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Pack
     where PackId = @PackId
  
  select @Error = @@Error
  
  
  return @Error
  
end
