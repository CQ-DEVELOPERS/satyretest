﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductCategory_Insert
  ///   Filename       : p_ProductCategory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:58
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ProductCategory table.
  /// </remarks>
  /// <param>
  ///   @ProductCategoryId int = null output,
  ///   @ProductCategoryType nvarchar(100) = null,
  ///   @ProductType nvarchar(40) = null,
  ///   @ProductCategory char(1) = null,
  ///   @PackingCategory char(1) = null,
  ///   @StackingCategory int = null,
  ///   @MovementCategory int = null,
  ///   @ValueCategory int = null,
  ///   @StoringCategory int = null 
  /// </param>
  /// <returns>
  ///   ProductCategory.ProductCategoryId,
  ///   ProductCategory.ProductCategoryType,
  ///   ProductCategory.ProductType,
  ///   ProductCategory.ProductCategory,
  ///   ProductCategory.PackingCategory,
  ///   ProductCategory.StackingCategory,
  ///   ProductCategory.MovementCategory,
  ///   ProductCategory.ValueCategory,
  ///   ProductCategory.StoringCategory 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductCategory_Insert
(
 @ProductCategoryId int = null output,
 @ProductCategoryType nvarchar(100) = null,
 @ProductType nvarchar(40) = null,
 @ProductCategory char(1) = null,
 @PackingCategory char(1) = null,
 @StackingCategory int = null,
 @MovementCategory int = null,
 @ValueCategory int = null,
 @StoringCategory int = null 
)

as
begin
	 set nocount on;
  
  if @ProductCategoryId = '-1'
    set @ProductCategoryId = null;
  
  if @ProductCategory = '-1'
    set @ProductCategory = null;
  
	 declare @Error int
 
  insert ProductCategory
        (ProductCategoryType,
         ProductType,
         ProductCategory,
         PackingCategory,
         StackingCategory,
         MovementCategory,
         ValueCategory,
         StoringCategory)
  select @ProductCategoryType,
         @ProductType,
         @ProductCategory,
         @PackingCategory,
         @StackingCategory,
         @MovementCategory,
         @ValueCategory,
         @StoringCategory 
  
  select @Error = @@Error, @ProductCategoryId = scope_identity()
  
  
  return @Error
  
end
