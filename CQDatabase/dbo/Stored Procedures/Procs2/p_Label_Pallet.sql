﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Label_Pallet
  ///   Filename       : p_Label_Pallet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Label_Pallet
(
 @Value              int output,
 @StorageUnitBatchId int = null,
 @LocationId         int = null,
 @Quantity           float = null
)

as
begin
	 set nocount on;
	 
	 if @StorageUnitBatchId = -1
	   set @StorageUnitBatchId = null
	 
  if @LocationId = -1
    set @LocationId = null
  
  if @Quantity = -1
    set @Quantity = null
  
	 declare @Error int
	 
  exec @Error = p_Pallet_Insert
   @PalletId           = @Value output,
   @StorageUnitBatchId = @StorageUnitBatchId,
   @LocationId         = @LocationId,
   @Weight             = null,
   @Tare               = null,
   @Quantity           = @Quantity,
   @Prints             = 1
  
  if @@Error <> 0
    return -1
  else
    return @Value
end
