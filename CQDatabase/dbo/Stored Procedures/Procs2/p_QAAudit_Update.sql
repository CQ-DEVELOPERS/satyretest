﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QAAudit_Update
  ///   Filename       : p_QAAudit_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:06
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the QAAudit table.
  /// </remarks>
  /// <param>
  ///   @QAAuditId int = null,
  ///   @JobId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @OrderedQuantity float = null,
  ///   @PickedQuantity float = null,
  ///   @CheckedQuantity float = null,
  ///   @WHQuantity float = null,
  ///   @PickedBy int = null,
  ///   @PickedDate datetime = null,
  ///   @CheckedBy int = null,
  ///   @CheckedDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QAAudit_Update
(
 @QAAuditId int = null,
 @JobId int = null,
 @StorageUnitBatchId int = null,
 @OrderedQuantity float = null,
 @PickedQuantity float = null,
 @CheckedQuantity float = null,
 @WHQuantity float = null,
 @PickedBy int = null,
 @PickedDate datetime = null,
 @CheckedBy int = null,
 @CheckedDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @QAAuditId = '-1'
    set @QAAuditId = null;
  
	 declare @Error int
 
  update QAAudit
     set JobId = isnull(@JobId, JobId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         OrderedQuantity = isnull(@OrderedQuantity, OrderedQuantity),
         PickedQuantity = isnull(@PickedQuantity, PickedQuantity),
         CheckedQuantity = isnull(@CheckedQuantity, CheckedQuantity),
         WHQuantity = isnull(@WHQuantity, WHQuantity),
         PickedBy = isnull(@PickedBy, PickedBy),
         PickedDate = isnull(@PickedDate, PickedDate),
         CheckedBy = isnull(@CheckedBy, CheckedBy),
         CheckedDate = isnull(@CheckedDate, CheckedDate) 
   where QAAuditId = @QAAuditId
  
  select @Error = @@Error
  
  
  return @Error
  
end
