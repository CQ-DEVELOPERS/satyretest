﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_General_Error
  ///   Filename       : p_Mobile_General_Error.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Sep 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_General_Error
(
 @instructionId   int,
 @pickError       bit = 0,
 @storeError      bit = 0
)

as
begin
	 set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         InstructionId,
         Pick,
         Store,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @InstructionId,
         @pickError,
         @storeError,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @InstructionTypeCode nvarchar(10),
          @WarehouseId         int,
          @StorageUnitBatchId  int,
          @Quantity            float,
          @OldPickLocationId   int,
          @StoreLocationId     int,
          @OperatorId          int,
          @ErrorId             int,
          @JobId               int,
          @Operator            nvarchar(50),
          @MoveId              int
  
  select @GetDate = dbo.ufn_Getdate()
  select @ErrorId = dbo.ufn_StatusId('IS','E')
  
  set @Error = 0
  
  select @InstructionTypeCode = it.InstructionTypeCode,
         @WarehouseId         = i.WarehouseId,
         @Quantity            = i.Quantity,
         @StorageUnitBatchId  = i.StorageUnitBatchId,
         @OldPickLocationId   = i.PickLocationId,
         @StoreLocationId     = i.StoreLocationId,
         @OperatorId          = i.OperatorId,
         @Operator            = o.Operator,
         @JobId               = i.JobId
    from Instruction      i (nolock)
    left
    join Operator         o (nolock) on i.OperatorId = o.OperatorId
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status           s (nolock) on i.StatusId          = s.StatusId
   where i.InstructionId = @instructionId
  
  if @@Rowcount = 0
  begin
    set @Error = 0
    goto result
  end
  
  if @storeError = 1
  begin
    if exists(select top 1 1
                from Job    j (nolock)
                join Status s (nolock) on j.StatusId = s.StatusId
               where JobId = @JobId
                 and s.StatusCode in ('S','RL'))
    begin
      exec p_Job_Update
       @JobId    = @JobId,
       @StatusId = @ErrorId
      
      if @Error <> 0
        goto result
    end
    
    if @Error <> 0
    begin
      set @Error = 1
      goto result
    end
  end
  
  if @pickError = 1
  begin
    if exists(select top 1 1
                from Job    j (nolock)
                join Status s (nolock) on j.StatusId = s.StatusId
               where JobId = @JobId
                 and s.StatusCode in ('S','RL'))
    begin
      if exists(select top 1 1
                  from AreaLocation al (nolock)
                  join Area          a (nolock) on al.AreaId = a.AreaId
                                               and a.Area = 'WavePick'
                 where al.LocationId = @OldPickLocationId)
      begin
        select @MoveId = InstructionId
          from Instruction      i (nolock)
          join Job              j (nolock) on i.JobId = j.JobId
          join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                                          and it.InstructionTypeCode = 'M'
          join Status           s (nolock) on j.StatusId = s.StatusId
                                          and s.StatusCode = 'PS'
         where i.InstructionRefId = @InstructionId
        
        exec @Error = p_Instruction_No_Stock
         @InstructionId     = @InstructionId
        
        if @Error <> 0
          goto result
        
        exec @Error = p_Instruction_Auto_Complete
         @InstructionId = @InstructionId,
         @NoStock       = 1
        
        if @Error <> 0
          goto result
        
        exec p_Instruction_Auto_Complete
         @InstructionId = @MoveId,
         @NoStock       = 1
        
        if @Error <> 0
          goto result
        
        exec @Error = p_Housekeeping_Stock_Take_Create_Product
         @warehouseId        = 1,
         @operatorId         = 2,
         @locationId         = @OldPickLocationId
        
        if @Error <> 0
          goto result
      end
      else
      begin
        exec @Error = p_Job_Update
         @JobId    = @JobId,
         @StatusId = @ErrorId
        
        if @Error <> 0
          goto result
      end
    end
    
    if @Error <> 0
    begin
      set @Error = 1
      goto result
    end
  end
  
  result:
    update MobileLog
       set EndDate = getdate()
     where MobileLogId = @MobileLogId
  
    select @Error = @@Error
    
    --select @Error
    return @Error
end

