﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Palletising_Options
  ///   Filename       : p_Planning_Palletising_Options.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Palletising_Options
(
 @OutboundShipmentId int = null
)

as
begin
	 set nocount on;
	 
	 select os.FP, os.SS, os.LP, os.FM, os.MP, os.PP, os.AlternatePallet, os.SubstitutePallet
    from OutboundShipment os (nolock)
   where OutboundShipmentId = @OutboundShipmentId
end
