﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Reason_By_ReasonCode
  ///   Filename       : p_Reason_By_ReasonCode.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Reason_By_ReasonCode
(
 @ReasonCode nvarchar(10)
)

as
begin
	 set nocount on;
  
  select -1     as 'ReasonId',
         'None' as 'Reason',
         'None' as 'ReasonCode'
  union
  select ReasonId,Reason,ReasonCode
    from Reason
   where ReasonCode like @ReasonCode + '%'
end
