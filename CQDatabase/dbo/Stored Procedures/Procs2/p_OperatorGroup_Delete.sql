﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroup_Delete
  ///   Filename       : p_OperatorGroup_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2012 07:59:18
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the OperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroup_Delete
(
 @OperatorGroupId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  delete OperatorGroup
     where OperatorGroupId = @OperatorGroupId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_OperatorGroupHistory_Insert
         @OperatorGroupId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
