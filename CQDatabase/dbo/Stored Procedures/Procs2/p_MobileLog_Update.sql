﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MobileLog_Update
  ///   Filename       : p_MobileLog_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:10
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the MobileLog table.
  /// </remarks>
  /// <param>
  ///   @MobileLogId int = null,
  ///   @ProcName nvarchar(256) = null,
  ///   @WarehouseId int = null,
  ///   @InstructionId int = null,
  ///   @ReferenceNumber nvarchar(100) = null,
  ///   @JobId int = null,
  ///   @PalletId int = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @Pick nvarchar(100) = null,
  ///   @Store nvarchar(100) = null,
  ///   @StorageUnitId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @PickLocationId int = null,
  ///   @StoreLocationId int = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @OperatorId int = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null,
  ///   @ErrorMsg varchar(max) = null,
  ///   @StatusCode varchar(10) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MobileLog_Update
(
 @MobileLogId int = null,
 @ProcName nvarchar(256) = null,
 @WarehouseId int = null,
 @InstructionId int = null,
 @ReferenceNumber nvarchar(100) = null,
 @JobId int = null,
 @PalletId int = null,
 @Barcode nvarchar(100) = null,
 @Pick nvarchar(100) = null,
 @Store nvarchar(100) = null,
 @StorageUnitId int = null,
 @StorageUnitBatchId int = null,
 @PickLocationId int = null,
 @StoreLocationId int = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @OperatorId int = null,
 @StartDate datetime = null,
 @EndDate datetime = null,
 @ErrorMsg varchar(max) = null,
 @StatusCode varchar(10) = null 
)

as
begin
	 set nocount on;
  
  if @MobileLogId = '-1'
    set @MobileLogId = null;
  
	 declare @Error int
 
  update MobileLog
     set ProcName = isnull(@ProcName, ProcName),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         InstructionId = isnull(@InstructionId, InstructionId),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         JobId = isnull(@JobId, JobId),
         PalletId = isnull(@PalletId, PalletId),
         Barcode = isnull(@Barcode, Barcode),
         Pick = isnull(@Pick, Pick),
         Store = isnull(@Store, Store),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         PickLocationId = isnull(@PickLocationId, PickLocationId),
         StoreLocationId = isnull(@StoreLocationId, StoreLocationId),
         Batch = isnull(@Batch, Batch),
         Quantity = isnull(@Quantity, Quantity),
         OperatorId = isnull(@OperatorId, OperatorId),
         StartDate = isnull(@StartDate, StartDate),
         EndDate = isnull(@EndDate, EndDate),
         ErrorMsg = isnull(@ErrorMsg, ErrorMsg),
         StatusCode = isnull(@StatusCode, StatusCode) 
   where MobileLogId = @MobileLogId
  
  select @Error = @@Error
  
  
  return @Error
  
end
