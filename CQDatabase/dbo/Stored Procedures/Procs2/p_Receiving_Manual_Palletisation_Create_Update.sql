﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Manual_Palletisation_Create_Update
  ///   Filename       : p_Receiving_Manual_Palletisation_Create_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Manual_Palletisation_Create_Update
(
 @InstructionId int,
 @Quantity      float,
 @CalcDifference bit = 1
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @ReceiptLineId     int,
          @UpdateId          int,
          @Difference        float,
          @UpdateQuantity    float
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Receiving_Manual_Palletisation_Create_Update'
  
  begin transaction
  
  if @CalcDifference = 1
  begin
    select @ReceiptLineId = ReceiptLineId,
           @Difference    = Quantity - @Quantity
      from Instruction (nolock)
     where InstructionId = @InstructionId
    
    if @Difference != 0
    begin
      select top 1
             @UpdateId       = InstructionId,
             @UpdateQuantity = Quantity + @Difference
        from Instruction
       where ReceiptLineId  = @ReceiptLineId
         and InstructionId  > @InstructionId
      order by InstructionId
      
      if @UpdateId is null
        select top 1
               @UpdateId       = InstructionId,
               @UpdateQuantity = Quantity + @Difference
          from Instruction
         where ReceiptLineId  = @ReceiptLineId
           and InstructionId != @InstructionId
        order by InstructionId desc
        
        select Quantity       - (Quantity + @Difference)
          from Instruction
         where ReceiptLineId  = @ReceiptLineId
           and InstructionId != @InstructionId
        order by InstructionId desc
      
      if @UpdateQuantity < 0 or @UpdateId is null
      begin
        set @Error = -1
        set @Errormsg = 'Error executing p_Receiving_Manual_Palletisation_Create_Update - Quantity cannot be updated to next line'
        goto error
      end
      
      exec @Error = p_Receiving_Manual_Palletisation_Create_Update
       @InstructionId  = @UpdateId,
       @Quantity       = @UpdateQuantity,
       @CalcDifference = 0
      
      if @Error <> 0
        goto error
    end
  end
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId       = @InstructionId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @Quantity      = @Quantity
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId       = @InstructionId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
