﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationOperatorGroup_Update
  ///   Filename       : p_LocationOperatorGroup_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Jul 2013 10:58:59
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the LocationOperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @LocationId int = null,
  ///   @OperatorGroupId int = null,
  ///   @OrderBy int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationOperatorGroup_Update
(
 @LocationId int = null,
 @OperatorGroupId int = null,
 @OrderBy int = null 
)

as
begin
	 set nocount on;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
	 declare @Error int
 
  update LocationOperatorGroup
     set LocationId = isnull(@LocationId, LocationId),
         OperatorGroupId = isnull(@OperatorGroupId, OperatorGroupId),
         OrderBy = isnull(@OrderBy, OrderBy) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
