﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Allocation_Result
  ///   Filename       : p_Planning_Allocation_Result.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Allocation_Result
(
 @OutboundShipmentId int = null,
 @IssueId            int = null
)

as
begin
	 set nocount on;
  
  declare @TableIssueLine as table
	 (
	  IssueLineId int
	 )
	 
	 declare @TableResult as table
	 (
	  InstructionId  int,
	  PickLocationId int
	 )
	 
	 if @OutboundShipmentId = -1
	   set @OutboundShipmentId = null
	 
	 if @IssueId = -1
	   set @IssueId = null
	 
	 if @IssueId is not null
	 begin
	   insert @TableIssueLine
	         (IssueLineId)
	   select il.IssueLineId
	     from IssueLine il (nolock)
	    where il.IssueId = @IssueId
  	 
	   insert @TableResult
	         (InstructionId,
	          PickLocationId)
	   select i.InstructionId,
	          i.PickLocationId
	     from @TableIssueLine  t
	     join Instruction      i (nolock) on t.IssueLineId       = i.IssueLineId
	 end
	 else if @OutboundShipmentId is not null
	 begin
	   insert @TableResult
	         (InstructionId,
	          PickLocationId)
	   select InstructionId,
	          PickLocationId
	     from Instruction (nolock)
	    where OutboundShipmentId = @OutboundShipmentId
	 end
	 
	 declare @Allocated int,
	         @NoStock   int
	 
	 select @Allocated  = count(1) from @TableResult where PickLocationId is not null
	 select @NoStock  = count(1) from @TableResult where PickLocationId is null
	 
	 select @Allocated as 'Allocated',
	        @NoStock   as 'No Stock'
end
