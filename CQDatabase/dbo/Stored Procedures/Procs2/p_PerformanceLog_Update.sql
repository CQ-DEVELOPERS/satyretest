﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PerformanceLog_Update
  ///   Filename       : p_PerformanceLog_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:56
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the PerformanceLog table.
  /// </remarks>
  /// <param>
  ///   @PerformanceLogId int = null,
  ///   @ProcedureName nvarchar(256) = null,
  ///   @Remarks nvarchar(max) = null,
  ///   @StartDate datetime = null,
  ///   @LogDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PerformanceLog_Update
(
 @PerformanceLogId int = null,
 @ProcedureName nvarchar(256) = null,
 @Remarks nvarchar(max) = null,
 @StartDate datetime = null,
 @LogDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @PerformanceLogId = '-1'
    set @PerformanceLogId = null;
  
	 declare @Error int
 
  update PerformanceLog
     set ProcedureName = isnull(@ProcedureName, ProcedureName),
         Remarks = isnull(@Remarks, Remarks),
         StartDate = isnull(@StartDate, StartDate),
         LogDate = isnull(@LogDate, LogDate) 
   where PerformanceLogId = @PerformanceLogId
  
  select @Error = @@Error
  
  
  return @Error
  
end
