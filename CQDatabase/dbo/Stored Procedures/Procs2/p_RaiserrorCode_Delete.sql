﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RaiserrorCode_Delete
  ///   Filename       : p_RaiserrorCode_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:36
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the RaiserrorCode table.
  /// </remarks>
  /// <param>
  ///   @RaiserrorCodeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RaiserrorCode_Delete
(
 @RaiserrorCodeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete RaiserrorCode
     where RaiserrorCodeId = @RaiserrorCodeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
