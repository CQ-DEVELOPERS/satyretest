﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Register_SerialNumber_Unlinked
  ///   Filename       : p_Register_SerialNumber_Unlinked.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Register_SerialNumber_Unlinked
(
 @JobId         int,
 @InstructionId int
)

as
begin
	 set nocount on;
  
  declare @StatusId int;
  
  select @StatusId = StatusId
    from Status
   where Type = 'I'
     and StatusCode = 'D';
  
  select i.JobId,
         sn.SerialNumberId,
         sn.SerialNumber
    from SerialNumber sn (nolock) 
    join Instruction  i  (nolock) on sn.StoreInstructionId = i.InstructionId
   where i.JobId = @JobId
     --and i.InstructionId != @InstructionId
     and i.StatusId = @StatusId
end
