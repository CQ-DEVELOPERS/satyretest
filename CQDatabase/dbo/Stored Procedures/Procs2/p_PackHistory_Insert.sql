﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackHistory_Insert
  ///   Filename       : p_PackHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Nov 2012 14:41:22
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PackHistory table.
  /// </remarks>
  /// <param>
  ///   @PackId int = null,
  ///   @StorageUnitId int = null,
  ///   @PackTypeId int = null,
  ///   @Quantity int = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @Length int = null,
  ///   @Width int = null,
  ///   @Height int = null,
  ///   @Volume numeric(13,3) = null,
  ///   @Weight numeric(13,3) = null,
  ///   @WarehouseId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @NettWeight numeric(13,6) = null,
  ///   @TareWeight numeric(13,6) = null,
  ///   @ProductionQuantity float = null,
  ///   @StatusId int = null 
  /// </param>
  /// <returns>
  ///   PackHistory.PackId,
  ///   PackHistory.StorageUnitId,
  ///   PackHistory.PackTypeId,
  ///   PackHistory.Quantity,
  ///   PackHistory.Barcode,
  ///   PackHistory.Length,
  ///   PackHistory.Width,
  ///   PackHistory.Height,
  ///   PackHistory.Volume,
  ///   PackHistory.Weight,
  ///   PackHistory.WarehouseId,
  ///   PackHistory.CommandType,
  ///   PackHistory.InsertDate,
  ///   PackHistory.NettWeight,
  ///   PackHistory.TareWeight,
  ///   PackHistory.ProductionQuantity,
  ///   PackHistory.StatusId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackHistory_Insert
(
 @PackId int = null,
 @StorageUnitId int = null,
 @PackTypeId int = null,
 @Quantity int = null,
 @Barcode nvarchar(100) = null,
 @Length int = null,
 @Width int = null,
 @Height int = null,
 @Volume numeric(13,3) = null,
 @Weight numeric(13,3) = null,
 @WarehouseId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @NettWeight numeric(13,6) = null,
 @TareWeight numeric(13,6) = null,
 @ProductionQuantity float = null,
 @StatusId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert PackHistory
        (PackId,
         StorageUnitId,
         PackTypeId,
         Quantity,
         Barcode,
         Length,
         Width,
         Height,
         Volume,
         Weight,
         WarehouseId,
         CommandType,
         InsertDate,
         NettWeight,
         TareWeight,
         ProductionQuantity,
         StatusId)
  select @PackId,
         @StorageUnitId,
         @PackTypeId,
         @Quantity,
         @Barcode,
         @Length,
         @Width,
         @Height,
         @Volume,
         @Weight,
         @WarehouseId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @NettWeight,
         @TareWeight,
         @ProductionQuantity,
         @StatusId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
