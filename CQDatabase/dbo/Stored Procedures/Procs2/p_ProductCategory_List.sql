﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductCategory_List
  ///   Filename       : p_ProductCategory_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:00
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ProductCategory table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ProductCategory.ProductCategoryId,
  ///   ProductCategory.ProductCategory 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductCategory_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ProductCategoryId
        ,'{All}' as ProductCategory
  union
  select
         ProductCategory.ProductCategoryId
        ,ProductCategory.ProductCategory
    from ProductCategory
  
end
