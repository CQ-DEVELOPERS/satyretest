﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Delivery_Note
  ///   Filename       : p_Report_Delivery_Note.sql
  ///   Create By      : Ruan
  ///   Date Created   : July 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Delivery_Note
(
 @OutboundShipmentId int,
 @IssueId            int
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   OutboundDocumentId int,
   IssueId            int,
   JobId              int,
   InstructionId      int,
   StatusId           int,
   StorageUnitBatchId int,
   Quantity           int,
   ConfirmedQuantity  int,
   ShortQuantity      int,
   DropSequence       int,
   RouteId            int,
   Route              nvarchar(50),
   DeliveryMethod	  nvarchar(50),
   CartonQty          decimal(15,5),
   Boxes              nvarchar(50),
   BoxesQuantity      nvarchar(10),
   WarehouseId		  int,
   StorageUnitId	  int,
   ReferenceNumber	  nvarchar(30),
   InvoiceNumber      nvarchar(30),
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(255),
   AddressLine1       nvarchar(255),
   AddressLine2       nvarchar(255),
   AddressLine3       nvarchar(255),
   AddressLine4       nvarchar(255),
   AddressLine5       nvarchar(255),
   DespatchDate       datetime,
   Comments           nvarchar(255),
   Container          nvarchar(50),
   ContainerTypeId    int,
   ContainerType      nvarchar(50),
   TareWeight		  numeric(13,6),
   TotalContainers    int,
   Weight             numeric(13,3),
   ExternalOrderNum   nvarchar(255),
   Pallets			  int,
   ContainerNumber    int,
   PalletQty		  int
  )
  
    declare @TableSumm as table
	(	  
	InvoiceNumber		nvarchar(30)
    ,OrderNumber		nvarchar(30)
    ,OutboundShipmentId int
    ,DespatchDate		datetime
    ,ExternalCompany	nvarchar(255)
    ,AddressLine1		nvarchar(255)
    ,AddressLine2		nvarchar(255)
    ,AddressLine3		nvarchar(255)
    ,AddressLine4		nvarchar(255)
    ,AddressLine5		nvarchar(255)
    ,Comments			nvarchar(255)
    ,DeliveryMethod		nvarchar(50)
    ,TotalContainers	int
    ,Container			nvarchar(50)
    ,ContainerType		nvarchar(50)  
    ,Items				decimal(15,5)    
    ,Weight				decimal(13,3)
    ,CartonQty			decimal(15,5)
    ,ExternalOrderNum	nvarchar(255)
    ,ReferenceNumber	nvarchar(30)
    ,Pallets			int
    ,ContainerNumber    int
    ,JobId              int
    ,ContainerTypeId    int
    ,TareWeight			numeric(13,6)
    ,BoxesQuantity		nvarchar(10)
    ,StorageUnitBatchId int
    ,WarehouseId		int
    )

  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
	 
  if @OutboundShipmentId is not null
    set @IssueId = null
  
  insert @TableResult
        (OutboundShipmentId,
         OutboundDocumentId,
         IssueId,
         JobId,
         StatusId,
         InstructionId,
         StorageUnitBatchId,
         WarehouseId,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity,
         ReferenceNumber)
  select distinct ili.OutboundShipmentId,
         ili.OutboundDocumentId,
         ili.IssueId,
         j.JobId,
         i.StatusId,
         i.InstructionId,
         i.StorageUnitBatchId,
         i.WarehouseId,
         i.ConfirmedQuantity,
         isnull(i.ConfirmedQuantity, 0),
         i.Quantity - isnull(i.ConfirmedQuantity, 0),
         j.ReferenceNumber
  from Instruction			  i
  join Status                si (nolock) on i.StatusId           = si.StatusId
  join Job                    j (nolock) on i.JobId              = j.JobId
  join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
  join OutboundDocument      od (nolock) on od.OutboundDocumentId = ili.OutboundDocumentId
  --join Instruction          ins (nolock) on ili.InstructionId    = ins.InstructionId
  where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))
    and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))
    and (i.ConfirmedQuantity > 0 or i.CheckQuantity > 0)
    
     
  update tr
     set DespatchDate = os.ShipmentDate,
         RouteId      = os.RouteId,
         DropSequence = osi.DropSequence
    from @TableResult           tr
    join OutboundShipment       os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId
   where tr.OutboundShipmentId is not null
  
  update tr
     set DespatchDate = i.DeliveryDate,
         RouteId      = i.RouteId,
         DropSequence = i.DropSequence
    from @TableResult  tr
    join Issue          i (nolock) on tr.IssueId = i.IssueId
   where tr.OutboundShipmentId is null
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId         
    from @TableResult      tr
    join OutboundDocument  od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
   
  update tr
     set ContainerTypeId = j.ContainerTypeId
        ,ContainerType   = ct.ContainerTypeCode
        ,TotalContainers = j.pallets
        ,TareWeight		 = ct.TareWeight
    from @TableResult tr
    inner join Job           j (nolock) on tr.JobId = j.JobId
    left  join ContainerType ct (nolock) on j.ContainerTypeId = ct.ContainerTypeId

    
  update tr
     set Weight = tr.ConfirmedQuantity * p.Weight,
         StorageUnitId = sub.StorageUnitId
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join Pack               p (nolock) on sub.StorageUnitId     = p.StorageUnitId
   where p.Quantity = 1
   and   p.WarehouseId = tr.WarehouseId
   
  --update tr
  --   set DeliveryMethod = (select max(iep.DeliveryMethod) from InterfaceExportPackHeader iep
		--					where tr.OrderNumber = iep.OrderNumber)
  --  from @TableResult      tr

  update tr
     set Boxes = pt.PackType,
         BoxesQuantity = p.Quantity
    from @TableResult      tr
    join Pack               p (nolock) on tr.WarehouseId        = p.WarehouseId
                                      and tr.StorageUnitId      = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId
   where  pt.PackType = 'Carton'

  update tr
  set PalletQty = (select MAX(Quantity) 
						from Pack p 
						join PackType pt on pt.PackTypeId = p.PackTypeId 
						where tr.WarehouseId = p.WarehouseId
						and tr.StorageUnitId = p.StorageUnitId)
  from  @TableResult tr
  where ContainerType in ('PCL', 'PLT')
  
  SET ARITHABORT OFF  
  SET ANSI_WARNINGS OFF 
  
  update tr
  set    Pallets = tr.ConfirmedQuantity / isnull(tr.Quantity, 1)
  from   @TableResult tr
  where  ContainerType in ('PCL', 'PLT') 
  and    tr.ConfirmedQuantity >= tr.PalletQty 
  
  update tr
  set    CartonQty = ConfirmedQuantity / isnull(BoxesQuantity, 1)
  from   @TableResult tr
  where  ContainerType in ('PCL', 'PLT') 
  
  update tr
  set    CartonQty = 1
  from   @TableResult tr
  where  ContainerType not in ('PCL', 'PLT') 

  
  update tr
     set Weight = tr.CartonQty * p.Weight,
         StorageUnitId = sub.StorageUnitId
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join Pack               p (nolock) on sub.StorageUnitId     = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId
   where p.WarehouseId = tr.WarehouseId
   and   ContainerType in ('PCL') 
   and   pt.PackType = 'Carton'   
 
  update tr
  set	InvoiceNumber = inv.PrimaryKey
		,ExternalCompany = inv.CustomerName
		,Comments     = Left(Message1 + Message2 + Message3, 255)
		,DespatchDate = inv.insertdate
  from	@TableResult      tr
  join	InterfaceImportIPHeader Inv on tr.IssueId = Inv.IssueId
 
  update tr
  set AddressLine1		= Additional4
     ,AddressLine2		= Additional5
     ,AddressLine3		= Additional6
     ,AddressLine4		= Additional7 
     ,AddressLine5		= Additional8
     ,ExternalOrderNum	= Additional1     
  from @TableResult      tr
  join InterfaceImportSOHeader soh on tr.OrderNumber = soh.OrderNumber
  and soh.RecordStatus = 'C'
  
  insert @TableSumm
        (InvoiceNumber
         ,OrderNumber
         ,OutboundShipmentId
         ,DespatchDate
         ,ExternalCompany
         ,AddressLine1
         ,AddressLine2
         ,AddressLine3
         ,AddressLine4
         ,AddressLine5
         ,Comments
         ,DeliveryMethod
         ,TotalContainers
         ,Container
         ,ContainerType     
         ,Items
         ,Weight
         ,CartonQty
         ,ExternalOrderNum
         ,ReferenceNumber
         ,Pallets
         ,ContainerTypeId
         ,JobId
         ,TareWeight)
  select distinct InvoiceNumber
         ,OrderNumber
         ,OutboundShipmentId
         ,DespatchDate
         ,ExternalCompany
         ,AddressLine1
         ,AddressLine2
         ,AddressLine3
         ,AddressLine4
         ,AddressLine5
         ,Comments
         ,DeliveryMethod
         ,sum(TotalContainers) as TotalContainers
         ,Container
         ,ContainerType     
         ,sum(ConfirmedQuantity) as 'Items'    
         ,sum(Weight) as 'Weight'
         ,CartonQty as CartonQty
         ,ExternalOrderNum
         ,isnull(ReferenceNumber, jobid) as ReferenceNumber
         ,Pallets
         ,ContainerTypeId
         ,JobId
         ,TareWeight
  from @TableResult			  tr
  where containertype not in ('PLT','PCL')
    group by InvoiceNumber
         ,OrderNumber
         ,OutboundShipmentId
         ,DespatchDate
         ,ExternalCompany
         ,AddressLine1
         ,AddressLine2
         ,AddressLine3
         ,AddressLine4
         ,AddressLine5
         ,Comments
         ,DeliveryMethod
         --,TotalContainers
         ,Container
         ,ContainerType     
         --,Items
         --,Weight
         ,CartonQty
         ,ExternalOrderNum
         ,ReferenceNumber
         ,Pallets
         ,ContainerTypeId
         ,JobId
         ,TareWeight
         
         
  insert @TableSumm
        (InvoiceNumber
         ,OrderNumber
         ,OutboundShipmentId
         ,DespatchDate
         ,ExternalCompany
         ,AddressLine1
         ,AddressLine2
         ,AddressLine3
         ,AddressLine4
         ,AddressLine5
         ,Comments
         ,DeliveryMethod
         ,TotalContainers
         ,Container
         ,ContainerType     
         ,Items
         ,Weight
         ,CartonQty
         ,ExternalOrderNum
         ,ReferenceNumber
         ,Pallets
         ,ContainerTypeId
         ,JobId
         ,TareWeight
         ,BoxesQuantity
         ,StorageUnitBatchId
         ,WarehouseId)
  Select distinct InvoiceNumber
         ,OrderNumber
         ,OutboundShipmentId
         ,DespatchDate
         ,ExternalCompany
         ,AddressLine1
         ,AddressLine2
         ,AddressLine3
         ,AddressLine4
         ,AddressLine5
         ,Comments
         ,DeliveryMethod
         ,sum(TotalContainers) as TotalContainers
         ,Container
         ,ContainerType     
         ,sum(ConfirmedQuantity) as 'Items'   
         ,sum(Weight) as 'Weight'
         ,sum(CartonQty) as 'CartonQty'
         ,ExternalOrderNum
         ,isnull(ReferenceNumber, jobid) as ReferenceNumber
	     ,Pallets
	     ,ContainerTypeId
	     ,JobId
	     ,TareWeight
	     ,BoxesQuantity
	     ,StorageUnitBatchId
	     ,WarehouseId
    from @TableResult
    where containertype in ('PLT','PCL')
    group by InvoiceNumber
         ,OrderNumber
         ,OutboundShipmentId
         ,DespatchDate
         ,ExternalCompany
         ,AddressLine1
         ,AddressLine2
         ,AddressLine3
         ,AddressLine4
         ,AddressLine5
         ,Comments
         ,DeliveryMethod
         --,TotalContainers
         ,Container
         ,ContainerType     
         --,Items
         --,Weight
         ,ExternalOrderNum
         ,ReferenceNumber
         ,Pallets
         ,ContainerTypeId
         ,JobId
         ,TareWeight
         ,BoxesQuantity
         ,StorageUnitBatchId
		 ,WarehouseId
		 
		 
  update tr
	 set CartonQty = ceiling(isnull(Items,0) / isnull(BoxesQuantity, 1))
	from @TableSumm tr
   where ContainerType in ('PCL', 'PLT')  
   
  update tr
	 set CartonQty = 1
	from @TableSumm tr
   where CartonQty < 1 
     and ContainerType in ('PCL')  
  
  update tr
     set Weight = tr.CartonQty * p.Weight
    from @TableSumm      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join Pack               p (nolock) on sub.StorageUnitId     = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId
   where p.WarehouseId = tr.WarehouseId
   and   ContainerType in ('PCL') 
   and   pt.PackType = 'Carton'   
   
    update tr
  set ContainerNumber =    r.ContainerNumber
    from @TableSumm      tr 
    inner join (select row_number() over(ORDER BY JobId) As ContainerNumber, ContainerTypeId, JobId from @TableSumm) r on tr.JobId = r.JobId  
 
    update tr
     set Weight = (isnull(tr.Weight,0) + isnull(tr.TareWeight,0))
    from @TableSumm      tr
   where ContainerType in ('BX1', 'BX2', 'BX3', 'BX4', 'BX5') 
 
  update tr
  set    TotalContainers = 1
  from   @TableResult tr
  where  ContainerType not in ('PCL', 'PLT')
   
  update tr
     set Container = convert(nvarchar(50), tr.ContainerNumber)
               + ' of '
               + convert(nvarchar(50), (select COUNT(distinct jobid) from @TableSumm tr1 where tr.OrderNumber = tr1.OrderNumber))
    from @TableSumm tr
    inner join Job           j (nolock) on tr.JobId = j.JobId
    left join ContainerType ct (nolock) on j.ContainerTypeId = ct.ContainerTypeId
 

select  * from @TableSumm
order by ContainerNumber


end
