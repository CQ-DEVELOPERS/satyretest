﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Backorder_Create_Search
  ///   Filename       : p_Outbound_Backorder_Create_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OutboundDocument table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   OutboundLineId,
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Backorder_Create_Search
(
 @IssueId int = null output
)

as
begin
	 set nocount on;
  
	 declare @Error    int
    
  if @IssueId = '-1'
    set @IssueId = null;
    
  select il.IssueLineId,
         vs.ProductCode,
         vs.Product,
         vs.SKUCode,
         vs.Batch,
         s.Status,
         il.Quantity
    from IssueLine           il (nolock)
    join viewStock           vs on il.StorageUnitBatchId = vs.StorageUnitBatchId
    join Status               s (nolock) on il.StatusId = s.StatusId
  where il.IssueId = @IssueId
end
