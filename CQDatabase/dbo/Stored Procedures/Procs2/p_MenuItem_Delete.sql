﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_Delete
  ///   Filename       : p_MenuItem_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 16:06:10
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the MenuItem table.
  /// </remarks>
  /// <param>
  ///   @MenuItemId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItem_Delete
(
 @MenuItemId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete MenuItem
     where MenuItemId = @MenuItemId
  
  select @Error = @@Error
  
  
  return @Error
  
end
