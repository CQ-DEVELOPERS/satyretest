﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Backorder_Search_Update
  ///   Filename       : p_Outbound_Backorder_Search_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Backorder_Search_Update
(
 @WarehouseId             int = null,
 @IssueId                 int = null,
 @OutboundDocumentTypeId	 int = null,
 @ExternalCompanyCode	    nvarchar(30) = null,
 @ExternalCompany	        nvarchar(255) = null,
 @OrderNumber	            nvarchar(30) = null,
 @FromDate	               datetime = null,
 @ToDate	                 datetime = null
)

as
begin
	 set nocount on;
  
	 declare @StatusId int
  
  declare @TableResult as
  table(
        IssueId                   int,
        OrderNumber               nvarchar(30),
        ExternalCompanyId         int,
        ExternalCompanyCode       nvarchar(30),
        ExternalCompany           nvarchar(255),
        NumberOfLines             int,
        DeliveryDate              datetime,
        CreateDate                datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        OutboundDocumentTypeId    int,
        OutboundDocumentType      nvarchar(30)
       );
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @IssueId is null
  begin
    select @StatusId = dbo.ufn_StatusId('IS','BP')
    
    insert @TableResult
          (IssueId,
           OrderNumber,
           ExternalCompanyId,
           ExternalCompanyCode,
           ExternalCompany,
           Status,
           OutboundDocumentTypeId,
           OutboundDocumentType,
           DeliveryDate,
           CreateDate)
    select i.IssueId,
           od.OrderNumber,
           ec.ExternalCompanyId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           s.Status,
           odt.OutboundDocumentTypeId,
           odt.OutboundDocumentType,
           od.DeliveryDate,
           od.CreateDate
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
--      join Status               s   (nolock) on od.StatusId               = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status                 s (nolock) on i.StatusId                = s.StatusId
     where od.OutboundDocumentTypeId    = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and ec.ExternalCompanyCode    like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany        like isnull(@ExternalCompany + '%', ec.ExternalCompany)
       and od.OrderNumber            like isnull(@OrderNumber  + '%', od.OrderNumber)
       and od.DeliveryDate        between isnull(@FromDate, od.DeliveryDate) and isnull(@ToDate, od.DeliveryDate)
       and s.StatusId                   = @StatusId
       and od.WarehouseId               = isnull(@WarehouseId, od.WarehouseId)
  end
  else
  begin
    insert @TableResult
          (IssueId,
           OrderNumber,
           ExternalCompanyId,
           ExternalCompanyCode,
           ExternalCompany,
           Status,
           OutboundDocumentTypeId,
           OutboundDocumentType,
           DeliveryDate,
           CreateDate)
    select i.IssueId,
           od.OrderNumber,
           ec.ExternalCompanyId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           s.Status,
           odt.OutboundDocumentTypeId,
           odt.OutboundDocumentType,
           od.DeliveryDate,
           od.CreateDate
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status                 s (nolock) on i.StatusId                = s.StatusId
     where i.IssueId = @IssueId
  end
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from IssueLine il
                           where t.IssueId = il.IssueId)
    from @TableResult t
  
  select IssueId,
         OrderNumber,
         ExternalCompanyId,
         ExternalCompanyCode,
         ExternalCompany,
         NumberOfLines,
         DeliveryDate,
         Status,
         OutboundDocumentTypeId,
         OutboundDocumentType
    from @TableResult
end
