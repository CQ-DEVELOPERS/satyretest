﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackExternalCompany_Update
  ///   Filename       : p_PackExternalCompany_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 21:17:03
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the PackExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StorageUnitId int = null,
  ///   @FromPackId int = null,
  ///   @FromQuantity float = null,
  ///   @ToPackId int = null,
  ///   @ToQuantity float = null,
  ///   @Comments ntext = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackExternalCompany_Update
(
 @WarehouseId int = null,
 @ExternalCompanyId int = null,
 @StorageUnitId int = null,
 @FromPackId int = null,
 @FromQuantity float = null,
 @ToPackId int = null,
 @ToQuantity float = null,
 @Comments ntext = null 
)

as
begin
	 set nocount on;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @FromPackId = '-1'
    set @FromPackId = null;
  
  if @ToPackId = '-1'
    set @ToPackId = null;
  
	 declare @Error int
 
  update PackExternalCompany
     set FromPackId = isnull(@FromPackId, FromPackId),
         FromQuantity = isnull(@FromQuantity, FromQuantity),
         ToPackId = isnull(@ToPackId, ToPackId),
         ToQuantity = isnull(@ToQuantity, ToQuantity),
         Comments = isnull(@Comments, Comments) 
   where WarehouseId = @WarehouseId
     and ExternalCompanyId = @ExternalCompanyId
     and StorageUnitId = @StorageUnitId
  
  select @Error = @@Error
  
  
  return @Error
  
end
