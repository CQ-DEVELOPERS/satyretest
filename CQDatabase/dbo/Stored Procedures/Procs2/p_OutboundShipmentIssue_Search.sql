﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipmentIssue_Search
  ///   Filename       : p_OutboundShipmentIssue_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jun 2013 12:36:35
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OutboundShipmentIssue table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null output,
  ///   @IssueId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OutboundShipmentIssue.OutboundShipmentId,
  ///   OutboundShipment.OutboundShipmentId,
  ///   OutboundShipmentIssue.IssueId,
  ///   Issue.IssueId,
  ///   OutboundShipmentIssue.DropSequence 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipmentIssue_Search
(
 @OutboundShipmentId int = null output,
 @IssueId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @OutboundShipmentId = '-1'
    set @OutboundShipmentId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
 
  select
         OutboundShipmentIssue.OutboundShipmentId
        ,OutboundShipmentIssue.IssueId
        ,OutboundShipmentIssue.DropSequence
    from OutboundShipmentIssue
    left
    join OutboundShipment OutboundShipmentOutboundShipmentId on OutboundShipmentOutboundShipmentId.OutboundShipmentId = OutboundShipmentIssue.OutboundShipmentId
    left
    join Issue IssueIssueId on IssueIssueId.IssueId = OutboundShipmentIssue.IssueId
   where isnull(OutboundShipmentIssue.OutboundShipmentId,'0')  = isnull(@OutboundShipmentId, isnull(OutboundShipmentIssue.OutboundShipmentId,'0'))
     and isnull(OutboundShipmentIssue.IssueId,'0')  = isnull(@IssueId, isnull(OutboundShipmentIssue.IssueId,'0'))
  
end
