﻿CREATE procedure [dbo].[p_Plascon_Import_Lot]
as
Begin
	set nocount on;

Declare @Code Varchar(255),
		@LotStatusId Varchar(20),
		@ExpiryDate Datetime,
		@ExpectedYield Int,
		@InterfaceImportLotId int,
		@Getdate datetime,
		@BatchId int,
		@ParentProductCode nvarchar(30)

Set @GetDate = dbo.ufn_Getdate()

set rowcount 200

Update L
   set ProcessedDate = @GetDate
	  ,LotStatusId = S.StatusID
  from InterfaceImportLot L
	   Inner Join InterfaceImportLotStatus S on L.LotStatus = S.HostId
 where L.RecordStatus = 'N'
   and L.ProcessedDate Is Null

Declare CursorLot cursor for 
Select InterfaceImportLotId
	,l.LotId
	,t.StatusId
	,l.ExpiryDate
	,b.BatchId
	,l.ExpectedYield
	,LTrim(l.ParentProductCode)
from InterfaceImportLot l
	Inner Join InterfaceImportLotStatus t on t.HostId = l.LotStatus
	Left Join Batch b on b.Batch = l.LotId
where l.ProcessedDate = @GetDate
Order by InsertDate

Open CursorLot

Fetch CursorLot 
into @InterfaceImportLotId
	,@Code
	,@LotStatusId
	,@ExpiryDate
	,@BatchId
	,@ExpectedYield
	,@ParentProductCode
While @@Fetch_Status = 0
Begin
	set @BatchId = null
	
	select @BatchId = BatchId
	  from Batch (nolock)
	 where Batch = @Code

	if @ExpiryDate is null
	begin
		select @ExpiryDate = Dateadd(dd, MAX(ShelfLifeDays), @GetDate) 
		from Product
		where ParentProductCode = @ParentProductCode
	end
	
	If @BatchId is null
	begin
		exec p_Batch_Insert
			   @BatchId					= @BatchId OUTPUT
			  ,@StatusId				= @LotStatusId
			  ,@WarehouseId				= 1
			  ,@Batch					= @Code
			  ,@CreateDate				= @GetDate
			  ,@ExpiryDate				= @ExpiryDate
			  ,@IncubationDays			= null
			  ,@ShelfLifeDays			= null
			  ,@ExpectedYield			= @ExpectedYield
			  ,@ActualYield				= null
			  ,@ECLNumber				= null
			  ,@BatchReferenceNumber	= null
			  ,@ProductionDateTime		= null
			  ,@FilledYield				= null
			  ,@COACertificate			= null
			  ,@StatusModifiedBy		= null
			  ,@StatusModifiedDate		= null
			  ,@CreatedBy				= null
			  ,@ModifiedBy				= null
			  ,@ModifiedDate			= null
			  ,@Prints					= null
			  ,@ParentProductCode=@ParentProductCode
			  
	end
	else
	begin
		Exec p_Batch_Update 
			 @BatchId=@BatchId
			--,@StatusId=@LotStatusId
			,@ExpiryDate=@ExpiryDate
			,@ExpectedYield=@ExpectedYield
			,@ParentProductCode=@ParentProductCode
	end
	
	insert StorageUnitBatch
	      (BatchId,
        StorageUnitId)
	select @BatchId,
        su.StorageUnitId
   from Product            p (nolock)
   join StorageUnit       su (nolock) on p.ProductId      = su.ProductId
   left
   join StorageUnitBatch sub (nolock) on su.StorageUnitId = sub.StorageUnitId
                                     and sub.BatchId      = @BatchId
  where p.ParentProductCode = @ParentProductCode
    and sub.StorageUnitBatchId is null
	
	Update InterfaceImportLot 
	   set RecordStatus = 'Y'
	 where  InterfaceImportLotId =  @InterfaceImportLotId
	 
	 Fetch CursorLot 
	  into @InterfaceImportLotId
		  ,@Code
		  ,@LotStatusId
		  ,@ExpiryDate
		  ,@BatchId
		  ,@ExpectedYield
		  ,@ParentProductCode
End

Close CursorLot
Deallocate CursorLot
return
End

