﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupInstructionType_Update
  ///   Filename       : p_OperatorGroupInstructionType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:06
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OperatorGroupInstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null,
  ///   @OperatorGroupId int = null,
  ///   @OrderBy int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupInstructionType_Update
(
 @InstructionTypeId int = null,
 @OperatorGroupId int = null,
 @OrderBy int = null 
)

as
begin
	 set nocount on;
  
  if @InstructionTypeId = '-1'
    set @InstructionTypeId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
	 declare @Error int
 
  update OperatorGroupInstructionType
     set OrderBy = isnull(@OrderBy, OrderBy) 
   where InstructionTypeId = @InstructionTypeId
     and OperatorGroupId = @OperatorGroupId
  
  select @Error = @@Error
  
  
  return @Error
  
end
