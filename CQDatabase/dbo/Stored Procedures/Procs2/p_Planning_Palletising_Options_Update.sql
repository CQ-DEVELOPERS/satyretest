﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Palletising_Options_Update
  ///   Filename       : p_Planning_Palletising_Options_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Palletising_Options_Update
(
 @OutboundShipmentId int,
 @FP                 bit,
 @SS                 bit,
 @LP                 bit,
 @FM                 bit,
 @MP                 bit,
 @PP                 bit,
 @AlternatePallet    bit,
 @SubstitutePallet   bit
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update OutboundShipment
     set FP = @FP,
         SS = @SS,
         LP = @LP,
         FM = @FM,
         MP = @MP,
         PP = @PP,
         AlternatePallet = @AlternatePallet,
         SubstitutePallet = @SubstitutePallet
    where OutboundShipmentId = @OutboundShipmentId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Planning_Palletising_Options_Update'); 
    rollback transaction
    return @Error
end
