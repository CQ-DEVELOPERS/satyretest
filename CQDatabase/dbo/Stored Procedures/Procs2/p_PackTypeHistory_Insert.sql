﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackTypeHistory_Insert
  ///   Filename       : p_PackTypeHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:40:52
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PackTypeHistory table.
  /// </remarks>
  /// <param>
  ///   @PackTypeId int = null,
  ///   @PackType varchar(30) = null,
  ///   @InboundSequence smallint = null,
  ///   @OutboundSequence smallint = null,
  ///   @CommandType varchar(10) = null,
  ///   @InsertDate datetime = null,
  ///   @PackTypeCode nvarchar(100) = null,
  ///   @Unit bit = null,
  ///   @Pallet bit = null 
  /// </param>
  /// <returns>
  ///   PackTypeHistory.PackTypeId,
  ///   PackTypeHistory.PackType,
  ///   PackTypeHistory.InboundSequence,
  ///   PackTypeHistory.OutboundSequence,
  ///   PackTypeHistory.CommandType,
  ///   PackTypeHistory.InsertDate,
  ///   PackTypeHistory.PackTypeCode,
  ///   PackTypeHistory.Unit,
  ///   PackTypeHistory.Pallet 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackTypeHistory_Insert
(
 @PackTypeId int = null,
 @PackType varchar(30) = null,
 @InboundSequence smallint = null,
 @OutboundSequence smallint = null,
 @CommandType varchar(10) = null,
 @InsertDate datetime = null,
 @PackTypeCode nvarchar(100) = null,
 @Unit bit = null,
 @Pallet bit = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert PackTypeHistory
        (PackTypeId,
         PackType,
         InboundSequence,
         OutboundSequence,
         CommandType,
         InsertDate,
         PackTypeCode,
         Unit,
         Pallet)
  select @PackTypeId,
         @PackType,
         @InboundSequence,
         @OutboundSequence,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @PackTypeCode,
         @Unit,
         @Pallet 
  
  select @Error = @@Error
  
  
  return @Error
  
end
