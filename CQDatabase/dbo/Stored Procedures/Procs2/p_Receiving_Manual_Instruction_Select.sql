﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Manual_Instruction_Select
  ///   Filename       : p_Receiving_Manual_Instruction_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Manual_Instruction_Select
(
 @InstructionId int
)

as
begin
	 set nocount on;
  
  select i.InstructionId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         i.Quantity
    from Instruction      i
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch            b   (nolock) on sub.BatchId           = b.BatchId
   where InstructionId = @InstructionId
end
