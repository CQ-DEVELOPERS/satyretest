﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Shipment_Details
  ///   Filename       : p_Outbound_Shipment_Details.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Shipment_Details
(
 @OutboundShipmentId int
)

as
begin
	 set nocount on;
  
  select os.OutboundShipmentId,
         os.ShipmentDate,
         l.Location,
         s.Status
    from OutboundShipment os (nolock)
    left outer
    join Location          l (nolock) on os.LocationId = l.LocationId
    join Status            s (nolock) on os.StatusId   = s.StatusId
   where OutboundShipmentId = @OutboundShipmentId
end
