﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mixed_Job_Details
  ///   Filename       : p_Mixed_Job_Details.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mixed_Job_Details
(
 @JobId int
)

as
begin
  set nocount on;
  
  select sum(i.Quantity)               as 'Quantity',
         sum(i.Quantity * p.Weight) as 'Weight'
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Pack               p (nolock) on sub.StorageUnitId    = p.StorageUnitId
   where i.JobId = @JobId
     and p.Quantity = 1
end
