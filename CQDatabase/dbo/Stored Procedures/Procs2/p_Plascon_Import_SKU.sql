﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Plascon_Import_SKU
  ///   Filename       : p_Plascon_Import_SKU.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 07 Nov 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Plascon_Import_SKU

as
begin
	 set nocount on;
  
  declare  @Error                           int
          ,@Errormsg                        varchar(500)
          ,@GetDate                         datetime
          ,@InterfaceImportSKUId            int
          ,@SKUId                           int
          ,@SKUCode                         varchar(10)
          ,@SKU                             varchar(50)
          ,@UnitOfMeasure                   varchar(50)
          ,@Quantity                        int
          ,@AlternatePallet                 int
          ,@SubstitutePallet                int
          ,@Litres                          numeric(13,3)
          ,@HostId                          varchar(30)
          
  
  select @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  
  update InterfaceImportSKU
     set ProcessedDate = @Getdate
   where RecordStatus in ('N','U')
     and ProcessedDate is null
  
  declare SKU_cursor cursor for
   select InterfaceImportSKUId
         ,SKUCode
         ,SKU
         ,UnitOfMeasure
         ,Quantity
         ,AlternatePallet
         ,SubstitutePallet
         ,Litres
         ,HostId          
     from InterfaceImportSKU
    where ProcessedDate = @Getdate
  
  open SKU_cursor
  
  fetch SKU_cursor into 
          @InterfaceImportSKUId
         ,@SKUCode
         ,@SKU
         ,@UnitOfMeasure
         ,@Quantity
         ,@AlternatePallet
         ,@SubstitutePallet
         ,@Litres
         ,@HostId  
  
  while (@@fetch_status = 0)
  begin
    begin transaction
        
    set @SKUId = null
    
    select @SKUId = SKUId,
           @Quantity = Quantity
      from SKU (nolock)
     where SKUCode = @SKUCode
    
    if @SKU is null
      set @SKU = @SKUCode
    
    if @Quantity is null
      set @Quantity = 99999
    
    if @SKUId is null
    begin
      exec @Error = p_SKU_Insert
       @SKUId            = @SKUId output,
       @UOMId            = 1,
       @SKU              = @SKU,
       @SKUCode          = @SKUCode,
       @Quantity         = @Quantity,
       @AlternatePallet  = @AlternatePallet,
       @SubstitutePallet = @SubstitutePallet,
       @Litres           = @Litres
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_SKU_Insert'
        goto error
      end
    end
    else
    begin
      exec @Error = p_SKU_Update
       @SKUId            = @SKUId,
       @UOMId            = 1,
       @SKU              = @SKU,
       @SKUCode          = @SKUCode,
       @Quantity         = @Quantity,
       @AlternatePallet  = @AlternatePallet,
       @SubstitutePallet = @SubstitutePallet,
       @Litres           = @Litres
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_SKU_Update'
        goto error
      end
    end
    
    error:
    if @Error = 0
    begin
      update InterfaceImportSKU
         set RecordStatus = 'C'
       where InterfaceImportSKUId = @InterfaceImportSKUId
      
      commit transaction
    end
    else
    begin
      if @@trancount > 0
        rollback transaction
      
      update InterfaceImportSKU
         set RecordStatus = 'E'
       where InterfaceImportSKUId = @InterfaceImportSKUId
       
       set @Error = 0
    end
    
    fetch SKU_cursor into 
          @InterfaceImportSKUId
         ,@SKUCode
         ,@SKU
         ,@UnitOfMeasure
         ,@Quantity
         ,@AlternatePallet
         ,@SubstitutePallet
         ,@Litres
         ,@HostId  
  end
  
  close SKU_cursor
  deallocate SKU_cursor

end
