﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_Valid_Pallet
  ///   Filename       : p_Mobile_Check_Valid_Pallet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 2017-11-02
  ///   Details        : SAT01 - Do not allow for a blank location 
  /// </newpara>
*/
CREATE procedure p_Mobile_Check_Valid_Pallet
(
 @barcode nvarchar(30),
 @type    nvarchar(10) = null
)

as
begin
	 set nocount on;
	 
	 declare @bool               bit,
	         @PalletId           int,
	         @StorageUnitBatchId int,
	         @Batch              nvarchar(30)

  if @barcode = ''													-- SAT01
	set @barcode = null												-- SAT01

  if @barcode like 'P:%'
    if isnumeric(replace(@barcode,'P:','')) = 1
      select @PalletId = replace(@barcode,'P:',''),
             @barcode  = null
  
  if @Barcode is null
  begin
    if @PalletId is null
      set @PalletId = -1
    
    if exists(select top 1 1 from Pallet (nolock) where PalletId = @PalletId)
      set @bool = 1
    else
      set @bool = 0
  end
  else
  begin
    exec @StorageUnitBatchId = p_Mobile_Stock_Take_Confirm_Product
     @barcode = @barcode,
     @batch   = @batch,
     @type    = @type
    
    if @StorageUnitBatchId = -1
      set @bool = 0
    else
      set @bool = 1
  end

  select @bool

end
