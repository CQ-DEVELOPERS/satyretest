﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Update_Used
  ///   Filename       : p_Location_Update_Used.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jun 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Update_Used

as
begin
	 --set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update l
     set Used = 0
    from Location l
   where not exists(select 1 from StorageUnitBatchLocation subl (nolock) where l.LocationId = subl.LocationId)
     and l.Used = 1
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update l
     set Used = 1
    from Location l
   where exists(select 1 from StorageUnitBatchLocation subl where l.LocationId = subl.LocationId)
     and l.Used = 0
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update l
     set ActivePicking = 1
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId    = a.AreaId
   where not exists(select 1 from StorageUnitBatchLocation subl (nolock) where l.LocationId = subl.LocationId)
     and l.ActivePicking = 0
     and a.AreaCode = 'BK'
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update l
     set ActiveBinning = 1
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId    = a.AreaId
   where not exists(select 1 from StorageUnitBatchLocation subl (nolock) where l.LocationId = subl.LocationId)
     and l.ActiveBinning = 0
     and a.AreaCode = 'BK'
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update l
     set StockTakeInd = 0
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId = a.AreaId
   where not exists(select 1
                      from Instruction      i (nolock)
                      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                      join Status           s (nolock) on i.StatusId          = s.StatusId
                     where l.LocationId              = i.PickLocationId
                       and it.InstructionTypeCode like 'ST%'
                       and s.StatusCode             in ('S','W'))
     and l.StockTakeInd = 1
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Location_Update_Used'); 
    rollback transaction
    return @Error
end
