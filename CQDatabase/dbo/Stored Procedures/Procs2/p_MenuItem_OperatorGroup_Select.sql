﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_OperatorGroup_Select
  ///   Filename       : p_MenuItem_OperatorGroup_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Jan 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItem_OperatorGroup_Select
(
 @OperatorGroupId int,
 @MenuId          int
)

as
begin
  set nocount on;
  
  declare @TableResult as table
  (
    Menu            nvarchar(50),
    MenuItem1       nvarchar(50),
    MenuItem2       nvarchar(50),
    MenuItem3       nvarchar(50),
    MenuItem4       nvarchar(50),
    MenuItemId      int,
    Access          bit,
    OperatorGroupId int
  )
  
  insert @TableResult
        (Menu,
         MenuItem1,
         MenuItem2,
         MenuItem3,
         MenuItem4,
         MenuItemId,
         Access,
         OperatorGroupId)
  select m.Menu,
         mi1.MenuItem,
         null,
         null,
         null,
         mi1.MenuItemId,
         ogmi.Access,
         ogmi.OperatorGroupId
    from Menu       m
    join MenuItem mi1 on mi1.MenuId= m.MenuId and mi1.MenuId = @MenuId
    join OperatorGroupMenuItem  ogmi on mi1.MenuItemId = ogmi.MenuItemId and ogmi.OperatorGroupId = @OperatorGroupId
   where mi1.ParentMenuItemId is null

  insert @TableResult
        (Menu,
         MenuItem1,
         MenuItem2,
         MenuItem3,
         MenuItem4,
         MenuItemId,
         Access,
         OperatorGroupId)
  select m.Menu,
         mi2.MenuItem,
         mi1.MenuItem,
         null,
         null,
         mi1.MenuItemId,
         ogmi.Access,
         ogmi.OperatorGroupId
    from Menu                      m
    join MenuItem                mi1 on mi1.MenuId= m.MenuId and mi1.MenuId = @MenuId
    join MenuItem                mi2 on mi1.ParentMenuItemId = mi2.MenuItemId
    join OperatorGroupMenuItem  ogmi on mi1.MenuItemId = ogmi.MenuItemId and ogmi.OperatorGroupId = @OperatorGroupId
   where mi2.ParentMenuItemId is null

  insert @TableResult
        (Menu,
         MenuItem1,
         MenuItem2,
         MenuItem3,
         MenuItem4,
         MenuItemId,
         Access,
         OperatorGroupId)
  select m.Menu,
         mi3.MenuItem,
         mi2.MenuItem,
         mi1.MenuItem,
         null,
         mi1.MenuItemId,
         ogmi.Access,
         ogmi.OperatorGroupId
    from Menu                      m
    join MenuItem                mi1 on mi1.MenuId= m.MenuId and mi1.MenuId = @MenuId
    join MenuItem                mi2 on mi1.ParentMenuItemId = mi2.MenuItemId
    join MenuItem                mi3 on mi2.ParentMenuItemId = mi3.MenuItemId
    join OperatorGroupMenuItem  ogmi on mi1.MenuItemId = ogmi.MenuItemId and ogmi.OperatorGroupId = @OperatorGroupId
   where mi3.ParentMenuItemId is null

  select *
    from @TableResult
  order by Menu,
           MenuItem1,
           MenuItem2,
           MenuItem3
end
