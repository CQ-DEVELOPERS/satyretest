﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Replenishment_Check
  ///   Filename       : p_Replenishment_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Aug 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Replenishment_Check
(
 @StorageUnitBatchId int,
 @LocationId         int
)

as
begin
	 set nocount on;
  
  declare @Error             int = -1,
          @WarehouseId       int,
          @PickLocationId    int,
          @StorageUnitId     int,
          @Quantity          float,
          @AreaType          nvarchar(10)
  
  select @StorageUnitId = StorageUnitId
    from StorageUnitBatch (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
  
  select @WarehouseId = a.WarehouseId,
         @AreaType    = isnull(a.AreaType,'')
    from AreaLocation al (nolock)
    join Area          a (nolock) on a.AreaId = al.AreaId
   where al.LocationId = @LocationId
  
  --if (select dbo.ufn_Configuration(163, @warehouseId)) = 0
      --  if (select AreaCode
      --        from AreaLocation al (nolock)
      --        join Area          a (nolock) on al.AreaId = a.AreaId
      --       where al.LocationId = @PickLocationId) = 'SP'
      --    set @Error = 0
  
  --if @Error = 0
  --  goto end_proc
  
  -- Is stock in a pick face?
  select top 1 @Error = 0
    from Location                    l (nolock)
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
    join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
   where l.ActivePicking   = 1 -- Yes
     and sub.StorageUnitId    = @StorageUnitId
     and a.WarehouseId        = @WarehouseId
     and a.AreaCode          in ('PK','SP')
     and subl.ActualQuantity > 0
     and subl.LocationId     != @PickLocationId
     and a.AreaType           = isnull(@AreaType,'')
  
  if @Error = 0
    goto end_proc
  
  select top 1 @Error = 9
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Status             s (nolock) on i.StatusId           = s.StatusId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where s.StatusCode in ('W','S')
     and i.StoreLocationId      = @LocationId    -- Same to Location
     and sub.StorageUnitId      = @StorageUnitId -- Same product
     and it.InstructionTypeCode = 'R'            -- Replenishment
  
  if @Error = 9
    goto end_proc
  
  select top 1 @Quantity = Quantity
    from Pack      p (nolock)
    join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
   where StorageUnitId = @StorageUnitId
     and WarehouseId = @WarehouseId
  order by pt.OutboundSequence
  
  exec @Error = p_Pick_Location_Get
   @WarehouseId        = @WarehouseId,
   @locationId         = @PickLocationId output,
   @StorageUnitBatchId = @StorageUnitBatchId output,
   @Quantity           = @Quantity output,
   @Replenishment      = 1,
   @AreaType           = @AreaType
  
  if @StorageUnitBatchId = -1 -- 'No Areas defined for Product!'
  begin
    set @Error = 11
    goto end_proc
  end
  
  if @PickLocationId is null or @PickLocationId = @LocationId -- 'No stock'
  begin
    set @Error = 6
    goto end_proc
  end
  else
    set @Error = 0
  
  end_proc:
  return @Error
end
