﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorShortPickPerformance_Search
  ///   Filename       : p_OperatorShortPickPerformance_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:31
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorShortPickPerformance table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorShortPickPerformance.OperatorId,
  ///   OperatorShortPickPerformance.InstructionTypeId,
  ///   OperatorShortPickPerformance.EndDate,
  ///   OperatorShortPickPerformance.ActivityStatus,
  ///   OperatorShortPickPerformance.PickingRate,
  ///   OperatorShortPickPerformance.Units,
  ///   OperatorShortPickPerformance.UnitsShort,
  ///   OperatorShortPickPerformance.Weight,
  ///   OperatorShortPickPerformance.WeightShort,
  ///   OperatorShortPickPerformance.Instructions,
  ///   OperatorShortPickPerformance.Orders,
  ///   OperatorShortPickPerformance.OrderLines,
  ///   OperatorShortPickPerformance.Jobs,
  ///   OperatorShortPickPerformance.ActiveTime,
  ///   OperatorShortPickPerformance.DwellTime,
  ///   OperatorShortPickPerformance.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorShortPickPerformance_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         OperatorShortPickPerformance.OperatorId
        ,OperatorShortPickPerformance.InstructionTypeId
        ,OperatorShortPickPerformance.EndDate
        ,OperatorShortPickPerformance.ActivityStatus
        ,OperatorShortPickPerformance.PickingRate
        ,OperatorShortPickPerformance.Units
        ,OperatorShortPickPerformance.UnitsShort
        ,OperatorShortPickPerformance.Weight
        ,OperatorShortPickPerformance.WeightShort
        ,OperatorShortPickPerformance.Instructions
        ,OperatorShortPickPerformance.Orders
        ,OperatorShortPickPerformance.OrderLines
        ,OperatorShortPickPerformance.Jobs
        ,OperatorShortPickPerformance.ActiveTime
        ,OperatorShortPickPerformance.DwellTime
        ,OperatorShortPickPerformance.WarehouseId
    from OperatorShortPickPerformance
  
end
