﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RawPallet_Insert
  ///   Filename       : p_RawPallet_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:15
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the RawPallet table.
  /// </remarks>
  /// <param>
  ///   @PalletId nvarchar(max) = null,
  ///   @ProductCode nvarchar(max) = null,
  ///   @SKUCode nvarchar(max) = null,
  ///   @Batch nvarchar(max) = null,
  ///   @CreateDate nvarchar(max) = null,
  ///   @ExpiryDate nvarchar(max) = null,
  ///   @Weight nvarchar(max) = null 
  /// </param>
  /// <returns>
  ///   RawPallet.PalletId,
  ///   RawPallet.ProductCode,
  ///   RawPallet.SKUCode,
  ///   RawPallet.Batch,
  ///   RawPallet.CreateDate,
  ///   RawPallet.ExpiryDate,
  ///   RawPallet.Weight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RawPallet_Insert
(
 @PalletId nvarchar(max) = null,
 @ProductCode nvarchar(max) = null,
 @SKUCode nvarchar(max) = null,
 @Batch nvarchar(max) = null,
 @CreateDate nvarchar(max) = null,
 @ExpiryDate nvarchar(max) = null,
 @Weight nvarchar(max) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert RawPallet
        (PalletId,
         ProductCode,
         SKUCode,
         Batch,
         CreateDate,
         ExpiryDate,
         Weight)
  select @PalletId,
         @ProductCode,
         @SKUCode,
         @Batch,
         @CreateDate,
         @ExpiryDate,
         @Weight 
  
  select @Error = @@Error
  
  
  return @Error
  
end
