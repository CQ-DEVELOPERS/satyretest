﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorHistory_Insert
  ///   Filename       : p_OperatorHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 13:19:56
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorHistory table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @OperatorGroupId int = null,
  ///   @WarehouseId int = null,
  ///   @Operator nvarchar(100) = null,
  ///   @OperatorCode nvarchar(100) = null,
  ///   @Password nvarchar(100) = null,
  ///   @NewPassword nvarchar(100) = null,
  ///   @ActiveIndicator bit = null,
  ///   @ExpiryDate datetime = null,
  ///   @LastInstruction datetime = null,
  ///   @Printer nvarchar(100) = null,
  ///   @Port nvarchar(100) = null,
  ///   @IPAddress nvarchar(100) = null,
  ///   @CultureId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @LoginTime datetime = null,
  ///   @LogoutTime datetime = null,
  ///   @LastActivity datetime = null,
  ///   @SiteType nvarchar(40) = null,
  ///   @Name nvarchar(200) = null,
  ///   @Surname nvarchar(200) = null,
  ///   @PrincipalId int = null,
  ///   @PickAisle nvarchar(20) = null,
  ///   @StoreAisle nvarchar(20) = null,
  ///   @MenuId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @Upload bit = null 
  /// </param>
  /// <returns>
  ///   OperatorHistory.OperatorId,
  ///   OperatorHistory.OperatorGroupId,
  ///   OperatorHistory.WarehouseId,
  ///   OperatorHistory.Operator,
  ///   OperatorHistory.OperatorCode,
  ///   OperatorHistory.Password,
  ///   OperatorHistory.NewPassword,
  ///   OperatorHistory.ActiveIndicator,
  ///   OperatorHistory.ExpiryDate,
  ///   OperatorHistory.LastInstruction,
  ///   OperatorHistory.Printer,
  ///   OperatorHistory.Port,
  ///   OperatorHistory.IPAddress,
  ///   OperatorHistory.CultureId,
  ///   OperatorHistory.CommandType,
  ///   OperatorHistory.InsertDate,
  ///   OperatorHistory.LoginTime,
  ///   OperatorHistory.LogoutTime,
  ///   OperatorHistory.LastActivity,
  ///   OperatorHistory.SiteType,
  ///   OperatorHistory.Name,
  ///   OperatorHistory.Surname,
  ///   OperatorHistory.PrincipalId,
  ///   OperatorHistory.PickAisle,
  ///   OperatorHistory.StoreAisle,
  ///   OperatorHistory.MenuId,
  ///   OperatorHistory.ExternalCompanyId,
  ///   OperatorHistory.Upload 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorHistory_Insert
(
 @OperatorId int = null,
 @OperatorGroupId int = null,
 @WarehouseId int = null,
 @Operator nvarchar(100) = null,
 @OperatorCode nvarchar(100) = null,
 @Password nvarchar(100) = null,
 @NewPassword nvarchar(100) = null,
 @ActiveIndicator bit = null,
 @ExpiryDate datetime = null,
 @LastInstruction datetime = null,
 @Printer nvarchar(100) = null,
 @Port nvarchar(100) = null,
 @IPAddress nvarchar(100) = null,
 @CultureId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @LoginTime datetime = null,
 @LogoutTime datetime = null,
 @LastActivity datetime = null,
 @SiteType nvarchar(40) = null,
 @Name nvarchar(200) = null,
 @Surname nvarchar(200) = null,
 @PrincipalId int = null,
 @PickAisle nvarchar(20) = null,
 @StoreAisle nvarchar(20) = null,
 @MenuId int = null,
 @ExternalCompanyId int = null,
 @Upload bit = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorHistory
        (OperatorId,
         OperatorGroupId,
         WarehouseId,
         Operator,
         OperatorCode,
         Password,
         NewPassword,
         ActiveIndicator,
         ExpiryDate,
         LastInstruction,
         Printer,
         Port,
         IPAddress,
         CultureId,
         CommandType,
         InsertDate,
         LoginTime,
         LogoutTime,
         LastActivity,
         SiteType,
         Name,
         Surname,
         PrincipalId,
         PickAisle,
         StoreAisle,
         MenuId,
         ExternalCompanyId,
         Upload)
  select @OperatorId,
         @OperatorGroupId,
         @WarehouseId,
         @Operator,
         @OperatorCode,
         @Password,
         @NewPassword,
         @ActiveIndicator,
         @ExpiryDate,
         @LastInstruction,
         @Printer,
         @Port,
         @IPAddress,
         @CultureId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @LoginTime,
         @LogoutTime,
         @LastActivity,
         @SiteType,
         @Name,
         @Surname,
         @PrincipalId,
         @PickAisle,
         @StoreAisle,
         @MenuId,
         @ExternalCompanyId,
         @Upload 
  
  select @Error = @@Error
  
  
  return @Error
  
end
