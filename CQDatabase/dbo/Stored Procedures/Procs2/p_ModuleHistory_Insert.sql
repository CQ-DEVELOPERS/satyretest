﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ModuleHistory_Insert
  ///   Filename       : p_ModuleHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:58
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ModuleHistory table.
  /// </remarks>
  /// <param>
  ///   @ModuleId int = null,
  ///   @Module nvarchar(100) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   ModuleHistory.ModuleId,
  ///   ModuleHistory.Module,
  ///   ModuleHistory.CommandType,
  ///   ModuleHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ModuleHistory_Insert
(
 @ModuleId int = null,
 @Module nvarchar(100) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ModuleHistory
        (ModuleId,
         Module,
         CommandType,
         InsertDate)
  select @ModuleId,
         @Module,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
