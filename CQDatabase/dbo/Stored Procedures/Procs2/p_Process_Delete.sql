﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Process_Delete
  ///   Filename       : p_Process_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Nov 2013 12:48:38
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Process table.
  /// </remarks>
  /// <param>
  ///   @ProcessId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Process_Delete
(
 @ProcessId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Process
     where ProcessId = @ProcessId
  
  select @Error = @@Error
  
  
  return @Error
  
end
