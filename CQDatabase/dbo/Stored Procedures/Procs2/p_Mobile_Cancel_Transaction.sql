﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Cancel_Transaction
  ///   Filename       : p_Mobile_Cancel_Transaction.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Cancel_Transaction
(
 @instructionId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Error = 0
  
  begin transaction
  
  
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Mobile_Cancel_Transaction'); 
    rollback transaction
    select @Error
    return @Error
end
