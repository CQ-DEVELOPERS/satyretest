﻿
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Job_Update
  ///   Filename       : p_Mobile_Product_Check_Job_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Oct 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Job_Update
(
 @operatorId    int,
 @jobId         int,
 @instructionId int,
 @packQuantity  numeric(13,6),
 @newJobId      int output,
 @Boxes         int,
 @Weight        numeric(13,6),
 @newJob        bit = 0,
 @OrderNumber	nvarchar(50)
)

as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Mobile_Product_Check_Job_Update',
          @GetDate           datetime,
          @Transaction       bit = 1,
          @NewBox            int,
          @ReferenceNumber   nvarchar(30),
          @Quantity          numeric(13,6),
          @JobOrderNumber	 nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  --update Job
  --   set Weight = @Weight
  -- where JobId = @jobId
  
  --select @Error = @@ERROR
  
  --if @Error != 0
  --  goto Error
  
  --Check if the product is going to the right box with the right Order Number (Tommy)
  
 
  select @JobOrderNumber = od.OrderNumber
	from OutboundDocument	od (nolock)
	join Issue			   iss (nolock) on od.OutboundDocumentId	=	iss.OutboundDocumentId
	join IssueLine			il (nolock) on iss.IssueId				=	il.IssueId
	join Instruction		 i (nolock) on il.IssueLineId			=	i.IssueLineId
   where i.JobId = @newJobId
  
  if (@OrderNumber <> @JobOrderNumber)
  begin
  
	set @Error = -1
	set @Errormsg = @Errormsg + ' - Product does not belong against the order'
	goto Error
    
  end
  begin
    select @Quantity = @packQuantity * @Boxes
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @CheckQuantity = @Quantity
    
    if @Error <> 0
      goto error
  end
  
  --if exists(select top 1 1
  --             from Instruction
  --            where JobId = @JobId
  --              and CheckQuantity > 0
  --              and (InstructionId = @instructionId or @instructionId is null)
  --              and ((convert(numeric(13,6), CheckQuantity) / 1) % 1) != 0) -- Checks for any deciamls
  --begin
  --  set @Error = -1
  --  set @Errormsg = @Errormsg + ' - This would create decimals which is not allowed'
  --  goto Error
  --end
  
  if exists(select top 1 1
               from Instruction
              where JobId = @JobId
                and CheckQuantity > 0
                and (InstructionId = @instructionId or @instructionId is null)
                and CheckQuantity > ConfirmedQuantity) -- Checks for any mis checks
  begin
    set @Error = -1
    set @Errormsg = @Errormsg + ' - This would create negatives which is not allowed'
    goto Error
  end
  
  if @jobId != @newJobId
  begin
    --set @NewBox=NULL
    
    --exec p_Label_Reference @Value=@NewBox output,@WarehouseId=1
    
    --set @ReferenceNumber = 'NB:' + CONVERT(nvarchar(10), @NewBox) 
    
    --exec @newJobId = p_Mobile_Product_Check_New_Box
    -- @jobId             = @jobId,
    -- @operatorId        = @operatorId,
    -- @referenceNumber   = @ReferenceNumber,
    -- @Instructionid     = @Instructionid
    
    --if @Error <> 0
    --  goto error
    
    exec @Error = p_Mobile_Product_Check_Create_Jobs
     @JobId         = @jobId,
     @Jobs          = @Boxes,
     @operatorId    = @operatorId,
     @instructionId = @instructionId,
     @newJobId      = @newJobId,
     @Weight        = @Weight
     
  end
  else
  begin
    update Job
       set Weight = @Weight
     where JobId = @jobId

    select @Error = @@ERROR

    if @Error != 0
      goto Error
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
