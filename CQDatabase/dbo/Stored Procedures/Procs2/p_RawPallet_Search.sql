﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RawPallet_Search
  ///   Filename       : p_RawPallet_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:16
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the RawPallet table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   RawPallet.PalletId,
  ///   RawPallet.ProductCode,
  ///   RawPallet.SKUCode,
  ///   RawPallet.Batch,
  ///   RawPallet.CreateDate,
  ///   RawPallet.ExpiryDate,
  ///   RawPallet.Weight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RawPallet_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         RawPallet.PalletId
        ,RawPallet.ProductCode
        ,RawPallet.SKUCode
        ,RawPallet.Batch
        ,RawPallet.CreateDate
        ,RawPallet.ExpiryDate
        ,RawPallet.Weight
    from RawPallet
  
end
