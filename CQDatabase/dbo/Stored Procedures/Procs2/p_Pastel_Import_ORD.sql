﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pastel_Import_ORD
  ///   Filename       : p_Pastel_Import_ORD.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Nov 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pastel_Import_ORD

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update i
     set OutboundDocument = 1,
         RecordStatus     = 'Y',
         ProcessedDate    = @GetDate
    from InterfaceImportOrderNumbers i
    join OutboundDocument           od (nolock) on i.OrderNumber = od.OrderNumber
   where i.RecordStatus = 'N'
  
  if @Error <> 0
    goto error
  
  update i
     set InboundDocument = 1,
         RecordStatus    = 'Y',
         ProcessedDate    = @GetDate
    from InterfaceImportOrderNumbers i
    join InboundDocument            id (nolock) on i.OrderNumber = id.OrderNumber
   where i.RecordStatus = 'N'
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Pastel_Import_ORD'); 
    rollback transaction
    return @Error
end
