﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RaiserrorCode_Search
  ///   Filename       : p_RaiserrorCode_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:36
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the RaiserrorCode table.
  /// </remarks>
  /// <param>
  ///   @RaiserrorCodeId int = null output,
  ///   @RaiserrorCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   RaiserrorCode.RaiserrorCodeId,
  ///   RaiserrorCode.RaiserrorCode,
  ///   RaiserrorCode.RaiserrorMessage,
  ///   RaiserrorCode.StoredProcedure 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RaiserrorCode_Search
(
 @RaiserrorCodeId int = null output,
 @RaiserrorCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @RaiserrorCodeId = '-1'
    set @RaiserrorCodeId = null;
  
  if @RaiserrorCode = '-1'
    set @RaiserrorCode = null;
  
 
  select
         RaiserrorCode.RaiserrorCodeId
        ,RaiserrorCode.RaiserrorCode
        ,RaiserrorCode.RaiserrorMessage
        ,RaiserrorCode.StoredProcedure
    from RaiserrorCode
   where isnull(RaiserrorCode.RaiserrorCodeId,'0')  = isnull(@RaiserrorCodeId, isnull(RaiserrorCode.RaiserrorCodeId,'0'))
     and isnull(RaiserrorCode.RaiserrorCode,'%')  like '%' + isnull(@RaiserrorCode, isnull(RaiserrorCode.RaiserrorCode,'%')) + '%'
  
end
