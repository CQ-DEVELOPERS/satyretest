﻿ 
   
/*  
  /// <summary>  
  ///   Procedure Name : p_Outbound_Load_Details_Search  
  ///   Filename       : p_Outbound_Load_Details_Search.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 18 Jun 2007  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Outbound_Load_Details_Search  
(  
 @OutboundShipmentId int = null,  
 @IssueId            int = null  
)  
  
as  
begin  
  set nocount on;  
    
  if @IssueId = '-1'  
    set @IssueId = null;  
    
  select il.IssueLineId,  
         il.IssueId,  
         od.OrderNumber,  
         p.ProductCode,  
         p.Product,  
         sku.SKUCode,  
         il.Quantity,  
         s.Status,  
         su.StorageUnitId,  
         il.StorageUnitBatchId  
    from IssueLine         il  
    join Issue              i (nolock) on il.IssueId            = i.IssueId  
    join OutboundLine       ol (nolock) on il.OutboundLineId      = ol.OutboundLineId  
    join OutboundDocument   od (nolock) on i.OutboundDocumentId   = od.OutboundDocumentId  
    join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId  
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId  
    join Product            p (nolock) on su.ProductId          = p.ProductId  
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId  
    join Status             s (nolock) on il.StatusId           = s.StatusId  
   where isnull(il.IssueId,'0')  = isnull(@IssueId, isnull(il.IssueId,'0'))  
  order by ProductCode,  
           Product,  
           SKUCode,  
           Quantity  
end  
