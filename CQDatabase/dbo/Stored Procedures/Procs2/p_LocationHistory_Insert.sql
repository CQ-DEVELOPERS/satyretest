﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationHistory_Insert
  ///   Filename       : p_LocationHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2013 08:13:20
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the LocationHistory table.
  /// </remarks>
  /// <param>
  ///   @LocationId int = null,
  ///   @LocationTypeId int = null,
  ///   @Location nvarchar(30) = null,
  ///   @Ailse nvarchar(20) = null,
  ///   @Column nvarchar(20) = null,
  ///   @Level nvarchar(20) = null,
  ///   @PalletQuantity float = null,
  ///   @SecurityCode int = null,
  ///   @RelativeValue numeric(13,3) = null,
  ///   @NumberOfSUB int = null,
  ///   @StocktakeInd bit = null,
  ///   @ActiveBinning bit = null,
  ///   @ActivePicking bit = null,
  ///   @Used bit = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @Latitude decimal(16,13) = null,
  ///   @Longitude decimal(16,13) = null,
  ///   @Direction nvarchar(20) = null,
  ///   @Height numeric(13,6) = null,
  ///   @Length numeric(13,6) = null,
  ///   @Width numeric(13,6) = null,
  ///   @Volume numeric(13,6) = null 
  /// </param>
  /// <returns>
  ///   LocationHistory.LocationId,
  ///   LocationHistory.LocationTypeId,
  ///   LocationHistory.Location,
  ///   LocationHistory.Ailse,
  ///   LocationHistory.[Column],
  ///   LocationHistory.[Level],
  ///   LocationHistory.PalletQuantity,
  ///   LocationHistory.SecurityCode,
  ///   LocationHistory.RelativeValue,
  ///   LocationHistory.NumberOfSUB,
  ///   LocationHistory.StocktakeInd,
  ///   LocationHistory.ActiveBinning,
  ///   LocationHistory.ActivePicking,
  ///   LocationHistory.Used,
  ///   LocationHistory.CommandType,
  ///   LocationHistory.InsertDate,
  ///   LocationHistory.Latitude,
  ///   LocationHistory.Longitude,
  ///   LocationHistory.Direction,
  ///   LocationHistory.Height,
  ///   LocationHistory.Length,
  ///   LocationHistory.Width,
  ///   LocationHistory.Volume 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationHistory_Insert
(
 @LocationId int = null,
 @LocationTypeId int = null,
 @Location nvarchar(30) = null,
 @Ailse nvarchar(20) = null,
 @Column nvarchar(20) = null,
 @Level nvarchar(20) = null,
 @PalletQuantity float = null,
 @SecurityCode int = null,
 @RelativeValue numeric(13,3) = null,
 @NumberOfSUB int = null,
 @StocktakeInd bit = null,
 @ActiveBinning bit = null,
 @ActivePicking bit = null,
 @Used bit = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @Latitude decimal(16,13) = null,
 @Longitude decimal(16,13) = null,
 @Direction nvarchar(20) = null,
 @Height numeric(13,6) = null,
 @Length numeric(13,6) = null,
 @Width numeric(13,6) = null,
 @Volume numeric(13,6) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert LocationHistory
        (LocationId,
         LocationTypeId,
         Location,
         Ailse,
         [Column],
         [Level],
         PalletQuantity,
         SecurityCode,
         RelativeValue,
         NumberOfSUB,
         StocktakeInd,
         ActiveBinning,
         ActivePicking,
         Used,
         CommandType,
         InsertDate,
         Latitude,
         Longitude,
         Direction,
         Height,
         Length,
         Width,
         Volume)
  select @LocationId,
         @LocationTypeId,
         @Location,
         @Ailse,
         @Column,
         @Level,
         @PalletQuantity,
         @SecurityCode,
         @RelativeValue,
         @NumberOfSUB,
         @StocktakeInd,
         @ActiveBinning,
         @ActivePicking,
         @Used,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @Latitude,
         @Longitude,
         @Direction,
         @Height,
         @Length,
         @Width,
         @Volume 
  
  select @Error = @@Error
  
  
  return @Error
  
end
