﻿
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Palletise_Mix
  ///   Filename       : p_Outbound_Palletise_Mix.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE p_Outbound_Palletise_Mix
(
 @WarehouseId        int,
 @StoreLocationId    int,
 @OutboundShipmentId int,
 @IssueId            int,
 @OperatorId         int,
 @Weight             numeric(13,6),
 @Volume             numeric(13,6),
 @OneProductPerPallet bit = 0,
 @JobId              int = null output
)
AS
 
declare @TableItems as table
(
 LineNumber         int,
 IssueLineId        int,
 StorageUnitBatchId int,
 qty                numeric(13,6),
 weight             numeric(13,6),
 volume             numeric(13,6)
)
 
declare @TableSequence as table
(
 LineNumber   int,
 DropSequence int,
 AreaSequence int
)

begin
  begin transaction tran_mix_pallet
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @DropSequence       int,
          @OldDropSequence    int = -1,
          @DropSequenceCount  int,
          @AreaSequence       int,
          @OldAreaSequence    int = -1,
          @RunWeight          numeric(13,6),
          @RunVolume          numeric(13,6),
          @DropSequenceWeight numeric(13,6),
          @DropSequenceVolume numeric(13,6),
          @ItemCount          int,
          @ItemQuantity       numeric(13,6),
          @ItemWeight         numeric(13,6),
          @ItemVolume         numeric(13,6),
          @StatusId           int,
          @PriorityId         int,
          @IssueLineId        int,
          @StorageUnitBatchId int,
          @JobWeight          numeric(13,6),
          @JobVolume          numeric(13,6),
          @GetDate            datetime,
          @rowcount           int,
          @OutboundDocumentTypeCode nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @PriorityId = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = 'PM'
   
  select @OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode
    from OutboundDocumentType odt
    join OutboundDocument od (nolock) on odt.OutboundDocumentTypeId = od.OutboundDocumentTypeId
    join Issue			   i (nolock) on od.OutboundDocumentId	 = i.OutboundDocumentId
   where i.IssueId = @IssueId
  
  /**********************/
  /* Process each drop  */
  /**********************/
  INSERT @TableSequence
  select LineNumber,
         DropSequence,
         AreaSequence
    from PalletiseStock
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
  group by LineNumber, DropSequence, AreaSequence
  
  select @DropSequenceCount = @@RowCount
  
  /**********************************************/
  /* Create the job card for the first customer */
  /**********************************************/
  select @StatusId = dbo.ufn_StatusId('IS','P')
  
  select @RunWeight = 0,
         @RunVolume    = 0
  
  while @DropSequenceCount > 0
  begin
    select @OldAreaSequence = @AreaSequence,
           @OldDropSequence = @DropSequence
    
    select top 1
           @DropSequence = DropSequence,
           @AreaSequence = AreaSequence
      from @TableSequence
    order by LineNumber desc, DropSequence desc, AreaSequence
    
    delete @TableSequence
     where DropSequence = @DropSequence
       and AreaSequence = @AreaSequence
    
    select @DropSequenceCount = @DropSequenceCount - 1
    
    /**********************************/
    /* Determine if the total qty     */
    /* volume and weight of all stock */
    /* for a customer can fit on the  */
    /* pallet                         */
    /**********************************/
    select @DropSequenceWeight = isnull(SUM(UnitWeight * OriginalQuantity),0),
           @DropSequenceVolume = isnull(SUM(UnitVolume * OriginalQuantity),0)
      from PalletiseStock ps
     where DropSequence       = @DropSequence
       and AreaSequence       = @AreaSequence
       and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
       and Quantity           > 0 -- Already been used - 2012-02-04
    
    --select @StorageUnitBatchId, @JobId as '@JobId', @DropSequence as '@DropSequence', @Weight as '@Weight', @RunWeight as '@RunWeight',  @Volume as '@Volume', @RunVolume as '@RunVolume', @ItemWeight as '@ItemWeight', @ItemVolume as '@ItemVolume', @DropSequenceWeight as '@DropSequenceWeight', @DropSequenceVolume as '@DropSequenceVolume'
    
    select @RunWeight = @RunWeight + @DropSequenceWeight
    select @RunVolume = @RunVolume + @DropSequenceVolume
    
    if (select dbo.ufn_Configuration(268, @WarehouseId)) = 1 -- Outbound Palletising - Palletise across drops
    and @OneProductPerPallet = 0 -- If only allowed One Pallet Per Product overrise 268 config
      set @OldDropSequence = @DropSequence
    --else 
    --select @OldAreaSequence '@OldAreaSequence',
    --       @AreaSequence '@AreaSequence',
    --       @OldDropSequence '@OldDropSequence',
    --       @DropSequence '@DropSequence'
    
    if (@Weight < @RunWeight OR @Volume < @RunVolume)
    or (@JobId is null)
    or (@OldAreaSequence != @AreaSequence)
    or (@OldDropSequence != @DropSequence) -- Change this to mix across drops
    begin
      /*******************************/
      /* Create a new job card for   */
      /* the customer                */
      /*******************************/
      select @StatusId = dbo.ufn_StatusId('IS','P')
      
      set @JobId = null;
      
      if (dbo.ufn_Configuration(477, @WarehouseId)) = 0 
      or @JobId is null
      begin
        exec @Error = p_Job_Insert
         @JobId           = @JobId output,
         @PriorityId      = @PriorityId,
         @OperatorId      = @OperatorId,
         @StatusId        = @StatusId,
         @WarehouseId     = @WarehouseId,
         @DropSequence    = @DropSequence
        
        if @Error <> 0
          goto error
        
        exec @Error = p_OutboundPerformance_Insert
         @JobId         = @JobId,
         @CreateDate    = @GetDate
        
        if @error <> 0
          goto error
      end
      
      select @RunWeight = @DropSequenceWeight
      select @RunVolume = @DropSequenceVolume
      
      select @JobWeight = 0
      select @JobVolume = 0
    end
    
    /**************************************************/
    /* Add the customers stock items to the pallet    */
    /**************************************************/
    INSERT @TableItems
    select LineNumber,
           IssueLineId,
           StorageUnitBatchId,
           OriginalQuantity,
           isnull(UnitWeight,0) * convert(numeric(13,6), OriginalQuantity),
           isnull(UnitVolume,0) * convert(numeric(13,6), OriginalQuantity)
      from PalletiseStock
     where DropSequence       = @DropSequence
       and AreaSequence       = @AreaSequence
       and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
       and Quantity           > 0
    order by LineNumber desc, PalletQuantity, Quantity desc
    
    select @ItemCount = @@ROWCOUNT
    
    while @ItemCount > 0
    begin
      select top 1
             @IssueLineId        = IssueLineId,
             @StorageUnitBatchId = StorageUnitBatchId,
             @ItemQuantity       = qty,
             @ItemWeight         = weight,
             @ItemVolume         = volume
        from @TableItems
      
      delete @TableItems
       where isnull(IssueLineId, -1) = isnull(@IssueLineId, -1)
         and StorageUnitBatchId      = @StorageUnitBatchId
         and qty                     = @ItemQuantity
         and weight                  = @ItemWeight
         and volume                  = @ItemVolume
      
      select @ItemCount = @ItemCount - 1
      --select @JobId as '@JobId', @JobWeight as '@JobWeight', @Weight as '@Weight', @ItemWeight as '@ItemWeight'
      select @JobWeight = isnull(@JobWeight,0) + @ItemWeight
      select @JobVolume = isnull(@JobVolume,0) + @ItemVolume
      
--      select @JobWeight = sum(i.Quantity * p.Weight),
--             @JobVolume = sum(i.Quantity * p.Volume)
--        from Instruction        i (nolock)
--        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
--        join Pack               p (nolock) on sub.StorageUnitId = p.StorageUnitId
--       where i.JobId    = @JobId
--         and p.PackTypeId = 2
      
      -- Insert new job if over weight or job is null
      if @JobWeight >= @Weight OR @JobVolume >= @Volume or @JobId is null
      begin
        if @JobId is not null
        begin
          select @JobWeight = 0
          select @JobVolume = 0
        end
        
        select @StatusId = dbo.ufn_StatusId('IS','P')
        
        if (dbo.ufn_Configuration(477, @WarehouseId)) = 0 
        or @JobId is null
        begin
        exec @Error = p_Job_Insert
         @JobId           = @JobId output,
         @PriorityId      = @PriorityId,
         @OperatorId      = @OperatorId,
         @StatusId        = @StatusId,
         @WarehouseId     = @WarehouseId,
         @DropSequence    = @DropSequence
        
        if @Error <> 0
          goto error
        end
      end
      
      set @rowcount = null
      
      update Instruction
         set Quantity          = Quantity + @ItemQuantity,
             ConfirmedQuantity = isnull(ConfirmedQuantity,0) + @ItemQuantity,
             Weight            = isnull(Weight,0) + @ItemWeight
       where JobId              = @JobId
         and StorageUnitBatchId = @StorageUnitBatchId
      
      select @Error = @@ERROR, @rowcount = @@ROWCOUNT
      
      if isnull(@rowcount,0) = 0
      begin
      select @StoreLocationId = al.LocationId
        from StorageUnitBatch sub (nolock)
        join StorageUnitArea sua (nolock) on sub.StorageUnitId = sua.StorageUnitId
        join Area              a (nolock) on sua.AreaId = a.AreaId
        join AreaLocation     al (nolock) on a.CheckingAreaId = al.AreaId
       where sub.StorageUnitBatchId = @StorageUnitBatchId
      
      if (@OutboundDocumentTypeCode in ('COL'))
      begin
      
		select @StoreLocationId = odt.CheckingAreaId
		  from OutboundDocumentType odt
	     where odt.OutboundDocumentTypeCode = @OutboundDocumentTypeCode
      
      end
      
      select @Error = @@Error
    
      if @Error <> 0
        goto error
        
        if (dbo.ufn_Configuration(172, @WarehouseId)) = 1
          set @JobId = null;
        
        exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @InstructionTypeCode = 'PM',
         -------------------------------------------------------------------------------------------
         @StorageUnitBatchId  = @StorageUnitBatchId,
         -------------------------------------------------------------------------------------------
         @PickLocationId      = null,
         @StoreLocationId     = @StoreLocationId,
         @Quantity            = @ItemQuantity,
         @Weight              = @ItemWeight,
         @IssueLineId         = @IssueLineId,
         @JobId               = @JobId,
         @OutboundShipmentId  = @OutboundShipmentId,
         @DropSequence        = @DropSequence
        
        if @Error <> 0  
          goto error
      end
    end
    
    delete @TableItems
    
    delete PalletiseStock
     where DropSequence       = @DropSequence
       and AreaSequence       = @AreaSequence
       and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
  end
  
  delete @TableSequence
  
  --select SUM(OrderedQuantity) from viewIssueLine where OutboundShipmentId = 193
  --select SUM(Quantity) from Instruction where OutboundShipmentId = 193
  
  if (dbo.ufn_Configuration(477, @WarehouseId)) = 0 
    set @JobId = null;
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_Outbound_Palletise_Mix'
    rollback transaction
    return @Error
end
