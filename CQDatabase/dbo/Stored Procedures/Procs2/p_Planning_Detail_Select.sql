﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Detail_Select
  ///   Filename       : p_Planning_Detail_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Detail_Select
(
 @OutboundShipmentId int,
 @IssueId int
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   IssueId               int,
   IssueLineId           int,
   JobId                 int,
   InstructionId         int,
   OrderNumber           nvarchar(30),
   StorageUnitBatchId    int,
   StorageUnitId         int,
   ProductCode           nvarchar(30),
   Product               nvarchar(50),
   BatchId               int,
   Batch                 nvarchar(50),
   SKUCode               nvarchar(50),
   SKU                   nvarchar(50),
   PickLocationId        int,
   PickLocation          nvarchar(15),
   Quantity              float,
   InstructionType       nvarchar(30),
   Status                nvarchar(50),
   AvailabilityIndicator nvarchar(20),
   OperatorId            int,
   Operator              nvarchar(50)
  )
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @OutboundShipmentId is not null
  begin
    insert @TableResult
          (IssueLineId,
           JobId,
           InstructionId,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           Quantity,
           Status,
           InstructionType,
           PickLocationId,
           OperatorId)
    select ili.IssueLineId,
           i.JobId,
           i.InstructionId,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           ili.Quantity,
           s.Status,
           it.InstructionType,
           i.PickLocationId,
           i.OperatorId
      from IssueLineInstruction  ili (nolock)
      join Instruction             i (nolock) on ili.InstructionId    = i.InstructionId
      join InstructionType        it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
      join StorageUnitBatch      sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join Job                     j (nolock) on i.JobId              = j.JobId
      join Status                  s (nolock) on j.StatusId           = s.StatusId
     where i.OutboundShipmentId = @OutboundShipmentId
    if @@rowcount > 0
      set @IssueId = null
    else
      update tr
         set IssueId = il.IssueId,
             OrderNumber = od.OrderNumber
        from @TableResult tr
        join IssueLine    il on tr.IssueLineId = il.IssueLineId
        join Issue             i (nolock) on il.IssueId = i.IssueId
        join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      -- where i.IssueId = @IssueId
  end
  
  if @IssueId is not null
  begin
    insert @TableResult
          (IssueId,
           JobId,
           InstructionId,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           Quantity,
           Status,
           InstructionType,
           PickLocationId)
    select il.IssueId,
           i.JobId,
           i.InstructionId,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           i.Quantity,
           s.Status,
           it.InstructionType,
           i.PickLocationId
      from IssueLine         il (nolock)
      join Instruction        i (nolock) on il.IssueLineId       = i.IssueLineId
      join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join Job                     j (nolock) on i.JobId              = j.JobId
      join Status                  s (nolock) on j.StatusId           = s.StatusId
     where il.IssueId = @IssueId
--    union
--    select i.IssueId,
--           ins.JobId,
--           ins.InstructionId,
--           sub.StorageUnitBatchId,
--           sub.StorageUnitId,
--           sub.BatchId,
--           ili.Quantity,
--           s.Status,
--           it.InstructionType,
--           ins.PickLocationId
--      from IssueLineInstruction  ili (nolock)
--      join IssueLine              il (nolock) on ili.IssueLineId        = il.IssueLineId
--      join Issue                   i (nolock) on il.IssueId             = i.IssueId
--      join Instruction           ins (nolock) on ili.InstructionId      = ins.InstructionId
--      join InstructionType        it (nolock) on ins.InstructionTypeId  = it.InstructionTypeId
--      join StorageUnitBatch      sub (nolock) on ins.StorageUnitBatchId = sub.StorageUnitBatchId
--      join Status                  s (nolock) on ins.StatusId           = s.StatusId
--     where i.IssueId = @IssueId
    
    update tr
       set OrderNumber = od.OrderNumber
      from @TableResult tr
      join Issue             i (nolock) on tr.IssueId = i.IssueId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
  end
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         Batch       = b.Batch,
         SKUCode     = sku.SKUCode,
         SKU         = sku.SKU
    from @TableResult tr
    join StorageUnit      su  (nolock) on tr.StorageUnitId      = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch            b   (nolock) on tr.BatchId           = b.BatchId
  
  update @TableResult
     set AvailabilityIndicator = 'Blue'
   where PickLocation is not null
  
  update @TableResult
     set AvailabilityIndicator = 'Standard'
   where PickLocation is null
  
  select distinct JobId,
         InstructionId,
         OrderNumber,
         StorageUnitBatchId,
         StorageUnitId,
         ProductCode,
         Product,
         Batch,
         SKUCode,
         SKU,
         Quantity,
         PickLocation,
         InstructionType,
         Status,
         AvailabilityIndicator,
         Operator
    from @TableResult
  order by JobId,
           ProductCode,
           Product,
           Batch,
           SKUCode,
           Quantity
end
