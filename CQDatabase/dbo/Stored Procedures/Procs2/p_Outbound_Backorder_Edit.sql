﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Backorder_Edit
  ///   Filename       : p_Outbound_Backorder_Edit.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Backorder_Edit
(
 @issueLineId int,
 @quantity    float
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @OutboundLineId    int
  
  set @Errormsg = 'Error executing p_Outbound_Backorder_Edit'
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_IssueLine_Update
   @IssueLineId    = @IssueLineId,
   @Quantity       = @quantity
  
  if @Error <> 0
    goto error
  
  select @OutboundLineId = OutboundLineId
    from IssueLine (nolock)
   where IssueLineId = @IssueLineId
  
  if ((select sum(ConfirmedQuatity) from IssueLine (nolock) where OutboundLineId = @OutboundLineId) + @quantity)
      >
     (select sum(Quantity) from OutboundLine (nolock) where OutboundLineId = @OutboundLineId)
  begin
    set @Error = -1
    set @Errormsg = 'Line cannot be ajusted more than ordered quantity'
    goto error
  end
  
  if dbo.ufn_StatusId('IS','BP') != (select StatusId
                                       from IssueLine
                                      where IssueLineId = @issueLineId)
  begin
    set @Error = -1
    set @Errormsg = 'Line not in the correct status'
    goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
