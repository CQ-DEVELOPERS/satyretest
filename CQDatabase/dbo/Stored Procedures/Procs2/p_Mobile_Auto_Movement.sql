﻿
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Auto_Movement
  ///   Filename       : p_Mobile_Auto_Movement.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Oct 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Auto_Movement
(
@operatorId          int = null,
@storageUnitBatchId  int,
@pickLocation        nvarchar(15),
@storeLocation       nvarchar(15),
@confirmedQuantity   numeric(13,6),
@InstructionTypeCode nvarchar(10),
@WarehouseId         int = null
)

as
begin
      set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @DwellTime          int,
          @ProductCode        nvarchar(30),
          @Product            nvarchar(50),
          @SKUCode            nvarchar(50),
          @Batch              nvarchar(50),
          @PickLocationId     int,
          @PickSecurityCode   int,
          @StoreLocationId    int,
          @StoreSecurityCode  int,
--          @StorageUnitBatchId int,
          @StatusId           int,
          @InstructionTypeId  int,
          @Quantity           numeric(13,6),
          @JobId              int,
          @Weight             float,
          @InstructionId      int,
          @PriorityId         int,
          @StorageUnitId      int,
          @AreaId             int,
          @AreaCode           nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @storeLocation = replace(@storeLocation,'L:','')
  select @pickLocation = replace(@pickLocation,'L:','')
--  select @palletId = replace(@palletId, 'P:','')
  
  set @Error = 0
  
--  exec @DwellTime = p_Mobile_Dwell_Time
--   @Type       = 2,
--   @OperatorId = @OperatorId
  
  if @WarehouseId is null
    select @WarehouseId = WarehouseId
      from Operator (nolock)
     where OperatorId = @operatorId
  
--  if @pickLocation is null
--    select @PickLocationId = l.LocationId,
--           @pickLocation   = l.Location
--      from Pallet      p
--      join Location    l on p.LocationId = l.LocationId
--     where p.PalletId = @PalletId
--  else
  
  select @StorageUnitId = StorageUnitId
    from StorageUnitBatch (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
  
  select top 1
         @PickLocationId = l.LocationId
    from Location                    l (nolock)
    join AreaLocation               al (nolock) on l.Locationid = al.Locationid
    join Area                        a (nolock) on al.AreaId = a.AreaId
    join StorageUnitBatchLocation subl (nolock) on l.LocationId = subl.LocationId
                                               and subl.StorageUnitBatchId = @StorageUnitBatchId
   where l.Location = @pickLocation
     and a.WarehouseId = @WarehouseId
  
  select top 1
         @StoreLocationId = l.LocationId,
         @AreaId          = al.AreaId,
         @AreaCode        = a.AreaCode
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.Locationid = al.Locationid
    join Area          a (nolock) on al.AreaId = a.AreaId
    join StorageUnitArea sua (nolock) on a.AreaId = sua.AreaId
                                     and sua.StorageUnitId = @StorageUnitId
    left join StorageUnitLocation sul (nolock) on l.LocationId = sul.LocationId
                                     and sul.StorageUnitId = @StorageUnitId
   where l.Location = @storeLocation
     and a.WarehouseId = @WarehouseId
  order by sua.AreaId desc, sul.LocationId desc
  
  if (@StoreLocationId is null)
  begin
  
        select top 1
                  @AreaCode        = a.AreaCode
            from Location      l (nolock)
            join AreaLocation al (nolock) on l.Locationid = al.Locationid
            join Area          a (nolock) on al.AreaId = a.AreaId
         where l.Location = @storeLocation
            and a.WarehouseId = @WarehouseId
  
              if @AreaCode in ('J','JJ')
              begin
                  
                  select top 1
                                    @StoreLocationId = l.LocationId,
                                    @AreaId          = al.AreaId
                              from Location      l (nolock)
                              join AreaLocation al (nolock) on l.Locationid = al.Locationid
                              join Area          a (nolock) on al.AreaId = a.AreaId
                           where l.Location = @storeLocation
                              and a.WarehouseId = @WarehouseId
                          order by a.AreaId desc, l.LocationId desc
              
              end
              else
              select @AreaCode = null
              
  end
  
  if (dbo.ufn_Configuration(349, @warehouseId) = 1)
    if @AreaCode not in ('J','JJ','SR')
    and (select StatusCode
           from StorageUnitBatch sub
           join Batch              b on sub.BatchId = b.BatchId
           join Status             s on b.StatusId = s.StatusId
          where StorageUnitBatchId = @storageUnitBatchId) != 'A'
    begin
      begin transaction
      set @Error = -11
      set @InstructionId = -11
      set @Errormsg = 'Invalid Batch Status'
      goto result
    end
  
  if @InstructionTypeCode != 'O'
  if (@StoreLocationId is null and @AreaId is null)
  --if  not exists(select top 1 1 from StorageUnitLocation (nolock) where StorageUnitId = @StorageUnitId and LocationId = @StoreLocationId)
  --and not exists(select top 1 1 from StorageUnitArea (nolock) where StorageUnitId = @StorageUnitId and AreaId = @AreaId)
  begin
    begin transaction
    set @Error = -10
    set @InstructionId = -10
    set @Errormsg = 'Invalid Storage Area'
    goto result
  end
  
  if @InstructionTypeCode = 'O' and @StoreLocationId is null
  begin
    select top 1
           @storeLocation   = l.Location,
           @StoreLocationId = l.LocationId,
           @AreaId          = al.AreaId
      from Location      l (nolock)
      join AreaLocation al (nolock) on l.Locationid = al.Locationid
      join Area          a (nolock) on al.AreaId = a.AreaId
     where a.WarehouseId = @WarehouseId
       and a.AreaCode    = 'JJ'
  end
  
  if exists(select 1
              from Instruction        i (nolock)
              join Status             s (nolock) on i.StatusId           = s.StatusId
              join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
             where s.StatusCode in ('W','S')
               and i.InstructionTypeId = @InstructionTypeId -- Replenishment
               and i.WarehouseId       = @WarehouseId       -- Same Warehouse (not really necessary)
               and i.StoreLocationId   = @StoreLocationId   -- Same to Location
               and sub.StorageUnitId   = @StorageUnitId)    -- Same product
  begin
    begin transaction
    set @Error = -9
    set @InstructionId = -9
    set @Errormsg = 'Replenishment already created'
    goto result
  end
  
  select @Quantity = ActualQuantity
    from StorageUnitBatchLocation
   where StorageUnitBatchId = @StorageUnitBatchId
     and LocationId         = @PickLocationId
  
  begin transaction
  
  select @InstructionTypeId = InstructionTypeId,
         @PriorityId        = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = @InstructionTypeCode
  
  if @Quantity < 0 or @Quantity is null
  begin
    set @Error = 2
    goto Result
  end
  
  if @confirmedQuantity is null
    set @confirmedQuantity = 0
  
  if @confirmedQuantity > @Quantity or @confirmedQuantity is null
  begin
    set @Error = 2
    goto Result
  end
  
  set @StatusId = dbo.ufn_StatusId('I','W')
  
  exec @Error = p_Job_Insert
   @JobId       = @JobId output,
   @PriorityId  = @PriorityId,
   @OperatorId  = @OperatorId,
   @StatusId    = @StatusId,
   @WarehouseId = @WarehouseId
  
  if @Error <> 0 or @JobId is null
  begin
    set @InstructionId = -1
    goto Result
  end
  
  set @StatusId = dbo.ufn_StatusId('I','W')
  
  exec @Error = p_Instruction_Insert
   @InstructionId       = @InstructionId output,
   @InstructionTypeId   = @InstructionTypeId,
   @StorageUnitBatchId  = @StorageUnitBatchId,
   @WarehouseId         = @WarehouseId,
   @StatusId            = @StatusId,
   @JobId               = @JobId,
   @OperatorId          = @OperatorId,
   @PickLocationId      = @PickLocationId,
   @StoreLocationId     = @StoreLocationId,
   @InstructionRefId    = null,
   @Quantity            = @confirmedQuantity,
   @ConfirmedQuantity   = @confirmedQuantity,
   @Weight              = @Weight,
   @ConfirmedWeight     = 0,
--   @PalletId            = @PalletId,
   @CreateDate          = @GetDate
  
  if @Error <> 0 or @InstructionId is null
  begin
    set @InstructionId = -1
    goto Result
  end
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId = @InstructionId
  
  if @Error <> 0
    goto Result
  
  exec @Error = p_Instruction_Started
   @InstructionId = @InstructionId,
   @operatorId    = @operatorId
  
  if @Error <> 0
    goto Result
  
  exec @Error = p_StorageUnitBatchLocation_Allocate
   @InstructionId       = @instructionId,
   @Confirmed           = 1
  
  if @Error <> 0
    goto Result
  
  exec @Error = p_Instruction_Finished
   @InstructionId = @InstructionId,
   @operatorId    = @operatorId
  
  if @Error <> 0
    goto Result
  
  Result:
    if @InstructionId is null
      set @InstructionId = -1
    
    if @Error <> 0 or @InstructionId = -1
      rollback transaction
    else
      commit transaction
    
    select @InstructionId
    return @InstructionId
end
