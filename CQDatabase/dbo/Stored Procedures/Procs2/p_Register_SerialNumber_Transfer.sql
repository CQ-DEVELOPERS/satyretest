﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Register_SerialNumber_Transfer
  ///   Filename       : p_Register_SerialNumber_Transfer.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Register_SerialNumber_Transfer
(
 @SerialNumberId int,
 @InstructionId  int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_SerialNumber_Update
   @SerialNumberId     = @SerialNumberId,
   @StoreInstructionId = @InstructionId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Register_SerialNumber_Transfer'); 
    rollback transaction
    return @Error
end
