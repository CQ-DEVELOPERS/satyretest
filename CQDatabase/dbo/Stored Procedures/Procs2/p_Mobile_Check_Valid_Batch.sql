﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_Valid_Batch
  ///   Filename       : p_Mobile_Check_Valid_Batch.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
/// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 2017-11-02
  ///   Details        : SAT01 - Do not allow for a blank location 
  /// </newpara>
*/
CREATE procedure p_Mobile_Check_Valid_Batch
(
 @productCode nvarchar(30),
 @batch       nvarchar(50)
)

as
begin
	 set nocount on;
	 
	 declare @bool               bit


  if @productCode = ''												-- SAT01
	set @productCode = null											-- SAT01

  if @batch = ''													-- SAT01
	set @batch = null												-- SAT01
		 
  select @batch = replace(@batch,'A:','')
  select @batch = replace(@batch,'Q:','')
  select @batch = replace(@batch,'QC:','')
  
  --if exists(select top 1 1 from viewStock (nolock) where ProductCode = @productCode and Batch = @batch)
  
  --if @batch = ''
  --begin
  --  set @bool = 1
  --  goto Result
  --end
  
  --if exists(select top 1 1 from viewStock (nolock) where Batch = @batch)								-- SAT01
  if exists(select top 1 1 from viewStock (nolock) where ProductCode = @productCode and Batch = @batch) -- SAT01
    set @bool = 1
  else
    set @bool = 0
  
  Result:
  select @bool
end
