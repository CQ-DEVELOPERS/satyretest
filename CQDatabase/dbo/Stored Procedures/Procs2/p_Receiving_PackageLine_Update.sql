﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_PackageLine_Update
  ///   Filename       : p_Receiving_PackageLine_Update.sql
  ///   Create By      : Willis
  ///   Date Created   : Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_PackageLine_Update
(
 @PackageLineId			int,
 @StorageUnitBatchId	int,
 @OperatorId			int,
 @AcceptedQuantity		numeric(13,6),
 @DeliveryNoteQuantity	numeric(13,6),
 @RejectQuantity		numeric(13,6)
)

as
begin
	Declare @StatusCode nvarchar(10),
			@NewStatus	nvarchar(10)
	
	Declare @Return int
	Set @Return = -1

	Select @StatusCode=s.StatusCode 
		from ReceiptLinePackaging r 
			Join Status s on s.StatusId = r.StatusId
	where PackageLineId = @PackageLineId 

	--print @StatusCode
	If (@StatusCode not in ('RC','C'))
	Begin
		If Isnull(@StorageUnitBatchId,-1) = -1
			Select @StorageUnitBatchId = StorageUnitBatchId from ReceiptLinePackaging where PackageLineId = @PackageLineId
		
		--Print @StorageUnitBatchId
		
		Update ReceiptLinePackaging set Operatorid = Isnull(@OperatorId,Operatorid),
										AcceptedQuantity = Isnull(@AcceptedQuantity,AcceptedQuantity),
										DeliveryNoteQuantity = Isnull(@DeliveryNoteQuantity,DeliveryNoteQuantity),
										RejectQuantity = Isnull(@RejectQuantity,RejectQuantity)
		where PackageLineId = @PackageLineId --and ReceiptId = @ReceiptId

		If (@StatusCode = 'W')
			Update ReceiptLinePackaging set Statusid = s.StatusId
			From ReceiptLinePackaging r			
					Join Status s on s.Type = 'R' and s.StatusCode = 'R'
			where r.PackageLineId = @PackageLineId -- and ReceiptId = @ReceiptId
			Set @Return = @PackageLineId
	End
		
UpdateExit:
Select @Return
end
