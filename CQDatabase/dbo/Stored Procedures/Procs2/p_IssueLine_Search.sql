﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLine_Search
  ///   Filename       : p_IssueLine_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2013 08:43:17
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the IssueLine table.
  /// </remarks>
  /// <param>
  ///   @IssueLineId int = null output,
  ///   @IssueId int = null,
  ///   @OutboundLineId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   IssueLine.IssueLineId,
  ///   IssueLine.IssueId,
  ///   Issue.IssueId,
  ///   IssueLine.OutboundLineId,
  ///   OutboundLine.OutboundLineId,
  ///   IssueLine.StorageUnitBatchId,
  ///   StorageUnitBatch.StorageUnitBatchId,
  ///   IssueLine.StatusId,
  ///   Status.Status,
  ///   IssueLine.OperatorId,
  ///   Operator.Operator,
  ///   IssueLine.Quantity,
  ///   IssueLine.ConfirmedQuatity,
  ///   IssueLine.Weight,
  ///   IssueLine.ConfirmedWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLine_Search
(
 @IssueLineId int = null output,
 @IssueId int = null,
 @OutboundLineId int = null,
 @StorageUnitBatchId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @IssueLineId = '-1'
    set @IssueLineId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
  if @OutboundLineId = '-1'
    set @OutboundLineId = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
 
  select
         IssueLine.IssueLineId
        ,IssueLine.IssueId
        ,IssueLine.OutboundLineId
        ,IssueLine.StorageUnitBatchId
        ,IssueLine.StatusId
         ,StatusStatusId.Status as 'Status'
        ,IssueLine.OperatorId
         ,OperatorOperatorId.Operator as 'Operator'
        ,IssueLine.Quantity
        ,IssueLine.ConfirmedQuatity
        ,IssueLine.Weight
        ,IssueLine.ConfirmedWeight
    from IssueLine
    left
    join Issue IssueIssueId on IssueIssueId.IssueId = IssueLine.IssueId
    left
    join OutboundLine OutboundLineOutboundLineId on OutboundLineOutboundLineId.OutboundLineId = IssueLine.OutboundLineId
    left
    join StorageUnitBatch StorageUnitBatchStorageUnitBatchId on StorageUnitBatchStorageUnitBatchId.StorageUnitBatchId = IssueLine.StorageUnitBatchId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = IssueLine.StatusId
    left
    join Operator OperatorOperatorId on OperatorOperatorId.OperatorId = IssueLine.OperatorId
   where isnull(IssueLine.IssueLineId,'0')  = isnull(@IssueLineId, isnull(IssueLine.IssueLineId,'0'))
     and isnull(IssueLine.IssueId,'0')  = isnull(@IssueId, isnull(IssueLine.IssueId,'0'))
     and isnull(IssueLine.OutboundLineId,'0')  = isnull(@OutboundLineId, isnull(IssueLine.OutboundLineId,'0'))
     and isnull(IssueLine.StorageUnitBatchId,'0')  = isnull(@StorageUnitBatchId, isnull(IssueLine.StorageUnitBatchId,'0'))
     and isnull(IssueLine.StatusId,'0')  = isnull(@StatusId, isnull(IssueLine.StatusId,'0'))
     and isnull(IssueLine.OperatorId,'0')  = isnull(@OperatorId, isnull(IssueLine.OperatorId,'0'))
  
end
