﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequestType_Insert
  ///   Filename       : p_ManualFileRequestType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:03
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ManualFileRequestType table.
  /// </remarks>
  /// <param>
  ///   @ManualFileRequestTypeId int = null output,
  ///   @Description nvarchar(100) = null,
  ///   @DocumentTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   ManualFileRequestType.ManualFileRequestTypeId,
  ///   ManualFileRequestType.Description,
  ///   ManualFileRequestType.DocumentTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequestType_Insert
(
 @ManualFileRequestTypeId int = null output,
 @Description nvarchar(100) = null,
 @DocumentTypeCode nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
  if @ManualFileRequestTypeId = '-1'
    set @ManualFileRequestTypeId = null;
  
	 declare @Error int
 
  insert ManualFileRequestType
        (Description,
         DocumentTypeCode)
  select @Description,
         @DocumentTypeCode 
  
  select @Error = @@Error, @ManualFileRequestTypeId = scope_identity()
  
  
  return @Error
  
end
