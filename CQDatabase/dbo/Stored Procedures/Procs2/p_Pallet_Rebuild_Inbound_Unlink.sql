﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Inbound_Unlink
  ///   Filename       : p_Pallet_Rebuild_Inbound_Unlink.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Inbound_Unlink
(
 @JobId         int,
 @ReceiptLineId   int,
 @InstructionId int,
 @Quantity      float,
 @InboundShipmentId int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @NewInstructionId  int,
          @InstructionRefId  int,
          @ConfirmedQuantity float,
          @NewReceiptLineId    int,
          @NewReceiptId        int,
          @ReceiptId           int,
          @NewInboundShipmentId int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Pallet_Rebuild_Link'
  
  select @ReceiptId = ReceiptId
    from ReceiptLine (nolock)
   where ReceiptLineId = @ReceiptLineId
  
  begin transaction
  
  --select * from viewINS where InstructionId = @InstructionId
  --select * from ReceiptLineInstruction where ReceiptLineId=54
  
  if @InboundShipmentId is not null
  begin
    insert Receipt
          (InboundDocumentId
           ,PriorityId
           ,WarehouseId
           ,LocationId
           ,StatusId
           ,OperatorId
           ,DeliveryNoteNumber
           ,DeliveryDate
           ,SealNumber
           ,VehicleRegistration
           ,Remarks
           ,CreditAdviceIndicator
           ,Delivery
           ,AllowPalletise
           ,Interfaced
           ,StagingLocationId
           ,NumberOfLines
           ,ReceivingCompleteDate
           ,ParentReceiptId)
    select InboundDocumentId
           ,PriorityId
           ,WarehouseId
           ,LocationId
           ,StatusId
           ,OperatorId
           ,DeliveryNoteNumber
           ,DeliveryDate
           ,SealNumber
           ,VehicleRegistration
           ,Remarks
           ,CreditAdviceIndicator
           ,Delivery
           ,AllowPalletise
           ,Interfaced
           ,StagingLocationId
           ,NumberOfLines
           ,ReceivingCompleteDate
           ,@ReceiptId
      from Receipt
     where ReceiptId = @ReceiptId
    
    select @Error = @@Error, @NewReceiptId = SCOPE_IDENTITY()
    
    if @Error <> 0
      goto error
    
    insert ReceiptLine
          (ReceiptId
           ,InboundLineId
           ,StorageUnitBatchId
           ,StatusId
           ,OperatorId
           ,RequiredQuantity
           ,ReceivedQuantity
           ,AcceptedQuantity
           ,RejectQuantity
           ,DeliveryNoteQuantity
           ,SampleQuantity
           ,AcceptedWeight
           ,ChildStorageUnitBatchId
           ,NumberOfPallets
           ,AssaySamples
           ,ParentReceiptLineId)
    select @NewReceiptId
           ,InboundLineId
           ,StorageUnitBatchId
           ,StatusId
           ,OperatorId
           ,RequiredQuantity
           ,ReceivedQuantity
           ,AcceptedQuantity
           ,RejectQuantity
           ,DeliveryNoteQuantity
           ,SampleQuantity
           ,AcceptedWeight
           ,ChildStorageUnitBatchId
           ,NumberOfPallets
           ,AssaySamples
           ,@ReceiptLineId
      from ReceiptLine
     where ReceiptLineId = @ReceiptLineId
    
    select @Error = @@Error, @NewReceiptLineId = SCOPE_IDENTITY()
    
    if @Error <> 0
      goto error
    
    insert InboundShipment
          (StatusId
           ,WarehouseId
           ,ShipmentDate
           ,Remarks)
    select StatusId
           ,WarehouseId
           ,ShipmentDate
           ,Remarks
      from InboundShipment 
     where InboundShipmentId = @InboundShipmentId
    
    select @Error = @@Error, @NewInboundShipmentId = scope_identity()
    
    if @Error <> 0
      goto error
    
    insert InboundShipmentReceipt
          (InboundShipmentId,
           ReceiptId)
    select @NewInboundShipmentId,
           @NewReceiptId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  insert Instruction
        (ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         IssueLineId,
         ReceiptLineId,
         InstructionRefId,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         CheckQuantity,
         CheckWeight,
         --InboundShipmentId,
         DropSequence)
  select ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         @JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         IssueLineId,
         ReceiptLineId,
         InstructionRefId,
         @Quantity,
         @Quantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         null,
         null,
         --isnull(@NewInboundShipmentId, InboundShipmentId),
         DropSequence
    from Instruction
   where InstructionId = @InstructionId
  
  select @Error = @@Error, @NewInstructionId = SCOPE_IDENTITY()
  
  if @Error <> 0
    goto error
  
  --insert ReceiptLineInstruction
  --      (InboundDocumentTypeId,
  --       InboundShipmentId,
  --       InboundDocumentId,
  --       ReceiptId,
  --       ReceiptLineId,
  --       InstructionId,
  --       DropSequence,
  --       Quantity,
  --       ConfirmedQuantity,
  --       Weight,
  --       ConfirmedWeight)
  --select InboundDocumentTypeId,
  --       isnull(@NewInboundShipmentId, InboundShipmentId),
  --       InboundDocumentId,
  --       isnull(@NewReceiptId, ReceiptId),
  --       isnull(@NewReceiptLineId, ReceiptLineId),
  --       @NewInstructionId,
  --       DropSequence,
  --       @Quantity,
  --       @Quantity,
  --       null,--Weight,
  --       null --ConfirmedWeight
  --  from ReceiptLineInstruction
  -- where InstructionId = @InstructionId
  --   and ReceiptLineId   = @ReceiptLineId
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  --update ReceiptLineInstruction
  --   set ConfirmedQuantity = ConfirmedQuantity - @Quantity
  --       --ConfirmedWeight   = isnull(ConfirmedWeight, 1) / ???
  -- where InstructionId = @InstructionId
  --   and ReceiptLineId   = @ReceiptLineId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update Instruction
     set ConfirmedQuantity = ConfirmedQuantity - @Quantity
         --ConfirmedWeight   = isnull(ConfirmedWeight, 1) / ???
   where InstructionId = @InstructionId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  select @InstructionRefId  = InstructionRefId,
         @ConfirmedQuantity = ConfirmedQuantity
    from Instruction
   where InstructionId = @InstructionId
  
  exec @Error = p_Instruction_Update_ili
   @InstructionId    = @InstructionId,
   @InstructionRefId = @InstructionRefId,
   @insConfirmed     = @ConfirmedQuantity
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update_ili
   @InstructionId    = @NewInstructionId,
   @InstructionRefId = null,
   @insConfirmed     = @Quantity
  
  if @Error <> 0
    goto error
  
  --select * from viewINS where InstructionId = @InstructionId or InstructionId = @NewInstructionId
  --select * from ReceiptLineInstruction where ReceiptLineId=54
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
