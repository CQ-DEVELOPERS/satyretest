﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Query_Picking_Job
  ///   Filename       : p_Query_Picking_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Query_Picking_Job
(
 @WarehouseId     int,
 @OperatorId      int,
 @ReferenceNumber nvarchar(30)
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  JobId              int,
   ReferenceNumber    nvarchar(30),
   PalletId           int,
   OperatorId         int,
   Operator           nvarchar(50),
   StatusId           int,
   Status             nvarchar(50),
   StartDate          datetime,
   DateDifference     int,
   Indicator          nvarchar(20)
	 )
	 
	 declare @JobId    int,
	         @PalletId int
  
  if @OperatorId = -1
    set @OperatorId = null
  
  if @ReferenceNumber = '' or @ReferenceNumber = '-1'
    set @ReferenceNumber = null
  
--  if @OperatorId is null and @ReferenceNumber is null
--    goto result
  
  if isnumeric(replace(@ReferenceNumber,'J:','')) = 1
    select @JobId           = replace(@ReferenceNumber,'J:',''),
           @ReferenceNumber = null
  
  if isnumeric(replace(@referenceNumber,'P:','')) = 1
    select @PalletId        = replace(@ReferenceNumber,'P:',''),
           @ReferenceNumber = null
  
  if @ReferenceNumber is not null or @JobId is not null or @PalletId is not null
    insert @TableResult
          (JobId,
           ReferenceNumber,
           OperatorId,
           StatusId,
           Status)
    select distinct j.JobId,
           j.ReferenceNumber,
           j.OperatorId,
           j.StatusId,
           s.Status
      from Instruction            i (nolock)
      join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId or i.InstructionRefId = ili.InstructionId
      join Job                    j (nolock) on i.JobId         = j.JobId
      join Status                 s (nolock) on j.StatusId      = s.StatusId
     where i.WarehouseId = @WarehouseId
       and i.JobId                         = isnull(@JobId, i.JobId)
       and isnull(j.ReferenceNumber, '-1') = isnull(@ReferenceNumber, isnull(j.ReferenceNumber, '-1'))
       and isnull(i.PalletId,        '-1') = isnull(@PalletId,        isnull(i.PalletId,   '-1'))
       and isnull(j.OperatorId,      '-1') = isnull(@OperatorId,      isnull(j.OperatorId, '-1'))
  else if @OperatorId is null
    insert @TableResult
          (JobId,
           ReferenceNumber,
           OperatorId,
           StatusId,
           Status)
    select distinct j.JobId,
           j.ReferenceNumber,
           j.OperatorId,
           j.StatusId,
           s.Status
      from Instruction            i (nolock)
      join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId or i.InstructionRefId = ili.InstructionId
      join Job                    j (nolock) on i.JobId         = j.JobId
      join Status                 s (nolock) on j.StatusId      = s.StatusId
     where i.WarehouseId = @WarehouseId
       and i.JobId = isnull(@JobId, i.JobId)
       and isnull(j.ReferenceNumber, '-1') = isnull(@ReferenceNumber, isnull(j.ReferenceNumber, '-1'))
       and isnull(i.PalletId,        '-1') = isnull(@PalletId,        isnull(i.PalletId,   '-1'))
       and isnull(j.OperatorId,      '-1') = isnull(@OperatorId,      isnull(j.OperatorId, '-1'))
       and s.StatusCode = 'S'
  
  if @OperatorId is not null
    insert @TableResult
          (JobId,
           ReferenceNumber,
           OperatorId,
           StatusId,
           Status)
    select distinct j.JobId,
           j.ReferenceNumber,
           j.OperatorId,
           j.StatusId,
           s.Status
      from Instruction            i (nolock)
      join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId or i.InstructionRefId = ili.InstructionId
      join Job                    j (nolock) on i.JobId         = j.JobId
      join Status                 s (nolock) on j.StatusId      = s.StatusId
     where i.WarehouseId = @WarehouseId
       and i.JobId                         = isnull(@JobId, i.JobId)
       and isnull(j.OperatorId,      '-1') = isnull(@OperatorId,      isnull(j.OperatorId, '-1'))
       and (i.StartDate > convert(varchar(10), getdate(), 120)
       or   s.StatusCode = 'S')
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set StartDate = (select max(StartDate) from Instruction i (nolock) where tr.JobId = i.JobId)
    from @TableResult tr
  
  update tr
     set StartDate = (select max(StartDate) from Instruction i (nolock) where tr.JobId = i.JobId)
    from @TableResult tr
  
  update @tableResult
     set DateDifference = datediff(mi, startdate, getdate())
  
  update @TableResult
     set Indicator = 'Standard'
   where DateDifference is null
  
  update @TableResult
     set Indicator = 'Green'
   where DateDifference < 10
   
  update @TableResult
     set Indicator = 'Yellow'
   where DateDifference > 10
  
  update @TableResult
     set Indicator = 'Orange'
   where DateDifference > 15
  
  update @TableResult
     set Indicator = 'Red'
   where DateDifference > 30
  
  result:
  select JobId,
         StatusId,
         Status,
         ReferenceNumber,
         isnull(OperatorId,-1) as 'OperatorId',
         Operator,
         StartDate,
         LTRIM(STR(DateDifference/60))+':'+REPLACE(STR(DateDifference%60,2),SPACE(1),'0')+':00' [Duration],
         Indicator
    from @TableResult
   order by StartDate
end
