﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Search_Update
  ///   Filename       : p_OutboundDocument_Search_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Search_Update
(
 @WarehouseId             int = null,
 @OutboundDocumentId      int = null,
 @OutboundDocumentTypeId	 int = null,
 @ExternalCompanyCode	    nvarchar(30) = null,
 @ExternalCompany	        nvarchar(255) = null,
 @OrderNumber	            nvarchar(30) = null,
 @FromDate	               datetime = null,
 @ToDate	                 datetime = null
)

as
begin
	 set nocount on;
  
	 declare @StatusId int
  
  declare @TableResult as
  table(
        OutboundDocumentId         int,
        OrderNumber               nvarchar(30),
        ExternalCompanyId         int,
        ExternalCompanyCode       nvarchar(30),
        ExternalCompany           nvarchar(255),
        NumberOfLines             int,
        DeliveryDate              datetime,
        CreateDate                datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        OutboundDocumentTypeId    int,
        OutboundDocumentType      nvarchar(255),
        ReferenceNumber           nvarchar(30),
        PrincipalId               int,
        Principal                 nvarchar(255)
       );
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundDocumentId is null
  begin
    select @StatusId = StatusId
      from Status
     where StatusCode = 'N'
       and Type       = 'OD';
    
    insert @TableResult
          (OutboundDocumentId,
           OrderNumber,
           ExternalCompanyId,
           ExternalCompanyCode,
           ExternalCompany,
           Status,
           OutboundDocumentTypeId,
           OutboundDocumentType,
           DeliveryDate,
           CreateDate,
           ReferenceNumber,
           PrincipalId,
           Principal)
    select od.OutboundDocumentId,
           od.OrderNumber,
           ec.ExternalCompanyId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           s.Status,
           odt.OutboundDocumentTypeId,
           odt.OutboundDocumentType,
           od.DeliveryDate,
           od.CreateDate,
           od.ReferenceNumber,
           od.PrincipalId,
           p.Principal
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
      join Status               s   (nolock) on od.StatusId          = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      left
      join Principal              p (nolock) on od.PrincipalId = p.PrincipalId
     where od.OutboundDocumentTypeId    = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and ec.ExternalCompanyCode    like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany        like isnull(@ExternalCompany + '%', ec.ExternalCompany)
       and od.OrderNumber            like isnull(@OrderNumber  + '%', od.OrderNumber)
       and od.DeliveryDate        between isnull(@FromDate, od.DeliveryDate) and isnull(@ToDate, od.DeliveryDate)
       and s.StatusId                   = @StatusId
       and od.WarehouseId               = isnull(@WarehouseId, od.WarehouseId)
  end
  else
  begin
    insert @TableResult
          (OutboundDocumentId,
           OrderNumber,
           ExternalCompanyId,
           ExternalCompanyCode,
           ExternalCompany,
           Status,
           OutboundDocumentTypeId,
           OutboundDocumentType,
           DeliveryDate,
           CreateDate,
           ReferenceNumber,
           PrincipalId,
           Principal)
    select od.OutboundDocumentId,
           od.OrderNumber,
           ec.ExternalCompanyId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           s.Status,
           odt.OutboundDocumentTypeId,
           odt.OutboundDocumentType,
           od.DeliveryDate,
           od.CreateDate,
           od.ReferenceNumber,
           od.PrincipalId,
           p.Principal
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
      join Status               s   (nolock) on od.StatusId          = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      left
      join Principal              p (nolock) on od.PrincipalId = p.PrincipalId
     where od.OutboundDocumentId        = @OutboundDocumentId
  end
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from OutboundLine il
                           where t.OutboundDocumentId = il.OutboundDocumentId)
    from @TableResult t
  
  select OutboundDocumentId,
         OrderNumber,
         ExternalCompanyId,
         ExternalCompanyCode,
         ExternalCompany,
         NumberOfLines,
         DeliveryDate,
         Status,
         OutboundDocumentTypeId,
         OutboundDocumentType,
         ReferenceNumber,
         PrincipalId,
         Principal
    from @TableResult
end
