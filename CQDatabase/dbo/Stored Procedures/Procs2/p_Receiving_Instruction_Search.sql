﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Instruction_Search
  ///   Filename       : p_Receiving_Instruction_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Instruction_Search
(
 @InboundShipmentId int,
 @ReceiptId         int,
 @PageSize          int = 10,
 @PageNumber        int = 1,
 @Rowcount          int = null output
)

as
begin
	 set nocount on;
  declare @TableHeader as table
  (
   OrderNumber       nvarchar(30),
   InboundShipmentId int null,
   ReceiptId         int,
   ReceiptLineId     int
  )
  
  declare @TableResult as table
  (
   ReceiptLineId      int,
   StorageUnitBatchId int,
   ProductCode	       nvarchar(30) null,
   Product	           nvarchar(50) null,
   SKUCode            nvarchar(50) null,
   Quantity           float null,
   JobId              int null,
   StatusId           int null,
   Status             nvarchar(50) null,
   PickLocationId     int null,
   PickLocation       nvarchar(15) null,
   StoreLocationId    int null,
   StoreLocation      nvarchar(15) null,
   PalletId           int null,
   CreateDate         datetime null,
   Batch              nvarchar(50) null,
   ECLNumber          nvarchar(10) null,
   InstructionType	   nvarchar(30),
   OperatorId         int null,
   Operator           nvarchar(50) null
  )
	 declare @Error    int,
          @RowStart int,
          @RowEnd   int
  
  if @PageNumber > 0 
  begin
    set @PageNumber = @PageNumber -1;
    set @RowStart   = @PageSize * @PageNumber + 1; 
    set @RowEnd     = @RowStart + @PageSize - 1; 
    
    insert @TableHeader
          (OrderNumber,
           InboundShipmentId,
           ReceiptId,
           ReceiptLineId)
    select id.OrderNumber,
           isr.InboundShipmentId,
           r.ReceiptId,
           rl.ReceiptLineId
      from InboundDocument         id (nolock)
      join Receipt                  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
      join ReceiptLine             rl (nolock) on r.ReceiptId  = rl.ReceiptId
left outer
      join InboundShipmentReceipt isr (nolock) on r.ReceiptId  = isr.ReceiptId
     where isnull(isr.InboundShipmentId,'0') = isnull(@InboundShipmentId,'0')
       and isnull(r.ReceiptId,'0')           = isnull(@ReceiptId,'0')
    
    insert @TableResult
          (ReceiptLineId,
           StorageUnitBatchId,
           Quantity,
           JobId,
           StatusId,
           PickLocationId,
           StoreLocationId,
           PalletId,
           CreateDate,
           InstructionType,
           OperatorId)
    select j.ReceiptLineId,
           i.StorageUnitBatchId,
           i.Quantity,
           j.JobId,
           i.StatusId,
           i.PickLocationId,
           i.StoreLocationId,
           i.PalletId,
           i.CreateDate,
           it.InstructionType,
           i.OperatorId
      from @TableHeader        th
      join Job                 j   (nolock) on th.ReceiptLineId     = j.ReceiptLineId
      join Instruction         i   (nolock) on j.JobId              = i.JobId
      join InstructionType     it  (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    
    update tr
       set ProductCode = p.ProductCode,
           Product     = p.Product,
           SKUCode     = sku.SKUCode,
           Batch       = b.Batch,
           ECLNumber   = b.ECLNumber
      from @TableResult tr
      join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit         su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join Product             p   (nolock) on su.ProductId         = p.ProductId
      join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
      join Batch               b   (nolock) on sub.BatchId          = b.BatchId
    
    update tr
       set Status = s.Status
      from @TableResult tr
      join Status       s  (nolock) on tr.StatusId = s.StatusId
    
    update tr
       set PickLocation = l.Location
      from @TableResult tr
      join Location     l  (nolock) on tr.PickLocationId = l.LocationId
    
    update tr
       set StoreLocation = l.Location
      from @TableResult tr
      join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
    
    update tr
       set Operator = o.Operator
      from @TableResult tr
      join Operator     o  (nolock) on tr.OperatorId = o.OperatorId
    
    select @Rowcount = count(1) from @TableResult;
    
    -- Use row_number() function
    with Result as
    (
     select 'RowNumber' = row_number() over(order by ProductCode, SKUCode), 
            ProductCode,
            Product,
            SKUCode,
            Quantity,
            JobId,
            Status,
            PickLocation,
            StoreLocation,
            PalletId,
            CreateDate,
            Batch,
            ECLNumber,
            OrderNumber,
            InboundShipmentId,
            InstructionType,
            Operator
       from @TableHeader th
       join @TableResult tr on th.ReceiptLineId = tr.ReceiptLineId
     )
     
    select ProductCode,
           Product,
           SKUCode,
           Quantity,
           JobId,
           Status,
           PickLocation,
           StoreLocation,
           PalletId,
           CreateDate,
           Batch,
           ECLNumber,
           OrderNumber,
           InboundShipmentId,
           InstructionType,
           Operator
      from Result
     where RowNumber between @RowStart and @RowEnd
  end
end
