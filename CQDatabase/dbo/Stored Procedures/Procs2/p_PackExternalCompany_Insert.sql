﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackExternalCompany_Insert
  ///   Filename       : p_PackExternalCompany_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 21:17:02
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PackExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @WarehouseId int = null output,
  ///   @ExternalCompanyId int = null output,
  ///   @StorageUnitId int = null output,
  ///   @FromPackId int = null,
  ///   @FromQuantity float = null,
  ///   @ToPackId int = null,
  ///   @ToQuantity float = null,
  ///   @Comments ntext = null 
  /// </param>
  /// <returns>
  ///   PackExternalCompany.WarehouseId,
  ///   PackExternalCompany.ExternalCompanyId,
  ///   PackExternalCompany.StorageUnitId,
  ///   PackExternalCompany.FromPackId,
  ///   PackExternalCompany.FromQuantity,
  ///   PackExternalCompany.ToPackId,
  ///   PackExternalCompany.ToQuantity,
  ///   PackExternalCompany.Comments 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackExternalCompany_Insert
(
 @WarehouseId int = null output,
 @ExternalCompanyId int = null output,
 @StorageUnitId int = null output,
 @FromPackId int = null,
 @FromQuantity float = null,
 @ToPackId int = null,
 @ToQuantity float = null,
 @Comments ntext = null 
)

as
begin
	 set nocount on;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @FromPackId = '-1'
    set @FromPackId = null;
  
  if @ToPackId = '-1'
    set @ToPackId = null;
  
	 declare @Error int
 
  insert PackExternalCompany
        (WarehouseId,
         ExternalCompanyId,
         StorageUnitId,
         FromPackId,
         FromQuantity,
         ToPackId,
         ToQuantity,
         Comments)
  select @WarehouseId,
         @ExternalCompanyId,
         @StorageUnitId,
         @FromPackId,
         @FromQuantity,
         @ToPackId,
         @ToQuantity,
         @Comments 
  
  select @Error = @@Error, @StorageUnitId = scope_identity()
  
  
  return @Error
  
end
