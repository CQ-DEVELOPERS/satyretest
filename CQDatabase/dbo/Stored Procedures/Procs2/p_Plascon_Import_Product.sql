﻿   
/*  
  /// <summary>  
  ///   Procedure Name : p_Plascon_Import_Product  
  ///   Filename       : p_Plascon_Import_Product.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 19 Feb 2009  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    : Karen  
  ///   Modified Date  : 21 September 2010  
  ///   Details        : Create detailed error log InterfaceDashboardBulkLog  
  /// </newpara>  
*/  
CREATE procedure p_Plascon_Import_Product  
  
as  
begin  
  set nocount on;  
    
  declare @Error                           int,  
          @Errormsg                        varchar(500),  
          @GetDate                         datetime,  
          @ProductId                       int,  
          @SKUId                           int,  
          @StorageUnitBatchId              int,  
          @BatchId                         int,  
          @Quantity                        int,  
          @StatusId                        int,  
          @InterfaceImportProductId        int,  
          @RecordStatus                    char(1),  
          @ProcessedDate                   datetime,  
          @ProductCode                     varchar(30) ,  
          @Product                         varchar(255),  
          @SKUCode                         varchar(10),  
          @SKU                             varchar(10),  
          @PalletQuantity                  int,  
          @Barcode                         varchar(50),  
          @MinimumQuantity                 int,  
          @ReorderQuantity                 int,  
          @MaximumQuantity                 int,  
          @CuringPeriodDays                int,  
          @ShelfLifeDays                   int,  
          @QualityAssuranceIndicator       bit,  
          @ProductType                     varchar(20),  
          @ProductCategory                 varchar(50),  
          @OverReceipt                     numeric(13,3),  
          @RetentionSamples                int,  
          @HostId                          varchar(30),  
          @Samples                         int,  
          @PackCode                        varchar(10),  
          @PackDescription                 varchar(255),  
          @Length                          numeric(13,6),  
          @Width                           numeric(13,6),  
          @Height                          numeric(13,6),  
          @Volume                          numeric(13,6),  
          @NettWeight                      numeric(13,6),  
          @GrossWeight                     numeric(13,6),  
          @TareWeight                      numeric(13,6),  
          @PackingCategory                 char(1),  
          @PickEmpty                       bit,  
          @StackingCategory                int,  
          @MovementCategory                int,  
          @ValueCategory                   int,  
          @StoringCategory                 int,  
          @PickPartPallet                  int,  
          @PackTypeId                      int,  
          @StorageUnitId                   int,  
          @PackId                          int,  
          @WarehouseId                     int,  
          @ParentProductCode               nvarchar(30)  
    
  select @GetDate = dbo.ufn_Getdate()  
  set @Error = 0  
    
  update InterfaceImportProduct  
     set ProcessedDate = @Getdate  
   where RecordStatus in ('N','U')  
     and ProcessedDate is null
     
  update p
     set Additional1 = (select top 1 Additional1
					      from InterfaceImportProduct psub
					     where psub.ProductCode = p.ProductCode
					       and ProcessedDate = @GetDate
     					   and Additional1 <> '')
    from InterfaceImportProduct p
   where ProcessedDate = @GetDate
     and Additional1 = ''
    
  declare Product_cursor cursor for  
   select InterfaceImportProductId,  
          ProductCode,  
          Product,  
          SKUCode,  
          SKU,  
          PalletQuantity,  
          Barcode,  
          MinimumQuantity,  
          ReorderQuantity,  
          MaximumQuantity,  
          CuringPeriodDays,  
          ShelfLifeDays,  
          QualityAssuranceIndicator,  
          ProductType,  
          ProductCategory,  
          OverReceipt,  
          RetentionSamples,  
          HostId,  
          Additional1  
     from InterfaceImportProduct  
    where ProcessedDate = @Getdate  
    
  open Product_cursor  
    
  fetch Product_cursor into @InterfaceImportProductId,  
                            @ProductCode,  
                            @Product,  
                            @SKUCode,  
                            @SKU,  
                            @PalletQuantity,  
                            @Barcode,  
                            @MinimumQuantity,  
                            @ReorderQuantity,  
                            @MaximumQuantity,  
                            @CuringPeriodDays,  
                            @ShelfLifeDays,  
                            @QualityAssuranceIndicator,  
                            @ProductType,  
                            @ProductCategory,  
                            @OverReceipt,  
                            @RetentionSamples,  
                            @HostId,  
                            @ParentProductCode  
    
  while (@@fetch_status = 0)  
  begin  
    begin transaction  
      
    select @StatusId = dbo.ufn_StatusId('P','A')  
      
    if @QualityAssuranceIndicator is null  
      set @QualityAssuranceIndicator = 0  
      
    set @ProductId = null  
      
    select @ProductId = ProductId  
      from Product  
     where ProductCode = @ProductCode  
      
    if @ParentProductCode is null or @ParentProductCode = ''  
      set @ParentProductCode = @ProductCode  
      
    if @ProductId is null  
    begin  
      exec @Error = p_Product_Insert  
       @ProductId                 = @ProductId output,  
       @StatusId                  = @StatusId,  
       @ProductCode               = @ProductCode,  
       @Product                   = @Product,  
       @Barcode                   = @Barcode,  
       @MinimumQuantity           = @MinimumQuantity,  
       @ReorderQuantity           = @ReorderQuantity,  
       @MaximumQuantity           = @MaximumQuantity,  
       @CuringPeriodDays          = @CuringPeriodDays,  
       @ShelfLifeDays             = @ShelfLifeDays,  
       @QualityAssuranceIndicator = @QualityAssuranceIndicator,  
       @ProductType               = @ProductType,  
       @OverReceipt               = @OverReceipt,  
       @HostId                    = @HostId,  
       @RetentionSamples          = @RetentionSamples,  
       @Samples                   = @Samples,  
       @ParentProductCode         = @ParentProductCode  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_Product_Insert'  
        goto error  
      end  
    end  
    else  
    begin  
      exec @Error = p_Product_Update  
       @ProductId                 = @ProductId,  
       @StatusId                  = @StatusId,  
       @ProductCode               = @ProductCode,  
       @Product                   = @Product,  
       @Barcode                   = @Barcode,  
       @MinimumQuantity           = @MinimumQuantity,  
       @ReorderQuantity           = @ReorderQuantity,  
       @MaximumQuantity           = @MaximumQuantity,  
       @CuringPeriodDays          = @CuringPeriodDays,  
       @ShelfLifeDays             = @ShelfLifeDays,  
       @QualityAssuranceIndicator = @QualityAssuranceIndicator,  
       @ProductType               = @ProductType,  
       @OverReceipt               = @OverReceipt,  
       @HostId                    = @HostId,  
       @RetentionSamples          = @RetentionSamples,  
       @Samples                   = @Samples,  
       @ParentProductCode         = @ParentProductCode  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_Product_Update'  
        goto error  
      end  
    end  
      
    set @SKUId = null  
      
    select @SKUId = SKUId,  
           @Quantity = Quantity  
      from SKU (nolock)  
     where SKUCode = @SKUCode  
      
    if @SKU is null  
      set @SKU = @SKUCode  
      
    if @Quantity is null  
      set @Quantity = 99999  
      
    if @SKUId is null  
    begin  
      exec @Error = p_SKU_Insert  
       @SKUId            = @SKUId output,  
       @UOMId            = 1,  
       @SKU              = @SKU,  
       @SKUCode          = @SKUCode,  
       @Quantity         = @Quantity,  
       @AlternatePallet  = null,  
       @SubstitutePallet = null,  
       @Litres           = null  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_SKU_Insert'  
        goto error  
      end  
    end  
    else  
    begin  
      exec @Error = p_SKU_Update  
       @SKUId            = @SKUId,  
       @UOMId            = 1,  
       @SKU              = @SKU,  
       @SKUCode          = @SKUCode,  
       @Quantity         = @Quantity,  
       @AlternatePallet  = null,  
       @SubstitutePallet = null,  
       @Litres           = null  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_SKU_Update'  
        goto error  
      end  
    end  
      
    set @StorageUnitId = null  
      
    select @StorageUnitId = StorageUnitId  
      from StorageUnit  
     where ProductId = @ProductId  
       and SKUId     = @SKUId  
      
    if @StorageUnitId is null  
    begin  
      exec @Error = p_StorageUnit_Insert  
       @StorageUnitId    = @StorageUnitId output,  
       @SKUId            = @SKUId,  
       @ProductId        = @ProductId  
       --@ProductCategory  = @ProductCategory,  
       --@PackingCategory  = @PackingCategory,  
       --@PickEmpty        = @PickEmpty,  
       --@StackingCategory = @StackingCategory,  
       --@MovementCategory = @MovementCategory,  
       --@ValueCategory    = @ValueCategory,  
       --@StoringCategory  = @StoringCategory,  
       --@PickPartPallet   = @PickPartPallet,  
       --@MinimumQuantity  = @MinimumQuantity,  
       --@ReorderQuantity  = @ReorderQuantity,  
       --@MaximumQuantity  = @MaximumQuantity  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_StorageUnit_Insert'  
        goto error  
      end  
    end  
    else  
    begin  
      exec @Error = p_StorageUnit_Update  
       @StorageUnitId    = @StorageUnitId,  
       @SKUId            = @SKUId,  
       @ProductId        = @ProductId  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_StorageUnit_Update'  
        goto error  
      end  
    end  
      
    select @StatusId = dbo.ufn_StatusId('B','A')  
      
    set @BatchId = null  
      
    select @BatchId = BatchId  
      from Batch  
     where Batch = 'Default'  
      
    if @BatchId is null  
    begin  
      exec @Error = p_Batch_Insert  
         @BatchId        = @BatchId output,  
         @StatusId       = @StatusId,  
         @WarehouseId    = 1,  
         @Batch          = 'Default',  
         @CreateDate     = @getdate,  
         @ExpiryDate     = '2029-12-31',  
         @IncubationDays = 0,  
         @ShelfLifeDays  = 365,  
         @ExpectedYield  = null,  
         @ActualYield  = null,  
         @ECLNumber   = null  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_StorageUnitBatch_Insert'  
        goto error  
      end  
    end  
      
    set @StorageUnitBatchId = null  
      
    select @StorageUnitBatchId = StorageUnitBatchId  
      from StorageUnitBatch  
     where StorageUnitId = @StorageUnitId  
       and BatchId       = @BatchId  
      
    if @StorageUnitBatchId is null  
    begin  
      exec @Error = p_StorageUnitBatch_Insert  
       @StorageUnitBatchId = @StorageUnitBatchId output,  
       @BatchId            = @BatchId,  
       @StorageUnitId      = @StorageUnitId  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_StorageUnitBatch_Insert'  
        goto error  
      end  
    end  
    else  
    begin  
      exec @Error = p_StorageUnitBatch_Update  
       @StorageUnitBatchId = @StorageUnitBatchId,  
       @BatchId            = @BatchId,  
       @StorageUnitId      = @StorageUnitId  
        
      if @Error != 0  
      begin  
        select @ErrorMsg = 'Error executing p_StorageUnitBatch_Update'  
        goto error  
      end  
    end  
      
    declare Pack_cursor cursor for  
     select PackCode,  
            PackDescription,  
            Quantity,  
            ISNULL(Barcode, @Barcode),  
            Length,  
            Width,  
            Height,  
            Volume,  
            NettWeight,  
            GrossWeight,  
            TareWeight,  
            ProductCategory,  
            PackingCategory,  
            PickEmpty,  
            StackingCategory,  
            MovementCategory,  
            ValueCategory,  
            StoringCategory,  
            PickPartPallet  
       from InterfaceImportPack  
      where InterfaceImportProductId = @InterfaceImportProductId  
      
    open Pack_cursor  
      
    fetch Pack_cursor into @PackCode,  
                           @PackDescription,  
                           @Quantity,  
                           @Barcode,  
                           @Length,  
                           @Width,  
                           @Height,  
                           @Volume,  
                           @NettWeight,  
                           @GrossWeight,  
                           @TareWeight,  
                           @ProductCategory,  
                           @PackingCategory,  
                           @PickEmpty,  
                           @StackingCategory,  
                           @MovementCategory,  
                           @ValueCategory,  
                           @StoringCategory,  
                           @PickPartPallet  
      
    while (@@fetch_status = 0)  
    begin  
      set @PackTypeId = null  
        
      select @PackTypeId = PackTypeId  
        from PackType  
       where PackType = @PackCode  
        
      if @PackTypeId is null  
      begin  
        exec @Error = p_PackType_Insert  
         @PackTypeId       = @PackTypeId output,  
         @PackType         = @PackCode,  
         @InboundSequence  = 1,  
         @OutboundSequence = 1  
          
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_PackType_Insert'  
          goto error  
        end  
      end  
      else  
      begin  
        exec @Error = p_PackType_Update  
         @PackTypeId       = @PackTypeId,  
         @PackType         = @PackCode  
          
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_PackType_Update'  
          goto error  
        end  
      end  
        
      set @PackId = null  
        
      select @PackId = PackId  
        from Pack  
       where StorageUnitId = @StorageUnitId  
         and PackTypeId    = @PackTypeId  
        
      set @WarehouseId = 1  
        
      if @PackId is null  
      begin  
        exec @Error = p_Pack_Insert  
         @PackId        = @PackId output,  
         @WarehouseId   = @WarehouseId,  
         @StorageUnitId = @StorageUnitId,  
         @PackTypeId    = @PackTypeId,  
         @Quantity      = @Quantity,  
         @Barcode       = @Barcode,  
         @Length        = @Length,  
         @Width         = @Width,  
         @Height        = @Height,  
         @Volume        = @Volume,  
         @Weight        = @GrossWeight,  
         @NettWeight    = @NettWeight,  
         @TareWeight    = @TareWeight  
          
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_PackType_Insert'  
          goto error  
        end  
      end  
      else  
      begin  
        exec @Error = p_Pack_Update  
         @PackId        = @PackId,  
         @WarehouseId   = @WarehouseId,  
         @StorageUnitId = @StorageUnitId,  
         @PackTypeId    = @PackTypeId,  
         @Quantity      = @Quantity,  
         @Barcode       = @Barcode,  
         @Length        = @Length,  
         @Width         = @Width,  
         @Height        = @Height,  
         @Volume        = @Volume,  
         @Weight        = @GrossWeight,  
         @NettWeight    = @NettWeight,  
         @TareWeight    = @TareWeight  
          
        if @Error != 0  
        begin  
          select @ErrorMsg = 'Error executing p_PackType_Update'  
          goto error  
        end  
      end  
        
      fetch Pack_cursor into @PackCode,  
                             @PackDescription,  
                             @Quantity,  
                             @Barcode,  
                             @Length,  
                             @Width,  
                             @Height,  
                             @Volume,  
                             @NettWeight,  
                             @GrossWeight,  
                             @TareWeight,  
                             @ProductCategory,  
                             @PackingCategory,  
                             @PickEmpty,  
                             @StackingCategory,  
                             @MovementCategory,  
                             @ValueCategory,  
                             @StoringCategory,  
                             @PickPartPallet  
    end  
      
    close Pack_cursor  
    deallocate Pack_cursor  
      
    error:  
    if @Error = 0  
    begin  
      update InterfaceImportProduct  
         set RecordStatus = 'C'  
       where InterfaceImportProductId = @InterfaceImportProductId  
        
      commit transaction  
        
      INSERT InterfaceMessage  
    ([InterfaceMessageCode]  
    ,[InterfaceMessage]  
    ,[InterfaceId]  
    ,[InterfaceTable]  
    ,[Status]  
    ,[OrderNumber]  
    ,[CreateDate]  
    ,[ProcessedDate])  
   select 'Processed'  
    ,'Success'  
    ,@InterfaceImportProductId  
    ,'InterfaceImportProduct'  
    ,'N'  
    ,@ProductCode  
    ,@GetDate  
    ,@GetDate  
    end  
    else  
    begin  
      if @@trancount > 0  
        rollback transaction  
        
      update InterfaceImportProduct  
         set RecordStatus = 'E'  
       where InterfaceImportProductId = @InterfaceImportProductId  
         
       INSERT InterfaceMessage  
    ([InterfaceMessageCode]  
    ,[InterfaceMessage]  
    ,[InterfaceId]  
    ,[InterfaceTable]  
    ,[Status]  
    ,[OrderNumber]  
    ,[CreateDate]  
    ,[ProcessedDate])  
   select 'Failed'  
    ,@ErrorMsg  
    ,@InterfaceImportProductId  
    ,'InterfaceImportProduct'  
    ,'E'  
    ,@ProductCode  
    ,@GetDate  
    ,@GetDate  
    end  
      
    fetch Product_cursor into @InterfaceImportProductId,  
                              @ProductCode,  
                              @Product,  
                              @SKUCode,  
                              @SKU,  
                              @PalletQuantity,  
                              @Barcode,  
                              @MinimumQuantity,  
                              @ReorderQuantity,  
                              @MaximumQuantity,  
                              @CuringPeriodDays,  
                              @ShelfLifeDays,  
                              @QualityAssuranceIndicator,  
                              @ProductType,  
                              @ProductCategory,  
                              @OverReceipt,  
                              @RetentionSamples,  
                              @HostId,  
                              @ParentProductCode  
  end  
    
  close Product_cursor  
  deallocate Product_cursor  
  
  update Pack  
     set TareWeight = isnull(Weight,0) - isnull(NettWeight,0)  
   where TareWeight is null  
end  
  
