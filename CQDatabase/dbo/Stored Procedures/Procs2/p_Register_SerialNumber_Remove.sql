﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Register_SerialNumber_Remove
  ///   Filename       : p_Register_SerialNumber_Remove.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Register_SerialNumber_Remove
(
 @SerialNumberId int,
 @InstructionId  int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @JobId             int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @JobId = JobId
    from Instruction (nolock)
   where InstructionId = @InstructionId
  
  select @InstructionId = i.InstructionId
    from Instruction i (nolock)
    join Status      s (nolock) on i.StatusId = s.StatusId
   where i.JobId = @JobId
     and s.Type = 'I'
     and s.StatusCode = 'D'
  
  begin transaction
  
  exec @Error = p_SerialNumber_Update
   @SerialNumberId     = @SerialNumberId,
   @StoreInstructionId = @InstructionId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Register_SerialNumber_Remove'); 
    rollback transaction
    return @Error
end
