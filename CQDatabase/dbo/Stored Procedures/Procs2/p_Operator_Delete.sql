﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Delete
  ///   Filename       : p_Operator_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 13:19:57
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Operator table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Delete
(
 @OperatorId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Operator
     where OperatorId = @OperatorId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_OperatorHistory_Insert
         @OperatorId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
