﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_Pallet
  ///   Filename       : p_Mobile_Check_Pallet.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   @type       int,
  ///   @operatorId int,
  ///   @palletId   int,
  ///   @barcode    nvarchar(50) = null,
  ///   @batch      nvarchar(50) = null,
  ///   @quantity   float         = null
  /// </param>
  /// <returns>
  ///   Result    = 2 -- Incorrect status
  ///               3 -- Incorrect barcode
  //                4 -- Incorrect batch
  ///               5 -- Incorrect quantity
  ///   DwellTime = 0 -- OK
  ///               1 -- Reason Screen
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Check_Pallet
(
 @type       int,
 @operatorId int,
 @palletId   int,
 @barcode    nvarchar(50) = null,
 @batch      nvarchar(50) = null,
 @quantity   float         = null
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @Rowcount           int,
          @InstructionId      int,
          @StorageUnitBatchId int,
          @ProductId          int,
          @StorageUnitId      int,
          @ActualBatch        nvarchar(50),
          @StatusId           int,
          @ActualQuantity     float,
          @DwellTime          int,
          @ProductBarcode     nvarchar(50),
          @PackBarcode        nvarchar(50),
          @WarehouseId        int
  
  set @Errormsg = 'You may continue.'
  set @Error = 0
  
  exec @DwellTime = p_Mobile_Dwell_Time
   @Type       = 2,
   @OperatorId = @OperatorId
  
  select top 1
         @InstructionId      = InstructionId,
         @StorageUnitBatchId = StorageUnitBatchId,
         @StatusId           = StatusId,
         @ActualQuantity     = Quantity,
         @WarehouseId        = WarehouseId
    from Instruction
   where PalletId = @PalletId
  order by InstructionId
  
  select @Rowcount = @@rowcount
  
  if @Rowcount = 0
  begin
    set @Error = 2
    set @Errormsg = 'Incorrect status'
    goto result
  end
  
  --if @Type = 0 -- check barcode (Product or Pack)
  if(select dbo.ufn_Configuration(32, @WarehouseId)) = 1
  begin
    if @Barcode is null
    begin
      set @Error = 3
      set @Errormsg = 'Incorrect barcode'
      goto result
    end
    
    select @StorageUnitId = StorageUnitId
      from StorageUnitBatch (nolock)
     where StorageUnitBatchId = @StorageUnitBatchId
    
    select @ProductId = ProductId
      from StorageUnit (nolock)
     where StorageUnitId = @StorageUnitId
    
    select @ProductBarcode = Barcode
      from Product (nolock)
     where ProductId = @ProductId
    
    if @ProductBarcode != @barcode
    begin
      select @PackBarcode = Barcode
        from Pack
       where StorageUnitId = @StorageUnitId
         and Barcode = @barcode
      
      if @PackBarcode is null
      begin
        set @Error = 3
        set @Errormsg = 'Incorrect barcode'
        goto result
      end
    end
  end
  --else if @type = 1 -- Check batch
  if(select dbo.ufn_Configuration(34, @WarehouseId)) = 1
  begin
    select @ActualBatch = b.Batch
      from StorageUnitBatch sub (nolock)
      join Batch              b (nolock) on b.BatchId = sub.BatchId
     where StorageUnitBatchId = @StorageUnitBatchId
    
    if @batch is null
    begin
      set @Error = 4
      set @Errormsg = 'Incorrect batch'
      goto result
    end
    
    if @batch != @ActualBatch
    begin
      set @Error = 4
      set @Errormsg = 'Incorrect batch'
      goto result
    end
  end
  --else if @type = 2 -- Check quantity
  begin
    if @quantity is null
    begin
      set @Error = 5
      set @Errormsg = 'Incorrect quantity'
      goto result
    end
    
    if @quantity != @ActualQuantity
    begin
      set @Error = 5
      set @Errormsg = 'Incorrect quantity'
      goto result
    end
  end
  
  result:
    select @Error             as 'Result',
           @Errormsg          as 'Message',
           @DwellTime         as 'DwellTime'
end
