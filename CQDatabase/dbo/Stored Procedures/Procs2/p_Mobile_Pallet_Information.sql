﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Pallet_Information
  ///   Filename       : p_Mobile_Pallet_Information.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Pallet_Information
(
 @PalletId      int = null,
 @ProductCode   nvarchar(30) = null output,
 @SKUCode       nvarchar(50) = null output,
 @Batch         nvarchar(50) = null output,
 @Product       nvarchar(50) = null output,
 @InstructionId int = null
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @StorageUnitBatchId int,
          @StorageUnitId      int,
          @BatchId            int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @InstructionId is null
    select @StorageUnitBatchId = StorageUnitBatchId
      from Pallet (nolock)
     where PalletId = @PalletId
  else
    select @StorageUnitBatchId = StorageUnitBatchId
      from Instruction (nolock)
     where InstructionId = @InstructionId
  
  select @StorageUnitId = StorageUnitId,
         @BatchId       = BatchId
    from StorageUnitBatch
   where StorageUnitBatchId = @StorageUnitBatchId
  
  select @Product     = p.Product,
         @ProductCode = p.ProductCode,
         @SKUCode     = sku.SKUCode
    from StorageUnit       su (nolock)
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
   where su.StorageUnitId = @StorageUnitId
  
  select @Batch = Batch
    from Batch
   where BatchId = @BatchId
  
--  select ProductCode,
--         SKUCode,
--         Batch
--    from Pallet            pl (nolock)
--    join StorageUnitBatch sub (nolock) on pl.StorageUnitBatchId = sub.StorageUnitBatchId
--    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
--    join Product            p (nolock) on su.ProductId          = p.ProductId
--    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
--    join Batch              b (nolock) on sub.BatchId           = b.BatchId
  
  if @Error <> 0
    goto error
  
  return
  
  error:
    return @Error
end
