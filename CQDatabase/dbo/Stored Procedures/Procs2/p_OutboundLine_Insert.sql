﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundLine_Insert
  ///   Filename       : p_OutboundLine_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jan 2014 07:55:59
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundLine table.
  /// </remarks>
  /// <param>
  ///   @OutboundLineId int = null output,
  ///   @OutboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null,
  ///   @BatchId int = null,
  ///   @UnitPrice float = null,
  ///   @UOM nvarchar(60) = null,
  ///   @BOELineNumber nvarchar(100) = null,
  ///   @TariffCode nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   OutboundLine.OutboundLineId,
  ///   OutboundLine.OutboundDocumentId,
  ///   OutboundLine.StorageUnitId,
  ///   OutboundLine.StatusId,
  ///   OutboundLine.LineNumber,
  ///   OutboundLine.Quantity,
  ///   OutboundLine.BatchId,
  ///   OutboundLine.UnitPrice,
  ///   OutboundLine.UOM,
  ///   OutboundLine.BOELineNumber,
  ///   OutboundLine.TariffCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundLine_Insert
(
 @OutboundLineId int = null output,
 @OutboundDocumentId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @LineNumber int = null,
 @Quantity float = null,
 @BatchId int = null,
 @UnitPrice float = null,
 @UOM nvarchar(60) = null,
 @BOELineNumber nvarchar(100) = null,
 @TariffCode nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @OutboundLineId = '-1'
    set @OutboundLineId = null;
  
  if @OutboundDocumentId = '-1'
    set @OutboundDocumentId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
	 declare @Error int
 
  insert OutboundLine
        (OutboundDocumentId,
         StorageUnitId,
         StatusId,
         LineNumber,
         Quantity,
         BatchId,
         UnitPrice,
         UOM,
         BOELineNumber,
         TariffCode)
  select @OutboundDocumentId,
         @StorageUnitId,
         @StatusId,
         @LineNumber,
         @Quantity,
         @BatchId,
         @UnitPrice,
         @UOM,
         @BOELineNumber,
         @TariffCode 
  
  select @Error = @@Error, @OutboundLineId = scope_identity()
  
  if @Error = 0
    exec @Error = p_OutboundLineHistory_Insert
         @OutboundLineId = @OutboundLineId,
         @OutboundDocumentId = @OutboundDocumentId,
         @StorageUnitId = @StorageUnitId,
         @StatusId = @StatusId,
         @LineNumber = @LineNumber,
         @Quantity = @Quantity,
         @BatchId = @BatchId,
         @UnitPrice = @UnitPrice,
         @UOM = @UOM,
         @BOELineNumber = @BOELineNumber,
         @TariffCode = @TariffCode,
         @CommandType = 'Insert'
  
  return @Error
  
end
