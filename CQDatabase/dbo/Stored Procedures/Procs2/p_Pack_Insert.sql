﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pack_Insert
  ///   Filename       : p_Pack_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Nov 2012 14:41:23
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Pack table.
  /// </remarks>
  /// <param>
  ///   @PackId int = null output,
  ///   @StorageUnitId int = null,
  ///   @PackTypeId int = null,
  ///   @Quantity int = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @Length int = null,
  ///   @Width int = null,
  ///   @Height int = null,
  ///   @Volume numeric(13,3) = null,
  ///   @Weight numeric(13,3) = null,
  ///   @WarehouseId int = null,
  ///   @NettWeight numeric(13,6) = null,
  ///   @TareWeight numeric(13,6) = null,
  ///   @ProductionQuantity float = null,
  ///   @StatusId int = null 
  /// </param>
  /// <returns>
  ///   Pack.PackId,
  ///   Pack.StorageUnitId,
  ///   Pack.PackTypeId,
  ///   Pack.Quantity,
  ///   Pack.Barcode,
  ///   Pack.Length,
  ///   Pack.Width,
  ///   Pack.Height,
  ///   Pack.Volume,
  ///   Pack.Weight,
  ///   Pack.WarehouseId,
  ///   Pack.NettWeight,
  ///   Pack.TareWeight,
  ///   Pack.ProductionQuantity,
  ///   Pack.StatusId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pack_Insert
(
 @PackId int = null output,
 @StorageUnitId int = null,
 @PackTypeId int = null,
 @Quantity int = null,
 @Barcode nvarchar(100) = null,
 @Length int = null,
 @Width int = null,
 @Height int = null,
 @Volume numeric(13,3) = null,
 @Weight numeric(13,3) = null,
 @WarehouseId int = null,
 @NettWeight numeric(13,6) = null,
 @TareWeight numeric(13,6) = null,
 @ProductionQuantity float = null,
 @StatusId int = null 
)

as
begin
	 set nocount on;
  
  if @PackId = '-1'
    set @PackId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @PackTypeId = '-1'
    set @PackTypeId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
	 declare @Error int
 
  insert Pack
        (StorageUnitId,
         PackTypeId,
         Quantity,
         Barcode,
         Length,
         Width,
         Height,
         Volume,
         Weight,
         WarehouseId,
         NettWeight,
         TareWeight,
         ProductionQuantity,
         StatusId)
  select @StorageUnitId,
         @PackTypeId,
         @Quantity,
         @Barcode,
         @Length,
         @Width,
         @Height,
         @Volume,
         @Weight,
         @WarehouseId,
         @NettWeight,
         @TareWeight,
         @ProductionQuantity,
         @StatusId 
  
  select @Error = @@Error, @PackId = scope_identity()
  
  
  return @Error
  
end
