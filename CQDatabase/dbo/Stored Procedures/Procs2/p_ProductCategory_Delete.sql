﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductCategory_Delete
  ///   Filename       : p_ProductCategory_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:59
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ProductCategory table.
  /// </remarks>
  /// <param>
  ///   @ProductCategoryId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductCategory_Delete
(
 @ProductCategoryId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ProductCategory
     where ProductCategoryId = @ProductCategoryId
  
  select @Error = @@Error
  
  
  return @Error
  
end
