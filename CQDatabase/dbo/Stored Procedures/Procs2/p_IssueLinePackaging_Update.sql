﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLinePackaging_Update
  ///   Filename       : p_IssueLinePackaging_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:54
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the IssueLinePackaging table.
  /// </remarks>
  /// <param>
  ///   @PackageLineId int = null,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null,
  ///   @JobId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLinePackaging_Update
(
 @PackageLineId int = null,
 @IssueId int = null,
 @IssueLineId int = null,
 @JobId int = null,
 @StorageUnitBatchId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @Quantity float = null 
)

as
begin
	 set nocount on;
  
  if @PackageLineId = '-1'
    set @PackageLineId = null;
  
	 declare @Error int
 
  update IssueLinePackaging
     set IssueId = isnull(@IssueId, IssueId),
         IssueLineId = isnull(@IssueLineId, IssueLineId),
         JobId = isnull(@JobId, JobId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         StatusId = isnull(@StatusId, StatusId),
         OperatorId = isnull(@OperatorId, OperatorId),
         Quantity = isnull(@Quantity, Quantity) 
   where PackageLineId = @PackageLineId
  
  select @Error = @@Error
  
  
  return @Error
  
end
