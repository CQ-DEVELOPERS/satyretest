﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Method_Delete
  ///   Filename       : p_Method_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:04
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Method table.
  /// </remarks>
  /// <param>
  ///   @MethodId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Method_Delete
(
 @MethodId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Method
     where MethodId = @MethodId
  
  select @Error = @@Error
  
  
  return @Error
  
end
