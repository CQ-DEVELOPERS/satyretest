﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RawPics_Insert
  ///   Filename       : p_RawPics_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:17
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the RawPics table.
  /// </remarks>
  /// <param>
  ///   @Data nvarchar(max) = null 
  /// </param>
  /// <returns>
  ///   RawPics.Data 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RawPics_Insert
(
 @Data nvarchar(max) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert RawPics
        (Data)
  select @Data 
  
  select @Error = @@Error
  
  
  return @Error
  
end
