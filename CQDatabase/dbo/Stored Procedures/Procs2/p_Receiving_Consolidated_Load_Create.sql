﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Consolidated_Load_Create
  ///   Filename       : p_Receiving_Consolidated_Load_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Mar 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Consolidated_Load_Create
(
 @OperatorId        int,
 @InboundShipmentId int
)

as
begin
	 set nocount on;
  
  declare @Error                 int,
          @Errormsg              varchar(500),
          @GetDate               datetime,
          @InboundDocumentId     int,
          @WarehouseId           int,
          @OrderNumber           nvarchar(30),
          @ReferenceNumber       nvarchar(30),
          @DeliveryDate          datetime,
          @InboundDocumentTypeId int,
          @InboundLineId         int,
          @StorageUnitId         int,
          @Quantity              float,
          @Id                    int
  
  declare @TableResult as table
  (
   Id            int primary key identity,
   StorageUnitId int,
   Quantity      float
  )
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @DeliveryDate = convert(datetime, @GetDate, 20)
  
  begin transaction
  
  select @InboundDocumentTypeId = InboundDocumentTypeId
    from InboundDocumentType
   where InboundDocumentTypeCode = 'GRV'
  
  if @InboundDocumentTypeId is null
  begin
    set @Error = -1
    goto error
  end
  
  select @WarehouseId = WarehouseId,
         @DeliveryDate = isnull(ShipmentDate, @GetDate)
    from InboundShipment (nolock)
   where InboundShipmentId = @InboundShipmentId
  
  select @ReferenceNumber = convert(nvarchar(30), @InboundShipmentId)
  
  exec p_InboundDocument_Create
   @InboundDocumentId     = @InboundDocumentId output,
   @InboundDocumentTypeId = @InboundDocumentTypeId,
   @WarehouseId           = @WarehouseId,
   @ReferenceNumber       = @ReferenceNumber,
   --@OrderNumber           = 'ISR',
   @DeliveryDate          = @DeliveryDate,
   @CreateDate            = @Getdate,
   @Remarks               = null
  
  if @Error <> 0
    goto error
  
  insert @TableResult
        (StorageUnitId,
         Quantity)
  select sub.StorageUnitId,
         sum(rl.RequiredQuantity)
    from InboundShipmentReceipt isr (nolock)
    join ReceiptLine             rl (nolock) on isr.ReceiptId = rl.ReceiptId
    join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId =  sub.StorageUnitBatchId
   where isr.InboundShipmentId = @InboundShipmentId
  group by sub.StorageUnitId
  
  if @Error <> 0
    goto error
  
  while exists(select top 1 1 from @TableResult)
  begin
    select @Id = Id,
           @StorageUnitId = StorageUnitId,
           @Quantity = Quantity
      from @TableResult
    
    delete @TableResult where Id = @Id
    
    exec p_InboundLine_Create
     @OperatorId        = @OperatorId,
     @InboundLineId     = @InboundLineId output,
     @InboundDocumentId = @InboundDocumentId,
     @StorageUnitId     = @StorageUnitId,
     @Quantity          = @Quantity
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Receiving_Consolidated_Load_Create'); 
    rollback transaction
    return @Error
end
