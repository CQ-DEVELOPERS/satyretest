﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Unlink
  ///   Filename       : p_Pallet_Rebuild_Unlink.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Unlink
(
 @JobId         int,
 @IssueLineId   int,
 @InstructionId int,
 @Quantity      float
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @NewInstructionId  int,
          @InstructionRefId  int,
          @ConfirmedQuantity float
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Pallet_Rebuild_Link'
  
  begin transaction
  
  --select * from viewINS where InstructionId = @InstructionId
  --select * from IssueLineInstruction where IssueLineId=54
  
  insert Instruction
        (ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         ReceiptLineId,
         IssueLineId,
         InstructionRefId,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         CheckQuantity,
         CheckWeight,
         OutboundShipmentId,
         DropSequence)
  select ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         @JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         ReceiptLineId,
         IssueLineId,
         InstructionRefId,
         @Quantity,
         @Quantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         null,
         null,
         OutboundShipmentId,
         DropSequence
    from Instruction
   where InstructionId = @InstructionId
  
  select @Error = @@Error, @NewInstructionId = SCOPE_IDENTITY()
  
  insert IssueLineInstruction
        (OutboundDocumentTypeId,
         OutboundShipmentId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         InstructionId,
         DropSequence,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight)
  select OutboundDocumentTypeId,
         OutboundShipmentId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         @NewInstructionId,
         DropSequence,
         @Quantity,
         @Quantity,
         null,--Weight,
         null --ConfirmedWeight
    from IssueLineInstruction
   where InstructionId = @InstructionId
     and IssueLineId   = @IssueLineId
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  update IssueLineInstruction
     set ConfirmedQuantity = ConfirmedQuantity - @Quantity
         --ConfirmedWeight   = isnull(ConfirmedWeight, 1) / ???
   where InstructionId = @InstructionId
     and IssueLineId   = @IssueLineId
  
  select @Error = @@Error
  
  update Instruction
     set ConfirmedQuantity = ConfirmedQuantity - @Quantity
         --ConfirmedWeight   = isnull(ConfirmedWeight, 1) / ???
   where InstructionId = @InstructionId
  
  select @Error = @@Error
  
  select @InstructionRefId  = InstructionRefId,
         @ConfirmedQuantity = ConfirmedQuantity
    from Instruction
   where InstructionId = @InstructionId
  
  exec @Error = p_Instruction_Update_ili
   @InstructionId    = @InstructionId,
   @InstructionRefId = @InstructionRefId,
   @insConfirmed     = @ConfirmedQuantity
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update_ili
   @InstructionId    = @NewInstructionId,
   @InstructionRefId = null,
   @insConfirmed     = @Quantity
  
  if @Error <> 0
    goto error
  
  --select * from viewINS where InstructionId = @InstructionId or InstructionId = @NewInstructionId
  --select * from IssueLineInstruction where IssueLineId=54
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
