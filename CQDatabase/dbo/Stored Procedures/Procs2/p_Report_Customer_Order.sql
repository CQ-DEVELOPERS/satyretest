﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Customer_Order
  ///   Filename       : p_Report_Customer_Order.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 28 March 2011
  ///   Details        : Added Nett weight
  /// </newpara>
*/
CREATE procedure p_Report_Customer_Order
(
 @ConnectionString   nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @OutboundShipmentId int,
 @IssueId            int
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   LineNumber         int,
   OutboundShipmentId int,
   IssueId            int,
   IssueLineId        int,
   OutboundDocumentId int,
   OrderNumber        nvarchar(30),
   ExternalCompanyId  int,
   ExternalCompany    nvarchar(255),
   ExternalCompanyCode nvarchar(30),
   DespatchDate       datetime,
   Pallets            int,
   JobId              int,
   InstructionId      int,
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Quantity           float,
   ConfirmedQuantity  float,
   ShortQuantity      float,
   DropSequence       int,
   RouteId            int,
   Route              nvarchar(50),
   NettWeight	  	  float
  )
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
	 
	 if @OutboundShipmentId is not null
	   set @IssueId = null
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         OutboundDocumentId,
         IssueLineId,
         JobId,
         InstructionId,
         StorageUnitBatchId,
         Quantity,
         ConfirmedQuantity,
         ShortQuantity)
  select ili.OutboundShipmentId,
         ili.IssueId,
         ili.OutboundDocumentId,
         ili.IssueLineId,
         ins.JobId,
         ins.InstructionId,
         ins.StorageUnitBatchId,
         ili.Quantity,
         isnull(ili.ConfirmedQuantity, 0),
         ili.Quantity - isnull(ili.ConfirmedQuantity, 0)
    from IssueLineInstruction  ili (nolock)
    join Instruction           ins (nolock) on ili.InstructionId    = ins.InstructionId
   where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId, -1))
     and isnull(ili.IssueId, -1)            = isnull(@IssueId, isnull(ili.IssueId, -1))
  
  update tr
     set DespatchDate = os.ShipmentDate,
         RouteId      = os.RouteId,
         DropSequence = osi.DropSequence
    from @TableResult     tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId = osi.OutboundShipmentId
   where tr.OutboundShipmentId is not null
  
  update tr
     set DespatchDate = i.DeliveryDate,
         RouteId      = i.RouteId,
         DropSequence = i.DropSequence
    from @TableResult tr
    join Issue         i (nolock) on tr.IssueId = i.IssueId
   where tr.OutboundShipmentId is null
  
  update tr
     set LineNumber = ol.LineNumber
    from @TableResult tr
    join IssueLine    il on tr.IssueLineId    = il.IssueLineId
    join OutboundLine ol on il.OutboundLineId = ol.OutboundLineId
  
  update tr
     set Route = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set OrderNumber       = od.OrderNumber,
         ExternalCompanyId = od.ExternalCompanyId
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set ExternalCompany     = ec.ExternalCompany,
         ExternalCompanyCode = ec.ExternalCompanyCode
    from @TableResult    tr
    join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode
    from @TableResult    tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  update tr
     set Pallets = (select count(distinct(tr2.JobId))
                      from @TableResult tr2
                     where tr.OrderNumber = tr2.OrderNumber)
    from @TableResult tr


    
    declare @InboundSequence smallint,
		 @PackTypeId	int,
		 @PackType   	nvarchar(30),
		 @ShowWeight	bit = null
		 
  select @ShowWeight = Indicator
  from Configuration where ConfigurationId = 206
 
  select @InboundSequence = max(InboundSequence)
  from PackType (nolock) 
  
  select @PackTypeId = pt.PackTypeId
  from PackType pt  where @InboundSequence = pt.InboundSequence

  if  @ShowWeight = 1
    --  update tr
    -- set tr.NettWeight = ((select max(pk.NettWeight)
    --                   from StorageUnitBatch       sub (nolock) 
				--	   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
				--	   join Product                  p (nolock) on su.ProductId         = p.ProductId
				--	   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
				--	   where pk.PackTypeId = @PackTypeId
    --                   and p.ProductCode = tr.ProductCode
    --                   and isnull(su.ProductCategory,'') != 'V')* tr.ConfirmedQuantity)
    
    --from @TableResult tr
    
   update tr
     set tr.NettWeight = ((select max(isnull(i.NettWeight,(i.ConfirmedWeight)))
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   join Instruction				 i (nolock) on i.InstructionId    = tr.InstructionId
					   where pk.PackTypeId = @PackTypeId
                       and p.ProductCode = tr.ProductCode
                       and isnull(su.ProductCategory,'') = 'V'))                                             
  from @TableResult tr
  if  @ShowWeight = 1
      update tr
     set tr.NettWeight = ((select max(pk.NettWeight)
                       from StorageUnitBatch       sub (nolock) 
					   join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
					   join Product                  p (nolock) on su.ProductId         = p.ProductId
					   join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
					   where pk.PackTypeId = @PackTypeId
                       and p.ProductCode = tr.ProductCode
                       and isnull(su.ProductCategory,'') != 'V')* tr.ConfirmedQuantity)
    
    from @TableResult tr
    where tr.NettWeight = 0 or tr.NettWeight is null
  
  select OutboundShipmentId,
         DropSequence,
         Route,
         OrderNumber,
         LineNumber,
         DespatchDate,
         ExternalCompanyCode,
         ExternalCompany,
         Pallets,
         ProductCode,
         Product,
         SKUCode,
         sum(Quantity)          as 'Quantity',
         sum(ConfirmedQuantity) as 'ConfirmedQuantity',
         sum(ShortQuantity)     as 'ShortQuantity',
         sum(NettWeight)        as 'NettWeight'
    from @TableResult
  group by OutboundShipmentId,
         DropSequence,
         Route,
         OrderNumber,
         LineNumber,
         DespatchDate,
         ExternalCompanyCode,
         ExternalCompany,
         Pallets,
         ProductCode,
         Product,
         SKUCode
  order by OutboundShipmentId, OrderNumber, LineNumber
end
