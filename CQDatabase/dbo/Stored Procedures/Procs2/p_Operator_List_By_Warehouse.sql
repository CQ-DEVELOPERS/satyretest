﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_List_By_Warehouse
  ///   Filename       : p_Operator_List_By_Warehouse.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Dec 2007 11:41:55
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Operator table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Operator.OperatorId,
  ///   Operator.Operator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_List_By_Warehouse
(
 @warehouseId int
)

as
begin
	 set nocount on;
  
	 declare @Error int
  select
         '-1' as OperatorId,
         '{All}' as Operator 
  union
  select
         Operator.OperatorId,
         Operator.Operator 
    from Operator
   where WarehouseId = @warehouseId
     and isnull(ActiveIndicator,1) = 1
  order by Operator
end
