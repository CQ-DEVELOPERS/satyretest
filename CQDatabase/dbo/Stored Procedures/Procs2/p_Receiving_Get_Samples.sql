﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Get_Samples
  ///   Filename       : p_Receiving_Get_Samples.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Get_Samples
(
 @receiptLineId int
)

as
begin
	 set nocount on;
	 
	 declare @StorageUnitId int
	 
	 select p.RetentionSamples,
         p.Samples as 'PharmacySamples',
         p.AssaySamples
	   from ReceiptLine       rl (nolock)
	   join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
	   join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
	   join Product            p (nolock) on su.ProductId = p.ProductId
	  where rl.ReceiptLineId = @receiptLineId
end
