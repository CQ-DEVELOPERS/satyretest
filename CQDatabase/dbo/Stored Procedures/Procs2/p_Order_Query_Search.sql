﻿
   
/*  
  /// <summary>  
  ///   Procedure Name : p_Order_Query_Search  
  ///   Filename       : p_Order_Query_Search.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 15 Jul 2008  
  /// </summary>  
  /// <summary>  
  ///   Procedure Name : p_Order_Query_Search  
  ///   Filename       : p_Order_Query_Search.sql  
  ///   Modified By      : Junaid Desai  
  ///   Date Modified   : 18 Jul 2008  
  /// </summary>  
  /// <remarks>  
  ///   Modified so that Percentage may be coorect value. the initial sp will have to be changed.  
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :  Karen 
  ///   Modified Date  :  25 Jan 2011
  ///   Details        :  If the delivery date has a time of zero then default it to 17:00 so that the 
  ///                     report does not indicate "overdue"
  /// </newpara>  
*/  
CREATE procedure p_Order_Query_Search  
(
 @WarehouseId       int = null,
 @OperatorId        int = null,
 @OrderNumber       nvarchar(30) = null,
 @InvoiceNumber     nvarchar(30) = null,
 @FromDate          datetime = null,
 @ToDate            datetime = null,
 @Status            nvarchar(20) = null
)  
  
as  
begin  
  set nocount on;
  
  declare @ExternalCompanyId int,
          @PrincipalId       int
    
  create table #TableResult
  (  
   OrderNumber                     nvarchar(30),  
   InvoiceNumber                   nvarchar(30),  
   OutboundDocumentTypeId          int,  
   OutboundDocumentTypeCode        nvarchar(10),  
   OutboundDocumentType            nvarchar(50),  
   OutboundShipmentId              int,  
   IssueId                         int,  
   ExternalCompanyId               int,  
   ExternalCompany                 nvarchar(255),  
   ExternalCompanyCode             nvarchar(30),  
   CreateDate                      datetime,  
   DeliveryDate                    datetime,  
   PlanningComplete                datetime,  
   Release                         datetime,  
   Checking                        datetime,  
   Checked                         datetime,  
   Despatch                        datetime,  
   DespatchChecked                 datetime,  
   Complete                        datetime,  
   PickComplete                    float,  
   PickTotal                       float,  
   PickPercentage                  float,  
   CheckingComplete                float,  
   CheckingTotal                   float,  
   CheckingPercentage              float,  
   Status                          nvarchar(50),  
   Extracted                       datetime,  
   ReceivedFromHost                datetime,  
   Data                            nvarchar(255),  
   TotalQuantity                   float,
   Delivery						                  int  ,
   PODCodeDesc					                nvarchar(255),
   PODDate                         datetime,
   OutboundSLAHours                int,
   DeliveryMethod                  nvarchar(255),
   ImageCreateDate                 nvarchar(10),
   ImagePlanningComplete           nvarchar(10),
   ImageRelease                    nvarchar(10),
   ImageChecking                   nvarchar(10),
   ImageChecked                    nvarchar(10),
   ImageDespatch                   nvarchar(10),
   ImageDespatchChecked            nvarchar(10),
   ImageComplete                   nvarchar(10)
  )  
    
  create table #TableJobs
  (  
   IssueId    int null,  
   JobId      int null,  
   StatusCode nvarchar(10) null
  )  
    
  if @WarehouseId = -1
     set @WarehouseId = null
  
  if @OperatorId = -1
    set @OperatorId = null
  
  if @OrderNumber is null
    set @OrderNumber = ''
  
  if @InvoiceNumber is null
    set @InvoiceNumber = ''
  
  select @ExternalCompanyId = ExternalCompanyId,
         @PrincipalId       = PrincipalId
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  insert #TableResult  
        (ExternalCompany,  
         ExternalCompanyCode,  
         OrderNumber,  
         OutboundShipmentId,  
         IssueId,  
         CreateDate,  
         DeliveryDate,  
         OutboundDocumentTypeId,  
         Status,  
         TotalQuantity,
         PickTotal,
         PickComplete,
         Delivery,
         PODCodeDesc,
         OutboundSLAHours)
  select ec.ExternalCompany,
         ec.ExternalCompanyCode,
         od.OrderNumber,
         null,
         i.IssueId,
         od.CreateDate,
         case when (datepart ("Hour", i.DeliveryDate) = 0)
             then dateadd ("Hour",17, i.DeliveryDate)
             else i.DeliveryDate
         end as DeliveryDate,
         od.OutboundDocumentTypeId,
         s.Status,
         0,
         i.Total,
         i.Complete,
         i.Delivery,
         null,
         ec.OutboundSLAHours
    from OutboundDocument        od (nolock)
    left
    join Principal                p (nolock) on od.PrincipalId = p.PrincipalId
    join Issue                    i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    join ExternalCompany         ec (nolock) on od.ExternalCompanyId  = ec.ExternalCompanyId
                                            and (ec.ExternalCompanyId = @ExternalCompanyId or @ExternalCompanyId is null) 
    join Status                   s (nolock) on i.StatusId            = s.StatusId
    --left join InterfaceInvoice   ii (nolock) on ii.OrderNumber = od.OrderNumber
    --left join InterfaceImportPOD pod (nolock) on pod.CustWayNo = ii.InvoiceNumber 
    --left join InterfaceImportPOD pod2 (nolock) on pod2.CustXRef = ii.OrderNumber
   where od.WarehouseId = isnull(@WarehouseId,od.WarehouseId)
     and ec.ExternalCompanyId = isnull(@ExternalCompanyId, ec.ExternalCompanyId)
     and od.OrderNumber    like '%' + @OrderNumber + '%'
     and isnull(od.ReferenceNumber,'') like '%' + @InvoiceNumber + '%'
     and od.CreateDate  between @FromDate and @ToDate
     and (p.PrincipalId = @PrincipalId or @PrincipalId is null)
   order by ec.ExternalCompany,  
            od.OrderNumber
  
  update tr
     set DeliveryDate = convert(date, tr.CreateDate)
    from #TableResult            tr
  
  --update tr
  --   set DeliveryDate = dateadd(dd, case when c.DayType = 'Work Day' then 1 else 2 end, tr.DeliveryDate)
  --  from #TableResult            tr
  --  join Calendar c on dateadd(dd, 1, convert(date, tr.DeliveryDate)) = c.DWDateKey
    
    update #TableResult
       set OutboundSLAHours = FLOOR(isnull(OutboundSLAHours, (select max(convert(int, Value)) from Configuration (nolock) where Configurationid = 424 and WarehouseId = @WarehouseId))/24)
     where OutboundSLAHours > 24 or OutboundSLAHours is null
    
    declare @count int = 3
    
    while @count > 0
    begin
      select @count = @count -1
      -- Add one day
      update tr
         set DeliveryDate = dateadd(dd, 1, tr.DeliveryDate)
        from #TableResult            tr
        join Calendar c on dateadd(dd, 1, convert(date, tr.DeliveryDate)) = c.DWDateKey
       where tr.OutboundSLAHours > 0
      
      -- If not work day add another day
      update tr
         set DeliveryDate = dateadd(dd, 1, tr.DeliveryDate)
        from #TableResult            tr
        join Calendar c on convert(date, tr.DeliveryDate) = c.DWDateKey
       where c.DayType != 'Work Day'
         and tr.OutboundSLAHours > 0
      
      -- If not work day add another day
      update tr
         set DeliveryDate = dateadd(dd, 1, tr.DeliveryDate)
        from #TableResult            tr
        join Calendar c on convert(date, tr.DeliveryDate) = c.DWDateKey
       where c.DayType != 'Work Day'
         and tr.OutboundSLAHours > 0
      
      -- If not work day add another day
      update tr
         set DeliveryDate = dateadd(dd, 1, tr.DeliveryDate)
        from #TableResult            tr
        join Calendar c on convert(date, tr.DeliveryDate) = c.DWDateKey
       where c.DayType != 'Work Day'
         and tr.OutboundSLAHours > 0
      
      -- If not work day add another day
      update tr
         set DeliveryDate = dateadd(dd, 1, tr.DeliveryDate)
        from #TableResult            tr
        join Calendar c on convert(date, tr.DeliveryDate) = c.DWDateKey
       where c.DayType != 'Work Day'
         and tr.OutboundSLAHours > 0
      
      -- If not work day add another day
      update tr
         set OutboundSLAHours = OutboundSLAHours - 24
        from #TableResult tr
       where tr.OutboundSLAHours > 0
  end
  
  update tr
     set TotalQuantity = (select sum(il.ConfirmedQuatity)
                            from IssueLine il (nolock)
                           where il.IssueId = tr.IssueId)
    from #TableResult tr
  
  --update tr
  --   set ReceivedFromHost = il.ProcessedDate,
  --       Data             = substring(il.Data, 1, 20)
  --  from #TableResult tr
  --  join InterfaceLog il (nolock) on il.Data like '%SO%' + tr.OrderNumber + '%'
  
  --update tr
  --   set ReceivedFromHost = il.ProcessedDate,
  --       Data             = substring(il.Data, 1, 20)
  --  from #TableResult tr
  --  join InterfaceLog il (nolock) on il.Data like '%DL%' + tr.OrderNumber + '%'
  -- where tr.Data is null
  
  update tr
     set Extracted = h.ProcessedDate
    from #TableResult tr
    join InterfaceExportSOHeader h (nolock) on tr.IssueId = h.IssueId
   where h.RecordStatus = 'Y'
  
  update tr
     set OutboundDocumentType = odt.OutboundDocumentType,
         OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode
    from #TableResult          tr
    join OutboundDocumentType odt on tr.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  
  update #TableResult
     set DeliveryDate = dateadd(hh, 3, CreateDate)
   where OutboundDocumentTypeCode in ('COD','CNC')
  
  insert #TableJobs
        (IssueId,
         JobId)
  select distinct
         ili.IssueId,
         i.JobId
    from #TableResult          tr
    join IssueLineInstruction ili (nolock) on tr.IssueId        = ili.IssueId
    join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId,i.InstructionId)
  
  update tj
     set StatusCode = s.StatusCode
    from #TableJobs tj
    join Job         j on tj.JobId = j.JobId
    join Status      s on j.StatusId = s.StatusId

  update tr
     set PlanningComplete = (select max(PlanningComplete)
                               from OutboundPerformance   op (nolock)
                               join #TableJobs            tj on op.JobId = tj.JobId
                              where tr.IssueId = tj.IssueId)
    from #TableResult tr
  
  update tr
     set Release = (select max(Release)
                       from OutboundPerformance   op (nolock)
                       join #TableJobs            tj on op.JobId = tj.JobId
                      where tr.IssueId = tj.IssueId)
    from #TableResult tr
  
  update tr
     set Checking = (select max(Checking)
                       from OutboundPerformance   op (nolock)
                       join #TableJobs            tj on op.JobId = tj.JobId
                      where tr.IssueId = tj.IssueId)
    from #TableResult tr
  
  update tr  
     set Checked = (select max(Checked)  
                      from OutboundPerformance   op (nolock)  
                      join #TableJobs            tj on op.JobId = tj.JobId  
                     where tr.IssueId = tj.IssueId)  
    from #TableResult tr  
    
  update tr  
     set Despatch = (select max(Despatch)  
                       from OutboundPerformance   op (nolock)  
                       join #TableJobs            tj on op.JobId = tj.JobId  
                      where tr.IssueId = tj.IssueId)  
    from #TableResult tr  
    
  update tr  
     set DespatchChecked = (select max(DespatchChecked)  
                              from OutboundPerformance   op (nolock)  
                              join #TableJobs            tj on op.JobId = tj.JobId  
                             where tr.IssueId = tj.IssueId)  
    from #TableResult tr  
    
  update tr  
     set Complete = (select max(Complete)  
                       from OutboundPerformance   op (nolock)  
                       join #TableJobs            tj on op.JobId = tj.JobId  
                      where tr.IssueId = tj.IssueId)  
    from #TableResult tr  
    
  
  update #TableResult  
     set PickPercentage = (PickComplete / PickTotal) * 100  
   where PickComplete > 0  
     and PickTotal    > 0  
    
  update tr  
     set CheckingTotal = (select count(distinct(tj.JobId))  
                            from #TableJobs tj  
                           where tr.IssueId = tj.IssueId)  
    from #TableResult tr  
    
  update tr  
     set CheckingComplete = (select count(distinct(tj.JobId))  
                               from #TableJobs tj  
                              where tr.IssueId     = tj.IssueId  
                                and tj.StatusCode in ('NS','CD','D','DC','C'))  
    from #TableResult tr  
    
  update #TableResult  
     set CheckingPercentage = (CheckingComplete / CheckingTotal) * 100  
   where CheckingComplete > 0  
     and CheckingTotal    > 0  
    
  update tr  
     set InvoiceNumber = ii.InvoiceNumber  
    from #TableResult     tr  
    join InterfaceInvoice ii on tr.OrderNumber = ii.OrderNumber collate SQL_Latin1_General_CP1_CI_AS
  
  update #TableResult
     set DeliveryDate = DATEADD(ss,86399,DeliveryDate)
   where DATEDIFF(ss, DeliveryDate, convert(nvarchar(10), DeliveryDate, 120)) = 0
  
  update #TableResult
     set ImageCreateDate        = case when DeliveryDate > CreateDate
                                       then 'Green'
                                       when CreateDate <= DeliveryDate
                                       then 'Organe'
                                       when DeliveryDate > GETDATE()
                                       then 'Yellow'
                                       else 'Red'
                                       end,
         ImagePlanningComplete  = case when DeliveryDate > PlanningComplete
                                       then 'Green'
                                       when PlanningComplete <= DeliveryDate
                                       then 'Organe'
                                       when PlanningComplete > GETDATE()
                                       then 'Yellow'
                                       else 'Red'
                                       end,
         ImageRelease           = case when DeliveryDate > Release
                                       then 'Green'
                                       when Release <= DeliveryDate
                                       then 'Organe'
                                       when Release > GETDATE()
                                       then 'Yellow'
                                       else 'Red'
                                       end,
         ImageChecking          = case when DeliveryDate > Checking
                                       then 'Green'
                                       when Checking <= DeliveryDate
                                       then 'Organe'
                                       when Checking > GETDATE()
                                       then 'Yellow'
                                       else 'Red'
                                       end,
         ImageChecked           = case when DeliveryDate > Checked
                                       then 'Green'
                                       when Checked <= DeliveryDate
                                       then 'Organe'
                                       when Checked > GETDATE()
                                       then 'Yellow'
                                       else 'Red'
                                       end,
         ImageDespatch          = case when DeliveryDate > Despatch
                                       then 'Green'
                                       when Despatch <= DeliveryDate
                                       then 'Organe'
                                       when Despatch > GETDATE()
                                       then 'Yellow'
                                       else 'Red'
                                       end,
         ImageDespatchChecked   = case when DeliveryDate > DespatchChecked
                                       then 'Green'
                                       when DespatchChecked <= DeliveryDate
                                       then 'Organe'
                                       when DespatchChecked > GETDATE()
                                       then 'Yellow'
                                       else 'Red'
                                       end,
         ImageComplete   = case when DeliveryDate > Complete
                                       then 'Green'
                                       when Complete <= DeliveryDate
                                       then 'Organe'
                                       when Complete > GETDATE()
                                       then 'Yellow'
                                       else 'Red'
                                       end
  
  if @Status = 'OnTime'
  begin
    delete #TableResult where ImageCreateDate in ('Red','Yellow','Organe')
    delete #TableResult where ImagePlanningComplete in ('Red','Yellow','Organe')
    delete #TableResult where ImageRelease in ('Red','Yellow','Organe')
    delete #TableResult where ImageChecking in ('Red','Yellow','Organe')
    delete #TableResult where ImageChecked in ('Red','Yellow','Organe')
  end
  
  if @Status = 'OverDue'
  begin
    delete #TableResult where ImageChecked not in ('Red','Yellow','Organe')
  end
  
  if  (select dbo.ufn_Configuration(346, @WarehouseId)) = 0    
  begin
    select isnull(OutboundShipmentId,-1) as 'OutboundShipmentId',  
           IssueId,  
           ExternalCompany,  
           ExternalCompanyCode,  
           case when Delivery > 1
                then OrderNumber + '(' + convert(nvarchar(10),isnull(Delivery,1)) + ')'
                else OrderNumber
                end as OrderNumber,
           InvoiceNumber,  
           OutboundDocumentType,
           DeliveryMethod,
           CreateDate,
           DeliveryDate,
           ImageCreateDate,
           PlanningComplete,
           ImagePlanningComplete,
           Release,
           ImageRelease,
           Checking,
           ImageChecking,
           Checked,
           ImageChecked,
           Despatch,
           ImageDespatch,
           DespatchChecked,
           ImageDespatchChecked,
           Complete,  
           ImageComplete,  
           PickPercentage,  
           CheckingPercentage,  
           Status,
           PODCodeDesc as 'PODStatus',
           PODDate,
           TotalQuantity,  
           Extracted,  
           ReceivedFromHost,  
           Data
      from #TableResult  
    order by ExternalCompany,OrderNumber 
  end
  else
  begin
    --update tr
    --   set tr.PODCodeDesc = (select max(pod.PODCodeDesc)
    --                          from InterfaceImportPOD  pod (nolock)
    --                          join #TableResult         tr on tr.InvoiceNumber = pod.CustWayNo collate SQL_Latin1_General_CP1_CI_AS)
    --  from #TableResult tr
    -- where tr.PODCodeDesc is null
    
    update tr
       set PODCodeDesc = pod.PODCodeDesc,
           PODDate = pod.InsertDate
      from InterfaceImportPOD  pod (nolock)
      join #TableResult         tr          on tr.OrderNumber = pod.CustXRef collate SQL_Latin1_General_CP1_CI_AS
     where tr.PODCodeDesc is null
    
    update tr
       set DeliveryMethod = h.DeliveryMethod
      from #TableResult tr
      join InterfaceExportPackHeader h (nolock) on tr.IssueId = h.IssueId
    
    select isnull(OutboundShipmentId,-1) as 'OutboundShipmentId',  
           IssueId,  
           ExternalCompany,  
           ExternalCompanyCode,  
           OrderNumber + '(' + convert(nvarchar(10),isnull(Delivery,1)) + ')' as OrderNumber,  
           InvoiceNumber,  
           OutboundDocumentType,
           DeliveryMethod,
           CreateDate,
           DeliveryDate,
           ImageCreateDate,
           PlanningComplete,
           ImagePlanningComplete,
           Release,
           ImageRelease,
           Checking,
           ImageChecking,
           Checked,
           ImageChecked,
           Despatch,
           ImageDespatch,
           DespatchChecked,
           ImageDespatchChecked,
           Complete,  
           ImageComplete,  
           PickPercentage,  
           CheckingPercentage,  
           Status,
           PODCodeDesc as 'PODStatus',
           PODDate,
           TotalQuantity,  
           Extracted,  
           ReceivedFromHost,  
           Data
      from #TableResult  
    order by ExternalCompany,OrderNumber
  end
end
