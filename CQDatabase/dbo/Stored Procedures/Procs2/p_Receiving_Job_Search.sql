﻿ 
   
/*  
  /// <summary>  
  ///   Procedure Name : p_Receiving_Job_Search  
  ///   Filename       : p_Receiving_Job_Search.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 02 Mar 2009  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Receiving_Job_Search  
(  
 @InboundShipmentId  int,  
 @ReceiptId             int,  
 @InstructionTypeCode nvarchar(10)  
)  
  
as  
begin  
  set nocount on;  
    
  declare @TableHeader as table  
  (  
   DocumentNumber nvarchar(30),  
   OrderNumber    nvarchar(30),  
   ReceiptLineId  int  
  )  
    
  declare @TableResult as table  
  (  
   JobId               int,  
   ReferenceNumber     nvarchar(255),  
   InstructionType     nvarchar(30),  
   PriorityId          int,  
   Priority            nvarchar(50),  
   StatusId            int,  
   Status              nvarchar(50),  
   OperatorId          int,  
   Operator            nvarchar(50),  
   Weight              numeric(13,6)  
  );  
    
  if @InboundShipmentId = -1  
    set @InboundShipmentId = null  
    
  if @ReceiptId = -1  
    set @ReceiptId = null  
    
  if @InboundShipmentId is not null  
    set @ReceiptId = null  
    
  if @ReceiptId is not null  
  begin  
    insert @TableHeader  
          (ReceiptLineId,  
           OrderNumber)  
    select rl.ReceiptLineId,  
           id.OrderNumber  
      from ReceiptLine           rl (nolock)  
      join Receipt                r (nolock) on rl.ReceiptId = r.ReceiptId  
      join InboundDocument       id (nolock) on r.InboundDocumentId = id.InboundDocumentId  
     where rl.ReceiptId = @ReceiptId  
      
    insert @TableResult  
          (InstructionType,  
           JobId,  
           PriorityId,  
           StatusId,  
           OperatorId,  
           ReferenceNumber,  
           Weight)  
    select distinct it.InstructionType,  
           i.JobId,  
           j.PriorityId,  
           j.StatusId,  
           j.OperatorId,  
           case when it.InstructionType = 'P'  
                then 'P:' + convert(nvarchar(10), i.PalletId)  
                else case when j.ReferenceNumber is null  
                          then 'J:' + convert(nvarchar(10), j.JobId)  
                          else j.ReferenceNumber  
                          end  
                end,  
           j.NettWeight  
      from @TableHeader th  
      join Instruction      i (nolock) on th.ReceiptLineId    = i.ReceiptLineId  
      join Job              j (nolock) on i.JobId             = j.JobId  
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId  
     where it.InstructionTypeCode in ('S','SM')  
  end  
    
  update tr
     set ReferenceNumber = isnull(ReferenceNumber,'') +   
                           case when tr.Weight is null  
                                then ' * please weight this pallet'  
                                else ''  
                                end  
    from @TableResult tr
    join Instruction   i on tr.JobId = i.JobId
    join StorageUnitBatch sub on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit  su on sub.StorageUnitId = su.StorageUnitId
   where su.ProductCategory = 'V'
  
  if @InboundShipmentId is not null  
  begin  
    insert @TableHeader  
          (ReceiptLineId,  
           DocumentNumber,  
           OrderNumber)  
    select rl.ReceiptLineId,  
           convert(nvarchar(30), isr.InboundShipmentId),  
           id.OrderNumber  
      from InboundShipmentReceipt isr (nolock)  
      join ReceiptLine             rl (nolock) on isr.ReceiptId       = rl.ReceiptId  
      join Receipt                  r (nolock) on rl.ReceiptId        = r.ReceiptId  
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId  
     where isr.InboundShipmentId = @InboundShipmentId  
      
    insert @TableResult  
          (InstructionType,  
           JobId,  
           PriorityId,  
           StatusId,  
           OperatorId,  
           ReferenceNumber,  
           Weight)  
    select distinct it.InstructionType,  
           i.JobId,  
           j.PriorityId,  
           j.StatusId,  
           j.OperatorId,  
           case when it.InstructionType = 'P'  
                then 'P:' + convert(nvarchar(10), i.PalletId)  
                else case when j.ReferenceNumber is null  
                          then 'J:' + convert(nvarchar(10), j.JobId)  
                          else j.ReferenceNumber  
                          end  
                end,  
           j.NettWeight  
      from @TableHeader th  
--      join IssueLineInstruction ili (nolock) on th.ReceiptLineId    = ili.ReceiptLineId  
      join Instruction            i (nolock) on th.ReceiptLineId    = i.ReceiptLineId  
      join Job                    j (nolock) on i.JobId             = j.JobId  
      join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId  
     where it.InstructionTypeCode in ('S','SM')  
  end  
    
  update tr  
     set Status = s.Status  
    from @TableResult tr  
    join Status        s (nolock) on tr.StatusId = s.StatusId  
    
  update tr  
     set Operator = o.Operator  
    from @TableResult tr  
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId  
    
  update tr  
     set Priority = p.Priority  
    from @TableResult tr  
    join Priority      p (nolock) on tr.PriorityId = p.PriorityId  
    
  select distinct JobId,  
                  ReferenceNumber,  
                  InstructionType,  
                  Priority,  
                  Status,  
                  Operator  
    from @TableResult  
end
 
