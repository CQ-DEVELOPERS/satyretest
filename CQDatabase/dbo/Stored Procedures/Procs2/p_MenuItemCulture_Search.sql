﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItemCulture_Search
  ///   Filename       : p_MenuItemCulture_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:33
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the MenuItemCulture table.
  /// </remarks>
  /// <param>
  ///   @MenuItemCultureId int = null output,
  ///   @MenuItemId int = null,
  ///   @CultureId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   MenuItemCulture.MenuItemCultureId,
  ///   MenuItemCulture.MenuItemId,
  ///   MenuItem.MenuItem,
  ///   MenuItemCulture.CultureId,
  ///   Culture.CultureId,
  ///   MenuItemCulture.MenuItem,
  ///   MenuItemCulture.ToolTip 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItemCulture_Search
(
 @MenuItemCultureId int = null output,
 @MenuItemId int = null,
 @CultureId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @MenuItemCultureId = '-1'
    set @MenuItemCultureId = null;
  
  if @MenuItemId = '-1'
    set @MenuItemId = null;
  
  if @CultureId = '-1'
    set @CultureId = null;
  
 
  select
         MenuItemCulture.MenuItemCultureId
        ,MenuItemCulture.MenuItemId
         ,MenuItemMenuItemId.MenuItem as 'MenuItem'
        ,MenuItemCulture.CultureId
        ,CultureName as 'Culture'
        ,MenuItemCulture.MenuItem
        ,MenuItemCulture.ToolTip
    from MenuItemCulture
    left
    join MenuItem MenuItemMenuItemId on MenuItemMenuItemId.MenuItemId = MenuItemCulture.MenuItemId
    left
    join Culture CultureCultureId on CultureCultureId.CultureId = MenuItemCulture.CultureId
   where isnull(MenuItemCulture.MenuItemCultureId,'0')  = isnull(@MenuItemCultureId, isnull(MenuItemCulture.MenuItemCultureId,'0'))
     and isnull(MenuItemCulture.MenuItemId,'0')  = isnull(@MenuItemId, isnull(MenuItemCulture.MenuItemId,'0'))
     and isnull(MenuItemCulture.CultureId,'0')  = isnull(@CultureId, isnull(MenuItemCulture.CultureId,'0'))
  
end
