﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MovementSequence_Parameter
  ///   Filename       : p_MovementSequence_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the MovementSequence table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   MovementSequence.MovementSequenceId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MovementSequence_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as MovementSequenceId
        ,null as 'MovementSequence'
  union
  select
         MovementSequence.MovementSequenceId
        ,MovementSequence.MovementSequenceId as 'MovementSequence'
    from MovementSequence
  
end
