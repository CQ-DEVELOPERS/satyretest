﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Fix_Pickface
  ///   Filename       : p_Location_Fix_Pickface.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Fix_Pickface
(
 @instructionId int
)

as
begin
	 set nocount on;
	 
	 declare @TableInstructions as table
	 (
	  InstructionId      int,
	  StorageUnitBatchId int,
	  LocationId         int
	 )
	 
	 declare @TableLocations as table
	 (
	  StorageUnitBatchId int,
	  LocationId         int
	 )
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @Rowcount           int,
          @LocationId         int,
          @StorageUnitBatchId int,
          @StorageUnitId      int,
          @UpdInstructionId   int,
          @ActualQuantity     float,
          @AllocatedQuantity  float,
          @ReservedQuantity   float
  
  select @LocationId         = StoreLocationId,
         @StorageUnitBatchId = StorageUnitBatchId
    from Instruction (nolock)
   where InstructionId = @instructionId
  
  select @StorageUnitId = StorageUnitId
    from StorageUnitBatch (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
  
  if (select AreaCode
        from AreaLocation al (nolock)
        join Area          a (nolock) on al.AreaId = a.AreaId
       where al.LocationId = @LocationId) != 'PK'
    return;
  
  insert @TableInstructions
        (InstructionId,
         StorageUnitBatchId,
         LocationId)
  select i.InstructionId,
         i.StorageUnitBatchId,
         @LocationId
    from StorageUnitBatchLocation subl
    join StorageUnitBatch          sub on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Instruction                 i on subl.StorageUnitBatchId = i.StorageUnitBatchId
                                      and subl.LocationId         = i.PickLocationId
    join Status                      s on i.StatusId              = s.StatusId
   where s.StatusCode            in ('W','S')
     and subl.LocationId          = @LocationId
     and subl.StorageUnitBatchId != @StorageUnitBatchId
     and sub.StorageUnitId        = @StorageUnitId
     --and subl.ActualQuantity     <= 0
  
  insert @TableLocations
        (StorageUnitBatchId,
         LocationId)
  select distinct
         StorageUnitBatchId,
         LocationId
    from @TableInstructions
  
  while exists(select top 1 1 from @TableInstructions)
  begin
    select @UpdInstructionId = InstructionId
      from @TableInstructions
    
    delete @TableInstructions
     where InstructionId = @UpdInstructionId
    
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @UpdInstructionId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Instruction_Update
     @InstructionId      = @UpdInstructionId,
     @StorageUnitBatchId = @StorageUnitBatchId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @UpdInstructionId
    
    if @Error <> 0
      goto error
  end
  
  update rep
     set ActualQuantity = (select sum(ActualQuantity)
                             from StorageUnitBatchLocation subl
                             join StorageUnitBatch          sub on subl.StorageUnitBatchId = sub.StorageUnitBatchId
                            where subl.LocationId    = @LocationId
                              and sub.StorageUnitId  = @StorageUnitId)
    from StorageUnitBatchLocation rep
   where StorageUnitBatchId = @StorageUnitBatchId
     and LocationId         = @LocationId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  update subl
     set ActualQuantity = 0
    from StorageUnitBatchLocation subl
    join StorageUnitBatch          sub on subl.StorageUnitBatchId = sub.StorageUnitBatchId
   where subl.LocationId          = @LocationId
     and sub.StorageUnitId        = @StorageUnitId
     and subl.StorageUnitBatchId != @StorageUnitBatchId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete subl
    from StorageUnitBatchLocation subl
    join StorageUnitBatch          sub on subl.StorageUnitBatchId = sub.StorageUnitBatchId
   where sub.StorageUnitId       = @StorageUnitId
     and subl.LocationId         = @LocationId
     and subl.ActualQuantity     = 0
     and subl.ReservedQuantity   = 0
     and subl.AllocatedQuantity  = 0
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
--  select @AllocatedQuantity = sum(AllocatedQuantity),
--         @ReservedQuantity  = sum(ReservedQuantity)
--    from @TableLocations             t
--    join StorageUnitBatchLocation subl on t.LocationId = subl.LocationId
--   where subl.StorageUnitBatchId = @StorageUnitBatchId
--     and subl.LocationId         = @LocationId
--  
--  update subl
--     set AllocatedQuantity = @AllocatedQuantity,
--         ReservedQuantity  = @ReservedQuantity
--    from @TableLocations             t
--    join StorageUnitBatchLocation subl on t.LocationId = subl.LocationId
--   where subl.StorageUnitBatchId = @StorageUnitBatchId
--     and subl.LocationId         = @LocationId
--  
--  select @Error = @@Error
--  
--  if @Error <> 0
--    goto error
--  
--  update subl
--     set AllocatedQuantity = 0,
--         ReservedQuantity  = 0
--    from @TableLocations             t
--    join StorageUnitBatchLocation subl on t.StorageUnitBatchId = subl.StorageUnitBatchId
--                                      and t.LocationId         = subl.LocationId
--  
--  select @Error = @@Error
--  
--  if @Error <> 0
--    goto error
  
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Location_Fix_Pickface'); 
    return @Error
end
