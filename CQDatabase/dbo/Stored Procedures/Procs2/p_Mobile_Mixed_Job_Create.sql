﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Mixed_Job_Create
  ///   Filename       : p_Mobile_Mixed_Job_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   @type       int,
  ///   @operatorId int,
  ///   @palletId   int,
  ///   @barcode    nvarchar(50) = null,
  ///   @batch      nvarchar(50) = null,
  ///   @quantity   float        = null
  /// </param>
  /// <returns>
  ///   Result    = 2 -- Incorrect status
  ///               3 -- Incorrect barcode
  //                4 -- Incorrect batch
  ///               5 -- Incorrect quantity
  ///   DwellTime = 0 -- OK
  ///               1 -- Reason Screen
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Mixed_Job_Create
(
 @type       int,
 @operatorId int,
 @palletId   int,
 @barcode    nvarchar(50) = null,
 @batch      nvarchar(50) = null,
 @quantity   float        = null
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @Rowcount           int,
          @InstructionId      int,
          @StorageUnitBatchId int,
          @ProductId          int,
          @StorageUnitId      int,
          @ActualBatch        nvarchar(50),
          @StatusId           int,
          @ActualQuantity     float,
          @DwellTime          int,
          @ProductBarcode     nvarchar(50),
          @PackBarcode        nvarchar(50),
          @JobId              int
  
  set @Errormsg = 'You may continue.'
  set @Error = 0
  
  set @JobId = @PalletId
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    set @PalletId = replace(@barcode,'P:','')
  
  exec @DwellTime = p_Mobile_Dwell_Time
   @Type       = 2,
   @OperatorId = @OperatorId
  
  begin transaction
  
  select top 1
         @JobId              = JobId,
         @InstructionId      = InstructionId,
         @StorageUnitBatchId = StorageUnitBatchId,
         @StatusId           = StatusId,
         @ActualQuantity     = Quantity
    from Instruction
   where (PalletId = @PalletId
      or  JobId    = @JobId)
  order by InstructionId
  
  select @Rowcount = @@rowcount
  
  if @Rowcount = 0
  begin
    set @Error = 2
    set @Errormsg = 'Incorrect status'
    goto result
  end
  
  --if @Type = 0 -- check barcode (Product or Pack)
  begin
    if @barcode is null
    begin
      set @Error = 3
      set @Errormsg = 'Incorrect barcode'
      goto result
    end
    
    if @barcode like 'P:%'
    begin
      select @InstructionId      = InstructionId,
             @StorageUnitBatchId = StorageUnitBatchId,
             @StatusId           = StatusId,
             @ActualQuantity     = Quantity
        from Instruction (nolock)
       where PalletId = @PalletId
      
      if @InstructionId is not null
      begin
        set @Error = 0
        set @Errormsg = 'Pallet is mixed and in correct status'
      end
      else
      begin
        set @Error = 3
        set @Errormsg = 'Pallet is not mixed or not in correct status'
      end
    end
    else
    begin
      select @InstructionId      = InstructionId,
             @StorageUnitBatchId = i.StorageUnitBatchId,
             @StatusId           = i.StatusId,
             @ActualQuantity     = i.Quantity
        from StorageUnitBatch sub (nolock)
        join Batch              b (nolock) on sub.BatchId            = b.BatchId
        join Pack              pk (nolock) on sub.StorageUnitId      = pk.StorageUnitId
        join StorageUnit       su (nolock) on sub.StorageUnitId      = su.StorageUnitId
        join Product            p (nolock) on su.ProductId           = p.ProductId
        join Instruction        i (nolock) on sub.StorageUnitBatchId = i.StorageUnitBatchId
        join Status             s (nolock) on i.StatusId             = s.StatusId
       where (pk.Barcode  = @barcode
          or  p.Barcode   = @barcode)
         and i.Quantity   = @quantity
         and b.Batch      = @batch
         and s.Type       = 'I'
         and s.StatusCode = 'W'
      
      if @StorageUnitBatchId is null
      begin
        set @Error = 3
        set @Errormsg = 'Incorrect barcode'
        goto result
      end
--      select @StorageUnitId = StorageUnitId
--        from StorageUnitBatch (nolock)
--       where StorageUnitBatchId = @StorageUnitBatchId
--      
--      select @ProductId = ProductId
--        from StorageUnit (nolock)
--       where StorageUnitId = @StorageUnitId
--      
--      select @ProductBarcode = Barcode
--        from Product (nolock)
--       where ProductId = @ProductId
--      
--      if @ProductBarcode != @barcode
--      begin
--        select @PackBarcode = Barcode
--          from Pack
--         where StorageUnitId = @StorageUnitId
--           and Barcode = @barcode
--        
--        if @PackBarcode is null
--        begin
--          set @Error = 3
--          set @Errormsg = 'Incorrect barcode'
--          goto result
--        end
--      end
    end
  end
  --else if @type = 1 -- Check batch
  begin
    select @ActualBatch = b.Batch
      from StorageUnitBatch sub (nolock)
      join Batch              b (nolock) on b.BatchId = sub.BatchId
     where StorageUnitBatchId = @StorageUnitBatchId
    
    if @batch is null
    begin
      set @Error = 4
      set @Errormsg = 'Incorrect batch'
      goto result
    end
    
    if @batch != @ActualBatch
    begin
      set @Error = 4
      set @Errormsg = 'Incorrect batch'
      goto result
    end
  end
  --else if @type = 2 -- Check quantity
  begin
    if @quantity is null
    begin
      set @Error = 5
      set @Errormsg = 'Incorrect quantity'
      goto result
    end
    
    if @quantity != @ActualQuantity
    begin
      set @Error = 5
      set @Errormsg = 'Incorrect quantity'
      goto result
    end
  end
  
  exec @Error = p_Instruction_Update
   @InstructionId = @InstructionId,
   @JobId         = @JobId
  
  if @Error <> 0
    goto error
  
  goto result
  error:
    rollback transaction
  
  result:
    commit transaction
    select @Error             as 'Result',
           @Errormsg          as 'Message',
           @DwellTime         as 'DwellTime',
           @JobId             as 'JobId'
end
