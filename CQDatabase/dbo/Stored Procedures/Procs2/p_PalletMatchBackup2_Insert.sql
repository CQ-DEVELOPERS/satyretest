﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletMatchBackup2_Insert
  ///   Filename       : p_PalletMatchBackup2_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:49
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PalletMatchBackup2 table.
  /// </remarks>
  /// <param>
  ///   @PalletId int = null,
  ///   @InstructionId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @SKUCode nvarchar(60) = null,
  ///   @Batch nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   PalletMatchBackup2.PalletId,
  ///   PalletMatchBackup2.InstructionId,
  ///   PalletMatchBackup2.StorageUnitBatchId,
  ///   PalletMatchBackup2.ProductCode,
  ///   PalletMatchBackup2.SKUCode,
  ///   PalletMatchBackup2.Batch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletMatchBackup2_Insert
(
 @PalletId int = null,
 @InstructionId int = null,
 @StorageUnitBatchId int = null,
 @ProductCode nvarchar(60) = null,
 @SKUCode nvarchar(60) = null,
 @Batch nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert PalletMatchBackup2
        (PalletId,
         InstructionId,
         StorageUnitBatchId,
         ProductCode,
         SKUCode,
         Batch)
  select @PalletId,
         @InstructionId,
         @StorageUnitBatchId,
         @ProductCode,
         @SKUCode,
         @Batch 
  
  select @Error = @@Error
  
  
  return @Error
  
end
