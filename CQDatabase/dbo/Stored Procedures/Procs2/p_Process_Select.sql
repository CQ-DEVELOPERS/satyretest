﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Process_Select
  ///   Filename       : p_Process_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Nov 2013 12:48:39
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Process table.
  /// </remarks>
  /// <param>
  ///   @ProcessId int = null 
  /// </param>
  /// <returns>
  ///   Process.ProcessId,
  ///   Process.ProcessCode,
  ///   Process.Process 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Process_Select
(
 @ProcessId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Process.ProcessId
        ,Process.ProcessCode
        ,Process.Process
    from Process
   where isnull(Process.ProcessId,'0')  = isnull(@ProcessId, isnull(Process.ProcessId,'0'))
  order by ProcessCode
  
end
