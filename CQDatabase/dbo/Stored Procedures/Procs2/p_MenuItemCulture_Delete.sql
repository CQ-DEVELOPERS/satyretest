﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItemCulture_Delete
  ///   Filename       : p_MenuItemCulture_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:33
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the MenuItemCulture table.
  /// </remarks>
  /// <param>
  ///   @MenuItemCultureId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItemCulture_Delete
(
 @MenuItemCultureId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete MenuItemCulture
     where MenuItemCultureId = @MenuItemCultureId
  
  select @Error = @@Error
  
  
  return @Error
  
end
