﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Remove
  ///   Filename       : p_OutboundDocument_Remove.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Remove
(
 @OutboundDocumentId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @IssueId           int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @IssueId = IssueId
    from Issue
   where OutboundDocumentId = @OutboundDocumentId
  
  begin transaction
  
  exec @Error = p_Issue_Delete
   @IssueId = @IssueId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_OutboundDocument_Delete
   @OutboundDocumentId = @OutboundDocumentId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_OutboundDocument_Remove'); 
    rollback transaction
    return @Error
end
