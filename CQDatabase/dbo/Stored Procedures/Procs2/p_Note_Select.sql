﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Note_Select
  ///   Filename       : p_Note_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:47
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Note table.
  /// </remarks>
  /// <param>
  ///   @NoteId int = null 
  /// </param>
  /// <returns>
  ///   Note.NoteId,
  ///   Note.NoteCode,
  ///   Note.Note 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Note_Select
(
 @NoteId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Note.NoteId
        ,Note.NoteCode
        ,Note.Note
    from Note
   where isnull(Note.NoteId,'0')  = isnull(@NoteId, isnull(Note.NoteId,'0'))
  order by NoteCode
  
end
