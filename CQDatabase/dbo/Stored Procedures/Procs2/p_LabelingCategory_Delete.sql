﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LabelingCategory_Delete
  ///   Filename       : p_LabelingCategory_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:56:44
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the LabelingCategory table.
  /// </remarks>
  /// <param>
  ///   @LabelingCategoryId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LabelingCategory_Delete
(
 @LabelingCategoryId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete LabelingCategory
     where LabelingCategoryId = @LabelingCategoryId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
