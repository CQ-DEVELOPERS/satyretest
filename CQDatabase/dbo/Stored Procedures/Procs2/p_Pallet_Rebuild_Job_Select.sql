﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Rebuild_Job_Select
  ///   Filename       : p_Pallet_Rebuild_Job_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Mar 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Rebuild_Job_Select
(
 @barcode nvarchar(30)
)

as
begin
	 set nocount on;
	 
	 declare @JobId              int,
          @PalletId           int
  
  if isnumeric(replace(@barcode,'J:','')) = 1
    select @JobId   = replace(@barcode,'J:',''),
           @barcode = null
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @PalletId is not null
    select @JobId = JobId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where PalletId = @PalletId
      --and it.InstructionTypeCode in ('PR','S','SM')
  
  if @barcode is not null
  begin
    select @JobId = JobId
      from Job (nolock)
     where ReferenceNumber = @barcode
  end
  
  if @JobId is null
  begin
    select @JobId = -1
    goto result
  end
  
  result:
    select @JobId
    return @JobId
end
