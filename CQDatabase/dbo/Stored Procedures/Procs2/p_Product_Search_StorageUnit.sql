﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Search_StorageUnit
  ///   Filename       : p_Product_Search_StorageUnit.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Product_Search_StorageUnit
(
 @storageUnitId int
)

as
begin
	 set nocount on;
  
  select su.StorageUnitId,
         p.ProductId,
         p.ProductCode,
         p.Product,
         sku.SKUId,
         sku.SKUCode,
         sku.SKU,
         p.CuringPeriodDays,
         p.ShelfLifeDays,
         convert(nvarchar(10), dateadd(dd, isnull(p.ShelfLifeDays,0), getdate()), 111) as 'ExpiryDate'
    from StorageUnit su
    join Product p on su.ProductId = p.ProductId
    join SKU sku on su.SKUId = sku.SKUId
   where StorageUnitId = @StorageUnitId
  union
  select -1,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         null,
         getdate() as 'ExpiryDate'
	Order By StorageUnitId Desc         
end
