﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Log_Batch_Receive
  ///   Filename       : p_Log_Batch_Receive.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Jan 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Log_Batch_Receive
(
 @warehouseId int,
 @batch       nvarchar(50),
 @operatorId  int
)

as
begin
  set nocount on;
  
  declare @GetDate      datetime,
          @Bit          bit,
          @Exception    nvarchar(255),
          @Error        int,
		  @BatchId		int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if exists(select top 1 1 from Batch where Batch = @batch)
    set @bit = 1
  else
    set @bit = 0
  
    select @Exception = 'Batch ' + isnull(@Batch, '') + ' sample started.'
    
    Select top 1 @BatchId=BatchId from Batch where Batch = @Batch
    
    exec @Error = p_Exception_Insert
     @ExceptionId   = null,
     @ExceptionCode = 'BATSAMRECV',
     @Detail		= @BatchId,
     @Exception     = @Exception,
     @CreateDate    = @GetDate,
     @ExceptionDate = @Getdate
  
  select @bit
  return @bit
end
