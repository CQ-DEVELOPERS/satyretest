﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Location_Get
  ///   Filename       : p_Receiving_Location_Get.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Jan 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Location_Get
(
 @WarehouseId int,
 @InboundDocumentTypeCode nvarchar(10),
 @PrincipalId int = null
)

as
begin
	 set nocount on;
	 
	 declare @LocationId int
  
  if @InboundDocumentTypeCode = 'RET' or @InboundDocumentTypeCode like 'CR%'
    select @LocationId = min(al.LocationId)
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId = al.AreaId
      left
      join AreaPrincipal ap (nolock) on a.AreaId = ap.AreaId
                                    and isnull(ap.PrincipalId, -1) = isnull(@PrincipalId, isnull(ap.PrincipalId, -1))
     where a.AreaCode    = 'J'
       and a.WarehouseId = @WarehouseId
  
  if @InboundDocumentTypeCode = 'BOND'
    select @LocationId = min(al.LocationId)
      from Area           a (nolock)
      join AreaLocation  al (nolock) on a.AreaId = al.AreaId
      left
      join AreaPrincipal ap (nolock) on a.AreaId = ap.AreaId
                                    and isnull(ap.PrincipalId, -1) = isnull(@PrincipalId, isnull(ap.PrincipalId, -1))
     where a.AreaCode    = 'R'
       and a.AreaType    = 'BOND'
       and a.WarehouseId = @WarehouseId
  
  if @InboundDocumentTypeCode = 'MO'
    select @LocationId = min(al.LocationId)
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId = al.AreaId
      left
      join AreaPrincipal ap (nolock) on a.AreaId = ap.AreaId
                                    and isnull(ap.PrincipalId, -1) = isnull(@PrincipalId, isnull(ap.PrincipalId, -1))
     where (a.AreaCode   = 'R'
       or (a.AreaCode = 'SP' and a.Area like '%Rec%'))
       and a.WarehouseId = @WarehouseId
       and a.AreaType    = 'Finished'
  
  if @LocationId is null
    select @LocationId = min(al.LocationId)
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId = al.AreaId
      left
      join AreaPrincipal ap (nolock) on a.AreaId = ap.AreaId
                                    and isnull(ap.PrincipalId, -1) = isnull(@PrincipalId, isnull(ap.PrincipalId, -1))
     where a.AreaCode    = 'R'
       and a.WarehouseId = @WarehouseId
  
  if @LocationId is null
    set @LocationId = -1
  
  return @LocationId
end
