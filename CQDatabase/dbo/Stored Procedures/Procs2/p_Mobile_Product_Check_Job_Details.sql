﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Job_Details
  ///   Filename       : p_Mobile_Product_Check_Job_Details.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Dec 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Job_Details
(
 @WarehouseId     int = null,
 @OperatorId      int = null,
 @ReferenceNumber nvarchar(30),
 @newJobId        int = null
)

as
begin
  set nocount on;
  
   declare @TableResult as table
  (
   WarehouseId                     int,
   JobId                           int,
   InstructionId                   int,
   InstructionRefId                int,
   StorageUnitId                   int,
   ProductCode                     nvarchar(60),
   Product                         nvarchar(510),
   SKUCode                         nvarchar(100),
   SKU                             nvarchar(100),
   Batch                           nvarchar(100),
   PickedQuantity                  numeric(13,6),
   CheckedQuantity                 numeric(13,6),
   Variance                        int,
   PackQuantity                    int,
   JobQuantity                     int,
   BoxQuantity                     numeric(13,6),
   Boxes                           numeric(13),
   IssueLineId					   int,
   OrderNumber					   nvarchar(50),
   OutboundShipmentId			   int
  )
  
  declare @TableJobs as table
  (
   JobId                           int,
   InstructionId                   int,
   InstructionRefId                int,
   PickedQuantity                  numeric(13,6),
   CheckedQuantity                 numeric(13,6)
  )
  
  declare @TableIssueLineInstruction as table
  (
   IssueLineId					   int,
   InstructionId                   int,
   ConfirmedQuantity			   int,
   StorageUnitBatchId			   int
  )
  
  declare @JobId				int,
		  @IssueLineId			int,
		  @InstructionId		int = null,
		  @ConfirmedQuantity	int,
		  @StorageUnitBatchId	int,
		  @NewInstructionsCreated bit = 0
  
  if isnumeric(replace(@ReferenceNumber,'J:','')) = 1
  begin
    select @JobId   = replace(@ReferenceNumber,'J:','')
  end
  
  insert @TableResult
        (WarehouseId,
         JobId,
         InstructionId,
         InstructionRefId,
         StorageUnitId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         PickedQuantity,
         CheckedQuantity,
         Variance,
         PackQuantity,
         JobQuantity,
         OutboundShipmentId)
  select i.WarehouseId,
         i.JobId,
         i.InstructionId,
         i.InstructionRefId,
         su.StorageUnitId,
         p.ProductCode,
         isnull(p.Description2,p.product),
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         i.ConfirmedQuantity as 'PickedQuantity',
         i.CheckQuantity     as 'CheckedQuantity',
         null as 'Variance',
         null as 'PackQuantity',
         null as 'JobQuantity',
         i.OutboundShipmentId
    from Instruction			i (nolock)
    join Job					j (nolock) on i.JobId               = j.JobId
    join Status					s (nolock) on j.StatusId            = s.StatusId
    join StorageUnitBatch	  sub (nolock) on i.StorageUnitBatchId  = sub.StorageUnitBatchId
    join StorageUnit		   su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product				p (nolock) on su.ProductId          = p.ProductId
    join SKU				  sku (nolock) on su.SKUId              = sku.SKUId
    join Batch					b (nolock) on sub.BatchId           = b.BatchId
   where (j.ReferenceNumber = @ReferenceNumber or j.JobId = @JobId)
     and s.StatusCode in ('QA','CK','CD')
  
  
  
  if (select dbo.ufn_Configuration(498, @WarehouseId)) = 1 --Box Checking - Insert instruction for original order lines
  begin
	if (select SUM(ISNULL(OutboundShipmentId,0)) from @TableResult) >0
	begin
	  insert @TableIssueLineInstruction
	  (
	   IssueLineId,
	   InstructionId,
	   ConfirmedQuantity,
	   StorageUnitBatchId
	  )
	  select ili.IssueLineId, tr.InstructionId, ili.ConfirmedQuantity, il.StorageUnitBatchId
		from @TableResult tr
		join IssueLineInstruction ili (nolock) on tr.InstructionId = ili.InstructionId
		join IssueLine			   il (nolock) on ili.IssueLineId  = il.IssueLineId
	  
	  while exists (select top 1 1 from @TableIssueLineInstruction)
	  begin
	  
	   select top 1 @IssueLineId = tii.IssueLineId,
			  @ConfirmedQuantity = tii.ConfirmedQuantity,
			  @StorageUnitBatchId= tii.StorageUnitBatchId
		 from @TableIssueLineInstruction tii
	   
	   delete @TableIssueLineInstruction
		where IssueLineId = @IssueLineId
	   
	   if not exists (select top 1 1 from Instruction where IssueLineId = @IssueLineId)
	   begin
	   insert   Instruction
				(InstructionTypeId,
				 StorageUnitBatchId,
				 WarehouseId,
				 StatusId,
				 JobId,
				 OperatorId,
				 PickLocationId,
				 StoreLocationId,
				 ReceiptLineId,
				 IssueLineId,
				 Quantity,
				 ConfirmedQuantity,
				 Weight,
				 ConfirmedWeight,
				 PalletId,
				 CreateDate,
				 StartDate,
				 EndDate,
				 Picked,
				 Stored,
				 CheckQuantity,
				 CheckWeight,
				 OutboundShipmentId,
				 DropSequence)
		  select top 1 InstructionTypeId,
				 @StorageUnitBatchId,
				 WarehouseId,
				 StatusId,
				 @JobId,
				 OperatorId,
				 PickLocationId,
				 StoreLocationId,
				 ReceiptLineId,
				 @IssueLineId,
				 @ConfirmedQuantity ,
				 @ConfirmedQuantity ,
				 null,
				 null,
				 PalletId,
				 CreateDate,
				 StartDate,
				 EndDate,
				 Picked,
				 Stored,
				 0,
				 0,
				 OutboundShipmentId,
				 DropSequence
			from Instruction
		   where JobId = @JobId
	       
		  select @InstructionId = scope_identity()
		  set @NewInstructionsCreated = 1
		  if (@InstructionId is not null)
		  begin
	      
			update IssueLineInstruction
			   set InstructionId = @InstructionId
			 where IssueLineId = @IssueLineId
	      
		  end
		 end
	  end
	  
	  if (@NewInstructionsCreated = 1)
	  begin
	  select top 1 @InstructionId = InstructionId
		from @TableResult
	  
	  delete Exception
	   where InstructionId in (select InstructionId from @TableResult)
	    
	  delete Instruction
	   where InstructionId in (select InstructionId from @TableResult)
		  or InstructionRefId in (select InstructionId from @TableResult)
	      
	  end
	  
	  delete @TableResult
	  
	  insert @TableResult
			(WarehouseId,
			 JobId,
			 InstructionId,
			 InstructionRefId,
			 StorageUnitId,
			 ProductCode,
			 Product,
			 SKUCode,
			 SKU,
			 Batch,
			 PickedQuantity,
			 CheckedQuantity,
			 Variance,
			 PackQuantity,
			 JobQuantity)
	  select i.WarehouseId,
			 i.JobId,
			 i.InstructionId,
			 i.InstructionRefId,
			 su.StorageUnitId,
			 p.ProductCode,
			 isnull(p.Description2,p.Product),
			 sku.SKUCode,
			 sku.SKU,
			 b.Batch,
			 i.ConfirmedQuantity as 'PickedQuantity',
			 i.CheckQuantity     as 'CheckedQuantity',
			 null as 'Variance',
			 null as 'PackQuantity',
			 null as 'JobQuantity'
		from Instruction			i (nolock)
		join Job					j (nolock) on i.JobId               = j.JobId
		join Status					s (nolock) on j.StatusId            = s.StatusId
		join StorageUnitBatch	  sub (nolock) on i.StorageUnitBatchId  = sub.StorageUnitBatchId
		join StorageUnit		   su (nolock) on sub.StorageUnitId     = su.StorageUnitId
		join Product				p (nolock) on su.ProductId          = p.ProductId
		join SKU				  sku (nolock) on su.SKUId              = sku.SKUId
		join Batch					b (nolock) on sub.BatchId           = b.BatchId
	   where (j.ReferenceNumber = @ReferenceNumber or j.JobId = @JobId)
		 and s.StatusCode in ('QA','CK','CD')
		 and i.ConfirmedQuantity >0
	end
  end
  --insert @TableJobs
  --      (InstructionId,
  --       PickedQuantity,
  --       CheckedQuantity)
  --select isnull(i.InstructionRefId, i.InstructionId),
  --       sum(i.ConfirmedQuantity),
  --       sum(i.CheckQuantity)
  --  from @TableResult tr
  --  join Instruction   i (nolock) on (tr.InstructionId = i.InstructionId or tr.InstructionId = i.InstructionRefId)
  --group by isnull(i.InstructionRefId, i.InstructionId)
  
  insert @TableJobs
        (JobId,
         InstructionId,
         InstructionRefId,
         PickedQuantity,
         CheckedQuantity)
  select i.JobId,
         i.InstructionId,
         i.InstructionRefId,
         sum(i.ConfirmedQuantity),
         sum(i.CheckQuantity)
    from @TableResult tr
    join Instruction   i (nolock) on ((tr.InstructionId = i.InstructionRefId and i.JobId != tr.JobId))
  group by i.JobId,
           i.InstructionId,
           i.InstructionRefId
  
  update tr
     set PickedQuantity  = isnull(tr.PickedQuantity,0) + (select isnull(sum(tj.PickedQuantity), 0)
                                                  from @TableJobs   tj 
                                                 where tr.InstructionId = tj.InstructionRefId)
    from @TableResult tr
  
  update tr
     set CheckedQuantity  = isnull(tr.CheckedQuantity,0) + (select isnull(sum(tj.CheckedQuantity), 0)
                                                  from @TableJobs   tj 
                                                 where tr.InstructionId = tj.InstructionRefId)
    from @TableResult tr
  
  --update tr
  --   set PickedQuantity  = tr.PickedQuantity + tj.PickedQuantity,
  --       CheckedQuantity = tr.CheckedQuantity + isnull(tj.CheckedQuantity,0)
  --  from @TableResult tr
  --  join @TableJobs   tj on ((tr.InstructionId = tj.InstructionRefId))
  
  update tr
     set JobQuantity = (select sum(i.CheckQuantity)
                          from Instruction i (nolock) 
                         where tr.InstructionId = isnull(i.InstructionRefId, tr.InstructionId)
                           and i.JobId = @newJobId)
   --where i.JobId = @newJobId
    from @TableResult      tr
  
  update @TableResult
     set Variance = PickedQuantity - isnull(CheckedQuantity, 0)
  
  update tr
     set BoxQuantity = p.Quantity
    from @TableResult   tr
    join Pack            p (nolock) on tr.StorageUnitId = p.StorageUnitId
                                   and tr.WarehouseId   = p.WarehouseId
    join PackType       pt (nolock) on p.PackTypeId     = pt.PackTypeId
   where pt.OutboundSequence   between 2 and (select max(OutboundSequence) - 1 from PackType (nolock))
  
  update @TableResult
     set Boxes = floor(PickedQuantity / BoxQuantity)
     
  update tr 
     set tr.OrderNumber = od.OrderNumber,
		 tr.IssueLineId = ili.IssueLineId
    from @TableResult tr
    join IssueLineInstruction ili (nolock) on tr.InstructionId = ili.InstructionId or tr.InstructionRefId = ili.InstructionId
    join OutboundDocument	   od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
  
  select JobId,
         InstructionId,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Batch,
         PickedQuantity,
         CheckedQuantity,
         BoxQuantity,
         Boxes,
         Variance,
         PackQuantity,
         JobQuantity,
         IssueLineId,
         OrderNumber
    from @TableResult
    order by OrderNumber,
			 ProductCode
end
