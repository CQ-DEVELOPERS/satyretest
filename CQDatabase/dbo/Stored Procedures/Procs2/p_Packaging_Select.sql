﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Packaging_Select
  ///   Filename       : p_Packaging_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Packaging_Select
(
 @JobId         int = null,
 @Null          bit = 0,
 @StorageUnitId int = null
)

as
begin
	 set nocount on;
	 
	 declare @TablePacks as table
	 (
	  JobId        int,
	  PackType     nvarchar(30),
	  PackQuantity numeric(13,6),
	  JobQuantity  numeric(13,6)
	 )
	 
	 declare @TableResult as table
	 (
	  StorageUnitBatchId int,
	  Packaging          nvarchar(255),
	  PackQuantity       numeric(13,6),
	  JobQuantity        numeric(13,6),
	  Quantity           numeric(13,6)
	 )
	 
	 if @StorageUnitId is null
	 begin
	   insert @TablePacks
	         (JobId,
	          PackType,
	          PackQuantity,
	          JobQuantity)
	   select i.JobId,
           pt.PackType,
           pk.Quantity,
           sum(i.ConfirmedQuantity)
      from Instruction        i (nolock)
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join SKU              sku (nolock) on su.SKUId             = sku.SKUId
      join Pack              pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
                                        and i.WarehouseId        = pk.WarehouseId
      join PackType          pt (nolock) on pk.PackTypeId        = pt.PackTypeId
	    where i.JobId = @JobId
	      and pk.TareWeight is not null
	   group by i.JobId,
             pt.PackType,
             pk.Quantity
	 end
	 else
	 begin
	   insert @TablePacks
	         (JobId,
	          PackType,
	          PackQuantity,
	          JobQuantity)
	   select i.JobId,
           pt.PackType,
           pk.Quantity,
           sum(i.ConfirmedQuantity)
      from Instruction        i (nolock)
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join SKU              sku (nolock) on su.SKUId             = sku.SKUId
      join Pack              pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
                                        and i.WarehouseId        = pk.WarehouseId
      join PackType          pt (nolock) on pk.PackTypeId        = pt.PackTypeId
	    where i.JobId = @JobId
	      and su.StorageUnitId = @StorageUnitId
	   group by i.JobId,
             pt.PackType,
             pk.Quantity
	 end
  
  insert @TableResult
        (StorageUnitBatchId,
         Packaging,
         PackQuantity,
         JobQuantity,
         Quantity)
  select distinct sub.StorageUnitBatchId,
         p.ProductCode + ' ' + sku.SKUCode as 'Packaging',
         tp.PackQuantity,
         tp.JobQuantity,
         round(convert(numeric(13,6), tp.JobQuantity / tp.PackQuantity),0) as 'Quantity'
    from StorageUnitBatch sub (nolock) 
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
    join Status             s (nolock) on b.StatusId            = s.StatusId
    join @TablePacks       tp          on p.ProductCode         = tp.PackType
   where b.Batch       = 'Default'
     and p.ProductType = 'PACKAGING'
     and tp.JobId      = @JobId
  
  if @Null = 1-- and @@rowcount = 0
  begin
    insert @TableResult
          (StorageUnitBatchId,
           Packaging,
           PackQuantity,
           JobQuantity,
           Quantity)
    select -1,
           'None' as 'Packaging',
           0,
           0,
           0
  end
  
  select StorageUnitBatchId,
         Packaging,
         PackQuantity,
         JobQuantity,
         Quantity
    from @TableResult
end
