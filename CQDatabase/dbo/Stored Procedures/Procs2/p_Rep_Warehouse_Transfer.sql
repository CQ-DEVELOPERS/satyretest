﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Rep_Warehouse_Transfer
  ///   Filename       : p_Rep_Warehouse_Transfer.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Mar 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Rep_Warehouse_Transfer
(
 @ReceiptId int
)

as
begin
	 set nocount on;
  return
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @SampleQuatity      numeric(13,6),
          @WarehouseId        int,
          @FromWarehouseCode  nvarchar(30),
          @ToWarehouseCode    nvarchar(30),
          @RecordType         nvarchar(3),
          @AssaySampleWarehouse nvarchar(30)
  
  set @Errormsg = 'Error executing p_Rep_Warehouse_Transfer'
  
  select @WarehouseId = r.WarehouseId,
         @FromWarehouseCode = id.ToLocation,
         @ToWarehouseCode   = a.WarehouseCode
    from Receipt r (nolock)
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    join AreaLocation    al (nolock) on r.LocationId = al.LocationId
    join Area             a (nolock) on al.AreaId = a.AreaId
   where r.ReceiptId = @ReceiptId
  
  begin transaction
  
  insert InterfaceExportStockAdjustment
        (RecordType,
         RecordStatus,
         ProductCode,
         SKUCode,
         Batch,
         Quantity,
         Additional1,
         Additional2,
         Additional3,
         Additional4)
  select @RecordType,
         'N',
         p.ProductCode,
         sku.SKUCode,
         b.Batch,
         rl.AcceptedQuantity,
         @FromWarehouseCode,
         @ToWarehouseCode,
         'Rep Sample Qty Received',
         'ReceiptLineId = ' + convert(nvarchar(10), rl.ReceiptLineId)
    from Receipt            r (nolock)
  	 join ReceiptLine       rl (nolock) on r.ReceiptId           = rl.ReceiptId
  	 join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
  	 join Batch              b (nolock) on sub.BatchId           = b.BatchId
  	 join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
  	 join Product            p (nolock) on su.ProductId          = p.ProductId
  	 join SKU              sku (nolock) on su.SKUId              = sku.SKUId
   where r.ReceiptId         = @ReceiptId
	    and rl.AcceptedQuantity > 0
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
