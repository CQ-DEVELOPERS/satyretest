﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Release_By_Operator
  ///   Filename       : p_Outbound_Release_By_Operator.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Dec 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Release_By_Operator
(
 @OutboundShipmentId int,
 @IssueId            int,
 @OperatorId         int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @OperatorGroupId   int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @OperatorId = -1
    set @OperatorId = null
  
  select @OperatorGroupId = OperatorGroupId
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  begin transaction
  
  --if @OperatorId is null
  --  goto Error
  
  -- Swapped for Moresport (they wanted to split shipment by operators)
  if @IssueId is not null
  begin
    exec @Error = p_Outbound_Release
     @IssueId            = @IssueId,
     @StatusCode         = 'RL'
    
    if @Error <> 0
      goto error
    
    update j
       set OperatorId = @OperatorId
      from Job                    j (nolock)
      join Status                 s (nolock) on j.StatusId = s.StatusId
      join Instruction            i (nolock) on j.JobId = i.JobId
      join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
      left join AreaLocation          al (nolock) on i.PickLocationId = al.LocationId
      left join AreaOperatorGroup    ago (nolock) on al.AreaId = ago.AreaId
                                            and ago.OperatorGroupId = @OperatorGroupId
     where ili.IssueId = @IssueId
       and it.InstructionTypeCode != 'P'
       and s.StatusCode in ('RL','PS')
    select @@ROWCOUNT
    if @Error <> 0
      goto error
  end
  else if @OutboundShipmentId is not null
  begin
    exec @Error = p_Outbound_Release
     @OutboundShipmentId = @OutboundShipmentId,
     @StatusCode         = 'RL'
    
    if @Error <> 0
      goto error
    
    update j
       set OperatorId = @OperatorId
      from Job                    j (nolock)
      join Status                 s (nolock) on j.StatusId = s.StatusId
      join Instruction            i (nolock) on j.JobId = i.JobId
      join InstructionType       it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
      left join AreaLocation          al (nolock) on i.PickLocationId = al.LocationId
      left join AreaOperatorGroup    ago (nolock) on al.AreaId = ago.AreaId
                                            and ago.OperatorGroupId = @OperatorGroupId
     where ili.OutboundShipmentId = @OutboundShipmentId
       and it.InstructionTypeCode != 'P'
       and s.StatusCode in ('RL','PS')
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Outbound_Release_By_Operator'); 
    rollback transaction
    return @Error
end
