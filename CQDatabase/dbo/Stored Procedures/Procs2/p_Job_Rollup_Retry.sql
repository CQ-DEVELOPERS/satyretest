﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Job_Rollup_Retry
  ///   Filename       : p_Job_Rollup_Retry.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Aug 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Job_Rollup_Retry

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  declare @TableResult as table
  (
   OutboundShipmentId int,
   IssueId            int,
   JobId              int,
   JobCode            nvarchar(10)
  )
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         JobId, 
         JobCode)
  select distinct OutboundShipmentId, IssueId, JobId, JobCode
    from viewOutboundStatus vos1
   where IssueCode = 'RL'
     and IssueLineCode = 'RL'
     and IssueId > 415335
     and not exists(select 1 from viewOutboundStatus vos2 where vos1.IssueId = vos2.IssueId and vos2.JobCode not in ('CD','D','DC','C','NS'))
  order by OutboundShipmentId, IssueId
  
  declare	@JobId   int,
          @JobCode nvarchar(10)

  declare status_cursor cursor for
   select	distinct JobId,
                   JobCode
  	  from	@TableResult
	  order by	JobId
  
  open status_cursor
  
  fetch status_cursor into @JobId,
                           @JobCode
  
  while (@@fetch_status = 0)
  begin
    if @JobCode = 'C'
      update Job
         set StatusId = dbo.ufn_StatusId('IS','DC')
       where JobId = @JobId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Status_Rollup @JobId
    
    if @Error <> 0
      goto error
    
    insert StatusRollupLog
          (JobId,
           JobCode,
           CreateDate)
    select @JobId,
           @JobCode,
           getdate()
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    fetch status_cursor into @JobId,
                             @JobCode
  end
  
  close status_cursor
  deallocate status_cursor
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Job_Rollup_Retry'); 
    rollback transaction
    return @Error
end
 
