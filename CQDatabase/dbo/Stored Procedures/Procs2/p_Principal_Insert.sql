﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Principal_Insert
  ///   Filename       : p_Principal_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 08:15:16
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Principal table.
  /// </remarks>
  /// <param>
  ///   @PrincipalId int = null output,
  ///   @PrincipalCode nvarchar(60) = null,
  ///   @Principal nvarchar(510) = null,
  ///   @Email nvarchar(100) = null,
  ///   @FirstName nvarchar(100) = null,
  ///   @LastName nvarchar(100) = null,
  ///   @Address1 nvarchar(2000) = null,
  ///   @Address2 nvarchar(2000) = null,
  ///   @Address3 nvarchar(2000) = null,
  ///   @Address4 nvarchar(2000) = null,
  ///   @Address5 nvarchar(2000) = null,
  ///   @Address6 nvarchar(2000) = null 
  /// </param>
  /// <returns>
  ///   Principal.PrincipalId,
  ///   Principal.PrincipalCode,
  ///   Principal.Principal,
  ///   Principal.Email,
  ///   Principal.FirstName,
  ///   Principal.LastName,
  ///   Principal.Address1,
  ///   Principal.Address2,
  ///   Principal.Address3,
  ///   Principal.Address4,
  ///   Principal.Address5,
  ///   Principal.Address6 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Principal_Insert
(
 @PrincipalId int = null output,
 @PrincipalCode nvarchar(60) = null,
 @Principal nvarchar(510) = null,
 @Email nvarchar(100) = null,
 @FirstName nvarchar(100) = null,
 @LastName nvarchar(100) = null,
 @Address1 nvarchar(2000) = null,
 @Address2 nvarchar(2000) = null,
 @Address3 nvarchar(2000) = null,
 @Address4 nvarchar(2000) = null,
 @Address5 nvarchar(2000) = null,
 @Address6 nvarchar(2000) = null 
)

as
begin
	 set nocount on;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @PrincipalCode = '-1'
    set @PrincipalCode = null;
  
  if @Principal = '-1'
    set @Principal = null;
  
	 declare @Error int
 
  insert Principal
        (PrincipalCode,
         Principal,
         Email,
         FirstName,
         LastName,
         Address1,
         Address2,
         Address3,
         Address4,
         Address5,
         Address6)
  select @PrincipalCode,
         @Principal,
         @Email,
         @FirstName,
         @LastName,
         @Address1,
         @Address2,
         @Address3,
         @Address4,
         @Address5,
         @Address6 
  
  select @Error = @@Error, @PrincipalId = scope_identity()
  
  
  return @Error
  
end
