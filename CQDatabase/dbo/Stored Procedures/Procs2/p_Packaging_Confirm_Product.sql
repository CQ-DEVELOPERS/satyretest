﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Packaging_Confirm_Product
  ///   Filename       : p_Packaging_Confirm_Product.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Packaging_Confirm_Product
(
 @warehouseId int,
 @barcode     nvarchar(50)
)

as
begin
	 set nocount on;
  
  declare @StorageUnitBatchId int
  
  select @StorageUnitBatchId = sub.StorageUnitBatchId
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Pack              pk (nolock) on su.StorageUnitId = pk.StorageUnitId
    join Product            p (nolock) on su.ProductId     = p.ProductId
    join Batch              b (nolock) on sub.BatchId      = b.BatchId
   where pk.WarehouseId = @WarehouseId
     and (p.ProductCode = @Barcode
      or  p.Barcode     = @Barcode
      or  pk.Barcode    = @Barcode)
     and b.Batch = 'Default'
  
  select @StorageUnitBatchId
  return @StorageUnitBatchId
end
