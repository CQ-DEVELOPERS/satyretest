﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Search_ProductId
  ///   Filename       : p_Product_Search_ProductId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007 11:54:26
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Product table.
  /// </remarks>
  /// <param>
  ///   @ProductId int = null output,
  ///   @StatusId int = null,
  ///   @ProductCode nvarchar(30) = null,
  ///   @Product nvarchar(50) = null,
  ///   @Barcode nvarchar(50) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ProductId,
  ///   ProductCode,
  ///   Product,
  ///   Barcode
  /// </returns>
  /// <newpara>
  ///   Modified by    : Junaid Desai	
  ///   Modified Date  : 12 February 2009
  ///   Details        : added Barcode and ShelfLifedays In Select Statement
  /// </newpara>
*/
CREATE procedure p_Product_Search_ProductId
(
 @ProductCode nvarchar(30) = null,
 @Product     nvarchar(50) = null
)

as
begin
	 set nocount on;
  
  if @ProductCode = '-1'
    set @ProductCode = null;
  
  if @Product = '-1'
    set @Product = null;
  
  select	ProductId,
			ProductCode,
			Product,
			Barcode,
			ShelfLifeDays
    from Product
   where isnull(ProductCode,'%')  like '%' + isnull(@ProductCode, isnull(ProductCode,'%')) + '%'
     and isnull(Product,'%')  like '%' + isnull(@Product, isnull(Product,'%')) + '%'
end
