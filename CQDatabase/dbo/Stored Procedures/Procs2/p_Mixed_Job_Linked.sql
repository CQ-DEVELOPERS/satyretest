﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mixed_Job_Linked
  ///   Filename       : p_Mixed_Job_Linked.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mixed_Job_Linked
(
 @JobId int
)

as
begin
  declare @TableResult as table
  (
   InstructionId      int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   ProductId          int,
   SKUId              int,
   OrderNumber        nvarchar(30),
   ProductCode        nvarchar(30),
   SKUCode            nvarchar(50),
   Quantity           float,
   Weight             numeric(13,3)
  )
	 set nocount on;
  
  insert @TableResult
        (InstructionId,
         StorageUnitBatchId,
         Quantity)
  select InstructionId,
         StorageUnitBatchId,
         Quantity
    from Instruction (nolock)
   where JobId = @JobId
  
  update tr
     set StorageUnitId = sub.StorageUnitId,
         ProductId     = su.ProductId,
         SKUId         = su.SKUId
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
  
  update tr
     set ProductCode = p.ProductCode,
         SKUCode     = sku.SKUCode
    from @TableResult      tr
    join Product            p (nolock) on tr.ProductId          = p.ProductId
    join SKU              sku (nolock) on tr.SKUId              = sku.SKUId
  
  update tr
     set Weight = tr.Quantity * p.Weight
    from @TableResult      tr
    join Pack               p (nolock) on tr.StorageUnitId = p.StorageUnitId
   where p.Quantity = 1
  
  update tr
     set OrderNumber = od.OrderNumber
    from @TableResult tr
    join IssueLineInstruction ili on tr.InstructionId = ili.InstructionId
    join IssueLine             il on ili.IssueLineId  = il.IssueLineId
    join Issue                  i on il.IssueId       = i.IssueId
    join OutboundDocument      od on i.OutboundDocumentId = od.OutboundDocumentId
  
  select InstructionId,
         StorageUnitBatchId,
         OrderNumber,
         ProductCode,
         SKUCode,
         Quantity,
         Weight
    from @TableResult
  order by SKUCode,
           Quantity
end
