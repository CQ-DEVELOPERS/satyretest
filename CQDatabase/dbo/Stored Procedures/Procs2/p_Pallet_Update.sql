﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Update
  ///   Filename       : p_Pallet_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Update
(
 @PalletId int = null,
 @StorageUnitBatchId int = null,
 @LocationId int = null,
 @Weight float = null,
 @Tare float = null,
 @Quantity float = null,
 @Prints int = null,
 @CreateDate datetime = null,
 @Nett float = null,
 @ReasonId int = null,
 @EmptyWeight float = null,
 @StatusId	int = null
)

as
begin
	 set nocount on;
  
  if @PalletId = '-1'
    set @PalletId = null;
  
  declare @Error int,
		  @PalletStatusId int = null
  
  update Pallet
     set StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         LocationId = isnull(@LocationId, LocationId),
         Weight = isnull(@Weight, Weight),
         Tare = isnull(@Tare, Tare),
         Quantity = isnull(@Quantity, Quantity),
         Prints = isnull(@Prints, Prints),
         CreateDate = isnull(@CreateDate, CreateDate),
         Nett = isnull(@Nett, Nett),
         ReasonId = isnull(@ReasonId, ReasonId),
         EmptyWeight = isnull(@EmptyWeight, EmptyWeight),
         StatusId = isnull(@StatusId, StatusId)  
   where PalletId = @PalletId
  
  select @Error = @@Error
  
  
  return @Error
  
end
