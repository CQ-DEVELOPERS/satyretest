﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Order_Update
  ///   Filename       : p_Planning_Order_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Order_Update
(
 @outboundShipmentId int,
 @issueId            int,
 @locationId         int,
 @priorityId         int,
 @routeId            int,
 @deliveryDate       datetime,
 @remarks            nvarchar(250),
 @areaType           nvarchar(10) = null,
 @AddressLine1       nvarchar(255),
 @AddressLine2       nvarchar(255),
 @AddressLine3       nvarchar(255),
 @AddressLine4       nvarchar(255),
 @AddressLine5       nvarchar(255)
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @locationId = -1
    set @locationId = null
  
  if @priorityId = -1
    set @priorityId = null
  
  if @routeId = -1
    set @routeId = null
  
  begin transaction
  
  exec @Error = p_Issue_Update
   @IssueId       = @issueId,
   @LocationId    = @locationId,
   @DespatchBay   = @locationId,
   @PriorityId    = @priorityId,
   @RouteId       = @routeId,
   @DeliveryDate  = @deliveryDate,
   @Remarks       = @remarks
  
  if @Error <> 0
    goto error
  
  update Issue
     set AreaType = @areaType,
         AddressLine1        = @AddressLine1,
         AddressLine2        = @AddressLine2,
         AddressLine3        = @AddressLine3,
         AddressLine4        = @AddressLine4,
         AddressLine5        = @AddressLine5
   where IssueId = @issueId
  
  if isnull(@outboundShipmentId, -1) != -1
    update j
	  	   set PriorityId = @priorityId
	     from IssueLineInstruction ili
	     join Instruction       i (nolock) on ili.InstructionId = i.InstructionId
	     join Job               j (nolock) on i.JobId = j.JobId
  	  where ili.OutboundShipmentId = @outboundShipmentId
	 else
    update j
		     set PriorityId = @priorityId
	     from IssueLineInstruction ili
  	   join Instruction       i (nolock) on ili.InstructionId = i.InstructionId
	     join Job               j (nolock) on i.JobId = j.JobId
	    where ili.IssueId = @issueId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  exec @Error = p_OutboundShipment_Update
   @OutboundShipmentId = @outboundShipmentId,
   @ShipmentDate       = @deliveryDate,
   @LocationId         = @locationId,
   @DespatchBay        = @locationId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Planning_Order_Update'); 
    rollback transaction
    return @Error
end
