﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OrderNumber_Search
  ///   Filename       : p_OrderNumber_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OrderNumber_Search
(
 @ConnectionString  nvarchar(1000) = 'Data Source=CQSERVER0001;Initial Catalog=CQuential',
 @InboundShipmentId int = null,
 @ReceiptId         int = null
)

as
begin
	 set nocount on;
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @ReceiptId = -1
    set @ReceiptId = null
  
  if @InboundShipmentId is null
    select OrderNumber
      from InboundDocument         id (nolock)
      join Receipt                  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
     where r.ReceiptId           = isnull(@ReceiptId, r.ReceiptId)
  else
    select OrderNumber
      from InboundDocument         id (nolock)
      join Receipt                  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
      join InboundShipmentReceipt isr (nolock) on r.ReceiptId          = isr.ReceiptId
     where isr.InboundShipmentId = isnull(@InboundShipmentId, isr.InboundShipmentId)
       and r.ReceiptId           = isnull(@ReceiptId, r.ReceiptId)
end
