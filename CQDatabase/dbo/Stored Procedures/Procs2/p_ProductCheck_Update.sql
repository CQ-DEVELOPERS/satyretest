﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductCheck_Update
  ///   Filename       : p_ProductCheck_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:01
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ProductCheck table.
  /// </remarks>
  /// <param>
  ///   @JobId int = null,
  ///   @OperatorId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductCheck_Update
(
 @JobId int = null,
 @OperatorId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update ProductCheck
     set JobId = isnull(@JobId, JobId),
         OperatorId = isnull(@OperatorId, OperatorId) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
