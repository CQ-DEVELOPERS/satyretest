﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Picking_Update_Line
  ///   Filename       : p_Picking_Update_Line.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Picking_Update_Line
(
 @instructionId     int,
 @confirmedQuantity float,
 @palletId          int,
 @operatorId        int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @OldStatusId       int,
          @Quantity          float
  
  select @GetDate = dbo.ufn_Getdate()
  set @GetDate = dateadd(ss, -1, @GetDate)
  
  set @Errormsg = 'Error executing p_Picking_Update_Line'
  
  if @palletId = 0
    set @palletId = null
  
  if @operatorId = -1
    set @operatorId = null
  
  select @StatusId = dbo.ufn_StatusId('I','F')
  
  select @Quantity = Quantity,
         @OldStatusId = StatusId
    from Instruction
   where InstructionId = @instructionId
  
  begin transaction
  
  if @confirmedQuantity < 0
  begin
    set @Errormsg = 'Error executing p_Picking_Update_Line - Invliad quantity, cannot be less than 0'
    set @Error = 2
    goto error
  end
  
  if @confirmedQuantity > @Quantity
  begin
    set @Errormsg = 'Error executing p_Picking_Update_Line - Invliad quantity, cannot be greater than Ordered quantity'
    set @Error = 2
    goto error
  end
  
  if @StatusId = @OldStatusId -- Both Finished
  begin
    set @Errormsg = 'Error executing p_Picking_Update_Line - Status already complete!'
    set @Error = 2
    goto error
  end
  
  exec @Error = p_Instruction_Started
   @instructionId       = @instructionId,
   @operatorId          = @operatorId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId       = @InstructionId,
   @Confirmed           = 1
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId     = @InstructionId,
   @ConfirmedQuantity = @confirmedQuantity,
   @PalletId          = @palletId,
   @StartDate         = @GetDate
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId       = @InstructionId,
   @Confirmed           = 1
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Finished
   @instructionId       = @instructionId,
   @operatorId          = @operatorId
  
  if @Error <> 0
    goto error
  
  commit transaction
  
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
