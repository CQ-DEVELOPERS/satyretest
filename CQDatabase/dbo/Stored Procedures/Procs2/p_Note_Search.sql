﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Note_Search
  ///   Filename       : p_Note_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:46
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Note table.
  /// </remarks>
  /// <param>
  ///   @NoteId int = null output,
  ///   @NoteCode nvarchar(510) = null,
  ///   @Note nvarchar(max) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Note.NoteId,
  ///   Note.NoteCode,
  ///   Note.Note 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Note_Search
(
 @NoteId int = null output,
 @NoteCode nvarchar(510) = null,
 @Note nvarchar(max) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @NoteId = '-1'
    set @NoteId = null;
  
  if @NoteCode = '-1'
    set @NoteCode = null;
  
  if @Note = '-1'
    set @Note = null;
  
 
  select
         Note.NoteId
        ,Note.NoteCode
        ,Note.Note
    from Note
   where isnull(Note.NoteId,'0')  = isnull(@NoteId, isnull(Note.NoteId,'0'))
     and isnull(Note.NoteCode,'%')  like '%' + isnull(@NoteCode, isnull(Note.NoteCode,'%')) + '%'
     and isnull(Note.Note,'%')  like '%' + isnull(@Note, isnull(Note.Note,'%')) + '%'
  order by NoteCode
  
end
