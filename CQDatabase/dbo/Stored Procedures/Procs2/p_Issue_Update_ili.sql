﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Issue_Update_ili
  ///   Filename       : p_Issue_Update_ili.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Oct 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Issue_Update_ili
(
 @IssueId int
)

as
begin
  set nocount on;
  
  declare @TableResut as table
  (
   InstructionId     int,
   InstructionRefId  int,
   ConfirmedQuantity numeric(13,6)
  )
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Issue_Update_ili',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @InstructionId     int,
          @InstructionRefId  int,
          @ConfirmedQuantity numeric(13,6)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  insert @TableResut
        (InstructionId,
         InstructionRefId,
         ConfirmedQuantity)
  select distinct i.InstructionId,
         i.InstructionRefId,
         i.ConfirmedQuantity
    from IssueLineInstruction ili (nolock)
    join Instruction            i (nolock) on isnull(i.InstructionRefId,i.InstructionId) = ili.InstructionId
   where ili.IssueId = @IssueId
  
  while exists(select top 1 1 from @TableResut)
  begin
    select top 1
           @InstructionId     = InstructionId,
           @InstructionRefId  = InstructionRefId,
           @ConfirmedQuantity = ConfirmedQuantity
      from @TableResut tr
    order by InstructionId
    
    delete @TableResut
     where InstructionId = @InstructionId
    
    exec p_Instruction_Update_ili
     @InstructionId    = @InstructionId,
     @InstructionRefId = @InstructionRefId,
     @insConfirmed     = @ConfirmedQuantity
    
    if @Error <> 0
      goto error
  end
  
  update parent
     set Quantity = parent.Quantity + (child.Quantity - child.ConfirmedQuatity)
    from IssueLine child
    join IssueLine parent on child.ParentIssueLineId = parent.IssueLineId
    join Status         s on parent.StatusId = s.StatusId
                         --and s.StatusCode in ('BP','W','OH')
   where child.IssueId = @IssueId
     and child.Quantity - child.ConfirmedQuatity > 0
  
  update child
     set Quantity = Quantity - (child.Quantity - child.ConfirmedQuatity)
    from IssueLine child
   where child.IssueId = @IssueId
     and child.Quantity - child.ConfirmedQuatity > 0
     and child.ParentIssueLineId is not null
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
