﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Quantity
  ///   Filename       : p_Mobile_Product_Check_Quantity.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Quantity
(
 @WarehouseId        int,
 @jobId              int,
 @storageUnitBatchId int,
 @quantity           numeric(13,6),
 @scanMode           bit = 0,
 @ShowMsg            bit = 1,
 @BarCode			 nvarchar(50)
)

as
begin
	 set nocount on;
  
  declare @MobileLogId int,
		  @DefQty float
	
	
set @DefQty = 0

if @BarCode != null and @BarCode != ''
begin
select @DefQty = p.Quantity
    from   Pack p (nolock)
    where  p.Barcode = @BarCode

set @quantity = @quantity * @DefQty
end
  
  insert MobileLog
        (ProcName,
         WarehouseId,
         JobId,
         StorageUnitBatchId,
         Quantity,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @WarehouseId,
         @JobId,
         @StorageUnitBatchId,
         @Quantity,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @ConfirmedQuantity numeric(13,6),
          @StorageUnitId     int
  
  if dbo.ufn_Configuration(34, @WarehouseId) = 1
  begin
    select @ConfirmedQuantity = sum(ConfirmedQuantity)
      from CheckingProduct (nolock)
     where JobId              = @jobId
       and StorageUnitBatchId = @storageUnitBatchId
    
    if @ConfirmedQuantity = @quantity or (@ConfirmedQuantity >= @quantity and dbo.ufn_Configuration(243, @WarehouseId) = 1 and @quantity > 0)
    begin
      if @scanMode = 1
      begin
        update CheckingProduct
           set CheckQuantity = case when @ShowMsg = 0 or SkipUpdate = 1
                                    then isnull(CheckQuantity,0) + @quantity
                                    else isnull(CheckQuantity,@quantity) --CheckQuantity - GS 2012-03-06 
                                    end,
               SkipUpdate    = @ShowMsg
         where JobId              = @jobId
           and StorageUnitBatchId = @storageUnitBatchId
      end
      else
      begin
        update CheckingProduct
           set CheckQuantity      = @quantity --ConfirmedQuantity
         where JobId              = @jobId
           and StorageUnitBatchId = @storageUnitBatchId
        
        if @@rowcount > 1
        begin
          if (select sum(ConfirmedQuantity)
                from CheckingProduct (nolock)
               where JobId              = @jobId
                 and StorageUnitBatchId = @storageUnitBatchId) = @quantity
          begin -- Everything equal
            update CheckingProduct
               set CheckQuantity      = ConfirmedQuantity
             where JobId              = @jobId
               and StorageUnitBatchId = @storageUnitBatchId
          end
          else
          begin -- Something wrong, only update one line with total quantity.
            update CheckingProduct
               set CheckQuantity      = null
             where JobId              = @jobId
               and StorageUnitBatchId = @storageUnitBatchId
            
            set rowcount 1 -- avoid updating more than one line as it could make it correct
            update CheckingProduct
               set CheckQuantity      = @quantity
             where JobId              = @jobId
               and StorageUnitBatchId = @storageUnitBatchId
            set rowcount 0
          end
        end
      end
      
      update i
         set CheckQuantity = cp.CheckQuantity --cp.ConfirmedQuantity
        from Instruction      i
        join CheckingProduct cp (nolock) on i.InstructionId = cp.InstructionId
       where cp.JobId              = @jobId
         and cp.StorageUnitBatchId = @storageUnitBatchId
      
      exec p_Mobile_Product_Check_Lines @JobId = @jobId
      
      if @ShowMsg = 1
        select StorageUnitBatchId,
               ProductCode,
               Product,
               SKUCode,
               Batch,
               ExpiryDate,
               sum(ConfirmedQuantity) as 'Quantity',
               Lines,
               ErrorMsg
        from CheckingProduct (nolock)
       where JobId = @jobId
         and StorageUnitBatchId = @storageUnitBatchId
      group by StorageUnitBatchId,
               ProductCode,
               Product,
               SKUCode,
               Batch,
               ExpiryDate,
               Lines,
               ErrorMsg
    end
    else
    begin
      select -1 as 'StorageUnitBatchId',
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ExpiryDate,
             sum(ConfirmedQuantity) as 'Quantity',
             Lines,
             ErrorMsg
      from CheckingProduct (nolock)
     where JobId = @jobId
       and StorageUnitBatchId = @storageUnitBatchId
    group by StorageUnitBatchId,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ExpiryDate,
             Lines,
             ErrorMsg
    end
  end
  else
  begin
    select @StorageUnitId = StorageUnitId
      from StorageUnitBatch (nolock)
     where StorageUnitBatchId = @storageUnitBatchId
    
    select @ConfirmedQuantity = sum(ConfirmedQuantity)
      from CheckingProduct (nolock)
     where JobId         = @jobId
       and StorageUnitId = @StorageUnitId
    
    if @ConfirmedQuantity = @quantity or (@ConfirmedQuantity >= @quantity and dbo.ufn_Configuration(243, @WarehouseId) = 1 and @quantity > 0)
    begin
      if @scanMode = 1
      begin
        update CheckingProduct
           set CheckQuantity = isnull(CheckQuantity,0) + @quantity --ConfirmedQuantity
         where JobId              = @jobId
           and StorageUnitBatchId = @storageUnitBatchId
      end
      else
      begin
        update CheckingProduct
           set CheckQuantity      = @quantity --ConfirmedQuantity
         where JobId              = @jobId
           and StorageUnitBatchId = @storageUnitBatchId
        
        if @@rowcount > 1
        begin
          if (select sum(ConfirmedQuantity)
                from CheckingProduct (nolock)
               where JobId              = @jobId
                 and StorageUnitBatchId = @storageUnitBatchId) = @quantity
          begin -- Everything equal
            update CheckingProduct
               set CheckQuantity      = ConfirmedQuantity
             where JobId              = @jobId
               and StorageUnitBatchId = @storageUnitBatchId
          end
          else
          begin -- Something wrong, only update one line with total quantity.
            update CheckingProduct
               set CheckQuantity      = null
             where JobId              = @jobId
               and StorageUnitBatchId = @storageUnitBatchId
            
            set rowcount 1 -- avoid updating more than one line as it could make it correct
            update CheckingProduct
               set CheckQuantity      = @quantity
             where JobId              = @jobId
               and StorageUnitBatchId = @storageUnitBatchId
            set rowcount 0
          end
        end
      end

      update i
         set CheckQuantity = cp.CheckQuantity
        from Instruction i
        join CheckingProduct cp on i.InstructionId = cp.InstructionId
       where cp.JobId         = @jobId
         and cp.StorageUnitId = @StorageUnitId
      
      exec p_Mobile_Product_Check_Lines @JobId = @jobId
      
      if @ShowMsg = 1
        select top 1
               sub2.StorageUnitBatchId,
               cp.ProductCode,
               cp.Product,
               cp.SKUCode,
               b.Batch,
               b.ExpiryDate,
               sum(cp.ConfirmedQuantity) as 'Quantity',
               cp.Lines,
               cp.ErrorMsg
          from CheckingProduct    cp (nolock)
          join StorageUnitBatch sub2 (nolock) on cp.StorageUnitId   = sub2.StorageUnitId
          join Batch               b (nolock) on sub2.BatchId       = b.BatchId
         where cp.JobId = @jobId
           and cp.StorageUnitId = @storageUnitId
           and cp.batch          = 'Default'
        group by sub2.StorageUnitBatchId,
                 cp.ProductCode,
                 cp.Product,
                 cp.SKUCode,
                 b.Batch,
                 b.ExpiryDate,
                 cp.Lines,
                 cp.ErrorMsg
    end
    else
    begin
      select top 1
             -1 as 'StorageUnitBatchId',
             cp.ProductCode,
             cp.Product,
             cp.SKUCode,
             b.Batch,
             b.ExpiryDate,
             sum(cp.ConfirmedQuantity) as 'Quantity',
             cp.Lines,
             cp.ErrorMsg
        from CheckingProduct    cp (nolock)
        join StorageUnitBatch sub2 (nolock) on cp.StorageUnitId   = sub2.StorageUnitId
        join Batch               b (nolock) on sub2.BatchId       = b.BatchId
       where cp.JobId = @jobId
         and cp.StorageUnitId = @storageUnitId
         and cp.batch          = 'Default'
      group by cp.ProductCode,
               cp.Product,
               cp.SKUCode,
               b.Batch,
               b.ExpiryDate,
               cp.Lines,
               cp.ErrorMsg
    end
  end
  
  update CheckingProduct
     set ErrorMsg = 'This product has already been scanned Quantity :' + convert(varchar(10), ISNULL(CheckQuantity, 0))
   where JobId              = @jobId
     and StorageUnitBatchId = @storageUnitBatchId
  
  update MobileLog
     set EndDate = getdate()
   where MobileLogId = @MobileLogId
end

