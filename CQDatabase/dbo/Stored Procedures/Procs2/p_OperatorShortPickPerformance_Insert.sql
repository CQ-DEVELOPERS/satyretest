﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorShortPickPerformance_Insert
  ///   Filename       : p_OperatorShortPickPerformance_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:30
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorShortPickPerformance table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @InstructionTypeId int = null,
  ///   @EndDate datetime = null,
  ///   @ActivityStatus char(1) = null,
  ///   @PickingRate int = null,
  ///   @Units int = null,
  ///   @UnitsShort int = null,
  ///   @Weight float = null,
  ///   @WeightShort float = null,
  ///   @Instructions int = null,
  ///   @Orders int = null,
  ///   @OrderLines int = null,
  ///   @Jobs int = null,
  ///   @ActiveTime int = null,
  ///   @DwellTime int = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  ///   OperatorShortPickPerformance.OperatorId,
  ///   OperatorShortPickPerformance.InstructionTypeId,
  ///   OperatorShortPickPerformance.EndDate,
  ///   OperatorShortPickPerformance.ActivityStatus,
  ///   OperatorShortPickPerformance.PickingRate,
  ///   OperatorShortPickPerformance.Units,
  ///   OperatorShortPickPerformance.UnitsShort,
  ///   OperatorShortPickPerformance.Weight,
  ///   OperatorShortPickPerformance.WeightShort,
  ///   OperatorShortPickPerformance.Instructions,
  ///   OperatorShortPickPerformance.Orders,
  ///   OperatorShortPickPerformance.OrderLines,
  ///   OperatorShortPickPerformance.Jobs,
  ///   OperatorShortPickPerformance.ActiveTime,
  ///   OperatorShortPickPerformance.DwellTime,
  ///   OperatorShortPickPerformance.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorShortPickPerformance_Insert
(
 @OperatorId int = null,
 @InstructionTypeId int = null,
 @EndDate datetime = null,
 @ActivityStatus char(1) = null,
 @PickingRate int = null,
 @Units int = null,
 @UnitsShort int = null,
 @Weight float = null,
 @WeightShort float = null,
 @Instructions int = null,
 @Orders int = null,
 @OrderLines int = null,
 @Jobs int = null,
 @ActiveTime int = null,
 @DwellTime int = null,
 @WarehouseId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorShortPickPerformance
        (OperatorId,
         InstructionTypeId,
         EndDate,
         ActivityStatus,
         PickingRate,
         Units,
         UnitsShort,
         Weight,
         WeightShort,
         Instructions,
         Orders,
         OrderLines,
         Jobs,
         ActiveTime,
         DwellTime,
         WarehouseId)
  select @OperatorId,
         @InstructionTypeId,
         @EndDate,
         @ActivityStatus,
         @PickingRate,
         @Units,
         @UnitsShort,
         @Weight,
         @WeightShort,
         @Instructions,
         @Orders,
         @OrderLines,
         @Jobs,
         @ActiveTime,
         @DwellTime,
         @WarehouseId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
