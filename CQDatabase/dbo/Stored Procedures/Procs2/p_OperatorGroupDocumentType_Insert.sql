﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupDocumentType_Insert
  ///   Filename       : p_OperatorGroupDocumentType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:18
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorGroupDocumentType table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @OutboundDocumentTypeId int = null,
  ///   @PriorityId int = null 
  /// </param>
  /// <returns>
  ///   OperatorGroupDocumentType.OperatorGroupId,
  ///   OperatorGroupDocumentType.OutboundDocumentTypeId,
  ///   OperatorGroupDocumentType.PriorityId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupDocumentType_Insert
(
 @OperatorGroupId int = null,
 @OutboundDocumentTypeId int = null,
 @PriorityId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorGroupDocumentType
        (OperatorGroupId,
         OutboundDocumentTypeId,
         PriorityId)
  select @OperatorGroupId,
         @OutboundDocumentTypeId,
         @PriorityId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
