﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Printer_Update
  ///   Filename       : p_Operator_Printer_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Printer_Update
(
 @operatorId int,
 @printer    nvarchar(50),
 @port       nvarchar(50),
 @iPAddress  nvarchar(50)
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_Operator_Update
   @OperatorId = @operatorId,
   @Printer    = @printer,
   @Port       = @port,
   @IPAddress  = @iPAddress
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Operator_Printer_Update'); 
    rollback transaction
    return @Error
end
