﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PickException_Lines_By_Job
  ///   Filename       : p_PickException_Lines_By_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PickException_Lines_By_Job
(
 @JobId               int,
 @InstructionTypeCode nvarchar(10)
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   DocumentNumber      nvarchar(30),
   OrderNumber         nvarchar(30),
   IssueLineId         int,
   JobId               int,
   InstructionId       int,
   InstructionType     nvarchar(30),
   StorageUnitBatchId  int,
   ProductCode         nvarchar(30),
   Product             nvarchar(50),
   SKUCode             nvarchar(50),
   SKU                 nvarchar(50),
   Area                nvarchar(50),
   PickLocationId      int,
   PickLocation        nvarchar(15),
   Quantity            float,
   ConfirmedQuantity   float,
   CheckQuantity       float,
   StatusId            int,
   Status              nvarchar(50),
   OperatorId          int,
   Operator            nvarchar(50)
  );
  
  insert @TableResult
        (JobId,
         InstructionId,
         InstructionType,
         StorageUnitBatchId,
         PickLocationId,
         Quantity,
         ConfirmedQuantity,
         StatusId,
         OperatorId,
         CheckQuantity)
  select i.JobId,
         i.InstructionId,
         it.InstructionType,
         i.StorageUnitBatchId,
         i.PickLocationId,
         i.Quantity,
         i.ConfirmedQuantity,
         i.StatusId,
         i.OperatorId,
         i.CheckQuantity
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where it.InstructionTypeCode in ('PM','PS','FM','PR')-- @InstructionTypeCode
     and i.JobId                 = @JobId
     and i.ConfirmedQuantity     > 0
    
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SkuCode,
         SKU         = sku.SKU
    from @TableResult tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.StatusId = s.StatusId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set PickLocation = l.Location,
         Area         = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId      = al.LocationId
    join Area          a (nolock) on al.AreaId         = a.AreaId
  
  select JobId,
         InstructionId,
         InstructionType,
         DocumentNumber,
         OrderNumber,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Quantity,
         ConfirmedQuantity,
         CheckQuantity,
         Status,
         Operator,
         Area,
         PickLocation
    from @TableResult
end
