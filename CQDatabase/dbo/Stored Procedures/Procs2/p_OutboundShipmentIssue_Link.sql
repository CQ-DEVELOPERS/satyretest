﻿
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipmentIssue_Link
  ///   Filename       : p_OutboundShipmentIssue_Link.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipmentIssue_Link
(
 @OutboundShipmentId int,
 @IssueId            int,
 @DropSequence       int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @LocationId        int,
          @RouteId           int,
          @StatusCode        nvarchar(10),
          @NumberOfOrders    int,
          @ExternalCompanyId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if not exists(select 1 from OutboundShipmentIssue (nolock) where OutboundShipmentId = @OutboundShipmentId and IssueId = @IssueId)
  begin
    select @StatusCode = s.StatusCode,
           @ExternalCompanyId = od.ExternalCompanyId,
           @DropSequence = isnull(@DropSequence, i.DropSequence)
      from Issue  i (nolock)
      join Status s (nolock) on i.StatusId = s.StatusId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where i.IssueId = @IssueId
    
    if @Error <> 0
      goto error
    
    if exists(select 1
                from OutboundShipmentIssue osi (nolock)
                join Issue                   i (nolock) on osi.IssueId = i.IssueId
                join Status                  s (nolock) on i.StatusId = s.StatusId and s.StatusCode != 'OH'
               where OutboundShipmentId = @OutboundShipmentId
                 and s.StatusCode      != @StatusCode)
      set @Error = -1
    
    if @Error <> 0
      goto error
    
    select @RouteId        = RouteId,
           @LocationId     = LocationId
           --@NumberOfOrders = isnull(NumberOfOrders,0) + 1
      from OutboundShipment (nolock)
     where OutboundShipmentId = @OutboundShipmentId
    
    select @NumberOfOrders = COUNT(1) + 1
      from OutboundShipmentIssue (nolock)
     where OutboundShipmentId = @OutboundShipmentId
    
    if @RouteId is not null
    begin
      exec @Error = p_Issue_Update
       @IssueId            = @IssueId,
       @LocationId         = @LocationId,
       @RouteId            = @RouteId
      
      if @Error <> 0
        goto error
    end
    
    select @LocationId = LocationId,
           @RouteId    = RouteId
      from Issue (nolock)
     where IssueId = @IssueId
    
    exec @Error = p_OutboundShipment_Update
     @OutboundShipmentId = @OutboundShipmentId,
     @LocationId         = @LocationId,
     @RouteId            = @RouteId,
     @NumberOfOrders     = @NumberOfOrders
    
    if @Error <> 0
      goto error
    
    --if @DropSequence is null
    begin
      select @DropSequence = DropSequence
        from ExternalCompany (nolock)
       where ExternalCompanyId = @ExternalCompanyId
    end
    
    if @DropSequence is not null
    begin
      exec @Error = p_Issue_Update
       @IssueId            = @IssueId,
       @DropSequence       = @DropSequence
      
      if @Error <> 0
        goto error
    end
    
    exec @Error = p_OutboundShipmentIssue_Insert
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId,
     @DropSequence       = @DropSequence
    
    if @Error <> 0
      goto error
  end
  
  update i
     set OutboundShipmentId = osi.OutboundShipmentId
    from Instruction             i (nolock)
    join IssueLine              il (nolock) on i.IssueLineId = il.IssueLineId
    join OutboundShipmentIssue osi (nolock) on il.IssueId = osi.IssueId
   where i.OutboundShipmentId is null
     and osi.OutboundShipmentId = @OutboundShipmentId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  insert InterfaceExportOrderPick
        (IssueId,
         OrderNumber,
         RecordStatus,
         ProcessedDate)
  select i.IssueId,
         od.OrderNumber,
         'N',
         null
    from OutboundShipmentIssue osi (nolock)
    join Issue                   i (nolock) on osi.IssueId = i.IssueId
    join OutboundDocument       od (nolock) on i.OutboundDocumentId      = od.OutboundDocumentId
    join OutboundDocumentType  odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where osi.OutboundShipmentId = @OutboundShipmentId
     and odt.OutboundDocumentTypeCode in ('EXP','DEL','FAR','COL','SAL','3RD')
     and not exists(select top 1 1 from InterfaceExportOrderPick op where i.IssueId = op.IssueId)
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  exec p_Outbound_Palletise_Create_Reference
   @OutboundShipmentId = @OutboundShipmentId,
   @IssueId            = @IssueId
  
  if @Error <> 0
    goto error
  
  if @@trancount > 0
    commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_OutboundShipmentIssue_Link'); 
    if @@trancount = 1
      rollback transaction
    else
      commit transaction
    return @Error
end


