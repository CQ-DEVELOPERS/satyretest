﻿/*
      <summary>
        Procedure Name : p_Palletised_Insert
        Filename       : p_Palletised_Insert.sql
        Create By      : Grant Schultz
        Date Created   : 23 May 2007 13:00:00
      </summary>
      <remarks>
        Inserts palletised lines into the instruction table
      </remarks>
      <param>
        @WarehouseId         int,
        @OperatorId          int,
        @InstructionTypeCode nvarchar(10) = null,
        @StorageUnitBatchId  int,
        @PickLocationId      int,
        @StoreLocationId     int,
        @Quantity            float
        @Weight              float = null,
        @PriorityId          int = null,
        @ReceiptLineId       int = null,
        @IssueLineId         int = null,
        @JobId               int = null,
        @ReasonId            int = null
      </param>
      <returns>
        @error
      </returns>
      <newpara>
        Modified by    : 
        Modified Date  : 
        Details        : 
      </newpara>
*/
CREATE procedure p_Palletised_Insert
(
 @WarehouseId         int,
 @OperatorId          int,
 @InstructionTypeCode nvarchar(10) = null,
 @StorageUnitBatchId  int,
 @PickLocationId      int,
 @StoreLocationId     int,
 @Quantity            float,
 @Weight              float = null,
 @PriorityId          int = null,
 @ReceiptLineId       int = null,
 @IssueLineId         int = null,
 @JobId               int = null output,
 @ReasonId            int = null,
 @PalletId            int = null,
 @InstructionRefId    int = null,
 @OutboundShipmentId  int = null,
 @DropSequence        int = null,
 @StatusId            int = null,
 @InstructionId       int = null output,
 @ReferenceNumber     nvarchar(30) = null,
 @TransactionControl  bit = 1,
 @AutoComplete        bit = null
)
as
begin
	 set nocount on
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @CreateDate        datetime,
          @InstructionTypeId int,
          @PalletQuantity    numeric(13,6),
          @ConfirmedQuantity numeric(13,6),
          @StorageUnitId     int,
          @GrossWeight       float,
          @NettWeight        float,
          @TareWeight        float,
          @JobGrossWeight    float,
          @JobNettWeight     float,
          @JobTareWeight     float
  
  select @CreateDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Palletised_Insert'
  
  if @StatusId is null
    select @StatusId = StatusId
      from Status (nolock)
     where StatusCode = 'W'
       and Type = 'I'
  
  select @StorageUnitId = StorageUnitId
    from StorageUnitBatch (nolock)
   where StorageUnitBatchId = @StorageUnitBatchId
  
  if @InstructionTypeCode is null
  begin
    select @PalletQuantity = max(Quantity)
      from Pack (nolock)
     where StorageUnitId = @StorageUnitId
    
    if @ReceiptLineId is not null
      if @PalletQuantity = @Quantity
        set @InstructionTypeCode = 'S'
      else
        set @InstructionTypeCode = 'SM'
    else if @IssueLineId is not null
      if @PalletQuantity = @Quantity
        set @InstructionTypeCode = 'P'
      else
        set @InstructionTypeCode = 'PM'
  end
  
  select @InstructionTypeId = InstructionTypeId
    from InstructionType (nolock)
   where InstructionTypeCode = @InstructionTypeCode
  
  if @PriorityId is null
    select @PriorityId  = PriorityId
      from InstructionType (nolock)
     where InstructionTypeCode = @InstructionTypeCode
  
  --if @Weight is null
  begin
    
    if (select ProductCategory
          from StorageUnit
         where StorageUnitId = @StorageUnitId) != 'V'
    begin
      select top 1
             @NettWeight  = NettWeight,
             @TareWeight  = TareWeight,
             @GrossWeight = Weight,
             @Weight      = Weight
        from Pack pk (nolock)
        join PackType pt (nolock) on pk.PackTypeId = pt.PackTypeId
       where StorageUnitId      = @StorageUnitId
         and pt.InboundSequence in (select max(InboundSequence) from PackType)
      order by pt.InboundSequence desc
      
      if @Weight is null
        set @Weight = 0
      
      if @Weight > 0 and @Quantity > 0
        select @Weight = @Quantity * @Weight
      
      if @NettWeight is null
        set @NettWeight = 0
      
      if @NettWeight > 0 and @Quantity > 0
        select @NettWeight = convert(numeric(13,6), @Quantity) * @NettWeight
      
      if @TareWeight is null
        set @TareWeight = 0
      
      if @TareWeight > 0 and @Quantity > 0
        select @TareWeight = convert(numeric(13,6), @Quantity) * @TareWeight
      
      if @GrossWeight is null
        set @GrossWeight = 0
      
      if @GrossWeight > 0 and @Quantity > 0
        select @GrossWeight = convert(numeric(13,6), @Quantity) * @GrossWeight
    end
    else
      set @NettWeight = null
  end
  
  if (select dbo.ufn_Configuration(31, @WarehouseId)) = 1
    set @ConfirmedQuantity = @Quantity
  else
    set @ConfirmedQuantity = 0
  
  if @TransactionControl = 1
    begin transaction
  
  -- Pick Location cannot = store location 
  if @PickLocationId = @StoreLocationId
  begin
    set @Error = -1
    goto error
  end
  
  if @JobId is null
  begin
    exec @Error = p_Job_Insert
     @JobId           = @JobId output,
     @PriorityId      = @PriorityId,
     @OperatorId      = @OperatorId,
     @StatusId        = @StatusId,
     @WarehouseId     = @WarehouseId,
     @ReferenceNumber = @ReferenceNumber,
     @TareWeight      = @TareWeight,
     @NettWeight      = @NettWeight,
     @Weight          = @Weight,
     @DropSequence    = @DropSequence
    
    if @error <> 0
      goto error
    
    exec @Error = p_OutboundPerformance_Insert
     @JobId         = @JobId output,
     @CreateDate    = @CreateDate
    
    if @error <> 0
      goto error
  end
  else
  begin
    select @JobNettWeight  = isnull(NettWeight,0),
           @JobTareWeight  = isnull(TareWeight,0),
           @JobGrossWeight = isnull(Weight,0)
      from Job (nolock)
     where JobId = @JobId
    
    select @JobNettWeight  = @JobNettWeight + @NettWeight,
           @JobTareWeight  = @JobTareWeight + @TareWeight,
           @JobGrossWeight = @JobGrossWeight + @Weight
    
    exec @Error = p_Job_Update
     @JobId           = @JobId,
     @TareWeight      = @JobTareWeight,
     @NettWeight      = @JobNettWeight,
     @Weight          = @JobGrossWeight
    
    if @error <> 0
      goto error
  end
  print 't'
  select @StatusId = dbo.ufn_StatusId('I','W')
  
  exec @Error = p_Instruction_Insert
   @InstructionId      = @InstructionId output,
   @ReasonId           = @ReasonId,
   @InstructionTypeId  = @InstructionTypeId,
   @StorageUnitBatchId = @StorageUnitBatchId,
   @WarehouseId        = @WarehouseId,
   @StatusId           = @StatusId,
   @JobId              = @JobId,
   @OperatorId         = @OperatorId,
   @PickLocationId     = @PickLocationId,
   @StoreLocationId    = @StoreLocationId,
   @InstructionRefId   = @InstructionRefId,
   @Quantity           = @Quantity,
   @ConfirmedQuantity  = @ConfirmedQuantity,
   @Weight             = @Weight,
   @ConfirmedWeight    = 0,
   @PalletId           = @PalletId,
   @CreateDate         = @CreateDate,
   @StartDate          = null,
   @EndDate            = null,
   @ReceiptLineId      = @ReceiptLineId,
   @IssueLineId        = @IssueLineId,
   @OutboundShipmentId = @OutboundShipmentId,
   @DropSequence       = @DropSequence,
   @AutoComplete       = @AutoComplete
  
  if @error <> 0
    goto error
  
  exec @error = p_StorageUnitBatchLocation_Reserve
   @InstructionId       = @InstructionId
  
  if @error <> 0
    goto error
  
  if @InstructionTypeCode in ('S','SM') and @PalletId is not null
  begin
    exec p_Instruction_Print @InstructionId=@InstructionId
    
    if @error <> 0
      goto error
  end
  
  if @TransactionControl = 1
    commit transaction
  return
  
  error:
    raiserror 900000 @Errormsg
    if @TransactionControl = 1
      rollback transaction
    return @error
end
