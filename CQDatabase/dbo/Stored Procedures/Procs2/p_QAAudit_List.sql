﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QAAudit_List
  ///   Filename       : p_QAAudit_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the QAAudit table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   QAAudit.QAAuditId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QAAudit_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as QAAuditId
        ,null as 'QAAudit'
  union
  select
         QAAudit.QAAuditId
        ,QAAudit.QAAuditId as 'QAAudit'
    from QAAudit
  
end
