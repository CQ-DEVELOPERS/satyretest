﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupMenuItemHistory_Insert
  ///   Filename       : p_OperatorGroupMenuItemHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:07
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorGroupMenuItemHistory table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @MenuItemId int = null,
  ///   @Access bit = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   OperatorGroupMenuItemHistory.OperatorGroupId,
  ///   OperatorGroupMenuItemHistory.MenuItemId,
  ///   OperatorGroupMenuItemHistory.Access,
  ///   OperatorGroupMenuItemHistory.CommandType,
  ///   OperatorGroupMenuItemHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupMenuItemHistory_Insert
(
 @OperatorGroupId int = null,
 @MenuItemId int = null,
 @Access bit = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorGroupMenuItemHistory
        (OperatorGroupId,
         MenuItemId,
         Access,
         CommandType,
         InsertDate)
  select @OperatorGroupId,
         @MenuItemId,
         @Access,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
