﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupHistory_Insert
  ///   Filename       : p_OperatorGroupHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2012 07:59:17
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorGroupHistory table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @OperatorGroup varchar(30) = null,
  ///   @RestrictedAreaIndicator bit = null,
  ///   @CommandType varchar(10) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   OperatorGroupHistory.OperatorGroupId,
  ///   OperatorGroupHistory.OperatorGroup,
  ///   OperatorGroupHistory.RestrictedAreaIndicator,
  ///   OperatorGroupHistory.CommandType,
  ///   OperatorGroupHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupHistory_Insert
(
 @OperatorGroupId int = null,
 @OperatorGroup varchar(30) = null,
 @RestrictedAreaIndicator bit = null,
 @CommandType varchar(10) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  insert OperatorGroupHistory
        (OperatorGroupId,
         OperatorGroup,
         RestrictedAreaIndicator,
         CommandType,
         InsertDate)
  select @OperatorGroupId,
         @OperatorGroup,
         @RestrictedAreaIndicator,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
