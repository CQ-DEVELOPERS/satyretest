﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLinePackaging_Search
  ///   Filename       : p_IssueLinePackaging_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:55
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the IssueLinePackaging table.
  /// </remarks>
  /// <param>
  ///   @PackageLineId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   IssueLinePackaging.PackageLineId,
  ///   IssueLinePackaging.IssueId,
  ///   IssueLinePackaging.IssueLineId,
  ///   IssueLinePackaging.JobId,
  ///   IssueLinePackaging.StorageUnitBatchId,
  ///   IssueLinePackaging.StorageUnitId,
  ///   IssueLinePackaging.StatusId,
  ///   IssueLinePackaging.OperatorId,
  ///   IssueLinePackaging.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLinePackaging_Search
(
 @PackageLineId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @PackageLineId = '-1'
    set @PackageLineId = null;
  
 
  select
         IssueLinePackaging.PackageLineId
        ,IssueLinePackaging.IssueId
        ,IssueLinePackaging.IssueLineId
        ,IssueLinePackaging.JobId
        ,IssueLinePackaging.StorageUnitBatchId
        ,IssueLinePackaging.StorageUnitId
        ,IssueLinePackaging.StatusId
        ,IssueLinePackaging.OperatorId
        ,IssueLinePackaging.Quantity
    from IssueLinePackaging
   where isnull(IssueLinePackaging.PackageLineId,'0')  = isnull(@PackageLineId, isnull(IssueLinePackaging.PackageLineId,'0'))
  
end
