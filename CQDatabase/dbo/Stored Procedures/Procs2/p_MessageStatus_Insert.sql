﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MessageStatus_Insert
  ///   Filename       : p_MessageStatus_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:05
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the MessageStatus table.
  /// </remarks>
  /// <param>
  ///   @StatusId int = null,
  ///   @StatusCode nvarchar(40) = null,
  ///   @StatusDesc nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   MessageStatus.StatusId,
  ///   MessageStatus.StatusCode,
  ///   MessageStatus.StatusDesc 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MessageStatus_Insert
(
 @StatusId int = null,
 @StatusCode nvarchar(40) = null,
 @StatusDesc nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert MessageStatus
        (StatusId,
         StatusCode,
         StatusDesc)
  select @StatusId,
         @StatusCode,
         @StatusDesc 
  
  select @Error = @@Error
  
  
  return @Error
  
end
