﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_AreaId
  ///   Filename       : p_Location_Search_AreaId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_AreaId
(
 @areaId int
)

as
begin
	 set nocount on;
	 
	 if @areaId = -1
	   set @areaId = null
  
  select l.LocationId,
         l.Location
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
   where al.AreaId = isnull(@areaId, al.AreaId)
     and l.StocktakeInd = 0
  order by l.Ailse, l.[Column], l.[Level]
end
