﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Backorder_Delete_Line
  ///   Filename       : p_Outbound_Backorder_Delete_Line.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Backorder_Delete_Line
(
 @IssueLineId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_IssueLine_Delete
   @IssueLineId = @IssueLineId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Outbound_Backorder_Delete_Line'); 
    rollback transaction
    return @Error
end
