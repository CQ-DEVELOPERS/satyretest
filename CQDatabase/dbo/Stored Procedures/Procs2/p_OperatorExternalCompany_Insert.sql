﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorExternalCompany_Insert
  ///   Filename       : p_OperatorExternalCompany_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:16
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @ExternalCompanyId int = null 
  /// </param>
  /// <returns>
  ///   OperatorExternalCompany.OperatorId,
  ///   OperatorExternalCompany.ExternalCompanyId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorExternalCompany_Insert
(
 @OperatorId int = null,
 @ExternalCompanyId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OperatorExternalCompany
        (OperatorId,
         ExternalCompanyId)
  select @OperatorId,
         @ExternalCompanyId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
