﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Next_Line
    ///   Filename       : p_Mobile_Next_Line.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 11 June 2007 14:15:00
    /// </summary>
    /// <remarks>
    ///   Inserts palletised lines into the instruction table
    /// </remarks>
    /// <param>
    ///   @JobId               int,
    ///   @InstructionId       int output
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
    ///   Details        : 
    /// </newpara>
*/
create procedure p_Mobile_Next_Line
(
 @JobId               int,
 @InstructionId       int output,
 @OperatorId          int = null,
 @Next                bit = 1
)
as
begin
	 set nocount on
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         OperatorId,
         JobId,
         InstructionId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @OperatorId,
         @JobId,
         @InstructionId,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @rowcount            int,
          @DropSequence        int,
          @WarehouseId         int,
          @InstructionTypeCode nvarchar(10),
          @PickLocationId      int,
          @NewPickLocationId   int,
          @OperatorGroupId     int,
          @StoreLocationId     int,
          @OverrideStore       bit,
          @IgnoreRep           bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @DropSequence   = DropSequence,
         @WarehouseId    = WarehouseId
    from Instruction (nolock)
   where InstructionId = @InstructionId
  
  if @@ROWCOUNT = 0
  begin
    select @DropSequence   = min(DropSequence),
           @WarehouseId    = min(WarehouseId)
      from Instruction (nolock)
     where JobId = @JobId
    
    if @DropSequence = 1
      set @DropSequence = 0
  end
  
  if @DropSequence is null
  begin
    exec @Error = p_Instruction_DropSequence
     @JobId = @JobId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
  end
  
  if @DropSequence is null
    set @DropSequence = -1
  
  select @IgnoreRep = dbo.ufn_Configuration(444, @WarehouseId)
  
  if @Next = 1
  begin
    select top 1 @InstructionId       = i.InstructionId,
                 @WarehouseId         = i.WarehouseId,
                 @InstructionTypeCode = it.InstructionTypeCode,
                 @PickLocationId      = i.PickLocationId,
                 @StoreLocationId     = i.StoreLocationId,
                 @OverrideStore       = isnull(i.OverrideStore,0)
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Status           s (nolock) on i.StatusId          = s.StatusId
     where i.JobId = @JobId
       and s.Type = 'I'
       and s.StatusCode not in ('F','NS')
       and isnull(i.DropSequence, 0) > @DropSequence
    order by case when @IgnoreRep = 1
                  then 'X'
                  else s.StatusCode
                  end desc,
                  i.DropSequence
    
    select @rowcount = @@rowcount
  end
  else
  begin
    select top 1 @InstructionId       = i.InstructionId,
                 @WarehouseId         = i.WarehouseId,
                 @InstructionTypeCode = it.InstructionTypeCode,
                 @PickLocationId      = i.PickLocationId,
                 @StoreLocationId     = i.StoreLocationId,
                 @OverrideStore       = isnull(i.OverrideStore,0)
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Status           s (nolock) on i.StatusId          = s.StatusId
     where i.JobId = @JobId
       and i.InstructionId <> isnull(@InstructionId, 0)
       and s.Type = 'I'
       and s.StatusCode not in ('F','NS')
       and isnull(i.DropSequence, -1) < @DropSequence
    order by case when @IgnoreRep = 1
                  then 'X'
                  else s.StatusCode
                  end desc,
                  i.DropSequence desc
    
    select @rowcount = @@rowcount
  end
  
  if @rowcount = 0
  begin
    select top 1 @InstructionId       = i.InstructionId,
                 @WarehouseId         = i.WarehouseId,
                 @InstructionTypeCode = it.InstructionTypeCode,
                 @PickLocationId      = i.PickLocationId,
                 @StoreLocationId     = i.StoreLocationId,
                 @OverrideStore       = isnull(i.OverrideStore,0)
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Status           s (nolock) on i.StatusId          = s.StatusId
     where i.JobId = @JobId
       and s.Type = 'I'
       and s.StatusCode not in ('F','NS')
    order by case when @IgnoreRep = 1
                  then 'X'
                  else s.StatusCode
                  end desc,
                  i.DropSequence
    
    select @rowcount = @@rowcount
  end
  
  if @rowcount = 0
  begin
    set @Error = -1
    goto Result
  end
  
  if @InstructionTypeCode = 'M' and @StoreLocationId is null and isnull(@OverrideStore,0) = 0
  begin
    exec @Error = p_Receiving_Manual_Palletisation_Auto_Locations
    @InstructionId = @InstructionId
    
    if @Error <> 0
    begin
      set @Error = -1
      goto Result
    end
  end
  
  if @InstructionTypeCode in ('PM','PS','FM')
    if dbo.ufn_Configuration(132, @WarehouseId) = 1
    begin
      exec @Error = p_Mobile_Location_Error
       @instructionId   = @InstructionId,
       @pickError       = 1,
       @storeError      = 0,
       @createStockTake = 0
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      select @NewPickLocationId = PickLocationId
        from Instruction (nolock)
       where InstructionId = @InstructionId
      
      if @PickLocationId != @NewPickLocationId
      begin
        update i
           set DropSequence = null
          from Instruction i (nolock)
          join Status      s (nolock) on i.StatusId = s.StatusId
         where i.JobId = @JobId
           and s.StatusCode in ('W','S')
        
        select @Error = @@Error
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
        
        exec @Error = p_Instruction_DropSequence
         @JobId = @JobId
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
      end
      
      if (select s.StatusCode
            from Instruction i (nolock)
            join Status      s (nolock) on i.StatusId = s.StatusId
           where i.InstructionId = @InstructionId) = 'NS'
      begin
        if (select a.AreaCode
              from Instruction i (nolock)
              join Location    l (nolock) on i.PickLocationId = l.LocationId
              join AreaLocation al (nolock) on l.LocationId = al.LocationId
              join Area          a (nolock) on al.AreaId = a.AreaId
             where i.InstructionId = @InstructionId) != 'TP'
        begin
          exec @Error = p_Mobile_Next_Line
           @JobId         = @JobId,
           @InstructionId = @InstructionId output,
           @OperatorId    = @operatorId,
           @Next          = @Next
		  
          if @Error <> 0
          begin
            set @Error = 99
            goto Result
          end
        end
      end
    end
  
  if @InstructionTypeCode = 'P' and @PickLocationId is null
  begin
    exec @Error = p_Mobile_Location_Error
     @instructionId   = @InstructionId,
     @pickError       = 1,
     @storeError      = 0,
     @createStockTake = 0
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    select @NewPickLocationId = PickLocationId
      from Instruction (nolock)
     where InstructionId = @InstructionId
    
    if @NewPickLocationId is null
    begin
      exec @Error = p_Instruction_No_Stock @InstructionId
      
      if @Error = 0
      begin
        set @Error = -99
        set @InstructionId = -1
      end
      
      goto Result
    end
    
    select @OperatorGroupId = OperatorGroupId
      from Operator (nolock)
     where OperatorId = @OperatorId
    
    if not exists(select aog.AreaId
                    from OperatorGroupInstructionType ogit (nolock)
                    join Instruction                  i    (nolock) on ogit.InstructionTypeId = i.InstructionTypeId
                                                                   and ogit.OperatorGroupId   = @OperatorGroupId
                    join AreaLocation                   al (nolock) on i.PickLocationId       = al.LocationId
                    join AreaOperatorGroup             aog (nolock) on al.AreaId              = aog.AreaId
                                                                   and ogit.OperatorGroupId   = aog.OperatorGroupId
                    join LocationOperatorGroup        lg   (nolock) on al.LocationId          = lg.LocationId
                                                                   and ogit.OperatorGroupId   = lg.OperatorGroupId
                   where i.InstructionId = @InstructionId)
    begin
      set @Error = -88
      set @InstructionId = -1
      
      update Job
         set OperatorId = null
        where JobId = @JobId
      
      goto Result
    end
  end
  
  if @OperatorId is not null
  begin
    exec @Error = p_Instruction_Started
     @InstructionId = @InstructionId,
     @OperatorId    = @operatorId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
  end
  
  Result:
    update MobileLog
       set EndDate = getdate(),
           InstructionId = @InstructionId
     where MobileLogId = @MobileLogId
     
    return @Error
end
