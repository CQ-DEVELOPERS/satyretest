﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Store_Full
  ///   Filename       : p_Mobile_Store_Full.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Mobile_Store_Full
(
@operatorId int,
@palletId int,
@instructionId int output
)
as
begin
     set nocount on
         
  declare @MobileLogId                  int
  
  insert MobileLog
         (ProcName,
         InstructionId,
         PalletId,
         OperatorId,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @InstructionId,
         @PalletId,
         @OperatorId,
         getdate()
         
  select @MobileLogId = scope_identity()
         
  declare @Error                        int,
          @Errormsg                     nvarchar(500),
          @GetDate                      datetime,
          @Rowcount                     int,
          @StatusId                     int,
          @JobId                        int,
          @PickLocationId               int,
          @StoreLocationId              int,
          @WarehouseId                  int,
          @OldInstructionId             int,
          @InstructionTypeCode          nvarchar(10),
          @Exception                    nvarchar(255),
          @ReceiptLineId				int,
          @OverrideStore				bit,
          @PalletStatusId				int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @PalletStatusId = p.StatusId
    from Pallet p
   where p.PalletId = @palletId
  
  if @PalletStatusId = dbo.ufn_StatusId('P','NA')
  begin
    begin transaction
	   set @InstructionId = -4
	   goto error
	 end

  select top 1
         @InstructionId = i.InstructionId,
         @JobId = i.JobId,
         @PalletId = i.PalletId,
         @PickLocationId = i.PickLocationId,
         @StoreLocationId = i.StoreLocationId,
         @WarehouseId = i.WarehouseId,
         @ReceiptLineId = i.ReceiptLineId,
         @OverrideStore = i.OverrideStore
    from Instruction i (nolock)
         join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
         join Job j /*lock*/ on i.JobId = j.JobId
         join Status sj (nolock) on j.StatusId = sj.StatusId
         join Status si (nolock) on i.StatusId = si.StatusId
   where i.PalletId = @PalletId
  and((it.InstructionTypeCode = 'PR' and sj.StatusCode in ('A','S','R','PR'))
  or (it.InstructionTypeCode in ('S','SM','M') and sj.StatusCode in ('PR','A','S','R'))
  or (it.InstructionTypeCode = 'M' and sj.StatusCode in ('W','S')))
     and si.StatusCode in ('W','S')
  order by case when i.StoreLocationId IS NOT NULL 
                then 0
                else 1
                end,
  i.InstructionId
  
  select @Rowcount = @@rowcount
  
  if @Rowcount = 0
  begin
    select top 1
           @InstructionId = i.InstructionId,
           @JobId = i.JobId,
           @PalletId = i.PalletId,
           @PickLocationId = i.PickLocationId,
           @StoreLocationId = i.StoreLocationId,
           @WarehouseId = i.WarehouseId,
           @ReceiptLineId = i.ReceiptLineId,
           @OverrideStore = i.OverrideStore
      from Instruction i (nolock)
           join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
           join Job j /*lock*/ on i.JobId = j.JobId
           join Status sj (nolock) on j.StatusId = sj.StatusId
           join Status si (nolock) on i.StatusId = si.StatusId
     where (i.PalletId = @palletId or j.ReferenceNumber = 'J:' + convert(nvarchar(10), @PalletId))
       and it.InstructionTypeCode in ('S','PR','SM','M')
       and sj.StatusCode in ('PR','A','S')
       and si.StatusCode in ('W','S')
    order by i.InstructionId
    
    select @Rowcount = @@rowcount
  end
  
  if @Rowcount = 0
  begin
    begin transaction
       set @InstructionId = -1
       set @Error = 2
    goto error
  end
  
  update ReceiptLine
     set PutawayStarted = @GetDate
   where ReceiptLineId = @ReceiptLineId
  
  if (select dbo.ufn_Configuration(387, @WarehouseId)) = 1
  begin
    update Instruction
       set AutoComplete = 1
     where JobId = @JobId
  end
  
  --if dbo.ufn_Configuration(238, @WarehouseId) = 1 -- moved lower down
  begin
    select @OldInstructionId = i.InstructionId,
           @JobId = i.JobId
      from Instruction i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Job j /*lock*/ on i.JobId = j.JobId
      join Status sj (nolock) on j.StatusId = sj.StatusId
      join Status si (nolock) on i.StatusId = si.StatusId
     where j.OperatorId = @operatorId
       and j.JobId != @JobId
       and it.InstructionTypeCode in ('S','PR','SM','M')
       and sj.StatusCode in ('S')--('PR','A','S')
       and si.StatusCode in ('W','S')
    
    if @OldInstructionId is not null
    begin
      set @StatusId = dbo.ufn_StatusId('PR','A')
      
      begin transaction
      
      if dbo.ufn_Configuration(238, @WarehouseId) = 0
      begin
        exec @Error = p_Job_Update
         @jobId = @jobId,
         @StatusId = @StatusId
        
        if @Error <> 0
          goto error
      end
      
      select @Exception = 'Operator tried to start another instruction (Old ins = ' + convert(varchar(10), isnull(@OldInstructionId,''))
      
      exec @Error = p_Exception_Insert
       @ExceptionId   = null,
       @ExceptionCode = 'STARTED',
       @Exception     = @Exception,
       @InstructionId = @InstructionId,
       @OperatorId    = @OperatorId,
       @JobId         = @JobId,
       @CreateDate    = @GetDate,
       @ExceptionDate = @Getdate
      
      if @Error <> 0
        goto error
      
      commit transaction
      
      begin transaction
      
      set @InstructionId = -3 --@OldInstructionId
      set @Error = 3
      goto error
    end
  end
  
  begin transaction
  
  if dbo.ufn_Configuration(429, @WarehouseId) = 0 and isnull(@OverrideStore, 0) = 0
  begin
    if @StoreLocationId is null
    begin
      exec @Error = p_Receiving_Manual_Palletisation_Auto_Locations
      @InstructionId = @InstructionId
      
      if @Error <> 0
        goto error
    end
    
    select @StoreLocationId = StoreLocationId
      from Instruction (nolock)
     where InstructionId = @InstructionId
    
    if @StoreLocationId is null
    begin
      set @InstructionId = -2
      set @Error = 2
      goto error
    end
  end
  
  select @StatusId = dbo.ufn_StatusId('R','S')
         
         exec @Error = p_Job_Update
         @JobId = @JobId,
         @StatusId = @StatusId,
         @OperatorId = @operatorId
         
  if @Error <> 0
  goto error
  
  exec @Error = p_Instruction_Started
  @InstructionId = @InstructionId,
  @OperatorId = @OperatorId
  
  if @Error <> 0
  goto error
  
  if dbo.ufn_Configuration(119, @WarehouseId) = 1
  begin
    exec @Error = p_Instruction_CrossDocking
    @InstructionId = @InstructionId,
    @OperatorId = @OperatorId,
    @PalletId = @PalletId
    
    if @Error <> 0
    goto error
  end
  
  exec @Error = p_Instruction_MovementSequence
  @InstructionId = @InstructionId,
  @operatorId = @operatorId
  
  if @Error <> 0
  goto error
  
  update MobileLog
     set EndDate = getdate()
   where MobileLogId = @MobileLogId
  
  commit transaction
  return
  
  error:
  rollback transaction
  
  update MobileLog
     set EndDate = getdate()
   where MobileLogId = @MobileLogId
  
  return @Error
end
