﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Movement_Create
  ///   Filename       : p_Mobile_Movement_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Movement_Create
(
 @instructionId int output
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @InstructionTypeId   int,
          @StorageUnitBatchId  int,
          @WarehouseId         int,
          @StatusId            int,
          @JobId               int,
          @OperatorId          int,
          @PickLocationId      int,
          @StoreLocationId     int,
          @PriorityId          int,
          @Quantity            float,
          @StorageUnitId       int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StorageUnitBatchId  = StorageUnitBatchId,
         @WarehouseId         = WarehouseId,
         @StatusId            = StatusId,
         @JobId               = JobId,
         @OperatorId          = OperatorId,
         @PickLocationId      = StoreLocationId,
         @Quantity            = Quantity
    from Instruction (nolock)
   where InstructionId = @instructionId
  
  select @InstructionTypeId = InstructionTypeId,
         @PriorityId        = PriorityId
    from InstructionType (nolock)
   where InstructionTypeCode = 'M'
  
  select @StatusId = dbo.ufn_StatusId('J','W')
  
  begin transaction
  
  exec @Error = p_Job_Insert
   @JobId              = @JobId output,
   @PriorityId         = @PriorityId,
   @OperatorId         = @OperatorId,
   @StatusId           = @StatusId,
   @WarehouseId        = @WarehouseId
  
  if @Error <> 0
    goto Error
  
  if (select dbo.ufn_Configuration(21, @WarehouseId)) = 1
  begin
    select @Quantity = p.Quantity
      from StorageUnitBatch sub
      join Pack               p on sub.StorageUnitId = p.StorageUnitId
     where p.PackTypeId = (select min(inboundSequence) from PackType (nolock))
       and sub.StorageUnitBatchId = @StorageUnitBatchId
    
    exec @Error = p_Location_Get
     @WarehouseId        = @WarehouseId,
     @LocationId         = @StoreLocationId output,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @Quantity           = @Quantity
    
    if @Error <> 0
      goto Error
  end
  
  select @StatusId = dbo.ufn_StatusId('I','W')
  
  exec @Error = p_Instruction_Insert
   @instructionId       = @instructionId output,
   @InstructionTypeId   = @InstructionTypeId,
   @StorageUnitBatchId  = @StorageUnitBatchId,
   @WarehouseId         = @WarehouseId,
   @StatusId            = @StatusId,
   @jobId               = @JobId,
   @operatorId          = @OperatorId,
   @PickLocationId      = @PickLocationId,
   @StoreLocationId     = @StoreLocationId,
   @InstructionRefId    = @instructionId,
   @Quantity            = 0,
   @confirmedQuantity   = 0,
   @Weight              = 0,
   @ConfirmedWeight     = 0,
   @PalletId            = null,
   @CreateDate          = @GetDate
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId       = @instructionId
  
  if @Error <> 0
    goto Error
  
  commit transaction
  return
  
  error:
    rollback transaction
    return @Error
end
