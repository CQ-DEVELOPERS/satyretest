﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductCategory_Search
  ///   Filename       : p_ProductCategory_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:00
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ProductCategory table.
  /// </remarks>
  /// <param>
  ///   @ProductCategoryId int = null output,
  ///   @ProductCategory char(1) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ProductCategory.ProductCategoryId,
  ///   ProductCategory.ProductCategoryType,
  ///   ProductCategory.ProductType,
  ///   ProductCategory.ProductCategory,
  ///   ProductCategory.PackingCategory,
  ///   ProductCategory.StackingCategory,
  ///   ProductCategory.MovementCategory,
  ///   ProductCategory.ValueCategory,
  ///   ProductCategory.StoringCategory 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductCategory_Search
(
 @ProductCategoryId int = null output,
 @ProductCategory char(1) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ProductCategoryId = '-1'
    set @ProductCategoryId = null;
  
  if @ProductCategory = '-1'
    set @ProductCategory = null;
  
 
  select
         ProductCategory.ProductCategoryId
        ,ProductCategory.ProductCategoryType
        ,ProductCategory.ProductType
        ,ProductCategory.ProductCategory
        ,ProductCategory.PackingCategory
        ,ProductCategory.StackingCategory
        ,ProductCategory.MovementCategory
        ,ProductCategory.ValueCategory
        ,ProductCategory.StoringCategory
    from ProductCategory
   where isnull(ProductCategory.ProductCategoryId,'0')  = isnull(@ProductCategoryId, isnull(ProductCategory.ProductCategoryId,'0'))
     and isnull(ProductCategory.ProductCategory,'%')  like '%' + isnull(@ProductCategory, isnull(ProductCategory.ProductCategory,'%')) + '%'
  
end
