﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualRequest_Search
  ///   Filename       : p_ManualRequest_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OutboundDocument table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualRequest_Search
(
 @StorageUnitBatchId int = null output
)

as
begin
	 set nocount on;
  
  declare @OutboundDocumentTypeId int
  
  select @OutboundDocumentTypeId = OutboundDocumentTypeId
    from OutboundDocumentType
   where OutboundDocumentTypeCode = 'MR'
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  select ol.OutboundLineId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         b.ECLNumber,
         s.Status,
         ol.Quantity,
         pk.Quantity as 'PalletQuantity'
    from OutboundDocument  od (nolock)
    join Issue              i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    join OutboundLine      ol (nolock) on od.OutboundDocumentId = ol.OutboundDocumentId
    join StorageUnit       su (nolock) on ol.StorageUnitId      = su.StorageUnitId
    join Status             s (nolock) on i.StatusId            = s.StatusId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on ol.BatchId            = b.BatchId
    join StorageUnitBatch sub (nolock) on su.StorageUnitId      = sub.StorageUnitId
                                         and b.BatchId          = sub.BatchId
    left
    join Pack                pk  (nolock) on su.StorageUnitId     = pk.StorageUnitId
                                         and pk.WarehouseId       = od.WarehouseId
                                         and pk.PackTypeId in (select pt.PackTypeId from PackType pt (nolock) where pt.OutboundSequence = 1)
  where od.OutboundDocumentTypeId = @OutboundDocumentTypeId
    and s.StatusCode = 'W'
    --and sub.StorageUnitBatchId   = @StorageUnitBatchId
end
