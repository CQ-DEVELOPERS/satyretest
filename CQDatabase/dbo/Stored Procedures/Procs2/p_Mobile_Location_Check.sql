﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Location_Check
  ///   Filename       : p_Mobile_Location_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Mar 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Location_Check
(
 @InstructionId     int,
 @confirmedQuantity float = null,
 @jobId             int = null output,
 @SelectValue       bit = 1
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @WarehouseId        int,
          @OperatorId         int,
          @PickLocationId     int,
          @StorageUnitBatchId int,
          @StorageUnitId      int,
          @ReferenceNumber    nvarchar(30),
          --@JobId              int,
          @AreaCode           nvarchar(10),
          @ActualQuantity     float
  
  select @WarehouseId        = WarehouseId,
         @OperatorId         = OperatorId,
         @PickLocationId     = PickLocationId,
         @StorageUnitBatchId = StorageUnitBatchId
    from Instruction
   where InstructionId = @InstructionId
  
  set @JobId = 0
  
  if exists(select 1
              from Location      l (nolock)
              join AreaLocation al (nolock) on l.LocationId = al.LocationId
              join Area          a (nolock) on al.AreaId    = a.AreaId
             where l.LocationId = @PickLocationId
               and (a.Area like '%Mezzanine%' or a.Area in ('Freezer','Fridge')))
  begin
    select @JobId
    return @JobId
  end
  
  begin transaction
  
--  if @CreateStockTake = 1
  begin
--    if exists(select top 1 1 from StorageUnitBatchLocation where StorageUnitBatchId = @StorageUnitBatchId and LocationId = @PickLocationId and ActualQuantity > 0)
    begin
      select @ReferenceNumber = 'Mixed Pick - InsId' + convert(nvarchar(10), @instructionId)
      
      set @JobId = null
      
      select top 1 @JobId = i.JobId
        from Instruction      i
        join InstructionType it on i.InstructionTypeId = it.InstructionTypeId
        join Status           s on i.StatusId          = s.StatusId
        join Job              j on i.JobId             = j.JobId
       where i.PickLocationId = @PickLocationId
         and it.InstructionTypeCode in ('STE','STL','STA','STP')
         and s.StatusCode = 'W'
         and isnull(j.OperatorId, @OperatorId) = @OperatorId
      
      --if (select StocktakeInd from Location where LocationId = @PickLocationId) = 0
      begin
        --if @JobId is null or @JobId = 0
        --begin
        --  exec @Error = p_Housekeeping_Stock_Take_Create_Job
        --   @warehouseId         = @warehouseId,
        --   @operatorId          = @operatorId,
        --   @instructionTypeCode = 'STE',
        --   @jobId               = @JobId output,
        --   @referenceNumber     = @ReferenceNumber
          
        --  if @Error <> 0
        --    goto error
          
        --  update Location set StocktakeInd = 0 where LocationId = @PickLocationId and StocktakeInd = 1
        --end
        
        --if @JobId is null
        --  set @JobId = 0
        --else
        --begin
          exec @Error = p_Housekeeping_Stock_Take_Create_Product_Mobile
           @warehouseId        = @WarehouseId,
           @operatorId         = @OperatorId,
           @jobId              = @JobId output,
           @locationId         = @PickLocationId,
           @storageUnitBatchId = @StorageUnitBatchId,
           @confirmedQuantity  = @confirmedQuantity
          
          if @Error <> 0
            goto error
          
          select @AreaCode = a.AreaCode
            from Area          a (nolock)
            join AreaLocation al (nolock) on a.AreaId = al.AreaId
           where al.LocationId = @PickLocationId
             and a.Replenish   = 1
          
          if @AreaCode = 'PK'
          begin
            select @StorageUnitId = StorageUnitId
              from StorageUnitBatch (nolock)
             where StorageUnitBatchId = @StorageUnitBatchId
            
            select @ActualQuantity = sum(subl.ActualQuantity)
              from StorageUnitBatchLocation subl
              join StorageUnitBatch          sub on subl.StorageUnitBatchId = sub.StorageUnitBatchId
                                                and sub.StorageUnitId       = @StorageUnitId
              where subl.LocationId = @PickLocationId
            
            if isnull(@ActualQuantity,0) = 0
            begin
              exec @Error = p_Mobile_Request_Replenishment @InstructionId = @InstructionId, @ShowMsg = 0
            end
          --end
        end
      end
    end
  end
  commit transaction
  
  if @SelectValue = 1
    select @JobId
  return @JobId
  
  error:
    set @Error = -1
    rollback transaction
    
    if @SelectValue = 1
      select @Error
    return @Error
end

