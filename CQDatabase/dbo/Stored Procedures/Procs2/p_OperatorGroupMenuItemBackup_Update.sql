﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupMenuItemBackup_Update
  ///   Filename       : p_OperatorGroupMenuItemBackup_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:22
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OperatorGroupMenuItemBackup table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @MenuItemId int = null,
  ///   @Access bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupMenuItemBackup_Update
(
 @OperatorGroupId int = null,
 @MenuItemId int = null,
 @Access bit = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update OperatorGroupMenuItemBackup
     set OperatorGroupId = isnull(@OperatorGroupId, OperatorGroupId),
         MenuItemId = isnull(@MenuItemId, MenuItemId),
         Access = isnull(@Access, Access) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
