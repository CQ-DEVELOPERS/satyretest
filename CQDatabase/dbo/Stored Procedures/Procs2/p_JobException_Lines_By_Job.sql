﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_JobException_Lines_By_Job
  ///   Filename       : p_JobException_Lines_By_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_JobException_Lines_By_Job
(
 @JobId               int,
 @InstructionTypeCode nvarchar(10)
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   DocumentNumber      nvarchar(30),
   OrderNumber         nvarchar(30),
   IssueId             int,
   IssueLineId         int,
   JobId               int,
   InstructionId       int,
   InstructionType     nvarchar(30),
   StorageUnitBatchId  int,
   ProductCode         nvarchar(30),
   Product             nvarchar(50),
   SKUCode             nvarchar(50),
   SKU                 nvarchar(50),
   Area                nvarchar(50),
   PickLocationId      int,
   PickLocation        nvarchar(15),
   Quantity            float,
   ConfirmedQuantity   float,
   CheckQuantity       float,
   StatusId            int,
   Status              nvarchar(50),
   OperatorId          int,
   Operator            nvarchar(50)
  );
  
  insert @TableResult
        (IssueLineId,
         JobId,
         InstructionId,
         InstructionType,
         StorageUnitBatchId,
         PickLocationId,
         Quantity,
         ConfirmedQuantity,
         StatusId,
         OperatorId,
         CheckQuantity)
  select isnull(i.IssueLineId, ili.IssueLineId),
         i.JobId,
         i.InstructionId,
         it.InstructionType,
         i.StorageUnitBatchId,
         i.PickLocationId,
         ili.Quantity,
         i.ConfirmedQuantity,
         i.StatusId,
         i.OperatorId,
         i.CheckQuantity
    from Job              j (nolock)
    join Instruction      i (nolock) on j.JobId             = i.JobId
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    left outer
    join IssueLineInstruction ili (nolock) on i.Instructionid = ili.InstructionId
   where it.InstructionTypeCode in ('P','PM','PS','FM','PR')-- @InstructionTypeCode
     and j.JobId                 = @JobId
  
  update tr
     set OrderNumber = od.OrderNumber,
         IssueId     = i.IssueId
    from @TableResult           tr
    join IssueLine              il (nolock) on tr.IssueLineId = il.IssueLineId
    join Issue                   i (nolock) on il.IssueId  = i.IssueId
    join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
  
  update tr
     set DocumentNumber = convert(nvarchar(30), osi.OutboundShipmentId)
    from @TableResult           tr
    join OutboundShipmentIssue osi (nolock) on tr.IssueId = osi.IssueId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SkuCode,
         SKU         = sku.SKU
    from @TableResult tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s (nolock) on tr.StatusId = s.StatusId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set PickLocation = l.Location,
         Area         = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.PickLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId      = al.LocationId
    join Area          a (nolock) on al.AreaId         = a.AreaId
  
  select JobId,
         InstructionId,
         InstructionType,
         DocumentNumber,
         OrderNumber,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         Quantity,
         ConfirmedQuantity,
         CheckQuantity,
         Status,
         Operator,
         Area,
         PickLocation
    from @TableResult
end
