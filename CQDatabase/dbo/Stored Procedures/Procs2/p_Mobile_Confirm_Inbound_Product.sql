﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Confirm_Inbound_Product
  ///   Filename       : p_Mobile_Confirm_Inbound_Product.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 2017-11-03
  ///   Details        : SAT01 - Do not allow for a blank location 
  /// </newpara>
*/
CREATE procedure p_Mobile_Confirm_Inbound_Product
(
 @receiptId int,
 @barcode   nvarchar(30)
)

as
begin
	 set nocount on;
  
  declare @Error         int,
          @ReceiptLineId int,
          @StatusCode    nvarchar(10),
          @RequiredQuantity   numeric(13,6),
          @ReceivedQuantity   numeric(13,6),
          @AcceptedQuantity   numeric(13,6),
          @InboundLineId      int,
          @StatusId           int,
          @OldSUB             int,
          @StorageUnitId      int,
          @StorageUnitBatchId int,
          @WarehouseId        int,
          @InboundDocumentTypeCode nvarchar(10),
          @rowcount           int
  
  set @Error = 0
  
  if @barcode = ''													-- SAT01
	set @barcode = null												-- SAT01

  select top 1
         @ReceiptLineId      = rl.ReceiptLineId,
         @StatusCode         = s.StatusCode,
         @InboundLineId      = il.InboundLineId,
         @StorageUnitId      = su.StorageUnitId,
         @StorageUnitBatchId = rl.StorageUnitBatchId,
         @ReceivedQuantity   = rl.ReceivedQuantity,
         @WarehouseId        = r.WarehouseId,
         @InboundDocumentTypeCode = idt.InboundDocumentTypeCode
    from ReceiptLine       rl (nolock)
    join Receipt            r (nolock) on rl.ReceiptId          = r.ReceiptId
    join InboundDocument   id (nolock) on r.InboundDocumentId   = id.InboundDocumentId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    join InboundLine       il (nolock) on rl.InboundLineId      = il.InboundLineId
    join Status             s (nolock) on rl.StatusId           = s.StatusId
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
   where rl.ReceiptId = @receiptId
     and sku.SKUCode   = isnull(@Barcode,sku.SKUCode)
  order by isnull(s.OrderBy,99), rl.ReceiptLineId desc
  
  select @rowcount = @@ROWCOUNT 
  
  if @rowcount = 0
    select top 1
           @ReceiptLineId      = rl.ReceiptLineId,
           @StatusCode         = s.StatusCode,
           @InboundLineId      = il.InboundLineId,
           @StorageUnitId      = su.StorageUnitId,
           @StorageUnitBatchId = rl.StorageUnitBatchId,
           @ReceivedQuantity   = rl.ReceivedQuantity,
           @WarehouseId        = r.WarehouseId,
           @InboundDocumentTypeCode = idt.InboundDocumentTypeCode
      from ReceiptLine       rl (nolock)
      join Receipt            r (nolock) on rl.ReceiptId          = r.ReceiptId
      join InboundDocument   id (nolock) on r.InboundDocumentId   = id.InboundDocumentId
      join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
      join InboundLine       il (nolock) on rl.InboundLineId      = il.InboundLineId
      join Status             s (nolock) on rl.StatusId           = s.StatusId
      join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
      join Batch              b (nolock) on sub.BatchId           = b.BatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Pack              pk (nolock) on su.StorageUnitId      = pk.StorageUnitId
      join Product            p (nolock) on su.ProductId          = p.ProductId
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId
     where rl.ReceiptId = @receiptId
       and (p.ProductCode = @Barcode
        or  sku.SKUCode   = @Barcode
        or  p.Barcode     = @Barcode
        or  pk.Barcode    = @Barcode)
    order by isnull(s.OrderBy,99), case when rl.RequiredQuantity - rl.ReceivedQuantity > 0 then 0 else 1 end, rl.ReceiptLineId desc
  
  select @rowcount = @@ROWCOUNT 
  
  if dbo.ufn_Configuration(422, @WarehouseId) = 0
  if @StatusCode = 'R'
  begin
    select @ReceivedQuantity   = isnull(rl.AcceptedQuantity,0) + isnull(rl.RejectQuantity,0),
           @RequiredQuantity   = rl.RequiredQuantity,
           @AcceptedQuantity   = rl.AcceptedQuantity
      from ReceiptLine rl
     where ReceiptLineId = @ReceiptLineId
    
    if (select (convert(numeric(13,3),sum(ReceivedQuantity) / convert(numeric(13,3),sum(RequiredQuantity))) * 100)
          from ReceiptLine       rl (nolock)
          join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
          join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
          join Product            p (nolock) on su.ProductId = p.ProductId
         where rl.ReceiptId = @ReceiptId
           and sub.StorageUnitId = @StorageUnitId
           and rl.InboundLineId = @inboundLineId) <= 0
    begin
      set @Error = -1
      set @ReceiptLineId = -1
      goto result
    end
    
    --if @RequiredQuantity > @ReceivedQuantity
    if @ReceivedQuantity > 0 -- GS - added 2013-01-07 - if line not yet recevied (zero) overwrite
    begin
      exec @Error = p_ReceiptLine_Update
       @ReceiptLineId        = @ReceiptLineId,
       @RequiredQuantity     = @AcceptedQuantity
      
      if @Error <> 0
      begin
        set @Error = -1
        goto result
      end
      
      select @StatusId = dbo.ufn_StatusId('R','W')
      
      select @RequiredQuantity = SUM(il.Quantity)
        from InboundLine il (nolock)
       where il.InboundLineId = @InboundLineId
      
      select @RequiredQuantity = isnull(@RequiredQuantity, 0) - SUM(rl.RequiredQuantity)
        from ReceiptLine rl (nolock)
       where rl.InboundLineId = @InboundLineId
      
      if @RequiredQuantity <= 0
        set @RequiredQuantity = @RequiredQuantity - @ReceivedQuantity
      
      if @RequiredQuantity < 0
        set @RequiredQuantity = 0
      
      if @InboundDocumentTypeCode != 'BOND'
      begin
        select @OldSUB = StorageUnitBatchId
          from StorageUnitBatch sub
          join Batch              b on sub.BatchId = b.BatchId
         where StorageUnitId = @StorageUnitId
           and Batch         = 'Default'
      end
      
      if @OldSUB is null
         set @OldSUB = @StorageUnitBatchId
      
      exec @Error = p_ReceiptLine_Insert
       @ReceiptLineId        = @receiptLineId output,
       @ReceiptId            = @ReceiptId,
       @InboundLineId        = @InboundLineId,
       @StorageUnitBatchId   = @OldSUB,
       @StatusId             = @StatusId,
       @RequiredQuantity     = @RequiredQuantity,
       @ReceivedQuantity     = null,
       @AcceptedQuantity     = null,
       @RejectQuantity       = null
       --@DeliveryNoteQuantity = @RequiredQuantity
      
      if @Error <> 0
      begin
        set @Error = -1
        goto result
      end
    end
  end
  if @barcode is null												-- SAT01
	set @ReceiptLineId = -1											-- SAT01

  if @ReceiptLineId is null
  begin
    set @ReceiptLineId = -1
    goto result
  end
  else
  begin
    set @Error = -1
    goto result
  end
  
  result:
    select @ReceiptLineId as '@ReceiptLineId'
    return @ReceiptLineId
end
