﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletiseStock_Update
  ///   Filename       : p_PalletiseStock_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:44
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the PalletiseStock table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null,
  ///   @DropSequence int = null,
  ///   @AreaSequence int = null,
  ///   @JobId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StorageUnitId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @Quantity float = null,
  ///   @OriginalQuantity float = null,
  ///   @PalletQuantity float = null,
  ///   @UnitWeight float = null,
  ///   @UnitVolume numeric(13,6) = null,
  ///   @ExternalCompanyId int = null,
  ///   @LineNumber int = null,
  ///   @BoxQuantity float = null,
  ///   @UnitQuantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletiseStock_Update
(
 @OutboundShipmentId int = null,
 @IssueId int = null,
 @IssueLineId int = null,
 @DropSequence int = null,
 @AreaSequence int = null,
 @JobId int = null,
 @StorageUnitBatchId int = null,
 @StorageUnitId int = null,
 @ProductCode nvarchar(60) = null,
 @SKUCode nvarchar(20) = null,
 @Quantity float = null,
 @OriginalQuantity float = null,
 @PalletQuantity float = null,
 @UnitWeight float = null,
 @UnitVolume numeric(13,6) = null,
 @ExternalCompanyId int = null,
 @LineNumber int = null,
 @BoxQuantity float = null,
 @UnitQuantity float = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update PalletiseStock
     set OutboundShipmentId = isnull(@OutboundShipmentId, OutboundShipmentId),
         IssueId = isnull(@IssueId, IssueId),
         IssueLineId = isnull(@IssueLineId, IssueLineId),
         DropSequence = isnull(@DropSequence, DropSequence),
         AreaSequence = isnull(@AreaSequence, AreaSequence),
         JobId = isnull(@JobId, JobId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         ProductCode = isnull(@ProductCode, ProductCode),
         SKUCode = isnull(@SKUCode, SKUCode),
         Quantity = isnull(@Quantity, Quantity),
         OriginalQuantity = isnull(@OriginalQuantity, OriginalQuantity),
         PalletQuantity = isnull(@PalletQuantity, PalletQuantity),
         UnitWeight = isnull(@UnitWeight, UnitWeight),
         UnitVolume = isnull(@UnitVolume, UnitVolume),
         ExternalCompanyId = isnull(@ExternalCompanyId, ExternalCompanyId),
         LineNumber = isnull(@LineNumber, LineNumber),
         BoxQuantity = isnull(@BoxQuantity, BoxQuantity),
         UnitQuantity = isnull(@UnitQuantity, UnitQuantity) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
