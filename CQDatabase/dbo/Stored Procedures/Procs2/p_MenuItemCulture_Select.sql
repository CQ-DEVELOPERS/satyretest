﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItemCulture_Select
  ///   Filename       : p_MenuItemCulture_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:34
  /// </summary>
  /// <remarks>
  ///   Selects rows from the MenuItemCulture table.
  /// </remarks>
  /// <param>
  ///   @MenuItemCultureId int = null 
  /// </param>
  /// <returns>
  ///   MenuItemCulture.MenuItemCultureId,
  ///   MenuItemCulture.MenuItemId,
  ///   MenuItemCulture.CultureId,
  ///   MenuItemCulture.MenuItem,
  ///   MenuItemCulture.ToolTip 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItemCulture_Select
(
 @MenuItemCultureId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         MenuItemCulture.MenuItemCultureId
        ,MenuItemCulture.MenuItemId
        ,MenuItemCulture.CultureId
        ,MenuItemCulture.MenuItem
        ,MenuItemCulture.ToolTip
    from MenuItemCulture
   where isnull(MenuItemCulture.MenuItemCultureId,'0')  = isnull(@MenuItemCultureId, isnull(MenuItemCulture.MenuItemCultureId,'0'))
  
end
