﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MovementSequence_Select
  ///   Filename       : p_MovementSequence_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:03
  /// </summary>
  /// <remarks>
  ///   Selects rows from the MovementSequence table.
  /// </remarks>
  /// <param>
  ///   @MovementSequenceId int = null 
  /// </param>
  /// <returns>
  ///   MovementSequence.MovementSequenceId,
  ///   MovementSequence.PickAreaId,
  ///   MovementSequence.StoreAreaId,
  ///   MovementSequence.StageAreaId,
  ///   MovementSequence.OperatorGroupId,
  ///   MovementSequence.StatusCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MovementSequence_Select
(
 @MovementSequenceId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         MovementSequence.MovementSequenceId
        ,MovementSequence.PickAreaId
        ,MovementSequence.StoreAreaId
        ,MovementSequence.StageAreaId
        ,MovementSequence.OperatorGroupId
        ,MovementSequence.StatusCode
    from MovementSequence
   where isnull(MovementSequence.MovementSequenceId,'0')  = isnull(@MovementSequenceId, isnull(MovementSequence.MovementSequenceId,'0'))
  
end
