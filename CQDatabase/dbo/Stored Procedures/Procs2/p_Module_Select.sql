﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Module_Select
  ///   Filename       : p_Module_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:49
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Module table.
  /// </remarks>
  /// <param>
  ///   @ModuleId int = null 
  /// </param>
  /// <returns>
  ///   Module.ModuleId,
  ///   Module.Module 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Module_Select
(
 @ModuleId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Module.ModuleId
        ,Module.Module
    from Module
   where isnull(Module.ModuleId,'0')  = isnull(@ModuleId, isnull(Module.ModuleId,'0'))
  
end
