﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Store_Mixed_Next
  ///   Filename       : p_Mobile_Store_Mixed_Next.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Store_Mixed_Next
(
 @operatorId    int,
 @instructionId int output,
 @barcode       nvarchar(50)
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @JobId             int,
          @PalletId          int,
          @OldInstructionId  int
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @OldInstructionId = @instructionId
  
  if isnumeric(replace(@barcode,'J:','')) = 1
    select @JobId   = replace(@barcode,'J:',''),
           @barcode = null
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  select @JobId = JobId
    from Instruction (nolock)
   where InstructionId = @instructionId
  
  set @instructionId = null
  
  if @PalletId is not null
    select @instructionId = InstructionId
      from Instruction (nolock)
     where PalletId = @PalletId
       and JobId    = @JobId
  
  if @barcode is not null
  begin
    select top 1
           @instructionId = i.InstructionId
      from Instruction        i (nolock)
      join Status             s (nolock) on i.StatusId           = s.StatusId
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join Product            p (nolock) on su.ProductId         = p.ProductId
     where s.Type         = 'I'
       and s.StatusCode  in ('W','S')
       and i.JobId        = @JobId
       and p.Barcode      = @barcode
       and InstructionId != @OldInstructionId
    
    if @instructionId is null
      select top 1
             @instructionId = i.InstructionId
        from Instruction        i (nolock)
        join Status             s (nolock) on i.StatusId           = s.StatusId
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
        join Pack               p (nolock) on sub.StorageUnitId    = p.StorageUnitId
     where s.Type        = 'I'
       and s.StatusCode in ('W','S')
       and i.JobId        = @JobId
       and p.Barcode     = @barcode
       and InstructionId != @OldInstructionId
  end
  
  begin transaction
  
  if @instructionId is null
  begin
    set @InstructionId = -1
    set @Error = 2
    goto error
  end
  
  select @StatusId = dbo.ufn_StatusId('I','S')
  
  exec @Error = p_Instruction_Started
   @InstructionId = @InstructionId,
   @OperatorId    = @OperatorId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    rollback transaction
    return @Error
end
