﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pack_Parameter
  ///   Filename       : p_Pack_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Nov 2012 14:41:26
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Pack table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Pack.PackId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pack_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as PackId
        ,null as 'Pack'
        ,null as 'FromPackId'
        ,null as 'ToPackId'
  union
  select
         top 10 Pack.PackId
        ,Pack.PackId as 'Pack'
        ,Pack.PackId as 'FromPackId'
        ,Pack.PackId as 'ToPackId'
    from Pack
  
end
