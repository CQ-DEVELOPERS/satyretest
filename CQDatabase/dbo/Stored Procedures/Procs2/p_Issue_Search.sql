﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Issue_Search
  ///   Filename       : p_Issue_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2015 09:30:36
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Issue table.
  /// </remarks>
  /// <param>
  ///   @IssueId int = null output,
  ///   @OutboundDocumentId int = null,
  ///   @PriorityId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @RouteId int = null,
  ///   @DespatchBay int = null,
  ///   @ContactListId int = null,
  ///   @ManufacturingId int = null,
  ///   @WaveId int = null,
  ///   @PlannedBy int = null,
  ///   @ReleasedBy int = null,
  ///   @VehicleId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Issue.IssueId,
  ///   Issue.OutboundDocumentId,
  ///   OutboundDocument.OutboundDocumentId,
  ///   Issue.PriorityId,
  ///   Priority.Priority,
  ///   Issue.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   Issue.LocationId,
  ///   Location.Location,
  ///   Issue.StatusId,
  ///   Status.Status,
  ///   Issue.OperatorId,
  ///   Operator.Operator,
  ///   Issue.RouteId,
  ///   Route.Route,
  ///   Issue.DeliveryNoteNumber,
  ///   Issue.DeliveryDate,
  ///   Issue.SealNumber,
  ///   Issue.VehicleRegistration,
  ///   Issue.Remarks,
  ///   Issue.DropSequence,
  ///   Issue.LoadIndicator,
  ///   Issue.DespatchBay,
  ///   Location.Location,
  ///   Issue.RoutingSystem,
  ///   Issue.Delivery,
  ///   Issue.NumberOfLines,
  ///   Issue.Total,
  ///   Issue.Complete,
  ///   Issue.AreaType,
  ///   Issue.Units,
  ///   Issue.ShortPicks,
  ///   Issue.Releases,
  ///   Issue.Weight,
  ///   Issue.ConfirmedWeight,
  ///   Issue.FirstLoaded,
  ///   Issue.Interfaced,
  ///   Issue.Loaded,
  ///   Issue.Pallets,
  ///   Issue.Volume,
  ///   Issue.ContactListId,
  ///   ContactList.ContactListId,
  ///   Issue.ManufacturingId,
  ///   Location.Location,
  ///   Issue.ReservedId,
  ///   Issue.ParentIssueId,
  ///   Issue.ASN,
  ///   Issue.WaveId,
  ///   Wave.Wave,
  ///   Issue.ReserveBatch,
  ///   Issue.InvoiceNumber,
  ///   Issue.TotalDue,
  ///   Issue.AddressLine1,
  ///   Issue.AddressLine2,
  ///   Issue.AddressLine3,
  ///   Issue.AddressLine4,
  ///   Issue.AddressLine5,
  ///   Issue.PlanningComplete,
  ///   Issue.PlannedBy,
  ///   Operator.Operator,
  ///   Issue.Release,
  ///   Issue.ReleasedBy,
  ///   Operator.Operator,
  ///   Issue.Checking,
  ///   Issue.Checked,
  ///   Issue.Despatch,
  ///   Issue.DespatchChecked,
  ///   Issue.CompleteDate,
  ///   Issue.Availability,
  ///   Issue.CheckingCount,
  ///   Issue.Branding,
  ///   Issue.Downsizing,
  ///   Issue.CheckSheetPrinted,
  ///   Issue.CreateDate,
  ///   Issue.VehicleId 
  ///   Vehicle.VehicleId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Issue_Search
(
 @IssueId int = null output,
 @OutboundDocumentId int = null,
 @PriorityId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @RouteId int = null,
 @DespatchBay int = null,
 @ContactListId int = null,
 @ManufacturingId int = null,
 @WaveId int = null,
 @PlannedBy int = null,
 @ReleasedBy int = null,
 @VehicleId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
  if @OutboundDocumentId = '-1'
    set @OutboundDocumentId = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @RouteId = '-1'
    set @RouteId = null;
  
  if @DespatchBay = '-1'
    set @DespatchBay = null;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @ManufacturingId = '-1'
    set @ManufacturingId = null;
  
  if @WaveId = '-1'
    set @WaveId = null;
  
  if @PlannedBy = '-1'
    set @PlannedBy = null;
  
  if @ReleasedBy = '-1'
    set @ReleasedBy = null;
  
  if @VehicleId = '-1'
    set @VehicleId = null;
  
 
  select
         Issue.IssueId
        ,Issue.OutboundDocumentId
        ,Issue.PriorityId
         ,PriorityPriorityId.Priority as 'Priority'
        ,Issue.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,Issue.LocationId
         ,LocationLocationId.Location as 'Location'
        ,Issue.StatusId
         ,StatusStatusId.Status as 'Status'
        ,Issue.OperatorId
         ,OperatorOperatorId.Operator as 'Operator'
        ,Issue.RouteId
         ,RouteRouteId.Route as 'Route'
        ,Issue.DeliveryNoteNumber
        ,Issue.DeliveryDate
        ,Issue.SealNumber
        ,Issue.VehicleRegistration
        ,Issue.Remarks
        ,Issue.DropSequence
        ,Issue.LoadIndicator
        ,Issue.DespatchBay
         ,LocationDespatchBay.Location as 'LocationDespatchBay'
        ,Issue.RoutingSystem
        ,Issue.Delivery
        ,Issue.NumberOfLines
        ,Issue.Total
        ,Issue.Complete
        ,Issue.AreaType
        ,Issue.Units
        ,Issue.ShortPicks
        ,Issue.Releases
        ,Issue.Weight
        ,Issue.ConfirmedWeight
        ,Issue.FirstLoaded
        ,Issue.Interfaced
        ,Issue.Loaded
        ,Issue.Pallets
        ,Issue.Volume
        ,Issue.ContactListId
        ,Issue.ManufacturingId
         ,LocationManufacturingId.Location as 'LocationManufacturingId'
        ,Issue.ReservedId
        ,Issue.ParentIssueId
        ,Issue.ASN
        ,Issue.WaveId
         ,WaveWaveId.Wave as 'Wave'
        ,Issue.ReserveBatch
        ,Issue.InvoiceNumber
        ,Issue.TotalDue
        ,Issue.AddressLine1
        ,Issue.AddressLine2
        ,Issue.AddressLine3
        ,Issue.AddressLine4
        ,Issue.AddressLine5
        ,Issue.PlanningComplete
        ,Issue.PlannedBy
         ,OperatorPlannedBy.Operator as 'OperatorPlannedBy'
        ,Issue.Release
        ,Issue.ReleasedBy
         ,OperatorReleasedBy.Operator as 'OperatorReleasedBy'
        ,Issue.Checking
        ,Issue.Checked
        ,Issue.Despatch
        ,Issue.DespatchChecked
        ,Issue.CompleteDate
        ,Issue.Availability
        ,Issue.CheckingCount
        ,Issue.Branding
        ,Issue.Downsizing
        ,Issue.CheckSheetPrinted
        ,Issue.CreateDate
        ,Issue.VehicleId
    from Issue
    left
    join OutboundDocument OutboundDocumentOutboundDocumentId on OutboundDocumentOutboundDocumentId.OutboundDocumentId = Issue.OutboundDocumentId
    left
    join Priority PriorityPriorityId on PriorityPriorityId.PriorityId = Issue.PriorityId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = Issue.WarehouseId
    left
    join Location LocationLocationId on LocationLocationId.LocationId = Issue.LocationId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = Issue.StatusId
    left
    join Operator OperatorOperatorId on OperatorOperatorId.OperatorId = Issue.OperatorId
    left
    join Route RouteRouteId on RouteRouteId.RouteId = Issue.RouteId
    left
    join Location LocationDespatchBay on LocationDespatchBay.LocationId = Issue.DespatchBay
    left
    join ContactList ContactListContactListId on ContactListContactListId.ContactListId = Issue.ContactListId
    left
    join Location LocationManufacturingId on LocationManufacturingId.LocationId = Issue.ManufacturingId
    left
    join Wave WaveWaveId on WaveWaveId.WaveId = Issue.WaveId
    left
    join Operator OperatorPlannedBy on OperatorPlannedBy.OperatorId = Issue.PlannedBy
    left
    join Operator OperatorReleasedBy on OperatorReleasedBy.OperatorId = Issue.ReleasedBy
    left
    join Vehicle VehicleVehicleId on VehicleVehicleId.VehicleId = Issue.VehicleId
   where isnull(Issue.IssueId,'0')  = isnull(@IssueId, isnull(Issue.IssueId,'0'))
     and isnull(Issue.OutboundDocumentId,'0')  = isnull(@OutboundDocumentId, isnull(Issue.OutboundDocumentId,'0'))
     and isnull(Issue.PriorityId,'0')  = isnull(@PriorityId, isnull(Issue.PriorityId,'0'))
     and isnull(Issue.WarehouseId,'0')  = isnull(@WarehouseId, isnull(Issue.WarehouseId,'0'))
     and isnull(Issue.LocationId,'0')  = isnull(@LocationId, isnull(Issue.LocationId,'0'))
     and isnull(Issue.StatusId,'0')  = isnull(@StatusId, isnull(Issue.StatusId,'0'))
     and isnull(Issue.OperatorId,'0')  = isnull(@OperatorId, isnull(Issue.OperatorId,'0'))
     and isnull(Issue.RouteId,'0')  = isnull(@RouteId, isnull(Issue.RouteId,'0'))
     and isnull(Issue.DespatchBay,'0')  = isnull(@DespatchBay, isnull(Issue.DespatchBay,'0'))
     and isnull(Issue.ContactListId,'0')  = isnull(@ContactListId, isnull(Issue.ContactListId,'0'))
     and isnull(Issue.ManufacturingId,'0')  = isnull(@ManufacturingId, isnull(Issue.ManufacturingId,'0'))
     and isnull(Issue.WaveId,'0')  = isnull(@WaveId, isnull(Issue.WaveId,'0'))
     and isnull(Issue.PlannedBy,'0')  = isnull(@PlannedBy, isnull(Issue.PlannedBy,'0'))
     and isnull(Issue.ReleasedBy,'0')  = isnull(@ReleasedBy, isnull(Issue.ReleasedBy,'0'))
     and isnull(Issue.VehicleId,'0')  = isnull(@VehicleId, isnull(Issue.VehicleId,'0'))
  order by DeliveryNoteNumber
  
end
