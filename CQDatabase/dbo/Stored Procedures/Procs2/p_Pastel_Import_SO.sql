﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pastel_Import_SO
  ///   Filename       : p_Pastel_Import_SO.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/  
CREATE procedure [dbo].[p_Pastel_Import_SO]
as
begin
  -- InterfaceImportSOHeader
  declare @InterfaceImportSOHeaderId    int,
          @OrderNumber                  varchar(30),
          @ExternalCompanyCode          varchar(30),
          @DeliveryDate                 datetime,
          @PreSchedule                  varchar(255),
          @AmountOfInvoice              varchar(255),
          @COD                          varchar(255),
          @PickUpCustomer               varchar(255),
          @LoadPriority                 varchar(255),
          @JobWindowStart               varchar(255),
          @JobWindowEnd                 varchar(255),
          @JobDuration                  varchar(255),
          @HoldStatus                   varchar(255),
          @Remarks                      varchar(255),
          @Instructions                 varchar(255),
          @NumberOfLines                int,
          -- InterfaceImportSODetail
          @LineNumber                   int,
          @ProductCode                  varchar(50),
          @Quantity                     float,
          @WhsCode                      varchar(255),
          @PickUp                       varchar(255),
          -- Internal variables
          @ExternalCompanyId            int,
          @ExternalCompany              varchar(255),
          @DropSequence                 int,
          @Weight                       numeric(13,3),
          @OutboundDocumentTypeCode     varchar(10),
          @PriorityId                   int,
          @ErrorMsg                     varchar(100),
          @Error                        int,
          @OutboundDocumentTypeId       int,
          @OutboundDocumentId           int,
          @StatusId                     int,
          @GetDate                      datetime,
          @ProductId                    int,
          @StorageUnitId                int,
          @StorageUnitBatchId           int,
          @OutboundLineId               int,
          @Exception                    varchar(255),
          @Delete                       bit,
          @OutboundShipmentId           int,
          @IssueId                      int,
          @WarehouseId                  int,
          @RecordType                   varchar(30),
          @Product                      varchar(255),
          @AreaType                     varchar(10)  ,
          @FromWarehouseCode            varchar(50),
          @FromWarehouseId              int,
          @ToWarehouseId                int,
          @ToWarehouseCode              varchar(50),
          @InterfaceMessage             varchar(255),
          @RouteId                      int,
          @Route                        nvarchar(50),
          @RouteCode                    nvarchar(10),
          @PQDocumentTypeCode           nvarchar(10),
          @InterfaceTableId             int,
          @InterfaceId                  int
      
      
  
  select @GetDate = dbo.ufn_Getdate()
  
  -- Leave null if does not exist
  select @PQDocumentTypeCode = OutboundDocumentTypeCode
    from OutboundDocumentType (nolock)
   where OutboundDocumentTypeCode = 'PRQ'
  
  select @InterfaceTableId = InterfaceTableId
    from InterfaceTables (nolock)
   where HeaderTable = 'InterfaceImportSOHeader'
  
  update d
     set InterfaceImportSOHeaderId = (select max(InterfaceImportSOHeaderId)
    from InterfaceImportSOHeader h
   where d.ForeignKey = h.PrimaryKey)
    from InterfaceImportSODetail d
   where InterfaceImportSOHeaderId is null
  
  update InterfaceImportSOHeader
     set RecordType = 'SAL'
   where RecordType is null
     and ToWarehouseCode is null
  
  update InterfaceImportSOHeader
     set RecordType = 'SAL'
   where RecordType not in (select OutboundDocumentTypeCode from OutboundDocumentType)
     and RecordType not in (select OutboundDocumentType from OutboundDocumentType)
     and ToWarehouseCode is null
  
  update InterfaceImportSOHeader
     set RecordType = 'IBT'
   where ToWarehouseCode is not null
  
  -- Reprocess Order which did not process properly
  update InterfaceImportSOHeader
     set ProcessedDate = null
   where RecordStatus     = 'N'
     and ProcessedDate   <= dateadd(mi, -10, @Getdate)
  
  update h
     set ProcessedDate = @Getdate,
         RecordType    = isnull(odt.OutboundDocumentTypeCode, h.RecordType)
    from InterfaceImportSOHeader h
         left
         join OutboundDocumentType odt (nolock) on h.RecordType = odt.OutboundDocumentType
   where exists(select 1 from InterfaceImportSODetail d where h.InterfaceImportSOHeaderId = d.InterfaceImportSOHeaderId)
     and (RecordType in ('SAL','IBT')
       or RecordType in (select OutboundDocumentTypeCode from OutboundDocumentType)
       or RecordType in (select OutboundDocumentType from OutboundDocumentType))
     --and not exists(select 1 from OutboundDocument od where h.OrderNumber = od.OrderNumber)
     and RecordStatus in ('N','U')
     and ProcessedDate is null
     and ToWarehouseCode is null
  
  declare header_cursor                 cursor for
  select InterfaceImportSOHeaderId,
         OrderNumber,   -- OrderNumber
         CustomerCode, -- CustomerCode
         convert(datetime, isnull(DeliveryDate, @getdate), 120), -- DeliveryDate
         Additional2,  -- PreSchedule
         Additional3,  -- AmountOfInvoice
         Additional4,  -- COD
         Additional5,  -- PickUpCustomer
         Additional6,  -- LoadPriority
         Additional7,  -- JobWindowStart
         Additional8,  -- JobWindowEnd
         Additional9,  -- JobDuration
         Additional10, -- HoldStatus
         Remarks,      -- Remarks
         Additional1,  -- Instructions
         NumberOfLines,-- NoOfLines
         RecordType,    -- Collection
         FromWarehouseCode,
         ToWarehouseCode,
         FromWarehouseCode,
         Route
    from InterfaceImportSOHeader h
   where ProcessedDate = @Getdate
  order by OrderNumber
  
  open header_cursor
  
  fetch header_cursor
    into @InterfaceImportSOHeaderId,
         @OrderNumber,
         @ExternalCompanyCode,
         @DeliveryDate,
         @PreSchedule,
         @AmountOfInvoice,
         @COD,
         @PickUpCustomer,
         @LoadPriority,
         @JobWindowStart,
         @JobWindowEnd,
         @JobDuration,
         @HoldStatus,
         @Remarks,
         @Instructions,
         @NumberOfLines,
         @RecordType  ,
         @FromWarehouseCode,
         @ToWarehouseCode,
         @FromWarehouseCode,
         @Route
         
  while (@@fetch_status = 0)
  begin
    if @FromWarehouseCode = '0'
      set @FromWarehouseCode = '1'
    
    If @FromWarehouseCode is null
    Begin
      Select @FromWarehouseId = Additional1 from InterfaceImportSODetail where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
             update InterfaceImportSOHeader set FromWarehouseCode = @FromWarehouseId where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
         Set @FromWarehouseCode = convert(nvarchar(10), @FromWarehouseId)
    End
    
    update InterfaceImportSOHeader
       set RecordStatus = 'C'
     where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
    
    begin transaction xml_import
    
    -- Amplux do not use Number of Lines
    --if (select NumberOfLines
          --      from InterfaceImportSOHeader
          --     where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId)
    --   !=
    --   (select count(1)
          --     from InterfaceImportSODetail
          --    where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId)
    --begin
    --  select @ErrorMsg = 'Error executing Number of line in header and detail are different'
    --  goto error_header
    --end
    
    exec @Error = p_Interface_Outbound_Order_Delete
    @OrderNumber = @OrderNumber
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Interface_Outbound_Order_Delete'
      goto error_header
    end
    
    if @ExternalCompanyCode is null
       set @ExternalCompanyCode = 'None'
    
    set @ExternalCompanyId = null
    
    if @RecordType = 'IBT'
    Begin
      Select @WarehouseId = ParentWarehouseId
        From Warehouse
       where HostId = @FromWarehouseCode
      
      if @WarehouseId is null
      begin
        select @ErrorMsg = 'Could not find Warehouse for IBT:' + @FromWarehouseId
        goto error_header
      end
      
      Select @FromWarehouseCode = WarehouseCode
        From Warehouse
       where WarehouseId= @WarehouseId
      
      if @FromWarehouseCode is null
      begin
        select @ErrorMsg = 'Could not find WarehouseCode for IBT:' + @WarehouseId
        goto error_header
      end
      
      Select @ExternalCompanyId = ExternalCompanyId
        from ExternalCompany (nolock)
       where ExternalCompanyCode = @FromWarehouseCode
         and ExternalCompanytypeId = 2
      
      if @ExternalCompanyId is null
      begin
        select @ErrorMsg = 'Could not find ExternalCompanyId for IBT:' + @FromWarehouseCode
        goto error_header
      end
    End
    Else
    Begin
      if @Route = ''
         set @Route = null
      
      if @Route is not null
      begin
        set @RouteId = null
        select @RouteId = RouteId
          from Route (nolock)
         where Route = @Route
        
        if @RouteId is null
        begin
             set @RouteCode = left(@Route,10)
                 
                 exec @Error = p_Route_Insert
                 @RouteId    = @RouteId output,
                 @Route      = @Route,
                 @RouteCode  = @RouteCode
        end
      end
      
      select @ExternalCompanyId = ExternalCompanyId
        from ExternalCompany
       where HostId = @ExternalCompanyCode
      
      if @ExternalCompanyId is null
      begin
        select @ExternalCompanyId   = ExternalCompanyId,
               @ExternalCompanyCode = ExternalCompanyCode,
               @ExternalCompany     = ExternalCompany
          from ExternalCompany (nolock)
         where ExternalCompanyCode = @ExternalCompanyCode
      End
      
      if @ExternalCompanyId is null
      begin
           set @ExternalCompany = @ExternalCompanyCode
               
               exec @Error = p_ExternalCompany_Insert
               @ExternalCompanyId         = @ExternalCompanyId output,
               @ExternalCompanyTypeId     = 1,
               @ExternalCompany           = @ExternalCompanyCode,
               @ExternalCompanyCode       = @ExternalCompany,
               @RouteId                   = @RouteId
               
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
          goto error_header
        end
      end
      else
      begin
        set @ExternalCompany = @ExternalCompanyCode

        exec @Error = p_ExternalCompany_Update
         @ExternalCompanyId         = @ExternalCompanyId,
         @ExternalCompany           = @ExternalCompanyCode,
         @ExternalCompanyCode       = @ExternalCompany,
         @RouteId                   = @RouteId
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
          goto error_header
        end
      end
    End
    
    set @OutboundDocumentTypeCode = null
           
    select @OutboundDocumentTypeCode = OutboundDocumentTypeCode,
           @AreaType                 = AreaType
      from OutboundDocumentType (nolock)
     where OutboundDocumentTypeCode = @RecordType
    
    if @OutboundDocumentTypeCode is null
       set @OutboundDocumentTypeCode = 'SAL'
           
    select @OutboundDocumentTypeId = OutboundDocumentTypeId,
           @PriorityId             = PriorityId
      from OutboundDocumentType (nolock)
     where OutboundDocumentTypeCode = @OutboundDocumentTypeCode
    
    select @StatusId = dbo.ufn_StatusId('OD','I')
           
    if not exists (select 1
                     from OutboundDocument
                    where OrderNumber = @OrderNumber
                      and OutboundDocumentTypeId = @OutboundDocumentTypeId)
    begin
         set @WarehouseId = null
             
      select @WarehouseId = ParentWarehouseId
        from Warehouse (nolock)
       where HostId = @FromWarehouseCode
      
      if @WarehouseId is null
      select @WarehouseId = ParentWarehouseId
        from Warehouse (nolock)
       where WarehouseCode = @FromWarehouseCode
      
      if @WarehouseId is null
      select @WarehouseId = WarehouseId
        from WarehouseCodeReference (nolock)
       where WarehouseCode = @FromWarehouseCode
         and DownloadType = 'SALE'
      
      if @RecordType = 'IBT'
      Select @WarehouseId = ParentWarehouseId
        From Warehouse
       where HostId = convert(nvarchar(10), @FromWarehouseId)
      
      if @WarehouseId is null
      begin
        select @ErrorMsg = 'Error Could not find warehouse:' + Isnull(@FromWarehouseCode ,'none')
        goto error_header
      end
      
      exec @Error = p_OutboundDocument_Insert
      @OutboundDocumentId     = @OutboundDocumentId output,
      @OrderNumber            = @OrderNumber,
      @OutboundDocumentTypeId = @OutboundDocumentTypeId,
      @ExternalCompanyId      = @ExternalCompanyId,
      @WarehouseId            = @WarehouseId,
      @StatusId               = @StatusId,
      @DeliveryDate           = @DeliveryDate,
      @CreateDate             = @GetDate,
      @ModifiedDate           = null
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_OutboundDocument_Insert'
        goto error_header
      end
    end
    
    declare detail_cursor                 cursor for
    select d.LineNumber,  -- LineNumber
           d.ProductCode, -- ProductCode
           d.Additional1, -- Location
           d.Quantity,    -- Quantity
           d.Additional2  -- PickUp
      from InterfaceImportSODetail d
     where d.InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
    
    open detail_cursor
    
    fetch detail_cursor
      into @LineNumber,
           @ProductCode,
           @WhsCode,
           @Quantity,
           @PickUp
           
    if @@fetch_status != 0
    begin
      select @ErrorMsg = 'Error executing no lines to import'
      goto error_detail
    end
    
    while (@@fetch_status = 0)
    begin
         set @StorageUnitBatchId = null
         set @StorageUnitId = null
         set @Product = null
             
      select @ProductCode = ProductCode,
             @Product     = Product
        from Product
       where HostId = @ProductCode
      
      if @@rowcount = 0
      select @ProductCode = ProductCode,
             @Product     = Product
        from Product
       where ProductCode = @ProductCode
      
      if @Product is null
      begin
        select @ErrorMsg = 'Error executing Product does not exist'
        goto error_detail
      end
      
      exec @Error = p_interface_xml_Product_Insert
      @ProductCode        = @ProductCode,
      @Product            = @Product,
      @WarehouseId        = @WarehouseId,
      @StorageUnitId      = @StorageUnitId output,
      @StorageUnitBatchId = @StorageUnitBatchId output
      
      if @Error != 0 or @StorageUnitBatchId is null
      begin
        select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
        goto error_detail
      end
      
      if exists (select 1
                   from OutboundLine
                  where OutboundDocumentId = @OutboundDocumentId
                    and LineNumber = @LineNumber)
      begin
        select @Exception = 'Attempt to insert duplicate line '
               + @ProductCode
               + '  Order No:'
               + @OrderNumber
               exec @Error = p_Exception_Insert
               @Exception     = @Exception,
               @ExceptionCode = 'INTERROR02',
               @CreateDate    = @GetDate
               
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Exception_Insert'
          goto error_detail
        end
      end
      else
      begin
        exec @Error = p_OutboundLine_Insert
        @OutboundLineId     = @OutboundLineId output,
        @OutboundDocumentId = @OutboundDocumentId,
        @StorageUnitId      = @StorageUnitId,
        @StatusId           = @StatusId,
        @LineNumber         = @LineNumber,
        @Quantity           = @Quantity,
        @BatchId            = null
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_OutboundLine_Insert'
          goto error_detail
        end
      end
      
      if @RecordType != 'PRQ' and @PQDocumentTypeCode = 'PRQ' and @InterfaceTableId is not null -- If Production Que Documents Enable insert 1 per sales order line
      begin
        set @InterfaceId = @InterfaceImportSOHeaderId
        
        exec p_Interface_Reprocess
         @InterfaceTableId = @InterfaceTableId,
         @InterfaceId      = @InterfaceId output,
         @LineNumber       = @LineNumber
        
        update InterfaceImportSOHeader
           set RecordType  = @PQDocumentTypeCode,
               OrderNumber = OrderNumber + ':' + convert(nvarchar(10), @LineNumber)
         where InterfaceImportSOHeaderId = @InterfaceId
      end
      
      fetch detail_cursor
        into @LineNumber,
             @ProductCode,
             @WhsCode,
             @Quantity,
             @PickUp
    end
    
    close detail_cursor
    deallocate detail_cursor
    
    exec @Error = p_Despatch_Create_Issue
    @OutboundDocumentId = @OutboundDocumentId,
    @OperatorId         = null,
    @Remarks            = @Remarks
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'
      goto error_header
    end
    
    select @IssueId     = IssueId,
           @WarehouseId = WarehouseId
      from Issue (nolock)
     where OutboundDocumentId = @OutboundDocumentId
    
    if @RecordType = 'IBT'
    Begin
      Select @ToWarehouseCode = WarehouseCode
        From Warehouse
       Where HostId = convert(nvarchar(10), @ToWarehouseId)
      PRINT CONVERT(CHAR(20),@ToWarehouseiD)
      
      if @ToWarehouseCode = 'XSCS'
      Select @AreaType  = AreaType
        From OutboundDocumentType
       where OutboundDocumentTypeCode = 'MM'
    End
    
    exec @Error = p_Issue_Update
     @IssueId  = @IssueId,
     @AreaType = @AreaType,
     @RouteId  = @RouteId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error updating Issue'
      goto error_header
    end
    
    if @HoldStatus = 'True'
    begin
      select @StatusId = dbo.ufn_StatusId('IS','OH')
      
      exec @Error = p_Issue_Update
       @IssueId  = @IssueId,
       @StatusId = @StatusId
             
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Issue_Update'
        goto error_header
      end
    end
    
    exec @Error = p_Outbound_Auto_Load
    @OutboundShipmentId = @OutboundShipmentId output,
    @IssueId            = @IssueId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
      goto error_header
    end
    
    exec @Error = p_Outbound_Auto_Release
    @OutboundShipmentId = @OutboundShipmentId,
    @IssueId            = @IssueId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Outbound_Auto_Release'
      goto error_header
    end
    
    Goto Commit_Transaction
    
    error_detail:
    
    close detail_cursor
    deallocate detail_cursor
    
    error_header:
    if @@trancount > 0
    rollback transaction xml_import
    
    -- added error message create into InterfaceMessage
    INSERT INTO InterfaceMessage
           ([InterfaceMessageCode]
           ,[InterfaceMessage]
           ,[InterfaceId]
           ,[InterfaceTable]
           ,[Status]
           ,[OrderNumber]
           ,[CreateDate]
           ,[ProcessedDate])
    VALUES
           ('Failed'
           ,LEFT(@ErrorMsg, 255)
           ,@InterfaceImportSOHeaderId
           ,'InterfaceImportSOHeader'
           ,'E'
           ,@OrderNumber
           ,@GetDate
           ,@GetDate)
           
           --select @OrderNumber as 'OrderNumber', 'Error 2', @@trancount as '@@trancount'
           update InterfaceImportSOHeader
       set RecordStatus = 'E',
           Additional9 = @ErrorMsg,
           RecordType = isnull(@OutboundDocumentTypeCode, RecordType)
     where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
    
    Goto next_header
    
    commit_Transaction:
    if @@trancount > 0
    commit transaction xml_import
    
    INSERT INTO InterfaceMessage
           ([InterfaceMessageCode]
           ,[InterfaceMessage]
           ,[InterfaceId]
           ,[InterfaceTable]
           ,[Status]
           ,[OrderNumber]
           ,[CreateDate]
           ,[ProcessedDate])
    VALUES
           ('Processed'
           ,'Success'
           ,@InterfaceImportSOHeaderId
           ,'InterfaceImportSOHeader'
           ,'N'
           ,@OrderNumber
           ,@GetDate
           ,@GetDate)
           
           
    next_header:
      fetch header_cursor
        into @InterfaceImportSOHeaderId,
             @OrderNumber,
             @ExternalCompanyCode,
             @DeliveryDate,
             @PreSchedule,
             @AmountOfInvoice,
             @COD,
             @PickUpCustomer,
             @LoadPriority,
             @JobWindowStart,
             @JobWindowEnd,
             @JobDuration,
             @HoldStatus,
             @Remarks,
             @Instructions,
             @NumberOfLines,
             @RecordType  ,
             @FromWarehouseCode,
             @ToWarehouseCode,
             @FromWarehouseCode,
             @Route
  end
  
  close header_cursor
  deallocate header_cursor
  
  update InterfaceImportSOHeader
     set ProcessedDate = null
   where RecordStatus = 'N'
     and ProcessedDate = @GetDate
  
  return
end
