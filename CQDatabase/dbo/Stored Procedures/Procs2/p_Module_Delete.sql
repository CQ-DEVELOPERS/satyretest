﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Module_Delete
  ///   Filename       : p_Module_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:48
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Module table.
  /// </remarks>
  /// <param>
  ///   @ModuleId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Module_Delete
(
 @ModuleId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Module
     where ModuleId = @ModuleId
  
  select @Error = @@Error
  
  
  return @Error
  
end
