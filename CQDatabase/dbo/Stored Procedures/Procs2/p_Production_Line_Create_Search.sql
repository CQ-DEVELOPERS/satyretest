﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Line_Create_Search
  ///   Filename       : p_Production_Line_Create_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Line_Create_Search
(
 @StorageUnitBatchId int = null output
)

as
begin
	 set nocount on;
  
  declare @InboundDocumentTypeId int
  
  select @InboundDocumentTypeId = InboundDocumentTypeId
    from InboundDocumentType
   where InboundDocumentTypeCode = 'PRV'
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  select il.InboundLineId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         b.ECLNumber,
         s.Status,
         il.Quantity,
         pk.Quantity as 'PalletQuantity',
         b.ExpectedYield,
         b.ActualYield,
         case when isnull(b.ExpectedYield,0) - isnull(b.ActualYield,0) > 0
           then 'False'
           else 'True'
           end as 'OverYield'
    from InboundDocument     id  (nolock)
    join InboundLine         il  (nolock) on id.InboundDocumentId = il.InboundDocumentId
    join StorageUnit         su  (nolock) on il.StorageUnitId     = su.StorageUnitId
    join Status              s   (nolock) on il.StatusId          = s.StatusId
    join Product             p   (nolock) on su.ProductId         = p.ProductId
    join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
    join Batch               b   (nolock) on il.BatchId           = b.BatchId
    join StorageUnitBatch    sub (nolock) on su.StorageUnitId     = sub.StorageUnitId
                                         and b.BatchId            = sub.BatchId
    left outer
    join Pack                pk  (nolock) on su.StorageUnitId     = pk.StorageUnitId
                                         and pk.WarehouseId       = id.WarehouseId
    join PackType             pt (nolock) on pk.PackTypeId        = pt.PackTypeId
                                         and pt.InboundSequence   = 1
  where id.InboundDocumentTypeId = @InboundDocumentTypeId
    and sub.StorageUnitBatchId   = @StorageUnitBatchId
end
