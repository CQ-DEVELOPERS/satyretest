﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Product_Area_Master_Import
  ///   Filename       : p_Product_Area_Master_Import.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Apr 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Product_Area_Master_Import
(
 @xmlstring nvarchar(max)
)

as
begin
	 set nocount on;
  
  declare @DesktopLogId int,
          @InterfaceImportProductId int
  
  insert DesktopLog
        (ProcName,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         getdate()
  
  select @DesktopLogId = scope_identity()
  
  declare @StorageUnitArea as table
  (
   PrincipalId                     int,
   PrincipalCode                   nvarchar(30),
   StorageUnitId                   int,
   ProductCode                     nvarchar(50),
   SKUCode                         nvarchar(20),
   AreaId                          int,
   Area                            nvarchar(50),
   Linked                          bit,
   StoreOrder                      int,
   PickOrder                       int,
   MinimumQuantity                 float,
   ReorderQuantity                 float,
   MaximumQuantity                 float
  )
  
  declare @Error                   int = 0,
          @Errormsg                varchar(max),
          @GetDate                 datetime,
          @command                 varchar(max),
          @InterfaceImportHeaderId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Errormsg = 'p_Product_Area_Master_Import - Error executing procedure'
  
  begin transaction
  
  declare @doc xml

  set @doc = convert(xml,@xmlstring)

  DECLARE @idoc int,
          @InsertDate datetime

  Set @InsertDate = Getdate()

  EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
  
  select @InterfaceImportProductId = MAX(InterfaceImportProductId)
    from InterfaceImportProduct
  
  set @Errormsg = 'p_Product_Area_Master_Import - Error inserting into InterfaceImportProduct'
  
		insert @StorageUnitArea
								(PrincipalCode,
         ProductCode,
         SKUCode,
         Area,
         Linked,
         StoreOrder,
         PickOrder,
         MinimumQuantity,
         ReorderQuantity,
         MaximumQuantity)
	 select PrincipalCode,
         ProductCode,
         SKUCode,
         Area,
         case when Linked in ('1','true')
              then 1
              else 0
              end,
         case when isnumeric(StoreOrder) = 1
              then StoreOrder
              else null
              end,
         case when isnumeric(PickOrder) = 1
              then PickOrder
              else null
              end,
         case when isnumeric(MinimumQuantity) = 1
              then MinimumQuantity
              else null
              end,
         case when isnumeric(ReorderQuantity) = 1
              then ReorderQuantity
              else null
              end,
         case when isnumeric(MaximumQuantity) = 1
              then MaximumQuantity
              else null
              end
		  from OPENXML (@idoc, '/root/Line',1)
            WITH (PrincipalCode             nvarchar(50) 'PrincipalCode',
                  ProductCode               nvarchar(50) 'ProductCode',
                  SKUCode                   nvarchar(50) 'SKUCode',
                  Area                      nvarchar(50) 'Area',
                  Linked                    nvarchar(50) 'Linked',
                  StoreOrder                nvarchar(50) 'StoreOrder',
                  PickOrder                 nvarchar(50) 'PickOrder',
                  MinimumQuantity           nvarchar(50) 'MinimumQuantity',
                  ReorderQuantity           nvarchar(50) 'ReorderQuantity',
                  MaximumQuantity           nvarchar(50) 'MaximumQuantity')
    where ProductCode not like '%ProductCode%'
  
  if @@Error != 0
  begin
    set @Errormsg = 'p_Product_Area_Master_Import - Error inserting into @StorageUnitArea'
    goto error
  end
  
  update @StorageUnitArea
     set PrincipalCode = null
   where PrincipalCode = ''
  
  update t
     set StorageUnitId = su.StorageUnitId
    from @StorageUnitArea t
    join Product          p (nolock) on t.ProductCode = p.ProductCode
    left
    join Principal       pn (nolock) on p.PrincipalId = pn.PrincipalId
    join SKU            sku (nolock) on t.SKUCode     = sku.SKUCode
    join StorageUnit     su (nolock) on p.ProductId   = su.ProductId
                                    and sku.SKUId     = su.SKUId
  
  if @@Error != 0
  begin
    set @Errormsg = 'p_Product_Area_Master_Import - Error updating @StorageUnitArea (StorageUnitId)'
    goto error
  end
  
  update t
     set PrincipalId = p.PrincipalId
    from @StorageUnitArea t
    join Principal        p (nolock) on t.PrincipalCode = p.PrincipalCode
  
  if @@Error != 0
  begin
    set @Errormsg = 'p_Product_Area_Master_Import - Error updating @StorageUnitArea (PrincipalId)'
    goto error
  end
  
  update t
     set AreaId = a.AreaId
    from @StorageUnitArea t
    join Area             a (nolock) on t.Area = a.Area
  
  if @@Error != 0
  begin
    set @Errormsg = 'p_Product_Area_Master_Import - Error updating @StorageUnitArea (AreaId)'
    goto error
  end
  select * from @StorageUnitArea
  delete sua
   from @StorageUnitArea t
   join StorageUnitArea sua (nolock) on t.StorageUnitId = sua.StorageUnitId
  where t.Linked = 0
  
  if @@Error != 0
  begin
    set @Errormsg = 'p_Product_Area_Master_Import - Error deleting StorageUnitArea'
    goto error
  end
  
  insert StorageUnitArea
        (StorageUnitId,
         AreaId,
         StoreOrder,
         PickOrder,
         MinimumQuantity,
         ReorderQuantity,
         MaximumQuantity)
  select t.StorageUnitId,
         t.AreaId,
         min(t.StoreOrder),
         min(t.PickOrder),
         t.MinimumQuantity,
         t.ReorderQuantity,
         t.MaximumQuantity
    from @StorageUnitArea t
    left
    join StorageUnitArea sua (nolock) on t.AreaId        = sua.AreaId
                            and t.StorageUnitId = sua.StorageUnitId
   where sua.AreaId is null
     and t.AreaId is not null
     and t.StorageUnitId is not null
     and t.Linked = 1
  group by t.StorageUnitId,
           t.AreaId,
           t.MinimumQuantity,
           t.ReorderQuantity,
           t.MaximumQuantity
  
  if @@Error != 0
  begin
    set @Errormsg = 'p_Product_Area_Master_Import - Error inserting into @StorageUnitArea'
    goto error
  end
  
		commit transaction
  
  update DesktopLog
     set Errormsg = 'Successful',
         EndDate  = getdate()
   where DesktopLogId = @DesktopLogId
  
  return 0
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    
    update DesktopLog
       set Errormsg = isnull(@xmlstring, 'Error executing ' + OBJECT_NAME(@@PROCID)),
           EndDate  = getdate()
     where DesktopLogId = @DesktopLogId
    
    return @Error
end
