﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipment_Search
  ///   Filename       : p_OutboundShipment_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Oct 2013 09:31:13
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OutboundShipment table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null output,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @DespatchBay int = null,
  ///   @ContactListId int = null,
  ///   @WaveId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OutboundShipment.OutboundShipmentId,
  ///   OutboundShipment.StatusId,
  ///   Status.Status,
  ///   OutboundShipment.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   OutboundShipment.LocationId,
  ///   Location.Location,
  ///   OutboundShipment.ShipmentDate,
  ///   OutboundShipment.Remarks,
  ///   OutboundShipment.SealNumber,
  ///   OutboundShipment.VehicleRegistration,
  ///   OutboundShipment.Route,
  ///   OutboundShipment.RouteId,
  ///   OutboundShipment.Weight,
  ///   OutboundShipment.Volume,
  ///   OutboundShipment.FP,
  ///   OutboundShipment.SS,
  ///   OutboundShipment.LP,
  ///   OutboundShipment.FM,
  ///   OutboundShipment.MP,
  ///   OutboundShipment.PP,
  ///   OutboundShipment.AlternatePallet,
  ///   OutboundShipment.SubstitutePallet,
  ///   OutboundShipment.DespatchBay,
  ///   Location.Location,
  ///   OutboundShipment.NumberOfOrders,
  ///   OutboundShipment.ContactListId,
  ///   ContactList.ContactListId,
  ///   OutboundShipment.TotalOrders,
  ///   OutboundShipment.IDN,
  ///   OutboundShipment.WaveId,
  ///   Wave.Wave,
  ///   OutboundShipment.ReferenceNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipment_Search
(
 @OutboundShipmentId int = null output,
 @StatusId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @DespatchBay int = null,
 @ContactListId int = null,
 @WaveId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @OutboundShipmentId = '-1'
    set @OutboundShipmentId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @DespatchBay = '-1'
    set @DespatchBay = null;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @WaveId = '-1'
    set @WaveId = null;
  
 
  select
         OutboundShipment.OutboundShipmentId
        ,OutboundShipment.StatusId
         ,StatusStatusId.Status as 'Status'
        ,OutboundShipment.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,OutboundShipment.LocationId
         ,LocationLocationId.Location as 'Location'
        ,OutboundShipment.ShipmentDate
        ,OutboundShipment.Remarks
        ,OutboundShipment.SealNumber
        ,OutboundShipment.VehicleRegistration
        ,OutboundShipment.Route
        ,OutboundShipment.RouteId
        ,OutboundShipment.Weight
        ,OutboundShipment.Volume
        ,OutboundShipment.FP
        ,OutboundShipment.SS
        ,OutboundShipment.LP
        ,OutboundShipment.FM
        ,OutboundShipment.MP
        ,OutboundShipment.PP
        ,OutboundShipment.AlternatePallet
        ,OutboundShipment.SubstitutePallet
        ,OutboundShipment.DespatchBay
         ,LocationDespatchBay.Location as 'LocationDespatchBay'
        ,OutboundShipment.NumberOfOrders
        ,OutboundShipment.ContactListId
        ,OutboundShipment.TotalOrders
        ,OutboundShipment.IDN
        ,OutboundShipment.WaveId
         ,WaveWaveId.Wave as 'Wave'
        ,OutboundShipment.ReferenceNumber
    from OutboundShipment
    left
    join Status StatusStatusId on StatusStatusId.StatusId = OutboundShipment.StatusId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = OutboundShipment.WarehouseId
    left
    join Location LocationLocationId on LocationLocationId.LocationId = OutboundShipment.LocationId
    left
    join Location LocationDespatchBay on LocationDespatchBay.LocationId = OutboundShipment.DespatchBay
    left
    join ContactList ContactListContactListId on ContactListContactListId.ContactListId = OutboundShipment.ContactListId
    left
    join Wave WaveWaveId on WaveWaveId.WaveId = OutboundShipment.WaveId
   where isnull(OutboundShipment.OutboundShipmentId,'0')  = isnull(@OutboundShipmentId, isnull(OutboundShipment.OutboundShipmentId,'0'))
     and isnull(OutboundShipment.StatusId,'0')  = isnull(@StatusId, isnull(OutboundShipment.StatusId,'0'))
     and isnull(OutboundShipment.WarehouseId,'0')  = isnull(@WarehouseId, isnull(OutboundShipment.WarehouseId,'0'))
     and isnull(OutboundShipment.LocationId,'0')  = isnull(@LocationId, isnull(OutboundShipment.LocationId,'0'))
     and isnull(OutboundShipment.DespatchBay,'0')  = isnull(@DespatchBay, isnull(OutboundShipment.DespatchBay,'0'))
     and isnull(OutboundShipment.ContactListId,'0')  = isnull(@ContactListId, isnull(OutboundShipment.ContactListId,'0'))
     and isnull(OutboundShipment.WaveId,'0')  = isnull(@WaveId, isnull(OutboundShipment.WaveId,'0'))
  order by Remarks
  
end
