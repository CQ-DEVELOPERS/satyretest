﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Locations_NotLinkedToOperatorGroup
  ///   Filename       : p_Locations_NotLinkedToOperatorGroup.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Locations_NotLinkedToOperatorGroup
( 
	@OperatorGroupId	int,
	@FromAisle			nvarchar(10),
	@ToAisle			nvarchar(10),
	@FromLevel			nvarchar(10),
	@ToLevel			nvarchar(10)
)


as
begin
	 set nocount on;
  
  select l.LocationId,
		 l.Location
    from Location l
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId    = a.AreaId
    where not exists(select 1 from LocationOperatorGroup lop where l.LocationId = lop.LocationId
														and lop.OperatorGroupId = @OperatorGroupId)
	and l.Ailse   between @FromAisle and @ToAisle
	and l.[Level] between @FromLevel and @ToLevel
	order by Location
end
