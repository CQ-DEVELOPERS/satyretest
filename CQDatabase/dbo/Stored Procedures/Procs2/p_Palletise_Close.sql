﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Palletise_Close
  ///   Filename       : p_Palletise_Close.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Palletise_Close
(
 @ReceiptId          int = null,
 @ReceiptLineId      int = null,
 @OutboundShipmentId int = null,
 @IssueId            int = null,
 @IssueLineId        int = null
)

as
begin
	 set nocount on;
	 
  declare @TableInstructions as table
  (
   InstructionId int,
   StatusCode    nvarchar(10),
   JobId         int
  )
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @Count             int,
          @InstructionId     int,
          @StatusCode        nvarchar(10),
          @JobId             int,
          @WaitingId         int,
          @NoStockId         int,
          @CompleteId        int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @WaitingId = dbo.ufn_StatusId('I','W')
  select @NoStockId = dbo.ufn_StatusId('I','NS')
  --select * from Status where StatusCode = 'P'
  
  if @ReceiptId          is null and
     @ReceiptLineId      is null and
     @OutboundShipmentId is null and
     @IssueId            is null and
     @IssueLineId        is null
    return;
  
  if @ReceiptId is not null
  begin
    select @CompleteId = dbo.ufn_StatusId('R','C')
    
    insert @TableInstructions
          (InstructionId,
           StatusCode,
           JobId)
    select InstructionId,
           StatusCode,
           JobId
      from Instruction  i
      join Status       s (nolock) on i.StatusId      = s.StatusId
      join ReceiptLine rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
     where ReceiptId     = isnull(@ReceiptId, ReceiptId)
  end
  else if @ReceiptLineId is not null
  begin
    select @CompleteId = dbo.ufn_StatusId('R','C')
    
    insert @TableInstructions
          (InstructionId,
           StatusCode,
           JobId)
    select InstructionId,
           StatusCode,
           JobId
      from Instruction i
      join Status      s (nolock) on i.StatusId = s.StatusId
     where ReceiptLineId = isnull(@ReceiptLineId, ReceiptLineId)
  end
  
  if @OutboundShipmentId is not null
  begin
    select @CompleteId = dbo.ufn_StatusId('IS','C')
    
    insert @TableInstructions
          (InstructionId,
           StatusCode,
           JobId)
    select InstructionId,
           StatusCode,
           JobId
      from Instruction i
      join Status      s (nolock) on i.StatusId    = s.StatusId
     where OutboundShipmentId = @OutboundShipmentId
  end
  
  if @IssueId is not null
  begin
    select @CompleteId = dbo.ufn_StatusId('IS','C')
    
    insert @TableInstructions
          (InstructionId,
           StatusCode,
           JobId)
    select InstructionId,
           StatusCode,
           JobId
      from Instruction i
      join Status      s (nolock) on i.StatusId    = s.StatusId
      join IssueLine  il (nolock) on i.IssueLineId = il.IssueLineId
     where IssueId = isnull(@IssueId, IssueId)
  end
  else if @IssueLineId is not null
  begin
    select @CompleteId = dbo.ufn_StatusId('IS','C')
    
    insert @TableInstructions
          (InstructionId,
           StatusCode,
           JobId)
    select InstructionId,
           StatusCode,
           JobId
      from Instruction i
      join Status      s (nolock) on i.StatusId = s.StatusId
     where IssueLineId = isnull(@IssueLineId, IssueLineId)
  end
  
  set @Count = (select count(1) from @TableInstructions)
  
  begin transaction
  
  while @Count > 0
  begin
    set @Count = @Count - 1
    
    select top 1 @InstructionId = InstructionId,
                 @StatusCode    = StatusCode,
                 @JobId         = JobId
      from @TableInstructions
    
    delete @TableInstructions where InstructionId = @InstructionId
    
    if @StatusCode in ('W','S')
    begin
      exec @Error = p_StorageUnitBatchLocation_Deallocate
       @InstructionId = @InstructionId
      
      if @Error <> 0
        goto error
      
      update Instruction
         set PickLocationId = null,
             StatusId       = @NoStockId,
             ConfirmedQuantity = 0,
             ConfirmedWeight = 0
       where InstructionId = @InstructionId
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
      
      exec p_InstructionHistory_Insert
       @InstructionId  = @InstructionId,
       @PickLocationId = null,
       @CommandType    = 'Update',
       @InsertDate     = @GetDate
    end
    
    if @OutboundShipmentId is not null or 
       @IssueId            is not null or
       @IssueLineId        is not null
    begin
      exec @Error = p_Job_Update
       @JobId    = @JobId,
       @StatusId = @CompleteId
      
      if @Error <> 0
        goto error
    end
  end
  
  if @OutboundShipmentId is not null or 
     @IssueId            is not null or
     @IssueLineId        is not null
  begin
    update il
       set StatusId = @CompleteId
      from IssueLineInstruction ili (nolock)
      join IssueLine             il on ili.IssueLineId = il.IssueLineId
     where isnull(ili.OutboundShipmentId,0) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId,0))
       and ili.IssueId            = isnull(@IssueId, ili.IssueId)
       and ili.IssueLineId        = isnull(@IssueLineId, ili.IssueLineId)
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert IssueLineHistory
          (IssueLineId,
           StatusId,
           CommandType,
           InsertDate)
    select IssueLineId,
           @CompleteId,
           'Update',
           @GetDate
      from IssueLineInstruction
     where isnull(OutboundShipmentId,0) = isnull(@OutboundShipmentId, isnull(OutboundShipmentId,0))
       and IssueId            = isnull(@IssueId, IssueId)
       and IssueLineId        = isnull(@IssueLineId, IssueLineId)
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update i
       set StatusId = @CompleteId
      from IssueLineInstruction ili (nolock)
      join Issue                  i on ili.IssueId = i.IssueId
     where isnull(ili.OutboundShipmentId,0) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId,0))
       and ili.IssueId            = isnull(@IssueId, ili.IssueId)
       and ili.IssueLineId        = isnull(@IssueLineId, ili.IssueLineId)
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert IssueHistory
          (IssueId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           IssueId,
           @CompleteId,
           'Update',
           @CompleteId
      from IssueLineInstruction
     where isnull(OutboundShipmentId,0) = isnull(@OutboundShipmentId, isnull(OutboundShipmentId,0))
       and IssueId            = isnull(@IssueId, IssueId)
       and IssueLineId        = isnull(@IssueLineId, IssueLineId)
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update os
       set StatusId = @CompleteId
      from IssueLineInstruction ili (nolock)
      join OutboundShipment      os on ili.OutboundShipmentId = os.OutboundShipmentId
     where isnull(ili.OutboundShipmentId,0) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId,0))
       and ili.IssueId            = isnull(@IssueId, ili.IssueId)
       and ili.IssueLineId        = isnull(@IssueLineId, ili.IssueLineId)
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert OutboundShipmentHistory
          (OutboundShipmentId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           OutboundShipmentId,
           @CompleteId,
           'Update',
           @GetDate
      from IssueLineInstruction
     where isnull(OutboundShipmentId,0) = isnull(@OutboundShipmentId, isnull(OutboundShipmentId,0))
       and IssueId            = isnull(@IssueId, IssueId)
       and IssueLineId        = isnull(@IssueLineId, IssueLineId)
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
