﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Logout
  ///   Filename       : p_Operator_Logout.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Logout
(
 @OperatorId        int,
 @SiteType          nvarchar(20)
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @Exception         nvarchar(255),
          @Operator          nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
	 
	 begin transaction
  
  exec @Error = p_Operator_Update
   @OperatorId = @OperatorId,
   @LogoutTime  = @GetDate
  
  if @Error <> 0
    goto error
  
  select @Operator = Operator
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  select @Exception = 'Operator ' + @Operator + ' logged out.'
  
  exec @Error = p_Exception_Insert
   @ExceptionId   = null,
   @ExceptionCode = 'LOGOUT',
   @Exception     = @Exception,
   @OperatorId    = @OperatorId,
   @CreateDate    = @GetDate,
   @ExceptionDate = @Getdate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Operator_Logout'); 
    rollback transaction
    return @Error
end
