﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_Variable_Weight
  ///   Filename       : p_Mobile_Check_Variable_Weight.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Check_Variable_Weight
(
 @barcode nvarchar(30)
)

as
begin
	 set nocount on;
	 
	 declare @bool               bit,
	         @PalletId           int,
	         @StorageUnitBatchId int,
	         @Batch              nvarchar(30)
  
  set @bool = 0 -- False
  
  if @barcode like 'P:%'
    if isnumeric(replace(@barcode,'P:','')) = 1
      select @PalletId = replace(@barcode,'P:',''),
             @barcode  = null
  
  if @Barcode is null
  begin
    if @PalletId is null
      set @PalletId = -1
    
      select @StorageUnitBatchId = StorageUnitBatchId from Pallet (nolock) where PalletId = @PalletId
      
      if @StorageUnitBatchId is null
        set @StorageUnitBatchId = -1
  end
  else
  begin
    exec @StorageUnitBatchId = p_Mobile_Stock_Take_Confirm_Product
     @barcode = @barcode,
     @batch   = @batch
  end
  
  if @StorageUnitBatchId = -1
    goto result
  
  if(select su.ProductCategory
       from StorageUnitBatch sub (nolock)
       join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
      where sub.StorageUnitBatchId = @StorageUnitBatchId) = 'V'
    set @bool = 1
  
  result:
    select @bool
end
