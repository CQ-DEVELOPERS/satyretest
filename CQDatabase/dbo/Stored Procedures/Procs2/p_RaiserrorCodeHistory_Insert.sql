﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RaiserrorCodeHistory_Insert
  ///   Filename       : p_RaiserrorCodeHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:34
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the RaiserrorCodeHistory table.
  /// </remarks>
  /// <param>
  ///   @RaiserrorCodeId int = null,
  ///   @RaiserrorCode nvarchar(20) = null,
  ///   @RaiserrorMessage nvarchar(510) = null,
  ///   @StoredProcedure nvarchar(512) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   RaiserrorCodeHistory.RaiserrorCodeId,
  ///   RaiserrorCodeHistory.RaiserrorCode,
  ///   RaiserrorCodeHistory.RaiserrorMessage,
  ///   RaiserrorCodeHistory.StoredProcedure,
  ///   RaiserrorCodeHistory.CommandType,
  ///   RaiserrorCodeHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RaiserrorCodeHistory_Insert
(
 @RaiserrorCodeId int = null,
 @RaiserrorCode nvarchar(20) = null,
 @RaiserrorMessage nvarchar(510) = null,
 @StoredProcedure nvarchar(512) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert RaiserrorCodeHistory
        (RaiserrorCodeId,
         RaiserrorCode,
         RaiserrorMessage,
         StoredProcedure,
         CommandType,
         InsertDate)
  select @RaiserrorCodeId,
         @RaiserrorCode,
         @RaiserrorMessage,
         @StoredProcedure,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
