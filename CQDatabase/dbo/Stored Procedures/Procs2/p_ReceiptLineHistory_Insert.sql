﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReceiptLineHistory_Insert
  ///   Filename       : p_ReceiptLineHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2013 07:02:50
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ReceiptLineHistory table.
  /// </remarks>
  /// <param>
  ///   @ReceiptLineId int = null,
  ///   @ReceiptId int = null,
  ///   @InboundLineId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @RequiredQuantity int = null,
  ///   @ReceivedQuantity int = null,
  ///   @AcceptedQuantity int = null,
  ///   @RejectQuantity int = null,
  ///   @DeliveryNoteQuantity int = null,
  ///   @SampleQuantity numeric(13,6) = null,
  ///   @AcceptedWeight numeric(13,6) = null,
  ///   @CommandType varchar(10) = null,
  ///   @InsertDate datetime = null,
  ///   @ChildStorageUnitBatchId int = null,
  ///   @NumberOfPallets int = null,
  ///   @AssaySamples float = null,
  ///   @BOELineNumber nvarchar(50) = null,
  ///   @CountryofOrigin nvarchar(50) = null,
  ///   @TariffCode nvarchar(50) = null,
  ///   @Reference1 nvarchar(50) = null,
  ///   @Reference2 nvarchar(50) = null,
  ///   @UnitPrice decimal(13,2) = null
  /// </param>
  /// <returns>
  ///   ReceiptLineHistory.ReceiptLineId,
  ///   ReceiptLineHistory.ReceiptId,
  ///   ReceiptLineHistory.InboundLineId,
  ///   ReceiptLineHistory.StorageUnitBatchId,
  ///   ReceiptLineHistory.StatusId,
  ///   ReceiptLineHistory.OperatorId,
  ///   ReceiptLineHistory.RequiredQuantity,
  ///   ReceiptLineHistory.ReceivedQuantity,
  ///   ReceiptLineHistory.AcceptedQuantity,
  ///   ReceiptLineHistory.RejectQuantity,
  ///   ReceiptLineHistory.DeliveryNoteQuantity,
  ///   ReceiptLineHistory.SampleQuantity,
  ///   ReceiptLineHistory.AcceptedWeight,
  ///   ReceiptLineHistory.CommandType,
  ///   ReceiptLineHistory.InsertDate,
  ///   ReceiptLineHistory.ChildStorageUnitBatchId,
  ///   ReceiptLineHistory.NumberOfPallets,
  ///   ReceiptLineHistory.AssaySamples,
  ///   ReceiptLineHistory.BOELineNumber,
  ///   ReceiptLineHistory.CountryofOrigin,
  ///   ReceiptLineHistory.TariffCode,
  ///   ReceiptLineHistory.Reference1,
  ///   ReceiptLineHistory.Reference2,
  ///   ReceiptLineHistory.UnitPrice
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReceiptLineHistory_Insert
(
 @ReceiptLineId int = null,
 @ReceiptId int = null,
 @InboundLineId int = null,
 @StorageUnitBatchId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @RequiredQuantity int = null,
 @ReceivedQuantity int = null,
 @AcceptedQuantity int = null,
 @RejectQuantity int = null,
 @DeliveryNoteQuantity int = null,
 @SampleQuantity numeric(13,6) = null,
 @AcceptedWeight numeric(13,6) = null,
 @CommandType varchar(10) = null,
 @InsertDate datetime = null,
 @ChildStorageUnitBatchId int = null,
 @NumberOfPallets int = null,
 @AssaySamples float = null,
 @BOELineNumber nvarchar(50) = null,
 @CountryofOrigin nvarchar(50) = null,
 @TariffCode nvarchar(50) = null,
 @Reference1 nvarchar(50) = null,
 @Reference2 nvarchar(50) = null,
 @UnitPrice decimal(13,2) = null,
 @ReceivedDate datetime = null,
 @PutawayStarted datetime = null,
 @PutawayEnded datetime = null
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ReceiptLineHistory
        (ReceiptLineId,
         ReceiptId,
         InboundLineId,
         StorageUnitBatchId,
         StatusId,
         OperatorId,
         RequiredQuantity,
         ReceivedQuantity,
         AcceptedQuantity,
         RejectQuantity,
         DeliveryNoteQuantity,
         SampleQuantity,
         AcceptedWeight,
         CommandType,
         InsertDate,
         ChildStorageUnitBatchId,
         NumberOfPallets,
         AssaySamples,
         BOELineNumber,
         CountryofOrigin,
         TariffCode,
         Reference1,
         Reference2,
         UnitPrice,
		 ReceivedDate,
		 PutawayStarted,
		 PutawayEnded)
  select @ReceiptLineId,
         @ReceiptId,
         @InboundLineId,
         @StorageUnitBatchId,
         @StatusId,
         @OperatorId,
         @RequiredQuantity,
         @ReceivedQuantity,
         @AcceptedQuantity,
         @RejectQuantity,
         @DeliveryNoteQuantity,
         @SampleQuantity,
         @AcceptedWeight,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @ChildStorageUnitBatchId,
         @NumberOfPallets,
         @AssaySamples,
         @BOELineNumber,
         @CountryofOrigin,
         @TariffCode,
         @Reference1,
         @Reference2,
         @UnitPrice,
		 @ReceivedDate,
		 @PutawayStarted,
		 @PutawayEnded 
  
  select @Error = @@Error
  
  
  return @Error
  
end
