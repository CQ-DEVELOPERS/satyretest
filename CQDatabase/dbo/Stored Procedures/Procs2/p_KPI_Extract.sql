﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_KPI_Extract
  ///   Filename       : p_KPI_Extract.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_KPI_Extract
(
 @FromDate		datetime,
 @ToDate		datetime
)

as
begin

	 set nocount on;
  
  declare @TableDetails as Table
  (
   ProductCode			nvarchar(30),
   Product				nvarchar(50),
   StorageUnitId		int,
   SKU					nvarchar(50),
   Quantity				int,
   Received				int,
   Damages				int,
   Samples				int,
   Putaway				int,
   Movements			int,
   Replenishments		int,
   Picking				int,
   Checking				int,
   MoveToDespatch		int,
   Despatch				int
  );

insert	@TableDetails
	   (ProductCode,
		Product,
		StorageUnitId,
		SKU,
		Quantity)
select	p.ProductCode,
		p.Product,
		su.StorageUnitId,
		sk.SKU,
		sk.Quantity
from StorageUnit su
join product p on su.ProductId = p.ProductId
join SKU sk on sk.SKUId = su.SKUId 
--where SKU = 'UNIT'

update td
     set Damages   = (select COUNT(il.InboundLineId) from InboundLine il
						join Reason r on il.ReasonId = r.ReasonId 
						where td.StorageUnitId = il.StorageUnitId
						and  ReasonCode in ('Damaged','Product Damaged','CAV Damage','DAMAGES / BREAKAGES (Distribution)'))
    from @TableDetails    td

select * from @TableDetails
order by ProductCode

end

