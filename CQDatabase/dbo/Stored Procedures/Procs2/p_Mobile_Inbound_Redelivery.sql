﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Inbound_Redelivery
  ///   Filename       : p_Mobile_Inbound_Redelivery.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Oct 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Inbound_Redelivery
(
 @receiptId           int
)

as
begin
	 set nocount on
  
  declare @Error              int,
          @GetDate            datetime,
          @StatusId           int,
          @newReceiptId       int
  
  select @GetDate = dbo.ufn_Getdate()
  
  exec @Error = p_Receiving_Redelivery_Insert
   @inboundShipmentId = null,
   @receiptId         = @receiptId,
   @newReceiptId      = @newReceiptId output
  
  if @Error <> 0
  begin
    set @ReceiptId = -1
    goto error
  end
  
  if @newReceiptId is null
    set @newReceiptId = -1
  
  select @receiptId = @newReceiptId
  
  select @ReceiptId
  return @ReceiptId
  
  error:
    select @ReceiptId
    return @ReceiptId
end
