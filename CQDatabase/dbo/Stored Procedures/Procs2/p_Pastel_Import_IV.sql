﻿--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
  /// <summary>
  ///   Procedure Name : p_Pastel_Import_IV
  ///   Filename       : p_Pastel_Import_IV.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 22 May 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pastel_Import_IV  
  
as  
begin  
  set nocount on;  
    
  declare @Error             int,  
          @Error2    int,  
          @Errormsg          varchar(500),  
          @GetDate           datetime,  
          -- InterfaceDashboardBulkLog  (Karen)  
          @FileKeyId                    INT,  
          @FileType                     VARCHAR(100),  
          @ProcessedDate                DATETIME,  
          @FileName                     VARCHAR(50) ,  
          @ErrorCode                    CHAR (5),  
          @ErrorDescription             VARCHAR (255),  
          @InterfaceMessage    VARCHAR (255)  
    
  select @GetDate = dbo.ufn_Getdate()  
    
  begin transaction  
    
  update h  
     set OutboundShipmentId = vi.OutboundShipmentId,  
         IssueId            = vi.IssueId  
    from InterfaceImportIPHeader h  
    join viewIssue              vi on h.OrderNumber = vi.OrderNumber  
   where h.IssueId is null  
    
  select @Error = @@error  
    
  if @Error <> 0  
  begin    
    
   -- Karen - set up error message for create into InterfaceMessage  
    
    SET @InterfaceMessage = 'Update to InterfaceImportIPHeader failed'   
       
    goto error
  end  
    
  update od  
     set ReferenceNumber = iv.PrimaryKey  
    from InterfaceImportIPHeader iv (nolock)  
    join OutboundDocument       od on iv.OrderNumber = od.OrderNumber  
   where od.ReferenceNumber is null  
     and iv.PrimaryKey  is not null  
    
  select @Error = @@error  
    
  if @Error <> 0  
  Begin  
   -- Karen - set up error message for create into InterfaceMessage  
    
    SET @InterfaceMessage = 'Update to OutboundDocument Failed'  
       
    goto error  
  end
    
  insert InterfaceInvoice  
        (OrderNumber,  
         InvoiceNumber,  
         ProcessedDate,  
         RecordStatus)  
  select OrderNumber,  
         PrimaryKey,  
         @GetDate,  
         'N'  
    from InterfaceImportIPHeader iv (nolock)  
   where not exists(select top 1 1 from InterfaceInvoice ii (nolock) where iv.OrderNumber = ii.OrderNumber and isnull(iv.PrimaryKey,'') = isnull(ii.InvoiceNumber,''))  
    
  select @Error = @@error  
    
  if @Error <> 0  
  begin  
     -- Karen - set up error message for create into InterfaceMessage  
    
    SET @InterfaceMessage = 'Insert to InterfaceInvoice failed'   
       
    goto error  
  end
  
  declare @invoices As Table
  (ID int identity(1,1)
  ,OrderNumber varchar(30)
  ,InvoiceNumber varchar(30)
  ,IssueId int)
  
  declare @id int
         ,@issueId int
  
  Insert Into @Invoices
        (Ordernumber
        ,InvoiceNumber)
  select OrderNumber
        ,InvoiceNumber
   from InterfaceInvoice
  where ProcessedDate = @GetDate
    and RecordStatus = 'N'
    
  update i
     set IssueId = vi.issueId
    from @Invoices i
    inner join viewIssue vi on i.OrderNumber = vi.OrderNumber
    
  while (select count(*) from @invoices) > 0
  begin
    select top 1 @id = id
                ,@issueId = issueId
    from @Invoices
        
    exec p_Interface_Export_PS_Insert @issueId
    
    delete from @Invoices
    where id = @id
    
    print @id
  end
  
  update InterfaceInvoice
  set RecordStatus = 'Y'
  where ProcessedDate = @GetDate
  
    
         
          
  commit transaction  
    
   -- Karen - added error message create into InterfaceMessage  
      INSERT INTO InterfaceMessage  
           ([InterfaceMessageCode]  
           ,[InterfaceMessage]  
           ,[InterfaceId]  
        ,[InterfaceTable]  
        ,[Status]  
        ,[OrderNumber]  
        ,[CreateDate]  
        ,[ProcessedDate])  
     VALUES  
        ('Processed'  
        ,'Success'  
        ,@FileKeyId  
        ,'InterfaceImportIPHeader'  
        ,'N'  
        ,NULL  
        ,@GetDate  
        ,@GetDate)  
  return  
    
  error:  
    
           
          
    RAISERROR (900000,-1,-1, 'Error executing p_Pastel_Import_IV'  ); 
    rollback transaction  
      
    -- Karen - added error message create into InterfaceMessage  
      INSERT INTO InterfaceMessage  
           ([InterfaceMessageCode]  
           ,[InterfaceMessage]  
           ,[InterfaceId]  
        ,[InterfaceTable]  
        ,[Status]  
        ,[OrderNumber]  
        ,[CreateDate]  
        ,[ProcessedDate])  
     VALUES  
        ('Failed'  
        ,@InterfaceMessage  
        ,@FileKeyId  
        ,'InterfaceImportIPHeader'  
        ,'E'  
        ,NULL  
        ,@GetDate  
        ,@GetDate)  
    return @Error  
end  



