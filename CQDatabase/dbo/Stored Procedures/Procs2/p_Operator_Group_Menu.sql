﻿
  /* <summary>
  ///   Procedure Name : p_Operator_Group_Menu
  ///   Filename       : p_Operator_Group_Menu.sql
  ///   Create By      : William
  ///   Date Created   : 27 Jun 2008
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Menu table.
  /// </remarks>
  /// 
  /// <returns>
  ///   mi.MenuItemId,
  ///   mic.MenuItemText,
  ///   mic.ToolTip,
  ///   mi.NavigateTo,
  ///   mi.ParentMenuItemId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Operator_Group_Menu](
						@OperatorGroupId int
						)
as
begin
	 set nocount on
Select p.OperatorGroupId ,
		     p.OperatorGroup,
		     i.MenuItem as ParentItemText,
		     i.MenuItemId as ParentItemId,
		     c.MenuItemid,
		     c.MenuItem as MenuItem,
		     '~/App_Themes/Default/' + str(isnull(c.access,0),1) + '.png' as [c.access],
		     d.MenuItemid3,
		     d.MenuItem3,
		     '~/App_Themes/Default/' + str(isnull(d.access,0),1) + '.png' as [d.access]
from vw_MenuItem_OperatorGroup p
       Left join Menuitem i on i.MenuitemId = p.MenuItemId
 	     Left join vw_MenuItem_OperatorGroup2 c 		on		c.ParentMenuItemId	= p.MenuItemId 		and c.operatorGRoupid	= p.OperatorGroupId
 	     Left join vw_MenuItem_OperatorGroup3 d 		on		d.ParentMenuItemId	= c.MenuItemId 		and d.operatorGRoupid	= p.OperatorGroupId
 	     

where p.parentMenuItemId is null and p.OperatorGroupId = @OperatorGroupId and d.menuitemid3 is not null
order  by p.OperatorGroup,
		p.OperatorGroupId,
		i.MenuItem,
		c.MenuItem,
		c.MenuItemid,
		c.Access,
		d.MenuItem3,
		d.MenuItemid3,
		d.Access
for xml AUTO,ROOT('Operator_Groups')

end

