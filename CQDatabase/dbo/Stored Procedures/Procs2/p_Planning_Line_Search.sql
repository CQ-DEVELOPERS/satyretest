﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Line_Search
  ///   Filename       : p_Planning_Line_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Line_Search
(
 @WarehouseId            int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @OrderNumber	           nvarchar(30),
 @FromDate	              datetime,
 @ToDate	                datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   IssueLineId                     int,
   IssueId                         int,
   OrderNumber                     nvarchar(30),
   StorageUnitBatchId              int,
   StorageUnitId                   int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(50),
   BatchId                         int,
   Batch                           nvarchar(50),
   SKUCode                         nvarchar(50),
   OrderQuantity                   float,
   Status                          nvarchar(50),
   AvailableQuantity               float,
   AvailabilityIndicator           nvarchar(20),
   AvailablePercentage             numeric(13,2)
  )
  
  declare @TableHeader as Table
  (
   IssueId int
  )
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  insert @TableHeader
        (IssueId)
  select i.IssueId
    from OutboundDocument     od  (nolock)
    join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
    join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join Status               s   (nolock) on i.StatusId               = s.StatusId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
     and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
     and i.DeliveryDate      between @FromDate and @ToDate
     and s.Type                    = 'IS'
     and s.StatusCode             in ('W','C','D','P') -- Waiting, Confirmed, Delivered
     and od.WarehouseId            = @WarehouseId
  
  insert @TableResult
        (IssueLineId,
         IssueId,
         OrderNumber,
         StorageUnitBatchId,
         StorageUnitId,
         BatchId,
         OrderQuantity,
         Status)
  select il.IssueLineId,
         il.IssueId,
         od.OrderNumber,
         sub.StorageUnitBatchId,
         sub.StorageUnitId,
         sub.BatchId,
         ol.Quantity,
         s.Status
    from IssueLine        il
    join Issue            i   (nolock) on il.IssueId            = i.IssueId
    join @TableHeader     th           on i.IssueId             = th.IssueId
    join OutboundLine     ol  (nolock) on il.OutboundLineId     = ol.OutboundLineId
    join OutboundDocument od  (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
    join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
    join Status           s   (nolock) on il.StatusId           = s.StatusId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         Batch       = b.Batch,
         SKUCode     = sku.SKUCode
    from @TableResult tr
    join StorageUnit      su  (nolock) on tr.StorageUnitId      = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch            b   (nolock) on tr.BatchId           = b.BatchId
  
  update tr
     set AvailableQuantity = (select sum(subl.ActualQuantity - subl.ReservedQuantity)
                                from StorageUnitBatchLocation subl
                                join StorageUnitBatch          sub on subl.StorageUnitBatchId = sub.StorageUnitBatchId
                               where sub.StorageUnitId = tr.StorageUnitId)
    from @TableResult tr
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), AvailableQuantity) / convert(numeric(13,3), OrderQuantity)) * 100
    from @TableResult
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailabilityIndicator = 'Green'
   where AvailableQuantity >= OrderQuantity
  
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where AvailableQuantity > 0
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where AvailableQuantity <= 0
  
  select IssueLineId,
         IssueId,
         OrderNumber,
         StorageUnitBatchId,
         StorageUnitId,
         ProductCode,
         Product,
         Batch,
         SKUCode,
         OrderQuantity,
         Status,
         AvailabilityIndicator
    from @TableResult
  order by ProductCode,
           Product,
           Batch,
           SKUCode,
           OrderQuantity
end
