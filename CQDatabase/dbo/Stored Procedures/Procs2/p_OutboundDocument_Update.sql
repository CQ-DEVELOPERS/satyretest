﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Update
  ///   Filename       : p_OutboundDocument_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 07:31:31
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OutboundDocument table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null,
  ///   @OutboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @FromLocation nvarchar(20) = null,
  ///   @ToLocation nvarchar(20) = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @Quantity float = null,
  ///   @PrincipalId int = null,
  ///   @OperatorId int = null,
  ///   @ExternalOrderNumber nvarchar(60) = null,
  ///   @DirectDeliveryCustomerId int = null,
  ///   @EmployeeCode nvarchar(100) = null,
  ///   @Collector nvarchar(100) = null,
  ///   @AdditionalText1 nvarchar(510) = null,
  ///   @VendorCode nvarchar(60) = null,
  ///   @CustomerOrderNumber nvarchar(60) = null,
  ///   @AdditionalText2 nvarchar(510) = null,
  ///   @BOE nvarchar(510) = null,
  ///   @Address1 nvarchar(510) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Update
(
 @OutboundDocumentId int = null,
 @OutboundDocumentTypeId int = null,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @OrderNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null,
 @ReferenceNumber nvarchar(60) = null,
 @FromLocation nvarchar(20) = null,
 @ToLocation nvarchar(20) = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @Quantity float = null,
 @PrincipalId int = null,
 @OperatorId int = null,
 @ExternalOrderNumber nvarchar(60) = null,
 @DirectDeliveryCustomerId int = null,
 @EmployeeCode nvarchar(100) = null,
 @Collector nvarchar(100) = null,
 @AdditionalText1 nvarchar(510) = null,
 @VendorCode nvarchar(60) = null,
 @CustomerOrderNumber nvarchar(60) = null,
 @AdditionalText2 nvarchar(510) = null,
 @BOE nvarchar(510) = null,
 @Address1 nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
  if @OutboundDocumentId = '-1'
    set @OutboundDocumentId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @DirectDeliveryCustomerId = '-1'
    set @DirectDeliveryCustomerId = null;
  
	 declare @Error int
 
  update OutboundDocument
     set OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, OutboundDocumentTypeId),
         ExternalCompanyId = isnull(@ExternalCompanyId, ExternalCompanyId),
         StatusId = isnull(@StatusId, StatusId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         DeliveryDate = isnull(@DeliveryDate, DeliveryDate),
         CreateDate = isnull(@CreateDate, CreateDate),
         ModifiedDate = isnull(@ModifiedDate, ModifiedDate),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         FromLocation = isnull(@FromLocation, FromLocation),
         ToLocation = isnull(@ToLocation, ToLocation),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         BatchId = isnull(@BatchId, BatchId),
         Quantity = isnull(@Quantity, Quantity),
         PrincipalId = isnull(@PrincipalId, PrincipalId),
         OperatorId = isnull(@OperatorId, OperatorId),
         ExternalOrderNumber = isnull(@ExternalOrderNumber, ExternalOrderNumber),
         DirectDeliveryCustomerId = isnull(@DirectDeliveryCustomerId, DirectDeliveryCustomerId),
         EmployeeCode = isnull(@EmployeeCode, EmployeeCode),
         Collector = isnull(@Collector, Collector),
         AdditionalText1 = isnull(@AdditionalText1, AdditionalText1),
         VendorCode = isnull(@VendorCode, VendorCode),
         CustomerOrderNumber = isnull(@CustomerOrderNumber, CustomerOrderNumber),
         AdditionalText2 = isnull(@AdditionalText2, AdditionalText2),
         BOE = isnull(@BOE, BOE),
         Address1 = isnull(@Address1, Address1) 
   where OutboundDocumentId = @OutboundDocumentId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_OutboundDocumentHistory_Insert
         @OutboundDocumentId = @OutboundDocumentId,
         @OutboundDocumentTypeId = @OutboundDocumentTypeId,
         @ExternalCompanyId = @ExternalCompanyId,
         @StatusId = @StatusId,
         @WarehouseId = @WarehouseId,
         @OrderNumber = @OrderNumber,
         @DeliveryDate = @DeliveryDate,
         @CreateDate = @CreateDate,
         @ModifiedDate = @ModifiedDate,
         @ReferenceNumber = @ReferenceNumber,
         @FromLocation = @FromLocation,
         @ToLocation = @ToLocation,
         @StorageUnitId = @StorageUnitId,
         @BatchId = @BatchId,
         @Quantity = @Quantity,
         @PrincipalId = @PrincipalId,
         @OperatorId = @OperatorId,
         @ExternalOrderNumber = @ExternalOrderNumber,
         @DirectDeliveryCustomerId = @DirectDeliveryCustomerId,
         @EmployeeCode = @EmployeeCode,
         @Collector = @Collector,
         @AdditionalText1 = @AdditionalText1,
         @VendorCode = @VendorCode,
         @CustomerOrderNumber = @CustomerOrderNumber,
         @AdditionalText2 = @AdditionalText2,
         @BOE = @BOE,
         @Address1 = @Address1,
         @CommandType = 'Update'
  
  return @Error
  
end
