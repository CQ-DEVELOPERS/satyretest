﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Palletise_Manual
  ///   Filename       : p_Outbound_Palletise_Manual.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Apr 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Palletise_Manual
(
 @WarehouseId        int = null,
 @OutboundShipmentId int = null,
 @IssueId            int = null,
 @Deallocate         bit = 0,
 @Palletise          bit = 1,
 @AllocateLocations  bit = 0,
 @PlanningComplete   bit = 0,
 @Release            bit = 0
)

as
begin
  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
  
  set @trancount = @@trancount;
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_Outbound_Palletise_Manual;
    
    begin
      -- update Issue set AreaType = 'BOND' where IssueId = 36

      --set @WarehouseId = null

      --select top 1
      --       @OutboundShipmentId = vi.OutboundShipmentId,
      --       @IssueId            = vi.IssueId,
      --       @WarehouseId        = vi.WarehouseId
      --  from viewIssue vi
      -- where OrderNumber = '81914955'
      
      --if @WarehouseId is null
      --  return

      declare @Error int = 0

      if @Deallocate = 1
      begin
        exec @Error = p_Palletise_Deallocate
         @OutboundShipmentId = @OutboundShipmentId,
         @IssueId            = @IssueId

        if @Error = 0
          update il
             set StorageUnitBatchId = def.StorageUnitBatchid
            from IssueLine il
            left
            join OutboundShipmentIssue osi on il.IssueId = osi.IssueId
            join StorageUnitBatch sub on il.StorageUnitBatchId = sub.StorageUnitBatchId
            join StorageUnitBatch def on sub.StorageUnitId = def.StorageUnitId
                                     and def.BatchId = 1
                                     and sub.StorageUnitBatchId != def.StorageUnitBatchId
           where osi.OutboundShipmentId = @OutboundShipmentId
        else 
        begin
          select 'ERROR'
          return
        end
      end
      
      declare @OperatorId int = (select min(OperatorId) from Operator)
      
      if @Palletise = 1
      exec @Error = p_Outbound_Palletise
       @WarehouseId        = @WarehouseId,
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId,
       @OperatorId         = @OperatorId,
       @Weight             = 650,
       @Volume             = 750000
      select @Error
      if @AllocateLocations = 1
      exec p_Despatch_Manual_Palletisation_Auto_Locations
       @OutboundShipmentId = @OutboundShipmentId, 
       @IssueId            = @IssueId

      if @PlanningComplete = 1
      exec p_Planning_Complete
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId

      if @Release = 1
      exec p_Outbound_Release
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId,
       @StatusCode         = 'RL'

      --update Job
      --   set StatusId = dbo.ufn_StatusId('IS','PS')
      -- where JobId in (select distinct JobId from viewInstruction where OutboundShipmentId = @OutboundShipmentId and JobCode = 'RL')
      
      --return
      
      if @OutboundShipmentId is not null or @IssueId is not null
      begin
        if @OutboundShipmentId is not null
          set @IssueId = null
        --select Status, * from viewIssue  where (OutboundShipmentId = @OutboundShipmentId or @OutboundShipmentId is null) and (IssueId = @IssueId or @IssueId is null) order by JobId, ProductCode, OrderNumber
        --select Status, * from viewIssueLine where (OutboundShipmentId = @OutboundShipmentId or @OutboundShipmentId is null) and (IssueId = @IssueId or @IssueId is null) order by JobId, ProductCode, OrderNumber
        select distinct OutboundShipmentId, OrderNumber,	InstructionTypeCode,	JobId, JobStatus, InstructionId, InstructionCode, ProductCode,	Product,	SKUCode, Batch, PickLocation, StoreLocation, Quantity, CreateDate from viewili where (OutboundShipmentId = @OutboundShipmentId or @OutboundShipmentId is null) and (IssueId = @IssueId or @IssueId is null) order by JobId, ProductCode, OrderNumber
        --select JobId, COUNT(distinct(JobId)) as 'Jobs', COUNT(1) as 'Instructions' from viewili where (OutboundShipmentId = @OutboundShipmentId or @OutboundShipmentId is null) and (IssueId = @IssueId or @IssueId is null) order by JobId, ProductCode, OrderNumber
      end
    end
	   
    lbexit:
    if @trancount = 0
		    commit;
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_Outbound_Palletise_Manual;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
end
