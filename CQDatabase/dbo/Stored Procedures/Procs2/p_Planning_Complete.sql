﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Complete
  ///   Filename       : p_Planning_Complete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Complete
(
 @OutboundShipmentId int = null,
 @IssueId            int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @rowcount          int,
          @Transaction       bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Planning_Complete'
  set @Error = 0
  
  if @@trancount = 0
  begin
    begin transaction
    set @Transaction = 1
  end
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  begin
    declare @TableHeader as table
    (
     OutboundShipmentId int,
     IssueId            int,
     IssueLineId        int,
     StatusCode         nvarchar(10)
    )
    
    if @OutboundShipmentId is null
    begin
      insert @TableHeader
            (OutboundShipmentId,
             IssueId,
             IssueLineId,
             StatusCode)
      select distinct null,
             ili.IssueId,
             ili.IssueLineId,
             s.StatusCode
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
        join Job                    j (nolock) on i.JobId           = j.JobId
        join Status                  s (nolock) on j.StatusId       = s.StatusId
       where ili.IssueId  = @IssueId
         and s.StatusCode in ('SA','P')-- in ('W','P','SA')
      
      select @rowcount = @@rowcount
    end
    else
    begin
      insert @TableHeader
            (OutboundShipmentId,
             IssueId,
             IssueLineId,
             StatusCode)
      select distinct ili.OutboundShipmentId,
             ili.IssueId,
             ili.IssueLineId,
             s.StatusCode
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
        join Job                    j (nolock) on i.JobId           = j.JobId
        join Status                  s (nolock) on j.StatusId       = s.StatusId
       where ili.OutboundShipmentId = @OutboundShipmentId
         and s.StatusCode           in ('SA','P')-- in ('W','P','SA')
      
      select @rowcount = @@rowcount
    end
    
    if @rowcount = 0
    begin
      set @Error = -1
      goto error
    end
    
--    if exists(select top 1 1 from @TableHeader where StatusCode != 'SA')
--    begin
--      set @Error = -1
--      set @Errormsg = 'p_Planning_Complete - Not all lines have stock allocated!'
--      goto error
--    end
    
    select @StatusId = dbo.ufn_StatusId('IS','PC')
    
    update IssueLine
       set StatusId = @StatusId
      from @TableHeader th
      join IssueLine    il on th.IssueLineId = il.IssueLineId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert IssueLineHistory
          (IssueLineId,
           CommandType,
           InsertDate)
    select IssueLineId,
           'Update',
           @GetDate
      from @TableHeader
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update i
       set StatusId = @StatusId
      from @TableHeader th
      join Issue         i on th.IssueId = i.IssueId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert IssueHistory
          (IssueId,
           CommandType,
           InsertDate)
    select distinct
           IssueId,
           'Update',
           @GetDate
      from @TableHeader
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update os
       set StatusId = @StatusId
      from @TableHeader     th
      join OutboundShipment os on th.OutboundShipmentId = os.OutboundShipmentId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert OutboundShipmentHistory
          (OutboundShipmentId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           OutboundShipmentId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update j
       set StatusId = @StatusId
      from @TableHeader     th
      join Instruction       i (nolock) on th.IssueLineId = i.IssueLineId
      join Job               j /*lock*/ on i.JobId        = j.JobId
      join Status            s (nolock) on j.StatusId     = s.StatusId
     where s.StatusCode != 'NS'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update op
       set PlanningComplete = @Getdate
      from @TableHeader     th
      join Instruction       i (nolock) on th.IssueLineId = i.IssueLineId
      join Job                  j on i.JobId               = j.JobId
      join OutboundPerformance op on j.JobId               = op.JobId
      join Status               s on j.StatusId            = s.StatusId
     where s.StatusCode != 'NS'
       and s.StatusCode in ('PC','M','RL','PS','SA')
       and op.PlanningComplete is null
    select @@ROWCOUNT
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert JobHistory
          (JobId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           j.JobId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader     th
      join Instruction       i (nolock) on th.IssueLineId = i.IssueLineId
      join Job               j (nolock) on i.JobId        = j.JobId
      join Status            s (nolock) on j.StatusId     = s.StatusId
     where s.StatusCode != 'NS'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update j
       set StatusId = @StatusId
      from @TableHeader     th
      join Instruction       i (nolock) on th.OutboundShipmentId = i.OutboundShipmentId
      join Job               j /*lock*/ on i.JobId               = j.JobId
      join Status            s (nolock) on j.StatusId     = s.StatusId
     where s.StatusCode != 'NS'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update op
       set PlanningComplete = @Getdate
      from @TableHeader     th
      join Instruction          i on th.OutboundShipmentId = i.OutboundShipmentId
      join Job                  j on i.JobId               = j.JobId
      join OutboundPerformance op on j.JobId               = op.JobId
      join Status               s on j.StatusId            = s.StatusId
     where s.StatusCode != 'NS'
       and s.StatusCode in ('PC','M','RL','PS','SA')
       and op.PlanningComplete is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert JobHistory
          (JobId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           j.JobId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader     th
      join Instruction       i (nolock) on th.OutboundShipmentId = i.OutboundShipmentId
      join Job               j (nolock) on i.JobId               = j.JobId
      join Status            s (nolock) on j.StatusId     = s.StatusId
     where s.StatusCode != 'NS'
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_Reserve_Batches_Delete
   @OutboundShipmentId = @OutboundShipmentId,
   @IssueId = @IssueId
  
  if @Error <> 0
    goto error
  
  result:
    if @Transaction = 1
      commit transaction
    return 0
  
  error:
    if @Transaction = 1
    begin
      RAISERROR (900000,-1,-1, @Errormsg);
      rollback transaction
    end
    return @Error
end
