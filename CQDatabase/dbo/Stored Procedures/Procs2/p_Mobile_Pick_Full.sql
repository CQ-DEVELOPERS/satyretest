﻿/*
    /// <summary>
    ///   Procedure Name : p_Mobile_Pick_Full
    ///   Filename       : p_Mobile_Pick_Full.sql
    ///   Create By      : Grant Schultz
    ///   Date Created   : 11 June 2007 14:15:00
    /// </summary>
    /// <remarks>
    ///   Performs a Pick and Store of Items.
    /// </remarks>
    /// <param>
    ///   @Type                int,
    ///           0 -- Get Next Tranaction
    ///           1 -- Cancel Pick
    ///           2 -- Pick Transaction
    ///           3 -- Request Replenishment
    ///           4 -- Store Transaction
    ///           5 -- Loation Error
    ///           6 -- Confirm Quantity
    ///   @OperatorId          int,
    ///   @PalletId            int,
    ///   @InstructionId       int output,
    ///   @ConfirmedQuantity   float
    /// </param>
    /// <returns>
    ///   @Error
    /// </returns>
    /// <newpara>
    ///   Modified by    : 
    ///   Modified Date  : 
    ///   Details        : 
    /// </newpara>
*/
create procedure p_Mobile_Pick_Full
(
 @Type                int,
 @OperatorId          int,
 @PalletId            int,
 @InstructionId       int output,
 @ConfirmedQuantity   float
)
as
begin
	 set nocount on
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @WarehouseId        int,
          @JobId              int,
          @PickLocationId     int,
          @ProductCode        nvarchar(30),
          @SKUCode            nvarchar(50),
          @Batch              nvarchar(50),
          @StorageUnitBatchId int,
          @Quantity           float,
          @StatusId           int,
          @InstructionTypeId  int,
          @StoreLocationId    int,
          @Weight             float
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @Type = 0 -- Get Next Tranaction
  begin
    begin transaction
    
    -- Notes Operator Group must be setup for to Area or in MovementSequence table.
    exec @Error = p_Mobile_Next_Job
     @OperatorId          = @OperatorId,
     @JobId               = @JobId output
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Mobile_Next_Line
     @JobId               = @JobId,
     @InstructionId       = @InstructionId output
    
    if @Error <> 0
      goto error
    
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'S'
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @operatorId    = @operatorId,
     @StartDate     = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    commit transaction
  end
  
--  if @PalletId is null
--  begin
    select @PalletId           = PalletId,
           @WarehouseId        = WarehouseId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @Quantity           = Quantity,
           @InstructionTypeId  = InstructionTypeId,
           @JobId              = JobId
      from Instruction
     where InstructionId = @InstructionId
    
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
--  end
  
  if @Type = 1 -- Cancel Pick
  begin
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'W'
    
    exec @Error = p_Job_Update
     @jobId      = @jobId,
     @StatusId   = @StatusId,
     @operatorId = null
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @operatorId    = null,
     @StartDate     = null
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @InstructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
  end
  else if @Type = 2 -- Pick Transaction
  begin
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @InstructionId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_Instruction_Update
     @InstructionId     = @InstructionId,
     @confirmedQuantity = @confirmedQuantity
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId = @InstructionId,
     @Store         = 0
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
  end
  else if @Type = 3 -- Request Replenishment
  begin
    return
  end
  else if @Type = 4 -- Store Transaction
  begin
    exec @Error = p_StorageUnitBatchLocation_Allocate
     @InstructionId = @InstructionId,
     @Pick          = 0
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    begin transaction
    
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'F'
    
    exec @Error = p_Job_Update
     @JobId      = @jobId,
     @StatusId   = @StatusId,
     @operatorId = null
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @EndDate       = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    -- Notes Operator Group must be setup for to Area or in MovementSequence table.
    exec @Error = p_Mobile_Next_Job
     @OperatorId          = @OperatorId,
     @JobId               = @JobId output
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Mobile_Next_Line
     @JobId               = @JobId,
     @InstructionId       = @InstructionId output
    
    if @Error <> 0
      goto error
    
    select @StatusId = StatusId
      from Status (nolock)
     where Type       = 'I'
       and StatusCode = 'S'
    
    exec @Error = p_Instruction_Update
     @InstructionId = @InstructionId,
     @StatusId      = @StatusId,
     @operatorId    = @operatorId,
     @StartDate     = @GetDate
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    commit transaction
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    else
    begin
      set @Error = 2
      goto Result
    end
  end
  else if @Type = 5 -- Loation Error
  begin
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId       = @InstructionId,
     @Store               = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec p_Pick_Location_Get
     @WarehouseId        = @WarehouseId,
     @LocationId         = @PickLocationId output,
     @StorageUnitBatchId = @StorageUnitBatchId,
     @Quantity           = @Quantity
    
    exec @Error = p_Instruction_Update
     @InstructionId      = @InstructionId,
     @PickLocationId     = @PickLocationId
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId       = @InstructionId,
     @Store               = 0 -- false
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
--    exec @Error = p_Mobile_Exception_Stock_Take
--     @OperatorId          = @OperatorId,
--     @StorageUnitBatchId  = 1,
--     @PickLocationId      = @PickLocationId,
--     @Quantity            = @Quantity
--    
--    if @Error <> 0
--    begin
--      set @Error = 1
--      goto Result
--    end
    
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    set @Error = 0
    goto result
  end
  else if @Type = 6 -- Confirm Quantity
  begin
    exec @Error = p_Mobile_Pallet_Information
     @PalletId,
     @ProductCode output,
     @SKUCode     output,
     @Batch       output
    
    if @Error <> 0
    begin
      set @Error = 1
      goto Result
    end
    
    if @ConfirmedQuantity > @Quantity
    begin
      set @Error = 7
      goto Result
    end
    else if @ConfirmedQuantity = @Quantity
    begin
      set @Error = 0
      goto Result
    end
    else
    begin
      exec @Error = p_Instruction_Update
       @InstructionId       = @InstructionId,
       @ConfirmedQuantity   = @ConfirmedQuantity
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      set @StatusId = dbo.ufn_StatusId('I','W')
      
      set @Quantity = @Quantity - @ConfirmedQuantity
      
      exec @Error = p_Instruction_Insert
       @InstructionId       = @InstructionId output,
       @InstructionTypeId   = @InstructionTypeId,
       @StorageUnitBatchId  = @StorageUnitBatchId,
       @WarehouseId         = @WarehouseId,
       @StatusId            = @StatusId,
       @JobId               = @JobId,
       @OperatorId          = @OperatorId,
       @PickLocationId      = @PickLocationId,
       @StoreLocationId     = @StoreLocationId,
       @InstructionRefId    = null,
       @Quantity            = @Quantity,
       @ConfirmedQuantity   = 0,
       @Weight              = @Weight,
       @ConfirmedWeight     = 0,
       @PalletId            = @PalletId,
       @CreateDate          = @GetDate
      
      if @Error <> 0
      begin
        set @Error = 1
        goto Result
      end
      
      if (select dbo.ufn_Configuration(21, @WarehouseId)) = 1
      begin
        exec p_Location_Get
         @WarehouseId        = @WarehouseId,
         @LocationId         = @StoreLocationId output,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @Quantity           = @Quantity
        
        exec @Error = p_Instruction_Update
         @InstructionId      = @InstructionId,
         @StoreLocationId    = @StoreLocationId
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
        
        exec @Error = p_StorageUnitBatchLocation_Reserve
         @InstructionId       = @InstructionId,
         @Pick                = 0 -- false
        
        if @Error <> 0
        begin
          set @Error = 1
          goto Result
        end
      end
    end
    
    set @Error = 0
    goto result
  end
  
  result:
    select @Error             as 'Result',
           i.InstructionId    as 'InstructionId',
           @ProductCode       as 'ProductCode',
           @SKUCode           as 'SKUCode',
           @Batch             as 'Batch',
           pick.Location      as 'PickLocation',
           pick.SecurityCode  as 'PickSecurityCode',
           store.Location     as 'StoreLocation',
           store.SecurityCode as 'StoreSecurityCode',
           i.Quantity         as 'Quantity',
           i.PalletId         as 'PalletId'
      from Instruction     i (nolock)
      left outer
      join Location     pick (nolock) on i.PickLocationId   = pick.LocationId
      left outer
      join Location    store (nolock) on i.StoreLocationId = store.LocationId
    where InstructionId = @InstructionId
  
  return
  
  error:
    rollback transaction
    return @Error
end
