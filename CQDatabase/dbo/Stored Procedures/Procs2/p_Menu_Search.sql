﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Menu_Search
  ///   Filename       : p_Menu_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:57
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Menu table.
  /// </remarks>
  /// <param>
  ///   @MenuId int = null output,
  ///   @Menu nvarchar(100) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Menu.MenuId,
  ///   Menu.Menu 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Menu_Search
(
 @MenuId int = null output,
 @Menu nvarchar(100) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @MenuId = '-1'
    set @MenuId = null;
  
  if @Menu = '-1'
    set @Menu = null;
  
 
  select
         Menu.MenuId
        ,Menu.Menu
    from Menu
   where isnull(Menu.MenuId,'0')  = isnull(@MenuId, isnull(Menu.MenuId,'0'))
     and isnull(Menu.Menu,'%')  like '%' + isnull(@Menu, isnull(Menu.Menu,'%')) + '%'
  
end
