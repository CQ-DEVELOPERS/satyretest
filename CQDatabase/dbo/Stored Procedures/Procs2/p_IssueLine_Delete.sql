﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLine_Delete
  ///   Filename       : p_IssueLine_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2013 08:43:17
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the IssueLine table.
  /// </remarks>
  /// <param>
  ///   @IssueLineId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLine_Delete
(
 @IssueLineId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete IssueLine
     where IssueLineId = @IssueLineId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_IssueLineHistory_Insert
         @IssueLineId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
