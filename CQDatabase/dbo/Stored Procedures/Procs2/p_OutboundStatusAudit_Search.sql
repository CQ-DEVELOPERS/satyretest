﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundStatusAudit_Search
  ///   Filename       : p_OutboundStatusAudit_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:38
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OutboundStatusAudit table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OutboundStatusAudit.IssueId,
  ///   OutboundStatusAudit.ExternalCompanyId,
  ///   OutboundStatusAudit.CreateDate,
  ///   OutboundStatusAudit.PlanningComplete,
  ///   OutboundStatusAudit.Checking,
  ///   OutboundStatusAudit.Checked,
  ///   OutboundStatusAudit.Despatch,
  ///   OutboundStatusAudit.DespatchChecked,
  ///   OutboundStatusAudit.Complete,
  ///   OutboundStatusAudit.PickComplete,
  ///   OutboundStatusAudit.PickTotal,
  ///   OutboundStatusAudit.PickPercentage,
  ///   OutboundStatusAudit.CheckingComplete,
  ///   OutboundStatusAudit.CheckingTotal,
  ///   OutboundStatusAudit.CheckingPercentage 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundStatusAudit_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         OutboundStatusAudit.IssueId
        ,OutboundStatusAudit.ExternalCompanyId
        ,OutboundStatusAudit.CreateDate
        ,OutboundStatusAudit.PlanningComplete
        ,OutboundStatusAudit.Checking
        ,OutboundStatusAudit.Checked
        ,OutboundStatusAudit.Despatch
        ,OutboundStatusAudit.DespatchChecked
        ,OutboundStatusAudit.Complete
        ,OutboundStatusAudit.PickComplete
        ,OutboundStatusAudit.PickTotal
        ,OutboundStatusAudit.PickPercentage
        ,OutboundStatusAudit.CheckingComplete
        ,OutboundStatusAudit.CheckingTotal
        ,OutboundStatusAudit.CheckingPercentage
    from OutboundStatusAudit
  
end
