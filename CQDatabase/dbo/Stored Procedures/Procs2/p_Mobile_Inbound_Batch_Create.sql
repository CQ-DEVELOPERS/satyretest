﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Inbound_Batch_Create
  ///   Filename       : p_Mobile_Inbound_Batch_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Inbound_Batch_Create
(
 @receiptId            int,
 @storageUnitId        int,
 @batch                nvarchar(30),
 @batchReferenceNumber nvarchar(30),
 @expiryDate           nvarchar(20)
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @GetDate            datetime,
          @BatchId            int,
          @StorageUnitBatchId int,
          @StatusId           int,
          @WarehouseId        int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @WarehouseId = WarehouseId
    from Receipt
   where ReceiptId = @receiptId
  
  set @Error = 0
  
  select @BatchId = BatchId
    from Batch
   where Batch                = @batch
     and isnull(BatchReferenceNumber,'') = @batchReferenceNumber
  
  if @BatchId is null
  begin
    select @StatusId = dbo.ufn_StatusId('P','Q')
    
    insert Batch
          (StatusId,
           WarehouseId,
           Batch,
           BatchReferenceNumber,
           CreateDate)
--           ExpiryDate)
    select @StatusId,
           @WarehouseId,
           @batch,
           @batchReferenceNumber,
           @getdate
           --@expiryDate
    
    select @Error = @@Error
    
    if @Error != 0
    begin
      select @StorageUnitBatchId = -1
      goto result
    end
    
    select @BatchId = scope_identity()
  end
  
  select @StorageUnitBatchId = StorageUnitBatchId
    from StorageUnitBatch
   where StorageUnitId = @storageUnitId
     and BatchId       = @BatchId
  
  if @StorageUnitBatchId is null
  begin
    exec @Error = p_StorageUnitBatch_Insert
     @StorageUnitBatchId = @StorageUnitBatchId output,
     @BatchId            = @BatchId,
     @StorageUnitId      = @StorageUnitId
    
    if @Error != 0
    begin
      select @StorageUnitBatchId = -1
      goto result
    end
  end
  
  if @StorageUnitBatchId is null
  begin
    set @StorageUnitBatchId = -1
    goto result
  end
  
  result:
    select @StorageUnitBatchId
    return @StorageUnitBatchId
end
