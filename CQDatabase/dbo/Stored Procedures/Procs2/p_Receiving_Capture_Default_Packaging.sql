﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Capture_Default_Packaging
  ///   Filename       : p_Receiving_Capture_Default_Packaging.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Feb 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Capture_Default_Packaging
(
 @InboundShipmentId int,
 @ReceiptId         int
)

as
begin
	 set nocount on;
	 
	 declare @TablePacks as table
	 (
	  JobId        int,
	  PackType     nvarchar(30),
	  PackQuantity numeric(13,6),
	  JobQuantity  numeric(13,6)
	 )
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @PackageLineId      int,
          @ReceiptLineId      int,
          @JobId              int,
          @StorageUnitBatchId int,
          @Quantity           numeric(13,6),
          @StatusId           int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Receiving_Capture_Default_Packaging'
  select @StatusId = dbo.ufn_StatusId('R','R')
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @ReceiptId = -1
    set @ReceiptId = null
  
  if @InboundShipmentId is not null
  begin
    insert @TablePacks
          (JobId,
	          PackType,
	          PackQuantity,
	          JobQuantity)
    select i.JobId,
           pt.PackType,
           pk.Quantity,
           sum(i.ConfirmedQuantity)
      from InboundShipmentReceipt isr (nolock)
      join ReceiptLine             rl (nolock) on isr.ReceiptId        = rl.ReceiptId
      join Instruction              i (nolock) on rl.ReceiptLineId     = i.ReceiptLineId
      join StorageUnitBatch       sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join SKU                    sku (nolock) on su.SKUId             = sku.SKUId
      join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
                                              and i.WarehouseId        = pk.WarehouseId
      join PackType                pt (nolock) on pk.PackTypeId        = pt.PackTypeId
     where isr.InboundShipmentId = @InboundShipmentId
    group by i.JobId,
             pt.PackType,
             pk.Quantity
  end
  else if @ReceiptId is not null
  begin
    insert @TablePacks
          (JobId,
	          PackType,
	          PackQuantity,
	          JobQuantity)
    select i.JobId,
           pt.PackType,
           pk.Quantity,
           sum(i.ConfirmedQuantity)
      from ReceiptLine rl (nolock)
      join Instruction  i (nolock) on rl.ReceiptLineId           = i.ReceiptLineId
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
      join SKU              sku (nolock) on su.SKUId             = sku.SKUId
      join Pack              pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
                                        and i.WarehouseId        = pk.WarehouseId
      join PackType          pt (nolock) on pk.PackTypeId        = pt.PackTypeId
     where rl.ReceiptId = @ReceiptId
    group by i.JobId,
             pt.PackType,
             pk.Quantity
  end
  
  begin transaction
  
  set @JobId = -1
  
  while @JobId is not null
  begin
    set @JobId = null
    
    select @JobId = JobId
      from @TablePacks
    
    select @PackageLineId = PackageLineId
      from ReceiptLinePackaging (nolock)
     where JobId = @JobId
    
    if @PackageLineId is null
    begin
      set @StorageUnitBatchId = null
      
      select @StorageUnitBatchId = sub.StorageUnitBatchId,
             @Quantity           = round(convert(numeric(13,6), tp.JobQuantity / tp.PackQuantity),0)
        from StorageUnitBatch sub (nolock) 
        join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
        join Product            p (nolock) on su.ProductId          = p.ProductId
        join SKU              sku (nolock) on su.SKUId              = sku.SKUId
        join Batch              b (nolock) on sub.BatchId           = b.BatchId
        join Status             s (nolock) on b.StatusId            = s.StatusId
        join @TablePacks       tp          on p.ProductCode         = tp.PackType
       where b.Batch       = 'Default'
         and p.ProductType = 'PACKAGING'
         and tp.JobId      = @JobId
      
      if @StorageUnitBatchId is not null
      begin
        insert ReceiptLinePackaging
              (ReceiptId,
               ReceiptLineId,
               JobId,
               StorageUnitBatchId,
               StatusId,
               OperatorId,
               RequiredQuantity,
               ReceivedQuantity,
               AcceptedQuantity,
               RejectQuantity,
               DeliveryNoteQuantity)
        select @ReceiptId,
               @ReceiptLineId,
               @JobId,
               @StorageUnitBatchId,
               @StatusId,
               null,
               0,
               @quantity,
               @quantity,
               null,
               null
        
        select @Error = @@Error
        
        if @Error <> 0
          goto error
      end
    end
    
    delete @TablePacks where JobId = @JobId
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
