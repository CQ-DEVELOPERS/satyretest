﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItemCulture_Update
  ///   Filename       : p_MenuItemCulture_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:33
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the MenuItemCulture table.
  /// </remarks>
  /// <param>
  ///   @MenuItemCultureId int = null,
  ///   @MenuItemId int = null,
  ///   @CultureId int = null,
  ///   @MenuItem nvarchar(100) = null,
  ///   @ToolTip nvarchar(510) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItemCulture_Update
(
 @MenuItemCultureId int = null,
 @MenuItemId int = null,
 @CultureId int = null,
 @MenuItem nvarchar(100) = null,
 @ToolTip nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
  if @MenuItemCultureId = '-1'
    set @MenuItemCultureId = null;
  
  if @MenuItemId = '-1'
    set @MenuItemId = null;
  
  if @CultureId = '-1'
    set @CultureId = null;
  
	 declare @Error int
 
  update MenuItemCulture
     set MenuItemId = isnull(@MenuItemId, MenuItemId),
         CultureId = isnull(@CultureId, CultureId),
         MenuItem = isnull(@MenuItem, MenuItem),
         ToolTip = isnull(@ToolTip, ToolTip) 
   where MenuItemCultureId = @MenuItemCultureId
  
  select @Error = @@Error
  
  
  return @Error
  
end
