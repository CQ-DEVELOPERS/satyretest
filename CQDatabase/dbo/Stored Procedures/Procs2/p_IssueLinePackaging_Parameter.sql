﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLinePackaging_Parameter
  ///   Filename       : p_IssueLinePackaging_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:55
  /// </summary>
  /// <remarks>
  ///   Selects rows from the IssueLinePackaging table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   IssueLinePackaging.PackageLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLinePackaging_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as PackageLineId
        ,null as 'IssueLinePackaging'
  union
  select
         IssueLinePackaging.PackageLineId
        ,IssueLinePackaging.PackageLineId as 'IssueLinePackaging'
    from IssueLinePackaging
  
end
