﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IssueLine_Insert
  ///   Filename       : p_IssueLine_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Mar 2013 08:43:16
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the IssueLine table.
  /// </remarks>
  /// <param>
  ///   @IssueLineId int = null output,
  ///   @IssueId int = null,
  ///   @OutboundLineId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuatity float = null,
  ///   @Weight float = null,
  ///   @ConfirmedWeight float = null 
  /// </param>
  /// <returns>
  ///   IssueLine.IssueLineId,
  ///   IssueLine.IssueId,
  ///   IssueLine.OutboundLineId,
  ///   IssueLine.StorageUnitBatchId,
  ///   IssueLine.StatusId,
  ///   IssueLine.OperatorId,
  ///   IssueLine.Quantity,
  ///   IssueLine.ConfirmedQuatity,
  ///   IssueLine.Weight,
  ///   IssueLine.ConfirmedWeight 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IssueLine_Insert
(
 @IssueLineId int = null output,
 @IssueId int = null,
 @OutboundLineId int = null,
 @StorageUnitBatchId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @Quantity float = null,
 @ConfirmedQuatity float = null,
 @Weight float = null,
 @ConfirmedWeight float = null 
)

as
begin
	 set nocount on;
  
  if @IssueLineId = '-1'
    set @IssueLineId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
  if @OutboundLineId = '-1'
    set @OutboundLineId = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
	 declare @Error int
 
  insert IssueLine
        (IssueId,
         OutboundLineId,
         StorageUnitBatchId,
         StatusId,
         OperatorId,
         Quantity,
         ConfirmedQuatity,
         Weight,
         ConfirmedWeight)
  select @IssueId,
         @OutboundLineId,
         @StorageUnitBatchId,
         @StatusId,
         @OperatorId,
         @Quantity,
         @ConfirmedQuatity,
         @Weight,
         @ConfirmedWeight 
  
  select @Error = @@Error, @IssueLineId = scope_identity()
  
  if @Error = 0
    exec @Error = p_IssueLineHistory_Insert
         @IssueLineId = @IssueLineId,
         @IssueId = @IssueId,
         @OutboundLineId = @OutboundLineId,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @StatusId = @StatusId,
         @OperatorId = @OperatorId,
         @Quantity = @Quantity,
         @ConfirmedQuatity = @ConfirmedQuatity,
         @Weight = @Weight,
         @ConfirmedWeight = @ConfirmedWeight,
         @CommandType = 'Insert'
  
  return @Error
  
end
