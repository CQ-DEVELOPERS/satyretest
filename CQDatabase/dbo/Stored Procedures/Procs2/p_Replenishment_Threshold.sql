﻿
/*
 /// <summary>
 ///   Procedure Name : p_Replenishment_Threshold
 ///   Filename       : p_Replenishment_Threshold.sql
 ///   Create By      : Grant Schultz
 ///   Date Created   : 15 Mar 2010
 /// </summary>
 /// <remarks>
 ///   
 /// </remarks>
 /// <param>
 ///   
 /// </param>
 /// <returns>
 ///   
 /// </returns>
 /// <newpara>
 ///   Modified by    : 
 ///   Modified Date  : 
 ///   Details        : 
 /// </newpara>
*/
CREATE procedure p_Replenishment_Threshold
(
@WarehouseId int
)

as
begin
	 set nocount on;

 declare @TableProducts as table
 (
  StorageUnitId     int,
  ProductCategory    nvarchar(1),
  MovementCategory  int,
  MinimumQuantity   float,
  ReorderQuantity   float,
  MaximumQuantity   float,
  SOH               float default 0,
  Percentage        float
 )

 declare @TableLocations as table
 (
  LocationId int
 )

 declare @Error             int,
         @Errormsg          nvarchar(500),
         @GetDate           datetime,
         @EmptyLocations    int,
         @StorageUnitId     int,
         @StorageUnitBatchId int,
         @StoreLocationId   int,
         @Quantity          numeric(13,6),
         @InstructionId     int

 select @GetDate = dbo.ufn_Getdate()

 begin transaction

 set @EmptyLocations = 0

 exec p_Location_Update_Used

 --insert @TableLocations
 --      (LocationId)
 --select LocationId
 --  from viewLocation
 -- where WarehouseId = @WarehouseId
 --   and AreaCode    = 'RK'
 --   and AreaType    = ''
 --   and Used        = 0

 --select @EmptyLocations = @@rowcount

 --select @EmptyLocations as '@EmptyLocations'

 set rowcount @EmptyLocations
 
 insert @TableProducts
       (StorageUnitId,
        ProductCategory,
        MovementCategory,
        MinimumQuantity,
        ReorderQuantity,
        MaximumQuantity)
 select su.StorageUnitId,
        su.ProductCategory,
        su.MovementCategory,
        sua.MinimumQuantity,
        sua.ReorderQuantity,
        sua.MaximumQuantity
   from StorageUnit                su (nolock)
   join StorageUnitArea           sua (nolock) on su.StorageUnitId       = sua.StorageUnitId
   join Area                        a (nolock) on sua.AreaId             = a.AreaId
--    left
--    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
  where a.WarehouseId = @WarehouseId
    and a.StockOnHand = 1
    and a.AreaCode in ('BK','RK','SP')
    and a.Replenish = 1
    and sua.MinimumQuantity is not null
    and exists(select 1
                 from StorageUnitBatch          sub (nolock)
                 join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                 join AreaLocation               al (nolock) on subl.LocationId        = al.LocationId
                 join Area                        a (nolock) on al.AreaId              = a.AreaId
                where su.StorageUnitId      = sub.StorageUnitId
                  and subl.ActualQuantity   > subl.ReservedQuantity
                  and a.WarehouseId         = @WarehouseId
                  and a.StockOnHand         = 1
                  and a.AreaCode           in ('RK','BK','SP')
                  and isnull(a.Replenish,0) = 0)
    and not exists(select 1
                     from Instruction        i (nolock)
                     join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
                     join Status             s (nolock) on i.StatusId           = s.StatusId
                     join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
                     join AreaLocation      al (nolock) on i.StoreLocationId    = al.LocationId
                     join Area              a2 (nolock) on al.AreaId            = a2.AreaId
                    where s.StatusCode in ('W','S')
                      and i.WarehouseId          = @WarehouseId       -- Same Warehouse (not really necessary)
                      and sub.StorageUnitId      = su.StorageUnitId   -- Same product
                      and it.InstructionTypeCode = 'R'
                      and a.AreaType != a2.AreaType)
 group by su.StorageUnitId,
          su.ProductCategory,
          su.MovementCategory,
          sua.MinimumQuantity,
          sua.ReorderQuantity,
          sua.MaximumQuantity
 order by su.ProductCategory,
          su.MovementCategory desc
 set rowcount 0
 
 update tp
    set SOH = (select sum(subl.ActualQuantity + subl.AllocatedQuantity - subl.ReservedQuantity)
                 from StorageUnitBatch          sub (nolock)
                 join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                 join AreaLocation               al (nolock) on subl.LocationId        = al.LocationId
                 join Area                        a (nolock) on al.AreaId              = a.AreaId
                where tp.StorageUnitId = sub.StorageUnitId
                  and a.StockOnHand = 1
                  and a.AreaType = ''
                  and a.AreaCode in ('RK','BK','SP')
                  and a.Replenish = 1)
   from @TableProducts tp
 
 update @TableProducts
    set SOH = 0
  where SOH is null
 
 update @TableProducts
    set Percentage = (SOH / MinimumQuantity) * 100
 
 select @EmptyLocations = count(1) from @TableProducts where Percentage < 100
 
 set @StoreLocationId = null

 select top 1 @StoreLocationId = l.LocationId
   from Area a
   join AreaLocation al on a.AreaId = al.AreaId
   join Location l on al.LocationId = l.LocationId
  where a.AreaCode = 'SP'
    and a.Replenish = 1

 select @EmptyLocations as '@EmptyLocations', * from @TableProducts where Percentage < 100

 while @EmptyLocations > 0
 begin
   select @EmptyLocations = @EmptyLocations - 1

   set @StorageUnitId = null

   select top 1 @StorageUnitId = StorageUnitId
     from @TableProducts
    where Percentage < 100
   order by Percentage

   delete @TableProducts where StorageUnitId = @StorageUnitId

   select top 1 @Quantity = Quantity
     from Pack      p (nolock)
     join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
    where StorageUnitId = @StorageUnitId
      and WarehouseId = @WarehouseId
   order by pt.OutboundSequence

   if @StorageUnitId is null
   begin
     set @EmptyLocations = 0
   end
   else
   begin
     select @StorageUnitBatchId = StorageUnitBatchId
       from StorageUnitBatch sub (nolock)
       join Batch              b (nolock) on sub.BatchId = b.BatchId
      where sub.StorageUnitId = @StorageUnitId
        and b.Batch = 'Default'

     if @StorageUnitBatchId is null
       select @StorageUnitBatchId = StorageUnitBatchId
         from StorageUnitBatch sub (nolock)
         join Batch              b (nolock) on sub.BatchId = b.BatchId
        where sub.StorageUnitId = @StorageUnitId
     
     set @InstructionId = null
     
     exec @Error = p_Mobile_Replenishment_Request
      @storeLocationId    = @StoreLocationId,
      @storageUnitBatchId = @StorageUnitBatchId,
      @operatorId         = null,
      @ShowMsg            = 0,
      @ManualReq          = 0,
      @AnyLocation        = 1,
      @AreaType           = 'Backup',
      @InstructionId      = @InstructionId output
     
     if @Error <> 0
       select @Error as '@Error', @StorageUnitId as '@StorageUnitId', @StoreLocationId as '@StoreLocationId'
     else if @InstructionId is not null
     begin
       exec @Error = p_Replenishment_Create_Pick
        @InstructionId = @InstructionId
       
       if @Error <> 0
         goto Error
     end
   end
 end

 declare @PriorityId int

 select top 1 @PriorityId = PriorityId
   from Priority
 order by OrderBy desc

 update j
    set PriorityId = @PriorityId
   from Instruction        i (nolock)
   join Status            si (nolock) on i.StatusId           = si.StatusId
   join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
   join Job                j (nolock) on i.JobId              = j.JobId
   left
   join viewLocation    pick (nolock) on i.PickLocationId     = pick.LocationId
   left
   join viewLocation   store (nolock) on i.StoreLocationId    = store.LocationId
  where si.StatusCode = 'W'
    and it.InstructionTypeCode = 'R'
    and pick.AreaCode = 'BK'
    and store.AreaCode = 'RK'
    and j.PriorityId != @PriorityId

 --declare	@Command nvarchar(max)

 --declare replen_cursor cursor for
 --select 'exec p_Housekeeping_Instruction_Delete @InstructionId = ' + convert(nvarchar(10), InstructionId)
 --  from Instruction i
 --  join Status            si (nolock) on i.StatusId           = si.StatusId
 --  join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
 --  join Job                j (nolock) on i.JobId              = j.JobId
 --  left
 --  join viewLocation    pick (nolock) on i.PickLocationId     = pick.LocationId
 --  left
 --  join viewLocation   store (nolock) on i.StoreLocationId    = store.LocationId
 -- where si.StatusCode = 'W'
 --   and it.InstructionTypeCode = 'R'
 --   and pick.AreaCode in ('RK','SP')
 --   and store.AreaCode = 'RK'

 --open replen_cursor

 --fetch replen_cursor into @Command

 --while (@@fetch_status = 0)
 --begin
 --  execute(@Command)

 --  fetch replen_cursor into @Command
 --end

 --close replen_cursor
 --deallocate replen_cursor

 commit transaction
 return

 error:
   RAISERROR (900000,-1,-1, 'Error executing p_Replenishment_Threshold'); 
   rollback transaction
   return @Error
end
