﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Batch
  ///   Filename       : p_Mobile_Product_Check_Batch.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Feb 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Batch
(
 @WarehouseId     int,
 @jobId           int,
 @storageUnitId   int,
 @batch           nvarchar(50)
)

as
begin
	 set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         WarehouseId,
         JobId,
         StorageUnitId,
         Batch,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @WarehouseId,
         @JobId,
         @StorageUnitId,
         @Batch,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error             int,
          @Errormsg          nvarchar(500)
  
  if dbo.ufn_Configuration(338, @WarehouseId) = 0
  begin
    print 0
    select StorageUnitBatchId,
           ProductCode,
           Product,
           SKUCode,
           'Default' as 'Batch',
           ExpiryDate,
           sum(ConfirmedQuantity) as 'Quantity',
           Lines,
           ErrorMsg
      from CheckingProduct (nolock)
     where JobId = @jobId
       and StorageUnitId = @storageUnitId
    group by StorageUnitBatchId,
             ProductCode,
             Product,
             SKUCode,
             --Batch,
             ExpiryDate,
             Lines,
             ErrorMsg
  end
else if dbo.ufn_Configuration(169, @WarehouseId) = 1
  begin
    select StorageUnitBatchId,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           ExpiryDate,
           sum(ConfirmedQuantity) as 'Quantity',
           Lines,
           ErrorMsg
      from CheckingProduct (nolock)
     where JobId = @jobId
       and StorageUnitId = @storageUnitId
    group by StorageUnitBatchId,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ExpiryDate,
             Lines,
             ErrorMsg
  end
  else if dbo.ufn_Configuration(34, @WarehouseId) = 1
  begin
    select top 1
           StorageUnitBatchId,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           ExpiryDate,
           sum(ConfirmedQuantity) as 'Quantity',
           Lines,
           ErrorMsg
      from CheckingProduct (nolock)
     where JobId = @jobId
       and StorageUnitId = @storageUnitId
       and batch         = @batch
    group by StorageUnitBatchId,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ExpiryDate,
             Lines,
             ErrorMsg
  end
  else
  begin
    select top 1
           sub2.StorageUnitBatchId,
           cp.ProductCode,
           cp.Product,
           cp.SKUCode,
           b.Batch,
           b.ExpiryDate,
           sum(ConfirmedQuantity) as 'Quantity',
           Lines,
           ErrorMsg
      from CheckingProduct    cp (nolock)
      join StorageUnitBatch sub2 (nolock) on cp.StorageUnitId   = sub2.StorageUnitId
      join Batch               b (nolock) on sub2.BatchId       = b.BatchId
     where cp.JobId = @jobId
       and cp.StorageUnitId = @storageUnitId
       and b.batch          = 'Default'
    group by sub2.StorageUnitBatchId,
             cp.ProductCode,
             cp.Product,
             cp.SKUCode,
             b.Batch,
             b.ExpiryDate,
             Lines,
             ErrorMsg
  end
  
  update MobileLog
     set EndDate = getdate()
   where MobileLogId = @MobileLogId
end
