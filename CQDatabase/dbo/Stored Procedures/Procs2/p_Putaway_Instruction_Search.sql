﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Putaway_Instruction_Search
  ///   Filename       : p_Putaway_Instruction_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Putaway_Instruction_Search
(
 @warehouseId int,
 @palletId    int,
 @jobId       int,
 @fromDate    datetime,
 @toDate      datetime,
 @statusId    int,
 @productCode nvarchar(30),
 @product     nvarchar(50),
 @skuCode     nvarchar(50),
 @batch       nvarchar(30)
)

as
begin
	 set nocount on;
  declare @TableHeader as table
  (
   OrderNumber       nvarchar(30),
   InboundShipmentId int null,
   ReceiptId         int,
   ReceiptLineId     int
  )
  
  declare @TableResult as table
  (
   InstructionId      int,
   ReceiptLineId      int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   ProductCode	      nvarchar(30) null,
   Product	          nvarchar(255) null,
   SKUCode            nvarchar(50) null,
   Quantity           float null,
   ConfirmedQuantity  float null,
   JobId              int null,
   StatusId           int null,
   Status             nvarchar(50) null,
   PickLocationId     int null,
   PickLocation       nvarchar(15) null,
   StoreLocationId    int null,
   StoreLocation      nvarchar(15) null,
   StoreArea          nvarchar(50) null,
   PalletId           int null,
   CreateDate         datetime null,
   Batch              nvarchar(50) null,
   ECLNumber          nvarchar(10) null,
   InstructionType	  nvarchar(30),
   OperatorId         int null,
   Operator           nvarchar(50) null,
   PalletIdStatus	  nvarchar(50) null,
   PalletIdStatusId	  int null
  )
  
  declare @GetDate           datetime,
          @StatusCode        nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @palletId = -1
    set @palletId = null
  
  if @jobId = -1
    set @jobId = null
  
  if @statusId = -1
    set @statusId = null
  
  select @StatusCode = StatusCode
    from Status
   where StatusId = @statusId
  
  if @StatusCode is null
    insert @TableResult
          (InstructionId,
           ReceiptLineId,
           StorageUnitBatchId,
           Quantity,
           ConfirmedQuantity,
           JobId,
           StatusId,
           PickLocationId,
           StoreLocationId,
           PalletId,
           CreateDate,
           InstructionType,
           OperatorId,
           Status,
           PalletIdStatus,
           PalletIdStatusId)
    select i.InstructionId,
           i.ReceiptLineId,
           i.StorageUnitBatchId,
           i.Quantity,
           i.ConfirmedQuantity,
           i.JobId,
           j.StatusId,
           i.PickLocationId,
           i.StoreLocationId,
           i.PalletId,
           i.CreateDate,
           it.InstructionType,
           j.OperatorId,
           sj.Status,
           sp.Status,
           sp.StatusId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Job              j (nolock) on i.JobId             = j.JobId
      join Pallet			p (nolock) on i.PalletId		  = p.PalletId
      join Status          sj (nolock) on j.StatusId          = sj.StatusId
      join Status          si (nolock) on i.StatusId          = si.StatusId
      join Status		   sp (nolock) on p.StatusId		  = sp.StatusId
     where i.CreateDate      between @FromDate and @ToDate
       --and sj.Type                 in ('R','PR','I')
       and si.StatusCode           in ('W','S')
       and i.JobId                 = isnull(@JobId, i.JobId)
       and isnull(i.PalletId,-1)   = isnull(@palletId, isnull(i.PalletId,-1))
       --and sj.StatusId              = isnull(@StatusId, sj.StatusId)
       and((it.InstructionTypeCode  = 'PR'       and sj.StatusCode in ('A','S'))
       or  (it.InstructionTypeCode in ('S','SM') and sj.StatusCode in ('PR','A','S')))
       and i.WarehouseId           = @warehouseId
  else
    insert @TableResult
          (InstructionId,
           ReceiptLineId,
           StorageUnitBatchId,
           Quantity,
           ConfirmedQuantity,
           JobId,
           StatusId,
           PickLocationId,
           StoreLocationId,
           PalletId,
           CreateDate,
           InstructionType,
           OperatorId,
           Status,
           PalletIdStatus,
           PalletIdStatusId)
    select i.InstructionId,
           i.ReceiptLineId,
           i.StorageUnitBatchId,
           i.Quantity,
           i.ConfirmedQuantity,
           i.JobId,
           j.StatusId,
           i.PickLocationId,
           i.StoreLocationId,
           i.PalletId,
           i.CreateDate,
           it.InstructionType,
           j.OperatorId,
           sj.Status,
           sp.Status,
           sp.StatusId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
      join Job              j (nolock) on i.JobId             = j.JobId
      join Pallet			p (nolock) on i.PalletId		  = p.PalletId
      join Status          sj (nolock) on j.StatusId          = sj.StatusId
      join Status          si (nolock) on i.StatusId          = si.StatusId
      join Status		   sp (nolock) on p.StatusId		  = sp.StatusId
     where i.CreateDate      between @FromDate and @ToDate
       --and sj.Type                 in ('R','PR','I')
       and sj.StatusCode            = @StatusCode
       and si.StatusCode           in ('W','S')
       and i.JobId                 = isnull(@JobId, i.JobId)
       and isnull(i.PalletId,-1)   = isnull(@palletId, isnull(i.PalletId,-1))
       --and sj.StatusId              = isnull(@StatusId, sj.StatusId)
       and it.InstructionTypeCode in ('S','SM','PR')
       and i.WarehouseId           = @warehouseId
  
  insert @TableHeader
        (OrderNumber,
         InboundShipmentId,
         ReceiptId,
         ReceiptLineId)
  select distinct
         id.OrderNumber,
         isr.InboundShipmentId,
         r.ReceiptId,
         rl.ReceiptLineId
    from InboundDocument         id (nolock)
    join Receipt                  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join ReceiptLine             rl (nolock) on r.ReceiptId          = rl.ReceiptId
    left outer
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId          = isr.ReceiptId
    join @TableResult            tr          on rl.ReceiptLineId     = tr.ReceiptLineId
  
  update tr
     set StorageUnitId = su.StorageUnitId,
         ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         ECLNumber     = b.ECLNumber
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit         su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId         = p.ProductId
    join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
    join Batch               b   (nolock) on sub.BatchId          = b.BatchId

  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId

  update tr
     set StoreLocation = l.Location,
         StoreArea     = a.Area
    from @TableResult tr
    join Location      l (nolock) on tr.StoreLocationId = l.LocationId
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId = a.AreaId

  update tr
     set StoreArea     = a.Area
    from @TableResult     tr
    join StorageUnitArea sua (nolock) on tr.StorageUnitId = sua.StorageUnitId
    join Area              a (nolock) on sua.AreaId       = a.AreaId
   where sua.StoreOrder = 1
     and a.AreaCode in ('RK','BK')
     and a.AreaType = ''
     and a.WarehouseId = @warehouseId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator     o  (nolock) on tr.OperatorId = o.OperatorId
  
  select InstructionId,
         ProductCode,
         Product,
         SKUCode,
         Quantity,
         ConfirmedQuantity,
         JobId,
         Status,
         PickLocation,
         StoreLocation,
         StoreArea,
         PalletId,
         datediff(mi, CreateDate, @GetDate) as DwellTime,
         CreateDate,
         InstructionType,
         -1 as 'OperatorId',
         Operator,
         OrderNumber,
         PalletIdStatus,
         PalletIdStatusId
    from @TableResult tr
    left outer
    join @TableHeader th on tr.ReceiptLineId = th.ReceiptLineId
   where ProductCode like '%' + @productCode + '%'
     and Product     like '%' + @product + '%'
     and SKUCode     like '%' + @skuCode + '%'
     and Batch       like '%' + @batch + '%'
end
 
