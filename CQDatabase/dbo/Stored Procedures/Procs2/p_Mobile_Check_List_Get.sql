﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_List_Get
  ///   Filename       : p_Mobile_Check_List_Get.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Mar 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Check_List_Get
(
 @warehouseId     int,
 @operatorId      int,
 @referenceNumber nvarchar(30)
)

as
begin
	 set nocount on;
	 
	 declare @PalletId int,
	         @JobId    int
	 
	 if isnumeric(replace(@referenceNumber,'J:','')) = 1
	 begin
    select @JobId           = replace(@referenceNumber,'J:','')
  end
  
  if isnumeric(replace(@referenceNumber,'P:','')) = 1
  begin
    select @PalletId        = replace(@referenceNumber,'P:','')
    
    select @JobId = JobId
      from viewili
     where WarehouseId = @warehouseId
       and PalletId    = @PalletId
  end
  
  if isnumeric(replace(@referenceNumber,'R:','')) = 1
  begin
    select @JobId = JobId 
      from Job (nolock)
     where WarehouseId     = @warehouseId
       and ReferenceNumber = @referenceNumber
  end
  
  select i.JobId,
         min(sub.StorageUnitBatchId) as 'StorageUnitBatchId',
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sum(i.Quantity) as 'Quantity',
         sum(i.ConfirmedQuantity) as 'ConfirmedQuantity',
         sum(isnull(i.CheckQuantity,0)) as 'CheckQuantity',
         case when sum(isnull(i.CheckQuantity,0)) = 0 and sum(i.ConfirmedQuantity) != 0
              then 0
              else 1
              end as 'OrderBy'
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product            p (nolock) on su.ProductId         = p.ProductId
    join SKU              sku (nolock) on su.SKUId             = sku.SKUId
   where i.JobId = @JobId
  group by i.JobId,
           su.StorageUnitId,
           p.ProductCode,
           p.Product,
           sku.SKUCode
  order by 'OrderBy', p.ProductCode
end
