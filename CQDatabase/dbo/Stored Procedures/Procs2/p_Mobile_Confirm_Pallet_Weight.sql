﻿--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Confirm_Pallet_Weight
  ///   Filename       : p_Mobile_Confirm_Pallet_Weight.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Confirm_Pallet_Weight
(
 @instructionId   int output,
 @confirmedWeight float,
 @Debug           bit = 0
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @GetDate            datetime,
          @WarehouseId        int,
          @StatusId           int,
          @Weight             float,
          @JobId              int,
          @TareWeight         float,
          @InstructionTypeCode varchar(10)  
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Error = 0
    
  select @WarehouseId         = i.WarehouseId,
         @StatusId            = i.StatusId,
         @JobId               = j.JobId,
         @Weight              = j.Weight,
         @TareWeight          = j.TareWeight,
         @InstructionTypeCode = it.InstructionTypeCode
    from Instruction i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job         j (nolock) on i.JobId = j.JobId
   where i.InstructionId = @instructionId
  
  update i
     set ConfirmedWeight = (pk.Weight * i.ConfirmedQuantity)
    from Instruction        i
    join StorageUnitBatch sub on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su on sub.StorageUnitId    = su.StorageUnitId
    join Pack              pk on su.StorageUnitId     = pk.StorageUnitId
    join PackType          pt on pk.PackTypeId        = pt.PackTypeId
   where pt.OutboundSequence in (select max(InboundSequence) from PackType)
     and isnull(su.ProductCategory,'') != 'V'
     and i.JobId = @JobId
  
  if @InstructionTypeCode = 'P' and @TareWeight is null
    set @TareWeight = 30
  
  if @InstructionTypeCode in ('PR','S','SM')
    select @Weight = Weight
      from Job
     where JobId = @JobId
  else
    select @Weight = sum(ConfirmedWeight) + @TareWeight
      from Instruction
     where JobId = @JobId
  
  --if @StatusId not in (select StatusId
  --                       from Status (nolock)
  --                      where (Type in ('I','PR')
  --                        and StatusCode in ('W','S'))
  --                        or (StatusCode in ('F'))) -- Picking - must be finished
  --begin
  --  set @Error = 5 -- Invliad Status
  --  goto Result
  --end
  
  if @Weight is null
    set @Weight = 0
  
  if @Debug = 1
    select @Weight as 'GrossWeight', @ConfirmedWeight as 'ConfirmedWeight', @TareWeight as 'TareWeight', @Weight + @Weight * -.11 as 'Minimum', @Weight + @Weight * .11 as 'Maximum'
  if @confirmedWeight not between @Weight + @Weight * -.11 and @Weight + @Weight * .11
  begin
    set @Error = 8
    goto Result
  end
  
  result:
    select @Error
    return @Error
end

