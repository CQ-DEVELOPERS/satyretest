﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Package_Line_Search
  ///   Filename       : p_Package_Line_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Package_Line_Search
(
 @ReceiptId int 
)
as
begin
	 set nocount on;
  
  select p.ProductCode,
		 p.Product, 
		sku.SKUCode,
		rl.PackageLineId,
         rl.ReceiptId,
         id.OrderNumber,
         rl.RequiredQuantity,
         rl.AcceptedQuantity,
         rl.RejectQuantity,
         rl.DeliveryNoteQuantity,
         s.Status,
		 sub.BatchId,
		b.Batch,
		 sub.StorageUnitId,
         rl.StorageUnitBatchId,
         r.CreditAdviceIndicator
    from ReceiptLinePackaging rl
    join Receipt                  r (nolock) on rl.ReceiptId        = r.ReceiptId
    join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    join Status                   s (nolock) on rl.StatusId         = s.StatusId
	Join StorageUnitBatch		sub (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
	Join StorageUnit			su (nolock) on su.StorageUnitId = sub.StorageunitId
	JOin Product				p (nolock) on p.ProductId = su.ProductId
	JOin SKU					sku	(nolock) on sku.Skuid = su.SkuId
	Join Batch					b (nolock) on b.BatchId = sub.BatchId
   where rl.ReceiptId          = @ReceiptId
     and s.Type = 'R'
     and s.StatusCode in ('W','R','P','S','LA')
end
