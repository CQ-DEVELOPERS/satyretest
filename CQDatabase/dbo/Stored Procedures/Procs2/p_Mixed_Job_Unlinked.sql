﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mixed_Job_Unlinked
  ///   Filename       : p_Mixed_Job_Unlinked.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mixed_Job_Unlinked
(
 @InstructionTypeCode nvarchar(10),
 @JobId               int,
 @ShipmentId          int,
 @OrderNumber         nvarchar(30),
 @ProductCode         nvarchar(30),
 @Product             nvarchar(50),
 @Location            nvarchar(15)
)

as
begin
	 set nocount on;
	 
	 declare @TableIssueLines as table
	 (
	  IssueLineId int
	 )
	 
	 declare @TableReceiptLines as table
	 (
	  ReceiptLineId int
	 )
	 
	 declare @TableResult as table
	 (
	  InstructionId      int,
   JobId              int,
   StorageUnitBatchId int,
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Quantity           float,
   Location           nvarchar(15),
   CreateDate         datetime,
   ReceiptId          int,
   ReceiptLineId      int,
   IssueId            int,
   IssueLineId        int,
   ShipmentId         int,
   OrderNumber        nvarchar(30)
	 )
  
  declare @StatusId int,
          @Status   nvarchar(50)
  
  select @StatusId = StatusId,
         @Status   = Status
    from Status (nolock)
   where Type       = 'I'
     and StatusCode = 'W'
  
  if @ShipmentId = -1
    set @ShipmentId = null
  
  if @OrderNumber = '-1'
    set @OrderNumber = null
  
  if @ShipmentId is null and @OrderNumber is null
  begin
    if @InstructionTypeCode = 'PM'
      insert @TableIssueLines
            (IssueLineId)
      select IssueLineId
        from OutboundDocument od (nolock)
        join Issue             i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
        join IssueLine        il (nolock) on i.IssueId             = il.IssueId
    
    if @InstructionTypeCode = 'SM'
      insert @TableReceiptLines
            (ReceiptLineId)
      select ReceiptLineId
        from InboundDocument id (nolock)
        join Receipt          r (nolock) on id.InboundDocumentId = r.InboundDocumentId
        join ReceiptLine     rl (nolock) on r.ReceiptId          = rl.ReceiptId
  end
  else 
  if @ShipmentId is not null
  begin
    if @InstructionTypeCode = 'PM'
      insert @TableIssueLines
            (IssueLineId)
      select IssueLineId
        from OutboundShipmentIssue osi (nolock)
        join IssueLine              il (nolock) on osi.IssueId = il.IssueId
       where osi.OutboundShipmentId = @ShipmentId
    
    if @InstructionTypeCode = 'SM'
      insert @TableReceiptLines
            (ReceiptLineId)
      select ReceiptLineId
        from InboundShipmentReceipt isr (nolock)
        join ReceiptLine             rl (nolock) on isr.ReceiptId = rl.ReceiptId
       where isr.InboundShipmentId = @ShipmentId
  end
  else if @OrderNumber is not null
  begin
    if @InstructionTypeCode = 'PM'
      insert @TableIssueLines
            (IssueLineId)
      select IssueLineId
        from OutboundDocument od (nolock)
        join Issue             i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
        join IssueLine        il (nolock) on i.IssueId             = il.IssueId
       where od.OrderNumber = @OrderNumber
    
    if @InstructionTypeCode = 'SM'
      insert @TableReceiptLines
            (ReceiptLineId)
      select ReceiptLineId
        from InboundDocument id (nolock)
        join Receipt          r (nolock) on id.InboundDocumentId = r.InboundDocumentId
        join ReceiptLine     rl (nolock) on r.ReceiptId          = rl.ReceiptId
       where id.OrderNumber = @OrderNumber
  end
  
  insert @TableResult
        (InstructionId,
         JobId,
         StorageUnitBatchId,
         Quantity,
         ReceiptLineId,
         IssueLineId)
  select i.InstructionId,
         i.JobId,
         i.StorageUnitBatchId,
         i.Quantity,
         i.ReceiptLineId,
         i.IssueLineId
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
   where i.StatusId             = @StatusId
     and it.InstructionTypeCode in ('PM','PS','FM','PR')
     and not exists(select 1 from Instruction i2 (nolock) where i.JobId = i2.JobId and i.InstructionId != i2.InstructionId)
     and (exists(select 1 from @TableIssueLines til where i.IssueLineId = til.IssueLineId)
      or  exists(select 1 from @TableReceiptLines trl where i.ReceiptLineId = trl.ReceiptLineId))
  union
  select i.InstructionId,
         i.JobId,
         i.StorageUnitBatchId,
         i.Quantity,
         i.ReceiptLineId,
         i.IssueLineId
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
   where i.StatusId             = @StatusId
     and i.OutboundShipmentId   = @ShipmentId
     and it.InstructionTypeCode in ('PM','PS','FM','PR')
     --and not exists(select 1 from Instruction i2 (nolock) where i.JobId = i2.JobId and i.InstructionId != i2.InstructionId)
  
  if exists(select 1 from @TableResult where IssueLineId is not null)
  begin
    update tr
       set OrderNumber = od.OrderNumber,
           IssueId     = i.IssueId
      from @TableResult     tr
      join IssueLine        il (nolock) on tr.IssueLineId = il.IssueLineId
      join Issue             i (nolock) on il.IssueId     = i.IssueId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
    
    update tr
       set ShipmentId = osi.OutboundShipmentId
      from @TableResult           tr
      join OutboundShipmentIssue osi (nolock) on tr.IssueId = osi.IssueId
  end
  
  if exists(select 1 from @TableResult where ReceiptLineId is not null)
  begin
    update tr
       set OrderNumber   = id.OrderNumber,
           ReceiptId     = r.ReceiptId
      from @TableResult    tr
      join ReceiptLine     rl (nolock) on tr.ReceiptLineId = rl.ReceiptLineId
      join Receipt          r (nolock) on rl.ReceiptId     = r.ReceiptId
      join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    
    update tr
       set ShipmentId = isi.InboundShipmentId
      from @TableResult           tr
      join InboundShipmentReceipt isi (nolock) on tr.ReceiptId = isi.ReceiptId
  end
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product            p (nolock) on su.ProductId         = p.ProductId
    join SKU              sku (nolock) on su.SKUId             = sku.SKUId
  
  select InstructionId,
         JobId,
         OrderNumber,
         ProductCode,
         SKUCode,
         Quantity
    from @TableResult
   where ProductCode         like isnull(@ProductCode,'%') + '%'
     and Product             like isnull(@Product,'%') + '%'
     and isnull(Location,'') like isnull(@Location,'%') + '%'
     and JobId                 != isnull(@JobId, 0)
  order by SKUCode,
           Quantity
end
