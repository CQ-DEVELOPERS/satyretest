﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Backorder_Planning
  ///   Filename       : p_Report_Backorder_Planning.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure dbo.p_Report_Backorder_Planning
(
 @WarehouseId            int,
 @StorageUnitId          int = null,
 @OutboundDocumentTypeId int = null,
 @ExternalCompanyId      int = null
)
 
as
begin
	 set nocount on;
	 
	 /*
	 if @WarehouseId = -1
	    set @WarehouseId = null
	 
	 if @OutboundDocumentTypeId = -1
	    set @OutboundDocumentTypeId = null
	    
	 if @ExternalCompanyCode = -1
	    set @ExternalCompanyCode = null
	    
     if @ExternalCompany = -1
        set @ExternalCompany = null
        
     if @OrderNumber = -1
        set @OrderNumber = null
        
     if @ProductCode = -1
        set @ProductCode = null
        
     if @Product = -1
        set @Product = null
      
     if @SKUCode = -1
        set @SKUCode = null     
    */
    --Latin1_General_CI_AS
  declare @TableResult as
  table(
        WarehouseId                 int,
        IssueId                     int,
        IssueLineId                 int,
        OrderNumber                 nvarchar(30), 
        ExternalOrderNumber         nvarchar(30)  ,
        AreaType                    nvarchar(10)  ,
        ExternalCompanyCode         nvarchar(30)  ,
        ExternalCompany             nvarchar(255)  ,
        DirectDeliveryCustomerId    int,
        DirectDeliveryCustomerCode  nvarchar(30)  ,
        DirectDeliveryCustomer      nvarchar(255)  ,
        DeliveryDate                datetime,
        OrderDate                   datetime,
        DueDate                     datetime,
        CreateDate                  datetime,
        OutboundDocumentType        nvarchar(30) ,
        StorageUnitBatchId          int,
        StorageUnitId               int,
        ProductCode                 nvarchar(30) ,
        Product                     nvarchar(50) ,
        SKUCode                     nvarchar(50) ,
        Batch                       nvarchar(50) ,
        OriginalQuantity            float,
        BackOrderQuantity           float,
        AllocateQuantity            float,
        RemainingQuantity           float,
        Pack                        float
       );
       
       declare @TableResult2 as  table
       (
        WarehouseId                 int,
        IssueId                     int,
        IssueLineId                 int,
        OrderNumber                 nvarchar(30), 
        ExternalOrderNumber         nvarchar(30)  ,
        AreaType                    nvarchar(10)  ,
        ExternalCompanyCode         nvarchar(30)  ,
        ExternalCompany             nvarchar(255)  ,
        DirectDeliveryCustomerId    int,
        DirectDeliveryCustomerCode  nvarchar(30)  ,
        DirectDeliveryCustomer      nvarchar(255)  ,
        DeliveryDate                datetime,
        OrderDate                   datetime,
        DueDate                     datetime,
        CreateDate                  datetime,
        OutboundDocumentType        nvarchar(30) ,
        StorageUnitBatchId          int,
        StorageUnitId               int,
        ProductCode                 nvarchar(30) ,
        Product                     nvarchar(50) ,
        SKUCode                     nvarchar(50) ,
        Batch                       nvarchar(50) ,
        OriginalQuantity            float,
        BackOrderQuantity           float,
        AllocateQuantity            float,
        RemainingQuantity           float,
        Pack                        float
       );
  
  declare @count int,
          @IssueLineId int,
          @RemainingQuantity float,
          @AllocateQuantity  float
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
    
  if @ExternalCompanyId = -1
    set @ExternalCompanyId = null
    
  if @StorageUnitId = -1
    set @StorageUnitId = null
  
  -- -- ** Inserted for history data ** Karen September 2014 --
  --insert @TableResult
  --      (WarehouseId,
  --       IssueId,
  --       IssueLineId,
  --       OrderNumber,
  --       ExternalOrderNumber,
  --       AreaType,
  --       ExternalCompanyCode,
  --       ExternalCompany,
  --       DirectDeliveryCustomerId,
  --       OutboundDocumentType,
  --       DeliveryDate,
  --       OrderDate,
  --       DueDate,
  --       CreateDate,
  --       StorageUnitBatchId,
  --       BackOrderQuantity,
  --       AllocateQuantity,
  --       OriginalQuantity)
  --select distinct i.WarehouseId,
  --       i.IssueId,
  --       il.IssueLineId,
  --       od.OrderNumber,
  --       od.ExternalOrderNumber,
  --       isnull(i.AreaType,''),
  --       ec.ExternalCompanyCode,
  --       ec.ExternalCompany,
  --       od.DirectDeliveryCustomerId,
  --       odt.OutboundDocumentType,
  --       od.DeliveryDate,
  --       iis.OrderDate,
  --       iis.DueDate,
  --       od.CreateDate,
  --       il.StorageUnitBatchId,
  --       il.Quantity,
  --       il.ConfirmedQuatity,
  --       ol.Quantity
  --  from CQ_CiplaTraining..OutboundDocument     od  (nolock)
  --  inner join CQ_CiplaTraining.dbo.ExternalCompany      ec  (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
  --  inner join CQ_CiplaTraining.dbo.OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  --  inner join CQ_CiplaTraining.dbo.Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
  --  inner join CQ_CiplaTraining.dbo.IssueLine             il (nolock) on i.IssueId                 = il.IssueId
  --  inner join CQ_CiplaTraining.dbo.OutboundLine          ol (nolock) on il.OutboundLineId         = ol.OutboundLineId
  --  inner join CQ_CiplaTraining.dbo.InterfaceImportSOHeader iis (nolock) on iis.OrderNumber = od.OrderNumber
  -- where od.OutboundDocumentTypeId    = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
  --   and ec.ExternalCompanyId         = isnull(@ExternalCompanyId, ec.ExternalCompanyId)    
  --   and i.StatusId                   = dbo.ufn_StatusId('IS','BP')
  --   and il.StatusId                  = dbo.ufn_StatusId('IS','BP')
  --   and od.WarehouseId               = isnull(@WarehouseId, od.WarehouseId)
  --   and ol.StorageUnitId             = ISNULL(@StorageUnitId, ol.StorageUnitId)
  --order by il.IssueLineId
  
  insert @TableResult
        (WarehouseId,
         IssueId,
         IssueLineId,
         OrderNumber,
         ExternalOrderNumber,
         AreaType,
         ExternalCompanyCode,
         ExternalCompany,
         DirectDeliveryCustomerId,
         OutboundDocumentType,
         DeliveryDate,
         OrderDate,
         DueDate,
         CreateDate,
         StorageUnitBatchId,
         BackOrderQuantity,
         AllocateQuantity,
         OriginalQuantity)
  select distinct i.WarehouseId,
         i.IssueId,
         il.IssueLineId,
         od.OrderNumber,
         od.ExternalOrderNumber,
         isnull(i.AreaType,''),
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         od.DirectDeliveryCustomerId,
         odt.OutboundDocumentType,
         od.DeliveryDate,
         max(iis.OrderDate),
         iis.DueDate,
         od.CreateDate,
         il.StorageUnitBatchId,
         il.Quantity,
         il.ConfirmedQuatity,
         ol.Quantity
    from OutboundDocument     od  (nolock)
    join ExternalCompany      ec  (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join IssueLine             il (nolock) on i.IssueId                 = il.IssueId
    join OutboundLine          ol (nolock) on il.OutboundLineId         = ol.OutboundLineId
    join InterfaceImportSOHeader iis (nolock) on iis.OrderNumber = od.OrderNumber
   where od.OutboundDocumentTypeId    = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
     and ec.ExternalCompanyId         = isnull(@ExternalCompanyId, ec.ExternalCompanyId)    
     and i.StatusId                   = dbo.ufn_StatusId('IS','BP')
     and il.StatusId                  = dbo.ufn_StatusId('IS','BP')
     and od.WarehouseId               = isnull(@WarehouseId, od.WarehouseId)
     and ol.StorageUnitId             = ISNULL(@StorageUnitId, ol.StorageUnitId)
     
     group by i.WarehouseId,
         i.IssueId,
         il.IssueLineId,
         od.OrderNumber,
         od.ExternalOrderNumber,
         isnull(i.AreaType,''),
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         od.DirectDeliveryCustomerId,
         odt.OutboundDocumentType,
         od.DeliveryDate,
         iis.DueDate,
         od.CreateDate,
         il.StorageUnitBatchId,
         il.Quantity,
         il.ConfirmedQuatity,
         ol.Quantity

  order by il.IssueLineId
  
  -- ** Inserted for history data ** Karen September 2014 --
  --insert @TableResult
  --      (WarehouseId,
  --       IssueId,
  --       IssueLineId,
  --       OrderNumber,
  --       ExternalOrderNumber,
  --       AreaType,
  --       ExternalCompanyCode,
  --       ExternalCompany,
  --       DirectDeliveryCustomerId,
  --       OutboundDocumentType,
  --       DeliveryDate,
  --       --OrderDate,
  --       DueDate,
  --       CreateDate,
  --       StorageUnitBatchId,
  --       BackOrderQuantity,
  --       AllocateQuantity,
  --       OriginalQuantity)
  --select distinct i.WarehouseId,
  --       i.IssueId,
  --       il.IssueLineId,
  --       od.OrderNumber,
  --       od.ExternalOrderNumber,
  --       isnull(i.AreaType,''),
  --       ec.ExternalCompanyCode,
  --       ec.ExternalCompany,
  --       od.DirectDeliveryCustomerId,
  --       odt.OutboundDocumentType,
  --       od.DeliveryDate,
  --       --iis.OrderDate,
  --       iis.DueDate,
  --       od.CreateDate,
  --       il.StorageUnitBatchId,
  --       il.Quantity,
  --       il.ConfirmedQuatity,
  --       ol.Quantity
  --  from CQ_CiplaTraining..OutboundDocument     od  (nolock)
  --  inner join CQ_CiplaTraining.dbo.ExternalCompany      ec  (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
  --  inner join CQ_CiplaTraining.dbo.OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  --  inner join CQ_CiplaTraining.dbo.Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
  --  inner join CQ_CiplaTraining.dbo.IssueLine             il (nolock) on i.IssueId                 = il.IssueId
  --  inner join CQ_CiplaTraining.dbo.OutboundLine          ol (nolock) on il.OutboundLineId         = ol.OutboundLineId
  --  inner join CQ_CiplaTraining.dbo.InterfaceImportSOHeader iis (nolock) on iis.OrderNumber = od.OrderNumber
  -- where od.OutboundDocumentTypeId    = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
  --   and ec.ExternalCompanyId         = isnull(@ExternalCompanyId, ec.ExternalCompanyId)    
  --   and i.StatusId                   = dbo.ufn_StatusId('IS','BP')
  --   and il.StatusId                  = dbo.ufn_StatusId('IS','BP')
  --   and od.WarehouseId               = isnull(@WarehouseId, od.WarehouseId)
  --   and ol.StorageUnitId             = ISNULL(@StorageUnitId, ol.StorageUnitId)
  --   and not exists (select top 1 ordernumber from @TableResult tr2 where tr2.OrderNumber = od.OrderNumber)
  --order by il.IssueLineId
  
  update tr
     set OrderDate = iis.OrderDate
    from @TableResult tr
    inner join InterfaceImportSOHeader iis (nolock) on iis.OrderNumber = tr.OrderNumber
    where tr.OrderDate is null  
    
  update tr
     set DueDate = iis.DueDate
    from @TableResult tr
    inner join InterfaceImportSOHeader iis (nolock) on iis.OrderNumber = tr.OrderNumber
    where tr.DueDate is null  
    
  update tr
     set ExternalOrderNumber = od.ExternalOrderNumber
    from @TableResult tr
    inner join OutboundDocument od (nolock) on od.OrderNumber = tr.OrderNumber
    where tr.ExternalOrderNumber is null  
    
  update tr
     set StorageUnitId = su.StorageUnitId,
         ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch
    from @TableResult tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId

  
  --update tr
  --   set Pack = p.Quantity
  --  from @TableResult tr
  --  join Pack          p (nolock) on tr.StorageUnitId = p.StorageUnitId
  --                               and p.WarehouseId    = @WarehouseId
  --  join PackType     pt (nolock) on p.PackTypeId     = pt.PackTypeId
  -- where pt.OutboundSequence != 1
  --   and p.Quantity > 1
  
  update tr
     set DirectDeliveryCustomerCode = ec.ExternalCompanyCode
        ,DirectDeliveryCustomer     = ec.ExternalCompany
    from @TableResult tr 
    join ExternalCompany ec on tr.DirectDeliveryCustomerId = ec.ExternalCompanyId
  
  select @count = count(distinct(StorageUnitId))
    from @TableResult
  
  set @StorageUnitId = null
  
  while @count >= 1
  begin
    select @count = @count - 1
    
    select top 1 @IssueLineId   = IssueLineId,
                 @StorageUnitId = StorageUnitId
      from @TableResult
     where AllocateQuantity >= 0 
       and RemainingQuantity is null
       and StorageUnitId != isnull(@StorageUnitId, -1)
    
    update @TableResult
       set RemainingQuantity = 0
     where IssueLineId = @IssueLineId
       and StorageUnitId = @StorageUnitId
    
    update tr
       set RemainingQuantity = (ActualQuantity - ReservedQuantity) - AllocateQuantity
      from @TableResult tr
      join viewAvailable v on tr.StorageUnitId = v.StorageUnitId
                          and tr.AreaType      = v.AreaType
                          and tr.WarehouseId   = v.WarehouseId
     where tr.IssueLineId = @IssueLineId
    
    select @RemainingQuantity = RemainingQuantity
      from @TableResult
     where IssueLineId = @IssueLineId
    
    if @RemainingQuantity > 0
    while exists(select top 1 1 from @TableResult tr where AllocateQuantity > 0 and RemainingQuantity is null and StorageUnitId = @StorageUnitId)
    begin
      select top 1 @IssueLineId      = IssueLineId,
                   @AllocateQuantity = AllocateQuantity
        from @TableResult
       where RemainingQuantity is null
         and AllocateQuantity > 0
         and StorageUnitId = @StorageUnitId
      
      if @RemainingQuantity > @AllocateQuantity
      begin
        set @RemainingQuantity = @RemainingQuantity - @AllocateQuantity
        set @AllocateQuantity = @RemainingQuantity
      end
      else
      begin
        set @AllocateQuantity = @RemainingQuantity
        set @RemainingQuantity = 0
      end
      
      update @TableResult
         set RemainingQuantity = @RemainingQuantity
       where IssueLineId = @IssueLineId
    end
  end
  
  update tr1
     set RemainingQuantity = (select min(RemainingQuantity)
                                from @TableResult tr2
                               where tr1.StorageUnitId = tr2.StorageUnitId)
    from @TableResult tr1
  
    insert @TableResult2
        (IssueLineId,
         OrderNumber,
         ExternalOrderNumber,
         OutboundDocumentType,
         AreaType,
         DeliveryDate,
         OrderDate,
         DueDate,
         ExternalCompanyCode,
         ExternalCompany,
         DirectDeliveryCustomerCode,
         DirectDeliveryCustomer,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         OriginalQuantity,
         BackOrderQuantity,
         AllocateQuantity,
         RemainingQuantity)   
  select distinct IssueLineId,
         OrderNumber,
         ExternalOrderNumber,
         OutboundDocumentType,
         AreaType,
         DeliveryDate,
         OrderDate,
         DueDate,
         ExternalCompanyCode,
         ExternalCompany,
         DirectDeliveryCustomerCode,
         DirectDeliveryCustomer,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         OriginalQuantity,
         BackOrderQuantity,
         AllocateQuantity,
         RemainingQuantity
    from @TableResult   
    where OrderDate is not null

           
   select distinct IssueLineId,
         ROW_NUMBER() OVER (ORDER BY IssueLineId) AS LineNumber,
         OrderNumber,
         ExternalOrderNumber,
         OutboundDocumentType,
         AreaType,
         DeliveryDate,
         OrderDate,
         DueDate,
         ExternalCompanyCode,
         ExternalCompany,
         DirectDeliveryCustomerCode,
         DirectDeliveryCustomer,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         OriginalQuantity,
         BackOrderQuantity,
         AllocateQuantity,
         RemainingQuantity
    from @TableResult2
  order by LineNumber,
           ExternalCompanyCode,
           ExternalCompany,
           OrderNumber
   
 
end

 
 
