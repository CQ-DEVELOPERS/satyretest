﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletiseStock_Insert
  ///   Filename       : p_PalletiseStock_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:43
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the PalletiseStock table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null,
  ///   @IssueId int = null,
  ///   @IssueLineId int = null,
  ///   @DropSequence int = null,
  ///   @AreaSequence int = null,
  ///   @JobId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StorageUnitId int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @Quantity float = null,
  ///   @OriginalQuantity float = null,
  ///   @PalletQuantity float = null,
  ///   @UnitWeight float = null,
  ///   @UnitVolume numeric(13,6) = null,
  ///   @ExternalCompanyId int = null,
  ///   @LineNumber int = null,
  ///   @BoxQuantity float = null,
  ///   @UnitQuantity float = null 
  /// </param>
  /// <returns>
  ///   PalletiseStock.OutboundShipmentId,
  ///   PalletiseStock.IssueId,
  ///   PalletiseStock.IssueLineId,
  ///   PalletiseStock.DropSequence,
  ///   PalletiseStock.AreaSequence,
  ///   PalletiseStock.JobId,
  ///   PalletiseStock.StorageUnitBatchId,
  ///   PalletiseStock.StorageUnitId,
  ///   PalletiseStock.ProductCode,
  ///   PalletiseStock.SKUCode,
  ///   PalletiseStock.Quantity,
  ///   PalletiseStock.OriginalQuantity,
  ///   PalletiseStock.PalletQuantity,
  ///   PalletiseStock.UnitWeight,
  ///   PalletiseStock.UnitVolume,
  ///   PalletiseStock.ExternalCompanyId,
  ///   PalletiseStock.LineNumber,
  ///   PalletiseStock.BoxQuantity,
  ///   PalletiseStock.UnitQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletiseStock_Insert
(
 @OutboundShipmentId int = null,
 @IssueId int = null,
 @IssueLineId int = null,
 @DropSequence int = null,
 @AreaSequence int = null,
 @JobId int = null,
 @StorageUnitBatchId int = null,
 @StorageUnitId int = null,
 @ProductCode nvarchar(60) = null,
 @SKUCode nvarchar(20) = null,
 @Quantity float = null,
 @OriginalQuantity float = null,
 @PalletQuantity float = null,
 @UnitWeight float = null,
 @UnitVolume numeric(13,6) = null,
 @ExternalCompanyId int = null,
 @LineNumber int = null,
 @BoxQuantity float = null,
 @UnitQuantity float = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert PalletiseStock
        (OutboundShipmentId,
         IssueId,
         IssueLineId,
         DropSequence,
         AreaSequence,
         JobId,
         StorageUnitBatchId,
         StorageUnitId,
         ProductCode,
         SKUCode,
         Quantity,
         OriginalQuantity,
         PalletQuantity,
         UnitWeight,
         UnitVolume,
         ExternalCompanyId,
         LineNumber,
         BoxQuantity,
         UnitQuantity)
  select @OutboundShipmentId,
         @IssueId,
         @IssueLineId,
         @DropSequence,
         @AreaSequence,
         @JobId,
         @StorageUnitBatchId,
         @StorageUnitId,
         @ProductCode,
         @SKUCode,
         @Quantity,
         @OriginalQuantity,
         @PalletQuantity,
         @UnitWeight,
         @UnitVolume,
         @ExternalCompanyId,
         @LineNumber,
         @BoxQuantity,
         @UnitQuantity 
  
  select @Error = @@Error
  
  
  return @Error
  
end
