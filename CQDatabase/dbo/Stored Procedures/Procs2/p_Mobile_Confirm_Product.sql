﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Confirm_Product
  ///   Filename       : p_Mobile_Confirm_Product.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Confirm_Product
(
 @instructionId int output,
 @barcode       nvarchar(50),
 @type          nvarchar(10) = null
)

as
begin
  set nocount on;
  
  declare @MobileLogId int
  
  insert MobileLog
        (ProcName,
         InstructionId,
         Barcode,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @InstructionId,
         @barcode,
         getdate()
  
  select @MobileLogId = scope_identity()
  
  declare @Error              int,
          @GetDate            datetime,
          @StorageUnitBatchId int,
          @StorageUnitId      int,
          @ProductId          int,
          @ProductBarcode     nvarchar(50),
          @PackBarcode        nvarchar(50),
          @PalletId           int,
          @ProductCode        nvarchar(30),
          @PickLocationId     int,
          @InstructionSUBId   int,
          @AreaCode           nvarchar(10),
          @NewInstructionId   int,
          @JobId              int,
          @WarehouseId        int,
          @StoreLocationId    int,
          @StatusId           int,
          @OperatorId         int,
          @InstructionTypeCode nvarchar(10),
          @InstructionRefId    int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @barcode = ''
	set @barcode = null

  if isnumeric(replace(@barcode,'P:','')) = 1 and @barcode like 'P:%'
  if isnull(@type,'') != 'RCV'
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if isnumeric(replace(@barcode,'J:','')) = 1 and @barcode like 'J:%'
    select top 1 @PalletId = store.PalletId,
                 @barcode  = null
      from Instruction   pick (nolock)
      join Instruction  store (nolock) on pick.StorageUnitBatchId = store.StorageUnitBatchId
                                      and pick.PickLocationId     = store.StoreLocationId
     where pick.InstructionId = @instructionId
       and store.PalletId is not null
    order by store.PalletId desc
  
  set @Error = 0
  
  select @JobId               = i.JobId,
         @WarehouseId         = i.WarehouseId,
         @OperatorId          = i.OperatorId,
         @InstructionTypeCode = it.InstructionTypeCode,
         @PickLocationId      = i.PickLocationId,
         @InstructionSUBId    = i.StorageUnitBatchId
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where InstructionId = @InstructionId
  
  if @Barcode is null
  begin
    --if @InstructionTypeCode != 'S' -- Can't scan pallet id for product confirmation
    --begin
    --  set @Error = -3
    --  set @InstructionId = -3
    --  goto result
    --end
    
    select @StorageUnitBatchId = StorageUnitBatchId
      from Pallet
     where PalletId = @PalletId
    
    if @StorageUnitBatchId is null
      select @StorageUnitBatchId = StorageUnitBatchId
        from Instruction i
       where InstructionId = @instructionId
         and PalletId = @PalletId
    
    --select @PickLocationId   = PickLocationId,
    --       @InstructionSUBId = StorageUnitBatchId
    --  from Instruction (nolock)
    -- where InstructionId = @instructionId
    
    select @AreaCode = a.AreaCode
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId = al.AreaId
     where al.LocationId = @PickLocationId
    
    if @AreaCode in ('BK','RK','PK') and @InstructionTypeCode in ('P','PM','PS','FM')
    begin
      if exists(select top 1 1
    from StorageUnitBatch ins (nolock)
                  join StorageUnitBatch pal (nolock) on ins.StorageUnitId = pal.StorageUnitId
                 where ins.StorageUnitBatchId = @InstructionSUBId
                   and pal.StorageUnitBatchId = @StorageUnitBatchId)
      begin
        select @InstructionRefId = InstructionId
          from Instruction (nolock)
         where InstructionRefId = @instructionId
        
        if @InstructionRefId is not null
        begin
          exec @Error = p_Instruction_Update
           @InstructionId = @InstructionRefId,
           @PalletId      = @PalletId
          
          if @Error <> 0
          begin
            set @Error = -3
            set @InstructionId = -3
            goto result
          end
        end
        
        exec @Error = p_Instruction_Update
         @InstructionId = @InstructionId,
         @PalletId      = @PalletId
        
        if @Error <> 0
        begin
          set @Error = -3
          set @InstructionId = -3
          goto result
        end
        else
          goto result
      end
      else
      begin
        begin
          set @Error = -3
          set @InstructionId = -3
          goto result
        end
      end
    end
    
    if @StorageUnitBatchId = @InstructionSUBId
    begin
      goto result
    end
    else
    begin
      set @Error = -3
      set @InstructionId = -3
      goto result
    end
  end
  
  select @JobId               = i.JobId,
         @WarehouseId         = i.WarehouseId,
         @OperatorId          = i.OperatorId,
         @InstructionTypeCode = it.InstructionTypeCode
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
   where InstructionId = @InstructionId
  
  if @InstructionTypeCode = 'SM'
  begin
    if (select dbo.ufn_Configuration(218, @WarehouseId)) = 1
      if (select dbo.ufn_Configuration(311, @WarehouseId)) = 1 -- ReceiveIntoWarehouse.aspx - only confim product code if no other barcode
        select @NewInstructionId = i.InstructionId,
               @StoreLocationId  = i.StoreLocationId
          from Instruction        i (nolock)
          join Status             s (nolock) on i.StatusId = s.StatusId
          join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
          join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
          join Pack              pk (nolock) on su.StorageUnitId = pk.StorageUnitId
          join Product            p (nolock) on su.ProductId     = p.ProductId
         where JobId          = @JobId
           and s.StatusCode  in ('W','S')
           and pk.WarehouseId = @WarehouseId
           and ((p.ProductCode = @Barcode and isnull(p.Barcode,'') = '' and isnull(pk.Barcode,'') = '')
            or  p.Barcode     = @Barcode
            or  pk.Barcode    = @Barcode)
      else
        select @NewInstructionId = i.InstructionId,
               @StoreLocationId  = i.StoreLocationId
          from Instruction        i (nolock)
          join Status             s (nolock) on i.StatusId = s.StatusId
          join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
          join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
          join Pack              pk (nolock) on su.StorageUnitId = pk.StorageUnitId
          join Product            p (nolock) on su.ProductId     = p.ProductId
         where JobId          = @JobId
           and s.StatusCode  in ('W','S')
           and pk.WarehouseId = @WarehouseId
           and (p.ProductCode = @Barcode
            or  p.Barcode     = @Barcode
            or  pk.Barcode    = @Barcode)
    else
      select @NewInstructionId = i.InstructionId,
             @StoreLocationId  = i.StoreLocationId
        from Instruction        i (nolock)
        join Status             s (nolock) on i.StatusId = s.StatusId
        join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
        join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
        join Pack              pk (nolock) on su.StorageUnitId = pk.StorageUnitId
        join Product            p (nolock) on su.ProductId     = p.ProductId
       where JobId          = @JobId
         and s.StatusCode  in ('W','S')
         and pk.WarehouseId = @WarehouseId
         and (p.Barcode     = @Barcode
          or  pk.Barcode    = @Barcode)
    
    if @NewInstructionId is not null
    begin
      if @InstructionId != @NewInstructionId
      begin
        set @InstructionId = @NewInstructionId
        
        if @StoreLocationId is null
        begin
          exec @Error = p_Receiving_Manual_Palletisation_Auto_Locations
           @InstructionId = @InstructionId
          
          if @Error <> 0
          begin
            set @Error = -3
            set @InstructionId = -3
            goto result
          end
        end
        
        select @StatusId = dbo.ufn_StatusId('I','S')
        
        exec @Error = p_Instruction_Update
         @InstructionId = @InstructionId,
         @StatusId      = @StatusId,
         @OperatorId    = @OperatorId,
         @StartDate     = @GetDate
        
        if @Error <> 0
        begin
          set @Error = -1
          goto result
        end
      end
    end
    else
    begin
        set @Error = -3
        set @InstructionId = -3
      goto result
    end
  end
  else
  begin
    if @InstructionTypeCode = 'P' and dbo.ufn_Configuration(287, @WarehouseId) = 1
    begin
      set @Error = -3
      set @InstructionId = -3
      goto result
    end
    
    select @StorageUnitBatchId = StorageUnitBatchId
      from Instruction (nolock)
     where InstructionId = @instructionId
    
    if convert(nvarchar(30), @StorageUnitBatchId) = @barcode
      goto result
    
    select @StorageUnitId = StorageUnitId
      from StorageUnitBatch (nolock)
     where StorageUnitBatchId = @StorageUnitBatchId
    
    select @ProductId = ProductId
      from StorageUnit (nolock)
     where StorageUnitId = @StorageUnitId
    
    if (select dbo.ufn_Configuration(218, @WarehouseId)) = 1
    begin
      select @ProductCode    = ProductCode,
             @ProductBarcode = Barcode
        from Product (nolock)
       where ProductId = @ProductId
    end
    else
    begin
      select @ProductBarcode = Barcode
        from Product (nolock)
       where ProductId = @ProductId
    end
    
    if @ProductBarcode is null
      set @ProductBarcode = '-1'
    
    if @ProductCode is null
      set @ProductCode = '-1'
    
    if @ProductBarcode != @barcode and @ProductCode != @barcode
    begin
      select @PackBarcode = Barcode
        from Pack (nolock)
       where StorageUnitId = @StorageUnitId
         and Barcode = @barcode
      
      if @PackBarcode is null
      begin
        select @PackBarcode = SKUCode
          from StorageUnit su (nolock)
          join SKU        sku (nolock) on su.SKUId = sku.SKUId
         where su.StorageUnitId = @StorageUnitId
           and sku.SKUCode      = @barcode
      end
      
      if @PackBarcode is null
      begin
        set @Error = -3
        set @InstructionId = -3
        goto result
      end
    end
  end
  
  result:
    update MobileLog
       set EndDate = getdate()
     where MobileLogId = @MobileLogId
    
    select @Error
    return @Error
end
