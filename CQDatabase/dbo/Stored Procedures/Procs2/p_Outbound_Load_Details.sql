﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Load_Details
  ///   Filename       : p_Outbound_Load_Details.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Load_Details
(
 @OutboundShipmentId int
)

as
begin
	 set nocount on;
  
  declare @OutboundDocumentId int,
          @LoadWeight         float,
          @LoadVolume         float,
          @NumberOfOrders     int,
          @NumberOfDrops      int,
          @InboundSequence    int
  
  declare @TableResult as table
  (
   WarehouseId        int,
   OutboundDocumentId int,
   ExternalCompanyId  int,
   Weight             float,
   Volume             float,
   NumberOfOrders     int
  )
  
  insert @TableResult
        (WarehouseId,
         OutboundDocumentId,
         ExternalCompanyId,
         Weight,
         Volume,
         NumberOfOrders)
  select i.WarehouseId,
         od.OutboundDocumentId,
         od.ExternalCompanyId,
         i.Weight,
         i.Volume,
         os.NumberOfOrders
    from OutboundShipmentIssue osi (nolock)
    join OutboundShipment       os (nolock) on osi.OutboundShipmentId = os.OutboundShipmentId
    join Issue                   i (nolock) on osi.IssueId = i.IssueId
    join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
   where osi.OutboundShipmentId = @OutboundShipmentId
  
  select @NumberOfOrders = count(1),
         @NumberOfDrops  = count(distinct(ExternalCompanyId))
    from @TableResult
  
  select sum(Weight)         as 'LoadWeight',
         sum(Volume)         as 'LoadVolume',
         max(NumberOfOrders) as 'NumberOfOrders',
         @NumberOfDrops      as 'NumberOfDrops'
    from @TableResult
end
