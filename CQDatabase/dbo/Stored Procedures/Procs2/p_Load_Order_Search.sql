﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Load_Order_Search
  ///   Filename       : p_Load_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Load_Order_Search
(
 @WarehouseId            int,
 @OutboundShipmentId	    int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @OrderNumber	           nvarchar(30),
 @FromDate	              datetime,
 @ToDate	                datetime,
 @ShowOrders             nvarchar(10)
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundDocumentId        int,
   OutboundDocumentType      nvarchar(30),
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   ExternalCompanyId         int,
   CustomerCode              nvarchar(30),
   Customer                  nvarchar(255),
   RouteId                   int,
   Route                     nvarchar(50),
   NumberOfLines             int,
   DeliveryDate              datetime,
   CreateDate                datetime,
   StatusId                  int,
   Status                    nvarchar(50),
   PriorityId                int,
   Priority                  nvarchar(50),
   LocationId                int,
   Location                  nvarchar(15),
   Rating                    int,
   VehicleRegistration       nvarchar(10),
   Remarks                   nvarchar(255),
   Total                     numeric(13,3),
   Complete                  numeric(13,3),
   PercentageComplete        numeric(13,3),
   Interfaced                bit,
   Loaded                    int,
   Pallets                   int,
   FirstLoaded               datetime
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
	 
	 if @ExternalCompanyCode is null
	   set @ExternalCompanyCode = ''
	 
	 if @ExternalCompany is null
	   set @ExternalCompany = ''
  
  if @OutboundShipmentId is null
  begin
      insert @TableResult
            (OutboundDocumentId,
             IssueId,
             LocationId,
             OrderNumber,
             ExternalCompanyId,
             CustomerCode,
             Customer,
             RouteId,
             StatusId,
             Status,
             PriorityId,
             DeliveryDate,
             CreateDate,
             OutboundDocumentType,
             Rating,
             VehicleRegistration,
             Remarks,
             NumberOfLines,
             Total,
             Complete,
             Interfaced,
             Loaded,
             Pallets,
             FirstLoaded)
      select od.OutboundDocumentId,
             i.IssueId,
             i.LocationId,
             od.OrderNumber,
             od.ExternalCompanyId,
             null,
             null,
             i.RouteId,
             i.StatusId,
             s.Status,
             i.PriorityId,
             i.DeliveryDate,
             od.CreateDate,
             odt.OutboundDocumentType,
             null,
             i.VehicleRegistration,
             i.Remarks,
             i.NumberOfLines,
             i.Total,
             i.Complete,
             isnull(i.Interfaced,0),
             i.Loaded,
             i.Pallets,
             i.FirstLoaded
        from OutboundDocument      od (nolock)
        join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
        join Status                 s (nolock) on i.StatusId                = s.StatusId
        join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
       where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
         and od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
         and od.CreateDate      between @FromDate and @ToDate
         and s.Type                    = 'IS'
         and (s.StatusCode             in ('CD','DC','D','RL','CK')
         or  (s.StatusCode             in ('C') and not exists(select 1 from InterfaceExportSOHeader h where i.IssueId = h.IssueId)))
         and od.WarehouseId            = @WarehouseId
      
      update tr
         set OutboundShipmentId = si.OutboundShipmentId,
             DeliveryDate = os.ShipmentDate,
             Route        = os.Route,
             RouteId      = os.RouteId,
             LocationId   = os.LocationId,
             StatusId     = os.StatusId,
             VehicleRegistration = os.VehicleRegistration
        from @TableResult  tr
        join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
        join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
    
    update tr
       set CustomerCode = ec.ExternalCompanyCode,
           Customer     = ec.ExternalCompany,
           Rating       = ec.Rating
      from @TableResult    tr
      join ExternalCompany ec (nolock) on tr.ExternalCompanyId = ec.ExternalCompanyId
  end
  else
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           LocationId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           OutboundDocumentType,
           Rating,
           VehicleRegistration,
           Remarks,
           NumberOfLines,
           Total,
           Complete,
           Interfaced,
           Loaded,
           Pallets,
           FirstLoaded)
    select osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           odt.OutboundDocumentType,
           ec.Rating,
           i.VehicleRegistration,
           i.Remarks,
           i.NumberOfLines,
           i.Total,
           i.Complete,
           isnull(i.Interfaced,0),
           i.Loaded,
           i.Pallets,
           i.FirstLoaded
      from OutboundDocument       od (nolock)
      join ExternalCompany        ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
      join Issue                   i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status                  s (nolock) on i.StatusId                = s.StatusId
      join OutboundDocumentType  odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundShipmentIssue osi (nolock) on i.IssueId                 = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and s.Type                 = 'IS'
       and (s.StatusCode             in ('CD','DC','D','RL','CK')
       or  (s.StatusCode             in ('C') and not exists(select 1 from InterfaceExportSOHeader h where i.IssueId = h.IssueId)))
  
  if @ShowOrders != 'true'
  begin
    update @TableResult
       set OrderNumber  = null,
           CustomerCode = null,
           Customer     = null
     where OutboundShipmentId is not null
  end
  
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
--  update @TableResult
--     set NumberOfLines = (select count(1)
--                            from OutboundLine ol (nolock)
--                           where tr.OutboundDocumentId = ol.OutboundDocumentId)
--    from @TableResult tr
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update @TableResult
     set PercentageComplete = (Complete / Total) * 100
   where Complete > 0
     and Total    > 0
  
  update @TableResult
     set PercentageComplete = 0
   where PercentageComplete is null
  
  --if @ShowOrders = 'true'
  --begin
    select case when @ShowOrders = 'true'
                then IssueId
                else -1
                end as 'IssueId',
           isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
           OrderNumber,
           CustomerCode,
           Customer,
           isnull(RouteId,-1) as 'RouteId',
           Route,
           sum(NumberOfLines) as 'NumberOfLines',
           DeliveryDate as 'ShipmentDate',
           min(CreateDate) as 'CreateDate',
           Status,
           PriorityId,
           Priority,
           min(OutboundDocumentType) as 'OutboundDocumentType',
           isnull(LocationId,-1) as 'LocationId',
           Location,
           VehicleRegistration,
           Remarks,
           sum(convert(int, round(PercentageComplete,0))) as 'PercentageComplete',
           Interfaced,
           Loaded,
           Pallets,
           FirstLoaded
      from @TableResult
     where isnull(CustomerCode,'') like isnull(@ExternalCompanyCode + '%', Customer)
       and isnull(Customer,'')     like isnull(@ExternalCompany + '%', Customer)
    group by case when @ShowOrders = 'true'
                then IssueId
                else -1
                end,
             OutboundShipmentId,
             OrderNumber,
             CustomerCode,
             Customer,
             RouteId,
             Route,
             DeliveryDate,
             Status,
             PriorityId,
             Priority,
             LocationId,
             Location,
             VehicleRegistration,
             Remarks,
             Interfaced,
             Loaded,
             Pallets,
             FirstLoaded
    order by OutboundShipmentId,
             OrderNumber
end
