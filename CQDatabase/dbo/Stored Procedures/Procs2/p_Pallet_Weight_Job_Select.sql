﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pallet_Weight_Job_Select
  ///   Filename       : p_Pallet_Weight_Job_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pallet_Weight_Job_Select
(
 @barcode varchar(30)
)

as
begin
	 set nocount on;
	 
	 declare @JobId              int,
          @PalletId           int
  
  if isnumeric(replace(@barcode,'J:','')) = 1
    select @JobId   = replace(@barcode,'J:',''),
           @barcode = null
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @PalletId is not null
    select @JobId = JobId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where PalletId = @PalletId
      --and it.InstructionTypeCode in ('PR','S','SM')
  
  if @barcode is not null
  begin
    select @JobId = JobId
      from Job (nolock)
     where ReferenceNumber = @barcode
  end
  
  if @JobId is null
    goto result
  
  declare @TableResult as table
  (
   InstructionId      int,
   StorageUnitBatchId int,
   ProductCode	      varchar(30) null,
   Product	          varchar(50) null,
   SKUCode            varchar(50) null,
   Quantity           float null,
   JobId              int null,
   StatusId           int null,
   Status             varchar(50) null,
   PalletId           int null,
   CreateDate         datetime null,
   Batch              varchar(50) null
  )

  insert @TableResult
        (InstructionId,
         StorageUnitBatchId,
         Quantity,
         JobId,
         StatusId,
         PalletId,
         CreateDate)
  select i.InstructionId,
         i.StorageUnitBatchId,
         i.Quantity,
         i.JobId,
         i.StatusId,
         i.PalletId,
         i.CreateDate
    from Instruction         i   (nolock)
    join Job                 j   (nolock) on i.JobId             = j.JobId
    join InstructionType     it  (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status              s   (nolock) on j.StatusId          = s.StatusId
   where i.JobId                 = isnull(@JobId, i.JobId)
     --and it.InstructionTypeCode in ('S', 'SM','PR')
  
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit         su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId         = p.ProductId
    join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
    join Batch               b   (nolock) on sub.BatchId          = b.BatchId

  update tr
     set Status = s.Status
    from @TableResult tr
    join Status       s  (nolock) on tr.StatusId = s.StatusId
  
  result:
    select InstructionId,
           ProductCode,
           Product,
           SKUCode,
           Quantity,
           JobId,
           Status,
           PalletId,
           CreateDate,
           Batch
      from @TableResult tr 
end
