﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ProductMovement_Search
  ///   Filename       : p_ProductMovement_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:04
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ProductMovement table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ProductMovement.ProductMovementId,
  ///   ProductMovement.WarehouseId,
  ///   ProductMovement.StorageUnitId,
  ///   ProductMovement.SKUId,
  ///   ProductMovement.AreaId,
  ///   ProductMovement.ProductCode,
  ///   ProductMovement.MovementDate,
  ///   ProductMovement.OpeningBalance,
  ///   ProductMovement.ReceivedQuantity,
  ///   ProductMovement.IssuedQuantity,
  ///   ProductMovement.Balance 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ProductMovement_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         ProductMovement.ProductMovementId
        ,ProductMovement.WarehouseId
        ,ProductMovement.StorageUnitId
        ,ProductMovement.SKUId
        ,ProductMovement.AreaId
        ,ProductMovement.ProductCode
        ,ProductMovement.MovementDate
        ,ProductMovement.OpeningBalance
        ,ProductMovement.ReceivedQuantity
        ,ProductMovement.IssuedQuantity
        ,ProductMovement.Balance
    from ProductMovement
  
end
