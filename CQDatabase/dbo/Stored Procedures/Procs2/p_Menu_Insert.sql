﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Menu_Insert
  ///   Filename       : p_Menu_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:55
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Menu table.
  /// </remarks>
  /// <param>
  ///   @MenuId int = null output,
  ///   @Menu nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   Menu.MenuId,
  ///   Menu.Menu 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Menu_Insert
(
 @MenuId int = null output,
 @Menu nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @MenuId = '-1'
    set @MenuId = null;
  
  if @Menu = '-1'
    set @Menu = null;
  
	 declare @Error int
 
  insert Menu
        (Menu)
  select @Menu 
  
  select @Error = @@Error, @MenuId = scope_identity()
  
  
  return @Error
  
end
