﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PickChecking_Document_Search
  ///   Filename       : p_PickChecking_Document_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PickChecking_Document_Search
(
 @WarehouseId              int,
 @OutboundDocumentTypeId   int,
 @OrderNumber	           nvarchar(30),
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @FromDate	               datetime,
 @ToDate	               datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId  int,
   IssueId             int,
   DocumentNumber      nvarchar(50),
   RouteId             int,
   Route               nvarchar(50),
   PriorityId          int,
   Priority            nvarchar(50),
   ExternalCompany     nvarchar(255),
   ExternalCompanyCode nvarchar(30),
   StatusCode          nvarchar(10),
   Status              nvarchar(50),
   LocationId          int,
   Location            nvarchar(15),
   DeliveryDate        datetime,
   PickedIndicator      nvarchar(20)
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
   
   -- Get all the QA Jobs
   select ili.IssueId, sj.StatusCode, sj.Status
     into #temp
     from IssueLineInstruction ili (nolock)
     join Instruction          ins (nolock) on ili.InstructionId = isnull(ins.InstructionRefId, ins.InstructionId)
     join Job                    j (nolock) on ins.JobId         = j.JobId
     join Status                sj (nolock) on j.StatusId        = sj.StatusId
    where sj.StatusCode  in ('QA','CK')
      --and ins.CreateDate between  @FromDate and @ToDate
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         DocumentNumber,
         RouteId,
         Route,
         PriorityId,
         StatusCode,
         Status,
         LocationId,
         DeliveryDate,
         ExternalCompany,
         ExternalCompanyCode)
  select distinct osi.OutboundShipmentId,
         i.IssueId,
         od.OrderNumber,
         i.RouteId,
         os.Route,
         i.PriorityId,
         t.StatusCode,
         t.Status,
         i.LocationId,
         i.DeliveryDate,
         ec.ExternalCompany,
         ec.ExternalCompanyCode
    from #temp                  t (nolock) 
    join Issue                i   (nolock) on i.IssueId                 = t.IssueId
    join IssueLine           il   (nolock) on i.IssueId                 = il.IssueId
    join OutboundDocument     od  (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join ExternalCompany      ec  (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    left outer
    join OutboundShipmentIssue osi (nolock) on i.IssueId                = osi.IssueId
    left outer
    join OutboundShipment       os (nolock) on osi.OutboundShipmentId   = os.OutboundShipmentId
   where od.WarehouseId            = @WarehouseId
     and od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
     and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
     and i.DeliveryDate      between @FromDate and @ToDate
  
  update @TableResult
     set DocumentNumber = convert(varchar(30), OutboundShipmentId) + ' - ' + DocumentNumber
   where OutboundShipmentId is not null
  
  update tr
     set Status = s.Status
    from @TableResult tr
    join Status        s on s.StatusId = dbo.ufn_StatusId('IS','QA')
   where tr.StatusCode not in ('CK','RL')
  
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update @TableResult
     set PickedIndicator = 'Standard'
  
  select isnull(OutboundShipmentId,-1) as 'OutboundShipmentId',
         isnull(IssueId,-1) as 'IssueId',
         DocumentNumber,
         Route,
         Priority,
         ExternalCompanyCode,
         ExternalCompany,
         Status,
         Location,
         DeliveryDate,
         PickedIndicator
    from @TableResult
end
