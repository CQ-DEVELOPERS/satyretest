﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Dwell_Time
  ///   Filename       : p_Mobile_Dwell_Time.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Dwell_Time
(
 @Type       smallint,
 @ReasonId   int = null,
 @OperatorId int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @DwellTime         int,
          @Exception         nvarchar(255),
          @bool              bit,
          @LastInstruction   datetime,
          @WarehouseId       int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @WarehouseId = WarehouseId
    from Operator (nolock)
   where OperatorId = @OperatorId
  
  if (dbo.ufn_Configuration(27, @WarehouseId) = 1)
  begin
    select @DwellTime = dbo.ufn_Configuration_IntegerValue(27, @WarehouseId)
    if @DwellTime is null
      set @DwellTime = 300 -- 5 mins
  end
  
  if @Type = 0
  begin
    select 0 as 'Result',
           ReasonId,
           Reason
      from Reason (nolock)
     where ReasonCode like 'DWELL%'
  end
  
  if @Type = 1
  begin
    set @Exception = 'Operator exceeded maximum dwell time of ' + convert(nvarchar(10), @DwellTime) + ' seconds.'
    
    select @LastInstruction = LastInstruction
      from Operator (nolock)
     where OperatorId = @OperatorId
    
    exec @Error = p_Exception_Insert
     @ReasonId       = @ReasonId,
     @Exception      = @Exception,
     @ExceptionCode  = 'DWELL',
     @CreateDate     = @GetDate,
     @ExceptionDate  = @LastInstruction,
     @OperatorId     = @OperatorId
    
    exec @Error = p_Operator_Update
     @OperatorId      = @OperatorId,
     @LastInstruction = @GetDate
    
    select @Error as 'Result',
           ReasonId,
           Reason
      from Reason (nolock)
     where ReasonId = @ReasonId
  end
  
  if @Type = 2
  begin
    if (select datediff(s, LastInstruction, @GetDate)
          from Operator (nolock)
         where OperatorId = @OperatorId) > @DwellTime
    begin
      set @bool = 1;
      select @bool;
      return @bool;
    end
    else
    begin
      set @bool = 0;
      select @bool;
      return @bool;
    end
  end
end
