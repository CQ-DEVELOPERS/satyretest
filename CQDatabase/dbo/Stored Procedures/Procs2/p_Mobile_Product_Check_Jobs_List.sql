﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Jobs_List
  ///   Filename       : p_Mobile_Product_Check_Jobs_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Jobs_List
(
 @JobId int
)

as
begin
  set nocount on;
  
  select distinct j.JobId,
         j.ReferenceNumber
    from Job         j (nolock)
    join Instruction i (nolock) on j.JobId = i.JobId
   where (i.InstructionRefId in (select InstructionId from Instruction where JobId = @JobId)
      or  i.JobId = @JobId)
  union
  select -1 as 'JobId',
         'New box' as 'ReferenceNumber'
  order by JobId
end
