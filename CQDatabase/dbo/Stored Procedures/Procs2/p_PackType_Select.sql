﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PackType_Select
  ///   Filename       : p_PackType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Sep 2014 20:40:55
  /// </summary>
  /// <remarks>
  ///   Selects rows from the PackType table.
  /// </remarks>
  /// <param>
  ///   @PackTypeId int = null 
  /// </param>
  /// <returns>
  ///   PackType.PackTypeId,
  ///   PackType.PackType,
  ///   PackType.InboundSequence,
  ///   PackType.OutboundSequence,
  ///   PackType.PackTypeCode,
  ///   PackType.Unit,
  ///   PackType.Pallet 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PackType_Select
(
 @PackTypeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         PackType.PackTypeId
        ,PackType.PackType
        ,PackType.InboundSequence
        ,PackType.OutboundSequence
        ,PackType.PackTypeCode
        ,PackType.Unit
        ,PackType.Pallet
    from PackType
   where isnull(PackType.PackTypeId,'0')  = isnull(@PackTypeId, isnull(PackType.PackTypeId,'0'))
  order by PackType
  
end
