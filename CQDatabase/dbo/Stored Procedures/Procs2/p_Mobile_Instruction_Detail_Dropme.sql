﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Instruction_Detail_Dropme
  ///   Filename       : p_Mobile_Instruction_Detail_Dropme.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Instruction_Detail_Dropme
(
 @instructionId int
)

as
begin
	 set nocount on;
	 
	 declare @CurrentLine int,
	         @TotalLines  int
	 
	 declare @TableResult as table
  (
   WarehouseId         int,
   InstructionId       int,
   InstructionType     nvarchar(30),
   InstructionTypeCode nvarchar(10),
   JobId               int,
   PalletId            int,
   StorageUnitBatchId  int,
   StorageUnitId       int,
   BatchId             int,
   ProductCode         nvarchar(30),
   Product             nvarchar(255),
   SKUCode             nvarchar(50),
   SKU                 nvarchar(50),
   Batch               nvarchar(50),
   PickLocationId      int,
   PickLocation        nvarchar(255),
   PickSecurityCode    int,
   PickAisle           nvarchar(10),
   PickColumn          nvarchar(10),
   PickLevel           nvarchar(10),
   StoreLocationId     int,
   StoreLocation       nvarchar(30),
   AreaType            nvarchar(10),
   StoreSecurityCode   int,
   StoreAreaCode       nvarchar(20),
   Quantity            numeric(19,6),
   DisplayQuantity     nvarchar(max),
   ConfirmedQuantity   float,
   Boxes               nvarchar(50),
   BoxesQuantity       nvarchar(10),
   Units               nvarchar(50),
   UnitsQuantity       nvarchar(10),
   Pickface            nvarchar(15),
   ProductValue        nvarchar(50),
   SOH                 float,
   PickAreaId          int
	 )
  
  insert @TableResult
        (WarehouseId,
         InstructionId,
         InstructionType,
         it.InstructionTypeCode,
         JobId,
         PalletId,
         StorageUnitBatchId,
         StorageUnitId,
         BatchId,
         PickLocationId,
         StoreLocationId,
         Quantity,
         ConfirmedQuantity)
  select i.WarehouseId,
         i.InstructionId,
         it.InstructionType,
         it.InstructionTypeCode,
         i.JobId,
         i.PalletId,
         sub.StorageUnitBatchId,
         sub.StorageUnitId,
         sub.BatchId,
         i.PickLocationId,
         i.StoreLocationId,
         i.Quantity,
         i.ConfirmedQuantity
    from Instruction        i (nolock)
    join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where InstructionId = @instructionId
  
  update tr 
     set ProductCode  = p.ProductCode,
         Product      = p.Product,
         SKUCode      = sku.SKUCode,
         SKU		  = sku.SKU,
         Batch        = b.Batch + isnull(' - ' + CONVERT(nvarchar(10), b.ExpiryDate, 120),''),
         ProductValue = right(dbo.ufn_RemoveNonNumeric(p.ProductCode),3) -- update Product set Barcode = right(dbo.ufn_RemoveNonNumeric(ProductCode),3)
    from @TableResult      tr
    join Batch              b (nolock) on tr.BatchId          = b.BatchId
    join StorageUnit       su (nolock) on tr.StorageUnitId    = su.StorageUnitId
    join Product            p (nolock) on su.ProductId         = p.ProductId
    join SKU              sku (nolock) on su.SKUId             = sku.SKUId
  
  update tr
     set PickLocation     = l.Location,
         PickSecurityCode = l.SecurityCode,
         PickAisle        = l.Ailse,
         PickColumn       = l.[Column],
         PickLevel        = l.[Level],
         PickAreaId       = al.AreaId
    from @TableResult      tr
    join Location           l (nolock) on tr.PickLocationId = l.LocationId
    join AreaLocation      al (nolock) on l.LocationId      = al.LocationId
  
  update tr
     set StoreLocation     = l.Location,
         StoreSecurityCode = l.SecurityCode,
         StoreAreaCode     = a.AreaCode,
         AreaType          = a.AreaType
    from @TableResult      tr
    join Location           l (nolock) on tr.StoreLocationId = l.LocationId
    join AreaLocation      al (nolock) on l.LocationId       = al.LocationId
    join Area               a (nolock) on al.AreaId          = a.AreaId
  
  select @CurrentLine = Count(1) + 1
    from @TableResult tr
    join Instruction   i (nolock) on tr.JobId   = i.JobId
    join Status        s (nolock) on i.StatusId = s.StatusId
   where s.Type        = 'I'
     and s.StatusCode in ('F','NS')
  
  if @CurrentLine = 0
    set @CurrentLine = 1
  
  select @TotalLines = Count(1)
    from @TableResult tr
    join Instruction   i (nolock) on tr.JobId = i.JobId
  
  update tr
     set InstructionTypeCode = 'R'
    from @TableResult tr
    join AreaLocation      al (nolock) on tr.PickAreaId = al.AreaId
    join Instruction        i (nolock) on al.LocationId = i.StoreLocationId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
                                      and tr.StorageUnitId = sub.StorageUnitId
    join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                                      and it.InstructionTypeCode in ('M','R','P')
    join Status             s (nolock) on i.StatusId = s.StatusId
                                      and s.StatusCode in ('W','S')
  
  if exists(select 1 from @TableResult where InstructionTypeCode = 'R')
  begin
    update tr
       set PickLocation = tr.PickLocation + '* Replenishment'
      from @TableResult      tr
      join Instruction        i (nolock) on i.WarehouseId     = tr.WarehouseId
                                        and tr.PickLocationId = i.StoreLocationId
      join Status             s (nolock) on i.StatusId           = s.StatusId
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
                                        and tr.StorageUnitId  = sub.StorageUnitId
     where s.StatusCode in ('W','S')
  end
  
  update tr
     set Pickface = l.Ailse
    from @TableResult tr
    join StorageUnitLocation sul (nolock) on tr.StorageUnitId = sul.StorageUnitId
    join Location              l (nolock) on sul.LocationId = L.LocationId
    join AreaLocation         al (nolock) on l.LocationId = al.LocationId
    join Area                  a (nolock) on al.AreaId = a.AreaId
   where a.AreaCode = 'PK'
  
  update @TableResult
     set StoreLocation = StoreLocation + ' ' + isnull(Pickface,'')
   where StoreAreaCode = 'SP'
     and AreaType = ''
  
  update tr
     set Boxes = pt.PackType,
         BoxesQuantity = ROUND(convert(float, i.Quantity) / convert(float, p.Quantity),0),
         UnitsQuantity = i.Quantity - floor(convert(float, i.Quantity) / convert(float, p.Quantity)) * p.Quantity
    from @TableResult      tr
    join Instruction        i (nolock) on tr.InstructionId     = i.InstructionId
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Pack               p (nolock) on i.WarehouseId        = p.WarehouseId
                                      and su.StorageUnitId     = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId         = pt.PackTypeId
  where i.InstructionId = @instructionId
    and p.PackTypeId in (select min(PackTypeId)
                           from Pack p2
                          where su.StorageUnitId = p2.StorageUnitId
                            and p.WarehouseId    = p2.WarehouseId
                            and p2.Quantity > 1)
  
  update tr
     set Units = pt.PackType
    from @TableResult      tr
    join Pack               p (nolock) on tr.WarehouseId       = p.WarehouseId
                                      and tr.StorageUnitId     = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId         = pt.PackTypeId
   where pt.OutboundSequence in (select max(OutboundSequence) from PackType (nolock))
  
  update @TableResult
     set DisplayQuantity = convert(nvarchar(max), Quantity) + '------(' +  BoxesQuantity + ' ' + 'x' + ' ' + Boxes + ')'
   where convert(float, BoxesQuantity) >= 1
     and convert(float, UnitsQuantity)  < 1
  
  update @TableResult
     set DisplayQuantity = convert(nvarchar(max), convert(float, Quantity)) + '------(' +  BoxesQuantity + ' ' + 'x' + ' ' + Boxes + ' ' + UnitsQuantity + ' ' + 'x' + ' ' + Units + ')'
   where convert(float, BoxesQuantity) >= 1
     and convert(float, UnitsQuantity) >= 1
  
  update @TableResult
     set DisplayQuantity = convert(nvarchar(max), convert(float, Quantity))
   where DisplayQuantity is null
  
  update tr
     set SOH = subl.ActualQuantity
    from @TableResult               tr
    join StorageUnitBatchLocation subl (nolock) on tr.StorageUnitBatchId = subl.StorageUnitBatchId
                                               and tr.PickLocationId = subl.LocationId
  
  select InstructionType,
         JobId,
         isnull('P:' + convert(nvarchar(10), PalletId),'J:' + convert(nvarchar(10), JobId)) as 'PalletId',
         ProductCode,
         ProductValue,
         Product,
         SKUCode,
         SKU,
         Batch,
         PickLocation,
         PickSecurityCode,
         StoreLocation,
         StoreSecurityCode,
         DisplayQuantity,
         SOH,
         Quantity,
         ConfirmedQuantity,
         SOH,
         @CurrentLine as 'CurrentLine',
         @TotalLines  as 'TotalLines', *
  from @TableResult
end
 
 
