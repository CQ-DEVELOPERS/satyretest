﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LabelingCategory_List
  ///   Filename       : p_LabelingCategory_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:56:45
  /// </summary>
  /// <remarks>
  ///   Selects rows from the LabelingCategory table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   LabelingCategory.LabelingCategoryId,
  ///   LabelingCategory.LabelingCategory 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LabelingCategory_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as LabelingCategoryId
        ,'{All}' as LabelingCategory
  union
  select
         LabelingCategory.LabelingCategoryId
        ,LabelingCategory.LabelingCategory
    from LabelingCategory
  order by LabelingCategory
  
end
 
