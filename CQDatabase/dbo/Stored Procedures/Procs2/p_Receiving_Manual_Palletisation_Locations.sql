﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Manual_Palletisation_Locations
  ///   Filename       : p_Receiving_Manual_Palletisation_Locations.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Manual_Palletisation_Locations
(
 @InstructionId   int,
 @StoreLocationId int
)

as
begin
	 set nocount on;
  
  declare @TableInstructions as table
  (
   InstructionId       int,
   WarehouseId         int,
   PickLocationId      int,
   StoreLocationId     int,
   StorageUnitBatchId  int,
   Quantity            float
  )
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @WarehouseId        int,
          @PickLocationId     int,
          @StorageUnitBatchId int,
          @Quantity           float,
          @StatusId           int,
          @OldStoreLocationId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @WarehouseId        = j.WarehouseId,
         @PickLocationId     = i.PickLocationId,
         @StorageUnitBatchId = i.StorageUnitBatchId,
         @Quantity           = i.Quantity,
         @OldStoreLocationId = i.StoreLocationId
    from Job         j (nolock)
    join Instruction i (nolock) on j.JobId = i.JobId
   where i.InstructionId = @InstructionId
  
  select @StatusId = StatusId
    from Status
   where StatusCode = 'W'
     and Type       = 'I';
  
  begin transaction
  
  if @OldStoreLocationId is not null
  begin
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId       = @InstructionId
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Deallocate'
      goto error
    end
  end
  
  exec @Error = p_Instruction_Update
    @InstructionId   = @InstructionId,
    @StoreLocationId = @StoreLocationId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId       = @InstructionId
  
  if @Error <> 0
  begin
    set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Reserve'
    goto error
  end
  
  commit transaction
  return 0
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
