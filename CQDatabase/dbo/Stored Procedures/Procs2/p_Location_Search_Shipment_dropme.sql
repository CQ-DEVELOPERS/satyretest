﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_Shipment_dropme
  ///   Filename       : p_Location_Search_Shipment_dropme.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_Shipment_dropme
(
 @warehouseId        int,
 @outboundShipmentId int = null,
 @issueId            int = null,
 @inboundShipmentId  int = null,
 @receiptId          int = null
)

as
begin
	 set nocount on;
	 
	 declare @areacode nvarchar(20)
	 
	 declare @TableResult as table
	 (
	  LocationId int,
	  Location   nvarchar(15)
	 )
	 
	 declare @LocationId  int,
          @VehicleId   int
	 
	 if @outboundShipmentId = -1
	   set @outboundShipmentId = null
	 
	 if @inboundShipmentId = -1
	   set @inboundShipmentId = null
	   
	 if @receiptId is not null
		set @areacode = 'R'
	 else
		set @areacode = null
  
  --insert @TableResult
  --      (LocationId,
	 --       Location)
  --select -1     as 'LocationId',
  --       'None' as 'Location'
  
  select @VehicleId = VehicleId,
         @LocationId = isnull(@LocationId, LocationId)
    from OutboundShipment (nolock)
   where OutboundShipmentId = @OutboundShipmentId
  
  insert @TableResult
        (LocationId,
	        Location)
  select distinct l.LocationId,
         l.Location
    from Area a (nolock) 
    join AreaLocation    al (nolock) on a.AreaId = al.AreaId
    join Location         l (nolock) on al.LocationId = l.LocationId
    join VehicleLocation vl (nolock) on l.LocationId = vl.LocationId
   where a.WarehouseId        = @warehouseId
     and vl.VehicleId = @VehicleId
     and a.AreaCode = isnull(@areacode,a.AreaCode)
  
 
  
  
  insert @TableResult
        (LocationId,
	        Location)
  select distinct l.LocationId,
         l.Location
    from OutboundShipmentIssue osi (nolock)
    join OutboundShipment       os (nolock) on osi.OutboundShipmentId = os.OutboundShipmentId
    join Location                l (nolock) on os.LocationId = l.LocationId
    left 
    join @TableResult           tr          on l.LocationId = tr.LocationId
   where os.WarehouseId        = @warehouseId
     and os.OutboundShipmentId = @OutboundShipmentId
     and tr.LocationId is null
  
  insert @TableResult
        (LocationId,
	        Location)
  select distinct l.LocationId,
         l.Location
    from InboundShipmentReceipt isr (nolock)
    join InboundShipment        iss (nolock) on isr.InboundShipmentId = iss.InboundShipmentId
    join Location                 l (nolock) on iss.LocationId = l.LocationId
    left 
    join @TableResult           tr          on l.LocationId = tr.LocationId
   where iss.WarehouseId        = @warehouseId
     and iss.InboundShipmentId = @InboundShipmentId--isnull(@InboundShipmentId, os.InboundShipmentId)
     and tr.LocationId is null
  
  insert @TableResult
        (LocationId,
	        Location)
  select distinct l.LocationId,
         l.Location
    from ReceiptLine             rl (nolock)
    join InboundLine             il (nolock) on rl.InboundLineId = il.InboundLineId
    join StorageUnitArea        sua (nolock) on il.StorageUnitId = sua.StorageUnitId
    join Area                     a (nolock) on sua.AreaId = a.AreaId
    join AreaLocation            al (nolock) on a.AreaId = al.AreaId
    join Location                 l (nolock) on al.LocationId = l.LocationId
    left 
    join @TableResult           tr          on l.LocationId = tr.LocationId
   where a.WarehouseId        = @warehouseId
     and a.AreaCode    in ('D','R')
     and rl.ReceiptId = @ReceiptId
     and tr.LocationId is null
  
  if (select count(1)
        from @TableResult) = 0
    insert @TableResult
          (LocationId,
	          Location)
    select l.LocationId,
           l.Location
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId      = al.AreaId
      join Location      l (nolock) on al.LocationId = l.LocationId
      left
      join @TableResult tr          on l.LocationId = tr.LocationId
     where a.WarehouseId = @warehouseId
       and a.AreaCode    in ('D','R')
       and tr.LocationId is null
       and a.AreaCode = isnull(@areacode,a.AreaCode)
  
   
  select LocationId,
         Location
    from @TableResult
  order by Location
end
 
