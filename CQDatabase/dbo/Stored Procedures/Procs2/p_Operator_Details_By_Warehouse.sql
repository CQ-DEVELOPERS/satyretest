﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Details_By_Warehouse
  ///   Filename       : p_Operator_Details_By_Warehouse.sql
  ///   Create By      : Willis
  ///   Date Created   : 04 Aug 2008
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Operator table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Operator.OperatorId,
  ///   Operator.Operator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Details_By_Warehouse
(
 @warehouseId	int,
 @rblActiveOnly		bit = 1
)

as
begin
	 set nocount on;
  
	 declare @Error int
  select o.OperatorId,
         g.OperatorGroupId,
         g.OperatorGroup,
         o.Operator ,
         o.OperatorCode,
         o.ActiveIndicator,
         o.ExpiryDate,
         o.LastInstruction,
         o.Printer,
         o.Port,
         o.IpAddress,
         c.CultureId,
         c.CultureName,
         o.WarehouseId,
         w.Warehouse
    from Operator      o
			 Join OperatorGroup g on g.OperatorGroupId = o.OperatorGroupId
			 join Warehouse     w on o.WarehouseId = w.WarehouseId
			 Left
			 Join Culture       c on c.CultureId = o.CultureId
   where o.WarehouseId = @WarehouseId
  order by o.ActiveIndicator desc, o.Operator
end
