﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationType_Search
  ///   Filename       : p_LocationType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:00
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the LocationType table.
  /// </remarks>
  /// <param>
  ///   @LocationTypeId int = null output,
  ///   @LocationType nvarchar(100) = null,
  ///   @LocationTypeCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   LocationType.LocationTypeId,
  ///   LocationType.LocationType,
  ///   LocationType.LocationTypeCode,
  ///   LocationType.DeleteIndicator,
  ///   LocationType.ReplenishmentIndicator,
  ///   LocationType.MultipleProductsIndicator,
  ///   LocationType.BatchTrackingIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationType_Search
(
 @LocationTypeId int = null output,
 @LocationType nvarchar(100) = null,
 @LocationTypeCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @LocationTypeId = '-1'
    set @LocationTypeId = null;
  
  if @LocationType = '-1'
    set @LocationType = null;
  
  if @LocationTypeCode = '-1'
    set @LocationTypeCode = null;
  
 
  select
         LocationType.LocationTypeId
        ,LocationType.LocationType
        ,LocationType.LocationTypeCode
        ,LocationType.DeleteIndicator
        ,LocationType.ReplenishmentIndicator
        ,LocationType.MultipleProductsIndicator
        ,LocationType.BatchTrackingIndicator
    from LocationType
   where isnull(LocationType.LocationTypeId,'0')  = isnull(@LocationTypeId, isnull(LocationType.LocationTypeId,'0'))
     and isnull(LocationType.LocationType,'%')  like '%' + isnull(@LocationType, isnull(LocationType.LocationType,'%')) + '%'
     and isnull(LocationType.LocationTypeCode,'%')  like '%' + isnull(@LocationTypeCode, isnull(LocationType.LocationTypeCode,'%')) + '%'
  
end
