﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LabelingCategory_Insert
  ///   Filename       : p_LabelingCategory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:56:42
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the LabelingCategory table.
  /// </remarks>
  /// <param>
  ///   @LabelingCategoryId int = null output,
  ///   @LabelingCategory nvarchar(510) = null,
  ///   @LabelingCategoryCode nvarchar(60) = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @ManualLabel bit = null,
  ///   @ScreenGaurd bit = null 
  /// </param>
  /// <returns>
  ///   LabelingCategory.LabelingCategoryId,
  ///   LabelingCategory.LabelingCategory,
  ///   LabelingCategory.LabelingCategoryCode,
  ///   LabelingCategory.Additional1,
  ///   LabelingCategory.Additional2,
  ///   LabelingCategory.Additional3,
  ///   LabelingCategory.Additional4,
  ///   LabelingCategory.Additional5,
  ///   LabelingCategory.ManualLabel,
  ///   LabelingCategory.ScreenGaurd 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LabelingCategory_Insert
(
 @LabelingCategoryId int = null output,
 @LabelingCategory nvarchar(510) = null,
 @LabelingCategoryCode nvarchar(60) = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @ManualLabel bit = null,
 @ScreenGaurd bit = null 
)

as
begin
	 set nocount on;
  
  if @LabelingCategoryId = '-1'
    set @LabelingCategoryId = null;
  
  if @LabelingCategory = '-1'
    set @LabelingCategory = null;
  
  if @LabelingCategoryCode = '-1'
    set @LabelingCategoryCode = null;
  
	 declare @Error int
 
  insert LabelingCategory
        (LabelingCategory,
         LabelingCategoryCode,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         ManualLabel,
         ScreenGaurd)
  select @LabelingCategory,
         @LabelingCategoryCode,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @ManualLabel,
         @ScreenGaurd 
  
  select @Error = @@Error, @LabelingCategoryId = scope_identity()
  
  
  return @Error
  
end
 
