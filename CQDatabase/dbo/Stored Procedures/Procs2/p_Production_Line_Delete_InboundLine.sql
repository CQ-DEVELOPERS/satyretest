﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Line_Delete_InboundLine
  ///   Filename       : p_Production_Line_Delete_InboundLine.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Line_Delete_InboundLine
(
 @InboundLineId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @ReceiptLineId       int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @ReceiptLineId = ReceiptLineId
    from ReceiptLine
   where InboundLineId = @InboundLineId
  
  begin transaction
  
  delete Instruction where ReceiptLineId = @ReceiptLineId
  
  if @@Error <> 0
  begin
    set @Error = -1
    goto error
  end
  
  exec @Error = p_ReceiptLine_Delete
   @ReceiptLineId = @ReceiptLineId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_InboundLine_Delete
   @InboundLineId = @InboundLineId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Production_Line_Delete_InboundLine'); 
    rollback transaction
    return @Error
end
