﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Manual_Palletisation_Auto_Locations
  ///   Filename       : p_Receiving_Manual_Palletisation_Auto_Locations.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Aug 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Manual_Palletisation_Auto_Locations
(
 @ReceiptId     int = null,
 @ReceiptLineId int = null,
 @InstructionId int = null
)

as
begin
	 set nocount on;
  
  declare @TableInstructions as table
  (
   InstructionId       int,
   WarehouseId         int,
   PickLocationId      int,
   StoreLocationId     int,
   StorageUnitBatchId  int,
   Quantity            float,
   ReceiptLineId       int,
   DocumentTypeCode    nvarchar(10)
  )
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @WarehouseId        int,
          @PickLocationId     int,
          @StoreLocationId    int,
          @StorageUnitBatchId int,
          @Quantity           float,
          @InstructionCount   int,
          @DocumentTypeCode   nvarchar(10),
          @StorageUnitId      int,
          @MaxLocationQuantity int,
          @StoreLocationRelativeValue float
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @ReceiptLineId is null and @InstructionId is null
    return
  
  if @ReceiptId is not null
    insert @TableInstructions
          (InstructionId,
           WarehouseId,
           PickLocationId,
           StoreLocationId,
           StorageUnitBatchId,
           Quantity,
           ReceiptLineId)
    select i.InstructionId,
           i.WarehouseId,
           i.PickLocationId,
           i.StoreLocationId,
           i.StorageUnitBatchId,
           i.Quantity,
           i.ReceiptLineId
      from Instruction  i (nolock)
      join ReceiptLine rl (nolock) on i.ReceiptLineId = rl.ReceiptLineId
      join Job          j (nolock) on i.JobId         = j.JobId
      join Status       s (nolock) on j.StatusId      = s.StatusId
     where rl.ReceiptId       = @ReceiptId
       and s.Type             = 'R'
       and s.StatusCode      in ('R','PR','P','LA')
       and i.StoreLocationId is null
  else
    insert @TableInstructions
          (InstructionId,
           WarehouseId,
           PickLocationId,
           StoreLocationId,
           StorageUnitBatchId,
           Quantity,
           ReceiptLineId)
    select i.InstructionId,
           i.WarehouseId,
           i.PickLocationId,
           i.StoreLocationId,
           i.StorageUnitBatchId,
           i.Quantity,
           i.ReceiptLineId
      from Instruction i (nolock)
      join Job         j (nolock) on i.JobId    = j.JobId
      join Status      s (nolock) on j.StatusId = s.StatusId
     where isnull(i.ReceiptLineId,0)    = isnull(@ReceiptLineId, isnull(i.ReceiptLineId,0))
       and i.InstructionId    = isnull(@InstructionId, i.InstructionId)
       and s.StatusCode      in ('RC','R','PR','P','LA','A','S','RL')
       and i.StoreLocationId is null
  
  update ti
     set DocumentTypeCode = idt.InboundDocumentTypeCode
    from @TableInstructions   ti
    join ReceiptLine          rl (nolock) on ti.ReceiptLineId         = rl.ReceiptLineId
    join Receipt               r (nolock) on rl.ReceiptId             = r.ReceiptId
    join InboundDocument      id (nolock) on r.InboundDocumentId      = id.InboundDocumentId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
  
  select @InstructionCount = Count(1) from @TableInstructions
  
  begin transaction
  
  while @InstructionCount > 0
  begin
    set @InstructionCount = @InstructionCount - 1
    
    select top 1
           @InstructionId      = InstructionId,
           @WarehouseId        = WarehouseId,
           @PickLocationId     = PickLocationId,
           @StoreLocationId    = StoreLocationId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @Quantity           = Quantity,
           @DocumentTypeCode   = DocumentTypeCode
      from @TableInstructions
    
    if @PickLocationId is null
      select top 1 @PickLocationId = l.LocationId
        from Location      l (nolock)
        join AreaLocation al (nolock) on l.LocationId = al.LocationId
        join Area          a (nolock) on al.AreaId    = a.AreaId
       where a.AreaCode = 'R'
    
    delete @TableInstructions where InstructionId = @InstructionId
    
    if @StoreLocationId is not null
    begin
      exec @Error = p_StorageUnitBatchLocation_Deallocate
        @InstructionId = @InstructionId,
        @Pick          = 1,
        @Store         = 1,
        @Confirmed     = 0
      
      if @Error <> 0
        goto error
    end
    
    if @DocumentTypeCode = 'RET'
    begin
      select @StorageUnitId = StorageUnitId
        from StorageUnitBatch (nolock)
       where StorageUnitBatchId = @StorageUnitBatchId
      
      set @StoreLocationId = null
      
      -- First get Mezzanine Staging Area
      select top 1 @StoreLocationId = al.LocationId
        from StorageUnitArea sua (nolock)
        join Area              a (nolock) on sua.AreaId = a.AreaId
        join AreaLocation     al (nolock) on a.AreaId = al.AreaId
        join Location          l (nolock) on al.LocationId = l.LocationId
       where a.AreaCode            = 'SP'
         and a.Area             like '%Mezz%'
         and a.WarehouseId         = @WarehouseId
         and sua.StorageUnitId     = @StorageUnitId
      
      -- If not Mezzanine Product get Pick face location
      if @StoreLocationId is null
        select top 1 @StoreLocationId = l.LocationId
          from StorageUnitLocation sul (nolock)
          join Location              l (nolock) on sul.LocationId = l.LocationId
          join AreaLocation         al (nolock) on l.Locationid   = al.LocationId
          join Area                  a (nolock) on al.AreaId      = a.AreaId
         where a.WarehouseId         = @WarehouseId
           and sul.StorageUnitId     = @StorageUnitId
           and a.AreaCode           != 'BK'
           and isnull(a.AreaType,'') = ''
    end
    else
    begin
      exec p_Location_Get
       @WarehouseId        = @WarehouseId,
       @LocationId         = @StoreLocationId output,
       @StorageUnitBatchId = @StorageUnitBatchId,
       @Quantity           = @Quantity
      
      if @Error <> 0
        goto error
      
      select @StoreLocationRelativeValue = RelativeValue 
        from Location
       where LocationId = @StoreLocationId
      
      select @StorageUnitId = StorageUnitId
        from StorageUnitBatch (nolock)
       where StorageUnitBatchId = @StorageUnitBatchId
       
	  select @MaxLocationQuantity = sul.MaximumQuantity
		from StorageUnitLocation sul (nolock)  
	   where sul.StorageUnitId = @StorageUnitId  
		 and sul.LocationId    = @StoreLocationId
	     
	  if (@Quantity > @MaxLocationQuantity)
	  begin
	  
		select @StoreLocationId = min(l.LocationId)
		  from Location			l (nolock)
		  join AreaLocation	   al (nolock) on l.LocationId = al.LocationId
		  join Area				a (nolock) on al.AreaId	   = a.AreaId
		 where a.AreaCode = 'RK'
		   and l.Used = 0
	  group by l.RelativeValue
      order by case when @StoreLocationRelativeValue > isnull(l.RelativeValue,0)
                    then @StoreLocationRelativeValue - isnull(l.RelativeValue,0)
                    else isnull(l.RelativeValue,0) - @StoreLocationRelativeValue
                     end
        end
    end
    
    if dbo.ufn_Configuration(146,@WarehouseId) = 1
    begin
      if @StoreLocationId is null
      begin
        select top 1 @StoreLocationId = l.LocationId
          from Location      l (nolock)
          join AreaLocation al (nolock) on l.LocationId = al.LocationId
          join Area          a (nolock) on al.AreaId    = a.AreaId
         where a.AreaCode = 'RO'
      end
    end
    
    if @StoreLocationId is not null
    begin
      exec @Error = p_Instruction_Update
        @InstructionId   = @InstructionId,
        @StoreLocationId = @StoreLocationId,
        @PickLocationId  = @PickLocationId
      
      if @Error <> 0
        goto error
      
      exec @Error = p_StorageUnitBatchLocation_Reserve
       @InstructionId       = @InstructionId
      
      if @Error <> 0
        goto error
    end
  end
  
  exec @Error = p_Inbound_Release
   @InboundShipmentId = null,
   @ReceiptId         = @ReceiptId,
   @ReceiptLineId     = @ReceiptLineId,
   @StatusCode        = 'LA'
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Receiving_Manual_Palletisation_Auto_Locations'); 
    rollback transaction
    return @Error
end
