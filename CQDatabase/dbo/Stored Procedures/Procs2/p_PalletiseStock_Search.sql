﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PalletiseStock_Search
  ///   Filename       : p_PalletiseStock_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:44
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the PalletiseStock table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   PalletiseStock.OutboundShipmentId,
  ///   PalletiseStock.IssueId,
  ///   PalletiseStock.IssueLineId,
  ///   PalletiseStock.DropSequence,
  ///   PalletiseStock.AreaSequence,
  ///   PalletiseStock.JobId,
  ///   PalletiseStock.StorageUnitBatchId,
  ///   PalletiseStock.StorageUnitId,
  ///   PalletiseStock.ProductCode,
  ///   PalletiseStock.SKUCode,
  ///   PalletiseStock.Quantity,
  ///   PalletiseStock.OriginalQuantity,
  ///   PalletiseStock.PalletQuantity,
  ///   PalletiseStock.UnitWeight,
  ///   PalletiseStock.UnitVolume,
  ///   PalletiseStock.ExternalCompanyId,
  ///   PalletiseStock.LineNumber,
  ///   PalletiseStock.BoxQuantity,
  ///   PalletiseStock.UnitQuantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PalletiseStock_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         PalletiseStock.OutboundShipmentId
        ,PalletiseStock.IssueId
        ,PalletiseStock.IssueLineId
        ,PalletiseStock.DropSequence
        ,PalletiseStock.AreaSequence
        ,PalletiseStock.JobId
        ,PalletiseStock.StorageUnitBatchId
        ,PalletiseStock.StorageUnitId
        ,PalletiseStock.ProductCode
        ,PalletiseStock.SKUCode
        ,PalletiseStock.Quantity
        ,PalletiseStock.OriginalQuantity
        ,PalletiseStock.PalletQuantity
        ,PalletiseStock.UnitWeight
        ,PalletiseStock.UnitVolume
        ,PalletiseStock.ExternalCompanyId
        ,PalletiseStock.LineNumber
        ,PalletiseStock.BoxQuantity
        ,PalletiseStock.UnitQuantity
    from PalletiseStock
  
end
