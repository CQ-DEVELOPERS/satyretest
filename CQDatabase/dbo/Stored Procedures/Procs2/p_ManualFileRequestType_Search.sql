﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequestType_Search
  ///   Filename       : p_ManualFileRequestType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:04
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ManualFileRequestType table.
  /// </remarks>
  /// <param>
  ///   @ManualFileRequestTypeId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ManualFileRequestType.ManualFileRequestTypeId,
  ///   ManualFileRequestType.Description,
  ///   ManualFileRequestType.DocumentTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequestType_Search
(
 @ManualFileRequestTypeId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ManualFileRequestTypeId = '-1'
    set @ManualFileRequestTypeId = null;
  
 
  select
         ManualFileRequestType.ManualFileRequestTypeId
        ,ManualFileRequestType.Description
        ,ManualFileRequestType.DocumentTypeCode
    from ManualFileRequestType
   where isnull(ManualFileRequestType.ManualFileRequestTypeId,'0')  = isnull(@ManualFileRequestTypeId, isnull(ManualFileRequestType.ManualFileRequestTypeId,'0'))
  
end
