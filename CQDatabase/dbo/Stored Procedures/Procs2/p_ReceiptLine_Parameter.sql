﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReceiptLine_Parameter
  ///   Filename       : p_ReceiptLine_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2013 07:02:53
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ReceiptLine table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ReceiptLine.ReceiptLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReceiptLine_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as ReceiptLineId
        ,null as 'ReceiptLine'
  union
  select
         ReceiptLine.ReceiptLineId
        ,ReceiptLine.ReceiptLineId as 'ReceiptLine'
    from ReceiptLine
  
end
