﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroup_Parameter
  ///   Filename       : p_OperatorGroup_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2012 07:59:18
  /// </summary>
  /// <remarks>
  ///   Selects rows from the OperatorGroup table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   OperatorGroup.OperatorGroupId,
  ///   OperatorGroup.OperatorGroup 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroup_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
  select
         OperatorGroup.OperatorGroupId,
         OperatorGroup.OperatorGroup 
    from OperatorGroup
  
end
