﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_Create
  ///   Filename       : p_MenuItem_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Aug 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItem_Create
(
 @MenuId           int,
 @MenuItemId       int = null output,
 @MenuItem         varchar(50),
 @NavigateTo       varchar(255),
 @ParentMenuItemId int,
 @OrderBy          int
)

as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_MenuItem_Create',
          @GetDate           datetime,
          @Transaction       bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  if exists(select 1 from MenuItem where MenuItem = @MenuItem and NavigateTo = @NavigateTo and MenuId = @MenuId)
    return
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end

  insert MenuItem (MenuId, MenuItem, ToolTip, NavigateTo, ParentMenuItemId, OrderBy)
  select @MenuId, @MenuItem,@MenuItem, @NavigateTo,	@ParentMenuItemId, @OrderBy
  
  select @Error = @@ERROR, @MenuItemId = SCOPE_IDENTITY()
  
  if @Error <> 0
    goto error

  /*
  declare @DeleteId int
  select @DeleteId = SCOPE_IDENTITY()
  select @DeleteId = 317
  delete OperatorGroupMenuItem where MenuItemId >= @DeleteId
  delete MenuItemCulture where MenuItemId >= @DeleteId
  delete MenuItem where MenuItemId >= @DeleteId
  exec sp_grants_reseed 'MenuItem'
  */
  insert OperatorGroupMenuItem
        (OperatorGroupId,
         MenuItemId,
         Access)
  select OperatorGroupId, @MenuItemId, 0
    from OperatorGroup og
   where not exists(select 1
                      from OperatorGroupMenuItem ogm
                     where @MenuItemId = ogm.MenuItemId
                       and og.OperatorGroupId = ogm.OperatorGroupId)
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error

  update OperatorGroupMenuItem set Access = 1 where MenuItemId = @MenuItemId and OperatorGroupId = 1
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error

  insert MenuItemCulture
        (MenuItemId,
         CultureId,
         MenuItem,
         ToolTip)
  select distinct MenuItemId,
         CultureId,
         MenuItem,
         ToolTip
    from MenuItem mi,
         Culture  c
   where not exists(select 1 from MenuItemCulture mic where mi.MenuItemId = mic.MenuItemId and c.CultureId = mic.CultureId)
  
  select @Error = @@ERROR
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
