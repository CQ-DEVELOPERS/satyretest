﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Planning_Pallet_Size_Options
  ///   Filename       : p_Planning_Pallet_Size_Options.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Planning_Pallet_Size_Options
(
 @OutboundShipmentId int = null
)

as
begin                                                                                                          
	 set nocount on;
  
  select Weight as 'Weight',
         Volume as 'Volume'
    from OutboundShipment (nolock)
   where OutboundShipmentId = @OutboundShipmentId
end
