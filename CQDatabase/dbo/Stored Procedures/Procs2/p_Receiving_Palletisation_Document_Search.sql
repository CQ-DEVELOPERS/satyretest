﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Palletisation_Document_Search
  ///   Filename       : p_Receiving_Palletisation_Document_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Palletisation_Document_Search
(
 @InboundDocumentTypeId	int,
 @InboundShipmentId     int,
 @ExternalCompanyCode	  nvarchar(30),
 @ExternalCompany	      nvarchar(255),
 @OrderNumber	          nvarchar(30),
 @FromDate	             datetime,
 @ToDate	               datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        InboundDocumentId         int,
        ReceiptId                 int,
        OrderNumber               nvarchar(30),
        InboundShipmentId         int,
        SupplierCode              nvarchar(30),
        Supplier                  nvarchar(255),
        NumberOfLines             int,
        DeliveryDate              datetime,
        CreateDate                datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        InboundDocumentType       nvarchar(30),
        LocationId                int,
        Location                  nvarchar(15),
        Rating                    int
       );
  
  if @InboundDocumentTypeId = -1
    set @InboundDocumentTypeId = null
  
  if @ExternalCompanyCode is null
    set @ExternalCompanyCode = '%'
  
  if @ExternalCompany is null
    set @ExternalCompany = '%'
  
  insert @TableResult
        (InboundDocumentId,
         ReceiptId,
         LocationId,
         OrderNumber,
         SupplierCode,
         Supplier,
         StatusId,
         Status,
         DeliveryDate,
         CreateDate,
         InboundDocumentType,
         Rating)
  select id.InboundDocumentId,
         r.ReceiptId,
         r.LocationId,
         id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         r.StatusId,
         s.Status,
         id.DeliveryDate,
         id.CreateDate,
         idt.InboundDocumentType,
         ec.Rating
    from InboundDocument     id  (nolock)
    join ExternalCompany     ec  (nolock) on id.ExternalCompanyId     = ec.ExternalCompanyId
    join Receipt             r   (nolock) on id.InboundDocumentId     = r.InboundDocumentId
    join Status              s   (nolock) on r.StatusId               = s.StatusId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
   where id.InboundDocumentTypeId    = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
     and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
     and isnull(r.DeliveryDate, id.DeliveryDate) between @FromDate and @ToDate
     and s.Type                      = 'R'
     and s.StatusCode               in ('R','P','RC')
  union
  select id.InboundDocumentId,
         r.ReceiptId,
         r.LocationId,
         id.OrderNumber,
         null,
         null,
         r.StatusId,
         s.Status,
         id.DeliveryDate,
         id.CreateDate,
         idt.InboundDocumentType,
         null
    from InboundDocument     id  (nolock)
    join Receipt             r   (nolock) on id.InboundDocumentId     = r.InboundDocumentId
    join Status              s   (nolock) on r.StatusId               = s.StatusId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
   where id.InboundDocumentTypeId    = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
     and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
     and isnull(r.DeliveryDate, id.DeliveryDate) between @FromDate and @ToDate
     and s.Type                      = 'R'
     and s.StatusCode               in ('R','P','RC')
     and not exists(select 1 from ExternalCompany ec (nolock) where id.ExternalCompanyId     = ec.ExternalCompanyId)
  
  delete r
    from @TableResult r
    join ReceiptLine rl (nolock) on r.ReceiptId      = rl.ReceiptId
    join Instruction  i (nolock) on rl.ReceiptLineId = i.ReceiptLineId
    join Job          j (nolock) on i.JobId          = j.JobId
    join Status       s (nolock) on j.StatusId       = s.StatusId
   where s.StatusCode = 'PR'
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from InboundLine il
                           where t.InboundDocumentId = il.InboundDocumentId)
    from @TableResult t
  
  update r
     set Location = l.Location
    from @TableResult r
    join Location     l (nolock) on r.LocationId = l.LocationId
  
  select InboundShipmentId,
         ReceiptId,
         OrderNumber,
         SupplierCode,
         Supplier,
         NumberOfLines,
         DeliveryDate,
         Status,
         InboundDocumentType,
         Location,
         Rating
    from @TableResult
end
