﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Principal_Delete
  ///   Filename       : p_Principal_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 08:15:18
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Principal table.
  /// </remarks>
  /// <param>
  ///   @PrincipalId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Principal_Delete
(
 @PrincipalId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Principal
     where PrincipalId = @PrincipalId
  
  select @Error = @@Error
  
  
  return @Error
  
end
