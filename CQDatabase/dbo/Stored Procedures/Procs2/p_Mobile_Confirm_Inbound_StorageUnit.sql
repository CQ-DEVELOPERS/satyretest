﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Confirm_Inbound_StorageUnit
  ///   Filename       : p_Mobile_Confirm_Inbound_StorageUnit.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Confirm_Inbound_StorageUnit
(
 @receiptId int,
 @barcode   nvarchar(50)
)

as
begin
	 set nocount on;
  
  declare @BatchId            int,
          @StorageUnitId      int

  select @StorageUnitId = su.StorageUnitId
    from Product      p (nolock)
    join StorageUnit su (nolock) on p.ProductId = su.ProductId
   where (Barcode     = @barcode
      or  ProductCode = @barcode)
  
  if @StorageUnitId is null
    select @StorageUnitId = StorageUnitId
      from Pack (nolock)
     where Barcode = @barcode
  
  if not exists(select sub.StorageUnitId
                  from ReceiptLine       rl (nolock)
                  join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
                 where sub.StorageUnitId = @StorageUnitId)
    set @StorageUnitId = -1
  
  select @StorageUnitId
  return @StorageUnitId
end
