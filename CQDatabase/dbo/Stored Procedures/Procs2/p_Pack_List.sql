﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pack_List
  ///   Filename       : p_Pack_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Nov 2012 14:41:25
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Pack table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Pack.PackId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pack_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as PackId
        ,null as 'Pack'
  union
  select
         Pack.PackId
        ,Pack.PackId as 'Pack'
    from Pack
  
end
