﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Packaging_Search_Barcode
  ///   Filename       : p_Packaging_Search_Barcode.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Packaging_Search_Barcode
(
 @barcode nvarchar(30)
)

as
begin
	 set nocount on;
	 
	 declare @JobId              int,
          @PalletId           int
  
  if isnumeric(replace(@barcode,'J:','')) = 1
    select @JobId   = replace(@barcode,'J:',''),
           @barcode = null
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @PalletId is not null
  begin
    select @JobId = JobId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where PalletId = @PalletId
      and it.InstructionTypeCode in ('PR','S','SM')
    
    if @JobId is null
      select -1 as 'JobId',
             PalletId,
             'P:' + convert(varchar(10), PalletId) as 'ReferenceNumber',
             Tare as 'TareWeight',
             Weight as 'GrossWeight'
        from Pallet
       where PalletId = @PalletId
    
    return
  end
  if @barcode is not null
  begin
    select @JobId = JobId
      from Job (nolock)
     where ReferenceNumber = @barcode
  end
  
  select JobId,
         -1 as 'PalletId',
         ReferenceNumber,
         TareWeight,
         Weight     as 'GrossWeight'
    from Job (nolock)
   where JobId = @JobId
end
