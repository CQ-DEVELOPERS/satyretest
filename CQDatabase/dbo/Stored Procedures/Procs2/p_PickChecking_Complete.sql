﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_PickChecking_Complete
  ///   Filename       : p_PickChecking_Complete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_PickChecking_Complete
(
 @WarehouseId        int = 1,
 @OutboundShipmentId int = null,
 @IssueId            int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_PickChecking_Complete'
  
  begin transaction
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  begin
    declare @TableHeader as table
    (
     OutboundShipmentId int,
     IssueId            int,
     IssueLineId        int,
     StatusCode         nvarchar(10)
    )
    
    declare @TableStatus as table
    (
     StatusCode         nvarchar(10)
    )
    
    if @OutboundShipmentId is null
    begin
--      if @IssueId is not null
--        insert @TableHeader
--              (OutboundShipmentId,
--               IssueId,
--               IssueLineId,
--               StatusCode)
--        select null,
--               il.IssueId,
--               il.IssueLineId,
--               s.StatusCode
--          from IssueLine         il (nolock)
--          join Status                  s (nolock) on il.StatusId = s.StatusId
--         where il.IssueId             = isnull(@IssueId, il.IssueId)
      if @IssueId is not null
        insert @TableHeader
              (OutboundShipmentId,
               IssueId,
               IssueLineId,
               StatusCode)
        select null,
             ili.IssueId,
             ili.IssueLineId,
             s.StatusCode
        from IssueLineInstruction ili (nolock)
        join Instruction          ins (nolocK) on ili.InstructionId = ins.InstructionId
        join Job                    j (nolock) on ins.JobId = j.JobId
        join Status                  s (nolock) on j.StatusId = s.StatusId
       where IssueId       = @IssueId
         and s.StatusCode in ('A','QA')
    end
    else
    begin
--      insert @TableHeader
--            (OutboundShipmentId,
--             IssueId,
--             IssueLineId,
--             StatusCode)
--      select osi.OutboundShipmentId,
--             il.IssueId,
--             il.IssueLineId,
--             s.StatusCode
--        from OutboundShipmentIssue osi (nolock)
--        join IssueLine              il (nolock) on osi.IssueId = il.IssueId
--        join Status                  s (nolock) on il.StatusId = s.StatusId
--       where osi.OutboundShipmentId = @OutboundShipmentId
      insert @TableHeader
            (OutboundShipmentId,
             IssueId,
             IssueLineId,
             StatusCode)
      select ili.OutboundShipmentId,
             ili.IssueId,
             ili.IssueLineId,
             s.StatusCode
        from IssueLineInstruction ili (nolock)
        join Instruction          ins (nolocK) on ili.InstructionId = ins.InstructionId
        join Job                    j (nolock) on ins.JobId = j.JobId
        join Status                  s (nolock) on j.StatusId = s.StatusId
       where ili.OutboundShipmentId = @OutboundShipmentId
         and s.StatusCode in ('A','QA')
    end
    
    insert @TableStatus
          (StatusCode)
    select distinct s.StatusCode
      from @TableHeader th
      join IssueLineInstruction ili on th.IssueLineId    = ili.IssueLineId
      join Instruction            i on ili.InstructionId = i.InstructionId
      join Job                    j on i.JobId           = j.JobId
      join Status                 s on j.StatusId        = s.StatusId
    
--    if exists(select top 1 1 from @TableStatus where StatusCode not in ('QA','WC','CD'))
--    begin
--      set @Error = -1
--      set @Errormsg = 'p_PickChecking_Complete - Not all lines have been checked!'
--      goto error
--    end
    
    if exists(select top 1 1 from @TableStatus where StatusCode in ('QA','A'))
      select @StatusId = dbo.ufn_StatusId('IS','QA')
    else
    begin
      --select @StatusId = dbo.ufn_StatusId('IS','WC')
    
    if      dbo.ufn_Configuration(41, @WarehouseId) = 1
      select @StatusId = dbo.ufn_StatusId('IS','WC')
      
    else if dbo.ufn_Configuration(84, @WarehouseId) = 1
      select @StatusId = dbo.ufn_StatusId('IS','CD')
      
    else if dbo.ufn_Configuration(39, @WarehouseId) = 1
      select @StatusId = dbo.ufn_StatusId('IS','D')
      
    else if dbo.ufn_Configuration(40, @WarehouseId) = 1
      select @StatusId = dbo.ufn_StatusId('IS','DC')
      
    else if dbo.ufn_Configuration(38, @WarehouseId) = 1
      select @StatusId = dbo.ufn_StatusId('IS','C')
    end
    
    update IssueLine
       set StatusId = @StatusId
      from @TableHeader th
      join IssueLine    il on th.IssueLineId = il.IssueLineId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert IssueLineHistory
          (IssueLineId,
           CommandType,
           InsertDate)
    select IssueLineId,
           'Update',
           @GetDate
      from @TableHeader
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update i
       set StatusId = @StatusId
      from @TableHeader th
      join Issue         i on th.IssueId = i.IssueId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert IssueHistory
          (IssueId,
           CommandType,
           InsertDate)
    select distinct
           IssueId,
           'Update',
           @GetDate
      from @TableHeader
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update os
       set StatusId = @StatusId
      from @TableHeader     th
      join OutboundShipment os on th.OutboundShipmentId = os.OutboundShipmentId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert OutboundShipmentHistory
          (OutboundShipmentId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           OutboundShipmentId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update j
       set StatusId = @StatusId
      from @TableHeader     th
      join Job               j on th.IssueLineId = j.IssueLineId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert JobHistory
          (JobId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           j.JobId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader     th
      join Job               j on th.IssueLineId = j.IssueLineId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
