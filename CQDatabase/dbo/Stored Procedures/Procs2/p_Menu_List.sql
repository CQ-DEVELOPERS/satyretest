﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Menu_List
  ///   Filename       : p_Menu_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:57
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Menu table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Menu.MenuId,
  ///   Menu.Menu 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Menu_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as MenuId
        ,'{All}' as Menu
  union
  select
         Menu.MenuId
        ,Menu.Menu
    from Menu
  
end
