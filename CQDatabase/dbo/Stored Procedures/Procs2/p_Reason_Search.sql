﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Reason_Search
  ///   Filename       : p_Reason_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:22
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Reason table.
  /// </remarks>
  /// <param>
  ///   @ReasonId int = null output,
  ///   @Reason nvarchar(100) = null,
  ///   @ReasonCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Reason.ReasonId,
  ///   Reason.Reason,
  ///   Reason.ReasonCode,
  ///   Reason.AdjustmentType,
  ///   Reason.ToWarehouseCode,
  ///   Reason.RecordType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Reason_Search
(
 @ReasonId int = null output,
 @Reason nvarchar(100) = null,
 @ReasonCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @Reason = '-1'
    set @Reason = null;
  
  if @ReasonCode = '-1'
    set @ReasonCode = null;
  
 
  select
         Reason.ReasonId
        ,Reason.Reason
        ,Reason.ReasonCode
        ,Reason.AdjustmentType
        ,Reason.ToWarehouseCode
        ,Reason.RecordType
    from Reason
   where isnull(Reason.ReasonId,'0')  = isnull(@ReasonId, isnull(Reason.ReasonId,'0'))
     and isnull(Reason.Reason,'%')  like '%' + isnull(@Reason, isnull(Reason.Reason,'%')) + '%'
     and isnull(Reason.ReasonCode,'%')  like '%' + isnull(@ReasonCode, isnull(Reason.ReasonCode,'%')) + '%'
  
end
