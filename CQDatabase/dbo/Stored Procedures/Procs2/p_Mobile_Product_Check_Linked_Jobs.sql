﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Linked_Jobs
  ///   Filename       : p_Mobile_Product_Check_Linked_Jobs.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Linked_Jobs
(
 @JobId int
)

as
begin
  set nocount on;
  
    select distinct j.JobId,
         j.ReferenceNumber,
         od.OrderNumber,
         j.Prints,
         j.Weight
    from Job         j (nolock)
    join Instruction i (nolock) on j.JobId = i.JobId
    join IssueLineInstruction	 ili(nolock) on i.IssueLineId = ili.IssueLineId
    join OutboundDocument od (nolock) on ili.OutboundDocumentId = od.OutboundDocumentId
   where (i.InstructionRefId in (select InstructionId from Instruction where JobId = @JobId)
      or  i.JobId = @JobId)
     and  j.ReferenceNumber like '%NB%'
  order by JobId
end
