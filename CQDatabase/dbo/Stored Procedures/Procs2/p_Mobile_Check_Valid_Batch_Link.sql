﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_Valid_Batch_Link
  ///   Filename       : p_Mobile_Check_Valid_Batch_Link.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jun 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Check_Valid_Batch_Link
(
 @location nvarchar(15),
 @product  nvarchar(30),
 @batch    nvarchar(50),
 @warehouseId int = 1
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
   WarehouseId        int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   StoreArea          nvarchar(max),
   Location           nvarchar(15),
   ProductCode        nvarchar(30),
   Product            nvarchar(255),
   SKUCode            nvarchar(max),
   Batch              nvarchar(50),
   ActualQuantity     float,
   AllocatedQuantity  float,
   ReservedQuantity   float,
   Boxes              nvarchar(50),
   BoxesQuantity      nvarchar(10),
   Pickface           nvarchar(max),
   BatchMatch         nvarchar(15),
   PickFaceQuantity   float
	 )
	 
	 declare @bool               bit,
	         @PalletId           int,
	         @StorageUnitBatchId int,
          @LocationId         int,
          @StorageUnitId      int,
          @Pickface           nvarchar(15),
          @AreaId             int,
          @Area               nvarchar(50),
          @StoreArea          nvarchar(max),
          @count              int,
          @StoreOrder         int
  
  select @location = replace(@location,'L:','')
	 
	 select @batch = replace(@batch,'A:','')
  select @batch = replace(@batch,'Q:','')
  select @batch = replace(@batch,'QC:','')

  if left(@product, 2) = 'P:' and ISNUMERIC(substring(@product, 3, 30)) = 1
  begin
    set @PalletId = convert(int, substring(@product, 3, 30))
    
    select @product = p.ProductCode
      from Pallet pal
      join StorageUnitBatch sub on pal.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit su on sub.StorageUnitId = su.StorageUnitId
      join Product p on su.ProductId = p.ProductId
     where pal.PalletId = @PalletId
  end

  if @product is not null and @batch is not null
  begin
    if @batch = ''
      insert @TableResult
            (WarehouseId,
             StorageUnitBatchId,
             StorageUnitId,
             Location,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
      select distinct a.WarehouseId,
             sub.StorageUnitBatchId,
             su.StorageUnitId,
             l.Location,
             p.ProductCode,
             p.Product,
             sku.SKUCode,
             b.Batch,
             subl.ActualQuantity,
             subl.AllocatedQuantity,
             subl.ReservedQuantity
        from StorageUnitBatch          sub (nolock)
        join StorageUnit                su (nolock) on sub.StorageUnitId     = su.StorageUnitId
        join Product                     p (nolock) on su.ProductId          = p.ProductId
        join SKU                       sku (nolock) on su.SKUId              = sku.SKUId
        join Batch                       b (nolock) on sub.BatchId           = b.BatchId
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
        join Location                    l (nolock) on subl.LocationId        = l.LocationId
        join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
        join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
        join Area                        a (nolock) on al.AreaId              = a.AreaId
        left
        join Pack                       pk (nolock) on su.StorageUnitId       = pk.StorageUnitId
                                                   and a.WarehouseId          = pk.WarehouseId
       where a.WarehouseId = @warehouseId
         and (l.Location     = @location or convert(nvarchar(15), l.SecurityCode) = @location)
         and (p.ProductCode = @product or p.barcode = @product or pk.barcode = @product)
    else
      insert @TableResult
            (WarehouseId,
             StorageUnitBatchId,
             StorageUnitId,
             Location,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             ActualQuantity,
             AllocatedQuantity,
             ReservedQuantity)
      select distinct a.WarehouseId,
             sub.StorageUnitBatchId,
             su.StorageUnitId,
             l.Location,
             p.ProductCode,
             p.Product,
             sku.SKUCode,
             b.Batch,
             subl.ActualQuantity,
             subl.AllocatedQuantity,
             subl.ReservedQuantity
        from StorageUnitBatch          sub (nolock)
        join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
        join Product                     p (nolock) on su.ProductId           = p.ProductId
        join SKU                       sku (nolock) on su.SKUId               = sku.SKUId
        join Batch                       b (nolock) on sub.BatchId            = b.BatchId
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
        join Location                    l (nolock) on subl.LocationId        = l.LocationId
        join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
        join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
        join Area                        a (nolock) on al.AreaId              = a.AreaId
        left
        join Pack                       pk (nolock) on su.StorageUnitId       = pk.StorageUnitId
                                                   and a.WarehouseId          = pk.WarehouseId
       where a.WarehouseId = @warehouseId
         and l.Location     = @location
         and (p.ProductCode = @product or p.barcode = @product or pk.barcode = @product)
         and b.Batch        = @batch
  end
  else
  begin
    insert @TableResult
          (WarehouseId,
           StorageUnitBatchId,
           StorageUnitId,
           Location,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           ActualQuantity,
           AllocatedQuantity,
           ReservedQuantity)
    select soh.WarehouseId,
           soh.StorageUnitBatchId,
           soh.StorageUnitId,
           soh.Location,
           soh.ProductCode,
           soh.Product,
           soh.SKUCode,
           soh.Batch,
           soh.ActualQuantity,
           soh.AllocatedQuantity,
           soh.ReservedQuantity
      from viewSOH soh
     where 1 = 2
  end
  
  update tr
     set Boxes = pt.PackType,
         BoxesQuantity = p.Quantity
    from @TableResult      tr
    join Pack               p (nolock) on tr.WarehouseId        = p.WarehouseId
                                      and tr.StorageUnitId      = p.StorageUnitId
    join PackType          pt (nolock) on p.PackTypeId          = pt.PackTypeId
  where pt.InboundSequence > 1
    and p.Quantity         > 1
  
  update @TableResult
     set SKUCode = SKUCode + isnull(' (' +  Boxes + ' of ' + BoxesQuantity + ' )','')
   where convert(float, BoxesQuantity) >= 1
  
  declare @TableStoreAreas as table
  (
   StorageUnitId int,
   AreaId        int,
   Area          nvarchar(50),
   StoreOrder    int
  )
  
  select @count = count(distinct(StorageUnitId))
    from @TableResult
  
  while @count > 0
  begin
    select @count = @count -1
    
    select @StorageUnitId = min(StorageUnitId)
      from @TableResult
     where StorageUnitId > isnull(@StorageUnitId, 0)
    
    insert @TableStoreAreas
          (StorageUnitId,
           AreaId,
           Area,
           StoreOrder)
    select distinct sua.StorageUnitId,
           sua.AreaId,
           a.Area,
           sua.StoreOrder
      from @TableResult         tr
      join StorageUnitArea sua (nolock) on tr.StorageUnitid = sua.StorageUnitId
      join Area              a (nolock) on sua.Areaid = a.AreaId
     where a.AreaCode in ('BK','RK','SP')
       and sua.StorageUnitId = @StorageUnitId
    
    set @StoreArea = null
    
    while exists(select top 1 1 from @TableStoreAreas)
    begin
      select top 1 @StoreArea  = isnull(@StoreArea,'') + convert(nvarchar(10), StoreOrder) + ') ' + Area + '<br />',
                   @AreaId       = AreaId,
                   @StoreOrder = StoreOrder
        from @TableStoreAreas
       where StorageUnitId = @StorageUnitId
      order by StoreOrder, Area
      
      delete @TableStoreAreas
       where StorageUnitId = @StorageUnitId
         and AreaId = @AreaId
    end
    
    update @TableResult
       set StoreArea = ISNULL(StoreArea, '') + @StoreArea
     where StorageUnitId = @StorageUnitId
  end
  
  update @TableResult
     set Pickface = l.Location + isnull(' (MAX: ' + CONVERT(nvarchar(20), sul.MaximumQuantity) + ')',''),
         PickFaceQuantity = subl.ActualQuantity
    from @TableResult         tr
    join StorageUnitLocation sul (nolock) on tr.StorageUnitid = sul.StorageUnitId
    join Location              l (nolock) on sul.LocationId = l.LocationId
    join AreaLocation         al (nolock) on l.LocationId = al.LocationId
    join Area                  a (nolock) on al.Areaid = a.AreaId
    left
    join StorageUnitBatchLocation subl (nolock) on tr.StorageUnitBatchId = subl.StorageUnitBatchId
                                               and l.LocationId          = subl.LocationId
   where a.WarehouseId = @warehouseId
     and a.AreaCode in ('PK','SP')
  
  update @TableResult
     set Pickface = l.Location + isnull(' (MAX: ' + CONVERT(nvarchar(20), sul.MaximumQuantity) + ')',''),
         PickFaceQuantity = subl.ActualQuantity
    from @TableResult               tr
    join StorageUnitBatch          sub (nolock) on tr.StorageUnitId = sub.StorageUnitId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    left
    join StorageUnitLocation       sul (nolock) on sub.StorageUnitId = sul.StorageUnitId
                                               and subl.LocationId = sul.LocationId
    join Location                    l (nolock) on subl.LocationId = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId = al.LocationId
    join Area                        a (nolock) on al.Areaid = a.AreaId
    join Batch                       b (nolock) on sub.BatchId = b.BatchId
   where a.WarehouseId = @warehouseId
     and a.AreaCode in ('PK','SP')
     and tr.Pickface is null
  
  if (select dbo.ufn_Configuration(372, @WarehouseId)) = 1 -- Back order - direct putaway to back order area
  begin
      update tr
         set Pickface = l.Location + isnull(' (MAX: ' + CONVERT(nvarchar(20), sul.MaximumQuantity) + ')',''),
             PickFaceQuantity = subl.ActualQuantity
        from @TableResult      tr
        join StorageUnitBatch sub (nolock) on tr.StorageUnitId       = sub.StorageUnitId
        join IssueLine         il (nolock) on sub.StorageUnitBatchId = il.StorageUnitBatchId
        join Status             s (nolock) on il.StatusId            = s.StatusId
        
        join Location              l (nolock) on 1 = 1
        join AreaLocation         al (nolock) on l.LocationId = al.LocationId
        join Area                  a (nolock) on al.Areaid = a.AreaId
        left
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                                                   and al.LocationId = subl.LocationId
        left
        join StorageUnitLocation       sul (nolock) on sub.StorageUnitId = sul.StorageUnitId
                                                   and subl.LocationId = sul.LocationId
      where s.StatusCode = 'BP'
        and a.WarehouseId = @warehouseId
        and a.AreaType in ('Backorder','CrossDock')
  end
  
  update tr
     set BatchMatch = l.Location
    from @TableResult tr
    join StorageUnitBatchLocation subl (nolock) on tr.StorageUnitBatchId = subl.StorageUnitBatchId
    join Location                    l (nolock) on subl.LocationId = l.LocationId
                                               and l.Location != tr.Location
    join AreaLocation               al (nolock) on l.LocationId = al.LocationId
    join Area                        a (nolock) on al.AreaId = a.AreaId
                                               and a.AreaCode in ('PK','RK','BK','SP')
                                               and a.WarehouseId = @warehouseId
  
  
  select StorageUnitBatchId,
         StoreArea,
         Location,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         ActualQuantity,
         AllocatedQuantity,
         ReservedQuantity,
         Pickface,
         PickFaceQuantity,
         BatchMatch
    from @TableResult
end
