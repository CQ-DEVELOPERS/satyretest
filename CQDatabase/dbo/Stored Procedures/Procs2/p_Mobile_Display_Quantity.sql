﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Display_Quantity
  ///   Filename       : p_Mobile_Display_Quantity.sql 
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Display_Quantity
(
 @InstructionId int
)

as
begin
	 set nocount on;
	 
	 declare @WarehouseId int,
	         @ConfirmedQuantity float,
	         @ActualQuantity    float
	 
	 select @WarehouseId       = i.WarehouseId,
	        @ConfirmedQuantity = i.ConfirmedQuantity,
	        @ActualQuantity    = isnull(subl.ActualQuantity, i.ConfirmedQuantity)
    from Instruction        i (nolock)
    left
    join StorageUnitBatchLocation subl (nolock) on i.StorageUnitBatchId = subl.StorageUnitBatchId
                                               and i.PickLocationId = subl.LocationId
   where i.InstructionId = @InstructionId
  
  if (select dbo.ufn_Configuration(381, @WarehouseId)) = 1 -- Mixed Picking - Display SOH as Quantity to Pick
  begin
    select @ActualQuantity as 'DisplayQuantity'
  end
  else if (select dbo.ufn_Configuration(430, @WarehouseId)) = 1 -- Mixed Picking - Display max SOH as Quantity to Pick
  begin
    if @ConfirmedQuantity > @ActualQuantity
      select @ActualQuantity as 'DisplayQuantity'
    else
      select @ConfirmedQuantity as 'DisplayQuantity'
  end
  else
  begin
    select null as 'DisplayQuantity'
  end
end
