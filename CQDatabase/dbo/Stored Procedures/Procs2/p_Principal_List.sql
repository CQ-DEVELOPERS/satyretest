﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Principal_List
  ///   Filename       : p_Principal_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 08:15:18
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Principal table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Principal.PrincipalId,
  ///   Principal.Principal 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Principal_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as PrincipalId
        ,'{All}' as Principal
  union
  select
         Principal.PrincipalId
        ,Principal.Principal
    from Principal
  order by Principal
  
end
