﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupMenuItem_Insert
  ///   Filename       : p_OperatorGroupMenuItem_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:24
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OperatorGroupMenuItem table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null output,
  ///   @MenuItemId int = null output,
  ///   @Access bit = null 
  /// </param>
  /// <returns>
  ///   OperatorGroupMenuItem.OperatorGroupId,
  ///   OperatorGroupMenuItem.MenuItemId,
  ///   OperatorGroupMenuItem.Access 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupMenuItem_Insert
(
 @OperatorGroupId int = null output,
 @MenuItemId int = null output,
 @Access bit = null 
)

as
begin
	 set nocount on;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
  if @MenuItemId = '-1'
    set @MenuItemId = null;
  
	 declare @Error int
 
  insert OperatorGroupMenuItem
        (OperatorGroupId,
         MenuItemId,
         Access)
  select @OperatorGroupId,
         @MenuItemId,
         @Access 
  
  select @Error = @@Error, @MenuItemId = scope_identity()
  
  
  return @Error
  
end
