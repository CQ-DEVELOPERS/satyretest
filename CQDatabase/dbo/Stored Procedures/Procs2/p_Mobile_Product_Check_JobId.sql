﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_JobId
  ///   Filename       : p_Mobile_Product_Check_JobId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Aug 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_JobId
(
 @operatorId      int,
 @referenceNumber nvarchar(30)
)

as
begin
  set nocount on;
  
  declare @JobId int
  
  if isnumeric(replace(@ReferenceNumber,'J:','')) = 1
  begin
    select @JobId   = replace(@ReferenceNumber,'J:','')
  end
  else
  begin
    select @JobId = JobId
      from Job
     where ReferenceNumber = @referenceNumber
  end
  
  select @JobId
end
