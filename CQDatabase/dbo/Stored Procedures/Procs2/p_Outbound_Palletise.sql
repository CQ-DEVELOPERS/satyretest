﻿CREATE PROCEDURE [dbo].[p_Outbound_Palletise]
(
 @WarehouseId        int,
 @OutboundShipmentId int,
 @IssueId            int,
 @OperatorId         int,
 @Weight             numeric(13,6) = null,
 @Volume             numeric(13,6) = null,
 @BulkPick           bit = null, -- GS Used to be set to 1 for Faamous Brands Midrand
 @SelectiveBulkPickByOrder  bit = null,
 @SelectiveBulkPickByCustomer bit = null,
 @PickByOrder        bit = null,
 @PickByCustomer     bit = null,
 @PickByFloStop      bit = null
-- @Mixed              tinyint
)
as
begin
  set nocount on;
  
  if @BulkPick is null and @SelectiveBulkPickByOrder is null and @SelectiveBulkPickByCustomer is null and @PickByOrder is null and @PickByCustomer is null -- If not specified get from config - GS 2015-04-10
  begin
    select @BulkPick = dbo.ufn_Configuration(478, @WarehouseId)
    select @SelectiveBulkPickByOrder = dbo.ufn_Configuration(479, @WarehouseId)
    select @PickByOrder = dbo.ufn_Configuration(480, @WarehouseId)
    select @PickByCustomer = dbo.ufn_Configuration(489, @WarehouseId)
    select @SelectiveBulkPickByCustomer = dbo.ufn_Configuration(493, @WarehouseId)
  end
  
  select @PickByFloStop = dbo.ufn_Configuration(494, @WarehouseId)
  
  if @PickByFloStop = 1
  begin
    if exists(select top 1 1
                from FloScheduleImport       f
                join OutboundDocument       od (nolock) on f.UniqueNumber = od.OrderNumber
                join Issue                   i (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
                join OutboundShipmentIssue osi (nolock) on osi.IssueId = i.IssueId
                join ExternalCompany        ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
                join Route                   r (nolock) on i.RouteId = r.RouteId
               where osi.OutboundShipmentId = @OutboundShipmentId
                 and f.RecordStatus = 'C')
    begin
      update osi 
         set DropSequence = ISNULL(f.StopNumber, isnull(ec.DropSequence, od.ExternalCompanyId))
        from FloScheduleImport       f
        join OutboundDocument       od (nolock) on f.UniqueNumber = od.OrderNumber
        join Issue                   i (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
        join OutboundShipmentIssue osi (nolock) on osi.IssueId = i.IssueId
        join ExternalCompany        ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
        join Route                   r (nolock) on i.RouteId = r.RouteId
       where osi.OutboundShipmentId = @OutboundShipmentId
         and f.RecordStatus = 'C'
    end
  end
  
  if (select dbo.ufn_Configuration(221, @WarehouseId)) = 1 or @PickByOrder = 1 or @SelectiveBulkPickByOrder = 1
  begin
    if @OutboundShipmentId > 0
    begin
      update osi 
         set DropSequence = osi.IssueId
        from OutboundShipmentIssue osi
       where osi.OutboundShipmentId = @OutboundShipmentId
      
      update i
         set DropSequence = osi.IssueId
        from OutboundShipmentIssue osi
        join Issue i on osi.IssueId = i.IssueId
       where osi.OutboundShipmentId = @OutboundShipmentId
    end
    else if @IssueId > 0
    begin
      update i
         set DropSequence = i.IssueId
        from Issue i
       where i.IssueId = @IssueId
    end
  end
  else if @PickByCustomer = 1 or @SelectiveBulkPickByCustomer = 1 -- GS Added for Famous Brands (Want selective bulk pick by customer)
  begin
    if @OutboundShipmentId > 0
    begin
        if @PickByFloStop = 0
        begin
          update osi 
             set DropSequence = isnull(ec.DropSequence, od.ExternalCompanyId)
            from OutboundShipmentIssue osi
            join Issue                   i (nolock) on osi.IssueId = i.IssueId
            join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
            join ExternalCompany        ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
           where osi.OutboundShipmentId = @OutboundShipmentId
        end
        
        update i
           set DropSequence = osi.DropSequence
          from OutboundShipmentIssue osi
          join Issue i on osi.IssueId = i.IssueId
         where osi.OutboundShipmentId = @OutboundShipmentId
    end
    else if @IssueId > 0
    begin
      update i
         set DropSequence = i.IssueId
        from Issue i
       where i.IssueId = @IssueId
    end
  end
  else if @BulkPick = 1
  begin
    if @OutboundShipmentId > 0
    begin
      update osi 
         set DropSequence = 1
        from OutboundShipmentIssue osi
       where osi.OutboundShipmentId = @OutboundShipmentId
      
      update i
         set DropSequence = 1
        from OutboundShipmentIssue osi
        join Issue i on osi.IssueId = i.IssueId
       where osi.OutboundShipmentId = @OutboundShipmentId
    end
    else if @IssueId > 0
    begin
      update i
         set DropSequence = 1
        from Issue i
       where i.IssueId = @IssueId
    end
  end
  
  Declare @MaxAreaSequence int
  
  declare @TableIssueLines as table
  (
   IssueId     int,
   IssueLineId int
  )
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @StoreLocationId     int,
          @DelIssueId          int,
          @DelIssueLineId      int,
          @FP                  bit,
          @SS                  bit,
          @LP                  bit,
          @FM                  bit,
          @MP     bit,
          @PP                  bit,
          @PriorityId          int,
          @AlternatePallet     bit,
          @SubstitutePallet    bit,
          @ExternalCompanyCode nvarchar(30),
          @ToWarehouseId       int,
          @rowcount            int,
          @OrderLineSequence   bit,
          @OneProductPerPallet bit,
          @JobId               int
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @OutboundShipmentId is not null
  begin
    set @IssueId = null
    
    select @ExternalCompanyCode = ec.ExternalCompanyCode,
           @OrderLineSequence   = ec.OrderLineSequence,
           @OneProductPerPallet = ec.OneProductPerPallet
      from OutboundShipmentIssue osi (nolock)
      join Issue             i (nolock) on osi.IssueId = i.IssueId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join ExternalCompany  ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
     where osi.OutboundShipmentId = @OutboundShipmentId
    
    --select @ToWarehouseId = ToWarehouseId
    --  from WarehouseCodeReference (nolock)
    -- where WarehouseId = @WarehouseId
    --   and ToWarehouseCode = @ExternalCompanyCode
    
    insert InterfaceExportOrderPick
          (IssueId,
           OrderNumber,
           RecordStatus,
           ProcessedDate)
    select i.IssueId,
           od.OrderNumber,
           'N',
           null
      from OutboundShipmentIssue osi (nolock)
      join Issue                   i (nolock) on osi.IssueId = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId      = od.OutboundDocumentId
      join OutboundDocumentType  odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and odt.OutboundDocumentTypeCode in ('EXP','DEL','FAR','COL','SAL','3RD')
       and not exists(select top 1 1 from InterfaceExportOrderPick op where i.IssueId = op.IssueId)
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert InterfaceExportHeader
          (PrimaryKey,
           OrderNumber,
           IssueId,
           RecordType,
           RecordStatus,
           PrincipalCode)
    select convert(nvarchar(10), i.IssueId),
           od.OrderNumber,
           i.IssueId,
           'Allocated',
           'N',
           p.PrincipalCode
      from Issue                   i (nolock)
      join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join Principal               p (nolock) on od.PrincipalId = p.PrincipalId
      left
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
      left
      join InterfaceExportHeader   h (nolock) on od.OrderNumber = h.OrderNumber -- Only insert 1 record per Inbound Document
                                           and h.RecordType = 'Allocated'
     where (i.IssueId = @IssueId or osi.OutboundShipmentId = @OutboundShipmentId)
       and h.InterfaceExportHeaderId is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    if exists(select 1
                from OutboundShipmentIssue osi (nolock)
                join Issue                   i (nolock) on osi.IssueId = i.IssueId
                join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
               where osi.OutboundShipmentId = @OutboundShipmentId
                 and od.OrderNumber like 'MO%'
                 and (i.LocationId is null or i.DespatchBay is null))
    begin
      set @Error = -1
      goto error
    end
  end
  else if @IssueId = -1
    set @IssueId = null
  
  if @IssueId is not null
  begin
    select @ExternalCompanyCode = ec.ExternalCompanyCode,
           @OrderLineSequence   = ec.OrderLineSequence,
           @OneProductPerPallet = ec.OneProductPerPallet
      from Issue             i (nolock)
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
      join ExternalCompany  ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
     where IssueId = @IssueId
    
    --select @ToWarehouseId = ToWarehouseId
    --  from WarehouseCodeReference (nolock)
    -- where WarehouseId = @WarehouseId
    --   and ToWarehouseCode = @ExternalCompanyCode
    
    insert InterfaceExportOrderPick
          (IssueId,
           OrderNumber,
           RecordStatus,
           ProcessedDate)
    select i.IssueId,
           od.OrderNumber,
           'N',
           null
      from Issue                   i (nolock)
      join OutboundDocument       od (nolock) on i.OutboundDocumentId      = od.OutboundDocumentId
      join OutboundDocumentType  odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
     where i.IssueId = @IssueId
       and odt.OutboundDocumentTypeCode in ('EXP','DEL','FAR','COL','SAL','3RD')
       and not exists(select top 1 1 from InterfaceExportOrderPick op where i.IssueId = op.IssueId)
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    if exists(select 1
                from Issue                   i (nolock)
                join OutboundDocument       od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
               where i.IssueId = @IssueId
                 and od.OrderNumber like 'MO%'
                 and (i.LocationId is null or i.DespatchBay is null))
    begin
      set @Error = -1
      goto error
    end
  end
  
  if @OutboundShipmentId is not null
    select @Weight = Weight,
           @Volume = Volume,
           @FP     = FP,
           @SS     = SS,
           @LP     = LP,
           @FM     = FM,
           @MP     = MP,
           @PP     = PP,
           @AlternatePallet = AlternatePallet,
           @SubstitutePallet = SubstitutePallet
      from OutboundShipment (nolock)
     where OutboundShipmentId = @OutboundShipmentId
  
  if isnull(@AlternatePallet,0) = 1 and isnull(@SubstitutePallet,0) = 1
  begin
    goto error
  end
  
  if @FP is null
    select @FP = dbo.ufn_Configuration(48, @WarehouseId)
  
  if @SS is null
    select @SS = dbo.ufn_Configuration(49, @WarehouseId)
  
  if @LP is null
    select @LP = dbo.ufn_Configuration(50, @WarehouseId)
  
  if @FM is null
    select @FM = dbo.ufn_Configuration(51, @WarehouseId)
  
  if @MP is null
    select @MP = dbo.ufn_Configuration(52, @WarehouseId)
  
  if @PP is null
    select @PP = dbo.ufn_Configuration(53, @WarehouseId)
  
  if @Weight is null
  begin
    select @Weight = convert(decimal(13,3), Value)
      from Configuration
     where WarehouseId = @WarehouseId
       and ConfigurationId = 54
  end
  
  if @Volume is null
  begin
    select @Volume = convert(decimal(13,3), Value)
      from Configuration
     where WarehouseId = @WarehouseId
       and ConfigurationId = 55
  end
  
  if @OrderLineSequence is null
    set @OrderLineSequence = 0
  
  if @OneProductPerPallet is null
    set @OneProductPerPallet = 0
  
  begin transaction
  
  if @OutboundShipmentId is not null
  begin
    delete PalletiseStock where OutboundShipmentId = @OutboundShipmentId
    
    if @@Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error delete PalletiseStock (OutboundShipmentId)'
      goto error 
    end
  end
  
  if @IssueId is not null
  begin
    delete PalletiseStock where IssueId = @IssueId
    
    if @@Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error delete PalletiseStock (IssueId)'
      goto error 
    end
  end
  
  exec @Error = p_Palletise_Reverse
   @OutboundShipmentId = @OutboundShipmentId,
   @IssueId            = @IssueId
  
  if @Error <> 0
  begin
    set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Palletise_Reverse, p_Outbound_Palletise_Check'
    goto error 
  end
  
  /******************************/
  /* Insert the values into the */
  /* table for palletisation    */
  /******************************/
  if @OutboundShipmentId is null
  begin
    select @StoreLocationId = LocationId
      from Issue (nolock)
     where IssueId = @IssueId
    
    insert PalletiseStock
          (OutboundShipmentId,
           IssueId,
           IssueLineId,
           DropSequence,
           StorageUnitBatchId,
           StorageUnitId,
           ProductCode,
           SKUCode,
           Quantity,
           OriginalQuantity,
           ExternalCompanyId,
           LineNumber)
    select -1,
           il.IssueId,
           min(il.IssueLineId),
           case when @OneProductPerPallet = 0
                then isnull(i.DropSequence,1)
                else ol.OutboundLineId
                end,
           il.StorageUnitBatchId,
           sub.StorageUnitId,
           p.ProductCode,
           sku.SKUCode,
           (isnull(max(AllocationCategory), 100) / 100.000) * convert(numeric(13,6), sum(il.Quantity)),
           (isnull(min(AllocationCategory), 100) / 100.000) * convert(numeric(13,6), sum(il.Quantity)),
           pec.ExternalCompanyId,
           case when @OrderLineSequence = 1
                then ol.LineNumber
                else 0
                end
      from IssueLine         il
      join OutboundLine      ol (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join Issue              i (nolock) on il.IssueId            = i.IssueId
      join OutboundDocument  od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product            p (nolock) on su.ProductId          = p.ProductId
      join SKU              sku (nolock) on su.SKUId              = sku.SKUId
      left 
      join StorageUnitExternalCompany suec on su.StorageUnitId = suec.StorageUnitId
                                          and od.ExternalCompanyId = suec.ExternalCompanyId
      left 
      join PackExternalCompany         pec on su.StorageUnitId     = pec.StorageUnitId
                                          and od.ExternalCompanyId = pec.ExternalCompanyId
                                          and pec.WarehouseId      = i.WarehouseId
     where il.IssueId                = isnull(@IssueId, -1)
       and isnull(p.ProductType,'') != 'PRODUCTION'
--       and not exists(select top 1 1 from Instruction i where i.IssueLineId = il.IssueLineId)
    group by il.IssueId,
             case when @OneProductPerPallet = 0
                  then isnull(i.DropSequence,1)
                  else ol.OutboundLineId
                  end,
             il.StorageUnitBatchId,
             sub.StorageUnitId,
             p.ProductCode,
             sku.SKUCode,
             pec.ExternalCompanyId,
             case when @OrderLineSequence = 1
                then ol.LineNumber
                else 0
                end
   
   select @Error = @@ERROR
   
   if @Error <> 0
     goto error
   
    update il
       set StatusId = dbo.ufn_StatusId('IS','CD')
      from IssueLine         il
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product            p (nolock) on su.ProductId          = p.ProductId
     where il.IssueId                = isnull(@IssueId, -1)
       and (isnull(p.ProductType,'') = 'PRODUCTION'
        or  il.Quantity             <= 0)
     
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
  end
  else
  begin
    select @StoreLocationId = LocationId
      from OutboundShipment (nolock)
     where OutboundShipmentId = @OutboundShipmentId
    
    insert PalletiseStock
          (OutboundShipmentId,
           IssueId,
           IssueLineId,
           DropSequence,
           StorageUnitBatchId,
           StorageUnitId,
           ProductCode,
           SKUCode,
           Quantity,
           OriginalQuantity,
           ExternalCompanyId,
           LineNumber)
    select osi.OutboundShipmentId,
           -1,
           null,--il.IssueLineId,
           --isnull(osi.DropSequence,1),
           case when @OneProductPerPallet = 0
                then case when @OrderLineSequence = 1
                          then isnull(i.DropSequence,1)
                          when @SelectiveBulkPickByOrder = 1 or @SelectiveBulkPickByCustomer = 1
                          then case when p.ProductType = 'BULK'
                                    then 99999
                                    else isnull(i.DropSequence,1)
                                    end
                          else case when p.ProductType in ('LCP','Perla','BULK') and @BulkPick = 0
                                    then 0
                                    when p.ProductType in ('NON PG', 'NON P&G') and @BulkPick = 0
                                    then 99999
                                    else isnull(osi.DropSequence,isnull(i.DropSequence,1))
                                    end
                          end
                else ol.OutboundLineId
                end,
           il.StorageUnitBatchId,
           sub.StorageUnitId,
           p.ProductCode,
           sku.SKUCode,
           (isnull(min(AllocationCategory), 100) / 100.000) * convert(numeric(13,6), sum(il.Quantity)),
           (isnull(min(AllocationCategory), 100) / 100.000) * convert(numeric(13,6), sum(il.Quantity)),
           pec.ExternalCompanyId,
           case when @OrderLineSequence = 1
                then ol.LineNumber
                else 0
                end
      from OutboundShipmentIssue osi (nolock)
      join IssueLine              il (nolock) on osi.IssueId           = il.IssueId
      join OutboundLine           ol (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join Issue                   i (nolock) on il.IssueId            = i.IssueId
      join OutboundDocument       od (nolock) on i.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit            su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                 p (nolock) on su.ProductId          = p.ProductId
      join SKU                   sku (nolock) on su.SKUId              = sku.SKUId
      left 
      join StorageUnitExternalCompany suec on su.StorageUnitId = suec.StorageUnitId
                                          and od.ExternalCompanyId = suec.ExternalCompanyId
      left 
      join PackExternalCompany         pec on su.StorageUnitId     = pec.StorageUnitId
                                          and od.ExternalCompanyId = pec.ExternalCompanyId
                                          and pec.WarehouseId      = i.WarehouseId
     where OutboundShipmentId        = isnull(@OutboundShipmentId, -1)
       and isnull(p.ProductType,'') != 'PRODUCTION'
--       and not exists(select top 1 1 from IssueLineInstruction ili where ili.IssueLineId = il.IssueLineId)
    group by osi.OutboundShipmentId,
             --isnull(osi.DropSequence,1),
             case when @OneProductPerPallet = 0
                then case when @OrderLineSequence = 1
                          then isnull(i.DropSequence,1)
                          when @SelectiveBulkPickByOrder = 1 or @SelectiveBulkPickByCustomer = 1
                          then case when p.ProductType = 'BULK'
                                    then 99999
                                    else isnull(i.DropSequence,1)
                      end
                          else case when p.ProductType in ('LCP','Perla','BULK') and @BulkPick = 0
                                    then 0
                                    when p.ProductType in ('NON PG', 'NON P&G') and @BulkPick = 0
                                    then 99999
                                    else isnull(osi.DropSequence,isnull(i.DropSequence,1))
                                    end
                          end
                else ol.OutboundLineId
                end,
             il.StorageUnitBatchId,
             sub.StorageUnitId,
             p.ProductCode,
             sku.SKUCode,
             pec.ExternalCompanyId,
             case when @OrderLineSequence = 1
                then ol.LineNumber
                else 0
                end
    
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
   
    update il
       set StatusId = dbo.ufn_StatusId('IS','CD')
      from OutboundShipmentIssue osi (nolock)
      join IssueLine              il (nolock) on osi.IssueId           = il.IssueId
      join StorageUnitBatch      sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit            su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                 p (nolock) on su.ProductId          = p.ProductId
     where OutboundShipmentId        = isnull(@OutboundShipmentId, -1)
       and (isnull(p.ProductType,'') = 'PRODUCTION'
        or  il.Quantity             <= 0)
     
    select @Error = @@ERROR
    
    if @Error <> 0
      goto error
  end
  
  --update ps
  --   set AreaSequence = a.AreaSequence
  --  from PalletiseStock   ps
  --  join Product           p (nolock) on ps.ProductCode   = p.ProductCode
  --  join SKU             sku (nolock) on ps.SKUCode       = sku.SKUCode
  --  join StorageUnit      su (nolock) on p.ProductId      = su.ProductId
  --                                   and sku.SKUId        = su.SKUId
  --  join StorageUnitArea sua (nolock) on su.StorageUnitId = sua.StorageUnitId
  --  join Area              a (nolock) on sua.AreaId       = a.AreaId
  -- where a.WarehouseId = @WarehouseId
  --   and a.AreaCode in ('RK','BK','SP','PK')
  
  -- Update prefered area first
  update ps
     set AreaSequence = (select max(a.AreaSequence) 
                          from StorageUnit      su (nolock)
                          join StorageUnitArea sua (nolock) on su.StorageUnitId = sua.StorageUnitId
                          join Area              a (nolock) on sua.AreaId       = a.AreaId
                         where a.WarehouseId = @WarehouseId
                           and a.AreaCode in ('RK','BK','SP','PK')
                           and su.StorageUnitId = ps.StorageUnitId
                           and sua.StoreOrder = 1)
    from PalletiseStock   ps
   where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and ps.IssueId            = isnull(@IssueId, -1)
  
  -- Update in case no prefered
  update ps
     set AreaSequence = (select max(a.AreaSequence) 
                          from StorageUnit      su (nolock)
                          join StorageUnitArea sua (nolock) on su.StorageUnitId = sua.StorageUnitId
                          join Area              a (nolock) on sua.AreaId       = a.AreaId
                         where a.WarehouseId = @WarehouseId
                           and a.AreaCode in ('RK','BK','SP','PK')
                           and su.StorageUnitId = ps.StorageUnitId)
    from PalletiseStock   ps
   where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and ps.IssueId            = isnull(@IssueId, -1)
     and ps.AreaSequence is null
  
  --update ps
  --   set DropSequence = 1
  --  from PalletiseStock ps
  --  join Product         p on ps.ProductCode = p.ProductCode 
  -- where p.ProductType in ('LCP','Perla','BULK')
  
  if @OrderLineSequence = 0 and @OneProductPerPallet = 0
  begin
    if (select dbo.ufn_Configuration(172, @WarehouseId)) = 1 -- Outbound Palletising - Make each pick a sigle job
    begin
      update PalletiseStock
         set AreaSequence = 0
       where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
         and IssueId            = isnull(@IssueId, -1)
      
      set @rowcount = @@ROWCOUNT
      
      while @rowcount > 0
      begin
        set rowcount 1
        update PalletiseStock
           set AreaSequence = @rowcount
         where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and IssueId            = isnull(@IssueId, -1)
           and AreaSequence       = 0
        set rowcount 0
        
        set @rowcount = @rowcount - 1
      end
    end
    
    if (select dbo.ufn_Configuration(443, @WarehouseId)) = 0 -- Outbound Palletising - Ignore BOM Sequence
    begin
      declare @BOM as table
      (
       StorageUnitId    int
      ,BOMInstructionId int
      )
      
      insert @BOM
            (StorageUnitId
            ,BOMInstructionId)
      select ps.StorageUnitId,
             bi.BOMInstructionId
        from PalletiseStock             ps (nolock)
      join Issue                i (nolock) on ps.IssueId           = i.IssueId
      join BOMInstruction      bi (nolock) on i.OutboundDocumentId = bi.OutboundDocumentId
      join BOMInstructionLine bil (nolock) on bi.BOMInstructionId  = bil.BOMInstructionId
      join BOMProduct          bp (nolock) on bil.BOMLineId        = bp.BOMLineId
                                          and ps.StorageUnitId     = bp.StorageUnitId
       where bp.LineNumber = 1
         and ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
         and ps.IssueId            = isnull(@IssueId, -1)
        group by ps.StorageUnitId,
                 bi.BOMInstructionId
    end
    
    --if (select dbo.ufn_Configuration(439, @WarehouseId)) = 1 -- Palletise reduce ordered quantity by available
    --begin
    --  update ps
    --     set Quantity = case when ps.Quantity > (va.ActualQuantity - va.ReservedQuantity)
    --                         then ps.Quantity
    --                         else case when (va.ActualQuantity - va.ReservedQuantity) > 0
    --                                   then (va.ActualQuantity - va.ReservedQuantity)
    --                                   else 0
    --                                   end
    --                         end
    --    from PalletiseStock ps
    --    join viewAvailable  va on ps.StorageUnitId = va.StorageUnitId
    --   where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
    --     and ps.IssueId            = isnull(@IssueId, -1)
    --end
    
    if (select dbo.ufn_Configuration(118, @WarehouseId)) = 1 -- Palletise No-Stocks on separate pallets
    or (select dbo.ufn_Configuration(439, @WarehouseId)) = 1 -- Palletise reduce ordered quantity by available
    or (select dbo.ufn_Configuration(491, @WarehouseId)) = 1 -- Palletise fail if not 100% SOH available
    begin
      declare @StockOnHand as table
      (
       StorageUnitId    int,
       ActualQuantity   int,
       RequiredQuantity int
      )
      
      insert @StockOnHand
            (StorageUnitId,
             RequiredQuantity)
      select ps.StorageUnitId,
             sum(ps.Quantity)
        from PalletiseStock             ps (nolock)
        left
        join @BOM                      bom on ps.StorageUnitId = bom.StorageUnitId
       where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
         and ps.IssueId            = isnull(@IssueId, -1)
         and bom.StorageUnitId is null
      group by ps.StorageUnitId
      
      update soh
         set ActualQuantity = (select sum(subl.ActualQuantity)
                                 from StorageUnitBatch          sub (nolock)
                                 join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                                 join AreaLocation     al (nolock) on subl.LocationId = al.LocationId
                                 join Area                        a (nolock) on al.AreaId = a.AreaId
                                where a.WarehouseId = @WarehouseId
                                  and a.StockOnHand     = 1
                                  and sub.StorageUnitId = soh.StorageUnitId)
        from @StockOnHand soh
      
      if (select dbo.ufn_Configuration(491, @WarehouseId)) = 1 -- Palletise fail if not 100% SOH available
      begin
       if exists(select top 1 1
                   from PalletiseStock ps
                  where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
                    and ps.IssueId            = isnull(@IssueId, -1)
                  group by ps.StorageUnitId
                  having COUNT(distinct(StorageUnitBatchId)) > 1)
        begin
          if exists(select top 1 1
                      from PalletiseStock    ps
                      join viewAvailableSUB  va on ps.StorageUnitBatchId = va.StorageUnitBatchId
                     where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
                       and ps.IssueId            = isnull(@IssueId, -1)
                       and (va.ActualQuantity - va.ReservedQuantity) < ps.Quantity)
            select @Error = -1
          
          if @Error <> 0
            goto error
        end
        else
        begin
          if exists(select top 1 1
                      from PalletiseStock    ps
                      join viewAvailable     va on ps.StorageUnitId = va.StorageUnitId
                     where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
                       and ps.IssueId            = isnull(@IssueId, -1)
                       and (va.ActualQuantity - va.ReservedQuantity) < ps.Quantity)
            select @Error = -1
          
          if @Error <> 0
            goto error
        end
      end
      else if (select dbo.ufn_Configuration(439, @WarehouseId)) = 1 -- Palletise reduce ordered quantity by available
      begin
        if exists(select top 1 1
                   from PalletiseStock ps
                  where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
                    and ps.IssueId            = isnull(@IssueId, -1)
                  group by ps.StorageUnitId
                  having COUNT(distinct(StorageUnitBatchId)) > 1)
        begin
          update ps
             set Quantity = case when (va.ActualQuantity - va.ReservedQuantity) >= ps.Quantity
                                 then ps.Quantity
                                 else case when (va.ActualQuantity - va.ReservedQuantity) > 0
                                           then (va.ActualQuantity - va.ReservedQuantity)
                                           else 0
                                           end
                                 end
            from PalletiseStock    ps
            join viewAvailableSUB  va on ps.StorageUnitBatchId = va.StorageUnitBatchId
           where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
             and ps.IssueId            = isnull(@IssueId, -1)
           
          select @Error = @@ERROR
          
          if @Error <> 0
            goto error
        end
        else
        begin
          update ps
             set Quantity = case when (va.ActualQuantity - va.ReservedQuantity) >= ps.Quantity
                                 then ps.Quantity
                                 else case when (va.ActualQuantity - va.ReservedQuantity) > 0
                                           then (va.ActualQuantity - va.ReservedQuantity)
                                           else 0
                                           end
                                 end
            from PalletiseStock    ps
            join viewAvailable     va on ps.StorageUnitId = va.StorageUnitId
           where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
             and ps.IssueId            = isnull(@IssueId, -1)
           
          select @Error = @@ERROR
          
          if @Error <> 0
            goto error
        end
        
        update PalletiseStock
           set OriginalQuantity = Quantity
         where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and IssueId            = isnull(@IssueId, -1)
         
        select @Error = @@ERROR
        
        if @Error <> 0
          goto error
        
        update il
           set SOHQuantity = 0
          from OutboundShipmentIssue osi
          join IssueLine              il on osi.IssueId = il.IssueId
         where osi.OutboundShipmentId = @OutboundShipmentId
           and il.SOHQuantity is null
         
        select @Error = @@ERROR
        
        if @Error <> 0
          goto error
        
        update il
           set SOHQuantity = 0
          from IssueLine      il
         where IssueId = @IssueId
           and SOHQuantity is null
         
        select @Error = @@ERROR
        
        if @Error <> 0
          goto error
        
        update il
           set il.SOHQuantity = ps.Quantity
           from PalletiseStock ps
           join IssueLine      il on ps.IssueLineId = il.IssueLineId
         where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and ps.IssueId            = isnull(@IssueId, -1)
         
        select @Error = @@ERROR
        
        if @Error <> 0
          goto error
      end
      else
      if (select dbo.ufn_Configuration(368, @WarehouseId)) = 1 -- Palletise - On 1 pallet (Ignore AreaSequence)
      begin
        update ps
           set AreaSequence = 999
          from PalletiseStock ps
          join @StockOnHand  soh on ps.StorageUnitId = soh.StorageUnitId
         where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and ps.IssueId            = isnull(@IssueId, -1)
           and isnull(soh.ActualQuantity,0) < soh.RequiredQuantity
      end
      else
      begin
        update ps
           set AreaSequence = AreaSequence + 10
          from PalletiseStock ps
          join @StockOnHand  soh on ps.StorageUnitId = soh.StorageUnitId
         where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and ps.IssueId            = isnull(@IssueId, -1)
           and isnull(soh.ActualQuantity,0) < soh.RequiredQuantity
      end
    end
    
    if (select dbo.ufn_Configuration(188, @WarehouseId)) = 1 -- Palletise Products for Repacking on separate pallets
    begin
      update ps
         set AreaSequence = isnull(ps.AreaSequence,1) + 11
        from PalletiseStock ps
        join PackExternalCompany pec on ps.ExternalCompanyId = pec.ExternalCompanyId
                                    and ps.StorageUnitId     = pec.StorageUnitId
                                    and pec.WarehouseId      = @WarehouseId
       where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
         and ps.IssueId            = isnull(@IssueId, -1)
    end
    
    if (select dbo.ufn_Configuration(189, @WarehouseId)) = 1 -- Palletise Variable Weights on separate pallets
    begin
      update ps
         set AreaSequence = isnull(ps.AreaSequence,1) + 12
        from PalletiseStock ps
        join StorageUnit    su on ps.StorageUnitId = su.StorageUnitId
       where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
         and ps.IssueId            = isnull(@IssueId, -1)
         and su.ProductCategory    = 'V'
    end
  end
  
  if (select dbo.ufn_Configuration(394, @WarehouseId)) = 1 -- Palletise Wave SOH Separate to Pickface SOH
  begin
    update ps
       set AreaSequence = isnull(ps.AreaSequence,1) + 13
      from PalletiseStock ps
      join viewAvailable va on ps.StorageUnitId = va.StorageUnitId
                           and va.WarehouseId  = @WarehouseId
                           and va.AreaType      = 'Backup'
     where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and ps.IssueId            = isnull(@IssueId, -1)
  end
  
  if (select dbo.ufn_Configuration(269, @WarehouseId)) = 1 -- Outbound Palletising - Update Area Sequence to 1
    update PalletiseStock
       set AreaSequence = 1
     where AreaSequence is null
  else
    update PalletiseStock
       set AreaSequence = 99
     where AreaSequence is null
  
  -- Palletise product separatly base on Packing category - 2012-01-25
  update ps
     set AreaSequence = isnull(ps.AreaSequence,1) * su.PackingCategory
    from PalletiseStock ps
    join StorageUnit    su on ps.StorageUnitId = su.StorageUnitId
   where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and ps.IssueId            = isnull(@IssueId, -1)
     and su.PackingCategory   is not null
   
  --select @AlternatePallet as '@AlternatePallet', @SubstitutePallet as '@SubstitutePallet'
  
  if isnull(@AlternatePallet, 0) = 0 and isnull(@SubstitutePallet, 0) = 0
  begin
    update PalletiseStock
       set PalletQuantity = p.Quantity
      from PalletiseStock ps
      join Pack            p (nolock) on ps.StorageUnitId = p.StorageUnitId
      join PackType       pt (nolock) on p.PackTypeId     = pt.PackTypeId
     where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and ps.IssueId            = isnull(@IssueId, -1)
       and pt.OutboundSequence   = 1
       and p.WarehouseId         = isnull(@ToWarehouseId, @WarehouseId)
    
    update PalletiseStock
       set BoxQuantity = p.Quantity
      from PalletiseStock ps
      join Pack            p (nolock) on ps.StorageUnitId = p.StorageUnitId
      join PackType       pt (nolock) on p.PackTypeId     = pt.PackTypeId
     where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and ps.IssueId            = isnull(@IssueId, -1)
       and p.WarehouseId         = isnull(@ToWarehouseId, @WarehouseId)
       and pt.OutboundSequence   between 2 and (select max(OutboundSequence) - 1 from PackType (nolock))
  end
  else if @AlternatePallet = 1 -- Palletising change for Branches 2008-05-23
    update PalletiseStock
       set PalletQuantity = skui.AlternatePallet
      from PalletiseStock ps (nolock)
      join StorageUnit    su (nolock) on ps.StorageUnitId = su.StorageUnitId
      join SKU           sku (nolock) on su.SKUId         = sku.SKUId
      join SKUInfo      skui (nolock) on sku.SKUId        = skui.SKUId
     where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and ps.IssueId            = isnull(@IssueId, -1)
       and skui.WarehouseId      = isnull(@ToWarehouseId, @WarehouseId)
  else if @SubstitutePallet = 1 -- Palletising change for Exports 2008-06-10
    update PalletiseStock
       set PalletQuantity = skui.SubstitutePallet
      from PalletiseStock ps (nolock)
      join StorageUnit    su (nolock) on ps.StorageUnitId = su.StorageUnitId
      join SKU           sku (nolock) on su.SKUId         = sku.SKUId
      join SKUInfo      skui (nolock) on sku.SKUId        = skui.SKUId
     where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and ps.IssueId            = isnull(@IssueId, -1)
       and skui.WarehouseId      = isnull(@ToWarehouseId, @WarehouseId)
  
  -- Set the Theoretical Weight and Volume
  update PalletiseStock
     set UnitWeight = 100.000 / convert(numeric(13,6), PalletQuantity),
         UnitVolume = 100.000 / convert(numeric(13,6), PalletQuantity)
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
  
  update PalletiseStock
     set UnitWeight = 1
   where UnitWeight <= 0
  
  update PalletiseStock
     set UnitVolume = 1
   where UnitVolume <= 0
  
  if (select dbo.ufn_Configuration(250, @WarehouseId)) = 1
  begin
    update PalletiseStock
       set BoxQuantity = floor(Quantity / BoxQuantity) * BoxQuantity
     where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
    
    update PalletiseStock
       set UnitQuantity = Quantity - isnull(BoxQuantity,0)
     where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
    
    insert PalletiseStock
          (OutboundShipmentId,
           IssueId,
           IssueLineId,
           DropSequence,
           AreaSequence,
           JobId,
           StorageUnitBatchId,
           StorageUnitId,
           ProductCode,
           SKUCode,
           Quantity,
           OriginalQuantity,
           PalletQuantity,
           UnitWeight,
           UnitVolume,
           ExternalCompanyId,
           LineNumber)
    select OutboundShipmentId,
           IssueId,
           IssueLineId,
           DropSequence,
           AreaSequence,
           JobId,
           StorageUnitBatchId,
           StorageUnitId,
           ProductCode,
           SKUCode,
           BoxQuantity,
           BoxQuantity,
           PalletQuantity,
           UnitWeight,
           UnitVolume,
           ExternalCompanyId,
           LineNumber
      from PalletiseStock
     where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
       and BoxQuantity       > 0
    
    insert PalletiseStock
          (OutboundShipmentId,
           IssueId,
           IssueLineId,
           DropSequence,
           AreaSequence,
           JobId,
           StorageUnitBatchId,
           StorageUnitId,
           ProductCode,
           SKUCode,
           Quantity,
           OriginalQuantity,
           PalletQuantity,
           UnitWeight,
           UnitVolume,
           ExternalCompanyId,
           LineNumber)
    select OutboundShipmentId,
           IssueId,
           IssueLineId,
           DropSequence,
           AreaSequence + 100,
           JobId,
           StorageUnitBatchId,
           StorageUnitId,
           ProductCode,
           SKUCode,
           UnitQuantity,
           UnitQuantity,
           PalletQuantity,
           UnitWeight,
           UnitVolume,
           ExternalCompanyId,
           LineNumber
      from PalletiseStock
     where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
       and UnitQuantity       > 0
    
    delete PalletiseStock
     where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
       and (BoxQuantity is not null
         or UnitQuantity is not null)
  end
  
  select @MaxAreaSequence = MAX(AreaSequence)
  From PalletiseStock
  Where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
    and IssueId            = isnull(@IssueId, -1)
  
  /**************************************
   * Kitting change                     *
   * Ensure Kits are on their own jobs  *
   **************************************/
  if (select dbo.ufn_Configuration(445, @WarehouseId)) = 1 -- p_Outbound_Palletise - Ensure Kits are on their own jobs
  begin
    Update Ps
       Set AreaSequence = BOM.AreaSequence + @MaxAreaSequence
      From PalletiseStock ps
      Inner Join
      (select ps.StorageUnitId,
             ps.IssueId,
             max(ps.AreaSequence) over (Partition by b.BOMInstructionId) As AreaSequence
        from PalletiseStock ps
        join @BOM b on ps.StorageUnitId = b.StorageUnitId) As BOM On ps.StorageUnitId = BOM.StorageUnitId
                                      And ps.IssueId = BOM.IssueId
    Where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
      and ps.IssueId            = isnull(@IssueId, -1)
  end
  
  --Update Ps
  --Set AreaSequence = BOM.AreaSequence + @MaxAreaSequence
  --From PalletiseStock ps
  --Inner Join
  --  (select ps.StorageUnitId,
  --         ps.IssueId,
  --         max(ps.AreaSequence) over (Partition by bi.BOMInstructionId) As AreaSequence
  --    from PalletiseStock ps
  --    join Issue                i (nolock) on ps.IssueId           = i.IssueId
  --    join BOMInstruction      bi (nolock) on i.OutboundDocumentId = bi.OutboundDocumentId
  --    join BOMInstructionLine bil (nolock) on bi.BOMInstructionId  = bil.BOMInstructionId
  --    join BOMProduct          bp (nolock) on bil.BOMLineId        = bp.BOMLineId
  --                                        and ps.StorageUnitId     = bp.StorageUnitId
  --   where bp.LineNumber = 1) As BOM On ps.StorageUnitId = BOM.StorageUnitId
  --                                  And ps.IssueId = BOM.IssueId
  --Where ps.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
  --  and ps.IssueId            = isnull(@IssueId, -1)
  --  --and ps.AreaSequence      != 999 -- no-stock
  
  --select distinct b.StorageUnitId, ps.*
  --  from PalletiseStock ps
  --  left 
  --  join @BOM b on ps.StorageUnitId = b.StorageUnitId
  --order by b.StorageUnitId,
  --         ps.AreaSequence
  
  if @@trancount > 0
    commit transaction
  
  -- GS moved to top to get correct reserved quantities
  --exec @Error = p_Palletise_Reverse
  -- @OutboundShipmentId = @OutboundShipmentId,
  -- @IssueId            = @IssueId
  
  --if @Error <> 0
  --begin
  --  set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Palletise_Reverse, p_Outbound_Palletise_Check'
  --  goto error 
  --end
  
  begin transaction
  
  if exists(select 1
              from PalletiseStock
             where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
               and IssueId            = isnull(@IssueId, -1)
               and (isnull(UnitWeight,0) = 0
                or  isnull(PalletQuantity,0) = 0)
               and Quantity > 0)
  begin
    select *
      from PalletiseStock
     where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
       and (isnull(UnitWeight,0) = 0
        or  isnull(PalletQuantity,0) = 0)
    
    set @ErrorMsg = 'p_Outbound_Palletise - Error on PalletiseStock table'
    goto error
  end
  
  if (select dbo.ufn_Configuration(452, @WarehouseId)) = 1
  begin
    update ps
       set PalletQuantity = case when floor(Quantity / PalletQuantity) < 1
                                 then BoxQuantity
                                 else floor(Quantity / PalletQuantity) * PalletQuantity
                                 end
      from PalletiseStock ps
     where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
  end
  
  if (select dbo.ufn_Configuration(155, @WarehouseId)) = 1
  begin
--    print 'p_Outbound_Palletise_SOH'
    exec @Error = p_Outbound_Palletise_SOH
     @WarehouseId        = @WarehouseId,
     @ToWarehouseId      = @ToWarehouseId,
     @StoreLocationId    = @StoreLocationId,
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId,
     @OperatorId         = @OperatorId,
     @SubstitutePallet   = @SubstitutePallet,
     @JobId              = @JobId output
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Palletise_Full'
      goto error 
    end
    
    if not exists(select top 1 1
                    from PalletiseStock
                   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
                     and IssueId            = isnull(@IssueId, -1)
                     and (isnull(Quantity,0) > 0
                      or  isnull(OriginalQuantity,0) > 0))
      goto CreateReference
  end
  
  if @OrderLineSequence = 0
  begin
    /**********************************/
    /* Execute the full palletisation */
    /**********************************/
    if (select dbo.ufn_Configuration(48, @WarehouseId)) = 1 and @FP = 1
    begin
      exec @Error = p_Outbound_Palletise_Full
       @WarehouseId        = @WarehouseId,
       @ToWarehouseId      = @ToWarehouseId,
       @StoreLocationId    = @StoreLocationId,
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId,
       @OperatorId         = @OperatorId,
       @SubstitutePallet   = @SubstitutePallet,
       @JobId              = @JobId output
      
      if @Error <> 0
      begin
        set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Palletise_Full'
        goto error 
      end
    end
    
    /************************************/
    /* Execute the pseudo palletisation */
    /************************************/
    if (select dbo.ufn_Configuration(49, @WarehouseId)) = 1 and @SS = 1
    begin
      exec @Error = p_Outbound_Palletise_Pseudo
       @WarehouseId        = @WarehouseId,
       @ToWarehouseId      = @ToWarehouseId,
       @StoreLocationId    = @StoreLocationId,
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId,
       @OperatorId         = @OperatorId,
       @AlternatePallet    = @AlternatePallet,
       @SubstitutePallet   = @SubstitutePallet,
       @JobId              = @JobId output
      
      if @Error <> 0
      begin
        set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Palletise_Pseudo'
        goto error 
      end
    end
    
    /************************************/
    /* Execute the Layers palletisation */
    /************************************/
    --if (select dbo.ufn_Configuration(50, @WarehouseId)) = 1 -- Layered Pallet
    --begin
    --  print 'p_Outbound_Palletise_Layers'
    --end
    
    /****************************************************/
    /* Execute the pseudo palletisation for second time */
    /****************************************************/
    if (select dbo.ufn_Configuration(49, @WarehouseId)) = 1 and @SS = 1
    begin
      exec @Error = p_Outbound_Palletise_Pseudo
       @WarehouseId        = @WarehouseId,
       @ToWarehouseId      = @ToWarehouseId,
       @StoreLocationId    = @StoreLocationId,
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId,
       @OperatorId         = @OperatorId,
       @AlternatePallet    = @AlternatePallet,
       @SubstitutePallet   = @SubstitutePallet,
       @JobId              = @JobId output
      
      if @Error <> 0
      begin
        set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Palletise_Pseudo'
        goto error 
      end
    end
    
    /****************************************/
    /* Execute the full mixed palletisation */
    /****************************************/
    if (select dbo.ufn_Configuration(51, @WarehouseId)) = 1 and @FM = 1
    begin
      exec @Error = p_Outbound_Palletise_Full_Mix
       @WarehouseId        = @WarehouseId,
       @StoreLocationId    = @StoreLocationId,
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId,
       @OperatorId         = @OperatorId,
       @Weight             = @Weight,
       @Volume             = @Volume,
       @JobId              = @JobId output
      
      if @Error <> 0
      begin
        set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Palletise_Full_Mix'
        goto error 
      end
    end
    
    /************************************************/
    /* Execute the mixed palletisation If requested */
    /************************************************/
    --if @Mixed = 1
    --begin
    if (select dbo.ufn_Configuration(52, @WarehouseId)) = 1 and @MP = 1
    begin
    print 'jobId'
    print @JobId
      exec @Error = p_Outbound_Palletise_Mix
       @WarehouseId        = @WarehouseId,
       @StoreLocationId    = @StoreLocationId,
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId,
       @OperatorId         = @OperatorId,
       @Weight             = @Weight,
       @Volume             = @Volume,
       @OneProductPerPallet = @OneProductPerPallet,
       @JobId              = @JobId output
      
      if @Error <> 0
      begin
        set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Palletise_Mix'
        goto error 
      end
    end
    --end
  end
  else
  begin -- Palletise by Order Line Sequence
    exec @Error = p_Outbound_Palletise_OrderLineSequence
     @WarehouseId        = @WarehouseId,
     @StoreLocationId    = @StoreLocationId,
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId,
     @OperatorId         = @OperatorId,
     @Weight             = @Weight,
     @Volume             = @Volume
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Palletise_OrderLineSequence'
      goto error 
    end
  end
  
  /**********************************/
  /* Execute the Part palletisation */
  /**********************************/
  --if (select dbo.ufn_Configuration(53, @WarehouseId)) = 1 and @PP = 1
  --begin
  --  print 'p_Outbound_Palletise_Part'
  --end
  CreateReference:
  /******************************************************/
  /* Create Reference between Instruction and IssueLine */
  /******************************************************/
  exec @Error = p_Outbound_Palletise_Create_Reference
   @OutboundShipmentId  = @OutboundShipmentId,
   @IssueId             = @IssueId
   --@OneProductPerPallet = @OneProductPerPallet
  
  if @Error <> 0
  begin
    set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Palletise_Create_Reference'
    goto error 
  end
  
  -- GS - 2013-11-04 - this is wrong - doesn't even have proper where clause and looks across the whole database
  --/*********************************************/
  --/* Check for Multiple companies on single job */
  --/*********************************************/
  --if Exists(Select JobId, COUNT(distinct o.ExternalCompanyId) 
  --              from OutboundShipmentIssue  osi
    --               JOin Issue         i (nolock) on i.IssueId = osi.IssueId
    --               Join OutboundDocument    o (nolock) on o.OutboundDocumentId = i.OutboundDocumentId
    --               Join IssueLineInstruction ili (nolock) on ili.IssueId = i.IssueId
    --               Join Instruction     ins (nolock) on ins.InstructionId = ili.InstructionId
    --               Join StorageUnitBatch    sub (nolock) on sub.StorageUnitBatchId = ins.StorageUnitBatchId
    --               Join StorageUnit      su (nolock) on su.StorageUnitId = sub.StorageUnitId
    --               Join Product       p (nolock) on p.ProductId = su.ProductId
    --            where Producttype not in ('LCP','Perla', 'NON PG', 'NON P&G','BULK') and osi.OutboundShipmentId = @OutboundShipmentId
    --           Group by JobId
    --           having   COUNT(distinct ExternalCompanyId) > 1)
   --Begin      
    --  set @ErrorMsg = 'p_Outbound_Palletise - Error Multiple ExternalCompany per Job'
        
    --  exec @Error = p_Palletise_Reverse
    --   @OutboundShipmentId = @OutboundShipmentId,
    --   @IssueId            = @IssueId
        
    --  if @Error <> 0
    --  begin
    --    set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Palletise_Reverse, Multiple Companies per job'
    --    goto error 
    --  end
    --  goto error
   --End
   
   if (select dbo.ufn_Configuration(221, @WarehouseId)) = 1 or @SelectiveBulkPickByOrder = 1 or @SelectiveBulkPickByCustomer = 1
   begin
     if exists(select top 1 1
               from IssueLineInstruction ili (nolock)
                 join Instruction            i (nolock) on ili.InstructionId = i.InstructionId -- Don't need InstructionRefId as no jobs have been picked yet
                 join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
                 join StorageUnit           su (nolock) on sub.StorageUnitId = su.StorageUnitId
                 join Product                p (nolock) on su.ProductId = p.ProductId
                                                       and p.ProductType not in ('LCP','Perla','BULK','NON PG', 'NON P&G')
                where isnull(ili.OutboundShipmentId, -1) = isnull(@OutboundShipmentId, -1)
                 and ili.IssueId                        = isnull(@IssueId, ili.IssueId)
              group by i.JobId
              having COUNT(distinct(ili.IssueId)) > 1)
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error Multiple Orders per Job'
       
       exec @Error = p_Palletise_Reverse
         @OutboundShipmentId = @OutboundShipmentId,
         @IssueId            = @IssueId
          
        if @Error <> 0
        begin
          set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Palletise_Reverse, Multiple Orders per job'
          goto error 
        end
        goto error
     end
   end
  /***********************************/
  /* Ensure the Palletise is correct */
  /***********************************/

  exec @Error = p_Outbound_Palletise_Check
   @OutboundShipmentId = @OutboundShipmentId,
   @IssueId            = @IssueId
  
  if @Error <> 0
  begin
    set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Palletise_Check'
    
    exec @Error = p_Palletise_Reverse
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Palletise_Reverse, p_Outbound_Palletise_Check'
      goto error 
    end
    
    goto error
  end
  
  if @outboundShipmentId is not null
    select @PriorityId = min(PriorityId)
      from OutboundShipmentIssue osi (nolock)
      join Issue                   i (nolock) on osi.IssueId = i.IssueId
     where OutboundShipmentId = @outboundShipmentId
  
  if @IssueId is not null
    select @PriorityId = PriorityId
      from Issue (nolock)
     where IssueId = @IssueId
  
  if @PriorityId is not null
  begin
    exec @Error = p_WIP_Order_Update
     @outboundShipmentId = @outboundShipmentId,
     @issueId            = @IssueId,
     @priorityId         = @PriorityId
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_WIP_Order_Update'
      goto error 
    end
  end
  
  /************************************/
  /* Change the status to palletised */
  /************************************/
  if (select dbo.ufn_Configuration(155, @WarehouseId)) = 0
  begin
    exec p_Outbound_Release
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId,
     @StatusCode         = 'P'
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Release'
      goto error 
    end
    
    if (select dbo.ufn_Configuration(506, @WarehouseId)) = 1
    begin
    exec @Error = p_Despatch_Manual_Palletisation_Auto_Locations
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId
           
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Despatch_Manual_Palletisation_Auto_Locations'
      goto error
    end
    end
  end
  else
  begin
    exec @Error = p_Despatch_Manual_Palletisation_Auto_Locations
     @OutboundShipmentId = @OutboundShipmentId,
     @IssueId            = @IssueId
           
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Despatch_Manual_Palletisation_Auto_Locations'
      goto error
    end
    
--    exec p_Outbound_Release
--     @OutboundShipmentId = @OutboundShipmentId,
--     @StatusCode         = 'P'
--    
--    if @Error <> 0
--    begin
--      set @ErrorMsg = 'p_Outbound_Palletise - Error executing p_Outbound_Release'
--      goto error 
--    end
  end
  
  if @OutboundShipmentId is not null
  begin
update i
       set DropSequence = null
      from Instruction            i (nolock)
      join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
     where ili.OutboundShipmentId = @OutboundShipmentId
    
    select @Error = @@Error
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error updating DropSequence'
      goto error 
    end
    
    update i
       set Total = (select count(1)
                           from IssueLineInstruction ili (nolock)
                           join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
                          where i.IssueId   = ili.IssueId)
      from OutboundShipmentIssue osi (nolock)
      join Issue                   i (nolock) on osi.IssueId = i.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
    
    select @Error = @@Error
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error updating Total'
      goto error 
    end
      
    update i
       set Pallets = (select count(distinct (JobId))
                           from IssueLineInstruction ili (nolock)
                           join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
                          where i.IssueId   = ili.IssueId)
      from OutboundShipmentIssue osi (nolock)
      join Issue                   i (nolock) on osi.IssueId = i.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
    
    select @Error = @@Error
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error updating Pallets'
      goto error 
    end
  end
  else if @IssueId is not null
  begin
    update i
       set DropSequence = null
      from Instruction            i (nolock)
      join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
     where ili.IssueId               = @IssueId
    
    select @Error = @@Error
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error updating DropSequence'
      goto error 
    end
    
    update i
       set Total = (select count(1)
                           from IssueLineInstruction ili (nolock)
                           join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
                          where i.IssueId = ili.IssueId)
      from Issue i
     where i.IssueId = @IssueId
    
    select @Error = @@Error
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error updating Total'
      goto error 
    end
      
    update i
       set Pallets = (select count(distinct (JobId))
                           from IssueLineInstruction ili (nolock)
                           join Instruction          ins (nolock) on ili.InstructionId = ins.InstructionId
                          where i.IssueId   = ili.IssueId)
      from Issue i
     where i.IssueId = @IssueId
    
    select @Error = @@Error
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error updating Pallets'
      goto error 
    end
  end
  
  if @OrderLineSequence = 1
  begin
    update i
       set DropSequence = ol.LineNumber
      from Instruction            i (nolock)
      join IssueLineInstruction ili (nolock) on i.InstructionId   = ili.InstructionId
      join IssueLine             il (nolock) on ili.IssueLineId   = il.IssueLineId
      join OutboundLine          ol (nolock) on il.OutboundLineId = ol.OutboundLineId
     where ili.OutboundShipmentId = isnull(@OutboundShipmentId, ili.OutboundShipmentId)
       and ili.IssueId            = isnull(@IssueId, ili.IssueId)
    
    select @Error = @@Error
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'p_Outbound_Palletise - Error deleting PalletiseStock'
      goto error 
    end
  end
  
  delete PalletiseStock
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
  
  select @Error = @@Error
  
  if @Error <> 0
  begin
    set @ErrorMsg = 'p_Outbound_Palletise - Error deleting PalletiseStock'
    goto error 
  end
  
/*update i
     set LoadIndicator = 1
    from Issue                   i
    join OutboundShipmentIssue osi on i.IssueId = osi.IssueId
   where osi.OutboundShipmentId = isnull(@OutboundShipmentId, -1)
  
  if @@Error <> 0
  begin
    set @ErrorMsg = 'p_Outbound_Palletise - Error updating Issue - LoadIndicator'
    goto error 
  end*/
  
  update Issue
     set LoadIndicator = 0
   where IssueId = @IssueId
  
  if @@Error <> 0
  begin
    set @ErrorMsg = 'p_Outbound_Palletise - Error updating Issue - LoadIndicator 2'
    goto error 
  end
  
  declare @TableJobs as table
  (
   Id           int identity,
   JobId        int,
   DropSequence int
  )
  
  insert @TableJobs
        (JobId,
         DropSequence)
  select distinct j.JobId,
         j.DropSequence
    from Job                     j
    join Instruction            i (nolock) on j.JobId = i.JobId
    join IssueLineInstruction ili (nolock) on i.InstructionId   = ili.InstructionId
   where isnull(ili.OutboundShipmentId,-1) = isnull(@OutboundShipmentId, isnull(ili.OutboundShipmentId,-1))
     and ili.IssueId            = isnull(@IssueId, ili.IssueId)
  order by j.Dropsequence,
           j.JobId
  
  select @Error = @@error, @rowcount = @@rowcount
  
  if @@Error <> 0
  begin
    set @ErrorMsg = 'p_Outbound_Palletise - Error inserting into @TableJobs'
    goto error 
  end
  
  update j
     set DropSequence = t.Id,
         Pallets      = @rowcount
    from @TableJobs t
    join Job        j on t.JobId = j.JobId
  
  select @Error = @@error
  
  if @@Error <> 0
  begin
    set @ErrorMsg = 'p_Outbound_Palletise - Error updating Job - DropSequence'
    goto error 
  end
  
  if @@trancount > 0
    commit transaction
  return
  
  error:
    if @@trancount = 1
      rollback transaction
    else
      commit transaction
    raiserror 900000 @ErrorMsg
    return @Error
END

