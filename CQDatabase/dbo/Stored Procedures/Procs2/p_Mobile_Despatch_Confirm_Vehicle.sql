﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Despatch_Confirm_Vehicle
  ///   Filename       : p_Mobile_Despatch_Confirm_Vehicle.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Apr 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Despatch_Confirm_Vehicle
(
 @DocumentNumber nvarchar(50) = null,
 @Vehicle        nvarchar(50) = null,
 @Barcode        nvarchar(50) = null,
 @Pallets        int = null output,
 @Total          int = null output
)

as
begin
	 set nocount on;
  
  declare @Error              int = 0,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @OutboundShipmentId int,
          @OutboundDocumentId int,
          @JobId              int,
          @StatusCode         nvarchar(10),
          @PalletId           int,
          @ReferenceNumber    nvarchar(50),
          @IssueId            int,
          @ShipmentId         int,
          @DocumentId         int,
          @InstructionId      int,
          @Count              int,
          @Stored             int,
          @Picked             int,
          @StorageUnitBatchId int
          
  declare @InstructionTable table
  (
	  InstructionId int
  )        
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Pallets = 0;
  set @Total   = 0;
  
  if @Barcode = '-1'
    set @Barcode = null
  
  begin transaction
  
  if @DocumentNumber is not null
  begin
    if @DocumentNumber is null and @Vehicle is null
    begin
      set @Error = 900001
      set @Errormsg = 'Document and Vehicle cannot both be null'
      goto error
    end
    
    if isnumeric(@DocumentNumber) = 1
    begin
      select @ShipmentId = OutboundShipmentId
        from OutboundShipment (nolock)
       where OutboundShipmentId = convert(int, @DocumentNumber)
    end
    
    if @ShipmentId is null
    begin
      select @DocumentId = OutboundDocumentId
        from OutboundDocument (nolock)
       where OrderNumber = @DocumentNumber
    end
    
    if @ShipmentId is null and @DocumentId is null
    begin
      set @Error = 900002
      set @Errormsg = 'Document not valid'
      goto error
    end
  end
  
  if @barcode is not null
  begin
    if isnumeric(replace(@barcode,'J:','')) = 1
    begin
      select @JobId           = replace(@barcode,'J:',''),
             @barcode = null
      
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
             @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
       where j.JobId = @JobId
       
    end
    else if isnumeric(replace(@barcode,'P:','')) = 1
    begin
      select @PalletId        = replace(@barcode,'P:',''),
             @barcode = null
      
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
             @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
       where i.PalletId = @PalletId
         and s.StatusCode = 'DC'
    end
    else if isnumeric(replace(@barcode,'R:','')) = 1
      select @ReferenceNumber = @barcode,
             @barcode         = null
    begin
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
             @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
       where j.ReferenceNumber = @ReferenceNumber
    end
    
    if @JobId is null
    begin
      set @Error = 900006
      set @ErrorMsg = 'Could not find the pallet'
      goto error
    end
    
    if @StatusCode = 'C'
    begin
      set @Error = 900005
      set @ErrorMsg = 'The pallet is already complete'
      goto error
    end
    
    if @StatusCode in ('CD','D','DC')
    begin
      update Job
         set StatusId = dbo.ufn_StatusId('IS','C')
       where JobId = @JobId
    end
    else
    begin
      set @Error = 900003
      set @ErrorMsg = 'Incorrect Status'
      goto error
    end
    
    if @ShipmentId <> @OutboundShipmentId
    begin
      set @Error = 900004
      set @ErrorMsg = 'Could not tie up shipment and pallet'
      goto error
    end
  end
  else
  begin
   set @OutboundShipmentId = @ShipmentId
   set @OutboundDocumentId = @DocumentId
  end
------  else if @Vehicle is not null
------  begin
------    select @OutboundShipmentId = OutboundShipmentId
------      from OutboundShipment
------     where VehicleRegistration = @Vehicle
------  end
  
  if @OutboundShipmentId is not null
  begin
    select @Pallets = count(distinct(ins.JobId))
      from Instruction           ins (nolock)
      join Job                     j (nolock) on ins.JobId = j.JobId
      join Status                  s (nolock) on j.StatusId = s.StatusId
     where ins.OutboundShipmentId = @OutboundShipmentId
       and s.StatusCode != 'C'
    
    select @Total = count(distinct(ins.JobId))
      from Instruction           ins (nolock)
      join Job                     j (nolock) on ins.JobId = j.JobId
      join Status                  s (nolock) on j.StatusId = s.StatusId
     where ins.OutboundShipmentId = @OutboundShipmentId
    
    exec @Error = p_OutboundShipment_Update
     @OutboundShipmentId = @OutboundShipmentId,
     @VehicleRegistration = @Vehicle
    
    if @Error <> 0
    begin
      set @Error = 900007
      set @ErrorMsg = 'Error updating OutboundShipment'
      goto error
    end
  end
  else if @DocumentId is not null
  begin
    if @DocumentId <> @OutboundDocumentId
    begin
      set @Error = 900004
      set @ErrorMsg = 'Could not tie up document and pallet'
      goto error
    end
    
    select @Pallets = count(distinct(ins.JobId))
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId = ins.InstructionId
      join Job                     j (nolock) on ins.JobId = j.JobId
      join Status                  s (nolock) on j.StatusId = s.StatusId
     where ili.IssueId = @IssueId
       and s.StatusCode != 'C'
    
    select @Total = count(distinct(ins.JobId))
      from IssueLineInstruction  ili (nolock)
      join Instruction           ins (nolock) on ili.InstructionId = ins.InstructionId
     where ili.IssueId = @IssueId
    
    select @Vehicle = left(@Vehicle,10)
    
    exec @Error = p_Issue_Update
     @IssueId             = @IssueId,
     @VehicleRegistration = @Vehicle
    
    if @Error <> 0
    begin
      set @Error = 900007
      set @ErrorMsg = 'Error updating Issue'
      goto error
    end
  end
  
  insert @InstructionTable 
		      (InstructionId) 
  select InstructionId 
    from Instruction (nolock)
   where JobId = @JobId
 
  while exists (select top 1 1 from @InstructionTable)
	 begin
    select top 1 @InstructionId = InstructionId
      from @InstructionTable
    order by InstructionId asc
    
    delete @InstructionTable
    where InstructionId = @InstructionId
    
    select @Stored = Stored,
           @Picked = Picked,
           @StorageUnitBatchId = StorageUnitBatchId
      from Instruction
     where Instructionid = @InstructionId
    
    update Instruction
       set Stored = 1
     where InstructionId = @instructionId
    
    exec	@Error = p_StorageUnitBatchLocation_Reverse
	    @instructionId = @instructionId,
	    @Pick  = 0,
	    @Store = 1,
	    @Confirmed = 1
    
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @instructionId = @instructionId,
     @Pick  = 0,
     @Store = 1,
     @Confirmed = 1
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Deallocate'
      goto error
    end
	 end
	
	 commit transaction
  select @Error
  return @Error
  
  error:
    --raiserror @Error @Errormsg
    rollback transaction
    select @Error
    return @Error
end
