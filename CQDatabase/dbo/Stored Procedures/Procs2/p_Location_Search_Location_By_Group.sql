﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Location_Search_Location_By_Group
  ///   Filename       : p_Location_Search_Location_By_Group.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Search_Location_By_Group
(
 @WarehouseId int,
 @Location   varchar(15),
 @StockTakeGroupId int
)

as
begin
	 set nocount on;
  
  select l.LocationId,
         l.Location
    from Location      l (nolock)
    join AreaLocation al (nolock) on l.LocationId = al.LocationId
    join Area          a (nolock) on al.AreaId    = a.AreaId
   where a.WarehouseId = @WarehouseId
     and l.Location like '%' + isnull(@Location,'') + '%'
     and not exists
     (select 1 from StockTakeGroupLocation stgl
     --join Location lo (nolock) on @Location = lo.Location 
     where stgl.StockTakeGroupId = @StockTakeGroupId 
     and stgl.LocationId = l.LocationId)
end
