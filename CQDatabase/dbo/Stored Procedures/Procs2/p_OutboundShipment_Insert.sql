﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipment_Insert
  ///   Filename       : p_OutboundShipment_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Oct 2013 09:31:08
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundShipment table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null output,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @LocationId int = null,
  ///   @ShipmentDate datetime = null,
  ///   @Remarks nvarchar(500) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @VehicleRegistration nvarchar(20) = null,
  ///   @Route nvarchar(80) = null,
  ///   @RouteId int = null,
  ///   @Weight float = null,
  ///   @Volume decimal(13,3) = null,
  ///   @FP bit = null,
  ///   @SS bit = null,
  ///   @LP bit = null,
  ///   @FM bit = null,
  ///   @MP bit = null,
  ///   @PP bit = null,
  ///   @AlternatePallet bit = null,
  ///   @SubstitutePallet bit = null,
  ///   @DespatchBay int = null,
  ///   @NumberOfOrders int = null,
  ///   @ContactListId int = null,
  ///   @TotalOrders int = null,
  ///   @IDN bit = null,
  ///   @WaveId int = null,
  ///   @ReferenceNumber nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   OutboundShipment.OutboundShipmentId,
  ///   OutboundShipment.StatusId,
  ///   OutboundShipment.WarehouseId,
  ///   OutboundShipment.LocationId,
  ///   OutboundShipment.ShipmentDate,
  ///   OutboundShipment.Remarks,
  ///   OutboundShipment.SealNumber,
  ///   OutboundShipment.VehicleRegistration,
  ///   OutboundShipment.Route,
  ///   OutboundShipment.RouteId,
  ///   OutboundShipment.Weight,
  ///   OutboundShipment.Volume,
  ///   OutboundShipment.FP,
  ///   OutboundShipment.SS,
  ///   OutboundShipment.LP,
  ///   OutboundShipment.FM,
  ///   OutboundShipment.MP,
  ///   OutboundShipment.PP,
  ///   OutboundShipment.AlternatePallet,
  ///   OutboundShipment.SubstitutePallet,
  ///   OutboundShipment.DespatchBay,
  ///   OutboundShipment.NumberOfOrders,
  ///   OutboundShipment.ContactListId,
  ///   OutboundShipment.TotalOrders,
  ///   OutboundShipment.IDN,
  ///   OutboundShipment.WaveId,
  ///   OutboundShipment.ReferenceNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipment_Insert
(
 @OutboundShipmentId int = null output,
 @StatusId int = null,
 @WarehouseId int = null,
 @LocationId int = null,
 @ShipmentDate datetime = null,
 @Remarks nvarchar(500) = null,
 @SealNumber nvarchar(60) = null,
 @VehicleRegistration nvarchar(20) = null,
 @Route nvarchar(80) = null,
 @RouteId int = null,
 @Weight float = null,
 @Volume decimal(13,3) = null,
 @FP bit = null,
 @SS bit = null,
 @LP bit = null,
 @FM bit = null,
 @MP bit = null,
 @PP bit = null,
 @AlternatePallet bit = null,
 @SubstitutePallet bit = null,
 @DespatchBay int = null,
 @NumberOfOrders int = null,
 @ContactListId int = null,
 @TotalOrders int = null,
 @IDN bit = null,
 @WaveId int = null,
 @ReferenceNumber nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
  if @OutboundShipmentId = '-1'
    set @OutboundShipmentId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
  if @DespatchBay = '-1'
    set @DespatchBay = null;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @WaveId = '-1'
    set @WaveId = null;
  
	 declare @Error int
 
  insert OutboundShipment
        (StatusId,
         WarehouseId,
         LocationId,
         ShipmentDate,
         Remarks,
         SealNumber,
         VehicleRegistration,
         Route,
         RouteId,
         Weight,
         Volume,
         FP,
         SS,
         LP,
         FM,
         MP,
         PP,
         AlternatePallet,
         SubstitutePallet,
         DespatchBay,
         NumberOfOrders,
         ContactListId,
         TotalOrders,
         IDN,
         WaveId,
         ReferenceNumber)
  select @StatusId,
         @WarehouseId,
         @LocationId,
         @ShipmentDate,
         @Remarks,
         @SealNumber,
         @VehicleRegistration,
         @Route,
         @RouteId,
         @Weight,
         @Volume,
         @FP,
         @SS,
         @LP,
         @FM,
         @MP,
         @PP,
         @AlternatePallet,
         @SubstitutePallet,
         @DespatchBay,
         @NumberOfOrders,
         @ContactListId,
         @TotalOrders,
         @IDN,
         @WaveId,
         @ReferenceNumber 
  
  select @Error = @@Error, @OutboundShipmentId = scope_identity()
  
  
  return @Error
  
end
