﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundLine_Edit
  ///   Filename       : p_OutboundLine_Edit.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundLine_Edit
(
 @outboundLineId int,
 @quantity       float
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @IssueLineId     int,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('ID','N')
  
  begin transaction
  
  if @StatusId != (select StatusId
                     from OutboundLine
                    where OutboundLineId = @OutboundLineId)
  begin
    set @Errormsg = 'Line not in the correct status'
    goto error
  end
  
  exec @Error = p_OutboundLine_Update
   @OutboundLineId = @OutboundLineId,
   @Quantity       = @quantity
  
  if @Error <> 0
    goto error
  
  select @IssueLineId = IssueLineId
    from IssueLine
   where OutboundLineId = @OutboundLineId
  
  exec @Error = p_IssueLine_Update
   @IssueLineId    = @IssueLineId,
   @OutboundLineId = @OutboundLineId,
   @Quantity       = @quantity
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_OutboundLine_Edit'); 
    rollback transaction
    return @Error
end
