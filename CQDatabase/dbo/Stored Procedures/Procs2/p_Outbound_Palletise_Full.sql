﻿
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Palletise_Full
  ///   Filename       : p_Outbound_Palletise_Full.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Dec 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE PROCEDURE p_Outbound_Palletise_Full
(
 @WarehouseId        int,
 @ToWarehouseId      int,
 @StoreLocationId    int,
 @OutboundShipmentId int,
 @IssueId            int,
 @OperatorId         int,
 @SubstitutePallet   bit = 0,
 @OrderLineSequence  bit = 0,
 @JobId              int = null output
) 
AS
  declare @full_DropSequence as table
  (
   DropSequence int,
   AreaSequence int
  )
  
  declare @full_stock as table
  (
   StorageUnitBatchId int,
   PalletQuantity     float
  )
BEGIN
  
  begin transaction
  
  declare @Getdate             datetime = getdate(),
          @Error               int,
          @Errormsg            nvarchar(500),
          @DropSequence        int,
          @DropSequenceCount   int,
          @AreaSequence        int,
          @IssueLineId         int,
          @StorageUnitBatchId  int,
          @ProductCount        int,
          @PackQuantity        float,
          @Quantity            float,
          @InstructionTypeCode nvarchar(10),
          @MaxFullPallet       bit = 0,
          @PriorityId          int,
          @StatusId            int
  
  /**********************/
  /* Process each drop  */
  /**********************/
  insert @full_DropSequence
        (DropSequence,
         AreaSequence)
  SELECT DropSequence,
         AreaSequence
    FROM PalletiseStock
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
  group by DropSequence, AreaSequence
  
  SELECT @DropSequenceCount = @@RowCount
  
  select @MaxFullPallet = dbo.ufn_Configuration(452, @WarehouseId)
  
  WHILE @DropSequenceCount > 0
  BEGIN
    select top 1
           @DropSequence = DropSequence,
           @AreaSequence = AreaSequence
      from @full_DropSequence
    order by DropSequence desc, AreaSequence
    
    DELETE @full_DropSequence
     WHERE DropSequence = @DropSequence
       and AreaSequence = @AreaSequence
    
    SELECT @DropSequenceCount = @DropSequenceCount - 1
    
    /*******************************/
    /* Process each stock lot item */
    /*******************************/
    INSERT @full_stock
    SELECT StorageUnitBatchId,
           case when @OrderLineSequence = 0 and dbo.ufn_Configuration(452, @WarehouseId) = 0
                then null
                else PalletQuantity
                end
      FROM PalletiseStock
     where DropSequence       = @DropSequence
       and AreaSequence       = @AreaSequence
       and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
       and IssueId            = isnull(@IssueId, -1)
    
    SELECT @ProductCount = @@rowcount
    
    WHILE @ProductCount > 0
    BEGIN
      set @PackQuantity = null
      
      SELECT top 1
             @StorageUnitBatchId = StorageUnitBatchId,
             @PackQuantity       = PalletQuantity
        FROM @full_stock
      
      DELETE @full_stock
       WHERE  StorageUnitBatchId = @StorageUnitBatchId
      
      SELECT @ProductCount = @ProductCount - 1
      
      /************************************/
      /* Get the default qty weight & vol */
      /* for the given stock item   */
      /************************************/
    
      if isnull(@SubstitutePallet,0) = 0
        set @InstructionTypeCode = 'P'
      else
        set @InstructionTypeCode = 'FM'
      
      if @PackQuantity is null
      begin
        if isnull(@SubstitutePallet,0) = 0
        begin
          select top 1
                 @PackQuantity = p.Quantity
            from StorageUnitBatch sub (nolock)
            join Pack               p (nolock) on sub.StorageUnitId = p.StorageUnitId
            join PackType          pt (nolock) on p.PackTypeId    = pt.PackTypeId
           where sub.StorageUnitBatchId = @StorageUnitBatchId
             and p.WarehouseId = isnull(@ToWarehouseId, @WarehouseId)
             and pt.OutboundSequence = 1
          
          if @WarehouseId != isnull(@ToWarehouseId, @WarehouseId)
            if @PackQuantity != (select top 1 p.Quantity
                                   from StorageUnitBatch sub (nolock)
                                   join Pack               p (nolock) on sub.StorageUnitId = p.StorageUnitId
                                   join PackType          pt (nolock) on p.PackTypeId    = pt.PackTypeId
                                  where sub.StorageUnitBatchId = @StorageUnitBatchId
                                    and p.WarehouseId = @WarehouseId
                                    and pt.OutboundSequence = 1)
              set @InstructionTypeCode = 'FM'
        end
        else
        begin
          select @PackQuantity = skui.SubstitutePallet
            from StorageUnitBatch sub (nolock)
            join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
            join SKU              sku (nolock) on su.SKUId          = sku.SKUId
            join SKUInfo         skui (nolock) on sku.SKUId         = skui.SKUId
           where skui.WarehouseId = isnull(@ToWarehouseId, @WarehouseId)
             and sub.StorageUnitBatchId = @StorageUnitBatchId
        end
      end
      
      /**********************************/
      /* Get the actual qty of stock to */
      /* be palletised                  */
      /**********************************/
      SELECT @Quantity    = isnull(Quantity,0),
             @IssueLineId = IssueLineId
        FROM PalletiseStock
       where DropSequence       = @DropSequence
         and AreaSequence       = @AreaSequence
         and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
         and IssueId            = isnull(@IssueId, -1)
         AND StorageUnitBatchId = @StorageUnitBatchId
      
      IF @PackQuantity = 0
      BEGIN
        RAISERROR 50028 'p_full_palletise - Pack Quantity = 0!'
        rollback transaction
        RETURN
      END
      
      /******************************/
      /* Create each full pallet  */
      /******************************/
      WHILE Floor(@Quantity/@PackQuantity) > 0
      BEGIN
        if @MaxFullPallet = 1 and @JobId is null
        begin
          if @PriorityId is null
            select @PriorityId  = PriorityId
              from InstructionType (nolock)
             where InstructionTypeCode = @InstructionTypeCode
          
          select @StatusId = dbo.ufn_StatusId('IS','P')
          
          if (dbo.ufn_Configuration(477, @WarehouseId)) = 0 
          or @JobId is null
          begin
          exec @Error = p_Job_Insert
           @JobId           = @JobId output,
           @PriorityId      = @PriorityId,
           @OperatorId      = @OperatorId,
           @StatusId        = @StatusId,
           @WarehouseId     = @WarehouseId,
           @DropSequence    = @DropSequence
          
          if @error <> 0
            goto error
          end
          
          exec @Error = p_OutboundPerformance_Insert
           @JobId         = @JobId,
           @CreateDate    = @Getdate
          
          if @error <> 0
            goto error
        end
        
        exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @InstructionTypeCode = @InstructionTypeCode,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = null,
         @StoreLocationId     = @StoreLocationId,
         @Quantity            = @PackQuantity,
         @IssueLineId         = @IssueLineId,
         @JobId               = @JobId,
         @OutboundShipmentId  = @OutboundShipmentId,
         @DropSequence        = @DropSequence
        
        if @Error <> 0  
          goto error 
        
        update PalletiseStock
           set Quantity         = Quantity - @PackQuantity,
               OriginalQuantity = OriginalQuantity - @PackQuantity
         where DropSequence       = @DropSequence
           and AreaSequence       = @AreaSequence
           and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and IssueId            = isnull(@IssueId, -1)
           and StorageUnitBatchId = @StorageUnitBatchId
        
        /**********************************/
        /* Get new  qty of stock still to */
        /* be palletised                  */
        /**********************************/
        SELECT @Quantity = isnull(Quantity,0)
          FROM PalletiseStock
         where DropSequence       = @DropSequence
           and AreaSequence       = @AreaSequence
           and OutboundShipmentId = isnull(@OutboundShipmentId, -1)
           and IssueId            = isnull(@IssueId, -1)
           and StorageUnitBatchId = @StorageUnitBatchId
      END
    END
    
    DELETE @full_stock
  END
  DELETE @full_DropSequence
  
  DELETE PalletiseStock
   where OutboundShipmentId = isnull(@OutboundShipmentId, -1)
     and IssueId            = isnull(@IssueId, -1)
     and Quantity           = 0
  
  if (dbo.ufn_Configuration(477, @WarehouseId)) = 0 
    set @JobId = null;
  
  commit transaction
  return
  
  error:
    raiserror 900000 'Error executing p_pseudo_palletise2'
    rollback transaction
    return @Error
END
