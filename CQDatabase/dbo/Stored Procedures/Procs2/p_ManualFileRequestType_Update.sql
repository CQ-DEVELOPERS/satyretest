﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ManualFileRequestType_Update
  ///   Filename       : p_ManualFileRequestType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:04
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ManualFileRequestType table.
  /// </remarks>
  /// <param>
  ///   @ManualFileRequestTypeId int = null,
  ///   @Description nvarchar(100) = null,
  ///   @DocumentTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ManualFileRequestType_Update
(
 @ManualFileRequestTypeId int = null,
 @Description nvarchar(100) = null,
 @DocumentTypeCode nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
  if @ManualFileRequestTypeId = '-1'
    set @ManualFileRequestTypeId = null;
  
	 declare @Error int
 
  update ManualFileRequestType
     set Description = isnull(@Description, Description),
         DocumentTypeCode = isnull(@DocumentTypeCode, DocumentTypeCode) 
   where ManualFileRequestTypeId = @ManualFileRequestTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
