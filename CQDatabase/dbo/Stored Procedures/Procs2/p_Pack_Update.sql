﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Pack_Update
  ///   Filename       : p_Pack_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Nov 2012 14:41:24
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Pack table.
  /// </remarks>
  /// <param>
  ///   @PackId int = null,
  ///   @StorageUnitId int = null,
  ///   @PackTypeId int = null,
  ///   @Quantity int = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @Length int = null,
  ///   @Width int = null,
  ///   @Height int = null,
  ///   @Volume numeric(13,3) = null,
  ///   @Weight numeric(13,3) = null,
  ///   @WarehouseId int = null,
  ///   @NettWeight numeric(13,6) = null,
  ///   @TareWeight numeric(13,6) = null,
  ///   @ProductionQuantity float = null,
  ///   @StatusId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Pack_Update
(
 @PackId int = null,
 @StorageUnitId int = null,
 @PackTypeId int = null,
 @Quantity int = null,
 @Barcode nvarchar(100) = null,
 @Length int = null,
 @Width int = null,
 @Height int = null,
 @Volume numeric(13,3) = null,
 @Weight numeric(13,3) = null,
 @WarehouseId int = null,
 @NettWeight numeric(13,6) = null,
 @TareWeight numeric(13,6) = null,
 @ProductionQuantity float = null,
 @StatusId int = null 
)

as
begin
	 set nocount on;
  
  if @PackId = '-1'
    set @PackId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @PackTypeId = '-1'
    set @PackTypeId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
	 declare @Error int
 
  update Pack
     set StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         PackTypeId = isnull(@PackTypeId, PackTypeId),
         Quantity = isnull(@Quantity, Quantity),
         Barcode = isnull(@Barcode, Barcode),
         Length = isnull(@Length, Length),
         Width = isnull(@Width, Width),
         Height = isnull(@Height, Height),
         Volume = isnull(@Volume, Volume),
         Weight = isnull(@Weight, Weight),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         NettWeight = isnull(@NettWeight, NettWeight),
         TareWeight = isnull(@TareWeight, TareWeight),
         ProductionQuantity = isnull(@ProductionQuantity, ProductionQuantity),
         StatusId = isnull(@StatusId, StatusId) 
   where PackId = @PackId
  
  select @Error = @@Error
  
  
  return @Error
  
end
