﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_SKU_Check
  ///   Filename       : p_Mobile_SKU_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Jan 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_SKU_Check
(
 @WarehouseId       int,
 @Barcode           nvarchar(30) = null,
 @SKUCode           nvarchar(50),
 @ConfirmedQuantity float,
 @OperatorId        int = null
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @JobId              int,
          @StatusCode         nvarchar(10),
          @PalletId           int,
          @ReferenceNumber    nvarchar(30),
          @ReasonId           int,
          @Reason             nvarchar(50),
          @ReasonCode         nvarchar(10),
          @StatusId           int
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Error = 0
  
  if @Barcode = '-1'
    set @Barcode = null
  
  begin transaction
  
  if isnumeric(@barcode) = 1
    set @barcode = 'J:' + @barcode
  
  if @barcode is not null
  begin
    if isnumeric(replace(@barcode,'J:','')) = 1
    begin
    -- First Check if Inter Branch Transfer Putaway
    select @JobId = max(JobId)
      from Job (nolock)
     where ReferenceNumber = @barcode
       and WarehouseId     = @WarehouseId
    
    if @JobId is null
      select @JobId           = replace(@barcode,'J:',''),
             @barcode = null
      
      select top 1
             @JobId      = j.JobId,
             @StatusCode = s.StatusCode
        from Job    j (nolock)
        join Status s (nolock) on j.StatusId = s.StatusId
       where j.JobId = @JobId
         and j.WarehouseId     = @WarehouseId
    end
    else if isnumeric(replace(@barcode,'P:','')) = 1
    begin
      select @PalletId        = replace(@barcode,'P:',''),
             @barcode = null
      
      select top 1
             @JobId      = j.JobId,
             @StatusCode = s.StatusCode
        from Instruction            i (nolock)
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
       where i.PalletId    = @PalletId
         and s.StatusCode  = 'CK'
         and j.WarehouseId = @WarehouseId
    end
    else if isnumeric(replace(@barcode,'R:','')) = 1
      select @ReferenceNumber = @barcode,
             @barcode         = null
    begin
      select top 1
             @JobId      = j.JobId,
             @StatusCode = s.StatusCode
        from Job    j (nolock)
        join Status s (nolock) on j.StatusId = s.StatusId
       where j.ReferenceNumber = @ReferenceNumber
         and j.WarehouseId     = @WarehouseId
    end
    
    if @JobId is null
    begin
      set @Error = 900006
      set @ErrorMsg = 'Could not find the pallet'
      goto error
    end
  end
  
  if @StatusCode = 'C'
  begin
    set @Error = 900005
    set @ErrorMsg = 'The pallet is already complete'
    goto error
  end
  
  if @StatusCode = 'CD'
  begin
    set @Error = 900008
    set @ErrorMsg = 'Pallet has already been checked'
    goto error
  end
  
  if @StatusCode not in ('CK','PR')
  begin
    set @Error = 900003
    set @ErrorMsg = 'Incorrect Status'
    goto error
  end
  
  update SKUCheck
     set ConfirmedQuantity = @ConfirmedQuantity
   where JobId   = @JobId
     and SKUCode = @SKUCode
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  if exists(select top 1 1
              from SKUCheck (nolock)
             where JobId = @JobId
               and isnull(ConfirmedQuantity, Quantity)  != Quantity)
  begin
    update SKUCheck
       set ConfirmedQuantity = 0
     where JobId   = @JobId
       and ConfirmedQuantity is null
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  if not exists(select top 1 1
                  from SKUCheck (nolock)
                 where JobId = @JobId
                   and Quantity != isnull(ConfirmedQuantity, 0))
  begin
    update Instruction
       set CheckQuantity = ConfirmedQuantity
     where JobId = @jobId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Status_Rollup
     @jobId = @jobId
    
    if @Error <> 0
      goto error
  end
  else if not exists(select top 1 1
                       from SKUCheck
                      where JobId = @JobId
                        and ConfirmedQuantity is null)
  begin
    select @ReasonId   = ReasonId,
           @Reason     = Reason,
           @ReasonCode = ReasonCode
      from Reason (nolock)
     where ReasonCode = 'SKU01'
    
    exec @Error = p_Exception_Insert
     @ReasonId       = @ReasonId,
     @Exception      = @Reason,
     @ExceptionCode  = @ReasonCode,
     @CreateDate     = @GetDate,
     @OperatorId     = @OperatorId,
     @JobId          = @JobId
    
    if @Error <> 0
      goto error
    
    select @StatusId = dbo.ufn_StatusId('IS','QA')
    
    exec @Error = p_Job_Update
     @JobId          = @JobId,
     @StatusId       = @StatusId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Status_Rollup
     @JobId          = @JobId
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  select @Error
  return @Error
  
  error:
    rollback transaction
    select @Error
    return @Error
end

