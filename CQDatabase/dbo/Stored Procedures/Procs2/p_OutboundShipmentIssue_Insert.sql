﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundShipmentIssue_Insert
  ///   Filename       : p_OutboundShipmentIssue_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jun 2013 12:36:33
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundShipmentIssue table.
  /// </remarks>
  /// <param>
  ///   @OutboundShipmentId int = null output,
  ///   @IssueId int = null output,
  ///   @DropSequence int = null 
  /// </param>
  /// <returns>
  ///   OutboundShipmentIssue.OutboundShipmentId,
  ///   OutboundShipmentIssue.IssueId,
  ///   OutboundShipmentIssue.DropSequence 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundShipmentIssue_Insert
(
 @OutboundShipmentId int = null output,
 @IssueId int = null output,
 @DropSequence int = null 
)

as
begin
	 set nocount on;
  
  if @OutboundShipmentId = '-1'
    set @OutboundShipmentId = null;
  
  if @IssueId = '-1'
    set @IssueId = null;
  
	 declare @Error int
 
  insert OutboundShipmentIssue
        (OutboundShipmentId,
         IssueId,
         DropSequence)
  select @OutboundShipmentId,
         @IssueId,
         @DropSequence 
  
  select @Error = @@Error, @IssueId = scope_identity()
  
  
  return @Error
  
end
