﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorExternalCompany_Update
  ///   Filename       : p_OperatorExternalCompany_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:17
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OperatorExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @OperatorId int = null,
  ///   @ExternalCompanyId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorExternalCompany_Update
(
 @OperatorId int = null,
 @ExternalCompanyId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update OperatorExternalCompany
     set OperatorId = isnull(@OperatorId, OperatorId),
         ExternalCompanyId = isnull(@ExternalCompanyId, ExternalCompanyId) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
