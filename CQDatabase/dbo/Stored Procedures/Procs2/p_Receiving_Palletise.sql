﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Palletise
  ///   Filename       : p_Receiving_Palletise.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Palletise
(
 @ReceiptId     int = null,
 @ReceiptLineId int = null,
 @PalletId      nvarchar(30) = null,
 @Reject        bit = 0
)

as
begin
	 set nocount on;
  
  declare @TableQuantity as table
  (
   TableQuantityId    int identity,
   ReceiptLineId      int,
   AcceptedQuantity   numeric(13,6),
   StorageUnitId      int,
   StorageUnitBatchId int
  );
  
  declare @TablePack as table
  (
   StorageUnitId   int,
   Quantity        numeric(13,6),
   InboundSequence smallint
  );
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @WarehouseId        int,
          @TableQuantityId    int,
          @StorageUnitId      int,
          @StorageUnitBatchId int,
          @PickLocationId     int,
          @StoreLocationId    int,
          @PalletQuantity     numeric(13,6),
          @AcceptedQuantity   numeric(13,6),
          @RemainingQuantity  numeric(13,6),
          @PalletCount        int,
          @InboundSequence    smallint,
          @TablePackCount     int,
          @Rowcount           int,
          @UpdReceiptLineId   int,
          @NewPalletId        int,
          @JobId              int,
          @PriorityId         int,
          @StatusId           int,
          @Transaction        bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Receiving_Palletise'
  
  if @ReceiptLineId = -1
    set @ReceiptLineId = null
  
  if @ReceiptLineId is not null
    set @ReceiptId = null
  
  if @ReceiptId is null
    select @ReceiptId = ReceiptId
      from ReceiptLine (nolock)
     where ReceiptLineId = @ReceiptLineId
  
  select @WarehouseId    = WarehouseId,
         @PickLocationId = LocationId
    from Receipt (nolock)
   where ReceiptId = @ReceiptId
	 
	 if @Reject = 1
    select top 1 @StoreLocationId = l.LocationId
      from Location      l (nolock)
      join AreaLocation al (nolock) on l.LocationId = al.LocationId
      join Area          a (nolock) on al.AreaId    = a.AreaId
     where a.AreaCode in ('GS','JJ')
       and a.WarehouseId = @WarehouseId
  
  if @PalletId is not null
    if ISNUMERIC(replace(@PalletId,'P:','')) = 1
    begin
      set @NewPalletId = replace(@PalletId,'P:','')
      
      select @StorageUnitBatchId = StorageUnitBatchId
        from Pallet (nolock)
       where PalletId = @NewPalletId
      
      if (dbo.ufn_Configuration(395, @WarehouseId) != 1) -- Allow Multiple Products on 1 PalletId
      and @StorageUnitBatchId is not null -- Must be same StorageUnitId
      begin
        set @StorageUnitBatchId = null
         
        select @StorageUnitBatchId = rl.StorageUnitBatchId
          from ReceiptLine        rl (nolock)
          join StorageUnitBatch  sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
          join StorageUnitBatch sub2 (nolock) on sub.StorageUnitId = sub2.StorageUnitId
          join Pallet              p (nolock) on sub2.StorageUnitBatchId = p.StorageUnitBatchId
         where rl.ReceiptLineId = @ReceiptLineId
           and p.PalletId       = @NewPalletId
      end
      else
        select @StorageUnitBatchId = StorageUnitBatchId
          from ReceiptLine (nolock)
         where ReceiptLineId = @ReceiptLineId
      
      if @StorageUnitBatchId is null
      begin
        set @Error = -1
        goto Error
      end
      else
      begin
		
		declare @PalletStatusId int
		
		if exists 
		(select top 1 1 
			   from AreaCheck ac 
			   join StorageUnitArea sua on ac.AreaId = sua.AreaId 
			   join StorageUnitBatch sub on sua.StorageUnitId = sub.StorageUnitId
			  where sub.StorageUnitBatchId = @StorageUnitBatchId)
		begin
			select @PalletStatusId = dbo.ufn_StatusId('P','NA')
		end
		print 'p_Pallet_Update before'
        exec @Error = p_Pallet_Update
         @PalletId           = @NewPalletId,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @StatusId			 = @PalletStatusId
        
        if @error <> 0
          goto error
      end
    end
    else
    begin
      set @Error = -1
      goto Error
    end
  
  insert @TableQuantity
        (ReceiptLineId,
         AcceptedQuantity,
         StorageUnitId,
         StorageUnitBatchId)
  select rl.ReceiptLineId,
         case when @Reject = 0
              then rl.AcceptedQuantity - isnull(rl.rejectquantity,0) - isnull(rl.SampleQuantity,0)
              else rl.RejectQuantity
              end,
         su.StorageUnitId,
         rl.StorageUnitBatchId
    from ReceiptLine      rl  (nolock)
    join Status             s (nolock) on rl.StatusId = s.StatusId
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  (nolock) on sub.StorageUnitId     = su.StorageUnitId
   where rl.ReceiptId     = isnull(@ReceiptId, rl.ReceiptId)
     and rl.ReceiptLineId = isnull(@ReceiptLineId, rl.ReceiptLineId)
     and s.StatusCode in ('R','RC','P','LA')
  
  select @TableQuantityId = scope_identity(), @Rowcount = @@rowcount
  
  insert @TablePack
        (StorageUnitId,
         Quantity,
         InboundSequence)
  select tq.StorageUnitId,
         max(p.Quantity),
         min(isnull(pt.InboundSequence,1))
    from @TableQuantity tq
    join Pack           p  (nolock) on tq.StorageUnitId = p.StorageUnitId
    join PackType       pt (nolock) on p.PackTypeId     = pt.PackTypeId
   where p.WarehouseId = @WarehouseId
     and pt.InboundSequence = 1
  group by tq.StorageUnitId
  
  insert @TablePack
        (StorageUnitId,
         Quantity,
         InboundSequence)
  select distinct tq.StorageUnitId,
         sku.Quantity,
         1
    from @TableQuantity tq
    join StorageUnit su on tq.StorageUnitId = su.StorageUnitId
    join SKU         sku on su.SKUId = sku.SKUId
   where --sku.WarehouseId = @WarehouseId
     not exists(select 1 from @TablePack tp where tq.StorageUnitId = tp.StorageUnitId)
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  if @Rowcount = 0
  begin
    set @Errormsg = 'Record not in correct Status'
    set @Error = -1
    goto error
  end
  
  if @ReceiptId is null and @ReceiptLineId is null
  begin
    set @Errormsg = 'Key value null'
    set @Error = -1
    goto Error
  end
  
  if @PalletId is null and @Reject = 0
  begin
    exec @Error = p_Palletise_Reverse
     @ReceiptId          = @ReceiptId,
     @ReceiptLineId      = @ReceiptLineId
    
    if @error <> 0
      goto error
  end
  
--  exec @Error = p_Receipt_Update
--   @ReceiptId = @ReceiptId,
--   @StatusId  = @StatusId
--  
--  if @error <> 0
--    goto error
  
  if @PriorityId is null
    select @PriorityId  = PriorityId
      from InstructionType (nolock)
     where InstructionTypeCode = 'SM'
  
  select @StatusId = StatusId
    from Status (nolock)
   where StatusCode = 'W'
     and Type = 'I'
  
  while @TableQuantityId > 0
  begin
    select @AcceptedQuantity   = AcceptedQuantity,
           @StorageUnitId      = StorageUnitId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @UpdReceiptLineId   = ReceiptLineId
      from @TableQuantity
     where TableQuantityId = @TableQuantityId
    
--    exec @Error = p_ReceiptLine_Update
--     @ReceiptLineId = @UpdReceiptLineId,
--     @StatusId      = @StatusId
--    
--    if @error <> 0
--      goto error
    
    set @InboundSequence = 0
    
    select @TablePackCount = count(1) from @TablePack where StorageUnitId = @StorageUnitId
    
    while @TablePackCount > 0
    begin
      select top 1
             @PalletQuantity = Quantity,
             @InboundSequence = InboundSequence
        from @TablePack
       where StorageUnitId = @StorageUnitId
         and InboundSequence > @InboundSequence
      order by InboundSequence
      
      if dbo.ufn_Configuration(437, @warehouseId) = 1
		      set @PalletQuantity = 99999
     
      if @PalletQuantity is null
        set @PalletQuantity = 99999
      
      set @PalletCount      = floor(@AcceptedQuantity / @PalletQuantity)
      set @AcceptedQuantity = @AcceptedQuantity - (@PalletQuantity * @PalletCount)
      
      if @AcceptedQuantity > 0
      begin
        select @JobId = j.JobId
          from Instruction      i (nolock)
          join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                                          and it.InstructionTypeCode in ('PR','S','SM')
          join Job              j (nolock) on i.JobId = j.JobId
          join Status           s (nolock) on j.StatusId = s.StatusId
                                          and s.StatusCode = 'PR'
         where i.PalletId = @NewPalletId
        
        if @JobId is null
          exec @Error = p_Job_Insert
           @JobId           = @JobId output,
           @PriorityId      = @PriorityId,
           @StatusId        = @StatusId,
           @WarehouseId     = @WarehouseId
        
        if @error <> 0
          goto error
        
        exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = @PickLocationId,
         @StoreLocationId     = @StoreLocationId,
         @Quantity            = @AcceptedQuantity,
         @ReceiptLineId       = @UpdReceiptLineId,
         @PalletId            = @NewPalletId,
         @JobId               = @JobId,
         @AutoComplete        = 1
        
        if @error <> 0
          goto error
      end
      
      while @PalletCount > 0
      begin
        set @PalletCount = @PalletCount - 1
        
        exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = @PickLocationId,
         @StoreLocationId     = @StoreLocationId,
         @Quantity            = @PalletQuantity,
         @ReceiptLineId       = @UpdReceiptLineId,
         @PalletId            = @NewPalletId,
         @InstructionTypeCode = 'S',
         @AutoComplete        = 1
        
        if @error <> 0
          goto error
        
--      -- GS - added 2008-12-16
--      if @PalletCount = 0
--        set @PalletCount = 1
--      
--      if @PalletCount > 0
--      begin
--        if @PalletQuantity = 1 and @InboundSequence > 1 -- If is in Units combine otherwise put onto unit it's own pallet
--        begin
--          set @RemainingQuantity = @PalletQuantity * @PalletCount
--          
--          exec @Error = p_Palletised_Insert
--           @WarehouseId         = @WarehouseId,
--           @OperatorId          = null,
--           @StorageUnitBatchId  = @StorageUnitBatchId,
--           @PickLocationId      = @PickLocationId,
--           @StoreLocationId     = null,
--           @Quantity            = @RemainingQuantity,
--           @ReceiptLineId       = @UpdReceiptLineId,
--           @PalletId            = @NewPalletId
--          
--          if @error <> 0
--            goto error
--        end
--        else
--        begin
--          while @PalletCount > 0
--          begin
--            set @PalletCount = @PalletCount - 1
--            
--            exec @Error = p_Palletised_Insert
--             @WarehouseId         = @WarehouseId,
--             @OperatorId          = null,
--             @StorageUnitBatchId  = @StorageUnitBatchId,
--             @PickLocationId      = @PickLocationId,
--             @StoreLocationId     = null,
--             @Quantity            = @PalletQuantity,
--             @ReceiptLineId       = @UpdReceiptLineId,
--             @PalletId            = @NewPalletId
--            
--            if @error <> 0
--              goto error
--          end
--        end
      end
      
      set @TablePackCount = @TablePackCount - 1
    end
--    -- Insert Remainder
--    exec @Error = p_Palletised_Insert
--     @WarehouseId         = @WarehouseId,
--     @OperatorId          = null,
--     @InstructionTypeCode = 'S', -- Store
--     @StorageUnitBatchId  = @StorageUnitBatchId,
--     @PickLocationId      = @PickLocationId,
--     @StoreLocationId     = null,
--     @Quantity            = @AcceptedQuantity,
--     @ReceiptLineId       = @UpdReceiptLineId,
--     @PalletId            = @NewPalletId
--    
--    if @error <> 0
--      goto error
    
    set @TableQuantityId = @TableQuantityId - 1
  end
  
  --exec @Error = p_Receiving_Palletise_Sample  
	 --    @ReceiptId,  
	 --    @ReceiptLineId  
    
  --if @Error <> 0  
  --begin  
  --  set @ErrorMsg = 'Error executing p_Receiving_Palletise_Sample'  
  --  goto error  
  --end  
  
  if @PalletId is not null
  begin
  
    exec @Error = p_Inbound_Release
     @InboundShipmentId = null,
     @ReceiptId         = @ReceiptId,
     @ReceiptLineId     = @ReceiptLineId,
     @StatusCode        = 'P',
     @NewPalletId       = @NewPalletId
  
    if @Error <> 0
      goto error
  
    if @NewPalletId is not null
    begin
      exec @Error = p_Inbound_Release
       @InboundShipmentId = null,
       @ReceiptId         = @ReceiptId,
       @ReceiptLineId     = @ReceiptLineId,
       @StatusCode        = 'R',
       @NewPalletId       = @NewPalletId
    
      if @Error <> 0
        goto error
    end
  
    if @Reject = 0 -- If 1 it's already been executed
    begin
    
      exec @Error = p_Receiving_Palletise
       @ReceiptId     = @ReceiptId,
       @ReceiptLineId = @ReceiptLineId,
       @Reject        = 1
    
      if @Error <> 0
      begin
        set @ErrorMsg = 'Error executing p_Receiving_Palletise (Reject)'
        goto error
      end
    end
  end
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
