﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Delivery_Schedule
  ///   Filename       : p_Report_Delivery_Schedule.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Delivery_Schedule

as
begin
	 set nocount on;
	 
	 declare @GetDate datetime
	 
	 declare @TableResult as table
	 (
	  Bay      nvarchar(15),
	  StartDate datetime,
	  EndDate   datetime
	 )
  
  select @GetDate = '2007-10-24 10:00:00.000'--convert(datetime, convert(nvarchar(10), Getdate(), 110))
  
  insert @TableResult (Bay, StartDate, EndDate)
  select 'Bay 1', '2007-10-24 10:00:00.000', '2007-10-24 11:00:00.000' union
  select 'Bay 1', '2007-10-24 11:00:00.000', '2007-10-24 11:30:00.000' union
  select 'Bay 1', '2007-10-24 11:30:00.000', '2007-10-24 12:30:00.000' union
  select 'Bay 1', '2007-10-24 13:00:00.000', '2007-10-24 14:00:00.000' union
  select 'Bay 1', '2007-10-24 14:30:00.000', '2007-10-24 15:00:00.000' union
  select 'Bay 1', '2007-10-24 15:00:00.000', '2007-10-24 15:30:00.000' union
  select 'Bay 2', '2007-10-24 16:00:00.000', '2007-10-24 16:45:00.000' union
  select 'Bay 2', '2007-10-24 10:00:00.000', '2007-10-24 11:00:00.000' union
  select 'Bay 2', '2007-10-24 11:30:00.000', '2007-10-24 12:30:00.000' union
  select 'Bay 2', '2007-10-24 13:00:00.000', '2007-10-24 14:00:00.000' union
  select 'Bay 2', '2007-10-24 14:30:00.000', '2007-10-24 15:00:00.000' union
  select 'Bay 2', '2007-10-24 15:00:00.000', '2007-10-24 15:30:00.000'
  
  select Bay,
         StartDate,
         EndDate,
         convert(nvarchar(10), datediff(mi, @GetDate, StartDate)) + 'cm' as 'StartPos',
         convert(nvarchar(10), datediff(mi, @GetDate, EndDate)) + 'cm'   as 'EndPos',
         datediff(mi, @GetDate, EndDate) - datediff(mi, @GetDate, StartDate) as 'Diff'
    from @TableResult
end
