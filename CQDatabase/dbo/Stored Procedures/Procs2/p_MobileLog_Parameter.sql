﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MobileLog_Parameter
  ///   Filename       : p_MobileLog_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:11
  /// </summary>
  /// <remarks>
  ///   Selects rows from the MobileLog table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   MobileLog.MobileLogId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MobileLog_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as MobileLogId
        ,null as 'MobileLog'
  union
  select
         MobileLog.MobileLogId
        ,MobileLog.MobileLogId as 'MobileLog'
    from MobileLog
  
end
