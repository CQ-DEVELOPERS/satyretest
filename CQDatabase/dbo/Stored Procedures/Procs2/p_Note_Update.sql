﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Note_Update
  ///   Filename       : p_Note_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:45
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Note table.
  /// </remarks>
  /// <param>
  ///   @NoteId int = null,
  ///   @NoteCode nvarchar(510) = null,
  ///   @Note nvarchar(max) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Note_Update
(
 @NoteId int = null,
 @NoteCode nvarchar(510) = null,
 @Note nvarchar(max) = null 
)

as
begin
	 set nocount on;
  
  if @NoteId = '-1'
    set @NoteId = null;
  
  if @NoteCode = '-1'
    set @NoteCode = null;
  
  if @Note = '-1'
    set @Note = null;
  
	 declare @Error int
 
  update Note
     set NoteCode = isnull(@NoteCode, NoteCode),
         Note = isnull(@Note, Note) 
   where NoteId = @NoteId
  
  select @Error = @@Error
  
  
  return @Error
  
end
