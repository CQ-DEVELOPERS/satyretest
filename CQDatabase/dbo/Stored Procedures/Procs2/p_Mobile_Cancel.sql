﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Cancel
  ///   Filename       : p_Mobile_Cancel.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Cancel
(
 @instructionId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @JobId             int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('I','W')
  
  select @JobId = JobId
    from Instruction (nolock)
   where InstructionId = @instructionId
  
  exec @Error = p_Job_Update
   @jobId      = @jobId,
   @StatusId   = @StatusId,
   @operatorId = null
  
  if @Error <> 0
  begin
    set @Error = 1
    goto result
  end
  
  exec @Error = p_Instruction_Update
   @InstructionId = @instructionId,
   @StatusId      = @StatusId,
   @operatorId    = null,
   @StartDate     = null
  
  if @Error <> 0
  begin
    set @Error = 1
    goto result
  end
  
  result:
    select @Error
    return 0
end
