﻿
/*
  /// <summary>
  ///   Procedure Name : p_Location_Master_Import
  ///   Filename       : p_Location_Master_Import.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Apr 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Location_Master_Import
(
 @xmlstring nvarchar(max)
)

as
begin
	 set nocount on;
  
  declare @DesktopLogId int,
          @InterfaceImportProductId int,
          @LocationId int
  
  insert DesktopLog
        (ProcName,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         getdate()
  
  select @DesktopLogId = scope_identity()
  
  declare @Error                   int = 0,
          @Errormsg                varchar(max),
          @GetDate                 datetime,
          @command                 varchar(max),
          @InterfaceImportHeaderId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Errormsg = 'p_Location_Master_Import - Error executing procedure'
  
  begin transaction
  
  if @xmlstring != ''
  begin
  declare @doc xml

  set @doc = convert(xml,@xmlstring)

  DECLARE @idoc int,
          @InsertDate datetime

  Set @InsertDate = Getdate()

  EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
  
  select @InterfaceImportProductId = MAX(InterfaceImportProductId)
    from InterfaceImportProduct
  
  set @Errormsg = 'p_Location_Master_Import - Error inserting into InterfaceImportProduct'
  
		insert LocationMaster
								(Warehouse,
         WarehouseCode,
         Area,
         AreaCode,
         Location,
         Ailse,
         [Column],
         [Level],
         SecurityCode,
         RelativeValue,
         ActiveBinning,
         ActivePicking,
         LocationType,
         PalletQuantity,
         NumberOfSUB,
         NumberOfSU,
         Width,
         Length,
         Height,
         Volume,
         Weight,
         TrackLPN,
         FreightForwarding,
         ProductCode,
         SKUCode,
         MinimumQuantity,
         HandlingQuantity,
         MaximumQuantity)
	 select Warehouse,
         WarehouseCode,
         Area,
         AreaCode,
         Location,
         Ailse,
         [Column],
         [Level],
         case when isnumeric(SecurityCode) = 1
              then SecurityCode
              else null
              end,
         case when isnumeric(RelativeValue) = 1
              then RelativeValue
              else null
              end,
         case when ActiveBinning in ('1','true')
              then 1
              else 0
              end,
         case when ActivePicking in ('1','true')
              then 1
              else 0
              end,
         LocationType,
         case when isnumeric(PalletQuantity) = 1
              then PalletQuantity
              else null
              end,
         case when isnumeric(NumberOfSUB) = 1
              then NumberOfSUB
              else 1
              end,
         case when isnumeric(NumberOfSU) = 1
              then NumberOfSU
              else 1
              end,
         case when isnumeric(Width) = 1
              then Width
              else null
              end,
         case when isnumeric(Length) = 1
              then Length
              else null
              end,
         case when isnumeric(Height) = 1
              then Height
              else null
              end,
         case when isnumeric(Volume) = 1
              then Volume
              else null
              end,
         case when isnumeric(Weight) = 1
              then Weight
              else null
              end,
         case when TrackLPN in ('1','true')
              then 1
              else 0
              end,
         FreightForwarding,
         ProductCode,
         SKUCode,
         case when isnumeric(MinimumQuantity) = 1
              then MinimumQuantity
              else null
              end,
         case when isnumeric(HandlingQuantity) = 1
              then HandlingQuantity
              else null
              end,
         case when isnumeric(MaximumQuantity) = 1
              then MaximumQuantity
              else null
              end
		  from OPENXML (@idoc, '/root/Line',1)
            WITH (Warehouse nvarchar(50) 'Warehouse',
                  WarehouseCode nvarchar(50) 'WarehouseCode',
                  Area nvarchar(50) 'Area',
                  AreaCode nvarchar(50) 'AreaCode',
                  Location nvarchar(50) 'Location',
                  Ailse nvarchar(50) 'Ailse',
                  [Column] nvarchar(50) 'Column',
                  [Level] nvarchar(50) 'Level',
                  SecurityCode nvarchar(50) 'SecurityCode',
                  RelativeValue nvarchar(50) 'RelativeValue',
                  ActiveBinning nvarchar(50) 'ActiveBinning',
                  ActivePicking nvarchar(50) 'ActivePicking',
                  LocationType nvarchar(50) 'LocationType',
                  PalletQuantity nvarchar(50) 'PalletQuantity',
                  NumberOfSUB nvarchar(50) 'NumberOfSUB',
                  NumberOfSU nvarchar(50) 'NumberOfSU',
                  Width nvarchar(50) 'Width',
                  Length nvarchar(50) 'Length',
                  Height nvarchar(50) 'Height',
                  Volume nvarchar(50) 'Volume',
                  Weight nvarchar(50) 'Weight',
                  TrackLPN nvarchar(50) 'TrackLPN',
                  FreightForwarding nvarchar(50) 'FreightForwarding',
                  ProductCode nvarchar(50) 'ProductCode',
                  SKUCode nvarchar(50) 'SKUCode',
                  MinimumQuantity nvarchar(50) 'MinimumQuantity',
                  HandlingQuantity nvarchar(50) 'HandlingQuantity',
                  MaximumQuantity nvarchar(50) 'MaximumQuantity'
                 )
    where AreaCode not like '%Area%Code%'
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error inserting into LocationMaster'
    goto error
  end
  end
  
  update LocationMaster
     set ProcessedDate = @GetDate
   where RecordStatus = 'N'
     and ProcessedDate is null
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating LocationMaster'
    goto error
  end
  
  update lm
     set WarehouseId = w.WarehouseId
    from LocationMaster lm (nolock)
    join Warehouse       w (nolock) on lm.Warehouse = w.Warehouse
   where lm.ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating LocationMaster'
    goto error
  end
  
  update lm
     set WarehouseId = w.ParentWarehouseId
    from LocationMaster lm (nolock)
    join Warehouse       w (nolock) on lm.WarehouseCode = w.WarehouseCode
   where lm.ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating LocationMaster'
    goto error
  end
  
  insert Warehouse
        (CompanyId,
         Warehouse,
         WarehouseCode,
         HostId,
         ParentWarehouseId,
         AddressId,
         DesktopMaximum,
         DesktopWarning,
         MobileWarning,
         MobileMaximum)
  select distinct
         1,
         t.Warehouse,
         t.WarehouseCode,
         null,
         1,
         null,
         15,
         10,
         10,
         15
    from LocationMaster t
   where WarehouseId is null
     and isnull(Warehouse,'') != ''
     and t.ProcessedDate = @GetDate
  order by Warehouse
  
  --insert WarehouseCodeReference([WarehouseCode],[DownloadType],[WarehouseId],[InboundDocumentTypeId],[OutboundDocumentTypeId],[ToWarehouseId],[ToWarehouseCode]) select  lm.WarehouseCode,N'RCP',lm.WarehouseId,N'1',null,lm.WarehouseId,N'01' from LocationMaster lm where ProcessedDate = @GetDate and not exists(select 1 from WarehouseCodeReference w where w.DownloadType = 'RCP' and w.WarehouseId = lm.WarehouseId)
  --insert WarehouseCodeReference([WarehouseCode],[DownloadType],[WarehouseId],[InboundDocumentTypeId],[OutboundDocumentTypeId],[ToWarehouseId],[ToWarehouseCode]) select  lm.WarehouseCode,N'RET',lm.WarehouseId,N'5',null,lm.WarehouseId,N'01' from LocationMaster lm where ProcessedDate = @GetDate and not exists(select 1 from WarehouseCodeReference w where w.DownloadType = 'RET' and w.WarehouseId = lm.WarehouseId)
  --insert WarehouseCodeReference([WarehouseCode],[DownloadType],[WarehouseId],[InboundDocumentTypeId],[OutboundDocumentTypeId],[ToWarehouseId],[ToWarehouseCode]) select  lm.WarehouseCode,N'SAL',lm.WarehouseId,null,N'1',lm.WarehouseId,N'01' from LocationMaster lm where ProcessedDate = @GetDate and not exists(select 1 from WarehouseCodeReference w where w.DownloadType = 'SAL' and w.WarehouseId = lm.WarehouseId)
  --insert WarehouseCodeReference([WarehouseCode],[DownloadType],[WarehouseId],[InboundDocumentTypeId],[OutboundDocumentTypeId],[ToWarehouseId],[ToWarehouseCode]) select  lm.WarehouseCode,N'CNC',lm.WarehouseId,null,N'2',lm.WarehouseId,N'01' from LocationMaster lm where ProcessedDate = @GetDate and not exists(select 1 from WarehouseCodeReference w where w.DownloadType = 'CNC' and w.WarehouseId = lm.WarehouseId)
  --insert WarehouseCodeReference([WarehouseCode],[DownloadType],[WarehouseId],[InboundDocumentTypeId],[OutboundDocumentTypeId],[ToWarehouseId],[ToWarehouseCode]) select  lm.WarehouseCode,N'IBT',lm.WarehouseId,null,N'3',lm.WarehouseId,N'01' from LocationMaster lm where ProcessedDate = @GetDate and not exists(select 1 from WarehouseCodeReference w where w.DownloadType = 'IBT' and w.WarehouseId = lm.WarehouseId)
  --insert WarehouseCodeReference([WarehouseCode],[DownloadType],[WarehouseId],[InboundDocumentTypeId],[OutboundDocumentTypeId],[ToWarehouseId],[ToWarehouseCode]) select  lm.WarehouseCode,N'RTS',lm.WarehouseId,null,N'4',lm.WarehouseId,N'01' from LocationMaster lm where ProcessedDate = @GetDate and not exists(select 1 from WarehouseCodeReference w where w.DownloadType = 'RTS' and w.WarehouseId = lm.WarehouseId)
  
  update lm
     set WarehouseId = w.WarehouseId
    from LocationMaster lm (nolock)
    join Warehouse       w (nolock) on lm.WarehouseCode = w.WarehouseCode
   where lm.ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating LocationMaster'
    goto error
  end
  
  update lm
     set AreaId = a.AreaId
    from LocationMaster lm (nolock)
    join Warehouse       w (nolock) on lm.WarehouseId = w.WarehouseId
    join Area            a (nolock) on lm.Area = a.Area
                                   and w.WarehouseId = a.WarehouseId
   where lm.ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating LocationMaster'
    goto error
  end
  
  update a
     set WarehouseId   = t.WarehouseId,
         AreaCode      = t.AreaCode,
         WarehouseCode = t.WarehouseCode
    from LocationMaster t (nolock)
    join Area           a (nolock) on t.AreaId = a.AreaId
   where t.ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error inserting into Area'
    goto error
  end
  
  insert Area
        (WarehouseId,
         Area,
         AreaCode,
         StockOnHand,
         AreaSequence,
         WarehouseCode)
  select distinct
         WarehouseId,
         Area,
         AreaCode,
         1,
         1,
         WarehouseCode
    from LocationMaster t
   where AreaId is null
     and isnull(Area,'') != ''
     and t.ProcessedDate = @GetDate
  order by AreaCode, Area
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error inserting into Area'
    goto error
  end
  
  update lm
     set AreaId = a.AreaId
    from LocationMaster lm (nolock)
    join Warehouse       w (nolock) on lm.WarehouseId = w.WarehouseId
    join Area            a (nolock) on lm.Area = a.Area
                                   and w.WarehouseId = a.WarehouseId
   where lm.ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating LocationMaster'
    goto error
  end
  
  update lm
     set LocationId = l.LocationId
    from LocationMaster lm (nolock)
    join Location        l (nolock) on lm.Location  = l.Location
    join AreaLocation   al (nolock) on l.LocationId = al.LocationId
    join Area            a (nolock) on al.AreaId    = a.AreaId
                                   and lm.AreaId    = a.AreaId
   where lm.ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating LocationMaster'
    goto error
  end
  
  update l
     set Ailse = t.Ailse,
         [Column] = t.[Column],
         [Level] = t.[Level],
         PalletQuantity = t.PalletQuantity,
         SecurityCode = isnull(t.SecurityCode,'99'),
         RelativeValue = t.RelativeValue,
         ActiveBinning = t.ActiveBinning,
         ActivePicking = t.ActivePicking,
         --LocationType = LocationType,
         NumberOfSUB = t.NumberOfSUB,
         NumberOfSU = t.NumberOfSU,
         Width = t.Width,
         Length = t.Length,
         Height = t.Height,
         Volume = t.Volume,
         Weight = t.Weight,
         TrackLPN = t.TrackLPN,
         FreightForwarding = t.FreightForwarding
    from LocationMaster t (nolock)
    join Location       l (nolock) on t.LocationId = l.LocationId
   where t.ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating Location'
    goto error
  end
  
  select @LocationId = max(LocationId)
    from Location
  
  if @LocationId is null
     set @LocationId = 0
  
  insert Location
        (LocationTypeId,
         Location,
         Ailse,
         [Column],
         Level,
         PalletQuantity,
         SecurityCode,
         RelativeValue,
         StocktakeInd,
         ActiveBinning,
         ActivePicking,
         Used,
         NumberOfSUB,
         NumberOfSU,
         Width,
         Length,
         Height,
         Volume,
         Weight,
         TrackLPN,
         FreightForwarding)
  select distinct isnull(lt.LocationTypeId,1),
         t.Location,
         t.Ailse,
         t.[Column],
         t.[Level],
         t.PalletQuantity,
         isnull(t.SecurityCode,'99'),
         RelativeValue,
         0,
         ActiveBinning,
         ActivePicking,
         0,
         NumberOfSUB,
         NumberOfSU,
         Width,
         Length,
         Height,
         Volume,
         Weight,
         TrackLPN,
         FreightForwarding
    from LocationMaster t
    left
    join LocationType lt on t.AreaCode = lt.LocationTypeCode collate SQL_Latin1_General_CP1_CI_AS
   where LocationId is null
     and isnull(Location,'') != ''
     and t.ProcessedDate = @GetDate
  order by t.Ailse, t.Location
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error inserting into Location'
    goto error
  end
  
  update lm
     set LocationId = l.LocationId
    from LocationMaster lm
    join Location l (nolock) on lm.Location = l.Location
   where l.LocationId > @LocationId
     and lm.ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating LocationMaster'
    goto error
  end
  
  insert AreaLocation
        (AreaId,
         LocationId)
  select distinct
         t.AreaId,
         t.LocationId
    from LocationMaster t
    left
    join AreaLocation al on t.LocationId = al.LocationId
   where al.LocationId is null
     and t.LocationId  is not null
     and t.AreaId      is not null
     and t.ProcessedDate = @GetDate
  order by t.LocationId
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error inserting into AreaLocation'
    goto error
  end
  
  update lm
     set StorageUnitId = vp.StorageUnitId
    from LocationMaster lm (nolock)
    join viewProduct    vp (nolock) on lm.ProductCode = vp.ProductCode
                                   and lm.SKUCode     = vp.SKUCode
   where lm.ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating LocationMaster'
    goto error
  end
  
  insert StorageUnitLocation
        (StorageUnitId,
         LocationId,
         MinimumQuantity,
         HandlingQuantity,
         MaximumQuantity)
  select distinct
         t.StorageUnitId,
         t.LocationId,
         t.MinimumQuantity,
         t.HandlingQuantity,
         t.MaximumQuantity
    from LocationMaster t
    left 
    join StorageUnitLocation sul on t.StorageUnitId = sul.StorageUnitId
                                and t.LocationId    = sul.LocationId
   where sul.StorageUnitId is null
     and t.LocationId    is not null
     and t.StorageUnitId is not null
     and t.ProcessedDate = @GetDate
  order by t.LocationId
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error inserting into AreaLocation'
    goto error
  end
  
  update LocationMaster
     set RecordStatus = 'Y'
   where RecordStatus = 'N'
     and ProcessedDate = @GetDate
  
  select @Error = @@error
  
  if @Error != 0
  begin
    set @Errormsg = 'p_Location_Master_Import - Error updating LocationMaster'
    goto error
  end
  
  commit transaction
  
  update DesktopLog
     set Errormsg = 'Successful',
         EndDate  = getdate()
   where DesktopLogId = @DesktopLogId
  
  return 0
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    
    update DesktopLog
       set Errormsg = isnull(@xmlstring, 'Error executing ' + OBJECT_NAME(@@PROCID)),
           EndDate  = getdate()
     where DesktopLogId = @DesktopLogId
    
    return @Error
end

