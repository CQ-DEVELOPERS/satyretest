﻿ 
/*
/// <summary>
///   Procedure Name : p_Mobile_Confirm_Location_No_Stock_Take
///   Filename       : p_Mobile_Confirm_Location_No_Stock_Take.sql
///   Create By      : Karen
///   Date Created   : April 2016
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
create procedure p_Mobile_Confirm_Location_No_Stock_Take
(
@instructionId int,
@storeBarcode  nvarchar(50) = null,
@pickBarcode   nvarchar(50) = null,
@operatorId    int = null
)
as
begin
     set nocount on;
         
  declare @MobileLogId                  int
  
  insert MobileLog
         (ProcName,
         InstructionId,
         Pick,
         Store,
         StartDate)
  select OBJECT_NAME(@@PROCID),
         @InstructionId,
         @pickBarcode,
         @storeBarcode,
         getdate()
         
  select @MobileLogId = scope_identity()
         
  declare @TableInstructions            as table
  (
  InstructionId int,
  Stored        bit
  )
  
  declare @Error                        int,
          @Errormsg                     nvarchar(500),
          @GetDate                      datetime,
          @StoreSecurityCode            nvarchar(15),
          @StoreLocation                nvarchar(15),
          @PickSecurityCode             nvarchar(15),
          @PickLocation                 nvarchar(15),
          @InstructionTypeCode          nvarchar(10),
          @LocationId                   int,
          @OutboundShipmentId           int,
          @IssueId                      int,
          @WarehouseId                  int,
          @PickAreaCode                 nvarchar(10),
          @StoreAreaCode                nvarchar(10),
          @JobId                        int,
          @StorageUnitId                int,
          @AreaType                     nvarchar(10),
          @ReceiptLineId                int,
          @StocktakeInd					bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
         
  select @storeBarcode = replace(@storeBarcode,'L:','')
  select @pickBarcode = replace(@pickBarcode,'L:','')
  
  set @Error = 0
  
  if @pickBarcode is not null
  begin
    select @PickLocation     = l.Location,
           @PickSecurityCode = l.SecurityCode,
           @WarehouseId      = i.WarehouseId,
           @PickAreaCode     = a.AreaCode,
           @JobId            = i.JobId
      from Instruction   i (nolock)
           join Location      l (nolock) on i.PickLocationId = l.LocationId
           join AreaLocation al (nolock) on l.LocationId     = al.LocationId
           join Area          a (nolock) on al.AreaId        = a.AreaId
     where i.InstructionId = @instructionId
     
    select @StocktakeInd     = l.StocktakeInd
      from Instruction   i (nolock)
           join Location      l (nolock) on i.PickLocationId = l.LocationId
     where i.InstructionId = @instructionId
    
    if @StocktakeInd = 1
    begin
		set @Error = 6
		goto Result
	end
    
    --if left(@pickBarcode,1) = '0'
    --   set @pickBarcode = substring(@pickBarcode, 2,14)
    
  --  if (select dbo.ufn_Configuration(217, @WarehouseId)) = 0 and @PickAreaCode in ('SP','RK') -- Confirm Location Code and Security Code
  --  -- or (@PickAreaCode = 'RK' and (select dbo.ufn_Configuration(216, @WarehouseId)) = 1)
  --  begin
  --    if  @pickBarcode != convert(nvarchar(15), @PickSecurityCode)
  --    begin
  --         set @Error = 6
  --      goto Result
  --    end
  --  end
  --  else
  --  begin
  --    if @pickBarcode != @PickLocation
  --       and @pickBarcode != convert(nvarchar(15), @PickSecurityCode)
  --    begin
  --         set @Error = 6
  --      goto Result
  --    end
  --  end
  --end
  
--  if @storeBarcode is not null
--  begin
--    select @StoreLocation       = l.Location,
--           @StoreSecurityCode   = l.SecurityCode,
--           @InstructionTypeCode = it.InstructionTypeCode,
--           @WarehouseId         = i.WarehouseId,
--           @StoreAreaCode       = a.AreaCode,
--           @JobId               = i.JobId,
--           @StorageUnitId       = sub.StorageUnitId,
--           @ReceiptLineId       = i.ReceiptLineId
--      from Instruction      i (nolock)
--           join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchid
--           join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
--           left
--           join Location         l (nolock) on i.StoreLocationId   = l.LocationId
--           left
--           join AreaLocation    al (nolock) on l.LocationId        = al.LocationId
--           left
--           join Area             a (nolock) on al.AreaId           = a.AreaId
--     where i.InstructionId = @instructionId
    
--    select @AreaType = idt.AreaType
--      from ReceiptLine          rl (nolock)
--           join InboundLine          il (nolock) on rl.InboundLineId = il.InboundLineId
--           join InboundDocument      id (nolock) on il.InboundDocumentId = id.InboundDocumentId
--           join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
--     where rl.ReceiptLineId = @ReceiptLineId
    
--    if @StoreLocation is null
--    begin
--      if @InstructionTypeCode in ('S','SM','PR','M')
--      begin
--        select @LocationId = l.LocationId
--          from Location          l (nolock)
--               join AreaLocation     al (nolock) on l.LocationId = al.LocationId
--               join Area              a (nolock) on al.AreaId    = a.AreaId
--               join StorageUnitArea sua (nolock) on a.AreaId     = sua.AreaId
--           and sua.StorageUnitId = @StorageUnitId
--         where a.WarehouseId = @WarehouseId
--           and (@storeBarcode = case when dbo.ufn_Configuration(217, @WarehouseId) = 0
--                                     then convert(nvarchar(15), l.SecurityCode)
--                                     else l.Location
--                                      end
--            or @storeBarcode = convert(nvarchar(15), l.SecurityCode))
--           and isnull(a.AreaType,'') = isnull(@AreaType,'')
--      end
--      else
--      begin
--        select @LocationId = l.LocationId
--          from Location          l (nolock)
--          join AreaLocation     al (nolock) on l.LocationId = al.LocationId
--          join Area              a (nolock) on al.AreaId    = a.AreaId
--         where a.WarehouseId = @WarehouseId
--           and (@storeBarcode = case when dbo.ufn_Configuration(217, @WarehouseId) = 0
--                                     then convert(nvarchar(15), l.SecurityCode)
--                                     else l.Location
--                                     end
--            or @storeBarcode = convert(nvarchar(15), l.SecurityCode))
--      end
  
--  if @LocationId is null
--  begin
--       set @Error = 6
--    goto result
--  end
  
--  exec @Error = p_StorageUnitBatchLocation_Deallocate
--  @InstructionId = @InstructionId,
--  @Pick          = 0,
--  @Store         = 1,
--  @Confirmed     = 1
  
--  if @Error <> 0
--  begin
--       set @Error = 6
--    goto result
--  end
  
--  exec @Error = p_Instruction_Update
--  @InstructionId = @InstructionId,
--  @StoreLocationId = @LocationId
  
--  if @Error <> 0
--  begin
--       set @Error = 6
--    goto result
--  end
  
--  exec p_StorageUnitBatchLocation_Reserve
--  @InstructionId = @InstructionId,
--  @Pick          = 0,
--  @Store         = 1,
--  @Confirmed     = 1
  
--  if @Error <> 0
--  begin
--       set @Error = 6
--    goto result
--  end
  
--  if (select dbo.ufn_Configuration(423, @WarehouseId)) = 1
--  Begin
--    exec p_StorageUnitBatchLocation_Allocate
--    @InstructionId = @InstructionId,
--    @Pick          = 0,
--    @Store         = 1,
--    @Confirmed     = 1
    
--    if @Error <> 0
--    begin
--         set @Error = 6
--      goto Result
--    end
--  end
--  --goto Result
--end
--if (select dbo.ufn_Configuration(291, @WarehouseId)) = 0 or (@InstructionTypeCode in ('PR','SM','S','M'))
--begin -- Allow checking location to change false
--  if (select dbo.ufn_Configuration(217, @WarehouseId)) = 0 and @StoreAreaCode in ('RK','SP') -- Confirm Location Code and Security Code
--  begin
--  if  @StoreBarcode != convert(nvarchar(15), @StoreSecurityCode)
--    begin
--         set @Error = 6
--      goto Result
--    end
--  end
--  else
--  begin
--    if @StoreBarcode != @StoreLocation
--       and @StoreBarcode != convert(nvarchar(15), @StoreSecurityCode)
--    begin
--         set @Error = 6
--      goto Result
--    end
--  end
--end -- Allow checking location to change false
--else
--begin -- Allow checking location to change true
--  if (select dbo.ufn_Configuration(217, @WarehouseId)) = 0
--  --or (@StoreAreaCode = 'RK' and (select dbo.ufn_Configuration(216, @WarehouseId)) = 0)
--     set @StoreLocation = '-1'
--  -- Commented out???
--  if @storeBarcode != isnull(@StoreLocation,'-1')
--  and @storeBarcode != isnull(convert(nvarchar(15), @StoreSecurityCode),'-1')
--  begin -- Location not equal to preivious
  
--    if @InstructionTypeCode in ('P','FM','PM','PS','R')
--    begin
--      if (@storeBarcode = isnull(@StoreLocation,'-1')
--      or  @storeBarcode = isnull(convert(nvarchar(15), @StoreSecurityCode),'-1'))
--      or (@StoreLocation = 'Despatch')
--      begin -- Despatch Location Check
--        if @LocationId Is Null
--           and (@StoreLocation in (select Location from viewLocation where AreaCode in ('CK','D'))
--            or  @StoreSecurityCode in (select SecurityCode from viewLocation where AreaCode in ('CK','D')))
--        begin-- Store Code check
--          select @StoreLocation     = l.Location,
--                 @StoreSecurityCode = l.SecurityCode,
--                 @LocationId        = l.LocationId
--            from Location      l (nolock)
--                 join AreaLocation al (nolock) on l.LocationId = al.LocationId
--                 join Area          a (nolock) on al.AreaId    = a.AreaId
--           where a.WarehouseId = @WarehouseId
--             and a.AreaCode in ('D','CK')
--             and (l.Location     = @storeBarcode
--                  or   convert(nvarchar(10), l.SecurityCode) = @storeBarcode)
        
--          if @@rowcount = 0 and @InstructionTypeCode = 'R'
--          begin
--            select @StoreLocation     = l.Location,
--                   @StoreSecurityCode = l.SecurityCode,
--                   @LocationId        = l.LocationId
--              from Location      l (nolock)
--                   join AreaLocation al (nolock) on l.LocationId = al.LocationId
--                   join Area          a (nolock) on al.AreaId    = a.AreaId
--             where a.WarehouseId = @WarehouseId
--               and (l.Location     = @storeBarcode
--                    or   convert(nvarchar(10), l.SecurityCode) = @storeBarcode)
--          end
          
--          if @LocationId is null
--          begin
--            set @Error = 6
--            goto result
--          end
--        end -- Store Code check
--      end -- Despatch Location Check
--      else
--      begin
--        begin
--          set @Error = 6
--          goto result
--        end
--      end
      
--      select @OutboundShipmentId = ili.OutboundShipmentId,
--             @IssueId            = ili.IssueId
--        from IssueLineInstruction (nolock) ili
--             inner join Instruction (nolock) i on ili.InstructionId = isnull(i.instructionRefId, i.instructionId)
--       where i.InstructionId = @InstructionId
      
--      --if @OutboundShipmentId is not null
--      --begin
--      --  if exists(select distinct s.StatusCode
--                    --              from IssueLineInstruction ili (nolock)
--                    --              join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
--                    --              join Job                    j (nolock) on i.JobId           = j.JobId
--                    --              join Status                 s (nolock) on j.StatusId        = s.StatusId
--                    --             where ili.OutboundShipmentId = @OutboundShipmentId
--                    --               and s.StatusCode in ('CK','CD','WC','D','DC','C'))
--      --  begin
--      --    set @Error = 6
-- --    goto result
--      --  end
--      --end
--      --else
--      --begin
--      --  if exists(select distinct s.StatusCOde
--                    --              from IssueLineInstruction ili (nolock)
--                    --              join Instruction       i (nolock) on ili.InstructionId = i.InstructionId
--                    --              join Job                    j (nolock) on i.JobId           = j.JobId
--                    --              join Status                 s (nolock) on j.StatusId        = s.StatusId
--                    --             where ili.IssueId = @IssueId
--                    --               and s.StatusCode in ('CK','CD','WC','D','DC','C'))
--      --  begin
--      --    set @Error = 6
--      --    goto result
--      --  end
--      --end
      
--      if @OutboundShipmentId is not null
--      begin -- @OutboundShipmentId is not null
--        update OutboundShipment
--           set LocationId = @LocationId
--         where OutboundShipmentId = @OutboundShipmentId
        
--        select @Error = @@Error
               
--        if @Error <> 0
--        begin
--             set @Error = 6
--          goto result
--        end
        
--        update i
--           set LocationId = @LocationId
--          from Issue                   i
--               join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
--         where OutboundShipmentId = @OutboundShipmentId
        
--        select @Error = @@Error
               
--        if @Error <> 0
--        begin
--             set @Error = 6
--          goto result
--        end
        
--        if (dbo.ufn_Configuration(363, @WarehouseId)) = 1
--        begin
--          insert @TableInstructions
--                 (InstructionId,
--                 Stored)
--          select i.InstructionId,
--                 isnull(i.Stored,0)
--            from Instruction i
--           where i.JobId            = @JobId
--             and isnull(i.StoreLocationId, -1) != @LocationId
--        end
--        else
--        begin
--          insert @TableInstructions
--                 (InstructionId,
--                 Stored)
--          select i.InstructionId,
--                 isnull(i.Stored,0)
--            from Instruction            i
--                 join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
--           where ili.OutboundShipmentId = @OutboundShipmentId
--             and isnull(i.StoreLocationId, -1)  != @LocationId
--        end

--        update i
--           set StoreLocationId = @LocationId
--          from Instruction         i
--               join @TableInstructions ti on i.InstructionId = ti.InstructionId
--         where ti.Stored          = 1
--           and i.StoreLocationId != @LocationId
        
--        select @Error = @@Error
               
--        if @Error <> 0
--        begin
--             set @Error = 6
--          goto result
--        end
--      end -- @OutboundShipmentId is not null
--      else if @IssueId is not null
--      begin -- @IssueId is not null
--        update Issue
--           set LocationId = @LocationId
--   where IssueId = @IssueId
        
--        select @Error = @@Error
               
--        if @Error <> 0
--        begin
--             set @Error = 6
--          goto result
--        end
        
--        if (dbo.ufn_Configuration(363, @WarehouseId)) = 1
--        begin
--          insert @TableInstructions
--                 (InstructionId,
--                 Stored)
--          select i.InstructionId,
--                 isnull(i.Stored,0)
--            from Instruction i
--           where i.JobId            = @JobId
--             and isnull(i.StoreLocationId, -1) != @LocationId
--        end
--        else
--        begin
--          insert @TableInstructions
--                 (InstructionId,
--                 Stored)
--          select i.InstructionId,
--                 isnull(i.Stored,0)
--            from Instruction            i
--                 join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
--           where ili.IssueId        = @IssueId
--             and isnull(i.StoreLocationId, -1) != @LocationId
--        end
--      end -- @IssueId is not null
      
--      update Issue set LocationId = @LocationId where IssueId = @IssueId
      
--      update i
--         set StoreLocationId = @LocationId
--        from Instruction         i
--             join @TableInstructions ti on i.InstructionId = ti.InstructionId
--       where ti.Stored          = 1
--         and i.StoreLocationId != @LocationId
      
--      select @Error = @@Error
             
--      if @Error <> 0
--      begin
--           set @Error = 6
--        goto result
--      end
--      --end
      
--      --delete @TableInstructions
--      -- where Stored = 1
      
--      while exists(select top 1 1
--                     from @TableInstructions)
--      begin
--        select top 1 @InstructionId = InstructionId
--          from @TableInstructions

--        exec @Error = p_StorageUnitBatchLocation_Reverse
--        @InstructionId = @InstructionId,
--        @Pick          = 0,
--        @Store         = 1,
--        @Confirmed     = 1
        
--        delete @TableInstructions
--         where InstructionId = @InstructionId
        
--        exec @Error = p_StorageUnitBatchLocation_Deallocate
--        @InstructionId = @InstructionId,
--        @Pick          = 0,
--        @Store         = 1,
--        @Confirmed     = 1
        
--        if @Error <> 0
--        begin
--             set @Error = 6
--          goto result
--        end
        
--        exec @Error = p_Instruction_Update
--        @InstructionId = @InstructionId,
--        @StoreLocationId = @LocationId,
--        @Stored = 0
        
--        if @Error <> 0
--        begin
--             set @Error = 6
--          goto result
--        end
        
--        update Instruction set Stored = null where InstructionId = @Instructionid
        
--        exec @Error = p_StorageUnitBatchLocation_Reserve
--        @InstructionId = @InstructionId,
--        @Pick          = 0,
--        @Store         = 1,
--        @Confirmed     = 1
        
--        if @Error <> 0
--        begin
--          set @Error = 6
--          goto result
--        end

--        if (select dbo.ufn_Configuration(423, @WarehouseId)) = 1
--        Begin
--          exec p_StorageUnitBatchLocation_Allocate
--          @InstructionId = @InstructionId,
--          @Pick          = 0,
--          @Store         = 1,
--          @Confirmed     = 1
          
--          if @Error <> 0
--          begin
--               set @Error = 6
--            goto result
--          end
          
--          exec @Error = p_Instruction_MovementSequence
--          @InstructionId = @InstructionId,
--          @operatorId = 1
          
--          if @Error <> 0
--          begin
--               set @Error = 6
--            goto result
--   end
--        end
--      end --End While
--      goto result
--      --end
--         set @Error = 6
--      goto Result
--    end
--    else
--    begin
--      set @Error = 6
--      goto result
--    end
--  end -- Location not equal to preivious
--end -- Allow checking location to change true
end
 
result:
update MobileLog
   set EndDate = getdate()
 where MobileLogId = @MobileLogId
 
select @Error
return @Error
end
