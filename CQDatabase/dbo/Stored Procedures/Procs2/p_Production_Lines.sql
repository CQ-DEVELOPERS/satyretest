﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Lines
  ///   Filename       : p_Production_Lines.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Lines
(
 @warehouseId int,
 @palletId    int,
 @jobId       int,
 @fromDate    datetime,
 @toDate      datetime,
 @statusId    int,
 @productCode nvarchar(30),
 @product     nvarchar(50),
 @skuCode     nvarchar(50),
 @batch       nvarchar(30)
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   WarehouseId        int,
   InstructionId      int,
   JobId              int,
   StorageUnitId      int,
   ProductBarcode     nvarchar(50),
   ProductCode        nvarchar(30),
   Product            nvarchar(50),
   SKUCode            nvarchar(50),
   Batch              nvarchar(50),
   Quantity           float,
   ConfirmedQuantity  float,
   Status             nvarchar(50),
   CreateDate         datetime,
   PalletId           nvarchar(13),
   StorageUnitBatchId int,
   OperatorId         int,
   Operator           nvarchar(50),
   OrderNumber        nvarchar(30),
   ReceiptLineId      int,
   ReferenceNumber    nvarchar(30),
   Weight             float,
   NettWeight         float
  )
  
  declare @GetDate datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @palletId = -1
    set @palletId = null
  
  if @jobId = -1
    set @jobId = null
  
  if @statusId = -1
    set @statusId = null
  
  if @productCode is null
     set @productCode = '%'
  
  if @product is null
     set @product = '%'
  
  if @skuCode is null
     set @skuCode = '%'
  
  if @batch is null
     set @batch = '%'
  
  if (select dbo.ufn_Configuration(229, @WarehouseId)) = 1 -- Retrieve by original delivery date
  begin
    insert @TableResult
          (WarehouseId,
           InstructionId,
           JobId,
           Quantity,
           ConfirmedQuantity,
           Status,
           CreateDate,
           PalletId,
           StorageUnitBatchId,
           OperatorId,
           ReceiptLineId,
           ReferenceNumber,
           Weight,
           NettWeight)
    select i.WarehouseId,
           i.InstructionId,
           i.JobId,
           i.Quantity,
           i.ConfirmedQuantity,
           s.Status,
           id.DeliveryDate,
           'P:' + convert(nvarchar(10), i.PalletId) as 'PalletId',
           i.StorageUnitBatchId,
           i.OperatorId,
           i.ReceiptLineId,
           j.ReferenceNumber,
           j.Weight,
           j.NettWeight
      from Instruction        i (nolock)
      join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
      join Job                j (nolock) on i.JobId              = j.JobId
      join Status             s (nolock) on j.StatusId           = s.StatusId
      join ReceiptLine       rl (nolock) on i.ReceiptLineId      = rl.ReceiptLineId
      join InboundLine       il (nolock) on rl.InboundLineId     = il.InboundLineId
      join InboundDocument   id (nolock) on il.InboundDocumentId = id.InboundDocumentId
     where i.WarehouseId          = @warehouseId
       and isnull(i.PalletId,-1)  = isnull(@palletId, isnull(i.PalletId,-1))
       and i.jobId                = isnull(@jobId, isnull(i.jobId,-1))
       and id.DeliveryDate  between @fromDate and @toDate
       and s.StatusId             = isnull(@statusId, s.StatusId)
       and it.InstructionTypeCode = 'PR'
       and s.StatusCode in ('W','PR','RE')
  end
  else
  begin -- Retrieve by create date
    insert @TableResult
          (WarehouseId,
           InstructionId,
           JobId,
           Quantity,
           ConfirmedQuantity,
           Status,
           CreateDate,
           PalletId,
           StorageUnitBatchId,
           OperatorId,
           ReceiptLineId,
           ReferenceNumber,
           Weight,
           NettWeight)
    select i.WarehouseId,
           i.InstructionId,
           i.JobId,
           i.Quantity,
           i.ConfirmedQuantity,
           s.Status,
           i.CreateDate,
           'P:' + convert(nvarchar(10), i.PalletId) as 'PalletId',
           i.StorageUnitBatchId,
           i.OperatorId,
           i.ReceiptLineId,
           j.ReferenceNumber,
           j.Weight,
           j.NettWeight
      from Instruction        i (nolock)
      join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
      join Job                j (nolock) on i.JobId              = j.JobId
      join Status             s (nolock) on j.StatusId           = s.StatusId
     where i.WarehouseId          = @warehouseId
       and isnull(i.PalletId,-1)  = isnull(@palletId, isnull(i.PalletId,-1))
       and i.jobId                = isnull(@jobId, isnull(i.jobId,-1))
       and i.CreateDate     between @fromDate and @toDate
       and s.StatusId             = isnull(@statusId, s.StatusId)
       and it.InstructionTypeCode = 'PR'
       and s.StatusCode in ('W','PR','RE')
  end
  
  update tr
     set StorageUnitId  = su.StorageUnitId,
         ProductCode    = p.ProductCode,
         ProductBarcode = p.Barcode,
         Product        = p.Product,
         SKUCode        = sku.SKUCode,
         Batch          = b.Batch
    from @TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Batch              b (nolock) on sub.BatchId           = b.BatchId
    join Product            p (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  update tr
     set OrderNumber = id.OrderNumber
    from @TableResult tr
    join ReceiptLine  rl on tr.ReceiptLineId = rl.ReceiptLineId
    join Receipt       r on rl.ReceiptId     = r.ReceiptId
    join InboundDocument id on r.InboundDocumentId = id.InboundDocumentId
  
  select distinct tr.InstructionId,
         tr.JobId,
         tr.OrderNumber,
         tr.Quantity,
         tr.ConfirmedQuantity,
         tr.Status,
         tr.CreateDate,
         datediff(mi, tr.CreateDate, @GetDate) as 'DwellTime',
         tr.PalletId,
         tr.StorageUnitBatchId,
         tr.ProductCode,
         tr.Product,
         tr.SKUCode,
         tr.Batch,
         tr.Operator,
         tr.ReferenceNumber,
         tr.Weight,
         tr.NettWeight
    from @TableResult tr
    left
    join Pack pk (nolock) on tr.WarehouseId = pk.WarehouseId
                         and tr.StorageUnitId = pk.StorageUnitId
   where (ProductCode like '%' + @productCode + '%' or ProductBarcode like '%' + @productCode + '%' or pk.Barcode like '%' + @productCode + '%')
     and Product     like '%' + @product + '%'
     and SKUCode     like '%' + @skuCode + '%'
     and Batch       like '%' + @batch + '%'
  order by Batch, ProductCode
end
