﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_MenuItem_Select
  ///   Filename       : p_MenuItem_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Sep 2012 16:06:10
  /// </summary>
  /// <remarks>
  ///   Selects rows from the MenuItem table.
  /// </remarks>
  /// <param>
  ///   @MenuItemId int = null 
  /// </param>
  /// <returns>
  ///   MenuItem.MenuItemId,
  ///   MenuItem.MenuId,
  ///   MenuItem.MenuItem,
  ///   MenuItem.ToolTip,
  ///   MenuItem.NavigateTo,
  ///   MenuItem.ParentMenuItemId,
  ///   MenuItem.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_MenuItem_Select
(
 @MenuItemId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         MenuItem.MenuItemId
        ,MenuItem.MenuId
        ,MenuItem.MenuItem
        ,MenuItem.ToolTip
        ,MenuItem.NavigateTo
        ,MenuItem.ParentMenuItemId
        ,MenuItem.OrderBy
    from MenuItem
   where isnull(MenuItem.MenuItemId,'0')  = isnull(@MenuItemId, isnull(MenuItem.MenuItemId,'0'))
  order by MenuItem
  
end
