﻿  
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Despatch_Confirm_Location
  ///   Filename       : p_Mobile_Despatch_Confirm_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Jan 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Despatch_Confirm_Location
(
 @DocumentNumber nvarchar(30) = null,
 @Location       nvarchar(15),
 @Barcode        nvarchar(30) = null,
 @Pallets        int = null output,
 @Total          int = null output
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @OutboundShipmentId int,
          @OutboundDocumentId int,
          @JobId              int,
          @StatusCode         nvarchar(10),
          @PalletId           int,
          @ReferenceNumber    nvarchar(30),
          @IssueId            int,
          @LocationId         int,
          @InstructionId      int,
          @ShipmentId         int,
          @DocumentId         int
  
  select @GetDate = dbo.ufn_Getdate()
  select @Location = replace(@Location,'L:','')
  set @Error = 0
  set @Pallets = 0
  set @Total = 0
  
  
  select @LocationId = LocationId
    from Location (nolock)
   where Location = @Location
  
  begin transaction
  
  if @LocationId is null
  begin
    set @Error = 900007
    set @Errormsg = 'Invalid Location'
    goto error
  end
  
  if @DocumentNumber is not null
  begin
    if @DocumentNumber is null and @Location is null
    begin
      set @Error = 900001
      set @Errormsg = 'Document and Location cannot both be null'
      goto error
    end
    
    if isnumeric(@DocumentNumber) = 1
    begin
      select @ShipmentId = OutboundShipmentId
        from OutboundShipment (nolock)
       where OutboundShipmentId = convert(int, @DocumentNumber)
    end
    
    if @ShipmentId is null
    begin
      select @DocumentId = OutboundDocumentId
        from OutboundDocument (nolock)
       where OrderNumber = @DocumentNumber
    end
    
    if @ShipmentId is null and @DocumentId is null
    begin
      set @Error = 900002
      set @Errormsg = 'Document not valid'
      goto error
    end
  end
  
  if @barcode is not null
  begin
    if isnumeric(replace(@barcode,'J:','')) = 1
    begin
      select @JobId           = replace(@barcode,'J:',''),
             @barcode = null
      
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
             @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
       where j.JobId = @JobId
    end
    else if isnumeric(replace(@barcode,'P:','')) = 1
    begin
      select @PalletId        = replace(@barcode,'P:',''),
             @barcode = null
      
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
             @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
       where i.PalletId = @PalletId
    end
    else if isnumeric(replace(@barcode,'R:','')) = 1
      select @ReferenceNumber = @barcode,
             @barcode         = null
    begin
      select top 1
             @JobId              = j.JobId,
             @StatusCode         = s.StatusCode,
             @OutboundShipmentId = ili.OutboundShipmentId,
             @IssueId            = ili.IssueId
        from IssueLineInstruction ili (nolock)
        join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
        join Job                    j (nolock) on i.JobId = j.JobId
        join Status                 s (nolock) on j.StatusId = s.StatusId
       where j.ReferenceNumber = @ReferenceNumber
    end
    
    if @JobId is null
    begin
      set @Error = 900006
      set @ErrorMsg = 'Could not find the pallet'
      goto error
    end
    
    if @StatusCode = 'C'
    begin
      set @Error = 900005
      set @ErrorMsg = 'The pallet is already complete'
      goto error
    end
    
    if @StatusCode in ('CD','D')
    begin
      while (select s.StatusCode
               from Job    j (nolock)
               join Status s (nolock) on j.StatusId = s.StatusId
              where j.JobId = @JobId) != 'DC'
      begin
        exec @Error = p_Status_Rollup
         @jobId = @jobId
        
        if @Error <> 0
          goto error
      end
    end
    else
    begin
      set @Error = 900003
      set @ErrorMsg = 'Incorrect Status'
      goto error
    end
    
    if @OutboundShipmentId is not null
    begin
      if @ShipmentId <> @OutboundShipmentId
      begin
        set @Error = 900004
        set @ErrorMsg = 'Could not tie up shipment and pallet'
        goto error
      end
      
      select @Pallets = count(distinct(ins.JobId))
        from Instruction           ins (nolock)
        join Job                     j (nolock) on ins.JobId = j.JobId
        join Status                  s (nolock) on j.StatusId = s.StatusId
       where ins.OutboundShipmentId = @OutboundShipmentId
         and s.StatusCode != 'DC'
      
      select @Total = count(distinct(ins.JobId))
        from Instruction           ins (nolock)
        join Job                     j (nolock) on ins.JobId = j.JobId
      join Status                    s (nolock) on j.StatusId = s.StatusId
       where ins.OutboundShipmentId = @OutboundShipmentId
    end
    else if @IssueId is not null
    begin
      if @DocumentId <> @OutboundDocumentId
      begin
        set @Error = 900004
        set @ErrorMsg = 'Could not tie up document and pallet'
        goto error
      end
      
      select @Pallets = count(distinct(ins.JobId))
        from IssueLineInstruction  ili (nolock)
        join Instruction           ins (nolock) on ili.InstructionId = ins.InstructionId
        join Job                     j (nolock) on ins.JobId = j.JobId
        join Status                  s (nolock) on j.StatusId = s.StatusId
       where ili.IssueId = @IssueId
         and s.StatusCode != 'DC'
      
      select @Total = count(distinct(ins.JobId))
        from IssueLineInstruction  ili (nolock)
        join Instruction           ins (nolock) on ili.InstructionId = ins.InstructionId
       where ili.IssueId = @IssueId
    end
  end
  
--  if @OutboundShipmentId is null and @OutboundDocumentId is null
--  begin
--    select @Pallets = count(distinct(ins.JobId))
--      from Instruction           ins
--      join Job                     j on ins.JobId = j.JobId
--      join Status                  s on j.StatusId = s.StatusId
--     where s.StatusCode = 'DC'
--    
--    select @Total = count(distinct(ins.JobId))
--      from Instruction           ins
--      join Job                     j on ins.JobId = j.JobId
--      join Status                  s on j.StatusId = s.StatusId
--     where s.StatusCode = 'DC'
--  end
  

--  begin
--    set @Error = 900007
--    set @Errormsg = 'No outstanding documents found for this location'
--    goto error
--  end
  
--  begin
--    set @Error = 900004
--    set @ErrorMsg = 'Could not tie up document and pallet'
--    goto error
--  end
  
  commit transaction
  select @Error
  return @Error
  
  error:
    --raiserror @Error @Errormsg
    rollback transaction
    select @Error
    return @Error
end
