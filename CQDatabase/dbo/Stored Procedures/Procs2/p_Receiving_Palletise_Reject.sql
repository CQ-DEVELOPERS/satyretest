﻿/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Palletise_Reject
  ///   Filename       : p_Receiving_Palletise_Reject.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure dbo.p_Receiving_Palletise_Reject
(
 @ReceiptId    int = null,
 @ReceiptLineId int = null
)

as
begin
	 set nocount on;
  
  declare @TableQuantity as table
  (
   TableQuantityId    int identity,
   ReceiptLineId      int,
   RejectQuantity     numeric(13,6),
   StorageUnitId      int,
   StorageUnitBatchId int
  );
  
  declare @TablePack as table
  (
   StorageUnitId   int,
   Quantity        numeric(13,6),
   InboundSequence smallint
  );
  
  declare @Error				int,
          @Errormsg				nvarchar(500),
          @GetDate				datetime,
          @WarehouseId			int,
          @TableQuantityId		int,
          @StorageUnitId		int,
          @StorageUnitBatchId	int,
          @PickLocationId		int,
          @StoreLocationId		int,
          @PalletQuantity		numeric(13,6),
          @RejectQuantity		numeric(13,6),
          @RemainingQuantity	numeric(13,6),
          @PalletCount			int,
          @InboundSequence		smallint,
          @TablePackCount		int,
          @Rowcount				int,
          @InstructionId		int,
          @InboundDocumentTypeCode	varchar(30),
          @AreaType				nvarchar(10)
          
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Receiving_Palletise_Reject'
  
  if @ReceiptLineId is not null
    set @ReceiptId = null
  
  if @ReceiptId is null
    select @ReceiptId = ReceiptId
      from ReceiptLine
     where ReceiptLineId = @ReceiptLineId
  
  select @WarehouseId    = WarehouseId,
         @PickLocationId = LocationId
    from Receipt (nolock)
   where ReceiptId = @ReceiptId   
 
  insert @TableQuantity
        (ReceiptLineId,
         RejectQuantity,
         StorageUnitId,
         StorageUnitBatchId)
  select rl.ReceiptLineId,
         rl.RejectQuantity,
         su.StorageUnitId,
         rl.StorageUnitBatchId
    from ReceiptLine      rl  (nolock)
    join Status             s (nolock) on rl.StatusId = s.StatusId
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  (nolock) on sub.StorageUnitId     = su.StorageUnitId
   where rl.ReceiptId     = isnull(@ReceiptId, rl.ReceiptId)
     and rl.ReceiptLineId = isnull(@ReceiptLineId, rl.ReceiptLineId)
     and s.StatusCode in ('R','RC','P','LA')
     and rl.RejectQuantity > 0
  
  select @TableQuantityId = scope_identity(), @Rowcount = @@rowcount
  
  insert @TablePack
        (StorageUnitId,
         Quantity,
         InboundSequence)
  select tq.StorageUnitId,
         p.Quantity,
         isnull(pt.InboundSequence,1)
    from @TableQuantity tq
    join Pack           p  (nolock) on tq.StorageUnitId = p.StorageUnitId
    join PackType       pt (nolock) on p.PackTypeId     = pt.PackTypeId
   where p.WarehouseId = @WarehouseId
  
  update Pack set Quantity = 99999 where Quantity = 0
  
  insert @TablePack
        (StorageUnitId,
         Quantity,
         InboundSequence)
  select tq.StorageUnitId,
         sku.Quantity,
         1
    from @TableQuantity tq
    join StorageUnit su on tq.StorageUnitId = su.StorageUnitId
    join SKU         sku on su.SKUId = sku.SKUId
   where --sku.WarehouseId = @WarehouseId
     not exists(select 1 from @TablePack tp where tq.StorageUnitId = tp.StorageUnitId)
  
  if @Rowcount = 0
  begin
    set @Error = 1
    goto result
  end
  
  if @ReceiptId is null and @ReceiptLineId is null
  begin
    set @Error = -1
    goto Error
  end
  
  while @TableQuantityId > 0
  begin
    select @RejectQuantity   = RejectQuantity,
           @StorageUnitId      = StorageUnitId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @ReceiptLineId      = ReceiptLineId
      from @TableQuantity
     where TableQuantityId = @TableQuantityId
    
    set @InboundSequence = 0
    
    select @TablePackCount = count(1) from @TablePack where StorageUnitId = @StorageUnitId
    
    select @AreaType = idt.AreaType,
		   @InboundDocumentTypeCode    = InboundDocumentTypeCode
      from ReceiptLine          rl (nolock)
      join InboundLine          il (nolock) on rl.InboundLineId = il.InboundLineId
      join InboundDocument      id (nolock) on il.InboundDocumentId = id.InboundDocumentId
      join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
     where rl.ReceiptLineId = @ReceiptLineId

    --select top 1 @StoreLocationId = l.LocationId
    --  from Location      l (nolock)
    --  join AreaLocation al (nolock) on l.LocationId = al.LocationId
    --  join Area          a (nolock) on al.AreaId    = a.AreaId
    -- where a.AreaCode in ('GS','JJ')
    --   and a.WarehouseId = @WarehouseId
       
    if @InboundDocumentTypeCode in ('Standard','BOND','RET','REWORK')
      begin
        select top 1 @StoreLocationId = l.LocationId
      from Location      l (nolock)
      join AreaLocation al (nolock) on l.LocationId = al.LocationId
      join Area          a (nolock) on al.AreaId    = a.AreaId
     where a.AreaCode in ('GS','JJ')
       and a.WarehouseId = @WarehouseId
       and isnull(a.AreaType,'') = isnull(@AreaType,'')
      end
    
    while @TablePackCount > 0
    begin
      select top 1
             @PalletQuantity = Quantity,
             @InboundSequence = InboundSequence
        from @TablePack
       where StorageUnitId = @StorageUnitId
         and InboundSequence > @InboundSequence
      order by InboundSequence
      
      set @PalletCount      = floor(@RejectQuantity / @PalletQuantity)
      set @RejectQuantity = @RejectQuantity - (@PalletQuantity * @PalletCount)
      
      if @PalletCount > 0
      begin
        if @PalletQuantity = 1 and @InboundSequence > 1 -- If is in Units combine otherwise put onto unit it's own pallet
        begin
          set @RemainingQuantity = @PalletQuantity * @PalletCount
          
          exec @Error = p_Palletised_Insert
           @WarehouseId         = @WarehouseId,
           @OperatorId          = null,
           @StorageUnitBatchId  = @StorageUnitBatchId,
           @PickLocationId      = @PickLocationId,
           @StoreLocationId     = @StoreLocationId,
           @Quantity            = @RemainingQuantity,
           @ReceiptLineId       = @ReceiptLineId,
           @InstructionId       = @InstructionId output
          
          if @error <> 0
            goto error
          
          
        end
        else
        begin
          while @PalletCount > 0
          begin
            set @PalletCount = @PalletCount - 1
            
            exec @Error = p_Palletised_Insert
             @WarehouseId         = @WarehouseId,
             @OperatorId          = null,
             @StorageUnitBatchId  = @StorageUnitBatchId,
             @PickLocationId      = @PickLocationId,
             @StoreLocationId     = @StoreLocationId,
             @Quantity            = @PalletQuantity,
             @ReceiptLineId       = @ReceiptLineId,
             @InstructionId       = @InstructionId output
            
            if @error <> 0
              goto error
          end
        end
      end
      
      set @TablePackCount = @TablePackCount - 1
    end
    
    -- Insert Remainder
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @InstructionTypeCode = 'S', -- Store
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = @StoreLocationId,
     @Quantity            = @RejectQuantity,
     @ReceiptLineId       = @ReceiptLineId
    
    if @error <> 0
      goto error
    
    set @TableQuantityId = @TableQuantityId - 1
  end
  
  result:
--  commit transaction
  return
  
  error:
--    RAISERROR (900000,-1,-1, @Errormsg);
--    rollback transaction
    return @Error
end
