﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Operator_Get_Upload_Access
  ///   Filename       : p_Operator_Get_Upload_Access.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Operator_Get_Upload_Access
(
 @OperatorId int
)

as
begin
	 set nocount on;
  
  select ISNULL(Upload, 0) as 'Upload'
    from Operator
   where OperatorId = @OperatorId
end
