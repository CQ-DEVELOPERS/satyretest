﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ReceiptLine_Search
  ///   Filename       : p_ReceiptLine_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Feb 2013 07:02:53
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ReceiptLine table.
  /// </remarks>
  /// <param>
  ///   @ReceiptLineId int = null output,
  ///   @ReceiptId int = null,
  ///   @InboundLineId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @StatusId int = null,
  ///   @OperatorId int = null,
  ///   @ChildStorageUnitBatchId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ReceiptLine.ReceiptLineId,
  ///   ReceiptLine.ReceiptId,
  ///   Receipt.ReceiptId,
  ///   ReceiptLine.InboundLineId,
  ///   InboundLine.InboundLineId,
  ///   ReceiptLine.StorageUnitBatchId,
  ///   StorageUnitBatch.StorageUnitBatchId,
  ///   ReceiptLine.StatusId,
  ///   Status.Status,
  ///   ReceiptLine.OperatorId,
  ///   Operator.Operator,
  ///   ReceiptLine.RequiredQuantity,
  ///   ReceiptLine.ReceivedQuantity,
  ///   ReceiptLine.AcceptedQuantity,
  ///   ReceiptLine.RejectQuantity,
  ///   ReceiptLine.DeliveryNoteQuantity,
  ///   ReceiptLine.SampleQuantity,
  ///   ReceiptLine.AcceptedWeight,
  ///   ReceiptLine.ChildStorageUnitBatchId,
  ///   StorageUnitBatch.StorageUnitBatchId,
  ///   ReceiptLine.NumberOfPallets,
  ///   ReceiptLine.AssaySamples,
  ///   ReceiptLine.BOELineNumber,
  ///   ReceiptLine.CountryofOrigin,
  ///   ReceiptLine.TariffCode,
  ///   ReceiptLine.Reference1,
  ///   ReceiptLine.Reference2,
  ///   ReceiptLine.UnitPrice
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ReceiptLine_Search
(
 @ReceiptLineId int = null output,
 @ReceiptId int = null,
 @InboundLineId int = null,
 @StorageUnitBatchId int = null,
 @StatusId int = null,
 @OperatorId int = null,
 @ChildStorageUnitBatchId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ReceiptLineId = '-1'
    set @ReceiptLineId = null;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
  if @InboundLineId = '-1'
    set @InboundLineId = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @ChildStorageUnitBatchId = '-1'
    set @ChildStorageUnitBatchId = null;
  
 
  select
         ReceiptLine.ReceiptLineId
        ,ReceiptLine.ReceiptId
        ,ReceiptLine.InboundLineId
        ,ReceiptLine.StorageUnitBatchId
        ,ReceiptLine.StatusId
         ,StatusStatusId.Status as 'Status'
        ,ReceiptLine.OperatorId
         ,OperatorOperatorId.Operator as 'Operator'
        ,ReceiptLine.RequiredQuantity
        ,ReceiptLine.ReceivedQuantity
        ,ReceiptLine.AcceptedQuantity
        ,ReceiptLine.RejectQuantity
        ,ReceiptLine.DeliveryNoteQuantity
        ,ReceiptLine.SampleQuantity
        ,ReceiptLine.AcceptedWeight
        ,ReceiptLine.ChildStorageUnitBatchId
        ,ReceiptLine.NumberOfPallets
        ,ReceiptLine.AssaySamples
        ,ReceiptLine.BOELineNumber
        ,ReceiptLine.CountryofOrigin
        ,ReceiptLine.TariffCode
        ,ReceiptLine.Reference1
        ,ReceiptLine.Reference2
        ,ReceiptLine.UnitPrice
		,ReceiptLine.ReceivedDate
		,ReceiptLine.PutawayStarted
		,ReceiptLine.PutawayEnded 
    from ReceiptLine
    left
    join Receipt ReceiptReceiptId on ReceiptReceiptId.ReceiptId = ReceiptLine.ReceiptId
    left
    join InboundLine InboundLineInboundLineId on InboundLineInboundLineId.InboundLineId = ReceiptLine.InboundLineId
    left
    join StorageUnitBatch StorageUnitBatchStorageUnitBatchId on StorageUnitBatchStorageUnitBatchId.StorageUnitBatchId = ReceiptLine.StorageUnitBatchId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = ReceiptLine.StatusId
    left
    join Operator OperatorOperatorId on OperatorOperatorId.OperatorId = ReceiptLine.OperatorId
    left
    join StorageUnitBatch StorageUnitBatchChildStorageUnitBatchId on StorageUnitBatchChildStorageUnitBatchId.StorageUnitBatchId = ReceiptLine.ChildStorageUnitBatchId
   where isnull(ReceiptLine.ReceiptLineId,'0')  = isnull(@ReceiptLineId, isnull(ReceiptLine.ReceiptLineId,'0'))
     and isnull(ReceiptLine.ReceiptId,'0')  = isnull(@ReceiptId, isnull(ReceiptLine.ReceiptId,'0'))
     and isnull(ReceiptLine.InboundLineId,'0')  = isnull(@InboundLineId, isnull(ReceiptLine.InboundLineId,'0'))
     and isnull(ReceiptLine.StorageUnitBatchId,'0')  = isnull(@StorageUnitBatchId, isnull(ReceiptLine.StorageUnitBatchId,'0'))
     and isnull(ReceiptLine.StatusId,'0')  = isnull(@StatusId, isnull(ReceiptLine.StatusId,'0'))
     and isnull(ReceiptLine.OperatorId,'0')  = isnull(@OperatorId, isnull(ReceiptLine.OperatorId,'0'))
     and isnull(ReceiptLine.ChildStorageUnitBatchId,'0')  = isnull(@ChildStorageUnitBatchId, isnull(ReceiptLine.ChildStorageUnitBatchId,'0'))
  
end
