﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Check_Valid_Location
  ///   Filename       : p_Mobile_Check_Valid_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Karen
  ///   Modified Date  : 2017-11-02
  ///   Details        : SAT01 - Do not allow for a blank location 
  /// </newpara>
*/
CREATE procedure p_Mobile_Check_Valid_Location
(
 @barcode nvarchar(30),
 @locationId int = null
)

as
begin
	 set nocount on;
  
  declare @bool bit

  if @barcode = ''													-- SAT01
	set @barcode = null												-- SAT01

  select @barcode = replace(@barcode,'L:','')
  
  if @locationId is null
  begin
    if exists(select top 1 1 from Location (nolock) where Location = @barcode)
      set @bool = 1
    else
      set @bool = 0
    
    select @bool
  end
  else
  begin
    if @locationId in (select locationId from Location (nolock) where Location = @barcode or convert(nvarchar(10), SecurityCode) = @barcode)
      set @bool = 1
    else
      set @bool = 0
    
    select @bool
  end
end
