﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Daily_Summary
  ///   Filename       : p_Report_Daily_Summary.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Daily_Summary

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   PickedOrders     int,
   OrdersDespatched int,
   OrdersNotDespatched int
   
  )
end
