﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Method_Insert
  ///   Filename       : p_Method_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:57:03
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Method table.
  /// </remarks>
  /// <param>
  ///   @MethodId int = null output,
  ///   @MethodCode nvarchar(20) = null,
  ///   @Method nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   Method.MethodId,
  ///   Method.MethodCode,
  ///   Method.Method 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Method_Insert
(
 @MethodId int = null output,
 @MethodCode nvarchar(20) = null,
 @Method nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
  if @MethodId = '-1'
    set @MethodId = null;
  
  if @MethodCode = '-1'
    set @MethodCode = null;
  
  if @Method = '-1'
    set @Method = null;
  
	 declare @Error int
 
  insert Method
        (MethodCode,
         Method)
  select @MethodCode,
         @Method 
  
  select @Error = @@Error, @MethodId = scope_identity()
  
  
  return @Error
  
end
