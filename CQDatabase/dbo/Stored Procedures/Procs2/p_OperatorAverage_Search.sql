﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorAverage_Search
  ///   Filename       : p_OperatorAverage_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:15
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorAverage table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorAverage.OperatorGroupId,
  ///   OperatorAverage.InstructionTypeId,
  ///   OperatorAverage.EndDate,
  ///   OperatorAverage.Units,
  ///   OperatorAverage.Weight,
  ///   OperatorAverage.Instructions,
  ///   OperatorAverage.Orders,
  ///   OperatorAverage.OrderLines,
  ///   OperatorAverage.Jobs,
  ///   OperatorAverage.ActiveTime,
  ///   OperatorAverage.DwellTime,
  ///   OperatorAverage.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorAverage_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         OperatorAverage.OperatorGroupId
        ,OperatorAverage.InstructionTypeId
        ,OperatorAverage.EndDate
        ,OperatorAverage.Units
        ,OperatorAverage.Weight
        ,OperatorAverage.Instructions
        ,OperatorAverage.Orders
        ,OperatorAverage.OrderLines
        ,OperatorAverage.Jobs
        ,OperatorAverage.ActiveTime
        ,OperatorAverage.DwellTime
        ,OperatorAverage.WarehouseId
    from OperatorAverage
  
end
