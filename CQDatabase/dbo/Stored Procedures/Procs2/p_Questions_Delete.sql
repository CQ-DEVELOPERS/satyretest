﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Questions_Delete
  ///   Filename       : p_Questions_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:05
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Questions table.
  /// </remarks>
  /// <param>
  ///   @QuestionId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Questions_Delete
(
 @QuestionId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Questions
     where QuestionId = @QuestionId
  
  select @Error = @@Error
  
  
  return @Error
  
end
