﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Packaging_Insert
  ///   Filename       : p_Packaging_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Packaging_Insert
(
 @jobId              int,
 @storageUnitBatchId int,
 @quantity           float,
 @operatorId         int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @ReceiptId         int,
          @ReceiptLineId     int,
          @StatusId          int,
          @PackageLineId     int,
          @PackagingWeight   numeric(13,6),
          @AcceptedWeight    numeric(13,6),
          @WarehouseId       int,
          @InboundSequence   int,
          @rowcount          int
  
  select @GetDate = dbo.ufn_Getdate()
	 
	 select @StatusId = dbo.ufn_StatusId('R','R')
	 
	 select @ReceiptLineId = i.ReceiptLineId,
	        @WarehouseId   = j.WarehouseId
	   from Job         j (nolock)
	   join Instruction i (nolock) on j.JobId = i.JobId
	  where j.JobId = @jobId
  
  select @ReceiptId   = ReceiptId
    from ReceiptLine (nolock)
   where ReceiptLineId = @ReceiptLineId
  
  select @InboundSequence = max(InboundSequence)
    from PackType (nolock)
  
  begin transaction
  
  update i
     set ConfirmedWeight = i.ConfirmedQuantity * pk.NettWeight
    from Instruction              i (nolock)
    join StorageUnitBatch       sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit             su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product                  p (nolock) on su.ProductId         = p.ProductId
    join Pack                    pk (nolock) on su.StorageUnitId     = pk.StorageUnitId
                                            and pk.WarehouseId       = i.WarehouseId
    join PackType                pt (nolock) on pk.PackTypeId        = pt.PackTypeId
  where i.JobId                        = @JobId
    and pt.InboundSequence             = @InboundSequence
    and isnull(su.ProductCategory,'') != 'V'
  
  select @error = @@error, @rowcount = @@rowcount
  
  if @Error <> 0
    goto error
  
  if @ReceiptId is not null
  begin
    delete ReceiptLinePackaging
     where JobId = @jobId
       and StorageUnitBatchId != @StorageUnitBatchId
    
    select @PackageLineId = PackageLineId
      from ReceiptLinePackaging (nolock)
     where JobId = @jobId
       and StorageUnitBatchId = @StorageUnitBatchId
    
    if @PackageLineId is null
    begin
      insert ReceiptLinePackaging
            (ReceiptId,
             ReceiptLineId,
             JobId,
             StorageUnitBatchId,
             StatusId,
             OperatorId,
             RequiredQuantity,
             ReceivedQuantity,
             AcceptedQuantity,
             RejectQuantity,
             DeliveryNoteQuantity)
      select @ReceiptId,
             @ReceiptLineId,
             @JobId,
             @StorageUnitBatchId,
             @StatusId,
             @operatorId,
             0,
             @quantity,
             @quantity,
             null,
             null
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
    end
    else
    begin
      update ReceiptLinePackaging
         set OperatorId       = @operatorId,
             ReceivedQuantity = @quantity,
             AcceptedQuantity = @quantity
       where PackageLineId = @PackageLineId
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
    end
  end
  if @rowcount != 0
  begin
    update j
       set NettWeight = (select sum(i.ConfirmedWeight)
                           from Instruction i (nolock)
                          where j.JobId = i.JobId)
      from Job         j
     where j.JobId = @JobId
    
    select @error = @@error
    
    if @Error <> 0
      goto error
  end
  else
  begin
    declare @TableResult as table
    (
     PackType         nvarchar(50),
     AcceptedQuantity numeric(13,6)
    )
    
    insert @TableResult
          (PackType,
           AcceptedQuantity)
    select sku.SKUCode,
           rlp.AcceptedQuantity
      from ReceiptLinePackaging rlp (nolock)
      join StorageUnitBatch     sub (nolock) on rlp.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit           su (nolock) on sub.StorageUnitId      = su.StorageUnitId
      join Product                p (nolock) on su.ProductId           = p.ProductId
      join SKU                  sku (nolock) on su.SKUId               = sku.SKUId
     where rlp.JobId             = @JobId
    
    select @PackagingWeight = tr.AcceptedQuantity * pk.TareWeight
      from Instruction            i (nolock)
      join StorageUnitBatch     sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit           su (nolock) on sub.StorageUnitId     = su.StorageUnitId
      join Product                p (nolock) on su.ProductId          = p.ProductId
      join Pack                  pk (nolock) on su.StorageUnitId      = pk.StorageUnitId
      join PackType              pt (nolock) on pk.PackTypeId         = pt.PackTypeId
      join @TableResult          tr          on pt.PackType           = tr.PackType
     where i.JobId = @JobId
       and pk.TareWeight is not null
    
    update Job
       set NettWeight = isnull(Weight,0) - isnull(TareWeight,0) - @PackagingWeight
     where JobId = @JobId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  select @AcceptedWeight = sum(isnull(j.NettWeight,0))
    from Job         j (nolock)
    join Instruction i (nolock) on j.JobId = i.JobId
   where i.ReceiptLineId = @ReceiptLineId
  
  if @AcceptedWeight is not null
  begin
    update ReceiptLine
       set AcceptedWeight = @AcceptedWeight
     where ReceiptLineId = @ReceiptLineId
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Packaging_Insert'); 
    rollback transaction
    return @Error
end
