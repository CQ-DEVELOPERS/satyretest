﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Process_Parameter
  ///   Filename       : p_Process_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Nov 2013 12:48:39
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Process table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Process.ProcessId,
  ///   Process.Process 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Process_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as ProcessId
        ,'{All}' as Process
  union
  select
         Process.ProcessId
        ,Process.Process
    from Process
  order by Process
  
end
