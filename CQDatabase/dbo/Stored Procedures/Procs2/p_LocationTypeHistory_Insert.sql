﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_LocationTypeHistory_Insert
  ///   Filename       : p_LocationTypeHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:47
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the LocationTypeHistory table.
  /// </remarks>
  /// <param>
  ///   @LocationTypeId int = null,
  ///   @LocationType nvarchar(100) = null,
  ///   @LocationTypeCode nvarchar(20) = null,
  ///   @DeleteIndicator bit = null,
  ///   @ReplenishmentIndicator bit = null,
  ///   @MultipleProductsIndicator bit = null,
  ///   @BatchTrackingIndicator bit = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   LocationTypeHistory.LocationTypeId,
  ///   LocationTypeHistory.LocationType,
  ///   LocationTypeHistory.LocationTypeCode,
  ///   LocationTypeHistory.DeleteIndicator,
  ///   LocationTypeHistory.ReplenishmentIndicator,
  ///   LocationTypeHistory.MultipleProductsIndicator,
  ///   LocationTypeHistory.BatchTrackingIndicator,
  ///   LocationTypeHistory.CommandType,
  ///   LocationTypeHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_LocationTypeHistory_Insert
(
 @LocationTypeId int = null,
 @LocationType nvarchar(100) = null,
 @LocationTypeCode nvarchar(20) = null,
 @DeleteIndicator bit = null,
 @ReplenishmentIndicator bit = null,
 @MultipleProductsIndicator bit = null,
 @BatchTrackingIndicator bit = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert LocationTypeHistory
        (LocationTypeId,
         LocationType,
         LocationTypeCode,
         DeleteIndicator,
         ReplenishmentIndicator,
         MultipleProductsIndicator,
         BatchTrackingIndicator,
         CommandType,
         InsertDate)
  select @LocationTypeId,
         @LocationType,
         @LocationTypeCode,
         @DeleteIndicator,
         @ReplenishmentIndicator,
         @MultipleProductsIndicator,
         @BatchTrackingIndicator,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
