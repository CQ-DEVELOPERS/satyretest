﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocumentType_Update
  ///   Filename       : p_OutboundDocumentType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Jan 2014 09:16:05
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OutboundDocumentType table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentTypeId int = null,
  ///   @OutboundDocumentTypeCode nvarchar(20) = null,
  ///   @OutboundDocumentType nvarchar(60) = null,
  ///   @PriorityId int = null,
  ///   @AlternatePallet bit = null,
  ///   @SubstitutePallet bit = null,
  ///   @AutoRelease bit = null,
  ///   @AddToShipment bit = null,
  ///   @MultipleOnShipment bit = null,
  ///   @LIFO bit = null,
  ///   @MinimumShelfLife numeric(13,3) = null,
  ///   @AreaType nvarchar(20) = null,
  ///   @AutoSendup bit = null,
  ///   @CheckingLane int = null,
  ///   @DespatchBay int = null,
  ///   @PlanningComplete bit = null,
  ///   @AutoInvoice bit = null,
  ///   @Backorder bit = null,
  ///   @AutoCheck bit = null,
  ///   @ReserveBatch bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocumentType_Update
(
 @OutboundDocumentTypeId int = null,
 @OutboundDocumentTypeCode nvarchar(20) = null,
 @OutboundDocumentType nvarchar(60) = null,
 @PriorityId int = null,
 @AlternatePallet bit = null,
 @SubstitutePallet bit = null,
 @AutoRelease bit = null,
 @AddToShipment bit = null,
 @MultipleOnShipment bit = null,
 @LIFO bit = null,
 @MinimumShelfLife numeric(13,3) = null,
 @AreaType nvarchar(20) = null,
 @AutoSendup bit = null,
 @CheckingLane int = null,
 @DespatchBay int = null,
 @PlanningComplete bit = null,
 @AutoInvoice bit = null,
 @Backorder bit = null,
 @AutoCheck bit = null,
 @ReserveBatch bit = null 
)

as
begin
	 set nocount on;
  
  if @OutboundDocumentTypeId = '-1'
    set @OutboundDocumentTypeId = null;
  
  if @OutboundDocumentTypeCode = '-1'
    set @OutboundDocumentTypeCode = null;
  
  if @OutboundDocumentType = '-1'
    set @OutboundDocumentType = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
  if @CheckingLane = '-1'
    set @CheckingLane = null;
  
  if @DespatchBay = '-1'
    set @DespatchBay = null;
  
	 declare @Error int
 
  update OutboundDocumentType
     set OutboundDocumentTypeCode = isnull(@OutboundDocumentTypeCode, OutboundDocumentTypeCode),
         OutboundDocumentType = isnull(@OutboundDocumentType, OutboundDocumentType),
         PriorityId = isnull(@PriorityId, PriorityId),
         AlternatePallet = isnull(@AlternatePallet, AlternatePallet),
         SubstitutePallet = isnull(@SubstitutePallet, SubstitutePallet),
         AutoRelease = isnull(@AutoRelease, AutoRelease),
         AddToShipment = isnull(@AddToShipment, AddToShipment),
         MultipleOnShipment = isnull(@MultipleOnShipment, MultipleOnShipment),
         LIFO = isnull(@LIFO, LIFO),
         MinimumShelfLife = isnull(@MinimumShelfLife, MinimumShelfLife),
         AreaType = isnull(@AreaType, AreaType),
         AutoSendup = isnull(@AutoSendup, AutoSendup),
         CheckingLane = isnull(@CheckingLane, CheckingLane),
         DespatchBay = isnull(@DespatchBay, DespatchBay),
         PlanningComplete = isnull(@PlanningComplete, PlanningComplete),
         AutoInvoice = isnull(@AutoInvoice, AutoInvoice),
         Backorder = isnull(@Backorder, Backorder),
         AutoCheck = isnull(@AutoCheck, AutoCheck),
         ReserveBatch = isnull(@ReserveBatch, ReserveBatch) 
   where OutboundDocumentTypeId = @OutboundDocumentTypeId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_OutboundDocumentTypeHistory_Insert
         @OutboundDocumentTypeId = @OutboundDocumentTypeId,
         @OutboundDocumentTypeCode = @OutboundDocumentTypeCode,
         @OutboundDocumentType = @OutboundDocumentType,
         @PriorityId = @PriorityId,
         @AlternatePallet = @AlternatePallet,
         @SubstitutePallet = @SubstitutePallet,
         @AutoRelease = @AutoRelease,
         @AddToShipment = @AddToShipment,
         @MultipleOnShipment = @MultipleOnShipment,
         @LIFO = @LIFO,
         @MinimumShelfLife = @MinimumShelfLife,
         @AreaType = @AreaType,
         @AutoSendup = @AutoSendup,
         @CheckingLane = @CheckingLane,
         @DespatchBay = @DespatchBay,
         @PlanningComplete = @PlanningComplete,
         @AutoInvoice = @AutoInvoice,
         @Backorder = @Backorder,
         @AutoCheck = @AutoCheck,
         @ReserveBatch = @ReserveBatch,
         @CommandType = 'Update'
  
  return @Error
  
end
