﻿
/*
  /// <summary>
  ///   Procedure Name : p_Location_Get_Dropme
  ///   Filename       : p_Location_Get_Dropme.sql
  ///   Create By      : GS
  ///   Date Created   : 21 Mar 2007
  /// </summary>
  /// <remarks>
  ///   Gets a Location base on the StorageUnitBatch and Quantity
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyId             int = null
  /// </param>
  /// <returns>
  ///   ExternalCompanyId             int,
  ///   ECTypeId                      int,
  ///   Code                          nvarchar(30),
  ///   Name                          nvarchar(50),
  ///   Rating                        int 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Location_Get_Dropme
(
 @WarehouseId        int,
 @LocationId         int output,
 @StorageUnitBatchId int,
 @Quantity           float,
 @FullPalletInd      bit = null,
 @AreaType           nvarchar(10) = '' -- Used for QA Area
)
as
begin
  declare @StorageUnitArea as table
  (
   AreaId           int,
   AreaCode         nvarchar(10),
   MinimumShelfLife float,
   StoreOrder       int
  )
  
  declare @TableOverFlow as table
  (
   LocationId     int,
   RelativeValue  numeric(13,3),
   StorageUnitId  int,
   PalletQuantity float,
   Pallets        numeric(13,3)
  )
  
  declare @TableResult as table
  (
   LocationId     int,
   RelativeValue  numeric(13,3),
   PalletQuantity float,
   Pallets        numeric(13,3)
  )
  
  declare @OldLocationId      int,
          @FullPallet         int,
          @StorageUnitId      int,
          @rowcount           int,
          @AreaId             int,
          @NearLocationId     int,
          @RelativeValue      numeric(13,3),
          @TrackBatchPF       bit,
          @FillPFFirst        bit,
          @AllowPartRK        bit,
          @AreaCode           nvarchar(10),
          @Error              int,
          @StackingCategory   int,
          @BatchStatus        nvarchar(10),
          @MinimumShelfLife   float,
          @ShelfLifePecentage float,
          @OldLocation        nvarchar(15),
          @MixStorageUnitId   int,
          @StoreOrder         int,
          @RackingCount       int,
          @MaxRacking         int
  
  set @OldLocationId = @LocationId
  set @LocationId = null
  select @FillPFFirst = dbo.ufn_Configuration(20, @warehouseId)
  
  if dbo.ufn_Configuration(341, @warehouseId) = 1
    select @MaxRacking = dbo.ufn_Configuration_Value(341, @warehouseId)
  
  select @StorageUnitId      = sub.StorageUnitId,
         @BatchStatus        = s.StatusCode,
         @ShelfLifePecentage = (convert(float, datediff(dd, getdate(), ExpiryDate)) / convert(float, p.ShelfLifeDays)) * 100
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId      = p.ProductId
    join Batch              b (nolock) on sub.BatchId       = b.BatchId
    join Status             s (nolock) on b.StatusId        = s.StatusId
   where sub.StorageUnitBatchId = @StorageUnitBatchId
  
  if @ShelfLifePecentage is null
    set @ShelfLifePecentage = 0
  
  if dbo.ufn_Configuration(143, @warehouseId) = 1
    if @BatchStatus = 'QC'
      set @AreaType = @BatchStatus
  
  select @StackingCategory = isnull(StackingCategory,1)
    from StorageUnit (nolock) 
   where StorageUnitId = @StorageUnitId
  
  select @FullPallet = p.Quantity
    from Pack      p (nolock)
    join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
   where p.StorageUnitId   = @StorageUnitId
     and pt.InboundSequence = 1 -- Full Pallet
     and p.WarehouseId = @WarehouseId
  
  if @FillPFFirst = 1
  or
  not exists(select top 1 1
               from StorageUnitBatchLocation subl (nolock)
               join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
               join AreaLocation               al (nolock) on subl.LocationId         = al.LocationId
               join Area                        a (nolock) on al.AreaId               = a.AreaId
              where a.WarehouseId       = @WarehouseId
                and a.StockOnHand       = 1
                and a.AreaCode         in ('RK','BK','SP')
                and sub.StorageUnitId   = @StorageUnitId
                and subl.ActualQuantity > 0) -- No Stock in warehouse (i.e. new batch in pick face is ok)
  begin
    -- Get empty location for product
    select top 1 @LocationId = l.LocationId
      from StorageUnitLocation sul (nolock)
      join Location              l (nolock) on sul.LocationId = l.LocationId
      join AreaLocation         al (nolock) on l.Locationid   = al.LocationId
      join Area                  a (nolock) on al.AreaId      = a.AreaId
     where a.WarehouseId         = @WarehouseId
       and sul.StorageUnitId     = @StorageUnitId
       and l.StocktakeInd        = 0
       and l.Used                = 0
       and a.AreaCode           != 'BK'
       and isnull(a.AreaType,'') = @AreaType
    
    if @LocationId is null
    begin
      select top 1 @LocationId = l.LocationId
        from StorageUnitLocation       sul (nolock)
        join Location                    l (nolock) on sul.LocationId         = l.LocationId
        join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
        join Area                        a (nolock) on al.AreaId              = a.AreaId
       where l.ActiveBinning        = 1 -- Yes
         and l.StocktakeInd         = 0
         and isnull(a.AreaType,'')  = @AreaType
         and sul.StorageUnitId      = @StorageUnitId
         and a.WarehouseId          = @WarehouseId
         and not exists(select top 1 1
                          from StorageUnitBatch          sub (nolock)
                          join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                         where sul.StorageUnitId      = sub.StorageUnitId
                           and l.LocationId           = subl.LocationId
                           and (subl.ActualQuantity    > 0
                             or subl.AllocatedQuantity > 0))
      order by a.AreaCode
      
      if dbo.ufn_Configuration(283, @warehouseId) = 1 -- If same batch is already in the pickface
      begin
        select top 1 @LocationId = l.LocationId
          from StorageUnitLocation       sul (nolock)
          join Location                    l (nolock) on sul.LocationId         = l.LocationId
          join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
          join Area                        a (nolock) on al.AreaId              = a.AreaId
          join StorageUnitBatchLocation subl (nolock) on l.LocationId           = subl.LocationId
         where l.ActiveBinning         = 1 -- Yes
           and l.StocktakeInd          = 0
           and isnull(a.AreaType,'')   = @AreaType
           and sul.StorageUnitId       = @StorageUnitId
           and a.WarehouseId           = @WarehouseId
           and subl.StorageUnitBatchId = @StorageUnitBatchId
           and subl.ActualQuantity + @Quantity <= @FullPallet
        order by a.AreaCode
      end
    end
    
    if @LocationId is not null
      goto LocationFound
  end
  
  -- Get location you want the product to be near
  select top 1 @NearLocationId = l.LocationId,
               @RelativeValue = RelativeValue
    from StorageUnitLocation sul (nolock)
    join Location              l (nolock) on sul.LocationId = l.LocationId
    join AreaLocation               al (nolocK) on l.LocationId = al.LocationId
    join Area                        a (nolock) on al.AreaId    = a.AreaId
   where sul.StorageUnitId = @StorageUnitId
     and isnull(a.AreaType,'') = @AreaType
  
  -- Get the next location anywhere in warehouse
  if @NearLocationId is null
    select top 1 @NearLocationId = l.LocationId, @RelativeValue = RelativeValue
      from Location                    l (nolock)
      join AreaLocation               al (nolocK) on l.LocationId = al.LocationId
      join Area                        a (nolock) on al.AreaId    = a.AreaId
      join StorageUnitBatchLocation subl (nolock) on l.LocationId = subl.LocationId
      join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
     where sub.StorageUnitId     = @StorageUnitId
       and isnull(a.AreaType,'') = @AreaType
  
  --select @NearLocationId as '@NearLocationId', @RelativeValue as 'RelativeValue'
  
  if @RelativeValue is null
    set @RelativeValue = 0
  
  if @FullPalletInd is null
    if @Quantity = @FullPallet
      set @FullPalletInd = 1
    else
      set @FullPalletInd = 0
  
  if @FullPalletInd = 1
  begin
    -- Get the Areas allowed for StorageUnit
    insert @StorageUnitArea
          (AreaId,
           AreaCode,
           MinimumShelfLife,
           StoreOrder)
    select a.AreaId,
           a.AreaCode,
           a.MinimumShelfLife,
           sua.StoreOrder
      from StorageUnitArea sua (nolock)
      join Area              a (nolock) on sua.AreaId = a.AreaId
     where sua.StorageUnitId     = @StorageUnitId
       and a.WarehouseId         = @WarehouseId
       and isnull(a.AreaType,'') = @AreaType
       and @ShelfLifePecentage >= isnull(a.MinimumShelfLife, @ShelfLifePecentage)
    order by StoreOrder
    
    select @rowcount = @@rowcount
    
    if @rowcount = 0
    begin
      -- Get the Areas allowed for StorageUnit
      insert @StorageUnitArea
            (AreaId,
             AreaCode,
             MinimumShelfLife,
             StoreOrder)
      select a.AreaId,
             a.AreaCode,
             a.MinimumShelfLife,
             sua.StoreOrder
        from StorageUnitArea sua (nolock)
        join Area              a (nolock) on sua.AreaId = a.AreaId
       where sua.StorageUnitId     = @StorageUnitId
         and a.WarehouseId         = @WarehouseId
         and isnull(a.AreaType,'') = @AreaType
      order by StoreOrder
      
      select @rowcount = @@rowcount
      
      if @rowcount = 0
      begin
        print 'No Areas defined for Product!'
        return
      end
    end
    
    while @rowcount > 0
    begin
      select @rowcount = @rowcount - 1
      
      select top 1 @AreaId           = AreaId,
                   @AreaCode         = AreaCode,
                   @MinimumShelfLife = MinimumShelfLife,
                   @StoreOrder       = StoreOrder
        from @StorageUnitArea
      
      if @AreaCode in ('BK','RK') and @StoreOrder = 1 and isnull(@MaxRacking,0) > 0
      begin
        select @RackingCount = count(distinct(subl.LocationId))
          from StorageUnitBatchLocation subl (nolock)
          join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
          join AreaLocation               al (nolock) on subl.LocationId         = al.LocationId
          join Area                        a (nolock) on al.AreaId               = a.AreaId
         where a.WarehouseId       = @WarehouseId
           and a.StockOnHand       = 1
           and a.AreaCode         in ('RK')
           and sub.StorageUnitId   = @StorageUnitId
           and (subl.ActualQuantity > 0 or subl.AllocatedQuantity > 0)
        
        if @MaxRacking > @RackingCount and @AreaCode = 'BK' -- If Bulk get racking locations first
          select top 1 @AreaId           = AreaId,
                       @AreaCode         = AreaCode,
                       @MinimumShelfLife = MinimumShelfLife,
                       @StoreOrder       = StoreOrder
            from @StorageUnitArea
           where AreaId != @AreaId
             and AreaCode = 'RK'
          order by StoreOrder
        
        if @RackingCount > @MaxRacking
          delete @StorageUnitArea
           where AreaCode = 'RK'
      end
      
      delete @StorageUnitArea
        where AreaId = @AreaId
      
--      if @FillPFFirst = 1
--      begin
--        select top 1 @LocationId = l.LocationId
--          from StorageUnitLocation       sul (nolock)
--          join Location                    l (nolock) on sul.LocationId         = l.LocationId
--          join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
--          join Area                        a (nolock) on al.AreaId              = a.AreaId
--          join StorageUnitBatch          sub (nolock) on sul.StorageUnitId      = sub.StorageUnitId
--          left outer
--          join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
--                                                     and l.LocationId           = subl.LocationId
--         where l.ActiveBinning       = 1 -- Yes
--           and l.StocktakeInd        = 0
--           and isnull(a.AreaType,'') = @AreaType
--           and sub.StorageUnitId     = @StorageUnitId
--           and a.WarehouseId         = @WarehouseId
--           and a.AreaCode            = 'PK'
--        order by ActualQuantity - ReservedQuantity desc,
--                 case when @RelativeValue > isnull(l.RelativeValue,0)
--                      then @RelativeValue - isnull(l.RelativeValue,0)
--                      else isnull(l.RelativeValue,0) - @RelativeValue
--                      end
--        
--        if @LocationId is not null
--          goto LocationFound
--      end
      
      if dbo.ufn_Configuration(252, @warehouseId) = 1
        if @AreaCode = 'SP'
          set @AreaCode = 'WW'
      
      if @AreaCode in ('BK','WW') and @Quantity >= @FullPallet
      begin
--        -- Get bulk location where product already in
--        select top 1 @LocationId = l.LocationId
--          from Location                    l (nolock)
--          join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
--          join StorageUnitBatchLocation subl /*lock*/ on l.LocationId            = subl.LocationId
--          join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
--         where al.AreaId       = @AreaId
--           and l.ActiveBinning = 1 -- Yes
--           and l.LocationId != isnull(@OldLocationId,'0')
--           and isnull(l.PalletQuantity,100) * @StackingCategory > ((subl.ActualQuantity + subl.AllocatedQuantity) / @FullPallet) -- Can it fit?
--           and sub.StorageUnitId = @StorageUnitId
--        order by case when @RelativeValue > isnull(l.RelativeValue,0)
--                      then @RelativeValue - isnull(l.RelativeValue,0)
--                      else isnull(l.RelativeValue,0) - @RelativeValue
--                      end
        
        if dbo.ufn_Configuration(275, @warehouseId) = 0 -- Mix products in bulk lanes
          set @MixStorageUnitId = @StorageUnitId
        
        insert @TableOverFlow
              (Locationid,
               RelativeValue,
               StorageUnitId,
               PalletQuantity,
               Pallets)
        select l.LocationId,
               l.RelativeValue,
               @StorageUnitId,
               l.PalletQuantity * @StackingCategory,
               sum(convert(numeric(13,3), (subl.ActualQuantity + subl.AllocatedQuantity)) / convert(numeric(13,3), pk.Quantity))
          from Location                    l (nolock)
          join AreaLocation               al (nolock) on l.LocationId = al.LocationId
          join StorageUnitBatchLocation subl /*lock*/ on l.LocationId = subl.LocationId
          join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
          join Pack                       pk (nolock) on sub.StorageUnitId = pk.StorageUnitId
          join PackType                   pt (nolock) on pk.PackTypeId = pt.PackTypeId
         where al.AreaId       = @AreaId
           and l.ActiveBinning = 1 -- Yes
           --and l.StockTakeInd  = 0 -- No 
           and l.LocationId   != isnull(@OldLocationId,'0')
           and pt.InboundSequence = 1
           --and sub.StorageUnitId = @StorageUnitId
           and exists(select 1 -- Add to allow mixing products
                        from StorageUnitBatchLocation subl2 (nolock)
                        join StorageUnitBatch          sub2 (nolock) on subl2.StorageUnitBatchId = sub2.StorageUnitBatchId and sub2.StorageUnitId = isnull(@MixStorageUnitId, sub2.StorageUnitId)
                       where subl2.LocationId = subl.LocationId) -- Product N
        --group by l.LocationId, RelativeValue, sub.StorageUnitId, PalletQuantity
        group by l.LocationId, RelativeValue, PalletQuantity
        
        if @@rowcount > 0
        begin
          insert @TableResult
                (Locationid,
                 RelativeValue,
                 PalletQuantity,
                 Pallets)
          select LocationId,
                 RelativeValue,
                 PalletQuantity,
                 sum(Pallets)
            from @TableOverFlow
          group by LocationId, RelativeValue, PalletQuantity
          
          select top 1 @LocationId = LocationId
            from @TableResult
           where PalletQuantity > Pallets
          order by case when @RelativeValue > isnull(RelativeValue,0)
                        then @RelativeValue - isnull(RelativeValue,0)
                        else isnull(RelativeValue,0) - @RelativeValue
                        end
          
          if @LocationId is not null
            goto LocationFound
        end
        
        if @LocationId is null
          -- Get empty bulk location where product is allocated to
          select top 1 @LocationId = l.LocationId
            from Location                    l (nolock)
            join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
            join StorageUnitLocation       sul (nolock) on al.LocationId           = sul.LocationId
                                                       and sul.StorageUnitId       = @StorageUnitId
           where al.AreaId       = @AreaId
             and l.Used          = 0 -- No
             and l.ActiveBinning = 1 -- Yes
             --and l.StockTakeInd  = 0 -- No
             and l.LocationId != isnull(@OldLocationId,'0')
          order by l.StockTakeInd,
                   case when @RelativeValue > isnull(l.RelativeValue,0)
                        then @RelativeValue - isnull(l.RelativeValue,0)
                        else isnull(l.RelativeValue,0) - @RelativeValue
                        end
        
        if dbo.ufn_Configuration(225, @warehouseId) = 0 -- Use dedicated bulk lanes only
          if @LocationId is null
            -- Get empty bulk location where product is not allocated and not reserved but other product
            select top 1 @LocationId = l.LocationId
              from Location                    l (nolock)
              join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
              join Area                        a (nolock) on al.AreaId               = a.AreaId
              join StorageUnitArea           sua (nolock) on a.AreaId                = sua.AreaId
                                                         and sua.StorageUnitId       = @StorageUnitId
              left
              join StorageUnitLocation       sul (nolock) on al.LocationId           = sul.LocationId
                                                         and sul.StorageUnitId      != @StorageUnitId
             where al.AreaId       = @AreaId
               and a.AreaCode      = 'BK'
               and l.Used          = 0 -- No
               and l.ActiveBinning = 1 -- Yes
               and l.StockTakeInd  = 0 -- No 
               and l.LocationId != isnull(@OldLocationId,'0')
            order by case when @RelativeValue > isnull(l.RelativeValue,0)
                          then @RelativeValue - isnull(l.RelativeValue,0)
                          else isnull(l.RelativeValue,0) - @RelativeValue
                          end
        
        if @LocationId is not null
          goto LocationFound
      end
      
      if @AreaCode = 'WW'
        set @AreaCode = 'SP'
      
      if @AreaCode in ('SP','PK')
      begin
        select @OldLocation = Location
          from Location (nolock)
         where LocationId = @OldLocationId
        
        if @OldLocation is null
          set @OldLocation = '0'
        
        -- Get empty location near to NearLocationId
        select top 1 @LocationId = l.LocationId
          from Location      l (nolock)
          join AreaLocation al (nolock) on l.LocationId = al.LocationId
          join StorageUnitLocation sul (nolock) on l.LocationId = sul.LocationId
         where al.AreaId       = @AreaId
           and l.Location      > isnull(@OldLocation,'0')
           and sul.StorageUnitId = @StorageUnitId
        order by case when @RelativeValue > isnull(l.RelativeValue,0)
                      then @RelativeValue - isnull(l.RelativeValue,0)
                      else isnull(l.RelativeValue,0) - @RelativeValue
                      end,
                 l.Location
        
        if @@rowcount = 0
        begin
          -- Get empty location near to NearLocationId
          select top 1 @LocationId = l.LocationId
            from Location      l (nolock)
            join AreaLocation al (nolock) on l.LocationId = al.LocationId
            join StorageUnitLocation sul (nolock) on l.LocationId = sul.LocationId
           where al.AreaId       = @AreaId
             and l.Location      < isnull(@OldLocation,'0')
             and sul.StorageUnitId = @StorageUnitId
          order by case when @RelativeValue > isnull(l.RelativeValue,0)
                        then @RelativeValue - isnull(l.RelativeValue,0)
                        else isnull(l.RelativeValue,0) - @RelativeValue
                        end,
                 l.Location
        end
        
        if @LocationId is not null
          goto LocationFound
        
        -- Get empty location near to NearLocationId
        select top 1 @LocationId = l.LocationId
          from Location      l (nolock)
          join AreaLocation al (nolock) on l.LocationId = al.LocationId
         where al.AreaId       = @AreaId
           and l.LocationId   != isnull(@OldLocationId,'0')
        order by case when @RelativeValue > isnull(l.RelativeValue,0)
                      then @RelativeValue - isnull(l.RelativeValue,0)
                      else isnull(l.RelativeValue,0) - @RelativeValue
                      end
        
        if @LocationId is not null
          goto LocationFound
      end
      
      if @AreaCode = 'OF'
      begin
        insert @TableOverFlow
              (Locationid,
               RelativeValue,
               StorageUnitId,
               PalletQuantity,
               Pallets)
        select l.LocationId,
               l.RelativeValue,
               sub.StorageUnitId,
               l.PalletQuantity * @StackingCategory,
               sum(convert(numeric(13,3), (subl.ActualQuantity + subl.AllocatedQuantity)) / convert(numeric(13,3), pk.Quantity))
          from Location                    l (nolock)
          join AreaLocation               al (nolock) on l.LocationId = al.LocationId
          join StorageUnitBatchLocation subl /*lock*/ on l.LocationId = subl.LocationId
          join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
          join Pack                       pk (nolock) on sub.StorageUnitId = pk.StorageUnitId
          join PackType                   pt (nolock) on pk.PackTypeId = pt.PackTypeId
         where al.AreaId       = @AreaId
           and l.ActiveBinning = 1 -- Yes
           and l.StockTakeInd  = 0 -- No 
           and l.LocationId   != isnull(@OldLocationId,'0')
           and pt.InboundSequence = 1
        group by l.LocationId, RelativeValue, sub.StorageUnitId, PalletQuantity
        
        insert @TableResult
              (Locationid,
               RelativeValue,
               PalletQuantity,
               Pallets)
        select LocationId,
               RelativeValue,
               PalletQuantity,
               sum(Pallets)
          from @TableOverFlow
        group by LocationId, RelativeValue, PalletQuantity
        
        select top 1 @LocationId = LocationId
          from @TableResult
         where PalletQuantity > Pallets
        order by case when @RelativeValue > isnull(RelativeValue,0)
                      then @RelativeValue - isnull(RelativeValue,0)
                      else isnull(RelativeValue,0) - @RelativeValue
                      end
        
        if @LocationId is not null
          goto LocationFound
        
        -- Get empty location near to NearLocationId
        select top 1 @LocationId = l.LocationId
          from Location      l (nolock)
          join AreaLocation al (nolock) on l.LocationId = al.LocationId
          left
          join StorageUnitBatchLocation subl /*lock*/ on l.LocationId = subl.LocationId
         where al.AreaId       = @AreaId
           and l.Used          = 0 -- No
           and l.ActiveBinning = 1 -- Yes
           and l.StockTakeInd  = 0 -- No 
           and l.LocationId   != isnull(@OldLocationId,'0')
        order by case when @RelativeValue > isnull(l.RelativeValue,0)
                      then @RelativeValue - isnull(l.RelativeValue,0)
                      else isnull(l.RelativeValue,0) - @RelativeValue
                      end
        
        if @LocationId is not null
          goto LocationFound
      end
      
      if @AreaCode = 'RK'
        -- Get empty location near to NearLocationId
        select top 1 @LocationId = l.LocationId
          from Location      l (nolock)
          join AreaLocation al (nolock) on l.LocationId = al.LocationId
         where al.AreaId       = @AreaId
           and l.Used          = 0 -- No
           and l.ActiveBinning = 1 -- Yes
           and l.StockTakeInd  = 0 -- No 
           and l.LocationId   != isnull(@OldLocationId,'0')
        order by case when @RelativeValue > isnull(l.RelativeValue,0)
                      then @RelativeValue - isnull(l.RelativeValue,0)
                      else isnull(l.RelativeValue,0) - @RelativeValue
                      end
      else
        -- Get empty location near to NearLocationId
        select top 1 @LocationId = l.LocationId
          from Location              l (nolock)
          join AreaLocation         al (nolock) on l.LocationId = al.LocationId
          join StorageUnitLocation sul (nolock) on l.LocationId = sul.LocationId
         where al.AreaId         = @AreaId
           and l.Used            = 0 -- No
           and l.ActiveBinning   = 1 -- Yes
           and l.StockTakeInd    = 0 -- No 
           and l.LocationId     != isnull(@OldLocationId,'0')
           and sul.StorageUnitId = @StorageUnitId
        order by case when @RelativeValue > isnull(l.RelativeValue,0)
                      then @RelativeValue - isnull(l.RelativeValue,0)
                      else isnull(l.RelativeValue,0) - @RelativeValue
                      end
      
      if @LocationId is not null
        goto LocationFound
    end
  end
  else
  begin
    select @TrackBatchPF = dbo.ufn_Configuration(62, @warehouseId)
    select @AllowPartRK = dbo.ufn_Configuration(25, @warehouseId)
    
    if @TrackBatchPF = 1
    begin
      if @FillPFFirst = 1
        select top 1 @LocationId = l.LocationId
          from StorageUnitLocation       sul (nolock)
          join Location                    l (nolock) on sul.LocationId         = l.LocationId
          join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
          join Area                        a (nolock) on al.AreaId              = a.AreaId
          join StorageUnitBatch          sub (nolock) on sul.StorageUnitId      = sub.StorageUnitId
          left outer
          join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                                                     and l.LocationId           = subl.LocationId
         where l.ActiveBinning        = 1 -- Yes
           and isnull(a.AreaType,'')  = @AreaType
           and sub.StorageUnitBatchId = @StorageUnitBatchId
           and a.WarehouseId          = @WarehouseId
           and (isnull(subl.ActualQuantity + subl.AllocatedQuantity,0))              < isnull(sul.MinimumQuantity, @FullPallet)  -- Current Qty is less than minimum
           and (isnull(subl.ActualQuantity + subl.AllocatedQuantity,0)) + @Quantity <= isnull(sul.MaximumQuantity, @FullPallet)  -- Not Greater than maximum
           and                                                            @Quantity <= isnull(sul.HandlingQuantity, @FullPallet) -- Not Greater than Handling
           and @ShelfLifePecentage >= isnull(a.MinimumShelfLife, @ShelfLifePecentage)
        order by a.AreaCode
      
      -- If this a bulk product send it to the Pickface - because cannot fit into Racking at Prominent
      if @LocationId is null
      and exists (select top 1 1
                   from StorageUnitLocation sul
                   join AreaLocation         al on sul.LocationId = al.LocationId
                   join Area                  a on al.AreaId      = a.AreaId
                  where a.AreaCode            = 'BK'
                    and isnull(a.AreaType,'') = @AreaType
                    and sul.StorageUnitId     = @StorageUnitId
                    and a.WarehouseId         = @WarehouseId)
      begin
        -- Allow part pallets in racking is false, send to pickface regardless and make full pallet if required
        select top 1 @LocationId = l.LocationId
          from StorageUnitLocation       sul (nolock)
          join Location                    l (nolock) on sul.LocationId         = l.LocationId
          join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
          join Area                        a (nolock) on al.AreaId              = a.AreaId
          join StorageUnitBatch          sub (nolock) on sul.StorageUnitId      = sub.StorageUnitId
          left outer
          join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                                                     and l.LocationId           = subl.LocationId
         where l.ActiveBinning       = 1 -- Yes
           and isnull(a.AreaType,'') = @AreaType
           and sub.StorageUnitId     = @StorageUnitId
           and a.WarehouseId         = @WarehouseId
           and a.AreaCode            = 'PK'
           and @ShelfLifePecentage >= isnull(a.MinimumShelfLife, @ShelfLifePecentage)
        order by ActualQuantity - ReservedQuantity desc,
                 case when @RelativeValue > isnull(l.RelativeValue,0)
                      then @RelativeValue - isnull(l.RelativeValue,0)
                      else isnull(l.RelativeValue,0) - @RelativeValue
                      end
      end
      
      if @LocationId is null
      begin
        if exists(select 1 from viewSUL where StorageUnitId = @StorageUnitId and AreaCode = 'SP')
        begin
          select @OldLocation = Location
            from Location (nolock)
           where LocationId = @OldLocationId
          
          if @OldLocation is null
            set @OldLocation = '0'
          
          select top 1
                 @LocationId = LocationId
            from viewSUL
           where StorageUnitId = @StorageUnitId
             and Location      > @OldLocation
          order by Location
          
          if @@rowcount = 0
            select top 1
                   @LocationId = LocationId
              from viewSUL
             where StorageUnitId = @StorageUnitId
               and Location      < @OldLocation
            order by Location
        end
      end
      
      -- If no free pick-face send to any
      if dbo.ufn_Configuration(235, @WarehouseId) = 1
      begin
        if @LocationId is null
        begin
          select top 1 @LocationId = l.LocationId
            from StorageUnitLocation       sul (nolock)
            join Location                    l (nolock) on sul.LocationId         = l.LocationId
            join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
            join Area                        a (nolock) on al.AreaId              = a.AreaId
            join StorageUnitBatch          sub (nolock) on sul.StorageUnitId      = sub.StorageUnitId
            left outer
            join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                                                       and l.LocationId           = subl.LocationId
           where l.ActiveBinning       = 1 -- Yes
             and isnull(a.AreaType,'') = @AreaType
             and sub.StorageUnitId     = @StorageUnitId
             and a.WarehouseId         = @WarehouseId
             and a.AreaCode            = 'PK'
             and @ShelfLifePecentage >= isnull(a.MinimumShelfLife, @ShelfLifePecentage)
          order by ActualQuantity - ReservedQuantity desc,
                   case when @RelativeValue > isnull(l.RelativeValue,0)
                        then @RelativeValue - isnull(l.RelativeValue,0)
                        else isnull(l.RelativeValue,0) - @RelativeValue
                        end
        end
      end
      
      if @LocationId is null
        exec p_Location_Get_Dropme
         @WarehouseId        = @WarehouseId,
         @LocationId         = @LocationId output,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @Quantity           = @Quantity,
         @FullPalletInd      = 1,
         @AreaType           = @AreaType
      
      if @LocationId is not null
        goto LocationFound
    end
    
    -- Get location with least quantity and location near to NearLocationId
    if @AllowPartRK = 1
    begin
      if @FillPFFirst = 1
        -- Check if it fits into the pickface
        select top 1 @LocationId = l.LocationId
          from StorageUnitLocation       sul (nolock)
          join Location                    l (nolock) on sul.LocationId         = l.LocationId
          join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
          join Area                        a (nolock) on al.AreaId              = a.AreaId
          join StorageUnitBatch          sub (nolock) on sul.StorageUnitId      = sub.StorageUnitId
          left outer
          join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                                                     and l.LocationId           = subl.LocationId
         where l.ActiveBinning       = 1 -- Yes
           and isnull(a.AreaType,'') = @AreaType
           and sub.StorageUnitId     = @StorageUnitId
           and a.WarehouseId         = @WarehouseId
           and (subl.ActualQuantity + subl.AllocatedQuantity)              < isnull(sul.MinimumQuantity, @FullPallet)  -- Current Qty is less than minimum
           and (subl.ActualQuantity + subl.AllocatedQuantity) + @Quantity <= isnull(sul.MaximumQuantity, @FullPallet)  -- Not Greater than maximum
           and                                                  @Quantity <= isnull(sul.HandlingQuantity, @FullPallet) -- Not Greater than Handling
           and @ShelfLifePecentage >= isnull(a.MinimumShelfLife, @ShelfLifePecentage)
        order by a.AreaCode,
                 ActualQuantity - ReservedQuantity desc,
                 case when @RelativeValue > isnull(l.RelativeValue,0)
                      then @RelativeValue - isnull(l.RelativeValue,0)
                      else isnull(l.RelativeValue,0) - @RelativeValue
                      end
      -- If no free pick-face send to any
      if dbo.ufn_Configuration(235, @WarehouseId) = 1
      begin
        if @LocationId is null
        begin
          select top 1 @LocationId = l.LocationId
            from StorageUnitLocation       sul (nolock)
            join Location                    l (nolock) on sul.LocationId         = l.LocationId
            join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
            join Area                        a (nolock) on al.AreaId              = a.AreaId
            join StorageUnitBatch          sub (nolock) on sul.StorageUnitId      = sub.StorageUnitId
            left outer
            join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                                                       and l.LocationId           = subl.LocationId
           where l.ActiveBinning       = 1 -- Yes
             and isnull(a.AreaType,'') = @AreaType
             and sub.StorageUnitId     = @StorageUnitId
             and a.WarehouseId         = @WarehouseId
             and a.AreaCode            = 'PK'
             and @ShelfLifePecentage >= isnull(a.MinimumShelfLife, @ShelfLifePecentage)
          order by ActualQuantity - ReservedQuantity desc,
                   case when @RelativeValue > isnull(l.RelativeValue,0)
                        then @RelativeValue - isnull(l.RelativeValue,0)
                        else isnull(l.RelativeValue,0) - @RelativeValue
                        end
        end
      end
      
      if @LocationId is null
        exec p_Location_Get_Dropme
         @WarehouseId        = @WarehouseId,
         @LocationId         = @LocationId output,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @Quantity           = @Quantity,
         @FullPalletInd      = 1,
         @AreaType           = @AreaType
      
      if @LocationId is not null
        goto LocationFound
    end
    else
    begin
      -- Allow part pallets in racking is false, send to pickface regardless and make full pallet if required
      select top 1 @LocationId = l.LocationId
        from StorageUnitLocation       sul (nolock)
        join Location                    l (nolock) on sul.LocationId         = l.LocationId
        join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
        join Area                        a (nolock) on al.AreaId              = a.AreaId
        join StorageUnitBatch          sub (nolock) on sul.StorageUnitId      = sub.StorageUnitId
        left outer
        join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                                                   and l.LocationId           = subl.LocationId
       where l.ActiveBinning         = 1 -- Yes
           and isnull(a.AreaType,'') = @AreaType
         and sub.StorageUnitId       = @StorageUnitId
         and a.WarehouseId           = @WarehouseId
         and a.AreaCode              = 'PK'
         and @ShelfLifePecentage >= isnull(a.MinimumShelfLife, @ShelfLifePecentage)
      order by ActualQuantity - ReservedQuantity desc,
               case when @RelativeValue > isnull(l.RelativeValue,0)
                    then @RelativeValue - isnull(l.RelativeValue,0)
                    else isnull(l.RelativeValue,0) - @RelativeValue
                    end
    end
    
    if @LocationId is not null
      goto LocationFound
  end
  
  return
  
  LocationFound:
    if dbo.ufn_Configuration(131, @WarehouseId) = 1
    begin
      print 'Check if near to other product which it`s not allowed to be'
      
--      exec p_Location_Get_Dropme
--       @WarehouseId        = @WarehouseId,
--       @LocationId         = @LocationId output,
--       @StorageUnitBatchId = @StorageUnitBatchId,
--       @Quantity           = @Quantity,
--       @FullPalletInd      = 1
--      
--      if @LocationId is null
--        goto LocationFound
    end 
    
    if dbo.ufn_Configuration(130, @WarehouseId) = 1
    begin
      exec @Error = p_Location_Bulk_Indicators
       @StorageUnitId = @StorageUnitId,
       @LocationId    = @LocationId
    end
end
