﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Line_Insert
  ///   Filename       : p_Production_Line_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Line_Insert
(
 @warehouseId        int,
 @operatorId         int,
 @storageUnitBatchId int,
 @quantity           float,
 @receiptLineId      int = null
)

as
begin
	 set nocount on;
  
  declare @Error                 int,
          @Errormsg              nvarchar(500),
          @GetDate               datetime,
          @OrderNumber           nvarchar(20),
          @LineNumber            int,
          @InboundDocumentTypeId int,
          @InboundDocumentId     int,
          @InboundLineId         int,
          @StorageUnitId         int,
          @BatchId               int,
          @StatusId              int,
          @Weight                numeric(13,3),
          @PalletQuantity        numeric(13,3),
          @ExternalCompanyId     int,
          @PriorityId            int,
          @ActualYield           int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @BatchId       = BatchId,
         @StorageUnitId = StorageUnitId
    from StorageUnitBatch
   where StorageUnitBatchId = @storageUnitBatchId
  
  if (select dbo.ufn_Configuration(271, @WarehouseId)) = 0 -- Palletise Products for Repacking on separate pallets
  begin
    if @receiptLineId is null
    begin
      select @receiptLineId = max(rl.ReceiptLineId)
        from InboundDocument      id
        join InboundDocumentType idt on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
        join Receipt               r on id.InboundDocumentId     = r.InboundDocumentId
        join ReceiptLine          rl on r.ReceiptId              = rl.ReceiptId
       where id.StorageUnitId = @StorageUnitId
         and id.BatcHid       = @BatchId
         and case when idt.InboundDocumentTypeCode = 'PRV'
                  then datediff(dd, id.CreateDate, @getdate)
                  else 0
                  end >= 7
    end
  end
  
  begin transaction
  
  if @receiptLineId is null
  begin
    select @StatusId = dbo.ufn_StatusId('ID','W')
    
    set @OrderNumber = 'PRV' + convert(nvarchar(20), @Getdate, 120)
    
    select @InboundDocumentTypeId = InboundDocumentTypeId,
           @PriorityId            = PriorityId
      from InboundDocumentType
     where InboundDocumentTypeCode = 'PRV'
    
    exec @Error = p_InboundDocument_Insert
     @InboundDocumentId     = @InboundDocumentId output,
     @InboundDocumentTypeId = @InboundDocumentTypeId,
     @ExternalCompanyId     = @ExternalCompanyId,
     @StatusId              = @StatusId,
     @WarehouseId           = @WarehouseId,
     @OrderNumber           = @OrderNumber,
     @DeliveryDate          = @GetDate,
     @CreateDate            = @GetDate,
     @ModifiedDate          = null,
     @StorageUnitId         = @StorageUnitId,
     @BatchId               = @BatchId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_InboundDocument_Insert'
      goto error
    end
    
    select @LineNumber = count(1) from InboundLine where InboundDocumentId = @InboundDocumentId
    
    if @LineNumber is null
      set @LineNumber = 1
    else
      select @LineNumber = @LineNumber + 1
    
    exec @Error = p_InboundLine_Insert
     @InboundLineId     = @InboundLineId output,
     @InboundDocumentId = @InboundDocumentId,
     @StorageUnitId     = @StorageUnitId,
     @StatusId          = @StatusId,
     @LineNumber        = @LineNumber,
     @Quantity          = @Quantity,
     @BatchId           = @BatchId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_InboundLine_Insert'
      goto error
    end
    
    exec @Error = p_Receiving_Create_Receipt
     @InboundDocumentId = @InboundDocumentId,
     @InboundLineId     = @InboundLineId,
     @OperatorId        = null
           
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Receiving_Create_Receipt'
      goto error
    end
    
		  select @StatusId = dbo.ufn_StatusId('PR','W')
  		
    select @ReceiptLineId = ReceiptLineId
      from ReceiptLine
     where InboundLineId = @InboundLineId
		end
  
  select @PalletQuantity = p.Quantity
    from Pack      p (nolock)
    join PackType pt (nolock) on p.PackTypeId = pt.PackTypeId
   where p.StorageUnitId    = @StorageUnitId
     and p.WarehouseId      = @WarehouseId
     and pt.InboundSequence = 1
		
  if @PalletQuantity = 0 or @PalletQuantity is null
  begin
    select @ErrorMsg = 'The default pallet quantity cannot be zero.'
    goto error
  end
		
		select @ActualYield = isnull(ActualYield, 0)
		  from Batch
		 where BatchId = @BatchId
		
		select @ActualYield = @Quantity + @ActualYield
		
		exec p_Batch_Update
		 @BatchId = @BatchId,
		 @ActualYield = @ActualYield
  
  if @Error != 0
  begin
    select @ErrorMsg = 'Error executing p_Batch_Update'
    goto error
  end
		
		while floor(@Quantity/@PalletQuantity) > 0
		begin --loop
		  
		  exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @InstructionTypeCode = 'PR',
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = null,
     @StoreLocationId     = null,
     @Quantity            = @PalletQuantity,
     @Weight              = @Weight,
     @PriorityId          = @PriorityId,
     @ReceiptLineId       = @ReceiptLineId,
     @StatusId            = @StatusId
		  
		  if @error != 0
		  begin
      select @ErrorMsg = 'Error executing p_Palletised_Insert'
      goto error
		  end
		  
		  select @Quantity = @Quantity - @PalletQuantity
		END --loop
		 
		/***************************/
		/* Insert any spare units */
		/***************************/
		IF @Quantity > 0
		BEGIN --dil
		  
		  exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @InstructionTypeCode = 'PR',
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = null,
     @StoreLocationId     = null,
     @Quantity            = @Quantity,
     @Weight              = @Weight,
     @PriorityId          = @PriorityId,
     @ReceiptLineId       = @ReceiptLineId,
     @StatusId            = @StatusId
		  
		  if @error != 0
		  begin
      select @ErrorMsg = 'Error executing p_Palletised_Insert'
      goto error
		  end
		  
		end --dil
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
