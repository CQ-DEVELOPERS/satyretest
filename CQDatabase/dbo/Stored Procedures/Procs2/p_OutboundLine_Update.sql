﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundLine_Update
  ///   Filename       : p_OutboundLine_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jan 2014 07:56:00
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OutboundLine table.
  /// </remarks>
  /// <param>
  ///   @OutboundLineId int = null,
  ///   @OutboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null,
  ///   @BatchId int = null,
  ///   @UnitPrice float = null,
  ///   @UOM nvarchar(60) = null,
  ///   @BOELineNumber nvarchar(100) = null,
  ///   @TariffCode nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundLine_Update
(
 @OutboundLineId int = null,
 @OutboundDocumentId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @LineNumber int = null,
 @Quantity float = null,
 @BatchId int = null,
 @UnitPrice float = null,
 @UOM nvarchar(60) = null,
 @BOELineNumber nvarchar(100) = null,
 @TariffCode nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @OutboundLineId = '-1'
    set @OutboundLineId = null;
  
  if @OutboundDocumentId = '-1'
    set @OutboundDocumentId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
	 declare @Error int
 
  update OutboundLine
     set OutboundDocumentId = isnull(@OutboundDocumentId, OutboundDocumentId),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         StatusId = isnull(@StatusId, StatusId),
         LineNumber = isnull(@LineNumber, LineNumber),
         Quantity = isnull(@Quantity, Quantity),
         BatchId = isnull(@BatchId, BatchId),
         UnitPrice = isnull(@UnitPrice, UnitPrice),
         UOM = isnull(@UOM, UOM),
         BOELineNumber = isnull(@BOELineNumber, BOELineNumber),
         TariffCode = isnull(@TariffCode, TariffCode) 
   where OutboundLineId = @OutboundLineId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_OutboundLineHistory_Insert
         @OutboundLineId = @OutboundLineId,
         @OutboundDocumentId = @OutboundDocumentId,
         @StorageUnitId = @StorageUnitId,
         @StatusId = @StatusId,
         @LineNumber = @LineNumber,
         @Quantity = @Quantity,
         @BatchId = @BatchId,
         @UnitPrice = @UnitPrice,
         @UOM = @UOM,
         @BOELineNumber = @BOELineNumber,
         @TariffCode = @TariffCode,
         @CommandType = 'Update'
  
  return @Error
  
end
