﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupDocumentType_Update
  ///   Filename       : p_OperatorGroupDocumentType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:34:19
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the OperatorGroupDocumentType table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @OutboundDocumentTypeId int = null,
  ///   @PriorityId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupDocumentType_Update
(
 @OperatorGroupId int = null,
 @OutboundDocumentTypeId int = null,
 @PriorityId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update OperatorGroupDocumentType
     set OperatorGroupId = isnull(@OperatorGroupId, OperatorGroupId),
         OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, OutboundDocumentTypeId),
         PriorityId = isnull(@PriorityId, PriorityId) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
