﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Redelivery_Insert
  ///   Filename       : p_Receiving_Redelivery_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Redelivery_Insert
(
 @inboundShipmentId int,
 @receiptId         int,
 @newReceiptId      int = null output
)

as
begin
	 set nocount on;
	 
	 declare @TableReceipt as table
	 (
	  ReceiptId int
	 )
	 
	 declare @TableReceiptLine as table
	 (
	  InboundLineId      int,
   StorageUnitBatchId int,
   RequiredQuantity   numeric(13,6)
	 )
  
  declare @Error                 int,
          @Errormsg              nvarchar(500),
          @GetDate               datetime,
          @NewReceiptLineId      int,
          @NewRequiredQuantity   numeric(13,6),
          @InboundDocumentId     int,
          @PriorityId            int,
          @WarehouseId           int,
          @StatusId              int,
          @DeliveryNoteNumber    nvarchar(30),
          @InboundLineId         int,
          @StorageUnitBatchId    int,
          @Count                 int,
          @Delivery              int,
          @Tran                  bit = 1,
          @LocationId            int,
          @InboundDocumentTypeCode nvarchar(10),
          @ContainerNumber         nvarchar(50),
          @AdditionalText1         nvarchar(255),
          @AdditionalText2         nvarchar(255),
          @BOE                     nvarchar(255),
          @Incoterms               nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @receiptId = '-1'
    set @receiptId = null;
  
  if @inboundShipmentId in ('-1','0')
    set @inboundShipmentId = null
  
  if @inboundShipmentId is not null
    set @receiptId = null
  
  if @receiptId is null
    insert @TableReceipt
          (ReceiptId)
    select isr.ReceiptId
      from InboundShipmentReceipt isr (nolock)
      join Receipt                  r (nolock) on isr.ReceiptId = r.ReceiptId
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
      join InboundDocumentType    idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
     where isr.InboundShipmentId = @inboundShipmentId
       and idt.QuantityToFollowIndicator = 1
  else
    insert @TableReceipt
          (ReceiptId)
    select r.ReceiptId
      from Receipt                  r (nolock)
      join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
      join InboundDocumentType    idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
     where r.ReceiptId = @receiptId
       and idt.QuantityToFollowIndicator = 1
  
  if @@trancount > 0
    set @Tran = 0
  
  if @Tran = 1
    begin transaction
  
  if (select count(1) from @TableReceipt) = 0 
  begin
    goto Error
  end
  
  while exists(select top 1 1 from @TableReceipt)
  begin
    select @receiptId = ReceiptId
      from @TableReceipt
    
    delete @TableReceipt where ReceiptId = @receiptId
    
    select @InboundDocumentId     = InboundDocumentId,
           @PriorityId            = PriorityId,
           @WarehouseId           = WarehouseId,
           @DeliveryNoteNumber    = null,--DeliveryNoteNumber,
           @Delivery              = isnull(Delivery, 1) + 1,
           @ContainerNumber       = ContainerNumber,
           @AdditionalText1       = AdditionalText1,
           @AdditionalText2       = AdditionalText2,
           @BOE                   = BOE,
           @Incoterms             = Incoterms
      from Receipt
     where ReceiptId = @receiptId
    
    select @InboundDocumentTypeCode = idt.InboundDocumentTypeCode
      from InboundDocument      id (nolock)
      join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
     where id.InboundDocumentId = @InboundDocumentId
    
    set @count = 0
    
    if not exists(select 1
                    from Receipt
                   where InboundDocumentId = @InboundDocumentId
                     and Delivery = @Delivery)
    begin
      if @InboundDocumentTypeCode = 'BOND'
      begin
        insert Batch
              (StatusId,
               WarehouseId,
               Batch,
               CreateDate,
               ExpiryDate,
               ShelfLifeDays,
               BOELineNumber,
               BillOfEntry)
        select dbo.ufn_StatusId('B','A'),
               b.WarehouseId,
               b.BillOfEntry + b.BOELineNumber,
               max(b.CreateDate),
               min(b.ExpiryDate),
               b.ShelfLifeDays,
               b.BOELineNumber,
               b.BillOfEntry
          from ReceiptLine rl (nolock)
          join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
          join Batch              b (nolock) on sub.BatchId = b.BatchId
          left
          join Batch            old (nolock) on b.BillOfEntry + b.BOELineNumber = old.Batch
         where rl.ReceiptId = @receiptId
           and old.BatchId is null
        group by b.WarehouseId,
                 b.BillOfEntry + b.BOELineNumber,
                 b.ShelfLifeDays,
                 b.BOELineNumber,
                 b.BillOfEntry
        
        insert StorageUnitBatch
              (StorageUnitId,
               BatchId)
        select distinct sub.StorageUnitId,
               old.BatchId
          from ReceiptLine rl (nolock)
          join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
          join Batch              b (nolock) on sub.BatchId = b.BatchId
          join Batch            old (nolock) on b.BillOfEntry + b.BOELineNumber = old.Batch
          left
          join StorageUnitBatch oldsub (nolock) on sub.StorageUnitId = oldsub.StorageUnitId
                                               and old.BatchId = oldsub.BatchId
         where rl.ReceiptId = @receiptId
           and oldsub.StorageUnitBatchId is null
      end
      
      if (select dbo.ufn_Configuration(357, @WarehouseId)) = 1 -- Receiving Send Delivery Note Qty
        insert @TableReceiptLine
              (InboundLineId,
               StorageUnitBatchId,
               RequiredQuantity)
        select rl.InboundLineId,
               isnull(def.StorageUnitBatchId, sub.StorageUnitBatchId),
               sum(isnull(rl.RequiredQuantity,0)) - sum(isnull(rl.DeliveryNoteQuantity,0))
          from ReceiptLine rl (nolock)
          join InboundLine il (nolock) on rl.InboundLineId = il.InboundLineId
          join Receipt      r (nolock) on rl.ReceiptId = r.ReceiptId
          join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
          left
          join StorageUnitBatch def (nolock) on sub.StorageUnitId = def.StorageUnitId
                                            and def.BatchId = 1
          left
          join Batch              b (nolock) on def.BatchId = b.BatchId
                                            and case when 'BOND' = 'BOND'
                                                     then r.BOE + il.BOELineNumber
                                                     else 'Default'
                                                     end = b.Batch
         where rl.ReceiptId = @ReceiptId
        group by rl.InboundLineId,
                 isnull(def.StorageUnitBatchId, sub.StorageUnitBatchId)
      else
        insert @TableReceiptLine
              (InboundLineId,
               StorageUnitBatchId,
               RequiredQuantity)
        select rl.InboundLineId,
               isnull(def.StorageUnitBatchId, sub.StorageUnitBatchId),
               sum(isnull(rl.RequiredQuantity,0)) - sum(isnull(rl.ReceivedQuantity,0))
          from ReceiptLine rl (nolock)
          join InboundLine il (nolock) on rl.InboundLineId = il.InboundLineId
          join Receipt      r (nolock) on rl.ReceiptId = r.ReceiptId
          join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
          left
          join StorageUnitBatch def (nolock) on sub.StorageUnitId = def.StorageUnitId
                                            and def.BatchId = 1
          left
          join Batch              b (nolock) on def.BatchId = b.BatchId
                                            and case when 'BOND' = 'BOND'
                                                     then r.BOE + il.BOELineNumber
                                                     else 'Default'
                                                     end = b.Batch
         where rl.ReceiptId = @ReceiptId
        group by rl.InboundLineId,
                 isnull(def.StorageUnitBatchId, sub.StorageUnitBatchId)
      
      delete @TableReceiptLine where RequiredQuantity <= 0
      
      select @Count = COUNT(1) from @TableReceiptLine
      
      if isnull(@Count,0) < 1
      begin
        if @Tran = 1
          rollback transaction
        return 0
      end
      
      select @StatusId = dbo.ufn_StatusId('R', 'W')
      
      exec @LocationId = p_Receiving_Location_Get
       @WarehouseId             = @WarehouseId,
       @InboundDocumentTypeCode = @InboundDocumentTypeCode
      
      if @LocationId in (0,-1)
        set @LocationId = null
      
      exec @Error = p_Receipt_Insert
       @ReceiptId             = @NewReceiptId output,
       @InboundDocumentId     = @InboundDocumentId,
       @PriorityId            = @PriorityId,
       @WarehouseId           = @WarehouseId,
       @LocationId            = @LocationId,
       @StatusId              = @StatusId,
       @OperatorId            = null,
       @DeliveryNoteNumber    = @DeliveryNoteNumber,
       @DeliveryDate          = null,
       @SealNumber            = null,
       @VehicleRegistration   = null,
       @Remarks               = null,
       @CreditAdviceIndicator = null,
       @Delivery              = @Delivery,
       @ContainerNumber       = @ContainerNumber,
       @AdditionalText1       = @AdditionalText1,
       @AdditionalText2       = @AdditionalText2,
       @BOE                   = @BOE,
       @Incoterms             = @Incoterms
      
      if @Error <> 0
        goto error
    end
    
    while @Count > 0
    begin
      set @Count = @Count - 1
      
      select top 1 @InboundLineId       = InboundLineId,
                   @StorageUnitBatchId  = StorageUnitBatchId,
                   @NewRequiredQuantity = RequiredQuantity
        from @TableReceiptLine
      
      delete @TableReceiptLine
       where InboundLineId      = @InboundLineId
         and StorageUnitBatchId = @StorageUnitBatchId
      
      exec @Error = p_ReceiptLine_Insert
       @ReceiptLineId        = @NewReceiptLineId output,
       @ReceiptId            = @NewReceiptId,
       @InboundLineId        = @InboundLineId,
       @StorageUnitBatchId   = @StorageUnitBatchId,
       @StatusId             = @StatusId,
       @OperatorId           = null,
       @RequiredQuantity     = @NewRequiredQuantity,
       @ReceivedQuantity     = 0,
       @AcceptedQuantity     = null,
       @RejectQuantity       = null,
       @DeliveryNoteQuantity = null
      
      if @Error <> 0
        goto error
    end
    
    update ReceiptLine
       set RequiredQuantity = 0
     where ReceiptId = @receiptId
       and RequiredQuantity > 0
       and ReceivedQuantity = 0
  end
  
  update r
       set NumberOfLines = (select count(1)
                              from ReceiptLine rl (nolock)
                             where r.ReceiptId = rl.ReceiptId)
      from Receipt r
     where r.ReceiptId = @NewReceiptId
  
  if (select dbo.ufn_Configuration(397, @WarehouseId)) = 1 -- Receiving - Auto Complete ON REDELIVERY
  begin
    exec p_Inbound_Release
     @InboundShipmentId = @InboundShipmentId,
     @ReceiptId         = @ReceiptId,
     @StatusCode        = 'RC'
    
    if @Error <> 0
      goto error
  end
  
  if @Tran = 1
    commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Receiving_Redelivery_Insert'); 
    rollback transaction
    return @Error
end
