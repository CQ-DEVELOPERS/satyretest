﻿--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
/*
  /// <summary>
  ///   Procedure Name : p_Production_Stock_Adjustment
  ///   Filename       : p_Production_Stock_Adjustment.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Production_Stock_Adjustment
(
 @ReceiptId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @Display           nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()

  select @Display = 'Order: ' + id.OrderNumber
    from Receipt          r (nolock)
    join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
   where r.ReceiptId = @ReceiptId
  
  begin transaction
  
  insert InterfaceExportStockAdjustment
        (RecordType,
         RecordStatus,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight,
         Additional1,
         Additional3,
         Additional5) -- Only used for scripts
  select 'ADJ',
         'N',
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
         rl.AcceptedQuantity,
         null,
         'FG',
         @Display,
         'CQ FG Id: ' + convert(varchar(10), rl.ReceiptLineId)
    from ReceiptLine       rl (nolock)
    join Receipt            r (nolock) on rl.ReceiptId = r.ReceiptId
    join Status             s (nolock) on rl.StatusId = s.StatusId
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId = p.ProductId
    join SKU              sku (nolock) on su.SKUId = sku.SKUId
    join Batch              b (nolock) on sub.BatchId = b.BatchId
   where rl.ReceiptId = @ReceiptId
     and rl.AcceptedQuantity > 0
     and isnull(r.Interfaced, 0) = 0
     --and not exists(select 1 from InterfaceExportStockAdjustment sa (nolock) where Additional5 = 'CQ FG Id: ' + convert(varchar(10), rl.ReceiptLineId))
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Receipt_Update
   @ReceiptId     = @ReceiptId,
   @Interfaced    = 1
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Production_Stock_Adjustment'); 
    rollback transaction
    return @Error
end

