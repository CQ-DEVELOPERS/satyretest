﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OperatorGroupInstructionType_Search
  ///   Filename       : p_OperatorGroupInstructionType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:20:07
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the OperatorGroupInstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null output,
  ///   @OperatorGroupId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   OperatorGroupInstructionType.InstructionTypeId,
  ///   OperatorGroupInstructionType.OperatorGroupId,
  ///   OperatorGroupInstructionType.OrderBy 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OperatorGroupInstructionType_Search
(
 @InstructionTypeId int = null output,
 @OperatorGroupId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InstructionTypeId = '-1'
    set @InstructionTypeId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
 
  select
         OperatorGroupInstructionType.InstructionTypeId
        ,OperatorGroupInstructionType.OperatorGroupId
        ,OperatorGroupInstructionType.OrderBy
    from OperatorGroupInstructionType
   where isnull(OperatorGroupInstructionType.InstructionTypeId,'0')  = isnull(@InstructionTypeId, isnull(OperatorGroupInstructionType.InstructionTypeId,'0'))
     and isnull(OperatorGroupInstructionType.OperatorGroupId,'0')  = isnull(@OperatorGroupId, isnull(OperatorGroupInstructionType.OperatorGroupId,'0'))
  
end
