﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_QuestionAnswers_Search
  ///   Filename       : p_QuestionAnswers_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 May 2014 20:34:28
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the QuestionAnswers table.
  /// </remarks>
  /// <param>
  ///   @QuestionAnswersId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   QuestionAnswers.QuestionId,
  ///   QuestionAnswers.Answer,
  ///   QuestionAnswers.JobId,
  ///   QuestionAnswers.ReceiptId,
  ///   QuestionAnswers.OrderNumber,
  ///   QuestionAnswers.PalletId,
  ///   QuestionAnswers.DateAsked,
  ///   QuestionAnswers.QuestionAnswersId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_QuestionAnswers_Search
(
 @QuestionAnswersId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @QuestionAnswersId = '-1'
    set @QuestionAnswersId = null;
  
 
  select
         QuestionAnswers.QuestionId
        ,QuestionAnswers.Answer
        ,QuestionAnswers.JobId
        ,QuestionAnswers.ReceiptId
        ,QuestionAnswers.OrderNumber
        ,QuestionAnswers.PalletId
        ,QuestionAnswers.DateAsked
        ,QuestionAnswers.QuestionAnswersId
    from QuestionAnswers
   where isnull(QuestionAnswers.QuestionAnswersId,'0')  = isnull(@QuestionAnswersId, isnull(QuestionAnswers.QuestionAnswersId,'0'))
  order by Answer
  
end
