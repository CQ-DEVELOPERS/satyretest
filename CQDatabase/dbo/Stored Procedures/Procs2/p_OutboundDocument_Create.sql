﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocument_Create
  ///   Filename       : p_OutboundDocument_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 July 2007
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundDocument table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null output,
  ///   @OutboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber nvarchar(30) = null,
  ///   @DeliveryDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null 
  /// </param>
  /// <returns>
  ///   @Error int
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocument_Create
(
 @OutboundDocumentId     int = null output,
 @OutboundDocumentTypeId int = null,
 @PrincipalId            int = null,
 @ExternalCompanyId      int = null,
 @StatusId               int = null,
 @WarehouseId            int = null,
 @OrderNumber            nvarchar(30) = null,
 @DeliveryDate           datetime = null,
 @CreateDate             datetime = null,
 @ModifiedDate           datetime = null,
 @ReferenceNumber        nvarchar(30) = null,
 @OperatorId			 int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  set @Error = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @ExternalCompanyId = -1
    set @ExternalCompanyId = null
  
  if @PrincipalId in (-1,0)
     set @PrincipalId = null;

  if @ExternalCompanyId is null
  begin
    select @ExternalCompanyId = od.ExternalCompanyId
      from OutboundDocument      od
      join OutboundDocumentType odt on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
     where od.OrderNumber             = @OrderNumber
       and odt.OutboundDocumentTypeCode != 'RET'
  end
  
  if exists(select OrderNumber
              from OutboundDocument      od
              join OutboundDocumentType odt on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
             where od.ExternalCompanyId      = @ExternalCompanyId
               and od.OrderNumber            = @OrderNumber
               and odt.OutboundDocumentTypeId = @OutboundDocumentTypeId)
  begin
    RAISERROR (900000,-1,-1, 'p_OutboundDocument_Create - Duplicate Order Number / Customer'); 
    return -1
  end
  
  if @OrderNumber is null
    set @OrderNumber = ''
  
  if @StatusId is null
    select @StatusId = StatusId
      from Status (nolock)
     where StatusCode = 'N'
       and Type       = 'OD'
  
  if @CreateDate is null
    set @CreateDate = @GetDate
  
  begin transaction
  
  exec @Error = p_OutboundDocument_Insert
   @OutboundDocumentId     = @OutboundDocumentId output,
   @OutboundDocumentTypeId = @OutboundDocumentTypeId,
   @PrincipalId            = @PrincipalId,
   @ExternalCompanyId      = @ExternalCompanyId,
   @StatusId               = @StatusId,
   @WarehouseId            = @WarehouseId,
   @OrderNumber            = @OrderNumber,
   @DeliveryDate           = @DeliveryDate,
   @CreateDate             = @CreateDate,
   @ModifiedDate           = @ModifiedDate,
   @ReferenceNumber        = @ReferenceNumber,
   @OperatorId			   = @OperatorId
  
  if @Error <> 0
    goto error
  
  if @OrderNumber = ''
  begin
    set @OrderNumber = CONVERT(nvarchar(30), @OutboundDocumentId)
    
    exec @Error = p_OutboundDocument_Update
     @OutboundDocumentId = @OutboundDocumentId,
     @OrderNumber        = @OrderNumber
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return @Error
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_OutboundDocument_Create'); 
    rollback transaction
    return @Error
end
