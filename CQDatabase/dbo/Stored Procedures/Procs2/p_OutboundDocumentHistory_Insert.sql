﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_OutboundDocumentHistory_Insert
  ///   Filename       : p_OutboundDocumentHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 May 2014 07:31:30
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the OutboundDocumentHistory table.
  /// </remarks>
  /// <param>
  ///   @OutboundDocumentId int = null,
  ///   @OutboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @FromLocation nvarchar(20) = null,
  ///   @ToLocation nvarchar(20) = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @Quantity float = null,
  ///   @PrincipalId int = null,
  ///   @OperatorId int = null,
  ///   @ExternalOrderNumber nvarchar(60) = null,
  ///   @DirectDeliveryCustomerId int = null,
  ///   @Collector nvarchar(100) = null,
  ///   @EmployeeCode nvarchar(100) = null,
  ///   @AdditionalText2 nvarchar(510) = null,
  ///   @VendorCode nvarchar(60) = null,
  ///   @CustomerOrderNumber nvarchar(60) = null,
  ///   @AdditionalText1 nvarchar(510) = null,
  ///   @BOE nvarchar(510) = null,
  ///   @Address1 nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   OutboundDocumentHistory.OutboundDocumentId,
  ///   OutboundDocumentHistory.OutboundDocumentTypeId,
  ///   OutboundDocumentHistory.ExternalCompanyId,
  ///   OutboundDocumentHistory.StatusId,
  ///   OutboundDocumentHistory.WarehouseId,
  ///   OutboundDocumentHistory.OrderNumber,
  ///   OutboundDocumentHistory.DeliveryDate,
  ///   OutboundDocumentHistory.CreateDate,
  ///   OutboundDocumentHistory.ModifiedDate,
  ///   OutboundDocumentHistory.CommandType,
  ///   OutboundDocumentHistory.InsertDate,
  ///   OutboundDocumentHistory.ReferenceNumber,
  ///   OutboundDocumentHistory.FromLocation,
  ///   OutboundDocumentHistory.ToLocation,
  ///   OutboundDocumentHistory.StorageUnitId,
  ///   OutboundDocumentHistory.BatchId,
  ///   OutboundDocumentHistory.Quantity,
  ///   OutboundDocumentHistory.PrincipalId,
  ///   OutboundDocumentHistory.OperatorId,
  ///   OutboundDocumentHistory.ExternalOrderNumber,
  ///   OutboundDocumentHistory.DirectDeliveryCustomerId,
  ///   OutboundDocumentHistory.Collector,
  ///   OutboundDocumentHistory.EmployeeCode,
  ///   OutboundDocumentHistory.AdditionalText2,
  ///   OutboundDocumentHistory.VendorCode,
  ///   OutboundDocumentHistory.CustomerOrderNumber,
  ///   OutboundDocumentHistory.AdditionalText1,
  ///   OutboundDocumentHistory.BOE,
  ///   OutboundDocumentHistory.Address1 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_OutboundDocumentHistory_Insert
(
 @OutboundDocumentId int = null,
 @OutboundDocumentTypeId int = null,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @OrderNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @ReferenceNumber nvarchar(60) = null,
 @FromLocation nvarchar(20) = null,
 @ToLocation nvarchar(20) = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @Quantity float = null,
 @PrincipalId int = null,
 @OperatorId int = null,
 @ExternalOrderNumber nvarchar(60) = null,
 @DirectDeliveryCustomerId int = null,
 @Collector nvarchar(100) = null,
 @EmployeeCode nvarchar(100) = null,
 @AdditionalText2 nvarchar(510) = null,
 @VendorCode nvarchar(60) = null,
 @CustomerOrderNumber nvarchar(60) = null,
 @AdditionalText1 nvarchar(510) = null,
 @BOE nvarchar(510) = null,
 @Address1 nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert OutboundDocumentHistory
        (OutboundDocumentId,
         OutboundDocumentTypeId,
         ExternalCompanyId,
         StatusId,
         WarehouseId,
         OrderNumber,
         DeliveryDate,
         CreateDate,
         ModifiedDate,
         CommandType,
         InsertDate,
         ReferenceNumber,
         FromLocation,
         ToLocation,
         StorageUnitId,
         BatchId,
         Quantity,
         PrincipalId,
         OperatorId,
         ExternalOrderNumber,
         DirectDeliveryCustomerId,
         Collector,
         EmployeeCode,
         AdditionalText2,
         VendorCode,
         CustomerOrderNumber,
         AdditionalText1,
         BOE,
         Address1)
  select @OutboundDocumentId,
         @OutboundDocumentTypeId,
         @ExternalCompanyId,
         @StatusId,
         @WarehouseId,
         @OrderNumber,
         @DeliveryDate,
         @CreateDate,
         @ModifiedDate,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @ReferenceNumber,
         @FromLocation,
         @ToLocation,
         @StorageUnitId,
         @BatchId,
         @Quantity,
         @PrincipalId,
         @OperatorId,
         @ExternalOrderNumber,
         @DirectDeliveryCustomerId,
         @Collector,
         @EmployeeCode,
         @AdditionalText2,
         @VendorCode,
         @CustomerOrderNumber,
         @AdditionalText1,
         @BOE,
         @Address1 
  
  select @Error = @@Error
  
  
  return @Error
  
end
