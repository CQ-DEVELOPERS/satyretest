﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Issue_Update_Status
  ///   Filename       : p_Issue_Update_Status.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Issue_Update_Status
(
 @IssueId int,
 @StatusId  int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if (select count(distinct(StatusId)) from Issue where IssueId = @IssueId) = 1
  begin
    begin transaction
    
    exec @Error = p_Issue_Update
     @IssueId = @IssueId,
     @StatusId  = @StatusId
    
    if @error <> 0
      goto error
    
    if @Error <> 0
      goto error
    
    commit transaction
  end
  
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Issue_Update_Status'); 
    rollback transaction
    return @Error
end
