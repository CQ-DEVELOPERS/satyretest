﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Register_SerialNumber_Linked
  ///   Filename       : p_Register_SerialNumber_Linked.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 03 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Register_SerialNumber_Linked
(
 @InstructionId int
)

as
begin
	 set nocount on;
  
  select i.InstructionId,
         sn.SerialNumberId,
         sn.SerialNumber
    from SerialNumber sn (nolock) 
    join Instruction  i  (nolock) on sn.StoreInstructionId = i.InstructionId
   where i.InstructionId = @InstructionId
end
