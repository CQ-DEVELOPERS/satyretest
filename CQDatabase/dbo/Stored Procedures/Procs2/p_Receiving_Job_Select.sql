﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Receiving_Job_Select
  ///   Filename       : p_Receiving_Job_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Receiving_Job_Select
(
 @barcode nvarchar(30)
)

as
begin
	 set nocount on;
	 
	 declare @JobId              int,
          @PalletId           int
  
  if isnumeric(replace(@barcode,'J:','')) = 1
    select @JobId   = replace(@barcode,'J:',''),
           @barcode = null
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  if @PalletId is not null
    select @JobId = JobId
      from Instruction      i (nolock)
      join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
     where PalletId = @PalletId
      and it.InstructionTypeCode in ('PR','S','SM')
  
  if @barcode is not null
  begin
    select @JobId = JobId
      from Job (nolock)
     where ReferenceNumber = @barcode
  end
  
  if @JobId is null
    goto result
  
  declare @TableHeader as table
  (
   OrderNumber       nvarchar(30),
   InboundShipmentId int null,
   ReceiptId         int,
   ReceiptLineId     int
  )
  
  declare @TableResult as table
  (
   InstructionId      int,
   ReceiptLineId      int,
   StorageUnitBatchId int,
   ProductCode	       nvarchar(30) null,
   Product	           nvarchar(50) null,
   SKUCode            nvarchar(50) null,
   Quantity           float null,
   JobId              int null,
   StatusId           int null,
   Status             nvarchar(50) null,
   PickLocationId     int null,
   PickLocation       nvarchar(15) null,
   StoreLocationId    int null,
   StoreLocation      nvarchar(15) null,
   PalletId           int null,
   CreateDate         datetime null,
   Batch              nvarchar(50) null,
   ECLNumber          nvarchar(10) null,
   InstructionType	   nvarchar(30),
   OperatorId         int null,
   Operator           nvarchar(50) null,
   Dwell              nvarchar(50) null,
   StockLevel         nvarchar(50) null
  )

  insert @TableResult
        (InstructionId,
         ReceiptLineId,
         StorageUnitBatchId,
         Quantity,
         JobId,
         StatusId,
         PickLocationId,
         StoreLocationId,
         PalletId,
         CreateDate,
         InstructionType,
         OperatorId)
  select i.InstructionId,
         i.ReceiptLineId,
         i.StorageUnitBatchId,
         isnull(i.ConfirmedQuantity,i.Quantity) as Quantity,
         i.JobId,
         i.StatusId,
         i.PickLocationId,
         i.StoreLocationId,
         i.PalletId,
         i.CreateDate,
         it.InstructionType,
         i.OperatorId
    from Instruction         i   (nolock)
    join Job                 j   (nolock) on i.JobId             = j.JobId
    join InstructionType     it  (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status              s   (nolock) on j.StatusId          = s.StatusId
   where it.InstructionTypeCode in ('S', 'SM','PR')
     and s.Type                 in ('PR','R')
     and s.StatusCode           in ('PR','W')--,'A')
     and i.JobId                 = isnull(@JobId, i.JobId)
  
  insert @TableHeader
        (OrderNumber,
         InboundShipmentId,
         ReceiptId,
         ReceiptLineId)
  select distinct
         id.OrderNumber,
         isr.InboundShipmentId,
         r.ReceiptId,
         rl.ReceiptLineId
    from InboundDocument         id (nolock)
    join Receipt                  r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join ReceiptLine             rl (nolock) on r.ReceiptId          = rl.ReceiptId
    left outer
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId          = isr.ReceiptId
    join @TableResult            tr          on rl.ReceiptLineId     = tr.ReceiptLineId
  
  update tr
     set ProductCode   = p.ProductCode,
         Product       = p.Product,
         SKUCode       = sku.SKUCode,
         Batch         = b.Batch,
         ECLNumber     = b.ECLNumber
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit         su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId         = p.ProductId
    join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
    join Batch               b   (nolock) on sub.BatchId          = b.BatchId

  update tr
     set Status = s.Status
    from @TableResult tr
    join Status       s  (nolock) on tr.StatusId = s.StatusId

  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId

  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId

  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator     o  (nolock) on tr.OperatorId = o.OperatorId
  
  update @TableResult
     set Dwell = 'RedFlag.gif'
   where datediff(hh, CreateDate, GetDate()) > 24
  
  update @TableResult
     set Dwell = 'GreenFlag.gif'
   where datediff(hh, CreateDate, GetDate()) < 6
     and Dwell is null
  
  update @TableResult
     set Dwell = 'YellowFlag.gif'
   where datediff(hh, CreateDate, GetDate()) > 6
     and Dwell is null
  
  update @TableResult
     set StockLevel = 'RedFlag.gif'
   where datediff(hh, CreateDate, GetDate()) > 24
  
  update @TableResult
     set StockLevel = 'GreenFlag.gif'
   where datediff(hh, CreateDate, GetDate()) < 6
     and StockLevel is null
  
  update @TableResult
     set StockLevel = 'YellowFlag.gif'
   where datediff(hh, CreateDate, GetDate()) > 6
     and StockLevel is null
  
  result:
    select InstructionId,
           ProductCode,
           Product,
           SKUCode,
           Quantity,
           JobId,
           Status,
           PickLocation,
           StoreLocation,
           PalletId,
           CreateDate,
           Batch,
           ECLNumber,
           OrderNumber,
           InboundShipmentId,
           InstructionType,
           Operator,
           Dwell,
           StockLevel
      from @TableResult tr 
      left outer
      join @TableHeader th on th.ReceiptLineId = tr.ReceiptLineId
end
