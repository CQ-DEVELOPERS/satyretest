﻿
/*
 /// <summary>
 ///   Procedure Name : p_Mobile_Stock_Take_Count
 ///   Filename       : p_Mobile_Stock_Take_Count.sql
 ///   Create By      : Grant Schultz
 ///   Date Created   : 12 Sep 2007
 /// </summary>
 /// <remarks>
 ///   
 /// </remarks>
 /// <param>
 ///   
 /// </param>
 /// <returns>
 ///   
 /// </returns>
 /// <newpara>
 ///   Modified by    : 
 ///   Modified Date  : 
 ///   Details        : 
 /// </newpara>
*/
CREATE procedure p_Mobile_Stock_Take_Count
(
@type               int,
@operatorId         int,
@jobId              int,
@barcode            nvarchar(50) = null,
@batch              nvarchar(50) = null,
@locationId         int = null,
@storageUnitBatchId int = null,
@quantity           float = null,
@weight             float = null
)

as
begin
	 set nocount on;

 declare @Error                 int,
         @Errormsg              nvarchar(500),
         @GetDate               datetime,
         @WarehouseId           int,
         @Location              nvarchar(150),
         @SecurityCode          int,
         @InstructionId         int,
         @PalletId              int,
         @rowcount              int,
         @StatusId              int,
         @InstructionTypeId     int,
         @ConfirmedQuantity     float,
         @StatusCode            nvarchar(10),
         @CurrentLine           int,
	        @TotalLines            int,
	        @AreaCode              nvarchar(10),
         @Stored                int,
         @DefaultSUBId          int,
         @StorageUnitId         int,
         @NewStorageUnitBatchId int,
         @ProductCode           nvarchar(50),
         @Product               nvarchar(255),
         @SKUCode               nvarchar(50),
         @Remarks               nvarchar(max),
         @InstrStatusCode       nvarchar(10),
         @Exception				nvarchar(255)

 select @GetDate = dbo.ufn_Getdate()
 
 INSERT INTO ProcLog
           ([Type]
           ,[OperatorId]
           ,[JobId]
           ,[Barcode]
           ,[Batch]
           ,[LocationId]
           ,[StorageUnitBatchId]
           ,[Quantity]
           ,[Weight]
           ,[CreateDate])
     VALUES
           (@type
           ,@operatorId
           ,@jobId
           ,@barcode
           ,@batch
           ,@locationId
           ,@storageUnitBatchId
           ,@quantity
           ,@weight
           ,@GetDate)

	 select @batch = replace(@batch,'A:','')
  select @batch = replace(@batch,'Q:','')
  select @batch = replace(@batch,'QC:','')
  
  select @NewStorageUnitBatchId = StorageUnitBatchId
    from StorageUnitBatch sub (nolock)
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId      = p.ProductId
    join SKU              sku (nolock) on su.SKUId          = sku.SKUId
    join Batch              b (nolock) on sub.BatchId       = b.BatchId
   where (p.ProductCode = @barcode or sku.SKUCode = @barcode)
     and Batch       = @batch
  
  if @NewStorageUnitBatchId != @storageUnitBatchId
    set @storageUnitBatchId = null
 
 if @StorageUnitBatchId = -1
   set @StorageUnitBatchId = null

 set @Error = 0

 select @WarehouseId = WarehouseId
   from Operator
  where OperatorId = @operatorId

 begin transaction

 if @type = 0 -- Get next location
 begin
   if(select isnull(OperatorId,@OperatorId) from Job where JobId = @JobId) = @OperatorId
   begin
     exec @Error = p_Job_Update
      @JobId      = @JobId,
      @OperatorId = @OperatorId

     if @Error <> 0
       goto error

     select top 1
            @locationId         = i.PickLocationId,
            @StorageUnitBatchId = i.StorageUnitBatchId,
            @InstructionId      = i.InstructionId,
            @Remarks            = case when i.InstructionRefId is null
                                       then null
                                       else 'Recount item: '
                                       end
       from Instruction i (nolock)
       join Status      s (nolock) on i.StatusId = s.StatusId
       join Location    l (nolock) on i.PickLocationId = l.LocationId
      where i.JobId      = @jobId
        and s.Type       = 'I'
        and s.StatusCode in ('W','S')
     -- Should be ordered by selection on create screen
     --order by i.InstructionId
     order by l.RelativeValue, l.Ailse, l.[Column], l.[Level], i.StorageUnitBatchId
     --   and i.PickLocationId <> isnull(@locationId, i.PickLocationId)

     if @Remarks is not null
       select @ProductCode = p.ProductCode,
              @Product     = p.Product,
              @SKUCode     = sku.SKUCode
         from StorageUnitBatch sub (nolock)
         join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
         join Product            p (nolock) on su.ProductId = p.ProductId
         join SKU              sku (nolock) on su.SKUId = sku.SKUId

     --if(select ReferenceNumber from Job where JobId = @jobId) like 'Mixed Pick%'
     --  if @StorageUnitBatchId is null -- GS 2012-08-31 Was causing wonrg product to be counted in location
         set @StorageUnitBatchId = -1

     if @locationId is not null
       select @Location     = Location,
              @SecurityCode = SecurityCode
         from Location
        where LocationId = @locationId

--      select @Location = @Location + ' ' + ProductCode
--        from StorageUnitBatch sub
--        join StorageUnit       su on sub.StorageUnitId = su.StorageUnitId
--        join Product            p on su.ProductId      = p.ProductId
--       where sub.StorageUnitBatchId = @StorageUnitBatchId

     exec @Error = p_Instruction_Started
      @InstructionId = @InstructionId,
      @OperatorId    = @operatorId

     if @Error <> 0
       goto error
   end
   else
   begin
     set @Error = -1
     goto result
   end
 end

 if @type in (1,3) -- Confirm Count / Confirm Product
 begin
   select @AreaCode = a.AreaCode
     from Area          a (nolock)
     join AreaLocation al (nolock) on a.AreaId      = al.AreaId
     join Location      l (nolock) on al.LocationId = l.LocationId
    where l.LocationId = @locationId

   if @Barcode like 'P:%'
   begin
     if isnumeric(replace(@Barcode,'P:','')) = 1
     begin
       set @StorageUnitBatchId = null

       select @PalletId = replace(@Barcode,'P:',''),
              @barcode  = null

       select @StorageUnitBatchId = sub.StorageUnitBatchId,
              @StorageUnitId      = sub.StorageUnitId
         from Pallet             p (nolock)
         join StorageUnitBatch sub (nolock) on p.StorageUnitBatchId = sub.StorageUnitBatchId
        where PalletId = @PalletId

       if @AreaCode = 'BK'
       begin
         select @StorageUnitBatchId = StorageUnitBatchId
           from StorageUnitBatch sub (nolock)
           join Batch              b (nolock) on sub.BatchId = b.BatchId
          where sub.StorageUnitId = @StorageUnitId
            and b.Batch = 'Default'
       end
     end
   end

   if @StorageUnitBatchId is null
   begin
     if @batch = ''
       set @batch = 'Default'

     exec @StorageUnitBatchId = p_Mobile_Stock_Take_Confirm_Product
      @barcode    = @barcode,
      @batch      = @batch,
      @locationId = @locationId
     
     if @StorageUnitBatchId = -1
     begin
       set @StorageUnitBatchId = null
       set @Error = -1
     end
     else
     begin
       if dbo.ufn_Configuration(34, @WarehouseId) = 0
       begin
         select @StorageUnitId = StorageUnitId
           from StorageUnitBatch (nolock)
          where StorageUnitBatchId = @StorageUnitBatchId

         select top 1 @StorageUnitBatchId = subl.StorageUnitBatchId
           from StorageUnitBatchLocation subl (nolock)
           join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
          where sub.StorageUnitId = @StorageUnitId
            and subl.LocationId   = @LocationId
         order by subl.ActualQuantity desc
       end
     end
   end

   if @StorageUnitBatchId is not null
   begin

     declare @TableProducts as table
     (
      InstructionId      int,
      InstructionTypeId  int,
      StorageUnitBatchId int,
      ProductCode        nvarchar(30),
      ProductBarcode     nvarchar(50),
      PackBarcode        nvarchar(50),
      ConfirmedQuantity  float
     )

     if @quantity > 0 and @AreaCode = 'BK'
       if dbo.ufn_Configuration(158, @WarehouseId) = 1
         select @quantity = @quantity * p.Quantity
           from Pack               p (nolock)
           join PackType          pt (nolock) on p.PackTypeId    = pt.PackTypeId
           join StorageUnitBatch sub (nolock) on p.StorageUnitId = sub.StorageUnitId
          where p.WarehouseId          = @WarehouseId
            and sub.StorageUnitBatchId = @StorageUnitBatchId
            and pt.InboundSequence     = 1

     if @quantity > 0 and @AreaCode = 'BK'
       if dbo.ufn_Configuration(246, @WarehouseId) = 1
         select @quantity = @quantity * p.Quantity
           from Pack               p (nolock)
           join PackType          pt (nolock) on p.PackTypeId    = pt.PackTypeId
           join StorageUnitBatch sub (nolock) on p.StorageUnitId = sub.StorageUnitId
          where p.WarehouseId          = @WarehouseId
            and sub.StorageUnitBatchId = @StorageUnitBatchId
            and pt.InboundSequence     > 1
            and p.Quantity             > 1

     if @quantity > 0 and @AreaCode = 'RK'
       if dbo.ufn_Configuration(247, @WarehouseId) = 1
         select @quantity = @quantity * p.Quantity
           from Pack               p (nolock)
           join PackType          pt (nolock) on p.PackTypeId    = pt.PackTypeId
           join StorageUnitBatch sub (nolock) on p.StorageUnitId = sub.StorageUnitId
          where p.WarehouseId          = @WarehouseId
            and sub.StorageUnitBatchId = @StorageUnitBatchId
            and pt.InboundSequence     > 1
            and p.Quantity             > 1
     
--      if @AreaCode in ('PK','BK')
     if @AreaCode in ('PK','BK','SP') and @type = 3
     begin
       insert @TableProducts
             (InstructionId,
              InstructionTypeId,
              StorageUnitBatchId,
              ConfirmedQuantity)
       select InstructionId,
              InstructionTypeId,
              StorageUnitBatchId,
              ConfirmedQuantity
         from Instruction
        where JobId          = @jobId
          and PickLocationId = @locationId
          and StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId)

       select @rowcount = @@rowcount

       if @rowcount = 0 -- Check if it's the same product but different batch
       begin
         select top 1 @DefaultSUBId = StorageUnitBatchId
           from StorageUnitLocation sul (nolock)
           join StorageUnitBatch    sub (nolock) on sul.StorageUnitId = sub.StorageUnitId
           join Batch                 b (nolock) on sub.BatchId       = b.BatchId
          where b.Batch        = 'Default'
            and sul.LocationId = @locationId

         insert @TableProducts
               (InstructionId,
                InstructionTypeId,
                StorageUnitBatchId,
                ConfirmedQuantity)
         select i.InstructionId,
                i.InstructionTypeId,
                sub2.StorageUnitBatchId,
                i.ConfirmedQuantity
           from Instruction         i (nolock)
           join StorageUnitBatch  sub (nolock) on isnull(i.StorageUnitBatchId,@DefaultSUBId)  = sub.StorageUnitBatchId
           join StorageUnitBatch sub2 (nolock) on sub.StorageUnitId   = sub2.StorageUnitId
--                                      and sub.BatchId        != sub2.BatchId
          where JobId          = @jobId
            and PickLocationId = @locationId
            and sub2.StorageUnitBatchId = isnull(@StorageUnitBatchId, sub2.StorageUnitBatchId)
            
         select @rowcount = @@rowcount
/*--------------------------------------------------------------------------------------------------------------------------------------*/
         if @rowcount = 0
         begin
           insert @TableProducts
                 (InstructionId,
                  InstructionTypeId,
                  StorageUnitBatchId,
                  ConfirmedQuantity)
           select i.InstructionId,
                  i.InstructionTypeId,
                  @StorageUnitBatchId,
                  i.ConfirmedQuantity
             from Instruction            i (nolock)
             join StorageUnitBatch     sub (nolock) on sub.StorageUnitBatchId = @StorageUnitBatchId
             join StorageUnitLocation  sul (nolock) on sub.StorageUnitId = sul.StorageUnitId
                                                   and i.PickLocationId = sul.LocationId
            where JobId          = @jobId
              and PickLocationId = @locationId

           select @rowcount = @@rowcount
         end

         if @rowcount = 0
         begin
           insert @TableProducts
                 (InstructionId,
                  InstructionTypeId,
                  StorageUnitBatchId,
                  ConfirmedQuantity)
           select i.InstructionId,
                  i.InstructionTypeId,
                  @StorageUnitBatchId,
                  i.ConfirmedQuantity
             from Instruction            i (nolock)
             join AreaLocation          al (nolock) on i.PickLocationId       = al.LocationId
             join StorageUnitBatch     sub (nolock) on sub.StorageUnitBatchId = @StorageUnitBatchId
             join StorageUnitArea      sua (nolock) on sub.StorageUnitId      = sua.StorageUnitId
                                          and al.AreaId              = sua.AreaId
            where JobId          = @jobId
              and PickLocationId = @locationId

           select @rowcount = @@rowcount
         end
         
         if @rowcount = 0
         begin
           insert @TableProducts
                 (InstructionId,
                  InstructionTypeId,
                  StorageUnitBatchId,
                  ConfirmedQuantity)
           select i.InstructionId,
                  i.InstructionTypeId,
                  @StorageUnitBatchId,
                  i.ConfirmedQuantity
             from Instruction i (nolock)
            where JobId          = @jobId
              and PickLocationId = @locationId
              and not exists(select top 1 1
                               from StorageUnitLocation subl
                              where i.PickLocationId = subl.LocationId)

           select @rowcount = @@rowcount
         end
/*--------------------------------------------------------------------------------------------------------------------------------------*/
       end
     end
     else
     begin -- Must be same batch for other locations
       insert @TableProducts
             (InstructionId,
              InstructionTypeId,
              StorageUnitBatchId,
              ConfirmedQuantity)
       select InstructionId,
              InstructionTypeId,
              StorageUnitBatchId,
              ConfirmedQuantity
         from Instruction
        where JobId          = @jobId
          and PickLocationId = @locationId
          and StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId)

       select @rowcount = @@rowcount
     end

     if exists(select 1 from @TableProducts where isnull(StorageUnitBatchId,'-1') = isnull(@StorageUnitBatchId,'-1'))
     begin
       select top 1 @InstructionId     = InstructionId,
                    @InstructionTypeId = InstructionTypeId
         from @TableProducts
        where isnull(StorageUnitBatchId,'-1') = isnull(@StorageUnitBatchId,'-1')

       if @type = 1
       begin
         exec @Error = p_Instruction_Started
          @InstructionId = @InstructionId,
          @OperatorId    = @operatorId

         if @Error <> 0
           goto error
--          if @AreaCode = 'BK'
--          begin
--            exec @Error = p_Instruction_Update
--             @InstructionId       = @InstructionId,
--             @StorageUnitBatchId  = @DefaultSUBId,
--             @OperatorId          = @operatorId,
--             @ConfirmedQuantity   = @quantity,
--             @Weight              = 0,
--             @ConfirmedWeight     = 0,
--             @PalletId            = @PalletId
--            
--            if @Error <> 0
--              goto error
--          end
--          else
         begin
           exec @Error = p_Instruction_Update
            @InstructionId       = @InstructionId,
            @StorageUnitBatchId  = @StorageUnitBatchId,
            @OperatorId          = @operatorId,
            @ConfirmedQuantity   = @quantity,
            @ConfirmedWeight     = @Weight,
            @PalletId            = @PalletId

           if @Error <> 0
             goto error
         end
       end
       else
       begin
         select @Error = 0
       end
     end
     else
     begin
       select top 1 @InstructionId     = InstructionId,
                    @InstructionTypeId = InstructionTypeId,
                    @ConfirmedQuantity = isnull(ConfirmedQuantity, 0)
         from Instruction
        where JobId          = @jobId
          and PickLocationId = @locationId

       if @type = 1
       begin
         select @StatusId = dbo.ufn_StatusId('I','S')

         exec @Error = p_Instruction_Insert
          @InstructionId       = @InstructionId output,
          @InstructionTypeId   = @InstructionTypeId,
          @StorageUnitBatchId  = @StorageUnitBatchId,
          @WarehouseId         = @WarehouseId,
          @StatusId            = @StatusId,
          @JobId               = @jobId,
          @OperatorId          = @operatorId,
          @PickLocationId      = @LocationId,
          @InstructionRefId    = @InstructionId,
          @Quantity            = @ConfirmedQuantity,
          @ConfirmedQuantity   = @quantity,
          @ConfirmedWeight     = @Weight,
          @PalletId            = @PalletId,
          @CreateDate          = @GetDate

         if @Error <> 0
           goto error
       end
       else
       begin
         if @AreaCode in ('PK','BK')
           set @Error = 2
         else
           set @Error = 1
       end
     end
   end

   --select @ProductId = ProductId
 end

 if @type = 2 -- Finished
 begin
   select @StatusId = dbo.ufn_StatusId('J','S')

   exec @Error = p_Job_Update
    @JobId      = @jobId,
    @OperatorId = @operatorId,
    @StatusId   = @StatusId

   declare @TableFinished as table
   (
    InstructionId int,
    StatusCode    nvarchar(10),
    Stored        bit
   )

   insert @TableFinished
         (InstructionId,
          StatusCode,
          Stored)
   select i.InstructionId,
          s.StatusCode,
          case when Quantity = ConfirmedQuantity then 1 else null end
     from Instruction i
     join Status      s (nolock) on i.StatusId = s.StatusId
    where JobId          = @jobId
      and PickLocationId = @locationId

   select @rowcount = @@rowcount

   while @rowcount > 0
   begin
     set @rowcount = @rowcount - 1
     select top 1
            @InstructionId = InstructionId,
            @StatusCode    = StatusCode,
            @Stored        = Stored
       from @TableFinished

     delete @TableFinished where InstructionId = @InstructionId

     if @StatusCode = 'W'
     begin
       exec @Error = p_Instruction_Started
        @InstructionId = @InstructionId,
        @OperatorId    = @operatorId

       if @Error <> 0
         goto error
		   set @InstrStatusCode = (select StatusCode 
									 from Instruction i (nolock) 
									 join Status s (nolock) on i.StatusId = s.StatusId 
									where i.InstructionId = @InstructionId)	
			if @InstrStatusCode = 'F'
			begin
				exec p_Exception_Insert 
					 @InstructionId	= @InstructionId,
					 @reasonId		= 0,
					 @Exception		= 'Stock take error',
					 @ExceptionCode	= 'STKERR',
					 @CreateDate	= @GetDate,
					 @OperatorId	= @OperatorId,
					 @ExceptionDate	= @GetDate,
					 @quantity		= @quantity
			end
						
				
       exec @Error = p_Instruction_Finished
        @InstructionId = @InstructionId,
        @OperatorId    = @operatorId

       if @Error <> 0
         goto error
     end

     if @StatusCode = 'S'
     begin
		set @InstrStatusCode = (select StatusCode 
									 from Instruction i (nolock) 
									 join Status s (nolock) on i.StatusId = s.StatusId 
									where i.InstructionId = @InstructionId)	
			if @InstrStatusCode = 'F'
			begin
				exec p_Exception_Insert 
					 @InstructionId	= @InstructionId,
					 @reasonId		= 0,
					 @Exception		= 'Stock take error',
					 @ExceptionCode	= 'STKERR',
					 @CreateDate	= @GetDate,
					 @OperatorId	= @OperatorId,
					 @ExceptionDate	= @GetDate,
					 @quantity		= @quantity
			end
			
       exec @Error = p_Instruction_Finished
        @InstructionId = @InstructionId,
        @OperatorId    = @operatorId

       if @Error <> 0
         goto error
     end

--      if @Stored is not null
--      begin
--        exec @Error = p_Instruction_Update
--         @InstructionId = @InstructionId,
--         @Stored        = @Stored
--      end

     if exists(select top 1 1
                 from Job (nolock)
                where JobId = @jobId
                  and ReferenceNumber like 'Mixed Pick%')
     begin
       exec @Error = p_Housekeeping_Stock_Take_Update
        @instructionId = @instructionId,
        @operatorId    = @operatorId,
        @statusCode    = 'A'

       if @Error <> 0
         goto error
     end
     else if dbo.ufn_Configuration(307, @WarehouseId) = 1
         and exists(select top 1 1
                      from Instruction (nolock)
                     where InstructionId = @instructionId
                       and Quantity = ConfirmedQuantity)
     begin
       exec @Error = p_Housekeeping_Stock_Take_Update
        @instructionId = @instructionId,
        @operatorId    = @operatorId,
        @statusCode    = 'A'

       if @Error <> 0
         goto error
     end

     if (select count(1)
           from Instruction i (nolock)
           join Status      s (nolock) on i.StatusId = s.StatusId
          where i.JobId       = @JobId
            and s.StatusCode  in ('W','S','NS')) = 0
     begin
       select @StatusId = dbo.ufn_StatusId('J','F')

       exec @Error = p_Job_Update
        @JobId     = @JobId,
        @StatusId  = @StatusId

       if @Error <> 0
         goto error
     end
   end
 end

 if @Error <> 0
   goto error

 result:
   commit transaction

   select @CurrentLine = Count(1)
     from Instruction   i (nolock)
     join Status        s (nolock) on i.StatusId = s.StatusId
    where s.Type       = 'I'
      and s.StatusCode = 'F'
      and i.JobId      = @jobId
      --and InstructionRefId is null

   if @CurrentLine = 0
     set @CurrentLine = 1

   select @TotalLines = Count(1)
     from Instruction   i (nolock)
    where i.JobId           = @jobId
      --and InstructionRefId is null

   if @LocationId is null
     set @LocationId = -1

   select @Error              as 'Error',
          @InstructionId      as 'InstructionId',
          @LocationId         as 'LocationId',
          @Location           as 'Location',
          @SecurityCode       as 'SecurityCode',
          @CurrentLine        as 'CurrentLine',
          @TotalLines         as 'TotalLines',
          @StorageUnitBatchId as 'StorageUnitBatchId',
          @ProductCode        as 'ProductCode',
          @Product            as 'Product',
          @SKUCode            as 'SKUCode',
          @Remarks            as 'Remarks'
   return

 error:
   rollback transaction
   select @Error
   return @Error
end

 
 
 
 
 
