﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Mixed_Detail
  ///   Filename       : p_Mobile_Mixed_Detail.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Mixed_Detail
(
 @instructionId int
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  OutboundShipmentId   int,
	  IssueId              int,
	  OutboundDocumentId   int,
	  OrderNumber          nvarchar(30),
   RouteId              int,
   [Route]              nvarchar(50),
   [Load]               int,
   DespatchDate         datetime,
   JobId                int,
   ReferenceNumber      nvarchar(30),
   ContainerTypeId      int,
   ContainerType        nvarchar(50),
   OutboundDocumentType nvarchar(30),
   Area                 nvarchar(50),
   Items                int,
   Weight               numeric(13,6),
   PickLocationId       int,
   WaveId               int,
   Wave                 nvarchar(50)
	 )
	 declare @RouteId            int,
	         @Route              nvarchar(10),
	         @DespatchDate       datetime,
	         @JobId              int,
	         @ReferenceNumber    nvarchar(30),
	         @Weight             numeric(13,6),
	         @Items              int
	 
	 declare @IssueLineId int
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         OutboundDocumentId,
         JobId,
         ReferenceNumber,
         PickLocationId)
  select distinct
         ili.OutboundShipmentId,
         ili.IssueId,
         ili.OutboundDocumentId,
         j.JobId,
         j.ReferenceNumber,
         i.PickLocationId
    from Instruction i (nolock)
    join Job         j (nolock) on i.JobId = j.JobId
    left outer
    join IssueLineInstruction ili (nolock) on i.InstructionId = ili.InstructionId
   where i.InstructionId = @instructionId
  
  update tr
     set Area = a.Area
    from @TableResult tr
    join AreaLocation al (nolock) on tr.PickLocationId = al.LocationId
    join Area          a (nolock) on al.AreaId = a.AreaId
  
  select @JobId = JobId
    from Instruction
   where InstructionId = @instructionId
  
  if (select dbo.ufn_Configuration(438, 1)) = 1
    update i
       set Weight = i.ConfirmedQuantity * p.Weight
      from Instruction        i (nolock)
      join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
      join Pack               p (nolock) on sub.StorageUnitId = p.StorageUnitId
                                        and i.WarehouseId = p.WarehouseId
                                        and p.Quantity = 1
     where i.JobId = @JobId
  
  select @Weight = SUM(Weight),
         @Items  = COUNT(distinct(StorageUnitBatchId))
    from Instruction (nolock)
   where JobId = @JobId
  
  update tr
     set Weight = @Weight,
         Items = @Items
    from @TableResult tr
  
  if (select dbo.ufn_Configuration(221, 1)) = 1
  begin
    update @TableResult
       set RouteId       = i.RouteId,
           DespatchDate  = i.DeliveryDate,
           WaveId        = i.WaveId,
           OrderNumber   = od.OrderNumber
      from @TableResult tr
      join Issue         i (nolock) on tr.IssueId = i.IssueId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
  end
  else
  begin
    update @TableResult
       set RouteId       = i.RouteId,
           DespatchDate  = i.DeliveryDate,
           WaveId        = i.WaveId,
           OrderNumber   = od.OrderNumber
      from @TableResult tr
      join Issue         i (nolock) on tr.IssueId = i.IssueId
      join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
     where tr.OutboundShipmentId is null
  end
  
  update @TableResult
     set RouteId      = os.RouteId,
         DespatchDate = os.ShipmentDate,
         WaveId        = os.WaveId
    from @TableResult tr
    join OutboundShipment os (nolock) on tr.OutboundShipmentId = os.OutboundShipmentId
   where os.OutboundShipmentId is not null
  
  update @TableResult
     set Route      = r.Route
    from @TableResult tr
    join Route         r (nolock) on tr.RouteId = r.RouteId
  
  update @TableResult
     set Wave      = w.Wave
    from @TableResult tr
    join Wave          w (nolock) on tr.WaveId = w.WaveId
  
  update @TableResult
     set ContainerType = ct.ContainerType,
         OutboundDocumentType = odt.OutboundDocumentType
    from @TableResult     tr
    join OutboundDocument od (nolock) on tr.OutboundDocumentId = od.OutboundDocumentId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    join ExternalCompany  ec (nolock) on od.ExternalCompanyId = ec.ExternalCompanyId
    left
    join ContainerType    ct (nolock) on ec.ContainerTypeId = ct.ContainerTypeId
  
  select [Route],
         OutboundShipmentId as 'Load',
         DespatchDate,
         JobId,
         ReferenceNumber,
         OrderNumber,
         ContainerType,
         OutboundDocumentType,
         Area,
         Weight,
         Items,
         Wave,
         null as 'Manifest'
    from @TableResult
end
