﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Outbound_Shipment_Unlinked_Picked_Search
  ///   Filename       : p_Outbound_Shipment_Unlinked_Picked_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Outbound_Shipment_Unlinked_Picked_Search
(
 @WarehouseId            int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	  nvarchar(30),
 @ExternalCompany	      nvarchar(255),
 @OrderNumber	          nvarchar(30),
 @FromDate	             datetime,
 @ToDate	               datetime,
 @PrincipalId			int
)

as
begin
	 set nocount on;
  
  declare @TableHeader as table
  (
   IssueId               int,
   OutboundDocumentId    int,
   OrderNumber           nvarchar(30),
   ExternalCompanyCode   nvarchar(30),
   ExternalCompany       nvarchar(255),
   RouteId               int,
   Route                 nvarchar(50),
   NumberOfLines         int,
   DeliveryDate          datetime,
   StatusId              int,
   Status                nvarchar(50),
   OutboundDocumentType  nvarchar(50),
   LocationId            int,
   Location              nvarchar(15),
   CreateDate            datetime,
   Rating                int,
   AvailabilityIndicator nvarchar(20)
  );
  
  declare @TableDetail as table
  (
   OutboundDocumentId int,
   OrderVolume        float,
   OrderWeight        float
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  insert @TableHeader
        (IssueId,
         id.OutboundDocumentId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         RouteId,
         DeliveryDate,
         StatusId,
         Status,
         OutboundDocumentType,
         LocationId,
         CreateDate,
         Rating)
  select i.IssueId,
         id.OutboundDocumentId,
         id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         i.RouteId,
         i.DeliveryDate,
         i.StatusId,
         s.Status,
         idt.OutboundDocumentType,
         i.LocationId,
         id.CreateDate,
         ec.Rating
    from OutboundDocument     id  (nolock)
    join OutboundDocumentType idt (nolock) on id.OutboundDocumentTypeId = idt.OutboundDocumentTypeId
    join ExternalCompany      ec  (nolock) on id.ExternalCompanyId     = ec.ExternalCompanyId
    join Issue                i   (nolock) on id.OutboundDocumentId     = i.OutboundDocumentId
    join Status               s   (nolock) on i.StatusId               = s.StatusId
   where id.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, id.OutboundDocumentTypeId)
     and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
     and i.DeliveryDate        between @FromDate and @ToDate
     and not exists(select top 1 1 from OutboundShipmentIssue isi (nolock) where i.IssueId = isi.IssueId)
     and id.WarehouseId = @WarehouseId
     and s.Type                    = 'IS'
     and s.StatusCode              in ('CD','D','DC','C') -- = 'F' -- Finished
  
  update tr
     set Location = l.Location
    from @TableHeader tr
    join Location     l  (nolock) on tr.LocationId = l.LocationId
  
  update @TableHeader
     set NumberOfLines = (select count(1)
                            from OutboundLine il (nolock)
                           where t.OutboundDocumentId = il.OutboundDocumentId)
    from @TableHeader t
  
  update tr
     set Route = r.Route
    from @TableHeader  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
  insert @TableDetail
        (OutboundDocumentId,
         OrderVolume,
         OrderWeight)
  select th.OutboundDocumentId,
         round(sum(il.Quantity) * sum(p.Quantity),0),
         round(sum(il.Quantity) * sum(p.Weight),0)
    from @TableHeader th
    join OutboundLine ol (nolock) on th.OutboundDocumentId = ol.OutboundDocumentId
    join IssueLine    il (nolock) on ol.OutboundLineId     = il.OutboundLineId
    join Pack          p (nolock) on ol.StorageUnitId      = p.StorageUnitId
   where p.Quantity = 1
  group by th.OutboundDocumentId
  
  update @TableHeader
     set AvailabilityIndicator = 'Standard'
   where dbo.ufn_Configuration_Value(47, @WarehouseId) <= DateDiff(hh, @GetDate, DeliveryDate)
   
  update @TableHeader
     set AvailabilityIndicator = 'Yellow'
   where dbo.ufn_Configuration_Value(46, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableHeader
     set AvailabilityIndicator = 'Orange'
   where dbo.ufn_Configuration_Value(45, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableHeader
     set AvailabilityIndicator = 'Red'
   where dbo.ufn_Configuration_Value(44, @WarehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  select IssueId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         Route,
         NumberOfLines,
         DeliveryDate,
         Status,
         OutboundDocumentType,
         Location,
         CreateDate,
         Rating,
         AvailabilityIndicator
        ,td.OrderVolume
        ,td.OrderWeight
    from @TableHeader tr
    left outer
    join @TableDetail td on tr.OutboundDocumentId = td.OutboundDocumentId
end
 
