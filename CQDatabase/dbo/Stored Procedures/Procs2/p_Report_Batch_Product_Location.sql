﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Batch_Product_Location
  ///   Filename       : p_Report_Batch_Product_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Batch_Product_Location
(
 @WarehouseId       int,
 @StorageUnitId     int = null,
 @BatchId           int = null
)

as
begin
	 set nocount on;
	 
	 
  if @StorageUnitId = -1 set @StorageUnitId = null
  if @BatchId = -1 set @BatchId = null
  

  select --sub.StorageUnitId,
		 --sub.BatchId,
			p.ProductCode,
         p.Product,
         sku.SKUCode,
         b.Batch,
		dbo.ConvertTo128(b.Batch) as BatchBarcode,
		 s.status,
         a.Area,
         l.Location,
         subl.ActualQuantity
    from StorageUnitBatchLocation subl (nolock)
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId            = p.ProductId
    join SKU                       sku (nolock) on su.SKUId                = sku.SKUId
    join Batch						 b (nolock) on b.BatchId			   = sub.BatchId
    join Location                    l (nolock) on subl.Locationid         = l.LocationId
    join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
    join Area                        a (nolock) on al.AreaId               = a.AreaId
	Join Status						 s (nolock) on s.StatusId			   = b.StatusId
   where a.WarehouseId      = @WarehouseId
     and su.StorageUnitId   = isnull(@StorageUnitId, su.StorageUnitId)
     and sub.BatchId          = isnull(@BatchId, sub.BatchId)
	 and a.StockOnHand = 1
  

end
