﻿/*
  /// <summary>
  ///   Procedure Name : p_Menu_Items
  ///   Filename       : p_Menu_Items.sql
  ///   Create By      : William
  ///   Date Created   : 10 July 2008
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Menu table.
  /// </remarks>
  /// 
  /// <returns>
  ///   mi.MenuItemId,
  ///   mic.MenuItemText,
  ///   mic.ToolTip,
  ///   mi.NavigateTo,
  ///   mi.ParentMenuItemId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure dbo.p_Menu_Items
as
begin
	 set nocount on

Select mi.MenuItemId,
		     m.Menu + ' \ ' + mi.MenuItem as 'MenuItem'
  from Menuitem mi
  join Menu m on mi.MenuId= m.MenuId
 where mi.ParentMenuItemId is null 
order by m.Menu,
         mi.MenuItem
end
