﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_Bond_Book
  ///   Filename       : p_Report_Bond_Book.sql
  ///   Create By      : Karen
  ///   Date Created   : February 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_Bond_Book
(
 @FromDate          datetime,
 @ToDate            datetime,
 @PrincipalId		int,
 @ExternalCompanyId	int
)

as
begin
	 set nocount on;
	 
declare @TableResult as table
  (
   ExternalCompanyId	int,
   ExternalCompany		nvarchar(255),
   DocumentTypeId		int,
   DocumentType			nvarchar(30),
   StatusId				int,
   Status				nvarchar(50),
   WarehouseId			int,
   Warehouse			nvarchar(50),
   WarehouseReference	nvarchar(50),
   FileReference		nvarchar(50),
   ContainerNumber		nvarchar(50),
   StorageUnitBatchId	int,
   StorageUnitId		int,
   ProductCode			nvarchar(30),
   Product				nvarchar(50),
   OrderNumber			nvarchar(50),
   DeliveryDate			datetime,
   Incoterms			nvarchar(50),
   InBOE				nvarchar(50),
   OutBOE				nvarchar(50),
   BOELineNumber        nvarchar(50),
   LineId				int,
   QuantityIn			float,
   QuantityOut			float,
   RCVValue				float,
   EXPValue				float,
   StockBal				float,
   UnitPrice			float,
   Sequence				int
  )


declare @TableSeq as table
  (
   ExternalCompanyId	int,
   ExternalCompany		nvarchar(255),
   DocumentTypeId		int,
   DocumentType			nvarchar(30),
   StatusId				int,
   Status				nvarchar(50),
   WarehouseId			int,
   Warehouse			nvarchar(50),
   ContainerNumber		nvarchar(50),
   StorageUnitBatchId	int,
   StorageUnitId		int,
   ProductCode			nvarchar(30),
   Product				nvarchar(50),
   OrderNumber			nvarchar(50),
   DeliveryDate			datetime,
   Incoterms			nvarchar(50),
   InBOE				nvarchar(50),
   OutBOE				nvarchar(50),
   BOELineNumber        nvarchar(50),
   LineId				int,
   QuantityIn			float,
   QuantityOut			float,
   RCVValue				float,
   EXPValue				float,
   StockBal				float,
   UnitPrice			float,
   Sequence				int
  )
  
	Declare @Principal	nvarchar(100)
	
	
	if @PrincipalId = -1
		set @PrincipalId = null
		
    if @ExternalCompanyId = -1
		set @ExternalCompanyId = null
		
	insert	@TableResult
			(ExternalCompanyId,
			DocumentTypeId,
			DocumentType,
			StatusId,
			WarehouseId,
			WarehouseReference,
			FileReference,
			ContainerNumber,
			StorageUnitBatchId,
			OrderNumber,
			DeliveryDate,
			InBOE,
			BOELineNumber,
			Incoterms,
			LineId,
			QuantityIn,
			UnitPrice)
	select 
			id.ExternalCompanyId,
			id.InboundDocumentTypeId,
			idt.InboundDocumentType,
			rl.StatusId,
			id.WarehouseId,
			rl.Reference1,
			r.GRN,
			ContainerNumber,
			rl.StorageUnitBatchId,
			id.OrderNumber,
			r.DeliveryDate,
			r.BOE,
			rl.BOELineNumber,
			r.Incoterms,
			rl.BOELineNumber,
			rl.AcceptedQuantity,
			rl.UnitPrice
	  from	InboundDocument     id (nolock)
	  join	InboundLine		    il (nolock) on id.InboundDocumentId = il.InboundDocumentId
	  join	Receipt			     r (nolock) on id.InboundDocumentId = r.InboundDocumentId
	  join	ReceiptLine		    rl (nolock) on r.ReceiptId = rl.ReceiptId
	  join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
	 where InboundDocumentTypeCode = 'BOND'
	   and r.DeliveryDate between @FromDate and @ToDate
	   and id.PrincipalId = isnull(@PrincipalId,id.PrincipalId)
	   and id.ExternalCompanyId = isnull(@ExternalCompanyId,id.ExternalCompanyId)
	
	  
	insert	@TableResult
			(ExternalCompanyId,
			DocumentTypeId,
			DocumentType,
			StatusId,
			WarehouseId,
			FileReference,
			StorageUnitBatchId,
			OrderNumber,
			DeliveryDate,
			LineId,
			QuantityOut,
			UnitPrice)
	select 
			od.ExternalCompanyId,
			od.OutboundDocumentTypeId,
			odt.OutboundDocumentType,
			il.StatusId,
			od.WarehouseId,
			osi.OutboundShipmentId,
			il.StorageUnitBatchId,
			od.OrderNumber,
			i.DeliveryDate,
			il.IssueLineId,
			il.ConfirmedQuatity,
			ol.UnitPrice
	  from	OutboundDocument      od (nolock)
	  join  OutboundLine		  ol (nolock) on od.OutboundDocumentId = ol.OutboundDocumentId 
	  join	Issue			       i (nolock)  on od.OutboundDocumentId = i.OutboundDocumentId
	  join	IssueLine		      il (nolock) on i.IssueId = il.IssueId
	  join  OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
	  left join OutboundShipmentIssue osi (nolock) on osi.IssueId = i.IssueId
	 where  odt.OutboundDocumentTypeCode = 'BOND'
	   and  i.DeliveryDate between @FromDate and @ToDate
	   and  od.PrincipalId = isnull(@PrincipalId, od.PrincipalId)
	   and  od.ExternalCompanyId = isnull(@ExternalCompanyId,od.ExternalCompanyId)
	  
	update tr
       set ExternalCompany = ec.ExternalCompany
      from @TableResult tr
      join ExternalCompany ec on tr.ExternalCompanyId = ec.ExternalCompanyId 
      
    update tr
       set Warehouse = w.Warehouse
      from @TableResult tr
      join Warehouse w on tr.WarehouseId = w.WarehouseId
      
    update tr
       set Status = s.Status
      from @TableResult tr
      join Status s on tr.StatusId = s.StatusId
      
     set @Principal = (select p.Principal
						 from Principal p 
						where @PrincipalId = p.PrincipalId)
      
    update tr
       set StorageUnitId = su.StorageUnitId,
		   ProductCode = p.ProductCode,
		   Product = p.Product
      from @TableResult tr
      join StorageUnitBatch sub on tr.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit su on sub.StorageUnitId = su.StorageUnitId
      join Product p on su.ProductId = p.ProductId  
      
      
    update tr
       set RCVValue = tr.QuantityIn * UnitPrice,
		   EXPValue = tr.QuantityOut * UnitPrice
      from @TableResult tr
      
    --update tr
    --   set Sequence = ROW_NUMBER() OVER (partition by ProductCode ORDER BY ProductCode)
    --  from @TableResult tr
      
    insert @TableSeq
          (ExternalCompanyId,
		   ExternalCompany,
		   DocumentTypeId,
		   DocumentType,
		   StatusId,
		   Status,
		   WarehouseId,
		   Warehouse,
		   ContainerNumber,
		   StorageUnitBatchId,
		   StorageUnitId,
		   ProductCode,
		   Product,
		   OrderNumber,
		   DeliveryDate,
		   Incoterms,
		   InBOE,
		   OutBOE,
		   BOELineNumber,
		   LineId,
		   QuantityIn,
		   QuantityOut,
		   RCVValue,
		   EXPValue,
		   StockBal,
		   Sequence)
  select distinct 
           ExternalCompanyId,
		   ExternalCompany,
		   DocumentTypeId,
		   DocumentType,
		   StatusId,
		   Status,
		   WarehouseId,
		   Warehouse,
		   ContainerNumber,
		   StorageUnitBatchId,
		   StorageUnitId,
		   ProductCode,
		   Product,
		   OrderNumber,
		   DeliveryDate,
		   Incoterms,
		   InBOE,
		   OutBOE,
		   BOELineNumber,
		   LineId,
		   QuantityIn,
		   QuantityOut,
		   RCVValue,
		   EXPValue,
		   isnull(QuantityIn,0) - isnull(QuantityOut,0),
		   ROW_NUMBER() OVER (partition by ProductCode ORDER BY ProductCode, DeliveryDate) Rank
    from @TableResult td 

	
      
	--SELECT a.StockBal, a.StockBal + (SELECT SUM(b.QuantityIn)
	--				 FROM   @TableSeq b
	--				WHERE  b.Sequence <= a.Sequence) + (SELECT SUM(-b.QuantityOut)
	--				 FROM   @TableSeq b
	--				WHERE  b.Sequence <= a.Sequence)
	--  FROM   @TableSeq a
	-- ORDER  BY a.Sequence

     
	select distinct ExternalCompanyId,
		   ExternalCompany,
		   DocumentTypeId,
		   DocumentType,
		   StatusId,
		   Status,
		   WarehouseId,
		   Warehouse,
		   WarehouseReference,
		   FileReference,
		   ContainerNumber,
		   StorageUnitBatchId,
		   StorageUnitId,
		   ProductCode,
		   Product,
		   OrderNumber,
		   DeliveryDate,
		   Incoterms,
		   InBOE,
		   OutBOE,
		   BOELineNumber,
		   LineId,
		   QuantityIn,
		   QuantityOut,
		   RCVValue,
		   EXPValue,
		   isnull(QuantityIn,0) - isnull(QuantityOut,0),
		   @Principal as Principal	
    from @TableResult tr
    order by 
		ProductCode,
		tr.DeliveryDate

end
