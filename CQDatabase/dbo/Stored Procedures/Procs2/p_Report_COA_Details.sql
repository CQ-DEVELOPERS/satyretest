﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Report_COA_Details
  ///   Filename       : p_Report_COA_Details.sql
  ///   Create By      : Karen
  ///   Date Created   : October 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Report_COA_Details
(
 @COAId       int
)

as
begin
	 set nocount on;
	 
  select t.Test,
		 (convert(nvarchar, isnull(coat.value,'')) + ' ' + r.Result) as Result,
		 m.Method
    from COATest coat (nolock)  
    join  Test		t (nolock) on coat.TestId = t.TestId
    join  Result	r (nolock) on coat.ResultId = r.ResultId
    join  Method	m (nolock) on coat.MethodId = m.MethodId
    where COAId     = @COAId

end
