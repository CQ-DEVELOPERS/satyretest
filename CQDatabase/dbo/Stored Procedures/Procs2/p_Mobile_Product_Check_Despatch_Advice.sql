﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Despatch_Advice
  ///   Filename       : p_Mobile_Product_Check_Despatch_Advice.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Despatch_Advice
(
 @jobId         int,
 @IssueId       int output
)

as
begin
	 set nocount on;
  
  select @IssueId = ili.IssueId
    from Instruction            i (nolock)
    join IssueLineInstruction ili (nolock) on isnull(i.InstructionRefId, i.InstructionId) = ili.InstructionId
   where i.JobId = @jobId
  
  if exists(select 1
              from IssueLineInstruction ili (nolock)
              join Instruction            i (nolock) on ili.InstructionId = isnull(i.InstructionRefId, i.InstructionId)
              join Job                    j (nolock) on i.JobId = j.JobId
              join Status                 s (nolock) on j.StatusId = s.StatusId
             where ili.IssueId = @IssueId
               and s.StatusCode not in ('NS','CD','QA','DC','D','C'))
    set @issueId = -1
end
