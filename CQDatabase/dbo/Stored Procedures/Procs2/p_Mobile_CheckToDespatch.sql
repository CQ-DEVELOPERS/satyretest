﻿
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_CheckToDespatch
  ///   Filename       : p_Mobile_CheckToDespatch.sql
  ///   Create By      : Karen
  ///   Date Created   : September 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_CheckToDespatch
(
 @OrderNumber			nvarchar(30),
 @Location				nvarchar(30)
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
	 (
	  Id					int identity,
	  operatorId            int,
	  storageUnitBatchId    int,
	  pickLocation			nvarchar(30),
	  pickLocationId	    int,
	  storeLocation			nvarchar(30),
	  storeLocationId		int,
	  confirmedQuantity     int,
	  instructionTypeCode   nvarchar(30),
	  JobId					int
	 )
	 
	declare @TableResult2 as table
	 (
	  Id					int identity,
	  IssueId				int,
	  IssueLineId		    int,
	  JobId					int
	 )
	 
	   declare	@Error					int,
				@Id						int,
				@operatorId				int,
				@storageUnitBatchId		int,
				@pickLocation			nvarchar(30),
				@pickLocationId			int,
				@storeLocation			nvarchar(30),
				@storeLocationId		int,
				@confirmedQuantity		int,
				@instructionTypeCode	nvarchar(30),
				@JobId					int,
				@LocationId				int,
				@IssueId			    int,
				@IssueLineId		    int,
				@Branded				bit,
				@CheckingCount			int,
				@StatusId				int,
				@IssueStatusId			int,
				@AreaCode				nvarchar(50),
				@Downsizing				bit,
				@OutboundDocumentId		int,
				@warehouseid			int
    	



  set nocount on;
  
  declare @trancount int
         ,@ErrorId int
         ,@ErrorSeverity int
         ,@ErrorMessage varchar(4000)
         ,@ErrorState int
         ,@ExternalCompanyId int
         ,@InterfaceImportPOHeaderId int;
  
  set @trancount = @@trancount;
  
  begin try
		  if @trancount = 0
			   begin transaction
		  else
			   save transaction p_Wizard_Receipt_Insert;
    
		  -- Do the actual work here
	
	set @Branded = 0
	set @Branded = (select 1 
				      from InterfaceImportSOHeader 
				     where OrderNumber = @OrderNumber 
				       and (Branding like '%yes%'or Branding like '%true%'))
				       
	set @Downsizing = 0
	set @Downsizing = (select 1 
				      from InterfaceImportSOHeader 
				     where OrderNumber = @OrderNumber 
				       and (Downsizing like '%yes%'or Downsizing like '%true%'))
				  
	set @IssueId = (select IssueId from OutboundDocument od (nolock) 
								   join Issue             i (nolock)  on od.OutboundDocumentId = i.OutboundDocumentId
								  where od.OrderNumber = @OrderNumber)
	  
	set @warehouseid = (select WarehouseId from Issue i where i.IssueId = @IssueId)
				       			
	select @LocationId = l.LocationId,
	       @Location = l.Location
	  from viewLocation l (nolock)
  where (l.Location = @Location or convert(nvarchar(10), l.SecurityCode) = @Location)
	   and l.WarehouseId = @warehouseid
	
	set @AreaCode = (select AreaCode from viewLocation a 
				   --join AreaLocation al on a.AreaId = al.AreaId
				  where a.Locationid = @LocationId
					and a.WarehouseId = @warehouseid)
								  
	set @IssueStatusId = (select i.StatusId from OutboundDocument od (nolock) 
								   join Issue             i (nolock)  on od.OutboundDocumentId = i.OutboundDocumentId
								  where od.OrderNumber = @OrderNumber)
	
	set  @CheckingCount = (select CheckingCount from Issue i where i.IssueId = @IssueId)
	
	set @OutboundDocumentId = (select OutboundDocumentId 
								 from OutboundDocument od (nolock)
								where od.OrderNumber = @OrderNumber)
	
	--print '@Branded'
	--print @Branded
	--print '@CheckingCount'
	--print @CheckingCount
	--print '@AreaCode'
	--print @AreaCode
	--print '@IssueStatusId'
	--print @IssueStatusId
 
	 if (@Branded = 1 or @Downsizing = 1)
	and @CheckingCount = 1
	--and @IssueStatusId = dbo.ufn_StatusId('IS','CD')
	and @AreaCode = 'CK'
	begin
   insert @TableResult2
             (IssueId,
              IssueLineId,
              JobId)
      select ili.IssueId,
             ili.IssueLineId,
             ili.JobId
        from viewili ili 
       where ili.ordernumber = @OrderNumber
   
		 set @StatusId = dbo.ufn_StatusId('IS','CK')
		 
		exec @Error = p_Issue_Update
			 @IssueId		= @IssueId,
			 @StatusId		= @StatusId,
			 @DespatchBay	= @LocationId
			 
	 exec @Error = p_OutboundDocument_Update
			 @OutboundDocumentId	= @OutboundDocumentId,
			 @ToLocation			= @Location
		
		while exists(select top 1 1 from @TableResult2)
		begin	
		  select	@Id							= Id,
			      @IssueId					= IssueId,
			      @IssueLineId				= IssueLineId,
			      @JobId						= JobId
	   from @TableResult2 tr
   
   delete @TableResult2 where Id = @Id
	 
		 exec @Error = p_IssueLine_Update
			   @IssueId	= @IssueId,
			   @IssueLineId = @IssueLineId,
			   @StatusId	= @StatusId
			   
	     exec @Error = p_Job_Update
			   @JobId		= @JobId,
			   @StatusId	= @StatusId
		end
					
	end

--**
	if @AreaCode = 'D'
	and @IssueStatusId != dbo.ufn_StatusId('IS','CD')
	begin
		 if (@Branded = 1 or @Downsizing = 1)
		and @CheckingCount < 2
		begin
			set @error = 1
			goto error
		end
	end
	
	insert @TableResult
            (operatorId,
             storageUnitBatchId,
             pickLocation,
             pickLocationId,
             storeLocation,
             storeLocationId,
             confirmedQuantity,
             instructionTypeCode,
             ili.JobId)
      select ili.operatorid,
             ili.StorageUnitBatchId,
             ili.PickLocation,
             ili.PickLocationId,
             ili.StoreLocation,
             ili.StoreLocationId,
             ili.confirmedQuantity,
             'M',
             JobId
        from viewili ili 
       where ili.ordernumber = @OrderNumber
  
  --select @Error         = 1
  --  from viewili ili
  --  join Issue i on ili.IssueId = i.IssueId
  -- where ili.ordernumber = @ordernumber
  --   and i.StatusId = dbo.ufn_StatusId('IS','CK')
  
  while exists(select top 1 1 from @TableResult)
  begin
    select	top 1 @Id							= Id,
			        @operatorId					= operatorId,
			        @storageUnitBatchId			= storageUnitBatchId,
			        @pickLocation				= StoreLocation,
			        @pickLocationId				= StoreLocationId,
			        @storeLocation				= @Location,
			        @confirmedQuantity			= confirmedQuantity,
			        @instructionTypeCode		= instructionTypeCode,
			        @JobId						= JobId
	     from @TableResult tr
	   
    delete @TableResult where Id = @Id
    
    if @confirmedQuantity > 0 and @pickLocation != @storeLocation
    begin
      exec @Error = p_Mobile_Auto_Movement 
		     @operatorId			= @operatorId,
		     @storageUnitBatchId	=@storageUnitBatchId,
		     @pickLocation			=@pickLocation,
		    
 @storeLocation			=@storeLocation,
		     @confirmedQuantity		=@confirmedQuantity,
		     @instructionTypeCode	=@instructionTypeCode,
		     @Override = 1
		   
		   if @Error < 0
		     RAISERROR (900000,-1,-1, 'Movement failed'); 
   end
	 
    update instruction 

	      set storelocationId = @LocationId
     where jobid = @jobid
	end
	
	if (@Branded = 1	and @CheckingCount = 2)
	or (@Downsizing = 1 and @CheckingCount = 2)
	or (@Branded = 0	and @Downsizing = 0)
	begin
   if (select Area from AreaLocation al
       
join Area a on al.AreaId = a.AreaId 
      where al.LocationId = @LocationId) in ('Dispatch','Despatch')

   exec @Error = p_Interface_Export_SO_Insert
    @IssueId = @IssueId

   if @Error <> 0
     goto error
end
	   
	   
    lbexit:
    if @trancount = 0
		    commit;
    
  error:
	 
  result:
    select @Error as '@Error'
    return @Error
  end try
  begin catch
		  select @ErrorId       = ERROR_NUMBER()
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorMessage  = ERROR_MESSAGE()
          ,@ErrorState    = XACT_STATE();
		  
    if @ErrorState = -1
			  rollback;
		  if @ErrorState = 1 and @trancount = 0
			  rollback;
		  if @ErrorState = 1 and @trancount > 0
			  rollback transaction p_Mobile_CheckToDespatch;
    
		  RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
              );
  end catch
 end
 
 
