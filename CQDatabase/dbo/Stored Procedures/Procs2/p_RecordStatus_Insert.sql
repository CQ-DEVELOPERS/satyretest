﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_RecordStatus_Insert
  ///   Filename       : p_RecordStatus_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:35:22
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the RecordStatus table.
  /// </remarks>
  /// <param>
  ///   @StatusId int = null,
  ///   @StatusCode nvarchar(40) = null,
  ///   @StatusDesc nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   RecordStatus.StatusId,
  ///   RecordStatus.StatusCode,
  ///   RecordStatus.StatusDesc 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_RecordStatus_Insert
(
 @StatusId int = null,
 @StatusCode nvarchar(40) = null,
 @StatusDesc nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert RecordStatus
        (StatusId,
         StatusCode,
         StatusDesc)
  select @StatusId,
         @StatusCode,
         @StatusDesc 
  
  select @Error = @@Error
  
  
  return @Error
  
end
