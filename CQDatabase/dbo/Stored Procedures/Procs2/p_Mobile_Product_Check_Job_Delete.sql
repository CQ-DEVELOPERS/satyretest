﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Mobile_Product_Check_Job_Delete
  ///   Filename       : p_Mobile_Product_Check_Job_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Aug 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Mobile_Product_Check_Job_Delete
(
 @operatorId    int,
 @jobId         int
)

as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Mobile_Product_Check_Job_Delete',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @rowcount          int = 0,
          @StatusCode        nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  select @StatusCode = s.StatusCode
              from Job j
              join Status s on j.StatusId = s.StatusId
             where j.JobId = @jobId
               and s.StatusCode = 'CD'
  
  if @StatusCode in ('CD','D','DC','C')
  begin
    if not exists(select top 1 1
                    from Instruction newi
                    join Job         newj on newi.JobId = newj.JobId
                    join Status      news on newj.StatusId = news.StatusId
                    join Instruction oldi on newi.InstructionRefId = oldi.InstructionId
                    join Job         oldj on oldi.Jobid = oldj.JobId
                    join Status      olds on oldj.StatusId = olds.StatusId
                   where newj.JobId = @jobId
                     and olds.StatusCode = 'CK')
    begin
      set @Error = -1
      goto error
    end
  end
  
  update old
     set Quantity          = old.Quantity + new.Quantity,
         ConfirmedQuantity = old.ConfirmedQuantity + new.ConfirmedQuantity
    from Instruction new
    join Instruction old on new.InstructionRefId = old.InstructionId
   where new.JobId = @jobId
  
  select @Error = @@error, @rowcount = @@rowcount
  
  if @Error != 0
    goto error
  
  if @rowcount = 0
  begin
    update Instruction
       set CheckQuantity = 0
     where JobId = @jobId
    
    select @Error = @@error, @rowcount = @@rowcount
    
    if @Error != 0
      goto error
  end
  else
  begin
    delete new
      from Instruction new
      join Instruction old on new.InstructionRefId = old.InstructionId
     where new.JobId = @jobId
    
    select @Error = @@error, @rowcount = @@rowcount
    
    if @Error != 0
      goto error
  end
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      return @Error
end
