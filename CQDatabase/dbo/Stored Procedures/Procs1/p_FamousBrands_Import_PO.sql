﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Import_PO
  ///   Filename       : p_FamousBrands_Import_PO.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_FamousBrands_Import_PO
as
begin
          -- InterfaceImportPOHeader
  declare @InterfaceImportPOHeaderId    int,
          @OrderNumber                  varchar(20),
          @ExternalCompanyCode          varchar(30),
          @CreateDate                   varchar(255),
          @Status                       varchar(255),
          @FromWarehouseCode            varchar(30),
          @Remarks                      varchar(255),
          @Instructions                 varchar(255),
          -- InterfaceImportPODetail
          @LineNumber                   int,
          @ProductCode                  varchar(30),
          @Quantity                     numeric(13,3),
          @PickUp                       varchar(255),
          -- Internal variables
          @Product                      varchar(50),
          @InboundDocumentTypeId        int,
          @InboundDocumentId            int,
          @InboundLineId                int,
          @ReceiptLineId                int,
          @GetDate                      datetime,
          @StorageUnitId                int,
          @StorageUnitBatchId           int,
          @BatchId                      int,
          @Batch                        varchar(30),
          @Error                        int,
          @StatusId                     int,
          @Weight                       numeric(13,3),
          @PalletQuantity               numeric(13,3),
          @ErrorMsg                     varchar(100),
          @ExternalCompanyId            int ,
          @ExternalCompany              varchar(50),
          @FromWarehouseId              int,
          @InboundDocumentTypeCode      varchar(10),
          @InPriorityId                 int
  
  select @GetDate = dbo.ufn_Getdate()
  
  update d
     set InterfaceImportPOHeaderId = (select max(InterfaceImportPOHeaderId)
                                        from InterfaceImportPOHeader h
                                       where d.ForeignKey = h.PrimaryKey)
    from InterfaceImportPODetail d
   where InterfaceImportPOHeaderId is null
  
  begin transaction
  
  update h
     set ProcessedDate = @Getdate
   	from	InterfaceImportPOHeader h
   where not exists(select 1 from InboundDocument  id where h.OrderNumber = id.OrderNumber)
     and exists(select 1 from InterfaceImportPODetail d where h.PrimaryKey = d.ForeignKey)
     and h.RecordStatus in ('N','U')
     and ProcessedDate is null
  
  declare header_cursor cursor for
   select InterfaceImportPOHeaderId,
          PrimaryKey,        -- OrderNumber
          SupplierCode,      -- SupplierCode
          Additional1,       -- CreateDate
          Additional2,       -- Status
          FromWarehouseCode, -- Location
          Remarks,           -- Remarks
          Additional3        -- Instructions
    	from	InterfaceImportPOHeader
    where ProcessedDate = @Getdate
   order by	PrimaryKey
  
  open header_cursor
  
  fetch header_cursor
   into @InterfaceImportPOHeaderId,
        @OrderNumber,
        @ExternalCompanyCode,
        @CreateDate,
        @Status,
        @FromWarehouseCode,
        @Remarks,
        @Instructions
  
  while (@@fetch_status = 0)
  begin
    set rowcount 1
    update InterfaceImportPOHeader
       set RecordStatus = 'C'
     where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
    set rowcount 0
    
    set @InboundDocumentTypeId = null
    set @InboundDocumentTypeCode = null
    set @FromWarehouseId = 1
    
    select @InboundDocumentTypeId    = InboundDocumentTypeId,
           @InboundDocumentTypeCode = InboundDocumentTypeCode
      from InboundDocumentType
     where InboundDocumentTypeCode = 'RCP'
    
    exec @Error = p_Inbound_Order_Delete
     @OrderNumber = @OrderNumber
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Inbound_Order_Delete'
      goto next_header
    end
    
    if not exists(select 1 from Warehouse where WarehouseId = @FromWarehouseId)
    begin
      if @@trancount > 0
        rollback transaction
      
      update InterfaceImportPOHeader
         set RecordStatus = 'E'
       where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
      select @@rowcount
      begin transaction
    end
    
    if @InboundDocumentTypeCode is not null
    begin
      set @ExternalCompanyId = null
      
      if @ExternalCompanyCode is not null
      begin
        select @ExternalCompanyId   = ExternalCompanyId,
               @ExternalCompanyCode = ExternalCompanyCode,
               @ExternalCompany     = ExternalCompany
          from ExternalCompany (nolock)
         where ExternalCompanyCode = @ExternalCompanyCode
        
        if @ExternalCompanyId is null
        begin
          set @ExternalCompany = @ExternalCompanyCode
          
          exec @Error = p_ExternalCompany_Insert
           @ExternalCompanyId         = @ExternalCompanyId output,
           @ExternalCompanyTypeId     = 2,
           @RouteId                   = null,
           --@drop
           @ExternalCompany           = @ExternalCompany,
           @ExternalCompanyCode       = @ExternalCompanyCode
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
            goto error_header
          end
        end
      end
      
      select @StatusId = dbo.ufn_StatusId('ID','W')
      
      exec @Error = p_InboundDocument_Insert
       @InboundDocumentId     = @InboundDocumentId output,
       @InboundDocumentTypeId = @InboundDocumentTypeId,
       @ExternalCompanyId     = @ExternalCompanyId,
       @StatusId              = @StatusId,
       @WarehouseId           = @FromWarehouseId,
       @OrderNumber           = @OrderNumber,
       @DeliveryDate          = @GetDate,
       @CreateDate            = @GetDate,
       @ModifiedDate          = null
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_InboundDocument_Insert'
        goto error_header
      end
    end
    
    declare detail_cursor cursor for
     select ForeignKey,  -- OrderNumber
            LineNumber,  -- LineNumber
            ProductCode, -- ProductCode
            Quantity,    -- Quantity
            Additional1  -- PickUp
      	from	InterfaceImportPODetail
     	where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
    
    open detail_cursor
    
    fetch detail_cursor
     into @OrderNumber,
          @LineNumber,
          @ProductCode,
          @Quantity,
          @PickUp
    
    while (@@fetch_status = 0)
    begin
      set @StorageUnitBatchId = null
      set @StorageUnitId = null
      set @Batch = null
      
      set @Batch = convert(varchar(30), @OrderNumber)
      
      exec @Error = p_interface_xml_Product_Insert
       @ProductCode        = @ProductCode,
       @Product            = @Product,
       @Batch              = @Batch,
       @WarehouseId        = @FromWarehouseId,
       @StorageUnitId      = @StorageUnitId output,
       @StorageUnitBatchId = @StorageUnitBatchId output,
       @BatchId            = @BatchId output
      
      if @Error != 0 or @StorageUnitBatchId is null
      begin
        select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
        goto error_detail
      end
      
      if @InboundDocumentTypeCode is not null
      begin
        exec @Error = p_InboundLine_Insert
         @InboundLineId     = @InboundLineId output,
         @InboundDocumentId = @InboundDocumentId,
         @StorageUnitId     = @StorageUnitId,
         @StatusId          = @StatusId,
         @LineNumber        = @LineNumber,
         @Quantity          = @Quantity,
         @BatchId           = @BatchId
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_InboundLine_Insert'
          goto error_detail
        end
        
        exec @Error = p_Receiving_Create_Receipt
         @InboundDocumentId = @InboundDocumentId,
         @InboundLineId     = @InboundLineId,
         @OperatorId        = null
               
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Receiving_Create_Receipt'
          goto error_detail
        end
      end
      
      fetch detail_cursor
       into @OrderNumber,
            @LineNumber,
            @ProductCode,
            @Quantity,
            @PickUp
    end
    
    close detail_cursor
    deallocate detail_cursor
    
    goto fetch_header
    next_header:
      update InterfaceImportPOHeader
         set RecordStatus = 'E'
       where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId
    
    fetch_header:
    
    fetch header_cursor
     into @InterfaceImportPOHeaderId,
          @OrderNumber,
          @ExternalCompanyCode,
          @CreateDate,
          @Status,
          @FromWarehouseCode,
          @Remarks,
          @Instructions
  end
  
  close header_cursor
  deallocate header_cursor
  
  commit transaction
  return
  
  error_detail:
    close detail_cursor
    deallocate detail_cursor
    
  error_header:
    close header_cursor
    deallocate header_cursor
    raiserror 900000 @ErrorMsg
    rollback transaction
    
    return
end
