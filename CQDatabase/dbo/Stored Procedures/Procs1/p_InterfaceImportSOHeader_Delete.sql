﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOHeader_Delete
  ///   Filename       : p_InterfaceImportSOHeader_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:17
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceImportSOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSOHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOHeader_Delete
(
 @InterfaceImportSOHeaderId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceImportSOHeader
     where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
