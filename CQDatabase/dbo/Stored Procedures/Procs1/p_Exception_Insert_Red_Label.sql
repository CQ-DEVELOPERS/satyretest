﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_Insert_Red_Label
  ///   Filename       : p_Exception_Insert_Red_Label.sql
  ///   Create By      : Karen	
  ///   Date Created   : March 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_Insert_Red_Label
(
	@ReceiptLineId int,
	@Quantity float,
	@OperatorId	int
)



as
begin
	 set nocount on;
  
 declare @Reasonid int
 
 SET @Reasonid = (SELECT ReasonId FROM Reason WHERE ReasonCode = 'LAB01')
 
 if @ReceiptLineId = -1
   set @ReceiptLineId = null
  
	Insert into Exception
		(ReceiptLineId,
		ReasonId,
		ExceptionCode,
		Exception, 
		Quantity, 
		CreateDate,
		ExceptionDate,
		OperatorId) 
    Values
		(@ReceiptLineId,
		@Reasonid,
		'LAB01',
		'Red Labels Printed',
		@Quantity,
		GETDATE(),
		GETDATE(),
		@OperatorId)
end

