﻿ 
/*
/// <summary>
///   Procedure Name : p_CQ_Product_Export
///   Filename       : p_CQ_Product_Export.sql
///   Create By      : Grant Schultz
///   Date Created   : 14 Apr 2010
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
create procedure p_CQ_Product_Export
(
 @xml        varchar(max) output
)
as
declare @StockLink int,
        @Id        int

declare @TableResult as table
(
 Id    int identity,
 [xml] nvarchar(max)
)
begin
  set nocount on;
  
  select @StockLink = min(StockLink)
    from StkItem (nolock)
   where ubIIcqExport = 0
  
  if @StockLink is null
  begin
    update StkItem
      set ubIIcqExport = 0
    
    select @StockLink = min(StockLink)
      from StkItem (nolock)
  end
  
  insert @TableResult
        ([xml])
  select N'<Product><StockLink>' + convert(nvarchar(50), si.StockLink) + N'</StockLink>' + 
         N'<Code><![CDATA[' + si.Code + ']]></Code>' + 
         N'<Description_1><![CDATA[' + si.Description_1 + ']]></Description_1>' + 
         N'<Description_2><![CDATA[' + isnull(si.Description_2,'') + ']]></Description_2>' + 
         N'<Description_3><![CDATA[' + isnull(si.Description_3,'') + ']]></Description_3>' + 
         N'<PalletQuantity>' + '</PalletQuantity>' + 
         N'<OuterCartonBarcode>' + '</OuterCartonBarcode>' + 
         N'<CaseQuantity>' + '</CaseQuantity>' + 
         N'<BarCode>' + isnull(ws.WHBarCode,'') + '</BarCode>' + 
             N'<PackInfo>' + 
                 N'<idPckTbl>' + convert(nvarchar(50), isnull(pk.idPckTbl,'')) + '</idPckTbl>' + 
                 N'<Code>' + isnull(pk.Code,'') + N'</Code>' + 
                 N'<PackSize>' + convert(varchar(50), isnull(pk.PackSize,'')) + '</PackSize>' + 
                 N'<Description>' + isnull(pk.Description,'') + '</Description>' + 
             N'</PackInfo>' + 
         N'</Product>'
    from StkItem si
    left
    join WhseStk ws on ws.WHStockLink = si.StockLink
                   and ws.WHWhseID = 1
    left
    join PckTbl  pk on si.Pack        = pk.Code
   where si.StockLink in (select top 1000 StockLink from StkItem (nolock) where StockLink >= @StockLink order by StockLink)
  
  update StkItem
     set ubIIcqExport = 1
   where StockLink in (select top 1000 StockLink from StkItem (nolock) where StockLink >= @StockLink order by StockLink)
  
  set @xml = '<root>' + isnull(@xml,'')
  
  while exists(select 1 from @TableResult)
  begin
    select @Id = min(Id) from @TableResult
    
    select @xml = @xml + isnull([xml],'')
      from @TableResult
     where Id = @Id
    
    delete @TableResult
     where Id = @Id
  end
  
  set @xml = @xml + N'</root>'
end
 
