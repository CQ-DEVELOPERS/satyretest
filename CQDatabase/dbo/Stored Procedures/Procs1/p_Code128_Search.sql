﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Code128_Search
  ///   Filename       : p_Code128_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:28
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Code128 table.
  /// </remarks>
  /// <param>
  ///   @Code128 nvarchar(100) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Code128.Code128,
  ///   Code128.Char,
  ///   Code128.Variant,
  ///   Code128.AsciiNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Code128_Search
(
 @Code128 nvarchar(100) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @Code128 = '-1'
    set @Code128 = null;
  
 
  select
         Code128.Code128
        ,Code128.Char
        ,Code128.Variant
        ,Code128.AsciiNumber
    from Code128
   where isnull(Code128.Code128,'%')  like '%' + isnull(@Code128, isnull(Code128.Code128,'%')) + '%'
  
end
