﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPONum_Search
  ///   Filename       : p_InterfaceImportPONum_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:01
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportPONum table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportPONum.id,
  ///   InterfaceImportPONum.Ordernumber,
  ///   InterfaceImportPONum.ReferenceNumber,
  ///   InterfaceImportPONum.docstate,
  ///   InterfaceImportPONum.statusId,
  ///   InterfaceImportPONum.OrderDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPONum_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportPONum.id
        ,InterfaceImportPONum.Ordernumber
        ,InterfaceImportPONum.ReferenceNumber
        ,InterfaceImportPONum.docstate
        ,InterfaceImportPONum.statusId
        ,InterfaceImportPONum.OrderDate
    from InterfaceImportPONum
  
end
