﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Check_Valid_Invoice
  ///   Filename       : p_InboundDocument_Check_Valid_Invoice.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Feb 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Check_Valid_Invoice
(
 @referenceNumber nvarchar(30)
)

as
begin
	 set nocount on;
  
  declare @bool bit
  
  set @bool = 0
  
  select @bool = 1
    from InterfaceInvoice (nolock)
   where InvoiceNumber = @referenceNumber
  
  if @bool = 0
    select @bool = 1
      from OutboundDocument (nolock)
     where ReferenceNumber = @referenceNumber
  
  select @bool
end
