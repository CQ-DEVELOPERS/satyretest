﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExceptionCode
  ///   Filename       : p_ExceptionCode.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExceptionCode

as
begin
  set nocount on;
	 
  Select distinct ExceptionCode
   	from Exception
  union select '{All}'
  order by ExceptionCode
end
