﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoad_Parameter
  ///   Filename       : p_IntransitLoad_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:33
  /// </summary>
  /// <remarks>
  ///   Selects rows from the IntransitLoad table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   IntransitLoad.IntransitLoadId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoad_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as IntransitLoadId
        ,null as 'IntransitLoad'
  union
  select
         IntransitLoad.IntransitLoadId
        ,IntransitLoad.IntransitLoadId as 'IntransitLoad'
    from IntransitLoad
  
end
