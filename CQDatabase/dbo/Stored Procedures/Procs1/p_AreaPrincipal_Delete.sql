﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaPrincipal_Delete
  ///   Filename       : p_AreaPrincipal_Delete.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2011
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the AreaPrincipal table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_AreaPrincipal_Delete
(
	@PrincipalId		int = null,
	@AreaId 			int = null
)
as
begin
	 set nocount on
	 declare @Error int


  delete AreaPrincipal
     where @PrincipalId = PrincipalId and @AreaId = AreaId
  
  select @Error = @@Error
   
end
