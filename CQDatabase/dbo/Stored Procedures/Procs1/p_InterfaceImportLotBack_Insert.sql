﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportLotBack_Insert
  ///   Filename       : p_InterfaceImportLotBack_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:43
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportLotBack table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportLotId int = null,
  ///   @RecordStatus nvarchar(4) = null,
  ///   @InsertDate datetime = null,
  ///   @ProcessedDate datetime = null,
  ///   @LotId nvarchar(100) = null,
  ///   @Code nvarchar(510) = null,
  ///   @WhseId nvarchar(100) = null,
  ///   @Whse nvarchar(510) = null,
  ///   @QtyOnHand nvarchar(100) = null,
  ///   @QtyFree nvarchar(100) = null,
  ///   @LotStatusId nvarchar(100) = null,
  ///   @LotStatus nvarchar(200) = null,
  ///   @ExpiryDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportLotBack.InterfaceImportLotId,
  ///   InterfaceImportLotBack.RecordStatus,
  ///   InterfaceImportLotBack.InsertDate,
  ///   InterfaceImportLotBack.ProcessedDate,
  ///   InterfaceImportLotBack.LotId,
  ///   InterfaceImportLotBack.Code,
  ///   InterfaceImportLotBack.WhseId,
  ///   InterfaceImportLotBack.Whse,
  ///   InterfaceImportLotBack.QtyOnHand,
  ///   InterfaceImportLotBack.QtyFree,
  ///   InterfaceImportLotBack.LotStatusId,
  ///   InterfaceImportLotBack.LotStatus,
  ///   InterfaceImportLotBack.ExpiryDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportLotBack_Insert
(
 @InterfaceImportLotId int = null,
 @RecordStatus nvarchar(4) = null,
 @InsertDate datetime = null,
 @ProcessedDate datetime = null,
 @LotId nvarchar(100) = null,
 @Code nvarchar(510) = null,
 @WhseId nvarchar(100) = null,
 @Whse nvarchar(510) = null,
 @QtyOnHand nvarchar(100) = null,
 @QtyFree nvarchar(100) = null,
 @LotStatusId nvarchar(100) = null,
 @LotStatus nvarchar(200) = null,
 @ExpiryDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportLotBack
        (InterfaceImportLotId,
         RecordStatus,
         InsertDate,
         ProcessedDate,
         LotId,
         Code,
         WhseId,
         Whse,
         QtyOnHand,
         QtyFree,
         LotStatusId,
         LotStatus,
         ExpiryDate)
  select @InterfaceImportLotId,
         @RecordStatus,
         isnull(@InsertDate, getdate()),
         @ProcessedDate,
         @LotId,
         @Code,
         @WhseId,
         @Whse,
         @QtyOnHand,
         @QtyFree,
         @LotStatusId,
         @LotStatus,
         @ExpiryDate 
  
  select @Error = @@Error
  
  
  return @Error
  
end
