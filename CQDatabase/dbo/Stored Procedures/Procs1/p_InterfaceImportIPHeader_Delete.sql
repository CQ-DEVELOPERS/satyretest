﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIPHeader_Delete
  ///   Filename       : p_InterfaceImportIPHeader_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:33
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceImportIPHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportIPHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIPHeader_Delete
(
 @InterfaceImportIPHeaderId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceImportIPHeader
     where InterfaceImportIPHeaderId = @InterfaceImportIPHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
