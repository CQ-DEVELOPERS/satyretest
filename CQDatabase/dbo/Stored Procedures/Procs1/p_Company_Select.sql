﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Company_Select
  ///   Filename       : p_Company_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:52
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Company table.
  /// </remarks>
  /// <param>
  ///   @CompanyId int = null 
  /// </param>
  /// <returns>
  ///   Company.CompanyId,
  ///   Company.Company,
  ///   Company.CompanyCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Company_Select
(
 @CompanyId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Company.CompanyId
        ,Company.Company
        ,Company.CompanyCode
    from Company
   where isnull(Company.CompanyId,'0')  = isnull(@CompanyId, isnull(Company.CompanyId,'0'))
  
end
