﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Search
  ///   Filename       : p_InboundDocument_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Feb 2013 14:47:31
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output,
  ///   @InboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @ReasonId int = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @DivisionId int = null,
  ///   @PrincipalId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InboundDocument.InboundDocumentId,
  ///   InboundDocument.InboundDocumentTypeId,
  ///   InboundDocumentType.InboundDocumentType,
  ///   InboundDocument.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompany,
  ///   InboundDocument.StatusId,
  ///   Status.Status,
  ///   InboundDocument.WarehouseId,
  ///   Warehouse.Warehouse,
  ///   InboundDocument.OrderNumber,
  ///   InboundDocument.DeliveryDate,
  ///   InboundDocument.CreateDate,
  ///   InboundDocument.ModifiedDate,
  ///   InboundDocument.ReferenceNumber,
  ///   InboundDocument.FromLocation,
  ///   InboundDocument.ToLocation,
  ///   InboundDocument.ReasonId,
  ///   Reason.Reason,
  ///   InboundDocument.Remarks,
  ///   InboundDocument.StorageUnitId,
  ///   StorageUnit.StorageUnitId,
  ///   InboundDocument.BatchId,
  ///   Batch.Batch,
  ///   InboundDocument.DivisionId,
  ///   Division.Division,
  ///   InboundDocument.PrincipalId 
  ///   Principal.Principal 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Search
(
 @InboundDocumentId int = null output,
 @InboundDocumentTypeId int = null,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @ReasonId int = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @DivisionId int = null,
 @PrincipalId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InboundDocumentId = '-1'
    set @InboundDocumentId = null;
  
  if @InboundDocumentTypeId = '-1'
    set @InboundDocumentTypeId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @DivisionId = '-1'
    set @DivisionId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
 
  select
         InboundDocument.InboundDocumentId
        ,InboundDocument.InboundDocumentTypeId
         ,InboundDocumentTypeInboundDocumentTypeId.InboundDocumentType as 'InboundDocumentType'
        ,InboundDocument.ExternalCompanyId
         ,ExternalCompanyExternalCompanyId.ExternalCompany as 'ExternalCompany'
        ,InboundDocument.StatusId
         ,StatusStatusId.Status as 'Status'
        ,InboundDocument.WarehouseId
         ,WarehouseWarehouseId.Warehouse as 'Warehouse'
        ,InboundDocument.OrderNumber
        ,InboundDocument.DeliveryDate
        ,InboundDocument.CreateDate
        ,InboundDocument.ModifiedDate
        ,InboundDocument.ReferenceNumber
        ,InboundDocument.FromLocation
        ,InboundDocument.ToLocation
        ,InboundDocument.ReasonId
         ,ReasonReasonId.Reason as 'Reason'
        ,InboundDocument.Remarks
        ,InboundDocument.StorageUnitId
        ,InboundDocument.BatchId
         ,BatchBatchId.Batch as 'Batch'
        ,InboundDocument.DivisionId
         ,DivisionDivisionId.Division as 'Division'
        ,InboundDocument.PrincipalId
         ,PrincipalPrincipalId.Principal as 'Principal'
    from InboundDocument
    left
    join InboundDocumentType InboundDocumentTypeInboundDocumentTypeId on InboundDocumentTypeInboundDocumentTypeId.InboundDocumentTypeId = InboundDocument.InboundDocumentTypeId
    left
    join ExternalCompany ExternalCompanyExternalCompanyId on ExternalCompanyExternalCompanyId.ExternalCompanyId = InboundDocument.ExternalCompanyId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = InboundDocument.StatusId
    left
    join Warehouse WarehouseWarehouseId on WarehouseWarehouseId.WarehouseId = InboundDocument.WarehouseId
    left
    join Reason ReasonReasonId on ReasonReasonId.ReasonId = InboundDocument.ReasonId
    left
    join StorageUnit StorageUnitStorageUnitId on StorageUnitStorageUnitId.StorageUnitId = InboundDocument.StorageUnitId
    left
    join Batch BatchBatchId on BatchBatchId.BatchId = InboundDocument.BatchId
    left
    join Division DivisionDivisionId on DivisionDivisionId.DivisionId = InboundDocument.DivisionId
    left
    join Principal PrincipalPrincipalId on PrincipalPrincipalId.PrincipalId = InboundDocument.PrincipalId
   where isnull(InboundDocument.InboundDocumentId,'0')  = isnull(@InboundDocumentId, isnull(InboundDocument.InboundDocumentId,'0'))
     and isnull(InboundDocument.InboundDocumentTypeId,'0')  = isnull(@InboundDocumentTypeId, isnull(InboundDocument.InboundDocumentTypeId,'0'))
     and isnull(InboundDocument.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(InboundDocument.ExternalCompanyId,'0'))
     and isnull(InboundDocument.StatusId,'0')  = isnull(@StatusId, isnull(InboundDocument.StatusId,'0'))
     and isnull(InboundDocument.WarehouseId,'0')  = isnull(@WarehouseId, isnull(InboundDocument.WarehouseId,'0'))
     and isnull(InboundDocument.ReasonId,'0')  = isnull(@ReasonId, isnull(InboundDocument.ReasonId,'0'))
     and isnull(InboundDocument.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(InboundDocument.StorageUnitId,'0'))
     and isnull(InboundDocument.BatchId,'0')  = isnull(@BatchId, isnull(InboundDocument.BatchId,'0'))
     and isnull(InboundDocument.DivisionId,'0')  = isnull(@DivisionId, isnull(InboundDocument.DivisionId,'0'))
     and isnull(InboundDocument.PrincipalId,'0')  = isnull(@PrincipalId, isnull(InboundDocument.PrincipalId,'0'))
  order by OrderNumber
  
end
