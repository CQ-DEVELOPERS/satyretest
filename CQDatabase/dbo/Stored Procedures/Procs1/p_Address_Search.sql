﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Address_Search
  ///   Filename       : p_Address_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:20
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Address table.
  /// </remarks>
  /// <param>
  ///   @AddressId int = null output,
  ///   @ExternalCompanyId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Address.AddressId,
  ///   Address.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompany,
  ///   Address.Street,
  ///   Address.Suburb,
  ///   Address.Town,
  ///   Address.Country,
  ///   Address.Code 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Address_Search
(
 @AddressId int = null output,
 @ExternalCompanyId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @AddressId = '-1'
    set @AddressId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
 
  select
         Address.AddressId
        ,Address.ExternalCompanyId
         ,ExternalCompanyExternalCompanyId.ExternalCompany as 'ExternalCompany'
        ,Address.Street
        ,Address.Suburb
        ,Address.Town
        ,Address.Country
        ,Address.Code
    from Address
    left
    join ExternalCompany ExternalCompanyExternalCompanyId on ExternalCompanyExternalCompanyId.ExternalCompanyId = Address.ExternalCompanyId
   where isnull(Address.AddressId,'0')  = isnull(@AddressId, isnull(Address.AddressId,'0'))
     and isnull(Address.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(Address.ExternalCompanyId,'0'))
  
end
