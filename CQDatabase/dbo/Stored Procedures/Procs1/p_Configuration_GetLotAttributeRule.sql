﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_GetLotAttributeRule
  ///   Filename       : p_Configuration_GetLotAttributeRule.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_GetLotAttributeRule
(
 @receiptLineId int = null,
 @storageUnitId int = null
)

as
begin
  declare @result NVarChar(50)
  
  if @receiptLineId = -1
	   set @receiptLineId = null

  if @storageUnitId = -1
	   set @storageUnitId = null

  if @storageUnitId is not null
	  select @result = su.LotAttributeRule
		   from StorageUnit       su (nolock)
	   where su.StorageUnitId = @storageUnitId
else
  if @receiptLineId is not null
	   select @result = case when idt.InboundDocumentTypeCode = 'BOND'
	                         then 'CreateOnReceipt'
	                         else su.LotAttributeRule
	                         end
		    from ReceiptLine       rl (nolock)
		    join Receipt            r (nolock) on rl.ReceiptId = r.ReceiptId
		    join InboundDocument   id (nolock) on r.InboundDocumentId = id.InboundDocumentId
		    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
		    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
		    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
	    where rl.ReceiptLineId = @receiptLineId
  
  select @result '@result'
end
 
 
