﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperatorGroup_Insert
  ///   Filename       : p_AreaOperatorGroup_Insert.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2011
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the AreaOperatorGroup table.
  /// </remarks>
  /// <param>

  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperatorGroup_Insert
(	@OperatorGroupId		int = null,
	@AreaId 			    int = null
)
    
    

as
begin
	 set nocount on;
  
  declare @Error VARCHAR(255)
  
 
	  if not exists(select top 1 1 from AreaOperatorGroup aog where @OperatorGroupId = aog.OperatorGroupId and @AreaId = aog.AreaId)
	  insert	AreaOperatorGroup
			   (AreaId,
				OperatorGroupId)
	  select	@AreaId,
				@OperatorGroupId	  
    
  SELECT @Error
end
