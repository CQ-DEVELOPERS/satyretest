﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_Create
  ///   Filename       : p_InboundShipment_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_Create
(
 @WarehouseId       int,
 @ShipmentDate      datetime,
 @Remarks           nvarchar(255),
 @InboundShipmentId int = -1 output
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  select @StatusId = StatusId
    from Status
   where StatusCode = 'W'
     and Type       = 'R'
  
  exec @Error = p_InboundShipment_Insert
   @InboundShipmentId = @InboundShipmentId output,
   @StatusId          = @StatusId,
   @WarehouseId       = @WarehouseId,
   @ShipmentDate      = @ShipmentDate,
   @Remarks           = @Remarks
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_InboundShipment_Create'); 
    rollback transaction
    return @Error
end
