﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMProduct_List
  ///   Filename       : p_BOMProduct_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:14
  /// </summary>
  /// <remarks>
  ///   Selects rows from the BOMProduct table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   BOMProduct.BOMLineId,
  ///   BOMProduct.LineNumber,
  ///   BOMProduct.StorageUnitId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMProduct_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as BOMLineId
        ,null as 'BOMProduct'
        ,'-1' as LineNumber
        ,null as 'BOMProduct'
        ,'-1' as StorageUnitId
        ,null as 'BOMProduct'
  union
  select
         BOMProduct.BOMLineId
        ,BOMProduct.BOMLineId as 'BOMProduct'
        ,BOMProduct.LineNumber
        ,BOMProduct.LineNumber as 'BOMProduct'
        ,BOMProduct.StorageUnitId
        ,BOMProduct.StorageUnitId as 'BOMProduct'
    from BOMProduct
  
end
