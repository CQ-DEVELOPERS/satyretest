﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COATest_List
  ///   Filename       : p_COATest_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:09
  /// </summary>
  /// <remarks>
  ///   Selects rows from the COATest table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   COATest.COATestId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COATest_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as COATestId
        ,null as 'COATest'
  union
  select
         COATest.COATestId
        ,COATest.COATestId as 'COATest'
    from COATest
  
end
