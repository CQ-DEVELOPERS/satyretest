﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceField_Delete
  ///   Filename       : p_InterfaceField_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:46:47
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceField table.
  /// </remarks>
  /// <param>
  ///   @InterfaceFieldId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceField_Delete
(
 @InterfaceFieldId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceField
     where InterfaceFieldId = @InterfaceFieldId
  
  select @Error = @@Error
  
  
  return @Error
  
end
