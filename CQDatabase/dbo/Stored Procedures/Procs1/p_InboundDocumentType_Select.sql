﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentType_Select
  ///   Filename       : p_InboundDocumentType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 10:28:18
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundDocumentType table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentTypeId int = null 
  /// </param>
  /// <returns>
  ///   InboundDocumentType.InboundDocumentTypeId,
  ///   InboundDocumentType.InboundDocumentTypeCode,
  ///   InboundDocumentType.InboundDocumentType,
  ///   InboundDocumentType.OverReceiveIndicator,
  ///   InboundDocumentType.AllowManualCreate,
  ///   InboundDocumentType.QuantityToFollowIndicator,
  ///   InboundDocumentType.PriorityId,
  ///   InboundDocumentType.AutoSendup,
  ///   InboundDocumentType.BatchButton,
  ///   InboundDocumentType.BatchSelect,
  ///   InboundDocumentType.DeliveryNoteNumber,
  ///   InboundDocumentType.VehicleRegistration,
  ///   InboundDocumentType.AreaType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentType_Select
(
 @InboundDocumentTypeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InboundDocumentType.InboundDocumentTypeId
        ,InboundDocumentType.InboundDocumentTypeCode
        ,InboundDocumentType.InboundDocumentType
        ,InboundDocumentType.OverReceiveIndicator
        ,InboundDocumentType.AllowManualCreate
        ,InboundDocumentType.QuantityToFollowIndicator
        ,InboundDocumentType.PriorityId
        ,InboundDocumentType.AutoSendup
        ,InboundDocumentType.BatchButton
        ,InboundDocumentType.BatchSelect
        ,InboundDocumentType.DeliveryNoteNumber
        ,InboundDocumentType.VehicleRegistration
        ,InboundDocumentType.AreaType
    from InboundDocumentType
   where isnull(InboundDocumentType.InboundDocumentTypeId,'0')  = isnull(@InboundDocumentTypeId, isnull(InboundDocumentType.InboundDocumentTypeId,'0'))
  order by InboundDocumentTypeCode
  
end
