﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Credit_Advice_Search_Reject
  ///   Filename       : p_Credit_Advice_Search_Reject.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Credit_Advice_Search_Reject
(
 @ReceiptLineId   int,
 @RejectIndicator bit = 0
)

as
begin
	 set nocount on;
  
  declare @TableResult as
  table(
        ReceiptLineId             int,
        InboundLineId             int,
        ExternalCompanyId         int,
        SupplierCode              nvarchar(30),
        Supplier                  nvarchar(255),
        OrderNumber               nvarchar(30),
        DeliveryDate              datetime,
        DeliveryNoteNumber        nvarchar(30),
        Quantity                  float,
        DeliveryNoteQuantity      float,
        ReceivedQuantity          float,
        RejectQuantity            float,
        ReasonId                  int,
        Reason                    nvarchar(50),
        Exception                 nvarchar(255)
       );
  
  declare @StatusId int
  
  select @StatusId = StatusId
    from Status
   where StatusCode = 'R'
     and Type       = 'R'
  
  insert @TableResult
        (ReceiptLineId,
         InboundLineId,
         DeliveryDate,
         DeliveryNoteNumber,
         DeliveryNoteQuantity,
         ReceivedQuantity,
         RejectQuantity)
  select rl.ReceiptLineId,
         rl.InboundLineId,
         r.DeliveryDate,
         r.DeliveryNoteNumber,
         rl.DeliveryNoteQuantity,
         rl.ReceivedQuantity,
         rl.RejectQuantity
    from ReceiptLine     rl (nolock)
    join Receipt          r  (nolock) on rl.ReceiptId = r.ReceiptId
   where rl.ReceiptLineId = @ReceiptLineId
--     and r.StatusId = @StatusId
  
  update r
     set OrderNumber       = id.OrderNumber,
         Quantity          = il.Quantity,
         ExternalCompanyId = id.ExternalCompanyId
    from @TableResult             r
    join InboundLine              il (nolock) on r.InboundLineId = il.InboundLineId
    join InboundDocument          id (nolock) on il.InboundDocumentId = id.InboundDocumentId
  
  update r
     set SupplierCode = ec.ExternalCompanyCode,
         Supplier     = ec.ExternalCompany
    from @TableResult             r
    join ExternalCompany ec (nolock) on r.ExternalCompanyId = ec.ExternalCompanyId
  
  update t
     set ReasonId   = r.ReasonId,
         Reason     = r.Reason,
         Exception  = e.Exception
    from @TableResult t
    join Exception    e (nolock) on t.ReceiptLineId = e.ReceiptLineId
    left outer
    join Reason       r (nolock) on e.ReasonId      = r.ReasonId
   where ExceptionCode = 'REJECT'
  
  select ReceiptLineId,
         SupplierCode,
         Supplier,
         OrderNumber,
         DeliveryDate,
         DeliveryNoteNumber,
         Quantity,
         DeliveryNoteQuantity,
         ReceivedQuantity,
         RejectQuantity,
         isnull(ReasonId,-1) as 'ReasonId',
         Reason,
         Exception
    from @TableResult
end
