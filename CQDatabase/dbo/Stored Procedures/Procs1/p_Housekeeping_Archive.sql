﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Archive
  ///   Filename       : p_Housekeeping_Archive.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jun 2010
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Archive

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @Rowcount           int,
          @ArchiveDate        datetime,
          @OutboundShipmentId int,
          @OutboundDocumentId int
  
  select @ArchiveDate = '2009-04-02'
  
  declare @OutboundDocument as table
  (
   OutboundShipmentId int,
   OutboundDocumentId int,
   OutboundLineId     int,
   IssueId            int,
   IssueLineId        int
  )
  
  select top 1
         @OutboundShipmentId = osi.OutboundShipmentId,
         @OutboundDocumentId = od.OutboundDocumentId
    from OutboundDocument       od (nolock)
    join Issue                   i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
    join OutboundShipmentIssue osi (nolock) on i.IssueId            = osi.IssueId
   where od.CreateDate < @ArchiveDate
  
  -- Archive Picking
  if @OutboundShipmentId is null
  begin
    insert @OutboundDocument
    select null,
           od.OutboundDocumentId,
           ol.OutboundLineId,
           i.IssueId,
           il.IssueLineId
      from OutboundDocument       od
      join OutboundLine           ol on od.OutboundDocumentId = ol.OutboundDocumentId
      join Issue                   i on od.OutboundDocumentId = i.OutboundDocumentId
      join IssueLine              il on i.IssueId             = il.IssueId
                                    and ol.OutboundLineId     = il.OutboundLineId
     where od.OutboundDocumentId = @OutboundDocumentId
  end
  else
  begin
    insert @OutboundDocument
    select osi.OutboundShipmentId,
           od.OutboundDocumentId,
           ol.OutboundLineId,
           i.IssueId,
           il.IssueLineId
      from OutboundDocument       od
      join OutboundLine           ol on od.OutboundDocumentId = ol.OutboundDocumentId
      join Issue                   i on od.OutboundDocumentId = i.OutboundDocumentId
      join IssueLine              il on i.IssueId             = il.IssueId
                                    and ol.OutboundLineId     = il.OutboundLineId
      join OutboundShipmentIssue osi on i.IssueId = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
  end
  
  begin transaction
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  select JobId
    into #Jobs
    from @OutboundDocument od
    join IssueLineInstruction ili on od.IssueLineId = ili.IssueLineId
    join Instruction            i on ili.InstructionId = i.InstructionId
  
  select i.InstructionId
    into #Instructions
    from #Jobs       j
    join Instruction i on j.JobId = i.JobId
  
  insert ProminentPaintsArchive..Exception
        (ExceptionId,
         ReceiptLineId,
         InstructionId,
         IssueLineId,
         ReasonId,
         Exception,
         ExceptionCode,
         CreateDate,
         OperatorId,
         ExceptionDate,
         JobId,
         Detail)
  select distinct
         live.ExceptionId,
         live.ReceiptLineId,
         live.InstructionId,
         live.IssueLineId,
         live.ReasonId,
         live.Exception,
         live.ExceptionCode,
         live.CreateDate,
         live.OperatorId,
         live.ExceptionDate,
         live.JobId,
         live.Detail
    from Exception live 
   where Live.IssueLineId in (select IssueLineId from @OutboundDocument)
  union
  select distinct
         live.ExceptionId,
         live.ReceiptLineId,
         live.InstructionId,
         live.IssueLineId,
         live.ReasonId,
         live.Exception,
         live.ExceptionCode,
         live.CreateDate,
         live.OperatorId,
         live.ExceptionDate,
         live.JobId,
         live.Detail
    from Exception live
   where live.JobId in (select JobId from #Jobs)
  union
  select distinct
         live.ExceptionId,
         live.ReceiptLineId,
         live.InstructionId,
         live.IssueLineId,
         live.ReasonId,
         live.Exception,
         live.ExceptionCode,
         live.CreateDate,
         live.OperatorId,
         live.ExceptionDate,
         live.JobId,
         live.Detail
    from Exception live
   where live.InstructionId in (select InstructionId from #Instructions)
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from ProminentPaints..Exception    live
    join ProminentPaintsArchive..Exception archive on live.ExceptionId = archive.ExceptionId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..IssueLineInstruction
        (OutboundDocumentTypeId,
         OutboundShipmentId,
         OutboundDocumentId,
         IssueId,
         IssueLineId,
         InstructionId,
         DropSequence,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight)
  select distinct
         live.OutboundDocumentTypeId,
         live.OutboundShipmentId,
         live.OutboundDocumentId,
         live.IssueId,
         live.IssueLineId,
         live.InstructionId,
         live.DropSequence,
         live.Quantity,
         live.ConfirmedQuantity,
         live.Weight,
         live.ConfirmedWeight
    from @OutboundDocument    t
    join IssueLineInstruction live on t.IssueLineId = live.IssueLineId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @OutboundDocument    t
    join IssueLineInstruction live on t.IssueLineId = live.IssueLineId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  select getdate()
  insert ProminentPaintsArchive..Instruction
        (InstructionId,
         ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         ReceiptLineId,
         IssueLineId,
         InstructionRefId,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         CheckQuantity,
         CheckWeight,
         OutboundShipmentId,
         DropSequence)
  select distinct
         live.InstructionId,
         live.ReasonId,
         live.InstructionTypeId,
         live.StorageUnitBatchId,
         live.WarehouseId,
         live.StatusId,
         live.JobId,
         live.OperatorId,
         live.PickLocationId,
         live.StoreLocationId,
         live.ReceiptLineId,
         live.IssueLineId,
         live.InstructionRefId,
         live.Quantity,
         live.ConfirmedQuantity,
         live.Weight,
         live.ConfirmedWeight,
         live.PalletId,
         live.CreateDate,
         live.StartDate,
         live.EndDate,
         live.Picked,
         live.Stored,
         live.CheckQuantity,
         live.CheckWeight,
         live.OutboundShipmentId,
         live.DropSequence
    from #Instructions t
    join Instruction   live on t.InstructionId = live.InstructionId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from ProminentPaints..Instruction           live
    join ProminentPaintsArchive..Instruction archive on live.InstructionId = archive.InstructionId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..SKUCheck
        (JobId,
         SKUCode,
         Quantity,
         ConfirmedQuantity)
  select distinct
         live.JobId,
         live.SKUCode,
         live.Quantity,
         live.ConfirmedQuantity
    from #Jobs t
    join SKUCheck live on t.JobId = live.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from ProminentPaints..SKUCheck           live
    join ProminentPaintsArchive..SKUCheck archive on live.JobId = archive.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..OperatorQA
        (OperatorId,
         EndDate,
         JobId,
         QA,
         InstructionTypeId,
         ActivityStatus,
         PickingRate,
         Units,
         Weight,
         Instructions,
         Orders,
         OrderLines,
         Jobs,
         ActiveTime,
         DwellTime)
  select distinct
         live.OperatorId,
         live.EndDate,
         live.JobId,
         live.QA,
         live.InstructionTypeId,
         live.ActivityStatus,
         live.PickingRate,
         live.Units,
         live.Weight,
         live.Instructions,
         live.Orders,
         live.OrderLines,
         live.Jobs,
         live.ActiveTime,
         live.DwellTime
    from #Jobs t
    join OperatorQA live on t.JobId = live.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from ProminentPaints..OperatorQA           live
    join ProminentPaintsArchive..OperatorQA archive on live.JobId = archive.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..Job
        (JobId,
         PriorityId,
         OperatorId,
         StatusId,
         WarehouseId,
         ReceiptLineId,
         IssueLineId,
         ContainerTypeId,
         ReferenceNumber,
         TareWeight,
         Weight)
  select distinct
         live.JobId,
         live.PriorityId,
         live.OperatorId,
         live.StatusId,
         live.WarehouseId,
         live.ReceiptLineId,
         live.IssueLineId,
         live.ContainerTypeId,
         live.ReferenceNumber,
         live.TareWeight,
         live.Weight
    from #Jobs t
    join Job   live on t.JobId = live.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from ProminentPaints..Job    live
    join ProminentPaintsArchive..Job archive on live.JobId = archive.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..OutboundShipmentIssue
        (OutboundShipmentId,
         IssueId,
         DropSequence)
  select distinct
         live.OutboundShipmentId,
         live.IssueId,
         live.DropSequence
    from @OutboundDocument     t
    join OutboundShipmentIssue live on t.OutboundShipmentId = live.OutboundShipmentId
                                   and t.IssueId            = live.IssueId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @OutboundDocument        t
    join OutboundShipmentIssue live on t.OutboundShipmentId = live.OutboundShipmentId
                                   and t.IssueId            = live.IssueId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..OutboundShipment
        (OutboundShipmentId,
         StatusId,
         WarehouseId,
         LocationId,
         ShipmentDate,
         Remarks,
         SealNumber,
         VehicleRegistration,
         Route,
         RouteId,
         Weight,
         Volume,
         FP,
         SS,
         LP,
         FM,
         MP,
         PP,
         AlternatePallet,
         SubstitutePallet,
         DespatchBay)
  select distinct
         live.OutboundShipmentId,
         live.StatusId,
         live.WarehouseId,
         live.LocationId,
         live.ShipmentDate,
         live.Remarks,
         live.SealNumber,
         live.VehicleRegistration,
         live.Route,
         live.RouteId,
         live.Weight,
         live.Volume,
         live.FP,
         live.SS,
         live.LP,
         live.FM,
         live.MP,
         live.PP,
         live.AlternatePallet,
         live.SubstitutePallet,
         live.DespatchBay
    from @OutboundDocument   t
    join OutboundShipment live on t.OutboundShipmentId = live.OutboundShipmentId
   where not exists(select top 1 1 from ProminentPaintsArchive..OutboundShipment archive where archive.OutboundShipmentId = live.OutboundShipmentId)
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @OutboundDocument       t
    join OutboundShipment     live on t.OutboundShipmentId = live.OutboundShipmentId
   where not exists(select 1 from OutboundShipmentIssue osi where live.OutboundShipmentId = osi.OutboundShipmentId)
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..IssueLine
        (IssueLineId,
         IssueId,
         OutboundLineId,
         StorageUnitBatchId,
         StatusId,
         OperatorId,
         Quantity,
         ConfirmedQuatity)
  select distinct
         live.IssueLineId,
         live.IssueId,
         live.OutboundLineId,
         live.StorageUnitBatchId,
         live.StatusId,
         live.OperatorId,
         live.Quantity,
         live.ConfirmedQuatity
    from ProminentPaints..IssueLine           live
    join ProminentPaintsArchive..IssueLine archive on live.IssueLineId = archive.IssueLineId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @OutboundDocument t
    join IssueLine      live on t.IssueLineId = live.IssueLineId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..Issue
        (IssueId,
         OutboundDocumentId,
         PriorityId,
         WarehouseId,
         LocationId,
         StatusId,
         OperatorId,
         RouteId,
         DeliveryNoteNumber,
         DeliveryDate,
         SealNumber,
         VehicleRegistration,
         Remarks,
         DropSequence,
         LoadIndicator,
         DespatchBay,
         RoutingSystem,
         Delivery,
         NumberOfLines,
         Total,
         Complete,
         AreaType)
  select distinct
         live.IssueId,
         live.OutboundDocumentId,
         live.PriorityId,
         live.WarehouseId,
         live.LocationId,
         live.StatusId,
         live.OperatorId,
         live.RouteId,
         live.DeliveryNoteNumber,
         live.DeliveryDate,
         live.SealNumber,
         live.VehicleRegistration,
         live.Remarks,
         live.DropSequence,
         live.LoadIndicator,
         live.DespatchBay,
         live.RoutingSystem,
         live.Delivery,
         live.NumberOfLines,
         live.Total,
         live.Complete,
         live.AreaType
    from @OutboundDocument t
    join Issue          live on t.IssueId = live.IssueId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @OutboundDocument t
    join Issue          live on t.IssueId = live.IssueId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..OutboundLine
        (OutboundLineId,
         OutboundDocumentId,
         StorageUnitId,
         StatusId,
         LineNumber,
         Quantity,
         BatchId)
  select distinct
         live.OutboundLineId,
         live.OutboundDocumentId,
         live.StorageUnitId,
         live.StatusId,
         live.LineNumber,
         live.Quantity,
         live.BatchId
    from @OutboundDocument t
    join OutboundLine   live on t.OutboundLineId = live.OutboundLineId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @OutboundDocument t
    join OutboundLine   live on t.OutboundLineId = live.OutboundLineId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error

  insert ProminentPaintsArchive..OutboundDocument
        (OutboundDocumentId,
         OutboundDocumentTypeId,
         ExternalCompanyId,
         StatusId,
         WarehouseId,
         OrderNumber,
         DeliveryDate,
         CreateDate,
         ModifiedDate,
         ReferenceNumber,
         FromLocation,
         ToLocation)
  select distinct
         live.OutboundDocumentId,
         live.OutboundDocumentTypeId,
         live.ExternalCompanyId,
         live.StatusId,
         live.WarehouseId,
         live.OrderNumber,
         live.DeliveryDate,
         live.CreateDate,
         live.ModifiedDate,
         live.ReferenceNumber,
         live.FromLocation,
         live.ToLocation
    from @OutboundDocument t
    join OutboundDocument  live on t.OutboundDocumentId = live.OutboundDocumentId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @OutboundDocument t
    join OutboundDocument  live on t.OutboundDocumentId = live.OutboundDocumentId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  commit transaction
  
  return
  
  begin transaction
  
  declare @InterfaceImportHeaderId int
  
  select @InterfaceImportHeaderId = max(InterfaceImportHeaderId) from InterfaceImportHeader where ProcessedDate < @ArchiveDate
  
  insert ProminentPaintsArchive..InterfaceImportDetail
  select *
    from InterfaceImportDetail
    where InterfaceImportHeaderId < @InterfaceImportHeaderId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..InterfaceImportHeader
  select *
    from InterfaceImportHeader
   where InterfaceImportHeaderId < @InterfaceImportHeaderId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete InterfaceImportDetail where InterfaceImportHeaderId < @InterfaceImportHeaderId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete InterfaceImportHeader where InterfaceImportHeaderId < @InterfaceImportHeaderId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  declare @InterfaceExportHeaderId int
  
  select @InterfaceExportHeaderId = max(InterfaceExportHeaderId) from InterfaceExportHeader where ProcessedDate < @ArchiveDate
  
  insert ProminentPaintsArchive..InterfaceExportDetail
  select *
    from InterfaceExportDetail
   where InterfaceExportHeaderId < @InterfaceExportHeaderId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..InterfaceExportHeader
  select *
    from InterfaceExportHeader
   where InterfaceExportHeaderId < @InterfaceExportHeaderId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete InterfaceExportDetail where InterfaceExportHeaderId < @InterfaceExportHeaderId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete InterfaceExportHeader where InterfaceExportHeaderId < @InterfaceExportHeaderId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  --insert XXX
  --      ()
  --select live.
  --  from #YYY t
  --  join ZZZ  live on t.AAA = live.AAA
  
  --select @Error = @@Error, @Rowcount = @@Rowcount
  
  --if @Error <> 0
  --  goto error
  
  commit transaction
  
  truncate table #jobs
  truncate table #Instructions
  
--------------------------------------------------------------------------------------------------------------------------------------
  declare @InboundDocument as table
  (
   InboundShipmentId int,
   InboundDocumentId int,
   InboundLineId     int,
   ReceiptId            int,
   ReceiptLineId        int
  )
  
  begin transaction
  
  -- Archive Receiving
  insert @InboundDocument
  select isr.InboundShipmentId,
         od.InboundDocumentId,
         ol.InboundLineId,
         i.ReceiptId,
         il.ReceiptLineId
    from InboundDocument       od
    join InboundLine           ol on od.InboundDocumentId = ol.InboundDocumentId
    join Receipt                i on od.InboundDocumentId = i.InboundDocumentId
    join ReceiptLine           il on i.ReceiptId          = il.ReceiptId
                                  and ol.InboundLineId    = il.InboundLineId
    left
    join InboundShipmentReceipt isr on i.ReceiptId = isr.ReceiptId
   where od.CreateDate < @ArchiveDate
     --and od.InboundDocumentId = 78793
  
  insert #jobs
  select JobId
    from @InboundDocument od
    join Instruction       i on od.ReceiptLineId = i.ReceiptLineId
  
  insert #Instructions
  select i.InstructionId
    from #Jobs       j
    join Instruction i on j.JobId = i.JobId
  
  insert ProminentPaintsArchive..Exception
        (ExceptionId,
         ReceiptLineId,
         InstructionId,
         IssueLineId,
         ReasonId,
         Exception,
         ExceptionCode,
         CreateDate,
         OperatorId,
         ExceptionDate,
         JobId,
         Detail)
  select distinct
         live.ExceptionId,
         live.ReceiptLineId,
         live.InstructionId,
         live.IssueLineId,
         live.ReasonId,
         live.Exception,
         live.ExceptionCode,
         live.CreateDate,
         live.OperatorId,
         live.ExceptionDate,
         live.JobId,
         live.Detail
    from @InboundDocument t
    join Exception      live on t.ReceiptLineId = Live.ReceiptLineId
  union
  select distinct
         live.ExceptionId,
         live.ReceiptLineId,
         live.InstructionId,
         live.ReceiptLineId,
         live.ReasonId,
         live.Exception,
         live.ExceptionCode,
         live.CreateDate,
         live.OperatorId,
         live.ExceptionDate,
         live.JobId,
         live.Detail
    from #Jobs     t
    join Exception live on t.JobId = live.JobId
  union
  select distinct
         live.ExceptionId,
         live.ReceiptLineId,
         live.InstructionId,
         live.ReceiptLineId,
         live.ReasonId,
         live.Exception,
         live.ExceptionCode,
         live.CreateDate,
         live.OperatorId,
         live.ExceptionDate,
         live.JobId,
         live.Detail
    from #Instructions t
    join Exception  live on t.InstructionId = live.InstructionId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from ProminentPaints..Exception    live
    join ProminentPaintsArchive..Exception archive on live.ExceptionId = archive.ExceptionId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..Instruction
        (InstructionId,
         ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         ReceiptLineId,
         IssueLineId,
         InstructionRefId,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         CheckQuantity,
         CheckWeight,
         OutboundShipmentId,
         DropSequence)
  select distinct
         live.InstructionId,
         live.ReasonId,
         live.InstructionTypeId,
         live.StorageUnitBatchId,
         live.WarehouseId,
         live.StatusId,
         live.JobId,
         live.OperatorId,
         live.PickLocationId,
         live.StoreLocationId,
         live.ReceiptLineId,
         live.IssueLineId,
         live.InstructionRefId,
         live.Quantity,
         live.ConfirmedQuantity,
         live.Weight,
         live.ConfirmedWeight,
         live.PalletId,
         live.CreateDate,
         live.StartDate,
         live.EndDate,
         live.Picked,
         live.Stored,
         live.CheckQuantity,
         live.CheckWeight,
         live.OutboundShipmentId,
         live.DropSequence
    from #Instructions t
    join Instruction   live on t.InstructionId = live.InstructionId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from ProminentPaints..Instruction           live
    join ProminentPaintsArchive..Instruction archive on live.InstructionId = archive.InstructionId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..SKUCheck
        (JobId,
         SKUCode,
         Quantity,
         ConfirmedQuantity)
  select distinct
         live.JobId,
         live.SKUCode,
         live.Quantity,
         live.ConfirmedQuantity
    from #Jobs t
    join SKUCheck live on t.JobId = live.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from ProminentPaints..SKUCheck           live
    join ProminentPaintsArchive..SKUCheck archive on live.JobId = archive.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..OperatorQA
        (OperatorId,
         EndDate,
         JobId,
         QA,
         InstructionTypeId,
         ActivityStatus,
         PickingRate,
         Units,
         Weight,
         Instructions,
         Orders,
         OrderLines,
         Jobs,
         ActiveTime,
         DwellTime)
  select distinct
         live.OperatorId,
         live.EndDate,
         live.JobId,
         live.QA,
         live.InstructionTypeId,
         live.ActivityStatus,
         live.PickingRate,
         live.Units,
         live.Weight,
         live.Instructions,
         live.Orders,
         live.OrderLines,
         live.Jobs,
         live.ActiveTime,
         live.DwellTime
    from #Jobs t
    join OperatorQA live on t.JobId = live.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  if @Rowcount > 0
  begin
    delete live
      from ProminentPaints..OperatorQA           live
      join ProminentPaintsArchive..OperatorQA archive on live.JobId = archive.JobId
    
    select @Error = @@Error, @Rowcount = @@Rowcount
    
    if @Error <> 0
      goto error
  end
  
  insert ProminentPaintsArchive..Job
        (JobId,
         PriorityId,
         OperatorId,
         StatusId,
         WarehouseId,
         ReceiptLineId,
         IssueLineId,
         ContainerTypeId,
         ReferenceNumber,
         TareWeight,
         Weight)
  select distinct
         live.JobId,
         live.PriorityId,
         live.OperatorId,
         live.StatusId,
         live.WarehouseId,
         live.ReceiptLineId,
         live.IssueLineId,
         live.ContainerTypeId,
         live.ReferenceNumber,
         live.TareWeight,
         live.Weight
    from #Jobs t
    join Job   live on t.JobId = live.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from ProminentPaints..Job    live
    join ProminentPaintsArchive..Job archive on live.JobId = archive.JobId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..InboundShipmentReceipt
        (InboundShipmentId,
         ReceiptId)
  select distinct
         live.InboundShipmentId,
         live.ReceiptId
    from @InboundDocument     t
    join InboundShipmentReceipt live on t.InboundShipmentId = live.InboundShipmentId
                                    and t.ReceiptId         = live.ReceiptId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @InboundDocument        t
    join InboundShipmentReceipt live on t.InboundShipmentId = live.InboundShipmentId
                                    and t.ReceiptId         = live.ReceiptId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..InboundShipment
        (InboundShipmentId,
         StatusId,
         WarehouseId,
         ShipmentDate,
         Remarks)
  select distinct
         live.InboundShipmentId,
         live.StatusId,
         live.WarehouseId,
         live.ShipmentDate,
         live.Remarks
    from @InboundDocument   t
    join InboundShipment live on t.InboundShipmentId = live.InboundShipmentId
   where not exists(select top 1 1 from ProminentPaintsArchive..InboundShipment archive where archive.InboundShipmentId = live.InboundShipmentId)
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @InboundDocument       t
    join InboundShipment     live on t.InboundShipmentId = live.InboundShipmentId
   where not exists(select 1 from InboundShipmentReceipt isr where live.InboundShipmentId = isr.InboundShipmentId)
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..ReceiptLine
        (ReceiptLineId,
         ReceiptId,
         InboundLineId,
         StorageUnitBatchId,
         StatusId,
         OperatorId,
         RequiredQuantity,
         ReceivedQuantity,
         AcceptedQuantity,
         RejectQuantity,
         DeliveryNoteQuantity)
  select distinct
         live.ReceiptLineId,
         live.ReceiptId,
         live.InboundLineId,
         live.StorageUnitBatchId,
         live.StatusId,
         live.OperatorId,
         live.RequiredQuantity,
         live.ReceivedQuantity,
         live.AcceptedQuantity,
         live.RejectQuantity,
         live.DeliveryNoteQuantity
    from @InboundDocument t
    join ReceiptLine      live on t.ReceiptLineId = live.ReceiptLineId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @InboundDocument t
    join ReceiptLine      live on t.ReceiptLineId = live.ReceiptLineId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..Receipt
        (ReceiptId,
         InboundDocumentId,
         PriorityId,
         WarehouseId,
         LocationId,
         StatusId,
         OperatorId,
         DeliveryNoteNumber,
         DeliveryDate,
         SealNumber,
         VehicleRegistration,
         Remarks,
         CreditAdviceIndicator,
         Delivery,
         AllowPalletise,
         Interfaced)
  select distinct
         live.ReceiptId,
         live.InboundDocumentId,
         live.PriorityId,
         live.WarehouseId,
         live.LocationId,
         live.StatusId,
         live.OperatorId,
         live.DeliveryNoteNumber,
         live.DeliveryDate,
         live.SealNumber,
         live.VehicleRegistration,
         live.Remarks,
         live.CreditAdviceIndicator,
         live.Delivery,
         live.AllowPalletise,
         live.Interfaced
    from @InboundDocument t
    join Receipt          live on t.ReceiptId = live.ReceiptId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @InboundDocument t
    join Receipt          live on t.ReceiptId = live.ReceiptId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  insert ProminentPaintsArchive..InboundLine
        (InboundLineId,
         InboundDocumentId,
         StorageUnitId,
         StatusId,
         LineNumber,
         Quantity,
         BatchId,
         UnitPrice,
         ReasonId)
  select distinct
         live.InboundLineId,
         live.InboundDocumentId,
         live.StorageUnitId,
         live.StatusId,
         live.LineNumber,
         live.Quantity,
         live.BatchId,
         live.UnitPrice,
         live.ReasonId
    from @InboundDocument t
    join InboundLine   live on t.InboundLineId = live.InboundLineId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @InboundDocument t
    join InboundLine   live on t.InboundLineId = live.InboundLineId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error

  insert ProminentPaintsArchive..InboundDocument
        (InboundDocumentId,
         InboundDocumentTypeId,
         ExternalCompanyId,
         StatusId,
         WarehouseId,
         OrderNumber,
         DeliveryDate,
         CreateDate,
         ModifiedDate,
         ReferenceNumber,
         FromLocation,
         ToLocation,
         ReasonId,
         Remarks)
  select distinct
         live.InboundDocumentId,
         live.InboundDocumentTypeId,
         live.ExternalCompanyId,
         live.StatusId,
         live.WarehouseId,
         live.OrderNumber,
         live.DeliveryDate,
         live.CreateDate,
         live.ModifiedDate,
         live.ReferenceNumber,
         live.FromLocation,
         live.ToLocation,
         live.ReasonId,
         live.Remarks
    from @InboundDocument t
    join InboundDocument  live on t.InboundDocumentId = live.InboundDocumentId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  delete live
    from @InboundDocument t
    join InboundDocument  live on t.InboundDocumentId = live.InboundDocumentId
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  commit transaction
--------------------------------------------------------------------------------------------------------------------------------------
  
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Archive'); 
    rollback transaction
    return @Error
end
