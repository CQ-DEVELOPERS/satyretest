﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoadHistory_Insert
  ///   Filename       : p_IntransitLoadHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012 12:08:06
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the IntransitLoadHistory table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadId int = null,
  ///   @WarehouseId int = null,
  ///   @StatusId int = null,
  ///   @DeliveryNoteNumber nvarchar(120) = null,
  ///   @DeliveryDate datetime = null,
  ///   @SealNumber nvarchar(200) = null,
  ///   @VehicleRegistration nvarchar(200) = null,
  ///   @Route nvarchar(200) = null,
  ///   @DriverId int = null,
  ///   @Remarks nvarchar(max) = null,
  ///   @CreatedById int = null,
  ///   @ReceivedById int = null,
  ///   @CreateDate datetime = null,
  ///   @LoadClosed datetime = null,
  ///   @ReceivedDate datetime = null,
  ///   @Offloaded datetime = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   IntransitLoadHistory.IntransitLoadId,
  ///   IntransitLoadHistory.WarehouseId,
  ///   IntransitLoadHistory.StatusId,
  ///   IntransitLoadHistory.DeliveryNoteNumber,
  ///   IntransitLoadHistory.DeliveryDate,
  ///   IntransitLoadHistory.SealNumber,
  ///   IntransitLoadHistory.VehicleRegistration,
  ///   IntransitLoadHistory.Route,
  ///   IntransitLoadHistory.DriverId,
  ///   IntransitLoadHistory.Remarks,
  ///   IntransitLoadHistory.CreatedById,
  ///   IntransitLoadHistory.ReceivedById,
  ///   IntransitLoadHistory.CreateDate,
  ///   IntransitLoadHistory.LoadClosed,
  ///   IntransitLoadHistory.ReceivedDate,
  ///   IntransitLoadHistory.Offloaded,
  ///   IntransitLoadHistory.CommandType,
  ///   IntransitLoadHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoadHistory_Insert
(
 @IntransitLoadId int = null,
 @WarehouseId int = null,
 @StatusId int = null,
 @DeliveryNoteNumber nvarchar(120) = null,
 @DeliveryDate datetime = null,
 @SealNumber nvarchar(200) = null,
 @VehicleRegistration nvarchar(200) = null,
 @Route nvarchar(200) = null,
 @DriverId int = null,
 @Remarks nvarchar(max) = null,
 @CreatedById int = null,
 @ReceivedById int = null,
 @CreateDate datetime = null,
 @LoadClosed datetime = null,
 @ReceivedDate datetime = null,
 @Offloaded datetime = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  insert IntransitLoadHistory
        (IntransitLoadId,
         WarehouseId,
         StatusId,
         DeliveryNoteNumber,
         DeliveryDate,
         SealNumber,
         VehicleRegistration,
         Route,
         DriverId,
         Remarks,
         CreatedById,
         ReceivedById,
         CreateDate,
         LoadClosed,
         ReceivedDate,
         Offloaded,
         CommandType,
         InsertDate)
  select @IntransitLoadId,
         @WarehouseId,
         @StatusId,
         @DeliveryNoteNumber,
         @DeliveryDate,
         @SealNumber,
         @VehicleRegistration,
         @Route,
         @DriverId,
         @Remarks,
         @CreatedById,
         @ReceivedById,
         @CreateDate,
         @LoadClosed,
         @ReceivedDate,
         @Offloaded,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
