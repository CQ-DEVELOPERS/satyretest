﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLine_Update_Check
  ///   Filename       : p_InboundLine_Update_Check.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jul 2007 19:51:27
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InboundLine table.
  /// </remarks>
  /// <param>
  ///   @InboundLineId int = null,
  ///   @InboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null,
  ///   @BatchId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLine_Update_Check
(
 @InboundLineId int = null,
 @InboundDocumentId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @LineNumber int = null,
 @Quantity float = null,
 @BatchId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  update InboundLine
     set InboundDocumentId = isnull(@InboundDocumentId, InboundDocumentId),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         StatusId = isnull(@StatusId, StatusId),
         LineNumber = isnull(@LineNumber, LineNumber),
         Quantity = isnull(@Quantity, Quantity),
         BatchId = isnull(@BatchId, BatchId) 
   where InboundLineId = @InboundLineId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_InboundLineHistory_Insert
         @InboundLineId,
         @InboundDocumentId,
         @StorageUnitId,
         @StatusId,
         @LineNumber,
         @Quantity,
         @BatchId 
        ,@CommandType = 'Update'
  
  return @Error
  
end
