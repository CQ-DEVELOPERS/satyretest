﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_Search_IssueLineId
  ///   Filename       : p_Area_Search_IssueLineId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_Search_IssueLineId
(
 @IssueLineId int
)

as
begin
	 set nocount on;
  declare @StorageUnitId int
  
  select @StorageUnitId = sub.StorageUnitId
    from IssueLine         il (nolock)
    join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
   where il.IssueLineId = @IssueLineId
  
  select a.AreaId,
         a.Area
    from StorageUnitArea sua (nolock)
    join Area              a (nolock) on sua.AreaId = a.AreaId
   where sua.StorageUnitId = @StorageUnitId
  union
  select a.AreaId,
         a.Area
    from StorageUnitLocation sul (nolock)
    join AreaLocation         al (nolock) on sul.LocationId = al.LocationId
    join Area                  a (nolock) on al.AreaId = a.AreaId
   where sul.StorageUnitId = @StorageUnitId
end
