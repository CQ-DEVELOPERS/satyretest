﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ConfigurationHistory_Insert
  ///   Filename       : p_ConfigurationHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 12:11:58
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ConfigurationHistory table.
  /// </remarks>
  /// <param>
  ///   @ConfigurationId int = null,
  ///   @ModuleId int = null,
  ///   @Configuration nvarchar(510) = null,
  ///   @Indicator bit = null,
  ///   @IntegerValue int = null,
  ///   @Value sql_variant = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  ///   ConfigurationHistory.ConfigurationId,
  ///   ConfigurationHistory.ModuleId,
  ///   ConfigurationHistory.Configuration,
  ///   ConfigurationHistory.Indicator,
  ///   ConfigurationHistory.IntegerValue,
  ///   ConfigurationHistory.Value,
  ///   ConfigurationHistory.CommandType,
  ///   ConfigurationHistory.InsertDate,
  ///   ConfigurationHistory.WarehouseId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ConfigurationHistory_Insert
(
 @ConfigurationId int = null,
 @ModuleId int = null,
 @Configuration nvarchar(510) = null,
 @Indicator bit = null,
 @IntegerValue int = null,
 @Value sql_variant = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @WarehouseId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ConfigurationHistory
        (ConfigurationId,
         ModuleId,
         Configuration,
         Indicator,
         IntegerValue,
         Value,
         CommandType,
         InsertDate,
         WarehouseId)
  select @ConfigurationId,
         @ModuleId,
         @Configuration,
         @Indicator,
         @IntegerValue,
         @Value,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @WarehouseId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
