﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerHeader_Update
  ///   Filename       : p_ContainerHeader_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:53
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ContainerHeader table.
  /// </remarks>
  /// <param>
  ///   @ContainerHeaderId int = null,
  ///   @PalletId int = null,
  ///   @JobId int = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @PickLocationId int = null,
  ///   @StoreLocationId int = null,
  ///   @CreatedBy int = null,
  ///   @CreateDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerHeader_Update
(
 @ContainerHeaderId int = null,
 @PalletId int = null,
 @JobId int = null,
 @ReferenceNumber nvarchar(60) = null,
 @PickLocationId int = null,
 @StoreLocationId int = null,
 @CreatedBy int = null,
 @CreateDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @ContainerHeaderId = '-1'
    set @ContainerHeaderId = null;
  
	 declare @Error int
 
  update ContainerHeader
     set PalletId = isnull(@PalletId, PalletId),
         JobId = isnull(@JobId, JobId),
         ReferenceNumber = isnull(@ReferenceNumber, ReferenceNumber),
         PickLocationId = isnull(@PickLocationId, PickLocationId),
         StoreLocationId = isnull(@StoreLocationId, StoreLocationId),
         CreatedBy = isnull(@CreatedBy, CreatedBy),
         CreateDate = isnull(@CreateDate, CreateDate) 
   where ContainerHeaderId = @ContainerHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
