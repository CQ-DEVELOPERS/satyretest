﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_Search_Warehouse
  ///   Filename       : p_Area_Search_Warehouse.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_Search_Warehouse
(
 @warehouseId int
)

as
begin
	 set nocount on;
  
  select *
    from Area
   where WarehouseId = @warehouseId
     and StockOnHand = 1
  order by Area
end
