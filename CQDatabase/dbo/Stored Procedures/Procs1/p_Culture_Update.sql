﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Culture_Update
  ///   Filename       : p_Culture_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:36
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Culture table.
  /// </remarks>
  /// <param>
  ///   @CultureId int = null,
  ///   @CultureName nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Culture_Update
(
 @CultureId int = null,
 @CultureName nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @CultureId = '-1'
    set @CultureId = null;
  
	 declare @Error int
 
  update Culture
     set CultureName = isnull(@CultureName, CultureName) 
   where CultureId = @CultureId
  
  select @Error = @@Error
  
  
  return @Error
  
end
