﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_Console_Orders
  ///   Filename       : p_Interface_Console_Orders.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Apr 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Interface_Console_Orders
(
 @WarehouseId int = null,
 @orderNumber nvarchar(50),
 @companyCode nvarchar(50),
 @company     nvarchar(255),
 @productCode nvarchar(50),
 @product     nvarchar(255),
 @batch       nvarchar(50),
 @status      nvarchar(50),
 @message     nvarchar(max),
 @recordType  nvarchar(50),
 @fromDate    datetime,
 @toDate      datetime
)

as
begin
	 set nocount on;
	 
  select distinct im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceImportPOHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceImportPOHeaderId as 'InterfaceId', h.OrderNumber, h.PrimaryKey, h.RecordType, h.RecordStatus	, h.ProcessedDate, h.InsertDate, h.SupplierCode as 'CompanyCode', h.Supplier as 'Company',
         h.Additional1 as 'hAdditional1', h.Additional2 as 'hAdditional2', h.Additional3 as 'hAdditional3', h.Additional4 as 'hAdditional4', h.Additional5 as 'hAdditional5', h.Additional6 as 'hAdditional6', h.Additional7 as 'hAdditional7', h.Additional8 as 'hAdditional8', h.Additional9 as 'hAdditional9', h.Additional10 as 'hAdditional10'
         --d.ForeignKey, d.LineNumber, d.ProductCode, d.Product, d.Batch, d.Quantity, d.Additional1 as 'dAdditional1', d.Additional2 as 'dAdditional2', d.Additional3 as 'dAdditional3', d.Additional4 as 'dAdditional4', d.Additional5 as 'dAdditional5'
         ,(select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceImportPOHeader') as 'InterfaceTableId'
    from InterfaceImportPOHeader h (nolock)
    left
    join InterfaceImportPODetail d (nolock) on h.InterfaceImportPOHeaderId = d.InterfaceImportPOHeaderId
    left
    join InterfaceMessage       im (nolock) on h.InterfaceImportPOHeaderId = im.InterfaceId
                                           and im.InterfaceTable = 'InterfaceImportPOHeader'
  where h.InsertDate between @fromDate and @toDate
    and isnull(h.OrderNumber,'%')     like '%' + isnull(@OrderNumber, isnull(h.OrderNumber,'%')) + '%'
    and isnull(h.SupplierCode,'%')     like '%' + isnull(@CompanyCode, isnull(h.SupplierCode,'%')) + '%'
    and isnull(h.Supplier,'%')     like '%' + isnull(@Company, isnull(h.Supplier,'%')) + '%'
    and isnull(d.ProductCode,'%')     like '%' + isnull(@ProductCode, isnull(d.ProductCode,'%')) + '%'
    and isnull(d.Product,'%')     like '%' + isnull(@product, isnull(d.Product,'%')) + '%'
    and isnull(d.Batch,'%')     like '%' + isnull(@Batch, isnull(d.Batch,'%')) + '%'
    and isnull(h.RecordType,'%')     like '%' + isnull(@recordType, isnull(h.RecordType,'%')) + '%'
    and isnull(im.InterfaceMessageCode,'%') like '%' + isnull(@status, isnull(im.InterfaceMessageCode,'%')) + '%'
    and isnull(im.InterfaceMessage,'%')     like '%' + isnull(@message, '') + '%'
    and h.RecordStatus != 'R'
  union
  select distinct im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceExportPOHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceExportPOHeaderId, h.OrderNumber, h.PrimaryKey, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, h.SupplierCode as 'CompanyCode', h.Supplier as 'Company',
         h.Additional1 as 'hAdditional1', h.Additional2 as 'hAdditional2', h.Additional3 as 'hAdditional3', h.Additional4 as 'hAdditional4', h.Additional5 as 'hAdditional5', h.Additional6 as 'hAdditional6', h.Additional7 as 'hAdditional7', h.Additional8 as 'hAdditional8', h.Additional9 as 'hAdditional9', h.Additional10 as 'hAdditional10'
         --d.ForeignKey, d.LineNumber, d.ProductCode, d.Product, d.Batch, d.Quantity, d.Additional1 as 'dAdditional1', d.Additional2 as 'dAdditional2', d.Additional3 as 'dAdditional3', d.Additional4 as 'dAdditional4', d.Additional5 as 'dAdditional5'
         ,(select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceExportPOHeader') as 'InterfaceTableId'
    from InterfaceExportPOHeader h (nolock)
    left
    join InterfaceExportPODetail d (nolock) on h.InterfaceExportPOHeaderId = d.InterfaceExportPOHeaderId
    left
    join InterfaceMessage       im (nolock) on h.InterfaceExportPOHeaderId = im.InterfaceId
                                           and im.InterfaceTable = 'InterfaceExportPOHeader'
  where h.InsertDate between @fromDate and @toDate
    and isnull(h.OrderNumber,'%')     like '%' + isnull(@OrderNumber, isnull(h.OrderNumber,'%')) + '%'
    and isnull(h.SupplierCode,'%')     like '%' + isnull(@CompanyCode, isnull(h.SupplierCode,'%')) + '%'
    and isnull(h.Supplier,'%')     like '%' + isnull(@Company, isnull(h.Supplier,'%')) + '%'
    and isnull(d.ProductCode,'%')     like '%' + isnull(@ProductCode, isnull(d.ProductCode,'%')) + '%'
    and isnull(d.Product,'%')     like '%' + isnull(@product, isnull(d.Product,'%')) + '%'
    and isnull(d.Batch,'%')     like '%' + isnull(@Batch, isnull(d.Batch,'%')) + '%'
    and isnull(h.RecordType,'%')     like '%' + isnull(@recordType, isnull(h.RecordType,'%')) + '%'
    and isnull(im.InterfaceMessageCode,'%') like '%' + isnull(@status, isnull(im.InterfaceMessageCode,'%')) + '%'
    and isnull(im.InterfaceMessage,'%')     like '%' + isnull(@message, '') + '%'
    and h.RecordStatus != 'R'
  union
  select distinct im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceImportSOHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceImportSOHeaderId, h.OrderNumber, h.PrimaryKey, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, h.CustomerCode as 'CompanyCode', h.Customer as 'Company',
         h.Additional1 as 'hAdditional1', h.Additional2 as 'hAdditional2', h.Additional3 as 'hAdditional3', h.Additional4 as 'hAdditional4', h.Additional5 as 'hAdditional5', h.Additional6 as 'hAdditional6', h.Additional7 as 'hAdditional7', h.Additional8 as 'hAdditional8', h.Additional9 as 'hAdditional9', h.Additional10 as 'hAdditional10'
         --d.ForeignKey, d.LineNumber, d.ProductCode, d.Product, d.Batch, d.Quantity, d.Additional1 as 'dAdditional1', d.Additional2 as 'dAdditional2', d.Additional3 as 'dAdditional3', d.Additional4 as 'dAdditional4', d.Additional5 as 'dAdditional5'
         ,(select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceImportSOHeader') as 'InterfaceTableId'
    from InterfaceImportSOHeader h (nolock)
    left
    join InterfaceImportSODetail d (nolock) on h.InterfaceImportSOHeaderId = d.InterfaceImportSOHeaderId
    left
    join InterfaceMessage       im (nolock) on h.InterfaceImportSOHeaderId = im.InterfaceId
                                           and im.InterfaceTable = 'InterfaceImportSOHeader'
  where h.InsertDate between @fromDate and @toDate
    and isnull(h.OrderNumber,'%')     like '%' + isnull(@OrderNumber, isnull(h.OrderNumber,'%')) + '%'
    and isnull(h.CustomerCode,'%')     like '%' + isnull(@CompanyCode, isnull(h.CustomerCode,'%')) + '%'
    and isnull(h.Customer,'%')     like '%' + isnull(@Company, isnull(h.Customer,'%')) + '%'
    and isnull(d.ProductCode,'%')     like '%' + isnull(@ProductCode, isnull(d.ProductCode,'%')) + '%'
    and isnull(d.Product,'%')     like '%' + isnull(@product, isnull(d.Product,'%')) + '%'
    and isnull(d.Batch,'%')     like '%' + isnull(@Batch, isnull(d.Batch,'%')) + '%'
    and isnull(h.RecordType,'%')     like '%' + isnull(@recordType, isnull(h.RecordType,'%')) + '%'
    and isnull(im.InterfaceMessageCode,'%') like '%' + isnull(@status, isnull(im.InterfaceMessageCode,'%')) + '%'
    and isnull(im.InterfaceMessage,'%')     like '%' + isnull(@message, '') + '%'
    and h.RecordStatus != 'R'
  union
  select distinct im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceExportSOHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceExportSOHeaderId, h.OrderNumber, h.PrimaryKey, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, h.CustomerCode as 'CompanyCode', h.Customer as 'Company',
         h.Additional1 as 'hAdditional1', h.Additional2 as 'hAdditional2', h.Additional3 as 'hAdditional3', h.Additional4 as 'hAdditional4', h.Additional5 as 'hAdditional5', h.Additional6 as 'hAdditional6', h.Additional7 as 'hAdditional7', h.Additional8 as 'hAdditional8', h.Additional9 as 'hAdditional9', h.Additional10 as 'hAdditional10'
         --d.ForeignKey, d.LineNumber, d.ProductCode, d.Product, d.Batch, d.Quantity, d.Additional1 as 'dAdditional1', d.Additional2 as 'dAdditional2', d.Additional3 as 'dAdditional3', d.Additional4 as 'dAdditional4', d.Additional5 as 'dAdditional5'
         ,(select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceExportSOHeader') as 'InterfaceTableId'
    from InterfaceExportSOHeader h (nolock)
    left
    join InterfaceExportSODetail d (nolock) on h.InterfaceExportSOHeaderId = d.InterfaceExportSOHeaderId
    left
    join InterfaceMessage       im (nolock) on h.InterfaceExportSOHeaderId = im.InterfaceId
                                           and im.InterfaceTable = 'InterfaceExportSOHeader'
  where h.InsertDate between @fromDate and @toDate
    and isnull(h.OrderNumber,'%')     like '%' + isnull(@OrderNumber, isnull(h.OrderNumber,'%')) + '%'
    and isnull(h.CustomerCode,'%')     like '%' + isnull(@CompanyCode, isnull(h.CustomerCode,'%')) + '%'
    and isnull(h.Customer,'%')     like '%' + isnull(@Company, isnull(h.Customer,'%')) + '%'
    and isnull(d.ProductCode,'%')     like '%' + isnull(@ProductCode, isnull(d.ProductCode,'%')) + '%'
    and isnull(d.Product,'%')     like '%' + isnull(@product, isnull(d.Product,'%')) + '%'
    and isnull(d.Batch,'%')     like '%' + isnull(@Batch, isnull(d.Batch,'%')) + '%'
    and isnull(h.RecordType,'%')     like '%' + isnull(@recordType, isnull(h.RecordType,'%')) + '%'
    and isnull(im.InterfaceMessageCode,'%') like '%' + isnull(@status, isnull(im.InterfaceMessageCode,'%')) + '%'
    and isnull(im.InterfaceMessage,'%')     like '%' + isnull(@message, '') + '%'
    and h.RecordStatus != 'R'
  union
  select distinct im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceImportHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceImportHeaderId, h.OrderNumber, h.PrimaryKey, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, h.CompanyCode, h.Company,
         h.Additional1 as 'hAdditional1', h.Additional2 as 'hAdditional2', h.Additional3 as 'hAdditional3', h.Additional4 as 'hAdditional4', h.Additional5 as 'hAdditional5', h.Additional6 as 'hAdditional6', h.Additional7 as 'hAdditional7', h.Additional8 as 'hAdditional8', h.Additional9 as 'hAdditional9', h.Additional10 as 'hAdditional10'
         --d.ForeignKey, d.LineNumber, d.ProductCode, d.Product, d.Batch, d.Quantity, d.Additional1 as 'dAdditional1', d.Additional2 as 'dAdditional2', d.Additional3 as 'dAdditional3', d.Additional4 as 'dAdditional4', d.Additional5 as 'dAdditional5'
         ,(select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceImportHeader') as 'InterfaceTableId'
    from InterfaceImportHeader h (nolock)
    left
    join InterfaceImportDetail d (nolock) on h.InterfaceImportHeaderId = d.InterfaceImportHeaderId
    left
    join InterfaceMessage       im (nolock) on h.InterfaceImportHeaderId = im.InterfaceId
                                           and im.InterfaceTable = 'InterfaceImportHeader'
  where h.InsertDate between @fromDate and @toDate
    and isnull(h.OrderNumber,'%')     like '%' + isnull(@OrderNumber, isnull(h.OrderNumber,'%')) + '%'
    and isnull(h.CompanyCode,'%')     like '%' + isnull(@CompanyCode, isnull(h.CompanyCode,'%')) + '%'
    and isnull(h.Company,'%')     like '%' + isnull(@Company, isnull(h.Company,'%')) + '%'
    and isnull(d.ProductCode,'%')     like '%' + isnull(@ProductCode, isnull(d.ProductCode,'%')) + '%'
    and isnull(d.Product,'%')     like '%' + isnull(@product, isnull(d.Product,'%')) + '%'
    and isnull(d.Batch,'%')     like '%' + isnull(@Batch, isnull(d.Batch,'%')) + '%'
    and isnull(h.RecordType,'%')     like '%' + isnull(@recordType, isnull(h.RecordType,'%')) + '%'
    and isnull(im.InterfaceMessageCode,'%') like '%' + isnull(@status, isnull(im.InterfaceMessageCode,'%')) + '%'
    and isnull(im.InterfaceMessage,'%')     like '%' + isnull(@message, '') + '%'
    and h.RecordStatus != 'R'
  union
  select distinct im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceExportHeader') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceExportHeaderId, h.OrderNumber, h.PrimaryKey, h.RecordType, h.RecordStatus, h.ProcessedDate, h.InsertDate, h.CompanyCode, h.Company,
         h.Additional1 as 'hAdditional1', h.Additional2 as 'hAdditional2', h.Additional3 as 'hAdditional3', h.Additional4 as 'hAdditional4', h.Additional5 as 'hAdditional5', h.Additional6 as 'hAdditional6', h.Additional7 as 'hAdditional7', h.Additional8 as 'hAdditional8', h.Additional9 as 'hAdditional9', h.Additional10 as 'hAdditional10'
         --d.ForeignKey, d.LineNumber, d.ProductCode, d.Product, d.Batch, d.Quantity, d.Additional1 as 'dAdditional1', d.Additional2 as 'dAdditional2', d.Additional3 as 'dAdditional3', d.Additional4 as 'dAdditional4', d.Additional5 as 'dAdditional5'
         ,(select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceExportHeader') as 'InterfaceTableId'
    from InterfaceExportHeader h (nolock)
    left
    join InterfaceExportDetail d (nolock) on h.InterfaceExportHeaderId = d.InterfaceExportHeaderId
    left
    join InterfaceMessage     im (nolock) on h.InterfaceExportHeaderId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceExportHeader'
  where h.InsertDate between @fromDate and @toDate
    and isnull(h.OrderNumber,'%')     like '%' + isnull(@OrderNumber, isnull(h.OrderNumber,'%')) + '%'
    and isnull(h.CompanyCode,'%')     like '%' + isnull(@CompanyCode, isnull(h.CompanyCode,'%')) + '%'
    and isnull(h.Company,'%')     like '%' + isnull(@Company, isnull(h.Company,'%')) + '%'
    and isnull(d.ProductCode,'%')     like '%' + isnull(@ProductCode, isnull(d.ProductCode,'%')) + '%'
    and isnull(d.Product,'%')     like '%' + isnull(@product, isnull(d.Product,'%')) + '%'
    and isnull(d.Batch,'%')     like '%' + isnull(@Batch, isnull(d.Batch,'%')) + '%'
    and isnull(h.RecordType,'%')     like '%' + isnull(@recordType, isnull(h.RecordType,'%')) + '%'
    and isnull(im.InterfaceMessageCode,'%') like '%' + isnull(@status, isnull(im.InterfaceMessageCode,'%')) + '%'
    and isnull(im.InterfaceMessage,'%')     like '%' + isnull(@message, '') + '%'
    and h.RecordStatus != 'R'
  union
  select distinct im.InterfaceMessageId, im.InterfaceMessageCode, im.InterfaceMessage, im.Status, isnull(im.InterfaceTable, 'InterfaceExportShipmentDelivery') as 'InterfaceTable', im.CreateDate as 'MessageDate', h.InterfaceExportShipmentDeliveryId, h.WaybillNo, convert(nvarchar(30), h.WaveId), h.RecordType, h.RecordStatus, h.ProcessedDate, null, h.ConsigneeCode, h.ConsigneeName,
         null as 'hAdditional1', null as 'hAdditional2', null as 'hAdditional3', null as 'hAdditional4', null as 'hAdditional5', null as 'hAdditional6', null as 'hAdditional7', null as 'hAdditional8', null as 'hAdditional9', null as 'hAdditional10'
         --null, d.LineNumber, d.ProductCode, d.Product, d.Batch, d.Quantity, null as 'dAdditional1', null as 'dAdditional2', null as 'dAdditional3', null as 'dAdditional4', null as 'dAdditional5'
         ,(select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceExportShipmentDelivery') as 'InterfaceTableId'
    from InterfaceExportShipmentDelivery h (nolock)
    left
    join InterfaceExportShipmentDetail d (nolock) on h.InterfaceExportShipmentDeliveryId = d.InterfaceExportShipmentDeliveryId
    left
    join InterfaceMessage     im (nolock) on h.InterfaceExportShipmentDeliveryId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceExportShipmentDelivery'
  where h.ProcessedDate between @fromDate and @toDate
    and isnull(h.WaybillNo,'%')     like '%' + isnull(@OrderNumber, isnull(h.WaybillNo,'%')) + '%'
    and isnull(h.ConsigneeCode,'%')     like '%' + isnull(@CompanyCode, isnull(h.ConsigneeCode,'%')) + '%'
    and isnull(h.ConsigneeName,'%')     like '%' + isnull(@Company, isnull(h.ConsigneeName,'%')) + '%'
    and isnull(d.ProductCode,'%')     like '%' + isnull(@ProductCode, isnull(d.ProductCode,'%')) + '%'
    and isnull(d.Product,'%')     like '%' + isnull(@product, isnull(d.Product,'%')) + '%'
    and isnull(d.Batch,'%')     like '%' + isnull(@Batch, isnull(d.Batch,'%')) + '%'
    and isnull(h.RecordType,'%')     like '%' + isnull(@recordType, isnull(h.RecordType,'%')) + '%'
    and isnull(im.InterfaceMessageCode,'%') like '%' + isnull(@status, isnull(im.InterfaceMessageCode,'%')) + '%'
    and isnull(im.InterfaceMessage,'%')     like '%' + isnull(@message, '') + '%'
    and h.RecordStatus != 'R'
  
  --select im.InterfaceMessageId,
  --       im.InterfaceMessageCode,
  --       im.InterfaceMessage,
  --       im.Status,
  --       isnull(im.InterfaceMessageCode, 'No Response') as 'InterfaceMessageCode',
  --       im.CreateDate as 'MessageDate',
  --       (select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceExportStockAdjustment') as 'InterfaceTableId',
  --       h.InterfaceExportStockAdjustmentId as 'InterfaceId',
  --       h.RecordType,
  --       h.RecordStatus,
  --       h.ProcessedDate,
  --       h.InsertDate,
  --       h.ProductCode,
  --       h.Product,
  --       h.Batch,
  --       h.Quantity, h.Additional1, h.Additional2, h.Additional3, h.Additional4, h.Additional5
  --from InterfaceExportStockAdjustment h (nolock)
  --left
  --join InterfaceMessage       im (nolock) on h.InterfaceExportStockAdjustmentId = im.InterfaceId
  --                                       and im.InterfaceTable = 'InterfaceExportStockAdjustment'
                                         
  --where h.InsertDate between @fromDate and @toDate
  --  and isnull(h.ProductCode,'%')     like '%' + isnull(@ProductCode, isnull(h.ProductCode,'%')) + '%'
  --  and isnull(h.Product,'%')     like '%' + isnull(@product, isnull(h.Product,'%')) + '%'
  --  and isnull(h.Batch,'%')     like '%' + isnull(@Batch, isnull(h.Batch,'%')) + '%'
  --  and isnull(h.Additional1,'%')     like '%' + isnull(@additional1, isnull(h.Additional1,'%')) + '%'
  --  and isnull(h.Additional2,'%')     like '%' + isnull(@additional2, isnull(h.Additional2,'%')) + '%'
  --  and isnull(h.Additional3,'%')     like '%' + isnull(@additional3, isnull(h.Additional3,'%')) + '%'
  --  and isnull(h.RecordType,'%')     like '%' + isnull(@recordType, isnull(h.RecordType,'%')) + '%'
  --  and isnull(im.InterfaceMessageCode,'%') like '%' + isnull(@status, isnull(im.InterfaceMessageCode,'%')) + '%'
  --  and isnull(im.InterfaceMessage,'%')     like '%' + isnull(@message, '') + '%'
  --  and h.RecordStatus != 'R'
end
