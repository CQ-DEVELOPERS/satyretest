﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Insert
  ///   Filename       : p_Container_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 11:46:27
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Container table.
  /// </remarks>
  /// <param>
  ///   @ContainerId int = null output,
  ///   @PalletId int = null,
  ///   @JobId int = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @StorageUnitBatchId int = null,
  ///   @PickLocationId int = null,
  ///   @StoreLocationId int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  ///   Container.ContainerId,
  ///   Container.PalletId,
  ///   Container.JobId,
  ///   Container.ReferenceNumber,
  ///   Container.StorageUnitBatchId,
  ///   Container.PickLocationId,
  ///   Container.StoreLocationId,
  ///   Container.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Insert
(
 @ContainerId int = null output,
 @PalletId int = null,
 @JobId int = null,
 @ReferenceNumber nvarchar(60) = null,
 @StorageUnitBatchId int = null,
 @PickLocationId int = null,
 @StoreLocationId int = null,
 @Quantity float = null 
)

as
begin
	 set nocount on;
  
  if @ContainerId = '-1'
    set @ContainerId = null;
  
	 declare @Error int
 
  insert Container
        (PalletId,
         JobId,
         ReferenceNumber,
         StorageUnitBatchId,
         PickLocationId,
         StoreLocationId,
         Quantity)
  select @PalletId,
         @JobId,
         @ReferenceNumber,
         @StorageUnitBatchId,
         @PickLocationId,
         @StoreLocationId,
         @Quantity 
  
  select @Error = @@Error, @ContainerId = scope_identity()
  
  
  return @Error
  
end
