﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPack_Insert
  ///   Filename       : p_InterfaceImportPack_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:53
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportPack table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportProductId int = null,
  ///   @PackCode nvarchar(60) = null,
  ///   @PackDescription nvarchar(510) = null,
  ///   @Quantity float = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @Length numeric(13,6) = null,
  ///   @Width numeric(13,6) = null,
  ///   @Height numeric(13,6) = null,
  ///   @Volume float = null,
  ///   @NettWeight float = null,
  ///   @GrossWeight float = null,
  ///   @TareWeight float = null,
  ///   @ProductCategory char(1) = null,
  ///   @PackingCategory char(1) = null,
  ///   @PickEmpty bit = null,
  ///   @StackingCategory int = null,
  ///   @MovementCategory int = null,
  ///   @ValueCategory int = null,
  ///   @StoringCategory int = null,
  ///   @PickPartPallet int = null,
  ///   @HostId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportPack.InterfaceImportProductId,
  ///   InterfaceImportPack.PackCode,
  ///   InterfaceImportPack.PackDescription,
  ///   InterfaceImportPack.Quantity,
  ///   InterfaceImportPack.Barcode,
  ///   InterfaceImportPack.Length,
  ///   InterfaceImportPack.Width,
  ///   InterfaceImportPack.Height,
  ///   InterfaceImportPack.Volume,
  ///   InterfaceImportPack.NettWeight,
  ///   InterfaceImportPack.GrossWeight,
  ///   InterfaceImportPack.TareWeight,
  ///   InterfaceImportPack.ProductCategory,
  ///   InterfaceImportPack.PackingCategory,
  ///   InterfaceImportPack.PickEmpty,
  ///   InterfaceImportPack.StackingCategory,
  ///   InterfaceImportPack.MovementCategory,
  ///   InterfaceImportPack.ValueCategory,
  ///   InterfaceImportPack.StoringCategory,
  ///   InterfaceImportPack.PickPartPallet,
  ///   InterfaceImportPack.HostId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPack_Insert
(
 @InterfaceImportProductId int = null,
 @PackCode nvarchar(60) = null,
 @PackDescription nvarchar(510) = null,
 @Quantity float = null,
 @Barcode nvarchar(100) = null,
 @Length numeric(13,6) = null,
 @Width numeric(13,6) = null,
 @Height numeric(13,6) = null,
 @Volume float = null,
 @NettWeight float = null,
 @GrossWeight float = null,
 @TareWeight float = null,
 @ProductCategory char(1) = null,
 @PackingCategory char(1) = null,
 @PickEmpty bit = null,
 @StackingCategory int = null,
 @MovementCategory int = null,
 @ValueCategory int = null,
 @StoringCategory int = null,
 @PickPartPallet int = null,
 @HostId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportPack
        (InterfaceImportProductId,
         PackCode,
         PackDescription,
         Quantity,
         Barcode,
         Length,
         Width,
         Height,
         Volume,
         NettWeight,
         GrossWeight,
         TareWeight,
         ProductCategory,
         PackingCategory,
         PickEmpty,
         StackingCategory,
         MovementCategory,
         ValueCategory,
         StoringCategory,
         PickPartPallet,
         HostId)
  select @InterfaceImportProductId,
         @PackCode,
         @PackDescription,
         @Quantity,
         @Barcode,
         @Length,
         @Width,
         @Height,
         @Volume,
         @NettWeight,
         @GrossWeight,
         @TareWeight,
         @ProductCategory,
         @PackingCategory,
         @PickEmpty,
         @StackingCategory,
         @MovementCategory,
         @ValueCategory,
         @StoringCategory,
         @PickPartPallet,
         @HostId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
