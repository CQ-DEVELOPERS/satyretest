﻿ 
       
/*      
/// <summary>      
///   Procedure Name : p_Interface_WebService_PRD      
///   Filename       : p_Interface_WebService_PRD.sql      
///   Create By      : Grant Schultz      
///   Date Created   : 14 Mar 2013      
/// </summary>      
/// <remarks>      
///      
/// </remarks>      
/// <param>      
///      
/// </param>      
/// <returns>      
///      
/// </returns>      
/// <newpara>      
///   Modified by    :      
///   Modified Date  :      
///   Details        :      
/// </newpara>      
*/      
CREATE procedure [dbo].[p_Interface_WebService_PRD]      
(      
 @XMLBody nvarchar(max) output,      
 @principalCode nvarchar(30) = null      
)      
--with encryption      
As      
Begin      
        
  declare @doc                          xml      
  declare @idoc                         int      
  declare @InsertDate                   datetime      
  declare @ERROR                        AS VARCHAR(255)      
  declare @InterfaceImportProductId  int      
        
  SELECT @doc = convert(xml,@XMLBody)      
         , @InsertDate = Getdate()      
               
         EXEC sp_xml_preparedocument @idoc OUTPUT, @doc      
               
  BEGIN TRY      
          
    INSERT INTO [InterfaceImportProduct]      
           ([RecordStatus]      
           ,[ProcessedDate]      
           ,[PrimaryKey]      
           ,[ProductCode]      
           ,[Product]      
           ,[SKUCode]      
           ,[SKU]      
           ,[Style]      
           ,[Colour]      
           ,[Size]      
           ,[PalletQuantity]      
           ,[Barcode]      
           ,[MinimumQuantity]      
           ,[ReorderQuantity]      
           ,[MaximumQuantity]      
           ,[CuringPeriodDays]      
           ,[ShelfLifeDays]      
           ,[QualityAssuranceIndicator]      
           ,[ProductType]      
           ,[ProductCategory]      
           ,[OverReceipt]      
           ,[RetentionSamples]      
           ,[HostId]      
           ,[UnitPrice]      
           ,[Additional1]      
           ,[Additional2]      
           ,[Additional3]      
           ,[Additional4]      
           ,[Additional5]      
           ,[Additional6]      
           ,[Additional7]      
           ,[Additional8]      
           ,[Additional9]      
           ,[Additional10]      
           ,[DangerousGoodsCode]      
           ,[ABCStatus]      
           ,[Samples]      
           ,[AssaySamples]      
           ,[PrincipalCode]      
           ,[InsertDate]      
           ,[HostRecordStatus])      
    SELECT 'W'      
           ,Null      
           ,nodes.entity.value('PrimaryKey[1]',         'nvarchar(30)')  'PrimaryKey'      
           ,nodes.entity.value('ProductCode[1]',        'nvarchar(30)')  'ProductCode'      
           ,nodes.entity.value('Product[1]',            'nvarchar(255)') 'Product'      
           ,nodes.entity.value('SKUCode[1]',            'nvarchar(50)')  'SKUCode'      
           ,nodes.entity.value('SKU[1]',                'nvarchar(50)')  'SKU'      
           ,nodes.entity.value('Style[1]',              'nvarchar(10)')  'Style'      
           ,nodes.entity.value('Colour[1]',             'nvarchar(10)')  'Colour'      
           ,nodes.entity.value('Size[1]',               'nvarchar(10)')  'Size'      
           ,99999
           --,nodes.entity.value('PalletQuantity[1]',     'int')           'PalletQuantity'      
           ,nodes.entity.value('Barcode[1]',            'nvarchar(50)')  'Barcode'      
           ,nodes.entity.value('MinimumQuantity[1]',    'int')           'MinimumQuantity'      
           ,nodes.entity.value('ReorderQuantity[1]',    'int')           'ReorderQuantity'      
           ,nodes.entity.value('MaximumQuantity[1]',    'int')           'MaximumQuantity'      
           ,nodes.entity.value('CuringPeriodDays[1]',   'int')           'CuringPeriodDays'      
           ,nodes.entity.value('ShelfLifeDays[1]',      'int')           'ShelfLifeDays'      
           ,nodes.entity.value('QualityAssuranceIndicator[1]', 'bit')    'QualityAssuranceIndicator'      
           ,nodes.entity.value('ProductType[1]',        'nvarchar(20)')  'ProductType'      
           ,nodes.entity.value('ProductCategory[1]',    'nvarchar(20)')  'ProductCategory'      
           ,nodes.entity.value('OverReceipt[1]',        'numeric(13,3)') 'OverReceipt'      
           ,nodes.entity.value('RetentionSamples[1]',   'int')           'RetentionSamples'      
           ,nodes.entity.value('HostId[1]',   'nvarchar(30)')  'HostId'      
           ,nodes.entity.value('UnitPrice[1]',          'nvarchar(255)') 'UnitPrice'      
           ,nodes.entity.value('Additional1[1]',        'nvarchar(255)') 'Additional1'      
           ,nodes.entity.value('Additional2[1]',        'nvarchar(255)') 'Additional2'      
           ,nodes.entity.value('Additional3[1]',        'nvarchar(255)') 'Additional3'      
           ,nodes.entity.value('Additional4[1]',        'nvarchar(255)') 'Additional4'      
           ,nodes.entity.value('Additional5[1]',        'nvarchar(255)') 'Additional5'      
           ,nodes.entity.value('Additional6[1]',        'nvarchar(255)') 'Additional6'      
           ,nodes.entity.value('Additional7[1]',        'nvarchar(255)') 'Additional7'      
           ,nodes.entity.value('Additional8[1]',        'nvarchar(255)') 'Additional8'      
           ,nodes.entity.value('Additional9[1]',        'nvarchar(255)') 'Additional9'      
           ,nodes.entity.value('Additional10[1]',       'nvarchar(255)') 'Additional10'      
           ,nodes.entity.value('DangerousGoodsCode[1]', 'nvarchar(255)') 'DangerousGoodsCode'      
           ,nodes.entity.value('ABCStatus[1]',          'nvarchar(1)')   'ABCStatus'      
           ,nodes.entity.value('Samples[1]',            'nvarchar(50)')  'Samples'      
           ,nodes.entity.value('AssaySamples[1]',       'float')         'AssaySamples'      
           ,@principalCode      
           ,@InsertDate      
           ,nodes.entity.value('RecordStatus[1]',            'char(10)')  'RecordStatus'      
           --,CAST(ISNULL(nodes.entity.value('VatPercentage[1]', 'varchar(50)'),'0.00') AS decimal(13,3)) 'VatPercentage'      
                 
           --,nodes.entity.value('ProcessedDate[1]',      'datetime      
                                        --,nodes.entity.value('InsertDate[1]',         'datetime      
            FROM      
            @doc.nodes('/root/Body/Item') AS nodes(entity)      
                 
 SET @InterfaceImportProductId = SCOPE_IDENTITY()      
                 
    INSERT INTO [InterfaceImportPack]      
           (InterfaceImportProductId      
           ,[ForeignKey]      
           ,[PackCode]      
           ,[PackDescription]      
           ,[Quantity]      
           ,[Barcode]      
           ,[Length]      
           ,[Width]      
           ,[Height]      
           ,[Volume]      
           ,[NettWeight]      
           ,[GrossWeight]      
           ,[TareWeight]      
           ,[ProductCategory]      
           ,[PackingCategory]      
           ,[PickEmpty]      
           ,[StackingCategory]      
           ,[MovementCategory]      
           ,[ValueCategory]      
           ,[StoringCategory]      
           ,[PickPartPallet])      
     SELECT h.InterfaceImportProductId       
           ,nodes.entity.value('ForeignKey[1]',         'nvarchar(30)')   'ForeignKey'      
           ,'Unit'                    'PackCode'      
           ,CASE WHEN nodes.entity.value('PackDescription[1]', 'nvarchar(255)') = ''      
                 THEN nodes.entity.value('PackType[1]',     'nvarchar(30)')      
                 ELSE nodes.entity.value('PackDescription[1]', 'nvarchar(255)')      
                 END                 'PackDescription'      
           ,nodes.entity.value('Quantity[1]',           'int')            'Quantity'      
           ,nodes.entity.value('Barcode[1]',            'nvarchar(50)')   'Barcode'      
           ,nodes.entity.value('Length[1]',             'numeric(13,6)')  'Length'      
      ,nodes.entity.value('Width[1]',              'numeric(13,6)')  'Width'      
           ,nodes.entity.value('Height[1]',             'numeric(13,6)')  'Height'      
           ,nodes.entity.value('Volume[1]',             'numeric(13,6)')  'Volume'      
           ,nodes.entity.value('NettWeight[1]',         'numeric(13,6)')  'NettWeight'      
           ,nodes.entity.value('GrossWeight[1]',        'numeric(13,6)')  'GrossWeight'      
           ,nodes.entity.value('TareWeight[1]',         'numeric(13,6)')  'TareWeight'      
           ,nodes.entity.value('ProductCategory[1]',    'nchar(1)')       'ProductCategory'      
           ,nodes.entity.value('PackingCategory[1]',    'nchar(1)')       'PackingCategory'      
           ,nodes.entity.value('PickEmpty[1]',          'bit')            'PickEmpty'      
           ,nodes.entity.value('StackingCategory[1]',   'int')            'StackingCategory'      
           ,nodes.entity.value('MovementCategory[1]',   'int')            'MovementCategory'      
           ,nodes.entity.value('ValueCategory[1]',      'int')            'ValueCategory'      
           ,nodes.entity.value('StoringCategory[1]',    'int')            'StoringCategory'      
           ,nodes.entity.value('PickPartPallet[1]',     'int')            'PickPartPallet'      
           FROM      
           @doc.nodes('/root/Body/Item/ItemLine') AS nodes(entity)      
           join InterfaceImportProduct h on nodes.entity.value('ForeignKey[1]',         'nvarchar(30)') = h.PrimaryKey      
                                        and h.InsertDate = @InsertDate      
           --inner join InterfaceImportProduct p on p.PrimaryKey = nodes.entity.value('ForeignKey[1]', 'nvarchar(30)')      
           --WHERE      
           --p.RecordStatus = 'W'      
                 
    Update prd      
       set RecordStatus = 'N'      
           --PrincipalCode = @principalCode      
      From InterfaceImportProduct prd      
      Left join InterfaceImportPack pck on prd.InterfaceImportProductId = pck.InterfaceImportProductId      
     Where RecordSTatus = 'W'      
    and InsertDate = @InsertDate   
  
 --Update working tables  
 exec p_Generic_Import_Product   
          
  END TRY      
  BEGIN CATCH      
          
    SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)      
                 
           PRINT @ERROR      
  END CATCH      
        
  --exec p_Generic_Import_Product      
        
     SET @XMLBody = '<root><status>'      
         + CASE WHEN LEN(@ERROR) > 0 THEN @ERROR      
         ELSE 'Product(s) successfully imported'      
END      
+ '</status></root>'      
end      
      