﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Search_By_Type
  ///   Filename       : p_ExternalCompany_Search_By_Type.sql
  ///   Create By      : Karen
  ///   Date Created   : October 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Search_By_Type
(
 @externalCompanyTypeCode nvarchar(10) = null
)

as
begin
  set nocount on;
  
  select ec.ExternalCompanyId as ShippingAgentId,
         ec.ExternalCompany as ShippingAgent,
         ec.ExternalCompanyCode as ShippingAgentCode,
         ec.ExternalCompanyId,
         ec.ExternalCompany,
         ec.ExternalCompanyCode
    from ExternalCompany     ec  (nolock)
    join ExternalCompanyType ect (nolock) on ect.ExternalCompanyTypeId = ec.ExternalCompanyTypeId
   where ect.ExternalCompanyTypeCode = @externalCompanyTypeCode
  union
  select '-1' as ShippingAgentId,
         '{NONE}' as ShippingAgent,
         null as ShippingAgentCode,
         '-1',
         '{NONE}' ,
         null
  order by ExternalCompany
end
 
 
 
