﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentDesc_Search
  ///   Filename       : p_InterfaceDocumentDesc_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:34:59
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceDocumentDesc table.
  /// </remarks>
  /// <param>
  ///   @DocId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceDocumentDesc.DocId,
  ///   InterfaceDocumentDesc.Doc,
  ///   InterfaceDocumentDesc.DocDesc 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentDesc_Search
(
 @DocId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @DocId = '-1'
    set @DocId = null;
  
 
  select
         InterfaceDocumentDesc.DocId
        ,InterfaceDocumentDesc.Doc
        ,InterfaceDocumentDesc.DocDesc
    from InterfaceDocumentDesc
   where isnull(InterfaceDocumentDesc.DocId,'0')  = isnull(@DocId, isnull(InterfaceDocumentDesc.DocId,'0'))
  
end
