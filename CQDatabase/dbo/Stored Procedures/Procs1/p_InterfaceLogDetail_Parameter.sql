﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLogDetail_Parameter
  ///   Filename       : p_InterfaceLogDetail_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:29
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceLogDetail table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceLogDetail.InterfaceLogDetailId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLogDetail_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceLogDetailId
        ,null as 'InterfaceLogDetail'
  union
  select
         InterfaceLogDetail.InterfaceLogDetailId
        ,InterfaceLogDetail.InterfaceLogDetailId as 'InterfaceLogDetail'
    from InterfaceLogDetail
  
end
