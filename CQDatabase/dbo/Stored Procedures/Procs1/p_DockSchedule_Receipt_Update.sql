﻿ 
/*
/// <summary>
///   Procedure Name : p_DockSchedule_Receipt_Update
///   Filename       : p_DockSchedule_Receipt_Update.sql
///   Create By      : Junaid Desai
///   Date Created   : 19 Nov 2008
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
CREATE procedure p_DockSchedule_Receipt_Update
(
 @ReceiptId           int,
 @DeliveryDate        datetime = null,
 @LocationId          int = null,
 @grn                 nvarchar(30) = null,
 @PriorityId          int = null,
 @DeliveryNoteNumber  nvarchar(30) = null,
 @SealNumber          nvarchar(30) = null,
 @VehicleRegistration nvarchar(10) = null,
 @Remarks             nvarchar(250) = null,
 @ContainerNumber     nvarchar(50) = null,
 @ContainerSize       nvarchar(50) = null,
 @ShippingAgentId     int = null,
 @BOE                 nvarchar(255) = null,
 @AdditionalText1     nvarchar(255) = null,
 @AdditionalText2     nvarchar(255) = null,
 @VehicleTypeId       int = null
)

as
begin
     set nocount on;
         
  declare @Error                        int,
          @Errormsg                     nvarchar(500),
          @GetDate                      datetime,
          @InboundDocumentId            int,
          @Delivery                     int
  
  if @LocationId = -1
     set @LocationId = null
         
  if @ShippingAgentId = -1
     set @ShippingAgentId = null
  
  if @VehicleTypeId = -1
    set @VehicleTypeId = null
  
  --  if (select count(distinct(StatusId)) from ReceiptLine where ReceiptId = @ReceiptId) = 1
  --  begin
  begin transaction
  
  select @InboundDocumentId = InboundDocumentId
         ,@Delivery          = Delivery
    from Receipt
   where ReceiptId = @ReceiptId
  
  if exists(select top 1 1 from Receipt where InboundDocumentId = @InboundDocumentId and DeliveryNoteNumber = @DeliveryNoteNumber)
  begin
    select @DeliveryNoteNumber = @DeliveryNoteNumber + '_' + CONVERT(nvarchar(10), @Delivery)
  end
  
  UPDATE Receipt
     SET PriorityId = @PriorityId
         ,LocationId = @LocationId
         ,DeliveryNoteNumber = @DeliveryNoteNumber
         ,SealNumber = @SealNumber
         ,VehicleRegistration = @VehicleRegistration
         ,Remarks = @Remarks
         ,DeliveryDate = @DeliveryDate
         ,ContainerNumber = @ContainerNumber
         ,ShippingAgentId = @ShippingAgentId
         ,BOE = @BOE
         ,AdditionalText1 = @AdditionalText1
         ,AdditionalText2 = @AdditionalText2
         ,VehicleTypeId = @VehicleTypeId
         ,GRN = @grn
   where ReceiptId = @ReceiptId
  
  if @Error <> 0
  goto error
  
  commit transaction
  --  end
  
  return
  
  error:
  RAISERROR (900000,-1,-1, 'Error executing p_DockSchedule_Receipt_Update'); 
  rollback transaction
  return @Error
end
