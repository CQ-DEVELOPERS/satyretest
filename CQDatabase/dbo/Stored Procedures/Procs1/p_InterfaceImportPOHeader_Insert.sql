﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPOHeader_Insert
  ///   Filename       : p_InterfaceImportPOHeader_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:58
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportPOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportPOHeaderId int = null output,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @RecordType char(3) = null,
  ///   @RecordStatus char(1) = null,
  ///   @SupplierCode nvarchar(60) = null,
  ///   @Supplier nvarchar(510) = null,
  ///   @Address nvarchar(510) = null,
  ///   @FromWarehouseCode nvarchar(20) = null,
  ///   @ToWarehouseCode nvarchar(20) = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @ContainerNumber nvarchar(60) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @NumberOfLines int = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportPOHeader.InterfaceImportPOHeaderId,
  ///   InterfaceImportPOHeader.PrimaryKey,
  ///   InterfaceImportPOHeader.OrderNumber,
  ///   InterfaceImportPOHeader.RecordType,
  ///   InterfaceImportPOHeader.RecordStatus,
  ///   InterfaceImportPOHeader.SupplierCode,
  ///   InterfaceImportPOHeader.Supplier,
  ///   InterfaceImportPOHeader.Address,
  ///   InterfaceImportPOHeader.FromWarehouseCode,
  ///   InterfaceImportPOHeader.ToWarehouseCode,
  ///   InterfaceImportPOHeader.DeliveryNoteNumber,
  ///   InterfaceImportPOHeader.ContainerNumber,
  ///   InterfaceImportPOHeader.SealNumber,
  ///   InterfaceImportPOHeader.DeliveryDate,
  ///   InterfaceImportPOHeader.Remarks,
  ///   InterfaceImportPOHeader.NumberOfLines,
  ///   InterfaceImportPOHeader.Additional1,
  ///   InterfaceImportPOHeader.Additional2,
  ///   InterfaceImportPOHeader.Additional3,
  ///   InterfaceImportPOHeader.Additional4,
  ///   InterfaceImportPOHeader.Additional5,
  ///   InterfaceImportPOHeader.ProcessedDate,
  ///   InterfaceImportPOHeader.Additional6,
  ///   InterfaceImportPOHeader.Additional7,
  ///   InterfaceImportPOHeader.Additional8,
  ///   InterfaceImportPOHeader.Additional9,
  ///   InterfaceImportPOHeader.Additional10,
  ///   InterfaceImportPOHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPOHeader_Insert
(
 @InterfaceImportPOHeaderId int = null output,
 @PrimaryKey nvarchar(60) = null,
 @OrderNumber nvarchar(60) = null,
 @RecordType char(3) = null,
 @RecordStatus char(1) = null,
 @SupplierCode nvarchar(60) = null,
 @Supplier nvarchar(510) = null,
 @Address nvarchar(510) = null,
 @FromWarehouseCode nvarchar(20) = null,
 @ToWarehouseCode nvarchar(20) = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @ContainerNumber nvarchar(60) = null,
 @SealNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @Remarks nvarchar(510) = null,
 @NumberOfLines int = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceImportPOHeaderId = '-1'
    set @InterfaceImportPOHeaderId = null;
  
	 declare @Error int
 
  insert InterfaceImportPOHeader
        (PrimaryKey,
         OrderNumber,
         RecordType,
         RecordStatus,
         SupplierCode,
         Supplier,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         DeliveryNoteNumber,
         ContainerNumber,
         SealNumber,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         ProcessedDate,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10,
         InsertDate)
  select @PrimaryKey,
         @OrderNumber,
         @RecordType,
         @RecordStatus,
         @SupplierCode,
         @Supplier,
         @Address,
         @FromWarehouseCode,
         @ToWarehouseCode,
         @DeliveryNoteNumber,
         @ContainerNumber,
         @SealNumber,
         @DeliveryDate,
         @Remarks,
         @NumberOfLines,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @ProcessedDate,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9,
         @Additional10,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error, @InterfaceImportPOHeaderId = scope_identity()
  
  
  return @Error
  
end
