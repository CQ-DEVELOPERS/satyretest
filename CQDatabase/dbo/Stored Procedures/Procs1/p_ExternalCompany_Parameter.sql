﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Parameter
  ///   Filename       : p_ExternalCompany_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Jul 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Parameter
(
@ExternalCompany nvarchar(50) = null
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        -1 as ExternalCompanyId
        ,'{All}' as ExternalCompany
  union
  select
         ExternalCompany.ExternalCompanyId
        ,ExternalCompany.ExternalCompany
    from ExternalCompany
   --where ExternalCompany like '%'+@ExternalCompany+'%'
  order by ExternalCompany
  
end
