﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COATest_Delete
  ///   Filename       : p_COATest_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:08
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the COATest table.
  /// </remarks>
  /// <param>
  ///   @COATestId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COATest_Delete
(
 @COATestId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete COATest
     where COATestId = @COATestId
  
  select @Error = @@Error
  
  
  return @Error
  
end
