﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_Webservice_CR
  ///   Filename       : p_Interface_Webservice_CR.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Interface_Webservice_CR
(
 @XMLBody nvarchar(max) output
)
--with encryption
as
begin
  DECLARE @doc xml
  DECLARE @idoc int
  DECLARE @InsertDate datetime
  DECLARE @ERROR AS VARCHAR(255)
	 
	 SELECT @doc = convert(xml,@XMLBody)
	       ,@InsertDate = Getdate()
	 
	 EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	 
	 BEGIN TRY
		  INSERT [InterfaceImportHeader]
          ([RecordStatus]
          ,[InsertDate]
          ,[PrimaryKey]
          ,[OrderNumber]
          ,[InvoiceNumber]
          ,[RecordType]
          ,[CompanyCode]
          ,[Company]
          ,[Address]
          ,[FromWarehouseCode]
          ,[ToWarehouseCode]
          ,[Route]
          ,[DeliveryNoteNumber]
          ,[ContainerNumber]
          ,[SealNumber]
          ,[DeliveryDate]
          ,[Remarks]
          ,[NumberOfLines]
          ,[VatPercentage]
          ,[VatSummary]
          ,[Total]
          ,[Additional1]
          ,[Additional2]
          ,[Additional3]
          ,[Additional4]
          ,[Additional5]
          --,[Additional6]
          --,[Additional7]
          --,[Additional8]
          --,[Additional9]
          ,[Additional10]
          ,[ItemLine])
   SELECT 'W'
         ,@InsertDate
         ,header.h.value('PrimaryKey[1]',         'varchar(30)')  'PrimaryKey'
         ,header.h.value('OrderNumber[1]',        'varchar(30)')  'OrderNumber'
         ,header.h.value('InvoiceNumber[1]',      'varchar(30)')  'InvoiceNumber'
         ,header.h.value('ItemType[1]',           'varchar(30)')  'ItemType'
         --,header.h.value('RecordStatus[1]',       'char(1)') ''
         ,header.h.value('CompanyCode[1]',        'varchar(30)')  'CompanyCode'
         ,header.h.value('Company[1]',            'varchar(255)') 'Company'
         ,header.h.value('Address[1]',            'varchar(255)') 'Address'
         ,header.h.value('FromWarehouseCode[1]',  'varchar(10)')  'FromWarehouseCode'
         ,header.h.value('ToWarehouseCode[1]',    'varchar(10)')  'ToWarehouseCode'
         ,header.h.value('Route[1]',              'varchar(50)')  'Route'
         ,header.h.value('DeliveryNoteNumber[1]', 'varchar(30)')  'DeliveryNoteNumber'
         ,header.h.value('ContainerNumber[1]',    'varchar(30)')  'ContainerNumber'
         ,header.h.value('SealNumber[1]',         'varchar(30)')  'SealNumber'
         ,header.h.value('DeliveryDate[1]',       'varchar(20)')  'DeliveryDate'
         ,header.h.value('Remarks[1]',            'varchar(255)') 'Remarks'
         ,header.h.value('NumberOfLines[1]',      'int')          'NumberOfLines'
         ,CAST(ISNULL(header.h.value('VatPercentage[1]', 'varchar(50)'),'0.00') AS decimal(13,3)) 'VatPercentage'
         ,CAST(ISNULL(header.h.value('VatSummary[1]',    'varchar(50)'),'0.00') AS decimal(13,3)) 'VatSummary'
         ,CAST(ISNULL(header.h.value('Total[1]',         'varchar(50)'),'0.00') AS decimal(13,3)) 'Total'
         ,header.h.value('Additional1[1]',        'varchar(255)') 'Additional1'
         ,header.h.value('Additional2[1]',        'varchar(255)') 'Additional2'
         ,header.h.value('Additional3[1]',        'varchar(255)') 'Additional3'
         ,header.h.value('Additional4[1]',        'varchar(255)') 'Additional4'
         ,header.h.value('Additional5[1]',        'varchar(255)') 'Additional5'
         --,header.h.value('Additional6[1]',        'varchar(255)') 'Additional6'
         --,header.h.value('Additional7[1]',   'varchar(255)') 'Additional7'
         --,header.h.value('Additional8[1]',        'varchar(255)') 'Additional8'
         --,header.h.value('Additional9[1]',        'varchar(255)') 'Additional9'
         ,header.h.value('HoldStatus[1]',       'varchar(255)') 'Additional10'
         --,header.h.value('ProcessedDate[1]',      'datetime
         --,header.h.value('InsertDate[1]',         'datetime		       
         ,header.h.query('ItemLine')
	    FROM @doc.nodes('/root/Body/Item') AS header(h)
   
   INSERT [InterfaceImportDetail]
         ([InterfaceImportHeaderId]
         ,[ForeignKey]
         ,[LineNumber]
         ,[ProductCode]
         ,[Product]
         ,[SKUCode]
         ,[Batch]
         ,[Quantity]
         ,[Weight]
         ,[RetailPrice]
         ,[NetPrice]
         ,[LineTotal]
         ,[Volume]
         ,[Additional1]
         ,[Additional2]
         ,[Additional3]
         ,[Additional4]
         ,[Additional5])
   SELECT h.InterfaceImportHeaderId
         ,detail.d.value('ForeignKey[1]',         'varchar(30)')  'ForeignKey'
         ,detail.d.value('LineNumber[1]',         'int')          'LineNumber'
         ,detail.d.value('ProductCode[1]',        'varchar(30)')  'ProductCode'
         ,detail.d.value('Product[1]',            'varchar(50)')  'Product'
         ,detail.d.value('SKUCode[1]',            'varchar(10)')  'SKUCode'
         ,detail.d.value('Batch[1]',              'varchar(50)')  'Batch'
         ,detail.d.value('Quantity[1]',           'numeric(13,6)')          'Quantity'
         ,CAST(ISNULL(detail.d.value('Weight[1]',      'varchar(50)'),'0.00') AS decimal(13,3))  'Weight'
         ,CAST(ISNULL(detail.d.value('RetailPrice[1]', 'varchar(50)'),'0.00') AS decimal(13,2))  'RetailPrice'
         ,CAST(ISNULL(detail.d.value('NetPrice[1]',    'varchar(50)'),'0.00') AS decimal(13,2))  'NetPrice'
         ,CAST(ISNULL(detail.d.value('LineTotal[1]',   'varchar(50)'),'0.00') AS decimal(13,2))  'LineTotal'
         ,CAST(ISNULL(detail.d.value('Volume[1]',      'varchar(50)'),'0.00') AS decimal(13,3))  'Volume'
         ,detail.d.value('Additional1[1]',        'varchar(255)') 'Additional1'
         ,detail.d.value('Additional2[1]',        'varchar(255)') 'Additional2'
         ,detail.d.value('Additional3[1]',        'varchar(255)') 'Additional3'
         ,detail.d.value('Additional4[1]',        'varchar(255)') 'Additional4'
         ,detail.d.value('Additional5[1]',        'varchar(255)') 'Additional5'
         --,detail.d.value('Additional6[1]',        'varchar(255)') 'Additional6'
         --,detail.d.value('Additional7[1]',        'varchar(255)') 'Additional7'
         --,detail.d.value('Additional8[1]',        'varchar(255)') 'Additional8'
         --,detail.d.value('Additional9[1]',        'varchar(255)') 'Additional9'
         --,detail.d.value('Additional10[1]',       'varchar(255)') 'Additional10'
     from InterfaceImportHeader as h
     cross apply h.ItemLine.nodes('/ItemLine') as detail(d)
    where RecordSTatus = 'W'
      and InsertDate = @InsertDate
    	
	    update InterfaceImportHeader
	       set RecordStatus = 'N'
	          --,ItemLine = null --Save some space
	     where RecordSTatus = 'W'
	       and InsertDate = @InsertDate
	END TRY
	BEGIN CATCH
	    
	   SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)
	END CATCH
	
	exec p_Generic_Import_Order
	
	SET @XMLBody = '<root><status>' 
	    + CASE WHEN LEN(@ERROR) > 0 THEN @ERROR
	           ELSE 'Order(s) successfully imported'
	      END
	    + '</status></root>'
end
