﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_Parameter
  ///   Filename       : p_InboundShipment_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:21
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundShipment table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InboundShipment.InboundShipmentId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InboundShipmentId
        ,null as 'InboundShipment'
  union
  select
         InboundShipment.InboundShipmentId
        ,InboundShipment.InboundShipmentId as 'InboundShipment'
    from InboundShipment
  
end
