﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportSODetail_Search
  ///   Filename       : p_InterfaceExportSODetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:11
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceExportSODetail table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceExportSODetail.InterfaceExportSOHeaderId,
  ///   InterfaceExportSODetail.ForeignKey,
  ///   InterfaceExportSODetail.LineNumber,
  ///   InterfaceExportSODetail.ProductCode,
  ///   InterfaceExportSODetail.Product,
  ///   InterfaceExportSODetail.SKUCode,
  ///   InterfaceExportSODetail.Batch,
  ///   InterfaceExportSODetail.Quantity,
  ///   InterfaceExportSODetail.Weight,
  ///   InterfaceExportSODetail.Additional1,
  ///   InterfaceExportSODetail.Additional2,
  ///   InterfaceExportSODetail.Additional3,
  ///   InterfaceExportSODetail.Additional4,
  ///   InterfaceExportSODetail.Additional5,
  ///   InterfaceExportSODetail.IssueLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportSODetail_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceExportSODetail.InterfaceExportSOHeaderId
        ,InterfaceExportSODetail.ForeignKey
        ,InterfaceExportSODetail.LineNumber
        ,InterfaceExportSODetail.ProductCode
        ,InterfaceExportSODetail.Product
        ,InterfaceExportSODetail.SKUCode
        ,InterfaceExportSODetail.Batch
        ,InterfaceExportSODetail.Quantity
        ,InterfaceExportSODetail.Weight
        ,InterfaceExportSODetail.Additional1
        ,InterfaceExportSODetail.Additional2
        ,InterfaceExportSODetail.Additional3
        ,InterfaceExportSODetail.Additional4
        ,InterfaceExportSODetail.Additional5
        ,InterfaceExportSODetail.IssueLineId
    from InterfaceExportSODetail
  
end
