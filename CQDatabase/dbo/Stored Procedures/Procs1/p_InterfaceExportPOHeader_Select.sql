﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportPOHeader_Select
  ///   Filename       : p_InterfaceExportPOHeader_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:09
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceExportPOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportPOHeaderId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportPOHeader.InterfaceExportPOHeaderId,
  ///   InterfaceExportPOHeader.PrimaryKey,
  ///   InterfaceExportPOHeader.OrderNumber,
  ///   InterfaceExportPOHeader.RecordType,
  ///   InterfaceExportPOHeader.RecordStatus,
  ///   InterfaceExportPOHeader.SupplierCode,
  ///   InterfaceExportPOHeader.Supplier,
  ///   InterfaceExportPOHeader.Address,
  ///   InterfaceExportPOHeader.FromWarehouseCode,
  ///   InterfaceExportPOHeader.ToWarehouseCode,
  ///   InterfaceExportPOHeader.DeliveryNoteNumber,
  ///   InterfaceExportPOHeader.ContainerNumber,
  ///   InterfaceExportPOHeader.SealNumber,
  ///   InterfaceExportPOHeader.DeliveryDate,
  ///   InterfaceExportPOHeader.Remarks,
  ///   InterfaceExportPOHeader.NumberOfLines,
  ///   InterfaceExportPOHeader.Additional1,
  ///   InterfaceExportPOHeader.Additional2,
  ///   InterfaceExportPOHeader.Additional3,
  ///   InterfaceExportPOHeader.Additional4,
  ///   InterfaceExportPOHeader.Additional5,
  ///   InterfaceExportPOHeader.ProcessedDate,
  ///   InterfaceExportPOHeader.Additional6,
  ///   InterfaceExportPOHeader.Additional7,
  ///   InterfaceExportPOHeader.Additional8,
  ///   InterfaceExportPOHeader.Additional9,
  ///   InterfaceExportPOHeader.Additional10,
  ///   InterfaceExportPOHeader.InsertDate,
  ///   InterfaceExportPOHeader.ReceiptId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportPOHeader_Select
(
 @InterfaceExportPOHeaderId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceExportPOHeader.InterfaceExportPOHeaderId
        ,InterfaceExportPOHeader.PrimaryKey
        ,InterfaceExportPOHeader.OrderNumber
        ,InterfaceExportPOHeader.RecordType
        ,InterfaceExportPOHeader.RecordStatus
        ,InterfaceExportPOHeader.SupplierCode
        ,InterfaceExportPOHeader.Supplier
        ,InterfaceExportPOHeader.Address
        ,InterfaceExportPOHeader.FromWarehouseCode
        ,InterfaceExportPOHeader.ToWarehouseCode
        ,InterfaceExportPOHeader.DeliveryNoteNumber
        ,InterfaceExportPOHeader.ContainerNumber
        ,InterfaceExportPOHeader.SealNumber
        ,InterfaceExportPOHeader.DeliveryDate
        ,InterfaceExportPOHeader.Remarks
        ,InterfaceExportPOHeader.NumberOfLines
        ,InterfaceExportPOHeader.Additional1
        ,InterfaceExportPOHeader.Additional2
        ,InterfaceExportPOHeader.Additional3
        ,InterfaceExportPOHeader.Additional4
        ,InterfaceExportPOHeader.Additional5
        ,InterfaceExportPOHeader.ProcessedDate
        ,InterfaceExportPOHeader.Additional6
        ,InterfaceExportPOHeader.Additional7
        ,InterfaceExportPOHeader.Additional8
        ,InterfaceExportPOHeader.Additional9
        ,InterfaceExportPOHeader.Additional10
        ,InterfaceExportPOHeader.InsertDate
        ,InterfaceExportPOHeader.ReceiptId
    from InterfaceExportPOHeader
   where isnull(InterfaceExportPOHeader.InterfaceExportPOHeaderId,'0')  = isnull(@InterfaceExportPOHeaderId, isnull(InterfaceExportPOHeader.InterfaceExportPOHeaderId,'0'))
  
end
