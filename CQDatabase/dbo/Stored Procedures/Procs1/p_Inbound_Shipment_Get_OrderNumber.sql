﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Inbound_Shipment_Get_OrderNumber
  ///   Filename       : p_Inbound_Shipment_Get_OrderNumber.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Mar 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Inbound_Shipment_Get_OrderNumber
(
 @InboundShipmentId int
)

as
begin
	 set nocount on;
  
  select OrderNumber
    from InboundDocument id
    join InboundDocumentType idt on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
  where id.ReferenceNumber = convert(nvarchar(30), @InboundShipmentId)
end
