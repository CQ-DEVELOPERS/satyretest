﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMProduct_Insert
  ///   Filename       : p_BOMProduct_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:13
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the BOMProduct table.
  /// </remarks>
  /// <param>
  ///   @BOMLineId int = null output,
  ///   @LineNumber int = null output,
  ///   @StorageUnitId int = null output,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  ///   BOMProduct.BOMLineId,
  ///   BOMProduct.LineNumber,
  ///   BOMProduct.StorageUnitId,
  ///   BOMProduct.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMProduct_Insert
(
 @BOMLineId int = null output,
 @LineNumber int = null output,
 @StorageUnitId int = null output,
 @Quantity float = null 
)

as
begin
	 set nocount on;
  
  if @BOMLineId = '-1'
    set @BOMLineId = null;
  
  if @LineNumber = '-1'
    set @LineNumber = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
	 declare @Error int
 
  insert BOMProduct
        (BOMLineId,
         LineNumber,
         StorageUnitId,
         Quantity)
  select @BOMLineId,
         @LineNumber,
         @StorageUnitId,
         @Quantity 
  
  select @Error = @@Error, @StorageUnitId = scope_identity()
  
  
  return @Error
  
end
