﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIPHeader_Insert
  ///   Filename       : p_InterfaceImportIPHeader_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:31
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportIPHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportIPHeaderId int = null output,
  ///   @OutboundShipmentId int = null,
  ///   @IssueId int = null,
  ///   @InvoiceDate datetime = null,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @CustomerAddress1 nvarchar(510) = null,
  ///   @CustomerAddress2 nvarchar(510) = null,
  ///   @CustomerAddress3 nvarchar(510) = null,
  ///   @CustomerAddress4 nvarchar(510) = null,
  ///   @CustomerAddress5 nvarchar(510) = null,
  ///   @CustomerAddress6 nvarchar(510) = null,
  ///   @DeliveryAddress1 nvarchar(510) = null,
  ///   @DeliveryAddress2 nvarchar(510) = null,
  ///   @DeliveryAddress3 nvarchar(510) = null,
  ///   @DeliveryAddress4 nvarchar(510) = null,
  ///   @DeliveryAddress5 nvarchar(510) = null,
  ///   @DeliveryAddress6 nvarchar(510) = null,
  ///   @CustomerName nvarchar(100) = null,
  ///   @CustomerCode nvarchar(100) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @ReferenceNumber nvarchar(100) = null,
  ///   @ChargeTax nvarchar(20) = null,
  ///   @TaxReference nvarchar(100) = null,
  ///   @SalesCode nvarchar(100) = null,
  ///   @TotalNettValue numeric(13,2) = null,
  ///   @LogisticsDataFee numeric(13,2) = null,
  ///   @NettExcLogDataFee numeric(13,2) = null,
  ///   @Tax numeric(13,2) = null,
  ///   @TotalDue numeric(13,2) = null,
  ///   @Message1 nvarchar(510) = null,
  ///   @Message2 nvarchar(510) = null,
  ///   @Message3 nvarchar(510) = null,
  ///   @Printed int = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportIPHeader.InterfaceImportIPHeaderId,
  ///   InterfaceImportIPHeader.OutboundShipmentId,
  ///   InterfaceImportIPHeader.IssueId,
  ///   InterfaceImportIPHeader.InvoiceDate,
  ///   InterfaceImportIPHeader.PrimaryKey,
  ///   InterfaceImportIPHeader.CustomerAddress1,
  ///   InterfaceImportIPHeader.CustomerAddress2,
  ///   InterfaceImportIPHeader.CustomerAddress3,
  ///   InterfaceImportIPHeader.CustomerAddress4,
  ///   InterfaceImportIPHeader.CustomerAddress5,
  ///   InterfaceImportIPHeader.CustomerAddress6,
  ///   InterfaceImportIPHeader.DeliveryAddress1,
  ///   InterfaceImportIPHeader.DeliveryAddress2,
  ///   InterfaceImportIPHeader.DeliveryAddress3,
  ///   InterfaceImportIPHeader.DeliveryAddress4,
  ///   InterfaceImportIPHeader.DeliveryAddress5,
  ///   InterfaceImportIPHeader.DeliveryAddress6,
  ///   InterfaceImportIPHeader.CustomerName,
  ///   InterfaceImportIPHeader.CustomerCode,
  ///   InterfaceImportIPHeader.OrderNumber,
  ///   InterfaceImportIPHeader.ReferenceNumber,
  ///   InterfaceImportIPHeader.ChargeTax,
  ///   InterfaceImportIPHeader.TaxReference,
  ///   InterfaceImportIPHeader.SalesCode,
  ///   InterfaceImportIPHeader.TotalNettValue,
  ///   InterfaceImportIPHeader.LogisticsDataFee,
  ///   InterfaceImportIPHeader.NettExcLogDataFee,
  ///   InterfaceImportIPHeader.Tax,
  ///   InterfaceImportIPHeader.TotalDue,
  ///   InterfaceImportIPHeader.Message1,
  ///   InterfaceImportIPHeader.Message2,
  ///   InterfaceImportIPHeader.Message3,
  ///   InterfaceImportIPHeader.Printed,
  ///   InterfaceImportIPHeader.ProcessedDate,
  ///   InterfaceImportIPHeader.RecordStatus,
  ///   InterfaceImportIPHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIPHeader_Insert
(
 @InterfaceImportIPHeaderId int = null output,
 @OutboundShipmentId int = null,
 @IssueId int = null,
 @InvoiceDate datetime = null,
 @PrimaryKey nvarchar(60) = null,
 @CustomerAddress1 nvarchar(510) = null,
 @CustomerAddress2 nvarchar(510) = null,
 @CustomerAddress3 nvarchar(510) = null,
 @CustomerAddress4 nvarchar(510) = null,
 @CustomerAddress5 nvarchar(510) = null,
 @CustomerAddress6 nvarchar(510) = null,
 @DeliveryAddress1 nvarchar(510) = null,
 @DeliveryAddress2 nvarchar(510) = null,
 @DeliveryAddress3 nvarchar(510) = null,
 @DeliveryAddress4 nvarchar(510) = null,
 @DeliveryAddress5 nvarchar(510) = null,
 @DeliveryAddress6 nvarchar(510) = null,
 @CustomerName nvarchar(100) = null,
 @CustomerCode nvarchar(100) = null,
 @OrderNumber nvarchar(60) = null,
 @ReferenceNumber nvarchar(100) = null,
 @ChargeTax nvarchar(20) = null,
 @TaxReference nvarchar(100) = null,
 @SalesCode nvarchar(100) = null,
 @TotalNettValue numeric(13,2) = null,
 @LogisticsDataFee numeric(13,2) = null,
 @NettExcLogDataFee numeric(13,2) = null,
 @Tax numeric(13,2) = null,
 @TotalDue numeric(13,2) = null,
 @Message1 nvarchar(510) = null,
 @Message2 nvarchar(510) = null,
 @Message3 nvarchar(510) = null,
 @Printed int = null,
 @ProcessedDate datetime = null,
 @RecordStatus nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceImportIPHeaderId = '-1'
    set @InterfaceImportIPHeaderId = null;
  
	 declare @Error int
 
  insert InterfaceImportIPHeader
        (OutboundShipmentId,
         IssueId,
         InvoiceDate,
         PrimaryKey,
         CustomerAddress1,
         CustomerAddress2,
         CustomerAddress3,
         CustomerAddress4,
         CustomerAddress5,
         CustomerAddress6,
         DeliveryAddress1,
         DeliveryAddress2,
         DeliveryAddress3,
         DeliveryAddress4,
         DeliveryAddress5,
         DeliveryAddress6,
         CustomerName,
         CustomerCode,
         OrderNumber,
         ReferenceNumber,
         ChargeTax,
         TaxReference,
         SalesCode,
         TotalNettValue,
         LogisticsDataFee,
         NettExcLogDataFee,
         Tax,
         TotalDue,
         Message1,
         Message2,
         Message3,
         Printed,
         ProcessedDate,
         RecordStatus,
         InsertDate)
  select @OutboundShipmentId,
         @IssueId,
         @InvoiceDate,
         @PrimaryKey,
         @CustomerAddress1,
         @CustomerAddress2,
         @CustomerAddress3,
         @CustomerAddress4,
         @CustomerAddress5,
         @CustomerAddress6,
         @DeliveryAddress1,
         @DeliveryAddress2,
         @DeliveryAddress3,
         @DeliveryAddress4,
         @DeliveryAddress5,
         @DeliveryAddress6,
         @CustomerName,
         @CustomerCode,
         @OrderNumber,
         @ReferenceNumber,
         @ChargeTax,
         @TaxReference,
         @SalesCode,
         @TotalNettValue,
         @LogisticsDataFee,
         @NettExcLogDataFee,
         @Tax,
         @TotalDue,
         @Message1,
         @Message2,
         @Message3,
         @Printed,
         @ProcessedDate,
         @RecordStatus,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error, @InterfaceImportIPHeaderId = scope_identity()
  
  
  return @Error
  
end
