﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentContactList_Update
  ///   Filename       : p_InboundDocumentContactList_Update.sql
  ///   Create By      : Karen
  ///   Date Created   : Aug 2011
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InboundDocumentContactList table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null,
  ///   @ContactListId int = null,
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentContactList_Update
(
 @InboundDocumentId int = null,
 @ContactListId int = null
)

as
begin
	 set nocount on;
  
	 declare @Error int
  update InboundDocumentContactList
     set ContactListId = isnull(@ContactListId, ContactListId)
   where InboundDocumentId = @InboundDocumentId
  
  select @Error = @@Error
   
  return @Error
  
end
