﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentDesc_Insert
  ///   Filename       : p_InterfaceDocumentDesc_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:34:58
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceDocumentDesc table.
  /// </remarks>
  /// <param>
  ///   @DocId int = null output,
  ///   @Doc nvarchar(20) = null,
  ///   @DocDesc nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   InterfaceDocumentDesc.DocId,
  ///   InterfaceDocumentDesc.Doc,
  ///   InterfaceDocumentDesc.DocDesc 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentDesc_Insert
(
 @DocId int = null output,
 @Doc nvarchar(20) = null,
 @DocDesc nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
  if @DocId = '-1'
    set @DocId = null;
  
	 declare @Error int
 
  insert InterfaceDocumentDesc
        (Doc,
         DocDesc)
  select @Doc,
         @DocDesc 
  
  select @Error = @@Error, @DocId = scope_identity()
  
  
  return @Error
  
end
