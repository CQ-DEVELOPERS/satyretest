﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLog_Insert
  ///   Filename       : p_InterfaceLog_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:25
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceLog table.
  /// </remarks>
  /// <param>
  ///   @InterfaceLogId int = null output,
  ///   @Data nvarchar(510) = null,
  ///   @RecordStatus char(1) = null,
  ///   @ProcessedDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceLog.InterfaceLogId,
  ///   InterfaceLog.Data,
  ///   InterfaceLog.RecordStatus,
  ///   InterfaceLog.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLog_Insert
(
 @InterfaceLogId int = null output,
 @Data nvarchar(510) = null,
 @RecordStatus char(1) = null,
 @ProcessedDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceLogId = '-1'
    set @InterfaceLogId = null;
  
	 declare @Error int
 
  insert InterfaceLog
        (Data,
         RecordStatus,
         ProcessedDate)
  select @Data,
         @RecordStatus,
         @ProcessedDate 
  
  select @Error = @@Error, @InterfaceLogId = scope_identity()
  
  
  return @Error
  
end
