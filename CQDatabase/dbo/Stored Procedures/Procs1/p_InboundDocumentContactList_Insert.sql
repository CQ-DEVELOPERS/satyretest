﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentContactList_Insert
  ///   Filename       : p_InboundDocumentContactList_Insert.sql
  ///   Create By      : Karen
  ///   Date Created   : August 2011
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundDocumentContactList table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output,
  ///   @ContactListId int = null,
  /// </param>
  /// <returns>
  ///   
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentContactList_Insert
(
 @InboundDocumentId int = null output,
 @ContactListId int = null
)

as
begin
	 set nocount on;
  
	 declare @Error int
  insert InboundDocumentContactList
        (InboundDocumentId,
         ContactListId)
  select @InboundDocumentId,
         @ContactListId 
  
  select @Error = @@Error, @InboundDocumentId = scope_identity()
  

  return @Error
  
end
