﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BatchHistory_Insert
  ///   Filename       : p_BatchHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Sep 2014 15:23:44
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the BatchHistory table.
  /// </remarks>
  /// <param>
  ///   @BatchId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @Batch nvarchar(100) = null,
  ///   @CreateDate datetime = null,
  ///   @ExpiryDate datetime = null,
  ///   @IncubationDays int = null,
  ///   @ShelfLifeDays int = null,
  ///   @ExpectedYield int = null,
  ///   @ActualYield int = null,
  ///   @ECLNumber nvarchar(20) = null,
  ///   @OperatorId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @BatchReferenceNumber nvarchar(100) = null,
  ///   @ProductionDateTime datetime = null,
  ///   @FilledYield int = null,
  ///   @COACertificate bit = null,
  ///   @StatusModifiedBy int = null,
  ///   @StatusModifiedDate datetime = null,
  ///   @CreatedBy int = null,
  ///   @ModifiedBy int = null,
  ///   @ModifiedDate datetime = null,
  ///   @Prints int = null,
  ///   @ParentProductCode nvarchar(60) = null,
  ///   @RI float = null,
  ///   @Density float = null,
  ///   @BOELineNumber nvarchar(100) = null,
  ///   @BillOfEntry nvarchar(100) = null,
  ///   @ReferenceNumber nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   BatchHistory.BatchId,
  ///   BatchHistory.StatusId,
  ///   BatchHistory.WarehouseId,
  ///   BatchHistory.Batch,
  ///   BatchHistory.CreateDate,
  ///   BatchHistory.ExpiryDate,
  ///   BatchHistory.IncubationDays,
  ///   BatchHistory.ShelfLifeDays,
  ///   BatchHistory.ExpectedYield,
  ///   BatchHistory.ActualYield,
  ///   BatchHistory.ECLNumber,
  ///   BatchHistory.OperatorId,
  ///   BatchHistory.CommandType,
  ///   BatchHistory.InsertDate,
  ///   BatchHistory.BatchReferenceNumber,
  ///   BatchHistory.ProductionDateTime,
  ///   BatchHistory.FilledYield,
  ///   BatchHistory.COACertificate,
  ///   BatchHistory.StatusModifiedBy,
  ///   BatchHistory.StatusModifiedDate,
  ///   BatchHistory.CreatedBy,
  ///   BatchHistory.ModifiedBy,
  ///   BatchHistory.ModifiedDate,
  ///   BatchHistory.Prints,
  ///   BatchHistory.ParentProductCode,
  ///   BatchHistory.RI,
  ///   BatchHistory.Density,
  ///   BatchHistory.BOELineNumber,
  ///   BatchHistory.BillOfEntry,
  ///   BatchHistory.ReferenceNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BatchHistory_Insert
(
 @BatchId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @Batch nvarchar(100) = null,
 @CreateDate datetime = null,
 @ExpiryDate datetime = null,
 @IncubationDays int = null,
 @ShelfLifeDays int = null,
 @ExpectedYield int = null,
 @ActualYield int = null,
 @ECLNumber nvarchar(20) = null,
 @OperatorId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @BatchReferenceNumber nvarchar(100) = null,
 @ProductionDateTime datetime = null,
 @FilledYield int = null,
 @COACertificate bit = null,
 @StatusModifiedBy int = null,
 @StatusModifiedDate datetime = null,
 @CreatedBy int = null,
 @ModifiedBy int = null,
 @ModifiedDate datetime = null,
 @Prints int = null,
 @ParentProductCode nvarchar(60) = null,
 @RI float = null,
 @Density float = null,
 @BOELineNumber nvarchar(100) = null,
 @BillOfEntry nvarchar(100) = null,
 @ReferenceNumber nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert BatchHistory
        (BatchId,
         StatusId,
         WarehouseId,
         Batch,
         CreateDate,
         ExpiryDate,
         IncubationDays,
         ShelfLifeDays,
         ExpectedYield,
         ActualYield,
         ECLNumber,
         OperatorId,
         CommandType,
         InsertDate,
         BatchReferenceNumber,
         ProductionDateTime,
         FilledYield,
         COACertificate,
         StatusModifiedBy,
         StatusModifiedDate,
         CreatedBy,
         ModifiedBy,
         ModifiedDate,
         Prints,
         ParentProductCode,
         RI,
         Density,
         BOELineNumber,
         BillOfEntry,
         ReferenceNumber)
  select @BatchId,
         @StatusId,
         @WarehouseId,
         @Batch,
         @CreateDate,
         @ExpiryDate,
         @IncubationDays,
         @ShelfLifeDays,
         @ExpectedYield,
         @ActualYield,
         @ECLNumber,
         @OperatorId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @BatchReferenceNumber,
         @ProductionDateTime,
         @FilledYield,
         @COACertificate,
         @StatusModifiedBy,
         @StatusModifiedDate,
         @CreatedBy,
         @ModifiedBy,
         @ModifiedDate,
         @Prints,
         @ParentProductCode,
         @RI,
         @Density,
         @BOELineNumber,
         @BillOfEntry,
         @ReferenceNumber 
  
  select @Error = @@Error
  
  
  return @Error
  
end
