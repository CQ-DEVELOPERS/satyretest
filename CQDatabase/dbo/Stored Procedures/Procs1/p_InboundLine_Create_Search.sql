﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLine_Create_Search
  ///   Filename       : p_InboundLine_Create_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLine_Create_Search
(
 @InboundDocumentId int = null output
)

as
begin
	 set nocount on;
    
    if @InboundDocumentId = '-1'
      set @InboundDocumentId = null;
  
  if exists(select top 1 1
              from InboundDocument id
              join InboundDocumentType idt on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
             where id.InboundDocumentId = @InboundDocumentId
               and idt.InboundDocumentTypeCode = 'RET')
  begin
    declare @TableResult as table
    (
      InboundDocumentId int,
      InboundLineId int,
      StorageUnitId int,
      ProductCode   nvarchar(30),
      Product       nvarchar(50),
      SKUCode       nvarchar(50),
      Batch         nvarchar(50),
      ECLNumber     nvarchar(10),
      Status        nvarchar(50),
      Quantity      float,
      UnitPrice     numeric(13,2),
      Remarks       nvarchar(255),
      ReasonId      int,
      Reason        nvarchar(50)
    )
    
    insert @TableResult
          (InboundDocumentId,
           InboundLineId,
           StorageUnitId,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           ECLNumber,
           Status,
           Quantity,
           UnitPrice,
           ReasonId)
    select il.InboundDocumentId,
           il.InboundLineId,
           il.StorageUnitId,
           p.ProductCode,
           p.Product,
           sku.SKUCode,
           b.Batch,
           b.ECLNumber,
           s.Status,
           il.Quantity,
           il.UnitPrice,
           isnull(il.ReasonId,-1)
      from InboundLine         il  (nolock)
      join StorageUnit         su  (nolock) on il.StorageUnitId = su.StorageUnitId
      join Status              s   (nolock) on il.StatusId      = s.StatusId
      join Product             p   (nolock) on su.ProductId     = p.ProductId
      join SKU                 sku (nolock) on su.SKUId         = sku.SKUId
      left outer
      join Batch               b   (nolock) on il.BatchId       = b.BatchId
     where il.InboundDocumentId = @InboundDocumentId
    
    update tr
       set Remarks = case when tr.Quantity > ol.Quantity
                          then 'Yellow'
                          else 'Green'
                          end
      from @TableResult     tr
      join InboundDocument  id on tr.InboundDocumentId  = id.InboundDocumentId
      join InterfaceInvoice ii on id.ReferenceNumber    = ii.InvoiceNumber
      join OutboundDocument od on ii.OrderNumber        = od.OrderNumber
      join OutboundLine     ol on od.OutboundDocumentId = ol.OutboundDocumentId
                              and tr.StorageUnitId      = ol.StorageUnitId
     where id.InboundDocumentId = @InboundDocumentId
    
    update @TableResult
       set Remarks = 'Red'
     where Remarks is null
    
    update tr
       set Reason = r.Reason
      from @TableResult tr
      join Reason        r on tr.ReasonId = r.ReasonId
    
    select InboundLineId,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           ECLNumber,
           Status,
           Quantity,
           UnitPrice,
           Remarks,
           ReasonId,
           Reason
      from @TableResult
  end
  else
  begin
    select il.InboundLineId,
           p.ProductCode,
           p.Product,
           sku.SKUCode,
           b.Batch,
           b.ECLNumber,
           s.Status,
           il.Quantity,
           null as 'UnitPrice',
           null as 'Remarks',
           -1 as 'ReasonId',
           null as 'Reason'
      from InboundLine         il  (nolock)
      join StorageUnit         su  (nolock) on il.StorageUnitId = su.StorageUnitId
      join Status              s   (nolock) on il.StatusId      = s.StatusId
      join Product             p   (nolock) on su.ProductId     = p.ProductId
      join SKU                 sku (nolock) on su.SKUId         = sku.SKUId
      left outer
      join Batch               b   (nolock) on il.BatchId       = b.BatchId
    where il.InboundDocumentId = @InboundDocumentId
  end
end
