﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerType_Delete
  ///   Filename       : p_ContainerType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:30
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ContainerType table.
  /// </remarks>
  /// <param>
  ///   @ContainerTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerType_Delete
(
 @ContainerTypeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ContainerType
     where ContainerTypeId = @ContainerTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
