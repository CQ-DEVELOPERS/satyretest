﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLogDetail_Update
  ///   Filename       : p_InterfaceLogDetail_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:28
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceLogDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceLogDetailId int = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @ReceivedByHost datetime = null,
  ///   @ProcessedDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLogDetail_Update
(
 @InterfaceLogDetailId int = null,
 @OrderNumber nvarchar(60) = null,
 @ReceivedByHost datetime = null,
 @ProcessedDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceLogDetailId = '-1'
    set @InterfaceLogDetailId = null;
  
  if @OrderNumber = '-1'
    set @OrderNumber = null;
  
	 declare @Error int
 
  update InterfaceLogDetail
     set OrderNumber = isnull(@OrderNumber, OrderNumber),
         ReceivedByHost = isnull(@ReceivedByHost, ReceivedByHost),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate) 
   where InterfaceLogDetailId = @InterfaceLogDetailId
  
  select @Error = @@Error
  
  
  return @Error
  
end
