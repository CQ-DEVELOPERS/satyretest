﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Checking_StoreLocation_Clear
  ///   Filename       : p_Checking_StoreLocation_Clear.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Jul 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Checking_StoreLocation_Clear
	@IssueId Int
	

as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Checking_StoreLocation_Clear',
          @GetDate           datetime,
          @Transaction       bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  if (Select Top 1 odt.OutboundDocumentTypeCode
      From Issue					   iss (nolock)
       Inner Join OutboundDocument		od (nolock) on iss.OutboundDocumentId = od.OutboundDocumentId
       Inner Join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      Where IssueId = @IssueId) in ('WAV')
      
      RETURN
  
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  create table #StockList
	(JobId				Int
	,StorageUnitBatchId	Int
	,LocationId			Int
	,Quantity			Float)

	Insert Into #Stocklist
		(JobId
		,StorageUnitBatchId
		,Quantity)
	Select ins.JobId
		     ,ins.StorageUnitBatchId
		     ,SUM(ins.CheckQuantity)
   From IssueLineInstruction ili (nolock) 
   Join Instruction          ins (nolock) On ili.InstructionId	= ins.InstructionRefId or ili.InstructionId = ins.InstructionId
  Where ili.IssueId = @IssueId
    And ISNULL(ins.CheckQuantity, 0) > 0
    And Exists	(select 1
	                 From Location			  loc (nolock) 
	                 Join AreaLocation  al (nolock) on loc.LocationId			= al.LocationId
	                 Join Area		    a (nolock) on al.AreaId				= a.AreaId
	                Where a.AreaCode in ('CK','D')
	                  And ins.StoreLocationId	= loc.LocationId)
  Group By  ins.JobId,
            ins.StorageUnitBatchId

	Update st
	   set LocationId = ins.StoreLocationId
	  from #Stocklist   st          INNER JOIN
		   Instruction ins (nolock) On st.JobId = ins.JobId
	 where ins.StoreLocationId IS NOT NULL
	 
	Update subl
	   set ActualQuantity = CASE WHEN (Quantity > ActualQuantity) THEN 0 ELSE ActualQuantity - Quantity END
	  From (Select StorageUnitBatchId 
				  ,LocationId
				  ,SUM(Quantity) Quantity 
			From #Stocklist
			Group By StorageUnitBatchId 
				  ,LocationId) st	INNER JOIN
	  	   StorageUnitBatchLocation subl ON st.StorageUnitBatchId = subl.StorageUnitBatchId
									  AND st.LocationId = subl.LocationId
 
	Delete From subl
	  From StorageUnitBatchLocation subl
	Where ActualQuantity     = 0
	  and AllocatedQuantity  = 0
	  and ReservedQuantity   = 0
	  and exists (Select 1 from #Stocklist st 
				  Where st.StorageUnitBatchId = subl.StorageUnitBatchId
					AND st.LocationId = subl.LocationId)			
       
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      return 0
    
    error:
      if @Transaction = 1
      begin
        rollback transaction
		RAISERROR (@Errormsg,11,1)

      end
      return @Error
end
