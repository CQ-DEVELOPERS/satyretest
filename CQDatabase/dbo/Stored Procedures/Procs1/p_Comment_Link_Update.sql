﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Comment_Link_Update
  ///   Filename       : p_Comment_Link_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jan 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Comment_Link_Update
(
 @CommentId       int,
 @Comment         nvarchar(max),
 @OperatorId      int
)

as
begin
	 set nocount on;
  
  declare @Error                  int,
          @Errormsg               varchar(500),
          @GetDate                datetime,
          @CommentCallOffHeaderId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update Comment
     set Comment      = @Comment,
         OperatorId   = @OperatorId,
         ModifiedDate = @GetDate
   where CommentId = @CommentId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Comment_Link_Update'); 
    rollback transaction
    return @Error
end
