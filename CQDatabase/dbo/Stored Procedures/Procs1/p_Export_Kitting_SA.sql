﻿/*
  /// <summary>
  ///   Procedure Name : p_Export_Kitting_SA
  ///   Filename       : p_Export_Kitting_SA.sql
  ///   Create By      : Karen
  ///   Date Created   : February 2016
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Export_Kitting_SA
(
 @ReceiptId int 
)
as
begin
  set nocount on
  
  
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @ProcessedDate        varchar(20),
          @FromWarehouseCode    varchar(10),
          @ProductCode          varchar(30),
          @Quantity             varchar(10),
          @ReasonCode           char(1),
          @Batch                varchar(50),
          @ExpiryDate           varchar(20),
          -- Internal
          @WarehouseId          int,
          @InstructionId        int,
          @fetch_status_header  int,
          @fetch_status_detail  int,
          @print_string         varchar(255),
          @InterfaceExportStockAdjustmentId int,
          @AdjustmentType		nvarchar(10),
          @SOOrderNumber		nvarchar(30),
          @POOrderNumber		nvarchar(30),
          @OrderNumber			nvarchar(30),
          @IssueId				int,
          @IssueLineId			int,
          @ConfirmedQuatity		float,
          @StorageUnitBatchId	int,
          @AllLinesChecked		bit = 1
  
   declare @TableResult as Table
  (
   OrderNumber			nvarchar(30),
   IssueId				int,
   IssueLineId			int,
   ConfirmedQuatity		float,
   StorageUnitBatchId	int,
   WarehouseId			int,
   StatusCode			nvarchar(10)

  )
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @AdjustmentType = 'ADJ'  
  
  set @ReasonCode = 'KITTING'
  
  set @POOrderNumber = (select top 1 OrderNumber 
						  from Receipt r (nolock) 
						  join InboundDocument id (nolock) on r.InboundDocumentId = id.InboundDocumentId
						 where r.ReceiptId = @ReceiptId)
						  
  set @POOrderNumber = SUBSTRING(@POOrderNumber,4,27) 
  set @POOrderNumber = RTRIM(LTRIM(@POOrderNumber))
  set @SOOrderNumber = (select top 1 OrderNumber 
						  from OutboundDocument od (nolock)
						 where od.OrderNumber like '%'+@POOrderNumber +'%')
  				 
  --print '@POOrderNumber'
  --print @POOrderNumber
  --print '@SOOrderNumber'
  --print @SOOrderNumber
  
   insert @TableResult
          (OrderNumber,
           IssueId,
           IssueLineId,
           ConfirmedQuatity,
           StorageUnitBatchId,
           WarehouseId,
           StatusCode)
   select OrderNumber,
		  i.IssueId,
		  il.IssueLineId,
		  ins.ConfirmedQuantity,
		  ins.StorageUnitBatchId,
		  ins.WarehouseId,
		  StatusCode
	 from OutboundDocument	od (nolock)
	 join Issue				 i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
	 join IssueLine			il (nolock) on i.IssueId = il.IssueId
	 join Status			 s (nolock) on s.StatusId = il.StatusId
	 join Instruction	   ins (nolock) on il.IssueLineId = ins.IssueLineId
    where od.OrderNumber like '%'+@SOOrderNumber+'%'
	
	--select * from @TableResult
	
	select @AllLinesChecked = 0
	  from @TableResult 
  	 where StatusCode != 'CD'
  	 
--begin transaction	
	if  @AllLinesChecked = 1
	begin
    while exists(select top 1 1 from @TableResult)
    begin
    
    select	top 1   @OrderNumber		= OrderNumber,
					@IssueId			= IssueId,
					@IssueLineId		= IssueLineId,
					@ConfirmedQuatity	= ConfirmedQuatity * -1, -- stock needs to down-adjust,
					@StorageUnitBatchId = StorageUnitBatchId,
					@WarehouseId		= WarehouseId
			  from  @TableResult tr
	  
	  delete @TableResult
	   where IssueLineId = @IssueLineId
			 		  
	  set @FromWarehouseCode = (select WarehouseCode 
								  from Warehouse w (nolock) 
								 where WarehouseId = @WarehouseId)	
								 	  
      if @FromWarehouseCode is not null  
      begin  
        insert InterfaceExportStockAdjustment  
              (RecordType,  
               RecordStatus,  
               ProductCode,  
               Product,
               SKUCode,  
               Batch,  
               Quantity,  
               Additional1,
               Additional3,
               PrincipalCode)  
        select @AdjustmentType,  
               'N',  
               p.ProductCode,  
               p.Product,
               sku.SKUCode,  
               b.Batch,  
               @ConfirmedQuatity,  
               @FromWarehouseCode,
               @ReasonCode,
               pn.PrincipalCode
          
from StorageUnitBatch sub (nolock)
          join StorageUnit       su (nolock) on sub.StorageUnitId      = su.StorageUnitId
          join Product            p (nolock) on su.ProductId          = p.ProductId
          join SKU              sku (nolock) on su.SKUId              = sku.SKUId
          join Batch              b (nolock) on sub.BatchId            = b.BatchId
          left
          join Principal         pn (nolock) on p.PrincipalId         = pn.PrincipalId
         where sub.StorageUnitBatchId = @StorageUnitBatchId
          
        select @Error = @@Error  
          
        --if @Error <> 0  
        --  goto error  
      end  
    end
    end
 --select * from viewSA
 --where InsertDate > '2016-02-15'  
--rollback

end
