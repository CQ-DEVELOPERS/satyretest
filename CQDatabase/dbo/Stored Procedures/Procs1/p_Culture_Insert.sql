﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Culture_Insert
  ///   Filename       : p_Culture_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:35
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Culture table.
  /// </remarks>
  /// <param>
  ///   @CultureId int = null output,
  ///   @CultureName nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   Culture.CultureId,
  ///   Culture.CultureName 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Culture_Insert
(
 @CultureId int = null output,
 @CultureName nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @CultureId = '-1'
    set @CultureId = null;
  
	 declare @Error int
 
  insert Culture
        (CultureName)
  select @CultureName 
  
  select @Error = @@Error, @CultureId = scope_identity()
  
  
  return @Error
  
end
