﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompanyType_Parameter
  ///   Filename       : p_ExternalCompanyType_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:40
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ExternalCompanyType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ExternalCompanyType.ExternalCompanyTypeId,
  ///   ExternalCompanyType.ExternalCompanyType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompanyType_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as ExternalCompanyTypeId
        ,'{All}' as ExternalCompanyType
  union
  select
         ExternalCompanyType.ExternalCompanyTypeId
        ,ExternalCompanyType.ExternalCompanyType
    from ExternalCompanyType
  
end
