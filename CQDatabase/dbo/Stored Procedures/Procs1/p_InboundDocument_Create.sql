﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Create
  ///   Filename       : p_InboundDocument_Create.sql
  ///   Create By      : Grant Schultza
  ///   Date Created   : 06 Jul 2007 19:09:53
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output,
  ///   @InboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber nvarchar(30) = null,
  ///   @DeliveryDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null 
  /// </param>
  /// <returns>
  ///   InboundDocument.InboundDocumentId,
  ///   InboundDocument.InboundDocumentTypeId,
  ///   InboundDocument.ExternalCompanyId,
  ///   InboundDocument.StatusId,
  ///   InboundDocument.WarehouseId,
  ///   InboundDocument.OrderNumber,
  ///   InboundDocument.DeliveryDate,
  ///   InboundDocument.CreateDate,
  ///   InboundDocument.ModifiedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Create
(
 @InboundDocumentId int = null output,
 @InboundDocumentTypeId int = null,
 @PrincipalId           int = null,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @OrderNumber nvarchar(30) = null,
 @DeliveryDate datetime = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null,
 @ReferenceNumber nvarchar(30) = null,
 @ReasonId int = null,
 @Remarks nvarchar(250) = null,
 @ContactListId int = null
)

as
begin
  set nocount on;
  
  declare @Error   int,
          @Errormsg          nvarchar(500),
          @GetDate datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @ExternalCompanyId = -1 or @ExternalCompanyId = 0
    set @ExternalCompanyId = null
  
  if @ContactListId = -1 or @ContactListId = 0
    set @ContactListId = null

  if @PrincipalId = -1 or @PrincipalId = 0
    set @PrincipalId = null
  
  if @ExternalCompanyId is null
  begin
    select @ExternalCompanyId = ExternalCompanyId
      from InboundDocument      id (nolock)
      join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
     where OrderNumber                  = @OrderNumber
       and idt.InboundDocumentTypeCode != 'RET'
    
    if @ExternalCompanyId is null -- Check if you can find the customer via the invoice number (if its a return)
      select @ExternalCompanyId = ExternalCompanyId
        from OutboundDocument (nolock)
       where ReferenceNumber = @ReferenceNumber
  end
  
  if exists(select OrderNumber
              from InboundDocument id
              join InboundDocumentType idt on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
             where ExternalCompanyId         = @ExternalCompanyId
               and OrderNumber               = @OrderNumber
               and idt.InboundDocumentTypeId = @InboundDocumentTypeId)
  begin
    RAISERROR (900000,-1,-1, 'p_InboundDocument_Create - Duplicate Order Number / Supplier'); 
    return -1
  end
  
  if @OrderNumber is null
    set @OrderNumber = ''
  
  if @StatusId is null
    select @StatusId = StatusId
      from Status (nolock)
     where StatusCode = 'N'
       and Type       = 'ID'
  
  if @CreateDate is null
    set @CreateDate = @GetDate
  
  begin transaction
  
  exec @Error = p_InboundDocument_Insert
   @InboundDocumentId = @InboundDocumentId output,
   @InboundDocumentTypeId = @InboundDocumentTypeId,
   @PrincipalId = @PrincipalId,
   @ExternalCompanyId = @ExternalCompanyId,
   @StatusId = @StatusId,
   @WarehouseId = @WarehouseId,
   @OrderNumber = @OrderNumber,
   @DeliveryDate = @DeliveryDate,
   @CreateDate = @CreateDate,
   @ModifiedDate = @ModifiedDate,
   @ReferenceNumber = @ReferenceNumber,
   @Remarks         = @Remarks
  
  if @OrderNumber = ''
  begin
    set @OrderNumber = CONVERT(nvarchar(30), @InboundDocumentId)
    
    exec @Error = p_InboundDocument_Update
     @InboundDocumentId = @InboundDocumentId,
     @OrderNumber       = @OrderNumber
    
    if @Error <> 0
      goto error
  end
  
  if @InboundDocumentTypeId = 5 and @ContactListId != 0
    exec @Error = p_InboundDocumentContactList_Insert
     @InboundDocumentId = @InboundDocumentId ,
     @ContactListId = @ContactListId
  
  if @Error <> 0
    goto error
  
  if @ReasonId not in (-1,0)
    update InboundDocument
       set ReasonId = @ReasonId
     where InboundDocumentId = @InboundDocumentId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return 0
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_InboundDocument_Create'); 
    rollback transaction
    return @Error
end
