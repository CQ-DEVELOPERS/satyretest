﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ApplicationException_Delete
  ///   Filename       : p_ApplicationException_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:37
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ApplicationException table.
  /// </remarks>
  /// <param>
  ///   @ApplicationExceptionId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ApplicationException_Delete
(
 @ApplicationExceptionId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ApplicationException
     where ApplicationExceptionId = @ApplicationExceptionId
  
  select @Error = @@Error
  
  
  return @Error
  
end
