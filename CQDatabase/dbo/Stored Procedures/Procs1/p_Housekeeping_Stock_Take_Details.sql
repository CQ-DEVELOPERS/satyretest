﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Details
  ///   Filename       : p_Housekeeping_Stock_Take_Details.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   @referenceNumber nvarchar(30)
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Details
(
 @referenceNumber nvarchar(30)
)

as
begin
	 set nocount on;
	 
  select j.ReferenceNumber,
         a.Area,
         max(i.CreateDate) as 'CreateDate',
         s.Status,
         count(1) as 'Count',
         j.JobId
    from Job           j (nolock)
    join Status        s (nolock) on j.StatusId       = s.StatusId
    join Instruction   i (nolock) on j.JobId          = i.JobId
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join AreaLocation al (nolock) on i.PickLocationId = al.LocationId
    join Area          a (nolock) on al.AreaId        = a.AreaId
   where it.InstructionTypeCode in ('STL','STA','STP')
     and j.ReferenceNumber like @referenceNumber + '%'
  group by j.ReferenceNumber,
           a.Area,
           s.Status,
           j.JobId
end
