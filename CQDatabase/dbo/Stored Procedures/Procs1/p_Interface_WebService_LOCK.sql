﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_LOCK
  ///   Filename       : p_Interface_WebService_LOCK.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Interface_WebService_LOCK]
(
 @XMLBody nvarchar(max) output
)
--with encryption
as
begin

    DECLARE @GETDATE DATETIME
    
    SELECT @GETDATE = dbo.ufn_GetDate()
    
    UPDATE InterfaceExportHeader
    SET ProcessedDate = @GETDATE
    WHERE RecordStatus = 'N'
    AND RecordType = 'Allocated'
    AND ProcessedDate IS NULL

	SELECT @XMLBody = '<?xml version="1.0" encoding="utf-16"?>' +
        (SELECT 
            (SELECT 
                 OrderNumber                    AS 'OrderNumber'
                 From InterfaceExportHeader
                 Where RecordStatus = 'N'
                 AND ProcessedDate = @GETDATE
                 AND RecordType = 'Allocated'
             FOR XML PATH('OrderPick'), TYPE)
        FOR XML PATH('root'))
        
    UPDATE InterfaceExportHeader
    SET RecordStatus = 'Y'
    WHERE RecordStatus = 'N'
    AND RecordType = 'Allocated'
    AND ProcessedDate = @GETDATE
    
End
