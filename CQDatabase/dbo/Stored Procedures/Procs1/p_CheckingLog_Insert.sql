﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CheckingLog_Insert
  ///   Filename       : p_CheckingLog_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:15
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the CheckingLog table.
  /// </remarks>
  /// <param>
  ///   @CheckingLogId int = null output,
  ///   @WarehouseId int = null,
  ///   @ReferenceNumber nvarchar(100) = null,
  ///   @JobId int = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @StorageUnitId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @OperatorId int = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null 
  /// </param>
  /// <returns>
  ///   CheckingLog.CheckingLogId,
  ///   CheckingLog.WarehouseId,
  ///   CheckingLog.ReferenceNumber,
  ///   CheckingLog.JobId,
  ///   CheckingLog.Barcode,
  ///   CheckingLog.StorageUnitId,
  ///   CheckingLog.StorageUnitBatchId,
  ///   CheckingLog.Batch,
  ///   CheckingLog.Quantity,
  ///   CheckingLog.OperatorId,
  ///   CheckingLog.StartDate,
  ///   CheckingLog.EndDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CheckingLog_Insert
(
 @CheckingLogId int = null output,
 @WarehouseId int = null,
 @ReferenceNumber nvarchar(100) = null,
 @JobId int = null,
 @Barcode nvarchar(100) = null,
 @StorageUnitId int = null,
 @StorageUnitBatchId int = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @OperatorId int = null,
 @StartDate datetime = null,
 @EndDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @CheckingLogId = '-1'
    set @CheckingLogId = null;
  
	 declare @Error int
 
  insert CheckingLog
        (WarehouseId,
         ReferenceNumber,
         JobId,
         Barcode,
         StorageUnitId,
         StorageUnitBatchId,
         Batch,
         Quantity,
         OperatorId,
         StartDate,
         EndDate)
  select @WarehouseId,
         @ReferenceNumber,
         @JobId,
         @Barcode,
         @StorageUnitId,
         @StorageUnitBatchId,
         @Batch,
         @Quantity,
         @OperatorId,
         @StartDate,
         @EndDate 
  
  select @Error = @@Error, @CheckingLogId = scope_identity()
  
  
  return @Error
  
end
