﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_Parameter
  ///   Filename       : p_Exception_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:06
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Exception table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Exception.ExceptionId,
  ///   Exception.Exception 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as ExceptionId
        ,'{All}' as Exception
  union
  select
         Exception.ExceptionId
        ,Exception.Exception
    from Exception
  
end
