﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPack_Update
  ///   Filename       : p_InterfaceImportPack_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:53
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportPack table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportProductId int = null,
  ///   @PackCode nvarchar(60) = null,
  ///   @PackDescription nvarchar(510) = null,
  ///   @Quantity float = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @Length numeric(13,6) = null,
  ///   @Width numeric(13,6) = null,
  ///   @Height numeric(13,6) = null,
  ///   @Volume float = null,
  ///   @NettWeight float = null,
  ///   @GrossWeight float = null,
  ///   @TareWeight float = null,
  ///   @ProductCategory char(1) = null,
  ///   @PackingCategory char(1) = null,
  ///   @PickEmpty bit = null,
  ///   @StackingCategory int = null,
  ///   @MovementCategory int = null,
  ///   @ValueCategory int = null,
  ///   @StoringCategory int = null,
  ///   @PickPartPallet int = null,
  ///   @HostId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPack_Update
(
 @InterfaceImportProductId int = null,
 @PackCode nvarchar(60) = null,
 @PackDescription nvarchar(510) = null,
 @Quantity float = null,
 @Barcode nvarchar(100) = null,
 @Length numeric(13,6) = null,
 @Width numeric(13,6) = null,
 @Height numeric(13,6) = null,
 @Volume float = null,
 @NettWeight float = null,
 @GrossWeight float = null,
 @TareWeight float = null,
 @ProductCategory char(1) = null,
 @PackingCategory char(1) = null,
 @PickEmpty bit = null,
 @StackingCategory int = null,
 @MovementCategory int = null,
 @ValueCategory int = null,
 @StoringCategory int = null,
 @PickPartPallet int = null,
 @HostId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceImportPack
     set InterfaceImportProductId = isnull(@InterfaceImportProductId, InterfaceImportProductId),
         PackCode = isnull(@PackCode, PackCode),
         PackDescription = isnull(@PackDescription, PackDescription),
         Quantity = isnull(@Quantity, Quantity),
         Barcode = isnull(@Barcode, Barcode),
         Length = isnull(@Length, Length),
         Width = isnull(@Width, Width),
         Height = isnull(@Height, Height),
         Volume = isnull(@Volume, Volume),
         NettWeight = isnull(@NettWeight, NettWeight),
         GrossWeight = isnull(@GrossWeight, GrossWeight),
         TareWeight = isnull(@TareWeight, TareWeight),
         ProductCategory = isnull(@ProductCategory, ProductCategory),
         PackingCategory = isnull(@PackingCategory, PackingCategory),
         PickEmpty = isnull(@PickEmpty, PickEmpty),
         StackingCategory = isnull(@StackingCategory, StackingCategory),
         MovementCategory = isnull(@MovementCategory, MovementCategory),
         ValueCategory = isnull(@ValueCategory, ValueCategory),
         StoringCategory = isnull(@StoringCategory, StoringCategory),
         PickPartPallet = isnull(@PickPartPallet, PickPartPallet),
         HostId = isnull(@HostId, HostId) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
