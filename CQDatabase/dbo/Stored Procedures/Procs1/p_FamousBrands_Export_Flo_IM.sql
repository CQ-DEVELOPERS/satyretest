﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_Flo_IM
  ///   Filename       : p_FamousBrands_Export_Flo_IM.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Export_Flo_IM
(
 @FileName varchar(30) = null output
)

as
begin
	 set nocount on;
  --select @FileName = 'InvoiceMaster.txt'
  select @FileName = ''
  
  select UniqueNumber + ',"' + 
         InvoiceNumber + '","' + 
         Detail + '","' + 
         CustomerNumber + '","' + 
         convert(varchar(20), DeliveryDateFrom, 120) + '","' + 
         convert(varchar(20), DeliveryDateTo, 120) + '","' + 
         Preschedule + '","' + 
         convert(varchar(20), isnull(AmountOfInvoice,0)) + '","' + 
         convert(varchar(20), isnull(COD,0)) + '","' + 
         isnull(PickUpCustomer,'') + '","' + 
         convert(varchar(10), isnull(LoadPriority,0)) + '","' + 
         isnull(JobWindowStart,'') + '","' + 
         isnull(JobWindowEnd,'') + '","' + 
         isnull(JobDuration,'') + '","' + 
         isnull(HoldStatus,'') + '","' + 
         isnull(SpecialInstructions,'') + '"'
    from FloInvoiceMasterExport
   where RecordStatus = 'N'
  
  update FloInvoiceMasterExport
     set RecordStatus = 'Y'
   where RecordStatus = 'N'
end
