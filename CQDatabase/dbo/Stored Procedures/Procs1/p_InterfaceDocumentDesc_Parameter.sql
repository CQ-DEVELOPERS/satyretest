﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentDesc_Parameter
  ///   Filename       : p_InterfaceDocumentDesc_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:35:00
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceDocumentDesc table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceDocumentDesc.DocId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentDesc_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as DocId
        ,null as 'InterfaceDocumentDesc'
  union
  select
         InterfaceDocumentDesc.DocId
        ,InterfaceDocumentDesc.DocId as 'InterfaceDocumentDesc'
    from InterfaceDocumentDesc
  
end
