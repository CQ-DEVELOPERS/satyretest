﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIPDetail_Search
  ///   Filename       : p_InterfaceImportIPDetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:30
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportIPDetail table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportIPDetail.InterfaceImportIPHeaderId,
  ///   InterfaceImportIPDetail.ForeignKey,
  ///   InterfaceImportIPDetail.ProductCode,
  ///   InterfaceImportIPDetail.Product,
  ///   InterfaceImportIPDetail.Batch,
  ///   InterfaceImportIPDetail.Quantity,
  ///   InterfaceImportIPDetail.Excluding,
  ///   InterfaceImportIPDetail.Including,
  ///   InterfaceImportIPDetail.NettValue,
  ///   InterfaceImportIPDetail.LineNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIPDetail_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportIPDetail.InterfaceImportIPHeaderId
        ,InterfaceImportIPDetail.ForeignKey
        ,InterfaceImportIPDetail.ProductCode
        ,InterfaceImportIPDetail.Product
        ,InterfaceImportIPDetail.Batch
        ,InterfaceImportIPDetail.Quantity
        ,InterfaceImportIPDetail.Excluding
        ,InterfaceImportIPDetail.Including
        ,InterfaceImportIPDetail.NettValue
        ,InterfaceImportIPDetail.LineNumber
    from InterfaceImportIPDetail
  
end
