﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Inbound_Shipment_Create_Search
  ///   Filename       : p_Inbound_Shipment_Create_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Inbound_Shipment_Create_Search
(
 @InboundShipmentId	    int,
 @InboundDocumentTypeId	int,
 @ExternalCompanyCode	  nvarchar(30),
 @ExternalCompany	      nvarchar(255),
 @OrderNumber	          nvarchar(30),
 @FromDate	             datetime,
 @ToDate	               datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
 (
  ReceiptId            int,
  InboundDocumentId    int,
  InboundShipmentId    int,
  OrderNumber          nvarchar(30),
  ExternalCompanyCode  nvarchar(30),
  ExternalCompany      nvarchar(255),
  NumberOfLines        int,
  DeliveryDate         datetime,
  StatusId             int,
  Status               nvarchar(50),
  InboundDocumentType  nvarchar(50),
  LocationId           int,
  Location             nvarchar(15),
  CreateDate           datetime,
  Rating               int
   );
  
  insert @TableResult
        (ReceiptId,
         id.InboundDocumentId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         DeliveryDate,
         StatusId,
         Status,
         InboundDocumentType,
         LocationId,
         Location,
         CreateDate,
         Rating)
  select r.ReceiptId,
         id.InboundDocumentId,
         id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         id.DeliveryDate,
         r.StatusId,
         s.Status,
         idt.InboundDocumentType,
         l.LocationId,
         l.Location,
         id.CreateDate,
         ec.Rating
    from InboundDocument     id  (nolock)
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    join ExternalCompany     ec  (nolock) on id.ExternalCompanyId     = ec.ExternalCompanyId
    join Receipt             r   (nolock) on id.InboundDocumentId     = r.InboundDocumentId
    join Status              s   (nolock) on r.StatusId               = s.StatusId
    join Location            l   (nolock) on r.LocationId             = l.LocationId
   where id.InboundDocumentTypeId  = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
     and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and id.OrderNumber         like isnull(@OrderNumber  + '%', id.OrderNumber)
     and r.DeliveryDate      between @FromDate and @ToDate
     and s.Type                    = 'R'
     and s.StatusCode             in ('W','C','D') -- Waiting, Confirmed, Delivered
  
  update @TableResult
     set InboundShipmentId = isr.InboundShipmentId
    from @TableResult t
    join InboundShipmentReceipt isr on t.ReceiptId = isr.ReceiptId
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from InboundLine il
                           where t.InboundDocumentId = il.InboundDocumentId)
    from @TableResult t
  
  select InboundShipmentId,
         ReceiptId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         NumberOfLines,
         DeliveryDate,
         StatusId,
         Status,
         InboundDocumentType,
         LocationId,
         Location,
         CreateDate,
         Rating
    from @TableResult
   where InboundShipmentId is not null
end
