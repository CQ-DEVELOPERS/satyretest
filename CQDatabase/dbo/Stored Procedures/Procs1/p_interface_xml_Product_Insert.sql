﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_interface_xml_Product_Insert
  ///   Filename       : p_interface_xml_Product_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Dec 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_interface_xml_Product_Insert
(
 @ProductCode        varchar(30),
 @Product            varchar(255),
 @SKUCode            varchar(10) = null,
 @SKU                varchar(255) = null,
 @Batch              varchar(50) = null,
 @UOM                varchar(50) = null,
 @WarehouseId        int,
 @StorageUnitId      int = null output,
 @BatchId            int = null output,
 @StorageUnitBatchId int = null output,
 @PalletQuantity     int = null,
 @PackQuantity       int = null,
 @Weight             numeric(13,3) = null,
 @Length             int = null,
 @Width              int = null,
 @Height             int = null,
 @ExternalCompanyId  int = null,
 @PrincipalId        int = null,
 @BillOfEntry        nvarchar(50) = null,
 @BOELineNumber      nvarchar(50) = null,
 @BatchReferenceNumber nvarchar(50) = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @ProductId         int,
          @StatusId          int,
          @SKUId             int,
          @AreaId            int,
          @LocationId        int,
          @Volume            int,
          @UOMId             int,
          @UOMCode           varchar(10),
          @PackId            int,
          @PackTypeId		 int,
          @newSKUId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
  
  begin transaction
  
  if @Batch is null or @Batch = ''
    set @Batch = 'Default'
  
  if @BillOfEntry = ''
    set @BillOfEntry = null
  
  if @BOELineNumber = ''
    set @BOELineNumber = null
  
  set @Product = left(@Product,50)
  set @SKU     = left(@SKU,50)
  set @UOM     = left(@UOM, 50)
  
  if @ExternalCompanyId is not null
  begin
    select @StorageUnitId = StorageUnitId
      from StorageUnitExternalCompany (nolock)
     where ExternalCompanyId = @ExternalCompanyId
       and ProductCode = @ProductCode
       and SKUCode     = @SKUCode
    
    if @StorageUnitId is not null
    begin
      select @StorageUnitBatchId = StorageUnitBatchId,
             @BatchId            = b.BatchId
        from StorageUnitBatch sub (nolock)
        join Batch              b (nolock) on sub.BatchId = b.BatchId
       where sub.StorageUnitId = @StorageUnitId
         and b.Batch           = 'Default'
      
      commit transaction
      return
    end
  end
  
  if @PrincipalId is null
    select @ProductId = ProductId
      from Product
     where ProductCode = @ProductCode
  else
    select @ProductId = ProductId
      from Product
     where ProductCode = @ProductCode
       and PrincipalId = @PrincipalId
  
  if @WarehouseId is null
    set @WarehouseId = 1
  
  if (select dbo.ufn_Configuration(450, @WarehouseId)) = 1 -- Document Import - Product must already exist
  and @ProductId is null
  begin
    select @ErrorMsg = 'Error executing p_Product_Insert'
    goto error
  end
  
  if @ProductId is null
  begin
    select @StatusId = dbo.ufn_StatusId('P','A')
    
    if @Product is null
      set @Product = @ProductCode
    
    exec @Error = p_Product_Insert
     @ProductId                  = @ProductId output,
     @StatusId                   = @StatusId,
     @ProductCode                = @ProductCode,
     @Product                    = @Product,
     @Barcode                    = null,
     @MinimumQuantity            = null,
     @ReorderQuantity            = null,
     @MaximumQuantity            = null,
     @CuringPeriodDays           = null,
     @ShelfLifeDays              = null,
     @QualityAssuranceIndicator  = 1,
     @PrincipalId                = @PrincipalId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Product_Insert'
      goto error
    end
  end
--  else
--  begin
--    if @Product is not null and @ProductCode <> @Product
--    begin
--      if not exists(select top 1 1 from viewProduct where ProductId = @ProductId and ProductCode = @ProductCode)
--      begin
--        exec @Error = p_Product_Update
--         @ProductId                  = @ProductId,
--         @ProductCode                = @ProductCode,
--         @Product                    = @Product
--        
--        if @Error != 0
--        begin
--          select @ErrorMsg = 'Error executing p_Product_Update'
--          goto error
--        end
--      end
--    end
--  end
  
  if @UOM is not null
  begin
    if @UOMCode is null
      set @UOMCode = substring(@UOM,1,10)
    
    select @UOMId = UOMId
      from UOM
     where UOMCode = @UOMCode
    
    if @UOMId is null
    begin
      select @UOMId = UOMId
        from UOM
       where UOM = @UOM
    end
    
    if @UOMId is null
    begin
      exec @Error = p_UOM_Insert
       @UOMId   = @UOMId output,
       @UOM     = @UOM,
       @UOMCode = @UOMCode
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_UOM_Insert'
        goto error
      end
    end
  end
  else
    select @UOMId = min(UOMId) from UOM
  
  set @SKUId = null
  
  -- Only allow one SKU per product
  select @SKUId   = sku.SKUId,
         @SKUCode = isnull(@SKUCode, sku.SKUCode),
         @SKU     = isnull(@SKU, sku.SKU)
    from SKU
   where sku.SKUCode = @SKUCode
   
  if @SKUId is null
  begin
  if exists(select top 1 1 from SKU where SKUCode = @SKUCode and SKUId != @SKUID)
  begin
    if @PalletQuantity is null
      set @PalletQuantity = 999
    
    exec @Error = p_SKU_Insert
     @SKUId    = @newSKUId output,
     @UOMId    = @UOMId,
     @SKU      = @SKU,
     @SKUCode  = @SKUCode,
     @Quantity = @PalletQuantity
    
    update StorageUnit
       set SKUId = @newSKUId
     where ProductId = @ProductId
       and SKUId = @SKUId
    
    set @SKUId = @newSKUId
  end
  else
  begin
    if @SKUId is not null
    begin
        exec @Error = p_SKU_Update
         @SKUId    = @SKUId,
         @UOMId    = @UOMId,
         @SKU      = @SKU,
         @SKUCode  = @SKUCode,
         @Quantity = @PalletQuantity
        
        if @error != 0
        begin
          select @ErrorMsg = 'Error executing p_SKU_Update'
          goto error
        end
    end
    else
    begin
      if @SKUCode is not null
      begin
        select @SKUId = SKUId
          from SKU
         where SKUCode = @SKUCode
        
        if @SKUId is null
        begin
          if @SKU is null
            select @SKU = @SKUCode
          
          if @PalletQuantity is null
            set @PalletQuantity = 999
          
          exec @Error = p_SKU_Insert
           @SKUId    = @SKUId output,
           @UOMId    = @UOMId,
           @SKU      = @SKU,
           @SKUCode  = @SKUCode,
           @Quantity = @PalletQuantity
          
          if @error != 0
          begin
            select @ErrorMsg = 'Error executing p_SKU_Insert'
            goto error
          end
        end
      end
      else
      begin
        select @SKUId = SKUId
          from StorageUnit
         where ProductId = @ProductId
        
        if @SKUId is null
        begin
          if @SKUCode is null
            select @SKUCode = ltrim(substring(@Product, datalength(@Product) - 2, 3))
          
          select @SKUId = SKUId
            from SKU
           where SKUCode = @SKUCode
          
          if @SKUId is null
          begin
            set @SKUCode = @SKUCode + 'T'
            select @SKUId = SKUId
              from SKU
             where SKUCode = @SKUCode
          end

          if @SKUId is null
          begin
            set @SKUCode = replace(@SKUCode, 'LT', ' LT')
            select @SKUId = SKUId
              from SKU
             where SKUCode = @SKUCode
          end

          if @SKUId is null
          begin
            select @SKUId = SKUId
              from SKU
             where SKUCode = '0'
          end
          
          if @SKUId is null
          begin
            if @SKU is null
              select @SKU = @ProductCode + ': inserted by interface'
            
            if @PalletQuantity is null
              set @PalletQuantity = 99999
            
            exec @Error = p_SKU_Insert
             @SKUId    = @SKUId output,
             @UOMId    = @UOMId,
             @SKU      = @SKU,
             @SKUCode  = '0',
             @Quantity = @PalletQuantity
            
            if @@error != 0
            begin
              select @ErrorMsg = 'Error executing p_SKU_Insert'
              goto error
            end
          end
        end
      end
    end
  end
  end
  set @StorageUnitId = null
  
  if @StorageUnitId is null
  begin
    select @StorageUnitId = StorageUnitId
      from StorageUnit
     where ProductId = @ProductId
       and SKUId     = @SKUId
    
    if @StorageUnitId is null
    begin
      exec @Error = p_StorageUnit_Insert
       @StorageUnitId = @StorageUnitId output,
       @SKUId         = @SKUId,
       @ProductId     = @ProductId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_StorageUnit_Insert'
        goto error
      end
    end
  end
  
  if @PackQuantity is null
    set @PackQuantity = 1
  
  select @Length = isnull(@Length,1),
         @Width  = isnull(@Width,1),
         @Height = isnull(@Height,1),
         @Volume = isnull(@Volume,720.000),
         @Weight = isnull(@Weight,1.200)
  
  if @Length != 1 and @Width != 1 and @Height != 1
   set @Volume = @Length * @Width * @Height
  
  select top 1 @PackTypeId = PackTypeId 
  from PackType
  Order By InboundSequence Desc
  
  select @PackId = PackId
    from Pack
   where WarehouseId   = @WarehouseId
     and StorageUnitId = @StorageUnitId
     and PackTypeId = @PackTypeId
  
  if @PackId is null
  begin
    exec @Error = p_Pack_Insert
     @PackId        = null,
     @WarehouseId   = @WarehouseId,
     @StorageUnitId = @StorageUnitId,
     @PackTypeId    = @PackTypeId,
     @Quantity      = @PackQuantity,
     @Barcode       = null,
     @Length        = @Length,
     @Width         = @Width,
     @Height        = @Height,
     @Volume        = @Volume,
     @Weight        = @Weight
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Pack_Insert'
      goto error
    end
  end
  --else
  --begin
  --  exec @Error = p_Pack_Update
  --   @PackId        = @PackId,
  --   @WarehouseId   = @WarehouseId,
  --   @StorageUnitId = @StorageUnitId,
  --   @PackTypeId    = @PackTypeId,
  --   @Quantity      = @PackQuantity,
  --   @Barcode       = null,
  --   @Length        = @Length,
  --   @Width         = @Width,
  --   @Height        = @Height,
  --   @Volume        = @Volume,
  --   @Weight        = @Weight
    
  --  if @Error != 0
  --  begin
  --    select @ErrorMsg = 'Error executing p_Pack_Insert'
  --    goto error
  --  end
  --end
  
  if @PalletQuantity is null
    set @PalletQuantity = 999999
  
  select @Length = @Length * @PalletQuantity,
         @Width  = @Width  * @PalletQuantity,
         @Height = @Height * @PalletQuantity,
         @Volume = @Volume * @PalletQuantity,
         @Weight = @Weight * @PalletQuantity
  
  select top 1 @PackTypeId = PackTypeId 
  from PackType
  Order By InboundSequence
  
  select @PackId = PackId
    from Pack
   where WarehouseId   = @WarehouseId
     and StorageUnitId = @StorageUnitId
     and PackTypeId = @PackTypeId
  
  if @PackId is null
  begin
    exec @Error = p_Pack_Insert
     @PackId        = null,
     @WarehouseId   = @WarehouseId,
     @StorageUnitId = @StorageUnitId,
     @PackTypeId    = @PackTypeId,
     @Quantity      = @PalletQuantity,
     @Barcode       = null,
     @Length        = @Length,
     @Width         = @Width,
     @Height        = @Height,
     @Volume        = @Volume,
     @Weight        = @Weight
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Pack_Insert'
      goto error
    end
  end
  --else
  --begin
  --  -- Don't update if exists
  --  if @PalletQuantity = 999999
  --    set @PalletQuantity = null
    
  --  exec @Error = p_Pack_Update
  --   @PackId        = @PackId,
  --   @WarehouseId   = @WarehouseId,
  --   @StorageUnitId = @StorageUnitId,
  --   @PackTypeId    = @PackTypeId,
  --   @Quantity      = @PalletQuantity,
  --   @Barcode       = null,
  --   @Length        = @Length,
  --   @Width         = @Width,
  --   @Height        = @Height,
  --   @Volume        = @Volume,
  --   @Weight        = @Weight
    
  --  if @Error != 0
  --  begin
  --    select @ErrorMsg = 'Error executing p_Pack_Update'
  --    goto error
  --  end
  --end
  
  --if not exists(select 1
  --                from StorageUnitArea sua (nolock)
  --                join Area              a (nolock) on sua.AreaId = a.AreaId
  --               where StorageUnitId = @StorageUnitId
  --                 and a.WarehouseId = @WarehouseId)
  --begin
  --  select @AreaId = AreaId
  --    from Area (nolock)
  --   where AreaCode = 'SP'
  --     and WarehouseId = @WarehouseId
    
  --  if @AreaId is not null
  --  begin
  --    exec @Error = p_StorageUnitArea_Insert
  --     @StorageUnitId = @StorageUnitId,
  --     @AreaId        = @AreaId,
  --     @StoreOrder    = 1,
  --     @PickOrder     = 1
      
  --    if @Error != 0
  --    begin
  --      select @ErrorMsg = 'Error executing p_StorageUnitArea_Insert'
  --      goto error
  --    end
  --  end
  --end
  
  if not exists(select 1
                  from StorageUnitLocation sul (nolock)
                  join AreaLocation         al (nolock) on sul.LocationId = al.LocationId
                  join Area                  a (nolock) on a.AreaId = a.AreaId
                 where a.WarehouseId     = @WarehouseId
                   and sul.StorageUnitId = @StorageUnitId)
  begin
    select @LocationId = l.LocationId
      from Area a
      join AreaLocation al on a.AreaId = al.AreaId
      join Location      l on al.Locationid = l.LocationId
      join LocationType lt on l.LocationTypeId = lt.LocationTypeId
     where a.AreaCode = 'SP'
       and a.WarehouseId = @WarehouseId
       and lt.LocationTypeCode = 'PK'
    
    if @AreaId is not null and @LocationId is not null
    begin
      exec @Error = p_StorageUnitLocation_Insert
       @StorageUnitId = @StorageUnitId,
       @LocationId        = @LocationId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_StorageUnitArea_Insert'
        goto error
      end
    end
  end
  
  -- Alway insert default batch
  select @StorageUnitBatchId = null
  
  select @StorageUnitBatchId = StorageUnitBatchId,
         @BatchId            = b.BatchId
    from StorageUnitBatch sub (nolock)
    join Batch              b (nolock) on sub.BatchId = b.BatchId
   where sub.StorageUnitId = @StorageUnitId
     and b.Batch           = 'Default'
  
  if @StorageUnitBatchId is null
  begin
    set @BatchId = null
    
    select @StatusId = dbo.ufn_StatusId('B','A')
    
    select @BatchId = BatchId
      from Batch
     where Batch = 'Default'
    
    if @BatchId is null
    begin
      exec @Error = p_Batch_Insert
       @BatchId        = @BatchId output,
       @StatusId       = @StatusId,
       @WarehouseId    = @WarehouseId,
       @Batch          = 'Default',
       @CreateDate     = @GetDate,
       @ExpiryDate     = null,
       @IncubationDays = null,
       @ShelfLifeDays  = null,
       @ExpectedYield  = null,
       @ActualYield    = null,
       @ECLNumber      = null
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Batch_Insert'
        goto error
      end
    end
    
    exec @Error = p_StorageUnitBatch_Insert
     @StorageUnitBatchId = @StorageUnitBatchId output,
     @BatchId            = @BatchId,
     @StorageUnitId      = @StorageUnitId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_StorageUnitBatch_Insert'
      goto error
    end
  end
  
  select @StorageUnitBatchId = null
  
  select @StorageUnitBatchId = StorageUnitBatchId,
         @BatchId            = b.BatchId
    from StorageUnitBatch sub (nolock)
    join Batch              b (nolock) on sub.BatchId = b.BatchId
   where sub.StorageUnitId = @StorageUnitId
     and b.Batch           = @Batch
     and isnull(b.BillOfEntry, '-1') = isnull(@BillOfEntry, isnull(b.BillOfEntry, '-1'))
     and isnull(b.BOELineNumber, '-1') = isnull(@BOELineNumber, isnull(b.BOELineNumber, '-1'))
  
  if @BatchId is not null
  begin
    exec @Error = p_Batch_Update
     @BatchId        = @BatchId,
     @BatchReferenceNumber = @BatchReferenceNumber
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Batch_Insert'
      goto error
    end
  end
  
  if @StorageUnitBatchId is null
  begin
    set @BatchId = null
    
    select @StatusId = dbo.ufn_StatusId('P','A')
    
    select @BatchId = BatchId
      from Batch b
     where Batch = @Batch
       and isnull(b.BillOfEntry, '-1') = isnull(@BillOfEntry, isnull(b.BillOfEntry, '-1'))
       and isnull(b.BOELineNumber, '-1') = isnull(@BOELineNumber, isnull(b.BOELineNumber, '-1'))
    
    if @BatchId is null
    begin
      exec @Error = p_Batch_Insert
       @BatchId        = @BatchId output,
       @StatusId       = @StatusId,
       @WarehouseId    = @WarehouseId,
       @Batch          = @Batch,
       @CreateDate     = @GetDate,
       @ExpiryDate     = null,
       @IncubationDays = null,
       @ShelfLifeDays  = null,
       @ExpectedYield  = null,
       @ActualYield    = null,
       @ECLNumber      = null,
       @BillOfEntry    = @BillOfEntry,
       @BOELineNumber  = @BOELineNumber,
       @BatchReferenceNumber = @BatchReferenceNumber
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Batch_Insert'
        goto error
      end
      
      --if @BatchReferenceNumber is null
      --  set @BatchReferenceNumber = 'B:' + CONVERT(nvarchar(50), @BatchId)
      
      --exec @Error = p_Batch_Update
      -- @BatchId        = @BatchId,
      -- @BatchReferenceNumber = @BatchReferenceNumber
      
      --if @Error != 0
      --begin
      --  select @ErrorMsg = 'Error executing p_Batch_Insert'
      --  goto error
      --end
    end
    
    exec @Error = p_StorageUnitBatch_Insert
     @StorageUnitBatchId = @StorageUnitBatchId output,
     @BatchId            = @BatchId,
     @StorageUnitId      = @StorageUnitId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_StorageUnitBatch_Insert'
      goto error
    end
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
 
