﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Manual_Palletisation_Create_Location
  ///   Filename       : p_Housekeeping_Manual_Palletisation_Create_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Manual_Palletisation_Create_Location
(
 @InstructionTypeCode nvarchar(10),
 @StorageUnitBatchId  int,
 @PickLocationId      int,
 @StoreLocationId	  int,
 @ReasonId            int
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @LineQuantity       float,
          @WarehouseId        int,
          @AcceptedQuantity   float,
          @PalletQuantity     float,
          @PalletCount        int,
          @RemainingQuantity  float,
          @StatusId           int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Housekeeping_Manual_Palletisation_Create_Location'
  
  if @ReasonId = -1
    set @ReasonId = null
  
  select @WarehouseId        = a.WarehouseId,
         @AcceptedQuantity   = subl.ActualQuantity - subl.ReservedQuantity
    from StorageUnitBatchLocation subl (nolock)
    join AreaLocation               al (nolock) on subl.LocationId = al.LocationId
    join Area                        a (nolock) on al.AreaId       = a.AreaId
   where subl.StorageUnitBatchId = @StorageUnitBatchId
     and subl.LocationId         = @PickLocationId
     and (subl.ActualQuantity - subl.ReservedQuantity) > 0
  
  if @AcceptedQuantity is null
    return -1
  
   exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @InstructionTypeCode = @InstructionTypeCode,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = @PickLocationId,
         @StoreLocationId     = @StoreLocationId,
         @Quantity            = @AcceptedQuantity,
         @ReasonId            = @ReasonId
        
  
    if @error <> 0
      goto error
  
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
