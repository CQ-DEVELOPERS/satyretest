﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Insert_Product
  ///   Filename       : p_FamousBrands_Insert_Product.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Dec 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Insert_Product
(
 @ProductCode        varchar(30),
 @Product            varchar(255),
 @SKUCode            varchar(10) = null,
 @SKU                varchar(255) = null,
 @Batch              varchar(50) = null,
 @UOM                varchar(50) = null,
 @WarehouseId        int,
 @StorageUnitId      int = null output,
 @BatchId            int = null output,
 @StorageUnitBatchId int = null output,
 @PalletQuantity     int = null,
 @PackQuantity       int = null,
 @Weight             numeric(13,3) = null,
 @Length             int = null,
 @Width              int = null,
 @Height             int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @ProductId         int,
          @StatusId          int,
          @SKUId             int,
          @AreaId            int,
          @LocationId        int,
          @Volume            int,
          @UOMId             int,
          @UOMCode           varchar(10),
          @PackId            int,
          @newSKUId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if @Batch is null
    set @Batch = 'Default'
  
  set @Product = left(@Product,50)
  set @SKU     = left(@SKU,50)
  set @UOM     = left(@UOM, 50)
  
  select @ProductId = ProductId
    from Product
   where ProductCode = @ProductCode
  
  if @ProductId is null
  begin
    select @StatusId = dbo.ufn_StatusId('P','A')
    
    if @Product is null
      set @Product = @ProductCode
    
    exec @Error = p_Product_Insert
     @ProductId                  = @ProductId output,
     @StatusId                   = @StatusId,
     @ProductCode                = @ProductCode,
     @Product                    = @Product,
     @Barcode                    = null,
     @MinimumQuantity            = null,
     @ReorderQuantity            = null,
     @MaximumQuantity            = null,
     @CuringPeriodDays           = null,
     @ShelfLifeDays              = null,
     @QualityAssuranceIndicator  = 1
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Product_Insert'
      goto error
    end
  end
  else
  begin
    if @Product is not null and @ProductCode <> @Product
    begin
      exec @Error = p_Product_Update
       @ProductId                  = @ProductId,
       @ProductCode                = @ProductCode,
       @Product                    = @Product
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Product_Insert'
        goto error
      end
    end
  end
  
  if @UOM is not null
  begin
    if @UOMCode is null
      set @UOMCode = substring(@UOM,1,10)
    
    select @UOMId = UOMId
      from UOM
     where UOMCode = @UOMCode
    
    if @UOMId is null
    begin
      select @UOMId = UOMId
        from UOM
       where UOM = @UOM
    end
    
    if @UOMId is null
    begin
      exec @Error = p_UOM_Insert
       @UOMId   = @UOMId output,
       @UOM     = @UOM,
       @UOMCode = @UOMCode
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Product_Insert'
        goto error
      end
    end
  end
  else
    select @UOMId = min(UOMId) from UOM
  
  set @SKUId = null
  
  -- Only allow one SKU per product
  select @SKUId   = sku.SKUId
    from StorageUnit su
    join SKU        sku on su.SKUId = sku.SKUId
   where su.ProductId = @ProductId
  
  select @newSKUId = SKUId
    from SKU
   where SKUCode = @SKUCode
  
  if @newSKUId is null
  begin
    if @PalletQuantity is null
      set @PalletQuantity = 999
    
    exec @Error = p_SKU_Insert
     @SKUId    = @newSKUId output,
     @UOMId    = @UOMId,
     @SKU      = @SKU,
     @SKUCode  = @SKUCode,
     @Quantity = @PalletQuantity
    
    update StorageUnit
       set SKUId     = @newSKUId
     where ProductId = @ProductId
       and SKUId     = @SKUId
    
    set @SKUId = @newSKUId
  end
  else if isnull(@SKUId,0) != @newSKUID
  begin
    update StorageUnit
       set SKUId     = @newSKUId
     where ProductId = @ProductId
       and SKUId     = @SKUId
    
    set @SKUId = @newSKUId
  end
  
  if not exists(select 1 from SKU where SKUId = @SKUId and SKUCode = @SKUCode)
  begin
    if @PalletQuantity is null
      set @PalletQuantity = 999
    
    exec @Error = p_SKU_Insert
     @SKUId    = @newSKUId output,
     @UOMId    = @UOMId,
     @SKU      = @SKU,
     @SKUCode  = @SKUCode,
     @Quantity = @PalletQuantity
    
    update StorageUnit
       set SKUId = @newSKUId
     where ProductId = @ProductId
       and SKUId = @SKUId
    
    set @SKUId = @newSKUId
  end
  
  set @StorageUnitId = null
  
  if @StorageUnitId is null
  begin
    select @StorageUnitId = StorageUnitId
      from StorageUnit
     where ProductId = @ProductId
       and SKUId     = @SKUId
    
    if @StorageUnitId is null
    begin
      exec @Error = p_StorageUnit_Insert
       @StorageUnitId = @StorageUnitId output,
       @SKUId         = @SKUId,
       @ProductId     = @ProductId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_StorageUnit_Insert'
        goto error
      end
    end
  end
  
  if @PackQuantity is null
    set @PackQuantity = 1
  
  select @Length = isnull(@Length,1),
         @Width  = isnull(@Width,1),
         @Height = isnull(@Height,1),
         @Volume = isnull(@Volume,720.000),
         @Weight = isnull(@Weight,1.200)
  
  if @Length != 1 and @Width != 1 and @Height != 1
   set @Volume = @Length * @Width * @Height
  
  select @PackId = PackId
    from Pack
   where WarehouseId   = @WarehouseId
     and StorageUnitId = @StorageUnitId
     and PackTypeId = 1
  
  if @PackId is null
  begin
    exec @Error = p_Pack_Insert
     @PackId        = null,
     @WarehouseId   = @WarehouseId,
     @StorageUnitId = @StorageUnitId,
     @PackTypeId    = 2,
     @Quantity      = @PackQuantity,
     @Barcode       = null,
     @Length        = @Length,
     @Width         = @Width,
     @Height        = @Height,
     @Volume        = @Volume,
     @Weight        = @Weight
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Pack_Insert'
      goto error
    end
  end
  else
  begin
    exec @Error = p_Pack_Update
     @PackId        = null,
     @WarehouseId   = @WarehouseId,
     @StorageUnitId = @StorageUnitId,
     @PackTypeId    = 2,
     @Quantity      = @PackQuantity,
     @Barcode       = null,
     @Length        = @Length,
     @Width         = @Width,
     @Height        = @Height,
     @Volume        = @Volume,
     @Weight        = @Weight
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Pack_Insert'
      goto error
    end
  end
  
  if @PalletQuantity is null
    set @PalletQuantity = 999999
  
  select @Length = @Length * @PalletQuantity,
         @Width  = @Width  * @PalletQuantity,
         @Height = @Height * @PalletQuantity,
         @Volume = @Volume * @PalletQuantity,
         @Weight = @Weight * @PalletQuantity
  
  select @PackId = PackId
    from Pack
   where WarehouseId   = @WarehouseId
     and StorageUnitId = @StorageUnitId
     and PackTypeId = 1
  
  if @PackId is null
  begin
    exec @Error = p_Pack_Insert
     @PackId        = null,
     @WarehouseId   = @WarehouseId,
     @StorageUnitId = @StorageUnitId,
     @PackTypeId    = 1,
     @Quantity      = @PalletQuantity,
     @Barcode       = null,
     @Length        = @Length,
     @Width         = @Width,
     @Height        = @Height,
     @Volume        = @Volume,
     @Weight        = @Weight
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Pack_Insert'
      goto error
    end
  end
  else
  begin
    -- Don't update if exists
    if @PalletQuantity = 999999
      set @PalletQuantity = null
    
    exec @Error = p_Pack_Update
     @PackId        = @PackId,
     @WarehouseId   = @WarehouseId,
     @StorageUnitId = @StorageUnitId,
     @PackTypeId    = 1,
     @Quantity      = @PalletQuantity,
     @Barcode       = null,
     @Length        = @Length,
     @Width         = @Width,
     @Height        = @Height,
     @Volume        = @Volume,
     @Weight        = @Weight
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Pack_Update'
      goto error
    end
  end
  
  if not exists(select 1
                  from StorageUnitArea sua (nolock)
                  join Area              a (nolock) on sua.AreaId = a.AreaId
                 where StorageUnitId = @StorageUnitId
                   and a.WarehouseId = @WarehouseId)
  begin
    select @AreaId = AreaId
      from Area (nolock)
     where AreaCode = 'SP'
       and WarehouseId = @WarehouseId
    
    if @AreaId is not null
    begin
      exec @Error = p_StorageUnitArea_Insert
       @StorageUnitId = @StorageUnitId,
       @AreaId        = @AreaId,
       @StoreOrder    = 1,
       @PickOrder     = 1
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_StorageUnitArea_Insert'
        goto error
      end
    end
  end
  
  if not exists(select 1
                  from StorageUnitLocation sul (nolock)
                  join AreaLocation         al (nolock) on sul.LocationId = al.LocationId
                  join Area                  a (nolock) on a.AreaId = a.AreaId
                 where a.WarehouseId     = @WarehouseId
                   and sul.StorageUnitId = @StorageUnitId)
  begin
    select @LocationId = l.LocationId
      from Area a
      join AreaLocation al on a.AreaId = al.AreaId
      join Location      l on al.Locationid = l.LocationId
      join LocationType lt on l.LocationTypeId = lt.LocationTypeId
     where a.AreaCode = 'SP'
       and a.WarehouseId = @WarehouseId
       and lt.LocationTypeCode = 'PK'
    
    if @AreaId is not null
    begin
      exec @Error = p_StorageUnitLocation_Insert
       @StorageUnitId = @StorageUnitId,
       @LocationId        = @LocationId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_StorageUnitArea_Insert'
        goto error
      end
    end
  end
  
  -- Alway insert default batch
  select @StorageUnitBatchId = null
  
  select @StorageUnitBatchId = StorageUnitBatchId,
         @BatchId            = b.BatchId
    from StorageUnitBatch sub (nolock)
    join Batch              b (nolock) on sub.BatchId = b.BatchId
   where sub.StorageUnitId = @StorageUnitId
     and b.Batch           = 'Default'
  
  if @StorageUnitBatchId is null
  begin
    set @BatchId = null
    
    select @StatusId = dbo.ufn_StatusId('P','A')
    
    select @BatchId = BatchId
      from Batch
     where Batch = 'Default'
    
    if @BatchId is null
    begin
      exec @Error = p_Batch_Insert
       @BatchId        = @BatchId output,
       @StatusId       = @StatusId,
       @WarehouseId    = @WarehouseId,
       @Batch          = 'Default',
       @CreateDate     = @GetDate,
       @ExpiryDate     = null,
       @IncubationDays = null,
       @ShelfLifeDays  = null,
       @ExpectedYield  = null,
       @ActualYield    = null,
       @ECLNumber      = null
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Batch_Insert'
        goto error
      end
    end
    
    exec @Error = p_StorageUnitBatch_Insert
     @StorageUnitBatchId = @StorageUnitBatchId output,
     @BatchId            = @BatchId,
     @StorageUnitId      = @StorageUnitId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_StorageUnitBatch_Insert'
      goto error
    end
  end
  
  select @StorageUnitBatchId = null
  
  select @StorageUnitBatchId = StorageUnitBatchId,
         @BatchId            = b.BatchId
    from StorageUnitBatch sub (nolock)
    join Batch              b (nolock) on sub.BatchId = b.BatchId
   where sub.StorageUnitId = @StorageUnitId
     and b.Batch           = @Batch
  
  if @StorageUnitBatchId is null
  begin
    set @BatchId = null
    
    select @StatusId = dbo.ufn_StatusId('P','A')
    
    select @BatchId = BatchId
      from Batch
     where Batch = @Batch
    
    if @BatchId is null
    begin
      exec @Error = p_Batch_Insert
       @BatchId        = @BatchId output,
       @StatusId       = @StatusId,
       @WarehouseId    = @WarehouseId,
       @Batch          = @Batch,
       @CreateDate     = @GetDate,
       @ExpiryDate     = null,
       @IncubationDays = null,
       @ShelfLifeDays  = null,
       @ExpectedYield  = null,
       @ActualYield    = null,
       @ECLNumber      = null
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Batch_Insert'
        goto error
      end
    end
    
    exec @Error = p_StorageUnitBatch_Insert
     @StorageUnitBatchId = @StorageUnitBatchId output,
     @BatchId            = @BatchId,
     @StorageUnitId      = @StorageUnitId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_StorageUnitBatch_Insert'
      goto error
    end
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_FamousBrands_Insert_Product'); 
    rollback transaction
    return @Error
end
