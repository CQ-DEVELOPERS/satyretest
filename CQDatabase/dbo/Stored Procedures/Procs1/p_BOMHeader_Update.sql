﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMHeader_Update
  ///   Filename       : p_BOMHeader_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:04
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the BOMHeader table.
  /// </remarks>
  /// <param>
  ///   @BOMHeaderId int = null,
  ///   @StorageUnitId int = null,
  ///   @Description nvarchar(100) = null,
  ///   @Remarks nvarchar(100) = null,
  ///   @Instruction varchar(max) = null,
  ///   @BOMType nvarchar(100) = null,
  ///   @HostId int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMHeader_Update
(
 @BOMHeaderId int = null,
 @StorageUnitId int = null,
 @Description nvarchar(100) = null,
 @Remarks nvarchar(100) = null,
 @Instruction varchar(max) = null,
 @BOMType nvarchar(100) = null,
 @HostId int = null,
 @Quantity float = null 
)

as
begin
	 set nocount on;
  
  if @BOMHeaderId = '-1'
    set @BOMHeaderId = null;
  
	 declare @Error int
 
  update BOMHeader
     set StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         Description = isnull(@Description, Description),
         Remarks = isnull(@Remarks, Remarks),
         Instruction = isnull(@Instruction, Instruction),
         BOMType = isnull(@BOMType, BOMType),
         HostId = isnull(@HostId, HostId),
         Quantity = isnull(@Quantity, Quantity) 
   where BOMHeaderId = @BOMHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
