﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Indicator_Update
  ///   Filename       : p_Indicator_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:24
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Indicator table.
  /// </remarks>
  /// <param>
  ///   @IndicatorId int = null,
  ///   @Header nvarchar(100) = null,
  ///   @Indicator nvarchar(100) = null,
  ///   @IndicatorValue sql_variant = null,
  ///   @IndicatorDisplay sql_variant = null,
  ///   @ThresholdGreen int = null,
  ///   @ThresholdYellow int = null,
  ///   @ThresholdRed int = null,
  ///   @ModifiedDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Indicator_Update
(
 @IndicatorId int = null,
 @Header nvarchar(100) = null,
 @Indicator nvarchar(100) = null,
 @IndicatorValue sql_variant = null,
 @IndicatorDisplay sql_variant = null,
 @ThresholdGreen int = null,
 @ThresholdYellow int = null,
 @ThresholdRed int = null,
 @ModifiedDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @IndicatorId = '-1'
    set @IndicatorId = null;
  
  if @Indicator = '-1'
    set @Indicator = null;
  
	 declare @Error int
 
  update Indicator
     set Header = isnull(@Header, Header),
         Indicator = isnull(@Indicator, Indicator),
         IndicatorValue = isnull(@IndicatorValue, IndicatorValue),
         IndicatorDisplay = isnull(@IndicatorDisplay, IndicatorDisplay),
         ThresholdGreen = isnull(@ThresholdGreen, ThresholdGreen),
         ThresholdYellow = isnull(@ThresholdYellow, ThresholdYellow),
         ThresholdRed = isnull(@ThresholdRed, ThresholdRed),
         ModifiedDate = isnull(@ModifiedDate, ModifiedDate) 
   where IndicatorId = @IndicatorId
  
  select @Error = @@Error
  
  
  return @Error
  
end
