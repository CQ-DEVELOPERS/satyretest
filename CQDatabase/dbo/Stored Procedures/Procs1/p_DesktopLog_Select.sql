﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DesktopLog_Select
  ///   Filename       : p_DesktopLog_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:35
  /// </summary>
  /// <remarks>
  ///   Selects rows from the DesktopLog table.
  /// </remarks>
  /// <param>
  ///   @DesktopLogId int = null 
  /// </param>
  /// <returns>
  ///   DesktopLog.DesktopLogId,
  ///   DesktopLog.ProcName,
  ///   DesktopLog.WarehouseId,
  ///   DesktopLog.OutboundShipmentId,
  ///   DesktopLog.IssueId,
  ///   DesktopLog.OperatorId,
  ///   DesktopLog.ErrorMsg,
  ///   DesktopLog.StartDate,
  ///   DesktopLog.EndDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DesktopLog_Select
(
 @DesktopLogId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         DesktopLog.DesktopLogId
        ,DesktopLog.ProcName
        ,DesktopLog.WarehouseId
        ,DesktopLog.OutboundShipmentId
        ,DesktopLog.IssueId
        ,DesktopLog.OperatorId
        ,DesktopLog.ErrorMsg
        ,DesktopLog.StartDate
        ,DesktopLog.EndDate
    from DesktopLog
   where isnull(DesktopLog.DesktopLogId,'0')  = isnull(@DesktopLogId, isnull(DesktopLog.DesktopLogId,'0'))
  
end
