﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMHeader_Parameter
  ///   Filename       : p_BOMHeader_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:05
  /// </summary>
  /// <remarks>
  ///   Selects rows from the BOMHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   BOMHeader.BOMHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMHeader_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as BOMHeaderId
        ,null as 'BOMHeader'
  union
  select
         BOMHeader.BOMHeaderId
        ,BOMHeader.BOMHeaderId as 'BOMHeader'
    from BOMHeader
  
end
