﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Get_PickLocation
  ///   Filename       : p_Container_Get_PickLocation.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Aug 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Get_PickLocation
(
 @warehouseId       int,
 @operatorId        int,
 @containerHeaderId int,
 @containerDetailId int
)

as
begin
  set nocount on;
  
  select l.Location
    from ContainerHeader ch (nolock)
    join Location         l on ch.PickLocationId = l.LocationId
   where ch.ContainerHeaderId = @containerHeaderId
end
