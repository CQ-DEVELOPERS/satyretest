﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Dock_Schedule
  ///   Filename       : p_Dashboard_Outbound_Dock_Schedule.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Dock_Schedule
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)

as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	 
	 --select 'TRUCK' + convert(varchar(10), isnull(datepart(wk, dh.DeliveryDate), 8)) as 'Truck',
	 --       tm.TransportMode as 'Bay',
	 --       count(distinct(DeliveryAdviceHeaderId)) as 'Value'
	 --  from TransportMode        tm (nolock)
	 --  left
	 --  join DeliveryAdviceHeader dh (nolock) on dh.TransportModeId = tm.TransportModeId
	 --group by datepart(wk, dh.DeliveryDate), tm.TransportMode
	 
	 select l.Location as 'BAY',
	        max(r.Route) as 'Truck',
	        1 as 'Value'
    from OutboundDocument      od (nolock)
    join ExternalCompany       ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join Status                si (nolock) on i.StatusId                = si.StatusId
    join Priority               p (nolock) on i.PriorityId              = p.PriorityId
    join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
    join OutboundShipment       os (nolocK) on osi.OutboundShipmentId = os.OutboundShipmentId
    left
    join Route                   r (nolock) on i.RouteId = r.RouteId
    left
    join Location                l (nolock) on os.LocationId = l.LocationId
	  where os.ShipmentDate > '2013-05-22'
	 group by os.OutboundShipmentId,
	          l.Location,
	          os.ShipmentDate
	 --select 'BAY1' as 'Bay',
	 --       'TRUCK7' as 'Truck',
	 --       2 as 'Value'
	 --union
	 --select 'BAY2' as 'Bay',
	 --       'TRUCK7' as 'Truck',
	 --       3 as 'Value'
	 --union
	 --select 'BAY3' as 'Bay',
	 --       'TRUCK7' as 'Truck',
	 --       1 as 'Value'
	 --union
	 --select 'BAY4' as 'Bay',
	 --       'TRUCK7' as 'Truck',
	 --       2 as 'Value'
	 --union
	 --select 'BAY1' as 'Bay',
	 --       'TRUCK8' as 'Truck',
	 --       0 as 'Value'
	 --union
	 --select 'BAY2' as 'Bay',
	 --       'TRUCK8' as 'Truck',
	 --       0 as 'Value'
	 --union
	 --select 'BAY3' as 'Bay',
	 --       'TRUCK8' as 'Truck',
	 --       4 as 'Value'
	 --union
	 --select 'BAY4' as 'Bay',
	 --       'TRUCK8' as 'Truck',
	 --       2 as 'Value'
  end
end
