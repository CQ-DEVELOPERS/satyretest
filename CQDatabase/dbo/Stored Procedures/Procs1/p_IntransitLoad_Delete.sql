﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoad_Delete
  ///   Filename       : p_IntransitLoad_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:32
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the IntransitLoad table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoad_Delete
(
 @IntransitLoadId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete IntransitLoad
     where IntransitLoadId = @IntransitLoadId
  
  select @Error = @@Error
  
  
  return @Error
  
end
