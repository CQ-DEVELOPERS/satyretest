﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_Team_Result
  ///   Filename       : p_InboundShipment_Team_Result.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_Team_Result
(
 @InboundShipmentId int,
 @instructionTypeId int = null,
 @operatorGroupId int = null,
 @requiredStart datetime = null output,
 @requiredEnd datetime = null output
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   InboundShipmentId int,
   InstructionTypeId  int,
   OperatorGroupId    int,
   InstructionType    nvarchar(30),
   RequiredQuantity   numeric(13,6),
   PlannedStart       datetime
  )
  
  declare @TableTeams as table
  (
   OperatorGroupId    int,
   OperatorGroup      nvarchar(30),
   InstructionTypeId  int,
   HourlyPickRate     numeric(13,6),
   Available          datetime,
   RequiredStart      datetime,
   PlannedStart       datetime,
   Planned            bit
  )
  
  insert @TableResult
        (InboundShipmentId,
         InstructionTypeId,
         OperatorGroupId,
         RequiredQuantity,
         PlannedStart)
  select oi.InboundShipmentId,
         oi.InstructionTypeId,
         oi.OperatorGroupId,
         oi.Quantity,
         ds.PlannedStart
    from InboundShipmentInstructionType  oi (nolock)
    left
    join DockSchedule                    ds (nolock) on oi.InboundShipmentId = ds.InboundShipmentId
    left
    join TeamSchedule                    ts (nolock) on oi.InboundShipmentId = ts.InboundShipmentId
                                                    and oi.InstructionTypeId = ts.InstructionTypeId
                                                    and oi.OperatorGroupId = ts.OperatorGroupId
   where oi.InboundShipmentId = @InboundShipmentId
     and oi.InstructionTypeId = isnull(@instructionTypeId, oi.InstructionTypeId)
  
  insert @TableTeams
        (OperatorGroupId,
         InstructionTypeId,
         HourlyPickRate)
  select OperatorGroupId,
         InstructionTypeId,
         HourlyPickRate
    from OperatorGroupInstructionType
   where HourlyPickRate is not null
     and OperatorGroupId between 1 and 10
     and OperatorGroupId = isnull(@operatorGroupId, OperatorGroupId)
  
  update tr
     set InstructionType = it.InstructionType
    from @TableResult    tr
    join InstructionType it (nolock) on tr.InstructionTypeId = it.InstructionTypeId
  
  update tt
     set OperatorGroup = og.OperatorGroup
    from @TableTeams tt
    join OperatorGroup og (nolock) on tt.OperatorGroupId = og.OperatorGroupId
  
  update tt
     set RequiredStart = dateadd(mi, -(RequiredQuantity / (HourlyPickRate/60)), tr.PlannedStart),
         PlannedStart = tr.PlannedStart
    from @TableResult tr
    join @TableTeams  tt on tr.InstructionTypeId = tt.InstructionTypeId
  
  delete tt
    from @TableResult tr
    join @TableTeams  tt on tr.InstructionTypeId = tt.InstructionTypeId
                        and tr.OperatorGroupId = tt.OperatorGroupId
  
  if @InboundShipmentId is not null and @instructionTypeId is not null and @operatorGroupId is not null
    select @requiredStart = tt.RequiredStart,
           @requiredEnd   = tr.PlannedStart
      from @TableResult tr
      join @TableTeams  tt on tr.InstructionTypeId = tt.InstructionTypeId
     order by tr.InboundShipmentId,
              tr.InstructionType,
              tt.OperatorGroup
  
  delete tt
    from @TableTeams tt
   where exists(select 1 from TeamSchedule ts where tt.OperatorGroupId = ts.OperatorGroupId and tt.RequiredStart between ts.PlannedStart and ts.PlannedEnd)
      or exists(select 1 from TeamSchedule ts where tt.OperatorGroupId = ts.OperatorGroupId and tt.PlannedStart between ts.PlannedStart and ts.PlannedEnd)
  
  select tr.InboundShipmentId,
         tr.InstructionTypeId,
         tt.OperatorGroupId,
         tr.InstructionType,
         tr.RequiredQuantity,
         tt.OperatorGroup,
         tt.HourlyPickRate,
         convert(nvarchar(5), tt.PlannedStart, 108) as 'PlannedStart',
         tr.RequiredQuantity,
         convert(nvarchar(5), tt.RequiredStart, 108) as 'RequiredStart',
         tt.Available,
         tt.Planned
    from @TableResult tr
    join @TableTeams  tt on tr.InstructionTypeId = tt.InstructionTypeId
   order by tr.InboundShipmentId,
            tr.InstructionType,
            tt.OperatorGroup
end
