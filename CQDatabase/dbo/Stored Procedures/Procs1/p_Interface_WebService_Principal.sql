﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_Principal
  ///   Filename       : p_Interface_WebService_Principal.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Jan 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Interface_WebService_Principal
(
 @operatorId int,
 @principalCode nvarchar(30) output
)
as
begin
	 select @PrincipalCode = p.PrincipalCode
    from Operator  o (nolock)
    join Principal p (nolock) on o.PrincipalId = p.PrincipalId
   where o.OperatorId = @OperatorId
end
