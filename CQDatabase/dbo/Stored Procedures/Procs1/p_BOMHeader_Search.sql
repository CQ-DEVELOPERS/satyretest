﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMHeader_Search
  ///   Filename       : p_BOMHeader_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:04
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the BOMHeader table.
  /// </remarks>
  /// <param>
  ///   @BOMHeaderId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   BOMHeader.BOMHeaderId,
  ///   BOMHeader.StorageUnitId,
  ///   BOMHeader.Description,
  ///   BOMHeader.Remarks,
  ///   BOMHeader.Instruction,
  ///   BOMHeader.BOMType,
  ///   BOMHeader.HostId,
  ///   BOMHeader.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMHeader_Search
(
 @BOMHeaderId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @BOMHeaderId = '-1'
    set @BOMHeaderId = null;
  
 
  select
         BOMHeader.BOMHeaderId
        ,BOMHeader.StorageUnitId
        ,BOMHeader.Description
        ,BOMHeader.Remarks
        ,BOMHeader.Instruction
        ,BOMHeader.BOMType
        ,BOMHeader.HostId
        ,BOMHeader.Quantity
    from BOMHeader
   where isnull(BOMHeader.BOMHeaderId,'0')  = isnull(@BOMHeaderId, isnull(BOMHeader.BOMHeaderId,'0'))
  
end
