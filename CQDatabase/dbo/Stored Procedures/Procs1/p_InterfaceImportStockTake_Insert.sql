﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportStockTake_Insert
  ///   Filename       : p_InterfaceImportStockTake_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:18
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportStockTake table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportStockTakeId int = null,
  ///   @Location nvarchar(1020) = null,
  ///   @ProductCode nvarchar(1020) = null,
  ///   @Description nvarchar(1020) = null,
  ///   @Batch nvarchar(1020) = null,
  ///   @CQQty float = null,
  ///   @Count1 nvarchar(1020) = null,
  ///   @Count2 nvarchar(1020) = null,
  ///   @Count3 nvarchar(1020) = null,
  ///   @NewItemCode nvarchar(1020) = null,
  ///   @NewDescription nvarchar(1020) = null,
  ///   @CurrentCount float = null,
  ///   @SKUCode nvarchar(1020) = null,
  ///   @Area nvarchar(1020) = null,
  ///   @RecordStatus char(1) = null,
  ///   @ProcessedDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportStockTake.InterfaceImportStockTakeId,
  ///   InterfaceImportStockTake.Location,
  ///   InterfaceImportStockTake.ProductCode,
  ///   InterfaceImportStockTake.Description,
  ///   InterfaceImportStockTake.Batch,
  ///   InterfaceImportStockTake.CQQty,
  ///   InterfaceImportStockTake.Count1,
  ///   InterfaceImportStockTake.Count2,
  ///   InterfaceImportStockTake.Count3,
  ///   InterfaceImportStockTake.NewItemCode,
  ///   InterfaceImportStockTake.NewDescription,
  ///   InterfaceImportStockTake.CurrentCount,
  ///   InterfaceImportStockTake.SKUCode,
  ///   InterfaceImportStockTake.Area,
  ///   InterfaceImportStockTake.RecordStatus,
  ///   InterfaceImportStockTake.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportStockTake_Insert
(
 @InterfaceImportStockTakeId int = null,
 @Location nvarchar(1020) = null,
 @ProductCode nvarchar(1020) = null,
 @Description nvarchar(1020) = null,
 @Batch nvarchar(1020) = null,
 @CQQty float = null,
 @Count1 nvarchar(1020) = null,
 @Count2 nvarchar(1020) = null,
 @Count3 nvarchar(1020) = null,
 @NewItemCode nvarchar(1020) = null,
 @NewDescription nvarchar(1020) = null,
 @CurrentCount float = null,
 @SKUCode nvarchar(1020) = null,
 @Area nvarchar(1020) = null,
 @RecordStatus char(1) = null,
 @ProcessedDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportStockTake
        (InterfaceImportStockTakeId,
         Location,
         ProductCode,
         Description,
         Batch,
         CQQty,
         Count1,
         Count2,
         Count3,
         NewItemCode,
         NewDescription,
         CurrentCount,
         SKUCode,
         Area,
         RecordStatus,
         ProcessedDate)
  select @InterfaceImportStockTakeId,
         @Location,
         @ProductCode,
         @Description,
         @Batch,
         @CQQty,
         @Count1,
         @Count2,
         @Count3,
         @NewItemCode,
         @NewDescription,
         @CurrentCount,
         @SKUCode,
         @Area,
         @RecordStatus,
         @ProcessedDate 
  
  select @Error = @@Error
  
  
  return @Error
  
end
