﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Search_Supplier
  ///   Filename       : p_ExternalCompany_Search_Supplier.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Jun 2007 12:31:43
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyId int = null output,
  ///   @ExternalCompanyTypeId int = null,
  ///   @ExternalCompany nvarchar(50) = null,
  ///   @ExternalCompanyCode nvarchar(10) = null 
  /// </param>
  /// <returns>
  ///   ec.ExternalCompanyId,
  ///   ec.ExternalCompanyTypeId,
  ///   ect.ExternalCompanyType,
  ///   ec.ExternalCompany,
  ///   ec.ExternalCompanyCode,
  ///   ec.Rating,
  ///   ec.RedeliveryIndicator,
  ///   ec.QualityAssuranceIndicator 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Search_Supplier
(
 @ExternalCompanyId int = null output,
 @ExternalCompanyTypeId int = null,
 @ExternalCompany nvarchar(255) = null,
 @ExternalCompanyCode nvarchar(30) = null
)

as
begin
  set nocount on;

 select ec.ExternalCompanyId,
        ec.ExternalCompany,
        ec.ExternalCompanyCode
   from ExternalCompany     ec
   join ExternalCompanyType ect on ect.ExternalCompanyTypeId = ec.ExternalCompanyTypeId
  where isnull(ec.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(ec.ExternalCompanyId,'0'))
    and isnull(ec.ExternalCompanyTypeId,'0')  = isnull(@ExternalCompanyTypeId, isnull(ec.ExternalCompanyTypeId,'0'))
    and isnull(ec.ExternalCompany,'%')  like '%' + isnull(@ExternalCompany, isnull(ec.ExternalCompany,'%')) + '%'
    and isnull(ec.ExternalCompanyCode,'%')  like '%' + isnull(@ExternalCompanyCode, isnull(ec.ExternalCompanyCode,'%')) + '%'
    and ect.ExternalCompanyTypeCode = 'SUPP'
end
