﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMessage_Insert
  ///   Filename       : p_InterfaceMessage_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:31
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceMessage table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMessageId int = null,
  ///   @InterfaceMessageCode nvarchar(100) = null,
  ///   @InterfaceMessage nvarchar(max) = null,
  ///   @InterfaceId int = null,
  ///   @InterfaceTable nvarchar(256) = null,
  ///   @Status nvarchar(40) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @CreateDate datetime = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceMessage.InterfaceMessageId,
  ///   InterfaceMessage.InterfaceMessageCode,
  ///   InterfaceMessage.InterfaceMessage,
  ///   InterfaceMessage.InterfaceId,
  ///   InterfaceMessage.InterfaceTable,
  ///   InterfaceMessage.Status,
  ///   InterfaceMessage.OrderNumber,
  ///   InterfaceMessage.CreateDate,
  ///   InterfaceMessage.ProcessedDate,
  ///   InterfaceMessage.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMessage_Insert
(
 @InterfaceMessageId int = null,
 @InterfaceMessageCode nvarchar(100) = null,
 @InterfaceMessage nvarchar(max) = null,
 @InterfaceId int = null,
 @InterfaceTable nvarchar(256) = null,
 @Status nvarchar(40) = null,
 @OrderNumber nvarchar(60) = null,
 @CreateDate datetime = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceMessageCode = '-1'
    set @InterfaceMessageCode = null;
  
  if @InterfaceMessage = '-1'
    set @InterfaceMessage = null;
  
	 declare @Error int
 
  insert InterfaceMessage
        (InterfaceMessageId,
         InterfaceMessageCode,
         InterfaceMessage,
         InterfaceId,
         InterfaceTable,
         Status,
         OrderNumber,
         CreateDate,
         ProcessedDate,
         InsertDate)
  select @InterfaceMessageId,
         @InterfaceMessageCode,
         @InterfaceMessage,
         @InterfaceId,
         @InterfaceTable,
         @Status,
         @OrderNumber,
         @CreateDate,
         @ProcessedDate,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
