﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoad_Insert
  ///   Filename       : p_IntransitLoad_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012 12:08:06
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the IntransitLoad table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadId int = null output,
  ///   @WarehouseId int = null,
  ///   @StatusId int = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @SealNumber nvarchar(100) = null,
  ///   @VehicleRegistration nvarchar(100) = null,
  ///   @Route nvarchar(100) = null,
  ///   @DriverId int = null,
  ///   @Remarks nvarchar(max) = null,
  ///   @CreatedById int = null,
  ///   @ReceivedById int = null,
  ///   @CreateDate datetime = null,
  ///   @LoadClosed datetime = null,
  ///   @ReceivedDate datetime = null,
  ///   @Offloaded datetime = null 
  /// </param>
  /// <returns>
  ///   IntransitLoad.IntransitLoadId,
  ///   IntransitLoad.WarehouseId,
  ///   IntransitLoad.StatusId,
  ///   IntransitLoad.DeliveryNoteNumber,
  ///   IntransitLoad.DeliveryDate,
  ///   IntransitLoad.SealNumber,
  ///   IntransitLoad.VehicleRegistration,
  ///   IntransitLoad.Route,
  ///   IntransitLoad.DriverId,
  ///   IntransitLoad.Remarks,
  ///   IntransitLoad.CreatedById,
  ///   IntransitLoad.ReceivedById,
  ///   IntransitLoad.CreateDate,
  ///   IntransitLoad.LoadClosed,
  ///   IntransitLoad.ReceivedDate,
  ///   IntransitLoad.Offloaded 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoad_Insert
(
 @IntransitLoadId int = null output,
 @WarehouseId int = null,
 @StatusId int = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @SealNumber nvarchar(100) = null,
 @VehicleRegistration nvarchar(100) = null,
 @Route nvarchar(100) = null,
 @DriverId int = null,
 @Remarks nvarchar(max) = null,
 @CreatedById int = null,
 @ReceivedById int = null,
 @CreateDate datetime = null,
 @LoadClosed datetime = null,
 @ReceivedDate datetime = null,
 @Offloaded datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  insert IntransitLoad
        (WarehouseId,
         StatusId,
         DeliveryNoteNumber,
         DeliveryDate,
         SealNumber,
         VehicleRegistration,
         Route,
         DriverId,
         Remarks,
         CreatedById,
         ReceivedById,
         CreateDate,
         LoadClosed,
         ReceivedDate,
         Offloaded)
  select @WarehouseId,
         @StatusId,
         @DeliveryNoteNumber,
         @DeliveryDate,
         @SealNumber,
         @VehicleRegistration,
         @Route,
         @DriverId,
         @Remarks,
         @CreatedById,
         @ReceivedById,
         @CreateDate,
         @LoadClosed,
         @ReceivedDate,
         @Offloaded 
  
  select @Error = @@Error, @IntransitLoadId = scope_identity()
  
  if @Error = 0
    exec @Error = p_IntransitLoadHistory_Insert
         @IntransitLoadId = @IntransitLoadId,
         @WarehouseId = @WarehouseId,
         @StatusId = @StatusId,
         @DeliveryNoteNumber = @DeliveryNoteNumber,
         @DeliveryDate = @DeliveryDate,
         @SealNumber = @SealNumber,
         @VehicleRegistration = @VehicleRegistration,
         @Route = @Route,
         @DriverId = @DriverId,
         @Remarks = @Remarks,
         @CreatedById = @CreatedById,
         @ReceivedById = @ReceivedById,
         @CreateDate = @CreateDate,
         @LoadClosed = @LoadClosed,
         @ReceivedDate = @ReceivedDate,
         @Offloaded = @Offloaded,
         @CommandType = 'Insert'
  
  return @Error
  
end
