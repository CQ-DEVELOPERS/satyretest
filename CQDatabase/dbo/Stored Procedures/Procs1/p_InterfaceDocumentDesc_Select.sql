﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentDesc_Select
  ///   Filename       : p_InterfaceDocumentDesc_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:35:00
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceDocumentDesc table.
  /// </remarks>
  /// <param>
  ///   @DocId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceDocumentDesc.DocId,
  ///   InterfaceDocumentDesc.Doc,
  ///   InterfaceDocumentDesc.DocDesc 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentDesc_Select
(
 @DocId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceDocumentDesc.DocId
        ,InterfaceDocumentDesc.Doc
        ,InterfaceDocumentDesc.DocDesc
    from InterfaceDocumentDesc
   where isnull(InterfaceDocumentDesc.DocId,'0')  = isnull(@DocId, isnull(InterfaceDocumentDesc.DocId,'0'))
  
end
