﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Upload_Accept
  ///   Filename       : p_Housekeeping_Stock_Take_Upload_Accept.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Upload_Accept
(
 @warehouseId    int,
 @comparisonDate datetime,
 @storageUnitId  int,
 @batchId        int,
 @warehouseCode  nvarchar(50) = null
)

as
begin
	 set nocount on;
  
  declare @Error                 int,
          @Errormsg              nvarchar(500),
          @GetDate               datetime,
          @Rowcount              int,
          @InterfaceId           int,
          @HousekeepingCompareId int,
          @RecordType            varchar(10),
          @ReasonCode            varchar(10),
          @OldWarehouseId        int,
          @isStockOnHand         bit,
          @Gain                  nvarchar(50),
          @Loss                  nvarchar(50),
          @PrincipalCode         nvarchar(30)
  
  set @OldWarehouseId = @WarehouseId
  
  if (select dbo.ufn_Configuration(266, @OldWarehouseId)) = 1 -- ST Upload - Combine warehouses
	   set @WarehouseId = null
	   
  if (select dbo.ufn_Configuration(272, @OldWarehouseId)) = 1 -- ST Upload - Send Stock on hand quantity
       set @isStockOnHand = 1
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @RecordType = convert(varchar(10), Value)
    from Configuration (nolock)
   where WarehouseId     = @OldWarehouseId
     and ConfigurationId = 264
  
  select @ReasonCode = convert(varchar(10), Value)
    from Configuration (nolock)
   where WarehouseId     = @OldWarehouseId
     and ConfigurationId = 265
  
  select @Gain = convert(nvarchar(50), Value)
    from Configuration (nolock)
   where WarehouseId     = @OldWarehouseId
     and ConfigurationId = 334
  
  select @Loss  = convert(nvarchar(50), Value)
    from Configuration (nolock)
   where WarehouseId     = @OldWarehouseId
     and ConfigurationId = 335
  
  if @Gain = ''
    set @Gain = null
  
  if @Loss = ''
    set @Loss = null
  
  if @Gain is not null and @Loss is not null
    set @RecordType = null
  
  if @RecordType is null
    set @RecordType = 'SOH'
  
  begin transaction
  
  if @storageUnitId = -1 and @batchId = -1
  begin
    update HousekeepingCompare
       set Sent     = 1,
           SentDate = @GetDate
     where ComparisonDate = @comparisonDate
       and isnull(Sent,0) = 0
    
    select @Error = @@Error, @Rowcount = @@Rowcount
    
    if @Error <> 0
      goto error
    
    if @Rowcount > 0
    begin
      if (select dbo.ufn_Configuration(234, @OldWarehouseId)) = 1 -- ST Upload - InterfaceExportHeader on|off
      begin
        insert InterfaceExportHeader  
              (RecordType,  
               RecordStatus,  
               FromWarehouseCode,  
               Additional1,  
               Additional2,
               PrincipalCode)  
        select '60',  
               'N',  
               hc.WarehouseCode,  
               p.ProductCode,  
               CASE @isStockOnHand WHEN 1 THEN hc.WMSQuantity ELSE hc.WMSQuantity - hc.HostQuantity END,
               pn.PrincipalCode
          from HousekeepingCompare hc
          join StorageUnit       su (nolock) on hc.StorageUnitId      = su.StorageUnitId
          join Product            p (nolock) on su.ProductId          = p.ProductId
          left
          join Principal         pn (nolock) on p.PrincipalId         = pn.PrincipalId
         where isnull(hc.WarehouseId, -1) = isnull(@warehouseId, -1)
           and hc.ComparisonDate = @comparisonDate
           and isnull(Sent,0)    = 1
           and hc.HostQuantity  <> hc.WMSQuantity
          
        select @Error = @@Error, @Rowcount = @@Rowcount
        
        if @Error <> 0
          goto error
      end
      else
      begin
        insert InterfaceExportStockAdjustment
              (RecordType,
               RecordStatus,
               Additional1,
               ProductCode,
               SKUCode,
               Batch,
               Quantity,
               Additional3,
               PrincipalCode)
        select case when @RecordType is not null 
                    then @RecordType
                    else case when hc.WMSQuantity - hc.HostQuantity > 0
                         then @Gain
                         else @Loss
                         end
                    end,
               'N',
               hc.WarehouseCode,
               p.ProductCode,
               sku.SKUCode,
               b.Batch,
               CASE @isStockOnHand WHEN 1 THEN hc.WMSQuantity ELSE hc.WMSQuantity - hc.HostQuantity END,
               @ReasonCode,
               pn.PrincipalCode
          from HousekeepingCompare hc
          join StorageUnit       su (nolock) on hc.StorageUnitId      = su.StorageUnitId
          join Product            p (nolock) on su.ProductId          = p.ProductId
          join SKU              sku (nolock) on su.SKUId              = sku.SKUId
          join Batch              b (nolock) on hc.BatchId            = b.BatchId
          left
          join Principal         pn (nolock) on p.PrincipalId         = pn.PrincipalId
         where isnull(hc.WarehouseId, -1) = isnull(@warehouseId, -1)
           and hc.ComparisonDate = @comparisonDate
           and isnull(Sent,0)    = 1
           and hc.HostQuantity  <> hc.WMSQuantity
        
        select @Error = @@Error, @Rowcount = @@Rowcount
        
        if @Error <> 0
          goto error
      end
    end
  end
  else
  begin
    select @HousekeepingCompareId = HousekeepingCompareId
      from HousekeepingCompare
     where isnull(WarehouseId, -1)  = isnull(@warehouseId, -1)
       and ComparisonDate           = @comparisonDate
       and StorageUnitId            = @storageUnitId
       and BatchId                  = @batchId
       and isnull(Sent,0)           = 0
       and isnull(WarehouseCode,'') = isnull(@warehouseCode,'')
    
    select @Error = @@Error, @Rowcount = @@Rowcount
    
    if @Error <> 0
      goto error
    
    if @Rowcount > 0
    begin
      if (select dbo.ufn_Configuration(234, @OldWarehouseId)) = 1 -- ST Upload - InterfaceExportHeader on|off
      begin
        insert InterfaceExportHeader
              (RecordType,
               RecordStatus,
               FromWarehouseCode,
               Additional1,
               Additional2,
               PrincipalCode)
        select '60',
               'N',
               hc.WarehouseCode,
               p.ProductCode,
               CASE @isStockOnHand WHEN 1 THEN hc.WMSQuantity ELSE hc.WMSQuantity - hc.HostQuantity END,
               pn.PrincipalCode
          from HousekeepingCompare hc
          join StorageUnit       su (nolock) on hc.StorageUnitId      = su.StorageUnitId
          join Product            p (nolock) on su.ProductId          = p.ProductId
          left
          join Principal         pn (nolock) on p.PrincipalId         = pn.PrincipalId
         where hc.HousekeepingCompareId = @HousekeepingCompareId
        
        select @Error = @@Error, @Rowcount = @@Rowcount, @InterfaceId = @@Identity
        
        if @Error <> 0
          goto error
      end
      else
      begin
        insert InterfaceExportStockAdjustment
              (RecordType,
               RecordStatus,
               Additional1,
               ProductCode,
               SKUCode,
               Batch,
               Quantity,
               Additional3,
               PrincipalCode)
        select case when @RecordType is not null 
                    then @RecordType
                    else case when hc.WMSQuantity - hc.HostQuantity > 0
                         then @Gain
                         else @Loss
                         end
                    end,
               'N',
               hc.WarehouseCode,
               p.ProductCode,
               sku.SKUCode,
               b.Batch,
               CASE @isStockOnHand WHEN 1 THEN hc.WMSQuantity ELSE hc.WMSQuantity - hc.HostQuantity END,
               @ReasonCode,
               pn.PrincipalCode
          from HousekeepingCompare hc
          join StorageUnit       su (nolock) on hc.StorageUnitId      = su.StorageUnitId
          join Product            p (nolock) on su.ProductId          = p.ProductId
          join SKU              sku (nolock) on su.SKUId              = sku.SKUId
          join Batch              b (nolock) on hc.BatchId            = b.BatchId
          left
          join Principal         pn (nolock) on p.PrincipalId         = pn.PrincipalId
         where hc.HousekeepingCompareId = @HousekeepingCompareId
         --where hc.WarehouseId    = @warehouseId
         --  and hc.ComparisonDate = @comparisonDate
         --  and hc.StorageUnitId  = @storageUnitId
         --  and hc.BatchId        = @batchId
         --  and isnull(WarehouseCode,'') = isnull(@warehouseCode,'')
         --  and hc.HostQuantity <> hc.WMSQuantity
        
        select @Error = @@Error, @Rowcount = @@Rowcount, @InterfaceId = @@Identity
        
        if @Error <> 0
          goto error
      end
      
      update HousekeepingCompare
         set Sent         = 1,
             SentDate     = @GetDate,
             InterfaceId  = @InterfaceId,
             RecordStatus = 'N'
       where HousekeepingCompareId = @HousekeepingCompareId
      
      select @Error = @@Error, @Rowcount = @@Rowcount
      
      if @Error <> 0
        goto error
    end
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_Take_Upload_Accept'); 
    rollback transaction
    return @Error
end
