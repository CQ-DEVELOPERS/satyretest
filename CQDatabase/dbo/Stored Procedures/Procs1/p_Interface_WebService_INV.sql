﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_INV
  ///   Filename       : p_Interface_WebService_INV.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Interface_WebService_INV
(
 @XMLBody nvarchar(max) output
)
--with encryption
as
begin
	
	DECLARE @doc xml
	DECLARE @idoc int
	DECLARE @InsertDate datetime
	DECLARE @ERROR AS VARCHAR(255)
	
	SELECT @doc = convert(xml,@XMLBody)
	     , @InsertDate = Getdate()
	     
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	
	BEGIN TRY
		                
	    INSERT INTO [InterfaceInvoice]
               ([RecordStatus]
               ,[OrderNumber]
               ,[InvoiceNumber])               
	    SELECT  'N'
	           ,nodes.entity.value('OrderNumber[1]',        'varchar(50)') 'OrderNumber'
               ,nodes.entity.value('InvoiceNumber[1]',        'varchar(50)') 'InvoiceNumber'               
	    FROM
	            @doc.nodes('/root/Body/Item') AS nodes(entity)
    	
	END TRY
	BEGIN CATCH
	    
	   SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)
	END CATCH
	
	SET @XMLBody = '<root><status>' 
	    + CASE WHEN LEN(@ERROR) > 0 THEN @ERROR
	           ELSE 'Order(s) successfully imported'
	      END
	    + '</status></root>'
end
