﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIPDetail_Insert
  ///   Filename       : p_InterfaceImportIPDetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:29
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportIPDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportIPHeaderId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(510) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Excluding numeric(13,2) = null,
  ///   @Including numeric(13,2) = null,
  ///   @NettValue numeric(13,2) = null,
  ///   @LineNumber int = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportIPDetail.InterfaceImportIPHeaderId,
  ///   InterfaceImportIPDetail.ForeignKey,
  ///   InterfaceImportIPDetail.ProductCode,
  ///   InterfaceImportIPDetail.Product,
  ///   InterfaceImportIPDetail.Batch,
  ///   InterfaceImportIPDetail.Quantity,
  ///   InterfaceImportIPDetail.Excluding,
  ///   InterfaceImportIPDetail.Including,
  ///   InterfaceImportIPDetail.NettValue,
  ///   InterfaceImportIPDetail.LineNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIPDetail_Insert
(
 @InterfaceImportIPHeaderId int = null,
 @ForeignKey nvarchar(60) = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(510) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Excluding numeric(13,2) = null,
 @Including numeric(13,2) = null,
 @NettValue numeric(13,2) = null,
 @LineNumber int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportIPDetail
        (InterfaceImportIPHeaderId,
         ForeignKey,
         ProductCode,
         Product,
         Batch,
         Quantity,
         Excluding,
         Including,
         NettValue,
         LineNumber)
  select @InterfaceImportIPHeaderId,
         @ForeignKey,
         @ProductCode,
         @Product,
         @Batch,
         @Quantity,
         @Excluding,
         @Including,
         @NettValue,
         @LineNumber 
  
  select @Error = @@Error
  
  
  return @Error
  
end
