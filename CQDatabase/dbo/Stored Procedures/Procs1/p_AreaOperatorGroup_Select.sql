﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperatorGroup_Select
  ///   Filename       : p_AreaOperatorGroup_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:44
  /// </summary>
  /// <remarks>
  ///   Selects rows from the AreaOperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @OperatorGroupId int = null 
  /// </param>
  /// <returns>
  ///   AreaOperatorGroup.AreaId,
  ///   AreaOperatorGroup.OperatorGroupId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperatorGroup_Select
(
 @AreaId int = null,
 @OperatorGroupId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         AreaOperatorGroup.AreaId
        ,AreaOperatorGroup.OperatorGroupId
    from AreaOperatorGroup
   where isnull(AreaOperatorGroup.AreaId,'0')  = isnull(@AreaId, isnull(AreaOperatorGroup.AreaId,'0'))
     and isnull(AreaOperatorGroup.OperatorGroupId,'0')  = isnull(@OperatorGroupId, isnull(AreaOperatorGroup.OperatorGroupId,'0'))
  
end
