﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Search_Code
  ///   Filename       : p_ExternalCompany_Search_Code.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 9 Jul 2007
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompany nvarchar(50) = null,
  ///   @ExternalCompanyCode nvarchar(10) = null 
  /// </param>
  /// <returns>
  ///   ExternalCompanyId,
  ///   ExternalCompany,
  ///   ExternalCompanyCode
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Search_Code
(
 
 @ExternalCompanyCode nvarchar(30) = null,
 @ExternalCompanyId int = null Output
)

as
begin
  set nocount on;

 select @ExternalCompanyId = ec.ExternalCompanyId	
        
   from ExternalCompany     ec  (nolock)
   where ec.ExternalCompanyCode = @ExternalCompanyCode
end
