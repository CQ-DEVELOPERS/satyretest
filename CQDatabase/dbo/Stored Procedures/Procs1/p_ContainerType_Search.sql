﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerType_Search
  ///   Filename       : p_ContainerType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:31
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ContainerType table.
  /// </remarks>
  /// <param>
  ///   @ContainerTypeId int = null output,
  ///   @ContainerType nvarchar(100) = null,
  ///   @ContainerTypeCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ContainerType.ContainerTypeId,
  ///   ContainerType.ContainerType,
  ///   ContainerType.ContainerTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerType_Search
(
 @ContainerTypeId int = null output,
 @ContainerType nvarchar(100) = null,
 @ContainerTypeCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ContainerTypeId = '-1'
    set @ContainerTypeId = null;
  
  if @ContainerType = '-1'
    set @ContainerType = null;
  
  if @ContainerTypeCode = '-1'
    set @ContainerTypeCode = null;
  
 
  select
         ContainerType.ContainerTypeId
        ,ContainerType.ContainerType
        ,ContainerType.ContainerTypeCode
    from ContainerType
   where isnull(ContainerType.ContainerTypeId,'0')  = isnull(@ContainerTypeId, isnull(ContainerType.ContainerTypeId,'0'))
     and isnull(ContainerType.ContainerType,'%')  like '%' + isnull(@ContainerType, isnull(ContainerType.ContainerType,'%')) + '%'
     and isnull(ContainerType.ContainerTypeCode,'%')  like '%' + isnull(@ContainerTypeCode, isnull(ContainerType.ContainerTypeCode,'%')) + '%'
  
end
