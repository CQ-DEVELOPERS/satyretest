﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DesktopLog_Insert
  ///   Filename       : p_DesktopLog_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:34
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the DesktopLog table.
  /// </remarks>
  /// <param>
  ///   @DesktopLogId int = null output,
  ///   @ProcName nvarchar(256) = null,
  ///   @WarehouseId int = null,
  ///   @OutboundShipmentId int = null,
  ///   @IssueId int = null,
  ///   @OperatorId int = null,
  ///   @ErrorMsg varchar(max) = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null 
  /// </param>
  /// <returns>
  ///   DesktopLog.DesktopLogId,
  ///   DesktopLog.ProcName,
  ///   DesktopLog.WarehouseId,
  ///   DesktopLog.OutboundShipmentId,
  ///   DesktopLog.IssueId,
  ///   DesktopLog.OperatorId,
  ///   DesktopLog.ErrorMsg,
  ///   DesktopLog.StartDate,
  ///   DesktopLog.EndDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DesktopLog_Insert
(
 @DesktopLogId int = null output,
 @ProcName nvarchar(256) = null,
 @WarehouseId int = null,
 @OutboundShipmentId int = null,
 @IssueId int = null,
 @OperatorId int = null,
 @ErrorMsg varchar(max) = null,
 @StartDate datetime = null,
 @EndDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @DesktopLogId = '-1'
    set @DesktopLogId = null;
  
	 declare @Error int
 
  insert DesktopLog
        (ProcName,
         WarehouseId,
         OutboundShipmentId,
         IssueId,
         OperatorId,
         ErrorMsg,
         StartDate,
         EndDate)
  select @ProcName,
         @WarehouseId,
         @OutboundShipmentId,
         @IssueId,
         @OperatorId,
         @ErrorMsg,
         @StartDate,
         @EndDate 
  
  select @Error = @@Error, @DesktopLogId = scope_identity()
  
  
  return @Error
  
end
