﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfacePackInfo_Search
  ///   Filename       : p_InterfacePackInfo_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:37
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfacePackInfo table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfacePackInfo.HostId,
  ///   InterfacePackInfo.ProductCode,
  ///   InterfacePackInfo.PackCode,
  ///   InterfacePackInfo.PackDescription,
  ///   InterfacePackInfo.UOM,
  ///   InterfacePackInfo.SaleUOM,
  ///   InterfacePackInfo.Ingredients,
  ///   InterfacePackInfo.Quantity,
  ///   InterfacePackInfo.UnitSize,
  ///   InterfacePackInfo.Weight,
  ///   InterfacePackInfo.Length,
  ///   InterfacePackInfo.Width,
  ///   InterfacePackInfo.Height 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfacePackInfo_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfacePackInfo.HostId
        ,InterfacePackInfo.ProductCode
        ,InterfacePackInfo.PackCode
        ,InterfacePackInfo.PackDescription
        ,InterfacePackInfo.UOM
        ,InterfacePackInfo.SaleUOM
        ,InterfacePackInfo.Ingredients
        ,InterfacePackInfo.Quantity
        ,InterfacePackInfo.UnitSize
        ,InterfacePackInfo.Weight
        ,InterfacePackInfo.Length
        ,InterfacePackInfo.Width
        ,InterfacePackInfo.Height
    from InterfacePackInfo
  
end
