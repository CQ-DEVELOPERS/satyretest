﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportStockAdjustment_Insert
  ///   Filename       : p_InterfaceExportStockAdjustment_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:14
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceExportStockAdjustment table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportStockAdjustmentId int = null output,
  ///   @RecordType char(3) = null,
  ///   @RecordStatus char(1) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Weight float = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null,
  ///   @Product nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportStockAdjustment.InterfaceExportStockAdjustmentId,
  ///   InterfaceExportStockAdjustment.RecordType,
  ///   InterfaceExportStockAdjustment.RecordStatus,
  ///   InterfaceExportStockAdjustment.ProductCode,
  ///   InterfaceExportStockAdjustment.SKUCode,
  ///   InterfaceExportStockAdjustment.Batch,
  ///   InterfaceExportStockAdjustment.Quantity,
  ///   InterfaceExportStockAdjustment.Weight,
  ///   InterfaceExportStockAdjustment.Additional1,
  ///   InterfaceExportStockAdjustment.Additional2,
  ///   InterfaceExportStockAdjustment.Additional3,
  ///   InterfaceExportStockAdjustment.Additional4,
  ///   InterfaceExportStockAdjustment.Additional5,
  ///   InterfaceExportStockAdjustment.ProcessedDate,
  ///   InterfaceExportStockAdjustment.InsertDate,
  ///   InterfaceExportStockAdjustment.Product 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportStockAdjustment_Insert
(
 @InterfaceExportStockAdjustmentId int = null output,
 @RecordType char(3) = null,
 @RecordStatus char(1) = null,
 @ProductCode nvarchar(60) = null,
 @SKUCode nvarchar(20) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Weight float = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null,
 @Product nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceExportStockAdjustmentId = '-1'
    set @InterfaceExportStockAdjustmentId = null;
  
  if @RecordStatus = '-1'
    set @RecordStatus = null;
  
	 declare @Error int
 
  insert InterfaceExportStockAdjustment
        (RecordType,
         RecordStatus,
         ProductCode,
         SKUCode,
         Batch,
         Quantity,
         Weight,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         ProcessedDate,
         InsertDate,
         Product)
  select @RecordType,
         @RecordStatus,
         @ProductCode,
         @SKUCode,
         @Batch,
         @Quantity,
         @Weight,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @ProcessedDate,
         isnull(@InsertDate, getdate()),
         @Product 
  
  select @Error = @@Error, @InterfaceExportStockAdjustmentId = scope_identity()
  
  
  return @Error
  
end
