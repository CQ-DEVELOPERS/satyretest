﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Search
  ///   Filename       : p_ExternalCompany_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:44:48
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyId int = null output,
  ///   @ExternalCompanyTypeId int = null,
  ///   @RouteId int = null,
  ///   @ExternalCompany nvarchar(510) = null,
  ///   @ExternalCompanyCode nvarchar(60) = null,
  ///   @ContainerTypeId int = null,
  ///   @PrincipalId int = null,
  ///   @ProcessId int = null,
  ///   @PricingCategoryId int = null,
  ///   @LabelingCategoryId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ExternalCompany.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompanyTypeId,
  ///   ExternalCompanyType.ExternalCompanyType,
  ///   ExternalCompany.RouteId,
  ///   Route.Route,
  ///   ExternalCompany.ExternalCompany,
  ///   ExternalCompany.ExternalCompanyCode,
  ///   ExternalCompany.Rating,
  ///   ExternalCompany.RedeliveryIndicator,
  ///   ExternalCompany.QualityAssuranceIndicator,
  ///   ExternalCompany.ContainerTypeId,
  ///   ContainerType.ContainerType,
  ///   ExternalCompany.Backorder,
  ///   ExternalCompany.HostId,
  ///   ExternalCompany.OrderLineSequence,
  ///   ExternalCompany.DropSequence,
  ///   ExternalCompany.AutoInvoice,
  ///   ExternalCompany.OneProductPerPallet,
  ///   ExternalCompany.PrincipalId,
  ///   Principal.Principal,
  ///   ExternalCompany.ProcessId,
  ///   Process.Process,
  ///   ExternalCompany.TrustedDelivery,
  ///   ExternalCompany.OutboundSLAHours,
  ///   ExternalCompany.PricingCategoryId,
  ///   PricingCategory.PricingCategory,
  ///   ExternalCompany.LabelingCategoryId 
  ///   LabelingCategory.LabelingCategory 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Search
(
 @ExternalCompanyId int = null output,
 @ExternalCompanyTypeId int = null,
 @RouteId int = null,
 @ExternalCompany nvarchar(510) = null,
 @ExternalCompanyCode nvarchar(60) = null,
 @ContainerTypeId int = null,
 @PrincipalId int = null,
 @ProcessId int = null,
 @PricingCategoryId int = null,
 @LabelingCategoryId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @ExternalCompanyTypeId = '-1'
    set @ExternalCompanyTypeId = null;
  
  if @RouteId = '-1'
    set @RouteId = null;
  
  if @ExternalCompany = '-1'
    set @ExternalCompany = null;
  
  if @ExternalCompanyCode = '-1'
    set @ExternalCompanyCode = null;
  
  if @ContainerTypeId = '-1'
    set @ContainerTypeId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @ProcessId = '-1'
    set @ProcessId = null;
  
  if @PricingCategoryId = '-1'
    set @PricingCategoryId = null;
  
  if @LabelingCategoryId = '-1'
    set @LabelingCategoryId = null;
  
 
  select
         ExternalCompany.ExternalCompanyId
        ,ExternalCompany.ExternalCompanyTypeId
         ,ExternalCompanyTypeExternalCompanyTypeId.ExternalCompanyType as 'ExternalCompanyType'
        ,ExternalCompany.RouteId
         ,RouteRouteId.Route as 'Route'
        ,ExternalCompany.ExternalCompany
        ,ExternalCompany.ExternalCompanyCode
        ,ExternalCompany.Rating
        ,ExternalCompany.RedeliveryIndicator
        ,ExternalCompany.QualityAssuranceIndicator
        ,ExternalCompany.ContainerTypeId
         ,ContainerTypeContainerTypeId.ContainerType as 'ContainerType'
        ,ExternalCompany.Backorder
        ,ExternalCompany.HostId
        ,ExternalCompany.OrderLineSequence
        ,ExternalCompany.DropSequence
        ,ExternalCompany.AutoInvoice
        ,ExternalCompany.OneProductPerPallet
        ,ExternalCompany.PrincipalId
         ,PrincipalPrincipalId.Principal as 'Principal'
        ,ExternalCompany.ProcessId
         ,ProcessProcessId.Process as 'Process'
        ,ExternalCompany.TrustedDelivery
        ,ExternalCompany.OutboundSLAHours
        ,ExternalCompany.PricingCategoryId
         ,PricingCategoryPricingCategoryId.PricingCategory as 'PricingCategory'
        ,ExternalCompany.LabelingCategoryId
         ,LabelingCategoryLabelingCategoryId.LabelingCategory as 'LabelingCategory'
    from ExternalCompany
    left
    join ExternalCompanyType ExternalCompanyTypeExternalCompanyTypeId on ExternalCompanyTypeExternalCompanyTypeId.ExternalCompanyTypeId = ExternalCompany.ExternalCompanyTypeId
    left
    join Route RouteRouteId on RouteRouteId.RouteId = ExternalCompany.RouteId
    left
    join ContainerType ContainerTypeContainerTypeId on ContainerTypeContainerTypeId.ContainerTypeId = ExternalCompany.ContainerTypeId
    left
    join Principal PrincipalPrincipalId on PrincipalPrincipalId.PrincipalId = ExternalCompany.PrincipalId
    left
    join Process ProcessProcessId on ProcessProcessId.ProcessId = ExternalCompany.ProcessId
    left
    join PricingCategory PricingCategoryPricingCategoryId on PricingCategoryPricingCategoryId.PricingCategoryId = ExternalCompany.PricingCategoryId
    left
    join LabelingCategory LabelingCategoryLabelingCategoryId on LabelingCategoryLabelingCategoryId.LabelingCategoryId = ExternalCompany.LabelingCategoryId
   where isnull(ExternalCompany.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(ExternalCompany.ExternalCompanyId,'0'))
     and isnull(ExternalCompany.ExternalCompanyTypeId,'0')  = isnull(@ExternalCompanyTypeId, isnull(ExternalCompany.ExternalCompanyTypeId,'0'))
     and isnull(ExternalCompany.RouteId,'0')  = isnull(@RouteId, isnull(ExternalCompany.RouteId,'0'))
     and isnull(ExternalCompany.ExternalCompany,'%')  like '%' + isnull(@ExternalCompany, isnull(ExternalCompany.ExternalCompany,'%')) + '%'
     and isnull(ExternalCompany.ExternalCompanyCode,'%')  like '%' + isnull(@ExternalCompanyCode, isnull(ExternalCompany.ExternalCompanyCode,'%')) + '%'
     and isnull(ExternalCompany.ContainerTypeId,'0')  = isnull(@ContainerTypeId, isnull(ExternalCompany.ContainerTypeId,'0'))
     and isnull(ExternalCompany.PrincipalId,'0')  = isnull(@PrincipalId, isnull(ExternalCompany.PrincipalId,'0'))
     and isnull(ExternalCompany.ProcessId,'0')  = isnull(@ProcessId, isnull(ExternalCompany.ProcessId,'0'))
     and isnull(ExternalCompany.PricingCategoryId,'0')  = isnull(@PricingCategoryId, isnull(ExternalCompany.PricingCategoryId,'0'))
     and isnull(ExternalCompany.LabelingCategoryId,'0')  = isnull(@LabelingCategoryId, isnull(ExternalCompany.LabelingCategoryId,'0'))
  order by ExternalCompany
  
end
 
