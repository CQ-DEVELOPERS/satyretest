﻿/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_TOPOSEND
  ///   Filename       : p_Interface_WebService_TOPOSEND.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Interface_WebService_TOPOSEND
(
 @doc  varchar(max) = null output
)
as
begin
  set nocount on
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @PrimaryKey           varchar(30),
          @OrderNumber          varchar(30),
          @Remarks              varchar(255),
          @Intransit            varchar(255),
          @Status               varchar(255),
          @DeliveryNoteNumber   varchar(255),
          @DateIssued           varchar(20),
          @DateReceived         varchar(20),
          @ProcessedDate        varchar(20),
          @FromWarehouseCode    varchar(10),
          @ToWarehouseCode      varchar(10),
          @LineNumber           varchar(10),
          @ProductCode          varchar(30),
          @Batch                varchar(50),
          @Quantity             varchar(10),
          @fQtyIssued         varchar(255),
          @fQtyDamaged          varchar(255),
          @fQtyVariance         varchar(255),
          -- Internal
          @fetch_status_header  int,
          @fetch_status_detail  int,
          @print_string         varchar(255),
          @InterfaceExportPOHeaderId int,
          @WarehouseId               int,
          @HostId                    int,
          @iWhseIDVariance           varchar(30),
          @iWhseIDDamaged            varchar(30),
          @Count					 int
  Set @Count = 0
  select @GetDate = dbo.ufn_Getdate()
  declare extract_header_cursor cursor for
  select top 1
         InterfaceExportPOHeaderId,
         PrimaryKey,
         OrderNumber,
         isnull(Remarks,''),
         isnull(FromWarehouseCode,''),
         isnull(ToWarehouseCode,''),
         isnull(Additional4,''),
         isnull(Additional10,''),
         isnull(Additional5,''),
         isnull(Additional6,''),
         isnull(Additional7,''),
         convert(varchar(10), isnull(DeliveryDate, @Getdate), 111),
         isnull(Additional3,'')
    from InterfaceExportPOHeader
   where RecordStatus = 'N' and
		 RecordType = 'IBT'
  order by PrimaryKey
  
  open extract_header_cursor
  
  fetch extract_header_cursor into @InterfaceExportPOHeaderId,
                                   @PrimaryKey,
                                   @OrderNumber,
                                   @Remarks,
                                   @FromWarehouseCode,
                                   @ToWarehouseCode,
                                   @Intransit,
                                   @Status,
                                   @DeliveryNoteNumber,
                                   @iWhseIDVariance,
                                   @iWhseIDDamaged,
                                   @DateIssued,
                                   @DateReceived
  
  select @fetch_status_header = @@fetch_status
  
  while (@fetch_status_header = 0)
  begin
    update InterfaceExportPOHeader
       set RecordStatus = 'Y',
           ProcessedDate = @GetDate
     where InterfaceExportPOHeaderId  = @InterfaceExportPOHeaderId
    set @Count = 1
    set @doc = @doc + '<?xml version="1.0" encoding="Windows-1252" standalone="yes" ?>'
    set @doc = @doc + '<root>'
    set @doc = @doc + '<TransferOrder>'
    set @doc = @doc + '		<IDWhseIBT>' + @PrimaryKey + '</IDWhseIBT>'
    set @doc = @doc + '		<InterfaceTable>InterfaceExportPOHeader</InterfaceTable>'
    set @doc = @doc + '		<InterfaceID>' + Convert(char(20),@InterfaceExportPOHeaderId) + '</InterfaceID>'
    set @doc = @doc + '		<cIBTNumber>' + @OrderNumber + '</cIBTNumber>'
    set @doc = @doc + '		<cIBTDescription>' + @Remarks + '</cIBTDescription>'
    set @doc = @doc + '	 <iWhseIDFrom>' + @FromWarehouseCode + '</iWhseIDFrom>'
    set @doc = @doc + '		<iWhseIDTo>' + @ToWarehouseCode + '</iWhseIDTo>'
    set @doc = @doc + '		<iWhseIDIntransit>' + @Intransit + '</iWhseIDIntransit>'
    set @doc = @doc + '	 <iWhseIDVariance>' + @iWhseIDVariance + '</iWhseIDVariance>'
    set @doc = @doc + '		<iWhseIDDamaged>' + @iWhseIDDamaged + '</iWhseIDDamaged>'
    set @doc = @doc + '		<iIBTStatus>' + @Status + '</iIBTStatus>'
    set @doc = @doc + '		<cDelNoteNumber>' + @DeliveryNoteNumber + '</cDelNoteNumber>'
    set @doc = @doc + '		<dDateIssued>' + @DateIssued + '</dDateIssued>'
    set @doc = @doc + '		<dDateReceived>' + @DateReceived + '</dDateReceived>'
    
    declare extract_detail_cursor cursor for
    select convert(varchar(10), LineNumber),
           isnull(ProductCode, ''),
           isnull(Batch,''),
           convert(varchar(10), Quantity),
           isnull(Additional1,'0'),
           isnull(Additional6,'0'),
           isnull(Additional7,'0')
      from InterfaceExportPODetail
     where InterfaceExportPOHeaderId = @InterfaceExportPOHeaderId
     order by LineNumber
      
    open extract_detail_cursor
    
    fetch extract_detail_cursor into @LineNumber,
                                     @ProductCode,
                                     @Batch,
                                     @Quantity,
                                     @fQtyIssued,
                                     @fQtyDamaged,
                                     @fQtyVariance
    
    select @fetch_status_detail = @@fetch_status
    
    while (@fetch_status_detail = 0)
    begin
      set @doc = @doc + '  <TransferOrderLine>'
      set @doc = @doc + '<iWhseIBTID>' + @PrimaryKey + '</iWhseIBTID>'
      set @doc = @doc + '    <IDWhseIBTLines>' + @LineNumber + '</IDWhseIBTLines>'
      set @doc = @doc + '    <iStockID>' + @ProductCode + '</iStockID>'
      set @doc = @doc + '    <cLotCode>' + @Batch + '</cLotCode>'
      set @doc = @doc + '    <fQtyIssued>' + @fQtyIssued + '</fQtyIssued>'
      set @doc = @doc + '    <fQtyReceived>' + @Quantity + '</fQtyReceived>'
      set @doc = @doc + '    <fQtyDamaged>' + @fQtyDamaged + '</fQtyDamaged>'
      set @doc = @doc + '    <fQtyVariance>' + @fQtyVariance + '</fQtyVariance>'
      set @doc = @doc + '  </TransferOrderLine>'
      fetch extract_detail_cursor into @LineNumber,
                                       @ProductCode,
                                       @Batch,
                                       @Quantity,
                                       @fQtyIssued,
                                       @fQtyDamaged,
                                       @fQtyVariance
      
      select @fetch_status_detail = @@fetch_status
    end
    
    close extract_detail_cursor
    deallocate extract_detail_cursor
    set @doc = @doc + '</TransferOrder>'
    fetch extract_header_cursor into @InterfaceExportPOHeaderId,
                                     @PrimaryKey,
                                     @OrderNumber,
                                     @Remarks,
                                     @FromWarehouseCode,
                                     @ToWarehouseCode,
                                     @Intransit,
                                     @Status,
                                     @DeliveryNoteNumber,
                                     @iWhseIDVariance,
                                     @iWhseIDDamaged,
                                     @DateIssued,
                                     @DateReceived
    
    select @fetch_status_header = @@fetch_status
  end
  
  close extract_header_cursor
  deallocate extract_header_cursor
  set @doc = @doc + '</root>'
  if @Count = 0
	Set @doc = ''
end
