﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Comment_Link_Delete
  ///   Filename       : p_Comment_Link_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Comment_Link_Delete
(
 @CommentId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  delete CommentLink
   where CommentId = @CommentId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  exec p_Comment_Delete
   @CommentId = @CommentId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Comment_Link_Delete'); 
    rollback transaction
    return @Error
end
