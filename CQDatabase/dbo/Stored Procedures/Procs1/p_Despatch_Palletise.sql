﻿/*
  /// <summary>
  ///   Procedure Name : p_Despatch_Palletise
  ///   Filename       : p_Despatch_Palletise.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Despatch_Palletise
(
 @IssueId    int = null,
 @IssueLineId int = null
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @WarehouseId        int,
          @TableQuantityId    float,
          @StorageUnitId      int,
          @StorageUnitBatchId int,
          @StoreLocationId    int,
          @PalletQuantity     numeric(10),
          @Quantity           numeric(10),
          @RemainingQuantity  float,
          @PalletCount        int,
          @InboundSequence    smallint,
          @TablePackCount     int,
          @StatusId           int,
          @Rowcount           int,
          @OutboundShipmentId int
  
  select @WarehouseId     = WarehouseId
    from Issue (nolock)
   where IssueId = @IssueId
	 
	 select @OutboundShipmentId = OutboundShipmentId from OutboundShipmentIssue where IssueId = @IssueId
  
  exec @Error = p_Outbound_Palletise
   @WarehouseId        = @WarehouseId,
   @OutboundShipmentId = @OutboundShipmentId,
   @IssueId            = @IssueId,
   @OperatorId         = null,
   @Weight             = null,
   @Volume             = null
  
  if @error <> 0
    goto error
	 
	 -----------------------------------------------------------------NOT USED YET-------------------------------------------------------
  return;
  
  declare @TableQuantity as table
  (
   TableQuantityId    int identity,
   IssueLineId        int,
   Quantity           float,
   StorageUnitId      int,
   StorageUnitBatchId int,
   StatusCode         nvarchar(10)
  );
  
  declare @TablePack as table
  (
   StorageUnitId   int,
   Quantity        float,
   InboundSequence smallint
  );
  
  if @IssueLineId = -1
    set @IssueLineId = null
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Despatch_Palletise'
  
  if @IssueId is null
    select @IssueId = IssueId
      from IssueLine
     where IssueLineId = @IssueLineId
  
  select @WarehouseId     = WarehouseId,
         @StoreLocationId = LocationId
    from Issue (nolock)
   where IssueId = @IssueId
  
  insert @TableQuantity
        (IssueLineId,
         Quantity,
         StorageUnitId,
         StorageUnitBatchId,
         StatusCode)
  select il.IssueLineId,
         il.Quantity,
         su.StorageUnitId,
         il.StorageUnitBatchId,
         s.StatusCode
    from IssueLine         il (nolock)
    join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Status             s (nolock) on il.StatusId           = s.StatusId
   where il.IssueId     = isnull(@IssueId, il.IssueId)
     and il.IssueLineId = isnull(@IssueLineId, il.IssueLineId)
     and s.StatusCode  in ('W','P','SA') -- Waiting, Palletised, Stock Allocated
  
  select @TableQuantityId = scope_identity(), @Rowcount = @@rowcount
  
  insert @TablePack
        (StorageUnitId,
         Quantity,
         InboundSequence)
  select tq.StorageUnitId,
         p.Quantity,
         pt.InboundSequence
    from @TableQuantity tq
    join Pack           p  (nolock) on tq.StorageUnitId = p.StorageUnitId
    join PackType       pt (nolock) on p.PackTypeId     = pt.PackTypeId
  
  select @StatusId = StatusId
    from Status
   where StatusCode = 'P' -- Palletised
     and Type       = 'IS'
  
  begin transaction
  
  if exists(select top 1 1 from @TableQuantity where StatusCode = 'SA')
    exec p_Palletise_Deallocate @IssueId = @IssueId
  
  if exists(select top 1 1 from @TableQuantity where StatusCode = 'P')
    exec p_Palletise_Reverse @IssueId = @IssueId
  
  if @Rowcount = 0
  begin
    set @Errormsg = 'Record not in correct Status'
    set @Error = -1
    goto error
  end
  
  if @IssueId is null and @IssueLineId is null
  begin
    set @Error = -1
    goto Error
  end
  
  exec @Error = p_Issue_Update
   @IssueId = @IssueId,
   @StatusId  = @StatusId
  
  if @error <> 0
    goto error
  
  while @TableQuantityId > 0
  begin
    select @Quantity           = Quantity,
           @StorageUnitId      = StorageUnitId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @IssueLineId        = IssueLineId
      from @TableQuantity
     where TableQuantityId = @TableQuantityId
    
    exec @Error = p_IssueLine_Update
     @IssueLineId = @IssueLineId,
     @StatusId    = @StatusId
    
    if @error <> 0
      goto error
    
    set @InboundSequence = 0
    
    select @TablePackCount = count(1) from @TablePack where StorageUnitId = @StorageUnitId
    
    while @TablePackCount > 0
    begin
      select top 1
             @PalletQuantity = Quantity,
             @InboundSequence = InboundSequence
        from @TablePack
       where StorageUnitId = @StorageUnitId
         and InboundSequence > @InboundSequence
      order by InboundSequence
      
      set @PalletCount      = floor(@Quantity / @PalletQuantity)
      set @Quantity = @Quantity - (@PalletQuantity * @PalletCount)
      
      if @PalletCount > 0
      begin
        if @PalletQuantity = 1 and @InboundSequence > 1 -- If is in Units combine otherwise put onto unit it's own pallet
        begin
          set @RemainingQuantity = @PalletQuantity * @PalletCount
          
          exec @Error = p_Palletised_Insert
           @WarehouseId         = @WarehouseId,
           @OperatorId          = null,
           @StorageUnitBatchId  = @StorageUnitBatchId,
           @PickLocationId      = null,
           @StoreLocationId     = @StoreLocationId,
           @Quantity            = @RemainingQuantity,
           @IssueLineId         = @IssueLineId
          
          if @error <> 0
            goto error
        end
        else
        begin
          while @PalletCount > 0
          begin
            set @PalletCount = @PalletCount - 1
            
            exec @Error = p_Palletised_Insert
             @WarehouseId         = @WarehouseId,
             @OperatorId          = null,
             @StorageUnitBatchId  = @StorageUnitBatchId,
             @PickLocationId      = null,
             @StoreLocationId     = @StoreLocationId,
             @Quantity            = @PalletQuantity,
             @IssueLineId       = @IssueLineId
            
            if @error <> 0
              goto error
          end
        end
      end
      
      set @TablePackCount = @TablePackCount - 1
    end
--    -- Insert Remainder
--    exec @Error = p_Palletised_Insert
--     @WarehouseId         = @WarehouseId,
--     @OperatorId          = null,
--     @InstructionTypeCode = 'S', -- Store
--     @StorageUnitBatchId  = @StorageUnitBatchId,
--     @PickLocationId      = @PickLocationId,
--     @StoreLocationId     = null,
--     @Quantity            = @Quantity,
--     @IssueLineId       = @IssueLineId
--    
--    if @error <> 0
--      goto error
    
    set @TableQuantityId = @TableQuantityId - 1
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
