﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceField_Update
  ///   Filename       : p_InterfaceField_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:46:46
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceField table.
  /// </remarks>
  /// <param>
  ///   @InterfaceFieldId int = null,
  ///   @InterfaceFieldCode nvarchar(60) = null,
  ///   @InterfaceField nvarchar(100) = null,
  ///   @InterfaceDocumentTypeId int = null,
  ///   @Datatype nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceField_Update
(
 @InterfaceFieldId int = null,
 @InterfaceFieldCode nvarchar(60) = null,
 @InterfaceField nvarchar(100) = null,
 @InterfaceDocumentTypeId int = null,
 @Datatype nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceFieldId = '-1'
    set @InterfaceFieldId = null;
  
  if @InterfaceFieldCode = '-1'
    set @InterfaceFieldCode = null;
  
  if @InterfaceField = '-1'
    set @InterfaceField = null;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
	 declare @Error int
 
  update InterfaceField
     set InterfaceFieldCode = isnull(@InterfaceFieldCode, InterfaceFieldCode),
         InterfaceField = isnull(@InterfaceField, InterfaceField),
         InterfaceDocumentTypeId = isnull(@InterfaceDocumentTypeId, InterfaceDocumentTypeId),
         Datatype = isnull(@Datatype, Datatype) 
   where InterfaceFieldId = @InterfaceFieldId
  
  select @Error = @@Error
  
  
  return @Error
  
end
