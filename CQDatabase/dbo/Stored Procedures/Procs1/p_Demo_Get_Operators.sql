﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Demo_Get_Operators
  ///   Filename       : p_Demo_Get_Operators.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Jun 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Demo_Get_Operators

as
begin
	 set nocount on;
  
  select OperatorId,
         Operator,
         case when datediff(dd, GETDATE(), LastInstruction) = 0
              then 'Green'
              else 'Red'
              end
         as 'AvailabilityIndicator'
    from Operator
  order by LastInstruction desc
end
