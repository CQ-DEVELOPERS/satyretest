﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Credit_Advice_Update
  ///   Filename       : p_Credit_Advice_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Credit_Advice_Update
(
 @ReceiptLineId  int,
 @RejectQuantity float = null,
 @ReasonId       int,
 @Exception      nvarchar(255)
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @ExceptionId       int,
          @ExceptionCode     nvarchar(10),
          @AcceptedQuantity  float
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @ReasonId = -1
    set @ReasonId = null
  
  begin transaction
  
  if @RejectQuantity is not null
  begin
    select @AcceptedQuantity = isnull(ReceivedQuantity,0) - isnull(@RejectQuantity,0)
      from ReceiptLine rl
      join Status       s (nolock) on rl.StatusId = s.StatusId
     where ReceiptLineId = @ReceiptLineId
       and Type = 'R'
       and StatusCode in ('W','S','R')
    
    if isnull(@AcceptedQuantity,-1) > -1
    begin
      exec @Error = p_ReceiptLine_Update
       @ReceiptLineId    = @ReceiptLineId,
       @RejectQuantity   = @RejectQuantity,
       @AcceptedQuantity = @AcceptedQuantity
      
      if @Error <> 0
        goto error
    end
    else
    begin
      set @Error = -1
      RAISERROR (900000,-1,-1, 'Cannot reject unles in the correct status'); 
      rollback transaction
      return @Error
    end
    
    set @ExceptionCode = 'REJECT'
  end
  else
    set @ExceptionCode = 'CAVUPD'
  
  select @ExceptionId = ExceptionId
    from Exception
   where ReceiptLineId = @ReceiptLineId
     and ExceptionCode = @ExceptionCode
  
  if @ExceptionId is null
  begin
    exec @Error = p_Exception_Insert
     @ExceptionId   = @ExceptionId output,
     @ReceiptLineId = @ReceiptLineId,
     @ReasonId      = @ReasonId,
     @Exception     = @Exception,
     @ExceptionCode = @ExceptionCode,
     @CreateDate    = @GetDate
    
    if @Error <> 0
      goto error
  end
  else
  begin
    exec @Error = p_Exception_Update
     @ExceptionId   = @ExceptionId,
     @ReceiptLineId = @ReceiptLineId,
     @ReasonId      = @ReasonId,
     @Exception     = @Exception,
     @ExceptionCode = @ExceptionCode,
     @CreateDate    = @GetDate
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Credit_Advice_Update'); 
    rollback transaction
    return @Error
end
