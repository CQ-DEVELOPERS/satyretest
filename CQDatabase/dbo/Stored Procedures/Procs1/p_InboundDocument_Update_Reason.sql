﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Update_Reason
  ///   Filename       : p_InboundDocument_Update_Reason.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Jul 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Update_Reason
(
 @InboundDocumentId int,
 @ReasonId          int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update InboundLine
     set ReasonId = @ReasonId
   where InboundDocumentId = @InboundDocumentId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_InboundDocument_Update_Reason'); 
    rollback transaction
    return @Error
end
