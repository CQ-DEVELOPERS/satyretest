﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Extract_KPI
  ///   Filename       : p_Extract_KPI.sql
  ///   Create By      : Karen
  ///   Date Created   : Jul 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Extract_KPI



as
begin
	 set nocount on;
	 
		 
Declare @Error				int,
        @Errormsg			varchar(500),
        @GetDate			datetime,
        @FromDate			datetime,
		@ToDate				datetime,
        @CheckedFrom		datetime,
        @CheckedTo			datetime
  
  --select @GetDate = dbo.ufn_Getdate()
  --print @getdate

  set @FromDate = DATEADD(d,DATEDIFF(d,0,getdate()),0)
  set @FromDate = DATEADD(HH, -12 ,@FromDate)
  set @ToDate = DATEADD(d,DATEDIFF(d,0,getdate()),0)
  set @ToDate = DATEADD(HH, 12 ,@ToDate)
  
  --** USE this to select a date range
  --set @FromDate = '2012-07-24 00:00:00'
  --set @ToDate = '2012-07-24 23:59:00'
  

  print @FromDate
  print @ToDate
  
  declare @TableDetail as Table
  (
    StorageUnitId		int NULL,
	PackType			nvarchar(30) NULL,
	EndDate				datetime NULL,
	Quantity			int NULL
  );
  

begin transaction  
 
 
  	insert	ProductKPI
			(StorageUnitId,
			PackType,	
			Quantity,
			EndDate,
			WarehouseId)
	select	vs.StorageUnitId,
			PackType,
			Quantity,
			@FromDate,
			WarehouseId
	from viewProduct vs
    join Pack      pk on vs.StorageUnitId = pk.StorageUnitId
    join PackType  pt on pk.PackTypeId = pt.PackTypeId
  
  
  	update pk
     set Received =	(select sum(rl.AcceptedQuantity) as Received
						from ReceiptLine            rl	(nolock)
						join Receipt r on r.ReceiptId = rl.ReceiptId
						join StorageUnitBatch sub (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
						--where r.DeliveryDate between '2012-07-19 00:01' and '2012-07-19 23:59'
						where r.DeliveryDate between @FromDate and @ToDate
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = r.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     set Samples =	(select sum(rl.SampleQuantity) as Samples
						from ReceiptLine            rl	(nolock)
						join Receipt r on r.ReceiptId = rl.ReceiptId
						join StorageUnitBatch sub (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
						where r.DeliveryDate between @FromDate and @ToDate
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = r.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     set Damages =	(select sum(rl.RejectQuantity) as Damages
						from ReceiptLine            rl	(nolock)
						join Receipt r on r.ReceiptId = rl.ReceiptId
						join StorageUnitBatch sub (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
						where r.DeliveryDate between @FromDate and @ToDate
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = r.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
	update pk
     --set PutAway =	(select count(distinct(j.JobId)) as PutAway
     set PutAway =	(select sum(i.ConfirmedQuantity) as PutAway
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join Job j						(nolock) on i.JobId = j.JobId
						join InstructionType it			(nolock) on it.InstructionTypeId = i.InstructionTypeId
						where i.EndDate between @FromDate and @ToDate
						and it.InstructionTypeCode in ('S','SM')
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk  
    where pk.EndDate between @FromDate and @ToDate
           
 
	update pk
     --set Movements =	(select count(distinct(j.JobId)) as Movements
     set Movements =	(select sum(i.ConfirmedQuantity) as Movements
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join Job j						(nolock) on i.JobId = j.JobId
						join InstructionType it			(nolock) on it.InstructionTypeId = i.InstructionTypeId
						where i.EndDate between @FromDate and @ToDate
						and it.InstructionTypeCode = 'M'
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     --set Replenishments =	(select count(distinct(j.JobId)) as Replenishments
     set Replenishments =	(select sum(i.ConfirmedQuantity) as Replenishments
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join Job j						(nolock) on i.JobId = j.JobId
						join InstructionType it			(nolock) on it.InstructionTypeId = i.InstructionTypeId
						where i.EndDate between @FromDate and @ToDate
						and it.InstructionTypeCode = 'R'
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     --set Picking =		(select count(distinct(j.JobId)) as Picking
     set Picking =		(select sum(i.ConfirmedQuantity) as Picking
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join Job j						(nolock) on i.JobId = j.JobId
						join InstructionType it			(nolock) on it.InstructionTypeId = i.InstructionTypeId
						where i.EndDate between @FromDate and @ToDate
						and it.InstructionTypeCode in ('P', 'PM', 'FM', 'PS')
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     --set Checking =		(select count(distinct(j.JobId)) as Checking
     set Checking =		(select sum(i.ConfirmedQuantity) as Checking
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join JobHistory j				(nolock) on i.JobId = j.JobId
						join Status s					(nolock) on s.StatusId = j.StatusId
						where j.InsertDate between @FromDate and @ToDate
						and s.StatusCode = 'CK'
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     --set MoveToDespatch =		(select count(distinct(j.JobId)) as MoveToDespatch
     set MoveToDespatch =		(select sum(i.ConfirmedQuantity) as MoveToDespatch
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join JobHistory j				(nolock) on i.JobId = j.JobId
						join Status s					(nolock) on s.StatusId = j.StatusId
						where j.InsertDate between @FromDate and @ToDate
						and s.status = 'Despatch'
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     --set Despatch =		(select count(distinct(j.JobId)) as Despatch
     set Despatch =		(select sum(i.ConfirmedQuantity) as Despatch
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join JobHistory j				(nolock) on i.JobId = j.JobId
						join Status s					(nolock) on s.StatusId = j.StatusId
						where j.InsertDate between @FromDate and @ToDate
						and s.status = 'Despatch Checked'
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
      
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
 
 
  commit transaction
  
  
    update pk1
    set pk1.Received =	pk2.Received / pk1.Quantity
    from ProductKPI pk1
    join ProductKPI pk2 on pk1.StorageUnitId = pk2.StorageUnitId 
						and pk1.PackType != pk2.PackType
						and pk1.EndDate = pk2.EndDate
						and pk1.WarehouseId = pk2.WarehouseId
						and pk2.Quantity = 1
						and pk1.Quantity != 1
	where pk1.EndDate between @FromDate and @ToDate
	
  delete from ProductKPI 
  where Checking is null
  and Damages is null
  and Despatch is null
  and MoveToDespatch is null
  and Movements is null
  and Picking is null
  and PutAway is null
  and Received is null
  and Replenishments is null
  and Samples is null

  
  select distinct p.Product, pk.* from ProductKPI pk
  join StorageUnit	su (nolock) on pk.StorageUnitId = su.StorageUnitId
  join Product		p  (nolock) on su.ProductId = p.ProductId
  where pk.Checking > 0
  or pk.Damages > 0
  or pk.Despatch > 0
  or pk.MoveToDespatch > 0
  or pk.Movements > 0
  or pk.Picking > 0
  or pk.PutAway > 0
  or pk.Received > 0
  or pk.Replenishments > 0
  or pk.Samples > 0
  order by p.Product
  
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Extract_KPI'); 
    rollback transaction
    return @Error
end
   		
