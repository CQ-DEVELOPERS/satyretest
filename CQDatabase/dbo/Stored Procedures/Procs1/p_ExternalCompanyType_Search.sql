﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompanyType_Search
  ///   Filename       : p_ExternalCompanyType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:39
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ExternalCompanyType table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyTypeId int = null output,
  ///   @ExternalCompanyType nvarchar(100) = null,
  ///   @ExternalCompanyTypeCode nvarchar(20) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   ExternalCompanyType.ExternalCompanyTypeId,
  ///   ExternalCompanyType.ExternalCompanyType,
  ///   ExternalCompanyType.ExternalCompanyTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompanyType_Search
(
 @ExternalCompanyTypeId int = null output,
 @ExternalCompanyType nvarchar(100) = null,
 @ExternalCompanyTypeCode nvarchar(20) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ExternalCompanyTypeId = '-1'
    set @ExternalCompanyTypeId = null;
  
  if @ExternalCompanyType = '-1'
    set @ExternalCompanyType = null;
  
  if @ExternalCompanyTypeCode = '-1'
    set @ExternalCompanyTypeCode = null;
  
 
  select
         ExternalCompanyType.ExternalCompanyTypeId
        ,ExternalCompanyType.ExternalCompanyType
        ,ExternalCompanyType.ExternalCompanyTypeCode
    from ExternalCompanyType
   where isnull(ExternalCompanyType.ExternalCompanyTypeId,'0')  = isnull(@ExternalCompanyTypeId, isnull(ExternalCompanyType.ExternalCompanyTypeId,'0'))
     and isnull(ExternalCompanyType.ExternalCompanyType,'%')  like '%' + isnull(@ExternalCompanyType, isnull(ExternalCompanyType.ExternalCompanyType,'%')) + '%'
     and isnull(ExternalCompanyType.ExternalCompanyTypeCode,'%')  like '%' + isnull(@ExternalCompanyTypeCode, isnull(ExternalCompanyType.ExternalCompanyTypeCode,'%')) + '%'
  
end
