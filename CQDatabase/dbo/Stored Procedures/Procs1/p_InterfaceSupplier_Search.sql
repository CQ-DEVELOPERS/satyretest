﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceSupplier_Search
  ///   Filename       : p_InterfaceSupplier_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:46
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceSupplier table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceSupplier.HostId,
  ///   InterfaceSupplier.SupplierCode,
  ///   InterfaceSupplier.SupplierName,
  ///   InterfaceSupplier.Address1,
  ///   InterfaceSupplier.Address2,
  ///   InterfaceSupplier.Address3,
  ///   InterfaceSupplier.Address4,
  ///   InterfaceSupplier.Modified,
  ///   InterfaceSupplier.ContactPerson,
  ///   InterfaceSupplier.Phone,
  ///   InterfaceSupplier.Fax,
  ///   InterfaceSupplier.Email,
  ///   InterfaceSupplier.ProcessedDate,
  ///   InterfaceSupplier.RecordStatus,
  ///   InterfaceSupplier.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceSupplier_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceSupplier.HostId
        ,InterfaceSupplier.SupplierCode
        ,InterfaceSupplier.SupplierName
        ,InterfaceSupplier.Address1
        ,InterfaceSupplier.Address2
        ,InterfaceSupplier.Address3
        ,InterfaceSupplier.Address4
        ,InterfaceSupplier.Modified
        ,InterfaceSupplier.ContactPerson
        ,InterfaceSupplier.Phone
        ,InterfaceSupplier.Fax
        ,InterfaceSupplier.Email
        ,InterfaceSupplier.ProcessedDate
        ,InterfaceSupplier.RecordStatus
        ,InterfaceSupplier.InsertDate
    from InterfaceSupplier
  
end
