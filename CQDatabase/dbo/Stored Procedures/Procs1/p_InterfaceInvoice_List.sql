﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceInvoice_List
  ///   Filename       : p_InterfaceInvoice_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:22
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceInvoice table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceInvoice.InterfaceInvoiceId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceInvoice_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceInvoiceId
        ,null as 'InterfaceInvoice'
  union
  select
         InterfaceInvoice.InterfaceInvoiceId
        ,InterfaceInvoice.InterfaceInvoiceId as 'InterfaceInvoice'
    from InterfaceInvoice
  
end
