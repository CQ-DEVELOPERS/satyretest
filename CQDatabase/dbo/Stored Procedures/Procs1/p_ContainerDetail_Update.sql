﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerDetail_Update
  ///   Filename       : p_ContainerDetail_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:56
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ContainerDetail table.
  /// </remarks>
  /// <param>
  ///   @ContainerDetailId int = null,
  ///   @ContainerHeaderId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerDetail_Update
(
 @ContainerDetailId int = null,
 @ContainerHeaderId int = null,
 @StorageUnitBatchId int = null,
 @Quantity float = null 
)

as
begin
	 set nocount on;
  
  if @ContainerDetailId = '-1'
    set @ContainerDetailId = null;
  
	 declare @Error int
 
  update ContainerDetail
     set ContainerHeaderId = isnull(@ContainerHeaderId, ContainerHeaderId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         Quantity = isnull(@Quantity, Quantity) 
   where ContainerDetailId = @ContainerDetailId
  
  select @Error = @@Error
  
  
  return @Error
  
end
