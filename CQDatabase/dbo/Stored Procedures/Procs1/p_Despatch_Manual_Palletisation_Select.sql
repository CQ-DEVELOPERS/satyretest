﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Despatch_Manual_Palletisation_Select
  ///   Filename       : p_Despatch_Manual_Palletisation_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 9 July 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Despatch_Manual_Palletisation_Select
(
 @IssueLineId int
)

as
begin
	 set nocount on;
  
  select il.IssueLineId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         il.Quantity,
         (select sum(Quantity)
            from Instruction i
           where il.IssueLineId = i.IssueLineId) as 'AllocatedQuantity'
    from IssueLine il
    join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit      su  (nolock) on sub.StorageUnitId     = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
   where IssueLineId = @IssueLineId
end
