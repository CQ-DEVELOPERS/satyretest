﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceFileType_Update
  ///   Filename       : p_InterfaceFileType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:47:28
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceFileType table.
  /// </remarks>
  /// <param>
  ///   @InterfaceFileTypeId int = null,
  ///   @InterfaceFileTypeCode nvarchar(60) = null,
  ///   @InterfaceFileType nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceFileType_Update
(
 @InterfaceFileTypeId int = null,
 @InterfaceFileTypeCode nvarchar(60) = null,
 @InterfaceFileType nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceFileTypeId = '-1'
    set @InterfaceFileTypeId = null;
  
  if @InterfaceFileTypeCode = '-1'
    set @InterfaceFileTypeCode = null;
  
  if @InterfaceFileType = '-1'
    set @InterfaceFileType = null;
  
	 declare @Error int
 
  update InterfaceFileType
     set InterfaceFileTypeCode = isnull(@InterfaceFileTypeCode, InterfaceFileTypeCode),
         InterfaceFileType = isnull(@InterfaceFileType, InterfaceFileType) 
   where InterfaceFileTypeId = @InterfaceFileTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
