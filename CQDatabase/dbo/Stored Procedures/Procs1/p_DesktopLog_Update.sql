﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DesktopLog_Update
  ///   Filename       : p_DesktopLog_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:34
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the DesktopLog table.
  /// </remarks>
  /// <param>
  ///   @DesktopLogId int = null,
  ///   @ProcName nvarchar(256) = null,
  ///   @WarehouseId int = null,
  ///   @OutboundShipmentId int = null,
  ///   @IssueId int = null,
  ///   @OperatorId int = null,
  ///   @ErrorMsg varchar(max) = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DesktopLog_Update
(
 @DesktopLogId int = null,
 @ProcName nvarchar(256) = null,
 @WarehouseId int = null,
 @OutboundShipmentId int = null,
 @IssueId int = null,
 @OperatorId int = null,
 @ErrorMsg varchar(max) = null,
 @StartDate datetime = null,
 @EndDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @DesktopLogId = '-1'
    set @DesktopLogId = null;
  
	 declare @Error int
 
  update DesktopLog
     set ProcName = isnull(@ProcName, ProcName),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         OutboundShipmentId = isnull(@OutboundShipmentId, OutboundShipmentId),
         IssueId = isnull(@IssueId, IssueId),
         OperatorId = isnull(@OperatorId, OperatorId),
         ErrorMsg = isnull(@ErrorMsg, ErrorMsg),
         StartDate = isnull(@StartDate, StartDate),
         EndDate = isnull(@EndDate, EndDate) 
   where DesktopLogId = @DesktopLogId
  
  select @Error = @@Error
  
  
  return @Error
  
end
