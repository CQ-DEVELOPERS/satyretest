﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOH_Select
  ///   Filename       : p_InterfaceImportSOH_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:12
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportSOH table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSOHId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportSOH.InterfaceImportSOHId,
  ///   InterfaceImportSOH.Location,
  ///   InterfaceImportSOH.ProductCode,
  ///   InterfaceImportSOH.Batch,
  ///   InterfaceImportSOH.Quantity,
  ///   InterfaceImportSOH.UnitPrice,
  ///   InterfaceImportSOH.ProcessedDate,
  ///   InterfaceImportSOH.RecordStatus,
  ///   InterfaceImportSOH.RecordType,
  ///   InterfaceImportSOH.InsertDate,
  ///   InterfaceImportSOH.SKUCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOH_Select
(
 @InterfaceImportSOHId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceImportSOH.InterfaceImportSOHId
        ,InterfaceImportSOH.Location
        ,InterfaceImportSOH.ProductCode
        ,InterfaceImportSOH.Batch
        ,InterfaceImportSOH.Quantity
        ,InterfaceImportSOH.UnitPrice
        ,InterfaceImportSOH.ProcessedDate
        ,InterfaceImportSOH.RecordStatus
        ,InterfaceImportSOH.RecordType
        ,InterfaceImportSOH.InsertDate
        ,InterfaceImportSOH.SKUCode
    from InterfaceImportSOH
   where isnull(InterfaceImportSOH.InterfaceImportSOHId,'0')  = isnull(@InterfaceImportSOHId, isnull(InterfaceImportSOH.InterfaceImportSOHId,'0'))
  
end
