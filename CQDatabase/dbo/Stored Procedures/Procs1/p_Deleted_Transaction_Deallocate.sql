﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Deleted_Transaction_Deallocate
  ///   Filename       : p_Deleted_Transaction_Deallocate.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Aug 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Deleted_Transaction_Deallocate

as
begin
  declare	@Command nvarchar(255)

  declare deallocate_cursor cursor for
   select 'p_StorageUnitBatchLocation_Deallocate ' + convert(nvarchar(10), InstructionId) + ',1,1,1'
     from viewInstruction
    where InstructionCode = 'Z'
      and (Stored = 0 or Picked = 0)
   order by	InstructionId

  open deallocate_cursor

  fetch deallocate_cursor into @Command

  while (@@fetch_status = 0)
  begin
    select @Command
    execute (@Command)
    
    fetch deallocate_cursor into @Command
  end

  close deallocate_cursor
  deallocate deallocate_cursor
end
