﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Indicator_Colour_All
  ///   Filename       : p_Indicator_Colour_All.sql
  ///   Create By      : Willis
  ///   Date Created   : 13 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
Create Procedure p_Indicator_Colour_All
AS
Begin
	
Select IndicatorId,
		Header,
		Indicator,
		IndicatorValue,
		IndicatorDisplay,
		Case When IndicatorValue < ThresholdGreen Then 'Green'
			 When IndicatorValue < ThresholdYellow Then 'Yellow'
			 When IndicatorValue > ThresholdYellow Then 'Red'
		Else 'Blue' end as Colour,
		ModifiedDate
	From Indicator (nolock)
	Order by Header,Indicator Desc
End
