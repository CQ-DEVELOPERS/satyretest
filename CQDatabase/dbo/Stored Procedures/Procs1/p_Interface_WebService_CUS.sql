﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_CUS
  ///   Filename       : p_Interface_WebService_CUS.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Interface_WebService_CUS]
(
 @XMLBody nvarchar(max) output,
 @principalCode nvarchar(30) = null
)
--with encryption
as
begin
	
	DECLARE @doc xml
	DECLARE @idoc int
	DECLARE @InsertDate datetime
	DECLARE @ERROR AS VARCHAR(255)
	
	SELECT @doc = convert(xml,@XMLBody)
	     , @InsertDate = dbo.ufn_Getdate()
	
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	
	BEGIN TRY
		                
	    INSERT INTO [dbo].[InterfaceCustomer]
               ([RecordStatus]
               ,[InsertDate]
               ,[HostId]
               ,[CustomerCode]
               ,[CustomerName]
               ,[Class]
               ,[DeliveryPoint]
               ,[Address1]
               ,[Address2]
               ,[Address3]
               ,[Address4]
               ,[Modified]
               ,[ContactPerson]
               ,[Phone]
               ,[Fax]
               ,[Email]
               ,[DeliveryGroup]
               ,[DepotCode]
               ,[VisitFrequency]
               ,[PrincipalCode])
	    SELECT  'N'
	           ,@InsertDate
	           ,nodes.entity.value('PrimaryKey[1]',         'varchar(30)')  'HostId'
	           ,nodes.entity.value('CustomerCode[1]',       'varchar(30)')  'CustomerCode'
               ,nodes.entity.value('CustomerName[1]',       'varchar(255)') 'CustomerName'
               ,nodes.entity.value('Class[1]',              'varchar(255)') 'Class'
               ,nodes.entity.value('DeliveryPoint[1]',      'varchar(255)') 'DeliveryPoint'
               ,nodes.entity.value('Address1[1]',           'varchar(255)') 'Address1'
               ,nodes.entity.value('Address2[1]',           'varchar(255)') 'Address2'
               ,nodes.entity.value('Address3[1]',           'varchar(255)') 'Address3'
               ,nodes.entity.value('Address4[1]',           'varchar(255)') 'Address4'
               ,nodes.entity.value('Modified[1]',           'varchar(10)')  'Modified'
               ,nodes.entity.value('ContactPerson[1]',      'varchar(100)') 'ContactPerson'
               ,nodes.entity.value('Phone[1]',              'varchar(255)') 'Phone'
               ,nodes.entity.value('Fax[1]',                'varchar(255)') 'Fax'
               ,nodes.entity.value('Email[1]',              'varchar(255)') 'Email'
               ,nodes.entity.value('DeliveryGroup[1]',      'varchar(255)') 'DeliveryGroup'
               ,nodes.entity.value('DepotCode[1]',          'varchar(255)') 'DepotCode'
               ,nodes.entity.value('VisitFrequency[1]',     'varchar(255)') 'VisitFrequency'
               --,nodes.entity.value('ProcessedDate[1]',      'datetime	       
               ,@principalCode
	    FROM
	            @doc.nodes('/root/Body/Item') AS nodes(entity)
    	    	
	
	END TRY
	BEGIN CATCH
	    
	   SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)
	END CATCH
	
	exec p_Pastel_Import_Customer
	
	SET @XMLBody = '<root><status>' 
	    + CASE WHEN LEN(@ERROR) > 0 THEN @ERROR
	           ELSE 'Customer(s) successfully imported'
	      END
	    + '</status></root>'
End
