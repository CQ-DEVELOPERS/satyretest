﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportProduct_Insert
  ///   Filename       : p_InterfaceImportProduct_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:02
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportProduct table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportProductId int = null output,
  ///   @RecordStatus char(1) = null,
  ///   @ProcessedDate datetime = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(510) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @SKU nvarchar(20) = null,
  ///   @PalletQuantity float = null,
  ///   @Barcode nvarchar(100) = null,
  ///   @MinimumQuantity float = null,
  ///   @ReorderQuantity float = null,
  ///   @MaximumQuantity float = null,
  ///   @CuringPeriodDays int = null,
  ///   @ShelfLifeDays int = null,
  ///   @QualityAssuranceIndicator bit = null,
  ///   @ProductType nvarchar(40) = null,
  ///   @ProductCategory nvarchar(40) = null,
  ///   @OverReceipt numeric(13,3) = null,
  ///   @RetentionSamples int = null,
  ///   @HostId nvarchar(60) = null,
  ///   @InsertDate datetime = null,
  ///   @Additional1 varchar(255) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @DangerousGoodsCode nvarchar(510) = null,
  ///   @ABCStatus nchar(2) = null,
  ///   @Samples nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportProduct.InterfaceImportProductId,
  ///   InterfaceImportProduct.RecordStatus,
  ///   InterfaceImportProduct.ProcessedDate,
  ///   InterfaceImportProduct.ProductCode,
  ///   InterfaceImportProduct.Product,
  ///   InterfaceImportProduct.SKUCode,
  ///   InterfaceImportProduct.SKU,
  ///   InterfaceImportProduct.PalletQuantity,
  ///   InterfaceImportProduct.Barcode,
  ///   InterfaceImportProduct.MinimumQuantity,
  ///   InterfaceImportProduct.ReorderQuantity,
  ///   InterfaceImportProduct.MaximumQuantity,
  ///   InterfaceImportProduct.CuringPeriodDays,
  ///   InterfaceImportProduct.ShelfLifeDays,
  ///   InterfaceImportProduct.QualityAssuranceIndicator,
  ///   InterfaceImportProduct.ProductType,
  ///   InterfaceImportProduct.ProductCategory,
  ///   InterfaceImportProduct.OverReceipt,
  ///   InterfaceImportProduct.RetentionSamples,
  ///   InterfaceImportProduct.HostId,
  ///   InterfaceImportProduct.InsertDate,
  ///   InterfaceImportProduct.Additional1,
  ///   InterfaceImportProduct.Additional2,
  ///   InterfaceImportProduct.Additional3,
  ///   InterfaceImportProduct.Additional4,
  ///   InterfaceImportProduct.Additional5,
  ///   InterfaceImportProduct.Additional6,
  ///   InterfaceImportProduct.Additional7,
  ///   InterfaceImportProduct.Additional8,
  ///   InterfaceImportProduct.Additional9,
  ///   InterfaceImportProduct.Additional10,
  ///   InterfaceImportProduct.DangerousGoodsCode,
  ///   InterfaceImportProduct.ABCStatus,
  ///   InterfaceImportProduct.Samples 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportProduct_Insert
(
 @InterfaceImportProductId int = null output,
 @RecordStatus char(1) = null,
 @ProcessedDate datetime = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(510) = null,
 @SKUCode nvarchar(20) = null,
 @SKU nvarchar(20) = null,
 @PalletQuantity float = null,
 @Barcode nvarchar(100) = null,
 @MinimumQuantity float = null,
 @ReorderQuantity float = null,
 @MaximumQuantity float = null,
 @CuringPeriodDays int = null,
 @ShelfLifeDays int = null,
 @QualityAssuranceIndicator bit = null,
 @ProductType nvarchar(40) = null,
 @ProductCategory nvarchar(40) = null,
 @OverReceipt numeric(13,3) = null,
 @RetentionSamples int = null,
 @HostId nvarchar(60) = null,
 @InsertDate datetime = null,
 @Additional1 varchar(255) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @DangerousGoodsCode nvarchar(510) = null,
 @ABCStatus nchar(2) = null,
 @Samples nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceImportProductId = '-1'
    set @InterfaceImportProductId = null;
  
	 declare @Error int
 
  insert InterfaceImportProduct
        (RecordStatus,
         ProcessedDate,
         ProductCode,
         Product,
         SKUCode,
         SKU,
         PalletQuantity,
         Barcode,
         MinimumQuantity,
         ReorderQuantity,
         MaximumQuantity,
         CuringPeriodDays,
         ShelfLifeDays,
         QualityAssuranceIndicator,
         ProductType,
         ProductCategory,
         OverReceipt,
         RetentionSamples,
         HostId,
         InsertDate,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10,
         DangerousGoodsCode,
         ABCStatus,
         Samples)
  select @RecordStatus,
         @ProcessedDate,
         @ProductCode,
         @Product,
         @SKUCode,
         @SKU,
         @PalletQuantity,
         @Barcode,
         @MinimumQuantity,
         @ReorderQuantity,
         @MaximumQuantity,
         @CuringPeriodDays,
         @ShelfLifeDays,
         @QualityAssuranceIndicator,
         @ProductType,
         @ProductCategory,
         @OverReceipt,
         @RetentionSamples,
         @HostId,
         isnull(@InsertDate, getdate()),
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9,
         @Additional10,
         @DangerousGoodsCode,
         @ABCStatus,
         @Samples 
  
  select @Error = @@Error, @InterfaceImportProductId = scope_identity()
  
  
  return @Error
  
end
