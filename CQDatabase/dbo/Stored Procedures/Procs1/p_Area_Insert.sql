﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_Insert
  ///   Filename       : p_Area_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Jul 2014 09:46:57
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Area table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null output,
  ///   @WarehouseId int = null,
  ///   @Area nvarchar(100) = null,
  ///   @AreaCode nvarchar(20) = null,
  ///   @StockOnHand bit = null,
  ///   @AreaSequence int = null,
  ///   @AreaType nvarchar(20) = null,
  ///   @WarehouseCode nvarchar(60) = null,
  ///   @Replenish bit = null,
  ///   @MinimumShelfLife float = null,
  ///   @Undefined bit = null,
  ///   @STEKPI int = null,
  ///   @BatchTracked bit = null,
  ///   @StockTake bit = null,
  ///   @NarrowAisle bit = null,
  ///   @Interleaving bit = null,
  ///   @PandD bit = null,
  ///   @SmallPick bit = null,
  ///   @AutoLink bit = null,
  ///   @WarehouseCode2 nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   Area.AreaId,
  ///   Area.WarehouseId,
  ///   Area.Area,
  ///   Area.AreaCode,
  ///   Area.StockOnHand,
  ///   Area.AreaSequence,
  ///   Area.AreaType,
  ///   Area.WarehouseCode,
  ///   Area.Replenish,
  ///   Area.MinimumShelfLife,
  ///   Area.Undefined,
  ///   Area.STEKPI,
  ///   Area.BatchTracked,
  ///   Area.StockTake,
  ///   Area.NarrowAisle,
  ///   Area.Interleaving,
  ///   Area.PandD,
  ///   Area.SmallPick,
  ///   Area.AutoLink,
  ///   Area.WarehouseCode2 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_Insert
(
 @AreaId int = null output,
 @WarehouseId int = null,
 @Area nvarchar(100) = null,
 @AreaCode nvarchar(20) = null,
 @StockOnHand bit = null,
 @AreaSequence int = null,
 @AreaType nvarchar(20) = null,
 @WarehouseCode nvarchar(60) = null,
 @Replenish bit = null,
 @MinimumShelfLife float = null,
 @Undefined bit = null,
 @STEKPI int = null,
 @BatchTracked bit = null,
 @StockTake bit = null,
 @NarrowAisle bit = null,
 @Interleaving bit = null,
 @PandD bit = null,
 @SmallPick bit = null,
 @AutoLink bit = null,
 @WarehouseCode2 nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
  if @AreaId = '-1'
    set @AreaId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @Area = '-1'
    set @Area = null;
  
  if @AreaCode = '-1'
    set @AreaCode = null;
  
	 declare @Error int
 
  insert Area
        (WarehouseId,
         Area,
         AreaCode,
         StockOnHand,
         AreaSequence,
         AreaType,
         WarehouseCode,
         Replenish,
         MinimumShelfLife,
         Undefined,
         STEKPI,
         BatchTracked,
         StockTake,
         NarrowAisle,
         Interleaving,
         PandD,
         SmallPick,
         AutoLink,
         WarehouseCode2)
  select @WarehouseId,
         @Area,
         @AreaCode,
         @StockOnHand,
         @AreaSequence,
         @AreaType,
         @WarehouseCode,
         @Replenish,
         @MinimumShelfLife,
         @Undefined,
         @STEKPI,
         @BatchTracked,
         @StockTake,
         @NarrowAisle,
         @Interleaving,
         @PandD,
         @SmallPick,
         @AutoLink,
         @WarehouseCode2 
  
  select @Error = @@Error, @AreaId = scope_identity()
  
  if @Error = 0
    exec @Error = p_AreaHistory_Insert
         @AreaId = @AreaId,
         @WarehouseId = @WarehouseId,
         @Area = @Area,
         @AreaCode = @AreaCode,
         @StockOnHand = @StockOnHand,
         @AreaSequence = @AreaSequence,
         @AreaType = @AreaType,
         @WarehouseCode = @WarehouseCode,
         @Replenish = @Replenish,
         @MinimumShelfLife = @MinimumShelfLife,
         @Undefined = @Undefined,
         @STEKPI = @STEKPI,
         @BatchTracked = @BatchTracked,
         @StockTake = @StockTake,
         @NarrowAisle = @NarrowAisle,
         @Interleaving = @Interleaving,
         @PandD = @PandD,
         @SmallPick = @SmallPick,
         @AutoLink = @AutoLink,
         @WarehouseCode2 = @WarehouseCode2,
         @CommandType = 'Insert'
  
  return @Error
  
end
