﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipmentReceipt_Select
  ///   Filename       : p_InboundShipmentReceipt_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:23
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundShipmentReceipt table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null,
  ///   @ReceiptId int = null 
  /// </param>
  /// <returns>
  ///   InboundShipmentReceipt.InboundShipmentId,
  ///   InboundShipmentReceipt.ReceiptId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipmentReceipt_Select
(
 @InboundShipmentId int = null,
 @ReceiptId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InboundShipmentReceipt.InboundShipmentId
        ,InboundShipmentReceipt.ReceiptId
    from InboundShipmentReceipt
   where isnull(InboundShipmentReceipt.InboundShipmentId,'0')  = isnull(@InboundShipmentId, isnull(InboundShipmentReceipt.InboundShipmentId,'0'))
     and isnull(InboundShipmentReceipt.ReceiptId,'0')  = isnull(@ReceiptId, isnull(InboundShipmentReceipt.ReceiptId,'0'))
  
end
