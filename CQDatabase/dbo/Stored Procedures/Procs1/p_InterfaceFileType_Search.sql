﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceFileType_Search
  ///   Filename       : p_InterfaceFileType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:47:29
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceFileType table.
  /// </remarks>
  /// <param>
  ///   @InterfaceFileTypeId int = null output,
  ///   @InterfaceFileTypeCode nvarchar(60) = null,
  ///   @InterfaceFileType nvarchar(100) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceFileType.InterfaceFileTypeId,
  ///   InterfaceFileType.InterfaceFileTypeCode,
  ///   InterfaceFileType.InterfaceFileType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceFileType_Search
(
 @InterfaceFileTypeId int = null output,
 @InterfaceFileTypeCode nvarchar(60) = null,
 @InterfaceFileType nvarchar(100) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceFileTypeId = '-1'
    set @InterfaceFileTypeId = null;
  
  if @InterfaceFileTypeCode = '-1'
    set @InterfaceFileTypeCode = null;
  
  if @InterfaceFileType = '-1'
    set @InterfaceFileType = null;
  
 
  select
         InterfaceFileType.InterfaceFileTypeId
        ,InterfaceFileType.InterfaceFileTypeCode
        ,InterfaceFileType.InterfaceFileType
    from InterfaceFileType
   where isnull(InterfaceFileType.InterfaceFileTypeId,'0')  = isnull(@InterfaceFileTypeId, isnull(InterfaceFileType.InterfaceFileTypeId,'0'))
     and isnull(InterfaceFileType.InterfaceFileTypeCode,'%')  like '%' + isnull(@InterfaceFileTypeCode, isnull(InterfaceFileType.InterfaceFileTypeCode,'%')) + '%'
     and isnull(InterfaceFileType.InterfaceFileType,'%')  like '%' + isnull(@InterfaceFileType, isnull(InterfaceFileType.InterfaceFileType,'%')) + '%'
  order by InterfaceFileTypeCode
  
end
