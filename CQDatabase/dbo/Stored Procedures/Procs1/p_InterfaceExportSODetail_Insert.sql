﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportSODetail_Insert
  ///   Filename       : p_InterfaceExportSODetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:10
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceExportSODetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportSOHeaderId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @LineNumber int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(100) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Weight float = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @IssueLineId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportSODetail.InterfaceExportSOHeaderId,
  ///   InterfaceExportSODetail.ForeignKey,
  ///   InterfaceExportSODetail.LineNumber,
  ///   InterfaceExportSODetail.ProductCode,
  ///   InterfaceExportSODetail.Product,
  ///   InterfaceExportSODetail.SKUCode,
  ///   InterfaceExportSODetail.Batch,
  ///   InterfaceExportSODetail.Quantity,
  ///   InterfaceExportSODetail.Weight,
  ///   InterfaceExportSODetail.Additional1,
  ///   InterfaceExportSODetail.Additional2,
  ///   InterfaceExportSODetail.Additional3,
  ///   InterfaceExportSODetail.Additional4,
  ///   InterfaceExportSODetail.Additional5,
  ///   InterfaceExportSODetail.IssueLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportSODetail_Insert
(
 @InterfaceExportSOHeaderId int = null,
 @ForeignKey nvarchar(60) = null,
 @LineNumber int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(100) = null,
 @SKUCode nvarchar(20) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Weight float = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @IssueLineId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceExportSODetail
        (InterfaceExportSOHeaderId,
         ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         IssueLineId)
  select @InterfaceExportSOHeaderId,
         @ForeignKey,
         @LineNumber,
         @ProductCode,
         @Product,
         @SKUCode,
         @Batch,
         @Quantity,
         @Weight,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @IssueLineId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
