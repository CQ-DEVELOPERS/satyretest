﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLocation_Insert
  ///   Filename       : p_InterfaceLocation_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:23
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceLocation table.
  /// </remarks>
  /// <param>
  ///   @HostId nvarchar(60) = null,
  ///   @Location nvarchar(60) = null,
  ///   @LocationDescription nvarchar(510) = null,
  ///   @Address1 nvarchar(510) = null,
  ///   @Address2 nvarchar(510) = null,
  ///   @Address3 nvarchar(510) = null,
  ///   @Address4 nvarchar(510) = null,
  ///   @City nvarchar(510) = null,
  ///   @State nvarchar(510) = null,
  ///   @Zip nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceLocation.HostId,
  ///   InterfaceLocation.Location,
  ///   InterfaceLocation.LocationDescription,
  ///   InterfaceLocation.Address1,
  ///   InterfaceLocation.Address2,
  ///   InterfaceLocation.Address3,
  ///   InterfaceLocation.Address4,
  ///   InterfaceLocation.City,
  ///   InterfaceLocation.State,
  ///   InterfaceLocation.Zip,
  ///   InterfaceLocation.ProcessedDate,
  ///   InterfaceLocation.RecordStatus,
  ///   InterfaceLocation.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLocation_Insert
(
 @HostId nvarchar(60) = null,
 @Location nvarchar(60) = null,
 @LocationDescription nvarchar(510) = null,
 @Address1 nvarchar(510) = null,
 @Address2 nvarchar(510) = null,
 @Address3 nvarchar(510) = null,
 @Address4 nvarchar(510) = null,
 @City nvarchar(510) = null,
 @State nvarchar(510) = null,
 @Zip nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceLocation
        (HostId,
         Location,
         LocationDescription,
         Address1,
         Address2,
         Address3,
         Address4,
         City,
         State,
         Zip,
         ProcessedDate,
         RecordStatus,
         InsertDate)
  select @HostId,
         @Location,
         @LocationDescription,
         @Address1,
         @Address2,
         @Address3,
         @Address4,
         @City,
         @State,
         @Zip,
         @ProcessedDate,
         @RecordStatus,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
