﻿
/*  
  /// <summary>  
  ///   Procedure Name : p_Interface_Outbound_Order_Delete  
  ///   Filename       : p_Interface_Outbound_Order_Delete.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 24 Oct 2008  
  /// </summary>  
  /// 
<remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  


*/  
CREATE procedure p_Interface_Outbound_Order_Delete  
(  
 @OrderNumber  varchar(30),
 @DeliveryDate datetime = null,
 @PrincipalId  int = null
)  
  
as  
begin  
  set nocount on;  
    
  declare @Error              int,  
          
@Errormsg           varchar(500),  
          @GetDate            datetime,  
          @OutboundDocumentId int,  
          @IssueId            int,  
          @IssueLineId        int,
          @BOMOrder          varchar(30)
    
  select @GetDate = dbo.ufn_Getdate()  
  set @Error = 0
  
  Select TOP 1 @BOMOrder = od.OrderNumber
  From OutboundDocument od
  Inner Join BOMInstruction bi On od.OutboundDocumentId = bi.OutboundDocumentId
  Inner Join OutboundDocument pod on bi.ParentId = pod.OutboundDocumentId
  Where pod.OrderNumber = @OrderNumber
  
  If @BOMOrder is not null
    exec p_Interface_Outbound_Order_Delete @OrderNumber = @BOMOrder
  if @DeliveryDate is null
    select @OutboundDocumentId = max(OutboundDocumentId)  
      from OutboundDocument  
     where OrderNumber = @OrderNumber
       and isnull(PrincipalId,-1) = isnull(@PrincipalId, isnull(PrincipalId,-1))
  else
    select @OutboundDocumentId = max(OutboundDocumentId)  
      from OutboundDocument  
     where OrderNumber = @OrderNumber  
       and DeliveryDate = @DeliveryDate
       and isnull(PrincipalId,-1) = isnull(@PrincipalId, isnull(PrincipalId,-1))
  
  if @@rowcount = 0  
    return;  
    
  select @IssueId = max(IssueId)  
    from Issue  
   where OutboundDocumentId = @OutboundDocumentId  
  
  begin transaction  
    
  if exists(select top 1 1  
              from Issue        i  
              join Status       s on i.StatusId = s.StatusId  
             where i.OutboundDocumentId = @OutboundDocumentId  
               and s.StatusCode not in ('W','OH','Z','P'))  
    goto error  
    
  exec @Error = p_Palletise_Reverse  
   @IssueId     = @IssueId,  
   @IssueLineId = @IssueLineId  
    
  if @OutboundDocumentId is not null  
  begin  
    delete il  
      from IssueLine il  
      join Issue      i on il.IssueId = i.IssueId  
     where il.IssueId = @IssueId  
      
    if @@Error <> 0  
      goto error  
      
    /*delete osi  
      from OutboundShipmentIssue osi  
      join Issue                  i on osi.IssueId = i.IssueId  
     where i.IssueId = @IssueId  
      
    if @@Error <> 0  
      goto error*/
      
    delete Issue  
     where IssueId = @IssueId  
      
    if @@Error <> 0  
      goto error  
      
    delete OutboundLine  
     where OutboundDocumentId = @OutboundDocumentId  
       and not exists(select top 1 1 from IssueLine where IssueLine.OutboundLineId = OutboundLine.OutboundLineId)  
      
    if @@Error <> 0  
      goto error  
      
    delete OutboundDocument  
     where OutboundDocumentId = @OutboundDocumentId  
       and not exists(select top 1 1 from Issue where Issue.OutboundDocumentId = OutboundDocument.OutboundDocumentId)  
      
    if @@Error <> 0  
      goto error  
  end  
    
  commit transaction  
  return 0  
  

  
  error:  
    set @Error = -1  
      
    if @@trancount > 0  
      commit transaction  
    else  
    begin  
      RAISERROR (900000,-1,-1, 'Error executing p_Interface_Outbound_Order_Delete' );  
      rollback transaction  
    end  
      
    return @Error  
end

 
 
