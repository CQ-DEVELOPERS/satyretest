﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentType_Insert
  ///   Filename       : p_InterfaceDocumentType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 16:26:00
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceDocumentType table.
  /// </remarks>
  /// <param>
  ///   @InterfaceDocumentTypeId int = null output,
  ///   @InterfaceDocumentTypeCode nvarchar(60) = null,
  ///   @InterfaceDocumentType nvarchar(100) = null,
  ///   @RecordType nvarchar(60) = null,
  ///   @InterfaceType nvarchar(60) = null,
  ///   @InDirectory bit = null,
  ///   @OutDirectory bit = null,
  ///   @SubDirectory nvarchar(max) = null,
  ///   @FilePrefix nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   InterfaceDocumentType.InterfaceDocumentTypeId,
  ///   InterfaceDocumentType.InterfaceDocumentTypeCode,
  ///   InterfaceDocumentType.InterfaceDocumentType,
  ///   InterfaceDocumentType.RecordType,
  ///   InterfaceDocumentType.InterfaceType,
  ///   InterfaceDocumentType.InDirectory,
  ///   InterfaceDocumentType.OutDirectory,
  ///   InterfaceDocumentType.SubDirectory,
  ///   InterfaceDocumentType.FilePrefix 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentType_Insert
(
 @InterfaceDocumentTypeId int = null output,
 @InterfaceDocumentTypeCode nvarchar(60) = null,
 @InterfaceDocumentType nvarchar(100) = null,
 @RecordType nvarchar(60) = null,
 @InterfaceType nvarchar(60) = null,
 @InDirectory bit = null,
 @OutDirectory bit = null,
 @SubDirectory nvarchar(max) = null,
 @FilePrefix nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
  if @InterfaceDocumentTypeCode = '-1'
    set @InterfaceDocumentTypeCode = null;
  
  if @InterfaceDocumentType = '-1'
    set @InterfaceDocumentType = null;
  
	 declare @Error int
 
  insert InterfaceDocumentType
        (InterfaceDocumentTypeCode,
         InterfaceDocumentType,
         RecordType,
         InterfaceType,
         InDirectory,
         OutDirectory,
         SubDirectory,
         FilePrefix)
  select @InterfaceDocumentTypeCode,
         @InterfaceDocumentType,
         @RecordType,
         @InterfaceType,
         @InDirectory,
         @OutDirectory,
         @SubDirectory,
         @FilePrefix 
  
  select @Error = @@Error, @InterfaceDocumentTypeId = scope_identity()
  
  
  return @Error
  
end
