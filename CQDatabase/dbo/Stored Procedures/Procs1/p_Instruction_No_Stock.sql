﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_No_Stock
  ///   Filename       : p_Instruction_No_Stock.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Aug 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_No_Stock
(
 @InstructionId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @NoStockId         int,
          @JobId             int,
          @StatusCode        nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @NoStockId = dbo.ufn_StatusId('I','NS')
  
  select @JobId      = i.JobId,
         @StatusCode = s.StatusCode
    from Instruction i (nolock)
    join Status      s (nolock) on i.StatusId = s.StatusId
   where i.InstructionId = @InstructionId
  
  if @StatusCode not in ('W','S')
   return
  
  begin transaction
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId = @InstructionId,
   @Pick          = 1,
   @Store         = 1,
   @Confirmed     = 1
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Instruction_Update
   @InstructionId     = @InstructionId,
   @StatusId          = @NoStockId,
   @ConfirmedQuantity = 0
   
  exec @Error = p_Instruction_Update_ili
   @InstructionId     = @InstructionId,
   @InstructionRefId  = null,
   @insConfirmed      = 0
  
  if @Error <> 0
    goto error
  
  -- Check to see if all lines are no-stock and then updates job too.
  if (select count(1) from Instruction (nolock) where JobId = @JobId and StatusId != @NoStockId) = 0
  begin
    exec @Error = p_Job_Update
     @JobId    = @JobId,
     @StatusId = @NoStockId
    
    if @Error <> 0
      goto error
  end
  else if (select count(1)
             from Instruction i (nolock)
             join Status      s (nolock) on i.StatusId = s.StatusId
            where i.JobId = @JobId
              and s.StatusCode in ('W')) = 0
  begin
    if (select s.StatusCode
          from Job    j (nolock)
          join Status s (nolock) on j.StatusId = s.StatusId
         where j.JobId = @JobId) in ('S','RL')
    begin
      exec @Error = p_Status_Rollup
       @JobId    = @JobId
      
      if @Error <> 0
        goto error
    end
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Instruction_No_Stock'); 
    rollback transaction
    return @Error
end
