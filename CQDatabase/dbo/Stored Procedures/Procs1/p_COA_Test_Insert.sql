﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Test_Insert
  ///   Filename       : p_COA_Test_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Test_Insert
 (
 @COAId      int,
 @COATestId  int,
 @TestId     int,
 @Test       nvarchar(max),
 @ResultId   int,
 @Result     nvarchar(max),
 @MethodId   int,
 @Method     nvarchar(max),
 @Pass       bit,
 @StartRange float,
 @EndRange   float,
 @Value      sql_variant
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  if @Test is not null or @Test != ''
    exec p_Test_Insert
     @TestId = @TestId output,
     @Test   = @Test
  
  if @Result is not null or @Result != ''
    exec p_Result_Insert
     @ResultId = @ResultId output,
     @Result   = @Result,
     @Pass     = 0
  
  if @Method is not null or @Method != ''
    exec p_Method_Insert
     @MethodId = @MethodId output,
     @Method   = @Method
  
  exec p_COATest_Insert
   @COATestId  = @COATestId output,
   @COAId      = @COAId,
   @TestId     = @TestId,
   @ResultId   = @ResultId,
   @MethodId   = @MethodId,
   @Pass       = @Pass,
   @StartRange = @StartRange,
   @EndRange   = @EndRange,
   @Value      = @Value
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_COA_Test_Insert'); 
    rollback transaction
    return @Error
end
