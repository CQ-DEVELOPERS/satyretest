﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Check_Product_SerialTracked
  ///   Filename       : p_Check_Product_SerialTracked.sql
  ///   Create By      : Karen	
  ///   Date Created   : August 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Check_Product_SerialTracked
(
	@InstructionId	int
)


as
begin
	 set nocount on;
  
 Select convert(int,SerialTracked) as SerialTracked
   from   Instruction i (nolock)
   join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   join StorageUnit su (nolock) on sub.StorageUnitId = su.StorageUnitId
  where  i.InstructionId = @InstructionId
 
end

 
