﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_SOH
  ///   Filename       : p_FamousBrands_Export_SOH.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Jun 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Export_SOH
(
 @FileName varchar(30) = null output
)

as
begin
  set nocount on
  
  declare @TableResult as table
  (
   Data varchar(500)
  )
  
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @fetch_status_detail  int,
          @WarehouseCode        varchar(10),
          @ProductCode          varchar(30),
          @Quantity             float
  
  select @GetDate = dbo.ufn_Getdate()
  
  declare extract_detail_cursor cursor for
  select isnull(Additional1, 'G200'),
         ProductCode,
         Quantity
    from InterfaceExportStockAdjustment
   where RecordType = 'SOH'
     and RecordStatus = 'N'
  order by Additional1, ProductCode
  
  open extract_detail_cursor
  
  fetch extract_detail_cursor into @WarehouseCode,
                                   @ProductCode,
                                   @Quantity
  
  select @fetch_status_detail = @@fetch_status
  
  if @fetch_status_detail < 0
    goto end_proc
  
  set @FileName = @WarehouseCode + replace(replace(replace(convert(varchar(23), @Getdate,121),'-',''),':',''),'.','')
  
  insert @TableResult (Data) select '<?xml version="1.0" encoding="iso-8859-1" ?>'
  insert @TableResult (Data) select '<root>'
  insert @TableResult (Data) select '<StockOnHand>'
  
  while (@fetch_status_detail = 0)
  begin
    update InterfaceExportStockAdjustment
       set RecordStatus = 'Y',
           ProcessedDate = @GetDate
     where isnull(Additional1, 'G200')    = @WarehouseCode
       and ProductCode                    = @ProductCode
       and Quantity                       = @Quantity
       and RecordStatus                   = 'N'
    
    insert @TableResult (Data) select '<StockOnHandLine>'
    insert @TableResult (Data) select '<Location>' + @WarehouseCode + '</Location>'
    insert @TableResult (Data) select '<ProductCode>' + @ProductCode + '</ProductCode>'
    insert @TableResult (Data) select '<Quantity>' + convert(varchar(10), @Quantity) + '</Quantity>'
    insert @TableResult (Data) select '</StockOnHandLine>'
    
    fetch extract_detail_cursor into @WarehouseCode,
                                     @ProductCode,
                                     @Quantity
    
    select @fetch_status_detail = @@fetch_status
  end
  
  insert @TableResult (Data) select '</StockOnHand>'
  insert @TableResult (Data) select '</root>'
  
  end_proc:
  
  close extract_detail_cursor
  deallocate extract_detail_cursor
  
  update @TableResult set Data = replace(Data, '&', '&amp;')
  update @TableResult set Data = replace(Data, '&amp;amp;', '&amp;')
    
  select Data
    from @TableResult
end


