﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_Console_PM
  ///   Filename       : p_Interface_Console_PM.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Apr 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Interface_Console_PM
(
 @WarehouseId int = null,
 @productCode nvarchar(50) = null,
 @product     nvarchar(255) = null,
 @status      nvarchar(50) = null,
 @message     nvarchar(max) = null,
 @recordType  nvarchar(50) = null,
 @fromDate    datetime = null,
 @toDate      datetime = null
)

as
begin
	 set nocount on;
  
  select im.InterfaceMessageId,
         im.InterfaceMessageCode,
         im.InterfaceMessage,
         im.Status,
         isnull(im.InterfaceMessageCode, 'No Response') as 'InterfaceMessageCode',
         im.CreateDate as 'MessageDate',
         (select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceExportStockAdjustment') as 'InterfaceTableId',
         h.InterfaceImportProductId as 'InterfaceId',
         --h.RecordType,
         h.RecordStatus,
         h.ProcessedDate,
         h.InsertDate,
         h.ProductCode,
         h.Product
  from InterfaceImportProduct h (nolock)
  join InterfaceMessage       im (nolock) on h.InterfaceImportProductId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceImportProduct'
                                         
  where h.ProcessedDate in (select MAX(ProcessedDate) from InterfaceImportProduct)
 order by im.Status
end
