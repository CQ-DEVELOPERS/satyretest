﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CompanyHistory_Insert
  ///   Filename       : p_CompanyHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:49
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the CompanyHistory table.
  /// </remarks>
  /// <param>
  ///   @CompanyId int = null,
  ///   @Company nvarchar(100) = null,
  ///   @CompanyCode nvarchar(20) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   CompanyHistory.CompanyId,
  ///   CompanyHistory.Company,
  ///   CompanyHistory.CompanyCode,
  ///   CompanyHistory.CommandType,
  ///   CompanyHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CompanyHistory_Insert
(
 @CompanyId int = null,
 @Company nvarchar(100) = null,
 @CompanyCode nvarchar(20) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert CompanyHistory
        (CompanyId,
         Company,
         CompanyCode,
         CommandType,
         InsertDate)
  select @CompanyId,
         @Company,
         @CompanyCode,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
