﻿CREATE  procedure [dbo].[p_Interface_WebService_SR]
(
@XMLBody varchar(max) output,
@principalCode nvarchar(30) = null
)
--with encryption
as
begin
  
  declare @doc                          xml
  declare @idoc                         int
  declare @InsertDate                   datetime
  declare @ERROR                        AS nvarchar(255)
  declare @InterfaceImportHeaderId      int
  
  SELECT @doc = convert(xml,@XMLBody)
         , @InsertDate = Getdate()
         
         EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
         
  BEGIN TRY
    
    INSERT INTO [InterfaceImportHeader]
           ([RecordStatus]
           ,[InsertDate]
           ,HostStatus
           ,[PrimaryKey]
           ,[OrderNumber]
           ,[InvoiceNumber]
           ,[RecordType]
           ,[CompanyCode]
           ,[Company]
           ,[Address]
           ,[FromWarehouseCode]
           ,[ToWarehouseCode]
           ,[Route]
           ,[DeliveryNoteNumber]
           ,[ContainerNumber]
           ,[SealNumber]
           ,[DeliveryDate]
           ,[Remarks]
           ,[NumberOfLines]
           --,[VatPercentage]
           --,[VatSummary]
           --,[Total]
           ,[Additional1]
           ,[Additional2]
           ,[Additional3]
           ,[Additional4]
           ,[Additional5]
           ,[Additional6]
           ,[Additional7]
           ,[Additional8]
           ,[Additional9]
           ,[Additional10])
    SELECT 'W'
           ,@InsertDate
           ,nodes.entity.value('ItemStatus[1]',         'nvarchar(30)')  'HostStatus'
           ,nodes.entity.value('PrimaryKey[1]',         'nvarchar(40)')  'PrimaryKey'
           ,nodes.entity.value('OrderNumber[1]',        'nvarchar(30)')  'OrderNumber'
           ,nodes.entity.value('InvoiceNumber[1]',      'nvarchar(30)')  'InvoiceNumber'
           ,nodes.entity.value('ItemType[1]',           'nvarchar(30)')  'ItemType'
           --,nodes.entity.value('RecordStatus[1]',       'char(1)') ''
           ,nodes.entity.value('CompanyCode[1]',        'nvarchar(30)')  'CompanyCode'
           ,nodes.entity.value('Company[1]',            'nvarchar(255)') 'Company'
           ,nodes.entity.value('Address[1]',            'nvarchar(255)') 'Address'
           ,nodes.entity.value('FromWarehouseCode[1]',  'nvarchar(10)')  'FromWarehouseCode'
           ,nodes.entity.value('ToWarehouseCode[1]',    'nvarchar(10)')  'ToWarehouseCode'
           ,nodes.entity.value('Route[1]',              'nvarchar(50)')  'Route'
           ,nodes.entity.value('DeliveryNoteNumber[1]', 'nvarchar(30)')  'DeliveryNoteNumber'
           ,nodes.entity.value('ContainerNumber[1]',    'nvarchar(30)')  'ContainerNumber'
           ,nodes.entity.value('SealNumber[1]',         'nvarchar(30)')  'SealNumber'
           ,nodes.entity.value('DeliveryDate[1]',       'nvarchar(20)')  'DeliveryDate'
           ,nodes.entity.value('Remarks[1]',            'nvarchar(255)') 'Remarks'
           ,nodes.entity.value('NumberOfLines[1]',      'int')          'NumberOfLines'
           --,CAST(ISNULL(nodes.entity.value('VatPercentage[1]', 'nvarchar(50)'),'0.00') AS decimal(13,3)) 'VatPercentage'
           --,CAST(ISNULL(nodes.entity.value('VatSummary[1]',    'nvarchar(50)'),'0.00') AS decimal(13,3)) 'VatSummary'
           --,CAST(ISNULL(nodes.entity.value('Total[1]',         'nvarchar(50)'),'0.00') AS decimal(13,3)) 'Total'
           ,nodes.entity.value('Additional1[1]',        'nvarchar(255)') 'Additional1'
           ,nodes.entity.value('Additional2[1]',        'nvarchar(255)') 'Additional2'
           ,nodes.entity.value('Additional3[1]',        'nvarchar(255)') 'Additional3'
           ,nodes.entity.value('Additional4[1]',        'nvarchar(255)') 'Additional4'
           ,nodes.entity.value('Additional5[1]',        'nvarchar(255)') 'Additional5'
           ,nodes.entity.value('Additional6[1]',        'nvarchar(255)') 'Additional6'
           ,nodes.entity.value('Additional7[1]',        'nvarchar(255)') 'Additional7'
           ,nodes.entity.value('Additional8[1]',        'nvarchar(255)') 'Additional8'
           ,nodes.entity.value('Additional9[1]',        'nvarchar(255)') 'Additional9'
           ,nodes.entity.value('Additional10[1]',       'nvarchar(255)') 'Additional10'
           --,nodes.entity.value('ProcessedDate[1]',      'datetime
                                        --,nodes.entity.value('InsertDate[1]',         'datetime
                                        FROM
                                        @doc.nodes('/root/Body/Item') AS nodes(entity)
           
       Set @InterfaceImportHeaderId = @@Identity
           
    INSERT INTO [InterfaceImportDetail]
           (InterfaceImportHeaderId,
           [ForeignKey]
           ,[LineNumber]
           ,[ProductCode]
           ,[Product]
           ,[SKUCode]
           ,[Batch]
           ,[Quantity]
           ,[Weight]
           ,[RetailPrice]
           ,[NetPrice]
           ,[LineTotal]
           ,[Volume]
           ,[Additional1]
           ,[Additional2]
           ,[Additional3]
           ,[Additional4]
           ,[Additional5]
           --,[Additional6]
           --,[Additional7]
           --,[Additional8]
           --,[Additional9]
           --,[Additional10]
           )
    SELECT @InterfaceImportHeaderId,
           nodes.entity.value('ForeignKey[1]',         'nvarchar(40)')  'ForeignKey'
           ,nodes.entity.value('LineNumber[1]',         'int')          'LineNumber'
           ,nodes.entity.value('ProductCode[1]',        'nvarchar(30)')  'ProductCode'
           ,nodes.entity.value('Product[1]',            'nvarchar(50)')  'Product'
           ,nodes.entity.value('SKUCode[1]',            'nvarchar(10)')  'SKUCode'
           ,nodes.entity.value('Batch[1]',              'nvarchar(50)')  'Batch'
           ,nodes.entity.value('Quantity[1]',           'float')          'Quantity'
           ,CAST(ISNULL(nodes.entity.value('Weight[1]',      'nvarchar(50)'),'0.00') AS decimal(13,3))  'Weight'
           ,CAST(ISNULL(nodes.entity.value('RetailPrice[1]', 'nvarchar(50)'),'0.00') AS decimal(13,2))  'RetailPrice'
           ,CAST(ISNULL(nodes.entity.value('NetPrice[1]',    'nvarchar(50)'),'0.00') AS decimal(13,2))  'NetPrice'
           ,CAST(ISNULL(nodes.entity.value('LineTotal[1]',   'nvarchar(50)'),'0.00') AS decimal(13,2))  'LineTotal'
           ,CAST(ISNULL(nodes.entity.value('Volume[1]',      'nvarchar(50)'),'0.00') AS decimal(13,3))  'Volume'
           ,nodes.entity.value('Additional1[1]',        'nvarchar(255)') 'Additional1'
           ,nodes.entity.value('Additional2[1]',        'nvarchar(255)') 'Additional2'
           ,nodes.entity.value('Additional3[1]',        'nvarchar(255)') 'Additional3'
           ,nodes.entity.value('Additional4[1]',        'nvarchar(255)') 'Additional4'
           ,nodes.entity.value('Additional5[1]',        'nvarchar(255)') 'Additional5'
           --,nodes.entity.value('Additional6[1]',        'nvarchar(255)') 'Additional6'
           --,nodes.entity.value('Additional7[1]',        'nvarchar(255)') 'Additional7'
           --,nodes.entity.value('Additional8[1]',        'nvarchar(255)') 'Additional8'
           --,nodes.entity.value('Additional9[1]',        'nvarchar(255)') 'Additional9'
           --,nodes.entity.value('Additional10[1]',       'nvarchar(255)') 'Additional10'
           FROM
           @doc.nodes('/root/Body/Item/ItemLine') AS nodes(entity)
           
           --update d
           --set InterfaceImportHeaderId = h.InterfaceImportHeaderid
           --from InterfaceImportDetail d
    -- Join InterfaceImportHeader h on h.PrimaryKey = d.ForeignKey
    --where h.RecordStatus = 'W' and InsertDate = @InsertDate
    --    and d.InterfaceImportHeaderid is null
    
    Update InterfaceImportHeader
       set RecordStatus = 'N',
           PrincipalCode = @principalCode
     Where RecordSTatus = 'W'
       and InsertDate = @InsertDate
    
    
  END TRY
  BEGIN CATCH
    
    SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)
  END CATCH
  
  --exec p_Generic_Import_Order
  
     SET @XMLBody = '<root><status>'
         + CASE WHEN LEN(@ERROR) > 0 THEN @ERROR
         ELSE 'Order(s) successfully imported'
END
+ '</status></root>'
End
 
 
