﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Get_SOH
  ///   Filename       : p_Container_Get_SOH.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Aug 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Get_SOH
(
 @WarehouseId       int,
 @OperatorId        int,
 @ContainerHeaderId int
)

as
begin
  set nocount on;
  
  select ch.ContainerHeaderId,
         cd.ContainerDetailId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         l.Location,
         subl.ActualQuantity
    from ContainerHeader            ch (nolock)
    join ContainerDetail            cd (nolock) on ch.ContainerHeaderId = cd.ContainerHeaderId
    join StorageUnitBatchLocation subl (nolock) on cd.StorageUnitBatchId = subl.StorageUnitBatchId
                                               and ch.StoreLocationId = subl.LocationId
    join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit                su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId = p.ProductId
    join SKU                       sku (nolock) on su.SKUId = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId = b.BatchId
    join Location                    l (nolock) on subl.LocationId = l.LocationId
   where ch.ContainerHeaderId = @ContainerHeaderId
end
