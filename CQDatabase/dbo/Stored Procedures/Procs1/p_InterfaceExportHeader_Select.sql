﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportHeader_Select
  ///   Filename       : p_InterfaceExportHeader_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceExportHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportHeaderId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportHeader.InterfaceExportHeaderId,
  ///   InterfaceExportHeader.IssueId,
  ///   InterfaceExportHeader.PrimaryKey,
  ///   InterfaceExportHeader.OrderNumber,
  ///   InterfaceExportHeader.RecordType,
  ///   InterfaceExportHeader.RecordStatus,
  ///   InterfaceExportHeader.CompanyCode,
  ///   InterfaceExportHeader.Company,
  ///   InterfaceExportHeader.Address,
  ///   InterfaceExportHeader.FromWarehouseCode,
  ///   InterfaceExportHeader.ToWarehouseCode,
  ///   InterfaceExportHeader.Route,
  ///   InterfaceExportHeader.DeliveryNoteNumber,
  ///   InterfaceExportHeader.ContainerNumber,
  ///   InterfaceExportHeader.SealNumber,
  ///   InterfaceExportHeader.DeliveryDate,
  ///   InterfaceExportHeader.Remarks,
  ///   InterfaceExportHeader.NumberOfLines,
  ///   InterfaceExportHeader.Additional1,
  ///   InterfaceExportHeader.Additional2,
  ///   InterfaceExportHeader.Additional3,
  ///   InterfaceExportHeader.Additional4,
  ///   InterfaceExportHeader.Additional5,
  ///   InterfaceExportHeader.Additional6,
  ///   InterfaceExportHeader.Additional7,
  ///   InterfaceExportHeader.Additional8,
  ///   InterfaceExportHeader.Additional9,
  ///   InterfaceExportHeader.Additional10,
  ///   InterfaceExportHeader.ProcessedDate,
  ///   InterfaceExportHeader.InsertDate,
  ///   InterfaceExportHeader.WebServiceMsg 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportHeader_Select
(
 @InterfaceExportHeaderId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceExportHeader.InterfaceExportHeaderId
        ,InterfaceExportHeader.IssueId
        ,InterfaceExportHeader.PrimaryKey
        ,InterfaceExportHeader.OrderNumber
        ,InterfaceExportHeader.RecordType
        ,InterfaceExportHeader.RecordStatus
        ,InterfaceExportHeader.CompanyCode
        ,InterfaceExportHeader.Company
        ,InterfaceExportHeader.Address
        ,InterfaceExportHeader.FromWarehouseCode
        ,InterfaceExportHeader.ToWarehouseCode
        ,InterfaceExportHeader.Route
        ,InterfaceExportHeader.DeliveryNoteNumber
        ,InterfaceExportHeader.ContainerNumber
        ,InterfaceExportHeader.SealNumber
        ,InterfaceExportHeader.DeliveryDate
        ,InterfaceExportHeader.Remarks
        ,InterfaceExportHeader.NumberOfLines
        ,InterfaceExportHeader.Additional1
        ,InterfaceExportHeader.Additional2
        ,InterfaceExportHeader.Additional3
        ,InterfaceExportHeader.Additional4
        ,InterfaceExportHeader.Additional5
        ,InterfaceExportHeader.Additional6
        ,InterfaceExportHeader.Additional7
        ,InterfaceExportHeader.Additional8
        ,InterfaceExportHeader.Additional9
        ,InterfaceExportHeader.Additional10
        ,InterfaceExportHeader.ProcessedDate
        ,InterfaceExportHeader.InsertDate
        ,InterfaceExportHeader.WebServiceMsg
    from InterfaceExportHeader
   where isnull(InterfaceExportHeader.InterfaceExportHeaderId,'0')  = isnull(@InterfaceExportHeaderId, isnull(InterfaceExportHeader.InterfaceExportHeaderId,'0'))
  
end
