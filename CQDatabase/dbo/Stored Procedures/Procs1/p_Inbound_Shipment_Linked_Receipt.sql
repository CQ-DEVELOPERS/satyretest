﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Inbound_Shipment_Linked_Receipt
  ///   Filename       : p_Inbound_Shipment_Linked_Receipt.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Inbound_Shipment_Linked_Receipt
(
 @InboundShipmentId int
)

as
begin
	 set nocount on;
  
  select isr.InboundShipmentId,
         r.ReceiptId,
         id.OrderNumber
    from InboundDocument        id  (nolock)
    join Receipt                r   (nolock) on id.InboundDocumentId = r.InboundDocumentId
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId          = isr.ReceiptId
   where InboundShipmentId = @InboundShipmentId
end
