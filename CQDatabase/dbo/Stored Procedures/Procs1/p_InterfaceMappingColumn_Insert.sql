﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingColumn_Insert
  ///   Filename       : p_InterfaceMappingColumn_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 Feb 2014 10:53:56
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceMappingColumn table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMappingColumnId int = null output,
  ///   @InterfaceMappingColumnCode nvarchar(60) = null,
  ///   @InterfaceMappingColumn nvarchar(100) = null,
  ///   @InterfaceDocumentTypeId int = null,
  ///   @InterfaceFieldId int = null,
  ///   @ColumnNumber int = null,
  ///   @StartPosition int = null,
  ///   @EndPostion int = null,
  ///   @Format nvarchar(60) = null,
  ///   @InterfaceMappingFileId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceMappingColumn.InterfaceMappingColumnId,
  ///   InterfaceMappingColumn.InterfaceMappingColumnCode,
  ///   InterfaceMappingColumn.InterfaceMappingColumn,
  ///   InterfaceMappingColumn.InterfaceDocumentTypeId,
  ///   InterfaceMappingColumn.InterfaceFieldId,
  ///   InterfaceMappingColumn.ColumnNumber,
  ///   InterfaceMappingColumn.StartPosition,
  ///   InterfaceMappingColumn.EndPostion,
  ///   InterfaceMappingColumn.Format,
  ///   InterfaceMappingColumn.InterfaceMappingFileId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingColumn_Insert
(
 @InterfaceMappingColumnId int = null output,
 @InterfaceMappingColumnCode nvarchar(60) = null,
 @InterfaceMappingColumn nvarchar(100) = null,
 @InterfaceDocumentTypeId int = null,
 @InterfaceFieldId int = null,
 @ColumnNumber int = null,
 @StartPosition int = null,
 @EndPostion int = null,
 @Format nvarchar(60) = null,
 @InterfaceMappingFileId int = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceMappingColumnId = '-1'
    set @InterfaceMappingColumnId = null;
  
  if @InterfaceMappingColumnCode = '-1'
    set @InterfaceMappingColumnCode = null;
  
  if @InterfaceMappingColumn = '-1'
    set @InterfaceMappingColumn = null;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
  if @InterfaceFieldId = '-1'
    set @InterfaceFieldId = null;
  
  if @InterfaceMappingFileId = '-1'
    set @InterfaceMappingFileId = null;
  
	 declare @Error int
 
  insert InterfaceMappingColumn
        (InterfaceMappingColumnCode,
         InterfaceMappingColumn,
         InterfaceDocumentTypeId,
         InterfaceFieldId,
         ColumnNumber,
         StartPosition,
         EndPostion,
         Format,
         InterfaceMappingFileId)
  select @InterfaceMappingColumnCode,
         @InterfaceMappingColumn,
         @InterfaceDocumentTypeId,
         @InterfaceFieldId,
         @ColumnNumber,
         @StartPosition,
         @EndPostion,
         @Format,
         @InterfaceMappingFileId 
  
  select @Error = @@Error, @InterfaceMappingColumnId = scope_identity()
  
  
  return @Error
  
end
