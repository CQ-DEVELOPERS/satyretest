﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Company_Delete
  ///   Filename       : p_Company_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:51
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Company table.
  /// </remarks>
  /// <param>
  ///   @CompanyId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Company_Delete
(
 @CompanyId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Company
     where CompanyId = @CompanyId
  
  select @Error = @@Error
  
  
  return @Error
  
end
