﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Update_Count
  ///   Filename       : p_Housekeeping_Stock_Take_Update_Count.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Update_Count
(
 @instructionId int,
 @operatorId    int,
 @latestCount   int
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @StatusId           int,
          @NewInstructionId   int,
          @InstructionTypeId	 int,
          @StorageUnitBatchId	int,
          @WarehouseId	       int,
          @JobId	             int,
          @PickLocationId	    int,
          @InstructionRefId	  int,
          @Quantity	          float,
          @ConfirmedQuantity  float,
          @CreateDate	        datetime,
          @statusCode         nvarchar(10),
          @JobStatusId        int
          
  select @GetDate = dbo.ufn_Getdate()
  
  select @ConfirmedQuantity = isnull(i.ConfirmedQuantity, 0),
         @JobId             = i.JobId,
         @JobStatusId       = j.StatusId
    from Instruction i (nolock)
    join Job         j (nolock) on i.JobId = j.JobId
   where i.InstructionId = @instructionId
  
  if @latestCount = @ConfirmedQuantity
    set @statusCode = 'F'
  else
    set @statusCode = 'U'
  
  begin transaction
  
  select @StatusId = dbo.ufn_StatusId('J','S')
  
  if @JobStatusId != @StatusId
  begin
    exec @Error = p_Job_Update
     @JobId     = @JobId,
     @StatusId  = @StatusId
    
    if @Error <> 0
      goto error
  end
  
  if @statusCode = 'F'
  begin
    select @StatusId = dbo.ufn_StatusId('I','F')
    
    exec p_Instruction_Update
     @InstructionId = @instructionId,
     @StatusId      = @StatusId,
     @ConfirmedQuantity = @latestCount
    
    if @Error <> 0
      goto error
  end
  
  if @statusCode = 'U'
  begin
    select @StatusId = dbo.ufn_StatusId('I','F')
    
    exec p_Instruction_Update
     @InstructionId     = @instructionId,
     @StatusId          = @StatusId,
     @ConfirmedQuantity = @latestCount
    
    if @Error <> 0
      goto error
    
--    select @InstructionTypeId	 = InstructionTypeId,
--           @StorageUnitBatchId	= StorageUnitBatchId,
--           @WarehouseId	       = WarehouseId,
--           @JobId              = JobId,
--           @PickLocationId	    = PickLocationId,
--           @Quantity           = Quantity
--      from Instruction
--     where InstructionId = @instructionId
--    
--    select @StatusId = dbo.ufn_StatusId('I','F')
--    
--    exec @Error = p_Instruction_Insert
--     @InstructionId	     = @NewInstructionId output,
--     @InstructionTypeId	 = @InstructionTypeId,
--     @StorageUnitBatchId	= @StorageUnitBatchId,
--     @WarehouseId	       = @WarehouseId,
--     @StatusId           = @StatusId,
--     @JobId	             = @JobId,
--     @OperatorId	        = null,
--     @PickLocationId	    = @PickLocationId,
--     @InstructionRefId	  = @instructionId,
--     @Quantity	          = @Quantity,
--     @ConfirmedQuantity  = @latestCount,
--     @CreateDate	        = @GetDate
--    
--    if @Error <> 0
--      goto error
  end
  else if @statusCode = 'A'
  begin
    select @StatusId = dbo.ufn_StatusId('I','F')
    
    exec p_Instruction_Update
     @InstructionId = @instructionId,
     @StatusId      = @StatusId
    
    if @Error <> 0
      goto error
  end
  
  if (select count(1)
        from Instruction i (nolock)
        join Status      s (nolock) on i.StatusId = s.StatusId
       where i.JobId       = @JobId
         and s.Type        = 'I'
         and s.StatusCode  = 'W') = 0
  begin
    select @StatusId = dbo.ufn_StatusId('J','F')
    
    exec @Error = p_Job_Update
     @JobId     = @JobId,
     @StatusId  = @StatusId
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_Take_Update_Count'); 
    rollback transaction
    return @Error
end
