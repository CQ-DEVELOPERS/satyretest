﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMProduct_Delete
  ///   Filename       : p_BOMProduct_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:14
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the BOMProduct table.
  /// </remarks>
  /// <param>
  ///   @BOMLineId int = null,
  ///   @LineNumber int = null,
  ///   @StorageUnitId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMProduct_Delete
(
 @BOMLineId int = null,
 @LineNumber int = null,
 @StorageUnitId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete BOMProduct
     where BOMLineId = @BOMLineId
       and LineNumber = @LineNumber
       and StorageUnitId = @StorageUnitId
  
  select @Error = @@Error
  
  
  return @Error
  
end
