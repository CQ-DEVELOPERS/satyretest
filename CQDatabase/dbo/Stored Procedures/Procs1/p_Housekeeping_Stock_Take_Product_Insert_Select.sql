﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Product_Insert_Select
  ///   Filename       : p_Housekeeping_Stock_Take_Product_Insert_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Product_Insert_Select
(
 @instructionId int
)

as
begin
	 set nocount on;
  
  select l.LocationId,
         l.Location,
         convert(int, null) as 'Quantity',
         convert(int, null) as 'StorageUnitBatchId'
    from Instruction i (nolock)
    join Location    l (nolock) on i.PickLocationId = l.LocationId
   where i.InstructionId    = @instructionId
  group by l.LocationId,
           l.Location
end
