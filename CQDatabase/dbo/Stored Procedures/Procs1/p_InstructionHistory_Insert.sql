﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionHistory_Insert
  ///   Filename       : p_InstructionHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Aug 2013 13:13:49
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InstructionHistory table.
  /// </remarks>
  /// <param>
  ///   @InstructionId int = null,
  ///   @ReasonId int = null,
  ///   @InstructionTypeId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @WarehouseId int = null,
  ///   @StatusId int = null,
  ///   @JobId int = null,
  ///   @OperatorId int = null,
  ///   @PickLocationId int = null,
  ///   @StoreLocationId int = null,
  ///   @ReceiptLineId int = null,
  ///   @IssueLineId int = null,
  ///   @InstructionRefId int = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuantity float = null,
  ///   @Weight float = null,
  ///   @ConfirmedWeight float = null,
  ///   @PalletId int = null,
  ///   @CreateDate datetime = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null,
  ///   @Picked bit = null,
  ///   @Stored bit = null,
  ///   @CheckQuantity float = null,
  ///   @CheckWeight float = null,
  ///   @OutboundShipmentId int = null,
  ///   @DropSequence int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @PackagingWeight float = null,
  ///   @PackagingQuantity float = null,
  ///   @NettWeight float = null,
  ///   @PreviousQuantity float = null,
  ///   @AuthorisedBy int = null,
  ///   @AuthorisedStatus nvarchar(20) = null,
  ///   @SOH float = null,
  ///   @AutoComplete bit = null 
  /// </param>
  /// <returns>
  ///   InstructionHistory.InstructionId,
  ///   InstructionHistory.ReasonId,
  ///   InstructionHistory.InstructionTypeId,
  ///   InstructionHistory.StorageUnitBatchId,
  ///   InstructionHistory.WarehouseId,
  ///   InstructionHistory.StatusId,
  ///   InstructionHistory.JobId,
  ///   InstructionHistory.OperatorId,
  ///   InstructionHistory.PickLocationId,
  ///   InstructionHistory.StoreLocationId,
  ///   InstructionHistory.ReceiptLineId,
  ///   InstructionHistory.IssueLineId,
  ///   InstructionHistory.InstructionRefId,
  ///   InstructionHistory.Quantity,
  ///   InstructionHistory.ConfirmedQuantity,
  ///   InstructionHistory.Weight,
  ///   InstructionHistory.ConfirmedWeight,
  ///   InstructionHistory.PalletId,
  ///   InstructionHistory.CreateDate,
  ///   InstructionHistory.StartDate,
  ///   InstructionHistory.EndDate,
  ///   InstructionHistory.Picked,
  ///   InstructionHistory.Stored,
  ///   InstructionHistory.CheckQuantity,
  ///   InstructionHistory.CheckWeight,
  ///   InstructionHistory.OutboundShipmentId,
  ///   InstructionHistory.DropSequence,
  ///   InstructionHistory.CommandType,
  ///   InstructionHistory.InsertDate,
  ///   InstructionHistory.PackagingWeight,
  ///   InstructionHistory.PackagingQuantity,
  ///   InstructionHistory.NettWeight,
  ///   InstructionHistory.PreviousQuantity,
  ///   InstructionHistory.AuthorisedBy,
  ///   InstructionHistory.AuthorisedStatus,
  ///   InstructionHistory.SOH,
  ///   InstructionHistory.AutoComplete 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionHistory_Insert
(
 @InstructionId int = null,
 @ReasonId int = null,
 @InstructionTypeId int = null,
 @StorageUnitBatchId int = null,
 @WarehouseId int = null,
 @StatusId int = null,
 @JobId int = null,
 @OperatorId int = null,
 @PickLocationId int = null,
 @StoreLocationId int = null,
 @ReceiptLineId int = null,
 @IssueLineId int = null,
 @InstructionRefId int = null,
 @Quantity float = null,
 @ConfirmedQuantity float = null,
 @Weight float = null,
 @ConfirmedWeight float = null,
 @PalletId int = null,
 @CreateDate datetime = null,
 @StartDate datetime = null,
 @EndDate datetime = null,
 @Picked bit = null,
 @Stored bit = null,
 @CheckQuantity float = null,
 @CheckWeight float = null,
 @OutboundShipmentId int = null,
 @DropSequence int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @PackagingWeight float = null,
 @PackagingQuantity float = null,
 @NettWeight float = null,
 @PreviousQuantity float = null,
 @AuthorisedBy int = null,
 @AuthorisedStatus nvarchar(20) = null,
 @SOH float = null,
 @AutoComplete bit = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InstructionHistory
        (InstructionId,
         ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         ReceiptLineId,
         IssueLineId,
         InstructionRefId,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         CheckQuantity,
         CheckWeight,
         OutboundShipmentId,
         DropSequence,
         CommandType,
         InsertDate,
         PackagingWeight,
         PackagingQuantity,
         NettWeight,
         PreviousQuantity,
         AuthorisedBy,
         AuthorisedStatus,
         SOH,
         AutoComplete)
  select @InstructionId,
         @ReasonId,
         @InstructionTypeId,
         @StorageUnitBatchId,
         @WarehouseId,
         @StatusId,
         @JobId,
         @OperatorId,
         @PickLocationId,
         @StoreLocationId,
         @ReceiptLineId,
         @IssueLineId,
         @InstructionRefId,
         @Quantity,
         @ConfirmedQuantity,
         @Weight,
         @ConfirmedWeight,
         @PalletId,
         @CreateDate,
         @StartDate,
         @EndDate,
         @Picked,
         @Stored,
         @CheckQuantity,
         @CheckWeight,
         @OutboundShipmentId,
         @DropSequence,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @PackagingWeight,
         @PackagingQuantity,
         @NettWeight,
         @PreviousQuantity,
         @AuthorisedBy,
         @AuthorisedStatus,
         @SOH,
         @AutoComplete 
  
  select @Error = @@Error
  
  
  return @Error
  
end
