﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLogDetail_Select
  ///   Filename       : p_InterfaceLogDetail_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:29
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceLogDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceLogDetailId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceLogDetail.InterfaceLogDetailId,
  ///   InterfaceLogDetail.OrderNumber,
  ///   InterfaceLogDetail.ReceivedByHost,
  ///   InterfaceLogDetail.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLogDetail_Select
(
 @InterfaceLogDetailId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceLogDetail.InterfaceLogDetailId
        ,InterfaceLogDetail.OrderNumber
        ,InterfaceLogDetail.ReceivedByHost
        ,InterfaceLogDetail.ProcessedDate
    from InterfaceLogDetail
   where isnull(InterfaceLogDetail.InterfaceLogDetailId,'0')  = isnull(@InterfaceLogDetailId, isnull(InterfaceLogDetail.InterfaceLogDetailId,'0'))
  
end
