﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperator_List
  ///   Filename       : p_AreaOperator_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:46
  /// </summary>
  /// <remarks>
  ///   Selects rows from the AreaOperator table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   AreaOperator.AreaId,
  ///   AreaOperator.OperatorId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperator_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as AreaId
        ,null as 'AreaOperator'
        ,'-1' as OperatorId
        ,null as 'AreaOperator'
  union
  select
         AreaOperator.AreaId
        ,AreaOperator.AreaId as 'AreaOperator'
        ,AreaOperator.OperatorId
        ,AreaOperator.OperatorId as 'AreaOperator'
    from AreaOperator
  
end
