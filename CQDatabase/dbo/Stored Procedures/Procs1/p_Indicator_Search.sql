﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Indicator_Search
  ///   Filename       : p_Indicator_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:25
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Indicator table.
  /// </remarks>
  /// <param>
  ///   @IndicatorId int = null output,
  ///   @Indicator nvarchar(100) = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Indicator.IndicatorId,
  ///   Indicator.Header,
  ///   Indicator.Indicator,
  ///   Indicator.IndicatorValue,
  ///   Indicator.IndicatorDisplay,
  ///   Indicator.ThresholdGreen,
  ///   Indicator.ThresholdYellow,
  ///   Indicator.ThresholdRed,
  ///   Indicator.ModifiedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Indicator_Search
(
 @IndicatorId int = null output,
 @Indicator nvarchar(100) = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @IndicatorId = '-1'
    set @IndicatorId = null;
  
  if @Indicator = '-1'
    set @Indicator = null;
  
 
  select
         Indicator.IndicatorId
        ,Indicator.Header
        ,Indicator.Indicator
        ,Indicator.IndicatorValue
        ,Indicator.IndicatorDisplay
        ,Indicator.ThresholdGreen
        ,Indicator.ThresholdYellow
        ,Indicator.ThresholdRed
        ,Indicator.ModifiedDate
    from Indicator
   where isnull(Indicator.IndicatorId,'0')  = isnull(@IndicatorId, isnull(Indicator.IndicatorId,'0'))
     and isnull(Indicator.Indicator,'%')  like '%' + isnull(@Indicator, isnull(Indicator.Indicator,'%')) + '%'
  
end
