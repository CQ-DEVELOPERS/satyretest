﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMappingFile_Update
  ///   Filename       : p_InterfaceMappingFile_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 09:03:33
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceMappingFile table.
  /// </remarks>
  /// <param>
  ///   @InterfaceMappingFileId int = null,
  ///   @PrincipalId int = null,
  ///   @InterfaceFileTypeId int = null,
  ///   @InterfaceDocumentTypeId int = null,
  ///   @Delimiter char(2) = null,
  ///   @StyleSheet xml = null,
  ///   @PrincipalFTPSite nvarchar(max) = null,
  ///   @FTPUserName nvarchar(100) = null,
  ///   @FTPPassword nvarchar(100) = null,
  ///   @HeaderRow int = null,
  ///   @SubDirectory nvarchar(max) = null,
  ///   @FilePrefix nvarchar(60) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMappingFile_Update
(
 @InterfaceMappingFileId int = null,
 @PrincipalId int = null,
 @InterfaceFileTypeId int = null,
 @InterfaceDocumentTypeId int = null,
 @Delimiter char(2) = null,
 @StyleSheet xml = null,
 @PrincipalFTPSite nvarchar(max) = null,
 @FTPUserName nvarchar(100) = null,
 @FTPPassword nvarchar(100) = null,
 @HeaderRow int = null,
 @SubDirectory nvarchar(max) = null,
 @FilePrefix nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceMappingFileId = '-1'
    set @InterfaceMappingFileId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @InterfaceFileTypeId = '-1'
    set @InterfaceFileTypeId = null;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
	 declare @Error int
 
  update InterfaceMappingFile
     set PrincipalId = isnull(@PrincipalId, PrincipalId),
         InterfaceFileTypeId = isnull(@InterfaceFileTypeId, InterfaceFileTypeId),
         InterfaceDocumentTypeId = isnull(@InterfaceDocumentTypeId, InterfaceDocumentTypeId),
         Delimiter = isnull(@Delimiter, Delimiter),
         StyleSheet = isnull(@StyleSheet, StyleSheet),
         PrincipalFTPSite = isnull(@PrincipalFTPSite, PrincipalFTPSite),
         FTPUserName = isnull(@FTPUserName, FTPUserName),
         FTPPassword = isnull(@FTPPassword, FTPPassword),
         HeaderRow = isnull(@HeaderRow, HeaderRow),
         SubDirectory = isnull(@SubDirectory, SubDirectory),
         FilePrefix = isnull(@FilePrefix, FilePrefix) 
   where InterfaceMappingFileId = @InterfaceMappingFileId
  
  select @Error = @@Error
  
  
  return @Error
  
end
