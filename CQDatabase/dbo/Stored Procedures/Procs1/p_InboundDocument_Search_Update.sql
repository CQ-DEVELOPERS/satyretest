﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Search_Update
  ///   Filename       : p_InboundDocument_Search_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Search_Update
(
 @WarehouseId           int = null,
 @InboundDocumentId     int = null,
 @InboundDocumentTypeId int = null,
 @ExternalCompanyCode   nvarchar(30) = null,
 @ExternalCompany       nvarchar(255) = null,
 @OrderNumber           nvarchar(30) = null,
 @FromDate              datetime = null,
 @ToDate                datetime = null,
 @returnType            nvarchar(3) = null,
 @ContactListId			int = null
)

as
begin
  set nocount on;
  
  declare @StatusId int
  
  declare @TableResult as
  table(
        InboundDocumentId         int,
        OrderNumber               nvarchar(30),
        ExternalCompanyId         int,
        ExternalCompanyCode       nvarchar(30),
        ExternalCompany           nvarchar(255),
        NumberOfLines             int,
        DeliveryDate              datetime,
        CreateDate                datetime,
        StatusId                  int,
        Status                    nvarchar(50),
        InboundDocumentTypeId     int,
        InboundDocumentType       nvarchar(30),
        InboundDocumentTypeCode   nvarchar(10),
        ReferenceNumber           nvarchar(30),
        ReasonId                  int,
        Reason                    nvarchar(50),
        Remarks                   nvarchar(250),
        ContactListId			          int,
        ContactPerson			          nvarchar(50),
        PrincipalId               int,
        Principal                 nvarchar(255)
       );
  
  select @StatusId = dbo.ufn_StatusId('ID','N');
  
  if @InboundDocumentTypeId = -1
    set @InboundDocumentTypeId = null
  
  if @InboundDocumentId = -1
    set @InboundDocumentId = null
  
  if @ExternalCompanyCode is null
    set @ExternalCompanyCode = ''
   
  if @ExternalCompany is null
    set @ExternalCompany = ''
  
  if @InboundDocumentId is null
  begin
    if @returnType = 'RET'
    begin
      insert @TableResult
            (InboundDocumentId,
             OrderNumber,
             ExternalCompanyId,
             ExternalCompanyCode,
             ExternalCompany,
             Status,
             InboundDocumentType,
             InboundDocumentTypeCode,
             DeliveryDate,
             CreateDate,
             InboundDocumentTypeId,
             ReferenceNumber,
             ReasonId,
             Remarks,
             ContactListId,
             ContactPerson,
             PrincipalId,
             Principal)
      select id.InboundDocumentId,
             id.OrderNumber,
             id.ExternalCompanyId,
             ec.ExternalCompanyCode,
             ec.ExternalCompany,
             s.Status,
             idt.InboundDocumentType,
             idt.InboundDocumentTypeCode,
             id.DeliveryDate,
             id.CreateDate,
             id.InboundDocumentTypeId,
             id.ReferenceNumber,
             id.ReasonId,
             id.Remarks,
             idc.ContactListId,
             cl.ContactPerson,
             p.PrincipalId,
             p.Principal
        from InboundDocument     id  (nolock)
        join Status              s   (nolock) on id.StatusId          = s.StatusId
        join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
        join ExternalCompany     ec  (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
        left join InboundDocumentContactList idc (nolock) on idc.InboundDocumentId = @InboundDocumentId
        left join ContactList    cl  (nolock) on cl.ContactListId = idc.ContactListId
        left join Principal              p (nolock) on id.PrincipalId = p.PrincipalId
       where idt.InboundDocumentTypeCode = 'RET'
         and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
         and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
         and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
         and id.DeliveryDate       between isnull(@FromDate, id.DeliveryDate) and isnull(@ToDate, id.DeliveryDate)
         and s.StatusId                  = @StatusId
         and id.WarehouseId              = @WarehouseId
         and id.WarehouseId              = isnull(@WarehouseId, id.WarehouseId)
         and id.ReferenceNumber is not null
      union
      select id.InboundDocumentId,
             id.OrderNumber,
             id.ExternalCompanyId,
             null,
             null,
             s.Status,
             idt.InboundDocumentType,
             idt.InboundDocumentTypeCode,
             id.DeliveryDate,
             id.CreateDate,
             id.InboundDocumentTypeId,
             id.ReferenceNumber,
             id.ReasonId,
             id.Remarks,
             idc.ContactListId,
             cl.ContactPerson,
             p.PrincipalId,
             p.Principal
        from InboundDocument     id  (nolock)
        join Status              s   (nolock) on id.StatusId          = s.StatusId
        join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
        left join InboundDocumentContactList idc (nolock) on idc.InboundDocumentId = @InboundDocumentId
        left join ContactList    cl  (nolock) on cl.ContactListId = idc.ContactListId
        left join Principal              p (nolock) on id.PrincipalId = p.PrincipalId
       where idt.InboundDocumentTypeCode = 'RET'
         and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
         and id.DeliveryDate       between isnull(@FromDate, id.DeliveryDate) and isnull(@ToDate, id.DeliveryDate)
         and s.StatusId                  = @StatusId
         and id.WarehouseId              = @WarehouseId
         and id.WarehouseId               = isnull(@WarehouseId, id.WarehouseId)
         and not exists(select 1 from ExternalCompany ec  (nolock) where id.ExternalCompanyId = ec.ExternalCompanyId)
         and id.ReferenceNumber is not null
    end
    else if @returnType = 'UAC'
    begin
      insert @TableResult
            (InboundDocumentId,
             OrderNumber,
             ExternalCompanyId,
             ExternalCompanyCode,
             ExternalCompany,
             Status,
             InboundDocumentType,
             InboundDocumentTypeCode,
             DeliveryDate,
             CreateDate,
             InboundDocumentTypeId,
             ReferenceNumber,
             ReasonId,
             Remarks,
             ContactListId,
             ContactPerson,
             PrincipalId,
             Principal)
      select id.InboundDocumentId,
             id.OrderNumber,
             id.ExternalCompanyId,
             ec.ExternalCompanyCode,
             ec.ExternalCompany,
             s.Status,
             idt.InboundDocumentType,
             idt.InboundDocumentTypeCode,
             id.DeliveryDate,
             id.CreateDate,
             id.InboundDocumentTypeId,
             id.ReferenceNumber,
             id.ReasonId,
             id.Remarks,
             idc.ContactListId,
             cl.ContactPerson,
             p.PrincipalId,
             p.Principal
        from InboundDocument     id  (nolock)
        join Status              s   (nolock) on id.StatusId          = s.StatusId
        join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
        join ExternalCompany     ec  (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
        left join InboundDocumentContactList idc (nolock) on idc.InboundDocumentId = @InboundDocumentId
        left join ContactList    cl  (nolock) on cl.ContactListId = idc.ContactListId
        left join Principal              p (nolock) on id.PrincipalId = p.PrincipalId
       where idt.InboundDocumentTypeCode = 'RET'
         and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
         and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
         and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
         and id.DeliveryDate       between isnull(@FromDate, id.DeliveryDate) and isnull(@ToDate, id.DeliveryDate)
         and s.StatusId                  = @StatusId
         and id.WarehouseId              = @WarehouseId
         and id.WarehouseId              = isnull(@WarehouseId, id.WarehouseId)
         and isnull(id.ReferenceNumber,'') = ''
      union
      select id.InboundDocumentId,
             id.OrderNumber,
             id.ExternalCompanyId,
             null,
             null,
             s.Status,
             idt.InboundDocumentType,
             idt.InboundDocumentTypeCode,
             id.DeliveryDate,
             id.CreateDate,
             id.InboundDocumentTypeId,
             id.ReferenceNumber,
             id.ReasonId,
             id.Remarks,
             idc.ContactListId,
             cl.ContactPerson,
             p.PrincipalId,
             p.Principal
        from InboundDocument     id  (nolock)
        join Status              s   (nolock) on id.StatusId          = s.StatusId
        join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
        left join InboundDocumentContactList idc (nolock) on idc.InboundDocumentId = @InboundDocumentId
        left join ContactList    cl  (nolock) on cl.ContactListId = idc.ContactListId
        left join Principal              p (nolock) on id.PrincipalId = p.PrincipalId
       where idt.InboundDocumentTypeCode = 'RET'
         and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
         and id.DeliveryDate       between isnull(@FromDate, id.DeliveryDate) and isnull(@ToDate, id.DeliveryDate)
         and s.StatusId                  = @StatusId
         and id.WarehouseId              = @WarehouseId
         and id.WarehouseId               = isnull(@WarehouseId, id.WarehouseId)
         and not exists(select 1 from ExternalCompany ec  (nolock) where id.ExternalCompanyId = ec.ExternalCompanyId)
         and isnull(id.ReferenceNumber,'') = ''
    end
    else
    begin
      insert @TableResult
            (InboundDocumentId,
             OrderNumber,
             ExternalCompanyId,
             ExternalCompanyCode,
             ExternalCompany,
             Status,
             InboundDocumentType,
             InboundDocumentTypeCode,
             DeliveryDate,
             CreateDate,
             InboundDocumentTypeId,
             ReferenceNumber,
             ReasonId,
             Remarks,
             ContactListId,
             ContactPerson,
             PrincipalId,
             Principal)
      select id.InboundDocumentId,
             id.OrderNumber,
             id.ExternalCompanyId,
             ec.ExternalCompanyCode,
             ec.ExternalCompany,
             s.Status,
             idt.InboundDocumentType,
             idt.InboundDocumentTypeCode,
             id.DeliveryDate,
             id.CreateDate,
             id.InboundDocumentTypeId,
             id.ReferenceNumber,
             id.ReasonId,
             id.Remarks,
             idc.ContactListId,
             cl.ContactPerson,
             p.PrincipalId,
             p.Principal
        from InboundDocument     id  (nolock)
        join Status              s   (nolock) on id.StatusId          = s.StatusId
        join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
        join ExternalCompany     ec  (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
        left join InboundDocumentContactList idc (nolock) on idc.InboundDocumentId = @InboundDocumentId
        left join ContactList    cl  (nolock) on cl.ContactListId = idc.ContactListId
        left join Principal              p (nolock) on id.PrincipalId = p.PrincipalId
       where id.InboundDocumentId        = isnull(@InboundDocumentId, id.InboundDocumentId)
         and id.InboundDocumentTypeId    = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
         and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
         and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
         and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
         and id.DeliveryDate       between isnull(@FromDate, id.DeliveryDate) and isnull(@ToDate, id.DeliveryDate)
         and s.StatusId                  = @StatusId
         and id.WarehouseId              = @WarehouseId
         and id.WarehouseId               = isnull(@WarehouseId, id.WarehouseId)
      union
      select id.InboundDocumentId,
             id.OrderNumber,
             id.ExternalCompanyId,
             null,
             null,
             s.Status,
             idt.InboundDocumentType,
             idt.InboundDocumentTypeCode,
             id.DeliveryDate,
             id.CreateDate,
             id.InboundDocumentTypeId,
             id.ReferenceNumber,
             id.ReasonId,
             id.Remarks,
             idc.ContactListId,
             cl.ContactPerson,
             p.PrincipalId,
             p.Principal
        from InboundDocument     id  (nolock)
        join Status              s   (nolock) on id.StatusId          = s.StatusId
        join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
        left join InboundDocumentContactList idc (nolock) on idc.InboundDocumentId = @InboundDocumentId
        left join ContactList    cl  (nolock) on cl.ContactListId = idc.ContactListId
        left join Principal              p (nolock) on id.PrincipalId = p.PrincipalId
       where id.InboundDocumentId        = isnull(@InboundDocumentId, id.InboundDocumentId)
         and id.InboundDocumentTypeId    = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
         and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
         and id.DeliveryDate       between isnull(@FromDate, id.DeliveryDate) and isnull(@ToDate, id.DeliveryDate)
         and s.StatusId                  = @StatusId
         and id.WarehouseId              = @WarehouseId
         and id.WarehouseId               = isnull(@WarehouseId, id.WarehouseId)
         and not exists(select 1 from ExternalCompany ec  (nolock) where id.ExternalCompanyId = ec.ExternalCompanyId)
    end
  end
  else
  begin
    insert @TableResult
          (InboundDocumentId,
           OrderNumber,
           ExternalCompanyId,
           ExternalCompanyCode,
           ExternalCompany,
           Status,
           InboundDocumentType,
           InboundDocumentTypeCode,
           DeliveryDate,
           CreateDate,
           InboundDocumentTypeId,
           ReferenceNumber,
           ReasonId,
           Remarks,
           ContactListId,
           ContactPerson,
           PrincipalId,
           Principal)
    select id.InboundDocumentId,
           id.OrderNumber,
           id.ExternalCompanyId,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           s.Status,
           idt.InboundDocumentType,
           idt.InboundDocumentTypeCode,
           id.DeliveryDate,
           id.CreateDate,
           id.InboundDocumentTypeId,
           id.ReferenceNumber,
           id.ReasonId,
           id.Remarks,
           idc.ContactListId,
           cl.ContactPerson,
           p.PrincipalId,
           p.Principal
      from InboundDocument     id  (nolock)
      join Status              s   (nolock) on id.StatusId          = s.StatusId
      join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
      left
      join ExternalCompany     ec  (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
      left join InboundDocumentContactList idc (nolock) on idc.InboundDocumentId = @InboundDocumentId
      left join ContactList    cl  (nolock) on cl.ContactListId = idc.ContactListId
      left join Principal              p (nolock) on id.PrincipalId = p.PrincipalId
     where id.InboundDocumentId        = @InboundDocumentId
  end
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from InboundLine il
                           where t.InboundDocumentId = il.InboundDocumentId)
    from @TableResult t
   where InboundDocumentTypeCode = 'RET'
  
  update tr
     set Reason = r.Reason
    from @TableResult tr
    join Reason        r (nolock) on tr.ReasonId = r.ReasonId
  
 
  select InboundDocumentId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         NumberOfLines,
         DeliveryDate,
         Status,
         InboundDocumentType,
         InboundDocumentTypeId,
         ExternalCompanyId,
         ReferenceNumber,
         Remarks,
         isnull(ReasonId, -1) as 'ReasonId',
         Reason,
         ContactListId,
         ContactPerson,
         PrincipalId,
         Principal
    from @TableResult
end
