﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceBOM_Insert
  ///   Filename       : p_InterfaceBOM_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:45
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceBOM table.
  /// </remarks>
  /// <param>
  ///   @InterfaceBOMId int = null output,
  ///   @ParentProductCode nvarchar(100) = null,
  ///   @OrderDescription nvarchar(510) = null,
  ///   @CreateDate nvarchar(100) = null,
  ///   @PlannedDate nvarchar(100) = null,
  ///   @Revision nvarchar(100) = null,
  ///   @BuildQuantity float = null,
  ///   @Units nvarchar(100) = null,
  ///   @Location nvarchar(100) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null 
  /// </param>
  /// <returns>
  ///   InterfaceBOM.InterfaceBOMId,
  ///   InterfaceBOM.ParentProductCode,
  ///   InterfaceBOM.OrderDescription,
  ///   InterfaceBOM.CreateDate,
  ///   InterfaceBOM.PlannedDate,
  ///   InterfaceBOM.Revision,
  ///   InterfaceBOM.BuildQuantity,
  ///   InterfaceBOM.Units,
  ///   InterfaceBOM.Location,
  ///   InterfaceBOM.ProcessedDate,
  ///   InterfaceBOM.RecordStatus 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceBOM_Insert
(
 @InterfaceBOMId int = null output,
 @ParentProductCode nvarchar(100) = null,
 @OrderDescription nvarchar(510) = null,
 @CreateDate nvarchar(100) = null,
 @PlannedDate nvarchar(100) = null,
 @Revision nvarchar(100) = null,
 @BuildQuantity float = null,
 @Units nvarchar(100) = null,
 @Location nvarchar(100) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceBOMId = '-1'
    set @InterfaceBOMId = null;
  
	 declare @Error int
 
  insert InterfaceBOM
        (ParentProductCode,
         OrderDescription,
         CreateDate,
         PlannedDate,
         Revision,
         BuildQuantity,
         Units,
         Location,
         ProcessedDate,
         RecordStatus)
  select @ParentProductCode,
         @OrderDescription,
         @CreateDate,
         @PlannedDate,
         @Revision,
         @BuildQuantity,
         @Units,
         @Location,
         @ProcessedDate,
         @RecordStatus 
  
  select @Error = @@Error, @InterfaceBOMId = scope_identity()
  
  
  return @Error
  
end
