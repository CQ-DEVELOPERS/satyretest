﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportORD_Insert
  ///   Filename       : p_InterfaceImportORD_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:48
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportORD table.
  /// </remarks>
  /// <param>
  ///   @RecordStatus nvarchar(40) = null,
  ///   @InsertDate datetime = null,
  ///   @RecordType nvarchar(40) = null,
  ///   @ID nvarchar(40) = null,
  ///   @Ordernumber nvarchar(100) = null,
  ///   @StatusId nvarchar(100) = null,
  ///   @OrderDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportORD.RecordStatus,
  ///   InterfaceImportORD.InsertDate,
  ///   InterfaceImportORD.RecordType,
  ///   InterfaceImportORD.ID,
  ///   InterfaceImportORD.Ordernumber,
  ///   InterfaceImportORD.StatusId,
  ///   InterfaceImportORD.OrderDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportORD_Insert
(
 @RecordStatus nvarchar(40) = null,
 @InsertDate datetime = null,
 @RecordType nvarchar(40) = null,
 @ID nvarchar(40) = null,
 @Ordernumber nvarchar(100) = null,
 @StatusId nvarchar(100) = null,
 @OrderDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportORD
        (RecordStatus,
         InsertDate,
         RecordType,
         ID,
         Ordernumber,
         StatusId,
         OrderDate)
  select @RecordStatus,
         @InsertDate,
         @RecordType,
         @ID,
         @Ordernumber,
         @StatusId,
         @OrderDate 
  
  select @Error = @@Error
  
  
  return @Error
  
end
