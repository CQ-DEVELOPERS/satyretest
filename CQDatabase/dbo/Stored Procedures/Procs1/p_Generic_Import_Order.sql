﻿
/*
/// <summary>
///   Procedure Name : p_Generic_Import
///   Filename       : p_Generic_Import.sql
///   Create By      : Grant Schultz
///   Date Created   : 10 Feb 2009
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
CREATE procedure [dbo].[p_Generic_Import_Order]
as
begin
  if exists(select 1
              from InterfaceImportProduct (nolock)
             where RecordStatus = 'N'
               and ProcessedDate is null)
  exec p_Generic_Import_Product
  
  insert StorageUnitArea
        (StorageUnitId,
         AreaId,
         StoreOrder,
         PickOrder)
  select su.StorageUnitId,
         ap.AreaId,
         1,
         1
    from AreaPrincipal ap (nolock)
    join Product        p (nolock) on ap.PrincipalId = p.PrincipalId
    join StorageUnit   su (nolock) on p.ProductId = su.ProductId
   where not exists(select top 1 1 from StorageUnitArea sua where su.StorageUnitId = sua.StorageUnitId)
  
  --update InterfaceImportHeader set Module = 'Inbound' where Module is null
  
  --insert WarehouseCodeReference
  --      (WarehouseCode,
           --       DownloadType,
           --       WarehouseId,
           --       InboundDocumentTypeId,
           --       OutboundDocumentTypeId,
           --       ToWarehouseId,
           --       ToWarehouseCode)
  --select distinct h.FromWarehouseCode,
  --       h.RecordType,
  --       1,
  --       idt.InboundDocumentTypeId,
  --       odt.OutboundDocumentTypeId,
  --       null,
  --       h.ToWarehouseCode
  --  from InterfaceImportHeader h (nolock)
  --  left join OutboundDocumentType odt (nolock) on h.RecordType = odt.OutboundDocumentTypeCode
  --  left join InboundDocumentType idt (nolock) on h.RecordType = idt.InboundDocumentTypeCode
  -- where h.InsertDate > '2012-02-21'
  --   and datalength(h.RecordType) = 8
  --   and not exists(select 1
                      --                    from WarehouseCodeReference wcr
                      --                   where wcr.DownloadType           = h.RecordType
                      --                     and wcr.WarehouseCode          = h.FromWarehouseCode
                      --                     and isnull(wcr.ToWarehouseCode,'')    = isnull(h.ToWarehouseCode,'')
  --                     and isnull(wcr.InboundDocumentTypeId,'')  = isnull(idt.InboundDocumentTypeId,'')
  --                     and isnull(wcr.OutboundDocumentTypeId,'') = isnull(odt.OutboundDocumentTypeId,''))
  -- order by h.RecordType
  
  update ExternalCompany set DropSequence = ExternalCompanyId where DropSequence is null
  
  -- InterfaceImportHeader
  declare @InterfaceImportHeaderId      int,
          @Object                       nvarchar(10),
          @DocEntry                     nvarchar(30),
          @OrderNumber                  nvarchar(30),
          @ExternalCompanyCode          nvarchar(30),
          @ExternalCompany              nvarchar(255),
          @Address                      nvarchar(100),
          @Remarks                      nvarchar(255),
          @HoldStatus                   nvarchar(255),
          @Route                        nvarchar(50),
          @DeliveryDate                 nvarchar(30),
          -- InterfaceImportDetail
          @LineNumber                   int,
          @ProductCode                  nvarchar(30),
          @Product                      nvarchar(50),
          @Quantity                     numeric(13,3),
          @TreeType                     char(1),
          @FromWarehouseCode            nvarchar(30),
          @ToWarehouseCode              nvarchar(30),
          -- Internal variables
          @InboundDocumentTypeId        int,
          @InboundDocumentId            int,
          @InboundLineId                int,
     @ReceiptLineId                int,
          @OutboundDocumentId int,
          @OutboundDocumentTypeId       int,
          @ExternalCompanyId            int ,
          @DropSequence                 int,
          @Weight                       numeric(13,3),
          @ErrorMsg                     nvarchar(100),
          @Error                        int,
          @StatusId                     int,
          @GetDate                      datetime,
          @ProductId                    int,
          @StorageUnitId                int,
          @StorageUnitBatchId           int,
          @BatchId                      int,
          @Batch                        nvarchar(30),
          @SkuCode                      nvarchar(50),
          @PalletQuantity               numeric(13,3),
          @UnitPrice                    numeric(13,3),
          @Exception                    nvarchar(255),
          @OutboundShipmentId           int,
          @IssueId                      int,
          @ReleaseDate                  datetime,
          @Days                         int,
          @FromWarehouseId              int,
          @ToWarehouseId                int,
          @InboundDocumentTypeCode      nvarchar(10),
          @InPriorityId                 int,
          @OutboundDocumentTypeCode     nvarchar(10),
          @OutPriorityId                int,
          @ODStatusId                   int,
          @OutboundLineId               int,
          @AutoRelease                  bit,
          @AddToShipment                bit,
          @MultipleOnShipment           bit,
          @HostStatus                   nvarchar(50),
          @FPCCode                      nvarchar(30),
          @FPCStorageUnitId             int,
          @FPCStorageUnitBatchId        int,
          @FPCBatchId                   int,
          @RouteId                      int,
          @RouteCode                    nvarchar(10),
          @rowcount                     int,
          @PrincipalCode                nvarchar(30),
          @PrincipalId                  int,
          @PreviousPrice				            numeric(13,2),
          @Additional1                  nvarchar(255),
          @Additional2                  nvarchar(255),
          @Additional3                  nvarchar(255),
          @BOELineNumber                nvarchar(50),
          @AlternativeSKU               nvarchar(100),
          @CountryofOrigin              nvarchar(100),
          @Height                       numeric(13,6),
          @Width                        numeric(13,6),
          @Length                       numeric(13,6),
          @TariffCode                   nvarchar(50),
          @Reference1                   nvarchar(50),
          @Reference2                   nvarchar(50),
          @ExpiryDate                   datetime,
          @SKU                          nvarchar(510),
          @ContainerNumber              nvarchar(30),
          @SealNumber                   nvarchar(30),
          @BillOfEntry                  nvarchar(50),
          @AdditionalText1              nvarchar(50),
          @AdditionalText2              nvarchar(50),
          @Incoterms                    nvarchar(50),
          @ReceiptId                    int,
          @Module                       nvarchar(50),
          @BatchReferenceNumber         nvarchar(50),
          @DeliveryNoteNumber           nvarchar(50),
          @AddressLine1                 nvarchar(50),
          @AddressLine2                 nvarchar(50),
          @AddressLine3                 nvarchar(50),
          @AddressLine4                 nvarchar(50),
          @AddressLine5                 nvarchar(50),
          @ExternalCompanyTypeId        int
  
  select @GetDate = dbo.ufn_Getdate()
         
  if(object_id('tempdb.dbo.##p_Generic_Import') is null)
  create table ##p_Generic_Import (objname sysname)
  else
  begin
    select 'RETURN'
    RETURN
  end
  
  update d
     set InterfaceImportHeaderId = (select max(InterfaceImportHeaderId)
    from InterfaceImportHeader h
   where d.ForeignKey = h.PrimaryKey)
    from InterfaceImportDetail d
   where InterfaceImportHeaderId is null
  
     set @Days = 0
       
     set @ReleaseDate = @GetDate/*convert(datetime, convert(nvarchar(10), @GetDate, 120))*/
     set @ReleaseDate = dateadd(hh, 12, @ReleaseDate)
  
  if @Getdate <= @ReleaseDate
     set @ReleaseDate = dateadd(hh, 48, @ReleaseDate)
         else
     set @ReleaseDate = dateadd(hh, 72, @ReleaseDate)
         
  if datename(dw, @ReleaseDate) = 'Saturday'
     set @Days = 2
         
  if datename(dw, @ReleaseDate) = 'Sunday'
     set @Days = 2
         
  select @ReleaseDate = dateadd(dd, @Days, @ReleaseDate)
         
  -- Reprocess Order which did not process properly
  update InterfaceImportHeader
     set ProcessedDate = null
   where RecordStatus     = 'N'
     and ProcessedDate   <= dateadd(mi, -10, @Getdate)
  
  update h
     set ProcessedDate = @Getdate
         --,DeliveryDate = CASE WHEN LEN(ISNULL(DeliveryDate,'')) = 0 THEN @GetDate ELSE DeliveryDate END
    from InterfaceImportHeader h
   where --not exists(select 1 from viewOrderNumber  vw where h.OrderNumber = vw.OrderNumber)
		       ProcessedDate is null
     and (exists(select 1 from InterfaceImportDetail d where h.InterfaceImportHeaderId = d.InterfaceImportHeaderId)
     and h.RecordStatus in ('N','U', 'D')
      or h.HostStatus in ('D'))
      
-- Add records to delete Purchase Orders
update h
     set ProcessedDate = @Getdate,
         RecordType = 'ZZZ'
    from InterfaceImportHeader h
   where ProcessedDate is null
     and h.RecordStatus in ('N','U','D')
     and InterfaceImportHeaderId not in(select InterfaceImportHeaderId from InterfaceImportDetail)
  
  --Change for Magic
  --update InterfaceImportDetail
  --   Set Quantity = Convert(float, LTRIM(Additional1))
  --      ,Additional1 = null
  -- Where Additional1 Is Not Null
  
  SELECT 'BEFORE HEADER CURSOR'
  
  declare header_cursor                 cursor for
  select InterfaceImportHeaderId,
         Module,
         RecordType,                       -- Object
         PrimaryKey,                       -- DocEntry
         OrderNumber,                      -- DocNum
         CompanyCode,      -- CardCode
         Company,                          -- CardName
         Address,                          -- Address
         Remarks,       -- PickRmrk
         FromWarehouseCode,                -- Filler
         ToWarehouseCode,                  -- U_ZLGNUL
         HostStatus,
         Route,
         isnull(rtrim(ltrim(DeliveryDate)), @getdate),
         --null,
         PrincipalCode,
         Additional1,
         Additional2,
         Additional3,
         ContainerNumber,
         SealNumber,
         BillOfEntry,
         Reference1,
         Reference2,
         Incoterms,
         DeliveryNoteNumber,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10
    from InterfaceImportHeader
   where ProcessedDate = @Getdate
     and RecordStatus = 'N'
  order by OrderNumber
  
  headercursor:
  
  open header_cursor
  
  --begin try
  fetch header_cursor
    into @InterfaceImportHeaderId,
         @Module,
         @Object,
         @DocEntry,
         @OrderNumber,
         @ExternalCompanyCode,
         @ExternalCompany,
         @Address,
         @Remarks,
         @FromWarehouseCode,
         @ToWarehouseCode,
         @HostStatus,
         @Route,
         @DeliveryDate,
         @PrincipalCode,
         @Additional1,
         @Additional2,
         @Additional3,
         @ContainerNumber,
         @SealNumber,
         @BillOfEntry,
         @AdditionalText1,
         @AdditionalText2,
         @Incoterms,
         @DeliveryNoteNumber,
      @AddressLine1,
         @AddressLine2,
         @AddressLine3,
         @AddressLine4,
      @AddressLine5
         --end try
    --begin catch
  --  update InterfaceImportHeader
  --     set RecordStatus = 'E',
  --         ProcessedDate = @GetDate,
  --         ErrorMsg = @ErrorMsg
  --   where InterfaceImportHeaderId = @InterfaceImportHeaderId
  
  --  close header_cursor
  --  goto headercursor
  --end catch
  
  If len(@ExternalCompanyCode) = 0
     Set @ExternalCompanyCode = null
         
  If len(@ExternalCompany) = 0
     Set @ExternalCompany = null
         
  If len(@Address) = 0
     Set @Address = null
   
  If len(@ToWarehouseCode) = 0
     Set @ToWarehouseCode = null
         
  If len(@FromWarehouseCode) = 0
     Set @FromWarehouseCode = null
         
  while (@@fetch_status = 0)
  begin
    begin transaction xml_import
    
    
    SELECT 'IN CURSOR'
    
    update InterfaceImportHeader
       set RecordStatus = 'C',
           ProcessedDate = @GetDate
     where InterfaceImportHeaderId = @InterfaceImportHeaderId
   
       set @PrincipalId = null
           
    select @PrincipalId = PrincipalId
      from Principal (nolock)
     where PrincipalCode = @PrincipalCode
    
    if isnumeric(@Additional3) = 1
    begin
      set @OutPriorityId = null
      select @OutPriorityId = PriorityId
        from Priority (nolock)
       where convert(nvarchar(255), PriorityId) = @Additional3
    end
    
    if isdate(@DeliveryDate) <> 1
    --  select @DeliveryDate = convert(nvarchar(20), @DeliveryDate)
    --else
      select @DeliveryDate = convert(nvarchar(10), @getdate, 120)
    
    If len(@DeliveryDate) = 0
       Set @DeliveryDate = null
           --exec @Error = p_Interface_Outbound_Order_Delete
           --@OrderNumber = @OrderNumber
           
           --if @Error != 0
           --begin
    --  select @ErrorMsg = 'Error executing p_Interface_Outbound_Order_Delete'
    --  goto error_header
    --end
    
    if @Object = 'ZZZ'
    Begin
       exec @Error = p_Inbound_Order_Delete
	   @OrderNumber = @OrderNumber
       if @Error != 0
       begin
          select @ErrorMsg = 'Error executing p_Inbound_Order_Delete'
          goto error_header
       end
       goto error_header
    End
    
    
     if @InboundDocumentTypeId is not null
     BEGIN
		exec @Error = p_Inbound_Order_Delete
		@OrderNumber = @OrderNumber
	    
		if @Error != 0
		begin
		  select @ErrorMsg = 'Error executing p_Inbound_Order_Delete'
		  goto error_header
		end
    END
    
    --update h
    --   set FromWarehouseCode = d.Additional1
    --  from InterfaceImportHeader h
    --  join InterfaceImportDetail d on h.InterfaceImportHeaderId = d.InterfaceImportHeaderId
    -- where h.InterfaceImportHeaderId = @InterfaceImportHeaderId
    --   and h.FromWarehouseCode is null
    --   and d.Additional1 is not null
    
    --if @@rowcount != 0
    
    --Set Object to OutboundDocType based on syspro user field
    Select @Object = ODT.OutboundDocumentTypeCode
      From InterfaceImportHeader H
           Inner Join OutboundDocumentType ODT ON H.Additional4 = ODT.OutboundDocumentType
     Where InterfaceImportHeaderId = @InterfaceImportHeaderId
     
     

    
    --select @FromWarehouseCode = FromWarehouseCode
    --  from InterfaceImportHeader
    -- where InterfaceImportHeaderId = @InterfaceImportHeaderId
    
       set @InboundDocumentTypeId = null
       set @OutboundDocumentTypeId = null
       set @InboundDocumentTypeCode = null
       set @OutboundDocumentTypeCode = null
       set @FromWarehouseId = null
       set @ToWarehouseId = null
       
       select @Object as 'Object',@FromWarehouseCode as '@FromWarehouseCode',@ToWarehouseCode as ToWarehouseCode
           
    select @InboundDocumentTypeId  = InboundDocumentTypeId,
           @OutboundDocumentTypeId = OutboundDocumentTypeId,
           @FromWarehouseId    = WarehouseId,
           @ToWarehouseId          = ToWarehouseId
      from WarehouseCodeReference
     where DownloadType  = @Object
       and WarehouseCode = @FromWarehouseCode
       and isnull(ToWarehouseCode,-1) = isnull(@ToWarehouseCode,-1)
       
       
       SELECT @InboundDocumentTypeId as 'InboundDocumentTypeId',@OutboundDocumentTypeId as '@OutboundDocumentTypeId',
              @FromWarehouseId as 'FromWarehouseId',@ToWarehouseId as 'ToWarehouseId'
    
    if @@rowcount = 0
      select @InboundDocumentTypeId  = InboundDocumentTypeId,
             @OutboundDocumentTypeId = OutboundDocumentTypeId,
             @FromWarehouseId        = WarehouseId,
             @ToWarehouseId          = ToWarehouseId
        from WarehouseCodeReference
       where DownloadType  = @Object
         and WarehouseCode = @FromWarehouseCode
         and isnull(ToWarehouseCode, '') = ''
    
    if @@rowcount = 0
    begin
      select @InboundDocumentTypeId  = InboundDocumentTypeId,
       @OutboundDocumentTypeId = OutboundDocumentTypeId,
             @FromWarehouseId        = WarehouseId,
             @ToWarehouseId          = ToWarehouseId
        from WarehouseCodeReference
       where DownloadType  = @Object
         --and isnull(WarehouseCode,'') = ''
         and ToWarehouseCode = @ToWarehouseCode
    end
    
    if @Object = '21'
    begin
      select @InboundDocumentTypeId  = null,
             @OutboundDocumentTypeId = null
 end
  
    if @Object in ('CR','CRCR')
    begin
      select @FromWarehouseId = 1
             
      select @InboundDocumentTypeId  = InboundDocumentTypeId
        from InboundDocumentType
       where InboundDocumentTypeCode in ('CRCR','CR')
    end
    
    if @OutboundDocumentTypeId is not null
    exec @Error = p_Interface_Outbound_Order_Delete
    @OrderNumber = @OrderNumber,
    @PrincipalId = @PrincipalId
    
    select @Error
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Interface_Outbound_Order_Delete'
      goto error_header
    end
    
    if @InboundDocumentTypeId is not null
    exec @Error = p_Inbound_Order_Delete
    @OrderNumber = @OrderNumber,
    @PrincipalId = @PrincipalId
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Inbound_Order_Delete'
      goto error_header
    end
    
    if @HostStatus = 'D' OR @Additional1 = 'X' -- Just delete order and move on.
    goto error_header
    
    --select @InboundDocumentTypeId  as '@InboundDocumentTypeId' ,
    --       @OutboundDocumentTypeId as '@OutboundDocumentTypeId',
    --       @FromWarehouseId        as '@FromWarehouseId       ',
    --       @ToWarehouseId          as '@ToWarehouseId         ',
    --       @FromWarehouseCode      as '@FromWarehouseCode',
    --       @ToWarehouseCode        as '@ToWarehouseCode',
    --       @OrderNumber            as '@OrderNumber',
    --       @ExternalCompanyCode    as '@ExternalCompanyCode',
    --       @ExternalCompany        as '@ExternalCompany',
    --       @Object                 as '@Object'


    
    if not exists(select 1 from Warehouse where WarehouseId in (@FromWarehouseId,@ToWarehouseId))
    begin
      select @ErrorMsg = 'Error executing p_Generic_Import - Warehouse does not exist'
      goto error_header
    end
    
    SELECT @InboundDocumentTypeId as 'InboundDocumentTypeId'
    
    if @InboundDocumentTypeId is not null and @OutboundDocumentTypeId is not null
    begin
      if @ToWarehouseId is not null
      begin
        select @InboundDocumentTypeCode = InboundDocumentTypeCode,
               @InPriorityId            = PriorityId
          from InboundDocumentType
         where InboundDocumentTypeId = @InboundDocumentTypeId
      end
    end
    
    if @InboundDocumentTypeId is not null and @OutboundDocumentTypeId is null
    begin
      if @ToWarehouseId is null
      select @ToWarehouseId = @FromWarehouseId
             
      select @InboundDocumentTypeCode = InboundDocumentTypeCode,
             @InPriorityId            = PriorityId
        from InboundDocumentType
       where InboundDocumentTypeId = @InboundDocumentTypeId
    end
    
    select @InboundDocumentTypeId  as '@InboundDocumentTypeId' ,
           @OutboundDocumentTypeId as '@OutboundDocumentTypeId',
           @FromWarehouseId        as '@FromWarehouseId       ',
           @ToWarehouseId as '@ToWarehouseId         ',
           @FromWarehouseCode      as '@FromWarehouseCode',
           @ToWarehouseCode        as '@ToWarehouseCode',
  @OrderNumber            as '@OrderNumber',
           @ExternalCompanyCode    as '@ExternalCompanyCode',
           @ExternalCompany        as '@ExternalCompany',
           @Object                 as '@Object'
    
    if @ExternalCompanyCode is null and @ExternalCompany is null and @Object != '67'
    begin
         set @ExternalCompanyCode = @FromWarehouseCode
    end
    
    if isnull(@Route,'') != ''
    begin
         set @RouteId = null
             
      select @RouteId = RouteId
    from Route
       where Route = @Route
      
      if @ROuteId is null
      begin
        select @RouteCode = substring(@Route, 1, 10)
               
               exec @Error = p_Route_Insert
          @ROuteId   = @RouteId output,
               @Route     = @Route,
               @RouteCode = @RouteCode
               
               select @RouteId, @Route
               
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Route_Insert'
  goto error_header
        end
      end
    end
    
    if @OutboundDocumentTypeId is not null
    begin
      select @OutboundDocumentTypeCode = OutboundDocumentTypeCode,
             @OutPriorityId      = isnull(@OutPriorityId, PriorityId),
             @AutoRelease              = AutoRelease,
             @AddToShipment    = AddToShipment,
             @MultipleOnShipment       = MultipleOnShipment
        from OutboundDocumentType
       where OutboundDocumentTypeId = @OutboundDocumentTypeId
      
      set @ExternalCompanyId = null;
      
      select @ExternalCompanyTypeId = ExternalCompanyTypeId
        from ExternalCompanyType
       where ExternalCompanyTypeCode = 'CUST'
      
      if @ExternalCompanyCode is not null
      begin
        select @ExternalCompanyId   = ExternalCompanyId,
               @ExternalCompanyCode = ExternalCompanyCode,
               @ExternalCompany     = ExternalCompany
          from ExternalCompany (nolock)
         where ExternalCompanyCode   = @ExternalCompanyCode
           and PrincipalId           = @PrincipalId
           and ExternalCompanyTypeId = @ExternalCompanyTypeId
        
        if @ExternalCompanyId is null
          select @ExternalCompanyId   = ExternalCompanyId,
                 @ExternalCompanyCode = ExternalCompanyCode,
                 @ExternalCompany     = ExternalCompany
            from ExternalCompany (nolock)
           where ExternalCompanyCode   = @ExternalCompanyCode
             and PrincipalId          is null
             and ExternalCompanyTypeId = @ExternalCompanyTypeId
        
        if @ExternalCompanyId is null
        begin
          if @ExternalCompany is null
             set @ExternalCompany = @ExternalCompanyCode
          
          exec @Error = p_ExternalCompany_Insert
           @ExternalCompanyId         = @ExternalCompanyId output,
           @ExternalCompanyTypeId     = @ExternalCompanyTypeId,
           @RouteId                   = @RouteId,
           @ExternalCompany           = @ExternalCompany,
           @ExternalCompanyCode       = @ExternalCompanyCode,
           @PrincipalId               = @PrincipalId
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
            goto error_header
          end
        end
        else
        begin
          exec @Error = p_ExternalCompany_Update
          @ExternalCompanyId         = @ExternalCompanyId,
          @RouteId                   = @RouteId
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
            goto error_header
          end
        end
      end
      else if @OutboundDocumentTypeCode = 'IBT'
      begin
        if @ToWarehouseCode is null
        begin
          select @ErrorMsg = 'Error executing p_Generic_Import - To Warehouse is null for IBT'
          goto error_header
        end
        
        select @ExternalCompanyId   = ExternalCompanyId,
               @ExternalCompanyCode = ExternalCompanyCode,
               @ExternalCompany     = ExternalCompany
          from ExternalCompany (nolock)
         where ExternalCompanyCode   = substring(@ToWarehouseCode, 1, 4)
           and PrincipalId           = @PrincipalId
           and ExternalCompanyTypeId = @ExternalCompanyTypeId
        
        if @ExternalCompanyId is null
          select @ExternalCompanyId   = ExternalCompanyId,
                 @ExternalCompanyCode = ExternalCompanyCode,
                 @ExternalCompany     = ExternalCompany
            from ExternalCompany (nolock)
           where ExternalCompanyCode   = substring(@ToWarehouseCode, 1, 4)
             and PrincipalId          is null
             and ExternalCompanyTypeId = @ExternalCompanyTypeId
        
        if @ExternalCompanyId is null
        begin
          set @ExternalCompanyCode = substring(@ToWarehouseCode, 1, 4)
                 
          if @ExternalCompany is null
             set @ExternalCompany = @ExternalCompanyCode
          
          exec @Error = p_ExternalCompany_Insert
           @ExternalCompanyId         = @ExternalCompanyId output,
           @ExternalCompanyTypeId     = @ExternalCompanyTypeId,
           @RouteId                   = @RouteId,
           @ExternalCompany           = @ExternalCompanyCode,
           @ExternalCompanyCode       = @ExternalCompanyCode,
           @PrincipalId               = @PrincipalId
                 
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
            goto error_header
          end
        end
        else
        begin
          exec @Error = p_ExternalCompany_Update
          @ExternalCompanyId         = @ExternalCompanyId,
          @RouteId                   = @RouteId
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
            goto error_header
          end
        end
      end
      
      select @ODStatusId = dbo.ufn_StatusId('OD','I')
      
      exec @Error = p_OutboundDocument_Insert
       @OutboundDocumentId     = @OutboundDocumentId output,
       @OutboundDocumentTypeId = @OutboundDocumentTypeId,
       @ExternalCompanyId      = @ExternalCompanyId,
       @StatusId               = @ODStatusId,
       @WarehouseId            = @FromWarehouseId,
       @OrderNumber            = @OrderNumber,
       @DeliveryDate           = @DeliveryDate,
       @CreateDate             = @GetDate,
       @ModifiedDate           = null,
       @PrincipalId            = @PrincipalId,
       @AdditionalText1        = @AdditionalText1,
       @AdditionalText2        = @AdditionalText2,
       @BOE                    = @BillOfEntry,
       @Address1               = @Address
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_OutboundDocument_Insert'
        goto error_header
      end
    end
    
     SELECT @InboundDocumentTypeCode as 'InboundDocumentTypeCode'
    
    if @InboundDocumentTypeCode is not null
    begin
      set @ExternalCompanyId = null;
      
      select @ExternalCompanyTypeId = ExternalCompanyTypeId
        from ExternalCompanyType
       where ExternalCompanyTypeCode = 'SUPP'
      
      if @Object = 'PO'
      begin
        if isnull(@Additional1,'') != '' and isnull(@Additional2,'') != ''
        begin
          select @ExternalCompanyCode = @Additional1
          select @ExternalCompany     = @Additional2
        end
      end
      
      SELECT @ExternalCompanyCode as 'ExternalCompanyCode'
      
      if @ExternalCompanyCode is not null
      begin
        select @ExternalCompanyId   = ExternalCompanyId,
               @ExternalCompanyCode = ExternalCompanyCode,
               @ExternalCompany     = ExternalCompany
          from ExternalCompany (nolock)
         where ExternalCompanyCode = @ExternalCompanyCode
           and PrincipalId         = @PrincipalId
           and ExternalCompanyTypeId = @ExternalCompanyTypeId
        
        if @ExternalCompanyId is null
          select @ExternalCompanyId   = ExternalCompanyId,
                 @ExternalCompanyCode = ExternalCompanyCode,
                 @ExternalCompany     = ExternalCompany
            from ExternalCompany (nolock)
           where ExternalCompanyCode   = @ExternalCompanyCode
             and PrincipalId          is null
             and ExternalCompanyTypeId = @ExternalCompanyTypeId
        
        if @ExternalCompanyId is null
        begin
          if @ExternalCompany is null
            set @ExternalCompany = @ExternalCompanyCode
          
          exec @Error = p_ExternalCompany_Insert
          @ExternalCompanyId         = @ExternalCompanyId output,
          @ExternalCompanyTypeId     = @ExternalCompanyTypeId,
          @RouteId                   = @RouteId,
          @ExternalCompany           = @ExternalCompany,
          @ExternalCompanyCode       = @ExternalCompanyCode,
          @PrincipalId               = @PrincipalId
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
            goto error_header
          end
        end
        else
        begin
          exec @Error = p_ExternalCompany_Update
          @ExternalCompanyId         = @ExternalCompanyId,
          @RouteId                   = @RouteId
      
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
            goto error_header
          end
        end
      end
      else
      begin
        select @ExternalCompanyId   = ExternalCompanyId,
               @ExternalCompanyCode = ExternalCompanyCode,
               @ExternalCompany     = ExternalCompany
          from ExternalCompany (nolock)
         where ExternalCompanyCode   = isnull(@FromWarehouseCode, @ToWarehouseCode)
           and PrincipalId           = @PrincipalId
           and ExternalCompanyTypeId = @ExternalCompanyTypeId
        
        if @ExternalCompanyId is null
          select @ExternalCompanyId   = ExternalCompanyId,
                 @ExternalCompanyCode = ExternalCompanyCode,
                 @ExternalCompany     = ExternalCompany
            from ExternalCompany (nolock)
           where ExternalCompanyCode   = isnull(@FromWarehouseCode, @ToWarehouseCode)
             and PrincipalId          is null
             and ExternalCompanyTypeId = @ExternalCompanyTypeId
        
        if @ExternalCompanyId is null
        begin
          set @ExternalCompanyCode = isnull(@FromWarehouseCode, @ToWarehouseCode)
          
          exec @Error = p_ExternalCompany_Insert
           @ExternalCompanyId         = @ExternalCompanyId output,
           @ExternalCompanyTypeId     = @ExternalCompanyTypeId,
           @RouteId                   = @RouteId,
           @ExternalCompany           = @ExternalCompanyCode,
           @ExternalCompanyCode       = @ExternalCompanyCode,
           @PrincipalId               = @PrincipalId
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
            goto error_header
          end
        end
        else
        begin
          exec @Error = p_ExternalCompany_Update
          @ExternalCompanyId         = @ExternalCompanyId,
          @RouteId                   = @RouteId
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
            goto error_header
          end
        end
      end
      
      --if convert(nvarchar(10), @DeliveryDate, 120) < convert(nvarchar(10), @GetDate, 120)
      --begin
      --  set @Error = -1
      --  select @ErrorMsg = 'Error Importing order, delivery date cannot be in the past'
      --  goto error_header
      --end
      
      select @StatusId = dbo.ufn_StatusId('ID','W')
      
      
      SELECT 'BEFORE p_InboundDocument_Insert'
      
      exec @Error = p_InboundDocument_Insert
       @InboundDocumentId     = @InboundDocumentId output,
       @InboundDocumentTypeId = @InboundDocumentTypeId,
       @ExternalCompanyId     = @ExternalCompanyId,
       @StatusId              = @StatusId,
       @WarehouseId           = @ToWarehouseId,
       @OrderNumber           = @OrderNumber,
       @DeliveryDate          = @DeliveryDate,
       @CreateDate            = @GetDate,
       @ModifiedDate          = null,
       @Remarks				           = @Remarks,
       @PrincipalId           = @PrincipalId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_InboundDocument_Insert'
        goto error_header
      end
    end
    
    declare detail_cursor                 cursor for
    select ForeignKey,  -- DocEntry
           ISNULL(LineNumber, ROW_NUMBER() OVER (Order By ProductCode)),  -- LineNum
           ProductCode, -- ItemCode
           Product,     -- Dscription
           SkuCode,     -- SKUCode
           Sku,         -- SKU
           Batch,       -- Batch
           Quantity,    -- Quantity
           NetPrice,    -- NetPrice
           Additional2, -- TreeType
           Additional1, -- FPC Code
           BOELineNumber,
           AlternativeSKU,
           CountryofOrigin,
           Height,
           Width,
           Length,
           TariffCode,
           Reference1,
           Reference2,
           ExpiryDate
           
      from InterfaceImportDetail
     where InterfaceImportHeaderId = @InterfaceImportHeaderId
       and substring(ProductCode, 1, 3) not in ('SZA','XZA','DZA')
       and Quantity > 0
    
  open detail_cursor
    
    fetch detail_cursor
      into @DocEntry,
           @LineNumber,
           @ProductCode,
           @Product,
           @SkuCode,
           @Sku,
           @Batch,
           @Quantity,
           @UnitPrice,
           @TreeType,
           @FPCCode,
           @BOELineNumber,
           @AlternativeSKU,
           @CountryofOrigin,
           @Height,
           @Width,
           @Length,
           @TariffCode,
           @Reference1,
           @Reference2,
           @ExpiryDate
           
    while (@@fetch_status = 0)
    begin
    select @@fetch_status as '@@fetch_status'
      if isnull(@TreeType,'') != 'S'
      begin
           set @StorageUnitBatchId = null
           set @StorageUnitId = null
               --set @Batch = null
               
               
               --Select @DocEntry, @LineNumber, @ProductCode,  @Product, @Quantity,   @TreeType
               
        if left(@InboundDocumentTypeCode,2) = 'CR'
           set @Batch = convert(nvarchar(30), @OrderNumber)
               
        if @OutboundDocumentTypeCode = 'BOND' or @InboundDocumentTypeCode = 'BOND'
        begin
           set @BatchReferenceNumber = @Batch
           set @Batch = @Batch + isnull(@BillOfEntry, '') + isnull(@BOELineNumber, '')
        end
        
        select @PrincipalId as '@PrincipalId', @Batch as 'Batch'
        
        select @StorageUnitId      = su.StorageUnitId,
               @StorageUnitBatchId = sub.StorageUnitBatchId,
               @BatchId			         = sub.BatchId
          from Product            p (nolock)
          join StorageUnit       su (nolock) on p.ProductId      = su.ProductId
          join SKU                  (nolock) on sku.SKUId        = su.SKUId
          join StorageUnitBatch sub (nolock) on su.StorageUnitId = sub.StorageUnitId
          join Batch              b (nolock) on sub.BatchId      = b.BatchId
         where p.ProductCode = @ProductCode
           and sku.SKUCode   = isnull(@SkuCode,sku.SKUCode)
           and (p.PrincipalId = @PrincipalId or @PrincipalId is null)
          --and b.Batch = 'Default'  -Pratik
          and b.Batch = @Batch
          
          
           --Pratik V Commented
           --and isnull(b.BillOfEntry, '-1') = isnull(@BillOfEntry, isnull(b.BillOfEntry, '-1'))
          -- and isnull(b.BOELineNumber, '-1') = isnull(@BOELineNumber, isnull(b.BOELineNumber, '-1'))
           
           select @StorageUnitBatchId as '@StorageUnitBatchId',@BatchId as '@BatchId'
        
        /*removed to prevent adding new products */
        if (select dbo.ufn_Configuration(428, @FromWarehouseId)) = 1 and (@StorageUnitBatchId is null or @Batch != '')
        begin
		  set @StorageUnitId      = null
		  set @StorageUnitBatchId = null
		  set @BatchId			= null
		        
          exec @Error = p_interface_xml_Product_Insert
           @ProductCode        = @ProductCode,
           @Product            = @Product,
           @SKUCode            = @SkuCode,
           @SKU                = @SkuCode,
           @Batch              = @Batch,
           @WarehouseId        = @FromWarehouseId,
           @StorageUnitId      = @StorageUnitId output,
           @StorageUnitBatchId = @StorageUnitBatchId output,
           @BatchId            = @BatchId output,
           @ExternalCompanyId  = @ExternalCompanyId,
           @PrincipalId		   = @PrincipalId,
           @BillOfEntry        = @BillOfEntry,
           @BOELineNumber      = @BOELineNumber,
           @BatchReferenceNumber = @BatchReferenceNumber
        end
        
	       if @Error != 0 or @StorageUnitBatchId is null
	       begin
		        set @Error = -1
		      select @ErrorMsg = 'Error: could not find product details for PLU ' + isnull(@ProductCode,'') + ', SKU ' + isnull(@SkuCode,'')
		        goto error_detail
	       end
	       
        if isnull(@FPCCode,'') != ''
        begin
          set @FPCStorageUnitId = null
          set @FPCStorageUnitBatchId = null
          set @FPCBatchId = null
          
          exec @Error = p_interface_xml_Product_Insert
           @ProductCode        = @FPCCode,
 @Product            = @Product,
           @SKUCode            = @SkuCode,
 @SKU    = @SkuCode,
           @Batch              = @Batch,
           @WarehouseId        = @FromWarehouseId,
           @StorageUnitId      = @FPCStorageUnitId output,
           @StorageUnitBatchId = @FPCStorageUnitBatchId output,
           @BatchId            = @FPCBatchId output,
           @ExternalCompanyId  = @ExternalCompanyId,
           @PrincipalId		      = @PrincipalId,
           @Weight             = @Weight,
           @Height             = @Height,
           @Width              = @Width,
           @Length             = @Length
          
          if @Error != 0 or @StorageUnitBatchId is null
          begin
            select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
            goto error_detail
          end
        END
        
        --IF ISNULL(@UnitPrice,0) <> 0
        --begin
        --  select @PreviousPrice = UnitPrice
		      --    from StorageUnit
		      --   where StorageUnitId = @StorageUnitId       
              
        --  EXEC @Error = p_StorageUnit_Update
		      --   @StorageUnitId		= @StorageUnitId
		      --  ,@UnitPrice			= @UnitPrice
		      --  ,@PreviousUnitPrice = @PreviousPrice
        --end
        
        if @OutboundDocumentTypeCode is not null
        begin
          exec @Error = p_OutboundLine_Insert
          @OutboundLineId     = @OutboundLineId output,
          @OutboundDocumentId = @OutboundDocumentId,
          @StorageUnitId      = @StorageUnitId,
          @StatusId           = @ODStatusId,
          @LineNumber         = @LineNumber,
          @Quantity           = @Quantity,
          @BatchId            = @BatchId,
          @BOELineNumber      = @BOELineNumber,
          @UnitPrice          = @UnitPrice,
          @TariffCode         = @TariffCode
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_OutboundLine_Insert'
            goto error_detail
          end

          exec @Error = p_Despatch_Create_Issue
          @OutboundDocumentId = @OutboundDocumentId,
          @OutboundLineId     = @OutboundLineId,
          @OperatorId         = null
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'
            goto error_detail
          end
        end
        
        if @InboundDocumentTypeCode is not null
        begin
          exec @Error = p_InboundLine_Insert
          @InboundLineId      = @InboundLineId output,
          @InboundDocumentId  = @InboundDocumentId,
          @StorageUnitId      = @StorageUnitId,
          @StatusId           = @StatusId,
          @LineNumber         = @LineNumber,
          @Quantity           = @Quantity,
          @BatchId            = @BatchId,
          @UnitPrice          = @UnitPrice,
          @ChildStorageUnitId = @FPCStorageUnitId,
          @BOELineNumber      = @BOELineNumber,
          @TariffCode         = @TariffCode,
          @CountryofOrigin    = @CountryofOrigin,
          @Reference1         = @Reference1,
          @Reference2         = @Reference2
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_InboundLine_Insert'
          goto error_detail
          end
          
          exec @Error = p_Receiving_Create_Receipt
			        @InboundDocumentId = @InboundDocumentId,
			        @InboundLineId     = @InboundLineId,
			        @OperatorId        = null,
			        @ChildStorageUnitBatchId = @FPCStorageUnitBatchId
 	        
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_Receiving_Create_Receipt'
       goto error_detail
          end
       
          select @ReceiptId = ReceiptId
            from Receipt
       where InboundDocumentId = @InboundDocumentId
          
     if @InboundDocumentTypeCode is not null
          begin
            exec p_Receipt_Update
             @ReceiptId       = @ReceiptId,
             @SealNumber      = @SealNumber,
             @ContainerNumber = @ContainerNumber,
             @AdditionalText1 = @AdditionalText1,
             @AdditionalText2 = @AdditionalText2,
             @BOE             = @BillOfEntry,
             @Incoterms       = @Incoterms,
             @DeliveryNoteNumber = @DeliveryNoteNumber
          end
        end
        
        if @InboundDocumentTypeCode = 'PRV'
        begin
          select @PalletQuantity = Quantity
            from Pack (nolock)
           where StorageUnitId = @StorageUnitId
             and PackTypeId    = 1
             and WarehouseId = @FromWarehouseId
          
          if @PalletQuantity = 0 or @PalletQuantity is null
          begin
            select @ErrorMsg = 'The default pallet quantity cannot be zero.'
            goto error_detail
          end
          
          select @StatusId = dbo.ufn_StatusId('PR','W')
       
          select @ReceiptLineId = ReceiptLineId
            from ReceiptLine
           where InboundLineId = @InboundLineId
          
          while floor(@Quantity/@PalletQuantity) > 0
          begin --loop
            
            exec @Error = p_Palletised_Insert
            @WarehouseId         = @FromWarehouseId,
            @OperatorId          = null,
            @InstructionTypeCode = 'PR',
            @StorageUnitBatchId  = @StorageUnitBatchId,
            @PickLocationId      = null,
            @StoreLocationId     = null,
            @Quantity            = @PalletQuantity,
            @Weight              = @Weight,
            @PriorityId          = @InPriorityId,
            @ReceiptLineId       = @ReceiptLineId,
            @StatusId            = @StatusId
            
            if @error != 0
            begin
              select @ErrorMsg = 'Error executing p_Palletised_Insert'
              goto error_detail
        end
        
            select @Quantity = @Quantity - @PalletQuantity
          END --loop
          
          /***************************/
          /* Insert any spare units */
          /***************************/
          IF @Quantity > 0
          BEGIN --dil
            
            exec @Error = p_Palletised_Insert
            @WarehouseId         = @FromWarehouseId,
            @OperatorId          = null,
            @InstructionTypeCode = 'PR',
            @StorageUnitBatchId  = @StorageUnitBatchId,
            @PickLocationId      = null,
            @StoreLocationId     = null,
            @Quantity            = @Quantity,
            @Weight              = @Weight,
            @PriorityId          = @InPriorityId,
            @ReceiptLineId       = @ReceiptLineId,
            @StatusId            = @StatusId
            
            if @error != 0
            begin
              select @ErrorMsg = 'Error executing p_Palletised_Insert'
              goto error_detail
            end
            
          end --dil
        end
        
        if @Object = '21' and @InboundDocumentTypeCode is null and @OutboundDocumentTypeCode is null
        begin
          select @PalletQuantity = Quantity
            from Pack (nolock)
           where StorageUnitId = @StorageUnitId
             and PackTypeId    = 1
             and WarehouseId   = @FromWarehouseId
          
          if @PalletQuantity = 0 or @PalletQuantity is null
          begin
            select @ErrorMsg = 'The default pallet quantity cannot be zero.'
            goto error_detail
          end
          
          select @StatusId = dbo.ufn_StatusId('J','W')
                 
      while floor(@Quantity/@PalletQuantity) > 0
          begin --loop
            
            exec @Error = p_Palletised_Insert
       @WarehouseId         = @FromWarehouseId,
     @OperatorId          = null,
            @InstructionTypeCode = 'O',
            @StorageUnitBatchId  = @StorageUnitBatchId,
            @PickLocationId      = null,
            @StoreLocationId     = null,
            @Quantity            = @PalletQuantity,
            @Weight              = @Weight,
            @PriorityId          = @InPriorityId,
            @StatusId   = @StatusId
            
            if @error != 0
            begin
              select @ErrorMsg = 'Error executing p_Palletised_Insert'
              goto error_detail
            end
            
            select @Quantity = @Quantity - @PalletQuantity
          END --loop
          
          /***************************/
          /* Insert any spare units */
          /***************************/
          IF @Quantity > 0
          BEGIN --dil
            
            exec @Error = p_Palletised_Insert
            @WarehouseId         = @FromWarehouseId,
            @OperatorId          = null,
            @InstructionTypeCode = 'O',
            @StorageUnitBatchId = @StorageUnitBatchId,
            @PickLocationId      = null,
            @StoreLocationId     = null,
            @Quantity            = @Quantity,
            @Weight              = @Weight,
            @PriorityId          = @InPriorityId,
            @StatusId           = @StatusId
            
            if @error != 0
            begin
              select @ErrorMsg = 'Error executing p_Palletised_Insert'
              goto error_detail
            end
            
          end --dil
        end
      end
      fetch detail_cursor
        into @DocEntry,
             @LineNumber,
             @ProductCode,
             @Product,
             @SkuCode,
             @Sku,
             @Batch,
             @Quantity,
             @UnitPrice,
             @TreeType,
             @FPCCode,
             @BOELineNumber,
             @AlternativeSKU,
             @CountryofOrigin,
             @Height,
             @Width,
             @Length,
             @TariffCode,
             @Reference1,
             @Reference2,
             @ExpiryDate
    end
    
  close detail_cursor
    deallocate detail_cursor
    
    IF @OutboundDocumentTypeCode is not null
    begin
      exec @Error = p_Despatch_Create_Issue
      @OutboundDocumentId = @OutboundDocumentId,
      @OperatorId         = null,
      @Remarks            = @Remarks
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'
        goto error_detail
      end
      
      select @IssueId = IssueId
        from Issue (nolock)
       where OutboundDocumentId = @OutboundDocumentId
      
      exec @Error = p_Issue_Update
       @IssueId    = @IssueId,
       @PriorityId = @OutPriorityId,
       @AddressLine1 = @AddressLine1,
       @AddressLine2 = @AddressLine2,
       @AddressLine3 = @AddressLine3,
       @AddressLine4 = @AddressLine4,
       @AddressLine5 = @AddressLine5
             
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Issue_Update - Priority'
        goto error_detail
      end
      
      if @HoldStatus = 'True' or @Additional1 = 'P'
      begin
        select @StatusId = dbo.ufn_StatusId('IS','OH')
        
        exec @Error = p_Issue_Update
         @IssueId = @IssueId,
         @StatusId = @StatusId
               
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Issue_Update - On hold'
          goto error_detail
        end
      end
      
      if @OutboundDocumentTypeCode = 'BR'
      begin
  exec @Error = p_Reserve_Batches
        @IssueId = @IssueId
        
        if @Error <> 0
        goto error_detail
      end
      
      exec @Error = p_Outbound_Auto_Load
      @OutboundShipmentId = @OutboundShipmentId output,
      @IssueId    = @IssueId

      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
        goto error_detail
      end
      
      exec @Error = p_Outbound_Auto_Release
      @OutboundShipmentId = @OutboundShipmentId,
      @IssueId            = @IssueId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
        goto error_detail
      end
      
      if @Object = '13'
      begin
   exec @Error = p_Outbound_Palletise
        @WarehouseId        = @FromWarehouseId,
        @OutboundShipmentId = @OutboundShipmentId,
        @IssueId            = @IssueId,
        @OperatorId         = null,
        @Weight             = null,
        @Volume             = null
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Outbound_Palletise'
          goto error_detail
        end
        
        --exec p_Invoice_Locate_Pick
        -- @OutboundShipmentId = @OutboundShipmentId,
        -- @IssueId            = @IssueId
        
        --if @Error != 0
        --begin
        --  select @ErrorMsg = 'Error executing p_Invoice_Locate_Pick'
        --  goto error_detail
        --end
      end
    end
    
    error_detail:
    
     --Pratik 
        if(@Error = -1)
        BEGIN
			IF CURSOR_STATUS('global','detail_cursor')>=-1
			BEGIN
			 close detail_cursor
			 DEALLOCATE detail_cursor
		END
		END
    error_header:
    
    if @Error = 0
    begin
      if @@trancount > 0
      commit transaction xml_import
    
      INSERT InterfaceMessage
             ([InterfaceMessageCode]
             ,[InterfaceMessage]
             ,[InterfaceId]
             ,[InterfaceTable]
             ,[Status]
             ,[OrderNumber]
             ,[CreateDate]
             ,[ProcessedDate])
      VALUES
             ('Processed'
             ,'Success'
             ,@InterfaceImportHeaderId
             ,'InterfaceImportHeader'
             ,'N'
             ,@OrderNumber
             ,@GetDate
             ,@GetDate)
    end
    else
    begin
      if @@trancount > 0
      rollback transaction xml_import
      
      INSERT InterfaceMessage
           ([InterfaceMessageCode]
           ,[InterfaceMessage]
           ,[InterfaceId]
           ,[InterfaceTable]
           ,[Status]
           ,[OrderNumber]
           ,[CreateDate]
           ,[ProcessedDate])
    VALUES
           ('Failed'
           ,LEFT(@ErrorMsg, 255)
           ,@InterfaceImportHeaderId
           ,'InterfaceImportHeader'
           ,'E'
           ,@OrderNumber
           ,@GetDate
           ,@GetDate)
      
      update InterfaceImportHeader
         set RecordStatus = 'E',
             ProcessedDate = @GetDate,
             ErrorMsg = @ErrorMsg
       where InterfaceImportHeaderId = @InterfaceImportHeaderId
    end
    --begin try
    fetch header_cursor
      into @InterfaceImportHeaderId,
           @Module,
           @Object,
           @DocEntry,
           @OrderNumber,
           @ExternalCompanyCode,
           @ExternalCompany,
           @Address,
           @Remarks,
     @FromWarehouseCode,
           @ToWarehouseCode,
           @HostStatus,
           @Route,
           @DeliveryDate,
           @PrincipalCode,
           @Additional1,
           @Additional2,
           @Additional3,
           @ContainerNumber,
           @SealNumber,
           @BillOfEntry,
           @AdditionalText1,
           @AdditionalText2,
           @Incoterms,
           @DeliveryNoteNumber,
           @AddressLine1,
           @AddressLine2,
           @AddressLine3,
           @AddressLine4,
           @AddressLine5
           --end try
           --begin catch
    --  if @@trancount > 0
    --  rollback transaction xml_import
    
    --update InterfaceImportHeader
    --   set RecordStatus = 'E',
    --       ProcessedDate = @GetDate,
    --       ErrorMsg = @ErrorMsg
    -- where InterfaceImportHeaderId = @InterfaceImportHeaderId
    
    --close header_cursor
    --goto headercursor
    --end catch
    
    If len(@ExternalCompanyCode) = 0
       Set @ExternalCompanyCode = null
           
    If len(@ExternalCompany) = 0
       Set @ExternalCompany = null
           
    If len(@Address) = 0
       Set @Address = null
 
    -- If len(@ToWarehouseCode) = 0
    --Set @ToWarehouseCode = null
    
    -- If len(@FromWarehouseCode) = 0
    --Set @FromWarehouseCode = null
    
  end
  
  close header_cursor
  deallocate header_cursor
  
  --update i
  --   set DropSequence = ec.DropSequence
  --  from viewIssue vi
  --       join ExternalCompany ec on vi.CustomerCode = ec.ExternalCompanyCode
  --       join Issue i on vi.IssueId = i.IssueId
 -- where vi.DropSequence is null
  
  --update osi
  --   set DropSequence = i.DropSequence
  --  from OutboundShipmentIssue osi
  --       join Issue i on osi.IssueId = i.IssueId
  -- where osi.DropSequence is null
  
  --insert Pack
  --       (StorageUnitId,
            --       PackTypeId,
            --       Quantity,
            --       Barcode,
         --       Length,
            --       Width,
            --       Height,
            --       Volume,
            --       Weight,
            --       WarehouseId)
  --select pk1.StorageUnitId,
  --       pk1.PackTypeId,
  --       pk1.Quantity,
  --       null,
  --       null,
  --       null,
  --       null,
  --       null,
  --       null,
  --       2
  --  from Pack pk1
  -- where not exists(select 1 from Pack pk2 where pk1.StorageUnitId = pk2.StorageUnitId and pk2.WarehouseId = 2 and pk1.PackTypeId = pk2.PackTypeId)
  --   and pk1.WarehouseId = 1
  --order by pk1.StorageUnitId, pk1.PackTypeId
  
  --exec p_Issue_Count_Update
  
  if(object_id('tempdb.dbo.##p_Generic_Import') is not null)
  drop table ##p_Generic_Import
  
  --  return
end
