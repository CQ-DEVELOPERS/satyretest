﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Update
  ///   Filename       : p_Instruction_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Aug 2013 13:13:50
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Instruction table.
  /// </remarks>
  /// <param>
  ///   @InstructionId int = null,
  ///   @ReasonId int = null,
  ///   @InstructionTypeId int = null,
  ///   @StorageUnitBatchId int = null,
  ///   @WarehouseId int = null,
  ///   @StatusId int = null,
  ///   @JobId int = null,
  ///   @OperatorId int = null,
  ///   @PickLocationId int = null,
  ///   @StoreLocationId int = null,
  ///   @ReceiptLineId int = null,
  ///   @IssueLineId int = null,
  ///   @InstructionRefId int = null,
  ///   @Quantity float = null,
  ///   @ConfirmedQuantity float = null,
  ///   @Weight float = null,
  ///   @ConfirmedWeight float = null,
  ///   @PalletId int = null,
  ///   @CreateDate datetime = null,
  ///   @StartDate datetime = null,
  ///   @EndDate datetime = null,
  ///   @Picked bit = null,
  ///   @Stored bit = null,
  ///   @CheckQuantity float = null,
  ///   @CheckWeight float = null,
  ///   @OutboundShipmentId int = null,
  ///   @DropSequence int = null,
  ///   @PackagingWeight float = null,
  ///   @PackagingQuantity float = null,
  ///   @NettWeight float = null,
  ///   @PreviousQuantity float = null,
  ///   @AuthorisedBy int = null,
  ///   @AuthorisedStatus nvarchar(20) = null,
  ///   @SOH float = null,
  ///   @AutoComplete bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Update
(
 @InstructionId int = null,
 @ReasonId int = null,
 @InstructionTypeId int = null,
 @StorageUnitBatchId int = null,
 @WarehouseId int = null,
 @StatusId int = null,
 @JobId int = null,
 @OperatorId int = null,
 @PickLocationId int = null,
 @StoreLocationId int = null,
 @ReceiptLineId int = null,
 @IssueLineId int = null,
 @InstructionRefId int = null,
 @Quantity float = null,
 @ConfirmedQuantity float = null,
 @Weight float = null,
 @ConfirmedWeight float = null,
 @PalletId int = null,
 @CreateDate datetime = null,
 @StartDate datetime = null,
 @EndDate datetime = null,
 @Picked bit = null,
 @Stored bit = null,
 @CheckQuantity float = null,
 @CheckWeight float = null,
 @OutboundShipmentId int = null,
 @DropSequence int = null,
 @PackagingWeight float = null,
 @PackagingQuantity float = null,
 @NettWeight float = null,
 @PreviousQuantity float = null,
 @AuthorisedBy int = null,
 @AuthorisedStatus nvarchar(20) = null,
 @SOH float = null,
 @AutoComplete bit = null 
)

as
begin
	 set nocount on;
  
  if @InstructionId = '-1'
    set @InstructionId = null;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @InstructionTypeId = '-1'
    set @InstructionTypeId = null;
  
  if @StorageUnitBatchId = '-1'
    set @StorageUnitBatchId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @JobId = '-1'
    set @JobId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @PalletId = '-1'
    set @PalletId = null;
  
  if @AuthorisedBy = '-1'
    set @AuthorisedBy = null;
  
	 declare @Error int
 
  update Instruction
     set ReasonId = isnull(@ReasonId, ReasonId),
         InstructionTypeId = isnull(@InstructionTypeId, InstructionTypeId),
         StorageUnitBatchId = isnull(@StorageUnitBatchId, StorageUnitBatchId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         StatusId = isnull(@StatusId, StatusId),
         JobId = isnull(@JobId, JobId),
         OperatorId = isnull(@OperatorId, OperatorId),
         PickLocationId = isnull(@PickLocationId, PickLocationId),
         StoreLocationId = isnull(@StoreLocationId, StoreLocationId),
         ReceiptLineId = isnull(@ReceiptLineId, ReceiptLineId),
         IssueLineId = isnull(@IssueLineId, IssueLineId),
         InstructionRefId = isnull(@InstructionRefId, InstructionRefId),
         Quantity = isnull(@Quantity, Quantity),
         ConfirmedQuantity = isnull(@ConfirmedQuantity, ConfirmedQuantity),
         Weight = isnull(@Weight, Weight),
         ConfirmedWeight = isnull(@ConfirmedWeight, ConfirmedWeight),
         PalletId = isnull(@PalletId, PalletId),
         CreateDate = isnull(@CreateDate, CreateDate),
         StartDate = isnull(@StartDate, StartDate),
         EndDate = isnull(@EndDate, EndDate),
         Picked = isnull(@Picked, Picked),
         Stored = isnull(@Stored, Stored),
         CheckQuantity = isnull(@CheckQuantity, CheckQuantity),
         CheckWeight = isnull(@CheckWeight, CheckWeight),
         OutboundShipmentId = isnull(@OutboundShipmentId, OutboundShipmentId),
         DropSequence = isnull(@DropSequence, DropSequence),
         PackagingWeight = isnull(@PackagingWeight, PackagingWeight),
         PackagingQuantity = isnull(@PackagingQuantity, PackagingQuantity),
         NettWeight = isnull(@NettWeight, NettWeight),
         PreviousQuantity = isnull(@PreviousQuantity, PreviousQuantity),
         AuthorisedBy = isnull(@AuthorisedBy, AuthorisedBy),
         AuthorisedStatus = isnull(@AuthorisedStatus, AuthorisedStatus),
         SOH = isnull(@SOH, SOH),
         AutoComplete = isnull(@AutoComplete, AutoComplete) 
   where InstructionId = @InstructionId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_InstructionHistory_Insert
         @InstructionId = @InstructionId,
         @ReasonId = @ReasonId,
         @InstructionTypeId = @InstructionTypeId,
         @StorageUnitBatchId = @StorageUnitBatchId,
         @WarehouseId = @WarehouseId,
         @StatusId = @StatusId,
         @JobId = @JobId,
         @OperatorId = @OperatorId,
         @PickLocationId = @PickLocationId,
         @StoreLocationId = @StoreLocationId,
         @ReceiptLineId = @ReceiptLineId,
         @IssueLineId = @IssueLineId,
         @InstructionRefId = @InstructionRefId,
         @Quantity = @Quantity,
         @ConfirmedQuantity = @ConfirmedQuantity,
         @Weight = @Weight,
         @ConfirmedWeight = @ConfirmedWeight,
         @PalletId = @PalletId,
         @CreateDate = @CreateDate,
         @StartDate = @StartDate,
         @EndDate = @EndDate,
         @Picked = @Picked,
         @Stored = @Stored,
         @CheckQuantity = @CheckQuantity,
         @CheckWeight = @CheckWeight,
         @OutboundShipmentId = @OutboundShipmentId,
         @DropSequence = @DropSequence,
         @PackagingWeight = @PackagingWeight,
         @PackagingQuantity = @PackagingQuantity,
         @NettWeight = @NettWeight,
         @PreviousQuantity = @PreviousQuantity,
         @AuthorisedBy = @AuthorisedBy,
         @AuthorisedStatus = @AuthorisedStatus,
         @SOH = @SOH,
         @AutoComplete = @AutoComplete,
         @CommandType = 'Update'
  
  return @Error
  
end
