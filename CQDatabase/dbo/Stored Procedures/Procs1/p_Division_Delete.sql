﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Division_Delete
  ///   Filename       : p_Division_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:05
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Division table.
  /// </remarks>
  /// <param>
  ///   @DivisionId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Division_Delete
(
 @DivisionId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Division
     where DivisionId = @DivisionId
  
  select @Error = @@Error
  
  
  return @Error
  
end
