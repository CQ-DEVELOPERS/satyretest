﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Inbound_Shipment_Unlinked_Search
  ///   Filename       : p_Inbound_Shipment_Unlinked_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Inbound_Shipment_Unlinked_Search
(
 @WarehouseId           int,
 @InboundDocumentTypeId	int,
 @ExternalCompanyCode	  nvarchar(30),
 @ExternalCompany	      nvarchar(255),
 @OrderNumber	          nvarchar(30),
 @FromDate	             datetime,
 @ToDate	               datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   ReceiptId            int,
   InboundDocumentId    int,
   OrderNumber          nvarchar(30),
   ExternalCompanyCode  nvarchar(30),
   ExternalCompany      nvarchar(255),
   NumberOfLines        int,
   DeliveryDate         datetime,
   StatusId             int,
   Status               nvarchar(50),
   InboundDocumentType  nvarchar(50),
   LocationId           int,
   Location             nvarchar(15),
   CreateDate           datetime,
   Rating               int
  );
  
  if @InboundDocumentTypeId = -1
    set @InboundDocumentTypeId = null
  
  insert @TableResult
        (ReceiptId,
         id.InboundDocumentId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         DeliveryDate,
         StatusId,
         Status,
         InboundDocumentType,
         LocationId,
         CreateDate,
         Rating)
  select r.ReceiptId,
         id.InboundDocumentId,
         id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         id.DeliveryDate,
         r.StatusId,
         s.Status,
         idt.InboundDocumentType,
         r.LocationId,
         id.CreateDate,
         ec.Rating
    from InboundDocument     id  (nolock)
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    join ExternalCompany     ec  (nolock) on id.ExternalCompanyId     = ec.ExternalCompanyId
    join Receipt             r   (nolock) on id.InboundDocumentId     = r.InboundDocumentId
    join Status              s   (nolock) on r.StatusId               = s.StatusId
   where id.InboundDocumentTypeId = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
     and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
     and isnull(r.DeliveryDate, id.DeliveryDate) between @FromDate and @ToDate
     and not exists(select top 1 1 from InboundShipmentReceipt isr (nolock) where r.ReceiptId = isr.ReceiptId)
     and id.WarehouseId              = @WarehouseId
     and s.Type                    = 'R'
     and s.StatusCode             in ('W','C','D') -- Waiting, Confirmed, Delivered
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.LocationId = l.LocationId
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from InboundLine il (nolock)
                           where t.InboundDocumentId = il.InboundDocumentId)
    from @TableResult t
  
  select ReceiptId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         NumberOfLines,
         DeliveryDate,
         Status,
         InboundDocumentType,
         Location,
         CreateDate,
         Rating
    from @TableResult
end
