﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIVHeader_List
  ///   Filename       : p_InterfaceImportIVHeader_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:39
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportIVHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceImportIVHeader.InterfaceImportIVHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIVHeader_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceImportIVHeaderId
        ,null as 'InterfaceImportIVHeader'
  union
  select
         InterfaceImportIVHeader.InterfaceImportIVHeaderId
        ,InterfaceImportIVHeader.InterfaceImportIVHeaderId as 'InterfaceImportIVHeader'
    from InterfaceImportIVHeader
  
end
