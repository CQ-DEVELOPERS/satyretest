﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperatorHistory_Insert
  ///   Filename       : p_AreaOperatorHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:44
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the AreaOperatorHistory table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @OperatorId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   AreaOperatorHistory.AreaId,
  ///   AreaOperatorHistory.OperatorId,
  ///   AreaOperatorHistory.CommandType,
  ///   AreaOperatorHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperatorHistory_Insert
(
 @AreaId int = null,
 @OperatorId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert AreaOperatorHistory
        (AreaId,
         OperatorId,
         CommandType,
         InsertDate)
  select @AreaId,
         @OperatorId,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
