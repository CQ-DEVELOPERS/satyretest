﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Location_List
  ///   Filename       : p_Housekeeping_Location_List.sql
  ///   Create By      : Willis	
  ///   Date Created   : 18 Aug 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Location_List
(
	@WarehouseId   int,
 @FromAisle     nvarchar(10),
 @ToAisle       nvarchar(10),
 @FromLevel     nvarchar(10),
 @ToLevel       nvarchar(10)
)

as
begin
	 set nocount on;
  
  select l.LocationId,
         l.Location,
         l.Ailse as 'Aisle',
         case when datalength(l.[Column]) = 2
	                                then '0' + l.[Column]
	                                else l.[Column]
	                                end as 'Column',
         l.[Level],
         l.SecurityCode
	   from viewLocation l
	  where l.WarehouseId = @WarehouseId
	    and l.Ailse   between @FromAisle and @ToAisle
	    and l.[Level] between @FromLevel and @ToLevel
	 order by l.Area, l.Ailse, 'Column', l.[Level]
end
