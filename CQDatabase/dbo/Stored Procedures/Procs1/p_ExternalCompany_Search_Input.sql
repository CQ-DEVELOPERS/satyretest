﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Search_Input
  ///   Filename       : p_ExternalCompany_Search_Input.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Apr 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Search_Input
(
 @externalCompany nvarchar(255) = null,
 @externalCompanyCode nvarchar(30) = null,
 @externalCompanyTypeCode nvarchar(10) = 'CUST'
)

as
begin
  set nocount on;
  
  select ec.ExternalCompanyId,
         ec.ExternalCompany,
         ec.ExternalCompanyCode
    from ExternalCompany     ec  (nolock)
    join ExternalCompanyType ect (nolock) on ect.ExternalCompanyTypeId = ec.ExternalCompanyTypeId
   where isnull(ec.ExternalCompany,'%')  like '%' + isnull(@externalCompany, isnull(ec.ExternalCompany,'%')) + '%'
     and isnull(ec.ExternalCompanyCode,'%')  like '%' + isnull(@externalCompanyCode, isnull(ec.ExternalCompanyCode,'%')) + '%'
     and isnull(@externalCompanyTypeCode,ect.ExternalCompanyTypeCode) = @externalCompanyTypeCode
  order by ec.ExternalCompany
end
