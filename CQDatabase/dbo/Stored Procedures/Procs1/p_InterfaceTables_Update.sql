﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceTables_Update
  ///   Filename       : p_InterfaceTables_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:49
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceTables table.
  /// </remarks>
  /// <param>
  ///   @InterfaceTableId int = null,
  ///   @HeaderTable nvarchar(100) = null,
  ///   @DetailTable nvarchar(100) = null,
  ///   @HeaderTableKey nvarchar(100) = null,
  ///   @Description1 nvarchar(100) = null,
  ///   @Description2 nvarchar(100) = null,
  ///   @HeaderField1 nvarchar(100) = null,
  ///   @HeaderField2 nvarchar(100) = null,
  ///   @HeaderField3 nvarchar(100) = null,
  ///   @HeaderField4 nvarchar(100) = null,
  ///   @HeaderField5 nvarchar(100) = null,
  ///   @HeaderField6 nvarchar(100) = null,
  ///   @HeaderField7 nvarchar(100) = null,
  ///   @HeaderField8 nvarchar(100) = null,
  ///   @DetailField1 nvarchar(100) = null,
  ///   @DetailField2 nvarchar(100) = null,
  ///   @DetailField3 nvarchar(100) = null,
  ///   @DetailField4 nvarchar(100) = null,
  ///   @DetailField5 nvarchar(100) = null,
  ///   @DetailField6 nvarchar(100) = null,
  ///   @DetailField7 nvarchar(100) = null,
  ///   @DetailField8 nvarchar(100) = null,
  ///   @StartId int = null,
  ///   @InsertDetail int = null,
  ///   @GroupByKey nvarchar(100) = null,
  ///   @AltGroupByKey nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceTables_Update
(
 @InterfaceTableId int = null,
 @HeaderTable nvarchar(100) = null,
 @DetailTable nvarchar(100) = null,
 @HeaderTableKey nvarchar(100) = null,
 @Description1 nvarchar(100) = null,
 @Description2 nvarchar(100) = null,
 @HeaderField1 nvarchar(100) = null,
 @HeaderField2 nvarchar(100) = null,
 @HeaderField3 nvarchar(100) = null,
 @HeaderField4 nvarchar(100) = null,
 @HeaderField5 nvarchar(100) = null,
 @HeaderField6 nvarchar(100) = null,
 @HeaderField7 nvarchar(100) = null,
 @HeaderField8 nvarchar(100) = null,
 @DetailField1 nvarchar(100) = null,
 @DetailField2 nvarchar(100) = null,
 @DetailField3 nvarchar(100) = null,
 @DetailField4 nvarchar(100) = null,
 @DetailField5 nvarchar(100) = null,
 @DetailField6 nvarchar(100) = null,
 @DetailField7 nvarchar(100) = null,
 @DetailField8 nvarchar(100) = null,
 @StartId int = null,
 @InsertDetail int = null,
 @GroupByKey nvarchar(100) = null,
 @AltGroupByKey nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceTableId = '-1'
    set @InterfaceTableId = null;
  
	 declare @Error int
 
  update InterfaceTables
     set HeaderTable = isnull(@HeaderTable, HeaderTable),
         DetailTable = isnull(@DetailTable, DetailTable),
         HeaderTableKey = isnull(@HeaderTableKey, HeaderTableKey),
         Description1 = isnull(@Description1, Description1),
         Description2 = isnull(@Description2, Description2),
         HeaderField1 = isnull(@HeaderField1, HeaderField1),
         HeaderField2 = isnull(@HeaderField2, HeaderField2),
         HeaderField3 = isnull(@HeaderField3, HeaderField3),
         HeaderField4 = isnull(@HeaderField4, HeaderField4),
         HeaderField5 = isnull(@HeaderField5, HeaderField5),
         HeaderField6 = isnull(@HeaderField6, HeaderField6),
         HeaderField7 = isnull(@HeaderField7, HeaderField7),
         HeaderField8 = isnull(@HeaderField8, HeaderField8),
         DetailField1 = isnull(@DetailField1, DetailField1),
         DetailField2 = isnull(@DetailField2, DetailField2),
         DetailField3 = isnull(@DetailField3, DetailField3),
         DetailField4 = isnull(@DetailField4, DetailField4),
         DetailField5 = isnull(@DetailField5, DetailField5),
         DetailField6 = isnull(@DetailField6, DetailField6),
         DetailField7 = isnull(@DetailField7, DetailField7),
         DetailField8 = isnull(@DetailField8, DetailField8),
         StartId = isnull(@StartId, StartId),
         InsertDetail = isnull(@InsertDetail, InsertDetail),
         GroupByKey = isnull(@GroupByKey, GroupByKey),
         AltGroupByKey = isnull(@AltGroupByKey, AltGroupByKey) 
   where InterfaceTableId = @InterfaceTableId
  
  select @Error = @@Error
  
  
  return @Error
  
end
