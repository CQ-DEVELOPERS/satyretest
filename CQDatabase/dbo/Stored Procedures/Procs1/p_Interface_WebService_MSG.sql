﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_MSG
  ///   Filename       : p_Interface_WebService_MSG.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Interface_WebService_MSG]
(
 @XMLBody varchar(max) output
 ,	@PrincipalCode NVARCHAR(30) = NULL
)
--with encryption
as
begin
	
	DECLARE @doc xml
	DECLARE @idoc int
	DECLARE @InsertDate datetime
	DECLARE @ERROR AS nvarchar(255)
	Declare @InterfaceImportHeaderId int
	
	SELECT @doc = convert(xml,@XMLBody)
	     , @InsertDate = Getdate()
	     
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	
	BEGIN TRY
         
        INSERT INTO [InterfaceMessage]
           ([InterfaceMessageCode]
           ,[InterfaceMessage]
           ,[InterfaceId]
           ,[InterfaceTable]
           ,[Status]
           ,[OrderNumber]
           ,[CreateDate]
           ,[InsertDate])

     	    SELECT 
	            nodes.entity.value('InterfaceMessageCode[1]',   'nvarchar(50)')  'InterfaceMessageCode'               
               ,nodes.entity.value('InterfaceMessage[1]',       'nvarchar(MAX)') 'InterfaceMessage'               
               ,nodes.entity.value('InterfaceTableId[1]',       'int')           'InterfaceId'
               ,nodes.entity.value('InterfaceTable[1]',         'nvarchar(128)') 'InterfaceTable'
               ,nodes.entity.value('Status[1]',                 'nvarchar(20)') 'Status'
               ,nodes.entity.value('OrderNumber[1]',            'nvarchar(30)')  'OrderNumber'
               ,@InsertDate
               ,@InsertDate               
        FROM
	            @doc.nodes('/root/Body/Item/ItemLine') AS nodes(entity)
    	
	END TRY
	BEGIN CATCH
	    
	   SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)
	END CATCH
	
	--exec p_Pastel_Import_PO
	IF (@ERROR IS NULL)
    	SET @XMLBody = '<root><status>Success</status></root>'
    ELSE
        SET @XMLBody = '<root><status>' + @ERROR + '</status></root>'
End
