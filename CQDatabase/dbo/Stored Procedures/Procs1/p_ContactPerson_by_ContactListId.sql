﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContactPerson_by_ContactListId
  ///   Filename       : p_ContactPerson_by_ContactListId.sql
  ///   Create By      : Karen
  ///   Date Created   : August 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContactPerson_by_ContactListId


as
begin
	 set nocount on;
  
  select -1     as 'ContactListId',
         'None' as 'ContactPerson'
  union
  select idcl.ContactListId,
		 ContactPerson
    from InboundDocumentContactList idcl
    join ContactList cl (nolock) on idcl.ContactListId = cl.ContactListId
end
