﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CheckingLog_Select
  ///   Filename       : p_CheckingLog_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:16
  /// </summary>
  /// <remarks>
  ///   Selects rows from the CheckingLog table.
  /// </remarks>
  /// <param>
  ///   @CheckingLogId int = null 
  /// </param>
  /// <returns>
  ///   CheckingLog.CheckingLogId,
  ///   CheckingLog.WarehouseId,
  ///   CheckingLog.ReferenceNumber,
  ///   CheckingLog.JobId,
  ///   CheckingLog.Barcode,
  ///   CheckingLog.StorageUnitId,
  ///   CheckingLog.StorageUnitBatchId,
  ///   CheckingLog.Batch,
  ///   CheckingLog.Quantity,
  ///   CheckingLog.OperatorId,
  ///   CheckingLog.StartDate,
  ///   CheckingLog.EndDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CheckingLog_Select
(
 @CheckingLogId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         CheckingLog.CheckingLogId
        ,CheckingLog.WarehouseId
        ,CheckingLog.ReferenceNumber
        ,CheckingLog.JobId
        ,CheckingLog.Barcode
        ,CheckingLog.StorageUnitId
        ,CheckingLog.StorageUnitBatchId
        ,CheckingLog.Batch
        ,CheckingLog.Quantity
        ,CheckingLog.OperatorId
        ,CheckingLog.StartDate
        ,CheckingLog.EndDate
    from CheckingLog
   where isnull(CheckingLog.CheckingLogId,'0')  = isnull(@CheckingLogId, isnull(CheckingLog.CheckingLogId,'0'))
  
end
