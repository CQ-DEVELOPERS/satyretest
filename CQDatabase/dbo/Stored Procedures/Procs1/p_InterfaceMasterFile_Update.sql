﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMasterFile_Update
  ///   Filename       : p_InterfaceMasterFile_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:29:20
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceMasterFile table.
  /// </remarks>
  /// <param>
  ///   @MasterFileId int = null,
  ///   @MasterFile varchar(50) = null,
  ///   @MasterFileDescription varchar(255) = null,
  ///   @LastDate datetime = null,
  ///   @SendFlag bit = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMasterFile_Update
(
 @MasterFileId int = null,
 @MasterFile varchar(50) = null,
 @MasterFileDescription varchar(255) = null,
 @LastDate datetime = null,
 @SendFlag bit = null 
)

as
begin
	 set nocount on;
  
  if @MasterFileId = '-1'
    set @MasterFileId = null;
  
	 declare @Error int
 
  update InterfaceMasterFile
     set MasterFile = isnull(@MasterFile, MasterFile),
         MasterFileDescription = isnull(@MasterFileDescription, MasterFileDescription),
         LastDate = isnull(@LastDate, LastDate),
         SendFlag = isnull(@SendFlag, SendFlag) 
   where MasterFileId = @MasterFileId
  
  select @Error = @@Error
  
  
  return @Error
  
end
