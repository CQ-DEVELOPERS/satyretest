﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLine_Search
  ///   Filename       : p_InboundLine_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 08:48:44
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundLine table.
  /// </remarks>
  /// <param>
  ///   @InboundLineId int = null output,
  ///   @InboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @BatchId int = null,
  ///   @ReasonId int = null,
  ///   @ChildStorageUnitId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InboundLine.InboundLineId,
  ///   InboundLine.InboundDocumentId,
  ///   InboundDocument.InboundDocumentId,
  ///   InboundLine.StorageUnitId,
  ///   StorageUnit.StorageUnitId,
  ///   InboundLine.StatusId,
  ///   Status.Status,
  ///   InboundLine.LineNumber,
  ///   InboundLine.Quantity,
  ///   InboundLine.BatchId,
  ///   Batch.Batch,
  ///   InboundLine.UnitPrice,
  ///   InboundLine.ReasonId,
  ///   Reason.Reason,
  ///   InboundLine.ChildStorageUnitId,
  ///   StorageUnit.StorageUnitId,
  ///   InboundLine.BOELineNumber,
  ///   InboundLine.TariffCode,
  ///   InboundLine.CountryofOrigin,
  ///   InboundLine.Reference1,
  ///   InboundLine.Reference2 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLine_Search
(
 @InboundLineId int = null output,
 @InboundDocumentId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @BatchId int = null,
 @ReasonId int = null,
 @ChildStorageUnitId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InboundLineId = '-1'
    set @InboundLineId = null;
  
  if @InboundDocumentId = '-1'
    set @InboundDocumentId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @ChildStorageUnitId = '-1'
    set @ChildStorageUnitId = null;
  
 
  select
         InboundLine.InboundLineId
        ,InboundLine.InboundDocumentId
        ,InboundLine.StorageUnitId
        ,InboundLine.StatusId
         ,StatusStatusId.Status as 'Status'
        ,InboundLine.LineNumber
        ,InboundLine.Quantity
        ,InboundLine.BatchId
         ,BatchBatchId.Batch as 'Batch'
        ,InboundLine.UnitPrice
        ,InboundLine.ReasonId
         ,ReasonReasonId.Reason as 'Reason'
        ,InboundLine.ChildStorageUnitId
        ,InboundLine.BOELineNumber
        ,InboundLine.TariffCode
        ,InboundLine.CountryofOrigin
        ,InboundLine.Reference1
        ,InboundLine.Reference2
    from InboundLine
    left
    join InboundDocument InboundDocumentInboundDocumentId on InboundDocumentInboundDocumentId.InboundDocumentId = InboundLine.InboundDocumentId
    left
    join StorageUnit StorageUnitStorageUnitId on StorageUnitStorageUnitId.StorageUnitId = InboundLine.StorageUnitId
    left
    join Status StatusStatusId on StatusStatusId.StatusId = InboundLine.StatusId
    left
    join Batch BatchBatchId on BatchBatchId.BatchId = InboundLine.BatchId
    left
    join Reason ReasonReasonId on ReasonReasonId.ReasonId = InboundLine.ReasonId
    left
    join StorageUnit StorageUnitChildStorageUnitId on StorageUnitChildStorageUnitId.StorageUnitId = InboundLine.ChildStorageUnitId
   where isnull(InboundLine.InboundLineId,'0')  = isnull(@InboundLineId, isnull(InboundLine.InboundLineId,'0'))
     and isnull(InboundLine.InboundDocumentId,'0')  = isnull(@InboundDocumentId, isnull(InboundLine.InboundDocumentId,'0'))
     and isnull(InboundLine.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(InboundLine.StorageUnitId,'0'))
     and isnull(InboundLine.StatusId,'0')  = isnull(@StatusId, isnull(InboundLine.StatusId,'0'))
     and isnull(InboundLine.BatchId,'0')  = isnull(@BatchId, isnull(InboundLine.BatchId,'0'))
     and isnull(InboundLine.ReasonId,'0')  = isnull(@ReasonId, isnull(InboundLine.ReasonId,'0'))
     and isnull(InboundLine.ChildStorageUnitId,'0')  = isnull(@ChildStorageUnitId, isnull(InboundLine.ChildStorageUnitId,'0'))
  order by BOELineNumber
  
end
