﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Inbound_Release
  ///   Filename       : p_Inbound_Release.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Inbound_Release
(
 @InboundShipmentId int = null,
 @ReceiptId         int = null,
 @ReceiptLineId     int = null,
 @StatusCode        nvarchar(10) = 'RL',
 @NewPalletId       nvarchar(30) = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int,
          @WarehouseId       int,
          @InboundDocumentId int,
          @InboundDocumentTypeCode nvarchar(10),
          @AutoSendup        bit,
          @QuantityToFollowIndicator bit,
          @DeliveryNoteNumber nvarchar(30),
          @Tran               bit = 1,
          @DeliveryNoteNumberRequierd  bit = 0,
          @VehicleRegistration         nvarchar(30),
          @VehicleRegistrationRequierd bit = 0
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount > 0
    set @Tran = 0
  
  if @Tran = 1
    begin transaction
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @ReceiptId = -1
    set @ReceiptId = null
  
  if @ReceiptLineId = -1
    set @ReceiptLineId = null
  
  begin
    declare @TableHeader as table
    (
     InboundShipmentId int,
     ReceiptId            int,
     ReceiptLineId        int
    )
    
    if @StatusCode in ('RL','M','PS')
    begin
      if @InboundShipmentId is null
      begin
        if @ReceiptId is not null or @ReceiptLineId is not null
          insert @TableHeader
                (InboundShipmentId,
                 ReceiptId,
                 ReceiptLineId)
          select null,
                 il.ReceiptId,
                 il.ReceiptLineId
            from ReceiptLine         il (nolock)
            join Receipt              i (nolock) on il.ReceiptId  = i.ReceiptId
            join Status             s (nolock) on i.StatusId  = s.StatusId
           where il.ReceiptId             = isnull(@ReceiptId, il.ReceiptId)
             and il.ReceiptLineId         = isnull(@ReceiptLineId, il.ReceiptLineId)
             and s.StatusCode          in ('M','RL','PC','PS')
          
          update th
             set InboundShipmentId = osi.InboundShipmentId
            from @TableHeader th
            join InboundShipmentReceipt osi on th.ReceiptId = osi.ReceiptId
          
          select @WarehouseId = WarehouseId
            from Receipt
           where ReceiptId = @ReceiptId
      end
      else
      begin
        insert @TableHeader
              (InboundShipmentId,
               ReceiptId,
               ReceiptLineId)
        select osi.InboundShipmentId,
               il.ReceiptId,
               il.ReceiptLineId
          from InboundShipmentReceipt osi (nolock)
          join Receipt                   i (nolock) on osi.ReceiptId  = i.ReceiptId
          join ReceiptLine              il (nolock) on i.ReceiptId = il.ReceiptId
          join Status                  s (nolock) on i.StatusId  = s.StatusId
         where osi.InboundShipmentId = @InboundShipmentId
           and s.StatusCode          in ('M','RL','PC','PS')
        
        select @WarehouseId = WarehouseId
          from InboundShipment
         where InboundShipmentId = @InboundShipmentId
      end
    end
    else
    begin
      if @InboundShipmentId is null
      begin
        if @ReceiptId is not null or @ReceiptLineId is not null
          insert @TableHeader
                (InboundShipmentId,
                 ReceiptId,
                 ReceiptLineId)
          select null,
                 il.ReceiptId,
                 il.ReceiptLineId
            from ReceiptLine         il (nolock)
           where il.ReceiptId             = isnull(@ReceiptId, il.ReceiptId)
             and il.ReceiptLineId         = isnull(@ReceiptLineId, il.ReceiptLineId)
          
          select @WarehouseId        = WarehouseId,
                 @InboundDocumentId  = InboundDocumentId,
                 @DeliveryNoteNumber = DeliveryNoteNumber,
                 @VehicleRegistration = VehicleRegistration
            from Receipt
           where ReceiptId = @ReceiptId
      end
      else
      begin
        insert @TableHeader
              (InboundShipmentId,
               ReceiptId,
               ReceiptLineId)
        select osi.InboundShipmentId,
               il.ReceiptId,
               il.ReceiptLineId
          from InboundShipmentReceipt osi (nolock)
          join ReceiptLine         il (nolock) on osi.ReceiptId = il.ReceiptId
         where osi.InboundShipmentId = @InboundShipmentId
        
        select @WarehouseId       = r.WarehouseId,
               @InboundDocumentId = r.InboundDocumentId,
               @DeliveryNoteNumber = r.DeliveryNoteNumber
          from InboundShipmentReceipt isr
          join Receipt                  r on isr.ReceiptId = r.ReceiptId
         where isr.InboundShipmentId = @InboundShipmentId
      end
      
      select @InboundDocumentTypeCode   = idt.InboundDocumentTypeCode,
             @AutoSendup                = idt.Autosendup,
             @QuantityToFollowIndicator = idt.QuantityToFollowIndicator
        from InboundDocument      id (nolock)
        join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
       where id.InboundDocumentId = @InboundDocumentId
      
      --check for kitting
      if @InboundDocumentTypeCode = 'KIT'
      begin
        --get issue
        
        declare @IssueId Int
        
        select @StatusId = dbo.ufn_StatusId('IS','W')
              ,@IssueId = i.IssueId
        From InboundDocument id
        Inner Join BOMInstruction bi on id.InboundDocumentId = bi.InboundDocumentId
        Inner Join Issue i on bi.ParentId = i.OutboundDocumentId
        where id.InboundDocumentId = @InboundDocumentId
         
        exec @Error = p_Issue_Update  
         @IssueId = @IssueId,
         @StatusId = @StatusId,  
         @DeliveryDate = @GetDate
      end 
      --end kitting check         
    end
    
    if @StatusCode = 'RC'
    if (select dbo.ufn_Configuration(129,@WarehouseId)) = 0
    if exists(select top 1 1
                from @TableHeader     tr
                join ReceiptLine       rl (nolock) on tr.ReceiptLineId      = rl.ReceiptLineId
                join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
                join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
                join Batch              b (nolock) on sub.BatchId           = b.BatchId
               where b.Batch              = 'Default'
                 and su.ProductCategory  != 'P'
                 and rl.AcceptedQuantity > 0)
    begin
      set @Error = -2
      goto error
    end
    
    select @StatusId = dbo.ufn_StatusId('R',@StatusCode)
    
    select @InboundDocumentTypeCode = idt.InboundDocumentTypeCode,
           @AutoSendup              = idt.Autosendup,
           @QuantityToFollowIndicator = idt.QuantityToFollowIndicator,
           @DeliveryNoteNumberRequierd = ISNULL(idt.DeliveryNoteNumber, 0),
           @VehicleRegistrationRequierd = ISNULL(idt.VehicleRegistration, 0)
      from InboundDocument      id (nolock)
      join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
     where id.InboundDocumentId = @InboundDocumentId
    
    update rl
       set StatusId = @StatusId
      from @TableHeader th
      join ReceiptLine    rl on th.ReceiptLineId = rl.ReceiptLineId
      join Status          s on rl.StatusId = s.StatusId
     where s.StatusCode != 'RC' -- Don't insert if in final status
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert ReceiptLineHistory
          (ReceiptLineId,
           StatusId,
           CommandType,
           InsertDate)
    select rl.ReceiptLineId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader   th
      join ReceiptLine    rl on th.ReceiptLineId = rl.ReceiptLineId
      join Status          s on rl.StatusId = s.StatusId
     where s.StatusCode != 'RC' -- Don't insert if in final status
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update i
       set StatusId = @StatusId,
		   ReceivingCompleteDate = @GetDate
      from @TableHeader th
      join Receipt         i on th.ReceiptId = i.ReceiptId
      join Status          s on i.StatusId = s.StatusId
     where s.StatusCode != 'RC' -- Don't update if in final status
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert ReceiptHistory
          (ReceiptId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           th.ReceiptId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader   th
      join Receipt         i on th.ReceiptId = i.ReceiptId
      join Status          s on i.StatusId = s.StatusId
     where s.StatusCode != 'RC' -- Don't insert if in final status
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    update os
       set StatusId = @StatusId
      from @TableHeader     th
      join InboundShipment os on th.InboundShipmentId = os.InboundShipmentId
      join Status          s on os.StatusId = s.StatusId
     where s.StatusCode != 'RC' -- Don't update if in final status
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
    insert InboundShipmentHistory
          (InboundShipmentId,
           StatusId,
           CommandType,
           InsertDate)
    select distinct
           th.InboundShipmentId,
           @StatusId,
           'Update',
           @GetDate
      from @TableHeader    th
      join InboundShipment os on th.InboundShipmentId = os.InboundShipmentId
      join Status          s on os.StatusId = s.StatusId
     where s.StatusCode != 'RC' -- Don't update if in final status
    
    select @Error = @@Error
    
    if @Error <> 0
      goto error
    
--    update i
--       set StatusId = @StatusId
--      from @TableHeader     th
--      join Job               j on th.ReceiptLineId = j.ReceiptLineId
--      join Instruction       i on j.JobId        = i.JobId
--    
--    select @Error = @@Error
--    
--    if @Error <> 0
--      goto error
--    
--    insert InstructionHistory
--          (InstructionId,
--           StatusId,
--           CommandType,
--           InsertDate)
--    select distinct
--           InstructionId,
--           @StatusId,
--           'Update',
--           @GetDate
--      from @TableHeader     th
--      join Instruction       i on th.ReceiptLineId = i.ReceiptLineId
--      join Job               j on i.JobId        = j.JobId
--    
--    select @Error = @@Error
--    
--    if @Error <> 0
--      goto error
    
    if @StatusCode = 'RC'
    begin
      update j
         set StatusId = @StatusId
        from @TableHeader     th
        join Instruction       i on th.ReceiptLineId = i.ReceiptLineId
        join Job               j on i.JobId        = j.JobId
        join Status            s on j.StatusId     = s.StatusId
       where s.StatusCode in ('P')
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
      
      insert JobHistory
            (JobId,
             StatusId,
             CommandType,
             InsertDate)
      select distinct
             j.JobId,
             @StatusId,
             'Update',
             @GetDate
        from @TableHeader     th
        join Instruction       i on th.ReceiptLineId = i.ReceiptLineId
        join Job               j on i.JobId        = j.JobId
        join Status            s on j.StatusId     = s.StatusId
       where s.StatusCode in ('P')
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
    end
    else if @NewPalletId is null
    begin
      update j
         set StatusId = @StatusId
        from @TableHeader     th
        join Instruction       i on th.ReceiptLineId = i.ReceiptLineId
        join Job               j on i.JobId        = j.JobId
        join Status            s on j.StatusId     = s.StatusId
       where s.StatusCode != 'NS'
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
      
      insert JobHistory
            (JobId,
             StatusId,
             CommandType,
             InsertDate)
      select distinct
             j.JobId,
             @StatusId,
             'Update',
             @GetDate
        from @TableHeader     th
        join Instruction       i on th.ReceiptLineId = i.ReceiptLineId
        join Job               j on i.JobId        = j.JobId
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
    end
--    select @Rows = count(1) from @TableHeader
--    set @Count = 0
--    
--    while @Count < @Rows
--    begin
--      set @Count = @Count + 1
--      
--      select @ReceiptLineId = ReceiptLineId
--        from @TableHeader
--      
--      delete @TableHeader
--       where ReceiptLineId = @ReceiptLineId
--      
--      exec @Error = p_ReceiptLine_Update
--       @ReceiptLineId = @ReceiptLineId,
--       @StatusId    = @StatusId
--      
--      if @Error <> 0
--        goto error
--    end
--    
--    select @Rows = count(1) from @TableReceipt
--    set @Count = 0
--    
--    while @Count < @Rows
--    begin
--      set @Count = @Count + 1
--      
--      select @ReceiptId = ReceiptId
--        from @TableReceipt
--      
--      delete @TableReceipt
--       where ReceiptId = @ReceiptId
--      
--      exec @Error = p_Receipt_Update
--       @ReceiptId  = @ReceiptId,
--       @StatusId = @StatusId
--      
--      if @Error <> 0
--        goto error
--    end
  end
  
  if @StatusCode = 'RC' and @AutoSendup = 1 and @DeliveryNoteNumberRequierd = 1 and @DeliveryNoteNumber is null
  begin
    set @Error = -1
    goto Error
  end
  
  if @StatusCode = 'RC' and @AutoSendup = 1 and @VehicleRegistrationRequierd = 1 and @VehicleRegistration is null
  begin
    set @Error = -1
    goto Error
  end
  
  set @ReceiptId = 0
  
  if @StatusCode = 'RC' and @AutoSendup = 1
  begin
    while exists(select top 1 1 from @TableHeader where ReceiptId > @ReceiptId)
    begin
      select top 1 @ReceiptId = ReceiptId
        from @TableHeader
       where ReceiptId > @ReceiptId
      order by ReceiptId
      
      if @InboundDocumentTypeCode = 'GRV'
      begin
        exec @Error = p_Receiving_Consolidated_Load_Complete
         @ReceiptId = @ReceiptId
        
        if @Error <> 0
          goto error
      end
      else
      begin
        exec @Error = p_interface_xml_PO_Export_Insert
         @ReceiptId = @ReceiptId
        
        if @Error <> 0
          goto error
      end
    end
  end
  
  if (select dbo.ufn_Configuration(213,@WarehouseId)) = 1 and @InboundDocumentTypeCode = 'IBT'
		and @StatusCode = 'RC'
  begin
    exec @Error = p_Inbound_PO_Insert
     @ReceiptId = @ReceiptId
    
    if @Error <> 0
      goto error
  end
  
  --if (select dbo.ufn_Configuration(214,@WarehouseId)) = 1 and @InboundDocumentTypeCode = 'IBT'
  --begin
  --  exec @Error = p_Inbound_PO_Insert
  --   @ReceiptId = @ReceiptId
  --  
  --  if @Error <> 0
  --    goto error
  --end
  
  if @Tran = 1
    commit transaction
  
  if @StatusCode = 'RC'
  begin
    set @ReceiptId = 0
    
    while exists(select top 1 1 from @TableHeader where ReceiptId > @ReceiptId)
    begin
      select top 1 @ReceiptId = ReceiptId
        from @TableHeader
       where ReceiptId > @ReceiptId
      order by ReceiptId
      
      --exec @Error = p_Bill_Insert_Receipt
      -- @ReceiptId = @ReceiptId
      
      --if @Error <> 0
      --  goto error
      
      exec @Error = p_Sample_Quantity_Writeoff
       @ReceiptId = @ReceiptId
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Rep_Warehouse_Transfer
       @ReceiptId = @ReceiptId
      
      if @Error <> 0
        goto error
      
      if @QuantityToFollowIndicator = 1
        if (select dbo.ufn_Configuration(177,@WarehouseId)) = 1
        begin
          exec @Error = p_Receiving_Redelivery_Insert
           @inboundShipmentId = null,
           @receiptId         = @ReceiptId
          
          if @Error <> 0
            goto error
        end
    end
    
    if (select dbo.ufn_Configuration(195,@WarehouseId)) = 1
    begin
      exec @Error = p_Receiving_Capture_Default_Packaging
       @inboundShipmentId = @InboundShipmentId,
       @receiptId         = @ReceiptId
    end
  end
  
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Inbound_Release'); 
    if @Tran = 1
      rollback transaction
    return @Error
end
 
