﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Create_From_Issue
  ///   Filename       : p_InboundDocument_Create_From_Issue.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Create_From_Issue
(
 @IssueId            int = null,
 @OutboundShipmentId int = null,
 @IssueLineId        int = null,
 @InboundDocumentTypeCode nvarchar(30) = 'IBT'
)

as
begin
  set nocount on;
  
  declare @Error                 int,
          @Errormsg              varchar(500),
          @GetDate               datetime,
          @WarehouseId           Int,
          @Ordernumber           varchar(50),
          @FromWarehouseCode     varchar(50),
          @InboundDocumentTypeId int,
          @ExternalCompanyId     int,
          @DeliveryDate          Datetime,
          @InboundDocumentId     int,
          @StatusId              Int
    
    If @IssueLineId = -1
        Set @IssueLineId =null 
    
    If @OutboundShipmentId = -1
        Set @OutboundShipmentId =null         
    
    If @IssueId = -1
        Set @IssueId =null         
        
    select @GetDate = dbo.ufn_Getdate()
    Set @StatusId = dbo.ufn_StatusId('ID','W')
    
    Select @InboundDocumentTypeId = InboundDocumentTypeId 
      from InboundDocumentType 
     where InboundDocumentTypeCode = @InboundDocumentTypeCode
    
    if @InboundDocumentTypeId is null
      return
        
    Declare Header_Cursor_Create_From cursor for
    Select distinct w1.WarehouseCode,
            isnull(e2.ExternalCompanyId, e2.ExternalCompanyId),
            w1.WarehouseId,
            od.Ordernumber,
            i.deliveryDate,
            i.IssueId
       from Issue i (nolock)
       join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
       Left Join OutboundShipmentIssue o (nolock) on o.IssueId = i.IssueId
       Left Join IssueLine il (nolock) on il.IssueId = i.IssueId
       --Join Warehouse w (nolock) on w.WarehouseCode = i.CustomerCode
       Join warehouse w1 (nolock) on w1.WarehouseId = i.WarehouseId
       left
       Join ExternalCompany e1 on e1.externalCompanyCode = w1.WarehouseCode --and e1.ExternalCompanyTypeId = 2
       left
       Join ExternalCompany e2 on e2.ExternalCompanyId = od.ExternalCompanyId --and e1.ExternalCompanyTypeId = 2
    where Isnull(o.OutboundShipmentId,1) = Isnull(isnull(@OutboundShipmentId,o.OutboundShipmentId),1)
     and i.IssueId = Isnull(@IssueId,i.IssueId)
     and il.IssueLineId = Isnull(@IssueLineId,il.IssueLineId)
    
    Open Header_Cursor_Create_From
    Fetch Header_Cursor_Create_From Into 
        @FromWarehouseCode,
        @ExternalCompanyId,
        @WarehouseId,
        @Ordernumber,
        @DeliveryDate,
        @IssueId
    
    While @@Fetch_Status = 0
    Begin
                
            exec @Error = p_InboundDocument_Insert
                   @InboundDocumentId     = @InboundDocumentId output,
                   @InboundDocumentTypeId = @InboundDocumentTypeId,
                   @ExternalCompanyId     = @ExternalCompanyId,
                   @StatusId              = @StatusId,
                   @WarehouseId           = @WarehouseId,
                   @OrderNumber           = @OrderNumber,
                   @DeliveryDate          = @GetDate,
                   @CreateDate            = @GetDate,
                   @ModifiedDate          = null

            Declare  @OperatorId        int,
                     @InboundLineId     int,
                     @StorageUnitId     int,
                     @LineNumber        int,
                     @Quantity          float,
                     @BatchId           int 

            Declare Detail_Cursor_From_Issue cursor for 
            Select sub.StorageUnitId,
                    ol.LineNumber,
                    il.Quantity,
                    sub.BatchId
            from IssueLine il
                Join StorageUnitBatch sub on sub.StorageUnitBatchId = il.StorageUnitBatchId
                Join OutboundLine ol on ol.OutboundLineId = il.OutboundLineId
                --Join Batch b on b.BatchId = sub.BatchId
             where il.IssueId = @IssueId
                    and il.IssueLineId = Isnull(@IssueLineId,il.IssueLineId)

            Open Detail_Cursor_From_Issue
            Fetch  Detail_Cursor_From_Issue 
                into @StorageUnitId,
                     @LineNumber,
                     @Quantity,
                     @BatchId
            While @@Fetch_Status = 0
            Begin
                    Exec @Error = p_InboundLine_Create
                             @OperatorId,
                             @InboundLineId,
                             @InboundDocumentId,
                             @StorageUnitId,
                             @LineNumber,
                             @Quantity,
                             @BatchId
                             
                    Fetch Detail_Cursor_From_Issue 
                        into @StorageUnitId,
                             @LineNumber,
                             @Quantity,
                             @BatchId
            End
            
            Close Detail_Cursor_From_Issue
            Deallocate Detail_Cursor_From_Issue
            
            Fetch Header_Cursor_Create_From Into 
                @FromWarehouseCode,
                @ExternalCompanyId,
                @WarehouseId,
                @Ordernumber,
                @DeliveryDate,
                @IssueId
    End
    
    Close Header_Cursor_Create_From
    Deallocate Header_Cursor_Create_From
    
  return
end
