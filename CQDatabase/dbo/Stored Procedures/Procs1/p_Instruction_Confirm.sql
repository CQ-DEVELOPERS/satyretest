﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Confirm
  ///   Filename       : p_Instruction_Confirm.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Nov 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Confirm
(
 @instructionId     int,
 @confirmedQuantity float,
 @operatorId        int,
 @palletId			int = null,
 @PalletIdStatusId	int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @palletId is not null
  begin
  exec @Error = p_Pallet_Update
   @palletId	=	@palletId,
   @StatusId    =	@PalletIdStatusId
  
  if @Error <> 0
  print @Error
    goto error  
  end
  
  if @confirmedQuantity > (select Quantity from Instruction where InstructionId = @instructionId)
    return
  
  select @StatusId = dbo.ufn_StatusId('I','S')
  
  begin transaction
  
  if exists(select top 1 1
              from Instruction
             where InstructionId = @instructionId
               and StoreLocationId is null)
  begin
    set @Error = -1
    goto error
  end
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId = @InstructionId,
   @Confirmed     = 0
  
  if @Error <> 0	
    goto error
  
  waitfor delay '00:00:01'
  
  exec @Error = p_Instruction_Update
   @InstructionId     = @InstructionId,
   @StatusId          = @StatusId,
   @StartDate         = @GetDate,
   @ConfirmedQuantity = @confirmedQuantity
  
  if @Error <> 0
    goto error
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId = @InstructionId,
   @Confirmed     = 1
  
  if @Error <> 0
    goto error
  
  
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Instruction_Confirm'); 
    rollback transaction
    return @Error
end
