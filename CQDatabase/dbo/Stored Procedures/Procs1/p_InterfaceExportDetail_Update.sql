﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportDetail_Update
  ///   Filename       : p_InterfaceExportDetail_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:59
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceExportDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportHeaderId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @LineNumber int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(100) = null,
  ///   @SKUCode nvarchar(100) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Weight float = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @OrderQuantity float = null,
  ///   @RejectQuantity float = null,
  ///   @ReceivedQuantity float = null,
  ///   @AcceptedQuantity float = null,
  ///   @DeliveryNoteQuantity float = null,
  ///   @ReasonCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportDetail_Update
(
 @InterfaceExportHeaderId int = null,
 @ForeignKey nvarchar(60) = null,
 @LineNumber int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(100) = null,
 @SKUCode nvarchar(100) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Weight float = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @OrderQuantity float = null,
 @RejectQuantity float = null,
 @ReceivedQuantity float = null,
 @AcceptedQuantity float = null,
 @DeliveryNoteQuantity float = null,
 @ReasonCode nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceExportDetail
     set InterfaceExportHeaderId = isnull(@InterfaceExportHeaderId, InterfaceExportHeaderId),
         ForeignKey = isnull(@ForeignKey, ForeignKey),
         LineNumber = isnull(@LineNumber, LineNumber),
         ProductCode = isnull(@ProductCode, ProductCode),
         Product = isnull(@Product, Product),
         SKUCode = isnull(@SKUCode, SKUCode),
         Batch = isnull(@Batch, Batch),
         Quantity = isnull(@Quantity, Quantity),
         Weight = isnull(@Weight, Weight),
         Additional1 = isnull(@Additional1, Additional1),
         Additional2 = isnull(@Additional2, Additional2),
         Additional3 = isnull(@Additional3, Additional3),
         Additional4 = isnull(@Additional4, Additional4),
         Additional5 = isnull(@Additional5, Additional5),
         OrderQuantity = isnull(@OrderQuantity, OrderQuantity),
         RejectQuantity = isnull(@RejectQuantity, RejectQuantity),
         ReceivedQuantity = isnull(@ReceivedQuantity, ReceivedQuantity),
         AcceptedQuantity = isnull(@AcceptedQuantity, AcceptedQuantity),
         DeliveryNoteQuantity = isnull(@DeliveryNoteQuantity, DeliveryNoteQuantity),
         ReasonCode = isnull(@ReasonCode, ReasonCode) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
