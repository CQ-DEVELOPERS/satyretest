﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceBOM_List
  ///   Filename       : p_InterfaceBOM_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:46
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceBOM table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceBOM.InterfaceBOMId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceBOM_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceBOMId
        ,null as 'InterfaceBOM'
  union
  select
         InterfaceBOM.InterfaceBOMId
        ,InterfaceBOM.InterfaceBOMId as 'InterfaceBOM'
    from InterfaceBOM
  
end
