﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Despatch_Create_Issue
  ///   Filename       : p_Despatch_Create_Issue.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Ruan Groenewald
  ///   Modified Date  : 06 Oct 2011
  ///   Details        : Added kitting location check
  /// </newpara>
*/
CREATE procedure p_Despatch_Create_Issue
(
 @OutboundDocumentId int,
 @OutboundLineId     int = null,
 @OperatorId         int,
 @Remarks	         nvarchar(500) = null,
 @IssueId            int = null output,
 @IssueLineId        int = null output
)

as
begin
	 set nocount on;
  
  declare @TableOutboundLine as
  table(
        OutboundLineId                 int              null,
        StorageUnitId                  int              null,
        BatchId                        int              null,
        StorageUnitBatchId             int              null,
        Quantity                       float            null
       );
  
  declare @Error                  int,
          @Errormsg               nvarchar(500),
          @GetDate                datetime,
          @OutboundDocumentTypeId int,
          @PriorityId             int,
          @WarehouseId            int,
          @StatusId               int,
          @DeliveryDate           datetime,
          @StorageUnitBatchId     int,
          @Quantity               float,
          @LocationId             int,
          @DespatchBay            int,
          @ExternalCompanyId      int,
          @RouteId                int,
          @OutboundDocumentTypeCode nvarchar(10),
          @DropSequence           int,
          @CreateDate             datetime,
          @ReserveBatch           bit = 0,
          @AreaType               nvarchar(10),
          @PrincipalId            int,
          @ECPriorityId           int,
          @Branded				  bit,
          @OrderNumber			  nvarchar(30),
          @Downsizing			  bit
  
	set @Remarks = LEFT(@Remarks, 250);
	set @OrderNumber = (select OrderNumber 
						  from OutboundDocument 
						 where OutboundDocumentId = @OutboundDocumentId)
	set @Branded = 0
	set @Branded = (select top 1 1 
				               from InterfaceImportSOHeader 
				              where OrderNumber = @OrderNumber 
				                and (Branding like '%yes%'or Branding like '%true%'))
				       
    set @Downsizing = 0
   	set @Downsizing = (select top 1 1 
				                     from InterfaceImportSOHeader 
				                    where OrderNumber = @OrderNumber 
				                      and (Downsizing like '%yes%'or Downsizing like '%true%'))
	
  select @GetDate = dbo.ufn_Getdate()
  
--  if exists(select 1 from Issue where OutboundDocumentId = @OutboundDocumentId)
--  begin
--    begin transaction
--    set @Errormsg = 'Issue has already been inserted'
--    set @Error = -1
--    goto error
--
  --end
  
  select @OutboundDocumentTypeId = OutboundDocumentTypeId,
         @WarehouseId           = WarehouseId,
         @CreateDate            = CreateDate,
         @DeliveryDate          = DeliveryDate,
         @ExternalCompanyId     = ExternalCompanyId,
         @PrincipalId           = PrincipalId
    from OutboundDocument (nolock)
   where OutboundDocumentId = @OutboundDocumentId
  
  select @RouteId = RouteId,
         @DropSequence = DropSequence,
         @ECPriorityId = PriorityId
    from ExternalCompany (nolock)
   where ExternalCompanyId = @ExternalCompanyId
  
  select @PriorityId               = PriorityId,
         @OutboundDocumentTypeCode = OutboundDocumentTypeCode,
         @LocationId               = CheckingLane,
         @DespatchBay              = DespatchBay,
         @ReserveBatch             = ReserveBatch,
         @AreaType                 = AreaType
    from OutboundDocumentType (nolock)
   where OutboundDocumentTypeId = @OutboundDocumentTypeId
  
  select @StatusId = StatusId
    from Status (nolock)
   where StatusCode = 'W'
     and Type       = 'IS'
  
  insert @TableOutboundLine
        (OutboundLineId,
         StorageUnitId,
         BatchId,
         Quantity)
  select OutboundLineId,
         StorageUnitId,
         BatchId,
         Quantity
    from OutboundLine
   where OutboundDocumentId = @OutboundDocumentId
     and OutboundLineId     = isnull(@OutboundLineId, OutboundLineId)
  select * from @TableOutboundLine
  update il
     set StorageUnitBatchId = sub.StorageUnitBatchId
    from @TableOutboundLine il
    join StorageUnitBatch  sub (nolock) on il.StorageUnitId = sub.StorageUnitId
                                       and il.BatchId       = sub.BatchId
  
  update il
     set StorageUnitBatchId = sub.StorageUnitBatchId
    from @TableOutboundLine il,
         Batch             b   (nolock),
         StorageUnitBatch  sub (nolock)
   where il.StorageUnitId = sub.StorageUnitId
     and il.BatchId      is null
     and b.Batch          = 'Default'
     and b.BatchId        = sub.BatchId
  

  begin transaction
  
  if exists(select 1 from @TableOutboundLine where StorageUnitBatchId is null)
  begin
    set @Errormsg = 'Invalid batch'
    set @Error = -1
    goto error
  end
  
  select @IssueId = IssueId
    from Issue
   where OutboundDocumentId = @OutboundDocumentId
  
  if @IssueId is null
  begin
    if @LocationId is null
    begin
      
      exec @LocationId = p_Checking_Location_Get
        @WarehouseId              = @WarehouseId,
        @OutboundDocumentTypeCode = @OutboundDocumentTypeCode,
        @PriorityId               = @ECPriorityId,
        @PrincipalId              = @PrincipalId
      
      if @LocationId = -1
        set @LocationId = null
    end
    
    if @DespatchBay is null
    begin
      exec @DespatchBay = p_Despatch_Location_Get
        @WarehouseId              = @WarehouseId,
        @OutboundDocumentTypeCode = @OutboundDocumentTypeCode,
        @PriorityId               = @ECPriorityId,
        @PrincipalId              = @PrincipalId
      
      if @DespatchBay = -1
        set @DespatchBay = null
    end
    
    if (select dbo.ufn_Configuration(221, @WarehouseId)) = 1
      set @DropSequence = @OutboundDocumentId
    
    if (select dbo.ufn_Configuration(254, @WarehouseId)) = 1
      set @DeliveryDate = @CreateDate
    
    if @OutboundDocumentTypeCode like 'BOG%'
      set @RouteId = null
    
    exec @Error = p_Issue_Insert
     @IssueId               = @IssueId output,
     @OutboundDocumentId    = @OutboundDocumentId,
     @PriorityId            = @PriorityId,
     @WarehouseId           = @WarehouseId,
     @LocationId            = @LocationId,
     @DespatchBay           = @DespatchBay,
     @StatusId              = @StatusId,
     @OperatorId            = @OperatorId,
     @RouteId               = @RouteId,
     @DeliveryNoteNumber    = null,
     @DeliveryDate          = @DeliveryDate,
     @SealNumber            = null,
     @VehicleRegistration   = null,
     @Remarks               = @Remarks,
     @DropSequence          = @DropSequence,
     @ReserveBatch          = @ReserveBatch,
     @AreaType              = @AreaType
    
    if @Error <> 0
    begin
      set @Errormsg = 'Error executing p_Issue_Insert (1)'
      set @Error = -1
      goto error
    end
  end
  else
  begin
    exec @Error = p_Issue_Update
     @IssueId               = @IssueId,
     @Remarks               = @Remarks
    
    if @Error <> 0
    begin
      set @Errormsg = 'Error executing p_Issue_Update'
      set @Error = -1
      goto error
    end
  end
  
  update i
     set NumberOfLines = (select count(1)
                            from OutboundLine ol (nolock)
                           where i.OutboundDocumentId = ol.OutboundDocumentId)
    from Issue i
   where i.IssueId = @IssueId
    
  update i
       set Units = (select isnull(sum(ol.Quantity),0)
                      from OutboundLine ol (nolock)
                     where i.OutboundDocumentId = ol.OutboundDocumentId)
      from Issue i

   where i.IssueId = @IssueId
  
  while exists(select top 1 1 from @TableOutboundLine)
  begin
    select top 1 @OutboundLineId     = OutboundLineId,
                 @StorageUnitBatchId = StorageUnitBatchId,
                 @Quantity           = Quantity
      from @TableOutboundLine
    
    delete @TableOutboundLine where OutboundLineId = @OutboundLineId
    
    set @IssueLineId = null
    
    select @IssueLineId = IssueLineId
      from IssueLine (nolock)
     where OutboundLineId     = @OutboundLineId
       and StorageUnitBatchId = @StorageUnitBatchId
    
    if @IssueLineId is null
    begin
      exec @Error = p_IssueLine_Insert
       @IssueLineId        = @IssueLineId output,
       @IssueId            = @IssueId,
       @OutboundLineId     = @OutboundLineId,
       @StorageUnitBatchId = @StorageUnitBatchId,
       @StatusId           = @StatusId,
       @OperatorId         = @OperatorId,
       @Quantity           = @Quantity,
       @ConfirmedQuatity   = 0
      
      if @Error <> 0
      begin
        set @Errormsg = 'Error executing p_Issue_Insert - Insert IssueLine'
        set @Error = -1
        goto error
      end
    end
    else
    begin
      exec @Error = p_IssueLine_Update
       @IssueLineId        = @IssueLineId,
       @StorageUnitBatchId = @StorageUnitBatchId,
       @StatusId           = @StatusId,
       @OperatorId         = @OperatorId,
       @Quantity           = @Quantity
      
      if @Error <> 0
      begin
        set @Errormsg = 'Error executing p_Issue_Insert - Update Issue'
        set @Error = -1
        goto error
      end
    end
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
 
