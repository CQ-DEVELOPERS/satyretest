﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Culture_Select
  ///   Filename       : p_Culture_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:37
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Culture table.
  /// </remarks>
  /// <param>
  ///   @CultureId int = null 
  /// </param>
  /// <returns>
  ///   Culture.CultureId,
  ///   Culture.CultureName 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Culture_Select
(
 @CultureId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Culture.CultureId
        ,Culture.CultureName
    from Culture
   where isnull(Culture.CultureId,'0')  = isnull(@CultureId, isnull(Culture.CultureId,'0'))
  
end
