﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Split
  ///   Filename       : p_Instruction_Split.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Apr 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Split
(
 @InstructionId int,
 @SplitQuantity float,
 @ReasonId      int,
 @OperatorId    int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @Quantity          float,
          @ConfirmedQuantity float
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @ReasonId = -1
    set @ReasonId = null
  
  if @SplitQuantity < 0
  begin
    set @Error = -1
    set @ErrorMsg = 'Error executing p_Instruction_Split - quantity must be greater than 0'
    goto error
  end
  
  select @Quantity          = Quantity - @SplitQuantity,
         @ConfirmedQuantity = ConfirmedQuantity - @SplitQuantity
    from Instruction (nolock)
   where InstructionId = @InstructionId
  
  begin transaction
  
  if @ConfirmedQuantity <= 0
  begin
    set @Error = -1
    set @ErrorMsg = 'Error executing p_Instruction_Split - cannot split amount greater than avaiable'
    goto error
  end
  
  -- Set the old instruction amount
  exec @Error = p_Instruction_Update
   @InstructionId     = @instructionId,
   @Quantity          = @Quantity,
   @ConfirmedQuantity = @ConfirmedQuantity
  
  if @Error <> 0
    goto error
  
  -- Insert new Instruction
  insert Instruction
        (ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         ReceiptLineId,
         IssueLineId,
         InstructionRefId,
         Quantity,
         ConfirmedQuantity,
         Weight,
         ConfirmedWeight,
         PalletId,
         CreateDate,
         StartDate,
         EndDate,
         Picked,
         Stored,
         CheckQuantity,
         CheckWeight,
         OutboundShipmentId,
         DropSequence)
  select @ReasonId,
         InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         ReceiptLineId,
         IssueLineId,
         @InstructionId,
         @SplitQuantity,
         @SplitQuantity,
         null,
         null,
         PalletId,
         @GetDate,
         null,
         null,
         null,
         null,
         null,
         null,
         OutboundShipmentId,
         DropSequence
    from Instruction
   where InstructionId = @InstructionId
  
  select @Error = @@Error, @instructionId = scope_identity()
  
  if @Error <> 0
    goto error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
