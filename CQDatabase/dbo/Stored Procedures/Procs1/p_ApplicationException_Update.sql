﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ApplicationException_Update
  ///   Filename       : p_ApplicationException_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:36
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ApplicationException table.
  /// </remarks>
  /// <param>
  ///   @ApplicationExceptionId int = null,
  ///   @PriorityLevel int = null,
  ///   @Module nvarchar(100) = null,
  ///   @Page nvarchar(100) = null,
  ///   @Method nvarchar(200) = null,
  ///   @ErrorMsg ntext = null,
  ///   @CreateDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ApplicationException_Update
(
 @ApplicationExceptionId int = null,
 @PriorityLevel int = null,
 @Module nvarchar(100) = null,
 @Page nvarchar(100) = null,
 @Method nvarchar(200) = null,
 @ErrorMsg ntext = null,
 @CreateDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @ApplicationExceptionId = '-1'
    set @ApplicationExceptionId = null;
  
	 declare @Error int
 
  update ApplicationException
     set PriorityLevel = isnull(@PriorityLevel, PriorityLevel),
         Module = isnull(@Module, Module),
         Page = isnull(@Page, Page),
         Method = isnull(@Method, Method),
         ErrorMsg = isnull(@ErrorMsg, ErrorMsg),
         CreateDate = isnull(@CreateDate, CreateDate) 
   where ApplicationExceptionId = @ApplicationExceptionId
  
  select @Error = @@Error
  
  
  return @Error
  
end
