﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DI_Planning_Line_Select
  ///   Filename       : p_DI_Planning_Line_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DI_Planning_Line_Select
(
 @OutboundShipmentId int = null,
 @IssueId int = null
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   IssueLineId                     int,
   IssueId                         int,
   OrderNumber                     nvarchar(30),
   StorageUnitBatchId              int,
   StorageUnitId                   int,
   ProductCode                     nvarchar(30),
   Product                         nvarchar(50),
   BatchId                         int,
   Batch                           nvarchar(50),
   SKUCode                         nvarchar(50),
   OrderQuantity                   float,
   ConfirmedQuatity                float,
   AllocatedQuantity               float,
   Status                          nvarchar(50),
   StatusCode                      nvarchar(10),
   AvailableQuantity               float,
   AvailabilityIndicator           nvarchar(20),
   AvailablePercentage             numeric(13,2),
   LocationsAllocated              bit default 0,
   AreaType                        nvarchar(10)
  )
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null;
  
  if @OutboundShipmentId is not null
     set @IssueId = null
  
  if @IssueId = -1
    set @IssueId = null;
  
  if @IssueId is not null
  begin
    insert @TableResult
          (IssueLineId,
           IssueId,
           OrderNumber,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           OrderQuantity,
           ConfirmedQuatity,
           Status,
           StatusCode,
           AreaType)
    select il.IssueLineId,
           il.IssueId,
           od.OrderNumber,
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           ol.Quantity,
           il.ConfirmedQuatity,
           s.Status,
           s.StatusCode,
           isnull(r.AreaType,'')
      from IssueLine        il  (nolock)
      join Issue            r   (nolock) on il.IssueId            = r.IssueId
      join OutboundLine     ol  (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument od  (nolock) on r.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status           s   (nolock) on il.StatusId           = s.StatusId
     where il.IssueId = @IssueId
  end
  else if @OutboundShipmentId is not null
  begin
    insert @TableResult
          (IssueLineId,
           IssueId,
           OrderNumber,
           StorageUnitBatchId,
           StorageUnitId,
           BatchId,
           OrderQuantity,
           ConfirmedQuatity,
           Status,
           StatusCode,
           AreaType)
    select min(il.IssueLineId),
           min(il.IssueId),
           left(od.OrderNumber, 6),
           sub.StorageUnitBatchId,
           sub.StorageUnitId,
           sub.BatchId,
           sum(ol.Quantity),
           sum(il.ConfirmedQuatity),
           s.Status,
           s.StatusCode,
           isnull(r.AreaType,'')
      from IssueLine        il  (nolock)
      join Issue            r   (nolock) on il.IssueId            = r.IssueId
      join OutboundLine     ol  (nolock) on il.OutboundLineId     = ol.OutboundLineId
      join OutboundDocument od  (nolock) on r.OutboundDocumentId  = od.OutboundDocumentId
      join StorageUnitBatch sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
      join Status           s   (nolock) on il.StatusId           = s.StatusId
      join OutboundShipmentIssue osi (nolock) on il.IssueId = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
    group by left(od.OrderNumber, 6),
             sub.StorageUnitBatchId,
             sub.StorageUnitId,
             sub.BatchId,
             s.Status,
             s.StatusCode,
             isnull(r.AreaType,'')
  end
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         Batch       = b.Batch,
         SKUCode     = sku.SKUCode
    from @TableResult tr
    join StorageUnit      su  (nolock) on tr.StorageUnitId      = su.StorageUnitId
    join Product          p   (nolock) on su.ProductId          = p.ProductId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Batch            b   (nolock) on tr.BatchId           = b.BatchId
  
  
  update tr
     set AllocatedQuantity = (select sum(i.Quantity)
                                from IssueLineInstruction ili (nolock)
                                join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
                               where tr.IssueLineId = ili.IssueLineId
                                 and i.PickLocationId is not null)
    from @TableResult tr
  
  update @TableResult
     set AllocatedQuantity = 0
   where AllocatedQuantity is null
  
  update tr
     set LocationsAllocated = 1
    from @TableResult tr
    join IssueLineInstruction ili (nolock) on tr.IssueLineId = ili.IssueLineId
    join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
   where i.PickLocationId is not null
  
  --update tr
  --   set LocationsAllocated = 0
  --  from @TableResult tr
  -- where not exists(select top 1 1 from IssueLineInstruction ili (nolock) where tr.IssueLineId = ili.IssueLineId)
  
  --update tr
  --   set AvailableQuantity = soh.AvailableQuantity
  --  from @TableResult tr
  --  join SUSOH       soh (nolock) on tr.StorageUnitId = soh.StorageUnitId
  --                               and tr.AreaType      = soh.AreaType
  
  update tr
     set AvailableQuantity = (select sum(subl.ActualQuantity - subl.ReservedQuantity) + case when StatusCode in ('RL','S') 
                                                                                             then tr.OrderQuantity
                                                                                             else 0
                                                                                             end
                                from StorageUnitBatchLocation subl (nolock)
                                join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
                                join AreaLocation               al (nolock) on subl.LocationId         = al.LocationId
                                join Area                        a (nolock) on al.AreaId               = a.AreaId
                               where a.StockOnHand     = 1
                                 and sub.StorageUnitId = tr.StorageUnitId
                                 and a.AreaType        in (tr.AreaType,'Backup'))
    from @TableResult tr
  
  update @TableResult
     set AvailableQuantity = 0
   where AvailableQuantity is null
  
  update @TableResult
     set AvailablePercentage = (convert(numeric(13,3), AllocatedQuantity + AvailableQuantity) / convert(numeric(13,3), OrderQuantity)) * 100
   where AllocatedQuantity + AvailableQuantity != 0
     and OrderQuantity > 0
  
  update @TableResult
     set AvailablePercentage = 100
   where AvailablePercentage > 100
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailablePercentage = 0
   where AvailablePercentage < 0
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where AvailableQuantity <= 0
  
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where AvailableQuantity > 0
  
  update @TableResult
     set AvailabilityIndicator = 'Green'
   where (AvailableQuantity >= OrderQuantity)
--      or OrderQuantity = AllocatedQuantity)
  
  select IssueLineId,
         IssueId,
         OrderNumber,
         StorageUnitBatchId,
         StorageUnitId,
         ProductCode,
         Product,
         Batch,
         SKUCode,
         OrderQuantity,
         ConfirmedQuatity,
         Status,
         AvailabilityIndicator,
         AvailablePercentage,
         AvailableQuantity,
         LocationsAllocated
    from @TableResult
  order by ProductCode,
           Product,
           Batch,
           SKUCode,
           OrderQuantity
end
