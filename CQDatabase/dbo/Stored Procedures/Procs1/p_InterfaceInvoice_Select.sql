﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceInvoice_Select
  ///   Filename       : p_InterfaceInvoice_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:22
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceInvoice table.
  /// </remarks>
  /// <param>
  ///   @InterfaceInvoiceId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceInvoice.InterfaceInvoiceId,
  ///   InterfaceInvoice.OrderNumber,
  ///   InterfaceInvoice.InvoiceNumber,
  ///   InterfaceInvoice.ProcessedDate,
  ///   InterfaceInvoice.RecordStatus 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceInvoice_Select
(
 @InterfaceInvoiceId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceInvoice.InterfaceInvoiceId
        ,InterfaceInvoice.OrderNumber
        ,InterfaceInvoice.InvoiceNumber
        ,InterfaceInvoice.ProcessedDate
        ,InterfaceInvoice.RecordStatus
    from InterfaceInvoice
   where isnull(InterfaceInvoice.InterfaceInvoiceId,'0')  = isnull(@InterfaceInvoiceId, isnull(InterfaceInvoice.InterfaceInvoiceId,'0'))
  
end
