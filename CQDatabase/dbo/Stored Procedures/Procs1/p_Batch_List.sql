﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_List
  ///   Filename       : p_Batch_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Sep 2014 15:23:48
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Batch table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Batch.BatchId,
  ///   Batch.Batch 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as BatchId
        ,'{All}' as Batch
  union
  select
         Batch.BatchId
        ,Batch.Batch
    from Batch
  order by Batch
  
end
