﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportSOHeader_Insert
  ///   Filename       : p_InterfaceExportSOHeader_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:12
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceExportSOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportSOHeaderId int = null output,
  ///   @IssueId int = null,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @RecordType nvarchar(60) = null,
  ///   @RecordStatus char(1) = null,
  ///   @CustomerCode nvarchar(60) = null,
  ///   @Customer nvarchar(510) = null,
  ///   @Address nvarchar(510) = null,
  ///   @FromWarehouseCode nvarchar(20) = null,
  ///   @ToWarehouseCode nvarchar(20) = null,
  ///   @Route nvarchar(100) = null,
  ///   @DeliveryDate datetime = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @NumberOfLines int = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportSOHeader.InterfaceExportSOHeaderId,
  ///   InterfaceExportSOHeader.IssueId,
  ///   InterfaceExportSOHeader.PrimaryKey,
  ///   InterfaceExportSOHeader.OrderNumber,
  ///   InterfaceExportSOHeader.RecordType,
  ///   InterfaceExportSOHeader.RecordStatus,
  ///   InterfaceExportSOHeader.CustomerCode,
  ///   InterfaceExportSOHeader.Customer,
  ///   InterfaceExportSOHeader.Address,
  ///   InterfaceExportSOHeader.FromWarehouseCode,
  ///   InterfaceExportSOHeader.ToWarehouseCode,
  ///   InterfaceExportSOHeader.Route,
  ///   InterfaceExportSOHeader.DeliveryDate,
  ///   InterfaceExportSOHeader.Remarks,
  ///   InterfaceExportSOHeader.NumberOfLines,
  ///   InterfaceExportSOHeader.Additional1,
  ///   InterfaceExportSOHeader.Additional2,
  ///   InterfaceExportSOHeader.Additional3,
  ///   InterfaceExportSOHeader.Additional4,
  ///   InterfaceExportSOHeader.Additional5,
  ///   InterfaceExportSOHeader.Additional6,
  ///   InterfaceExportSOHeader.Additional7,
  ///   InterfaceExportSOHeader.Additional8,
  ///   InterfaceExportSOHeader.Additional9,
  ///   InterfaceExportSOHeader.Additional10,
  ///   InterfaceExportSOHeader.ProcessedDate,
  ///   InterfaceExportSOHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportSOHeader_Insert
(
 @InterfaceExportSOHeaderId int = null output,
 @IssueId int = null,
 @PrimaryKey nvarchar(60) = null,
 @OrderNumber nvarchar(60) = null,
 @RecordType nvarchar(60) = null,
 @RecordStatus char(1) = null,
 @CustomerCode nvarchar(60) = null,
 @Customer nvarchar(510) = null,
 @Address nvarchar(510) = null,
 @FromWarehouseCode nvarchar(20) = null,
 @ToWarehouseCode nvarchar(20) = null,
 @Route nvarchar(100) = null,
 @DeliveryDate datetime = null,
 @Remarks nvarchar(510) = null,
 @NumberOfLines int = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceExportSOHeaderId = '-1'
    set @InterfaceExportSOHeaderId = null;
  
	 declare @Error int
 
  insert InterfaceExportSOHeader
        (IssueId,
         PrimaryKey,
         OrderNumber,
         RecordType,
         RecordStatus,
         CustomerCode,
         Customer,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         Route,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10,
         ProcessedDate,
         InsertDate)
  select @IssueId,
         @PrimaryKey,
         @OrderNumber,
         @RecordType,
         @RecordStatus,
         @CustomerCode,
         @Customer,
         @Address,
         @FromWarehouseCode,
         @ToWarehouseCode,
         @Route,
         @DeliveryDate,
         @Remarks,
         @NumberOfLines,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9,
         @Additional10,
         @ProcessedDate,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error, @InterfaceExportSOHeaderId = scope_identity()
  
  
  return @Error
  
end
