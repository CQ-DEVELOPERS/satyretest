﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSKU_Search
  ///   Filename       : p_InterfaceImportSKU_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:06
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportSKU table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSKUId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportSKU.InterfaceImportSKUId,
  ///   InterfaceImportSKU.RecordStatus,
  ///   InterfaceImportSKU.SKUCode,
  ///   InterfaceImportSKU.SKU,
  ///   InterfaceImportSKU.UnitOfMeasure,
  ///   InterfaceImportSKU.Quantity,
  ///   InterfaceImportSKU.AlternatePallet,
  ///   InterfaceImportSKU.SubstitutePallet,
  ///   InterfaceImportSKU.Litres,
  ///   InterfaceImportSKU.HostId,
  ///   InterfaceImportSKU.ProcessedDate,
  ///   InterfaceImportSKU.Insertdate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSKU_Search
(
 @InterfaceImportSKUId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceImportSKUId = '-1'
    set @InterfaceImportSKUId = null;
  
 
  select
         InterfaceImportSKU.InterfaceImportSKUId
        ,InterfaceImportSKU.RecordStatus
        ,InterfaceImportSKU.SKUCode
        ,InterfaceImportSKU.SKU
        ,InterfaceImportSKU.UnitOfMeasure
        ,InterfaceImportSKU.Quantity
        ,InterfaceImportSKU.AlternatePallet
        ,InterfaceImportSKU.SubstitutePallet
        ,InterfaceImportSKU.Litres
        ,InterfaceImportSKU.HostId
        ,InterfaceImportSKU.ProcessedDate
        ,InterfaceImportSKU.Insertdate
    from InterfaceImportSKU
   where isnull(InterfaceImportSKU.InterfaceImportSKUId,'0')  = isnull(@InterfaceImportSKUId, isnull(InterfaceImportSKU.InterfaceImportSKUId,'0'))
  
end
