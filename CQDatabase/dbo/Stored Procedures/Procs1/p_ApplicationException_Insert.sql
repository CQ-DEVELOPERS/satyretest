﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ApplicationException_Insert
  ///   Filename       : p_ApplicationException_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:35
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ApplicationException table.
  /// </remarks>
  /// <param>
  ///   @ApplicationExceptionId int = null output,
  ///   @PriorityLevel int = null,
  ///   @Module nvarchar(100) = null,
  ///   @Page nvarchar(100) = null,
  ///   @Method nvarchar(200) = null,
  ///   @ErrorMsg ntext = null,
  ///   @CreateDate datetime = null 
  /// </param>
  /// <returns>
  ///   ApplicationException.ApplicationExceptionId,
  ///   ApplicationException.PriorityLevel,
  ///   ApplicationException.Module,
  ///   ApplicationException.Page,
  ///   ApplicationException.Method,
  ///   ApplicationException.ErrorMsg,
  ///   ApplicationException.CreateDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ApplicationException_Insert
(
 @ApplicationExceptionId int = null output,
 @PriorityLevel int = null,
 @Module nvarchar(100) = null,
 @Page nvarchar(100) = null,
 @Method nvarchar(200) = null,
 @ErrorMsg ntext = null,
 @CreateDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @ApplicationExceptionId = '-1'
    set @ApplicationExceptionId = null;
  
	 declare @Error int
 
  insert ApplicationException
        (PriorityLevel,
         Module,
         Page,
         Method,
         ErrorMsg,
         CreateDate)
  select @PriorityLevel,
         @Module,
         @Page,
         @Method,
         @ErrorMsg,
         @CreateDate 
  
  select @Error = @@Error, @ApplicationExceptionId = scope_identity()
  
  
  return @Error
  
end
