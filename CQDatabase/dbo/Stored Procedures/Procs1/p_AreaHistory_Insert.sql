﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaHistory_Insert
  ///   Filename       : p_AreaHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Jul 2014 09:46:56
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the AreaHistory table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @WarehouseId int = null,
  ///   @Area nvarchar(100) = null,
  ///   @AreaCode nvarchar(20) = null,
  ///   @StockOnHand bit = null,
  ///   @Replenish bit = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @AreaSequence int = null,
  ///   @AreaType nvarchar(20) = null,
  ///   @WarehouseCode nvarchar(60) = null,
  ///   @MinimumShelfLife float = null,
  ///   @Undefined bit = null,
  ///   @STEKPI int = null,
  ///   @BatchTracked bit = null,
  ///   @StockTake bit = null,
  ///   @NarrowAisle bit = null,
  ///   @Interleaving bit = null,
  ///   @PandD bit = null,
  ///   @SmallPick bit = null,
  ///   @AutoLink bit = null,
  ///   @WarehouseCode2 nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   AreaHistory.AreaId,
  ///   AreaHistory.WarehouseId,
  ///   AreaHistory.Area,
  ///   AreaHistory.AreaCode,
  ///   AreaHistory.StockOnHand,
  ///   AreaHistory.Replenish,
  ///   AreaHistory.CommandType,
  ///   AreaHistory.InsertDate,
  ///   AreaHistory.AreaSequence,
  ///   AreaHistory.AreaType,
  ///   AreaHistory.WarehouseCode,
  ///   AreaHistory.MinimumShelfLife,
  ///   AreaHistory.Undefined,
  ///   AreaHistory.STEKPI,
  ///   AreaHistory.BatchTracked,
  ///   AreaHistory.StockTake,
  ///   AreaHistory.NarrowAisle,
  ///   AreaHistory.Interleaving,
  ///   AreaHistory.PandD,
  ///   AreaHistory.SmallPick,
  ///   AreaHistory.AutoLink,
  ///   AreaHistory.WarehouseCode2 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaHistory_Insert
(
 @AreaId int = null,
 @WarehouseId int = null,
 @Area nvarchar(100) = null,
 @AreaCode nvarchar(20) = null,
 @StockOnHand bit = null,
 @Replenish bit = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @AreaSequence int = null,
 @AreaType nvarchar(20) = null,
 @WarehouseCode nvarchar(60) = null,
 @MinimumShelfLife float = null,
 @Undefined bit = null,
 @STEKPI int = null,
 @BatchTracked bit = null,
 @StockTake bit = null,
 @NarrowAisle bit = null,
 @Interleaving bit = null,
 @PandD bit = null,
 @SmallPick bit = null,
 @AutoLink bit = null,
 @WarehouseCode2 nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert AreaHistory
        (AreaId,
         WarehouseId,
         Area,
         AreaCode,
         StockOnHand,
         Replenish,
         CommandType,
         InsertDate,
         AreaSequence,
         AreaType,
         WarehouseCode,
         MinimumShelfLife,
         Undefined,
         STEKPI,
         BatchTracked,
         StockTake,
         NarrowAisle,
         Interleaving,
         PandD,
         SmallPick,
         AutoLink,
         WarehouseCode2)
  select @AreaId,
         @WarehouseId,
         @Area,
         @AreaCode,
         @StockOnHand,
         @Replenish,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @AreaSequence,
         @AreaType,
         @WarehouseCode,
         @MinimumShelfLife,
         @Undefined,
         @STEKPI,
         @BatchTracked,
         @StockTake,
         @NarrowAisle,
         @Interleaving,
         @PandD,
         @SmallPick,
         @AutoLink,
         @WarehouseCode2 
  
  select @Error = @@Error
  
  
  return @Error
  
end
