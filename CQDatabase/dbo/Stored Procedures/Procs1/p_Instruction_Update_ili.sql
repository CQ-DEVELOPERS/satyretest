﻿   
/*  
  /// <summary>  
  ///   Procedure Name : p_Instruction_Update_ili  
  ///   Filename       : p_Instruction_Update_ili.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 04 Mar 2008  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Instruction_Update_ili  
(  
 @InstructionId    int,  
 @InstructionRefId int = null,  
 @insConfirmed     float  
)  
  
as  
begin  
  set nocount on;  
    
  declare @IssueLine as table  
  (  
   IssueLineId       int,  
   InstructionId     int,  
   Quantity          float,  
   ConfirmedQuantity float  
  )  
    
  declare @Instruction as table  
  (  
   InstructionId int  
  )  
    
  declare @Error        int,  
          @IssueLineId  int,  
          @iliConfirmed float,  
          @iliQuantity  float  
    
  if @InstructionRefId is not null  
  begin  
    update i  
       set InstructionRefId = null  
      from Instruction i  
      join IssueLineInstruction ili on i.Instructionid = ili.InstructionId  
     where i.InstructionId = @InstructionId  
       --and i.InstructionRefId is not null  
      
    if @@rowcount > 0  
      set @InstructionRefId = null  
  end  
    
  select @insConfirmed = sum(ConfirmedQuantity)  
    from Instruction (nolock)  
   where (InstructionId    in (@InstructionId,@InstructionRefId)  
       or InstructionRefId in (@InstructionId,@InstructionRefId))  
    
  insert @IssueLine  
        (IssueLineId,  
         InstructionId,  
         Quantity,  
         ConfirmedQuantity)  
  select IssueLineId,  
         InstructionId,  
         Quantity,  
         0--isnull(ConfirmedQuantity, 0)  
    from IssueLineInstruction (nolock)  
   where InstructionId = isnull(@InstructionRefId, @InstructionId)  
    
  while @insConfirmed > 0 and (select count(1) from @IssueLine) > 0  
  begin  
    select top 1 @IssueLineId  = IssueLineId,  
                 @iliQuantity  = Quantity,  
                 @iliConfirmed = isnull(ConfirmedQuantity,0)  
      from @IssueLine  
     where Quantity - ConfirmedQuantity <> 0  
      
    if @insConfirmed > @iliQuantity - @iliConfirmed  
    begin  
      update @IssueLine  
         set ConfirmedQuantity = ConfirmedQuantity + (Quantity - ConfirmedQuantity)  
       where IssueLineId = @IssueLineId  
        
      select @Error = @@Error  
        
      if @Error <> 0  
        goto error  
        
      set @insConfirmed = @insConfirmed - (@iliQuantity - @iliConfirmed)  
    end  
    else  
    begin  
      update @IssueLine  
         set ConfirmedQuantity = ConfirmedQuantity + @insConfirmed  
       where IssueLineId = @IssueLineId  
        
      select @Error = @@Error  
        
      if @Error <> 0  
        goto error  
        
      set @insConfirmed = 0  
    end  
  end  
    
  update ili  
     set ConfirmedQuantity = il.ConfirmedQuantity  
    from @IssueLine            il  
    join IssueLineInstruction ili on il.IssueLineId = ili.IssueLineId  
                                 and il.Instructionid = ili.InstructionId  
   --where (ili.InstructionId = @InstructionId  
   --   or  ili.InstructionId = @InstructionRefId)  
    
  select @Error = @@Error  
    
  if @Error <> 0  
    goto error  
    
  update il  
     set il.ConfirmedQuatity = (select sum(isnull(ili.ConfirmedQuantity,0))  
                                  from IssueLineInstruction ili  
                                 where il.IssueLineId  = ili.IssueLineId)  
--                                   and t.InstructionId = ili.InstructionId)  
    from @IssueLine t  
    join IssueLine il on t.IssueLineId = il.IssueLineId  
    
  select @Error = @@Error  
    
  if @Error <> 0  
    goto error  
    
  return  
    
  error:  
    return @Error  
end  
