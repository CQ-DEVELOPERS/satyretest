﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Description_Value
  ///   Filename       : p_Configuration_Description_Value.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Jan 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Description_Value
(
 @Configuration nvarchar(50),
 @WarehouseId int = 1
)

as
begin
	 set nocount on;
  
  declare @Value int
  
  select @Value = convert(int, [Value])
    from Configuration (nolock)
   where Configuration = @Configuration
     and WarehouseId = @WarehouseId
  
  if @Value is null
    set @Value = 1
  
  select @Value as 'Value'
end
