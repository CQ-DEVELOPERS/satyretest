﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMHeader_Select
  ///   Filename       : p_BOMHeader_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:05
  /// </summary>
  /// <remarks>
  ///   Selects rows from the BOMHeader table.
  /// </remarks>
  /// <param>
  ///   @BOMHeaderId int = null 
  /// </param>
  /// <returns>
  ///   BOMHeader.BOMHeaderId,
  ///   BOMHeader.StorageUnitId,
  ///   BOMHeader.Description,
  ///   BOMHeader.Remarks,
  ///   BOMHeader.Instruction,
  ///   BOMHeader.BOMType,
  ///   BOMHeader.HostId,
  ///   BOMHeader.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMHeader_Select
(
 @BOMHeaderId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         BOMHeader.BOMHeaderId
        ,BOMHeader.StorageUnitId
        ,BOMHeader.Description
        ,BOMHeader.Remarks
        ,BOMHeader.Instruction
        ,BOMHeader.BOMType
        ,BOMHeader.HostId
        ,BOMHeader.Quantity
    from BOMHeader
   where isnull(BOMHeader.BOMHeaderId,'0')  = isnull(@BOMHeaderId, isnull(BOMHeader.BOMHeaderId,'0'))
  
end
