﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportHeader_Parameter
  ///   Filename       : p_InterfaceExportHeader_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:02
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceExportHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceExportHeader.InterfaceExportHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportHeader_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceExportHeaderId
        ,null as 'InterfaceExportHeader'
  union
  select
         InterfaceExportHeader.InterfaceExportHeaderId
        ,InterfaceExportHeader.InterfaceExportHeaderId as 'InterfaceExportHeader'
    from InterfaceExportHeader
  
end
