﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Update
  ///   Filename       : p_Housekeeping_Stock_Take_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Update
(
 @instructionId int,
 @operatorId    int,
 @statusCode    nvarchar(10)
)

as
begin
  set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500) = 'Error executing p_Housekeeping_Stock_Take_Update',
          @GetDate            datetime,
          @Transaction        bit = 0,
          @StatusId           int,
          @NewInstructionId   int,
          @InstructionTypeId	 int,
          @StorageUnitBatchId	int,
          @WarehouseId	       int,
          @JobId	             int,
          @PickLocationId	    int,
          @InstructionRefId	  int,
          @ConfirmedQuantity  float,
          @CreateDate	        datetime,
          @PriorityId         int,
          @ReferenceNumber    nvarchar(30),
          @OldJobId           int,
          @PalletId           int,
          @StorageUnitId      int,
          @ConfirmedWeight    numeric(13,6),
          @ExpectedQuantity   numeric(13,6),
          @PreviousQuantity   numeric(13,6),
          @Quantity           numeric(13,6),
          @FromWarehouseCode  nvarchar(30),
          @Ailse              nvarchar(10)
          
  select @GetDate = dbo.ufn_Getdate()
  
  if @@trancount = 0
    begin
  begin transaction
      set @Transaction = 1
    end
  
  if exists(select 1 from Instruction where InstructionId = @instructionId and isnull(Stored,0) = 1)
  begin
    set @Error = -1
    goto error
  end
    
  update Instruction
     set AuthorisedBy = @operatorId,
         AuthorisedStatus = @statusCode
   where InstructionId = @instructionId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  if @statusCode = 'RC'
  begin
    select @StatusId = dbo.ufn_StatusId('I','F')
    
    exec p_Instruction_Update
     @InstructionId = @instructionId,
     @StatusId      = @StatusId,
     @Picked        = 1, -- Stock is Finished (New count inserted)
     @Stored        = 1  -- Stock is Finished (New count inserted)
    
    if @Error <> 0
      goto error
    
    select @InstructionTypeId	 = InstructionTypeId,
           @StorageUnitBatchId	= StorageUnitBatchId,
           @WarehouseId	       = WarehouseId,
           @JobId              = JobId,
           @PickLocationId	    = PickLocationId,
           @ExpectedQuantity   = isnull(CheckQuantity, Quantity),
           @PreviousQuantity   = Quantity,
           @ConfirmedQuantity	 = isnull(ConfirmedQuantity,0)
      from Instruction
     where InstructionId = @instructionId
    
    select @PriorityId      = PriorityId,
           @ReferenceNumber = ReferenceNumber
      from Job (nolock)
     where JobId        = @JobId
    
    select @Ailse = Ailse
      from Location
     where LocationId = @PickLocationId
    
    select @OldJobId = @JobId,
           @JobId    = null
    
    select top 1
           @JobId           = JobId,
           @PriorityId      = PriorityId,
           @ReferenceNumber = @Ailse
      from Job    j (nolock)
      join Status s (nolock)on j.StatusId = s.StatusId
     where JobId          != @OldJobId
       and ReferenceNumber = @Ailse
       and s.Type          = 'J'
       and s.StatusCode    = @statusCode
    
    if @JobId is null
    begin
      select @StatusId = dbo.ufn_StatusId('J',@statusCode)
      
      exec @Error = p_Job_Insert
       @JobId           = @JobId output,
       @WarehouseId     = @WarehouseId,
       @PriorityId      = @PriorityId,
       @StatusId        = @StatusId,
       @ReferenceNumber = @Ailse
      
      if @Error <> 0
        goto error
    end
    
    select @StatusId = dbo.ufn_StatusId('I','W')
    
    exec @Error = p_Instruction_Insert
     @InstructionId	     = @NewInstructionId output,
     @InstructionTypeId	 = @InstructionTypeId,
     @StorageUnitBatchId	= @StorageUnitBatchId,
     @WarehouseId	       = @WarehouseId,
     @StatusId           = @StatusId,
     @JobId	             = @JobId,
     @OperatorId	        = null,
     @PickLocationId	    = @PickLocationId,
     @InstructionRefId	  = @instructionId,
     @Quantity	          = @ConfirmedQuantity,
     @ConfirmedQuantity  = 0,
     @CreateDate	        = @GetDate,
     @CheckQuantity      = @ExpectedQuantity,
     @PreviousQuantity   = @PreviousQuantity
    
    if @Error <> 0
      goto error
  end
  else if @statusCode = 'A'
  begin
    select @PickLocationId     = PickLocationId,
           @StorageUnitBatchId = StorageUnitBatchId,
           @PalletId           = PalletId,
           @JobId              = JobId,
           @ConfirmedWeight    = ConfirmedWeight,
           @Quantity           = ISNULL(ConfirmedQuantity - Quantity,0)
      from Instruction
     where InstructionId = @instructionId    
    
    exec @Error = p_Instruction_Update
     @InstructionId   = @instructionId,
     @StoreLocationId = @PickLocationId,
     @PickLocationId  = null
    
    if @Error <> 0
      goto error
      
    Insert StockTakeAuthorise
          (InstructionId,
           StatusCode,
           OperatorId,
           InsertDate)
    Select @instructionId,
           @statusCode,
           @operatorId,
           Getdate()
    
    select @Error = @@Error, @Errormsg = 'Insert StockTakeAuthorise'
    
    if @Error <> 0
      goto error
    
    ---- Update ReservedQuantity
    --update subl
    --   set ReservedQuantity = isnull((select sum(isnull(i.ConfirmedQuantity,0))
    --                             from Instruction i (nolock)
    --                             join Status      s (nolock) on i.StatusId = s.StatusId
    --                            where s.StatusCode        in ('W','S')
    --                              and i.Picked             = 0
    --                              and i.CreateDate         > DATEDIFF(dd, -14, @GetDate)
    --                              and i.StorageUnitBatchId = subl.StorageUnitBatchId
    --                              and i.PickLocationId     = subl.LocationId),0)
    --  from StorageUnitBatchLocation subl
    -- where subl.LocationId = @PickLocationId
    
    --select @Error = @@Error, @Errormsg = 'Update ReservedQuantity'
    
    --if @Error <> 0
    --  goto error
    
    ---- Update AllocatedQuantity
    --update subl
    --   set AllocatedQuantity = isnull((select sum(isnull(i.ConfirmedQuantity,0))
    --                             from Instruction i (nolock)
    --                             join Status      s (nolock) on i.StatusId = s.StatusId
    --                            where s.StatusCode        in ('W','S')
    --                              and i.Stored             = 0
    --                              and i.CreateDate         > DATEDIFF(dd, -14, @GetDate)
    --                              and i.StorageUnitBatchId = subl.StorageUnitBatchId
    --                              and i.StoreLocationId    = subl.LocationId),0)
    --  from StorageUnitBatchLocation subl
    -- where subl.LocationId = @PickLocationId
    
    --select @Error = @@Error, @Errormsg = 'Update AllocatedQuantity'
    
    --if @Error <> 0
    --  goto error
    
    if @StorageUnitBatchId is not null
    begin
      exec @Error = p_StorageUnitBatchLocation_Reserve
       @InstructionId = @instructionId,
       @Pick          = 0,
       @Store         = 1,
       @Confirmed     = 1
      
      if @Error <> 0
        goto error
      
      -- Set the Location to Zero
      exec @Error = p_StorageUnitBatchLocation_Update
        @StorageUnitBatchId = @StorageUnitBatchId,
        @LocationId         = @PickLocationId,
        @ActualQuantity     = 0,
        @NettWeight         = @ConfirmedWeight
      
      if @PalletId is not null
      begin
        exec @Error = p_Pallet_Update
         @PalletId   = @PalletId,
         @LocationId = @PickLocationId
        
        if @Error <> 0
          goto error
      end
      
      exec @Error = p_StorageUnitBatchLocation_Allocate
       @InstructionId = @instructionId,
       @Pick          = 1,
       @Store         = 1,
       @Confirmed     = 1
      
      if @Error <> 0
      begin -- Karen 2016-04-07 - added code for Plumblink to check that if soh update is unsuccessful then reset to authorise
        if (select top 1 1
			  from Instruction i (nolock) 
			  join viewSOH soh on i.StoreLocationId = soh.LocationId
			   and i.StorageUnitBatchId = soh.StorageUnitBatchId
			 where soh.ActualQuantity != i.ConfirmedQuantity
			   and i.InstructionId = @instructionId) = 1
        begin
        exec p_Instruction_Update
			 @InstructionId = @InstructionId,
			 @Picked        = 0,
			 @Stored        = 0	
		end		 
        goto error
      end
      
      
   if exists(select top 1 1 from sys.tables where name = 'StockTakeLog')
     insert StockTakeLog
           ([StockTakeLogCode]
           ,[InstructionId]
           ,[Area]
           ,[Location]
           ,[ProductCode]
           ,[Product]
           ,[SKUCode]
           ,[Batch]
           ,[Quantity]
           ,[ConfirmedQuantity]
           ,[ActualQuantity]
           ,[AllocatedQuantity]
           ,[ReservedQuantity]
           ,[InsertDate])
     select 'Authorise Proc'
           ,vi.InstructionId
           ,vi.StoreArea
           ,vi.StoreLocation
           ,vi.ProductCode
           ,vi.Product
           ,vi.SKUCode
           ,vi.Batch
           ,vi.Quantity
           ,vi.ConfirmedQuantity
           ,soh.ActualQuantity
           ,soh.AllocatedQuantity
           ,soh.ReservedQuantity
           ,GETDATE() as 'InsertDate'
       from viewInstruction vi
       left
       join StockTakeAuthorise sta on vi.InstructionId = sta.InstructionId
       left
       join StorageUnitBatchLocation soh on vi.StorageUnitBatchId = soh.StorageUnitBatchId
                                        and vi.StoreLocationId    = soh.LocationId
      where vi.InstructionId = @instructionId
      
      --if @Quantity != 0
      --begin
      --  if not exists(select top 1 1 
      --                  from StockTakeReferenceJob srj (nolock)
      --                  join StockTakeReference     sr (nolock) on srj.StockTakeReferenceId = sr.StockTakeReferenceId
      --                 where srj.JobId = @JobId
      --                   and sr.StockTakeType = 'Wall to Wall')
      --  begin
      --    select @FromWarehouseCode = a.WarehouseCode
      --      from AreaLocation al (nolock)
      --      join Area          a (nolock) on al.AreaId = a.AreaId
      --     where al.LocationId = @PickLocationId
          
      --    exec p_StockAdjustment_Insert
      --     @operatorId         = @operatorId
      --    ,@recordType         = 'STK'
      --    ,@storageUnitBatchId = @StorageUnitBatchId
      --    ,@quantity           = @Quantity
      --    ,@fromWarehouseCode  = @FromWarehouseCode
      --    ,@Msg                = 'Stock take authorise'
          
      --    select @Error = @@Error
          
      --    if @Error <> 0
      --      goto error
      --  end
      --end
    end
    
--    select @StatusId = dbo.ufn_StatusId('I','A')
--    
--    exec p_Instruction_Update
--     @InstructionId = @instructionId,
--     @StatusId      = @StatusId
    
    if @Error <> 0
      goto error
  end
  
  select @StorageUnitId = sub.StorageUnitId
    from Instruction        i
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.InstructionId = @InstructionId
  
  if not exists(select top 1 1
                  from Instruction        i (nolock)
                  join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
                  join Status             s (nolock) on i.StatusId           = s.StatusId
                 where i.JobId           = @JobId
                   and sub.StorageUnitId = @StorageUnitId
                   and s.StatusCode     != 'F')
  begin
    exec p_Instruction_Batch_Swap
     @LocationId    = @PickLocationId,
     @StorageUnitId = @StorageUnitId
  end
  
  if not exists(select top 1 1
                  from Instruction      i (nolock)
                  join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
                  join Status           s (nolock) on i.StatusId = s.StatusId
                 where i.PickLocationId        = @PickLocationId
                   and it.InstructionTypeCode in ('STE','STL','STA','STP')
                   and s.Type                  = 'I'
                   and s.StatusCode           != 'F')
  and 
  exists(select 1
               from StockTakeAuthorise sa (nolock)
              where InstructionId = @instructionId) -- GS 2013-06-18 - added to make sure stock take is authoprised before making location active again.
  begin
    exec @Error = p_Location_Update
     @locationId   = @PickLocationId,
     @StocktakeInd = 0
    
    if @Error <> 0
      goto error
  end
  
  result:
      if @Transaction = 1
  commit transaction
   if exists(select top 1 1 from sys.tables where name = 'StockTakeLog')
     insert StockTakeLog
           ([StockTakeLogCode]
           ,[InstructionId]
           ,[Area]
           ,[Location]
           ,[ProductCode]
           ,[Product]
           ,[SKUCode]
           ,[Batch]
           ,[Quantity]
           ,[ConfirmedQuantity]
           ,[ActualQuantity]
           ,[AllocatedQuantity]
           ,[ReservedQuantity]
           ,[InsertDate])
     select 'Commit'
           ,vi.InstructionId
           ,vi.StoreArea
           ,vi.StoreLocation
           ,vi.ProductCode
           ,vi.Product
           ,vi.SKUCode
           ,vi.Batch
           ,vi.Quantity
           ,vi.ConfirmedQuantity
           ,soh.ActualQuantity
           ,soh.AllocatedQuantity
           ,soh.ReservedQuantity
           ,GETDATE() as 'InsertDate'
       from viewInstruction vi
       left
       join StockTakeAuthorise sta on vi.InstructionId = sta.InstructionId
       left
       join StorageUnitBatchLocation soh on vi.StorageUnitBatchId = soh.StorageUnitBatchId
                                        and vi.StoreLocationId    = soh.LocationId
      where vi.InstructionId = @instructionId
      return 0
  
  error:
      if @Transaction = 1
      begin
        RAISERROR (@Errormsg,11,1)
    rollback transaction
     if exists(select top 1 1 from sys.tables where name = 'StockTakeLog')
     insert StockTakeLog
           ([StockTakeLogCode]
           ,[InstructionId]
           ,[Area]
           ,[Location]
           ,[ProductCode]
           ,[Product]
           ,[SKUCode]
           ,[Batch]
           ,[Quantity]
           ,[ConfirmedQuantity]
           ,[ActualQuantity]
           ,[AllocatedQuantity]
           ,[ReservedQuantity]
           ,[InsertDate])
     select 'Rollback'
           ,vi.InstructionId
           ,vi.StoreArea
           ,vi.StoreLocation
           ,vi.ProductCode
           ,vi.Product
           ,vi.SKUCode
           ,vi.Batch
           ,vi.Quantity
           ,vi.ConfirmedQuantity
           ,soh.ActualQuantity
           ,soh.AllocatedQuantity
           ,soh.ReservedQuantity
           ,GETDATE() as 'InsertDate'
       from viewInstruction vi
       left
       join StockTakeAuthorise sta on vi.InstructionId = sta.InstructionId
       left
       join StorageUnitBatchLocation soh on vi.StorageUnitBatchId = soh.StorageUnitBatchId
                                        and vi.StoreLocationId    = soh.LocationId
      where vi.InstructionId = @instructionId
      end
    return @Error
end
 
