﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Culture_Search
  ///   Filename       : p_Culture_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:36
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Culture table.
  /// </remarks>
  /// <param>
  ///   @CultureId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Culture.CultureId,
  ///   Culture.CultureName 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Culture_Search
(
 @CultureId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @CultureId = '-1'
    set @CultureId = null;
  
 
  select
         Culture.CultureId
        ,Culture.CultureName
    from Culture
   where isnull(Culture.CultureId,'0')  = isnull(@CultureId, isnull(Culture.CultureId,'0'))
  
end
