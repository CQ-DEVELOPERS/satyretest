﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Search
  ///   Filename       : p_Container_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 11:46:28
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the Container table.
  /// </remarks>
  /// <param>
  ///   @ContainerId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   Container.ContainerId,
  ///   Container.PalletId,
  ///   Container.JobId,
  ///   Container.ReferenceNumber,
  ///   Container.StorageUnitBatchId,
  ///   Container.PickLocationId,
  ///   Container.StoreLocationId,
  ///   Container.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Search
(
 @ContainerId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @ContainerId = '-1'
    set @ContainerId = null;
  
 
  select
         Container.ContainerId
        ,Container.PalletId
        ,Container.JobId
        ,Container.ReferenceNumber
        ,Container.StorageUnitBatchId
        ,Container.PickLocationId
        ,Container.StoreLocationId
        ,Container.Quantity
    from Container
   where isnull(Container.ContainerId,'0')  = isnull(@ContainerId, isnull(Container.ContainerId,'0'))
  
end
