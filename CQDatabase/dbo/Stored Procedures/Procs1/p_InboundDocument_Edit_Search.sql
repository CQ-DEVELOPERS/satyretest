﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Edit_Search
  ///   Filename       : p_InboundDocument_Edit_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Edit_Search
(
 @InboundDocumentTypeId int,
 @ExternalCompanyCode   nvarchar(30),
 @ExternalCompany       nvarchar(255),
 @OrderNumber           nvarchar(30),
 @FromDate              datetime,
 @ToDate                datetime
)

as
begin
	 set nocount on;
  
	 declare @StatusId int
    
  select @StatusId = StatusId
    from Status
   where StatusCode = 'N'
     and Type       = 'ID';
  
  select id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         count(1) as 'Number Of Line',
         id.DeliveryDate,
         s.Status,
         idt.InboundDocumentType,
         id.CreateDate
    from InboundDocument     id  (nolock)
    join InboundLine         il  (nolock) on id.InboundDocumentId = il.InboundDocumentId
    join Status              s   (nolock) on id.StatusId          = s.StatusId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    join ExternalCompany     ec  (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
   where idt.InboundDocumentTypeId = isnull(@InboundDocumentTypeId, idt.InboundDocumentTypeId)
     and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode,'') + '%'
     and ec.ExternalCompany     like isnull(@ExternalCompany,'') + '%'
     and id.OrderNumber         like isnull(@OrderNumber,'') + '%'
     and id.DeliveryDate     between @FromDate and @ToDate
     and s.StatusId                = @StatusId
  group by id.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           id.DeliveryDate,
           s.Status,
           idt.InboundDocumentType,
           id.CreateDate
end
