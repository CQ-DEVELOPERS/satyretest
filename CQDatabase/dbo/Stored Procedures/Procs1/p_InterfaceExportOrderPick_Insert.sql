﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportOrderPick_Insert
  ///   Filename       : p_InterfaceExportOrderPick_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:03
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceExportOrderPick table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportOrderPickId int = null output,
  ///   @IssueId int = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @RecordStatus char(1) = null,
  ///   @ProcessedDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportOrderPick.InterfaceExportOrderPickId,
  ///   InterfaceExportOrderPick.IssueId,
  ///   InterfaceExportOrderPick.OrderNumber,
  ///   InterfaceExportOrderPick.RecordStatus,
  ///   InterfaceExportOrderPick.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportOrderPick_Insert
(
 @InterfaceExportOrderPickId int = null output,
 @IssueId int = null,
 @OrderNumber nvarchar(60) = null,
 @RecordStatus char(1) = null,
 @ProcessedDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceExportOrderPickId = '-1'
    set @InterfaceExportOrderPickId = null;
  
	 declare @Error int
 
  insert InterfaceExportOrderPick
        (IssueId,
         OrderNumber,
         RecordStatus,
         ProcessedDate)
  select @IssueId,
         @OrderNumber,
         @RecordStatus,
         @ProcessedDate 
  
  select @Error = @@Error, @InterfaceExportOrderPickId = scope_identity()
  
  
  return @Error
  
end
