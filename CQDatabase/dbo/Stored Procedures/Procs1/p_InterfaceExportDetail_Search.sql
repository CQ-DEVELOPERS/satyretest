﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportDetail_Search
  ///   Filename       : p_InterfaceExportDetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:59
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceExportDetail table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceExportDetail.InterfaceExportHeaderId,
  ///   InterfaceExportDetail.ForeignKey,
  ///   InterfaceExportDetail.LineNumber,
  ///   InterfaceExportDetail.ProductCode,
  ///   InterfaceExportDetail.Product,
  ///   InterfaceExportDetail.SKUCode,
  ///   InterfaceExportDetail.Batch,
  ///   InterfaceExportDetail.Quantity,
  ///   InterfaceExportDetail.Weight,
  ///   InterfaceExportDetail.Additional1,
  ///   InterfaceExportDetail.Additional2,
  ///   InterfaceExportDetail.Additional3,
  ///   InterfaceExportDetail.Additional4,
  ///   InterfaceExportDetail.Additional5,
  ///   InterfaceExportDetail.OrderQuantity,
  ///   InterfaceExportDetail.RejectQuantity,
  ///   InterfaceExportDetail.ReceivedQuantity,
  ///   InterfaceExportDetail.AcceptedQuantity,
  ///   InterfaceExportDetail.DeliveryNoteQuantity,
  ///   InterfaceExportDetail.ReasonCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportDetail_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceExportDetail.InterfaceExportHeaderId
        ,InterfaceExportDetail.ForeignKey
        ,InterfaceExportDetail.LineNumber
        ,InterfaceExportDetail.ProductCode
        ,InterfaceExportDetail.Product
        ,InterfaceExportDetail.SKUCode
        ,InterfaceExportDetail.Batch
        ,InterfaceExportDetail.Quantity
        ,InterfaceExportDetail.Weight
        ,InterfaceExportDetail.Additional1
        ,InterfaceExportDetail.Additional2
        ,InterfaceExportDetail.Additional3
        ,InterfaceExportDetail.Additional4
        ,InterfaceExportDetail.Additional5
        ,InterfaceExportDetail.OrderQuantity
        ,InterfaceExportDetail.RejectQuantity
        ,InterfaceExportDetail.ReceivedQuantity
        ,InterfaceExportDetail.AcceptedQuantity
        ,InterfaceExportDetail.DeliveryNoteQuantity
        ,InterfaceExportDetail.ReasonCode
    from InterfaceExportDetail
  
end
