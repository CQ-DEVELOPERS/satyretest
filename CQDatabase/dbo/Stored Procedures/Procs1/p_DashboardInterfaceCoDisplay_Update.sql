﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DashboardInterfaceCoDisplay_Update
  ///   Filename       : p_DashboardInterfaceCoDisplay_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:32
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the DashboardInterfaceCoDisplay table.
  /// </remarks>
  /// <param>
  ///   @CompanyId int = null,
  ///   @DisplayCode char(5) = null,
  ///   @DisplayDesc nvarchar(100) = null,
  ///   @Display char(1) = null,
  ///   @Counter numeric(18,0) = null,
  ///   @ReportName nvarchar(100) = null,
  ///   @ExtractDescription nvarchar(100) = null,
  ///   @Show char(1) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DashboardInterfaceCoDisplay_Update
(
 @CompanyId int = null,
 @DisplayCode char(5) = null,
 @DisplayDesc nvarchar(100) = null,
 @Display char(1) = null,
 @Counter numeric(18,0) = null,
 @ReportName nvarchar(100) = null,
 @ExtractDescription nvarchar(100) = null,
 @Show char(1) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update DashboardInterfaceCoDisplay
     set CompanyId = isnull(@CompanyId, CompanyId),
         DisplayCode = isnull(@DisplayCode, DisplayCode),
         DisplayDesc = isnull(@DisplayDesc, DisplayDesc),
         Display = isnull(@Display, Display),
         Counter = isnull(@Counter, Counter),
         ReportName = isnull(@ReportName, ReportName),
         ExtractDescription = isnull(@ExtractDescription, ExtractDescription),
         Show = isnull(@Show, Show) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
