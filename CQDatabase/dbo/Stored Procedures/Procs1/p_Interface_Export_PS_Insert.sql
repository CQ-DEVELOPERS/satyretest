﻿--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*  
  /// <summary>  
  ///   Procedure Name : p_Interface_Export_PS_Insert  
  ///   Filename       : p_Interface_Export_PS_Insert.sql  
  ///   Create By      : Ruan Groenewald  
  ///   Date Created   : 22 May 2012  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Interface_Export_PS_Insert  
(  
 @IssueId int  
)  
  
as  
begin  
    set nocount on;  
  
    declare @Error                       int,          
            @Errormsg                    varchar(500),          
            @GetDate                     datetime,          
            @OrderNumber                 varchar(30),       
            @InvoiceNumber               varchar(30),    
            @OutboundDocumentId          int,          
            @Delivery                    int,          
            @InterfaceImportSOHeaderId   int,          
            @InterfaceExportPackHeaderId int,          
            @rowcount                    int,          
            @WarehouseId                 int,    
            @WarehouseCode               varchar(30),        
            @OutboundDocumentTypeCode    varchar(10),          
            @StatusId                    int,          
            @InboundLineId               int,          
            @BatchId                     int,          
            @InboundDocumentId           int,          
            @LineNumber                  int,          
            @InboundDocumentTypeId       int,           
            @ExternalCompanyId           int,          
            @Quantity                    numeric(13,6)          
  
    select @GetDate = dbo.ufn_Getdate()          
            
    set @Errormsg = 'Error executing p_Interface_Export_PS_Insert'          
            
    select  @OrderNumber              = od.OrderNumber,          
            @OutboundDocumentId       = od.OutboundDocumentId,          
            @Delivery                 = i.Delivery,          
            @OutboundDocumentTypeCode = odt.OutboundDocumentTypeCode,          
            @WarehouseId              = od.WarehouseId,  
            @ExternalCompanyId        = od.ExternalCompanyId          
    from OutboundDocument      od (nolock)          
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId          
    join Issue                  i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId          
    where i.IssueId = @IssueId          
        
--  if not exists(select 1 from IssueLine where IssueId = @IssueId and ConfirmedQuatity > 0)          
--    return 0          
            
    begin transaction          
            
    if not exists(select top 1 1          
                    from InterfaceExportPackHeader          
                   where IssueId = @IssueId)          
                   
    begin          
              
        select @InterfaceImportSOHeaderId = max(InterfaceImportSOHeaderId)          
          from InterfaceImportSOHeader          
         where RecordStatus = 'C'          
           and OrderNumber = @OrderNumber  
             
        select @InvoiceNumber = InvoiceNumber  
          from InterfaceInvoice  
         where OrderNumber = @OrderNumber  
                
        insert InterfaceExportPackHeader          
              (PrimaryKey  
              ,IssueId        
              ,OrderNumber  
              --,RecordType          
              ,RecordStatus  
              ,CustomerCode  
              ,Customer  
              ,DeliveryDate  
              ,DeliveryNoteNumber  
              ,Route  
              ,Comments)          
        select distinct @InvoiceNumber  
              ,@IssueId  
              ,od.OrderNumber  
              --,odt.OutboundDocumentTypeCode  
              ,'W'  
              ,ec.ExternalCompanyCode  
              ,ec.ExternalCompany       
              ,i.DeliveryDate     
              ,i.DeliveryNoteNumber  
              ,r.Route  
              ,i.Remarks          
          from OutboundDocument        od (nolock)          
          inner join OutboundDocumentType   odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId          
          inner join Issue                    i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId          
          left join Route r on i.RouteId = r.RouteId  
          --left join  OutboundShipmentIssue  osi (nolock) on i.IssueId = osi.IssueId  
          left join  ExternalCompany         ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId          
         where i.IssueId = @IssueId          
                
        select @InterfaceExportPackHeaderId = scope_identity()  
              ,@rowcount = @@rowcount  
              ,@Error = @@Error  
              ,@Errormsg ='p_Interface_Export_Pack_Insert - insert InterfaceExportPackHeader'          
                
        if @Error <> 0          
            goto error          
                
        if @InterfaceImportSOHeaderId is not null          
        begin          
          
            select @WarehouseCode = WarehouseCode  
            from Warehouse  
            where WareHouseId = @WarehouseId              
                          
            update PS          
               set --PrimaryKey        = SO.PrimaryKey,          
                   CustomerCode      = SO.CustomerCode,          
                   FromWarehouseCode = ISNULL(SO.FromWarehouseCode, @WarehouseCode),          
                   ToWarehouseCode   = SO.ToWarehouseCode,        
                   Comments          = SO.Additional3,  
                   DeliveryAdd1      = SO.Additional4,          
                   DeliveryAdd2      = SO.Additional5,          
                   DeliveryAdd3      = SO.Additional6,          
                   DeliveryAddPostalCode      = SO.Additional7  
              from InterfaceImportSOHeader SO (nolock),          
                   InterfaceExportPackHeader PS  
             where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId          
               and InterfaceExportPackHeaderId = @InterfaceExportPackHeaderId          
                  
            select @Error = @@Error, @Errormsg = 'p_Interface_Export_Pack_Insert - update InterfaceExportPackHeader'          
                  
            if @Error <> 0          
                goto error          
        end          
                
        if @rowcount > 0          
        begin          
            declare @Instructions as  table          
            (          
                 InstructionId      int,        
                 JobId              int,  
                 ReferenceNumber    varchar(30),  
                 StorageUnitId      int,  
                 ProductCode        varchar(30),  
                 Batch              varchar(50),  
                 OrginalQuantity    int,          
                 Quantity           int,  
                 PickLocationId     int,  
                 StoreLocationId    int,  
                 ContainerTypeId    int,  
                 CheckedBy          int,  
                 Weight             float                       
            )          
                  
            declare @OrderLines as  table          
            (          
                 Id int primary key identity,          
                 OrderNumber        varchar(30),          
                 LineNumber         varchar(10),          
                 JobId              int,  
                 ReferenceNumber    varchar(30),  
                 ProductCode        varchar(30),          
                 Product            varchar(50),          
                 SKUCode            varchar(10),          
                 Batch              varchar(50),   
                 ExpiryDate         datetime,         
                 Quantity           int,          
                 ConfirmedQuantity  int,          
                 PickLocationId     int,  
                 StoreLocationId    int,  
                 CheckedBy          int,  
                 PackLength         int,  
                 PackWidth          int,  
                 PackHeight         int,  
                 PackWeight         float,  
                 PackType           nvarchar(50),  
                 PackSize           nvarchar(50),  
                 Processed char(1) default 'N'          
            )          
                  
            insert @Instructions          
            select i.InstructionId,   
                   i.JobId,  
                   j.ReferenceNumber,      
                   vs.StorageUnitId,  
                   vs.ProductCode,  
                   vs.Batch,  
                   i.ConfirmedQuantity,          
                   case when s.StatusCode = 'NS'          
                        then 0          
                        else i.ConfirmedQuantity          
                   end,  
                   i.PickLocationId,  
                   i.StoreLocationId,  
                   j.ContainerTypeId,  
                   j.CheckedBy,  
                   i.Weight  
              from Instruction            i (nolock)          
              inner join Job                    j (nolock) on i.JobId = j.JobId  
              inner join Status                 s (nolock) on i.StatusId = s.StatusId     
              inner join viewStock             vs (nolock) on i.StorageUnitBatchId   = vs.StorageUnitBatchId          
             where (i.InstructionId in (select distinct InstructionId from IssueLineInstruction where IssueId = @IssueId)          
                 or i.InstructionRefId in (select distinct InstructionId from IssueLineInstruction where IssueId = @IssueId))          
                
                  
            insert @OrderLines          
                  (OrderNumber,          
                   LineNumber,          
                   ProductCode,          
                   Product,          
                   SKUCode,          
                   Batch,     
                   ExpiryDate,       
                   Quantity,          
                   ConfirmedQuantity)          
            select distinct od.OrderNumber,           
                   ol.LineNumber,          
                   vs.ProductCode,          
                   vs.Product,          
                   vs.SKUCode,          
                   vs.Batch,    
                   vs.ExpiryDate,        
                   il.Quantity,          
                   0          
              from OutboundDocument      od (nolock)          
              join OutboundLine          ol (nolock) on od.OutboundDocumentId  = ol.OutboundDocumentId          
              join IssueLine             il (nolock) on ol.OutboundLineId      = il.OutboundLineId           
              join viewStock             vs (nolock) on il.StorageUnitBatchId   = vs.StorageUnitBatchId          
             where il.IssueId = @IssueId          
               and il.Quantity > 0    
            --   and vs.ProductCode = 'MP018'          
             
            declare @InstructionId      int,          
                    @MyOrderNumber      varchar(30),          
                    @MyLineNumber       varchar(10),          
                    @JobId              int,  
                    @ReferenceNumber    varchar(30),  
                    @StorageUnitId      int,  
                    @ProductCode        varchar(30),          
                    @Product            varchar(50),          
                    @SKUCode            varchar(10),          
                    @LineBatch          varchar(50),          
                    @Batch              varchar(50),          
                    @MyQuantity         int,          
                    @ConfirmedQuantity  int,          
                    @InsQuantity        int,  
                    @Weight             float,  
                    @PickLocationId     int,  
                    @StoreLocationId    int,  
                    @ContainerTypeId    int,  
                    @CheckedBy          int,  
                    @Id                 int,          
                    @fetch_status       int,          
                    @show_msg           bit          
                  
            set @show_msg = 0  
                  
            while exists(select 1 from @OrderLines where Processed = 'N')          
            begin          
                set @Id = null          
                    
                select top 1          
                     @Id                = Id,          
                     @MyOrderNumber     = OrderNumber,          
                     @MyLineNumber      = LineNumber,          
                     @ProductCode       = ProductCode,          
                     @Product           = Product,          
                     @SKUCode           = SKUCode,          
                     @LineBatch         = Batch,          
                     @MyQuantity        = Quantity,          
                     @ConfirmedQuantity = ConfirmedQuantity  
                from @OrderLines          
               where Processed = 'N'          
               order by Id          
                    
                if @show_msg = 1          
                select @Id                as '@Id',          
                       @MyOrderNumber     as '@MyOrderNumber',          
                       @MyLineNumber      as '@MyLineNumber',          
                       @ProductCode       as '@ProductCode',          
                       @Product           as '@Product',          
                       @SKUCode           as '@SKUCode',          
                       @LineBatch         as '@LineBatch',          
                       @MyQuantity        as '@MyQuantity',          
                       @ConfirmedQuantity as '@ConfirmedQuantity'          
                    
                declare instruction_cursor cursor for          
                select InstructionId,    
                       JobId,  
                       ReferenceNumber,        
                       StorageUnitId,       
                       Batch,  
                       Quantity,  
                       PickLocationId,  
                       StoreLocationId,  
                       ContainerTypeId,  
                       CheckedBy,  
                       Weight  
                  from @Instructions  
                 where ProductCode = @ProductCode  
                   and Quantity > 0          
                order by Quantity desc          
                    
                open instruction_cursor          
                    
                fetch instruction_cursor   
                 into @InstructionId,   
                      @JobId,  
                      @ReferenceNumber,         
                      @StorageUnitId,  
                      @Batch,          
                      @InsQuantity,  
                      @PickLocationId,  
                      @StoreLocationId,  
                      @ContainerTypeId,  
                      @CheckedBy,  
                      @Weight   
                  
                select @fetch_status = @@fetch_status          
                     
                if @fetch_status <> 0          
                update @OrderLines          
                   set Processed = 'Y'          
                 where ProductCode = @ProductCode          
                     
                while (@fetch_status = 0)          
                begin          
                      
                    if @LineBatch = 'Default'          
                    set @LineBatch = @Batch          
                      
                    if @show_msg = 1          
                    select @InstructionId as '@InstructionId',          
                           @Batch         as '@Batch',          
                           @LineBatch     as '@LineBatch',          
                           @InsQuantity   as '@InsQuantity',          
                           @MyQuantity    as '@MyQuantity'          
                      
                    if @LineBatch != @Batch and @MyQuantity > 0          
                    begin          
                        select @LineBatch = @Batch          
                        
                        update @OrderLines          
                           set Processed = 'Y',          
                               Quantity = ConfirmedQuantity          
                         where Id = @Id          
                        
                        insert @OrderLines          
                            (OrderNumber,          
                            LineNumber,          
                            ProductCode,          
                            Product,          
                            SKUCode,          
                            Batch,          
                            Quantity,          
                            ConfirmedQuantity  
                            )          
                        select @MyOrderNumber,          
                            @MyLineNumber,          
                            @ProductCode,          
                            @Product,          
                            @SKUCode,          
                            @Batch,          
                            @MyQuantity - isnull(@ConfirmedQuantity,0),          
                            0  
                        
                        select @Id = scope_identity()          
                    end          
                      
                    if @MyQuantity > @InsQuantity          
                    begin          
                      if @show_msg = 1          
                        select '@MyQuantity > @InsQuantity', @Id as '@Id' ,@LineBatch as '@LineBatch', @Batch as '@Batch', @MyQuantity as'@MyQuantity', @ConfirmedQuantity as '@ConfirmedQuantity', @insQuantity as '@insQuantity'          
                      update @OrderLines          
                         set ConfirmedQuantity = isnull(ConfirmedQuantity,0) + @InsQuantity,          
                             Batch = @Batch,  
                             PackWeight = @Weight  
                       where Id = @Id          
                                
                      update @Instructions          
                         set Quantity = isnull(Quantity,0) - @InsQuantity          
                       where InstructionId = @InstructionId          
                                
                      set @MyQuantity = @MyQuantity - @InsQuantity          
                    end          
                    else if @MyQuantity > 0          
                    begin          
                      if @show_msg = 1          
                        select 'Update', @Id as '@Id' ,@LineBatch as '@LineBatch', @Batch as '@Batch', @MyQuantity as '@MyQuantity',@ConfirmedQuantity as '@ConfirmedQuantity', @insQuantity as '@insQuantity'          
                                  
                      update @OrderLines          
                         set ConfirmedQuantity = isnull(ConfirmedQuantity,0) + @MyQuantity,          
                             Batch = @Batch,  
                             PackWeight = @Weight,       
                             Processed = 'Y'          
                       where Id = @Id          
                                
                      update @Instructions          
                         set Quantity = isnull(Quantity,0) - @MyQuantity          
                       where InstructionId = @InstructionId          
                                
                      set @MyQuantity = @MyQuantity - @MyQuantity          
                    end          
                      
                    If (Select ContainerType From ContainerType Where ContainerTypeId = @ContainerTypeId) = 'Parcel'  
                    BEGIN  
                        Update @OrderLines  
                           Set PackType = 'Parcel'  
                              ,PackSize = pt.PackType  
                              ,PackLength = Length  
                              ,PackWidth = Width  
                              ,PackHeight = Height  
                          From  Pack p  
                         Inner join PackType pt on p.PackTypeId = pt.PackTypeId  
                         where StorageUnitId = @StorageUnitId  
                         and pt.PackType in ('Parcel','Carton', 'Shipper', 'Shrink')  
                    END  
                    ELSE  
                    BEGIN  
                        Update @OrderLines  
                           Set PackType     = ContainerType  
                              --,PackSize     = Description  
                              --,PackLength   = Length  
                              --,PackWidth    = Width  
                              --,PackHeight   = Height  
                          From ContainerType  
                         where ContainerTypeId = @ContainerTypeId  
                    END  
                                          
                    update @OrderLines  
                       set JobId = @JobId  
                          ,ReferenceNumber = @ReferenceNumber  
                          ,PickLocationId = @PickLocationId  
                          ,StoreLocationId = @StoreLocationId  
                          ,CheckedBy = @CheckedBy  
                     where Id = @Id  
                      
                    if @show_msg = 1          
                            select 'Result',* from @OrderLines          
              
                    fetch instruction_cursor   
                    into @InstructionId,   
                         @JobId,  
                         @ReferenceNumber,  
                         @StorageUnitId,  
                         @Batch,          
                         @InsQuantity,  
                         @PickLocationId,  
                         @StoreLocationId,  
                         @ContainerTypeId,  
                         @CheckedBy,  
                         @Weight  
                      
                    select @fetch_status = @@fetch_status          
                end          
                    
                close instruction_cursor          
                deallocate instruction_cursor          
            end          
          
            insert InterfaceExportPackDetail          
                  (InterfaceExportPackHeaderID,          
                   ForeignKey,  
                   JobId,  
                   ReferenceNumber,     
                   LineNumber,          
                   ProductCode,          
                   Product,          
                   SKUCode,          
                   Batch,  
                   PickArea,          
                   PickLocation,  
                   Quantity,     
                   ConfirmedQuantity,  
                   ShortQuantity,  
                   CheckQuantity,  
                   Weight,  
                   StoreArea,  
                   StoreLocation,  
                   CheckedBy,  
                   PackLength,  
                   PackWidth,  
                   PackHeight,  
                   PackType,  
                   PackSize,  
                   ExpiryDate)          
             select @InterfaceExportPackHeaderId,  
                    @InvoiceNumber,          
                    JobId,  
                    ReferenceNumber,  
                    LineNumber,          
                    ProductCode,          
                    Product,          
                    SKUCode,          
                    Batch,          
                    null,  
                    pl.Location,  
                    Quantity,  
                    ConfirmedQuantity,          
                    null,  
                    null,  
                    PackWeight,  
                    null,  
                    sl.Location,  
                    o.Operator,  
                    PackLength,  
                    PackWidth,  
                    PackHeight,  
                    PackType,  
                    PackSize,  
                    ol.ExpiryDate    
               from @OrderLines ol  
               left join Location pl on ol.PickLocationId = pl.LocationId  
               left join Location sl on ol.StoreLocationId = sl.LocationId  
               left join Operator o on ol.CheckedBy = o.OperatorId  
                  
            select @Error = @@Error          
                  ,@rowcount = @@ROWCOUNT  
                  
            if (@Error <> 0 OR @rowcount = 0)  
                goto error  
                                        
        end          
    end                 
     
    commit transaction          
    if @InterfaceExportPackHeaderId is not null       
    Begin      
          --Print 'Updating' + Convert(char(20),@InterfaceExportSOHeaderId )    
        Update InterfaceExportPackHeader   
        set RecordStatus = 'N'   
        where InterfaceExportPackHeaderId =  @InterfaceExportPackHeaderId    
    End    
    return          
                
    error:          
        RAISERROR (900000,-1,-1, @Errormsg);          
        rollback transaction          
        return @Error  
      
end  


