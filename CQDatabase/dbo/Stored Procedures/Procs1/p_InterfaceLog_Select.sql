﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLog_Select
  ///   Filename       : p_InterfaceLog_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:27
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceLog table.
  /// </remarks>
  /// <param>
  ///   @InterfaceLogId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceLog.InterfaceLogId,
  ///   InterfaceLog.Data,
  ///   InterfaceLog.RecordStatus,
  ///   InterfaceLog.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLog_Select
(
 @InterfaceLogId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceLog.InterfaceLogId
        ,InterfaceLog.Data
        ,InterfaceLog.RecordStatus
        ,InterfaceLog.ProcessedDate
    from InterfaceLog
   where isnull(InterfaceLog.InterfaceLogId,'0')  = isnull(@InterfaceLogId, isnull(InterfaceLog.InterfaceLogId,'0'))
  
end
