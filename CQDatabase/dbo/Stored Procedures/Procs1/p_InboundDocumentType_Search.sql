﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentType_Search
  ///   Filename       : p_InboundDocumentType_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 10:28:17
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocumentType table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentTypeId int = null output,
  ///   @InboundDocumentTypeCode nvarchar(20) = null,
  ///   @InboundDocumentType nvarchar(60) = null,
  ///   @PriorityId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InboundDocumentType.InboundDocumentTypeId,
  ///   InboundDocumentType.InboundDocumentTypeCode,
  ///   InboundDocumentType.InboundDocumentType,
  ///   InboundDocumentType.OverReceiveIndicator,
  ///   InboundDocumentType.AllowManualCreate,
  ///   InboundDocumentType.QuantityToFollowIndicator,
  ///   InboundDocumentType.PriorityId,
  ///   Priority.Priority,
  ///   InboundDocumentType.AutoSendup,
  ///   InboundDocumentType.BatchButton,
  ///   InboundDocumentType.BatchSelect,
  ///   InboundDocumentType.DeliveryNoteNumber,
  ///   InboundDocumentType.VehicleRegistration,
  ///   InboundDocumentType.AreaType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentType_Search
(
 @InboundDocumentTypeId int = null output,
 @InboundDocumentTypeCode nvarchar(20) = null,
 @InboundDocumentType nvarchar(60) = null,
 @PriorityId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InboundDocumentTypeId = '-1'
    set @InboundDocumentTypeId = null;
  
  if @InboundDocumentTypeCode = '-1'
    set @InboundDocumentTypeCode = null;
  
  if @InboundDocumentType = '-1'
    set @InboundDocumentType = null;
  
  if @PriorityId = '-1'
    set @PriorityId = null;
  
 
  select
         InboundDocumentType.InboundDocumentTypeId
        ,InboundDocumentType.InboundDocumentTypeCode
        ,InboundDocumentType.InboundDocumentType
        ,InboundDocumentType.OverReceiveIndicator
        ,InboundDocumentType.AllowManualCreate
        ,InboundDocumentType.QuantityToFollowIndicator
        ,InboundDocumentType.PriorityId
         ,PriorityPriorityId.Priority as 'Priority'
        ,InboundDocumentType.AutoSendup
        ,InboundDocumentType.BatchButton
        ,InboundDocumentType.BatchSelect
        ,InboundDocumentType.DeliveryNoteNumber
        ,InboundDocumentType.VehicleRegistration
        ,InboundDocumentType.AreaType
    from InboundDocumentType
    left
    join Priority PriorityPriorityId on PriorityPriorityId.PriorityId = InboundDocumentType.PriorityId
   where isnull(InboundDocumentType.InboundDocumentTypeId,'0')  = isnull(@InboundDocumentTypeId, isnull(InboundDocumentType.InboundDocumentTypeId,'0'))
     and isnull(InboundDocumentType.InboundDocumentTypeCode,'%')  like '%' + isnull(@InboundDocumentTypeCode, isnull(InboundDocumentType.InboundDocumentTypeCode,'%')) + '%'
     and isnull(InboundDocumentType.InboundDocumentType,'%')  like '%' + isnull(@InboundDocumentType, isnull(InboundDocumentType.InboundDocumentType,'%')) + '%'
     and isnull(InboundDocumentType.PriorityId,'0')  = isnull(@PriorityId, isnull(InboundDocumentType.PriorityId,'0'))
  order by InboundDocumentTypeCode
  
end
