﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Level_List
  ///   Filename       : p_Housekeeping_Level_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 May 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Level_List
(
 @WarehouseId int
)

as
begin
	 set nocount on;
  
  select distinct [Level]
    from viewLocation
   where WarehouseId = @WarehouseId
     and [Level] is not null
  order by [Level]
end
