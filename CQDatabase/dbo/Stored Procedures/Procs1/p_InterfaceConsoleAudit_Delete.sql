﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceConsoleAudit_Delete
  ///   Filename       : p_InterfaceConsoleAudit_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:50
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceConsoleAudit table.
  /// </remarks>
  /// <param>
  ///   @InterfaceConsoleAuditId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceConsoleAudit_Delete
(
 @InterfaceConsoleAuditId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceConsoleAudit
     where InterfaceConsoleAuditId = @InterfaceConsoleAuditId
  
  select @Error = @@Error
  
  
  return @Error
  
end
