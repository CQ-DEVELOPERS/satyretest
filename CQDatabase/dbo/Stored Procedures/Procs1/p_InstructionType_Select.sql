﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionType_Select
  ///   Filename       : p_InstructionType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:55
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InstructionType table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null 
  /// </param>
  /// <returns>
  ///   InstructionType.InstructionTypeId,
  ///   InstructionType.PriorityId,
  ///   InstructionType.InstructionType,
  ///   InstructionType.InstructionTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionType_Select
(
 @InstructionTypeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InstructionType.InstructionTypeId
        ,InstructionType.PriorityId
        ,InstructionType.InstructionType
        ,InstructionType.InstructionTypeCode
    from InstructionType
   where isnull(InstructionType.InstructionTypeId,'0')  = isnull(@InstructionTypeId, isnull(InstructionType.InstructionTypeId,'0'))
  
end
