﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_On_Hand_Upload
  ///   Filename       : p_Housekeeping_Stock_On_Hand_Upload.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Jun 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_On_Hand_Upload
(
 @WarehouseId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update Configuration
     set Indicator = 1
   where ConfigurationId = 137
     and WarehouseId = @WarehouseId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_On_Hand_Upload'); 
    rollback transaction
    return @Error
end
