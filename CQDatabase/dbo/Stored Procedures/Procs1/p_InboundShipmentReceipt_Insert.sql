﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipmentReceipt_Insert
  ///   Filename       : p_InboundShipmentReceipt_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:22
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundShipmentReceipt table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null output,
  ///   @ReceiptId int = null output 
  /// </param>
  /// <returns>
  ///   InboundShipmentReceipt.InboundShipmentId,
  ///   InboundShipmentReceipt.ReceiptId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipmentReceipt_Insert
(
 @InboundShipmentId int = null output,
 @ReceiptId int = null output 
)

as
begin
	 set nocount on;
  
  if @InboundShipmentId = '-1'
    set @InboundShipmentId = null;
  
  if @ReceiptId = '-1'
    set @ReceiptId = null;
  
	 declare @Error int
 
  insert InboundShipmentReceipt
        (InboundShipmentId,
         ReceiptId)
  select @InboundShipmentId,
         @ReceiptId 
  
  select @Error = @@Error, @ReceiptId = scope_identity()
  
  
  return @Error
  
end
