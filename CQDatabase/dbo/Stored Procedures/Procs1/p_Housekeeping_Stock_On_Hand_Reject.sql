﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_On_Hand_Reject
  ///   Filename       : p_Housekeeping_Stock_On_Hand_Reject.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_On_Hand_Reject
(
 @warehouseId    int,
 @comparisonDate datetime,
 @storageUnitId  int,
 @batchId        int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update StockOnHandCompare
     set Sent = null
   where ComparisonDate = @comparisonDate
     and StorageUnitId  = @storageUnitId
     and BatchId        = @batchId
  select @@rowcount
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_On_Hand_Reject'); 
    rollback transaction
    return @Error
end
