﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaSequence_Delete
  ///   Filename       : p_AreaSequence_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:02
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the AreaSequence table.
  /// </remarks>
  /// <param>
  ///   @AreaSequenceId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaSequence_Delete
(
 @AreaSequenceId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete AreaSequence
     where AreaSequenceId = @AreaSequenceId
  
  select @Error = @@Error
  
  
  return @Error
  
end
