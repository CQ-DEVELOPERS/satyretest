﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_Remove
  ///   Filename       : p_InboundShipment_Remove.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_Remove
(
 @inboundShipmentId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_InboundShipment_Delete
   @inboundShipmentId = @inboundShipmentId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_InboundShipment_Remove'); 
    rollback transaction
    return @Error
end
