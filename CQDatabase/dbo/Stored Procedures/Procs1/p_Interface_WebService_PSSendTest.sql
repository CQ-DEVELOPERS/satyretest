﻿
/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_PackSend
  ///   Filename       : p_Interface_WebService_PackSend.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 22 May 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure [dbo].[p_Interface_WebService_PSSendTest]
(
 @XMLBody varchar(max) output
 --,@principalCode nvarchar(50) = null
)
--with encryption
as
begin
	DECLARE @iDoc				        INT
	       ,@Error						NVARCHAR(MAX)
	       ,@GETDATE                    DATETIME
		   ,@ReferenceNumber	        NVARCHAR(50)
	       ,@JobId                      INT
	       ,@WarehouseId                INT
		   ,@WarehouseCode				NVARCHAR(50)
		   ,@IssueId                    INT
		   ,@OrderNumber                NVARCHAR(50)
		   ,@InterfaceImportHeaderId    INT

    SET @GETDATE = dbo.ufn_GetDate()	 

	EXEC sp_xml_preparedocument @idoc OUTPUT, @XMLBody

	SELECT @ReferenceNumber = ReferenceNumber
	  FROM  OPENXML (@idoc, '/root/Body/Item',1)
      WITH (ReferenceNumber varchar(50) 'ReferenceNumber')

	IF LEFT(@ReferenceNumber, 2) = 'J:'
    BEGIN
        select
             @JobId = JobId 
            ,@WarehouseId = WarehouseId
        from Job	j (nolock)
        join Status s (nolock) on j.StatusId = s.StatusId
       where JobId = CONVERT(Int, SUBSTRING(@ReferenceNumber, 3, LEN(@ReferenceNumber) - 2))
         and s.StatusCode IN ('CD','DC','D','C') --Checking
    END
    ELSE
    BEGIN
      select @JobId = JobId
            ,@WarehouseId = WarehouseId
        from Job    j (nolock)
        join Status s (nolock) on j.StatusId = s.StatusId
       where j.ReferenceNumber = @ReferenceNumber
         and s.StatusCode IN ('CD','DC','D','C') --Checking
    END  
    
    If @JobId IS NULL
    BEGIN
		SET @XMLBody = NULL
    
		RETURN
    END
		
	
	DECLARE @PackHeader AS TABLE
	(
		[ID] [int] IDENTITY(1,1) PRIMARY KEY,
		[PrimaryKey] [nvarchar](40) NULL,
		[OrderNumber] [nvarchar](30) NULL,
		[FromWarehouseCode] [nvarchar](50) NULL,
		[ToWarehouseCode] [nvarchar](50) NULL,
		[Route] [nvarchar](10) NULL,
		[DeliveryNoteNumber] [nvarchar](30) NULL,
		[DeliveryDate] [datetime] NULL,
		[OutboundShipmentId] [int] NULL,
		[IssueId] [int] NULL,
		[ManifestId] int NULL,
		[CustomerCode] [nvarchar](30) NULL,
		[Customer] [nvarchar](255) NULL,
		[DeliveryAdd1] [nvarchar](100) NULL,
		[DeliveryAdd2] [nvarchar](100) NULL,
		[DeliveryAdd3] [nvarchar](100) NULL,
		[DeliveryAdd4] [nvarchar](100) NULL,
		[DeliveryAddPostalCode] [nvarchar](20) NULL,
		[DeliveryMethod] [nvarchar](50) NULL,
		[ContactPerson] [nvarchar](50) NULL,
		[Comments] [nvarchar](255) NULL,
		[ManagerOverride] [nchar](1) NULL,
		[Manager] [nvarchar](50) NULL,
		[NumberOfLines] [int] NULL)

	DECLARE @PackDetail AS TABLE
	(
		[PackHeaderID] [int] NULL,
		[ForeignKey] [nvarchar](40) NULL,
		[PalletNumber] [nvarchar](30) NULL,
		[JobId] [int] NULL,
		[ReferenceNumber] [nvarchar](30) NULL,
		[LineNumber] [int] NULL,
		[ProductCode] [nvarchar](30) NULL,
		[Product] [nvarchar](50) NULL,
		[SKUCode] [nvarchar](10) NULL,
		[Batch] [nvarchar](50) NULL,
		[PickArea] [nvarchar](50) NULL,
		[PickLocation] [nvarchar](15) NULL,
		[Quantity] [int] NULL,
		[ConfirmedQuantity] [int] NULL,
		[ShortQuantity] [int] NULL,
		[CheckQuantity] [int] NULL,
		[Weight] [float] NULL,
		[StoreArea] [nvarchar](50) NULL,
		[StoreLocation] [nvarchar](15) NULL,
		[CheckedBy] [nvarchar](50) NULL,
		[PackLength] [float] NULL,
		[PackWidth] [float] NULL,
		[PackHeight] [float] NULL,
		[PackType] [nvarchar](50) NULL,
		[PackSize] [nvarchar](50) NULL,
		[ExpiryDate] [datetime] NULL,
		[PackItems] [int] NULL,
		[PackWeight] [numeric](13, 6) NULL,
		[PackTare] [numeric](13, 6) NULL)


	INSERT INTO @PackHeader
            (PrimaryKey    
            ,IssueId         
            ,ManifestId 
            ,OrderNumber    
            ,CustomerCode    
            ,Customer    
			,DeliveryAdd1
			,DeliveryAdd2 
			,DeliveryAdd3 
			,DeliveryAdd4 
			,DeliveryAddPostalCode
            ,DeliveryDate    
            ,DeliveryNoteNumber    
            ,Route    
            ,Comments)            
    select distinct i.IssueId    
            ,i.IssueId    
            ,i.WaveId
            ,od.OrderNumber    
            ,ec.ExternalCompanyCode    
            ,ec.ExternalCompany        
		    ,ad.Street
			,ad.Suburb
			,ad.Town
			,ad.Country
			,ad.Code
            ,i.DeliveryDate       
            ,i.DeliveryNoteNumber    
            ,r.Route    
            ,i.Remarks
        from OutboundDocument        od (nolock)            
        inner join OutboundDocumentType   odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId            
        inner join Issue                    i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId            
        left join Route                     r (nolock) on i.RouteId = r.RouteId    
        left join ExternalCompany          ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId            
		left join address                  ad (nolock) on ec.ExternalCompanyId = ad.ExternalCompanyId
        where Exists
		  (SELECT 1
		   FROM IssueLineInstruction ili_sub
		   INNER JOIN Instruction i_sub ON ili_sub.InstructionId = ISNULL(i_sub.InstructionRefId, i_sub.InstructionId)
		   WHERE ili_sub.IssueId = i.IssueId
		     AND i_sub.JobId = @JobId)

	SELECT @OrderNumber = OrderNumber,
		   @IssueId     = IssueId
	  FROM @PackHeader

	select @InterfaceImportHeaderId = max(InterfaceImportHeaderId)            
	  from InterfaceImportHeader            
	 where RecordStatus = 'C'            
	   and OrderNumber = @OrderNumber    

	if @InterfaceImportHeaderId is not null            
    begin            
            
        select @WarehouseCode = WarehouseCode    
        from Warehouse    
        where WareHouseId = @WarehouseId                
                            
        update @PackHeader            
            set CustomerCode      = SO.CompanyCode,            
                FromWarehouseCode = ISNULL(SO.FromWarehouseCode, @WarehouseCode),            
                ToWarehouseCode   = SO.ToWarehouseCode          
                --Comments          = left(SO.Additional3,255),    
            from InterfaceImportHeader SO (nolock)   
            where InterfaceImportHeaderId = @InterfaceImportHeaderId
    end 

	declare @Instructions as  table            
    (            
            InstructionId      int,          
            JobId              int,    
            ReferenceNumber    varchar(30),    
            StorageUnitId      int,    
            ProductCode        varchar(30),    
            Batch              varchar(50),    
            OrginalQuantity    int,            
            Quantity           int,    
            PickLocationId     int,    
            StoreLocationId    int,    
            ContainerTypeId    int,  
            CheckedBy          int,    
            Weight             float                         
    )            
                    
    declare @OrderLines as  table            
    (            
            Id int primary key identity,    
            IssueId			   int,        
            OrderNumber        varchar(30),            
            LineNumber         varchar(10),            
            JobId              int,    
            ReferenceNumber    varchar(30),    
            PalletNumber	   varchar(30),
            ProductCode        varchar(30),            
            Product            varchar(50),            
            SKUCode            varchar(10),            
            Batch              varchar(50),     
            ExpiryDate         datetime,           
            Quantity           int,            
            ConfirmedQuantity  int,
			ConfirmedWeight    numeric(13, 6),
            PickLocationId     int,    
            StoreLocationId    int,    
            CheckedBy          int,    
            PackLength         int,    
            PackWidth          int,    
            PackHeight         int,    
            PackWeight         float,    
            PackType           nvarchar(50),    
            PackSize           nvarchar(50),  
            PackTare           numeric(13,6),    
            PackItems          int,   
            Processed char(1) default 'N'   
	)

	insert @Instructions            
    select i.InstructionId,     
            i.JobId,    
            j.ReferenceNumber,        
            vs.StorageUnitId,    
            vs.ProductCode,    
            vs.Batch,    
            i.ConfirmedQuantity,            
            case when s.StatusCode = 'NS'            
                then 0            
                else i.ConfirmedQuantity            
            end,    
            i.PickLocationId,    
            i.StoreLocationId,    
            j.ContainerTypeId,    
            j.CheckedBy,    
            i.Weight    
        from Instruction            i (nolock)            
        inner join Job                    j (nolock) on i.JobId = j.JobId    
        inner join Status                 s (nolock) on i.StatusId = s.StatusId       
        inner join viewStock             vs (nolock) on i.StorageUnitBatchId   = vs.StorageUnitBatchId            
        where ISNULL(i.InstructionRefId, i.InstructionId) in (select distinct InstructionId from IssueLineInstruction where IssueId = @IssueId)
        --j.JobId = @JobId

	insert @OrderLines            
			(IssueId,
			OrderNumber,            
			LineNumber,            
			ProductCode,            
			Product,            
			SKUCode,            
			Batch,       
			ExpiryDate,         
			Quantity,            
			ConfirmedQuantity)            
	select distinct il.IssueId,
		    od.OrderNumber,             
			ol.LineNumber,            
			vs.ProductCode,            
			vs.Product,            
			vs.SKUCode,            
			vs.Batch,      
			vs.ExpiryDate,          
			il.Quantity,            
			0            
		from OutboundDocument      od (nolock)            
		join OutboundLine          ol (nolock) on od.OutboundDocumentId  = ol.OutboundDocumentId            
		join IssueLine             il (nolock) on ol.OutboundLineId      = il.OutboundLineId             
		join IssueLineInstruction ili (nolock) on il.IssueLineId         = ili.IssueLineId
		join Instruction            i (nolock) on ili.InstructionId      = isnull(i.InstructionRefId, i.InstructionId)
		join viewStock             vs (nolock) on il.StorageUnitBatchId  = vs.StorageUnitBatchId            
		where il.IssueId = @IssueId
		--i.JobId = @JobId
		and i.CheckQuantity > 0   

	declare @InstructionId      int,            
			@MyIssueId          int, 
			@MyOrderNumber      varchar(30),            
			@MyLineNumber       varchar(10),            
			@CurrentJobId		int,
			@StorageUnitId      int,    
			@ProductCode        varchar(30),            
			@Product            varchar(50),            
			@SKUCode            varchar(10),            
			@LineBatch          varchar(50),            
			@Batch              varchar(50),            
			@MyQuantity         int,            
			@ConfirmedQuantity  Numeric(13,6),            
			@InsQuantity        int,    
			@Weight             float,    
			@PickLocationId     int,    
			@StoreLocationId    int,    
			@ContainerTypeId    int,    
			@CheckedBy          int,    
			@Id                 int,            
			@fetch_status       int,            
			@show_msg           bit,  
			@row    int  
                    
        set @show_msg = 0    
                    
    while exists(select 1 from @OrderLines where Processed = 'N')            
    begin            
        set @Id = null            
                      
        select top 1            
                @Id                = Id,         
                @MyIssueId         = IssueId,   
                @MyOrderNumber     = OrderNumber,            
                @MyLineNumber      = LineNumber,            
                @ProductCode       = ProductCode,            
                @Product           = Product,            
                @SKUCode           = SKUCode,            
                @LineBatch         = Batch,            
                @MyQuantity        = Quantity,            
                @ConfirmedQuantity = ConfirmedQuantity    
        from @OrderLines            
        where Processed = 'N'            
        order by Id            
                      
        if @show_msg = 1            
        select @Id                as '@Id',            
                @MyOrderNumber     as '@MyOrderNumber',            
                @MyLineNumber      as '@MyLineNumber',            
                @ProductCode       as '@ProductCode',            
                @Product           as '@Product',            
                @SKUCode           as '@SKUCode',            
                @LineBatch         as '@LineBatch',            
                @MyQuantity        as '@MyQuantity',            
                @ConfirmedQuantity as '@ConfirmedQuantity'            
                      
        declare instruction_cursor cursor for            
        select ROW_NUMBER() over (partition by StorageUnitId order by Quantity desc),  
                InstructionId,      
                JobId,    
                ReferenceNumber,          
                StorageUnitId,         
                Batch,    
                Quantity,    
                PickLocationId,    
                StoreLocationId,    
                ContainerTypeId,    
                CheckedBy,    
                Weight    
            from @Instructions    
            where ProductCode = @ProductCode    
            and Quantity > 0            
        order by ROW_NUMBER() over (partition by StorageUnitId order by Quantity desc)              
                      
        open instruction_cursor            
                      
        fetch instruction_cursor     
            into @row,  
				@InstructionId,     
                @CurrentJobId,    
                @ReferenceNumber,           
                @StorageUnitId,    
                @Batch,            
                @InsQuantity,    
                @PickLocationId,    
                @StoreLocationId,    
                @ContainerTypeId,    
                @CheckedBy,    
                @Weight     
                    
        select @fetch_status = @@fetch_status            
                       
        if @fetch_status <> 0            
        update @OrderLines            
            set Processed = 'Y'            
            where ProductCode = @ProductCode            
                       
        while (@fetch_status = 0)            
        begin            
                        
            if @LineBatch = 'Default'            
            set @LineBatch = @Batch            
                        
            if @show_msg = 1            
            select @InstructionId as '@InstructionId',            
                    @Batch         as '@Batch',            
                    @LineBatch     as '@LineBatch',            
                    @InsQuantity   as '@InsQuantity',            
                    @MyQuantity    as '@MyQuantity'            
                        
            if @row > 1 and @MyQuantity > 0           
            begin            
                select @LineBatch = @Batch            
                          
                update @OrderLines            
                    set Processed = 'Y',            
                        Quantity = ConfirmedQuantity            
                    where Id = @Id            
                          
                insert @OrderLines            
                    (IssueId,
                    OrderNumber,            
                    LineNumber,            
                    ProductCode,            
                    Product,            
                    SKUCode,            
                    Batch,            
                    Quantity,            
                    ConfirmedQuantity    
                    )            
                select 
                    @MyIssueId,
                    @MyOrderNumber,            
                    @MyLineNumber,            
                    @ProductCode,            
                    @Product,            
                    @SKUCode,            
                    @Batch,            
                    @MyQuantity - isnull(@ConfirmedQuantity,0),            
                    0    
                          
                select @Id = scope_identity()            
            end            
                        
            if @MyQuantity > @InsQuantity            
            begin            
                if @show_msg = 1            
                select '@MyQuantity > @InsQuantity', @Id as '@Id' ,@LineBatch as '@LineBatch', @Batch as '@Batch', @MyQuantity as'@MyQuantity', @ConfirmedQuantity as '@ConfirmedQuantity', @insQuantity as '@insQuantity'            
                update @OrderLines            
                    set ConfirmedQuantity = isnull(ConfirmedQuantity,0) + @InsQuantity,            
                        Batch = @Batch    
                        --PackWeight = @Weight    
                where Id = @Id            
                                  
                update @Instructions            
                    set Quantity = isnull(Quantity,0) - @InsQuantity            
                where InstructionId = @InstructionId            
                                  
                set @MyQuantity = @MyQuantity - @InsQuantity            
            end            
            else if @MyQuantity > 0            
            begin            
                if @show_msg = 1            
                select 'Update', @Id as '@Id' ,@LineBatch as '@LineBatch', @Batch as '@Batch', @MyQuantity as '@MyQuantity',@ConfirmedQuantity as '@ConfirmedQuantity', @insQuantity as '@insQuantity'            
                                    
                update @OrderLines            
                    set ConfirmedQuantity = isnull(ConfirmedQuantity,0) + @MyQuantity,            
                        Batch = @Batch,    
                        --PackWeight = @Weight,         
                        Processed = 'Y'            
                where Id = @Id            
                                  
                update @Instructions            
                    set Quantity = isnull(Quantity,0) - @MyQuantity            
                where InstructionId = @InstructionId            
                                  
                set @MyQuantity = 0   
            end            
            
			Update @OrderLines    
				Set PackType     = ContainerType    
					,PackSize     = Description    
					,PackLength   = Length    
					,PackWidth    = Width    
					,PackHeight   = Height  
					,PackItems    = 1  
					,PackTare     = TareWeight  
				From ContainerType    
				where ContainerTypeId = @ContainerTypeId    
				and Id = @Id

			Update @OrderLines
				Set PackWeight = j.Weight
				From Job j
				Where j.JobId = @CurrentJobId

            Update @OrderLines    
                Set ConfirmedWeight = ConfirmedQuantity * p.Weight
                From Pack p  
                Inner Join PackType pt on p.PackTypeId = pt.PackTypeId  
                where StorageUnitId = @StorageUnitId    
                and Id = @Id   
                and PackWeight is null  
                and p.WarehouseId = @WarehouseId  
                and pt.PackType = 'Unit'  

            update @OrderLines    
                set JobId = @CurrentJobId    
                    ,ReferenceNumber = @ReferenceNumber    
                    ,PickLocationId = @PickLocationId    
                    ,StoreLocationId = @StoreLocationId    
                    ,CheckedBy = @CheckedBy    
                where Id = @Id    
                        
            if @show_msg = 1            
                    select 'Result',* from @OrderLines            
                
            fetch instruction_cursor     
            into @row,  
					@InstructionId,     
                    @CurrentJobId,    
                    @ReferenceNumber,    
                    @StorageUnitId,    
                    @Batch,            
                    @InsQuantity,    
                    @PickLocationId,    
                    @StoreLocationId,    
                    @ContainerTypeId,    
                    @CheckedBy,    
                    @Weight    
                        
            select @fetch_status = @@fetch_status            
        end            
                      
        close instruction_cursor            
        deallocate instruction_cursor                
  end      
  
  update ol
  Set PalletNumber = convert(varchar(10), isnull(j.DropSequence,'')) + ' of ' + convert(varchar(10), isnull(j.Pallets,'')) 
  From @OrderLines ol
  Inner Join Job j (NOLOCK) on ol.JobId = j.JobId
  
    insert into @PackDetail            
        (PackHeaderID,            
        ForeignKey,
        PalletNumber,    
        JobId,    
        ReferenceNumber,       
        LineNumber,            
        ProductCode,            
        Product,            
        SKUCode,            
        Batch,    
        PickArea,            
        PickLocation,    
        Quantity,       
        ConfirmedQuantity,    
        ShortQuantity,    
        CheckQuantity,    
        Weight,    
        StoreArea,    
        StoreLocation,    
        CheckedBy,    
        PackLength,    
        PackWidth,    
        PackHeight,    
        PackType,    
        PackSize,  
        PackTare,  
		PackWeight,
        PackItems,    
        ExpiryDate)            
    select ph.ID,    
        @IssueId,    
        PalletNumber,        
        JobId,    
        ReferenceNumber,    
        LineNumber,            
        ProductCode,            
        Product,            
        SKUCode,            
        Batch,            
        null,    
        pl.Location,    
        Quantity,    
        ConfirmedQuantity,            
        null,    
        null,    
        ConfirmedWeight,    
        null,    
        sl.Location,    
        o.Operator,    
        PackLength,    
        PackWidth,    
        PackHeight,    
        PackType,    
        PackSize,   
        PackTare,
		PackWeight,   
        PackItems,  
        ol.ExpiryDate      
    from @OrderLines ol    
    inner join @PackHeader ph on ol.IssueId = ph.IssueId
    left join Location pl on ol.PickLocationId = pl.LocationId    
    left join Location sl on ol.StoreLocationId = sl.LocationId    
    left join Operator o on ol.CheckedBy = o.OperatorId  
    order by LineNumber
                    
    select @Error = @@Error     
    
	SELECT @XMLBody = '<?xml version="1.0" encoding="utf-16"?>' +
        (SELECT 
            (SELECT 
                 'CQuential'                AS 'Source'
                ,'Value'                    AS 'Target'
                ,GETDATE()                  AS 'CreateDate'
                ,'PackingSlip'              AS 'FileType'        
             FOR XML PATH('Header'), TYPE)
            ,(SELECT
                (SELECT TOP 15
                     'PS'                           AS 'ItemType'
                    ,'N'						    AS 'ItemStatus'
                    ,'InterfaceExportPackingSlip'	AS 'InterfaceTable'
                    ,1						    	AS 'InterfaceTableId'
                    ,H.PrimaryKey                   AS 'PrimaryKey'
                    ,H.OrderNumber                  AS 'OrderNumber'
                    ,H.OutboundShipmentId           As 'ShipmentNo'
                    ,H.ManifestId					As 'ManifestNo'
                    ,H.CustomerCode                 AS 'CustomerCode'
                    ,H.Customer                     AS 'Customer'
                    ,H.DeliveryAdd1                 AS 'DeliveryAdd1'
                    ,H.DeliveryAdd2                 AS 'DeliveryAdd2'
                    ,H.DeliveryAdd3                 AS 'DeliveryAdd3'
                    ,H.DeliveryAdd4                 AS 'DeliveryAdd4'
                    ,H.DeliveryAddPostalCode        As 'DeliveryAddPostalCode'
                    ,H.DeliveryMethod               AS 'DeliveryMethod'
                    ,H.FromWarehouseCode            AS 'FromWarehouseCode'
                    ,H.ToWarehouseCode              AS 'ToWarehouseCode'
                    ,H.Route                        AS 'Route'
                    ,H.DeliveryNoteNumber           AS 'DeliveryNoteNumber'
                    ,H.DeliveryDate                 AS 'DeliveryDate'
                    ,H.Comments                     AS 'Comments'
                    ,H.NumberOfLines                AS 'NumberOfLines'
                    ,H.ManagerOverride              AS 'ManagerOverride'
                    ,(SELECT 
                         D1.ForeignKey						AS 'ForeignKey'
                        ,ROW_NUMBER() OVER (Order By JobId) AS 'LineNumber'
                        ,D1.PalletNumber					AS 'PalletNumber'
                        ,D1.JobId							AS 'JobNo'
                        ,D1.ReferenceNumber					As 'ReferenceNumber'
                        ,CONVERT(VARCHAR(50),D1.PackWeight)	AS 'PackWeight'
                        ,CONVERT(VARCHAR(50),D1.PackLength) AS 'PackLength'
                        ,CONVERT(VARCHAR(50),D1.PackWidth)  AS 'PackWidth'
                        ,CONVERT(VARCHAR(50),D1.PackHeight) AS 'PackHeight'
                        ,D1.PackType						AS 'PackType'
                        ,D1.PackSize						AS 'PackSize'
                        ,D1.PackItems						AS 'NumberOfProducts'
                        ,COUNT(*)							AS 'TotalDetails'
                        ,(SELECT 
                             ROW_NUMBER() OVER (Order By D2.LineNumber) AS 'DetailLineNumber'
                            ,D2.ProductCode                 AS 'ProductCode'
                            ,D2.Product                     AS 'Product'
                            ,D2.SKUCode                     AS 'SKUCode'
                            ,D2.Batch                       AS 'Batch'
                            ,D2.PickArea                    AS 'PickArea'
                            ,D2.PickLocation                AS 'PickLocation'
                            ,D2.Quantity                    AS 'Quantity'
                            ,D2.ConfirmedQuantity           AS 'ConfirmedQuantity'
                            ,D2.ShortQuantity               AS 'ShortQuantity'
                            ,D2.CheckQuantity               AS 'CheckQuantity'
                            ,CONVERT(VARCHAR(50),D2.Weight) AS 'Weight'
                            ,D2.ExpiryDate                  AS 'ExpiryDate'                
                          FROM @PackHeader H2
                          INNER JOIN @PackDetail D2 on H2.Id = D2.PackHeaderId
                          WHERE H2.Id = H1.Id
                          AND D2.JobId = D1.JobId
                          FOR XML PATH('Detail'), TYPE)
                      FROM @PackHeader H1
                      INNER JOIN @PackDetail D1 on H1.Id = D1.PackHeaderId
                      WHERE H1.Id = H.Id
						AND D1.JobId = @JobId
                      GROUP BY 
                         H1.Id
                        ,D1.ForeignKey
                        ,D1.PalletNumber
                        ,D1.JobId 
                        ,D1.ReferenceNumber
                        ,D1.PackLength              
                        ,D1.PackWidth               
                        ,D1.PackHeight 
						,D1.PackWeight             
                        ,D1.PackType                
                        ,D1.PackSize
                        ,D1.PackItems
                        ,D1.PackTare
                      FOR XML PATH('ItemLine'), TYPE),
                     (SELECT
						 (SELECT DISTINCT
							ISNULL(D1.ReferenceNumber, 'J:' + CONVERT(VARCHAR(50), D1.JobId)) 
						  FROM @PackHeader H1
						  INNER JOIN @PackDetail D1 on H1.Id = D1.PackHeaderId
						  WHERE H1.Id = H.Id
							AND D1.JobId <> @JobId
						  ORDER BY ISNULL(D1.ReferenceNumber, 'J:' + CONVERT(VARCHAR(50), D1.JobId)) 
						  FOR XML PATH('ReferenceNumber'), TYPE)
					  FOR XML PATH('RemainingLines'), TYPE)
                FROM @PackHeader H
                FOR XML PATH('Item'), TYPE)
            FOR XML PATH('Body'), TYPE)    
        FOR XML PATH('root'))
        
End

