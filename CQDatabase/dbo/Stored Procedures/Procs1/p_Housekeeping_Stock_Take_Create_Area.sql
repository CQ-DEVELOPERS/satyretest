﻿
/*
 /// <summary>
 ///   Procedure Name : p_Housekeeping_Stock_Take_Create_Area
 ///   Filename       : p_Housekeeping_Stock_Take_Create_Area.sql
 ///   Create By      : Grant Schultz
 ///   Date Created   : 21 Aug 2007
 /// </summary>
 /// <remarks>
 ///   
 /// </remarks>
 /// <param>
 ///   
 /// </param>
 /// <returns>
 ///   
 /// </returns>
 /// <newpara>
 ///   Modified by    : 
 ///   Modified Date  : 
 ///   Details        : 
 /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Create_Area
(
@warehouseId   int,
@operatorId    int,
@jobId         int,
@areaId        int,
@areaOrAisle   bit,
@split         int,
@columnOrLevel bit,
@StockTakeReferenceId	int
)

as
begin
	 set nocount on;
	 
	 declare @TableLocation as table
	 (
	  Aisle              nvarchar(10),
	  Split              int,
  LocationId         int,
	  StorageUnitBatchId int,
	  ActualQuantity     float
	 )

 declare @Error              int,
         @Errormsg           nvarchar(500),
         @GetDate            datetime,
         @InstructionTypeId  int,
         @InstructionId      int,
         @StatusId           int,
         @LocationId         int,
         @StorageUnitBatchId int,
         @ActualQuantity     float,
         @splitCount         int,
         @Count              int,
         @PriorityId         int,
         @ReferenceNumber    nvarchar(30)

 select @GetDate = dbo.ufn_Getdate()

 select @InstructionTypeId = InstructionTypeId
   from InstructionType
  where InstructionTypeCode = 'STL'

 select @PriorityId      = PriorityId,
        @ReferenceNumber = ReferenceNumber
   from Job (nolock)
  where JobId = @jobId

 begin transaction

 if @Error <> 0
   goto error

 insert @TableLocation
       (Aisle,
        LocationId,
	        StorageUnitBatchId,
	        ActualQuantity)
 select case when @areaOrAisle = 1
                       then isnull(l.[Ailse], '0')
                       else '-1'
                       end + case when @columnOrLevel = 1
                                  then ' ' + isnull(l.[Level], '')
                                  else ''
                                  end,
           l.LocationId,
	        subl.StorageUnitBatchId,
	        subl.ActualQuantity
	   from Location                    l (nolock)
	   join AreaLocation               al (nolock) on l.LocationId = al.LocationId
	   join Area                        a (nolock) on al.AreaId    = a.AreaId
	   left outer
	   join StorageUnitBatchLocation subl (nolock) on l.LocationId = subl.LocationId
	  where al.AreaId      = @AreaId
	    and l.StocktakeInd = 0
	    and a.StockOnHand  = 1
	 order by (case when @columnOrLevel = 0
	               then l.[Column]
               else l.[Level]
               end)

 declare	@Aisle nvarchar(10)

 declare Aisle_cursor cursor for
  select Aisle
 	  from	@TableLocation

 open Aisle_cursor

 fetch Aisle_cursor into @Aisle

 while (@@fetch_status = 0)
 begin
   select @Count = count(1) / @split
     from @TableLocation
    where Aisle = @Aisle

   set @splitCount = 0

   while @splitCount < @split
   begin
     set @splitCount = @splitCount + 1

     if @splitCount = @split
        set rowcount 0
     else
        set rowcount @Count

     update @TableLocation
        set Split = @splitCount
      where Split is null
        and Aisle = @Aisle
   end

   fetch Aisle_cursor into @Aisle
 end

 close Aisle_cursor
 deallocate Aisle_cursor
	 -- select * from @TableLocation order by Aisle, Split
	 
 declare Aisle_split_cursor cursor for
  select	distinct Aisle, Split
 	  from	@TableLocation

 open Aisle_split_cursor

 fetch Aisle_split_cursor into @Aisle, @split

 while (@@fetch_status = 0)
 begin
   select @StatusId = dbo.ufn_StatusId('J','W')

   exec @Error = p_Job_Insert
    @JobId           = @jobId output,
    @PriorityId      = @PriorityId,
    @OperatorId      = @operatorId,
    @StatusId        = @StatusId,
    @WarehouseId     = @warehouseId,
    @ReferenceNumber = @referenceNumber
	   
   if @Error <> 0
     goto error

   if @Error <> 0
     goto error

   select @StatusId = dbo.ufn_StatusId('I','W')

	   while exists(select 1 from @TableLocation where Aisle = @Aisle and Split = @split)
	   begin
	     select top 1
	            @LocationId         = LocationId,
	            @StorageUnitBatchId = StorageUnitBatchId,
	            @ActualQuantity     = isnull(ActualQuantity, 0)
	       from @TableLocation
	      where Aisle = @Aisle
	        and Split = @split
 	   
	     delete @TableLocation
	      where LocationId                   = @LocationId
	        and isnull(StorageUnitBatchId,0) = isnull(@StorageUnitBatchId,0)
	     
	     exec @Error = p_Location_Update
	      @locationId    = @LocationId,
	      @stocktakeInd  = 1
 	   
     if @Error <> 0
       goto error
 	   
     exec @Error = p_Instruction_Insert
      @InstructionId       = @InstructionId output,
      @InstructionTypeId   = @InstructionTypeId,
      @StorageUnitBatchId  = @StorageUnitBatchId,
      @WarehouseId         = @warehouseId,
      @StatusId            = @StatusId,
      @JobId               = @jobId,
      @OperatorId          = @OperatorId,
      @PickLocationId      = @LocationId,
      @Quantity            = @ActualQuantity,
      @CheckQuantity       = @ActualQuantity,
      @CreateDate          = @GetDate
 	   
     if @Error <> 0
       goto error
   end

   fetch Aisle_split_cursor into @Aisle, @split
 end

 close Aisle_split_cursor
 deallocate Aisle_split_cursor

 exec	@Error = p_StockTakeReferenceJob_Insert
		@StockTakeReferenceId	= @StockTakeReferenceId,
		@JobId					= @JobId 

 commit transaction
 return

 error:
   RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_Take_Create_Area'); 
   rollback transaction
   return @Error
end

