﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportDetail_Search
  ///   Filename       : p_InterfaceImportDetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:24
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportDetail table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportDetail.InterfaceImportHeaderId,
  ///   InterfaceImportDetail.ForeignKey,
  ///   InterfaceImportDetail.LineNumber,
  ///   InterfaceImportDetail.ProductCode,
  ///   InterfaceImportDetail.Product,
  ///   InterfaceImportDetail.SKUCode,
  ///   InterfaceImportDetail.Batch,
  ///   InterfaceImportDetail.Quantity,
  ///   InterfaceImportDetail.Weight,
  ///   InterfaceImportDetail.RetailPrice,
  ///   InterfaceImportDetail.NetPrice,
  ///   InterfaceImportDetail.LineTotal,
  ///   InterfaceImportDetail.Volume,
  ///   InterfaceImportDetail.Additional1,
  ///   InterfaceImportDetail.Additional2,
  ///   InterfaceImportDetail.Additional3,
  ///   InterfaceImportDetail.Additional4,
  ///   InterfaceImportDetail.Additional5 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportDetail_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportDetail.InterfaceImportHeaderId
        ,InterfaceImportDetail.ForeignKey
        ,InterfaceImportDetail.LineNumber
        ,InterfaceImportDetail.ProductCode
        ,InterfaceImportDetail.Product
        ,InterfaceImportDetail.SKUCode
        ,InterfaceImportDetail.Batch
        ,InterfaceImportDetail.Quantity
        ,InterfaceImportDetail.Weight
        ,InterfaceImportDetail.RetailPrice
        ,InterfaceImportDetail.NetPrice
        ,InterfaceImportDetail.LineTotal
        ,InterfaceImportDetail.Volume
        ,InterfaceImportDetail.Additional1
        ,InterfaceImportDetail.Additional2
        ,InterfaceImportDetail.Additional3
        ,InterfaceImportDetail.Additional4
        ,InterfaceImportDetail.Additional5
    from InterfaceImportDetail
  
end
