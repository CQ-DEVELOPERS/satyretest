﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_Select
  ///   Filename       : p_Exception_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:06
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Exception table.
  /// </remarks>
  /// <param>
  ///   @ExceptionId int = null 
  /// </param>
  /// <returns>
  ///   Exception.ExceptionId,
  ///   Exception.ReceiptLineId,
  ///   Exception.InstructionId,
  ///   Exception.IssueLineId,
  ///   Exception.ReasonId,
  ///   Exception.Exception,
  ///   Exception.ExceptionCode,
  ///   Exception.CreateDate,
  ///   Exception.OperatorId,
  ///   Exception.ExceptionDate,
  ///   Exception.JobId,
  ///   Exception.Detail,
  ///   Exception.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_Select
(
 @ExceptionId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Exception.ExceptionId
        ,Exception.ReceiptLineId
        ,Exception.InstructionId
        ,Exception.IssueLineId
        ,Exception.ReasonId
        ,Exception.Exception
        ,Exception.ExceptionCode
        ,Exception.CreateDate
        ,Exception.OperatorId
        ,Exception.ExceptionDate
        ,Exception.JobId
        ,Exception.Detail
        ,Exception.Quantity
    from Exception
   where isnull(Exception.ExceptionId,'0')  = isnull(@ExceptionId, isnull(Exception.ExceptionId,'0'))
  
end
