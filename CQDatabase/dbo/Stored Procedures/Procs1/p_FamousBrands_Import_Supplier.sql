﻿
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Import_Supplier
  ///   Filename       : p_FamousBrands_Import_Supplier.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Import_Supplier

as
begin
	 set nocount on;
  
  declare @Error               int,
          @Errormsg            varchar(500),
          @GetDate             datetime,
          @SupplierCode        varchar(30),
          @SupplierName        varchar(255),
          @Address1            varchar(255),
          @Address2            varchar(255),
          @Address3            varchar(255),
          @Address4            varchar(255),
          @Modified            varchar(10) ,
          @ContactPerson       varchar(100),
          @Phone               varchar(255),
          @Fax                 varchar(255),
          @Email               varchar(255),
          @ExternalCompanyId   int,
          @AddressId           int,
          @ContactListId       int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  
  update InterfaceSupplier
     set ProcessedDate = @Getdate
   where RecordStatus in ('N','U')
     and ProcessedDate is null
  
  declare Supplier_cursor cursor for
   select SupplierCode,
          SupplierName,
          Address1,
          Address2,
          Address3,
          Address4,
          Modified,
          ContactPerson,
          Phone,
          Fax,
          Email
  	  from	InterfaceSupplier
    where ProcessedDate = @Getdate
	  order by	SupplierCode
  
  open Supplier_cursor
  
  fetch Supplier_cursor into @SupplierCode,
                             @SupplierName,
                             @Address1,
                             @Address2,
                             @Address3,
                             @Address4,
                             @Modified,
                             @ContactPerson,
                             @Phone,
                             @Fax,
                             @Email

  while (@@fetch_status = 0)
  begin
    begin transaction
    
    set @ExternalCompanyId = null
    
    select @ExternalCompanyId = ExternalCompanyId
      from ExternalCompany (nolock)
     where ExternalCompanyCode = @SupplierCode
     
    if @ExternalCompanyId is null
    begin
      exec @Error = p_ExternalCompany_Insert
       @ExternalCompanyId         = @ExternalCompanyId output,
       @ExternalCompanyTypeId     = 2,
       @ExternalCompany           = @SupplierName,
       @ExternalCompanyCode       = @SupplierCode
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Insert'
        goto error
      end
    end
    else
    begin
      exec @Error = p_ExternalCompany_Update
       @ExternalCompanyId         = @ExternalCompanyId,
       @ExternalCompanyTypeId     = 2,
       @ExternalCompany           = @SupplierName,
       @ExternalCompanyCode       = @SupplierCode
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
        goto error
      end
    end
    
    select @AddressId = AddressId
      from Address
     where ExternalCompanyId = @ExternalCompanyId
    
    if @AddressId is null
    begin
      exec @Error = p_Address_Insert
       @AddressId         = @AddressId output,
       @ExternalCompanyId = @ExternalCompanyId,
       @Street            = @Address1,
       @Suburb            = @Address2,
       @Town              = @Address3,
       @Country           = @Address4
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Address_Insert'
        goto error
      end
    end
    else
    begin
      exec @Error = p_Address_Update
       @AddressId         = @AddressId,
       @ExternalCompanyId = @ExternalCompanyId,
       @Street            = @Address1,
       @Suburb            = @Address2,
       @Town              = @Address3,
       @Country           = @Address4
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ExternalCompany_Update'
        goto error
      end
    end
    
    select @ContactListId = ContactListId
      from ContactList
     where ExternalCompanyId = @ExternalCompanyId
       and ContactPerson = @ContactPerson
    
    if @ContactListId is null
    begin
      exec @Error = p_ContactList_Insert
       @ContactListId     = @ContactListId output,
       @ExternalCompanyId = @ExternalCompanyId,
       @OperatorId        = null,
       @ContactPerson     = @ContactPerson,
       @Telephone         = @Phone,
       @Fax               = @Fax,
       @EMail             = @Email
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ContactList_Insert'
        goto error
      end
    end
    else
    begin
      exec @Error = p_ContactList_Update
       @ContactListId     = @ContactListId,
       @ExternalCompanyId = @ExternalCompanyId,
       @OperatorId        = null,
       @ContactPerson     = @ContactPerson,
       @Telephone         = @Phone,
       @Fax               = @Fax,
       @EMail             = @Email
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_ContactList_Update'
        goto error
      end
    end
    
    error:
    if @Error = 0
    begin
      update InterfaceSupplier
         set RecordStatus = 'Y'
       where SupplierCode = @SupplierCode
         and ProcessedDate = @Getdate
         and RecordStatus = 'N'
      
      commit transaction
    end
    else
    begin
      if @@trancount > 0
        rollback transaction
      
      update InterfaceSupplier
         set RecordStatus = 'E'
       where SupplierCode = @SupplierCode
         and ProcessedDate = @Getdate
         and RecordStatus = 'N'
    end
    
    fetch Supplier_cursor into @SupplierCode,
                               @SupplierName,
                               @Address1,
                               @Address2,
                               @Address3,
                               @Address4,
                               @Modified,
                               @ContactPerson,
                               @Phone,
                               @Fax,
                               @Email
  end

  close Supplier_cursor
  deallocate Supplier_cursor
end
