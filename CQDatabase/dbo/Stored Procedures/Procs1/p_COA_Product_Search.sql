﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Product_Search
  ///   Filename       : p_COA_Product_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Product_Search
(
 @productCode nvarchar(30),
 @product      nvarchar(255),
 @skuCode     nvarchar(50),
 @batch       nvarchar(50)
)

as
begin
	 set nocount on;
	 
  insert COA
        (StorageUnitId)
  select su.StorageUnitId
    from StorageUnit su (nolock)
    left
    join COA          c (nolock) on c.StorageUnitId = su.StorageUnitId
   where c.StorageUnitId is null
     and c.StorageUnitBatchId is null
  
  select isnull(c.COAId, -1) as 'COAId',
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKU,
         sku.SKUCode,
         b.Batch
    from StorageUnitBatch sub (nolock)
    join Batch              b (nolock) on sub.BatchId = b.BatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId = p.ProductId
    join SKU              sku (nolock) on su.SKUId = sku.SKUId
    join COA                c (nolock) on c.StorageUnitBatchId = sub.StorageUnitBatchId
   where isnull(p.ProductCode,'%') like '%' + isnull(@ProductCode, isnull(p.ProductCode,'%')) + '%'
     and isnull(p.Product,'%')     like '%' + isnull(@Product, isnull(p.Product,'%')) + '%'
     and isnull(sku.SKUCode,'%')   like '%' + isnull(@SKUCode, isnull(sku.SKUCode,'%')) + '%'
     and isnull(b.Batch,'%')       like '%' + isnull(@Batch, isnull(b.Batch,'%')) + '%'
  union
  select isnull(c.COAId, -1) as 'COAId',
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         sku.SKU,
         sku.SKUCode,
         'Master'
    from COA                c (nolock)
    join StorageUnit       su (nolock) on c.StorageUnitId = su.StorageUnitId
    join Product            p (nolock) on su.ProductId = p.ProductId
    join SKU              sku (nolock) on su.SKUId = sku.SKUId
   where isnull(p.ProductCode,'%') like '%' + isnull(@ProductCode, isnull(p.ProductCode,'%')) + '%'
     and isnull(p.Product,'%')     like '%' + isnull(@Product, isnull(p.Product,'%')) + '%'
     and isnull(sku.SKUCode,'%')   like '%' + isnull(@SKUCode, isnull(sku.SKUCode,'%')) + '%'
end
