﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperatorGroup_Search
  ///   Filename       : p_AreaOperatorGroup_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:44
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the AreaOperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null output,
  ///   @OperatorGroupId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   AreaOperatorGroup.AreaId,
  ///   AreaOperatorGroup.OperatorGroupId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperatorGroup_Search
(
 @AreaId int = null output,
 @OperatorGroupId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @AreaId = '-1'
    set @AreaId = null;
  
  if @OperatorGroupId = '-1'
    set @OperatorGroupId = null;
  
 
  select
         AreaOperatorGroup.AreaId
        ,AreaOperatorGroup.OperatorGroupId
    from AreaOperatorGroup
   where isnull(AreaOperatorGroup.AreaId,'0')  = isnull(@AreaId, isnull(AreaOperatorGroup.AreaId,'0'))
     and isnull(AreaOperatorGroup.OperatorGroupId,'0')  = isnull(@OperatorGroupId, isnull(AreaOperatorGroup.OperatorGroupId,'0'))
  
end
