﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Checking_Achieved_Group
  ///   Filename       : p_Dashboard_Outbound_Checking_Achieved_Group.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Checking_Achieved_Group
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)

as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select convert(nvarchar(2), d.CreateDate, 108) + 'H00' as 'Legend',
           OperatorKPI,
           WarehouseKPI,
           sum(d.Value) as 'Value'
      from DashboardOutboundCheckingAchieved d
      join Operator o on d.OperatorId = o.OperatorId
      join DashboardKPI kpi on kpi.FunctionId = 'PICKING'
     where d.WarehouseId = @WarehouseId
    group by kpi.OperatorKPI,
             kpi.WarehouseKPI,
             convert(nvarchar(2), d.CreateDate, 108) + 'H00'
    order by convert(nvarchar(2), d.CreateDate, 108) + 'H00'
	 end
	 --else
	 --begin
	 --  truncate table DashboardOutboundCheckingAchieved
	   
	 --  insert DashboardOutboundCheckingAchieved
	 --        (WarehouseId,
  --         OperatorId,
  --         CreateDate,
  --         Legend,
  --         Value)
	 --  select i.WarehouseId,
  --         i.OperatorId,
  --         j.CheckedDate,
  --         null,
  --         SUM(i.CheckQuantity)
	 --    from Instruction i (nolock)
	 --    join Job         j (nolock) on i.JobId = j.JobId
	 --    join Status      s (nolock) on j.StatusId = s.StatusId
	 --   where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	 --     and s.StatusCode in ('CD','D','DC','C')
	 --     and j.CheckedDate >= CONVERT(nvarchar(10), getdate(), 120)
	 --  group by i.WarehouseId,
  --           i.OperatorId,
  --           j.CheckedDate
	 --end
end
