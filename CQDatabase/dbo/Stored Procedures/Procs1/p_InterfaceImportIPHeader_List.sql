﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIPHeader_List
  ///   Filename       : p_InterfaceImportIPHeader_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:33
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportIPHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceImportIPHeader.InterfaceImportIPHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIPHeader_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceImportIPHeaderId
        ,null as 'InterfaceImportIPHeader'
  union
  select
         InterfaceImportIPHeader.InterfaceImportIPHeaderId
        ,InterfaceImportIPHeader.InterfaceImportIPHeaderId as 'InterfaceImportIPHeader'
    from InterfaceImportIPHeader
  
end
