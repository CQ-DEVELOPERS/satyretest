﻿create procedure [dbo].[p_Famous_Import]
as
begin
          -- InterfaceImportHeader
  declare @InterfaceImportHeaderId    int,
          @Object                       nvarchar(10),
          @DocEntry                     nvarchar(30),
          @OrderNumber                  nvarchar(20),
          @ExternalCompanyCode          nvarchar(30),
          @ExternalCompany              nvarchar(255),
          @Address	                     nvarchar(100),
          @Remarks                      nvarchar(255),
          @TrnspCode                    int,
          @HoldStatus                   nvarchar(255),
          -- InterfaceImportDetail
          @LineNumber                   int,
          @ProductCode                  nvarchar(30),
          @Product                      nvarchar(50),
          @Quantity                     numeric(13,3),
          @TreeType                     char(1),
          @FromWarehouseCode            nvarchar(30),
          @ToWarehouseCode              nvarchar(30),
          -- Internal variables
          @InboundDocumentTypeId        int,
          @InboundDocumentId            int,
          @InboundLineId                int,
          @ReceiptLineId                int,
          @OutboundDocumentId           int,
          @OutboundDocumentTypeId       int,
          @ExternalCompanyId            int ,
          @DropSequence                 int,
          @Weight                       numeric(13,3),
          @ErrorMsg                     nvarchar(100),
          @Error                        int,
          @StatusId                     int,
          @GetDate                      datetime,
          @ProductId                    int,
          @StorageUnitId                int,
          @StorageUnitBatchId           int,
          @BatchId                      int,
          @Batch                        nvarchar(30),
          @PalletQuantity               numeric(13,3),
          @Exception                    nvarchar(255),
          @OutboundShipmentId           int,
          @IssueId                      int,
          @ReleaseDate                  nvarchar(max),
          @Days                         int,
          @FromWarehouseId              int,
          @ToWarehouseId                int,
          @InboundDocumentTypeCode      nvarchar(10),
          @InPriorityId                 int,
          @OutboundDocumentTypeCode     nvarchar(10),
          @OutPriorityId                int,
          @ODStatusId                   int,
          @OutboundLineId               int,
          @AutoRelease                  bit,
          @AddToShipment                bit,
          @MultipleOnShipment           bit,
          @HostStatus                   nvarchar(50)
  
  select @GetDate = dbo.ufn_Getdate()
  
  
  
  update InterfaceImportHeader
  set DeliveryDate = substring(DeliveryDate,7,4) + '-' + substring(DeliveryDate, 4,2) + '-' + left(DeliveryDate,2)
  where ISDATE(substring(DeliveryDate,7,4) + '-' + substring(DeliveryDate, 4,2) + '-' + left(DeliveryDate,2)) = 1
  and RecordStatus = 'N'

  
  if(object_id('tempdb.dbo.##p_Famous_Import') is null)
    create table ##p_Famous_Import (objname sysname)
  else
  begin
    select 'RETURN'
    RETURN
  end
  
  update d
     set InterfaceImportHeaderId = (select max(InterfaceImportHeaderId)
                                        from InterfaceImportHeader h
                                       where d.ForeignKey = h.PrimaryKey)
    from InterfaceImportDetail d
   where InterfaceImportHeaderId is null
 ------------------------------------------------------------------------------------------------ 
  --set @Days = 0
  
  --set @ReleaseDate = convert(datetime, convert(nvarchar(10), @GetDate, 120))
  --set @ReleaseDate = dateadd(hh, 12, @ReleaseDate)
  
  --if @Getdate <= @ReleaseDate
  --  set @ReleaseDate = dateadd(hh, 48, @ReleaseDate)
  --else
--  set @ReleaseDate = dateadd(hh, 72, @ReleaseDate)
  
  --if datename(dw, @ReleaseDate) = 'Saturday'
  --  set @Days = 2

  --if datename(dw, @ReleaseDate) = 'Sunday'
  --  set @Days = 2
  
  --select @ReleaseDate = dateadd(dd, @Days, @ReleaseDate)
  --------------------------------------------------------------------------------------------------
  -- Reprocess Order which did not process properly
  update InterfaceImportHeader
     set ProcessedDate = null
   where RecordStatus     = 'N'
     and ProcessedDate   <= dateadd(mi, -10, @Getdate)
  update InterfaceImportHeader
     set RecordStatus     = 'N'
   where RecordStatus     = 'W'
     and ProcessedDate is null
     and InsertDate <= dateadd(mi, -10, @Getdate)
  
  
  update h
     set ProcessedDate = @Getdate
    from InterfaceImportHeader h
   where --not exists(select 1 from viewOrderNumber  vw where h.OrderNumber = vw.OrderNumber)
         exists(select 1 from InterfaceImportDetail d where h.InterfaceImportHeaderId = d.InterfaceImportHeaderId)
     and h.RecordStatus in ('N','U')
     and ProcessedDate is null
  
  declare header_cursor cursor for
   select InterfaceImportHeaderId,
          RecordType,                       -- Object
          PrimaryKey,                       -- DocEntry
          OrderNumber,                      -- DocNum
          CompanyCode,                      -- CardCode
          Company,                          -- CardName
          Address,                          -- Address
          Remarks,                          -- PickRmrk
          null,                      -- TrnspCode
          FromWarehouseCode,                -- Filler
          ToWarehouseCode,					-- U_ZLGNUL
          Additional10,
          DeliveryDate,
          --case when ISDATE(DeliveryDate) = 1
          --     then DeliveryDate
          --     else @Getdate
          --     end,
          HostStatus
     from InterfaceImportHeader
    where ProcessedDate = @Getdate
   order by	OrderNumber
  
  open header_cursor
  
  fetch header_cursor
   into @InterfaceImportHeaderId,
        @Object,
        @DocEntry,
        @OrderNumber,
        @ExternalCompanyCode,
        @ExternalCompany,
        @Address,
        @Remarks,
        @TrnspCode,
        @FromWarehouseCode,
        @ToWarehouseCode,
        @HoldStatus,
        @ReleaseDate,
        @HostStatus

  select @HoldStatus as 'HoldStatus'
  
  If len(@ExternalCompanyCode) = 0
	Set @ExternalCompanyCode = null
	
  If len(@ExternalCompany) = 0
	Set @ExternalCompany = null
	
  If len(@Address) = 0
	Set @Address = null
  
  If len(@ToWarehouseCode) = 0
	Set @ToWarehouseCode = null
  
  If len(@FromWarehouseCode) = 0
	Set @FromWarehouseCode = null
  
  while (@@fetch_status = 0)
  begin
    begin transaction xml_import
    
    update InterfaceImportHeader
       set RecordStatus = 'C',
           ProcessedDate = @GetDate
     where InterfaceImportHeaderId = @InterfaceImportHeaderId
    print 1
    exec @Error = p_Interface_Outbound_Order_Delete
     @OrderNumber = @OrderNumber  
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Interface_Outbound_Order_Delete'
      goto error_header
    end
    
    exec @Error = p_Inbound_Order_Delete
     @OrderNumber = @OrderNumber
    
    if @Error != 0
    begin
      select @ErrorMsg = 'Error executing p_Inbound_Order_Delete'
      goto error_header
    end
    
    if @HostStatus = 'D' -- Just delete order and move on.
      goto error_header
    
    --if @@rowcount != 0
      select @FromWarehouseCode = FromWarehouseCode
        from InterfaceImportHeader
       where InterfaceImportHeaderId = @InterfaceImportHeaderId
    
    set @InboundDocumentTypeId = null
    set @OutboundDocumentTypeId = null
    set @InboundDocumentTypeCode = null
    set @OutboundDocumentTypeCode = null
    set @FromWarehouseId = null
    set @ToWarehouseId = null
    
    select @InboundDocumentTypeId  = InboundDocumentTypeId,
           @OutboundDocumentTypeId = OutboundDocumentTypeId,
           @FromWarehouseId    = WarehouseId,
           @ToWarehouseId          = ToWarehouseId
      from WarehouseCodeReference
     where DownloadType  = @Object
       and WarehouseCode = @FromWarehouseCode
       and ToWarehouseCode = @ToWarehouseCode
    
    if @@rowcount = 0
      select @InboundDocumentTypeId  = InboundDocumentTypeId,
             @OutboundDocumentTypeId = OutboundDocumentTypeId,
             @FromWarehouseId        = WarehouseId,
             @ToWarehouseId          = ToWarehouseId
        from WarehouseCodeReference
       where DownloadType  = @Object
         and WarehouseCode = @FromWarehouseCode
         and isnull(ToWarehouseCode, '') = ''
    
    if @@rowcount = 0
    begin
      select @InboundDocumentTypeId  = InboundDocumentTypeId,
       @OutboundDocumentTypeId = OutboundDocumentTypeId,
             @FromWarehouseId        = WarehouseId,
             @ToWarehouseId          = ToWarehouseId
        from WarehouseCodeReference
       where DownloadType  = @Object
         --and isnull(WarehouseCode,'') = ''
         and ToWarehouseCode = @ToWarehouseCode
      
      if @Object = '21'
      begin
        select @InboundDocumentTypeId  = null,
               @OutboundDocumentTypeId = null
      end
      
      if @Object = 'CR'
      begin
        select @FromWarehouseId = 1
        
        select @InboundDocumentTypeId  = InboundDocumentTypeId
          from InboundDocumentType
         where InboundDocumentTypeCode = 'RET'
      end
    end
    
    select @InboundDocumentTypeId  as '@InboundDocumentTypeId' ,
           @OutboundDocumentTypeId as '@OutboundDocumentTypeId',
           @FromWarehouseId        as '@FromWarehouseId       ',
           @ToWarehouseId          as '@ToWarehouseId         ',
           @FromWarehouseCode      as '@FromWarehouseCode',
           @ToWarehouseCode        as '@ToWarehouseCode',
           @ReleaseDate			   as '@ReleaseDate',
           @OrderNumber            as '@OrderNumber',
           @ExternalCompanyCode    as '@ExternalCompanyCode',
           @ExternalCompany        as '@ExternalCompany',
           @Object                 as '@Object'
    
    if not exists(select 1 from Warehouse where WarehouseId in (@FromWarehouseId,@ToWarehouseId))
    begin
      select @ErrorMsg = 'Error executing p_Famous_Import - Warehouse does not exist'
      goto error_header
    end
    
    if @InboundDocumentTypeId is not null and @OutboundDocumentTypeId is not null
    begin
      if @ToWarehouseId is not null
      begin
        select @InboundDocumentTypeCode = InboundDocumentTypeCode,
               @InPriorityId            = PriorityId
          from InboundDocumentType
         where InboundDocumentTypeId = @InboundDocumentTypeId
      end
    end
    
    if @InboundDocumentTypeId is not null and @OutboundDocumentTypeId is null
    begin
      if @ToWarehouseId is null
        select @ToWarehouseId = @FromWarehouseId
      
      select @InboundDocumentTypeCode = InboundDocumentTypeCode,
             @InPriorityId            = PriorityId
        from InboundDocumentType
       where InboundDocumentTypeId = @InboundDocumentTypeId
    end
    
    select @InboundDocumentTypeId  as '@InboundDocumentTypeId' ,
           @OutboundDocumentTypeId as '@OutboundDocumentTypeId',
           @FromWarehouseId        as '@FromWarehouseId       ',
           @ToWarehouseId          as '@ToWarehouseId         ',
           @FromWarehouseCode      as '@FromWarehouseCode',
           @ToWarehouseCode        as '@ToWarehouseCode',
           @ReleaseDate			   as '@ReleaseDate',
           @OrderNumber            as '@OrderNumber',
           @ExternalCompanyCode    as '@ExternalCompanyCode',
           @ExternalCompany   as '@ExternalCompany',
           @Object                 as '@Object'
    
    select @ExternalCompanyCode as '-------ecc-------------',
    @ExternalCompany as '-------ec-------------',
    @Object as '--------ob-------------',
    @FromWarehouseCode as '---------fwc--------'
    
    if @ExternalCompanyCode is null and @ExternalCompany is null and @Object != 'IBT'
    begin
      set @ExternalCompanyCode = @FromWarehouseCode
    end
    
    select @OutboundDocumentTypeCode as '-------odtc-------------',
    @OutboundDocumentTypeId as '-------odtid-------------',
    @ExternalCompanyCode as '--------ecc-------------'
    
    if @OutboundDocumentTypeId is not null
    begin
      
      select @OutboundDocumentTypeCode = OutboundDocumentTypeCode,
             @OutPriorityId            = PriorityId,
             @AutoRelease              = AutoRelease,
             @AddToShipment            = AddToShipment,
             @MultipleOnShipment       = MultipleOnShipment
        from OutboundDocumentType
       where OutboundDocumentTypeId = @OutboundDocumentTypeId

      set @ExternalCompanyId = null
      
      if @ExternalCompanyCode is not null
      begin
        select @ExternalCompanyId   = ExternalCompanyId,
               @ExternalCompanyCode = ExternalCompanyCode,
               @ExternalCompany     = ExternalCompany
          from ExternalCompany (nolock)
         where ExternalCompanyCode = @ExternalCompanyCode
        if @ExternalCompany is null
            Set @ExternalCompany = @ExternalCompanyCode
        if @ExternalCompanyId is null
        begin
          exec @Error = p_ExternalCompany_Insert
           @ExternalCompanyId         = @ExternalCompanyId output,
           @ExternalCompanyTypeId     = 2,
           @RouteId                   = null,
           --@drop
           @ExternalCompany           = @ExternalCompany,
           @ExternalCompanyCode       = @ExternalCompanyCode
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Insert Outbound'
            goto error_header
          end
        end
      end
      else if @OutboundDocumentTypeCode = 'IBT'
      begin
      select '111111',@OutboundDocumentTypeCode as '-------odtc-------------',
    @OutboundDocumentTypeId as '-------odtid-------------',
    @ExternalCompanyCode as '--------ecc-------------',
    @ToWarehouseCode as '------------twc-----------'
        if @ToWarehouseCode is null
        begin
          select @ErrorMsg = 'Error executing p_Famous_Import - To Warehouse is null for IBT'
          goto error_header
        end
        
        select @ExternalCompanyId   = ExternalCompanyId,
               @ExternalCompanyCode = ExternalCompanyCode,
               @ExternalCompany     = ExternalCompany
          from ExternalCompany (nolock)
         where ExternalCompanyCode = substring(@ToWarehouseCode, 1, 4)
select @ExternalCompanyId   as 'ecid',
       @ExternalCompanyCode as 'ecc',
       @ExternalCompany     as 'ec'
         
        
        if @ExternalCompanyId is null
        begin
          set @ExternalCompanyCode = substring(@ToWarehouseCode, 1, 4)
          if @ExternalCompany is null
            Set @ExternalCompany = @ExternalCompanyCode
          exec @Error = p_ExternalCompany_Insert
           @ExternalCompanyId         = @ExternalCompanyId output,
           @ExternalCompanyTypeId     = 1,
           @RouteId                   = null,
           --@drop
           @ExternalCompany           = @ExternalCompanyCode,
           @ExternalCompanyCode       = @ExternalCompanyCode
          select @ExternalCompanyId as '@ExternalCompanyId'
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Insert Inbound'
            goto error_header
          end
        end
      end
      
      select @ODStatusId = dbo.ufn_StatusId('OD','I')

      
      exec @Error = p_OutboundDocument_Insert
       @OutboundDocumentId     = @OutboundDocumentId output,
       @OutboundDocumentTypeId = @OutboundDocumentTypeId,
       @ExternalCompanyId      = @ExternalCompanyId,
       @StatusId               = @ODStatusId,
       @WarehouseId            = @FromWarehouseId,
       @OrderNumber            = @OrderNumber,
       @DeliveryDate           = @ReleaseDate,
       @CreateDate             = @GetDate,
       @ModifiedDate           = null
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_OutboundDocument_Insert'
        goto error_header
      end
    end
    
    if @InboundDocumentTypeCode is not null
    begin
      set @ExternalCompanyId = null
      
      if @ExternalCompanyCode is not null
      begin
        select @ExternalCompanyId   = ExternalCompanyId,
               @ExternalCompanyCode = ExternalCompanyCode,
               @ExternalCompany     = ExternalCompany
          from ExternalCompany (nolock)
         where ExternalCompanyCode = @ExternalCompanyCode
        if @ExternalCompany is null
            Set @ExternalCompany = @ExternalCompanyCode
        if @ExternalCompanyId is null
        begin
          exec @Error = p_ExternalCompany_Insert
           @ExternalCompanyId         = @ExternalCompanyId output,
           @ExternalCompanyTypeId     = 2,
           @RouteId                   = null,
           --@drop
           @ExternalCompany           = @ExternalCompany,
           @ExternalCompanyCode       = @ExternalCompanyCode
          
          if @Error != 0
          begin
           select @ErrorMsg = 'Error executing p_ExternalCompany_Insert 2'
            goto error_header
          end
        end
      end
      else
   begin
        select @ExternalCompanyId   = ExternalCompanyId,
               @ExternalCompanyCode = ExternalCompanyCode,
               @ExternalCompany     = ExternalCompany
          from ExternalCompany (nolock)
         where ExternalCompanyCode = isnull(@FromWarehouseCode, @ToWarehouseCode)
        
        if @ExternalCompanyId is null
        begin
          set @ExternalCompanyCode = isnull(@FromWarehouseCode, @ToWarehouseCode)
          if @ExternalCompany is null
            Set @ExternalCompany = @ExternalCompanyCode
          exec @Error = p_ExternalCompany_Insert
           @ExternalCompanyId         = @ExternalCompanyId output,
           @ExternalCompanyTypeId     = 1,
           @RouteId                   = null,
           --@drop
           @ExternalCompany           = @ExternalCompanyCode,
           @ExternalCompanyCode       = @ExternalCompanyCode
          
          if @Error != 0
          begin
            select @ErrorMsg = 'Error executing p_ExternalCompany_Insert 1'
       goto error_header
          end
        end
      end
      
      select @StatusId = dbo.ufn_StatusId('ID','W')
      
      exec @Error = p_InboundDocument_Insert
       @InboundDocumentId     = @InboundDocumentId output,
       @InboundDocumentTypeId = @InboundDocumentTypeId,
       @ExternalCompanyId     = @ExternalCompanyId,
       @StatusId              = @StatusId,
       @WarehouseId           = @ToWarehouseId,
       @OrderNumber           = @OrderNumber,
       @DeliveryDate          = @GetDate,
       @CreateDate            = @GetDate,
       @ModifiedDate          = null
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_InboundDocument_Insert'
        goto error_header
      end
    end
    
    if @Object = '202'
    begin
    declare detail_cursor cursor for
     select ForeignKey,  -- DocEntry
            LineNumber,  -- LineNum
            ProductCode, -- ItemCode
            Product,     -- Dscription
            Quantity,    -- Quantity
            Additional2  -- TreeType
       from InterfaceImportDetail
  where InterfaceImportHeaderId = @InterfaceImportHeaderId
        and substring(ProductCode, 1, 3) in ('PZA')
    end
    else
    begin
    declare detail_cursor cursor for
     select ForeignKey,  -- DocEntry
      LineNumber,  -- LineNum
            ProductCode, -- ItemCode
            Product,     -- Dscription
            Quantity,    -- Quantity
            Additional2  -- TreeType
       from InterfaceImportDetail
      where InterfaceImportHeaderId = @InterfaceImportHeaderId
        and substring(ProductCode, 1, 3) not in ('SZA','XZA','DZA')
    end
    
    open detail_cursor
    
    fetch detail_cursor
     into @DocEntry,
          @LineNumber,
          @ProductCode,
          @Product,
          @Quantity,
          @TreeType
    
    while (@@fetch_status = 0)
    begin
      if isnull(@TreeType,'') != 'S'
      begin
      set @StorageUnitBatchId = null
      set @StorageUnitId = null
      set @Batch = null
      
      
      Select @DocEntry, @LineNumber, @ProductCode,  @Product, @Quantity,   @TreeType
      
      --if isnull(@OutboundDocumentTypeCode, isnull(@InboundDocumentTypeCode,'')) not in ('SAL','RCP','CNC','MO')
      --  set @Batch = convert(nvarchar(30), @OrderNumber)
      
      exec @Error = p_interface_xml_Product_Insert
       @ProductCode        = @ProductCode,
       @Product            = @Product,
       @Batch              = @Batch,
       @WarehouseId        = @FromWarehouseId,
       @StorageUnitId      = @StorageUnitId output,
       @StorageUnitBatchId = @StorageUnitBatchId output,
       @BatchId            = @BatchId output,
       @ExternalCompanyId  = @ExternalCompanyId
      
      if @Error != 0 or @StorageUnitBatchId is null
      begin
        select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
        Print @ErrorMsg
        goto error_detail
      end
      
      if @OutboundDocumentTypeCode is not null
      begin
       Print 'p_OutboundLine_Insert'

        exec @Error = p_OutboundLine_Insert
         @OutboundLineId     = @OutboundLineId output,
         @OutboundDocumentId = @OutboundDocumentId,
         @StorageUnitId      = @StorageUnitId,
         @StatusId           = @ODStatusId,
         @LineNumber         = @LineNumber,
         @Quantity           = @Quantity,
         @BatchId            = @BatchId
               
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_OutboundLine_Insert'
          goto error_detail
        end
        
        exec @Error = p_Despatch_Create_Issue
         @OutboundDocumentId = @OutboundDocumentId,
         @OutboundLineId     = @OutboundLineId,
         @OperatorId         = null
               
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'
          goto error_detail
        end
      end
      Print 'InboundDocumentType:' + @InboundDocumentTypeCode
      
      if @InboundDocumentTypeCode is not null
      begin
        exec @Error = p_InboundLine_Insert
         @InboundLineId     = @InboundLineId output,
         @InboundDocumentId = @InboundDocumentId,
         @StorageUnitId     = @StorageUnitId,
         @StatusId          = @StatusId,
         @LineNumber        = @LineNumber,
         @Quantity          = @Quantity,
         @BatchId           = @BatchId
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_InboundLine_Insert'
          goto error_detail
        end
        
       exec @Error = p_Receiving_Create_Receipt
         @InboundDocumentId = @InboundDocumentId,
         @InboundLineId     = @InboundLineId,
         @OperatorId        = null
               
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Receiving_Create_Receipt'
          goto error_detail
        end
      end
      
      if @InboundDocumentTypeCode = 'PRV'
      begin
      		select @PalletQuantity = Quantity
      		  from Pack (nolock)
      		 where StorageUnitId = @StorageUnitId
      		   and PackTypeId    = 1
  		   and WarehouseId = @FromWarehouseId
      		
      		if @PalletQuantity = 0 or @PalletQuantity is null
      		begin
          select @ErrorMsg = 'The default pallet quantity cannot be zero.'
          goto error_detail
      		end
        
--        select @Quantity = isnull(di_qty,0) - isnull(di_qty_check,0)
--          from InboundLine
--         where InboundLineId = @InboundLineId
      		select @StatusId = dbo.ufn_StatusId('PR','W')
      		
    		  select @ReceiptLineId = ReceiptLineId
    		    from ReceiptLine
    		   where InboundLineId = @InboundLineId
      		
      		while floor(@Quantity/@PalletQuantity) > 0
      		begin --loop
      		  
      		  exec @Error = p_Palletised_Insert
           @WarehouseId         = @FromWarehouseId,
           @OperatorId          = null,
           @InstructionTypeCode = 'PR',
           @StorageUnitBatchId  = @StorageUnitBatchId,
           @PickLocationId      = null,
           @StoreLocationId     = null,
           @Quantity            = @PalletQuantity,
           @Weight              = @Weight,
           @PriorityId          = @InPriorityId,
           @ReceiptLineId       = @ReceiptLineId,
           @StatusId            = @StatusId
      		  
      		  if @error != 0
      		  begin
            select @ErrorMsg = 'Error executing p_Palletised_Insert'
            goto error_detail
      		  end
      		  
      		  select @Quantity = @Quantity - @PalletQuantity
      		END --loop
      		 
      		/***************************/
      		/* Insert any spare units */
      		/***************************/
      		IF @Quantity > 0
      		BEGIN --dil
      		  
      		  exec @Error = p_Palletised_Insert
           @WarehouseId         = @FromWarehouseId,
           @OperatorId          = null,
           @InstructionTypeCode = 'PR',
           @StorageUnitBatchId  = @StorageUnitBatchId,
           @PickLocationId      = null,
           @StoreLocationId     = null,
           @Quantity            = @Quantity,
           @Weight              = @Weight,
           @PriorityId          = @InPriorityId,
           @ReceiptLineId       = @ReceiptLineId,
           @StatusId            = @StatusId
      		  
      		  if @error != 0
      		  begin
            select @ErrorMsg = 'Error executing p_Palletised_Insert'
            goto error_detail
      		  end
      		  
      		end --dil
      end
      
      if @Object = '21' and @InboundDocumentTypeCode is null and @OutboundDocumentTypeCode is null
      begin
      		select @PalletQuantity = Quantity
      		  from Pack (nolock)
      		 where StorageUnitId = @StorageUnitId
      		   and PackTypeId    = 1
      		   and WarehouseId   = @FromWarehouseId
      		
      		if @PalletQuantity = 0 or @PalletQuantity is null
      		begin
          select @ErrorMsg = 'The default pallet quantity cannot be zero.'
          goto error_detail
      		end
        
      		select @StatusId = dbo.ufn_StatusId('J','W')
      		
      		while floor(@Quantity/@PalletQuantity) > 0
      		begin --loop
      		  
      		  exec @Error = p_Palletised_Insert
           @WarehouseId         = @FromWarehouseId,
           @OperatorId          = null,
           @InstructionTypeCode = 'O',
           @StorageUnitBatchId  = @StorageUnitBatchId,
           @PickLocationId      = null,
           @StoreLocationId     = null,
           @Quantity            = @PalletQuantity,
           @Weight              = @Weight,
           @PriorityId          = @InPriorityId,
           @StatusId            = @StatusId
      		  
      		  if @error != 0
      		  begin
            select @ErrorMsg = 'Error executing p_Palletised_Insert'
            goto error_detail
		  end
      		  
      		  select @Quantity = @Quantity - @PalletQuantity
      		END --loop
      		 
      		/***************************/
      		/* Insert any spare units */
      		/***************************/
      		IF @Quantity > 0
      		BEGIN --dil
      		  
      		  exec @Error = p_Palletised_Insert
           @WarehouseId         = @FromWarehouseId,
           @OperatorId          = null,
           @InstructionTypeCode = 'O',
           @StorageUnitBatchId  = @StorageUnitBatchId,
           @PickLocationId      = null,
           @StoreLocationId     = null,
           @Quantity            = @Quantity,
           @Weight              = @Weight,
           @PriorityId          = @InPriorityId,
           @StatusId            = @StatusId
      		  
      		  if @error != 0
      		  begin
            select @ErrorMsg = 'Error executing p_Palletised_Insert'
            goto error_detail
      		  end
      		  
      		end --dil
      end
      end
      fetch detail_cursor
       into @DocEntry,
            @LineNumber,
            @ProductCode,
            @Product,
            @Quantity,
            @TreeType
    end
    
    close detail_cursor
    deallocate detail_cursor
    
    IF @OutboundDocumentTypeCode is not null
    begin
      exec @Error = p_Despatch_Create_Issue
       @OutboundDocumentId = @OutboundDocumentId,
       @OperatorId         = null,
       @Remarks            = @Remarks
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Despatch_Create_Issue'
        goto error_detail
      end
      
      select @IssueId = IssueId
        from Issue (nolock)
       where OutboundDocumentId = @OutboundDocumentId
      
      if @HoldStatus = '1'
      begin
        select @StatusId = dbo.ufn_StatusId('IS','OH')
        
        exec @Error = p_Issue_Update
         @IssueId = @IssueId,
         @StatusId = @StatusId
      
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
          goto error_detail
        end
      end
      else
      Begin
       select @StatusId = dbo.ufn_StatusId('IS','W')
        exec @Error = p_Issue_Update
         @IssueId = @IssueId,
         @StatusId = @StatusId
      End
      
     if(@HoldStatus != 1)
     Begin
      exec @Error = p_Outbound_Auto_Load
       @OutboundShipmentId = @OutboundShipmentId output,
       @IssueId            = @IssueId
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
        goto error_detail
      end
      
      exec @Error = p_Outbound_Auto_Release
       @OutboundShipmentId = @OutboundShipmentId,
       @IssueId            = @IssueId
       
       select @OutboundShipmentId as 'Shipment', @IssueId as 'Issue', @Error as 'AUTO RELEASE'
      
      if @Error != 0
      begin
        select @ErrorMsg = 'Error executing p_Outbound_Auto_Load'
        goto error_detail
      end
      
      if @Object = '13'
      begin
        exec @Error = p_Outbound_Palletise
         @WarehouseId        = @FromWarehouseId,
         @OutboundShipmentId = @OutboundShipmentId,
         @IssueId            = @IssueId,
         @OperatorId         = null,
         @Weight             = null,
         @Volume             = null
        
        if @Error != 0
        begin
          select @ErrorMsg = 'Error executing p_Outbound_Palletise'
          goto error_detail
        end
        
        --exec p_Invoice_Locate_Pick
        -- @OutboundShipmentId = @OutboundShipmentId,
        -- @IssueId            = @IssueId
        
        --if @Error != 0
        --begin
        --  select @ErrorMsg = 'Error executing p_Invoice_Locate_Pick'
        --  goto error_detail
        --end
      end
     End
 end
    
    error_detail:
   error_header:
    
    if @Error = 0
   begin
      if @@trancount > 0
        commit transaction xml_import
    end
    else
    begin
      if @@trancount > 0
        rollback transaction xml_import
      
 update InterfaceImportHeader
         set RecordStatus = 'E',
             ProcessedDate = @GetDate,
			 ErrorMsg = @ErrorMsg
       where InterfaceImportHeaderId = @InterfaceImportHeaderId
    end
    
    fetch header_cursor
     into @InterfaceImportHeaderId,
        @Object,
        @DocEntry,
        @OrderNumber,
        @ExternalCompanyCode,
        @ExternalCompany,
        @Address,
        @Remarks,
        @TrnspCode,
        @FromWarehouseCode,
        @ToWarehouseCode,
        @HoldStatus,
        @ReleaseDate,
        @HostStatus
   
	  If len(@ExternalCompanyCode) = 0
	    Set @ExternalCompanyCode = null
		
	  If len(@ExternalCompany) = 0
		Set @ExternalCompany = null
		
	  If len(@Address) = 0
		Set @Address = null
	  
	 -- If len(@ToWarehouseCode) = 0
		--Set @ToWarehouseCode = null
	  
	 -- If len(@FromWarehouseCode) = 0
		--Set @FromWarehouseCode = null
	          
  end
  
  close header_cursor
  deallocate header_cursor
  

  update InterfaceImportHeader
     set ProcessedDate = null
   where RecordStatus = 'N'
     and ProcessedDate = @GetDate
  
  if(object_id('tempdb.dbo.##p_Famous_Import') is not null)
    drop table ##p_Famous_Import
  
  return
end
