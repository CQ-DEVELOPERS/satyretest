﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSKU_Delete
  ///   Filename       : p_InterfaceImportSKU_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:06
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceImportSKU table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSKUId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSKU_Delete
(
 @InterfaceImportSKUId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceImportSKU
     where InterfaceImportSKUId = @InterfaceImportSKUId
  
  select @Error = @@Error
  
  
  return @Error
  
end
