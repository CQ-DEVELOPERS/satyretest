﻿/*
  /// <summary>
  ///   Procedure Name : p_Batch_Search_ExpiryDate
  ///   Filename       : p_Batch_Search_ExpiryDate.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : Paballo Moloi
  ///   Modified Date  : 20 June 2017
  ///   Details        : Sorted the batches in ascending order
  /// </newpara>
*/
create procedure p_Batch_Search_ExpiryDate
(

 @StorageUnitId int 
)

as
begin
	 set nocount on;
	 
  select sub.StorageUnitBatchId,
         b.BatchId,
         b.Batch
    from StorageUnitBatch sub (nolock)
    join Batch            b   (nolock) on sub.BatchId = b.BatchId
   where sub.StorageUnitId                   = @StorageUnitId
   order by b.Batch asc
     --and isnull(b.ExpiryDate, b.CreateDate) >= dateadd(dd, -100, getdate())
	 end
