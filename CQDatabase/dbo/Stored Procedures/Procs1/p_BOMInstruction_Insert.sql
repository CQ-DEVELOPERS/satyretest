﻿
/*
  /// <summary>
  ///   Procedure Name : p_BOMInstruction_Insert
  ///   Filename       : BOM.sql
  ///   Create By      : Ruan Groenewald
  ///   Date Created   : 01 Sep 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   [@StorageUnitId] [int] NOT NULL,

  /// </param>
*/
create procedure p_BOMInstruction_Insert
     @BOMHeaderId        int
    ,@Quantity           int
    ,@OrderNumber        nvarchar(30)
    ,@ParentId           int = NULL
    ,@OutboundShipmentId int = null
    ,@IssueId            int = null
as
begin
    SET NOCOUNT ON;
    
    DECLARE @Error int,
            @BOMInstructionId int,
            @count int
    
    if @OutboundShipmentId = -1
      set @OutboundShipmentId = null
    
    if @IssueId = -1
      set @IssueId = null
    
    begin transaction
    
    if @OutboundShipmentId is not null
    begin
      update il
         set ConfirmedQuatity = isnull(il.ConfirmedQuatity, 0) + @Quantity
        from OutboundShipmentIssue osi (nolock)
        join IssueLine              il (nolock) on osi.IssueId = il.IssueId
       where osi.OutboundShipmentId = @OutboundShipmentId
      
      select @Error = @@Error
      
      if @Error != 0
        goto Error
      
      if exists(select top 1 1
                  from OutboundShipmentIssue osi (nolock)
                  join IssueLine              il (nolock) on osi.IssueId = il.IssueId
                 where osi.OutboundShipmentId = @OutboundShipmentId
                   and il.ConfirmedQuatity >= il.Quantity)
      begin
        update i
           set StatusId = dbo.ufn_StatusId('IS','P')
         from OutboundShipmentIssue osi (nolock)
         join Issue                   i (nolock) on osi.IssueId = i.IssueId
        where osi.OutboundShipmentId = @OutboundShipmentId
        
        select @Error = @@Error
        
        if @Error != 0
          goto Error
      end
    end
    else if @IssueId is not null
    begin
      
      update IssueLine
         set ConfirmedQuatity = isnull(ConfirmedQuatity, 0) + @Quantity
       where IssueId = @IssueId
      
      select @Error = @@Error
      
      if @Error != 0
        goto Error
      
      if exists(select top 1 1 from IssueLine where IssueId = @IssueId and ConfirmedQuatity >= Quantity)
      begin
        update Issue
           set StatusId = dbo.ufn_StatusId('IS','P')
         where IssueId = @IssueId
        
        select @Error = @@Error
        
        if @Error != 0
          goto Error
      end
    end
    
    select @count = COUNT(1) + 1
      from BOMInstruction (nolock)
     where BOMHeaderId = @BOMHeaderId
    
    if @count > 1
      select @OrderNumber = @OrderNumber + '(' + convert(nvarchar(10), @count) + ')'
    
    INSERT INTO [dbo].[BOMInstruction]
           ([BOMHeaderId]
           ,[Quantity]
           ,[OrderNumber]
           ,[ParentId]
           ,OutboundShipmentId
           ,IssueId)
     VALUES
           (@BOMHeaderId
           ,@Quantity
           ,@OrderNumber
           ,@ParentId
           ,@OutboundShipmentId
           ,@IssueId)
    
    select @Error = @@Error, @BOMInstructionId = @@IDENTITY
    
    if @Error != 0
      goto Error
    
    INSERT INTO [dbo].[BOMInstructionLine]
           ([BOMInstructionId]
           ,[BOMLineId]
           ,[LineNumber]
           ,[Quantity])
    
    SELECT @BOMInstructionId
        ,bl.BOMLineId
        ,1
        ,@Quantity
    FROM BOMInstruction bi
    INNER JOIN BOMHeader bh on bi.BOMHeaderId = bh.BOMHeaderId
    INNER JOIN BOMLine bl on bh.BOMHeaderId = bl.BOMHeaderId
    WHERE bi.BOMInstructionId = @BOMInstructionId
  
  select @Error = @@Error
  
  if @Error != 0
    goto Error
  
  commit transaction
  RETURN @Error
  
  Error:
  rollback transaction
  return @Error
end

