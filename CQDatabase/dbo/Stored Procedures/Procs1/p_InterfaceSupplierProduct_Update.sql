﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceSupplierProduct_Update
  ///   Filename       : p_InterfaceSupplierProduct_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:48
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceSupplierProduct table.
  /// </remarks>
  /// <param>
  ///   @SupplierCode nvarchar(60) = null,
  ///   @ProductCode nvarchar(100) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceSupplierProduct_Update
(
 @SupplierCode nvarchar(60) = null,
 @ProductCode nvarchar(100) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceSupplierProduct
     set SupplierCode = isnull(@SupplierCode, SupplierCode),
         ProductCode = isnull(@ProductCode, ProductCode),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         RecordStatus = isnull(@RecordStatus, RecordStatus) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
