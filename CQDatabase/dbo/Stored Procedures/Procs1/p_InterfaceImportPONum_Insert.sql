﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPONum_Insert
  ///   Filename       : p_InterfaceImportPONum_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:00
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportPONum table.
  /// </remarks>
  /// <param>
  ///   @id nvarchar(40) = null,
  ///   @Ordernumber nvarchar(100) = null,
  ///   @ReferenceNumber nvarchar(100) = null,
  ///   @docstate nvarchar(100) = null,
  ///   @statusId nvarchar(100) = null,
  ///   @OrderDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportPONum.id,
  ///   InterfaceImportPONum.Ordernumber,
  ///   InterfaceImportPONum.ReferenceNumber,
  ///   InterfaceImportPONum.docstate,
  ///   InterfaceImportPONum.statusId,
  ///   InterfaceImportPONum.OrderDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPONum_Insert
(
 @id nvarchar(40) = null,
 @Ordernumber nvarchar(100) = null,
 @ReferenceNumber nvarchar(100) = null,
 @docstate nvarchar(100) = null,
 @statusId nvarchar(100) = null,
 @OrderDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportPONum
        (id,
         Ordernumber,
         ReferenceNumber,
         docstate,
         statusId,
         OrderDate)
  select @id,
         @Ordernumber,
         @ReferenceNumber,
         @docstate,
         @statusId,
         @OrderDate 
  
  select @Error = @@Error
  
  
  return @Error
  
end
