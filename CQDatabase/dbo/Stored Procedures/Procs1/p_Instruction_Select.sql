﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Select
  ///   Filename       : p_Instruction_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 Aug 2013 13:13:51
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Instruction table.
  /// </remarks>
  /// <param>
  ///   @InstructionId int = null 
  /// </param>
  /// <returns>
  ///   Instruction.InstructionId,
  ///   Instruction.ReasonId,
  ///   Instruction.InstructionTypeId,
  ///   Instruction.StorageUnitBatchId,
  ///   Instruction.WarehouseId,
  ///   Instruction.StatusId,
  ///   Instruction.JobId,
  ///   Instruction.OperatorId,
  ///   Instruction.PickLocationId,
  ///   Instruction.StoreLocationId,
  ///   Instruction.ReceiptLineId,
  ///   Instruction.IssueLineId,
  ///   Instruction.InstructionRefId,
  ///   Instruction.Quantity,
  ///   Instruction.ConfirmedQuantity,
  ///   Instruction.Weight,
  ///   Instruction.ConfirmedWeight,
  ///   Instruction.PalletId,
  ///   Instruction.CreateDate,
  ///   Instruction.StartDate,
  ///   Instruction.EndDate,
  ///   Instruction.Picked,
  ///   Instruction.Stored,
  ///   Instruction.CheckQuantity,
  ///   Instruction.CheckWeight,
  ///   Instruction.OutboundShipmentId,
  ///   Instruction.DropSequence,
  ///   Instruction.PackagingWeight,
  ///   Instruction.PackagingQuantity,
  ///   Instruction.NettWeight,
  ///   Instruction.PreviousQuantity,
  ///   Instruction.AuthorisedBy,
  ///   Instruction.AuthorisedStatus,
  ///   Instruction.SOH,
  ///   Instruction.AutoComplete 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Select
(
 @InstructionId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Instruction.InstructionId
        ,Instruction.ReasonId
        ,Instruction.InstructionTypeId
        ,Instruction.StorageUnitBatchId
        ,Instruction.WarehouseId
        ,Instruction.StatusId
        ,Instruction.JobId
        ,Instruction.OperatorId
        ,Instruction.PickLocationId
        ,Instruction.StoreLocationId
        ,Instruction.ReceiptLineId
        ,Instruction.IssueLineId
        ,Instruction.InstructionRefId
        ,Instruction.Quantity
        ,Instruction.ConfirmedQuantity
        ,Instruction.Weight
        ,Instruction.ConfirmedWeight
        ,Instruction.PalletId
        ,Instruction.CreateDate
        ,Instruction.StartDate
        ,Instruction.EndDate
        ,Instruction.Picked
        ,Instruction.Stored
        ,Instruction.CheckQuantity
        ,Instruction.CheckWeight
        ,Instruction.OutboundShipmentId
        ,Instruction.DropSequence
        ,Instruction.PackagingWeight
        ,Instruction.PackagingQuantity
        ,Instruction.NettWeight
        ,Instruction.PreviousQuantity
        ,Instruction.AuthorisedBy
        ,Instruction.AuthorisedStatus
        ,Instruction.SOH
        ,Instruction.AutoComplete
    from Instruction
   where isnull(Instruction.InstructionId,'0')  = isnull(@InstructionId, isnull(Instruction.InstructionId,'0'))
  order by AuthorisedStatus
  
end
