﻿/*  
  /// <summary>  
  ///   Procedure Name : p_Daily_Product_Movement_Summary  
  ///   Filename       : p_Daily_Product_Movement_Summary.sql  
  ///   Create By      : Karen  
  ///   Date Created   : 7 Jan 2011  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Daily_Product_Movement_Summary  
  
  
as  
begin  
  set nocount on;  
    
	declare @Weeks		int,  
			@Count		int,  
			@Date		datetime,  
			@FromDate	datetime,  
			@ToDate		datetime            
    
    
	declare @TableResult as table  
			(
			StorageUnitId		int, 
			StorageUnitBatchId	int, 
			Opening				int,  
			Received			int,  
			Received2			int,  
			Issued				int,  
			Balance				int,  
			Variance			int,  
			Percentage			int,  
			StockOnHand			int,  
			WarehouseId			int,  
			ProductId			int,  
			SKUId				int  
			)  
    
	declare @Products  as table  
			(  
			StorageUnitId		int,  
			StorageUnitBatchId	int,
			ProductId			int,   
			WarehouseId			int,
			SOH					int  
			)  

    
	set @FromDate = (SELECT DATEADD(day, -1, (CONVERT (Date, SYSDATETIME()))))  
	--set @FromDate = '2011-06-02'  
	set @ToDate = @FromDate  
  
  
	insert	@Products  
		   (StorageUnitId, 
			StorageUnitBatchId, 
			ProductId,  
			WarehouseId,
			SOH)  
	select distinct 
			su.StorageUnitId,  
			sub.StorageUnitBatchId,
			su.ProductId,  
			a.WarehouseId,
			SUM(subl.ActualQuantity)
			--CASE 
			--	WHEN a.StockOnHand      = 1 THEN SUM(subl.ActualQuantity)
			--	WHEN a.StockOnHand      != 1 THEN 0 
			--END 			  
	  from StorageUnit su  
	  join StorageUnitBatch          sub (nolock) on su.StorageUnitId = sub.StorageUnitId
	  join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
	  join AreaLocation               al (nolock) on al.LocationId = subl.LocationId
	  join Area                        a (nolock) on al.AreaId = a.AreaId
	  where a.StockOnHand      = 1
	  group by	su.productid,
				su.StorageUnitId,
				sub.StorageUnitBatchId,   
				a.warehouseid,
				a.StockOnHand  
	 order by	su.productid, 
				su.StorageUnitId,
				sub.StorageUnitBatchId, 
				a.warehouseid      
      
    insert @TableResult  
          (StorageUnitId,  
           StorageUnitBatchId,
           ProductId,  
           WarehouseId,
           StockOnHand)  
    select distinct 
           p.StorageUnitId,  
           StorageUnitBatchId,
           p.ProductId,  
           WarehouseId,
           SOH
      from @Products p  

   
	update tr  
       set Received = ((select sum(rl.AcceptedQuantity)  
						  from ReceiptLine        rl (nolock)  
						  join Receipt             r (nolock) on rl.ReceiptId = r.ReceiptId  
						  join status              s (nolock) on rl.statusid = s.statusid  
						  join StorageUnitBatch  sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId  
					     where s.statuscode = 'RC'  
						   and CONVERT(datetime, CONVERT(varchar,DeliveryDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  
						   and tr.StorageUnitId = sub.StorageUnitId  
						   and tr.StorageUnitBatchId = sub.StorageUnitBatchId
						   and tr.WarehouseId   = r.WarehouseId))  
      from @TableResult tr  
         
	update tr  
       set Received2 = (select sum(i.ConfirmedQuantity)  
                          from Instruction        i (nolock)  
                          join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId  
                         where it.InstructionTypeCode in ('S','SM','PR') -- Store, Store mixed, Production receipt  
						   and CONVERT(datetime, CONVERT(varchar,i.EndDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  
						   and tr.StorageUnitBatchId = i.StorageUnitBatchId
						   and tr.WarehouseId   = i.WarehouseId)  
      from @TableResult tr  
      
    
	update tr  
       set Issued = (select sum(i.ConfirmedQuantity)  
                       from Instruction        i (nolock)  
                       join InstructionType   it (nolock) on i.InstructionTypeId = it.InstructionTypeId  
                      where it.InstructionTypeCode in ('P','PS','FM','PM') -- Pick, Single sku, Full/Mixed, Mixed pick  
                        and CONVERT(datetime, CONVERT(varchar,i.EndDate,101)) BETWEEN CONVERT(datetime, CONVERT(varchar,@FromDate,101)) AND CONVERT(datetime, CONVERT(varchar,@ToDate,101))  
                        and tr.StorageUnitBatchId = i.StorageUnitBatchId
                        and tr.WarehouseId = i.WarehouseId)  
      from @TableResult tr  
   
	update tr  
       set Balance = StockOnHand  
      from @TableResult tr            
 
	update tr  
       set Opening =  isnull(StockOnHand,0) - isnull(Received,0) + isnull(Issued,0)  
      from @TableResult tr    
	   
	INSERT	ProductMovement  
			(WarehouseId  
			,StorageUnitId
			,StorageUnitBatchId
			,ProductCode  
			,MovementDate  
			,OpeningBalance  
			,ReceivedQuantity  
			,IssuedQuantity  
			,Balance)  
	select	distinct   
			 tr.WarehouseId  
			,tr.StorageUnitId
			,tr.StorageUnitBatchId
			,p.ProductCode  
			,@FromDate  
			,tr.Opening  
			,(isnull(tr.Received,0) + isnull(tr.Received2,0))  
			,tr.Issued  
			,tr.balance  
	  from @TableResult tr  
	  join StorageUnit                su (nolock) on tr.StorageUnitId       = su.StorageUnitId  
	  join Product                     p (nolock) on su.ProductId           = p.ProductId  
  
end  
