﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Select
  ///   Filename       : p_ExternalCompany_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:44:49
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyId int = null 
  /// </param>
  /// <returns>
  ///   ExternalCompany.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompanyTypeId,
  ///   ExternalCompany.RouteId,
  ///   ExternalCompany.ExternalCompany,
  ///   ExternalCompany.ExternalCompanyCode,
  ///   ExternalCompany.Rating,
  ///   ExternalCompany.RedeliveryIndicator,
  ///   ExternalCompany.QualityAssuranceIndicator,
  ///   ExternalCompany.ContainerTypeId,
  ///   ExternalCompany.Backorder,
  ///   ExternalCompany.HostId,
  ///   ExternalCompany.OrderLineSequence,
  ///   ExternalCompany.DropSequence,
  ///   ExternalCompany.AutoInvoice,
  ///   ExternalCompany.OneProductPerPallet,
  ///   ExternalCompany.PrincipalId,
  ///   ExternalCompany.ProcessId,
  ///   ExternalCompany.TrustedDelivery,
  ///   ExternalCompany.OutboundSLAHours,
  ///   ExternalCompany.PricingCategoryId,
  ///   ExternalCompany.LabelingCategoryId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Select
(
 @ExternalCompanyId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ExternalCompany.ExternalCompanyId
        ,ExternalCompany.ExternalCompanyTypeId
        ,ExternalCompany.RouteId
        ,ExternalCompany.ExternalCompany
        ,ExternalCompany.ExternalCompanyCode
        ,ExternalCompany.Rating
        ,ExternalCompany.RedeliveryIndicator
        ,ExternalCompany.QualityAssuranceIndicator
        ,ExternalCompany.ContainerTypeId
        ,ExternalCompany.Backorder
        ,ExternalCompany.HostId
        ,ExternalCompany.OrderLineSequence
        ,ExternalCompany.DropSequence
        ,ExternalCompany.AutoInvoice
        ,ExternalCompany.OneProductPerPallet
        ,ExternalCompany.PrincipalId
        ,ExternalCompany.ProcessId
        ,ExternalCompany.TrustedDelivery
        ,ExternalCompany.OutboundSLAHours
        ,ExternalCompany.PricingCategoryId
        ,ExternalCompany.LabelingCategoryId
    from ExternalCompany
   where isnull(ExternalCompany.ExternalCompanyId,'0')  = isnull(@ExternalCompanyId, isnull(ExternalCompany.ExternalCompanyId,'0'))
  order by ExternalCompany
  
end
 
