﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceInvoice_Parameter
  ///   Filename       : p_InterfaceInvoice_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:22
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceInvoice table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceInvoice.InterfaceInvoiceId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceInvoice_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceInvoiceId
        ,null as 'InterfaceInvoice'
  union
  select
         InterfaceInvoice.InterfaceInvoiceId
        ,InterfaceInvoice.InterfaceInvoiceId as 'InterfaceInvoice'
    from InterfaceInvoice
  
end
