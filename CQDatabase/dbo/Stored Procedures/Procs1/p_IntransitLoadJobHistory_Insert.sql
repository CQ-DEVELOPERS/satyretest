﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoadJobHistory_Insert
  ///   Filename       : p_IntransitLoadJobHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012 11:58:11
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the IntransitLoadJobHistory table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadJobId int = null,
  ///   @IntransitLoadId int = null,
  ///   @StatusId int = null,
  ///   @JobId int = null,
  ///   @LoadedById int = null,
  ///   @UnLoadedById int = null,
  ///   @CreateDate datetime = null,
  ///   @ReceivedDate datetime = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   IntransitLoadJobHistory.IntransitLoadJobId,
  ///   IntransitLoadJobHistory.IntransitLoadId,
  ///   IntransitLoadJobHistory.StatusId,
  ///   IntransitLoadJobHistory.JobId,
  ///   IntransitLoadJobHistory.LoadedById,
  ///   IntransitLoadJobHistory.UnLoadedById,
  ///   IntransitLoadJobHistory.CreateDate,
  ///   IntransitLoadJobHistory.ReceivedDate,
  ///   IntransitLoadJobHistory.CommandType,
  ///   IntransitLoadJobHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoadJobHistory_Insert
(
 @IntransitLoadJobId int = null,
 @IntransitLoadId int = null,
 @StatusId int = null,
 @JobId int = null,
 @LoadedById int = null,
 @UnLoadedById int = null,
 @CreateDate datetime = null,
 @ReceivedDate datetime = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  insert IntransitLoadJobHistory
        (IntransitLoadJobId,
         IntransitLoadId,
         StatusId,
         JobId,
         LoadedById,
         UnLoadedById,
         CreateDate,
         ReceivedDate,
         CommandType,
         InsertDate)
  select @IntransitLoadJobId,
         @IntransitLoadId,
         @StatusId,
         @JobId,
         @LoadedById,
         @UnLoadedById,
         @CreateDate,
         @ReceivedDate,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
