﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOM_Packaging_Insert
  ///   Filename       : p_BOM_Packaging_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Dec 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOM_Packaging_Insert
(
@operatorId       int,
@bomInstructionId int,
@storageUnitId    int,
@quantity         float
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  insert BOMPackaging
        (OperatorId,
         CreateDate,
         BOMInstructionId,
         StorageUnitId,
         Quantity)
  select @operatorId,
         @GetDate,
         @bomInstructionId,
         @storageUnitId,
         @quantity
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_BOM_Packaging_Insert'); 
    rollback transaction
    return @Error
end
