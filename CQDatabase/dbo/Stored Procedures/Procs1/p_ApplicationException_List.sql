﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ApplicationException_List
  ///   Filename       : p_ApplicationException_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:37
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ApplicationException table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ApplicationException.ApplicationExceptionId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ApplicationException_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ApplicationExceptionId
        ,null as 'ApplicationException'
  union
  select
         ApplicationException.ApplicationExceptionId
        ,ApplicationException.ApplicationExceptionId as 'ApplicationException'
    from ApplicationException
  
end
