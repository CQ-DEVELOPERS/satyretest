﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportPOHeader_Insert
  ///   Filename       : p_InterfaceExportPOHeader_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:07
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceExportPOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportPOHeaderId int = null output,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @RecordType char(3) = null,
  ///   @RecordStatus char(1) = null,
  ///   @SupplierCode nvarchar(60) = null,
  ///   @Supplier nvarchar(510) = null,
  ///   @Address nvarchar(510) = null,
  ///   @FromWarehouseCode nvarchar(20) = null,
  ///   @ToWarehouseCode nvarchar(20) = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @ContainerNumber nvarchar(60) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @NumberOfLines int = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @InsertDate datetime = null,
  ///   @ReceiptId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportPOHeader.InterfaceExportPOHeaderId,
  ///   InterfaceExportPOHeader.PrimaryKey,
  ///   InterfaceExportPOHeader.OrderNumber,
  ///   InterfaceExportPOHeader.RecordType,
  ///   InterfaceExportPOHeader.RecordStatus,
  ///   InterfaceExportPOHeader.SupplierCode,
  ///   InterfaceExportPOHeader.Supplier,
  ///   InterfaceExportPOHeader.Address,
  ///   InterfaceExportPOHeader.FromWarehouseCode,
  ///   InterfaceExportPOHeader.ToWarehouseCode,
  ///   InterfaceExportPOHeader.DeliveryNoteNumber,
  ///   InterfaceExportPOHeader.ContainerNumber,
  ///   InterfaceExportPOHeader.SealNumber,
  ///   InterfaceExportPOHeader.DeliveryDate,
  ///   InterfaceExportPOHeader.Remarks,
  ///   InterfaceExportPOHeader.NumberOfLines,
  ///   InterfaceExportPOHeader.Additional1,
  ///   InterfaceExportPOHeader.Additional2,
  ///   InterfaceExportPOHeader.Additional3,
  ///   InterfaceExportPOHeader.Additional4,
  ///   InterfaceExportPOHeader.Additional5,
  ///   InterfaceExportPOHeader.ProcessedDate,
  ///   InterfaceExportPOHeader.Additional6,
  ///   InterfaceExportPOHeader.Additional7,
  ///   InterfaceExportPOHeader.Additional8,
  ///   InterfaceExportPOHeader.Additional9,
  ///   InterfaceExportPOHeader.Additional10,
  ///   InterfaceExportPOHeader.InsertDate,
  ///   InterfaceExportPOHeader.ReceiptId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportPOHeader_Insert
(
 @InterfaceExportPOHeaderId int = null output,
 @PrimaryKey nvarchar(60) = null,
 @OrderNumber nvarchar(60) = null,
 @RecordType char(3) = null,
 @RecordStatus char(1) = null,
 @SupplierCode nvarchar(60) = null,
 @Supplier nvarchar(510) = null,
 @Address nvarchar(510) = null,
 @FromWarehouseCode nvarchar(20) = null,
 @ToWarehouseCode nvarchar(20) = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @ContainerNumber nvarchar(60) = null,
 @SealNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @Remarks nvarchar(510) = null,
 @NumberOfLines int = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @InsertDate datetime = null,
 @ReceiptId int = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceExportPOHeaderId = '-1'
    set @InterfaceExportPOHeaderId = null;
  
	 declare @Error int
 
  insert InterfaceExportPOHeader
        (PrimaryKey,
         OrderNumber,
         RecordType,
         RecordStatus,
         SupplierCode,
         Supplier,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         DeliveryNoteNumber,
         ContainerNumber,
         SealNumber,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         ProcessedDate,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10,
         InsertDate,
         ReceiptId)
  select @PrimaryKey,
         @OrderNumber,
         @RecordType,
         @RecordStatus,
         @SupplierCode,
         @Supplier,
         @Address,
         @FromWarehouseCode,
         @ToWarehouseCode,
         @DeliveryNoteNumber,
         @ContainerNumber,
         @SealNumber,
         @DeliveryDate,
         @Remarks,
         @NumberOfLines,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @ProcessedDate,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9,
         @Additional10,
         isnull(@InsertDate, getdate()),
         @ReceiptId 
  
  select @Error = @@Error, @InterfaceExportPOHeaderId = scope_identity()
  
  
  return @Error
  
end
