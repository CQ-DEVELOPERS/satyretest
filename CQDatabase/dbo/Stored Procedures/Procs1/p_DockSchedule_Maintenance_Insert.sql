﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DockSchedule_Maintenance_Insert
  ///   Filename       : p_DockSchedule_Maintenance_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 24 May 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DockSchedule_Maintenance_Insert
(
 @OperatorId                      int,
 @Subject                         nvarchar(1024),
 @PlannedStart                    datetime,
 @PlannedEnd                      datetime,
 @LocationId                      int,
 @RecurrenceRule                  nvarchar(1024),
 @RecurrenceParentID              int,
 @Description                     nvarchar(max),
 @OutboundShipmentId              int = null,
 @IssueId                         int = null,
 @InboundShipmentId               int = null,
 @ReceiptId                       int = null,
 @CssClass                        nvarchar(50) = 'rsCategoryBlue',
 @Version                         int = 0
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @VehicleId         int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @IssueId = -1
    set @IssueId = null
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @ReceiptId = -1
    set @ReceiptId = null
  
  ---- Code for Demo
  --if @Version = 1 and @PlannedEnd is not null
  --   set @PlannedEnd = DATEADD(mi, 60, @PlannedEnd)
  --else
  --if @Version = 2 and @PlannedEnd is not null
  --   set @PlannedEnd = DATEADD(mi, 60, @PlannedEnd)
    
  
  begin transaction
  
  if @Subject is null and (@OutboundShipmentId is not null or @IssueId is not null)
  begin
    if @OutboundShipmentId is not null
    begin
      select @LocationId = isnull(@LocationId, LocationId)
        from DockSchedule
       where OutboundShipmentId = @OutboundShipmentId
      
      select @Subject = convert(nvarchar(30), OutboundShipmentId) + ' - ' + isnull(Route,''),
             @VehicleId = VehicleId,
             @LocationId = isnull(@LocationId, LocationId)
        from OutboundShipment (nolock)
       where OutboundShipmentId = @OutboundShipmentId
      
      delete [DockSchedule]
       where OutboundShipmentId = @OutboundShipmentId
         and ([Version] = @Version or @Version is null)
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
      
      update OutboundShipment
         set ShipmentDate = @PlannedStart
       where OutboundShipmentId = @OutboundShipmentId
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
     end
     else
     begin
      select @LocationId = isnull(@LocationId, LocationId)
        from DockSchedule
       where IssueId = @IssueId
      
      select @Subject = od.OrderNumber + ' - ' + isnull(Route,''),
             @VehicleId = i.VehicleId,
             @LocationId = isnull(@LocationId, i.LocationId)
        from Issue             i (nolock)
        join OutboundDocument od (nolock) on i.OutboundDocumentId = od.OutboundDocumentId
        left
        join Route             r (nolock) on i.RouteId = r.RouteId
       where i.IssueId = @IssueId
      
      delete [DockSchedule]
       where IssueId = @IssueId
         and ([Version] = @Version or @Version is null)
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
      
      update Issue
         set DeliveryDate = @PlannedStart
       where IssueId = @IssueId
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
    end
  end
  
  if @Subject is null and (@InboundShipmentId is not null or @ReceiptId is not null)
  begin
    if @InboundShipmentId is not null
    begin
      select @LocationId = isnull(@LocationId, LocationId)
        from DockSchedule
       where InboundShipmentId = @InboundShipmentId
      
      select @Subject = convert(nvarchar(30), InboundShipmentId),
             @VehicleId = VehicleId,
             @LocationId = isnull(@LocationId, LocationId)
        from InboundShipment (nolock)
       where InboundShipmentId = @InboundShipmentId
      
      delete [DockSchedule]
       where InboundShipmentId = @InboundShipmentId
         and ([Version] = @Version or @Version is null)
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
      
      update InboundShipment
         set ShipmentDate = @PlannedStart
       where InboundShipmentId = @InboundShipmentId
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
    end
    else
    begin
      select @LocationId = isnull(@LocationId, LocationId)
        from DockSchedule
       where ReceiptId = @ReceiptId
      
      select @Subject = id.OrderNumber + ' - ' + ec.ExternalCompany,
             @VehicleId = r.VehicleId,
             @LocationId = isnull(@LocationId, r.LocationId)
        from Receipt           r (nolock)
        join InboundDocument  id (nolock) on r.InboundDocumentId = id.InboundDocumentId 
        join ExternalCompany  ec (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
       where r.ReceiptId = @ReceiptId
      
      delete [DockSchedule]
       where ReceiptId = @ReceiptId
         and ([Version] = @Version or @Version is null)
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
      
      update Receipt
         set PlannedDeliveryDate = @PlannedStart,
             LocationId          = @LocationId
       where ReceiptId = @ReceiptId
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
    end
  end
  
  if @Subject is null
    set @Subject = ''
  
  select @PlannedEnd = dateadd(mi, MinutesToLoad, @PlannedStart)
    from VehicleLocation
   where VehicleId = @VehicleId
     and LocationId = @LocationId
  
  insert [DockSchedule]
        ([Subject], [PlannedStart], [PlannedEnd], [RecurrenceRule], [RecurrenceParentID], [Description], [LocationId], [OutboundShipmentId], [IssueId], [InboundShipmentId], [ReceiptId], CssClass, [Version])
  values (@Subject, @PlannedStart, @PlannedEnd, @RecurrenceRule, @RecurrenceParentID, @Description, @LocationId, @OutboundShipmentId, @IssueId, @InboundShipmentId, @ReceiptId, @CssClass, @Version)
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_DockSchedule_Maintenance_Insert'); 
    rollback transaction
    return @Error
end
 
 
 
