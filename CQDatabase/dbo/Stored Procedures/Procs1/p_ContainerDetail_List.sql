﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerDetail_List
  ///   Filename       : p_ContainerDetail_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:56
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ContainerDetail table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ContainerDetail.ContainerDetailId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerDetail_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ContainerDetailId
        ,null as 'ContainerDetail'
  union
  select
         ContainerDetail.ContainerDetailId
        ,ContainerDetail.ContainerDetailId as 'ContainerDetail'
    from ContainerDetail
  
end
