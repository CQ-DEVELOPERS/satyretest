﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportHeader_Insert
  ///   Filename       : p_InterfaceExportHeader_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:01
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceExportHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportHeaderId int = null output,
  ///   @IssueId int = null,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @RecordType nvarchar(60) = null,
  ///   @RecordStatus char(1) = null,
  ///   @CompanyCode nvarchar(60) = null,
  ///   @Company nvarchar(510) = null,
  ///   @Address nvarchar(510) = null,
  ///   @FromWarehouseCode nvarchar(20) = null,
  ///   @ToWarehouseCode nvarchar(20) = null,
  ///   @Route nvarchar(100) = null,
  ///   @DeliveryNoteNumber nvarchar(60) = null,
  ///   @ContainerNumber nvarchar(60) = null,
  ///   @SealNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @NumberOfLines int = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null,
  ///   @WebServiceMsg nvarchar(max) = null 
  /// </param>
  /// <returns>
  ///   InterfaceExportHeader.InterfaceExportHeaderId,
  ///   InterfaceExportHeader.IssueId,
  ///   InterfaceExportHeader.PrimaryKey,
  ///   InterfaceExportHeader.OrderNumber,
  ///   InterfaceExportHeader.RecordType,
  ///   InterfaceExportHeader.RecordStatus,
  ///   InterfaceExportHeader.CompanyCode,
  ///   InterfaceExportHeader.Company,
  ///   InterfaceExportHeader.Address,
  ///   InterfaceExportHeader.FromWarehouseCode,
  ///   InterfaceExportHeader.ToWarehouseCode,
  ///   InterfaceExportHeader.Route,
  ///   InterfaceExportHeader.DeliveryNoteNumber,
  ///   InterfaceExportHeader.ContainerNumber,
  ///   InterfaceExportHeader.SealNumber,
  ///   InterfaceExportHeader.DeliveryDate,
  ///   InterfaceExportHeader.Remarks,
  ///   InterfaceExportHeader.NumberOfLines,
  ///   InterfaceExportHeader.Additional1,
  ///   InterfaceExportHeader.Additional2,
  ///   InterfaceExportHeader.Additional3,
  ///   InterfaceExportHeader.Additional4,
  ///   InterfaceExportHeader.Additional5,
  ///   InterfaceExportHeader.Additional6,
  ///   InterfaceExportHeader.Additional7,
  ///   InterfaceExportHeader.Additional8,
  ///   InterfaceExportHeader.Additional9,
  ///   InterfaceExportHeader.Additional10,
  ///   InterfaceExportHeader.ProcessedDate,
  ///   InterfaceExportHeader.InsertDate,
  ///   InterfaceExportHeader.WebServiceMsg 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportHeader_Insert
(
 @InterfaceExportHeaderId int = null output,
 @IssueId int = null,
 @PrimaryKey nvarchar(60) = null,
 @OrderNumber nvarchar(60) = null,
 @RecordType nvarchar(60) = null,
 @RecordStatus char(1) = null,
 @CompanyCode nvarchar(60) = null,
 @Company nvarchar(510) = null,
 @Address nvarchar(510) = null,
 @FromWarehouseCode nvarchar(20) = null,
 @ToWarehouseCode nvarchar(20) = null,
 @Route nvarchar(100) = null,
 @DeliveryNoteNumber nvarchar(60) = null,
 @ContainerNumber nvarchar(60) = null,
 @SealNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @Remarks nvarchar(510) = null,
 @NumberOfLines int = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null,
 @WebServiceMsg nvarchar(max) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceExportHeaderId = '-1'
    set @InterfaceExportHeaderId = null;
  
	 declare @Error int
 
  insert InterfaceExportHeader
        (IssueId,
         PrimaryKey,
         OrderNumber,
         RecordType,
         RecordStatus,
         CompanyCode,
         Company,
         Address,
         FromWarehouseCode,
         ToWarehouseCode,
         Route,
         DeliveryNoteNumber,
         ContainerNumber,
         SealNumber,
         DeliveryDate,
         Remarks,
         NumberOfLines,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10,
         ProcessedDate,
         InsertDate,
         WebServiceMsg)
  select @IssueId,
         @PrimaryKey,
         @OrderNumber,
         @RecordType,
         @RecordStatus,
         @CompanyCode,
         @Company,
         @Address,
         @FromWarehouseCode,
         @ToWarehouseCode,
         @Route,
         @DeliveryNoteNumber,
         @ContainerNumber,
         @SealNumber,
         @DeliveryDate,
         @Remarks,
         @NumberOfLines,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9,
         @Additional10,
         @ProcessedDate,
         isnull(@InsertDate, getdate()),
         @WebServiceMsg 
  
  select @Error = @@Error, @InterfaceExportHeaderId = scope_identity()
  
  
  return @Error
  
end
