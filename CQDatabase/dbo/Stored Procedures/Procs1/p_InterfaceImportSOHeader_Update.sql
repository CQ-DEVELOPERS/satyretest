﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOHeader_Update
  ///   Filename       : p_InterfaceImportSOHeader_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:16
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportSOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSOHeaderId int = null,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @RecordType nvarchar(60) = null,
  ///   @RecordStatus char(1) = null,
  ///   @CustomerCode nvarchar(60) = null,
  ///   @Customer nvarchar(510) = null,
  ///   @Address nvarchar(510) = null,
  ///   @FromWarehouseCode nvarchar(20) = null,
  ///   @ToWarehouseCode nvarchar(20) = null,
  ///   @Route nvarchar(100) = null,
  ///   @DeliveryDate datetime = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @NumberOfLines int = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOHeader_Update
(
 @InterfaceImportSOHeaderId int = null,
 @PrimaryKey nvarchar(60) = null,
 @OrderNumber nvarchar(60) = null,
 @RecordType nvarchar(60) = null,
 @RecordStatus char(1) = null,
 @CustomerCode nvarchar(60) = null,
 @Customer nvarchar(510) = null,
 @Address nvarchar(510) = null,
 @FromWarehouseCode nvarchar(20) = null,
 @ToWarehouseCode nvarchar(20) = null,
 @Route nvarchar(100) = null,
 @DeliveryDate datetime = null,
 @Remarks nvarchar(510) = null,
 @NumberOfLines int = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceImportSOHeaderId = '-1'
    set @InterfaceImportSOHeaderId = null;
  
	 declare @Error int
 
  update InterfaceImportSOHeader
     set PrimaryKey = isnull(@PrimaryKey, PrimaryKey),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         RecordType = isnull(@RecordType, RecordType),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         CustomerCode = isnull(@CustomerCode, CustomerCode),
         Customer = isnull(@Customer, Customer),
         Address = isnull(@Address, Address),
         FromWarehouseCode = isnull(@FromWarehouseCode, FromWarehouseCode),
         ToWarehouseCode = isnull(@ToWarehouseCode, ToWarehouseCode),
         Route = isnull(@Route, Route),
         DeliveryDate = isnull(@DeliveryDate, DeliveryDate),
         Remarks = isnull(@Remarks, Remarks),
         NumberOfLines = isnull(@NumberOfLines, NumberOfLines),
         Additional1 = isnull(@Additional1, Additional1),
         Additional2 = isnull(@Additional2, Additional2),
         Additional3 = isnull(@Additional3, Additional3),
         Additional4 = isnull(@Additional4, Additional4),
         Additional5 = isnull(@Additional5, Additional5),
         Additional6 = isnull(@Additional6, Additional6),
         Additional7 = isnull(@Additional7, Additional7),
         Additional8 = isnull(@Additional8, Additional8),
         Additional9 = isnull(@Additional9, Additional9),
         Additional10 = isnull(@Additional10, Additional10),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         InsertDate = isnull(@InsertDate, InsertDate) 
   where InterfaceImportSOHeaderId = @InterfaceImportSOHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
