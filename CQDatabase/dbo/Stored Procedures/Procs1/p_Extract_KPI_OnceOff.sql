﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Extract_KPI_OnceOff
  ///   Filename       : p_Extract_KPI_OnceOff.sql
  ///   Create By      : Karen
  ///   Date Created   : Jul 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Extract_KPI_OnceOff
(
	@FromDate			datetime,
	@ToDate				datetime
)



as
begin
	 set nocount on;
	 
		 
Declare @Error				int,
        @Errormsg			varchar(500),
        @GetDate			datetime,
        @CheckedFrom		datetime,
        @CheckedTo			datetime
  
  
  declare @TableDetail as Table
  (
    StorageUnitId		int NULL,
	PackType			nvarchar(30) NULL,
	EndDate				datetime NULL,
	Quantity			int NULL
  );
  


  	insert	ProductKPI
			(StorageUnitId,
			PackType,	
			Quantity,
			EndDate,
			WarehouseId)
	select	vs.StorageUnitId,
			isnull(PackTypeCode,packtype),
			Quantity,
			@FromDate,
			WarehouseId
	from viewProduct vs
    join Pack      pk (nolock) on vs.StorageUnitId = pk.StorageUnitId
    join PackType  pt (nolock) on pk.PackTypeId = pt.PackTypeId
    where packtype != ' '
  
  
  	update pk
     set Received =	(select sum(rl.AcceptedQuantity) as Received
						from ReceiptLine            rl	(nolock)
						join Receipt			     r (nolock) on r.ReceiptId = rl.ReceiptId
						join StorageUnitBatch	   sub (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
						--where r.DeliveryDate between '2013-07-19 00:01' and '2013-07-19 23:59'
						where r.DeliveryDate between @FromDate and @ToDate
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = r.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     set Samples =	(select sum(rl.SampleQuantity) as Samples
						from ReceiptLine            rl	(nolock)
						join Receipt r (nolock) on r.ReceiptId = rl.ReceiptId
						join StorageUnitBatch sub (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
						where r.DeliveryDate between @FromDate and @ToDate
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = r.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     set Damages =	(select sum(rl.RejectQuantity) as Damages
						from ReceiptLine            rl	(nolock)
						join Receipt r (nolock) on r.ReceiptId = rl.ReceiptId
						join StorageUnitBatch sub (nolock) on sub.StorageUnitBatchId = rl.StorageUnitBatchId
						where r.DeliveryDate between @FromDate and @ToDate
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = r.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
	update pk
     --set PutAway =	(select count(distinct(j.JobId)) as PutAway
     set PutAway =	(select sum(i.ConfirmedQuantity) as PutAway
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join Job j						(nolock) on i.JobId = j.JobId
						join InstructionType it			(nolock) on it.InstructionTypeId = i.InstructionTypeId
						where i.EndDate between @FromDate and @ToDate
						and it.InstructionTypeCode in ('S','SM')
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk  
    where pk.EndDate between @FromDate and @ToDate
           
 
	update pk
     --set Movements =	(select count(distinct(j.JobId)) as Movements
     set Movements =	(select sum(i.ConfirmedQuantity) as Movements
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join Job j						(nolock) on i.JobId = j.JobId
						join InstructionType it			(nolock) on it.InstructionTypeId = i.InstructionTypeId
						where i.EndDate between @FromDate and @ToDate
						and it.InstructionTypeCode = 'M'
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     --set Replenishments =	(select count(distinct(j.JobId)) as Replenishments
     set Replenishments =	(select sum(i.ConfirmedQuantity) as Replenishments
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join Job j						(nolock) on i.JobId = j.JobId
						join InstructionType it			(nolock) on it.InstructionTypeId = i.InstructionTypeId
						where i.EndDate between @FromDate and @ToDate
						and it.InstructionTypeCode = 'R'
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     --set Picking =		(select count(distinct(j.JobId)) as Picking
     set Picking =		(select sum(i.ConfirmedQuantity) as Picking
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join Job j						(nolock) on i.JobId = j.JobId
						join InstructionType it			(nolock) on it.InstructionTypeId = i.InstructionTypeId
						where i.EndDate between @FromDate and @ToDate
						and it.InstructionTypeCode in ('P', 'PM', 'FM', 'PS')
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     --set Checking =		(select count(distinct(j.JobId)) as Checking
     set Checking =		(select sum(i.ConfirmedQuantity) as Checking
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join JobHistory j				(nolock) on i.JobId = j.JobId
						join Status s					(nolock) on s.StatusId = j.StatusId
						where j.InsertDate between @FromDate and @ToDate
						and s.StatusCode = 'CK'
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     --set MoveToDespatch =		(select count(distinct(j.JobId)) as MoveToDespatch
     set MoveToDespatch =		(select sum(i.ConfirmedQuantity) as MoveToDespatch
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join JobHistory j				(nolock) on i.JobId = j.JobId
						join Status s					(nolock) on s.StatusId = j.StatusId
						where j.InsertDate between @FromDate and @ToDate
						and s.status = 'Despatch'
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
    update pk
     --set Despatch =		(select count(distinct(j.JobId)) as Despatch
     set Despatch =		(select sum(i.ConfirmedQuantity) as Despatch
						from Instruction            i	(nolock)
						left join StorageUnitBatch sub	(nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
						join JobHistory j				(nolock) on i.JobId = j.JobId
						join Status s					(nolock) on s.StatusId = j.StatusId
						where j.InsertDate between @FromDate and @ToDate
						and s.status = 'Despatch Checked'
						and sub.StorageUnitId = pk.StorageUnitId
						and pk.WarehouseId = i.WarehouseId
						and pk.Quantity = 1)
    from ProductKPI pk
    where pk.EndDate between @FromDate and @ToDate
    
      
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
 
  
    update pk1
    set pk1.Received =	pk2.Received / pk1.Quantity
    from ProductKPI pk1 (nolock)
    join ProductKPI pk2 (nolock) on pk1.StorageUnitId = pk2.StorageUnitId 
						and pk1.PackType != pk2.PackType
						and pk1.EndDate = pk2.EndDate
						and pk1.WarehouseId = pk2.WarehouseId
						and pk2.Quantity = 1
						and pk1.Quantity != 1
	where pk1.EndDate between @FromDate and @ToDate
	
  delete from ProductKPI 
  where Checking is null
  and Damages is null
  and Despatch is null
  and MoveToDespatch is null
  and Movements is null
  and Picking is null
  and PutAway is null
  and Received is null
  and Replenishments is null
  and Samples is null

  
 
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Extract_KPI_OnceOff'); 
    rollback transaction
    return @Error
end
   		
