﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AddJobsToReference_Fix2
  ///   Filename       : p_AddJobsToReference_Fix2.sql
  ///   Create By      : Karen
  ///   Date Created   : April 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AddJobsToReference_Fix2

as
begin
	 set nocount on;
  
  Declare @ReferenceNumber int

 set @ReferenceNumber = 9

	
Declare @JobsResult as  Table 
		(JobId int)


Insert into @JobsResult 
			(JobId)
select distinct j.JobId from [Plascon].[dbo].[Job] j
join [Plascon].[dbo].[Instruction] i on i.JobId = j.JobId
join [Plascon].[dbo].[InstructionType] it on it.InstructionTypeId = i.InstructionTypeId
where it.InstructionTypeCode in ('STL', 'STE', 'STA', 'STP')
and CreateDate between '2012-04-21 00:00' and '2012-04-23 01:00'

Update StockTakeReferenceJob 
Set    StockTakeReferenceId = 9
From   @JobsResult jr
Join   StockTakeReferenceJob strj on jr.jobid = strj.JobId
where  jr.jobid = strj.JobId
    
end
