﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Address_Insert
  ///   Filename       : p_Address_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:18
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Address table.
  /// </remarks>
  /// <param>
  ///   @AddressId int = null output,
  ///   @ExternalCompanyId int = null,
  ///   @Street nvarchar(200) = null,
  ///   @Suburb nvarchar(200) = null,
  ///   @Town nvarchar(200) = null,
  ///   @Country nvarchar(200) = null,
  ///   @Code nvarchar(40) = null 
  /// </param>
  /// <returns>
  ///   Address.AddressId,
  ///   Address.ExternalCompanyId,
  ///   Address.Street,
  ///   Address.Suburb,
  ///   Address.Town,
  ///   Address.Country,
  ///   Address.Code 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Address_Insert
(
 @AddressId int = null output,
 @ExternalCompanyId int = null,
 @Street nvarchar(200) = null,
 @Suburb nvarchar(200) = null,
 @Town nvarchar(200) = null,
 @Country nvarchar(200) = null,
 @Code nvarchar(40) = null 
)

as
begin
	 set nocount on;
  
  if @AddressId = '-1'
    set @AddressId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
	 declare @Error int
 
  insert Address
        (ExternalCompanyId,
         Street,
         Suburb,
         Town,
         Country,
         Code)
  select @ExternalCompanyId,
         @Street,
         @Suburb,
         @Town,
         @Country,
         @Code 
  
  select @Error = @@Error, @AddressId = scope_identity()
  
  
  return @Error
  
end
