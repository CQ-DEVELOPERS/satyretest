﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMInstructionLine_Update
  ///   Filename       : p_BOMInstructionLine_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:08
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the BOMInstructionLine table.
  /// </remarks>
  /// <param>
  ///   @BOMInstructionId int = null,
  ///   @BOMLineId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMInstructionLine_Update
(
 @BOMInstructionId int = null,
 @BOMLineId int = null,
 @LineNumber int = null,
 @Quantity float = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update BOMInstructionLine
     set BOMInstructionId = isnull(@BOMInstructionId, BOMInstructionId),
         BOMLineId = isnull(@BOMLineId, BOMLineId),
         LineNumber = isnull(@LineNumber, LineNumber),
         Quantity = isnull(@Quantity, Quantity) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
