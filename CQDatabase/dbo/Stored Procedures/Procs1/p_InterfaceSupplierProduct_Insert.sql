﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceSupplierProduct_Insert
  ///   Filename       : p_InterfaceSupplierProduct_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:47
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceSupplierProduct table.
  /// </remarks>
  /// <param>
  ///   @SupplierCode nvarchar(60) = null,
  ///   @ProductCode nvarchar(100) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null 
  /// </param>
  /// <returns>
  ///   InterfaceSupplierProduct.SupplierCode,
  ///   InterfaceSupplierProduct.ProductCode,
  ///   InterfaceSupplierProduct.ProcessedDate,
  ///   InterfaceSupplierProduct.RecordStatus 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceSupplierProduct_Insert
(
 @SupplierCode nvarchar(60) = null,
 @ProductCode nvarchar(100) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceSupplierProduct
        (SupplierCode,
         ProductCode,
         ProcessedDate,
         RecordStatus)
  select @SupplierCode,
         @ProductCode,
         @ProcessedDate,
         @RecordStatus 
  
  select @Error = @@Error
  
  
  return @Error
  
end
