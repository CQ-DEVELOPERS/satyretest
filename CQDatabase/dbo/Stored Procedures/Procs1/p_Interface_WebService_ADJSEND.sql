﻿
/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_ADJSEND
  ///   Filename       : p_Interface_WebService_ADJSEND.sql
  ///   Create By      : Johan J v Rensburg
  ///   Date Created   : 12 Jun 2013
  /// </summary>
*/

CREATE PROCEDURE [p_Interface_WebService_ADJSEND]
(
	@XMLBody NVARCHAR(MAX) OUTPUT
,@PrincipalCode nvarchar(30) = null
)
--WITH ENCRYPTION
AS
BEGIN

    DECLARE @GETDATE DATETIME
    
    SELECT @GETDATE = dbo.ufn_GetDate()
    
    UPDATE InterfaceExportStockAdjustment
    SET ProcessedDate = @GETDATE
    WHERE RecordStatus = 'N'
    AND ProcessedDate IS NULL
	   AND (PrincipalCode = @PrincipalCode OR @PrincipalCode IS NULL)
	

	SELECT @XMLBody = '<?xml version="1.0" encoding="utf-16"?>' +
        (SELECT 
            (SELECT 
                 'Cquential'                AS 'Source'
                ,''                         AS 'Target'
                ,GETDATE()                  AS 'CreateDate'
                ,'StockAdjustment'          AS 'FileType'        
               FOR XML PATH('Header'), TYPE)
           ,(SELECT ''
           ,(SELECT H1.RecordType									AS	'ItemType'
				               ,'N'											       AS	'ItemStatus'
           ,(SELECT 'InterfaceExportStockAdjustment'			AS	'InterfaceTable'
                   ,H.InterfaceExportStockAdjustmentId	AS	'InterfaceTableId'
				               ,H.Additional1								AS	'FromWarehouseCode'
				               ,H.Additional2								AS	'ToWarehouseCode'
				               ,H.ProductCode								AS	'ProductCode'
				               ,H.Batch										    AS	'Batch'
				               ,''											        AS	'SerialNumber'
				               ,H.SKUCode									   AS	'SKUCode'
				               ,convert(nvarchar(50), H.Quantity)	AS	'Quantity'
				               ,h.Additional4 as 'Comments'
				               ,H.Additional3								AS	'ReasonCode'
				               ,ISNULL(H.InsertDate, dbo.ufn_Getdate())	AS	'TransactionDate'
					 FROM InterfaceExportStockAdjustment H
					WHERE H.RecordType = H1.RecordType
					  AND H.ProcessedDate = @GETDATE
					  AND H.RecordStatus = 'N'
					  AND (H.PrincipalCode = @PrincipalCode OR @PrincipalCode IS NULL)
					ORDER BY H.Additional1, H.ProductCode
        FOR XML PATH('ItemLine'), TYPE)
        FROM InterfaceExportStockAdjustment H1
					  WHERE H1.RecordType in ('SOH','ADJ','XFR','WHO')
					    AND H1.ProcessedDate = @GETDATE
					    AND H1.RecordStatus = 'N'
					    AND (H1.PrincipalCode = @PrincipalCode OR @PrincipalCode IS NULL)
					 GROUP BY H1.RecordType
					  FOR XML PATH('Item'), TYPE)
					  FOR XML PATH('Body'), TYPE)
        FOR XML PATH('root'))
		  
    UPDATE InterfaceExportStockAdjustment
    SET RecordStatus = 'Y'
    WHERE RecordStatus = 'N'
     	AND RecordType in ('SOH','ADJ','XFR','WHO')
      AND ProcessedDate = @GETDATE
	     AND (PrincipalCode = @PrincipalCode OR @PrincipalCode IS NULL)
    
END
 
 
