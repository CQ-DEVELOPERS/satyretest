﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Comment_Select
  ///   Filename       : p_Comment_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2014 08:27:24
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Comment table.
  /// </remarks>
  /// <param>
  ///   @CommentId int = null 
  /// </param>
  /// <returns>
  ///   Comment.CommentId,
  ///   Comment.Comment,
  ///   Comment.OperatorId,
  ///   Comment.CreateDate,
  ///   Comment.ModifiedDate,
  ///   Comment.ReadDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Comment_Select
(
 @CommentId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Comment.CommentId
        ,Comment.Comment
        ,Comment.OperatorId
        ,Comment.CreateDate
        ,Comment.ModifiedDate
        ,Comment.ReadDate
    from Comment
   where isnull(Comment.CommentId,'0')  = isnull(@CommentId, isnull(Comment.CommentId,'0'))
  order by Comment
  
end
