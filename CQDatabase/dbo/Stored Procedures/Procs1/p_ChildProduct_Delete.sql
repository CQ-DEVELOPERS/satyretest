﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ChildProduct_Delete
  ///   Filename       : p_ChildProduct_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:20
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ChildProduct table.
  /// </remarks>
  /// <param>
  ///   @ChildProductId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ChildProduct_Delete
(
 @ChildProductId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ChildProduct
     where ChildProductId = @ChildProductId
  
  select @Error = @@Error
  
  
  return @Error
  
end
