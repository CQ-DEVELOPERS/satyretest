﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionTypeHistory_Insert
  ///   Filename       : p_InstructionTypeHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:28
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InstructionTypeHistory table.
  /// </remarks>
  /// <param>
  ///   @InstructionTypeId int = null,
  ///   @PriorityId int = null,
  ///   @InstructionType nvarchar(60) = null,
  ///   @InstructionTypeCode nvarchar(20) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InstructionTypeHistory.InstructionTypeId,
  ///   InstructionTypeHistory.PriorityId,
  ///   InstructionTypeHistory.InstructionType,
  ///   InstructionTypeHistory.InstructionTypeCode,
  ///   InstructionTypeHistory.CommandType,
  ///   InstructionTypeHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionTypeHistory_Insert
(
 @InstructionTypeId int = null,
 @PriorityId int = null,
 @InstructionType nvarchar(60) = null,
 @InstructionTypeCode nvarchar(20) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InstructionTypeHistory
        (InstructionTypeId,
         PriorityId,
         InstructionType,
         InstructionTypeCode,
         CommandType,
         InsertDate)
  select @InstructionTypeId,
         @PriorityId,
         @InstructionType,
         @InstructionTypeCode,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
