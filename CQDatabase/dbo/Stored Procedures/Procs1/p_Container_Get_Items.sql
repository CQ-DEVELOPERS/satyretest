﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Get_Items
  ///   Filename       : p_Container_Get_Items.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Get_Items
(
 @warehouseId     int,
 @operatorId      int,
 @barcode         nvarchar(50),
 @productBarcode  nvarchar(50) = null,
 @batch           nvarchar(50) = null,
 @locationBarcode nvarchar(50) = null
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  Id                 int identity,
	  ReferenceNumber    nvarchar(30),
   StorageUnitBatchId int,
   PickLocationId     int,
   StoreLocationId    int,
   Quantity           numeric(13,6)
	 )
	 
	 declare @TableSOH as table
	 (
	  ContainerHeaderId  int,
	  ContainerDetailId  int,
   StorageUnitBatchId int,
   StorageUnitId      int,
   ProductCode        nvarchar(50),
   Product            nvarchar(255),
   SKUCode            nvarchar(10),
   Batch              nvarchar(50),
   Quantity           numeric(13,6),
   Location           nvarchar(255)
	 )
  
  declare @Error              int,
          @Errormsg           varchar(500),
          @GetDate            datetime,
          @ContainerHeaderId  int,
          @JobId              int,
          @PalletId           int,
          @ReferenceNumber    nvarchar(30),
          @StorageUnitBatchId int,
          @PickLocationId     int,
          @StoreLocationId    int,
          @StorageUnitId      int,
          @Quantity           numeric(13,6),
          @Id                 int,
          @Location           varchar(15),
          @rowcount           int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if isnumeric(replace(@barcode,'J:','')) = 1
    select @JobId           = replace(@barcode,'J:','')
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId        = replace(@barcode,'P:','')
  
  if isnumeric(replace(@barcode,'R:','')) = 1
    select @ReferenceNumber = @barcode
  
  if left(@barcode, 2) = 'L:'
    select @Location = replace(@barcode,'L:','')
  
  if @locationBarcode is not null
    select @pickLocationId = l.LocationId
      from Area          a (nolock)
      join AreaLocation al (nolock) on a.AreaId      = al.AreaId
      join Location      l (nolock) on al.LocationId = l.LocationId
     where a.WarehouseId = @warehouseId
       and l.Location    = replace(@locationBarcode,'L:','')
  
  if @JobId is not null
  begin
    select @ContainerHeaderId = ContainerHeaderId
      from ContainerHeader (nolock)
     where JobId = @JobId
    
    if @ContainerHeaderId is null
    begin
      insert @TableResult
            (ReferenceNumber,
             StorageUnitBatchId,
             PickLocationId,
             StoreLocationId,
             Quantity)
      select j.ReferenceNumber,
             i.StorageUnitBatchId,
             i.PickLocationId,
             i.StoreLocationId,
             sum(i.Quantity)
        from Instruction i (nolock)
        join Job         j (nolock) on i.JobId = j.JobId
       where j.JobId = @JobId
      group by j.ReferenceNumber,
               i.StorageUnitBatchId,
               i.PickLocationId,
               i.StoreLocationId
    end
  end
  
  if @ReferenceNumber is not null
  begin
    select @JobId = JobId
      from Job (nolock)
     where ReferenceNumber = @ReferenceNumber
    
    select @ContainerHeaderId = ContainerHeaderId
      from ContainerHeader (nolock)
     where ReferenceNumber = @ReferenceNumber
    
    if @ContainerHeaderId is null
    begin
      insert @TableResult
            (ReferenceNumber,
             StorageUnitBatchId,
             PickLocationId,
             StoreLocationId,
             Quantity)
      select j.ReferenceNumber,
             i.StorageUnitBatchId,
             i.PickLocationId,
             i.StoreLocationId,
             sum(i.Quantity)
        from Instruction i (nolock)
        join Job         j (nolock) on i.JobId = j.JobId
       where j.ReferenceNumber = @ReferenceNumber
      group by j.ReferenceNumber,
               i.StorageUnitBatchId,
               i.PickLocationId,
               i.StoreLocationId
      select @rowcount = @@rowcount
    end
    
    if @JobId is null and isnull(@rowcount,0) = 0
    begin
      insert @TableResult
            (ReferenceNumber)
      select @ReferenceNumber
      
      if @productBarcode is not null
        select @StorageUnitId = su.StorageUnitId
          from StorageUnit su (nolock)
          join Product      p (nolock) on su.ProductId = p.ProductId
                                      and (p.ProductCode = @productBarcode
                                       or  p.Barcode     = @productBarcode)
          left
          join Pack        pk (nolock) on su.StorageUnitId = pk.StorageUnitId
                                      and pk.WarehouseId = @WarehouseId
                                      and pk.Barcode    = @productBarcode
      
      if (@StorageUnitId is not null
      or @batch is not null)
      and @pickLocationId is null -- If Unpack
      begin
        insert @TableSOH
              (ContainerHeaderId,
               StorageUnitBatchId,
               StorageUnitId,
               ProductCode,
               Product,
               SKUCode,
               Batch,
               Quantity,
               Location)
        select distinct @ContainerHeaderId,
               cd.StorageUnitBatchId,
               su.StorageUnitId,
               p.ProductCode,
               p.Product,
               sku.SKUCode,
               b.Batch,
               cd.Quantity,
               l.Location
          from ContainerHeader   ch (nolock)
          join ContainerDetail   cd (nolock) on ch.ContainerHeaderId = cd.ContainerHeaderId
          join StorageUnitBatch sub (nolock) on cd.StorageUnitBatchId = sub.StorageUnitBatchId
          join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
          join Product            p (nolock) on su.ProductId = p.ProductId
          join SKU              sku (nolock) on su.SKUId = sku.SKUId
          join Batch              b (nolock) on sub.BatchId = b.BatchId
          join Location           l (nolock) on ch.PickLocationId  = l.LocationId
         where ch.ContainerHeaderId = @ContainerHeaderId
           and su.StorageUnitId     = isnull(@StorageUnitId, su.StorageUnitId)
           and b.Batch              = isnull(@batch, b.Batch)
        select @rowcount = @@rowcount
      end
      else
      if @StorageUnitId is not null
      or @batch is not null
      or @pickLocationId is not null -- If Pack
      begin
        insert @TableSOH
              (ContainerHeaderId,
               StorageUnitBatchId,
               StorageUnitId,
               ProductCode,
               Product,
               SKUCode,
               Batch,
               Quantity,
               Location)
        select distinct @ContainerHeaderId,
               subl.StorageUnitBatchId,
               su.StorageUnitId,
               p.ProductCode,
               p.Product,
               sku.SKUCode,
               b.Batch,
               (subl.ActualQuantity - subl.ReservedQuantity) - isnull(cd.Quantity,0),
               l.Location
          from StorageUnitBatchLocation subl (nolock)
          join StorageUnitBatch sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
          join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
          join Product            p (nolock) on su.ProductId = p.ProductId
          join SKU              sku (nolock) on su.SKUId = sku.SKUId
          join Batch              b (nolock) on sub.BatchId = b.BatchId
          join Location           l (nolock) on subl.LocationId  = l.LocationId
          left
          join ContainerDetail   cd (nolock) on cd.ContainerHeaderId = @ContainerHeaderId
                                            and sub.StorageUnitBatchId = cd.StorageUnitBatchId
         where su.StorageUnitId = isnull(@StorageUnitId, su.StorageUnitId)
           and b.Batch           = isnull(@batch, b.Batch)
           and subl.LocationId   = isnull(@pickLocationId, subl.LocationId)
           and subl.ActualQuantity - subl.ReservedQuantity > 0
        select @rowcount = @@rowcount
      end
    end
  end
  
  if @PalletId is not null
  begin
    select @ContainerHeaderId = max(ContainerHeaderId)
      from ContainerHeader (nolock)
     where PalletId = @PalletId
       and StoreLocationId is null
    
    if @ContainerHeaderId is null
    begin
      if (dbo.ufn_Configuration(402, @WarehouseId) = 0)
      begin
        insert @TableResult
              (StorageUnitBatchId,
               StoreLocationId,
               Quantity)
        select StorageUnitBatchId,
               LocationId,
               Quantity
          from Pallet (nolock)
         where PalletId = @PalletId
      end
      else
      begin
        select @JobId = MAX(i.JobId)
          from Instruction i
         where i.PalletId = @PalletId
        
        insert @TableResult
              (StorageUnitBatchId,
               PickLocationId,
               Quantity)
        select StorageUnitBatchId,
               StoreLocationId,
               null
          from Instruction (nolock)
         where PalletId = @PalletId
           and JobId = @JobId
      end
    end
    else
    begin
      update ContainerHeader
         set PickLocationId = @PickLocationId
       where ContainerHeaderId = @ContainerHeaderId
      
      update cd
         set Quantity = subl.ActualQuantity
        from ContainerDetail           cd
        join StorageUnitBatchLocation subl (nolock) on cd.StorageUnitBatchId = subl.StorageUnitBatchId
        join AreaLocation               al (nolock) on al.LocationId = subl.LocationId
        join Area                        a (nolock) on a.AreaId = al.AreaId
       where a.WarehouseId = @warehouseId
         and subl.LocationId = @PickLocationId
         and cd.ContainerHeaderId = @ContainerHeaderId
         and cd.Quantity is null
      
      --insert ContainerDetail
      --      (ContainerHeaderId,
      --       StorageUnitBatchId,
      --       Quantity)
      --select @ContainerHeaderId,
      --       subl.StorageUnitBatchId,
      --       subl.ActualQuantity
      --  from Area                        a (nolock)
      --  join AreaLocation               al (nolock) on a.AreaId = al.AreaId
      --  join Location                    l (nolock) on l.LocationId = al.LocationId
      --  join StorageUnitBatchLocation subl (nolock) on l.LocationId = subl.LocationId
      --  left
      --  join ContainerDetail            cd (nolock) on subl.StorageUnitBatchId = cd.StorageUnitBatchId
      -- where a.WarehouseId          = @warehouseId
      --   and l.LocationId           = @PickLocationId
      --   and cd.ContainerDetailId  is null
    end
  end
  
  if @Location is not null
  begin
    select @ContainerHeaderId = max(ContainerHeaderId)
      from Area            a (nolock)
      join AreaLocation   al (nolock) on a.AreaId = al.AreaId
      join Location        l (nolock) on l.LocationId = al.LocationId
      join ContainerHeader h (nolock) on l.LocationId = h.PickLocationId
     where a.WarehouseId = @warehouseId
       and l.Location    = @Location
       and h.CreateDate > dateadd(mi, -5, getdate())
    
    if @ContainerHeaderId is null
    begin
      insert @TableResult
            (StorageUnitBatchId,
             PickLocationId,
             Quantity)
      select subl.StorageUnitBatchId,
             subl.LocationId,
             subl.ActualQuantity
        from Area                        a (nolock)
        join AreaLocation               al (nolock) on a.AreaId = al.AreaId
        join Location                    l (nolock) on l.LocationId = al.LocationId
        join StorageUnitBatchLocation subl (nolock) on l.LocationId = subl.LocationId
       where a.WarehouseId = @warehouseId
         and l.Location    = @Location
    end
  end
  
  begin transaction
  
  if @ContainerHeaderId is null
  while exists(select top 1 1 from @TableResult)
  begin
    select @Id                 = Id,
           @ReferenceNumber    = ReferenceNumber,
           @StorageUnitBatchId = StorageUnitBatchId,
           @PickLocationId     = PickLocationId,
           @StoreLocationId    = StoreLocationId,
           @Quantity           = Quantity
      from @TableResult
    
    delete @TableResult where Id = @Id
    
    if @ContainerHeaderId is null
    begin
      exec @Error = p_ContainerHeader_Insert
       @ContainerHeaderId  = @ContainerHeaderId output,
       @PalletId           = @PalletId,
       @JobId              = @JobId,
       @ReferenceNumber    = @ReferenceNumber,
       @PickLocationId     = @PickLocationId,
       @StoreLocationId    = @StoreLocationId,
       @CreatedBy          = @operatorId,
       @CreateDate         = @GetDate
      
      if @Error <> 0 or @ContainerHeaderId is null
        goto error
    end
    
    if @StorageUnitBatchId is not null
    begin
      exec @Error = p_ContainerDetail_Insert
       @ContainerHeaderId  = @ContainerHeaderId,
       @StorageUnitBatchId = @StorageUnitBatchId,
       @Quantity           = @Quantity
      
      if @Error <> 0
        goto error
    end
  end
  
  if (select count(1) from @TableSOH) = 0
  begin
    if @productBarcode is not null
      select @StorageUnitId = su.StorageUnitId
        from StorageUnit su (nolock)
        join Product      p (nolock) on su.ProductId = p.ProductId
                                    and (p.ProductCode = @productBarcode
                                     or  p.Barcode     = @productBarcode)
        left
        join Pack        pk (nolock) on su.StorageUnitId = pk.StorageUnitId
                                    and pk.WarehouseId = @WarehouseId
                                    and pk.Barcode    = @productBarcode
    declare @BatchId int
    --if @StorageUnitId is not null and @batch is not null
    --  select @BatchId = b.BatchId
    --    from StorageUnitBatch sub (nolock)
    --    join Batch              b (nolock) on sub.BatchId = b.BatchId
    --   where b.Batch             = isnull(@batch, b.Batch)
    --     and sub.StorageUnitId = @StorageUnitId
      
    insert @TableSOH
          (ContainerHeaderId,
           ContainerDetailId,
           StorageUnitBatchId,
           StorageUnitId,
           ProductCode,
           Product,
           SKUCode,
           Batch,
           Quantity,
           Location)
    select d.ContainerHeaderId,
           d.ContainerDetailId,
           sub.StorageUnitBatchId,
           su.StorageUnitId,
           p.ProductCode,
           p.Product,
           sku.SKUCode,
           b.Batch,
           d.Quantity,
           l.Location
      from ContainerHeader    h (nolock)
      join ContainerDetail    d (nolock) on h.ContainerHeaderId = d.ContainerHeaderId
      join StorageUnitBatch sub (nolock) on d.StorageUnitBatchId = sub.StorageUnitBatchId
      join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
      join Product            p (nolock) on su.ProductId = p.ProductId
      join SKU              sku (nolock) on su.SKUId = sku.SKUId
      left
      join Batch              b (nolock) on sub.BatchId = b.BatchId
      left
      join Location           l (nolock) on isnull(h.StoreLocationId, h.PickLocationId) = l.LocationId
     where h.ContainerHeaderId = @ContainerHeaderId
       and sub.StorageUnitId   = isnull(@StorageUnitId, sub.StorageUnitId)
       and sub.BatchId         = isnull(@BatchId, sub.BatchId)
    
    select @rowcount = @@rowcount
    
    if @rowcount = 0
      insert @TableSOH
            (ContainerHeaderId,
             Location)
      select ContainerHeaderId,
             PickLocationId
        from ContainerHeader
       where ContainerHeaderId = @ContainerHeaderId
  end
  
  --update soh
  --   set Location = (select top 1 pick.Location
  --                     from Instruction      i (nolock)
  --                     join InstructionType it (nolock) on i.InstructionTypeId= it.InstructionTypeId
  --                                                     and it.InstructionTypeCode in ('P','PM','PS','FM')
  --                     join Location      pick (nolock) on i.PickLocationId = pick.LocationId
  --                     join Location     store (nolock) on i.StoreLocationId = store.LocationId
  --                                                     and store.Location = @Location
  --                    where soh.StorageUnitBatchId = i.StorageUnitBatchId
  --                      and i.StatusId = dbo.ufn_StatusId('I','F')
  --                    order by i.InstructionId desc)
  --  from @TableSOH soh
  -- where soh.ProductCode = @productBarcode
  
  --update soh
  --   set Location = isnull(soh.Location,'') + ' (' + l.Location + ')'
  --  from @TableSOH           soh
  --  join StorageUnitLocation sul (nolock) on soh.StorageUnitId = sul.StorageUnitId
  --  join Location              l (nolock) on sul.LocationId =l.LocationId
  --  join AreaLocation         al (nolock) on l.LocationId = al.LocationId
  --  join Area                  a (nolock) on al.AreaId = a.AreaId
  -- where a.AreaCode in ('PK','SP')
  
  --select @rowcount = @@rowcount
    
  --if @rowcount = 0
    update soh
       set Location = isnull(soh.Location,'')
      from @TableSOH           soh
      join StorageUnitLocation sul (nolock) on soh.StorageUnitId = sul.StorageUnitId
      join Location              l (nolock) on sul.LocationId = l.LocationId
  
  select ContainerHeaderId,
         ContainerDetailId,
         StorageUnitBatchId,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Location
    from @TableSOH
   where isnull(Quantity,1) > 0
  order by ProductCode, Batch
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Container_Get_Items'); 
    rollback transaction
    return @Error
end
