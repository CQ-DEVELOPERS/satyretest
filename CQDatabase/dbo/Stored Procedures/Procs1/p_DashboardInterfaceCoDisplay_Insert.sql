﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DashboardInterfaceCoDisplay_Insert
  ///   Filename       : p_DashboardInterfaceCoDisplay_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:32
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the DashboardInterfaceCoDisplay table.
  /// </remarks>
  /// <param>
  ///   @CompanyId int = null,
  ///   @DisplayCode char(5) = null,
  ///   @DisplayDesc nvarchar(100) = null,
  ///   @Display char(1) = null,
  ///   @Counter numeric(18,0) = null,
  ///   @ReportName nvarchar(100) = null,
  ///   @ExtractDescription nvarchar(100) = null,
  ///   @Show char(1) = null 
  /// </param>
  /// <returns>
  ///   DashboardInterfaceCoDisplay.CompanyId,
  ///   DashboardInterfaceCoDisplay.DisplayCode,
  ///   DashboardInterfaceCoDisplay.DisplayDesc,
  ///   DashboardInterfaceCoDisplay.Display,
  ///   DashboardInterfaceCoDisplay.Counter,
  ///   DashboardInterfaceCoDisplay.ReportName,
  ///   DashboardInterfaceCoDisplay.ExtractDescription,
  ///   DashboardInterfaceCoDisplay.Show 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DashboardInterfaceCoDisplay_Insert
(
 @CompanyId int = null,
 @DisplayCode char(5) = null,
 @DisplayDesc nvarchar(100) = null,
 @Display char(1) = null,
 @Counter numeric(18,0) = null,
 @ReportName nvarchar(100) = null,
 @ExtractDescription nvarchar(100) = null,
 @Show char(1) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert DashboardInterfaceCoDisplay
        (CompanyId,
         DisplayCode,
         DisplayDesc,
         Display,
         Counter,
         ReportName,
         ExtractDescription,
         Show)
  select @CompanyId,
         @DisplayCode,
         @DisplayDesc,
         @Display,
         @Counter,
         @ReportName,
         @ExtractDescription,
         @Show 
  
  select @Error = @@Error
  
  
  return @Error
  
end
