﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOM_Product_Check_Finished
  ///   Filename       : p_BOM_Product_Check_Finished.sql
  ///   Created By     : Ruan Groenewald
  ///   Date Created   : 08 Oct 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOM_Status_Rollup
(
 @IssueId       Int
)


as
begin
    set nocount on;
  
  return
 --   declare @Error               int,
 --           @Errormsg            nvarchar(500),
 --           @StatusId            int,
 --           @GetDate             datetime,
 --           @PickLocationId      int,
 --           @StoreLocationId     int,
 --           @ParentIssueId       Int,
 --           @JobId               int,
 --           @WarehouseId         int
	 
 --   select @GetDate = dbo.ufn_Getdate()
	 
	--if not exists  (select TOP 1 1
	--                From Issue                 (nolock) i
	--                Inner Join BOMInstruction  (nolock) bi on i.OutboundDocumentId = bi.OutboundDocumentId
	--                Where i.IssueId = @IssueId)
 --   Return
		 
 -- --If (select i.StatusId
 -- --      From Issue i 
 -- --      Where i.IssueId = @IssueId) <> dbo.ufn_StatusId('IS','CD')      
 -- --      Return

 --   Declare @BOMLines As Table
	--     (BOMInstructionId   Int
	--     ,JobId              Int
	--     ,InstructionId      Int)
	 
 --   Declare @Instructions As Table
 --       (ReasonId               int
 --       ,InstructionTypeId      int
 --       ,StorageUnitBatchId     int
 --       ,WarehouseId            int
 --       ,StatusId               int
 --       ,JobId                  int
 --       ,OperatorId             int
 --       ,PickLocationId         int
 --       ,StoreLocationId        int
 --       ,ReceiptLineId          int
 --       ,IssueLineId            int
 --       ,InstructionRefId       int
 --       ,Quantity               int
 --       ,ConfirmedQuantity      int
 --       ,Weight                 numeric(13, 3)
 --       ,ConfirmedWeight        numeric(13, 3)
 --       ,PalletId               int
 --       ,CreateDate             datetime
 --       ,StartDate              datetime
 --       ,EndDate                datetime
 --       ,Picked                 bit
 --       ,Stored                 bit
 --       ,CheckQuantity          int
 --       ,CheckWeight            numeric(13, 3)
 --       ,OutboundShipmentId     int
 --       ,DropSequence           int
 --       ,PackagingWeight        float
 --       ,PackagingQuantity      int
 --       ,NettWeight             float
 --       ,PreviousQuantity       float) 
        
 --    Declare @Jobs As Table
 --        (OldJobId          int
 --        ,NewJobId          int
 --        ,ReferenceNumber   nvarchar(30)
 --        ,isKit             bit
 --        ,StatusId          int
 --        ,WarehouseId       int
 --        ,Weight            Numeric(13,3)
 --        ,CheckedBy         int
 --        ,CheckedDate       DateTime
 --        ,DropSequence      int
 --        ,Pallets           int)

 --   if @@TRANCOUNT > 0
 --   begin transaction Kit
    
 --   --Set BOM Order to complete
 --   select @StatusId = dbo.ufn_StatusId('IS','C')
    
 --   exec @Error = p_Issue_Update 
 --               @IssueId = @IssueId
 --              ,@StatusId = @StatusId
    
 --    Insert Into @BOMLines
	-- Select Distinct jbp.BOMInstructionId, jbp.JobId, Jbp.InstructionId	 
	-- from JobBOMParent Jbp
	-- Inner Join BOMInstruction bi on Jbp.BOMInstructionId = bi.BOMInstructionId
	-- Inner Join Issue i on bi.OutboundDocumentId = i.OutboundDocumentId
	-- Where i.IssueId = @IssueId
	 
	-- --Up adjust parent product
	-- Insert Into InterfaceExportStockAdjustment
 --          ([RecordType]
 --          ,[RecordStatus]
 --          ,[ProductCode]
 --          ,[SKUCode]
 --          ,[Batch]
 --          ,[Quantity]
 --          ,[Weight]
 --          ,[Additional1]
 --          ,[Additional2]
 --          ,[Additional3]
 --          ,[Additional4]
 --          ,[Additional5]
 --          ,[InsertDate])
	-- Select 
	--        'ADJ'
	--       ,'N'
	--       ,vs.ProductCode
	--       ,vs.SKUCode
	--       ,vs.Batch
	--       ,ISNULL(i.CheckQuantity,0) * -1
	--       ,i.CheckWeight
	--       ,'JHB' As Additional1
	--       ,NULL As Additional2
 --          ,NULL As Additional3
 --          ,NULL As Additional4
 --          ,NULL As Additional5
 --          ,GETDATE()
	-- from @BOMLines bl
	-- Inner Join Instruction i on bl.InstructionId = i.InstructionId
	-- Inner Join viewStock vs on i.StorageUnitBatchId = vs.StorageUnitBatchId
     
	-- select @PickLocationId = LocationId
 --    from viewLocation
 --    where AreaCode = 'KIT'
        
 --    select @StoreLocationId = Location
 --    from viewLocation
 --    where AreaCode = 'Despatch'
     
 --    select @ParentIssueId = i1.IssueId
 --          ,@WarehouseId   = i1.WarehouseId
 --    From Issue i1
 --    Inner Join BOMInstruction bi on i1.OutboundDocumentId = bi.ParentId
 --    Inner Join Issue i2 on bi.OutboundDocumentId = i2.OutboundDocumentId
 --    where i2.IssueId = @IssueId
     
 --    IF @ParentIssueId IS NULL
 --       Goto Error

 --    --Parent order should not have any instructions
 --    If Not Exists ( Select 1
 --                   From IssueLineInstruction ili
 --                    Inner Join IssueLine il on ili.IssueLineId = il.IssueLineId
 --                    Inner Join OutboundLine ol on il.OutboundLineId = ol.OutboundLineId
 --                    Inner Join BOMHeader bh on ol.StorageUnitId = bh.StorageUnitId
 --                    Inner Join BOMInstruction bi on bi.BOMHeaderId = bh.BOMHeaderId
 --                   Where ili.IssueId = @ParentIssueId ) 
 --    Begin
             
 --       --Insert BOM Lines
 --       Insert Into @Instructions
	--    select 
	--        NULL                                                As ReasonId
	--       ,BOM.InstructionTypeId
	--       ,il.StorageUnitBatchId
	--       ,BOM.WarehouseId
	--       ,dbo.ufn_StatusId('I','F')
	--       ,JobId                                               As JobId
	--       ,NULL                                                As OperatorId
	--       ,@PickLocationId
	--       ,@StoreLocationId
	--       ,NULL                                                As ReceiptLineId
	--       ,il.IssueLineId 
	--       ,NULL                                                As InstructionRefIf
	--       ,ISNULL(BOM.Quantity,0)
	--       ,ISNULL(BOM.ConfirmedQuantity,0)
	--       ,BOM.Weight                                          As Weight
 --          ,BOM.ConfirmedWeight                                 As ConfirmedWeight
 --          ,NULL                                                As PalletId
 --          ,BOM.CreateDate
 --          ,BOM.StartDate
 --          ,BOM.EndDate
 --          ,1                                                   As Picked
 --          ,1                                                   As Stored
 --          ,NULL                                                As CheckQuantity
 --          ,NULL                                                As CheckWeight
 --          ,NULL                                                As OutboundShipmentId
 --          ,1                                                   As DropSequence
 --          ,NULL                                                As PackagingWeight
 --          ,NULL                                                As PackagingQuantity
 --          ,NULL                                                As NettWeight
 --          ,NULL                                                As PreviousQuantity            
 --       from 
	--        (select 
	--            bi.ParentId                                         As OutboundDocumentId
	--           ,bh.StorageUnitId
	--           ,i.InstructionTypeId   
	--           ,i.WarehouseId
	--           ,i.JobId
	--           ,MIN(ROUND(jbp.Quantity / bp.Quantity,0,1))          As Quantity
	--           ,MIN(ROUND(jbp.CheckQuantity / bp.Quantity,0,1))     As ConfirmedQuantity
	--           ,SUM(i.Weight)                                       As Weight
	--           ,SUM(i.ConfirmedWeight)                              As ConfirmedWeight
 --              ,MIN(i.CreateDate)                                   As CreateDate
 --              ,MIN(i.StartDate)                                    As StartDate
 --              ,MAX(i.EndDate)                                      As EndDate
 --              --,MIN(ROUND(cp.CheckQuantity / bp.Quantity,0,1))      As CheckQuantity
 --           from BOMInstruction bi
 --           Inner Join BOMHeader bh on bi.BOMHeaderId = bh.BOMHeaderId
 --           Inner Join BOMInstructionLine bil on bi.BOMInstructionId = bil.BOMInstructionId
 --           Inner Join BOMProduct bp on bil.BOMLineId = bp.BOMLineId
 --                                   and bil.LineNumber = bp.LineNumber
 --           --Inner Join CheckingProduct cp on bp.StorageUnitId = cp.StorageUnitId
 --           Inner Join JobBOMParent jbp on bi.BOMInstructionId = jbp.BOMInstructionId
 --                                       and bp.StorageUnitId = jbp.StorageUnitId
 --           Inner Join Instruction i on jbp.InstructionId = i.InstructionId
 --           Inner Join @BOMLines bl on bi.BOMInstructionId = bl.BOMInstructionId
 --                                      and jbp.JobId = jbp.JobId
 --                                      and jbp.InstructionId = jbp.InstructionId
 --           Group By
 --               bi.ParentId
 --              ,bh.StorageUnitId
 --              ,i.InstructionTypeId
 --              ,i.WarehouseId
 --              ,i.JobId) As BOM
 --       Inner Join Issue i on BOM.OutboundDocumentId = i.OutboundDocumentId
 --       Inner Join IssueLine il on i.IssueId = il.IssueId
 --       Inner Join OutboundLine ol on ol.OutboundLineId = il.OutboundLineId
 --       Where ol.StorageUnitId = BOM.StorageUnitId
        
 --       --Set parent "BOM" products to checking
 --       select @StatusId = dbo.ufn_StatusId('IS','CK')
        
 --       Insert Into @Jobs
 --           (OldJobId
 --           ,ReferenceNumber
 --           ,StatusId
 --           ,Weight
 --           ,CheckedBy
 --           ,CheckedDate
 --           ,Pallets
 --           ,isKit)
 --       Select Distinct 
 --            j.JobId
 --           ,IsNull(j.ReferenceNumber, 'J:' + CONVERT(VarChar(30),j.JobId))
 --           ,@StatusId
 --           ,j.Weight
 --           ,j.CheckedBy
 --           ,j.CheckedDate
 --           ,j.Pallets
 --           ,1
 --       From @Instructions i
 --       Inner Join Job j on i.JobId = j.JobId
    
 --       --If no receipt create adjustment
 --       IF dbo.ufn_Configuration(355, @WarehouseId) = 0
 --           Insert Into InterfaceExportStockAdjustment
 --              ([RecordType]
 --              ,[RecordStatus]
 --              ,[ProductCode]
 --              ,[SKUCode]
 --              ,[Batch]
 --              ,[Quantity]
 --              ,[Weight]
 --              ,[Additional1]
 --              ,[Additional2]
 --              ,[Additional3]
 --              ,[Additional4]
 --              ,[Additional5]
 --              ,[InsertDate])
 --           Select 
 --               'ADJ'
 --              ,'N'
 --              ,vs.ProductCode
 --              ,vs.SKUCode
 --              ,vs.Batch
 --              ,i.ConfirmedQuantity
 --              ,i.ConfirmedWeight
 --              ,'JHB' as WarehouseCode
 --              ,NULL As Additional2
 --              ,NULL As Additional3
 --              ,NULL As Additional4
 --              ,NULL As Additional5
 --              ,GETDATE()
 --           from @Instructions i
 --           Inner Join viewStock vs on i.StorageUnitBatchId = vs.StorageUnitBatchId
 --           Where i.ConfirmedQuantity > 0
        
 --       --Insert Non BOM Lines
 --       Insert Into @Instructions
 --           (ReasonId
 --           ,InstructionTypeId
 --           ,StorageUnitBatchId
 --           ,WarehouseId
 --           ,StatusId
 --           ,JobId
 --           ,OperatorId
 --           ,PickLocationId
 --           ,StoreLocationId
 --           ,ReceiptLineId
 --           ,IssueLineId
 --           ,InstructionRefId
 --           ,Quantity
 --           ,ConfirmedQuantity
 --           ,Weight
 --           ,ConfirmedWeight
 --           ,PalletId
 --           ,CreateDate
 --           ,StartDate
 --           ,EndDate
 --           ,Picked
 --           ,Stored
 --           ,CheckQuantity
 --           ,CheckWeight
 --           ,OutboundShipmentId
 --           ,DropSequence
 --           ,PackagingWeight
 --           ,PackagingQuantity
 --           ,NettWeight
 --           ,PreviousQuantity)
 --       select 
 --            ReasonId
 --           ,InstructionTypeId
 --           ,StorageUnitBatchId
 --           ,WarehouseId
 --           ,dbo.ufn_StatusId('I', 'F')
 --           ,JobId
 --           ,OperatorId
 --           ,PickLocationId
 --           ,StoreLocationId
 --           ,ReceiptLineId
 --           ,NULL
 --           ,NULL
 --           ,Quantity
 --           ,ConfirmedQuantity
 --           ,Weight
 --           ,ConfirmedWeight
 --           ,PalletId
 --           ,CreateDate
 --           ,StartDate
 --           ,EndDate
 --           ,Picked
 --           ,Stored
 --           ,CheckQuantity
 --           ,CheckWeight
 --           ,OutboundShipmentId
 --           ,DropSequence
 --           ,PackagingWeight
 --           ,PackagingQuantity
 --           ,NettWeight
 --           ,PreviousQuantity
 --       from Instruction i
 --       Inner Join Status s on i.StatusId = s.StatusId
 --       where
 --        s.Type = 'I' 
 --        and s.StatusCode in ('F','NS')
 --        and not exists (Select InstructionId
 --                         From JobBOMParent (nolock) jbp
 --                         Where jbp.InstructionId = i.InstructionId)
 --        and exists     (Select 1 
 --                         From IssueLineInstruction ili
 --                         Where IssueId = @IssueId
 --                         and ili.InstructionId = ISNULL(i.InstructionRefId, i.InstructionId))
        
 --       --Remaining "non BOM" jobs remain as checked
 --       select @StatusId = dbo.ufn_StatusId('IS','CD')
        
 --       Insert Into @Jobs
 --           (OldJobId
 --           ,ReferenceNumber
 --           ,StatusId
 --           ,Weight
 --           ,CheckedBy
 --           ,CheckedDate
 --           ,Pallets
 --           ,isKit)
 --       Select Distinct 
 --            j.JobId
 --           ,IsNull(j.ReferenceNumber, 'J:' + CONVERT(VarChar(30),j.JobId))
 --           ,@StatusId
 --           ,j.Weight
 --           ,j.CheckedBy
 --           ,j.CheckedDate
 --           ,j.Pallets
 --           ,0
 --       From @Instructions i
 --       Inner Join Job j on i.JobId = j.JobId
 --       Where Not Exists (Select 1 from @Jobs Where OldJobId = j.JobId)

 --       update Job
 --       set ReferenceNumber = null
 --       where Exists
 --        (select 1
 --         from @Jobs
 --         Where JobId = OldJobId)
          
 --       Insert Into Job
 --          (PriorityId           
 --          ,StatusId
 --          ,WarehouseId
 --          ,ReferenceNumber
 --          ,Weight
 --          ,CheckedBy
 --          ,CheckedDate
 --          ,DropSequence
 --          ,Pallets)
 --       select 
 --           3                           As Priority
 --          ,StatusId                    As StatusId
 --          ,@WarehouseId                As WarehouseId
 --          ,ReferenceNumber
 --          ,Weight
 --          ,CheckedBy
 --          ,CheckedDate
 --          ,ROW_NUMBER() Over (Order By OldJobId) + 1
 --          ,Pallets
 --       from @Jobs
        
 --       update jobs
 --       Set NewJobId = JobId
 --       From @Jobs jobs
 --       Inner Join Job j on jobs.ReferenceNumber = j.ReferenceNumber
     
 --       Update i
 --       Set JobId = NewJobId
 --       From @Instructions i
 --       Inner Join @Jobs j on i.JobId = j.OldJobId
        
 --       Update i
 --        Set IssueLineId = ISNULL(i.IssueLineId, il.IssueLineId)
 --        From @Instructions i
 --         inner join IssueLine il on i.StorageUnitBatchId = il.StorageUnitBatchId
 --        where il.IssueId = @ParentIssueId
         
 --       Update il
 --        Set ConfirmedQuatity = i.CheckQuantity
 --        From @Instructions i
 --         inner join IssueLine il on i.StorageUnitBatchId = il.StorageUnitBatchId
 --        where il.IssueId = @ParentIssueId
    
 --       Insert Into Instruction
 --           (ReasonId
 --           ,InstructionTypeId
 --           ,StorageUnitBatchId
 --           ,WarehouseId
 --           ,StatusId
 --           ,JobId
 --           ,OperatorId
 --           ,PickLocationId
 --           ,StoreLocationId
 --           ,ReceiptLineId
 --           ,IssueLineId
 --           ,InstructionRefId
 --           ,Quantity
 --           ,ConfirmedQuantity
 --           ,Weight
 --           ,ConfirmedWeight
 --           ,PalletId
 --           ,CreateDate
 --           ,StartDate
 --           ,EndDate
 --           ,Picked
 --           ,Stored
 --           ,CheckQuantity
 --           ,CheckWeight
 --           ,OutboundShipmentId
 --           ,DropSequence
 --           ,PackagingWeight
 --           ,PackagingQuantity
 --           ,NettWeight
 --           ,PreviousQuantity)
 --       Select 
 --           ReasonId
 --           ,InstructionTypeId
 --           ,StorageUnitBatchId
 --           ,WarehouseId
 --           ,StatusId
 --           ,JobId
 --           ,OperatorId
 --           ,PickLocationId
 --           ,StoreLocationId
 --           ,ReceiptLineId
 --           ,IssueLineId
 --           ,InstructionRefId
 --           ,Quantity
 --           ,ConfirmedQuantity
 --           ,Weight
 --           ,ConfirmedWeight
 --           ,PalletId
 --           ,CreateDate
 --           ,StartDate
 --           ,EndDate
 --           ,Picked
 --           ,Stored
 --           ,CheckQuantity
 --           ,CheckWeight
 --           ,OutboundShipmentId
 --           ,DropSequence
 --           ,PackagingWeight
 --           ,PackagingQuantity
 --           ,NettWeight
 --           ,PreviousQuantity
 --       From @Instructions
        
 --       --Set instruction reference
 --       Update i
 --       Set InstructionRefId = i_sub.InstructionId
 --       From Instruction i
 --       Inner Join 
 --           (Select MIN(InstructionId) InstructionId, i.IssueLineId
 --            from Instruction i
 --            Inner Join IssueLine il on i.IssueLineId = il.issueLineId
 --            Where il.IssueId = @ParentIssueId
 --            Group By i.IssueLineId
 --            Having COUNT(*) > 1) i_sub on i.IssueLineId = i_sub.IssueLineId
 --       Where i.InstructionId <> i_sub.InstructionId
        
 --   End
 --   Else
 --   Begin
 --       --Get Job
 --       Select Top 1 @JobId = JobId 
 --       From IssueLineInstruction ili
 --        Inner Join Instruction i on ili.InstructionId = i.InstructionId
 --       Where IssueId = @ParentIssueId
        
 --       Select @StatusId = dbo.ufn_StatusId('IS','CK')
    
 --       exec @Error = p_Job_Update @JobId = @JobId, @StatusId = @StatusId
        
 --       Select @StatusId = dbo.ufn_StatusId('I','F')
        
 --       Update SoI
 --       Set StatusId = @StatusId
 --          ,ConfirmedQuantity = kitI.ConfirmedQuantity
 --          ,CheckQuantity = kitI.CheckQuantity
 --       From Instruction SoI
 --        inner join Instruction kitI on SoI.StorageUnitBatchId = kitI.StorageUnitBatchId
 --        Inner Join IssueLineInstruction ili on kitI.InstructionId = ili.InstructionId
 --       Where ili.IssueId = @IssueId
 --        And SoI.JobId = @JobId
 --        and not Exists (Select InstructionId
 --                         From JobBOMParent (nolock) jbp
 --                         Where jbp.InstructionId = kitI.InstructionId)
                          
 --       Update i
 --       Set ConfirmedQuantity = ConfirmedQuantity+kit.CheckQuatity
 --       From Instruction i
 --       inner join 
 --          (Select MIN(cp.CheckQuantity / bp.Quantity) CheckQuatity, jbp.BOMInstructionId, sub.StorageUnitBatchId
 --           From @BOMLines jbp
 --            Inner Join CheckingProduct cp on jbp.InstructionId = cp.InstructionId
 --            Inner Join BOMInstruction bi on jbp.BOMInstructionId = bi.BOMInstructionId
 --            Inner Join BOMHeader bh on bi.BOMHeaderId = bh.BOMHeaderId
 --            Inner Join BOMInstructionLine bil on jbp.BOMInstructionId = bil.BOMInstructionId
 --            Inner Join BOMProduct bp on bil.BOMLineId = bp.BOMLineId
 --                                    and cp.StorageUnitId = bp.StorageUnitId
 --            Inner Join StorageUnitBatch sub on bh.StorageUnitId = sub.StorageUnitId
 --           Where bp.LineNumber = 1
 --            and sub.BatchId = 1
 --           Group By jbp.BOMInstructionId, sub.StorageUnitBatchId) kit on i.StorageUnitBatchId = kit.StorageUnitBatchId
 --       where i.JobId = @JobId
        
 --   End
     
 --   exec @Error = p_Outbound_Palletise_Create_Reference 
 --                   @OutboundShipmentId = Null--@OutboundShipmentId
 --                  ,@IssueId = @ParentIssueId
     
 --   if @Error <> 0
 --       goto error
        
 --   --Set IssueLineInstruction quantity
 --   Update ili
 --   Set ConfirmedQuantity = i.ConfirmedQuantity
 --   From IssueLineInstruction ili
 --    Inner Join IssueLine il on ili.IssueLineId = il.IssueLineId
 --    Inner Join Instruction i on ili.InstructionId = i.InstructionId
 --   Where il.IssueId = @ParentIssueId

 --   --Change IssueLine status to Job Status    
 --   Update il
 --   Set StatusId = j.StatusId
 --   From IssueLine il
 --    Inner Join IssueLineInstruction ili on ili.IssueLineId = il.IssueLineId
 --    Inner Join Instruction i on ili.InstructionId = i.InstructionId
 --    Inner Join Job j on i.JobId = j.JobId
 --   Where il.IssueId = @ParentIssueId
 --     And il.StatusId <> j.StatusId
       
 --   --Set Parent order to checking
 --   select @StatusId = dbo.ufn_StatusId('IS','RL') 

 --   exec @Error = p_Issue_Update 
 --                  @IssueId = @ParentIssueId
 --                 ,@StatusId = @StatusId

 --   if @Error <> 0
 --       goto error
    
 --   Delete from JobBOMParent
 --   Where Exists (Select 1 From @BOMLines Where JobId = JobBOMParent.JobId)
    
 --   if @@TRANCOUNT > 0    
 --   commit transaction Kit
 --   Return
    
 -- error:
 --   if @@TRANCOUNT = 1
 --   rollback transaction Kit
    
 --   return @Error
  
end

