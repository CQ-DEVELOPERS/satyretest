﻿ 
   
/*  
  /// <summary>  
  ///   Procedure Name : p_Batch_Auto_Insert  
  ///   Filename       : p_Batch_Auto_Insert.sql  
  ///   Create By      : Grant Schultz  
  ///   Date Created   : 02 Jun 2011  
  /// </summary>  
  /// <remarks>  
  ///     
  /// </remarks>  
  /// <param>  
  ///     
  /// </param>  
  /// <returns>  
  ///     
  /// </returns>  
  /// <newpara>  
  ///   Modified by    :   
  ///   Modified Date  :   
  ///   Details        :   
  /// </newpara>  
*/  
CREATE procedure p_Batch_Auto_Insert  
(
 @Date  datetime = null
)
  
as  
begin  
  set nocount on;  
    
  declare @Error             int,  
          @Errormsg          varchar(500),  
          @GetDate           datetime  
    
  select @GetDate = dbo.ufn_Getdate()  
    
  Declare @Product as table  
  (  
   ProductId     int,  
   StorageUnitId int,  
   ShelfLifeDays int  
  )  
  
  Declare @Batch as table
  (
    Year char(4)
   ,Month varchar(2)
   ,Day char(2)
   ,Date DateTime
   ,Batch varchar(50)
  )
  
  insert @Product  
        (ProductId,  
         StorageUnitId,  
         ShelfLifeDays)  
  select ProductId,  
         StorageUnitId,  
         ShelfLifeDays  
    from viewProduct  
   where SKUCode != '0'
     and ProductCode in ('2084','2085','1779','1775')
  
  declare @ToDate  datetime,  
          @Year  char(4),  
          @Month varchar(2),  
          @Day   char(2)
  
  if @Date is null
    set @Date = getdate()
  
  set @ToDate = DATEADD(MM, 1, @Date)
  
  while @Date <= @ToDate 
  begin  
    select @Year = datepart(YYYY, @Date)  
      
    select @Month = datepart(M, @Date)  
      
    select @Month = case when @Month = '1' then 'A'  
                         when @Month = '2' then 'B'  
                         when @Month = '3' then 'C'  
                         when @Month = '4' then 'D'  
                         when @Month = '5' then 'E'  
                         when @Month = '6' then 'F'  
                         when @Month = '7' then 'AA'  
                         when @Month = '8' then 'BB'  
                         when @Month = '9' then 'CC'  
                         when @Month = '10' then 'DD'  
                         when @Month = '11' then 'EE'  
                         when @Month = '12' then 'FF'  
                         else 'GG' end  
    
    select @Day = case when datalength(convert(varchar(2), datepart(dd, @Date))) = 1  
                       then '0' + convert(varchar(1), datepart(dd, @Date))  
                       else convert(char(2), datepart(dd, @Date))  
                       end  
    
    insert into @Batch
		(Year, Month, Day, Date, Batch)
    Values
		(@Year, @Month, @Day, @Date, @Year + @Month + @Day)
      
    set @Date = dateadd(dd, 1, @Date) 
    
  end
  
  insert Batch  
          (StatusId,  
           WarehouseId,  
           Batch,  
           CreateDate,  
           ExpiryDate,  
           IncubationDays,  
           ShelfLifeDays,  
           ExpectedYield,  
           ActualYield,  
           ECLNumber,  
           BatchReferenceNumber,  
           ProductionDateTime,  
           FilledYield,  
           COACertificate,  
           StatusModifiedBy,  
           StatusModifiedDate,  
           CreatedBy,  
           ModifiedBy,  
           ModifiedDate,  
           Prints) 
    select dbo.ufn_StatusId('B','A'),  
           1,  
            b.Batch,  
           b.Date,  
           dateadd(dd, ShelfLifeDays, b.Date),  
           0,  
           ShelfLifeDays,  
           0,  
           0,  
           CONVERT(varchar(10), StorageUnitId),  
           null,  
           getdate(),  
           0,  
           0,  
           null,  
           getdate(),  
           null,  
           null,  
           getdate(),
           -1
      from @Product p  
      cross join @Batch b
     where not exists(select 1 from StorageUnitBatch sub inner join Batch subb on sub.BatchId = subb.BatchId
					  where p.StorageUnitId = sub.StorageUnitId and subb.Batch = b.Batch)
    select * from @Product
    insert StorageUnitBatch  
          (BatchId,  
           StorageUnitId)  
    select distinct BatchId,  
           convert(int, b.ECLNumber)  
      from Batch b  
     where Prints = -1  
       and not exists(select 1 fr  
                        from StorageUnitBatch sub  
                       where b.BatchId = sub.BatchId  
                         and convert(int, b.ECLNumber) = sub.StorageUnitId)   
      
    update Batch set Prints = 0 where Prints = -1    
  
  /*  
  delete from Batch where BatchId > 1  
  exec sp_grants_reseed 'Batch'  
  */  
  --select * from Batch where BatchId > 1  
  
  --insert StorageUnitBatch  
  --      (BatchId,  
  --       StorageUnitId)  
  --select distinct BatchId,  
  --       StorageUnitId  
  --  from Batch b,  
  --       StorageUnit su  
  -- where not exists(select 1 fr  
  --                    from StorageUnitBatch sub  
  --                   where b.BatchId = sub.BatchId  
  --                     and su.StorageUnitId = sub.StorageUnitId)  
    
  begin transaction  
    
  if @Error <> 0  
    goto error  
    
  commit transaction  
  return  
    
  error:  
    RAISERROR (900000,-1,-1, 'Error executing p_Batch_Auto_Insert'  ); 
    rollback transaction  
    return @Error  
end
 
 
 
 
