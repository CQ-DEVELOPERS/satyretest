﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Test_Select
  ///   Filename       : p_COA_Test_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Test_Select
(
 @COAId         int,
 @StorageUnitId int
)

as
begin
	 set nocount on;
  
  select coa.COAId,
         ct.COATestId,
         t.TestId,
         t.Test,
         r.ResultId,
         r.Result,
         m.MethodId,
         m.Method,
         ct.Pass,
         ct.StartRange,
         ct.EndRange,
         ct.Value
    from COA coa
    join COATest ct on coa.COAId = ct.COAId
    left
    join Test       t (nolock) on ct.TestId = t.TestId
    left
    join Result     r (nolock) on ct.ResultId = r.ResultId
    left
    join Method     m (nolock) on ct.MethodId = m.MethodId
   where coa.COAId = @COAId
end
