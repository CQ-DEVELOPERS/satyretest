﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Create_Job
  ///   Filename       : p_Housekeeping_Stock_Take_Create_Job.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Create_Job
(
 @warehouseId			int,
 @operatorId			int,
 @instructionTypeCode	nvarchar(10),
 @jobId					int output,
 @referenceNumber		nvarchar(30),
 @StockTakeReferenceId	int = null
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @PriorityId        int,
          @StatusId          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('J','W')
  
  select @PriorityId = PriorityId
    from InstructionType (nolock)
   where instructionTypeCode = @instructionTypeCode
  
  begin transaction
  
  exec @Error = p_Job_Insert
   @JobId           = @jobId output,
   @PriorityId      = @PriorityId,
   @OperatorId      = @operatorId,
   @StatusId        = @StatusId,
   @WarehouseId     = @warehouseId,
   @ReferenceNumber = @referenceNumber
  
  if @Error <> 0
    goto error
  
  if @referenceNumber = '-1'
  begin
    set @referenceNumber = 'Job ' + convert(nvarchar(10), @jobId)
    
    exec @Error = p_Job_Update
     @JobId           = @jobId,
     @ReferenceNumber = @referenceNumber
    
    if @Error <> 0
      goto error
  end
  
  if @StockTakeReferenceId is not null
    exec	@Error = p_StockTakeReferenceJob_Insert
	    @StockTakeReferenceId	= @StockTakeReferenceId,
	    @JobId					= @JobId
  
  if @Error <> 0
    goto error
		
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_Take_Create_Job'); 
    rollback transaction
    return @Error
end
