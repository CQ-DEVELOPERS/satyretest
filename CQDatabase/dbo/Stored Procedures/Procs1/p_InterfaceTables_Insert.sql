﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceTables_Insert
  ///   Filename       : p_InterfaceTables_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:49
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceTables table.
  /// </remarks>
  /// <param>
  ///   @InterfaceTableId int = null output,
  ///   @HeaderTable nvarchar(100) = null,
  ///   @DetailTable nvarchar(100) = null,
  ///   @HeaderTableKey nvarchar(100) = null,
  ///   @Description1 nvarchar(100) = null,
  ///   @Description2 nvarchar(100) = null,
  ///   @HeaderField1 nvarchar(100) = null,
  ///   @HeaderField2 nvarchar(100) = null,
  ///   @HeaderField3 nvarchar(100) = null,
  ///   @HeaderField4 nvarchar(100) = null,
  ///   @HeaderField5 nvarchar(100) = null,
  ///   @HeaderField6 nvarchar(100) = null,
  ///   @HeaderField7 nvarchar(100) = null,
  ///   @HeaderField8 nvarchar(100) = null,
  ///   @DetailField1 nvarchar(100) = null,
  ///   @DetailField2 nvarchar(100) = null,
  ///   @DetailField3 nvarchar(100) = null,
  ///   @DetailField4 nvarchar(100) = null,
  ///   @DetailField5 nvarchar(100) = null,
  ///   @DetailField6 nvarchar(100) = null,
  ///   @DetailField7 nvarchar(100) = null,
  ///   @DetailField8 nvarchar(100) = null,
  ///   @StartId int = null,
  ///   @InsertDetail int = null,
  ///   @GroupByKey nvarchar(100) = null,
  ///   @AltGroupByKey nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   InterfaceTables.InterfaceTableId,
  ///   InterfaceTables.HeaderTable,
  ///   InterfaceTables.DetailTable,
  ///   InterfaceTables.HeaderTableKey,
  ///   InterfaceTables.Description1,
  ///   InterfaceTables.Description2,
  ///   InterfaceTables.HeaderField1,
  ///   InterfaceTables.HeaderField2,
  ///   InterfaceTables.HeaderField3,
  ///   InterfaceTables.HeaderField4,
  ///   InterfaceTables.HeaderField5,
  ///   InterfaceTables.HeaderField6,
  ///   InterfaceTables.HeaderField7,
  ///   InterfaceTables.HeaderField8,
  ///   InterfaceTables.DetailField1,
  ///   InterfaceTables.DetailField2,
  ///   InterfaceTables.DetailField3,
  ///   InterfaceTables.DetailField4,
  ///   InterfaceTables.DetailField5,
  ///   InterfaceTables.DetailField6,
  ///   InterfaceTables.DetailField7,
  ///   InterfaceTables.DetailField8,
  ///   InterfaceTables.StartId,
  ///   InterfaceTables.InsertDetail,
  ///   InterfaceTables.GroupByKey,
  ///   InterfaceTables.AltGroupByKey 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceTables_Insert
(
 @InterfaceTableId int = null output,
 @HeaderTable nvarchar(100) = null,
 @DetailTable nvarchar(100) = null,
 @HeaderTableKey nvarchar(100) = null,
 @Description1 nvarchar(100) = null,
 @Description2 nvarchar(100) = null,
 @HeaderField1 nvarchar(100) = null,
 @HeaderField2 nvarchar(100) = null,
 @HeaderField3 nvarchar(100) = null,
 @HeaderField4 nvarchar(100) = null,
 @HeaderField5 nvarchar(100) = null,
 @HeaderField6 nvarchar(100) = null,
 @HeaderField7 nvarchar(100) = null,
 @HeaderField8 nvarchar(100) = null,
 @DetailField1 nvarchar(100) = null,
 @DetailField2 nvarchar(100) = null,
 @DetailField3 nvarchar(100) = null,
 @DetailField4 nvarchar(100) = null,
 @DetailField5 nvarchar(100) = null,
 @DetailField6 nvarchar(100) = null,
 @DetailField7 nvarchar(100) = null,
 @DetailField8 nvarchar(100) = null,
 @StartId int = null,
 @InsertDetail int = null,
 @GroupByKey nvarchar(100) = null,
 @AltGroupByKey nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceTableId = '-1'
    set @InterfaceTableId = null;
  
	 declare @Error int
 
  insert InterfaceTables
        (HeaderTable,
         DetailTable,
         HeaderTableKey,
         Description1,
         Description2,
         HeaderField1,
         HeaderField2,
         HeaderField3,
         HeaderField4,
         HeaderField5,
         HeaderField6,
         HeaderField7,
         HeaderField8,
         DetailField1,
         DetailField2,
         DetailField3,
         DetailField4,
         DetailField5,
         DetailField6,
         DetailField7,
         DetailField8,
         StartId,
         InsertDetail,
         GroupByKey,
         AltGroupByKey)
  select @HeaderTable,
         @DetailTable,
         @HeaderTableKey,
         @Description1,
         @Description2,
         @HeaderField1,
         @HeaderField2,
         @HeaderField3,
         @HeaderField4,
         @HeaderField5,
         @HeaderField6,
         @HeaderField7,
         @HeaderField8,
         @DetailField1,
         @DetailField2,
         @DetailField3,
         @DetailField4,
         @DetailField5,
         @DetailField6,
         @DetailField7,
         @DetailField8,
         @StartId,
         @InsertDetail,
         @GroupByKey,
         @AltGroupByKey 
  
  select @Error = @@Error, @InterfaceTableId = scope_identity()
  
  
  return @Error
  
end
