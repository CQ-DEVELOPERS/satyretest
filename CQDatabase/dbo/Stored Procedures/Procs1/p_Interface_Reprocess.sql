﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_Reprocess
  ///   Filename       : p_Interface_Reprocess.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Jul 2008
  /// </summary> 
  /// <summary>
  ///   Procedure Name : p_Interface_Reprocess
  ///   Filename       : p_Interface_Reprocess.sql
  ///   Modified By    : William Smith
  ///   Date Modified  : 10 Dec 2010
  /// </summary>
  /// <remarks>
  ///   Modified so that Percentage may be coorect value. the initial sp will have to be changed.
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
Create Procedure p_Interface_Reprocess
(@InterfaceTableId int,
 @InterfaceId      int output,
 @LineNumber       int = null)
 AS
Begin
set nocount on

  declare @string         nvarchar(max),
          @HeaderTable    sysname,
          @DetailTable    sysname,
          @HeaderTableKey sysname,
          @DetailField1   sysname,
          @NewInterfaceId int,
          @OldInterfaceId int,
          @InsertDetail   int,
          @sql            nvarchar(1000),
          @parms          nvarchar(1000)
  
  Select @HeaderTable    = HeaderTable,
		       @DetailTable    = DetailTable,
		       @HeaderTableKey = HeaderTableKey,
		       @InsertDetail   = InsertDetail,
		       @DetailField1   = DetailField1
    from InterfaceTables (nolock)
   where InterfaceTableId = @InterfaceTableId

  if dbo.ufn_Configuration(404, 1) = 1
  begin
    select @string = 'update ' + @HeaderTable + char(13)
    select @string = @string + '   set RecordStatus = ''N''' + char(13)
    select @string = @string + '      ,ProcessedDate = null' + char(13)
    select @string = @string + ' where ' + @HeaderTableKey + ' =  ' + convert(nvarchar(10), @InterfaceId) + char(13)
    execute (@string)
  end
  else
  begin
    if @LineNumber is null
    begin
       --Select case when InterfaceMessageCode = 'Failed' then 0 else 1 end,
		     --  Left(right(InterfaceMessage,Len(InterfaceMessage)-CharIndex('~',InterfaceMessage)),charindex('-',right(InterfaceMessage,Len(InterfaceMessage)-CharIndex('~',InterfaceMessage)))-1),
		     --  *
       update d
          set additional5 = 0 --case when InterfaceMessageCode = 'Failed' then 0 else 1 end
	      from InterfaceMessage m
	      join InterfaceExportSODetail d on d.InterfaceExportSOHeaderId = m.InterfaceId 
                 					               and d.LineNumber = Left(convert(varchar(max),InterfaceMessage),CharIndex('~',InterfaceMessage)-1)
					                                and d.Batch = Left(right(convert(varchar(max),InterfaceMessage),Len(convert(varchar(max),InterfaceMessage))-CharIndex('~',InterfaceMessage)),charindex('-',right(convert(varchar(max),InterfaceMessage),Len(convert(varchar(max),InterfaceMessage))-CharIndex('~',InterfaceMessage)))-1)
	     where OrderNumber like  'IBT%'
	       and charindex('~',InterfaceMessage) > 0
			     and InterfaceMessageCode = 'Failed'
    	 
	     --Select case when InterfaceMessageCode = 'Failed' then 0 else 1 end,
		    --	 Left(right(InterfaceMessage,Len(InterfaceMessage)-CharIndex('~',InterfaceMessage)),charindex('-',right(InterfaceMessage,Len(InterfaceMessage)-CharIndex('~',InterfaceMessage)))-1),
		    --	 *
	     update d
	        set additional5 = 1 --case when InterfaceMessageCode = 'Failed' then 0 else 1 end
	       from InterfaceMessage m
		      join InterfaceExportSODetail d on d.InterfaceExportSOHeaderId = m.InterfaceId 
                  					               and d.LineNumber = Left(convert(varchar(max),InterfaceMessage),CharIndex('~',InterfaceMessage)-1)
					                                 and d.Batch = Left(right(convert(varchar(max),InterfaceMessage),Len(convert(varchar(max),InterfaceMessage))-CharIndex('~',InterfaceMessage)),charindex('-',right(convert(varchar(max),InterfaceMessage),Len(convert(varchar(max),InterfaceMessage))-CharIndex('~',InterfaceMessage)))-1)
	     where OrderNumber like  'IBT%'
	       and charindex('~',InterfaceMessage) > 0
			     and InterfaceMessageCode = 'Processed'
    end

    -- Get the current max id
    set @sql = N'select @cnt = max(' + @HeaderTableKey + ') from ' + @HeaderTable
    set @parms = N'@cnt int output'
    exec sp_executesql @stmt=@sql, @params=@parms, @cnt = @OldInterfaceId output
     
    select @string = 'insert ' + @HeaderTable + char(13)
    select @string = @string + '('
    select @string = @string + name from syscolumns where object_name(id) = @HeaderTable and colorder in (select min(colorder) from syscolumns where object_name(id) = @HeaderTable and name != @HeaderTableKey)
    select @string = @string + ',' + name from syscolumns where object_name(id) = @HeaderTable and colorder > (select min(colorder) from syscolumns where object_name(id) = @HeaderTable and name != @HeaderTableKey) order by colorder
    select @string = @string + ')' + char(13)
    select @string = @string + 'select '
    select @string = @string + name from syscolumns where object_name(id) = @HeaderTable and colorder in (select min(colorder) from syscolumns where object_name(id) = @HeaderTable and name != @HeaderTableKey)
    select @string = @string + ',' + name from syscolumns where object_name(id) = @HeaderTable and colorder > (select min(colorder) from syscolumns where object_name(id) = @HeaderTable and name != @HeaderTableKey) order by colorder
    select @string = @string + char(13) + '  from ' + @HeaderTable
    select @string = @string + char(13) + ' where ' + @HeaderTableKey + ' = ' + convert(nvarchar(10), @InterfaceId)
    select @string = @string + char(13) + '   and RecordStatus != ''R'''
    execute (@string)

    -- get the new key
    set @sql = N'select @cnt = max(' + @HeaderTableKey + ') from ' + @HeaderTable
    set @parms = N'@cnt int output'

    exec sp_executesql @stmt=@sql, @params=@parms, @cnt = @NewInterfaceId output

    if @OldInterfaceId = @NewInterfaceId
    begin
      raiserror 900000 'New record was not inserted'
      return;
    end

    if @InsertDetail = 1
    Begin
	    select @string = 'insert ' + @DetailTable + char(13)
	    select @string = @string + '(' + @HeaderTableKey 
	    select @string = @string + ',' + name from syscolumns where object_name(id) = @DetailTable and name != @HeaderTableKey order by colorder
	    select @string = @string + ')' + char(13)
	    select @string = @string + 'select '
	    select @string = @string + convert(nvarchar(10), @NewInterfaceId)
	    select @string = @string + ',' + name from syscolumns where object_name(id) = @DetailTable and name != @HeaderTableKey order by colorder
	    select @string = @string + char(13) + '  from ' + @DetailTable
	    select @string = @string + char(13) + ' where ' + @HeaderTableKey + ' = ' + convert(nvarchar(10), @InterfaceId)
    	
	    if @LineNumber is not null and @DetailField1 = 'LineNumber'
	      select @string = @string + char(13) + '   and ' + @DetailField1 + ' = ' + convert(nvarchar(10), @LineNumber)
    	
	    execute (@string)
    End

    -- update orignal status to 'R' for Reprocessed
    if @LineNumber is null
    begin
      select @string = 'update ' + @HeaderTable + char(13)
      select @string = @string + '   set RecordStatus = ''R''' + char(13)
      select @string = @string + ' where ' + @HeaderTableKey + ' =  ' + convert(nvarchar(10), @InterfaceId) + char(13)
      execute (@string)
    end

    -- update new status to 'N' to be imported
    select @string = 'update ' + @HeaderTable + char(13)
    select @string = @string + '   set RecordStatus = ''N'',' + char(13)
    select @string = @string + '       ProcessedDate = null' + char(13)
    select @string = @string + ' where ' + @HeaderTableKey + ' =  ' + convert(nvarchar(10), @NewInterfaceId) + char(13)
    execute (@string)

    set @InterfaceId = @NewInterfaceId

    return

    -- Check updated to 'R'
    select @string = 'select * from ' + @HeaderTable + ' where ' + @HeaderTableKey + ' = ' + convert(nvarchar(10), @InterfaceId)
    execute (@string)

    -- Check new Interface Records
    select @string = 'select * from ' + @HeaderTable + ' where ' + @HeaderTableKey + ' = ' + convert(nvarchar(10), @NewInterfaceId)
    execute (@string)

    if @InsertDetail = 1
    Begin
     select @string = 'select * from ' + @DetailTable + ' where ' + @HeaderTableKey + ' = ' + convert(nvarchar(10), @NewInterfaceId)
     execute (@string)
    end
  end
End
