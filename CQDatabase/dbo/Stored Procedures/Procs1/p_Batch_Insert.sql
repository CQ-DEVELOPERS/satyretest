﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Insert
  ///   Filename       : p_Batch_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Sep 2014 15:23:44
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Batch table.
  /// </remarks>
  /// <param>
  ///   @BatchId int = null output,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @Batch nvarchar(100) = null,
  ///   @CreateDate datetime = null,
  ///   @ExpiryDate datetime = null,
  ///   @IncubationDays int = null,
  ///   @ShelfLifeDays int = null,
  ///   @ExpectedYield int = null,
  ///   @ActualYield int = null,
  ///   @ECLNumber nvarchar(20) = null,
  ///   @BatchReferenceNumber nvarchar(100) = null,
  ///   @ProductionDateTime datetime = null,
  ///   @FilledYield int = null,
  ///   @COACertificate bit = null,
  ///   @StatusModifiedBy int = null,
  ///   @StatusModifiedDate datetime = null,
  ///   @CreatedBy int = null,
  ///   @ModifiedBy int = null,
  ///   @ModifiedDate datetime = null,
  ///   @Prints int = null,
  ///   @ParentProductCode nvarchar(60) = null,
  ///   @RI float = null,
  ///   @Density float = null,
  ///   @BOELineNumber nvarchar(100) = null,
  ///   @BillOfEntry nvarchar(100) = null,
  ///   @ReferenceNumber nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   Batch.BatchId,
  ///   Batch.StatusId,
  ///   Batch.WarehouseId,
  ///   Batch.Batch,
  ///   Batch.CreateDate,
  ///   Batch.ExpiryDate,
  ///   Batch.IncubationDays,
  ///   Batch.ShelfLifeDays,
  ///   Batch.ExpectedYield,
  ///   Batch.ActualYield,
  ///   Batch.ECLNumber,
  ///   Batch.BatchReferenceNumber,
  ///   Batch.ProductionDateTime,
  ///   Batch.FilledYield,
  ///   Batch.COACertificate,
  ///   Batch.StatusModifiedBy,
  ///   Batch.StatusModifiedDate,
  ///   Batch.CreatedBy,
  ///   Batch.ModifiedBy,
  ///   Batch.ModifiedDate,
  ///   Batch.Prints,
  ///   Batch.ParentProductCode,
  ///   Batch.RI,
  ///   Batch.Density,
  ///   Batch.BOELineNumber,
  ///   Batch.BillOfEntry,
  ///   Batch.ReferenceNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Insert
(
 @BatchId int = null output,
 @StatusId int = null,
 @WarehouseId int = null,
 @Batch nvarchar(100) = null,
 @CreateDate datetime = null,
 @ExpiryDate datetime = null,
 @IncubationDays int = null,
 @ShelfLifeDays int = null,
 @ExpectedYield int = null,
 @ActualYield int = null,
 @ECLNumber nvarchar(20) = null,
 @BatchReferenceNumber nvarchar(100) = null,
 @ProductionDateTime datetime = null,
 @FilledYield int = null,
 @COACertificate bit = null,
 @StatusModifiedBy int = null,
 @StatusModifiedDate datetime = null,
 @CreatedBy int = null,
 @ModifiedBy int = null,
 @ModifiedDate datetime = null,
 @Prints int = null,
 @ParentProductCode nvarchar(60) = null,
 @RI float = null,
 @Density float = null,
 @BOELineNumber nvarchar(100) = null,
 @BillOfEntry nvarchar(100) = null,
 @ReferenceNumber nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @Batch = '-1'
    set @Batch = null;
  
  if @StatusModifiedBy = '-1'
    set @StatusModifiedBy = null;
  
  if @CreatedBy = '-1'
    set @CreatedBy = null;
  
  if @ModifiedBy = '-1'
    set @ModifiedBy = null;
  
	 declare @Error int
 
  insert Batch
        (StatusId,
         WarehouseId,
         Batch,
         CreateDate,
         ExpiryDate,
         IncubationDays,
         ShelfLifeDays,
         ExpectedYield,
         ActualYield,
         ECLNumber,
         BatchReferenceNumber,
         ProductionDateTime,
         FilledYield,
         COACertificate,
         StatusModifiedBy,
         StatusModifiedDate,
         CreatedBy,
         ModifiedBy,
         ModifiedDate,
         Prints,
         ParentProductCode,
         RI,
         Density,
         BOELineNumber,
         BillOfEntry,
         ReferenceNumber)
  select @StatusId,
         @WarehouseId,
         @Batch,
         @CreateDate,
         @ExpiryDate,
         @IncubationDays,
         @ShelfLifeDays,
         @ExpectedYield,
         @ActualYield,
         @ECLNumber,
         @BatchReferenceNumber,
         @ProductionDateTime,
         @FilledYield,
         @COACertificate,
         @StatusModifiedBy,
         @StatusModifiedDate,
         @CreatedBy,
         @ModifiedBy,
         @ModifiedDate,
         @Prints,
         @ParentProductCode,
         @RI,
         @Density,
         @BOELineNumber,
         @BillOfEntry,
         @ReferenceNumber 
  
  select @Error = @@Error, @BatchId = scope_identity()
  
  if @Error = 0
    exec @Error = p_BatchHistory_Insert
         @BatchId = @BatchId,
         @StatusId = @StatusId,
         @WarehouseId = @WarehouseId,
         @Batch = @Batch,
         @CreateDate = @CreateDate,
         @ExpiryDate = @ExpiryDate,
         @IncubationDays = @IncubationDays,
         @ShelfLifeDays = @ShelfLifeDays,
         @ExpectedYield = @ExpectedYield,
         @ActualYield = @ActualYield,
         @ECLNumber = @ECLNumber,
         @BatchReferenceNumber = @BatchReferenceNumber,
         @ProductionDateTime = @ProductionDateTime,
         @FilledYield = @FilledYield,
         @COACertificate = @COACertificate,
         @StatusModifiedBy = @StatusModifiedBy,
         @StatusModifiedDate = @StatusModifiedDate,
         @CreatedBy = @CreatedBy,
         @ModifiedBy = @ModifiedBy,
         @ModifiedDate = @ModifiedDate,
         @Prints = @Prints,
         @ParentProductCode = @ParentProductCode,
         @RI = @RI,
         @Density = @Density,
         @BOELineNumber = @BOELineNumber,
         @BillOfEntry = @BillOfEntry,
         @ReferenceNumber = @ReferenceNumber,
         @CommandType = 'Insert'
  
  return @Error
  
end
