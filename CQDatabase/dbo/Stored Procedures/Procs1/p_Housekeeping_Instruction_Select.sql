﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Instruction_Select
  ///   Filename       : p_Housekeeping_Instruction_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 29 May 2007 11:00:34
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output
  /// </param>
  /// <returns>
  ///   ProductCode,
  ///   Product,
  ///   SKUCode,
  ///   Batch,
  ///   ECLNumber,
  ///   Status,
  ///   Quantity
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Instruction_Select
(
 @WarehouseId       int = null,
 @InstructionTypeId int
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   InstructionId      int,
   JobId              int,
   StorageUnitBatchId int,
   ProductCode	       nvarchar(30),
   Product	           nvarchar(255),
   SKUCode            nvarchar(50),
   Quantity           float,
   Status             nvarchar(50),
   PickLocationId     int,
   PickLocation       nvarchar(15),
   StoreLocationId    int,
   StoreLocation      nvarchar(15),
   PalletId           int,
   CreateDate         datetime,
   Batch              nvarchar(50),
   ECLNumber          nvarchar(10),
   PriorityId         int,
   Priority           nvarchar(50),
   OperatorId         int,
   Operator           nvarchar(50),
   ReferenceNumber    nvarchar(30)
  )
  
  if @InstructionTypeId = -1
    set @InstructionTypeId = null
  
  insert @TableResult
        (InstructionId,
         JobId,
         StorageUnitBatchId,
         Quantity,
         Status,
         PickLocationId,
         StoreLocationId,
         PalletId,
         CreateDate,
         PriorityId,
         OperatorId,
         ReferenceNumber)
  select i.InstructionId,
         j.JobId,
         i.StorageUnitBatchId,
         i.Quantity,
         sj.Status,
         i.PickLocationId,
         i.StoreLocationId,
         i.PalletId,
         i.CreateDate,
         j.PriorityId,
         j.OperatorId,
         j.ReferenceNumber
    from Job                 j   (nolock)
    join Instruction         i   (nolock) on j.JobId              = i.JobId
    join InstructionType     it  (nolock) on i.InstructionTypeId  = it.InstructionTypeId
    join Status              sj  (nolock) on j.StatusId           = sj.StatusId
    join Status              si  (nolock) on i.StatusId           = si.StatusId
   where i.WarehouseId        = @WarehouseId
     and it.InstructionTypeId = isnull(@InstructionTypeId, it.InstructionTypeId)
     and it.InstructionTypeCode in ('M','R','O','HS','RS')
     and si.StatusCode        in ('W','S')
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product,
         SKUCode     = sku.SKUCode,
         Batch       = b.Batch,
         ECLNumber   = b.ECLNumber
    from @TableResult tr
    join StorageUnitBatch    sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit         su  (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product             p   (nolock) on su.ProductId         = p.ProductId
    join SKU                 sku (nolock) on su.SKUId             = sku.SKUId
    join Batch               b   (nolock) on sub.BatchId          = b.BatchId
  
  update tr
     set PickLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.PickLocationId = l.LocationId
  
  update tr
     set StoreLocation = l.Location
    from @TableResult tr
    join Location     l  (nolock) on tr.StoreLocationId = l.LocationId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p  (nolock) on tr.PriorityId = p.PriorityId
  
  update tr
     set Operator = o.Operator
    from @TableResult tr
    join Operator      o (nolock) on tr.OperatorId = o.OperatorId
  
  select InstructionId,
         JobId,
         ProductCode,
         Product,
         SKUCode,
         Quantity,
         Status,
         PickLocation,
         StoreLocation,
         PalletId,
         CreateDate,
         Batch,
         ECLNumber,
         PriorityId,
         Priority,
         Operator,
         ReferenceNumber
    from @TableResult
  order by JobId, ProductCode, SKUCode
end
