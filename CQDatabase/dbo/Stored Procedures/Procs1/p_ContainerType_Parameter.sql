﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerType_Parameter
  ///   Filename       : p_ContainerType_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:31
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ContainerType table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ContainerType.ContainerTypeId,
  ///   ContainerType.ContainerType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerType_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as ContainerTypeId
        ,'{All}' as ContainerType
  union
  select
         ContainerType.ContainerTypeId
        ,ContainerType.ContainerType
    from ContainerType
  
end
