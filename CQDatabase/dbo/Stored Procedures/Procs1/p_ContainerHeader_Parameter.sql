﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerHeader_Parameter
  ///   Filename       : p_ContainerHeader_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:54
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ContainerHeader table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ContainerHeader.ContainerHeaderId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerHeader_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as ContainerHeaderId
        ,null as 'ContainerHeader'
  union
  select
         ContainerHeader.ContainerHeaderId
        ,ContainerHeader.ContainerHeaderId as 'ContainerHeader'
    from ContainerHeader
  
end
