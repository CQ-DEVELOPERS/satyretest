﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_Search_StorageUnit
  ///   Filename       : p_Area_Search_StorageUnit.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_Search_StorageUnit
(
 @StorageUnitId int
)

as
begin
	 set nocount on;
  
  select a.AreaId,
         a.Area
    from StorageUnitArea sua (nolock)
    join Area              a (nolock) on sua.AreaId = a.AreaId
   where sua.StorageUnitId = @StorageUnitId
end
