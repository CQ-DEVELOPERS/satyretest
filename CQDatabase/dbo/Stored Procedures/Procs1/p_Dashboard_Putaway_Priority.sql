﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Putaway_Priority
  ///   Filename       : p_Dashboard_Putaway_Priority.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Putaway_Priority
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)

as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
    select ROW_NUMBER() 
           OVER (ORDER BY RequiredQuantity desc) AS 'Row',
           vp.ProductCode,
           vp.Product,
           vp.SKUCode,
           RequiredQuantity,
           ReceivingQuantity
      from DashboardPutawayPriority tr
      join viewProduct  vp on tr.StorageUnitId = vp.StorageUnitId
     where tr.WarehouseId = @WarehouseId
    order by 'Row'
	 end
	 else
	 begin
	   truncate table DashboardPutawayPriority
	   
    insert DashboardPutawayPriority
          (WarehouseId,
           StorageUnitId,
           RequiredQuantity)
    select va.WarehouseId,
           va.StorageUnitId,
           sum(va.ReservedQuantity - va.ActualQuantity)
      from viewAvailableArea va
     where va.AreaCode in ('BK','PK','RK','SP')
       and va.WarehouseId = isnull(@WarehouseId, va.WarehouseId)
    group by va.WarehouseId,
             va.StorageUnitId
    having sum(va.ReservedQuantity - va.ActualQuantity) > 0
    
    insert DashboardPutawayPriority
          (WarehouseId,
           StorageUnitId,
           RequiredQuantity)
    select vi.WarehouseId,
           ili.StorageUnitId,
           sum(ili.ConfirmedQuantity)
      from viewili ili
      join viewIssue vi on ili.IssueId = vi.IssueId
     where vi.WarehouseId = isnull(@WarehouseId, vi.WarehouseId)
       and ili.JobCode = 'NS'
       and vi.StatusCode in ('RL''S')
    group by vi.WarehouseId,
             ili.StorageUnitId
    
    update tr
       set ReceivingQuantity = va.ActualQuantity
      from DashboardPutawayPriority tr
      join viewAvailableArea va on tr.WarehouseId = va.WarehouseId
                               and tr.StorageUnitId = va.StorageUnitId
     where va.AreaCode = 'R'
    
    --select * from viewAvailableArea where StorageUnitId = 1591
    
    --delete DashboardPutawayPriority where isnull(ReceivingQuantity, 0) <= 0 
	 end
end
