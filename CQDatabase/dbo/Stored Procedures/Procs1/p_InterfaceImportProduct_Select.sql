﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportProduct_Select
  ///   Filename       : p_InterfaceImportProduct_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:04
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportProduct table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportProductId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportProduct.InterfaceImportProductId,
  ///   InterfaceImportProduct.RecordStatus,
  ///   InterfaceImportProduct.ProcessedDate,
  ///   InterfaceImportProduct.ProductCode,
  ///   InterfaceImportProduct.Product,
  ///   InterfaceImportProduct.SKUCode,
  ///   InterfaceImportProduct.SKU,
  ///   InterfaceImportProduct.PalletQuantity,
  ///   InterfaceImportProduct.Barcode,
  ///   InterfaceImportProduct.MinimumQuantity,
  ///   InterfaceImportProduct.ReorderQuantity,
  ///   InterfaceImportProduct.MaximumQuantity,
  ///   InterfaceImportProduct.CuringPeriodDays,
  ///   InterfaceImportProduct.ShelfLifeDays,
  ///   InterfaceImportProduct.QualityAssuranceIndicator,
  ///   InterfaceImportProduct.ProductType,
  ///   InterfaceImportProduct.ProductCategory,
  ///   InterfaceImportProduct.OverReceipt,
  ///   InterfaceImportProduct.RetentionSamples,
  ///   InterfaceImportProduct.HostId,
  ///   InterfaceImportProduct.InsertDate,
  ///   InterfaceImportProduct.Additional1,
  ///   InterfaceImportProduct.Additional2,
  ///   InterfaceImportProduct.Additional3,
  ///   InterfaceImportProduct.Additional4,
  ///   InterfaceImportProduct.Additional5,
  ///   InterfaceImportProduct.Additional6,
  ///   InterfaceImportProduct.Additional7,
  ///   InterfaceImportProduct.Additional8,
  ///   InterfaceImportProduct.Additional9,
  ///   InterfaceImportProduct.Additional10,
  ///   InterfaceImportProduct.DangerousGoodsCode,
  ///   InterfaceImportProduct.ABCStatus,
  ///   InterfaceImportProduct.Samples 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportProduct_Select
(
 @InterfaceImportProductId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceImportProduct.InterfaceImportProductId
        ,InterfaceImportProduct.RecordStatus
        ,InterfaceImportProduct.ProcessedDate
        ,InterfaceImportProduct.ProductCode
        ,InterfaceImportProduct.Product
        ,InterfaceImportProduct.SKUCode
        ,InterfaceImportProduct.SKU
        ,InterfaceImportProduct.PalletQuantity
        ,InterfaceImportProduct.Barcode
        ,InterfaceImportProduct.MinimumQuantity
        ,InterfaceImportProduct.ReorderQuantity
        ,InterfaceImportProduct.MaximumQuantity
        ,InterfaceImportProduct.CuringPeriodDays
        ,InterfaceImportProduct.ShelfLifeDays
        ,InterfaceImportProduct.QualityAssuranceIndicator
        ,InterfaceImportProduct.ProductType
        ,InterfaceImportProduct.ProductCategory
        ,InterfaceImportProduct.OverReceipt
        ,InterfaceImportProduct.RetentionSamples
        ,InterfaceImportProduct.HostId
        ,InterfaceImportProduct.InsertDate
        ,InterfaceImportProduct.Additional1
        ,InterfaceImportProduct.Additional2
        ,InterfaceImportProduct.Additional3
        ,InterfaceImportProduct.Additional4
        ,InterfaceImportProduct.Additional5
        ,InterfaceImportProduct.Additional6
        ,InterfaceImportProduct.Additional7
        ,InterfaceImportProduct.Additional8
        ,InterfaceImportProduct.Additional9
        ,InterfaceImportProduct.Additional10
        ,InterfaceImportProduct.DangerousGoodsCode
        ,InterfaceImportProduct.ABCStatus
        ,InterfaceImportProduct.Samples
    from InterfaceImportProduct
   where isnull(InterfaceImportProduct.InterfaceImportProductId,'0')  = isnull(@InterfaceImportProductId, isnull(InterfaceImportProduct.InterfaceImportProductId,'0'))
  
end
