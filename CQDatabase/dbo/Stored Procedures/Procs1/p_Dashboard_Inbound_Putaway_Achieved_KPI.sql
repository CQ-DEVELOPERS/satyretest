﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Inbound_Putaway_Achieved_KPI
  ///   Filename       : p_Dashboard_Inbound_Putaway_Achieved_KPI.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Inbound_Putaway_Achieved_KPI
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)

as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select KPI,
	          sum(Value) / count(distinct(convert(nvarchar(2), CreateDate, 108))) as 'Value'
      from DashboardInboundPutawayAchieved
    group by KPI
	 end
end
