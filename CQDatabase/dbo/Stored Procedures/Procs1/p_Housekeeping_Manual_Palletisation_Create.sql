﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Manual_Palletisation_Create
  ///   Filename       : p_Housekeeping_Manual_Palletisation_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Manual_Palletisation_Create
(
 @InstructionTypeCode nvarchar(10),
 @StorageUnitBatchId  int,
 @PickLocationId      int,
 @NumberOfLines       int,
 @Quantity            float,
 @ReasonId            int
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @LineQuantity       float,
          @WarehouseId        int,
          @AcceptedQuantity   float,
          @PalletQuantity     float,
          @PalletCount        int,
          @RemainingQuantity  float,
          @StatusId           int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Housekeeping_Manual_Palletisation_Create'
  
  if @ReasonId = -1
    set @ReasonId = null
  
  select @WarehouseId        = a.WarehouseId,
         @AcceptedQuantity   = subl.ActualQuantity - subl.ReservedQuantity
    from StorageUnitBatchLocation subl (nolock)
    join AreaLocation               al (nolock) on subl.LocationId = al.LocationId
    join Area                        a (nolock) on al.AreaId       = a.AreaId
   where subl.StorageUnitBatchId = @StorageUnitBatchId
     and subl.LocationId         = @PickLocationId
     and (subl.ActualQuantity - subl.ReservedQuantity) > 0
  
  if @AcceptedQuantity is null
    return -1
  
  if @Quantity > @AcceptedQuantity
    return -2
  
  set @AcceptedQuantity = @Quantity
  
  select top 1 @PalletQuantity = p.Quantity
    from StorageUnitBatch sub (nolock)
    join Pack             p   (nolock) on sub.StorageUnitId = p.StorageUnitId
    join PackType         pt  (nolock) on p.PackTypeId      = pt.PackTypeId
   where StorageUnitBatchId = @StorageUnitBatchId
  order by InboundSequence
  
  begin transaction
  
  if (@AcceptedQuantity / @PalletQuantity) > @NumberOfLines
  begin
    set @Error = 100
    set @Errormsg = 'p_Housekeeping_Manual_Palletisation_Create - Cannot put more than a full pallet quantity onto single line'
    goto error
  end
  
  if @AcceptedQuantity < @NumberOfLines 
  begin
    set @Error = 200
    set @Errormsg = 'p_Housekeeping_Manual_Palletisation_Create - Cannot put a Quantity of ' + convert(nvarchar(10), @AcceptedQuantity) + ' on ' + convert(nvarchar(10), @NumberOfLines) + ' lines'
    goto error
  end
  
  if @AcceptedQuantity > @PalletQuantity
  begin
    set @PalletCount = floor(@AcceptedQuantity / @PalletQuantity)
    set @RemainingQuantity = @AcceptedQuantity - (@PalletQuantity * @PalletCount)
    
    if @PalletCount > 0
    begin
      while @PalletCount > 0
      begin
        set @PalletCount = @PalletCount - 1
        set @NumberOfLines = @NumberOfLines - 1
        
        exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @InstructionTypeCode = @InstructionTypeCode,
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = @PickLocationId,
         @StoreLocationId     = null,
         @Quantity            = @PalletQuantity,
         @ReasonId            = @ReasonId
        
        if @error <> 0
          goto error
      end
    end
  end
  if @NumberOfLines > 0
    if @RemainingQuantity is null or @RemainingQuantity = 0
    begin
      set @LineQuantity      = @AcceptedQuantity / @NumberOfLines
      
      set @RemainingQuantity = @AcceptedQuantity - (floor(@AcceptedQuantity / @NumberOfLines) * @NumberOfLines)
    end
    else
    begin
      set @LineQuantity = @RemainingQuantity / @NumberOfLines
      set @NumberOfLines = @NumberOfLines - 1
    end
  
  while @NumberOfLines > 0
  begin
    set @NumberOfLines = @NumberOfLines -1
    
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @InstructionTypeCode = @InstructionTypeCode,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = null,
     @Quantity            = @LineQuantity,
     @ReasonId            = @ReasonId

    if @error <> 0
      goto error
  end
  
  if @RemainingQuantity > 0
  begin
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @InstructionTypeCode = @InstructionTypeCode,
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = null,
     @Quantity            = @RemainingQuantity,
     @ReasonId            = @ReasonId

    if @error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
