﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_GetAvailableStock
  ///   Filename       : p_GetAvailableStock.sql
  ///   Create By      : Ruan groenewald
  ///   Date Created   : 05 Sept 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>	 
  /// </param>
  /// <returns>

  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_GetAvailableStock
(
     @StorageUnitId int = null
   , @StorageUnitBatchId int = null
)
AS
BEGIN
    declare @StockAvailable int

    select @StockAvailable = sum(subl.ActualQuantity - subl.ReservedQuantity + subl.AllocatedQuantity)
    from StorageUnitBatch sub
    inner join StorageUnitBatchLocation subl on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    inner join AreaLocation al on subl.LocationId = al.LocationId
    inner join Area a on al.AreaId = a.AreaId
    where a.StockOnHand = 1
      and ISNULL(@StorageUnitId,0) = CASE WHEN @StorageUnitId IS NULL THEN 0 ELSE sub.StorageUnitId END
      and ISNULL(@StorageUnitBatchId,0) = CASE WHEN @StorageUnitBatchId IS NULL THEN 0 ELSE sub.StorageUnitBatchId END

    return @StockAvailable
END
