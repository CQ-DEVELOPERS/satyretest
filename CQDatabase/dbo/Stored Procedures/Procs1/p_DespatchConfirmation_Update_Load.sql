﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DespatchConfirmation_Update_Load
  ///   Filename       : p_DespatchConfirmation_Update_Load.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DespatchConfirmation_Update_Load
(
 @outboundShipmentId int,
 @issueId            int,
 @despatchedDate     datetime
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  exec @Error = p_Issue_Update
   @IssueId      = @IssueId,
   @DeliveryDate = @despatchedDate
  
  if @Error <> 0
    goto error
  
  exec @Error = p_OutboundShipment_Update
   @OutboundShipmentId = @OutboundShipmentId,
   @ShipmentDate       = @despatchedDate
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Outbound_Release
   @OutboundShipmentId = @outboundShipmentId,
   @IssueId            = @issueId,
   @StatusCode         = 'C'
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_DespatchConfirmation_Update_Load'); 
    rollback transaction
    return @Error
end
