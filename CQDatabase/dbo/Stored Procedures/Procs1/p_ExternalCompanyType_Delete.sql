﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompanyType_Delete
  ///   Filename       : p_ExternalCompanyType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:39
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ExternalCompanyType table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompanyType_Delete
(
 @ExternalCompanyTypeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ExternalCompanyType
     where ExternalCompanyTypeId = @ExternalCompanyTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
