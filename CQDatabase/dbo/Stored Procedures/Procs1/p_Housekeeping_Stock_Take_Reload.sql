﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Reload
  ///   Filename       : p_Housekeeping_Stock_Take_Reload.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Oct 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Reload 
(  
	@WarehouseId int
)

as
begin
  set nocount on;

  declare @SOH as table
  (
   WarehouseId   int,
   WarehouseCode nvarchar(20),
   StorageUnitId nvarchar(30),
   BatchId       nvarchar(50),
   Quantity      float,
   NettWeight float
  )

  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @ProcessedDate     datetime,
          --@Insertdate        datetime,
          @Currentdate       datetime,
          @OldCompareDate    datetime,
          @OldWarehouseId    int,
          @Rows int

  declare @InsertDates As Table
  (
	 InsertDate DateTime 
	,PrincipalCode VarChar(50)
  )

  set @OldWarehouseId = @WarehouseId

  if (select dbo.ufn_Configuration(266, @OldWarehouseId)) = 1 -- ST Upload - Combine warehouses
    set @WarehouseId = null

  select @GetDate         = dbo.ufn_Getdate()
  --select @Insertdate      = max(InsertDate)     from InterfaceImportSOH  (nolock)
  select @CurrentDate     = max(HostDate)       from HousekeepingCompare (nolock) where WarehouseId = @WarehouseId
  select @OldCompareDate  = max(ComparisonDate) from HousekeepingCompare (nolock) where WarehouseId = @WarehouseId

  --if @Insertdate is null
  --  set @Insertdate = '1900-01-02' -- 1 day older than create date to force fresh start

  if @CurrentDate is null
    set @CurrentDate = '1900-01-01'

  select @GetDate        as '@GetDate',
         --@Insertdate     as '@Insertdate',
         @CurrentDate    as '@CurrentDate',
         @OldCompareDate as '@OldCompareDate'

  begin transaction
  
  delete HousekeepingCompare
  
  Insert Into @InsertDates
  Select MAX(InsertDate), PrincipalCode
  From InterfaceImportSOH (nolock)
  Where InsertDate >= @Currentdate
  Group By PrincipalCode

  Select @Rows = @@ROWCOUNT

  update InterfaceImportSOH set Batch  = null where Batch = ''

  update HousekeepingCompare
     set Sent = null
   where Sent = 0

  select @Error = @@Error

  if @Error <> 0
    goto error
  -------------------------------
  -- There is a new SOH from Host
  -------------------------------

  if (@Rows > 0)
  begin
    if (select dbo.ufn_Configuration(233, @OldWarehouseId)) = 1 -- ST Upload - Batches on
    begin
      insert HousekeepingCompare
            (WarehouseId,
             WarehouseCode,
             ComparisonDate,
             StorageUnitId,
             BatchId,
             WMSQuantity,
             HostQuantity,
             Sent)
      select @WarehouseId
         ,ISNULL(a.WarehouseCode, 'Unknown') as WarehouseCode
         ,@GetDate
         ,sub.StorageUnitId
         ,sub.BatchId
         ,SUM(subl.ActualQuantity)
         ,0
         ,0
      from StorageUnitBatch sub (nolock)
   inner join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
   inner join Location l (nolock) on subl.LocationId = l.LocationId
   inner join AreaLocation al (nolock) on l.LocationId = al.LocationId
   inner join Area a (nolock) on al.AreaId = a.AreaId
                                         and a.WarehouseCode in (select WarehouseCode from Warehouse (nolock) where isnull(UploadToHost,0) = 1)
     where a.StockOnHand = 1
     and a.WarehouseId = isnull(@WarehouseId, a.WarehouseId)
  group by a.WarehouseCode
           ,sub.StorageUnitId
           ,sub.BatchId
    end
    else
    begin -- ST Upload - Batches off
      insert HousekeepingCompare
            (WarehouseId,
             WarehouseCode,
             ComparisonDate,
             StorageUnitId,
             BatchId,
             WMSQuantity,
             HostQuantity,
             Sent)
      select @WarehouseId,
             Isnull(b.WarehouseCode,'Unknown') as WarehouseCode,
             @GetDate,
             b.StorageUnitId,
             a.BatchId,
             sum(b.ActualQuantity),
             0,
             0
        from
(select
 sub.StorageUnitId
,b.BatchId
from StorageUnitBatch sub (nolock)
inner join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
inner join Product            p (nolock) on su.ProductId          = p.ProductId
inner join SKU              sku (nolock) on su.SKUId              = sku.SKUId
inner join Batch              b (nolock) on sub.BatchId           = b.BatchId
inner join Status             s (nolock) on b.StatusId            = s.StatusId
where b.Batch = 'default') a
inner join
(select
 sub.StorageUnitId
,subl.ActualQuantity
,a.WarehouseId
,a.WarehouseCode
from StorageUnitBatch sub (nolock)
join StorageUnitBatchLocation subl on sub.StorageUnitBatchId = subl.StorageUnitBatchId join Location l on subl.LocationId = l.LocationId join AreaLocation al on l.LocationId = al.LocationId join Area a on al.AreaId = a.AreaId
                           and a.WarehouseCode in (select WarehouseCode from Warehouse (nolock) where isnull(UploadToHost,0) = 1) where a.StockOnHand = 1) b on a.StorageUnitId = b.StorageUnitId where WarehouseId = isnull(@WarehouseId, WarehouseId) group by b.WarehouseCode,  b.StorageUnitId,  a.BatchId
    end
  end
  else
  ----------------------------------------------------
  -- Update the WMS Quantities from SOH On all UNSENT
  ----------------------------------------------------
  begin
    update HousekeepingCompare
       set ComparisonDate = @GetDate
     where ComparisonDate = @OldCompareDate
       and WarehouseCode in (select WarehouseCode from Warehouse (nolock) where isnull(UploadToHost,0) = 1)

    if (select dbo.ufn_Configuration(233, @OldWarehouseId)) = 1 -- ST Upload - Batches on
    begin
      insert @SOH
            (WarehouseId,
             WarehouseCode,
             StorageUnitId,
             BatchId,
             Quantity,
             NettWeight)
      Select @WarehouseId,
             isnull(WarehouseCode,'Unknown'),
             StorageUnitId,
             BatchId,
             sum(ActualQuantity),
             SUM(Isnull(subl.NettWeight,0))
        from StorageUnitBatch sub (nolock)  join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId  join Location l (nolock) on subl.LocationId = l.LocationId  join AreaLocation al (nolock) on l.LocationId = al.LocationId  join Area a (nolock) on al.AreaId = a.AreaId
                                       and a.WarehouseCode in (select WarehouseCode from Warehouse (nolock) where isnull(UploadToHost,0) = 1)
   where a.StockOnHand = 1
             and a.WarehouseId = isnull(@WarehouseId, a.WarehouseId)
      group by a.WarehouseCode,
               sub.StorageUnitId,
               sub.BatchId
    end
    else
    begin -- ST Upload - Batches off
      insert @SOH
            (WarehouseId,
             WarehouseCode,
             StorageUnitId,
             BatchId,
             Quantity,
             NettWeight)
      Select @WarehouseId,
             isnull(b.WarehouseCode,'Unknown'),
             b.StorageUnitId,
             a.BatchId,
             sum(b.ActualQuantity),
             SUM(Isnull(b.NettWeight,0))
        from (select sub.StorageUnitId
                  ,b.BatchId
               from StorageUnitBatch sub (nolock)
              join StorageUnit       su (nolock) on sub.StorageUnitId     = su.StorageUnitId
              join Product            p (nolock) on su.ProductId          = p.ProductId
              join SKU              sku (nolock) on su.SKUId              = sku.SKUId
              join Batch              b (nolock) on sub.BatchId           = b.BatchId
              join Status             s (nolock) on b.StatusId            = s.StatusId
              where b.Batch = 'default') a
      join (select sub.StorageUnitId
                ,subl.ActualQuantity
                ,a.WarehouseId
                ,a.WarehouseCode
                ,subl.NettWeight
             from StorageUnitBatch sub (nolock)
                join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
                join Location l (nolock) on subl.LocationId = l.LocationId
                join AreaLocation al (nolock) on l.LocationId = al.LocationId
                join Area a (nolock) on al.AreaId = a.AreaId
                           and a.WarehouseCode in (select WarehouseCode from Warehouse (nolock) where isnull(UploadToHost,0) = 1) where a.StockOnHand = 1) b on a.StorageUnitId = b.StorageUnitId where WarehouseId = isnull(@WarehouseId, WarehouseId) group by b.WarehouseCode,  b.StorageUnitId,  a.BatchId
    end
    declare @InboundSequence int

    select @InboundSequence = max(InboundSequence)
    from PackType (nolock)

  --  update @SOH
  --   set soh.NettWeight = ((select max(pk.NettWeight)
  --                           from StorageUnitBatch       sub (nolock)
--                        join StorageUnit             su (nolock) on sub.StorageUnitId = su.StorageUnitId
--                        join Product                  p (nolock) on su.ProductId      = p.ProductId
--                        join Pack                    pk (nolock) on su.StorageUnitId  = pk.StorageUnitId
--                        join PackType                pt (nolock) on pk.PackTypeId     = pt.PackTypeId
  --                          where pt.InboundSequence = @InboundSequence
  --                            and sub.StorageUnitId = soh.StorageUnitId
  --                            and isnull(su.ProductCategory,'') != 'V')* soh.Quantity)
  -- from @SOH soh
--Join StorageUnit su on su.StorageUnitId = soh.StorageUnitId
  --where su.ProductCategory != 'V'


    update hc
       set WMSQuantity  = isnull(s.Quantity,0),
         CustomField1 = isnull(s.NettWeight,0),
           Sent         = 0
      from HousekeepingCompare hc (nolock)
      left
      join @SOH                 S on isnull(s.warehouseId,-1)   = isnull(hc.WarehouseId,-1) and
                                     s.WarehouseCode = hc.Warehousecode and
                                     s.StorageUnitId = hc.StorageUnitId and
                                     s.BatchId       = hc.BatchId
    where ComparisonDate = @GetDate
      and isnull(hc.sent,0) = 0

    ---------------------------------------------
    -- Insert new SOH Quantities
    ---------------------------------------------
    insert HousekeepingCompare
          (WarehouseId,
           WarehouseCode,
           ComparisonDate,
           StorageUnitId,
           BatchId,
           WMSQuantity,
           HostQuantity,
           Sent)
    select @WarehouseId,
           s.WarehouseCode,
           @GetDate,
           s.StorageUnitId,
           s.BatchId,
           s.Quantity,
           0,
           0
      from @SOH s
      left
      join HousekeepingCompare hc (nolock) on hc.WarehouseCode  = s.WarehouseCode and
                                     hc.StorageUnitId  = s.StorageUnitId and
                                     hc.BatchId        = s.BatchId and
                                     hc.Comparisondate = @GetDate  and
                                     hc.WarehouseId = isnull(@WarehouseId, -1)  where hc.StorageUnitId is null

    select @Error = @@Error

    if @Error <> 0
      goto error
  end

  update hc
     set WarehouseCode = wm.HostCode
    from HousekeepingCompare hc (nolock)
    join StorageUnit         su (nolock) on hc.StorageUnitId= su.StorageUnitId
    join Product              p (nolock) on su.ProductId = p.ProductId
    join Principal           pn (nolock) on p.PrincipalId = pn.PrincipalId
    join WarehouseMatrix     wm (nolock) on pn.PrincipalId = wm.PrincipalId
                                        and hc.WarehouseCode = wm.WarehouseCode
   where hc.ComparisonDate = @GetDate

  select @Error = @@Error

  if @Error <> 0
    goto error

  ---------------------------------------------
  -- Update Host Quantities from Latest Import
  ---------------------------------------------
  if (select dbo.ufn_Configuration(232, @OldWarehouseId)) = 1 -- ST Upload - Host Id on|off
  begin
    update hc
       set HostQuantity = host.Quantity,
           HostDate     = host.InsertDate,
           UnitPrice    = host.UnitPrice
      from InterfaceImportSOH host (nolock)
      join @InsertDates        ins          on host.insertDate                    = ins.InsertDate
                                 and isnull(host.PrincipalCode,'-1')= isnull(ins.PrincipalCode,'-1')
      join Warehouse             w (nolock) on w.HostId                           = host.Location
      join Product               p (nolock) on host.ProductCode                   = p.HostId
                                           and isnull(w.PrincipalId,-1)           = isnull(p.PrincipalId,-1)
      join StorageUnit          su (nolock) on p.ProductId                        = su.ProductId
      join SKU                 sku (nolock) on su.SKUId                           = sku.SKUId
                                           and isnull(host.SKUCode, sku.SKUCode)  = sku.SKUCode
      join Batch                 b (nolock) on isnull(host.Batch,'Default')       = b.Batch
      join HousekeepingCompare  hc (nolock) on su.StorageUnitId                   = hc.StorageUnitId
                                           and b.BatchId                          = hc.BatchId
                                           and isnull(w.WarehouseCode, 'Unknown') = hc.WarehouseCode
     where hc.ComparisonDate = @GetDate
       and isnull(hc.sent,0) = 0
       --and host.InsertDate   = @Insertdate

    select @Error = @@Error

    if @Error <> 0
      goto error
  end
  else
  begin
    update hc
       set HostQuantity = host.Quantity,
           HostDate     = host.Insertdate,
           UnitPrice    = host.UnitPrice
      from InterfaceImportSOH host (nolock)
      join @InsertDates        ins          on host.insertDate                   = ins.InsertDate
                                 and isnull(host.PrincipalCode,'-1')= isnull(ins.PrincipalCode,'-1')
     left join Product               p (nolock) on host.ProductCode                  = p.ProductCode
      left
      join Principal pr (nolock) on host.PrincipalCode     = pr.PrincipalCode
	   and p.PrincipalId = pr.PrincipalId
      left join StorageUnit          su (nolock) on p.ProductId                       = su.ProductId
      left join SKU                 sku (nolock) on su.SKUId                          = sku.SKUId
                                           and isnull(host.SKUCode, sku.SKUCode) = sku.SKUCode
      left join Batch                 b (nolock) on isnull(host.Batch,'Default')      = b.Batch
      join HousekeepingCompare  hc (nolock) on su.StorageUnitId                  = hc.StorageUnitId
                                           and b.BatchId                         = hc.BatchId
                                           and isnull(host.Location, 'Unknown')  = hc.WarehouseCode
     where hc.ComparisonDate = @GetDate
       and isnull(hc.sent,0) = 0
       --and host.InsertDate   = @Insertdate

    select @Error = @@Error

    if @Error <> 0
      goto error
  end

  ------------------------------------------------------
  -- Insert products from host that do not exist on WMS
  ------------------------------------------------------
  if (select dbo.ufn_Configuration(232, @OldWarehouseId)) = 1 -- ST Upload - Host Id on|off
  begin
    insert HousekeepingCompare
          (WarehouseId,
           WarehouseCode,
           ComparisonDate,
           HostDate,
           StorageUnitId,
           BatchId,
           WMSQuantity,
           HostQuantity,
           UnitPrice,
           Sent)
    select @WarehouseId,
           w.WarehouseCode,
           @getdate,
           s.Insertdate,
           su.StorageUnitId,
           b.BatchId,
           0,
           sum(s.Quantity),
           min(s.UnitPrice) as UnitPrice,
           0
      from InterfaceImportSOH    s  (nolock)
      join @InsertDates        ins          on s.insertDate                    = ins.InsertDate
                                 and isnull(s.PrincipalCode,'-1')= isnull(ins.PrincipalCode,'-1')
      --Left
      join Product               p (nolock) on s.ProductCode                  = p.HostId
      join StorageUnit          su (nolock) on p.ProductId                    = su.ProductId
      join SKU                 sku (nolock) on su.SKUId                       = sku.SKUId
                                           and isnull(s.SKUCode, sku.SKUCode) = sku.SKUCode
      join Batch                 b (nolock) on isnull(s.Batch,'Default')      = b.Batch
      join StorageUnitBatch    sub (nolock) on b.batchId                      = sub.batchId
                                           and su.StorageUnitId               = sub.StorageUnitId
      Join Warehouse             w (nolock) on w.HostId                       = s.Location
                                           and isnull(w.PrincipalId,-1)       = isnull(p.PrincipalId,-1)
      left
      join HousekeepingCompare hc (nolock) on hc.WarehouseCode                = w.WarehouseCode
                              and hc.StorageUnitId                            = su.StorageUnitId
                              and hc.BatchId                                  = b.BatchId
                              and hc.Comparisondate                           = @GetDate
     --where not exists(select 1
     --                   from HousekeepingCompare hc (nolock)
     --                  where hc.WarehouseCode  = w.WarehouseCode
     --                    and hc.StorageUnitId  = su.StorageUnitId
     --                    and hc.BatchId        = b.BatchId
     --                    and hc.Comparisondate = @GetDate)
     where --s.InsertDate = @Insertdate
        hc.HousekeepingCompareId is null
       and s.Location in (select HostId from Warehouse (nolock) where isnull(UploadToHost,0) = 1)
    group by w.WarehouseCode,
 s.InsertDate,
             su.StorageUnitId,
             b.BatchId
             --Isnull(p.Product,'Unk')
  end
  else
  begin
    insert HousekeepingCompare
          (WarehouseId,
           WarehouseCode,
           ComparisonDate,
           HostDate,
           StorageUnitId,
           BatchId,
           WMSQuantity,
           HostQuantity,
           UnitPrice,
           Sent)
    select @WarehouseId,
           s.Location,
           @getdate,
           s.Insertdate,
           su.StorageUnitId,
           b.BatchId,
           0,
           sum(s.Quantity),
           min(s.UnitPrice) as UnitPrice,
           0
      from InterfaceImportSOH    s (nolock)
      join @InsertDates        ins          on s.insertDate                    = ins.InsertDate
                                 and isnull(s.PrincipalCode,'-1')= isnull(ins.PrincipalCode,'-1')
      Left
      join Product               p (nolock) on s.ProductCode                  = p.ProductCode
      left join Principal pr (nolock) on p.PrincipalId  = pr.PrincipalId and pr.PrincipalCode  = s.PrincipalCode
      left join StorageUnit          su (nolock) on p.ProductId                    = su.ProductId
      left join SKU                 sku (nolock) on su.SKUId                       = sku.SKUId
                                           and isnull(s.SKUCode, sku.SKUCode) = sku.SKUCode
      left join Batch                 b (nolock) on isnull(s.Batch,'Default')      = b.Batch
      --Join Warehouse             w (nolock) on w.WarehouseId                  = b.WarehouseId
      join Warehouse             w (nolock) on w.WarehouseCode                = s.Location
                                           and w.ParentWarehouseId            = isnull(@WarehouseId, w.WarehouseId)
      left
      join HousekeepingCompare hc (nolock) on hc.WarehouseCode                = s.location
                              and hc.StorageUnitId                            = su.StorageUnitId
                              and hc.BatchId                                  = b.BatchId
                              and hc.Comparisondate                           = @GetDate
     --where not exists(select 1
     --                   from HousekeepingCompare hc
     --                  where hc.WarehouseCode  = s.Location
     --                    and hc.StorageUnitId  = su.StorageUnitId
     --                    and hc.BatchId        = b.BatchId
     --                    and hc.Comparisondate = @GetDate)
     where --s.InsertDate = @Insertdate
        hc.HousekeepingCompareId is null
       and s.Location in (select WarehouseCode from Warehouse (nolock) where isnull(UploadToHost,0) = 1)
    group by s.Location,
 s.InsertDate,
             su.StorageUnitId,
             b.BatchId
             --Isnull(p.Product,'Unk')
  end

  commit transaction
  return

  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_Take_Reload'); 
    rollback transaction
    return @Error
end
