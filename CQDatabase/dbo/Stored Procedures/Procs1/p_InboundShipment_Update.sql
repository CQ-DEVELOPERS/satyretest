﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipment_Update
  ///   Filename       : p_InboundShipment_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:19
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InboundShipment table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @ShipmentDate datetime = null,
  ///   @Remarks nvarchar(510) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipment_Update
(
 @InboundShipmentId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @ShipmentDate datetime = null,
 @Remarks nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
  if @InboundShipmentId = '-1'
    set @InboundShipmentId = null;
  
	 declare @Error int
 
  update InboundShipment
     set StatusId = isnull(@StatusId, StatusId),
         WarehouseId = isnull(@WarehouseId, WarehouseId),
         ShipmentDate = isnull(@ShipmentDate, ShipmentDate),
         Remarks = isnull(@Remarks, Remarks) 
   where InboundShipmentId = @InboundShipmentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
