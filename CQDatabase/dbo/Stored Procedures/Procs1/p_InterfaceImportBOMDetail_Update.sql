﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportBOMDetail_Update
  ///   Filename       : p_InterfaceImportBOMDetail_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:21
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportBOMDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportBOMId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @CompIndex int = null,
  ///   @CompProductCode nvarchar(60) = null,
  ///   @CompProduct nvarchar(510) = null,
  ///   @CompQuantity float = null,
  ///   @HostId nvarchar(60) = null,
  ///   @InsertDate datetime = null,
  ///   @Additional1 varchar(255) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportBOMDetail_Update
(
 @InterfaceImportBOMId int = null,
 @ForeignKey nvarchar(60) = null,
 @CompIndex int = null,
 @CompProductCode nvarchar(60) = null,
 @CompProduct nvarchar(510) = null,
 @CompQuantity float = null,
 @HostId nvarchar(60) = null,
 @InsertDate datetime = null,
 @Additional1 varchar(255) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceImportBOMDetail
     set InterfaceImportBOMId = isnull(@InterfaceImportBOMId, InterfaceImportBOMId),
         ForeignKey = isnull(@ForeignKey, ForeignKey),
         CompIndex = isnull(@CompIndex, CompIndex),
         CompProductCode = isnull(@CompProductCode, CompProductCode),
         CompProduct = isnull(@CompProduct, CompProduct),
         CompQuantity = isnull(@CompQuantity, CompQuantity),
         HostId = isnull(@HostId, HostId),
         InsertDate = isnull(@InsertDate, InsertDate),
         Additional1 = isnull(@Additional1, Additional1),
         Additional2 = isnull(@Additional2, Additional2),
         Additional3 = isnull(@Additional3, Additional3),
         Additional4 = isnull(@Additional4, Additional4),
         Additional5 = isnull(@Additional5, Additional5),
         Additional6 = isnull(@Additional6, Additional6),
         Additional7 = isnull(@Additional7, Additional7),
         Additional8 = isnull(@Additional8, Additional8),
         Additional9 = isnull(@Additional9, Additional9) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
