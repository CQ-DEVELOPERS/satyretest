﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIPHeader_Select
  ///   Filename       : p_InterfaceImportIPHeader_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:33
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportIPHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportIPHeaderId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportIPHeader.InterfaceImportIPHeaderId,
  ///   InterfaceImportIPHeader.OutboundShipmentId,
  ///   InterfaceImportIPHeader.IssueId,
  ///   InterfaceImportIPHeader.InvoiceDate,
  ///   InterfaceImportIPHeader.PrimaryKey,
  ///   InterfaceImportIPHeader.CustomerAddress1,
  ///   InterfaceImportIPHeader.CustomerAddress2,
  ///   InterfaceImportIPHeader.CustomerAddress3,
  ///   InterfaceImportIPHeader.CustomerAddress4,
  ///   InterfaceImportIPHeader.CustomerAddress5,
  ///   InterfaceImportIPHeader.CustomerAddress6,
  ///   InterfaceImportIPHeader.DeliveryAddress1,
  ///   InterfaceImportIPHeader.DeliveryAddress2,
  ///   InterfaceImportIPHeader.DeliveryAddress3,
  ///   InterfaceImportIPHeader.DeliveryAddress4,
  ///   InterfaceImportIPHeader.DeliveryAddress5,
  ///   InterfaceImportIPHeader.DeliveryAddress6,
  ///   InterfaceImportIPHeader.CustomerName,
  ///   InterfaceImportIPHeader.CustomerCode,
  ///   InterfaceImportIPHeader.OrderNumber,
  ///   InterfaceImportIPHeader.ReferenceNumber,
  ///   InterfaceImportIPHeader.ChargeTax,
  ///   InterfaceImportIPHeader.TaxReference,
  ///   InterfaceImportIPHeader.SalesCode,
  ///   InterfaceImportIPHeader.TotalNettValue,
  ///   InterfaceImportIPHeader.LogisticsDataFee,
  ///   InterfaceImportIPHeader.NettExcLogDataFee,
  ///   InterfaceImportIPHeader.Tax,
  ///   InterfaceImportIPHeader.TotalDue,
  ///   InterfaceImportIPHeader.Message1,
  ///   InterfaceImportIPHeader.Message2,
  ///   InterfaceImportIPHeader.Message3,
  ///   InterfaceImportIPHeader.Printed,
  ///   InterfaceImportIPHeader.ProcessedDate,
  ///   InterfaceImportIPHeader.RecordStatus,
  ///   InterfaceImportIPHeader.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIPHeader_Select
(
 @InterfaceImportIPHeaderId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceImportIPHeader.InterfaceImportIPHeaderId
        ,InterfaceImportIPHeader.OutboundShipmentId
        ,InterfaceImportIPHeader.IssueId
        ,InterfaceImportIPHeader.InvoiceDate
        ,InterfaceImportIPHeader.PrimaryKey
        ,InterfaceImportIPHeader.CustomerAddress1
        ,InterfaceImportIPHeader.CustomerAddress2
        ,InterfaceImportIPHeader.CustomerAddress3
        ,InterfaceImportIPHeader.CustomerAddress4
        ,InterfaceImportIPHeader.CustomerAddress5
        ,InterfaceImportIPHeader.CustomerAddress6
        ,InterfaceImportIPHeader.DeliveryAddress1
        ,InterfaceImportIPHeader.DeliveryAddress2
        ,InterfaceImportIPHeader.DeliveryAddress3
        ,InterfaceImportIPHeader.DeliveryAddress4
        ,InterfaceImportIPHeader.DeliveryAddress5
        ,InterfaceImportIPHeader.DeliveryAddress6
        ,InterfaceImportIPHeader.CustomerName
        ,InterfaceImportIPHeader.CustomerCode
        ,InterfaceImportIPHeader.OrderNumber
        ,InterfaceImportIPHeader.ReferenceNumber
        ,InterfaceImportIPHeader.ChargeTax
        ,InterfaceImportIPHeader.TaxReference
        ,InterfaceImportIPHeader.SalesCode
        ,InterfaceImportIPHeader.TotalNettValue
        ,InterfaceImportIPHeader.LogisticsDataFee
        ,InterfaceImportIPHeader.NettExcLogDataFee
        ,InterfaceImportIPHeader.Tax
        ,InterfaceImportIPHeader.TotalDue
        ,InterfaceImportIPHeader.Message1
        ,InterfaceImportIPHeader.Message2
        ,InterfaceImportIPHeader.Message3
        ,InterfaceImportIPHeader.Printed
        ,InterfaceImportIPHeader.ProcessedDate
        ,InterfaceImportIPHeader.RecordStatus
        ,InterfaceImportIPHeader.InsertDate
    from InterfaceImportIPHeader
   where isnull(InterfaceImportIPHeader.InterfaceImportIPHeaderId,'0')  = isnull(@InterfaceImportIPHeaderId, isnull(InterfaceImportIPHeader.InterfaceImportIPHeaderId,'0'))
  
end
