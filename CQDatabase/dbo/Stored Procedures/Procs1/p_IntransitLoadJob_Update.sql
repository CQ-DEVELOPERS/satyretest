﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoadJob_Update
  ///   Filename       : p_IntransitLoadJob_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012 11:58:12
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the IntransitLoadJob table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadJobId int = null,
  ///   @IntransitLoadId int = null,
  ///   @StatusId int = null,
  ///   @JobId int = null,
  ///   @LoadedById int = null,
  ///   @UnLoadedById int = null,
  ///   @CreateDate datetime = null,
  ///   @ReceivedDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoadJob_Update
(
 @IntransitLoadJobId int = null,
 @IntransitLoadId int = null,
 @StatusId int = null,
 @JobId int = null,
 @LoadedById int = null,
 @UnLoadedById int = null,
 @CreateDate datetime = null,
 @ReceivedDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  update IntransitLoadJob
     set IntransitLoadId = isnull(@IntransitLoadId, IntransitLoadId),
         StatusId = isnull(@StatusId, StatusId),
         JobId = isnull(@JobId, JobId),
         LoadedById = isnull(@LoadedById, LoadedById),
         UnLoadedById = isnull(@UnLoadedById, UnLoadedById),
         CreateDate = isnull(@CreateDate, CreateDate),
         ReceivedDate = isnull(@ReceivedDate, ReceivedDate) 
   where IntransitLoadJobId = @IntransitLoadJobId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_IntransitLoadJobHistory_Insert
         @IntransitLoadJobId = @IntransitLoadJobId,
         @IntransitLoadId = @IntransitLoadId,
         @StatusId = @StatusId,
         @JobId = @JobId,
         @LoadedById = @LoadedById,
         @UnLoadedById = @UnLoadedById,
         @CreateDate = @CreateDate,
         @ReceivedDate = @ReceivedDate,
         @CommandType = 'Update'
  
  return @Error
  
end
