﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceFileType_Select
  ///   Filename       : p_InterfaceFileType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:47:29
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceFileType table.
  /// </remarks>
  /// <param>
  ///   @InterfaceFileTypeId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceFileType.InterfaceFileTypeId,
  ///   InterfaceFileType.InterfaceFileTypeCode,
  ///   InterfaceFileType.InterfaceFileType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceFileType_Select
(
 @InterfaceFileTypeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceFileType.InterfaceFileTypeId
        ,InterfaceFileType.InterfaceFileTypeCode
        ,InterfaceFileType.InterfaceFileType
    from InterfaceFileType
   where isnull(InterfaceFileType.InterfaceFileTypeId,'0')  = isnull(@InterfaceFileTypeId, isnull(InterfaceFileType.InterfaceFileTypeId,'0'))
  order by InterfaceFileTypeCode
  
end
