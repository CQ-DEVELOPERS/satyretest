﻿create procedure [dbo].p_Interface_WebService_SM
(
 @doc2			varchar(max) output
)
--with encryption
as
begin

	declare @doc xml
	set @doc = convert(xml,@doc2)
	DECLARE @idoc int
	Declare @InsertDate datetime
	
	Set @InsertDate = Getdate()
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	sELECT * FROM InterfaceSupplier
		Insert Into 
		InterfaceSupplier(RecordStatus,
							InsertDate,
								HostId,
								SupplierCode,
								SupplierName)
		SELECT    'W',@InsertDate,*
		FROM       OPENXML (@idoc, '/root/Supplier',1)
            WITH (HostId varchar(50) 'DCLink',
				  SupplierCode  varchar(50) 'Account',
                  SupplierName varchar(50) 'Name')
		
		Update InterfaceSupplier set RecordStatus = 'N'
		Where RecordSTatus = 'W' and InsertDate = @InsertDate
		exec p_Pastel_Import_Supplier
		Set @doc2 = ''
End
