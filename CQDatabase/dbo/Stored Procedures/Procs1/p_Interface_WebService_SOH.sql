﻿CREATE procedure [dbo].[p_Interface_WebService_SOH]
(
 @doc2			varchar(max) output,
 @PrincipalCode NVARCHAR(30) = NULL
)
--with encryption
as
begin
  declare @doc xml

  set @doc = convert(xml,@doc2)

  DECLARE @idoc int,
          @InsertDate datetime

  Set @InsertDate = Getdate()

  EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
   
		Insert InterfaceImportSOH
		      (RecordStatus,
			   InsertDate,
			   Location,
			   ProductCode,
			   SKUCode,
			   Batch,
			   Quantity,
			   UnitPrice,
			   PrincipalCode)
		SELECT 'W',
			    @InsertDate,
			    Location,
			    ProductCode,
			    SKUCode,
			    Batch,
			    Replace(Quantity,',','.'),
			    UnitPrice,
			    @PrincipalCode
		  from OPENXML (@idoc, '/root/Body/Item/ItemLine',1)
            WITH (Location     nvarchar(50) 'Location',
				  ProductCode  nvarchar(50) 'ProductCode',
				  SKUCode	   nvarchar(50) 'SKUCode',
                  Batch        nvarchar(50) 'Batch',
				  Quantity     nvarchar(50) 'Quantity',
				  UnitPrice    nvarchar(50) 'UnitPrice')
				  
		update InterfaceImportSOH
		   set RecordStatus = 'N'
		 where RecordSTatus = 'W'
		   and InsertDate = @InsertDate
		
		set @doc2  = '<root><status>Success</status></root>'
End
