﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Create_Product_Mobile
  ///   Filename       : p_Housekeeping_Stock_Take_Create_Product_Mobile.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Create_Product_Mobile
(
 @warehouseId        int,
 @operatorId         int,
 @jobId              int = null output,
 @locationId         int,
 @storageUnitBatchId int = null,
 @confirmedQuantity  float = null
)

as
begin
	 set nocount on;
	 
	 declare @TableLocation as table
	 (
	  LocationId         int,
	  StorageUnitBatchId int,
	  ActualQuantity     float
	 )
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @InstructionTypeId   int,
          @InstructionId       int,
          @StatusId            int,
          @ActualQuantity      float,
          @referenceNumber     nvarchar(30),
          @InstructionTypeCode nvarchar(10),
          @StorageUnitId       int,
          @OldStorageUnitBatchId int,
          @CountQuantity         float,
          @InstructionRefId      int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @StatusId = dbo.ufn_StatusId('I','W')
  
  begin transaction
  
  if @Error <> 0
    goto error
  
  set @OldStorageUnitBatchId = @storageUnitBatchId
  
  select @StorageUnitId = StorageUnitId
    from StorageUnitBatch (nolock)
   where StorageUnitBatchId = @storageUnitBatchId
  
  if dbo.ufn_Configuration(412, @warehouseId) = 1
  begin
    insert @TableLocation
          (LocationId,
	          StorageUnitBatchId,
	          ActualQuantity)
    select l.LocationId,
	          subl.StorageUnitBatchId,
	          subl.ActualQuantity
	     from Location                    l (nolock)
	     join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
	     join Area                        a (nolock) on al.AreaId               = a.AreaId
	     join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
	     join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
	    where l.LocationId           = @locationId
	      and sub.StorageUnitBatchId = @OldStorageUnitBatchId
	      and l.StocktakeInd    = 0
	      and a.StockOnHand     = 1
  end
  else
  begin
    insert @TableLocation
          (LocationId,
	          StorageUnitBatchId,
	          ActualQuantity)
    select l.LocationId,
	          subl.StorageUnitBatchId,
	          subl.ActualQuantity
	     from Location                    l (nolock)
	     join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
	     join Area                        a (nolock) on al.AreaId               = a.AreaId
	     join StorageUnitBatchLocation subl (nolock) on l.LocationId            = subl.LocationId
	     join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
	    where l.LocationId      = @locationId
	      and sub.StorageUnitId = @StorageUnitId
	      and l.StocktakeInd    = 0
	      and a.StockOnHand     = 1
	 end
	 
	 if not exists(select 1
	                 from @TableLocation
	                where LocationId = @locationId
	                  and StorageUnitBatchId = @storageUnitBatchId)
	   insert @TableLocation
          (LocationId,
	          StorageUnitBatchId,
	          ActualQuantity)
    select l.LocationId,
	          @storageUnitBatchId,
	          0
	     from Location                    l (nolock)
	     join AreaLocation               al (nolock) on l.LocationId            = al.LocationId
	     join Area                        a (nolock) on al.AreaId               = a.AreaId
	    where l.LocationId      = @locationId
	      and l.StocktakeInd    = 0
	      and a.StockOnHand     = 1
	 
	 if @jobId is null
	 begin
    select @InstructionTypeId   = InstructionTypeId,
           @InstructionTypeCode = InstructionTypeCode
      from InstructionType
     where InstructionTypeCode = 'STE'
    
    select @referenceNumber = Location
      from Location
     where LocationId = @locationId
    
    select @referenceNumber = isnull(@referenceNumber, '') + ' / ' + Operator
      from Operator
     where OperatorId = @operatorId
    
    exec @Error = p_Housekeeping_Stock_Take_Create_Job
     @warehouseId         = @warehouseId,
     @operatorId          = @operatorId,
     @instructionTypeCode = @instructionTypeCode,
     @jobId               = @jobId output,
     @referenceNumber     = @referenceNumber
    
    if @Error <> 0
      goto error
	 end
	 else
	 begin
    select @InstructionTypeId = InstructionTypeId
      from InstructionType
     where InstructionTypeCode = 'STL'
	 end
	 
	 while exists(select 1 from @TableLocation)
	 begin
	   select top 1
	          @LocationId         = LocationId,
	          @StorageUnitBatchId = StorageUnitBatchId,
	          @ActualQuantity     = isnull(ActualQuantity, 0)
	     from @TableLocation
	   
	   if @OldStorageUnitBatchId = @StorageUnitBatchId
	     set @CountQuantity = @confirmedQuantity
	   else
	     set @CountQuantity = 0
	   
	   delete @TableLocation
	    where LocationId                   = @LocationId
	      and isnull(StorageUnitBatchId,0) = isnull(@StorageUnitBatchId,0)
	   
	   exec @Error = p_Location_Update
	    @locationId    = @LocationId,
	    @stocktakeInd  = 1
	   
    if @Error <> 0
      goto error
	   
	   select @InstructionRefId = max(i.InstructionId)
	     from Instruction i
	     join InstructionType it on i.InstructionTypeId = it.InstructionTypeId
	    where i.StorageUnitBatchId = @StorageUnitBatchId
	      and i.PickLocationId = @LocationId
	      and it.InstructionTypeCode in ('STE','STL','STA','STP')
	      and datediff(dd, i.CreateDate, @getdate) = 0
	   
    exec @Error = p_Instruction_Insert
     @InstructionId       = @InstructionId output,
     @InstructionRefId    = @InstructionRefId,
     @InstructionTypeId   = @InstructionTypeId,
     @StorageUnitBatchId  = @storageUnitBatchId,
     @WarehouseId         = @warehouseId,
     @StatusId            = @StatusId,
     @JobId               = @jobId,
     @OperatorId          = @OperatorId,
     @PickLocationId      = @LocationId,
     @Quantity            = @ActualQuantity,
     @CheckQuantity       = @ActualQuantity,
     @CreateDate          = @GetDate
    
    if @Error <> 0
      goto error
    
    -- Automatically Completed and Authorised Stock Take
    if dbo.ufn_Configuration(139, @warehouseId) = 1 and @confirmedQuantity is not null
    begin
      exec @Error = p_Housekeeping_Stock_Take_Update_Count
       @instructionId = @instructionId,
       @operatorId    = @operatorId,
       @latestCount   = @CountQuantity
      
      if @Error <> 0
        goto error
      
      exec @Error = p_Housekeeping_Stock_Take_Update
       @instructionId = @instructionId,
       @operatorId    = @operatorId,
       @statusCode    = 'A'
      
      if @Error <> 0
        goto error
      
      if not exists(select 1 from @TableLocation)
        set @JobId = 0 -- Stock Take Completed and Authorised
    end
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_Take_Create_Product_Mobile'); 
    rollback transaction
    return @Error
end
