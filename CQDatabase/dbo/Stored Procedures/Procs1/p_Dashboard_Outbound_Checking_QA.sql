﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Checking_QA
  ///   Filename       : p_Dashboard_Outbound_Checking_QA.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Checking_QA
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)

as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select Value,
           KPI
      from DashboardOutboundCheckingQA
     where WarehouseId = @WarehouseId
	 end
	 else
	 begin
	   truncate table DashboardOutboundCheckingQA
	   
	   insert DashboardOutboundCheckingQA
	         (WarehouseId,
           Value,
           KPI)
	   select j.WarehouseId,
	          count(distinct(j.JobId)),
	          25
	     from Instruction i (nolock)
	     join Job         j (nolock) on i.JobId = j.JobId
	     join Status      s (nolock) on j.StatusId = s.StatusId
	    where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      and s.StatusCode in ('QA')
	      --and i.EndDate >= CONVERT(nvarchar(10), getdate(), 120)
	   group by j.WarehouseId
	 end
end
