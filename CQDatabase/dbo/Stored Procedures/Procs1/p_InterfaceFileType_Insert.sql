﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceFileType_Insert
  ///   Filename       : p_InterfaceFileType_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:47:27
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceFileType table.
  /// </remarks>
  /// <param>
  ///   @InterfaceFileTypeId int = null output,
  ///   @InterfaceFileTypeCode nvarchar(60) = null,
  ///   @InterfaceFileType nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   InterfaceFileType.InterfaceFileTypeId,
  ///   InterfaceFileType.InterfaceFileTypeCode,
  ///   InterfaceFileType.InterfaceFileType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceFileType_Insert
(
 @InterfaceFileTypeId int = null output,
 @InterfaceFileTypeCode nvarchar(60) = null,
 @InterfaceFileType nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceFileTypeId = '-1'
    set @InterfaceFileTypeId = null;
  
  if @InterfaceFileTypeCode = '-1'
    set @InterfaceFileTypeCode = null;
  
  if @InterfaceFileType = '-1'
    set @InterfaceFileType = null;
  
	 declare @Error int
 
  insert InterfaceFileType
        (InterfaceFileTypeCode,
         InterfaceFileType)
  select @InterfaceFileTypeCode,
         @InterfaceFileType 
  
  select @Error = @@Error, @InterfaceFileTypeId = scope_identity()
  
  
  return @Error
  
end
