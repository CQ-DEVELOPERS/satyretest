﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSKU_Parameter
  ///   Filename       : p_InterfaceImportSKU_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:06
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportSKU table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceImportSKU.InterfaceImportSKUId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSKU_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceImportSKUId
        ,null as 'InterfaceImportSKU'
  union
  select
         InterfaceImportSKU.InterfaceImportSKUId
        ,InterfaceImportSKU.InterfaceImportSKUId as 'InterfaceImportSKU'
    from InterfaceImportSKU
  
end
