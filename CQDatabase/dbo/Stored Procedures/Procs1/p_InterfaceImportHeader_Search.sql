﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportHeader_Search
  ///   Filename       : p_InterfaceImportHeader_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:27
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportHeaderId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportHeader.InterfaceImportHeaderId,
  ///   InterfaceImportHeader.PrimaryKey,
  ///   InterfaceImportHeader.OrderNumber,
  ///   InterfaceImportHeader.InvoiceNumber,
  ///   InterfaceImportHeader.RecordType,
  ///   InterfaceImportHeader.RecordStatus,
  ///   InterfaceImportHeader.CompanyCode,
  ///   InterfaceImportHeader.Company,
  ///   InterfaceImportHeader.Address,
  ///   InterfaceImportHeader.FromWarehouseCode,
  ///   InterfaceImportHeader.ToWarehouseCode,
  ///   InterfaceImportHeader.Route,
  ///   InterfaceImportHeader.DeliveryNoteNumber,
  ///   InterfaceImportHeader.ContainerNumber,
  ///   InterfaceImportHeader.SealNumber,
  ///   InterfaceImportHeader.DeliveryDate,
  ///   InterfaceImportHeader.Remarks,
  ///   InterfaceImportHeader.NumberOfLines,
  ///   InterfaceImportHeader.VatPercentage,
  ///   InterfaceImportHeader.VatSummary,
  ///   InterfaceImportHeader.Total,
  ///   InterfaceImportHeader.Additional1,
  ///   InterfaceImportHeader.Additional2,
  ///   InterfaceImportHeader.Additional3,
  ///   InterfaceImportHeader.Additional4,
  ///   InterfaceImportHeader.Additional5,
  ///   InterfaceImportHeader.Additional6,
  ///   InterfaceImportHeader.Additional7,
  ///   InterfaceImportHeader.Additional8,
  ///   InterfaceImportHeader.Additional9,
  ///   InterfaceImportHeader.Additional10,
  ///   InterfaceImportHeader.ProcessedDate,
  ///   InterfaceImportHeader.InsertDate,
  ///   InterfaceImportHeader.ErrorMsg,
  ///   InterfaceImportHeader.HostStatus 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportHeader_Search
(
 @InterfaceImportHeaderId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceImportHeaderId = '-1'
    set @InterfaceImportHeaderId = null;
  
 
  select
         InterfaceImportHeader.InterfaceImportHeaderId
        ,InterfaceImportHeader.PrimaryKey
        ,InterfaceImportHeader.OrderNumber
        ,InterfaceImportHeader.InvoiceNumber
        ,InterfaceImportHeader.RecordType
        ,InterfaceImportHeader.RecordStatus
        ,InterfaceImportHeader.CompanyCode
        ,InterfaceImportHeader.Company
        ,InterfaceImportHeader.Address
        ,InterfaceImportHeader.FromWarehouseCode
        ,InterfaceImportHeader.ToWarehouseCode
        ,InterfaceImportHeader.Route
        ,InterfaceImportHeader.DeliveryNoteNumber
        ,InterfaceImportHeader.ContainerNumber
        ,InterfaceImportHeader.SealNumber
        ,InterfaceImportHeader.DeliveryDate
        ,InterfaceImportHeader.Remarks
        ,InterfaceImportHeader.NumberOfLines
        ,InterfaceImportHeader.VatPercentage
        ,InterfaceImportHeader.VatSummary
        ,InterfaceImportHeader.Total
        ,InterfaceImportHeader.Additional1
        ,InterfaceImportHeader.Additional2
        ,InterfaceImportHeader.Additional3
        ,InterfaceImportHeader.Additional4
        ,InterfaceImportHeader.Additional5
        ,InterfaceImportHeader.Additional6
        ,InterfaceImportHeader.Additional7
        ,InterfaceImportHeader.Additional8
        ,InterfaceImportHeader.Additional9
        ,InterfaceImportHeader.Additional10
        ,InterfaceImportHeader.ProcessedDate
        ,InterfaceImportHeader.InsertDate
        ,InterfaceImportHeader.ErrorMsg
        ,InterfaceImportHeader.HostStatus
    from InterfaceImportHeader
   where isnull(InterfaceImportHeader.InterfaceImportHeaderId,'0')  = isnull(@InterfaceImportHeaderId, isnull(InterfaceImportHeader.InterfaceImportHeaderId,'0'))
  
end
