﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_On_Hand_Reload
  ///   Filename       : p_Housekeeping_Stock_On_Hand_Reload.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 16 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_On_Hand_Reload
(
 @WarehouseId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  update StockOnHandCompare
     set Sent = null
   where Sent = 0
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  insert StockOnHandCompare
        (WarehouseId,
         ComparisonDate,
         StorageUnitId,
         BatchId,
         Quantity,
         WarehouseCode)
  select WarehouseId,
         @getdate,
         StorageUnitId,
         1,
         sum(ActualQuantity),
         WarehouseCode
    from viewSOH
   where WarehouseId = @WarehouseId
  group by WarehouseId,
           StorageUnitId,
           WarehouseCode
  
  insert StockOnHandCompare
        (WarehouseId,
         ComparisonDate,
         StorageUnitId,
         BatchId,
         Quantity,
         WarehouseCode)
  select @WarehouseId,
         @getdate,
         vp.StorageUnitId,
         1,
         0,
         a.WarehouseCode
    from viewProduct vp
		Left Join StorageUnitLocation l on l.StorageUnitId = vp.StorageUnitId
		Left Join AreaLocation al on al.LocationId = l.LocationId
		Left Join Area a on a.AreaId = al.AreaId
   where not exists(select 1 from viewSOH soh where vp.StorageUnitId = soh.StorageUnitId and WarehouseId = @WarehouseId)
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  update soh
     set UnitPrice = host.ActualCost
    from StockOnHandCompare soh
    join viewHOST          host on soh.StorageUnitId = host.StorageUnitId
   where soh.UnitPrice is null
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_On_Hand_Reload'); 
    rollback transaction
    return @Error
end
