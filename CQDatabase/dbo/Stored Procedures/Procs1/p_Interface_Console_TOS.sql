﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_Console_TOS
  ///   Filename       : p_Interface_Console_TOS.sql
  ///   Create By      : Karen
  ///   Date Created   : July 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Interface_Console_TOS
(
 @WarehouseId int = null,
 @location	  nvarchar(50) = null,
 @productCode nvarchar(50) = null,
 @batch		  nvarchar(50) = null,
 @status      nvarchar(50) = null,
 @message     nvarchar(max) = null,
 @recordType  nvarchar(50) = null,
 @fromDate    datetime = null,
 @toDate      datetime = null
)

as
begin

	 set nocount on;
  
  select im.InterfaceMessageId,
         im.InterfaceMessageCode,
         im.InterfaceMessage,
         im.Status,
         isnull(im.InterfaceMessageCode, 'No Response') as 'InterfaceMessageCode',
         im.CreateDate as 'MessageDate',
         (select InterfaceTableId from InterfaceTables where HeaderTable = 'InterfaceImportTakeOnStock') as 'InterfaceTableId',
         h.InterfaceImportTakeOnStockId as 'InterfaceId',
         --h.RecordType,
         h.RecordStatus,
         h.ProcessedDate,
         h.InsertDate,
         h.Location,
         h.ProductCode,
         h.Batch
  from InterfaceImportTakeOnStock   h (nolock)
  join InterfaceMessage				im (nolock) on h.InterfaceImportTakeOnStockId = im.InterfaceId
                                         and im.InterfaceTable = 'InterfaceImportTakeOnStock'
                                         
  where cast(h.ProcessedDate as DATE) in (select MAX(cast(ProcessedDate as date)) from InterfaceImportTakeOnStock)

 order by im.Status
end
--2015-11-03 06:54:02.793
 
 
