﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Division_Update
  ///   Filename       : p_Division_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Oct 2011 17:20:57
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Division table.
  /// </remarks>
  /// <param>
  ///   @DivisionId int = null,
  ///   @DivisionCode nvarchar(20) = null,
  ///   @Division nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Division_Update
(
 @DivisionId int = null,
 @DivisionCode nvarchar(20) = null,
 @Division nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
  update Division
     set DivisionCode = isnull(@DivisionCode, DivisionCode),
         Division = isnull(@Division, Division) 
   where DivisionId = @DivisionId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_DivisionHistory_Insert
         @DivisionId = @DivisionId,
         @DivisionCode = @DivisionCode,
         @Division = @Division,
         @CommandType = 'Update'
  
  return @Error
  
end
