﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLine_List
  ///   Filename       : p_InboundLine_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 08:48:44
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundLine table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InboundLine.InboundLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLine_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InboundLineId
        ,null as 'InboundLine'
  union
  select
         InboundLine.InboundLineId
        ,InboundLine.InboundLineId as 'InboundLine'
    from InboundLine
  
end
