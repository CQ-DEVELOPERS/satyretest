﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceReprocessCount_Insert
  ///   Filename       : p_InterfaceReprocessCount_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:40
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceReprocessCount table.
  /// </remarks>
  /// <param>
  ///   @InterfaceReprocessId int = null,
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @OtherKey nvarchar(60) = null,
  ///   @TableType nvarchar(60) = null 
  /// </param>
  /// <returns>
  ///   InterfaceReprocessCount.InterfaceReprocessId,
  ///   InterfaceReprocessCount.PrimaryKey,
  ///   InterfaceReprocessCount.OrderNumber,
  ///   InterfaceReprocessCount.OtherKey,
  ///   InterfaceReprocessCount.TableType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceReprocessCount_Insert
(
 @InterfaceReprocessId int = null,
 @PrimaryKey nvarchar(60) = null,
 @OrderNumber nvarchar(60) = null,
 @OtherKey nvarchar(60) = null,
 @TableType nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
  if @PrimaryKey = '-1'
    set @PrimaryKey = null;
  
	 declare @Error int
 
  insert InterfaceReprocessCount
        (InterfaceReprocessId,
         PrimaryKey,
         OrderNumber,
         OtherKey,
         TableType)
  select @InterfaceReprocessId,
         @PrimaryKey,
         @OrderNumber,
         @OtherKey,
         @TableType 
  
  select @Error = @@Error
  
  
  return @Error
  
end
