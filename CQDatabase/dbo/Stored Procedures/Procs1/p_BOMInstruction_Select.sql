﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMInstruction_Select
  ///   Filename       : p_BOMInstruction_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the BOMInstruction table.
  /// </remarks>
  /// <param>
  ///   @BOMInstructionId int = null 
  /// </param>
  /// <returns>
  ///   BOMInstruction.BOMInstructionId,
  ///   BOMInstruction.BOMHeaderId,
  ///   BOMInstruction.Quantity,
  ///   BOMInstruction.OrderNumber,
  ///   BOMInstruction.InboundDocumentId,
  ///   BOMInstruction.OutboundDocumentId,
  ///   BOMInstruction.ParentId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMInstruction_Select
(
 @BOMInstructionId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         BOMInstruction.BOMInstructionId
        ,BOMInstruction.BOMHeaderId
        ,BOMInstruction.Quantity
        ,BOMInstruction.OrderNumber
        ,BOMInstruction.InboundDocumentId
        ,BOMInstruction.OutboundDocumentId
        ,BOMInstruction.ParentId
    from BOMInstruction
   where isnull(BOMInstruction.BOMInstructionId,'0')  = isnull(@BOMInstructionId, isnull(BOMInstruction.BOMInstructionId,'0'))
  
end
