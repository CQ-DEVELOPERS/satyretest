﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Outbound_Checking_Workload
  ///   Filename       : p_Dashboard_Outbound_Checking_Workload.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Outbound_Checking_Workload
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)

as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select Legend,
           Value,
           Jobs
      from DashboardOutboundCheckingWorkload
   
  where WarehouseId = @WarehouseId
	 end
	 else
	 begin
	   truncate table DashboardOutboundCheckingWorkload
	   
	   insert DashboardOutboundCheckingWorkload
	         (WarehouseId,
           Legend,
           Value,
           Jobs)
	   select i.WarehouseId,
	          'Waiting',
	          sum(i.ConfirmedQuantity),
	          COUNT(distinct(j.JobId)) as 'Jobs'
	     from Instruction i (nolock)
	     join Job         j (nolock) on i.JobId = j.JobId
	     join Status      s (nolock) on j.StatusId = s.StatusId
	    where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      and s.StatusCode in ('CK')
	      and i.EndDate >= CONVERT(nvarchar(10), dateadd(dd, -3, getdate()), 120)
	   group by i.WarehouseId,
	            s.Status
	   
	   insert DashboardOutboundCheckingWorkload
	         (WarehouseId,
           Legend,
           Value,
           Jobs)
	   select i.WarehouseId,
	          'Checked',
	          sum(i.ConfirmedQuantity),
	          COUNT(distinct(j.JobId)) as 'Jobs'
	     from Instruction i (nolock)
	     join Job         j (nolock) on i.JobId = j.JobId
	     join Status      s (nolock) on j.StatusId = s.StatusId
	    where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      and s.StatusCode in ('CD','D','DC','C')
	      and isnull(j.CheckedDate, i.EndDate) >= CONVERT(nvarchar(10), getdate(), 120)
	   group by i.WarehouseId
	 end
end

 
