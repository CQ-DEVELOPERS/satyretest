﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMProduct_Select
  ///   Filename       : p_BOMProduct_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:14
  /// </summary>
  /// <remarks>
  ///   Selects rows from the BOMProduct table.
  /// </remarks>
  /// <param>
  ///   @BOMLineId int = null,
  ///   @LineNumber int = null,
  ///   @StorageUnitId int = null 
  /// </param>
  /// <returns>
  ///   BOMProduct.BOMLineId,
  ///   BOMProduct.LineNumber,
  ///   BOMProduct.StorageUnitId,
  ///   BOMProduct.Quantity 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMProduct_Select
(
 @BOMLineId int = null,
 @LineNumber int = null,
 @StorageUnitId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         BOMProduct.BOMLineId
        ,BOMProduct.LineNumber
        ,BOMProduct.StorageUnitId
        ,BOMProduct.Quantity
    from BOMProduct
   where isnull(BOMProduct.BOMLineId,'0')  = isnull(@BOMLineId, isnull(BOMProduct.BOMLineId,'0'))
     and isnull(BOMProduct.LineNumber,'0')  = isnull(@LineNumber, isnull(BOMProduct.LineNumber,'0'))
     and isnull(BOMProduct.StorageUnitId,'0')  = isnull(@StorageUnitId, isnull(BOMProduct.StorageUnitId,'0'))
  
end
