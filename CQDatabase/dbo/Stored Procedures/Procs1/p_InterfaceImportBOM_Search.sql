﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportBOM_Search
  ///   Filename       : p_InterfaceImportBOM_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:20
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportBOM table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportBOM.InterfaceImportBOMId,
  ///   InterfaceImportBOM.RecordStatus,
  ///   InterfaceImportBOM.ProcessedDate,
  ///   InterfaceImportBOM.ProductCode,
  ///   InterfaceImportBOM.Quantity,
  ///   InterfaceImportBOM.Product,
  ///   InterfaceImportBOM.PrimaryKey,
  ///   InterfaceImportBOM.HostID,
  ///   InterfaceImportBOM.InsertDate,
  ///   InterfaceImportBOM.Additional1,
  ///   InterfaceImportBOM.Additional2,
  ///   InterfaceImportBOM.Additional3,
  ///   InterfaceImportBOM.Additional4,
  ///   InterfaceImportBOM.Additional5,
  ///   InterfaceImportBOM.Additional6,
  ///   InterfaceImportBOM.Additional7,
  ///   InterfaceImportBOM.Additional8,
  ///   InterfaceImportBOM.Additional9 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportBOM_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportBOM.InterfaceImportBOMId
        ,InterfaceImportBOM.RecordStatus
        ,InterfaceImportBOM.ProcessedDate
        ,InterfaceImportBOM.ProductCode
        ,InterfaceImportBOM.Quantity
        ,InterfaceImportBOM.Product
        ,InterfaceImportBOM.PrimaryKey
        ,InterfaceImportBOM.HostID
        ,InterfaceImportBOM.InsertDate
        ,InterfaceImportBOM.Additional1
        ,InterfaceImportBOM.Additional2
        ,InterfaceImportBOM.Additional3
        ,InterfaceImportBOM.Additional4
        ,InterfaceImportBOM.Additional5
        ,InterfaceImportBOM.Additional6
        ,InterfaceImportBOM.Additional7
        ,InterfaceImportBOM.Additional8
        ,InterfaceImportBOM.Additional9
    from InterfaceImportBOM
  
end
