﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COATest_Select
  ///   Filename       : p_COATest_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:09
  /// </summary>
  /// <remarks>
  ///   Selects rows from the COATest table.
  /// </remarks>
  /// <param>
  ///   @COATestId int = null 
  /// </param>
  /// <returns>
  ///   COATest.COATestId,
  ///   COATest.COAId,
  ///   COATest.TestId,
  ///   COATest.ResultId,
  ///   COATest.MethodId,
  ///   COATest.ResultCode,
  ///   COATest.Result,
  ///   COATest.Pass,
  ///   COATest.StartRange,
  ///   COATest.EndRange,
  ///   COATest.Value 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COATest_Select
(
 @COATestId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         COATest.COATestId
        ,COATest.COAId
        ,COATest.TestId
        ,COATest.ResultId
        ,COATest.MethodId
        ,COATest.ResultCode
        ,COATest.Result
        ,COATest.Pass
        ,COATest.StartRange
        ,COATest.EndRange
        ,COATest.Value
    from COATest
   where isnull(COATest.COATestId,'0')  = isnull(@COATestId, isnull(COATest.COATestId,'0'))
  order by ResultCode
  
end
