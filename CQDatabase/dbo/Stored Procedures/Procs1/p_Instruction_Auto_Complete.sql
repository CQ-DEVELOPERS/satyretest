﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Auto_Complete
  ///   Filename       : p_Instruction_Auto_Complete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Jun 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Auto_Complete
(
 @InstructionId int = null,
 @NoStock       bit = 0
)

as
begin
	 set nocount on;
	 
	 declare @TableResult as table
	 (
	  InstructionId      int,
	  OperatorId         int,
	  StoreInstructionId int
	 )
  
  declare @Error              int,
          @Errormsg           varchar(500) = 'Error executing p_Instruction_Auto_Complete',
          @GetDate            datetime,
          @Transction         bit = 1,
          @UpdInstructionId   int,
          @JobId              int,
          @OperatorId         int,
          @StoreLocationId    int,
          @InstructionTypeId  int,
          @PalletId           int,
          @StoreArea          nvarchar(50),
          @WarehouseId        int,
          @StoreInstructionId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @WarehouseId       = WarehouseId,
         @StoreLocationId   = StoreLocationId,
         @JobId             = JobId,
         @InstructionTypeId = InstructionTypeId,
         @PalletId          = PalletId
    from Instruction (nolock)
   where InstructionId = @InstructionId
  
  select @StoreArea = a.Area
    from AreaLocation al (nolock)
    join Area          a (nolock) on al.AreaId = a.AreaId
   where al.LocationId = @StoreLocationId
  
  insert @TableResult
        (InstructionId,
         OperatorId,
         StoreInstructionId)
  select i.InstructionId,
         j.OperatorId,
         ref.InstructionId
    from Instruction i (nolock)
    left
    join Instruction ref (nolock) on i.InstructionId = ref.InstructionRefId
    join Job           j (nolock) on i.JobId = j.JobId
    join Status        s (nolock) on i.StatusId = s.StatusId
                                 and s.StatusCode = 'W'
   where j.JobId             = @JobId
     and i.InstructionTypeId = @InstructionTypeId
     and (i.AutoComplete      = 1)-- or i.PalletId = @PalletId)
  
  if @@trancount > 0
    set @Transction = 0
  
  if @Transction = 1
    begin transaction
  
  update i
     set StatusId = dbo.ufn_StatusId('I','S') 
    from @TableResult tr
    join Instruction i on tr.InstructionId = i.InstructionId
  
  select @Error = @@error
  
  if @Error <> 0
    goto error
  
  while exists(select top 1 1 from @TableResult)
  begin
    select top 1 
           @UpdInstructionId   = InstructionId,
           @OperatorId         = OperatorId,
           @StoreInstructionId = StoreInstructionId
      from @TableResult
    
    --select WarehouseId,
    --       InstructionTypeCode,
    --       JobId,
    --       JobCode,
    --       InstructionCode,
    --       InstructionId,
    --       InstructionRefId,
    --       PalletId,
    --       ReferenceNumber,
    --       Operator,
    --       StorageUnitBatchId,
    --       ProductCode,
    --       Product,
    --       SKUCode,
    --       SKU,
    --       Batch,
    --       PickLocation,
    --       Picked,
    --       StoreLocation,
    --       Stored,
    --       Quantity,
    --       ConfirmedQuantity,
    --       CreateDate,
    --       StartDate,
    --       EndDate
    --  from viewInstruction
    -- where InstructionId = @UpdInstructionId
    
    delete @TableResult
     where InstructionId = @UpdInstructionId
    
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @UpdInstructionId
    
    if @Error <> 0
      goto error
   
    update Instruction
       set StoreLocationId = @StoreLocationId
     where InstructionId = @UpdInstructionId
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
    
    if @NoStock = 1
    begin
      update Instruction
         set ConfirmedQuantity = 0
       where InstructionId = @UpdInstructionId
      
      select @Error = @@error
      
      if @Error <> 0
        goto error
    end
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @UpdInstructionId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Instruction_Started
     @InstructionId = @UpdInstructionId,
     @OperatorId    = @OperatorId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Instruction_Finished
     @InstructionId = @UpdInstructionId,
     @OperatorId    = @OperatorId
    
    if @Error <> 0
      goto error
    
    -- Update Store Instruction (The second leg with correct PickLocation)
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @StoreInstructionId
    
    if @Error <> 0
      goto error
   
    update Instruction
       set PickLocationId = @StoreLocationId
     where InstructionId = @StoreInstructionId
    
    select @Error = @@error
    
    if @Error <> 0
      goto error
    
    exec @Error = p_StorageUnitBatchLocation_Reserve
     @InstructionId = @StoreInstructionId
    
    if @Error <> 0
      goto error
  end
  
  -- If there are any other products already in this location, generate stock take.
  if @StoreArea = 'WavePick'
  begin
    if exists(select top 1 1
                from StorageUnitBatchLocation subl
                left
                join Instruction i (nolock) on subl.StorageUnitBatchId = i.StorageUnitBatchId
                                           and subl.Locationid         = i.StoreLocationId
               where subl.LocationId = @StoreLocationId
                 and i.JobId = @JobId
                 and i.InstructionId is null)
    begin
      exec @Error = p_Housekeeping_Stock_Take_Create_Product
       @warehouseId        = @WarehouseId,
       @operatorId         = @WarehouseId,
       @locationId         = @StoreLocationId
      
      if @Error <> 0
        goto error
    end
  end
  
  if @Transction = 1
    commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
