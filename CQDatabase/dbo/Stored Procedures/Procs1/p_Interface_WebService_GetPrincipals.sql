﻿/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_GetPrincipals
  ///   Filename       : p_Interface_WebService_GetPrincipals.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure [dbo].[p_Interface_WebService_GetPrincipals]
(
 @XMLBody nvarchar(max) output
)
as
begin
  declare @DatabaseName sysname,
          @Command      nvarchar(1000)
  
  create table #TableResult
  (
   [DatabaseName]                    nvarchar(256)    null    ,
   [PrincipalCode]                   nvarchar(60)     null    ,
   [UserName]                        nvarchar(100)    null    ,
   [Key]                             nvarchar(100)    null    
  )
  
  insert #TableResult
        ([DatabaseName],
         [PrincipalCode],
         [UserName],
         [Key])
  SELECT il.DatabaseName
        ,il.PrincipalCode
        ,il.InterfaceUsername
        ,il.InterfacePassword
    FROM [InterfaceLogin] il
   where il.IsActive = 1
  
  declare db_cursor cursor for
   select	distinct DatabaseName
  	  from	#TableResult
	  order by	DatabaseName

  open db_cursor

  fetch db_cursor into @DatabaseName

  while (@@fetch_status = 0)
  begin
    select @Command = 'delete tr
      from #TableResult                      tr
      join [' + @DatabaseName + '].dbo.Principal             p on tr.PrincipalCode = p.PrincipalCode
      left
					 join [' + @DatabaseName + '].dbo.InterfaceMappingFile mf on p.PrincipalId = mf.PrincipalId
					where mf.InterfaceMappingFileId is null'
				execute (@Command)
				
				fetch db_cursor into @DatabaseName
  end
  
  close db_cursor
  deallocate db_cursor
  
  SELECT @XMLBody = '<?xml version="1.0" encoding="utf-16"?>' +
	  (SELECT 
		  (SELECT 
			  (SELECT [DatabaseName] as 'Database',
             [PrincipalCode],
             [UserName],
             [Key]
				  FROM #TableResult
			  FOR XML PATH('Principal'), TYPE)
		   FOR XML PATH('Principals'), TYPE)
	  FOR XML PATH('root'))
	  
  drop table #TableResult
End

