﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Get_Value
  ///   Filename       : p_Configuration_Get_Value.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Jan 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Get_Value
(
 @ConfigurationId int,
 @WarehouseId int = 1
)

as
begin
	 set nocount on;
  
  declare @Value int
  
  select @Value = convert(int, [Value])
    from Configuration (nolock)
   where ConfigurationId = @ConfigurationId
     and WarehouseId = @warehouseId
  select @Value as 'Value'
end
