﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceBOM_Update
  ///   Filename       : p_InterfaceBOM_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:45
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceBOM table.
  /// </remarks>
  /// <param>
  ///   @InterfaceBOMId int = null,
  ///   @ParentProductCode nvarchar(100) = null,
  ///   @OrderDescription nvarchar(510) = null,
  ///   @CreateDate nvarchar(100) = null,
  ///   @PlannedDate nvarchar(100) = null,
  ///   @Revision nvarchar(100) = null,
  ///   @BuildQuantity float = null,
  ///   @Units nvarchar(100) = null,
  ///   @Location nvarchar(100) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceBOM_Update
(
 @InterfaceBOMId int = null,
 @ParentProductCode nvarchar(100) = null,
 @OrderDescription nvarchar(510) = null,
 @CreateDate nvarchar(100) = null,
 @PlannedDate nvarchar(100) = null,
 @Revision nvarchar(100) = null,
 @BuildQuantity float = null,
 @Units nvarchar(100) = null,
 @Location nvarchar(100) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceBOMId = '-1'
    set @InterfaceBOMId = null;
  
	 declare @Error int
 
  update InterfaceBOM
     set ParentProductCode = isnull(@ParentProductCode, ParentProductCode),
         OrderDescription = isnull(@OrderDescription, OrderDescription),
         CreateDate = isnull(@CreateDate, CreateDate),
         PlannedDate = isnull(@PlannedDate, PlannedDate),
         Revision = isnull(@Revision, Revision),
         BuildQuantity = isnull(@BuildQuantity, BuildQuantity),
         Units = isnull(@Units, Units),
         Location = isnull(@Location, Location),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         RecordStatus = isnull(@RecordStatus, RecordStatus) 
   where InterfaceBOMId = @InterfaceBOMId
  
  select @Error = @@Error
  
  
  return @Error
  
end
