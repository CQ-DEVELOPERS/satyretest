﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDocumentType_Update
  ///   Filename       : p_InterfaceDocumentType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Mar 2014 16:26:00
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceDocumentType table.
  /// </remarks>
  /// <param>
  ///   @InterfaceDocumentTypeId int = null,
  ///   @InterfaceDocumentTypeCode nvarchar(60) = null,
  ///   @InterfaceDocumentType nvarchar(100) = null,
  ///   @RecordType nvarchar(60) = null,
  ///   @InterfaceType nvarchar(60) = null,
  ///   @InDirectory bit = null,
  ///   @OutDirectory bit = null,
  ///   @SubDirectory nvarchar(max) = null,
  ///   @FilePrefix nvarchar(60) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDocumentType_Update
(
 @InterfaceDocumentTypeId int = null,
 @InterfaceDocumentTypeCode nvarchar(60) = null,
 @InterfaceDocumentType nvarchar(100) = null,
 @RecordType nvarchar(60) = null,
 @InterfaceType nvarchar(60) = null,
 @InDirectory bit = null,
 @OutDirectory bit = null,
 @SubDirectory nvarchar(max) = null,
 @FilePrefix nvarchar(60) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
  if @InterfaceDocumentTypeCode = '-1'
    set @InterfaceDocumentTypeCode = null;
  
  if @InterfaceDocumentType = '-1'
    set @InterfaceDocumentType = null;
  
	 declare @Error int
 
  update InterfaceDocumentType
     set InterfaceDocumentTypeCode = isnull(@InterfaceDocumentTypeCode, InterfaceDocumentTypeCode),
         InterfaceDocumentType = isnull(@InterfaceDocumentType, InterfaceDocumentType),
         RecordType = isnull(@RecordType, RecordType),
         InterfaceType = isnull(@InterfaceType, InterfaceType),
         InDirectory = isnull(@InDirectory, InDirectory),
         OutDirectory = isnull(@OutDirectory, OutDirectory),
         SubDirectory = isnull(@SubDirectory, SubDirectory),
         FilePrefix = isnull(@FilePrefix, FilePrefix) 
   where InterfaceDocumentTypeId = @InterfaceDocumentTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
