﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompanyHistory_Insert
  ///   Filename       : p_ExternalCompanyHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:44:45
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ExternalCompanyHistory table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyId int = null,
  ///   @ExternalCompanyTypeId int = null,
  ///   @RouteId int = null,
  ///   @ExternalCompany nvarchar(510) = null,
  ///   @ExternalCompanyCode nvarchar(60) = null,
  ///   @Rating int = null,
  ///   @RedeliveryIndicator bit = null,
  ///   @QualityAssuranceIndicator bit = null,
  ///   @ContainerTypeId int = null,
  ///   @Backorder bit = null,
  ///   @HostId nvarchar(60) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @OrderLineSequence bit = null,
  ///   @DropSequence int = null,
  ///   @AutoInvoice bit = null,
  ///   @OneProductPerPallet bit = null,
  ///   @PrincipalId int = null,
  ///   @ProcessId int = null,
  ///   @TrustedDelivery bit = null,
  ///   @OutboundSLAHours int = null,
  ///   @PricingCategoryId int = null,
  ///   @LabelingCategoryId int = null 
  /// </param>
  /// <returns>
  ///   ExternalCompanyHistory.ExternalCompanyId,
  ///   ExternalCompanyHistory.ExternalCompanyTypeId,
  ///   ExternalCompanyHistory.RouteId,
  ///   ExternalCompanyHistory.ExternalCompany,
  ///   ExternalCompanyHistory.ExternalCompanyCode,
  ///   ExternalCompanyHistory.Rating,
  ///   ExternalCompanyHistory.RedeliveryIndicator,
  ///   ExternalCompanyHistory.QualityAssuranceIndicator,
  ///   ExternalCompanyHistory.ContainerTypeId,
  ///   ExternalCompanyHistory.Backorder,
  ///   ExternalCompanyHistory.HostId,
  ///   ExternalCompanyHistory.CommandType,
  ///   ExternalCompanyHistory.InsertDate,
  ///   ExternalCompanyHistory.OrderLineSequence,
  ///   ExternalCompanyHistory.DropSequence,
  ///   ExternalCompanyHistory.AutoInvoice,
  ///   ExternalCompanyHistory.OneProductPerPallet,
  ///   ExternalCompanyHistory.PrincipalId,
  ///   ExternalCompanyHistory.ProcessId,
  ///   ExternalCompanyHistory.TrustedDelivery,
  ///   ExternalCompanyHistory.OutboundSLAHours,
  ///   ExternalCompanyHistory.PricingCategoryId,
  ///   ExternalCompanyHistory.LabelingCategoryId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompanyHistory_Insert
(
 @ExternalCompanyId int = null,
 @ExternalCompanyTypeId int = null,
 @RouteId int = null,
 @ExternalCompany nvarchar(510) = null,
 @ExternalCompanyCode nvarchar(60) = null,
 @Rating int = null,
 @RedeliveryIndicator bit = null,
 @QualityAssuranceIndicator bit = null,
 @ContainerTypeId int = null,
 @Backorder bit = null,
 @HostId nvarchar(60) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @OrderLineSequence bit = null,
 @DropSequence int = null,
 @AutoInvoice bit = null,
 @OneProductPerPallet bit = null,
 @PrincipalId int = null,
 @ProcessId int = null,
 @TrustedDelivery bit = null,
 @OutboundSLAHours int = null,
 @PricingCategoryId int = null,
 @LabelingCategoryId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ExternalCompanyHistory
        (ExternalCompanyId,
         ExternalCompanyTypeId,
         RouteId,
         ExternalCompany,
         ExternalCompanyCode,
         Rating,
         RedeliveryIndicator,
         QualityAssuranceIndicator,
         ContainerTypeId,
         Backorder,
         HostId,
         CommandType,
         InsertDate,
         OrderLineSequence,
         DropSequence,
         AutoInvoice,
         OneProductPerPallet,
         PrincipalId,
         ProcessId,
         TrustedDelivery,
         OutboundSLAHours,
         PricingCategoryId,
         LabelingCategoryId)
  select @ExternalCompanyId,
         @ExternalCompanyTypeId,
         @RouteId,
         @ExternalCompany,
         @ExternalCompanyCode,
         @Rating,
         @RedeliveryIndicator,
         @QualityAssuranceIndicator,
         @ContainerTypeId,
         @Backorder,
         @HostId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @OrderLineSequence,
         @DropSequence,
         @AutoInvoice,
         @OneProductPerPallet,
         @PrincipalId,
         @ProcessId,
         @TrustedDelivery,
         @OutboundSLAHours,
         @PricingCategoryId,
         @LabelingCategoryId 
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
