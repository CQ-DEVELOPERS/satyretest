﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundShipmentHistory_Insert
  ///   Filename       : p_InboundShipmentHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:18
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundShipmentHistory table.
  /// </remarks>
  /// <param>
  ///   @InboundShipmentId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @ShipmentDate datetime = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   InboundShipmentHistory.InboundShipmentId,
  ///   InboundShipmentHistory.StatusId,
  ///   InboundShipmentHistory.WarehouseId,
  ///   InboundShipmentHistory.ShipmentDate,
  ///   InboundShipmentHistory.Remarks,
  ///   InboundShipmentHistory.CommandType,
  ///   InboundShipmentHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundShipmentHistory_Insert
(
 @InboundShipmentId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @ShipmentDate datetime = null,
 @Remarks nvarchar(510) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InboundShipmentHistory
        (InboundShipmentId,
         StatusId,
         WarehouseId,
         ShipmentDate,
         Remarks,
         CommandType,
         InsertDate)
  select @InboundShipmentId,
         @StatusId,
         @WarehouseId,
         @ShipmentDate,
         @Remarks,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
