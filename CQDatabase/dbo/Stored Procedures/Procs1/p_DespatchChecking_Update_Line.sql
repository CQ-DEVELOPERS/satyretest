﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DespatchChecking_Update_Line
  ///   Filename       : p_DespatchChecking_Update_Line.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DespatchChecking_Update_Line
(
 @jobId int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          nvarchar(500),
          @GetDate           datetime,
          @StatusCode        nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  select @StatusCode = s.StatusCode
    from Job    j
    join Status s on j.StatusId = s.StatusId
   where j.JobId = @jobId
  
  if @StatusCode = 'D'
  begin
    exec @Error = p_Status_Rollup
     @jobId = @jobId
    
    if @Error <> 0
      goto error
  end
  else
  begin
    set @Error = -1
    goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_DespatchChecking_Update_Line'); 
    rollback transaction
    return @Error
end
