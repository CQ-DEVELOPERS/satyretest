﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Indicator_Insert
  ///   Filename       : p_Indicator_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:24
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Indicator table.
  /// </remarks>
  /// <param>
  ///   @IndicatorId int = null output,
  ///   @Header nvarchar(100) = null,
  ///   @Indicator nvarchar(100) = null,
  ///   @IndicatorValue sql_variant = null,
  ///   @IndicatorDisplay sql_variant = null,
  ///   @ThresholdGreen int = null,
  ///   @ThresholdYellow int = null,
  ///   @ThresholdRed int = null,
  ///   @ModifiedDate datetime = null 
  /// </param>
  /// <returns>
  ///   Indicator.IndicatorId,
  ///   Indicator.Header,
  ///   Indicator.Indicator,
  ///   Indicator.IndicatorValue,
  ///   Indicator.IndicatorDisplay,
  ///   Indicator.ThresholdGreen,
  ///   Indicator.ThresholdYellow,
  ///   Indicator.ThresholdRed,
  ///   Indicator.ModifiedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Indicator_Insert
(
 @IndicatorId int = null output,
 @Header nvarchar(100) = null,
 @Indicator nvarchar(100) = null,
 @IndicatorValue sql_variant = null,
 @IndicatorDisplay sql_variant = null,
 @ThresholdGreen int = null,
 @ThresholdYellow int = null,
 @ThresholdRed int = null,
 @ModifiedDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @IndicatorId = '-1'
    set @IndicatorId = null;
  
  if @Indicator = '-1'
    set @Indicator = null;
  
	 declare @Error int
 
  insert Indicator
        (Header,
         Indicator,
         IndicatorValue,
         IndicatorDisplay,
         ThresholdGreen,
         ThresholdYellow,
         ThresholdRed,
         ModifiedDate)
  select @Header,
         @Indicator,
         @IndicatorValue,
         @IndicatorDisplay,
         @ThresholdGreen,
         @ThresholdYellow,
         @ThresholdRed,
         @ModifiedDate 
  
  select @Error = @@Error, @IndicatorId = scope_identity()
  
  
  return @Error
  
end
