﻿create procedure [dbo].[p_FamousBrands_Export_PO]
(
 @FileName varchar(30) = null output
)
as
begin
  set nocount on
  
  declare @TableResult as table
  (
   Data varchar(500)
  )
  
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @InterfaceExportPOHeaderId int,
          @OrderNumber          varchar(30),
          @DeliveryNoteNumber   varchar(30),
          @ProcessedDate        varchar(20),
          @FromWarehouseCode    varchar(10),
          @ForeignKey           varchar(30),
          @LineNumber           varchar(10),
          @ProductCode          varchar(30),
          @Batch                varchar(50),
          @Quantity             varchar(10),
          @Remarks              varchar(255),
          -- Internal
          @fetch_status_header  int,
          @fetch_status_detail  int,
          @print_string         varchar(255)
  
  select @GetDate = dbo.ufn_Getdate()
  
  declare extract_header_cursor cursor for
  select top 1
         h.InterfaceExportPOHeaderId,
         h.PrimaryKey,
         isnull(h.DeliveryNoteNumber, ''),
         convert(varchar(20), isnull(h.ProcessedDate, @Getdate), 120),
         isnull(h.FromWarehouseCode, ''),
         isnull(h.Remarks,'')
    from InterfaceExportPOHeader h
    join InterfaceExportPODetail d on h.InterfaceExportPOHeaderId = d.InterfaceExportPOHeaderId
   where h.RecordStatus = 'N'
     and h.RecordType = 'PUR'
   order by h.InterfaceExportPOHeaderId
  
  open extract_header_cursor
  
  fetch extract_header_cursor into @InterfaceExportPOHeaderId,
                                   @OrderNumber,
                                   @DeliveryNoteNumber,
                                   @ProcessedDate,
                                   @FromWarehouseCode,
                                   @Remarks
  
  select @fetch_status_header = @@fetch_status
  
  while (@fetch_status_header = 0)
  begin
    update InterfaceExportPOHeader
       set RecordStatus = 'Y',
           ProcessedDate = @GetDate
     where InterfaceExportPOHeaderId = @InterfaceExportPOHeaderId                                   
    
    set @FileName = @OrderNumber + '(' + convert(varchar(10), @InterfaceExportPOHeaderId) + ')'
    
    insert @TableResult (Data) select '<?xml version="1.0" encoding="Windows-1252" standalone="yes" ?>'
    insert @TableResult (Data) select '<root>'
    insert @TableResult (Data) select '  <PurchaseOrder>'
    insert @TableResult (Data) select '    <OrderNumber>' + @OrderNumber + '</OrderNumber>'
    insert @TableResult (Data) select '    <DeliveryNoteNumber><![CDATA[' + @DeliveryNoteNumber + ']]></DeliveryNoteNumber>'
    insert @TableResult (Data) select '    <ReceivedDate>' + @ProcessedDate + '</ReceivedDate>'
    insert @TableResult (Data) select '    <Location>' + @FromWarehouseCode + '</Location>'
    insert @TableResult (Data) select '    <Remarks>' + @Remarks + '</Remarks>'
    
    declare extract_detail_cursor cursor for
    select ForeignKey,
           convert(varchar(10), LineNumber),
           isnull(ProductCode, ''),
           isnull(Batch, ''),
           convert(varchar(10), Quantity)
      from InterfaceExportPODetail
     where InterfaceExportPOHeaderId = @InterfaceExportPOHeaderId
     order by LineNumber
      
    open extract_detail_cursor
    
    fetch extract_detail_cursor into @ForeignKey,
                                     @LineNumber,
                                     @ProductCode,
                                     @Batch,
                                     @Quantity
    select @fetch_status_detail = @@fetch_status
    
    while (@fetch_status_detail = 0)
    begin
      insert @TableResult (Data) select '    <PurchaseOrderLine>'
      select @print_string = '      <OrderNumber>' + @OrderNumber + '</OrderNumber>'
      insert @TableResult (Data) select @print_string
      select @print_string = '      <LineNumber>' + @LineNumber + '</LineNumber>'
      insert @TableResult (Data) select @print_string
      select @print_string = '      <ProductCode>' + @ProductCode + '</ProductCode>'
      insert @TableResult (Data) select @print_string
      select @print_string = '      <Batch>' + @Batch + '</Batch>'
      insert @TableResult (Data) select @print_string
      select @print_string = '      <Quantity>' + @Quantity + '</Quantity>'
      insert @TableResult (Data) select @print_string
      insert @TableResult (Data) select '    </PurchaseOrderLine>'
      
      fetch extract_detail_cursor into @ForeignKey,
                                       @LineNumber,
                                       @ProductCode,
                                       @Batch,
                                       @Quantity
      
      select @fetch_status_detail = @@fetch_status
    end
    
    close extract_detail_cursor
    deallocate extract_detail_cursor
    
    insert @TableResult (Data) select '  </PurchaseOrder>'
    insert @TableResult (Data) select '</root>'
    
    fetch extract_header_cursor into @InterfaceExportPOHeaderId,
                                     @OrderNumber,
                                     @DeliveryNoteNumber,
                                     @ProcessedDate,
                                     @FromWarehouseCode,
                                     @Remarks
    
    select @fetch_status_header = @@fetch_status
  end
  
  close extract_header_cursor
  deallocate extract_header_cursor
  
  select Data
    from @TableResult
end
