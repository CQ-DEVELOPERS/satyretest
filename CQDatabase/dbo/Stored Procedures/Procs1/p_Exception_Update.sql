﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_Update
  ///   Filename       : p_Exception_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:05
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the Exception table.
  /// </remarks>
  /// <param>
  ///   @ExceptionId int = null,
  ///   @ReceiptLineId int = null,
  ///   @InstructionId int = null,
  ///   @IssueLineId int = null,
  ///   @ReasonId int = null,
  ///   @Exception nvarchar(510) = null,
  ///   @ExceptionCode nvarchar(20) = null,
  ///   @CreateDate datetime = null,
  ///   @OperatorId int = null,
  ///   @ExceptionDate datetime = null,
  ///   @JobId int = null,
  ///   @Detail sql_variant = null,
  ///   @Quantity float = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_Update
(
 @ExceptionId int = null,
 @ReceiptLineId int = null,
 @InstructionId int = null,
 @IssueLineId int = null,
 @ReasonId int = null,
 @Exception nvarchar(510) = null,
 @ExceptionCode nvarchar(20) = null,
 @CreateDate datetime = null,
 @OperatorId int = null,
 @ExceptionDate datetime = null,
 @JobId int = null,
 @Detail sql_variant = null,
 @Quantity float = null 
)

as
begin
	 set nocount on;
  
  if @ExceptionId = '-1'
    set @ExceptionId = null;
  
  if @Exception = '-1'
    set @Exception = null;
  
  if @ExceptionCode = '-1'
    set @ExceptionCode = null;
  
	 declare @Error int
 
  update Exception
     set ReceiptLineId = isnull(@ReceiptLineId, ReceiptLineId),
         InstructionId = isnull(@InstructionId, InstructionId),
         IssueLineId = isnull(@IssueLineId, IssueLineId),
         ReasonId = isnull(@ReasonId, ReasonId),
         Exception = isnull(@Exception, Exception),
         ExceptionCode = isnull(@ExceptionCode, ExceptionCode),
         CreateDate = isnull(@CreateDate, CreateDate),
         OperatorId = isnull(@OperatorId, OperatorId),
         ExceptionDate = isnull(@ExceptionDate, ExceptionDate),
         JobId = isnull(@JobId, JobId),
         Detail = isnull(@Detail, Detail),
         Quantity = isnull(@Quantity, Quantity) 
   where ExceptionId = @ExceptionId
  
  select @Error = @@Error
  
  
  return @Error
  
end
