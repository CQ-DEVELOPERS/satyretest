﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COA_Select
  ///   Filename       : p_COA_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 04 Oct 2012 14:56:51
  /// </summary>
  /// <remarks>
  ///   Selects rows from the COA table.
  /// </remarks>
  /// <param>
  ///   @COAId int = null 
  /// </param>
  /// <returns>
  ///   COA.COAId,
  ///   COA.COACode,
  ///   COA.COA,
  ///   COA.StorageUnitBatchId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COA_Select
(
 @COAId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         COA.COAId
        ,COA.COACode
        ,COA.COA
        ,COA.StorageUnitBatchId
    from COA
   where isnull(COA.COAId,'0')  = isnull(@COAId, isnull(COA.COAId,'0'))
  order by COACode
  
end
