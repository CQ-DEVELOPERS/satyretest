﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceBOMLine_Search
  ///   Filename       : p_InterfaceBOMLine_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:48
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceBOMLine table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceBOMLine.InterfaceBOMId,
  ///   InterfaceBOMLine.ParentProductCode,
  ///   InterfaceBOMLine.LineNumber,
  ///   InterfaceBOMLine.ProductCode,
  ///   InterfaceBOMLine.ProductDescription,
  ///   InterfaceBOMLine.Quantity,
  ///   InterfaceBOMLine.Units 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceBOMLine_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceBOMLine.InterfaceBOMId
        ,InterfaceBOMLine.ParentProductCode
        ,InterfaceBOMLine.LineNumber
        ,InterfaceBOMLine.ProductCode
        ,InterfaceBOMLine.ProductDescription
        ,InterfaceBOMLine.Quantity
        ,InterfaceBOMLine.Units
    from InterfaceBOMLine
  
end
