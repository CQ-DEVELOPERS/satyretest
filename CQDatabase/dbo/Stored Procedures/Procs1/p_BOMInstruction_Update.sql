﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMInstruction_Update
  ///   Filename       : p_BOMInstruction_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:06
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the BOMInstruction table.
  /// </remarks>
  /// <param>
  ///   @BOMInstructionId int = null,
  ///   @BOMHeaderId int = null,
  ///   @Quantity float = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @InboundDocumentId int = null,
  ///   @OutboundDocumentId int = null,
  ///   @ParentId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMInstruction_Update
(
 @BOMInstructionId int = null,
 @BOMHeaderId int = null,
 @Quantity float = null,
 @OrderNumber nvarchar(60) = null,
 @InboundDocumentId int = null,
 @OutboundDocumentId int = null,
 @ParentId int = null,
 @Remarks nvarchar(max) = null
)

as
begin
	 set nocount on;
  
  if @BOMInstructionId = '-1'
    set @BOMInstructionId = null;
  
	 declare @Error int
  
  update bil
     set Quantity = bil.Quantity / bi.Quantity
    from BOMInstruction bi
    join BOMInstructionLine bil on bi.BOMInstructionId = bil.BOMInstructionId
   where bi.BOMInstructionId = @BOMInstructionId
  
  select @Error = @@Error
  
  if @Error != 0
    goto Error
 
  update BOMInstruction
     set BOMHeaderId = isnull(@BOMHeaderId, BOMHeaderId),
         Quantity = isnull(@Quantity, Quantity),
         OrderNumber = isnull(@OrderNumber, OrderNumber),
         InboundDocumentId = isnull(@InboundDocumentId, InboundDocumentId),
         OutboundDocumentId = isnull(@OutboundDocumentId, OutboundDocumentId),
         ParentId = isnull(@ParentId, ParentId) 
   where BOMInstructionId = @BOMInstructionId
  
  select @Error = @@Error
  
  if @Error != 0
    goto Error
 
  update bh
     set Remarks = @Remarks
      FROM	BOMInstruction   bi (nolock)
      join BOMHeader        bh (nolock) on bi.BOMHeaderId = bh.BOMHeaderId
   where BOMInstructionId = @BOMInstructionId
  
  select @Error = @@Error
  
  if @Error != 0
    goto Error
  
  update bil
     set Quantity = bil.Quantity * bi.Quantity
    from BOMInstruction bi
    join BOMInstructionLine bil on bi.BOMInstructionId = bil.BOMInstructionId
   where bi.BOMInstructionId = @BOMInstructionId
  
  select @Error = @@Error
  
  if @Error != 0
    goto Error
  
  Error:
  return @Error
  
end

