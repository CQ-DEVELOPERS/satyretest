﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ChildProduct_Update
  ///   Filename       : p_ChildProduct_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:20
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ChildProduct table.
  /// </remarks>
  /// <param>
  ///   @ChildProductId int = null,
  ///   @ChildStorageUnitBatchId int = null,
  ///   @ParentStorageUnitBatchId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ChildProduct_Update
(
 @ChildProductId int = null,
 @ChildStorageUnitBatchId int = null,
 @ParentStorageUnitBatchId int = null 
)

as
begin
	 set nocount on;
  
  if @ChildProductId = '-1'
    set @ChildProductId = null;
  
	 declare @Error int
 
  update ChildProduct
     set ChildStorageUnitBatchId = isnull(@ChildStorageUnitBatchId, ChildStorageUnitBatchId),
         ParentStorageUnitBatchId = isnull(@ParentStorageUnitBatchId, ParentStorageUnitBatchId) 
   where ChildProductId = @ChildProductId
  
  select @Error = @@Error
  
  
  return @Error
  
end
