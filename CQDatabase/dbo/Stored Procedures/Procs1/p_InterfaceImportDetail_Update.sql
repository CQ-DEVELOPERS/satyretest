﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportDetail_Update
  ///   Filename       : p_InterfaceImportDetail_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:24
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportHeaderId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @LineNumber int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(100) = null,
  ///   @SKUCode nvarchar(60) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Weight float = null,
  ///   @RetailPrice decimal(13,2) = null,
  ///   @NetPrice decimal(13,2) = null,
  ///   @LineTotal decimal(13,2) = null,
  ///   @Volume decimal(13,3) = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportDetail_Update
(
 @InterfaceImportHeaderId int = null,
 @ForeignKey nvarchar(60) = null,
 @LineNumber int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(100) = null,
 @SKUCode nvarchar(60) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Weight float = null,
 @RetailPrice decimal(13,2) = null,
 @NetPrice decimal(13,2) = null,
 @LineTotal decimal(13,2) = null,
 @Volume decimal(13,3) = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceImportDetail
     set InterfaceImportHeaderId = isnull(@InterfaceImportHeaderId, InterfaceImportHeaderId),
         ForeignKey = isnull(@ForeignKey, ForeignKey),
         LineNumber = isnull(@LineNumber, LineNumber),
         ProductCode = isnull(@ProductCode, ProductCode),
         Product = isnull(@Product, Product),
         SKUCode = isnull(@SKUCode, SKUCode),
         Batch = isnull(@Batch, Batch),
         Quantity = isnull(@Quantity, Quantity),
         Weight = isnull(@Weight, Weight),
         RetailPrice = isnull(@RetailPrice, RetailPrice),
         NetPrice = isnull(@NetPrice, NetPrice),
         LineTotal = isnull(@LineTotal, LineTotal),
         Volume = isnull(@Volume, Volume),
         Additional1 = isnull(@Additional1, Additional1),
         Additional2 = isnull(@Additional2, Additional2),
         Additional3 = isnull(@Additional3, Additional3),
         Additional4 = isnull(@Additional4, Additional4),
         Additional5 = isnull(@Additional5, Additional5) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
