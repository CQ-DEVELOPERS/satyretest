﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaLocation_Insert
  ///   Filename       : p_AreaLocation_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:40
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the AreaLocation table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null output,
  ///   @LocationId int = null output 
  /// </param>
  /// <returns>
  ///   AreaLocation.AreaId,
  ///   AreaLocation.LocationId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaLocation_Insert
(
 @AreaId int = null output,
 @LocationId int = null output 
)

as
begin
	 set nocount on;
  
  if @AreaId = '-1'
    set @AreaId = null;
  
  if @LocationId = '-1'
    set @LocationId = null;
  
	 declare @Error int
 
  insert AreaLocation
        (AreaId,
         LocationId)
  select @AreaId,
         @LocationId 
  
  select @Error = @@Error, @LocationId = scope_identity()
  
  
  return @Error
  
end
