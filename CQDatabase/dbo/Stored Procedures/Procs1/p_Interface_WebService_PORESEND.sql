﻿create procedure [dbo].[p_Interface_WebService_PORESEND]
(
	@doc varchar(max) output
)
as
begin
set nocount on
	  set @doc = ''
	  
	  declare @Error int,
			  @Errormsg varchar(500),
			  @GetDate  datetime,
			  @InterfaceImportPOHeaderId int,
			  @PrimaryKey   varchar(30),
			  @OrderNumber  varchar(30),
			  @SupplierCode varchar(20),
			  @ProcessedDate varchar(20),
			  @CreateDate   varchar(255),
			  @Status   varchar(255),
			  -- Internal
			  @fetch_status_header  int,
			  @Count int
	  Set @Count = 0
	  select @GetDate = dbo.ufn_Getdate()
	  
	  declare extract_header_cursor cursor for
	  select  InterfaceImportPOHeaderId,
			 OrderNumber
	   from InterfaceImportPOHeader
	   where RecordStatus = 'N' and
			 RecordType = 'POQ' and
			 Ordernumber is not null 
	   order by InterfaceImportPOHeaderId
	  open extract_header_cursor
	  fetch extract_header_cursor into @InterfaceImportPOHeaderId,@OrderNumber
	  select @fetch_status_header = @@fetch_status
	  Set @doc = ''
	
	  while (@fetch_status_header = 0)
				  begin
				  set @Count=@count + 1
				  PRINT @Count
				  if @count = 1
					Set @doc = '(' + '''' + @OrderNumber  + ''''
				  else
					Set @doc = @doc + ',''' + @Ordernumber + ''''
				update InterfaceImportPOHeader
					   set RecordStatus = 'Y',
					   ProcessedDate = @GetDate
				 where InterfaceImportPOHeaderId = @InterfaceImportPOHeaderId   
				fetch extract_header_cursor into @InterfaceImportPOHeaderId,@OrderNumber
				select @fetch_status_header = @@fetch_status
		end
	  close extract_header_cursor
	  deallocate extract_header_cursor
	  Set @doc = @doc + ')'
	  if @Count = 0
		Set @doc = ''
end


