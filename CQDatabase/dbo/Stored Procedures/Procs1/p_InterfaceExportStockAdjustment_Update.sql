﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportStockAdjustment_Update
  ///   Filename       : p_InterfaceExportStockAdjustment_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:15
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceExportStockAdjustment table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportStockAdjustmentId int = null,
  ///   @RecordType char(3) = null,
  ///   @RecordStatus char(1) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Weight float = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @InsertDate datetime = null,
  ///   @Product nvarchar(510) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportStockAdjustment_Update
(
 @InterfaceExportStockAdjustmentId int = null,
 @RecordType char(3) = null,
 @RecordStatus char(1) = null,
 @ProductCode nvarchar(60) = null,
 @SKUCode nvarchar(20) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Weight float = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @InsertDate datetime = null,
 @Product nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceExportStockAdjustmentId = '-1'
    set @InterfaceExportStockAdjustmentId = null;
  
  if @RecordStatus = '-1'
    set @RecordStatus = null;
  
	 declare @Error int
 
  update InterfaceExportStockAdjustment
     set RecordType = isnull(@RecordType, RecordType),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         ProductCode = isnull(@ProductCode, ProductCode),
         SKUCode = isnull(@SKUCode, SKUCode),
         Batch = isnull(@Batch, Batch),
         Quantity = isnull(@Quantity, Quantity),
         Weight = isnull(@Weight, Weight),
         Additional1 = isnull(@Additional1, Additional1),
         Additional2 = isnull(@Additional2, Additional2),
         Additional3 = isnull(@Additional3, Additional3),
         Additional4 = isnull(@Additional4, Additional4),
         Additional5 = isnull(@Additional5, Additional5),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         InsertDate = isnull(@InsertDate, InsertDate),
         Product = isnull(@Product, Product) 
   where InterfaceExportStockAdjustmentId = @InterfaceExportStockAdjustmentId
  
  select @Error = @@Error
  
  
  return @Error
  
end
