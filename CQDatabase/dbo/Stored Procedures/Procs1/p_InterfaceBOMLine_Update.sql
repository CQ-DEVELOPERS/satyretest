﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceBOMLine_Update
  ///   Filename       : p_InterfaceBOMLine_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:48
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceBOMLine table.
  /// </remarks>
  /// <param>
  ///   @InterfaceBOMId int = null,
  ///   @ParentProductCode nvarchar(100) = null,
  ///   @LineNumber int = null,
  ///   @ProductCode nvarchar(100) = null,
  ///   @ProductDescription nvarchar(510) = null,
  ///   @Quantity float = null,
  ///   @Units nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceBOMLine_Update
(
 @InterfaceBOMId int = null,
 @ParentProductCode nvarchar(100) = null,
 @LineNumber int = null,
 @ProductCode nvarchar(100) = null,
 @ProductDescription nvarchar(510) = null,
 @Quantity float = null,
 @Units nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceBOMLine
     set InterfaceBOMId = isnull(@InterfaceBOMId, InterfaceBOMId),
         ParentProductCode = isnull(@ParentProductCode, ParentProductCode),
         LineNumber = isnull(@LineNumber, LineNumber),
         ProductCode = isnull(@ProductCode, ProductCode),
         ProductDescription = isnull(@ProductDescription, ProductDescription),
         Quantity = isnull(@Quantity, Quantity),
         Units = isnull(@Units, Units) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
