﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Contactist_Get_Drivers
  ///   Filename       : p_Contactist_Get_Drivers.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jan 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Contactist_Get_Drivers

as
begin
	 set nocount on;
  
  select ContactListId as DriverId,
         ContactPerson as 'Driver'
    from ContactList
    where Type = 'Driver'
end
