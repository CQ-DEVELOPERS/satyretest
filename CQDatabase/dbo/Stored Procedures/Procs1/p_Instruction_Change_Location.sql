﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_Change_Location
  ///   Filename       : p_Instruction_Change_Location.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_Change_Location
(
 @instructionId int,
 @locationId    int
)

as
begin
	 set nocount on;
  
  declare @TableInstructions as table
  (
   InstructionId       int,
   WarehouseId         int,
   PickLocationId      int,
   StoreLocationId     int,
   StorageUnitBatchId  int,
   Quantity            float
  )
  
  declare @Error               int,
          @Errormsg            nvarchar(500),
          @GetDate             datetime,
          @StorageUnitBatchId  int,
          @Quantity            float,
          @InstructionTypeCode nvarchar(10),
          @PickBatchCheck      bit
  
  select @GetDate = dbo.ufn_Getdate()
  set @ErrorMsg = 'Error executing p_Instruction_Change_Location'
  
  begin transaction
  
  select @StorageUnitBatchId  = i.StorageUnitBatchId,
         @Quantity            = i.Quantity,
         @InstructionTypeCode = it.InstructionTypeCode,
         @PickBatchCheck = case when i.PickLocationId = @locationId
                                then 1
                                else 0
                                end
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Status           s (nolock) on i.StatusId          = s.StatusId
   where i.InstructionId = @instructionId
     and s.StatusCode != 'F'
  
  if @@rowcount = 0
  begin
    set @Error = -1
    set @ErrorMsg = 'Error executing p_Instruction_Change_Location, Instruction finished.'
    goto error
  end
  
  if @PickBatchCheck = 1
  begin
    if not exists(select 1
                    from StorageUnitBatchLocation
                   where StorageUnitBatchId = @StorageUnitBatchId
                     and LocationId         = @locationId)
    begin
      select top 1
             @StorageUnitBatchId = newsub.StorageUnitBatchId
        from StorageUnitBatch       oldsub (nolock)
        join StorageUnitBatch       newsub (nolock) on oldsub.StorageUnitId = newsub.StorageUnitId
                                                   and oldsub.BatchId      != newsub.BatchId
        join Batch                       b (nolock) on newsub.BatchId       = b.BatchId
        join Status                      s (nolock) on b.StatusId           = s.StatusId
        join StorageUnitBatchLocation subl (nolock) on newsub.StorageUnitBatchId = subl.StorageUnitBatchId
       where oldsub.StorageUnitBatchId = @StorageUnitBatchId
         and LocationId                = @locationId
         and s.StatusCode              = 'A'
      order by b.ExpiryDate,
               b.Batch
    end
  end
  
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @instructionId = @instructionId
  
  if @Error <> 0
  begin
    set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Deallocate'
    goto error
  end
  
  if @InstructionTypeCode in ('S','SM','PR','M','R','O')
  begin
    exec @Error = p_Instruction_Update
      @instructionId      = @instructionId,
      @storeLocationId    = @locationId,
      @storageUnitBatchId = @StorageUnitBatchId
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'Error executing p_Instruction_Update'
      goto error
    end
  end
  else
  begin
    if not exists(select 1
                    from StorageUnitBatchLocation
                   where StorageUnitBatchId = @StorageUnitBatchId
                     and LocationId         = @locationId)
    begin
      select @StorageUnitBatchId = newsub.StorageUnitBatchId
        from StorageUnitBatch       oldsub (nolock)
        join StorageUnitBatch       newsub (nolock) on oldsub.StorageUnitId = newsub.StorageUnitId
                                                   and oldsub.BatchId      != newsub.BatchId
        join StorageUnitBatchLocation subl (nolock) on newsub.StorageUnitBatchId = subl.StorageUnitBatchId
       where oldsub.StorageUnitBatchId = @StorageUnitBatchId
         and LocationId                = @locationId
    end
    
    exec @Error = p_Instruction_Update
      @instructionId      = @instructionId,
      @pickLocationId     = @locationId,
      @storageUnitBatchId = @StorageUnitBatchId
    
    if @Error <> 0
    begin
      set @ErrorMsg = 'Error executing p_Instruction_Update'
      goto error
    end
  end
  
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @instructionId = @instructionId
  
  if @Error <> 0
  begin
    set @ErrorMsg = 'Error executing p_StorageUnitBatchLocation_Reserve'
    goto error
  end
  
  commit transaction
  return 0
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
