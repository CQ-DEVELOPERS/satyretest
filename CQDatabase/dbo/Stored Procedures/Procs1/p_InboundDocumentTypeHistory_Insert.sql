﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentTypeHistory_Insert
  ///   Filename       : p_InboundDocumentTypeHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 10:28:13
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundDocumentTypeHistory table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentTypeId int = null,
  ///   @InboundDocumentType nvarchar(60) = null,
  ///   @OverReceiveIndicator bit = null,
  ///   @AllowManualCreate bit = null,
  ///   @QuantityToFollowIndicator bit = null,
  ///   @PriorityId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @AutoSendup bit = null,
  ///   @InboundDocumentTypeCode nvarchar(20) = null,
  ///   @BatchButton bit = null,
  ///   @BatchSelect bit = null,
  ///   @DeliveryNoteNumber bit = null,
  ///   @VehicleRegistration bit = null,
  ///   @AreaType nvarchar(20) = null 
  /// </param>
  /// <returns>
  ///   InboundDocumentTypeHistory.InboundDocumentTypeId,
  ///   InboundDocumentTypeHistory.InboundDocumentType,
  ///   InboundDocumentTypeHistory.OverReceiveIndicator,
  ///   InboundDocumentTypeHistory.AllowManualCreate,
  ///   InboundDocumentTypeHistory.QuantityToFollowIndicator,
  ///   InboundDocumentTypeHistory.PriorityId,
  ///   InboundDocumentTypeHistory.CommandType,
  ///   InboundDocumentTypeHistory.InsertDate,
  ///   InboundDocumentTypeHistory.AutoSendup,
  ///   InboundDocumentTypeHistory.InboundDocumentTypeCode,
  ///   InboundDocumentTypeHistory.BatchButton,
  ///   InboundDocumentTypeHistory.BatchSelect,
  ///   InboundDocumentTypeHistory.DeliveryNoteNumber,
  ///   InboundDocumentTypeHistory.VehicleRegistration,
  ///   InboundDocumentTypeHistory.AreaType 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentTypeHistory_Insert
(
 @InboundDocumentTypeId int = null,
 @InboundDocumentType nvarchar(60) = null,
 @OverReceiveIndicator bit = null,
 @AllowManualCreate bit = null,
 @QuantityToFollowIndicator bit = null,
 @PriorityId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @AutoSendup bit = null,
 @InboundDocumentTypeCode nvarchar(20) = null,
 @BatchButton bit = null,
 @BatchSelect bit = null,
 @DeliveryNoteNumber bit = null,
 @VehicleRegistration bit = null,
 @AreaType nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InboundDocumentTypeHistory
        (InboundDocumentTypeId,
         InboundDocumentType,
         OverReceiveIndicator,
         AllowManualCreate,
         QuantityToFollowIndicator,
         PriorityId,
         CommandType,
         InsertDate,
         AutoSendup,
         InboundDocumentTypeCode,
         BatchButton,
         BatchSelect,
         DeliveryNoteNumber,
         VehicleRegistration,
         AreaType)
  select @InboundDocumentTypeId,
         @InboundDocumentType,
         @OverReceiveIndicator,
         @AllowManualCreate,
         @QuantityToFollowIndicator,
         @PriorityId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @AutoSendup,
         @InboundDocumentTypeCode,
         @BatchButton,
         @BatchSelect,
         @DeliveryNoteNumber,
         @VehicleRegistration,
         @AreaType 
  
  select @Error = @@Error
  
  
  return @Error
  
end
