﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSKU_Select
  ///   Filename       : p_InterfaceImportSKU_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:07
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportSKU table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSKUId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportSKU.InterfaceImportSKUId,
  ///   InterfaceImportSKU.RecordStatus,
  ///   InterfaceImportSKU.SKUCode,
  ///   InterfaceImportSKU.SKU,
  ///   InterfaceImportSKU.UnitOfMeasure,
  ///   InterfaceImportSKU.Quantity,
  ///   InterfaceImportSKU.AlternatePallet,
  ///   InterfaceImportSKU.SubstitutePallet,
  ///   InterfaceImportSKU.Litres,
  ///   InterfaceImportSKU.HostId,
  ///   InterfaceImportSKU.ProcessedDate,
  ///   InterfaceImportSKU.Insertdate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSKU_Select
(
 @InterfaceImportSKUId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceImportSKU.InterfaceImportSKUId
        ,InterfaceImportSKU.RecordStatus
        ,InterfaceImportSKU.SKUCode
        ,InterfaceImportSKU.SKU
        ,InterfaceImportSKU.UnitOfMeasure
        ,InterfaceImportSKU.Quantity
        ,InterfaceImportSKU.AlternatePallet
        ,InterfaceImportSKU.SubstitutePallet
        ,InterfaceImportSKU.Litres
        ,InterfaceImportSKU.HostId
        ,InterfaceImportSKU.ProcessedDate
        ,InterfaceImportSKU.Insertdate
    from InterfaceImportSKU
   where isnull(InterfaceImportSKU.InterfaceImportSKUId,'0')  = isnull(@InterfaceImportSKUId, isnull(InterfaceImportSKU.InterfaceImportSKUId,'0'))
  
end
