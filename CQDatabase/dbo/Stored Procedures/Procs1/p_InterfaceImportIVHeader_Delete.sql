﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIVHeader_Delete
  ///   Filename       : p_InterfaceImportIVHeader_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:39
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceImportIVHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportIVHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIVHeader_Delete
(
 @InterfaceImportIVHeaderId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceImportIVHeader
     where InterfaceImportIVHeaderId = @InterfaceImportIVHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
