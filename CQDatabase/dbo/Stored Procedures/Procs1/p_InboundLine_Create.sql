﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLine_Create
  ///   Filename       : p_InboundLine_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Jul 2007 19:59:21
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundLine table.
  /// </remarks>
  /// <param>
  ///   @InboundLineId int = null output,
  ///   @InboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null,
  ///   @BatchId int = null 
  /// </param>
  /// <returns>
  ///   InboundLine.InboundLineId,
  ///   InboundLine.InboundDocumentId,
  ///   InboundLine.StorageUnitId,
  ///   InboundLine.StatusId,
  ///   InboundLine.LineNumber,
  ///   InboundLine.Quantity,
  ///   InboundLine.BatchId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLine_Create
(
 @OperatorId        int,
 @InboundLineId     int = null output,
 @InboundDocumentId int = null,
 @StorageUnitId     int = null,
 @LineNumber        int = null,
 @Quantity          float = null,
 @BatchId           int = null,
 @ReasonId			int = null 
)

as
begin
  set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @StatusId           int,
          @OutboundDocumentId int,
          @ReferenceNumber    nvarchar(30),
          @InboundDocumentTypeCode nvarchar(10),
          @WarehouseId             int
  
  if @batchId = -1
    set @batchId = null
  
  if @ReasonId = -1
    set @ReasonId = null
  
  if @StatusId is null
    select @StatusId = StatusId
      from Status (nolock)
     where StatusCode = 'N'
       and Type       = 'ID'
  
  if @LineNumber is null
    set @LineNumber = isnull((select count(1) from InboundLine (nolock) where InboundDocumentId = @InboundDocumentId),1) + 1
  
  begin transaction
  
  select @ReferenceNumber         = id.ReferenceNumber,
         @InboundDocumentTypeCode = idt.InboundDocumentTypeCode,
         @WarehouseId             = id.WarehouseId
    from InboundDocument      id (nolock)
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
   where id.InboundDocumentId = @InboundDocumentId
     --and idt.InboundDocumentTypeCode    = 'RET'
     --and isnull(id.ReferenceNumber,'') != ''
  
  if (select dbo.ufn_Configuration(263, @WarehouseId)) = 0 and @InboundDocumentTypeCode = 'RET'
  if exists(select 1
              from InboundLine (nolock)
             where InboundDocumentId    = @InboundDocumentId
               and StorageUnitId        = @StorageUnitId
               and isnull(BatchId,'-1') = isnull(@BatchId,'-1'))
  begin
    set @Error = -1
    set @Errormsg = 'Product and batch combination already exists'
    goto error
  end
  
  if @ReferenceNumber = '' or @InboundDocumentTypeCode != 'RET'
    set @ReferenceNumber = null
  
  if @ReferenceNumber is not null
  begin
    select @OutboundDocumentId = OutboundDocumentId
      from OutboundDocument (nolock)
     where ReferenceNumber = @ReferenceNumber
    
    if @OutboundDocumentId is not null
      if not exists(select top 1 1
                      from OutboundLine ol (nolock)
                      join StorageUnit  su (nolock) on ol.StorageUnitId = su.StorageUnitId
                      join SKU         sku (nolock) on su.SKUId = sku.SKUId
                      join Product       p (nolock) on su.ProductId = p.ProductId
                     where ol.OutboundDocumentId = @OutboundDocumentId
                       and ol.StorageUnitId = @StorageUnitId)
      begin
        set @Error = -2
        set @Errormsg = 'Product and batch combination already exists'
        goto error
      end
  end
  
  if @InboundDocumentTypeCode = 'PRV'
  begin
    exec @Error = p_InboundDocument_Update
     @InboundDocumentId = @InboundDocumentId,
     @StorageUnitId     = @StorageUnitId,
     @BatchId           = @BatchId
    
    if @Error <> 0
      goto error
  end
  
  exec @Error = p_InboundLine_Insert
   @InboundLineId     = @InboundLineId output,
   @InboundDocumentId = @InboundDocumentId,
   @StorageUnitId     = @StorageUnitId,
   @StatusId          = @StatusId,
   @LineNumber        = @LineNumber,
   @Quantity          = @Quantity,
   @BatchId           = @BatchId,
   @ReasonId		        = @ReasonId
  
  if @Error <> 0
    goto error
  
  exec @Error = p_Receiving_Create_Receipt
   @InboundDocumentId = @InboundDocumentId,
   @InboundLineId     = @InboundLineId,
   @OperatorId        = @OperatorId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
