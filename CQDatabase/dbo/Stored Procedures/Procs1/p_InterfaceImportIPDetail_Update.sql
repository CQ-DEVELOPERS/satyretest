﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIPDetail_Update
  ///   Filename       : p_InterfaceImportIPDetail_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:29
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportIPDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportIPHeaderId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(510) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Excluding numeric(13,2) = null,
  ///   @Including numeric(13,2) = null,
  ///   @NettValue numeric(13,2) = null,
  ///   @LineNumber int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIPDetail_Update
(
 @InterfaceImportIPHeaderId int = null,
 @ForeignKey nvarchar(60) = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(510) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Excluding numeric(13,2) = null,
 @Including numeric(13,2) = null,
 @NettValue numeric(13,2) = null,
 @LineNumber int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceImportIPDetail
     set InterfaceImportIPHeaderId = isnull(@InterfaceImportIPHeaderId, InterfaceImportIPHeaderId),
         ForeignKey = isnull(@ForeignKey, ForeignKey),
         ProductCode = isnull(@ProductCode, ProductCode),
         Product = isnull(@Product, Product),
         Batch = isnull(@Batch, Batch),
         Quantity = isnull(@Quantity, Quantity),
         Excluding = isnull(@Excluding, Excluding),
         Including = isnull(@Including, Including),
         NettValue = isnull(@NettValue, NettValue),
         LineNumber = isnull(@LineNumber, LineNumber) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
