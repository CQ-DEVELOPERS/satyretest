﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DespatchChecking_Document_Search
  ///   Filename       : p_DespatchChecking_Document_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DespatchChecking_Document_Search
(
 @WarehouseId            int,
 @OutboundDocumentTypeId	int,
 @OrderNumber	           nvarchar(30),
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @FromDate	              datetime,
 @ToDate	                datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundShipmentId  int,
   IssueId             int,
   DocumentNumber      nvarchar(30),
   RouteId             int,
   Route               nvarchar(50),
   PriorityId          int,
   Priority            nvarchar(50),
   ExternalCompany     nvarchar(255),
   ExternalCompanyCode nvarchar(30),
   Status              nvarchar(50),
   LocationId          int,
   Location            nvarchar(15),
   DeliveryDate        datetime,
   PickedIndicator      nvarchar(20)
  );
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  insert @TableResult
        (OutboundShipmentId,
         IssueId,
         DocumentNumber,
         RouteId,
         Route,
         PriorityId,
         Status,
         LocationId,
         DeliveryDate)
  select osi.OutboundShipmentId,
         null,
         convert(nvarchar(30), osi.OutboundShipmentId),
         os.RouteId,
         os.Route,
         null,
         Status,
         os.LocationId,
         os.ShipmentDate
    from OutboundShipment       os (nolock)
    join OutboundShipmentIssue osi (nolock) on os.OutboundShipmentId     = osi.OutboundShipmentId
    join Issue                   i (nolock) on osi.IssueId               = i.IssueId
    join OutboundDocument       od (nolock) on i.OutboundDocumentId      = od.OutboundDocumentId
    join ExternalCompany        ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
    join Status                  s (nolock) on i.StatusId                = s.StatusId
    join OutboundDocumentType  odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
     and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and convert(nvarchar(30), os.OutboundShipmentId)
                                like isnull(@OrderNumber  + '%', convert(nvarchar(30), os.OutboundShipmentId))
     and os.ShipmentDate     between @FromDate and @ToDate
     and s.Type                    = 'IS'
     and s.StatusCode              = 'D'
     and od.WarehouseId            = @WarehouseId
  union
  select null,
         i.IssueId,
         od.OrderNumber,
         i.RouteId,
         null,
         i.PriorityId,
         s.Status,
         i.LocationId,
         i.DeliveryDate
    from OutboundDocument     od  (nolock)
    join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
    join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
    join Status               s   (nolock) on i.StatusId               = s.StatusId
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
   where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
     and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
     and i.DeliveryDate      between @FromDate and @ToDate
     and s.Type                    = 'IS'
     and s.StatusCode              = 'D'
     and od.WarehouseId            = @WarehouseId
     and not exists(select 1
                      from OutboundShipmentIssue osi (nolock)
                     where i.IssueId = osi.IssueId)
  
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update @TableResult
     set PickedIndicator = 'Standard'
  
  select isnull(OutboundShipmentId,-1) as 'OutboundShipmentId',
         IssueId,
         DocumentNumber,
         Route,
         Priority,
         ExternalCompanyCode,
         ExternalCompany,
         Status,
         Location,
         DeliveryDate,
         PickedIndicator
    from @TableResult
end
