﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOM_SO_Split
  ///   Filename       : p_BOM_SO_Split.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOM_SO_Split
(
 @issueId            int
)

as
begin
	 set nocount on;
	 
	 declare @Error              int = 0,
          @Errormsg           nvarchar(500) = 'Error executing p_BOM_SO_Split',
          @GetDate            datetime,
          @OutboundShipmentId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @issueId = -1
    set @issueId = null
  
  select @OutboundShipmentId = OutboundShipmentId
    from OutboundShipmentIssue (nolock)
   where IssueId = @issueId
  
  begin transaction
  
  if @OutboundShipmentId is not null
  begin
    exec @Error = p_OutboundShipmentIssue_Unlink
     @OutboundShipmentId = @OutboundShipmentId,
     @issueId            = @issueId
    
    if @Error <> 0
      goto error
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end

