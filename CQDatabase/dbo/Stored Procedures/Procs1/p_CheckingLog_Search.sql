﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_CheckingLog_Search
  ///   Filename       : p_CheckingLog_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:16
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the CheckingLog table.
  /// </remarks>
  /// <param>
  ///   @CheckingLogId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   CheckingLog.CheckingLogId,
  ///   CheckingLog.WarehouseId,
  ///   CheckingLog.ReferenceNumber,
  ///   CheckingLog.JobId,
  ///   CheckingLog.Barcode,
  ///   CheckingLog.StorageUnitId,
  ///   CheckingLog.StorageUnitBatchId,
  ///   CheckingLog.Batch,
  ///   CheckingLog.Quantity,
  ///   CheckingLog.OperatorId,
  ///   CheckingLog.StartDate,
  ///   CheckingLog.EndDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_CheckingLog_Search
(
 @CheckingLogId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @CheckingLogId = '-1'
    set @CheckingLogId = null;
  
 
  select
         CheckingLog.CheckingLogId
        ,CheckingLog.WarehouseId
        ,CheckingLog.ReferenceNumber
        ,CheckingLog.JobId
        ,CheckingLog.Barcode
        ,CheckingLog.StorageUnitId
        ,CheckingLog.StorageUnitBatchId
        ,CheckingLog.Batch
        ,CheckingLog.Quantity
        ,CheckingLog.OperatorId
        ,CheckingLog.StartDate
        ,CheckingLog.EndDate
    from CheckingLog
   where isnull(CheckingLog.CheckingLogId,'0')  = isnull(@CheckingLogId, isnull(CheckingLog.CheckingLogId,'0'))
  
end
