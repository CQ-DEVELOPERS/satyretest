﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Import_APSH
  ///   Filename       : p_FamousBrands_Import_APSH.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 31 Jul 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Import_APSH

as
begin
	 set nocount on;
  
  declare @Error               int,
          @Errormsg            varchar(500),
          @GetDate             datetime,
          @InterfaceImportSOHId int,
          @Location             varchar(10),
          @ProductCode          varchar(30),
          @Quantity             int,
          @UnitPrice            numeric(13,2),
          @SKUCode              varchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
  set @Error = 0
  
  update InterfaceImportSOH
     set ProcessedDate = @Getdate
   where RecordStatus in ('N','U')
     and ProcessedDate is null
  
  declare SOH_cursor cursor for
   select InterfaceImportSOHId,
          Location,
          ProductCode,
          Quantity,
          UnitPrice
  	  from	InterfaceImportSOH
    where ProcessedDate = @Getdate
	  order by	InterfaceImportSOHId
  
  open SOH_cursor
  
  fetch SOH_cursor into @InterfaceImportSOHId,
                        @Location,
                        @ProductCode,
                        @Quantity,
                        @UnitPrice

  while (@@fetch_status = 0)
  begin
    begin transaction
    
    delete HostStockOnHand where ProductCode = @ProductCode
    
    select @SKUCode = SKUCode
      from viewProduct
     where ProductCode = @ProductCode
    
    insert HostStockOnHand
          (ProductCode,
           Quantity,
           SKUCode,
           ActualCost,
           MostRecentCost,
           StandardCost)
    select @ProductCode,
           @Quantity,
           @SKUCode,
           @UnitPrice,
           @UnitPrice,
           @UnitPrice
    
    error:
    if @Error = 0
    begin
      update InterfaceImportSOH
         set RecordStatus = 'Y'
       where InterfaceImportSOHId = @InterfaceImportSOHId
      
      commit transaction
    end
    else
    begin
      if @@trancount > 0
        rollback transaction
      
      update InterfaceImportSOH
         set RecordStatus = 'E'
       where InterfaceImportSOHId = @InterfaceImportSOHId
    end
    
    fetch SOH_cursor into @InterfaceImportSOHId,
                          @Location,
                          @ProductCode,
                          @Quantity,
                          @UnitPrice
  end

  close SOH_cursor
  deallocate SOH_cursor
end
