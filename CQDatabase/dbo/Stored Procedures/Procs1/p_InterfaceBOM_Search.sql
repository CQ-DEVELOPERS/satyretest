﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceBOM_Search
  ///   Filename       : p_InterfaceBOM_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:46
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceBOM table.
  /// </remarks>
  /// <param>
  ///   @InterfaceBOMId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceBOM.InterfaceBOMId,
  ///   InterfaceBOM.ParentProductCode,
  ///   InterfaceBOM.OrderDescription,
  ///   InterfaceBOM.CreateDate,
  ///   InterfaceBOM.PlannedDate,
  ///   InterfaceBOM.Revision,
  ///   InterfaceBOM.BuildQuantity,
  ///   InterfaceBOM.Units,
  ///   InterfaceBOM.Location,
  ///   InterfaceBOM.ProcessedDate,
  ///   InterfaceBOM.RecordStatus 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceBOM_Search
(
 @InterfaceBOMId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceBOMId = '-1'
    set @InterfaceBOMId = null;
  
 
  select
         InterfaceBOM.InterfaceBOMId
        ,InterfaceBOM.ParentProductCode
        ,InterfaceBOM.OrderDescription
        ,InterfaceBOM.CreateDate
        ,InterfaceBOM.PlannedDate
        ,InterfaceBOM.Revision
        ,InterfaceBOM.BuildQuantity
        ,InterfaceBOM.Units
        ,InterfaceBOM.Location
        ,InterfaceBOM.ProcessedDate
        ,InterfaceBOM.RecordStatus
    from InterfaceBOM
   where isnull(InterfaceBOM.InterfaceBOMId,'0')  = isnull(@InterfaceBOMId, isnull(InterfaceBOM.InterfaceBOMId,'0'))
  
end
