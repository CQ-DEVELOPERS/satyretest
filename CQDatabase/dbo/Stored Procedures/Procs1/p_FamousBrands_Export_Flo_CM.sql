﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_Flo_CM
  ///   Filename       : p_FamousBrands_Export_Flo_CM.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Mar 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Export_Flo_CM
(
 @FileName varchar(30) = null output
)

as
begin
  set nocount on
  --select @FileName = 'Customer.txt'
  select @FileName = ''
  
  select distinct CustomerNumber + ',' + 
         replace(Name,',',' ') + ',"' + 
         isnull(Class,'') + '","' + 
         isnull(DeliveryPoint,'') + '","' + 
         isnull(Address1,'') + '","' + 
         isnull(Address2,'') + '","' + 
         isnull(Address3,'') + '","' +
         isnull(Address4,'') + '","' + 
         isnull(Contact,'') + '","' + 
         isnull(Tel,'') + '","' + 
         isnull(Fax,'') + '","' + 
         isnull(DeliveryGroup,'') + '","' + 
         isnull(DepotCode,'') + '","' + 
         isnull(convert(varchar(10), VisitFrequency),'') + '"' 
    from FloCustomerExport
   where RecordStatus = 'N'
  
  update FloCustomerExport
     set RecordStatus = 'Y'
   where RecordStatus = 'N'
end
