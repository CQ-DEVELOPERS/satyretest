﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_Webservice_RTSSEND
  ///   Filename       : p_Interface_Webservice_RTSSEND.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Interface_Webservice_RTSSEND
(
 @XMLBody nvarchar(max) output
)
--with encryption
as
begin

    DECLARE @GETDATE DATETIME
    
    SELECT @GETDATE = dbo.ufn_GetDate()
    
    UPDATE InterfaceExportHeader
    SET ProcessedDate = @GETDATE
    WHERE RecordStatus = 'N'
    AND RecordType != 'Allocated'
    AND RecordType = 'RTS'
    AND ProcessedDate IS NULL
	
	SELECT @XMLBody = '<?xml version="1.0" encoding="utf-16"?>' +
        (SELECT 
            (SELECT 
                 ''                         AS 'Source'
                ,''                         AS 'Target'
                ,GETDATE()                  AS 'CreateDate'
                ,'POConfirmation'           AS 'FileType'        
             FOR XML PATH('Header'), TYPE)
            ,(SELECT
                (SELECT
                     H.RecordType               AS 'ItemType'
                    ,'N'                        AS 'ItemStatus'
                    ,'InterfaceExportHeader'    AS 'InterfaceTable'
                    ,H.InterfaceExportHeaderId  AS 'InterfaceTableId'
                    ,H.PrimaryKey               AS 'PrimaryKey'
                    ,H.OrderNumber              AS 'OrderNumber'
                    ,H.OrderNumber              AS 'InvoiceNumber'
                    ,H.CompanyCode              AS 'CompanyCode'
                    ,H.Company                  AS 'Company'
                    ,H.Address                  AS 'Address'
                    ,H.FromWarehouseCode        AS 'FromWarehouseCode'
                    ,H.ToWarehouseCode          AS 'ToWarehouseCode'
                    ,H.Route                    AS 'Route'
                    ,H.DeliveryNoteNumber       AS 'DeliveryNoteNumber'
                    ,H.ContainerNumber          AS 'ContainerNumber'
                    ,H.SealNumber               AS 'SealNumber'
                    ,H.DeliveryDate             AS 'DeliveryDate'
                    ,H.Remarks                  AS 'Remarks'
                    ,H.NumberOfLines            AS 'NumberOfLines'
                    ,H.Additional1              AS 'Additional1'
                    ,H.Additional2              AS 'Additional2'
                    ,H.Additional3              AS 'Additional3'
                    ,H.Additional4              AS 'Additional4'
                    ,H.Additional5              AS 'Additional5'
                    ,H.Additional6              AS 'Additional6'
                    ,H.Additional7              AS 'Additional7'
                    ,H.Additional8              AS 'Additional8'
                    ,H.Additional9              AS 'Additional9'
                    ,H.Additional10             AS 'Additional10' 
                    ,(SELECT 
                         'InterfaceExportDetail'    AS 'InterfaceTable'
                        ,ROW_NUMBER() OVER (Order By LineNumber) AS 'InterfaceTableId'
                        ,D1.ForeignKey              AS 'ForeignKey'
                        ,D1.LineNumber              AS 'LineNumber'
                        ,D1.ProductCode             AS 'ProductCode'
                        ,D1.Product                 AS 'Product'
                        ,D1.SKUCode                 AS 'SKUCode'
                        ,D1.Batch                   AS 'Batch'
                        ,CONVERT(decimal,D1.Quantity)                AS 'Quantity'
                        ,CONVERT(decimal,D1.Weight)                  AS 'Weight'
        ,0 AS 'Volume'
    ,D1.Additional1             AS 'Additional1'
                        ,D1.Additional2             AS 'Additional2'
                        ,D1.Additional3             AS 'Additional3'
                        ,D1.Additional4             AS 'Additional4'
                        ,D1.Additional5             AS 'Additional5'
                        ,D1.Additional1             AS 'Additional6'
                        ,D1.Additional1             AS 'Additional7'
                        ,D1.Additional1             AS 'Additional8'
                        ,D1.Additional1             AS 'Additional9'
                        ,D1.Additional1             AS 'Additional10'
                      FROM InterfaceExportHeader H1 (nolock)
                      INNER JOIN InterfaceExportDetail D1 on H1.InterfaceExportHeaderId = D1.InterfaceExportHeaderId
                      WHERE H1.InterfaceExportHeaderId = H.InterfaceExportHeaderId
                      FOR XML PATH('ItemLine'), TYPE)
                FROM InterfaceExportHeader H (nolock)
                WHERE RecordStatus = 'N'
                AND ProcessedDate = @GETDATE
                FOR XML PATH('Item'), TYPE)
            FOR XML PATH('Body'), TYPE)    
        FOR XML PATH('root'))
        
    UPDATE InterfaceExportHeader 
    SET RecordStatus = 'Y'
    WHERE RecordStatus = 'N'
    AND RecordType != 'STARTED'
    AND RecordType = 'RTS'
    AND ProcessedDate = @GETDATE
    
End
