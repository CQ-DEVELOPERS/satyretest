﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLine_Delete
  ///   Filename       : p_InboundLine_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 08:48:44
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InboundLine table.
  /// </remarks>
  /// <param>
  ///   @InboundLineId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLine_Delete
(
 @InboundLineId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InboundLine
     where InboundLineId = @InboundLineId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_InboundLineHistory_Insert
         @InboundLineId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
