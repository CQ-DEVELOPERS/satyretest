﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLine_Select
  ///   Filename       : p_InboundLine_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 08:48:45
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundLine table.
  /// </remarks>
  /// <param>
  ///   @InboundLineId int = null 
  /// </param>
  /// <returns>
  ///   InboundLine.InboundLineId,
  ///   InboundLine.InboundDocumentId,
  ///   InboundLine.StorageUnitId,
  ///   InboundLine.StatusId,
  ///   InboundLine.LineNumber,
  ///   InboundLine.Quantity,
  ///   InboundLine.BatchId,
  ///   InboundLine.UnitPrice,
  ///   InboundLine.ReasonId,
  ///   InboundLine.ChildStorageUnitId,
  ///   InboundLine.BOELineNumber,
  ///   InboundLine.TariffCode,
  ///   InboundLine.CountryofOrigin,
  ///   InboundLine.Reference1,
  ///   InboundLine.Reference2 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLine_Select
(
 @InboundLineId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InboundLine.InboundLineId
        ,InboundLine.InboundDocumentId
        ,InboundLine.StorageUnitId
        ,InboundLine.StatusId
        ,InboundLine.LineNumber
        ,InboundLine.Quantity
        ,InboundLine.BatchId
        ,InboundLine.UnitPrice
        ,InboundLine.ReasonId
        ,InboundLine.ChildStorageUnitId
        ,InboundLine.BOELineNumber
        ,InboundLine.TariffCode
        ,InboundLine.CountryofOrigin
        ,InboundLine.Reference1
        ,InboundLine.Reference2
    from InboundLine
   where isnull(InboundLine.InboundLineId,'0')  = isnull(@InboundLineId, isnull(InboundLine.InboundLineId,'0'))
  order by BOELineNumber
  
end
