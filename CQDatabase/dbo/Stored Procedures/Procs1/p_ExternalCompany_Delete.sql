﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Delete
  ///   Filename       : p_ExternalCompany_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:44:48
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Delete
(
 @ExternalCompanyId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ExternalCompany
     where ExternalCompanyId = @ExternalCompanyId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
