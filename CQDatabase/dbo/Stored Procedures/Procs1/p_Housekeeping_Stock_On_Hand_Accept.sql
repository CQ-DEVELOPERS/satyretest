﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_On_Hand_Accept
  ///   Filename       : p_Housekeeping_Stock_On_Hand_Accept.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 15 Apr 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_On_Hand_Accept
(
 @warehouseId    int,
 @comparisonDate datetime,
 @storageUnitId  int,
 @batchId        int
)

as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @Rowcount          int,
          @isStockOnHand     bit, -- Added for Georgia 2014-04-22
          @MaxDate           datetime,
	         @MinDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
	   
  if (select dbo.ufn_Configuration(272, @WarehouseId)) = 1 -- ST Upload - Send Stock on hand quantity
       set @isStockOnHand = 1
	 
	 --select @MaxDate = max(ComparisonDate)
	 --  from StockOnHandCompare
	 
	 select @MaxDate = @comparisonDate
	 
	 select @MinDate = max(ComparisonDate)
	   from StockOnHandCompare
	  where ComparisonDate < @MaxDate
	    and ComparisonDate > dateadd(dd, -7, getdate())
  
  begin transaction
  
  update StockOnHandCompare
     set Sent = 1
   where ComparisonDate = @comparisonDate
     and StorageUnitId  = @storageUnitId
     and BatchId        = @batchId
     and isnull(Sent,0) = 0
  
  select @Error = @@Error, @Rowcount = @@Rowcount
  
  if @Error <> 0
    goto error
  
  if @Rowcount > 0
  begin
    insert InterfaceExportStockAdjustment
          (RecordType,
           RecordStatus,
           ProductCode,
           SKUCode,
           Batch,
           Additional1,
           Quantity)
    select 'SOH',
           'N',
           vs.ProductCode,
           vs.SKUCode,
           vs.Batch,
           sohmax.WarehouseCode,
           CASE @isStockOnHand WHEN 1 THEN sohmax.Quantity ELSE sohmax.Quantity - sohmin.Quantity END
      from StockOnHandCompare sohmax
      left
      join StockOnHandCompare sohmin on sohmax.StorageUnitId = sohmin.StorageUnitId
                                    and sohmax.BatchId       = sohmin.BatchId
                                    and sohmax.ComparisonDate = @MaxDate
                                    and sohmin.ComparisonDate = @MinDate
      join viewStock           vs on sohmax.StorageUnitId = vs.StorageUnitId
                                 and sohmax.BatchId       = vs.BatchId
     where sohmax.ComparisonDate = @comparisonDate
       and sohmax.StorageUnitId  = @storageUnitId
       and sohmax.BatchId        = @batchId
    
    select @Error = @@Error, @Rowcount = @@Rowcount
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Stock_On_Hand_Accept'); 
    rollback transaction
    return @Error
end
