﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_List
  ///   Filename       : p_Configuration_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 12:12:01
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Configuration table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Configuration.ConfigurationId,
  ///   Configuration.WarehouseId,
  ///   Configuration.Configuration 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ConfigurationId
        ,'-1' as WarehouseId
        ,'{All}' as Configuration
  union
  select
         Configuration.ConfigurationId
        ,Configuration.WarehouseId
        ,Configuration.Configuration
    from Configuration
  order by Configuration
  
end
 
