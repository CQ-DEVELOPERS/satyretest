﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Status
  ///   Filename       : p_Batch_Status.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Status

as
begin
	 set nocount on;
  create table #Status
(
 StatusId                        int              not null,
 Status                          nvarchar(50)      not null,

)
	Insert into #Status(StatusId,Status)
    Select 0,'{All}'

	Insert into #Status(StatusId,Status)
    select StatusId,
           Status
      from Status (nolock)
	
     where Type = 'B'
    order by Status
	
	Select * from #Status  
end
