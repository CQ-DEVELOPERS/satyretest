﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_GroupPerformance_Insert
  ///   Filename       : p_GroupPerformance_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:39
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the GroupPerformance table.
  /// </remarks>
  /// <param>
  ///   @OperatorGroupId int = null,
  ///   @PerformanceType nvarchar(2) = null,
  ///   @Units int = null,
  ///   @Weight float = null,
  ///   @Instructions numeric(13,3) = null,
  ///   @Orders numeric(13,3) = null,
  ///   @OrderLines numeric(13,3) = null,
  ///   @Jobs numeric(13,3) = null,
  ///   @QA numeric(13,3) = null 
  /// </param>
  /// <returns>
  ///   GroupPerformance.OperatorGroupId,
  ///   GroupPerformance.PerformanceType,
  ///   GroupPerformance.Units,
  ///   GroupPerformance.Weight,
  ///   GroupPerformance.Instructions,
  ///   GroupPerformance.Orders,
  ///   GroupPerformance.OrderLines,
  ///   GroupPerformance.Jobs,
  ///   GroupPerformance.QA 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_GroupPerformance_Insert
(
 @OperatorGroupId int = null,
 @PerformanceType nvarchar(2) = null,
 @Units int = null,
 @Weight float = null,
 @Instructions numeric(13,3) = null,
 @Orders numeric(13,3) = null,
 @OrderLines numeric(13,3) = null,
 @Jobs numeric(13,3) = null,
 @QA numeric(13,3) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert GroupPerformance
        (OperatorGroupId,
         PerformanceType,
         Units,
         Weight,
         Instructions,
         Orders,
         OrderLines,
         Jobs,
         QA)
  select @OperatorGroupId,
         @PerformanceType,
         @Units,
         @Weight,
         @Instructions,
         @Orders,
         @OrderLines,
         @Jobs,
         @QA 
  
  select @Error = @@Error
  
  
  return @Error
  
end
