﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Group_Search_Warehouse
  ///   Filename       : p_Group_Search_Warehouse.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Nov 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Group_Search_Warehouse
(
 @warehouseId int
)

as
begin
	 set nocount on;
  
  select distinct stg.stockTakeGroupId,
                  stg.StockTakeGroupCode,
                  stg.StockTakeGroup
    from StockTakeGroup stg
  --  join Location      l on lt.LocationTypeId = l.LocationTypeId
  --  join AreaLocation al on l.LocationId      = al.LocationId
  --  join Area          a on al.AreaId         = a.AreaId
  --  join StockTakeGroupLocation stgl on stgl
  -- where a.WarehouseId = @warehouseId
  --   and a.StockOnHand = 1
  order by stg.StockTakeGroup
  
    --select distinct lt.LocationTypeId,
  --                lt.LocationType,
  --                lt.LocationTypeCode
  --  from LocationType lt
  --  join Location      l on lt.LocationTypeId = l.LocationTypeId
  --  join AreaLocation al on l.LocationId      = al.LocationId
  --  join Area          a on al.AreaId         = a.AreaId
  -- where a.WarehouseId = @warehouseId
  --   and a.StockOnHand = 1
  --order by lt.LocationType
  
end
