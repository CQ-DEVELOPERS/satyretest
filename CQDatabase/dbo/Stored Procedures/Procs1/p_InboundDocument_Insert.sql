﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Insert
  ///   Filename       : p_InboundDocument_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Feb 2013 14:47:29
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null output,
  ///   @InboundDocumentTypeId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @StatusId int = null,
  ///   @WarehouseId int = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @DeliveryDate datetime = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null,
  ///   @ReferenceNumber nvarchar(60) = null,
  ///   @FromLocation nvarchar(100) = null,
  ///   @ToLocation nvarchar(100) = null,
  ///   @ReasonId int = null,
  ///   @Remarks nvarchar(510) = null,
  ///   @StorageUnitId int = null,
  ///   @BatchId int = null,
  ///   @DivisionId int = null,
  ///   @PrincipalId int = null 
  /// </param>
  /// <returns>
  ///   InboundDocument.InboundDocumentId,
  ///   InboundDocument.InboundDocumentTypeId,
  ///   InboundDocument.ExternalCompanyId,
  ///   InboundDocument.StatusId,
  ///   InboundDocument.WarehouseId,
  ///   InboundDocument.OrderNumber,
  ///   InboundDocument.DeliveryDate,
  ///   InboundDocument.CreateDate,
  ///   InboundDocument.ModifiedDate,
  ///   InboundDocument.ReferenceNumber,
  ///   InboundDocument.FromLocation,
  ///   InboundDocument.ToLocation,
  ///   InboundDocument.ReasonId,
  ///   InboundDocument.Remarks,
  ///   InboundDocument.StorageUnitId,
  ///   InboundDocument.BatchId,
  ///   InboundDocument.DivisionId,
  ///   InboundDocument.PrincipalId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Insert
(
 @InboundDocumentId int = null output,
 @InboundDocumentTypeId int = null,
 @ExternalCompanyId int = null,
 @StatusId int = null,
 @WarehouseId int = null,
 @OrderNumber nvarchar(60) = null,
 @DeliveryDate datetime = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null,
 @ReferenceNumber nvarchar(60) = null,
 @FromLocation nvarchar(100) = null,
 @ToLocation nvarchar(100) = null,
 @ReasonId int = null,
 @Remarks nvarchar(510) = null,
 @StorageUnitId int = null,
 @BatchId int = null,
 @DivisionId int = null,
 @PrincipalId int = null 
)

as
begin
	 set nocount on;
  
  if @InboundDocumentId = '-1'
    set @InboundDocumentId = null;
  
  if @InboundDocumentTypeId = '-1'
    set @InboundDocumentTypeId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @WarehouseId = '-1'
    set @WarehouseId = null;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @DivisionId = '-1'
    set @DivisionId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
	 declare @Error int
 
  insert InboundDocument
        (InboundDocumentTypeId,
         ExternalCompanyId,
         StatusId,
         WarehouseId,
         OrderNumber,
         DeliveryDate,
         CreateDate,
         ModifiedDate,
         ReferenceNumber,
         FromLocation,
         ToLocation,
         ReasonId,
         Remarks,
         StorageUnitId,
         BatchId,
         DivisionId,
         PrincipalId)
  select @InboundDocumentTypeId,
         @ExternalCompanyId,
         @StatusId,
         @WarehouseId,
         @OrderNumber,
         @DeliveryDate,
         @CreateDate,
         @ModifiedDate,
         @ReferenceNumber,
         @FromLocation,
         @ToLocation,
         @ReasonId,
         @Remarks,
         @StorageUnitId,
         @BatchId,
         @DivisionId,
         @PrincipalId 
  
  select @Error = @@Error, @InboundDocumentId = scope_identity()
  
  
  return @Error
  
end
