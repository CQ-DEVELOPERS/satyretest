﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Search_BatchId
  ///   Filename       : p_Batch_Search_BatchId.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Search_BatchId
(
 @batchId int
)

as
begin
	 set nocount on;
	 
	 if @batchId in (0,-1)
	   select @batchId = max(BatchId) from Batch
  
  select p.ProductCode,
         p.Product,
         b.BatchId,
         b.StatusId,
         s.Status,
         b.WarehouseId,
         b.Batch,
         b.ExpiryDate,
         b.IncubationDays,
         b.ShelfLifeDays,
         b.ExpectedYield,
         b.ActualYield,
         b.ECLNumber,
         b.BatchReferenceNumber,
         b.ProductionDateTime,
         b.FilledYield,
         isnull(b.COACertificate,0) as 'COACertificate',
         b.CreateDate,
         b.CreatedBy,
         b.ModifiedDate,
         b.ModifiedBy,
         b.StatusModifiedBy,
         b.StatusModifiedDate,
         b.RI,
         b.Density
    from Batch              b (nolock)
    join Status             s (nolock) on b.StatusId = s.StatusId
    left
    join StorageUnitBatch sub (nolock) on b.BatchId = sub.BatchId
    left
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
    left
    join Product            p (nolock) on su.ProductId = p.ProductId
   where b.BatchId = isnull(@batchId, b.batchId)
end
 
 
 
 
