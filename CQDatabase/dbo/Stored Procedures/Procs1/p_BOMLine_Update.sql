﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOMLine_Update
  ///   Filename       : p_BOMLine_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:11
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the BOMLine table.
  /// </remarks>
  /// <param>
  ///   @BOMLineId int = null,
  ///   @BOMHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOMLine_Update
(
 @BOMLineId int = null,
 @BOMHeaderId int = null 
)

as
begin
	 set nocount on;
  
  if @BOMLineId = '-1'
    set @BOMLineId = null;
  
	 declare @Error int
 
  update BOMLine
     set BOMHeaderId = isnull(@BOMHeaderId, BOMHeaderId) 
   where BOMLineId = @BOMLineId
  
  select @Error = @@Error
  
  
  return @Error
  
end
