﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Import_ST
  ///   Filename       : p_FamousBrands_Import_ST.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 23 Jul 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Import_ST

as
begin
  set nocount on;
  
  declare @Error                      int,
          @Errormsg                   varchar(500),
          @GetDate                    datetime,
          @warehouseId                int,
          @operatorId                 int,
          @JobId                      int,
          @ReferenceNumber            varchar(30),
          @Location                   varchar(15),
          @ProductCode                varchar(30),
          @NewItemCode                varchar(30),
          @CurrentCount               int,
          @InterfaceImportStockTakeId int,
          @LocationId                 int,
          @StorageUnitBatchId         int,
          @Batch                      varchar(50),
          @PalletId                   int,
          @OldStorageUnitBatchId      int
  
  select @GetDate = dbo.ufn_Getdate()
  
  update InterfaceImportStockTake
     set ProcessedDate = @GetDate
   where ProcessedDate is null
     and isnull(RecordStatus,'N') = 'N'
  
  if @Error <> 0
    goto error
  
  set @WarehouseId = 1
  set @OperatorId = 1
  
  declare Import_cursor cursor for
   select InterfaceImportStockTakeId,
          Location,
          ProductCode,
          Batch,
          NewItemCode,
          CurrentCount
     from InterfaceImportStockTake
    where ProcessedDate = @Getdate
      and CurrentCount is not null
   order by InterfaceImportStockTakeId
  
  open Import_cursor
  
  fetch Import_cursor into @InterfaceImportStockTakeId,
                           @Location,
                           @ProductCode,
                           @Batch,
                           @NewItemCode,
                           @CurrentCount
  
  if (@@fetch_status = 0)
  begin
    exec @Error = p_Housekeeping_Stock_Take_Create_Job
     @warehouseId         = @warehouseId,
     @operatorId          = @operatorId,
     @instructionTypeCode = 'STE',
     @jobId               = @JobId output,
     @referenceNumber     = @ReferenceNumber
    
    if @Error <> 0
      goto error
    
    while (@@fetch_status = 0)
    begin
      begin transaction
      
      set @LocationId = null
      
      select @LocationId = l.LocationId
        from Location      l (nolock)
        join AreaLOcation al (nolock) on l.LocationId = al.LocationId
        join Area          a (nolock) on al.AreaId = a.AreaId
       where a.WarehouseId = @WarehouseId
         and l.Location    = @Location
      
      if @NewItemCode is null
      begin
        set @StorageUnitBatchId = null
        
        select @StorageUnitBatchId = subl.StorageUnitBatchId
          from StorageUnitBatchLocation subl (nolock)
          join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
          join Batch                       b (nolock) on sub.BatchId             = b.BatchId
          join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
          join Product                     p (nolock) on su.ProductId            = p.ProductId
         where p.ProductCode = @ProductCode
           and b.Batch       = @Batch
        
        if @StorageUnitBatchId is null
          select @StorageUnitBatchId = sub.StorageUnitBatchId
            from StorageUnitBatch sub (nolock)
            join Batch              b (nolock) on sub.BatchId       = b.BatchId
            join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
            join Product            p (nolock) on su.ProductId      = p.ProductId
           where p.ProductCode = @ProductCode
             and b.Batch       = @Batch
      end
      else
      begin
        set @StorageUnitBatchId = null
        
        if substring(@NewItemCode, 1, 2) = 'P:'
        begin
          if isnumeric(replace(@NewItemCode,'P:','')) = 1
          begin
            select @PalletId = convert(int, replace(@NewItemCode,'P:',''))
            
            select @StorageUnitBatchId = StorageUnitBatchId
              from Pallet
             where PalletId = @PalletId
          end
        end
        else
        begin
          select @StorageUnitBatchId = subl.StorageUnitBatchId
            from StorageUnitBatchLocation subl (nolock)
            join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
            join Batch                       b (nolock) on sub.BatchId             = b.BatchId
            join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
            join Product                     p (nolock) on su.ProductId            = p.ProductId
           where p.ProductCode = @NewItemCode
             and b.Batch       = 'Default'
        end
        
        set @OldStorageUnitBatchId = null
        
        select @OldStorageUnitBatchId = subl.StorageUnitBatchId
          from StorageUnitBatchLocation subl (nolock)
          join StorageUnitBatch          sub (nolock) on subl.StorageUnitBatchId = sub.StorageUnitBatchId
          join Batch                       b (nolock) on sub.BatchId             = b.BatchId
          join StorageUnit                su (nolock) on sub.StorageUnitId       = su.StorageUnitId
          join Product                     p (nolock) on su.ProductId            = p.ProductId
         where p.ProductCode = @ProductCode
           and b.Batch       = @Batch
        
        if @OldStorageUnitBatchId is null
          select @OldStorageUnitBatchId = sub.StorageUnitBatchId
            from StorageUnitBatch sub (nolock)
            join Batch              b (nolock) on sub.BatchId       = b.BatchId
            join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
            join Product            p (nolock) on su.ProductId      = p.ProductId
           where p.ProductCode = @ProductCode
             and b.Batch       = @Batch
      end
      
      if @StorageUnitBatchId is not null and @LocationId is not null
      begin
        if @JobId is not null
        begin
          if not exists(select top 1 1 from StorageUnitBatchLocation where LocationId = @LocationId and StorageUnitBatchId = @StorageUnitBatchId)
            insert StorageUnitBatchLocation
                  (StorageUnitBatchId,
                   LocationId,
                   ActualQuantity,
                   AllocatedQuantity,
                   ReservedQuantity)
            select @StorageUnitBatchId,
                   @LocationId,
                   0,
                   0,
                   0
          
          update Location set StocktakeInd = 0 where LocationId = @LocationId
          
          exec @Error = p_Housekeeping_Stock_Take_Create_Product_Mobile
           @warehouseId        = @WarehouseId,
           @operatorId         = @OperatorId,
           @jobId              = @JobId,
           @locationId         = @LocationId,
           @storageUnitBatchId = @StorageUnitBatchId,
           @confirmedQuantity  = @CurrentCount
          
          if @Error <> 0
            goto error
          
          if @OldStorageUnitBatchId is not null
          begin
            if not exists(select top 1 1 from StorageUnitBatchLocation where LocationId = @LocationId and StorageUnitBatchId = @OldStorageUnitBatchId)
              insert StorageUnitBatchLocation
                    (StorageUnitBatchId,
                     LocationId,
                     ActualQuantity,
                     AllocatedQuantity,
                     ReservedQuantity)
              select @OldStorageUnitBatchId,
                     @LocationId,
                     0,
                     0,
                     0
            
            update Location set StocktakeInd = 0 where LocationId = @LocationId
            
            exec @Error = p_Housekeeping_Stock_Take_Create_Product_Mobile
             @warehouseId        = @WarehouseId,
             @operatorId         = @OperatorId,
             @jobId              = @JobId,
             @locationId         = @LocationId,
             @storageUnitBatchId = @OldStorageUnitBatchId,
             @confirmedQuantity  = 0
            
            if @Error <> 0
              goto error
          end
        end
      end
      else
      begin
        set @Error = -1
      end
      
      error:
      if @Error = 0
      begin
        update InterfaceImportStockTake
           set RecordStatus = 'Y'
         where InterfaceImportStockTakeId = @InterfaceImportStockTakeId
        
        commit transaction
      end
      else
      begin
        if @@trancount > 0
          rollback transaction
        
        update InterfaceImportStockTake
           set RecordStatus = 'E'
         where InterfaceImportStockTakeId = @InterfaceImportStockTakeId
      end
      
      fetch Import_cursor into @InterfaceImportStockTakeId,
                               @Location,
                               @ProductCode,
                               @Batch,
                               @NewItemCode,
                               @CurrentCount
    end
  end
  
  close Import_cursor
  deallocate Import_cursor
  
  exec p_Housekeeping_Stock_Take_Reload @WarehouseId = @WarehouseId
end
