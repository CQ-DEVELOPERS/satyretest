﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocumentContactList_Search
  ///   Filename       : p_InboundDocumentContactList_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:44
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InboundDocumentContactList table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InboundDocumentContactList.InboundDocumentId,
  ///   InboundDocumentContactList.ContactListId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocumentContactList_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InboundDocumentContactList.InboundDocumentId
        ,InboundDocumentContactList.ContactListId
    from InboundDocumentContactList
  
end
