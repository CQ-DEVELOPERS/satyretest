﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportPODetail_Insert
  ///   Filename       : p_InterfaceImportPODetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:55
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportPODetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportPOHeaderId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @LineNumber int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(100) = null,
  ///   @SKUCode nvarchar(20) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Weight float = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null,
  ///   @Additional6 nvarchar(510) = null,
  ///   @Additional7 nvarchar(510) = null,
  ///   @Additional8 nvarchar(510) = null,
  ///   @Additional9 nvarchar(510) = null,
  ///   @Additional10 nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportPODetail.InterfaceImportPOHeaderId,
  ///   InterfaceImportPODetail.ForeignKey,
  ///   InterfaceImportPODetail.LineNumber,
  ///   InterfaceImportPODetail.ProductCode,
  ///   InterfaceImportPODetail.Product,
  ///   InterfaceImportPODetail.SKUCode,
  ///   InterfaceImportPODetail.Batch,
  ///   InterfaceImportPODetail.Quantity,
  ///   InterfaceImportPODetail.Weight,
  ///   InterfaceImportPODetail.Additional1,
  ///   InterfaceImportPODetail.Additional2,
  ///   InterfaceImportPODetail.Additional3,
  ///   InterfaceImportPODetail.Additional4,
  ///   InterfaceImportPODetail.Additional5,
  ///   InterfaceImportPODetail.Additional6,
  ///   InterfaceImportPODetail.Additional7,
  ///   InterfaceImportPODetail.Additional8,
  ///   InterfaceImportPODetail.Additional9,
  ///   InterfaceImportPODetail.Additional10 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportPODetail_Insert
(
 @InterfaceImportPOHeaderId int = null,
 @ForeignKey nvarchar(60) = null,
 @LineNumber int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(100) = null,
 @SKUCode nvarchar(20) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Weight float = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null,
 @Additional6 nvarchar(510) = null,
 @Additional7 nvarchar(510) = null,
 @Additional8 nvarchar(510) = null,
 @Additional9 nvarchar(510) = null,
 @Additional10 nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportPODetail
        (InterfaceImportPOHeaderId,
         ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5,
         Additional6,
         Additional7,
         Additional8,
         Additional9,
         Additional10)
  select @InterfaceImportPOHeaderId,
         @ForeignKey,
         @LineNumber,
         @ProductCode,
         @Product,
         @SKUCode,
         @Batch,
         @Quantity,
         @Weight,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5,
         @Additional6,
         @Additional7,
         @Additional8,
         @Additional9,
         @Additional10 
  
  select @Error = @@Error
  
  
  return @Error
  
end
