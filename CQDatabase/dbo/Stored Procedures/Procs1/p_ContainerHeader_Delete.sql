﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerHeader_Delete
  ///   Filename       : p_ContainerHeader_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 Aug 2012 12:18:54
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the ContainerHeader table.
  /// </remarks>
  /// <param>
  ///   @ContainerHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerHeader_Delete
(
 @ContainerHeaderId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete ContainerHeader
     where ContainerHeaderId = @ContainerHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
