﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Exception_Label_Audit
  ///   Filename       : p_Exception_Label_Audit.sql
  ///   Create By      : Karen	
  ///   Date Created   : August 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Exception_Label_Audit
(
	@JobId			int,
	@InstructionId	int,
	@OperatorId		int,
	@LabelName		nvarchar(255)
)



as
begin
	 set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500),
          @GetDate           datetime,
          @Reasonid          int
  
  select @GetDate = dbo.ufn_Getdate()
  
  SET @Reasonid = (SELECT ReasonId FROM Reason WHERE ReasonCode = 'SWLA01')
  
  if @JobId = -1
    set @JobId = null
  
  if @InstructionId = -1
    set @InstructionId = null
  
  set @LabelName = REPLACE ( @LabelName , '.lbl' , '' )	
  
  begin transaction
 
  exec @Error = p_Exception_Insert
		 @JobId         = @JobId,
		 @InstructionId = @InstructionId,
		 @ReasonId      = @Reasonid,
		 @ExceptionCode = 'SWLA01',
		 @Exception     = @LabelName,
		 @Quantity      = 1,
		 @CreateDate    = @getdate,
		 @ExceptionDate = @getdate,
		 @OperatorId    = @OperatorId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Exception_Label_Audit'); 
    rollback transaction
    return @Error
end
