﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportIVDetail_Search
  ///   Filename       : p_InterfaceImportIVDetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:35
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportIVDetail table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportIVDetail.InterfaceImportIVHeaderId,
  ///   InterfaceImportIVDetail.ForeignKey,
  ///   InterfaceImportIVDetail.LineNumber,
  ///   InterfaceImportIVDetail.ProductCode,
  ///   InterfaceImportIVDetail.Product,
  ///   InterfaceImportIVDetail.SKUCode,
  ///   InterfaceImportIVDetail.Batch,
  ///   InterfaceImportIVDetail.Quantity,
  ///   InterfaceImportIVDetail.Weight,
  ///   InterfaceImportIVDetail.RetailPrice,
  ///   InterfaceImportIVDetail.NetPrice,
  ///   InterfaceImportIVDetail.LineTotal,
  ///   InterfaceImportIVDetail.Volume,
  ///   InterfaceImportIVDetail.Additional1,
  ///   InterfaceImportIVDetail.Additional2,
  ///   InterfaceImportIVDetail.Additional3,
  ///   InterfaceImportIVDetail.Additional4,
  ///   InterfaceImportIVDetail.Additional5 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportIVDetail_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportIVDetail.InterfaceImportIVHeaderId
        ,InterfaceImportIVDetail.ForeignKey
        ,InterfaceImportIVDetail.LineNumber
        ,InterfaceImportIVDetail.ProductCode
        ,InterfaceImportIVDetail.Product
        ,InterfaceImportIVDetail.SKUCode
        ,InterfaceImportIVDetail.Batch
        ,InterfaceImportIVDetail.Quantity
        ,InterfaceImportIVDetail.Weight
        ,InterfaceImportIVDetail.RetailPrice
        ,InterfaceImportIVDetail.NetPrice
        ,InterfaceImportIVDetail.LineTotal
        ,InterfaceImportIVDetail.Volume
        ,InterfaceImportIVDetail.Additional1
        ,InterfaceImportIVDetail.Additional2
        ,InterfaceImportIVDetail.Additional3
        ,InterfaceImportIVDetail.Additional4
        ,InterfaceImportIVDetail.Additional5
    from InterfaceImportIVDetail
  
end
