﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceField_Insert
  ///   Filename       : p_InterfaceField_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:46:46
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceField table.
  /// </remarks>
  /// <param>
  ///   @InterfaceFieldId int = null output,
  ///   @InterfaceFieldCode nvarchar(60) = null,
  ///   @InterfaceField nvarchar(100) = null,
  ///   @InterfaceDocumentTypeId int = null,
  ///   @Datatype nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   InterfaceField.InterfaceFieldId,
  ///   InterfaceField.InterfaceFieldCode,
  ///   InterfaceField.InterfaceField,
  ///   InterfaceField.InterfaceDocumentTypeId,
  ///   InterfaceField.Datatype 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceField_Insert
(
 @InterfaceFieldId int = null output,
 @InterfaceFieldCode nvarchar(60) = null,
 @InterfaceField nvarchar(100) = null,
 @InterfaceDocumentTypeId int = null,
 @Datatype nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceFieldId = '-1'
    set @InterfaceFieldId = null;
  
  if @InterfaceFieldCode = '-1'
    set @InterfaceFieldCode = null;
  
  if @InterfaceField = '-1'
    set @InterfaceField = null;
  
  if @InterfaceDocumentTypeId = '-1'
    set @InterfaceDocumentTypeId = null;
  
	 declare @Error int
 
  insert InterfaceField
        (InterfaceFieldCode,
         InterfaceField,
         InterfaceDocumentTypeId,
         Datatype)
  select @InterfaceFieldCode,
         @InterfaceField,
         @InterfaceDocumentTypeId,
         @Datatype 
  
  select @Error = @@Error, @InterfaceFieldId = scope_identity()
  
  
  return @Error
  
end
