﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportPODetail_Search
  ///   Filename       : p_InterfaceExportPODetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:06
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceExportPODetail table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceExportPODetail.InterfaceExportPOHeaderId,
  ///   InterfaceExportPODetail.ForeignKey,
  ///   InterfaceExportPODetail.LineNumber,
  ///   InterfaceExportPODetail.ProductCode,
  ///   InterfaceExportPODetail.Product,
  ///   InterfaceExportPODetail.SKUCode,
  ///   InterfaceExportPODetail.Batch,
  ///   InterfaceExportPODetail.Quantity,
  ///   InterfaceExportPODetail.Weight,
  ///   InterfaceExportPODetail.Additional1,
  ///   InterfaceExportPODetail.Additional2,
  ///   InterfaceExportPODetail.Additional3,
  ///   InterfaceExportPODetail.Additional4,
  ///   InterfaceExportPODetail.Additional5,
  ///   InterfaceExportPODetail.Additional6,
  ///   InterfaceExportPODetail.Additional7,
  ///   InterfaceExportPODetail.Additional8,
  ///   InterfaceExportPODetail.Additional9,
  ///   InterfaceExportPODetail.Additional10,
  ///   InterfaceExportPODetail.ReceiptLineId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportPODetail_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceExportPODetail.InterfaceExportPOHeaderId
        ,InterfaceExportPODetail.ForeignKey
        ,InterfaceExportPODetail.LineNumber
        ,InterfaceExportPODetail.ProductCode
        ,InterfaceExportPODetail.Product
        ,InterfaceExportPODetail.SKUCode
        ,InterfaceExportPODetail.Batch
        ,InterfaceExportPODetail.Quantity
        ,InterfaceExportPODetail.Weight
        ,InterfaceExportPODetail.Additional1
        ,InterfaceExportPODetail.Additional2
        ,InterfaceExportPODetail.Additional3
        ,InterfaceExportPODetail.Additional4
        ,InterfaceExportPODetail.Additional5
        ,InterfaceExportPODetail.Additional6
        ,InterfaceExportPODetail.Additional7
        ,InterfaceExportPODetail.Additional8
        ,InterfaceExportPODetail.Additional9
        ,InterfaceExportPODetail.Additional10
        ,InterfaceExportPODetail.ReceiptLineId
    from InterfaceExportPODetail
  
end
