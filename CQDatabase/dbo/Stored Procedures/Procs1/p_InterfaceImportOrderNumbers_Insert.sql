﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportOrderNumbers_Insert
  ///   Filename       : p_InterfaceImportOrderNumbers_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:50
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportOrderNumbers table.
  /// </remarks>
  /// <param>
  ///   @PrimaryKey nvarchar(60) = null,
  ///   @RecordStatus nvarchar(20) = null,
  ///   @OrderNumber nvarchar(60) = null,
  ///   @OrderDate datetime = null,
  ///   @Status nvarchar(60) = null,
  ///   @OutboundDocument bit = null,
  ///   @InboundDocument bit = null,
  ///   @ProcessedDate datetime = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportOrderNumbers.PrimaryKey,
  ///   InterfaceImportOrderNumbers.RecordStatus,
  ///   InterfaceImportOrderNumbers.OrderNumber,
  ///   InterfaceImportOrderNumbers.OrderDate,
  ///   InterfaceImportOrderNumbers.Status,
  ///   InterfaceImportOrderNumbers.OutboundDocument,
  ///   InterfaceImportOrderNumbers.InboundDocument,
  ///   InterfaceImportOrderNumbers.ProcessedDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportOrderNumbers_Insert
(
 @PrimaryKey nvarchar(60) = null,
 @RecordStatus nvarchar(20) = null,
 @OrderNumber nvarchar(60) = null,
 @OrderDate datetime = null,
 @Status nvarchar(60) = null,
 @OutboundDocument bit = null,
 @InboundDocument bit = null,
 @ProcessedDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportOrderNumbers
        (PrimaryKey,
         RecordStatus,
         OrderNumber,
         OrderDate,
         Status,
         OutboundDocument,
         InboundDocument,
         ProcessedDate)
  select @PrimaryKey,
         @RecordStatus,
         @OrderNumber,
         @OrderDate,
         @Status,
         @OutboundDocument,
         @InboundDocument,
         @ProcessedDate 
  
  select @Error = @@Error
  
  
  return @Error
  
end
