﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperatorGroup_Delete
  ///   Filename       : p_AreaOperatorGroup_Delete.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2011
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the AreaOperatorGroup table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_AreaOperatorGroup_Delete
(
	@OperatorGroupId		int = null,
	@AreaId 			    int = null
)
as
begin
	 set nocount on
	 declare @Error int


  delete AreaOperatorGroup
     where @OperatorGroupId = OperatorGroupId and @AreaId = AreaId
  
  select @Error = @@Error
   
end
