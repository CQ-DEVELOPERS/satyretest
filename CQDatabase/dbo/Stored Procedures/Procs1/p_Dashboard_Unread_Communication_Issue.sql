﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Unread_Communication_Issue
  ///   Filename       : p_Dashboard_Unread_Communication_Issue.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Unread_Communication_Issue
(
 @WarehouseId int = null,
 @Summarise   bit = 0,
 @OperatorId  int = null
)

as
begin
	 set nocount on;
  
  declare @ExternalCompanyId int,
          @PrincipalId       int,
          @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @ExternalCompanyId = ExternalCompanyId,
         @PrincipalId       = PrincipalId
    from Operator (nolock)
   where OperatorId = @OperatorId
	 
	 select od.OrderNumber,
	        c.Comment,
	        c.CreateDate,
	        o.Operator
	   from OutboundDocument      od (nolock)
	   join Issue                  i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
	   join CommentLink           cl (nolock) on cl.TableId = i.IssueId
	   join Comment                c (nolock) on cl.CommentId = c.CommentId
	   left
	   join Operator               o (nolock) on c.OperatorId = o.OperatorId
   where (od.PrincipalId = @PrincipalId or @PrincipalId is null)
     and od.ExternalCompanyId = isnull(@ExternalCompanyId, od.ExternalCompanyId)
     and c.ReadDate is null
	 
	 update c
	    set ReadDate = @GetDate
	   from OutboundDocument      od (nolock)
	   join Issue                  i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
	   join CommentLink           cl (nolock) on cl.TableId = i.IssueId
	   join Comment                c (nolock) on cl.CommentId = c.CommentId
	   left
	   join Operator               o (nolock) on c.OperatorId = o.OperatorId
   where (od.PrincipalId = @PrincipalId or @PrincipalId is null)
     and od.ExternalCompanyId = isnull(@ExternalCompanyId, od.ExternalCompanyId)
end
