﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceTables_Select
  ///   Filename       : p_InterfaceTables_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:21:50
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceTables table.
  /// </remarks>
  /// <param>
  ///   @InterfaceTableId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceTables.InterfaceTableId,
  ///   InterfaceTables.HeaderTable,
  ///   InterfaceTables.DetailTable,
  ///   InterfaceTables.HeaderTableKey,
  ///   InterfaceTables.Description1,
  ///   InterfaceTables.Description2,
  ///   InterfaceTables.HeaderField1,
  ///   InterfaceTables.HeaderField2,
  ///   InterfaceTables.HeaderField3,
  ///   InterfaceTables.HeaderField4,
  ///   InterfaceTables.HeaderField5,
  ///   InterfaceTables.HeaderField6,
  ///   InterfaceTables.HeaderField7,
  ///   InterfaceTables.HeaderField8,
  ///   InterfaceTables.DetailField1,
  ///   InterfaceTables.DetailField2,
  ///   InterfaceTables.DetailField3,
  ///   InterfaceTables.DetailField4,
  ///   InterfaceTables.DetailField5,
  ///   InterfaceTables.DetailField6,
  ///   InterfaceTables.DetailField7,
  ///   InterfaceTables.DetailField8,
  ///   InterfaceTables.StartId,
  ///   InterfaceTables.InsertDetail,
  ///   InterfaceTables.GroupByKey,
  ///   InterfaceTables.AltGroupByKey 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceTables_Select
(
 @InterfaceTableId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceTables.InterfaceTableId
        ,InterfaceTables.HeaderTable
        ,InterfaceTables.DetailTable
        ,InterfaceTables.HeaderTableKey
        ,InterfaceTables.Description1
        ,InterfaceTables.Description2
        ,InterfaceTables.HeaderField1
        ,InterfaceTables.HeaderField2
        ,InterfaceTables.HeaderField3
        ,InterfaceTables.HeaderField4
        ,InterfaceTables.HeaderField5
        ,InterfaceTables.HeaderField6
        ,InterfaceTables.HeaderField7
        ,InterfaceTables.HeaderField8
        ,InterfaceTables.DetailField1
        ,InterfaceTables.DetailField2
        ,InterfaceTables.DetailField3
        ,InterfaceTables.DetailField4
        ,InterfaceTables.DetailField5
        ,InterfaceTables.DetailField6
        ,InterfaceTables.DetailField7
        ,InterfaceTables.DetailField8
        ,InterfaceTables.StartId
        ,InterfaceTables.InsertDetail
        ,InterfaceTables.GroupByKey
        ,InterfaceTables.AltGroupByKey
    from InterfaceTables
   where isnull(InterfaceTables.InterfaceTableId,'0')  = isnull(@InterfaceTableId, isnull(InterfaceTables.InterfaceTableId,'0'))
  
end
