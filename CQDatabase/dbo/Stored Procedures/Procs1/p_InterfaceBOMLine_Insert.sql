﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceBOMLine_Insert
  ///   Filename       : p_InterfaceBOMLine_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:47
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceBOMLine table.
  /// </remarks>
  /// <param>
  ///   @InterfaceBOMId int = null,
  ///   @ParentProductCode nvarchar(100) = null,
  ///   @LineNumber int = null,
  ///   @ProductCode nvarchar(100) = null,
  ///   @ProductDescription nvarchar(510) = null,
  ///   @Quantity float = null,
  ///   @Units nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   InterfaceBOMLine.InterfaceBOMId,
  ///   InterfaceBOMLine.ParentProductCode,
  ///   InterfaceBOMLine.LineNumber,
  ///   InterfaceBOMLine.ProductCode,
  ///   InterfaceBOMLine.ProductDescription,
  ///   InterfaceBOMLine.Quantity,
  ///   InterfaceBOMLine.Units 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceBOMLine_Insert
(
 @InterfaceBOMId int = null,
 @ParentProductCode nvarchar(100) = null,
 @LineNumber int = null,
 @ProductCode nvarchar(100) = null,
 @ProductDescription nvarchar(510) = null,
 @Quantity float = null,
 @Units nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceBOMLine
        (InterfaceBOMId,
         ParentProductCode,
         LineNumber,
         ProductCode,
         ProductDescription,
         Quantity,
         Units)
  select @InterfaceBOMId,
         @ParentProductCode,
         @LineNumber,
         @ProductCode,
         @ProductDescription,
         @Quantity,
         @Units 
  
  select @Error = @@Error
  
  
  return @Error
  
end
