﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOM_Packaging_Search
  ///   Filename       : p_BOM_Packaging_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 05 Dec 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOM_Packaging_Search
(
 @bomInstructionId int
)

as
begin
	 set nocount on;
  
  select bp.BOMPackagingId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         bp.Quantity
    from BOMPackaging bp (nolock)
    join StorageUnit  su (nolock) on bp.StorageUnitId = su.StorageUnitId
    join Product       p (nolock) on su.ProductId = p.ProductId
    join SKU         sku (nolock) on su.SKUId = sku.SKUId
   where bp.BOMInstructionId = @bomInstructionId
end
