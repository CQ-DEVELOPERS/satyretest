﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLine_Update
  ///   Filename       : p_InboundLine_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 08:48:43
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InboundLine table.
  /// </remarks>
  /// <param>
  ///   @InboundLineId int = null,
  ///   @InboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null,
  ///   @BatchId int = null,
  ///   @UnitPrice numeric(13,2) = null,
  ///   @ReasonId int = null,
  ///   @ChildStorageUnitId int = null,
  ///   @BOELineNumber nvarchar(100) = null,
  ///   @TariffCode nvarchar(100) = null,
  ///   @CountryofOrigin nvarchar(200) = null,
  ///   @Reference1 nvarchar(100) = null,
  ///   @Reference2 nvarchar(100) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLine_Update
(
 @InboundLineId int = null,
 @InboundDocumentId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @LineNumber int = null,
 @Quantity float = null,
 @BatchId int = null,
 @UnitPrice numeric(13,2) = null,
 @ReasonId int = null,
 @ChildStorageUnitId int = null,
 @BOELineNumber nvarchar(100) = null,
 @TariffCode nvarchar(100) = null,
 @CountryofOrigin nvarchar(200) = null,
 @Reference1 nvarchar(100) = null,
 @Reference2 nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @InboundLineId = '-1'
    set @InboundLineId = null;
  
  if @InboundDocumentId = '-1'
    set @InboundDocumentId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @ChildStorageUnitId = '-1'
    set @ChildStorageUnitId = null;
  
	 declare @Error int
 
  update InboundLine
     set InboundDocumentId = isnull(@InboundDocumentId, InboundDocumentId),
         StorageUnitId = isnull(@StorageUnitId, StorageUnitId),
         StatusId = isnull(@StatusId, StatusId),
         LineNumber = isnull(@LineNumber, LineNumber),
         Quantity = isnull(@Quantity, Quantity),
         BatchId = isnull(@BatchId, BatchId),
         UnitPrice = isnull(@UnitPrice, UnitPrice),
         ReasonId = isnull(@ReasonId, ReasonId),
         ChildStorageUnitId = isnull(@ChildStorageUnitId, ChildStorageUnitId),
         BOELineNumber = isnull(@BOELineNumber, BOELineNumber),
         TariffCode = isnull(@TariffCode, TariffCode),
         CountryofOrigin = isnull(@CountryofOrigin, CountryofOrigin),
         Reference1 = isnull(@Reference1, Reference1),
         Reference2 = isnull(@Reference2, Reference2) 
   where InboundLineId = @InboundLineId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_InboundLineHistory_Insert
         @InboundLineId = @InboundLineId,
         @InboundDocumentId = @InboundDocumentId,
         @StorageUnitId = @StorageUnitId,
         @StatusId = @StatusId,
         @LineNumber = @LineNumber,
         @Quantity = @Quantity,
         @BatchId = @BatchId,
         @UnitPrice = @UnitPrice,
         @ReasonId = @ReasonId,
         @ChildStorageUnitId = @ChildStorageUnitId,
         @BOELineNumber = @BOELineNumber,
         @TariffCode = @TariffCode,
         @CountryofOrigin = @CountryofOrigin,
         @Reference1 = @Reference1,
         @Reference2 = @Reference2,
         @CommandType = 'Update'
  
  return @Error
  
end
