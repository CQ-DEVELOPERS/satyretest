﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Issue_Delete
  ///   Filename       : p_Issue_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 13 Feb 2015 09:30:35
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Issue table.
  /// </remarks>
  /// <param>
  ///   @IssueId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Issue_Delete
(
 @IssueId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Issue
     where IssueId = @IssueId
  
  select @Error = @@Error
  
  if @Error = 0
    exec @Error = p_IssueHistory_Insert
         @IssueId
        ,@CommandType = 'Delete'
  
  return @Error
  
end
