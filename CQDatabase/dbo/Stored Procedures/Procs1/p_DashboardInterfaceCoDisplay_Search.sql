﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DashboardInterfaceCoDisplay_Search
  ///   Filename       : p_DashboardInterfaceCoDisplay_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:33
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the DashboardInterfaceCoDisplay table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   DashboardInterfaceCoDisplay.CompanyId,
  ///   DashboardInterfaceCoDisplay.DisplayCode,
  ///   DashboardInterfaceCoDisplay.DisplayDesc,
  ///   DashboardInterfaceCoDisplay.Display,
  ///   DashboardInterfaceCoDisplay.Counter,
  ///   DashboardInterfaceCoDisplay.ReportName,
  ///   DashboardInterfaceCoDisplay.ExtractDescription,
  ///   DashboardInterfaceCoDisplay.Show 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DashboardInterfaceCoDisplay_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         DashboardInterfaceCoDisplay.CompanyId
        ,DashboardInterfaceCoDisplay.DisplayCode
        ,DashboardInterfaceCoDisplay.DisplayDesc
        ,DashboardInterfaceCoDisplay.Display
        ,DashboardInterfaceCoDisplay.Counter
        ,DashboardInterfaceCoDisplay.ReportName
        ,DashboardInterfaceCoDisplay.ExtractDescription
        ,DashboardInterfaceCoDisplay.Show
    from DashboardInterfaceCoDisplay
  
end
