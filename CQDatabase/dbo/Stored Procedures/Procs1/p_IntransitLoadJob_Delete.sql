﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_IntransitLoadJob_Delete
  ///   Filename       : p_IntransitLoadJob_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:35
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the IntransitLoadJob table.
  /// </remarks>
  /// <param>
  ///   @IntransitLoadJobId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_IntransitLoadJob_Delete
(
 @IntransitLoadJobId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete IntransitLoadJob
     where IntransitLoadJobId = @IntransitLoadJobId
  
  select @Error = @@Error
  
  
  return @Error
  
end
