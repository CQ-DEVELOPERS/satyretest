﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_SUP
  ///   Filename       : p_Interface_WebService_SUP.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Mar 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Interface_WebService_SUP
(
 @XMLBody nvarchar(max) output,
 @principalCode nvarchar(30) = null
)
--with encryption
as
begin
    Set Nocount On;
	
	DECLARE @doc xml
	DECLARE @idoc int
	DECLARE @InsertDate datetime
	DECLARE @ERROR AS VARCHAR(255)
	
	SELECT @doc = convert(xml,@XMLBody)
	     , @InsertDate = Getdate()
	     
	EXEC sp_xml_preparedocument @idoc OUTPUT, @doc
	
	BEGIN TRY
		                
	    INSERT INTO [InterfaceSupplier]
               ([RecordStatus]
               ,[InsertDate]
               ,[HostId]
               ,[SupplierCode]
               ,[SupplierName]
               ,[Address1]
               ,[Address2]
               ,[Address3]
               ,[Address4]
               ,[Modified]
               ,[ContactPerson]
               ,[Phone]
               ,[Fax]
               ,[Email]
               ,[PrincipalCode]
               --,[ProcessedDate]
               )    
	    SELECT  'W'
	           ,@InsertDate
	           ,nodes.entity.value('HostId[1]',             'varchar(30)')  'HostId'
	           ,nodes.entity.value('SupplierCode[1]',       'varchar(30)')  'SupplierCode'
               ,nodes.entity.value('SupplierName[1]',       'varchar(255)') 'SupplierName'
               ,nodes.entity.value('Address1[1]',           'varchar(255)') 'Address1'
               ,nodes.entity.value('Address2[1]',           'varchar(255)') 'Address2'
               ,nodes.entity.value('Address3[1]',           'varchar(255)') 'Address3'
               ,nodes.entity.value('Address4[1]',           'varchar(255)') 'Address4'
               ,nodes.entity.value('Modified[1]',           'varchar(255)') 'Modified'
               ,nodes.entity.value('ContactPerson[1]',      'varchar(100)') 'ContactPerson'
               ,nodes.entity.value('Phone[1]',              'varchar(255)') 'Phone'
               ,nodes.entity.value('Fax[1]',                'varchar(255)') 'Fax'
               ,nodes.entity.value('Email[1]',              'varchar(255)') 'Email'               
               --,nodes.entity.value('ProcessedDate[1]',      'datetime	       
               ,@principalCode
	    FROM
	            @doc.nodes('/root/Body/Item') AS nodes(entity)
    	
    	
	    INSERT INTO [InterfaceSupplierProduct]
               (RecordStatus
               ,ProcessedDate
               ,SupplierCode
               ,ProductCode
                )
     	    SELECT 				
     	        'W'
     	       ,@InsertDate							
	           ,nodes.entity.value('ForeignKey[1]',         'varchar(30)')  'ForeignKey'
               ,nodes.entity.value('ProductCode[1]',        'varchar(30)')  'ProductCode'
               
        FROM
	            @doc.nodes('/root/Body/Item/ItemLine') AS nodes(entity)
    				
	    Update sp
	    set RecordStatus = 'N'
	    from InterfaceSupplier s
		    Join InterfaceSupplierProduct sp on s.SupplierCode = sp.SupplierCode
	    where s.RecordStatus = 'W' and s.InsertDate = @InsertDate 
	    
	    Update s
	    Set RecordStatus = 'N'
	    From InterfaceSupplier s
	    Where s.RecordStatus = 'W' and s.InsertDate = @InsertDate 
	     
	END TRY
	BEGIN CATCH
	    
	   SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)
	END CATCH
	
	--exec p_Pastel_Import_Supplier
	
	SET @XMLBody = '<root><status>' 
	    + CASE WHEN LEN(@ERROR) > 0 THEN @ERROR
	           ELSE 'Supplier(s) successfully imported'
	      END
	    + '</status></root>'
end
