﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Comment_Link_Insert
  ///   Filename       : p_Comment_Link_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jan 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Comment_Link_Insert
(
 @tableId         int,
 @tableName       sysname = null,
 @comment         nvarchar(max),
 @operatorId      int
)

as
begin
	 set nocount on;
  
  declare @Error                  int,
          @Errormsg               varchar(500),
          @GetDate                datetime,
          @CommentId              int,
          @CommentLineId         int
  
  select @GetDate = dbo.ufn_Getdate()
  
  begin transaction
  
  insert Comment
        (Comment,
         OperatorId,
         CreateDate)
  select @Comment,
         @OperatorId,
         @GetDate
  
  select @CommentId = scope_identity(), @Error = @@error
  
  if @Error <> 0
    goto error
  
  insert CommentLink
        (CommentId,
         TableId,
         TableName)
  select @CommentId,
         @tableId,
         @tableName
  
  select @CommentLineId = scope_identity(), @Error = @@error
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Comment_Link_Insert'); 
    rollback transaction
    return @Error
end
