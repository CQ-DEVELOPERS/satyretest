﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompanyTypeHistory_Insert
  ///   Filename       : p_ExternalCompanyTypeHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:19:08
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ExternalCompanyTypeHistory table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyTypeId int = null,
  ///   @ExternalCompanyType nvarchar(100) = null,
  ///   @ExternalCompanyTypeCode nvarchar(20) = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  ///   ExternalCompanyTypeHistory.ExternalCompanyTypeId,
  ///   ExternalCompanyTypeHistory.ExternalCompanyType,
  ///   ExternalCompanyTypeHistory.ExternalCompanyTypeCode,
  ///   ExternalCompanyTypeHistory.CommandType,
  ///   ExternalCompanyTypeHistory.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompanyTypeHistory_Insert
(
 @ExternalCompanyTypeId int = null,
 @ExternalCompanyType nvarchar(100) = null,
 @ExternalCompanyTypeCode nvarchar(20) = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert ExternalCompanyTypeHistory
        (ExternalCompanyTypeId,
         ExternalCompanyType,
         ExternalCompanyTypeCode,
         CommandType,
         InsertDate)
  select @ExternalCompanyTypeId,
         @ExternalCompanyType,
         @ExternalCompanyTypeCode,
         @CommandType,
         isnull(@InsertDate, getdate()) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
