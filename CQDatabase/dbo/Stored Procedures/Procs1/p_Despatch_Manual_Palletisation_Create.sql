﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Despatch_Manual_Palletisation_Create
  ///   Filename       : p_Despatch_Manual_Palletisation_Create.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Despatch_Manual_Palletisation_Create
(
 @IssueLineId   int,
 @NumberOfLines int
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @LineQuantity       float,
          @IssueId          int,
          @WarehouseId        int,
          @PickLocationId     int,
          @StorageUnitBatchId int,
          @AcceptedQuantity   numeric(10),
          @PalletQuantity     numeric(10),
          @PalletCount        int,
          @RemainingQuantity  float,
          @StatusId           int
  
  select @GetDate = dbo.ufn_Getdate()
  set @Errormsg = 'Error executing p_Despatch_Manual_Palletisation_Create'
  
  if exists(select 1 from Job (nolock) where IssueLineId = @IssueLineId)
  begin
    begin transaction
    set @Error = -1
    set @Errormsg = 'This Issue Line has already been palletised (p_Despatch_Manual_Palletisation_Create).'
    goto error
  end
  
  select @StatusId = StatusId
    from Status
   where StatusCode = 'W'
     and Type       = 'IS';
  
  select @IssueId            = r.IssueId,
         @WarehouseId        = r.WarehouseId,
         @PickLocationId     = r.LocationId,
         @StorageUnitBatchId = rl.StorageUnitBatchId,
         @AcceptedQuantity   = rl.Quantity
    from IssueLine rl (nolock)
    join Issue     r  (nolock) on rl.IssueId = r.IssueID
   where IssueLineId = @IssueLineId
     and rl.StatusId   = @StatusId
  
  if @StorageUnitBatchId is null
    return -1
  
  select top 1 @PalletQuantity = p.Quantity
    from StorageUnitBatch sub (nolock)
    join Pack             p   (nolock) on sub.StorageUnitId = p.StorageUnitId
    join PackType         pt  (nolock) on p.PackTypeId      = pt.PackTypeId
   where StorageUnitBatchId = @StorageUnitBatchId
  order by InboundSequence
  
  begin transaction
  
  if (@AcceptedQuantity / @PalletQuantity) > @NumberOfLines
  begin
    set @Error = 100
    set @Errormsg = 'p_Despatch_Manual_Palletisation_Create - Cannot put more than a full pallet quantity onto single line'
    goto error
  end
    
  if @AcceptedQuantity < @NumberOfLines 
  begin
    set @Error = 200
    set @Errormsg = 'p_Despatch_Manual_Palletisation_Create - Cannot put a Quantity of ' + convert(nvarchar(10), @AcceptedQuantity) + ' on ' + convert(nvarchar(10), @NumberOfLines) + ' lines'
    goto error
  end
  
  if @AcceptedQuantity > @PalletQuantity
  begin
    set @PalletCount = floor(@AcceptedQuantity / @PalletQuantity)
    set @RemainingQuantity = @AcceptedQuantity - (@PalletQuantity * @PalletCount)
    
    if @PalletCount > 0
    begin
      while @PalletCount > 0
      begin
        set @PalletCount = @PalletCount - 1
        set @NumberOfLines = @NumberOfLines - 1
        
        exec @Error = p_Palletised_Insert
         @WarehouseId         = @WarehouseId,
         @OperatorId          = null,
         @InstructionTypeCode = 'S', -- Store
         @StorageUnitBatchId  = @StorageUnitBatchId,
         @PickLocationId      = @PickLocationId,
         @StoreLocationId     = null,
         @Quantity            = @PalletQuantity,
         @IssueLineId       = @IssueLineId
        
        if @error <> 0
          goto error
      end
    end
  end
  
  if @RemainingQuantity is null
  begin
    set @LineQuantity      = floor(@AcceptedQuantity / @NumberOfLines)
    set @RemainingQuantity = @AcceptedQuantity - (floor(@AcceptedQuantity / @NumberOfLines) * @NumberOfLines)
  end
  else
  begin
    set @LineQuantity = @RemainingQuantity / @NumberOfLines
    set @NumberOfLines = @NumberOfLines - 1
  end
  
  while @NumberOfLines > 0
  begin
    set @NumberOfLines = @NumberOfLines -1
    
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @InstructionTypeCode = 'S', -- Store
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = null,
     @Quantity            = @LineQuantity,
     @IssueLineId       = @IssueLineId

    if @error <> 0
      goto error
  end
  
  if @RemainingQuantity > 0
  begin
    exec @Error = p_Palletised_Insert
     @WarehouseId         = @WarehouseId,
     @OperatorId          = null,
     @InstructionTypeCode = 'S', -- Store
     @StorageUnitBatchId  = @StorageUnitBatchId,
     @PickLocationId      = @PickLocationId,
     @StoreLocationId     = null,
     @Quantity            = @RemainingQuantity,
     @IssueLineId       = @IssueLineId

    if @error <> 0
      goto error
  end
  
  select @StatusId = StatusId
    from Status
   where StatusCode = 'P' -- Palletised
     and Type       = 'IS'
  
  exec @Error = p_IssueLine_Update
   @IssueLineId = @IssueLineId,
   @StatusId      = @StatusId
  
  if @error <> 0
    goto error
  
  exec @Error = p_Issue_Update_Status
   @IssueId = @IssueId,
   @StatusId  = @StatusId
  
  if @error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end
