﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Supplier_Portal_Exceptions
  ///   Filename       : p_Dashboard_Supplier_Portal_Exceptions.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Supplier_Portal_Exceptions
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)

as
begin
	 set nocount on;
	 
	 select 'Amended Quantity' as 'Legend',
	        3 as 'Value'
	 union
	 select 'Amended delivery dates' as 'Legend',
	        6 as 'Value'
	 union
	 select 'Late deliveries' as 'Legend',
	        1 as 'Value'
end
