﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceFileType_Delete
  ///   Filename       : p_InterfaceFileType_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Feb 2014 10:47:28
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceFileType table.
  /// </remarks>
  /// <param>
  ///   @InterfaceFileTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceFileType_Delete
(
 @InterfaceFileTypeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceFileType
     where InterfaceFileTypeId = @InterfaceFileTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
