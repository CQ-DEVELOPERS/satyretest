﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ChildProduct_List
  ///   Filename       : p_ChildProduct_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:20
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ChildProduct table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   ChildProduct.ChildProductId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ChildProduct_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as ChildProductId
        ,null as 'ChildProduct'
  union
  select
         ChildProduct.ChildProductId
        ,ChildProduct.ChildProductId as 'ChildProduct'
    from ChildProduct
  
end
