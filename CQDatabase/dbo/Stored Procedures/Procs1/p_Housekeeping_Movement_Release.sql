﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Movement_Release
  ///   Filename       : p_Housekeeping_Movement_Release.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Aug 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Movement_Release
(
 @instructionId int,
 @operatorId    int,
 @statusCode    nvarchar(10)
)

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           nvarchar(500),
          @GetDate            datetime,
          @StatusId           int,
          @JobId              int
          
  select @GetDate = dbo.ufn_Getdate()
  
  if @operatorId = -1
    set @operatorId = null
  
  select @JobId = JobId
    from Instruction (nolock)
   where InstructionId = @instructionId
  
  begin transaction
  
  select @StatusId = dbo.ufn_StatusId('J',@statusCode)
  
  update Job
     set StatusId   = @StatusId,
         OperatorId = null
   where JobId       = @jobId
  
  select @Error = @@Error
  
  if @Error <> 0
    goto error
  
  exec @Error = p_JobHistory_Insert
   @JobId       = @jobId,
   @StatusId    = @StatusId,
   @OperatorId  = @operatorId,
   @CommandType = 'Update',
   @InsertDate  = @GetDate
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Housekeeping_Movement_Release'); 
    rollback transaction
    return @Error
end
