﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Comment_Insert
  ///   Filename       : p_Comment_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 19 Feb 2014 08:27:22
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the Comment table.
  /// </remarks>
  /// <param>
  ///   @CommentId int = null output,
  ///   @Comment nvarchar(max) = null,
  ///   @OperatorId int = null,
  ///   @CreateDate datetime = null,
  ///   @ModifiedDate datetime = null,
  ///   @ReadDate datetime = null 
  /// </param>
  /// <returns>
  ///   Comment.CommentId,
  ///   Comment.Comment,
  ///   Comment.OperatorId,
  ///   Comment.CreateDate,
  ///   Comment.ModifiedDate,
  ///   Comment.ReadDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Comment_Insert
(
 @CommentId int = null output,
 @Comment nvarchar(max) = null,
 @OperatorId int = null,
 @CreateDate datetime = null,
 @ModifiedDate datetime = null,
 @ReadDate datetime = null 
)

as
begin
	 set nocount on;
  
  if @CommentId = '-1'
    set @CommentId = null;
  
  if @Comment = '-1'
    set @Comment = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
	 declare @Error int
 
  insert Comment
        (Comment,
         OperatorId,
         CreateDate,
         ModifiedDate,
         ReadDate)
  select @Comment,
         @OperatorId,
         @CreateDate,
         @ModifiedDate,
         @ReadDate 
  
  select @Error = @@Error, @CommentId = scope_identity()
  
  
  return @Error
  
end
