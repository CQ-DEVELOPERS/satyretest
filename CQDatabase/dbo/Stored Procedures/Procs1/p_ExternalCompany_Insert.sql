﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Insert
  ///   Filename       : p_ExternalCompany_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:44:46
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the ExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyId int = null output,
  ///   @ExternalCompanyTypeId int = null,
  ///   @RouteId int = null,
  ///   @ExternalCompany nvarchar(510) = null,
  ///   @ExternalCompanyCode nvarchar(60) = null,
  ///   @Rating int = null,
  ///   @RedeliveryIndicator bit = null,
  ///   @QualityAssuranceIndicator bit = null,
  ///   @ContainerTypeId int = null,
  ///   @Backorder bit = null,
  ///   @HostId nvarchar(60) = null,
  ///   @OrderLineSequence bit = null,
  ///   @DropSequence int = null,
  ///   @AutoInvoice bit = null,
  ///   @OneProductPerPallet bit = null,
  ///   @PrincipalId int = null,
  ///   @ProcessId int = null,
  ///   @TrustedDelivery bit = null,
  ///   @OutboundSLAHours int = null,
  ///   @PricingCategoryId int = null,
  ///   @LabelingCategoryId int = null 
  /// </param>
  /// <returns>
  ///   ExternalCompany.ExternalCompanyId,
  ///   ExternalCompany.ExternalCompanyTypeId,
  ///   ExternalCompany.RouteId,
  ///   ExternalCompany.ExternalCompany,
  ///   ExternalCompany.ExternalCompanyCode,
  ///   ExternalCompany.Rating,
  ///   ExternalCompany.RedeliveryIndicator,
  ///   ExternalCompany.QualityAssuranceIndicator,
  ///   ExternalCompany.ContainerTypeId,
  ///   ExternalCompany.Backorder,
  ///   ExternalCompany.HostId,
  ///   ExternalCompany.OrderLineSequence,
  ///   ExternalCompany.DropSequence,
  ///   ExternalCompany.AutoInvoice,
  ///   ExternalCompany.OneProductPerPallet,
  ///   ExternalCompany.PrincipalId,
  ///   ExternalCompany.ProcessId,
  ///   ExternalCompany.TrustedDelivery,
  ///   ExternalCompany.OutboundSLAHours,
  ///   ExternalCompany.PricingCategoryId,
  ///   ExternalCompany.LabelingCategoryId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Insert
(
 @ExternalCompanyId int = null output,
 @ExternalCompanyTypeId int = null,
 @RouteId int = null,
 @ExternalCompany nvarchar(510) = null,
 @ExternalCompanyCode nvarchar(60) = null,
 @Rating int = null,
 @RedeliveryIndicator bit = null,
 @QualityAssuranceIndicator bit = null,
 @ContainerTypeId int = null,
 @Backorder bit = null,
 @HostId nvarchar(60) = null,
 @OrderLineSequence bit = null,
 @DropSequence int = null,
 @AutoInvoice bit = null,
 @OneProductPerPallet bit = null,
 @PrincipalId int = null,
 @ProcessId int = null,
 @TrustedDelivery bit = null,
 @OutboundSLAHours int = null,
 @PricingCategoryId int = null,
 @LabelingCategoryId int = null 
)

as
begin
	 set nocount on;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @ExternalCompanyTypeId = '-1'
    set @ExternalCompanyTypeId = null;
  
  if @RouteId = '-1'
    set @RouteId = null;
  
  if @ExternalCompany = '-1'
    set @ExternalCompany = null;
  
  if @ExternalCompanyCode = '-1'
    set @ExternalCompanyCode = null;
  
  if @ContainerTypeId = '-1'
    set @ContainerTypeId = null;
  
  if @PrincipalId = '-1'
    set @PrincipalId = null;
  
  if @ProcessId = '-1'
    set @ProcessId = null;
  
  if @PricingCategoryId = '-1'
    set @PricingCategoryId = null;
  
  if @LabelingCategoryId = '-1'
    set @LabelingCategoryId = null;
  
	 declare @Error int
 
  insert ExternalCompany
        (ExternalCompanyTypeId,
         RouteId,
         ExternalCompany,
         ExternalCompanyCode,
         Rating,
         RedeliveryIndicator,
         QualityAssuranceIndicator,
         ContainerTypeId,
         Backorder,
         HostId,
         OrderLineSequence,
         DropSequence,
         AutoInvoice,
         OneProductPerPallet,
         PrincipalId,
         ProcessId,
         TrustedDelivery,
         OutboundSLAHours,
         PricingCategoryId,
         LabelingCategoryId)
  select @ExternalCompanyTypeId,
         @RouteId,
         @ExternalCompany,
         @ExternalCompanyCode,
         @Rating,
         @RedeliveryIndicator,
         @QualityAssuranceIndicator,
         @ContainerTypeId,
         @Backorder,
         @HostId,
         @OrderLineSequence,
         @DropSequence,
         @AutoInvoice,
         @OneProductPerPallet,
         @PrincipalId,
         @ProcessId,
         @TrustedDelivery,
         @OutboundSLAHours,
         @PricingCategoryId,
         @LabelingCategoryId 
  
  select @Error = @@Error, @ExternalCompanyId = scope_identity()
  
  
  return @Error
  
end
 
