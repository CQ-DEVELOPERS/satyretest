﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLineHistory_Insert
  ///   Filename       : p_InboundLineHistory_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 08:48:42
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundLineHistory table.
  /// </remarks>
  /// <param>
  ///   @InboundLineId int = null,
  ///   @InboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null,
  ///   @BatchId int = null,
  ///   @CommandType nvarchar(20) = null,
  ///   @InsertDate datetime = null,
  ///   @ReasonId int = null,
  ///   @UnitPrice numeric(13,2) = null,
  ///   @ChildStorageUnitId int = null,
  ///   @BOELineNumber nvarchar(100) = null,
  ///   @TariffCode nvarchar(100) = null,
  ///   @CountryofOrigin nvarchar(200) = null,
  ///   @Reference1 nvarchar(100) = null,
  ///   @Reference2 nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   InboundLineHistory.InboundLineId,
  ///   InboundLineHistory.InboundDocumentId,
  ///   InboundLineHistory.StorageUnitId,
  ///   InboundLineHistory.StatusId,
  ///   InboundLineHistory.LineNumber,
  ///   InboundLineHistory.Quantity,
  ///   InboundLineHistory.BatchId,
  ///   InboundLineHistory.CommandType,
  ///   InboundLineHistory.InsertDate,
  ///   InboundLineHistory.ReasonId,
  ///   InboundLineHistory.UnitPrice,
  ///   InboundLineHistory.ChildStorageUnitId,
  ///   InboundLineHistory.BOELineNumber,
  ///   InboundLineHistory.TariffCode,
  ///   InboundLineHistory.CountryofOrigin,
  ///   InboundLineHistory.Reference1,
  ///   InboundLineHistory.Reference2 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLineHistory_Insert
(
 @InboundLineId int = null,
 @InboundDocumentId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @LineNumber int = null,
 @Quantity float = null,
 @BatchId int = null,
 @CommandType nvarchar(20) = null,
 @InsertDate datetime = null,
 @ReasonId int = null,
 @UnitPrice numeric(13,2) = null,
 @ChildStorageUnitId int = null,
 @BOELineNumber nvarchar(100) = null,
 @TariffCode nvarchar(100) = null,
 @CountryofOrigin nvarchar(200) = null,
 @Reference1 nvarchar(100) = null,
 @Reference2 nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InboundLineHistory
        (InboundLineId,
         InboundDocumentId,
         StorageUnitId,
         StatusId,
         LineNumber,
         Quantity,
         BatchId,
         CommandType,
         InsertDate,
         ReasonId,
         UnitPrice,
         ChildStorageUnitId,
         BOELineNumber,
         TariffCode,
         CountryofOrigin,
         Reference1,
         Reference2)
  select @InboundLineId,
         @InboundDocumentId,
         @StorageUnitId,
         @StatusId,
         @LineNumber,
         @Quantity,
         @BatchId,
         @CommandType,
         isnull(@InsertDate, getdate()),
         @ReasonId,
         @UnitPrice,
         @ChildStorageUnitId,
         @BOELineNumber,
         @TariffCode,
         @CountryofOrigin,
         @Reference1,
         @Reference2 
  
  select @Error = @@Error
  
  
  return @Error
  
end
