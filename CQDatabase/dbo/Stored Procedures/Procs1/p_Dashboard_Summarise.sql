﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Summarise
  ///   Filename       : p_Dashboard_Summarise.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Oct 2015
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Summarise

as
begin
	 set nocount on;
  
  exec p_Dashboard_Space_Utilisation @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Space_Utilisation @WarehouseId = 1
  
  exec p_Dashboard_Replenishment_Waiting @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Replenishment_Waiting 1
  
  exec p_Dashboard_Replenishment_Time @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Replenishment_Time 1
  
  exec p_Dashboard_Putaway_Priority @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Putaway_Priority @WarehouseId=1
  
  exec p_Dashboard_Outbound_Order_Status_Picking @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Outbound_Order_Status_Picking @WarehouseId = 1
  
  exec p_Dashboard_Outbound_Order_Status @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Outbound_Order_Status @WarehouseId = 1
  
  exec p_Dashboard_Outbound_Short_Picks @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Outbound_Short_Picks @WarehouseId = 1
  
  exec p_Dashboard_Outbound_Outstanding_Jobs @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Outbound_Outstanding_Jobs @WarehouseId = 1
  
  exec p_Dashboard_Outbound_Outstanding_Lines @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Outbound_Outstanding_Lines @WarehouseId = 1
  
  exec p_Dashboard_Exception_Stock_Take @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Exception_Stock_Take @WarehouseId = 1
  
  exec p_Dashboard_Dock_To_Stock @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Dock_To_Stock @WarehouseId = 1
  
  exec p_Dashboard_Outbound_Checking_Workload @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Outbound_Checking_Workload @WarehouseId = 1
  
  exec p_Dashboard_Outbound_Checking_Achieved @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Outbound_Checking_Achieved @WarehouseId = 1
  
  exec p_Dashboard_Outbound_Checking_QA @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Outbound_Checking_QA @WarehouseId = 1
  
  exec p_Dashboard_Outbound_Picking_Achieved @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Outbound_Picking_Achieved @WarehouseId = 1
    
  exec p_Dashboard_Inbound_Putaway_Achieved @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Inbound_Putaway_Achieved @WarehouseId = 1
  
  exec p_Dashboard_Inbound_Putaway_Achieved_KPI @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Inbound_Putaway_Achieved_KPI @WarehouseId = null
  
  exec p_Dashboard_Inbound_Putaway_Workload @WarehouseId = null, @Summarise = 1
  exec p_Dashboard_Inbound_Putaway_Workload @WarehouseId = 1
end
