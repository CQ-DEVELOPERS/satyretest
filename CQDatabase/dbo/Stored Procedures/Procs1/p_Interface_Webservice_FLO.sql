﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_Webservice_FLO
  ///   Filename       : p_Interface_Webservice_FLO.sql
  ///   Create By      : Pieter Doorewaard
  ///   Date Created   : 21 Jan 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
Create procedure [dbo].[p_Interface_Webservice_FLO]
(
 @XMLBody nvarchar(max) OUTPUT
)
--with encryption
As
Begin

Declare  @XML XML
Declare @ERROR AS VARCHAR(255)

SET @XML = @XMLBody

BEGIN TRY

Insert Into FloScheduleImport
      ([Route]
      ,[Driver]
      ,[Vehicle]
      ,[VehicleClass]
      ,[StopNumber]
      ,[UniqueNumber]
      ,[DeliveryPoint]
      ,[CustomerNumber]
      ,[CompanyName]
      ,[PickupTime]
      ,[Arrival]
      ,[TimeforJob]
      ,[DeliveryDate]
      ,[LineNumber]
      ,[CheckSummary])
      --,[RecordStatus]
      --,[ProcessedDate]
      --,[FloScheduleImportId]
      --,[InsertDate]
      --,[ErrorMsg])
		SELECT 
		(SELECT nodes.entity.value('Route[1]','varchar(100)') FROM  @XML.nodes('/root/Body/Item') AS nodes(entity)) AS 'Route',
		(SELECT nodes.entity.value('Driver[1]','varchar(100)') FROM  @XML.nodes('/root/Body/Item') AS nodes(entity)) AS 'Driver',
		(SELECT nodes.entity.value('Vehicle[1]','varchar(100)') FROM  @XML.nodes('/root/Body/Item') AS nodes(entity)) AS 'Vehicle',
		(SELECT LEFT(nodes.entity.value('VehicleClass[1]','varchar(100)'),20) FROM  @XML.nodes('/root/Body/Item') AS nodes(entity)) AS 'VehicleClass',
		nodes.entity.value('StopNumber[1]','varchar(100)') 'StopNumber',
		nodes.entity.value('UniqueNumber[1]','varchar(100)') 'UniqueNumber',
		nodes.entity.value('DeliveryPoint[1]','varchar(100)') 'DeliveryPoint',
		nodes.entity.value('CustomerNumber[1]','varchar(100)') 'CustomerNumber',
		nodes.entity.value('CompanyName[1]','varchar(100)') 'CompanyName',
		nodes.entity.value('PickupTime[1]','varchar(100)') 'PickupTime',
		nodes.entity.value('Arrival[1]','varchar(100)') 'Arrival',
		nodes.entity.value('TimeforJob[1]','float') 'TimeforJob',
		(SELECT nodes.entity.value('DeliveryDate[1]','varchar(100)') FROM  @XML.nodes('/root/Body/Item') AS nodes(entity)) AS 'DeliveryDate',
		nodes.entity.value('LineNumber[1]','varchar(100)') 'LineNumber',
		(SELECT nodes.entity.value('CheckSummary[1]','varchar(100)') FROM  @XML.nodes('/root/Body/Item') AS nodes(entity)) AS 'CheckSummary'

		FROM  @XML.nodes('/root/Body/ItemLine') AS nodes(entity)
			
	END TRY
	BEGIN CATCH
	    
	   SELECT @ERROR = LEFT(ERROR_MESSAGE(),255)
	END CATCH
	SET @XMLBody = '<root><status>' 
	    + CASE WHEN LEN(@ERROR) > 0 THEN @ERROR
	           ELSE 'Order(s) successfully imported'
	      END
	    + '</status></root>'
end
