﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_Delete
  ///   Filename       : p_Configuration_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 12:12:01
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Configuration table.
  /// </remarks>
  /// <param>
  ///   @ConfigurationId int = null,
  ///   @WarehouseId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_Delete
(
 @ConfigurationId int = null,
 @WarehouseId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Configuration
     where ConfigurationId = @ConfigurationId
       and WarehouseId = @WarehouseId
  
  select @Error = @@Error
  
  
  return @Error
  
end
 
