﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Housekeeping_Stock_Take_Authorise
  ///   Filename       : p_Housekeeping_Stock_Take_Authorise.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 21 Aug 2007
  /// </summary>
  /// <remarks>


  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Housekeeping_Stock_Take_Authorise
(
 @jobId       nvarchar(20) = null,
 @WarehouseId int = null,
 @AreaId      int = null
)

as
begin
	 set nocount on;
	 
	 create table #TableResult
	 (
      InstructionId      int,
      InstructionRefId   int,
      JobId              int,
      StorageUnitBatchId int,
      StorageUnitId      int,
      BatchId            int,
      Ident              int identity,
      LineNumber         int,
      LocationId         int,
      Location           nvarchar(15),
      ProductCode        nvarchar(30),
      Product            nvarchar(255),
      SKUCode            nvarchar(50),
      Batch              nvarchar(50),
      ExpectedQuantity   float,
      PreviousCount2     float,
      PreviousCount1     float,
      LatestCount        float,
      Status             nvarchar(50),
      Variance			 float
	 )
	 
	 if @AreaId = -1
	    set @AreaId = null
	 
	 set @jobId = replace(@jobId, 'J:', '')
	 
	 if @jobId = ''
		set @jobId = null
	 
	 insert #TableResult
	       (InstructionId,
         InstructionRefId,
         JobId,
         StorageUnitBatchId,
         LocationId,
         Location,
         PreviousCount1,
         LatestCount,
         Status,
         ExpectedQuantity,
         PreviousCount2)
  select i.InstructionId,
         i.InstructionRefId,
         i.JobId,
         i.StorageUnitBatchId,
         i.PickLocationId,
         l.Location,
         i.Quantity,
         isnull(i.ConfirmedQuantity,0),
         s.Status,
         i.CheckQuantity,
         i.PreviousQuantity
    from Instruction      i (nolock)
    join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
    join Job              j (nolock) on i.JobId             = j.JobId
    join Status           s (nolock) on i.StatusId          = s.StatusId
    join Location         l (nolock) on i.PickLocationId    = l.LocationId
    join AreaLocation    al (nolock) on l.LocationId        = al.LocationId
   where it.InstructionTypeCode in ('STE','STL','STA','STP')
     and s.Type              = 'I'
     and s.StatusCode        = 'F'
     and j.JobId             = isnull(@jobId, j.JobId)
     and i.WarehouseId       = @WarehouseId
     and i.StoreLocationId is null
     and isnull(i.Stored,0) != 1 -- Stock is Finished (New count inserted)
     and isnull(i.Picked,0) != 1 -- Stock is Finished (New count inserted)
     and al.AreaId           = isnull(@AreaId, al.AreaId)
  --order by i.JobId, i.PickLocationId, i.InstructionId
  order by l.Location, i.JobId
  
  update tr
     set StorageUnitId = sub.StorageUnitId,
         BatchId       = sub.BatchId
    from #TableResult      tr
    join StorageUnitBatch sub (nolock) on tr.StorageUnitBatchId = sub.StorageUnitBatchId
  
  update tr
     set ProductCode = p.ProductCode,
         Product     = p.Product
    from #TableResult tr
    join StorageUnit  su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join Product      p (nolock) on su.ProductId      = p.ProductId
  
  update tr
     set SKUCode = sku.SKUCode
    from #TableResult tr
    join StorageUnit  su (nolock) on tr.StorageUnitId = su.StorageUnitId
    join SKU         sku (nolock) on su.SKUId          = sku.SKUId
  
  update tr
     set Batch = b.Batch
    from #TableResult tr
    join Batch         b (nolock) on tr.BatchId = b.BatchId
  
  --update tr
  --   set Location = l.Location
  --  from #TableResult tr
  --  join Location      l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set LineNumber = tr.Ident - (select Min(Ident)
                                    from #TableResult tr2
                                   where tr.JobId = tr2.JobId) + 1
    from #TableResult tr
    
   update tr
     set Variance =  isnull(LatestCount,0) - isnull(ExpectedQuantity,0)
    from #TableResult tr
  
  select InstructionId,
         JobId,
         StorageUnitBatchId,
--         ROW_NUMBER() 
--         OVER (ORDER BY Location, JobId, LineNumber) AS Row,
         LineNumber,
         Location,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         ExpectedQuantity,
         PreviousCount2,
         PreviousCount1,
         LatestCount,
         Status,
         Variance
    from #TableResult
  order by Location, JobId, LineNumber
  
  --update i
  --   set CheckQuantity = i.Quantity
  --  from #TableResult tr
  --  join Job           j (nolock) on tr.JobId              = j.JobId
  --  join Job         ref (nolock) on j.ReferenceNumber     = ref.ReferenceNumber
  --  join Instruction   i (nolock) on ref.JobId             = i.JobId
  --                               and tr.StorageUnitBatchId = i.StorageUnitBatchId
  -- where i.InstructionRefId is null
  --   and i.CheckQuantity is null
  --   --and tr.Ident < 21 -- Only calcs first page on screen
  
  --update i
  --   set PreviousQuantity = i.Quantity
  --  from #TableResult tr
  --  join Instruction   i (nolock) on tr.InstructionRefId   = i.InstructionId
  --                               and tr.StorageUnitBatchId = i.StorageUnitBatchId
  -- where CheckWeight is null
  -- --where tr.Ident < 21 -- Only calcs first page on screen
  
  --update tri
  --   set CheckQuantity = i.Quantity
  --  from #TableResult tr
  --  join Instruction tri (nolock) on tr.InstructionId      = tri.InstructionId
  --  join Job           j (nolock) on tri.JobId              = j.JobId
  --  join Job         ref (nolock) on j.ReferenceNumber     = ref.ReferenceNumber
  --  join Instruction   i (nolock) on ref.JobId             = i.JobId
  --                               and tr.StorageUnitBatchId = i.StorageUnitBatchId
  -- where i.InstructionRefId is null
  --   and tri.CheckQuantity is null
  --   --and tr.Ident < 21 -- Only calcs first page on screen
  --select @@rowcount
  --update i
  --   set PreviousQuantity = i.Quantity
  --  from #TableResult tr
  --  join Instruction   i (nolock) on tr.InstructionRefId   = i.InstructionId
  --                               and tr.StorageUnitBatchId = i.StorageUnitBatchId
  -- where CheckWeight is null
  -- --where tr.Ident < 21 -- Only calcs first page on screen
end
