﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportSOHeader_Delete
  ///   Filename       : p_InterfaceExportSOHeader_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:13
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceExportSOHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportSOHeaderId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportSOHeader_Delete
(
 @InterfaceExportSOHeaderId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceExportSOHeader
     where InterfaceExportSOHeaderId = @InterfaceExportSOHeaderId
  
  select @Error = @@Error
  
  
  return @Error
  
end
