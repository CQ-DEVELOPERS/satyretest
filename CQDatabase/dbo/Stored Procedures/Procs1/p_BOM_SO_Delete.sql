﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_BOM_SO_Delete
  ///   Filename       : p_BOM_SO_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_BOM_SO_Delete
(
 @outboundShipmentId int = null,
 @issueId            int
)

as
begin
	 set nocount on;
	 
	 declare @Error              int = 0,
          @Errormsg           nvarchar(500) = 'Error executing p_BOM_SO_Delete',
          @GetDate            datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @issueId = -1
    set @issueId = null
  
  begin transaction
  
  if @OutboundShipmentId is not null
  begin
    exec @Error = p_BOM_SO_Split 
     @issueId = @issueId
    
    if @Error <> 0
      goto error
  end
  
  exec p_Outbound_Order_Delete
   @issueId = @issueId
  
  if @Error <> 0
    goto error
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end

