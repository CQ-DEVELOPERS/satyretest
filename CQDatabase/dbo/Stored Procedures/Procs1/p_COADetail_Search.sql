﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_COADetail_Search
  ///   Filename       : p_COADetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 08 Feb 2013 07:32:23
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the COADetail table.
  /// </remarks>
  /// <param>
  ///   @COADetailId int = null output,
  ///   @COAId int = null,
  ///   @NoteId int = null 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   COADetail.COADetailId,
  ///   COADetail.COAId,
  ///   COA.COA,
  ///   COADetail.NoteId,
  ///   Note.Note,
  ///   COADetail.NoteCode,
  ///   COADetail.Note 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_COADetail_Search
(
 @COADetailId int = null output,
 @COAId int = null,
 @NoteId int = null,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @COADetailId = '-1'
    set @COADetailId = null;
  
  if @COAId = '-1'
    set @COAId = null;
  
  if @NoteId = '-1'
    set @NoteId = null;
  
 
  select
         COADetail.COADetailId
        ,COADetail.COAId
         ,COACOAId.COA as 'COA'
        ,COADetail.NoteId
         ,NoteNoteId.Note as 'Note'
        ,COADetail.NoteCode
        ,COADetail.Note
    from COADetail
    left
    join COA COACOAId on COACOAId.COAId = COADetail.COAId
    left
    join Note NoteNoteId on NoteNoteId.NoteId = COADetail.NoteId
   where isnull(COADetail.COADetailId,'0')  = isnull(@COADetailId, isnull(COADetail.COADetailId,'0'))
     and isnull(COADetail.COAId,'0')  = isnull(@COAId, isnull(COADetail.COAId,'0'))
     and isnull(COADetail.NoteId,'0')  = isnull(@NoteId, isnull(COADetail.NoteId,'0'))
  order by NoteCode
  
end
