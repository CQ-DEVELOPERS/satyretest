﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceCustomer_Update
  ///   Filename       : p_InterfaceCustomer_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:52
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceCustomer table.
  /// </remarks>
  /// <param>
  ///   @HostId nvarchar(60) = null,
  ///   @CustomerCode nvarchar(60) = null,
  ///   @CustomerName nvarchar(510) = null,
  ///   @Class nvarchar(510) = null,
  ///   @DeliveryPoint nvarchar(510) = null,
  ///   @Address1 nvarchar(510) = null,
  ///   @Address2 nvarchar(510) = null,
  ///   @Address3 nvarchar(510) = null,
  ///   @Address4 nvarchar(510) = null,
  ///   @Modified nvarchar(20) = null,
  ///   @ContactPerson nvarchar(200) = null,
  ///   @Phone nvarchar(510) = null,
  ///   @Fax nvarchar(510) = null,
  ///   @Email nvarchar(510) = null,
  ///   @DeliveryGroup nvarchar(510) = null,
  ///   @DepotCode nvarchar(510) = null,
  ///   @VisitFrequency nvarchar(510) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null,
  ///   @InsertDate datetime = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceCustomer_Update
(
 @HostId nvarchar(60) = null,
 @CustomerCode nvarchar(60) = null,
 @CustomerName nvarchar(510) = null,
 @Class nvarchar(510) = null,
 @DeliveryPoint nvarchar(510) = null,
 @Address1 nvarchar(510) = null,
 @Address2 nvarchar(510) = null,
 @Address3 nvarchar(510) = null,
 @Address4 nvarchar(510) = null,
 @Modified nvarchar(20) = null,
 @ContactPerson nvarchar(200) = null,
 @Phone nvarchar(510) = null,
 @Fax nvarchar(510) = null,
 @Email nvarchar(510) = null,
 @DeliveryGroup nvarchar(510) = null,
 @DepotCode nvarchar(510) = null,
 @VisitFrequency nvarchar(510) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null,
 @InsertDate datetime = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceCustomer
     set HostId = isnull(@HostId, HostId),
         CustomerCode = isnull(@CustomerCode, CustomerCode),
         CustomerName = isnull(@CustomerName, CustomerName),
         Class = isnull(@Class, Class),
         DeliveryPoint = isnull(@DeliveryPoint, DeliveryPoint),
         Address1 = isnull(@Address1, Address1),
         Address2 = isnull(@Address2, Address2),
         Address3 = isnull(@Address3, Address3),
         Address4 = isnull(@Address4, Address4),
         Modified = isnull(@Modified, Modified),
         ContactPerson = isnull(@ContactPerson, ContactPerson),
         Phone = isnull(@Phone, Phone),
         Fax = isnull(@Fax, Fax),
         Email = isnull(@Email, Email),
         DeliveryGroup = isnull(@DeliveryGroup, DeliveryGroup),
         DepotCode = isnull(@DepotCode, DepotCode),
         VisitFrequency = isnull(@VisitFrequency, VisitFrequency),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         InsertDate = isnull(@InsertDate, InsertDate) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
