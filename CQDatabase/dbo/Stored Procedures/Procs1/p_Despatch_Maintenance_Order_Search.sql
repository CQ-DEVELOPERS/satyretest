﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Despatch_Maintenance_Order_Search
  ///   Filename       : p_Despatch_Maintenance_Order_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Despatch_Maintenance_Order_Search
(
 @WarehouseId            int,
 @OutboundShipmentId	    int,
 @OutboundDocumentTypeId	int,
 @ExternalCompanyCode	   nvarchar(30),
 @ExternalCompany	       nvarchar(255),
 @OrderNumber	           nvarchar(30),
 @FromDate	              datetime,
 @ToDate	                datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   OutboundDocumentId        int,
   OutboundDocumentType      nvarchar(30),
   IssueId                   int,
   OrderNumber               nvarchar(30),
   OutboundShipmentId        int,
   CustomerCode              nvarchar(30),
   Customer                  nvarchar(255),
   RouteId                   int,
   Route                     nvarchar(50),
   NumberOfLines             int,
   DeliveryDate              datetime,
   CreateDate                datetime,
   StatusId                  int,
   Status                    nvarchar(50),
   PriorityId                int,
   Priority                  nvarchar(50),
   LocationId                int,
   Location                  nvarchar(15),
   DespatchBayId             int,
   DespatchBay               nvarchar(15),
   Rating                    int,
   AvailabilityIndicator     nvarchar(20),
   Remarks                   nvarchar(255),
   Total                     numeric(13,3),
   Complete                  numeric(13,3),
   PercentageComplete        numeric(13,3),
   LoadIndicator             bit default 0,
   Collector				 nvarchar(50),
   EmployeeCode				 nvarchar(50)
  );
  
  declare @TableLines as table
  (
   IssueId       int,
   NumberOfLines int
  )
  
  declare @Total as table
  (
   IssueId int,
   Total   int
  )
  
  declare @Complete as table
  (
   IssueId  int,
   Complete int
  )
  
  declare @GetDate           datetime
  
  select @GetDate = dbo.ufn_Getdate()
  
  if @OutboundDocumentTypeId = -1
    set @OutboundDocumentTypeId = null
  
  if @OutboundShipmentId = -1
    set @OutboundShipmentId = null
  
  if @OutboundShipmentId is null
  begin
    insert @TableResult
          (OutboundDocumentId,
           IssueId,
           LocationId,
           DespatchBayId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           OutboundDocumentType,
           Rating,
           Remarks,
           LoadIndicator,
           Collector,
           EmployeeCode)
    select od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           i.DespatchBay,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           odt.OutboundDocumentType,
           ec.Rating,
           i.Remarks,
           i.LoadIndicator,
           od.Collector,
           od.EmployeeCode
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId               = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
     where od.OutboundDocumentTypeId = isnull(@OutboundDocumentTypeId, od.OutboundDocumentTypeId)
       and ec.ExternalCompanyCode like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
       and ec.ExternalCompany     like isnull(@ExternalCompany + '%', ec.ExternalCompany)
       and od.OrderNumber         like isnull(@OrderNumber  + '%', od.OrderNumber)
       and od.CreateDate      between @FromDate and @ToDate
       and s.Type                    = 'IS'
       and s.StatusCode             in ('CD','D','DC','CA')
       and od.WarehouseId            = @WarehouseId
    
    update tr
       set OutboundShipmentId = si.OutboundShipmentId,
           DeliveryDate       = os.ShipmentDate,
           Route              = os.Route,
           RouteId            = os.RouteId,
           LocationId         = os.LocationId,
           DespatchBayId      = os.DespatchBay,
           StatusId           = os.StatusId
      from @TableResult  tr
      join OutboundShipmentIssue si (nolock) on tr.IssueId = si.IssueId
      join OutboundShipment      os (nolock) on si.OutboundShipmentId = os.OutboundShipmentId
  end
  else
    insert @TableResult
          (OutboundShipmentId,
           OutboundDocumentId,
           IssueId,
           LocationId,
           DespatchBayId,
           OrderNumber,
           CustomerCode,
           Customer,
           RouteId,
           StatusId,
           Status,
           PriorityId,
           DeliveryDate,
           CreateDate,
           OutboundDocumentType,
           Rating,
           Remarks,
           LoadIndicator,
           Collector,
           EmployeeCode)
    select osi.OutboundShipmentId,
           od.OutboundDocumentId,
           i.IssueId,
           i.LocationId,
           i.DespatchBay,
           od.OrderNumber,
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.RouteId,
           i.StatusId,
           s.Status,
           i.PriorityId,
           i.DeliveryDate,
           od.CreateDate,
           odt.OutboundDocumentType,
           ec.Rating,
           i.Remarks,
           i.LoadIndicator,
           od.Collector,
           od.EmployeeCode
      from OutboundDocument     od  (nolock)
      join ExternalCompany      ec  (nolock) on od.ExternalCompanyId     = ec.ExternalCompanyId
      join Issue                i   (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      join Status               s   (nolock) on i.StatusId               = s.StatusId
      join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
     where osi.OutboundShipmentId = @OutboundShipmentId
       and s.Type                 = 'IS'
       and s.StatusCode             in ('CD','D','DC')
  
  update tr
     set Route = r.Route
    from @TableResult  tr
    join Route          r (nolock) on tr.RouteId = r.RouteId
  
  update tr
     set Location = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.LocationId = l.LocationId
  
  update tr
     set DespatchBay = l.Location
    from @TableResult tr
    join Location     l (nolock) on tr.DespatchBayId = l.LocationId
  
  update tr
     set Priority = p.Priority
    from @TableResult tr
    join Priority     p (nolock) on tr.PriorityId = p.PriorityId
  
  update @TableResult
     set AvailabilityIndicator = 'Standard'
   where dbo.ufn_Configuration_Value(47, @warehouseId) <= DateDiff(hh, @GetDate, DeliveryDate)
   
  update @TableResult
     set AvailabilityIndicator = 'Yellow'
   where dbo.ufn_Configuration_Value(46, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Orange'
   where dbo.ufn_Configuration_Value(45, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
  update @TableResult
     set AvailabilityIndicator = 'Red'
   where dbo.ufn_Configuration_Value(44, @warehouseId) > DateDiff(hh, @GetDate, DeliveryDate)
  
--  update tr
--     set Total = (select count(distinct(i.JobId))
--                         from IssueLineInstruction ili (nolock)
--                         join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
--                        where tr.IssueId     = ili.IssueId)
--    from @TableResult tr
--  
--  update tr
--     set Complete = (select count(distinct(i.JobId))
--                         from IssueLineInstruction ili (nolock)
--                         join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
--                         join Job                    j (nolock) on i.JobId           = j.JobId
--                         join Status                 s (nolock) on j.StatusId        = s.StatusId
--                        where tr.IssueId   = ili.IssueId
--                          and s.StatusCode in ('DC','C'))
--    from @TableResult tr
--  
--  update @TableResult
--     set NumberOfLines = (select count(1)
--                            from OutboundLine ol (nolock)
--                           where tr.OutboundDocumentId = ol.OutboundDocumentId)
--    from @TableResult tr
  
  insert @TableLines
        (IssueId,
         NumberofLines)
  select tr.IssueId,
         count(1)
    from @TableResult tr
    join IssueLine    il (nolock) on tr.IssueId = il.IssueId
  group by tr.IssueId
  
  update tr
     set NumberofLines = t.NumberofLines
    from @TableResult tr
    join @TableLines   t on tr.IssueId = t.IssueId
  
  insert @Total
        (IssueId,
         Total)
  select ili.IssueId,
         count(distinct(i.JobId))
    from @TableLines           tl
    join IssueLineInstruction ili (nolock) on tl.IssueId   = ili.IssueId
    join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
    join Job                    j (nolock) on i.JobId           = j.JobId
    join Status                 s (nolock) on j.StatusId        = s.StatusId
  group by ili.IssueId
  
  update tr
     set Total = t.Total
    from @TableResult tr
    join @Total        t on tr.IssueId = t.IssueId
  
  insert @Complete
        (IssueId,
         Complete)
  select ili.IssueId,
         count(distinct(i.JobId))
    from @TableLines           tl
    join IssueLineInstruction ili (nolock) on tl.IssueId   = ili.IssueId
    join Instruction            i (nolock) on ili.InstructionId = i.InstructionId
    join Job                    j (nolock) on i.JobId           = j.JobId
    join Status                 s (nolock) on j.StatusId        = s.StatusId
   where s.StatusCode in ('DC','C')
  group by ili.IssueId
  
  update tr
     set Complete = t.Complete
    from @TableResult tr
    join @Complete     t on tr.IssueId = t.IssueId
  
  update @TableResult
     set PercentageComplete = (Complete / Total) * 100
   where Complete > 0
     and Total    > 0
  
  update @TableResult
     set PercentageComplete = 0
   where PercentageComplete is null
  
  select tr.IssueId,
         isnull(tr.OutboundShipmentId, -1) as 'OutboundShipmentId',
         tr.OrderNumber,
         tr.CustomerCode,
         tr.Customer,
         isnull(tr.RouteId,-1) as 'RouteId',
         tr.Route,
         tl.NumberOfLines,
         tr.DeliveryDate,
         tr.CreateDate,
         tr.Status,
         tr.PriorityId,
         tr.Priority,
         tr.OutboundDocumentType,
         isnull(tr.LocationId,-1) as 'LocationId',
         tr.Location,
         tr.DespatchBay,
         tr.Rating,
         tr.AvailabilityIndicator,
         tr.Remarks,
         tr.Complete,
         tr.Total,
         tr.PercentageComplete,
         tr.Collector,
         tr.EmployeeCode
    from @TableResult tr
    left outer
    join @TableLines tl on tr.IssueId = tl.IssueId
  order by OutboundShipmentId,
           OrderNumber
--   where LoadIndicator != 1
--  union
--  select -1,
--         isnull(OutboundShipmentId, -1) as 'OutboundShipmentId',
--         null,
--         null,
--         null,
--         RouteId,
--         Route,
--         sum(NumberOfLines),
--         DeliveryDate,
--         min(CreateDate),
--         Status,
--         min(PriorityId),
--         min(Priority),
--         'Load',
--         LocationId,
--         Location,
--         null,
--         min(AvailabilityIndicator),
--         null,
--         sum(Complete),
--         sum(Total),
--         min(PercentageComplete)
--    from @TableResult
--   where LoadIndicator = 1
--  group by isnull(OutboundShipmentId, -1),
--           RouteId,
--           Route,
--           DeliveryDate,
--           Status,
--           LocationId,
--           Location
end
 
 
