﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaSequence_Update
  ///   Filename       : p_AreaSequence_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:01
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the AreaSequence table.
  /// </remarks>
  /// <param>
  ///   @AreaSequenceId int = null,
  ///   @PickAreaId int = null,
  ///   @StoreAreaId int = null,
  ///   @AdjustmentType nvarchar(20) = null,
  ///   @ReasonCode varchar(10) = null,
  ///   @InstructionTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaSequence_Update
(
 @AreaSequenceId int = null,
 @PickAreaId int = null,
 @StoreAreaId int = null,
 @AdjustmentType nvarchar(20) = null,
 @ReasonCode varchar(10) = null,
 @InstructionTypeCode nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
  if @AreaSequenceId = '-1'
    set @AreaSequenceId = null;
  
	 declare @Error int
 
  update AreaSequence
     set PickAreaId = isnull(@PickAreaId, PickAreaId),
         StoreAreaId = isnull(@StoreAreaId, StoreAreaId),
         AdjustmentType = isnull(@AdjustmentType, AdjustmentType),
         ReasonCode = isnull(@ReasonCode, ReasonCode),
         InstructionTypeCode = isnull(@InstructionTypeCode, InstructionTypeCode) 
   where AreaSequenceId = @AreaSequenceId
  
  select @Error = @@Error
  
  
  return @Error
  
end
