﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompanyType_Select
  ///   Filename       : p_ExternalCompanyType_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:40
  /// </summary>
  /// <remarks>
  ///   Selects rows from the ExternalCompanyType table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompanyTypeId int = null 
  /// </param>
  /// <returns>
  ///   ExternalCompanyType.ExternalCompanyTypeId,
  ///   ExternalCompanyType.ExternalCompanyType,
  ///   ExternalCompanyType.ExternalCompanyTypeCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompanyType_Select
(
 @ExternalCompanyTypeId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         ExternalCompanyType.ExternalCompanyTypeId
        ,ExternalCompanyType.ExternalCompanyType
        ,ExternalCompanyType.ExternalCompanyTypeCode
    from ExternalCompanyType
   where isnull(ExternalCompanyType.ExternalCompanyTypeId,'0')  = isnull(@ExternalCompanyTypeId, isnull(ExternalCompanyType.ExternalCompanyTypeId,'0'))
  
end
