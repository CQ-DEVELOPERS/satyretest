﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Empty_Container_Search_Barcode
  ///   Filename       : p_Empty_Container_Search_Barcode.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 12 Jan 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Empty_Container_Search_Barcode
(
 @barcode nvarchar(30)
)

as
begin
	 set nocount on;
	 
	 declare @JobId              int,
          @PalletId           int
  
  if isnumeric(replace(@barcode,'P:','')) = 1
    select @PalletId = replace(@barcode,'P:',''),
           @barcode  = null
  
  select PalletId,
         Tare       as 'TareWeight',
         Weight     as 'GrossWeight'
    from Pallet (nolock)
   where PalletId = @PalletId
end
