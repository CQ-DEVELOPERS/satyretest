﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Scheduled_Deliveries
  ///   Filename       : p_Dashboard_Scheduled_Deliveries.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 17 Feb 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Scheduled_Deliveries
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)

as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	 
	 --select 'Week ' + convert(varchar(10), isnull(datepart(wk, dh.DeliveryDate), 8)) as 'Week',
	 --       tm.TransportMode as 'Legend',
	 --       count(distinct(DeliveryAdviceHeaderId)) as 'Value'
	 --  from TransportMode        tm (nolock)
	 --  left
	 --  join DeliveryAdviceHeader dh (nolock) on dh.TransportModeId = tm.TransportModeId
	 --group by datepart(wk, dh.DeliveryDate), tm.TransportMode
	 
	 select 'Air Freight' as 'Legend',
	        'Week 7' as 'Week',
	        2 as 'Value'
	 union
	 select 'Rail' as 'Legend',
	        'Week 7' as 'Week',
	        3 as 'Value'
	 union
	 select 'Road' as 'Legend',
	        'Week 7' as 'Week',
	        1 as 'Value'
	 union
	 select 'Sea' as 'Legend',
	        'Week 7' as 'Week',
	        2 as 'Value'
	 union
	 select 'Air Freight' as 'Legend',
	        'Week 8' as 'Week',
	        0 as 'Value'
	 union
	 select 'Rail' as 'Legend',
	        'Week 8' as 'Week',
	        0 as 'Value'
	 union
	 select 'Road' as 'Legend',
	        'Week 8' as 'Week',
	        4 as 'Value'
	 union
	 select 'Sea' as 'Legend',
	        'Week 8' as 'Week',
	        2 as 'Value'
  end
end
