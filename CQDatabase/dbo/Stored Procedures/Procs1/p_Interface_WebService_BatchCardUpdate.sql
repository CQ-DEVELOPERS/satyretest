﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_WebService_BatchCardUpdate
  ///   Filename       : p_Interface_WebService_BatchCardUpdate.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 02 Aug 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_Interface_WebService_BatchCardUpdate
(
	@XMLBody nvarchar(max) output
)
--with encryption
as
begin
	set nocount on;
  
 	declare @doc                     xml
         ,@idoc                    int
         ,@InsertDate              datetime
         ,@ERROR                   nvarchar(255)
         ,@InterfaceImportHeaderId int
         ,@OperatorId              int
         ,@JobId                   int
         ,@ReferenceNumber         nvarchar(30)
         ,@NewBox                  bit
         ,@GetDate                 datetime
         ,@StatusCode              nvarchar(10)
  
  select @GetDate = dbo.ufn_Getdate()
	
	declare @CheckingTable as table
	(
	     InstructionRefId   int
	    ,InsertDate         DateTime
	    ,StorageUnitBatchId int
	    ,ProductCode        nvarchar(100)
	    ,Product            nvarchar(255)
	    ,SKUCode            nvarchar(10)
	    ,Batch              nvarchar(50)
	    ,Location           nvarchar(15)
	    ,ExpectedQuantity   float	
	    ,PickedQuantity	    float
	    ,CheckedQuantity    float
	)
	
	declare @InstructionTable as table
	(
	     InstructionId      int
	    ,InstructionRefId   int
	    ,StorageUnitBatchId int
	    ,ProductCode        nvarchar(50)
	    ,Product            nvarchar(255)
	    ,SKUCode            nvarchar(10)
	    ,Batch              nvarchar(50)
	    ,Location           nvarchar(15)
	    ,NewLocation        nvarchar(15)
	    ,ExpectedQuantity   float	
	    ,PickedQuantity	    float
	    ,CheckedQuantity    float
	)
	
	 select @doc = convert(xml,@XMLBody)
	       ,@InsertDate = Getdate()
 	     
	 exec sp_xml_preparedocument @idoc OUTPUT, @doc
		
	 begin try
	 begin transaction
		
		select @JobId           = nodes.entity.value('JobId[1]',            'int')       
		      ,@ReferenceNumber = nodes.entity.value('ReferenceNumber[1]',  'nvarchar(30)')
		      ,@NewBox          = nodes.entity.value('NewBox[1]',           'bit')        
		  from @doc.nodes('/root') AS nodes(entity)
	 
  select @OperatorId = j.OperatorId,
         @StatusCode = s.StatusCode
    from Job    j (nolock)
    join Status s (nolock) on j.StatusId = s.StatusId
   where j.JobId = @JobId
		
  insert @CheckingTable
        (InsertDate,
         StorageUnitBatchId,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Location,
         ExpectedQuantity,
         PickedQuantity,
         CheckedQuantity)
	 select @InsertDate
        ,nodes.entity.value('StorageUnitBatchId[1]', 'int')           'StorageUnitBatchId'
        ,nodes.entity.value('ProductCode[1]',        'nvarchar(50)')  'ProductCode'
        ,nodes.entity.value('Product[1]',			         'nvarchar(255)') 'Product'
        ,nodes.entity.value('SKUCode[1]',		          'nvarchar(30)')  'SKUCode'
        ,nodes.entity.value('Batch[1]',		            'nvarchar(50)')  'Batch'
        ,nodes.entity.value('Location[1]',		         'nvarchar(15)')  'Location'
        ,nodes.entity.value('ExpectedQuantity[1]',   'nvarchar(30)')  'ExpectedQuantity'
        ,nodes.entity.value('PickedQuantity[1]',     'nvarchar(30)')  'PickedQuantity'
        ,nodes.entity.value('CheckedQuantity[1]',    'nvarchar(30)')  'CheckedQuantity'
    from @doc.nodes('/root/Instruction') AS nodes(entity)
  
  insert @InstructionTable
        (InstructionId,
         InstructionRefId,
         StorageUnitBatchId,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Location,
         ExpectedQuantity,
         PickedQuantity,
         CheckedQuantity)
  select InstructionId
        ,isnull(i.InstructionRefId, i.InstructionId)
        ,sub.StorageUnitBatchId
        ,p.ProductCode
        ,p.Product
        ,sku.SKUCode
        ,b.Batch
        ,l.Location
        ,i.Quantity
        ,i.ConfirmedQuantity
        ,i.CheckQuantity
    from Instruction        i (nolock)
    join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
    join Batch              b (nolock) on sub.BatchId          = b.BatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
    join Product            p (nolock) on su.ProductId         = p.ProductId
    join SKU              sku (nolock) on su.SKUId             = sku.SKUId
    left
    join Location           l (nolock) on i.StoreLocationId    = l.LocationId
   where JobId = @JobId
  
  update it
     set PickedQuantity   = ct.PickedQuantity,
         ExpectedQuantity = ct.ExpectedQuantity,
         CheckedQuantity  = ct.CheckedQuantity
    from @InstructionTable it
    join @CheckingTable    ct on it.Product = ct.Product
                             and ct.StorageUnitBatchId > 0
  
  update i
     set Quantity          = it.CheckedQuantity,
         --ConfirmedQuantity = it.ExpectedQuantity,
         ConfirmedQuantity = it.CheckedQuantity,
         CheckQuantity     = it.CheckedQuantity
    from @InstructionTable it
    join Instruction        i on it.InstructionId = i.InstructionId
  
  update ct
     set InstructionRefId = it.InstructionRefId
    from @InstructionTable it
    join @CheckingTable    ct on it.Product = ct.Product
                             and ct.StorageUnitBatchId < 0
  
  --select * from @CheckingTable order by ProductCode
  --select * from @InstructionTable order by ProductCode
  
  insert Instruction
        (InstructionTypeId,
         StorageUnitBatchId,
         WarehouseId,
         StatusId,
         JobId,
         OperatorId,
         PickLocationId,
         StoreLocationId,
         IssueLineId,
         InstructionRefId,
         Quantity,
         ConfirmedQuantity,
         CheckQuantity,
         CreateDate,
         StartDate,
         EndDate,
         OutboundShipmentId,
         DropSequence)
  select InstructionTypeId,
         sub.StorageUnitBatchId,
         i.WarehouseId,
         dbo.ufn_StatusId('I','F'),
         @JobId,
         i.OperatorId,
         i.PickLocationId,
         i.StoreLocationId,
         IssueLineId,
         ct.InstructionRefId,
         ct.ExpectedQuantity,
         ct.PickedQuantity,
         ct.CheckedQuantity,
         @GetDate,
         StartDate,
         EndDate,
         OutboundShipmentId,
         DropSequence
    from @CheckingTable    ct
    join Product            p (nolock) on ct.ProductCode      = p.ProductCode
    join SKU              sku (nolock) on ct.SKUCode          = sku.SKUCode
    join StorageUnit       su (nolock) on p.ProductId         = su.StorageUnitId
                                      and sku.SKUId           = su.SKUId
    left
    join Batch              b (nolock) on ct.Batch            = b.Batch
    left
    join StorageUnitBatch sub (nolock) on b.BatchId           = sub.BatchId
                                      and su.StorageUnitId    = sub.StorageUnitId
    join Instruction        i (nolock) on ct.InstructionRefId = i.InstructionId
   where ct.StorageUnitBatchId < 0
  
  If @NewBox = 1
  begin
    exec @JobId = p_Mobile_Product_Check_New_Box
     @JobId           = @JobId,
     @operatorId      = @OperatorId,
     @ReferenceNumber = @ReferenceNumber
    
    declare @XML nvarchar(MAX)
    
    if (ISNULL(@ReferenceNumber,'') <> '')
      select @XML =  '<root><Header><ReferenceNumber>' + @ReferenceNumber + '</ReferenceNumber></Header></root>'
    else
      select @XML =  '<root><Header><ReferenceNumber>J:' + CONVERT(VARCHAR(20), @JobId) + '</ReferenceNumber></Header></root>'
    
    exec p_Interface_WebService_CheckGet @XML OUT
 			
 			SET @xmlBody = @xml
  end
  else
  begin
    --update i
    --   set ConfirmedQuantity = ti.CheckedQuantity,
    --       CheckQuantity     = ti.CheckedQuantity
    --  from @InstructionTable ti
    --  join Instruction        i (nolock) on ti.InstructionId = i.InstructionId
    
    if @StatusCode = 'CK'
    if not exists(select 1 from Instruction where JobId = @JobId and ConfirmedQuantity != CheckQuantity)
    begin
      update i
        set NewLocation = ct.Location
       from @InstructionTable i
       join @CheckingTable   ct on i.InstructionId =  ct.InstructionRefId
     
      update i
         set NewLocation = l.Location
        from @InstructionTable           i 
        join StorageUnitBatchLocation subl on i.StorageUnitBatchId = subl.StorageUnitBatchId
        join Location                    l on subl.LocationId = l.LocationId
                                          and l.Location = i.Location
      
      --update subl
      --   set ActualQuantity = ActualQuantity - i.CheckedQuantity
      --  from StorageUnitBatchLocation subl
      --  join @InstructionTable           i on subl.StorageUnitBatchId = i.StorageUnitBatchId
      --  join Location                    l on subl.LocationId = l.LocationId
      --                                    and l.Location = i.Location
      -- where subl.ActualQuantity - i.CheckedQuantity > 0
      
      update subl
         set ActualQuantity = ActualQuantity - i.CheckedQuantity
        from StorageUnitBatchLocation subl
        join @InstructionTable           i on subl.StorageUnitBatchId = i.StorageUnitBatchId
        join Location                    l on subl.LocationId = l.LocationId
                                          and l.Location = i.NewLocation
      
      --insert StorageUnitBatchLocation
      --      (StorageUnitBatchId,
      --       LocationId,
      --       ActualQuantity)
      --select i.StorageUnitBatchId,
      --       l.LocationId,
      --       i.CheckedQuantity
      --  from @InstructionTable           i
      --  join Location                    l on l.Location   = i.NewLocation
      --  join AreaLocation               al on l.LocationId = al.LocationId
      --  join Area                        a on al.AreaId    = a.AreaId
      --                                    and a.AreaType   = 'POT'
      --  left
      --  join StorageUnitBatchLocation subl on subl.StorageUnitBatchId = i.StorageUnitBatchId
      --                                    and subl.LocationId         = l.
      
      --insert InterfaceExportStockAdjustment
      --      (RecordType,
      --       RecordStatus,
      --       ProductCode,
      --       Product,
      --       SKUCode,
      --       Batch,
      --       Quantity,
      --       Weight,
      --       Additional1,
      --       Additional3,
      --       Additional5)
      --select 'ADJ',
      --       'N',
      --       p.ProductCode,
      --       p.Product,
      --       sku.SKUCode,
      --       b.Batch,
      --       it.CheckedQuantity - it.PickedQuantity,
      --       null,
      --       w.WarehouseCode,
      --       convert(nvarchar(10), @JobId),
      --       'CQ Pick Id: ' + convert(varchar(10), i.InstructionId)
      --  from @InstructionTable it
      --  join Product            p (nolock) on it.ProductCode      = p.ProductCode
      --  join SKU              sku (nolock) on it.SKUCode          = sku.SKUCode
      --  join StorageUnit       su (nolock) on p.ProductId         = su.StorageUnitId
      --                                    and sku.SKUId           = su.SKUId
      --  left
      --  join Batch              b (nolock) on it.Batch            = b.Batch
      --  left
      --  join StorageUnitBatch sub (nolock) on b.BatchId           = sub.BatchId
      --                                    and su.StorageUnitId    = sub.StorageUnitId
      --  join Instruction        i (nolock) on it.InstructionRefId = i.InstructionId
      --  join Warehouse          w (nolock) on w.WarehouseId       = i.WarehouseId
      -- where it.CheckedQuantity > it.PickedQuantity
      --   and not exists(select 1 from InterfaceExportStockAdjustment sa (nolock) where Additional5 = 'CQ Pick Id: ' + convert(varchar(10), i.InstructionId))
      
      exec @Error = p_Status_Rollup @JobId = @JobId
    end
    SET @XMLBody = '<root><status>Success</status></root>'
  end
  print 'commit transaction'
  commit transaction
end try
begin catch
  print 'rollback transaction'
  rollback transaction
  select @ERROR = LEFT(ERROR_MESSAGE(),255)
  set @XMLBody = '<root><status>'+@ERROR+'</status></root>'
end catch
end
