﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Address_Parameter
  ///   Filename       : p_Address_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:20
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Address table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   Address.AddressId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Address_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as AddressId
        ,null as 'Address'
  union
  select
         Address.AddressId
        ,Address.AddressId as 'Address'
    from Address
  
end
