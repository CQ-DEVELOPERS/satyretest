﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Container_Scan
  ///   Filename       : p_Container_Scan.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 22 Sep 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Container_Scan
(
 @WarehouseId int = null,
 @operatorId  int = null,
 @barcode     nvarchar(30) = null,
 @location    nvarchar(15) = null,
 @in          bit = 0,
 @out         bit = 0,
 @version     int = 1,
 @showMsg     bit = 1
)

as
begin
  set nocount on;
  
  declare @Error             int,
          @Errormsg          varchar(500) = 'Error executing p_Container_Scan',
          @GetDate           datetime,
          @Transaction       bit = 0,
          @InboundShipmentId int,
          @ReceiptId         int,
          @ActualPlannedStart      datetime,
          @ActualPlannedEnd        datetime,
          @PlannedStart      datetime,
          @PlannedEnd        datetime,
          @CssClass          nvarchar(50),
          @LocationId        int,
          @DockScheduleId    int,
          @Description		 nvarchar(255)
  
  select @GetDate = dbo.ufn_Getdate()
  print @GetDate
  if @@trancount = 0
    begin
      begin transaction
      set @Transaction = 1
    end
  
  if @location != ''
  begin
    select @LocationId = l.LocationId
      from Location      l (nolock)
      join AreaLocation al (nolock) on l.LocationId = al.LocationId
      join Area          a (nolock) on al.AreaId = a.AreaId
     where a.AreaCode in ('R','D')
       and l.Location = @Location
  end
  
  select @ReceiptId         = r.ReceiptId,
         @InboundShipmentId = isr.InboundShipmentId
    from InboundDocument id (nolock)
    join Receipt          r (nolock) on id.InboundDocumentId = r.InboundDocumentId
    left
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
   where id.OrderNumber = @barcode
  
  if @ReceiptId is null
  begin
    set @Error = -1
    goto Error
  end
  
  select @ActualPlannedStart   = PlannedStart,
         @ActualPlannedEnd     = PlannedEnd
    from DockSchedule (nolock)
   where isnull(InboundShipmentId,'-1') = isnull(@InboundShipmentId, isnull(InboundShipmentId,'-1'))
     and ReceiptId = @ReceiptId
     and [Version] = 0
     
  select @PlannedStart   = PlannedStart,
         @DockScheduleId = DockScheduleId,
         @LocationId     = isnull(@LocationId, LocationId)
    from DockSchedule (nolock)
   where isnull(InboundShipmentId,'-1') = isnull(@InboundShipmentId, isnull(InboundShipmentId,'-1'))
     and ReceiptId = @ReceiptId
     and ([Version] = @version or @version is null)
  
  if @DockScheduleId is null
    select @DockScheduleId = max(DockScheduleId)
      from DockSchedule (nolock)
     where isnull(InboundShipmentId,'-1') = isnull(@InboundShipmentId, isnull(InboundShipmentId,'-1'))
       and ReceiptId = @ReceiptId
  
  if @PlannedStart is null
    select @PlannedStart = @GetDate,
           @PlannedEnd   = dateadd(mi, 30, @GetDate)
  else
    select @PlannedEnd   = @GetDate
  
  if @DockScheduleId is not null and @locationId is not null
  if not exists(select top 1 1 from DockSchedule where DockScheduleId = @DockScheduleId and LocationId = @locationId)
  begin
    set @Error = '-2'
    goto error
  end
  
  if @In = 1
  begin
    set @CssClass = 'rsCategoryRed'
    set @PlannedEnd = @ActualPlannedEnd
    if @version = 1
		set @Description = 'Container Scanned Yard-In'
	else
	if @version = 2
		set @Description = 'Container Scanned Dock-In'
  end
  else if @out = 1
  begin
    set @CssClass = 'rsCategoryGreen'
    if @version = 1
		set @Description = 'Container Scanned Yard-Out'
	else
	if @version = 2
		set @Description = 'Container Scanned Dock-Out'
  end
  
  exec @Error = p_DockSchedule_Maintenance_Insert
   @operatorId                      = @operatorId,
   @Subject                         = null,
   @PlannedStart                    = @PlannedStart,
   @PlannedEnd                      = @PlannedEnd,
   @LocationId                      = @LocationId,
   @RecurrenceRule                  = null,
   @RecurrenceParentID              = 0,
   @Description                     = @Description,
   @InboundShipmentId               = @InboundShipmentId,
   @ReceiptId                       = @ReceiptId,
   @CssClass                        = @CssClass,
   @Version                         = @Version
  
  if @Error <> 0
    goto error
  
  result:
      if @Transaction = 1
        commit transaction
      if @showMsg = 1
        select 0
      return 0
    
    error:
      if @Transaction = 1
      begin
        if @in = 0
        RAISERROR (@Errormsg,11,1)
        rollback transaction
      end
      if @showMsg = 1
        select @Error
      return @Error
end
 
 
