﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_DespatchBay_Search_AreaCode
  ///   Filename       : p_DespatchBay_Search_AreaCode.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 11 Jul 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_DespatchBay_Search_AreaCode
(
 @WarehouseId int,
 @AreaCode    nvarchar(10)
)

as
begin
	 set nocount on;
  
  if (select dbo.ufn_Configuration(260, @WarehouseId)) = 1
    set @WarehouseId = null
  
  select -1     as 'DespatchBay',
         'None' as 'DespatchBayName'
  union
  select l.LocationId,
         l.Location + isnull(' (' + w.WarehouseCode + ')', '')
    from Area          a (nolock)
    join AreaLocation al (nolock) on a.AreaId      = al.AreaId
    join Location      l (nolock) on al.LocationId = l.LocationId
    join Warehouse     w (nolock) on a.WarehouseId = w.WarehouseId
    left
    join Warehouse     p (nolock) on w.ParentWarehouseId = p.WarehouseId
   where a.AreaCode = @AreaCode
     and a.WarehouseId = isnull(@WarehouseId, a.WarehouseId)
end
