﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceLog_Parameter
  ///   Filename       : p_InterfaceLog_Parameter.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:27
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceLog table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceLog.InterfaceLogId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceLog_Parameter

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        null as InterfaceLogId
        ,null as 'InterfaceLog'
  union
  select
         InterfaceLog.InterfaceLogId
        ,InterfaceLog.InterfaceLogId as 'InterfaceLog'
    from InterfaceLog
  
end
