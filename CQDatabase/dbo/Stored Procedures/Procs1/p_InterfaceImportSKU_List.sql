﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSKU_List
  ///   Filename       : p_InterfaceImportSKU_List.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:06
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceImportSKU table.
  /// </remarks>
  /// <param>
  /// </param>
  /// <returns>
  ///   InterfaceImportSKU.InterfaceImportSKUId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSKU_List

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
        '-1' as InterfaceImportSKUId
        ,null as 'InterfaceImportSKU'
  union
  select
         InterfaceImportSKU.InterfaceImportSKUId
        ,InterfaceImportSKU.InterfaceImportSKUId as 'InterfaceImportSKU'
    from InterfaceImportSKU
  
end
