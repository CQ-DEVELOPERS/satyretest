﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Driver_by_ContactListId
  ///   Filename       : p_Driver_by_ContactListId.sql
  ///   Create By      : Karen
  ///   Date Created   : August 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Driver_by_ContactListId


as
begin
	 set nocount on;
  
  select '-1' as 'ContactListId',
         null as 'ContactPerson'
  union
  select ContactListId,
		       ContactPerson
    from ContactList
    where Type = 'Driver'
   order by ContactPerson
end
