﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Inbound_Shipment_Search
  ///   Filename       : p_Inbound_Shipment_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Jun 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Inbound_Shipment_Search
(
 @WarehouseId           int = 1,
 @InboundShipmentId	    int,
 @InboundDocumentTypeId	int,
 @ExternalCompanyCode	  nvarchar(30),
 @ExternalCompany	      nvarchar(255),
 @OrderNumber	          nvarchar(30),
 @FromDate	             datetime,
 @ToDate	               datetime
)

as
begin
	 set nocount on;
  
  declare @TableResult as table
  (
   ReceiptId            int,
   InboundDocumentId    int,
   InboundShipmentId    int,
   OrderNumber          nvarchar(30),
   ExternalCompanyCode  nvarchar(30),
   ExternalCompany      nvarchar(255),
   NumberOfLines        int,
   DeliveryDate         datetime,
   StatusId             int,
   Status               nvarchar(50),
   InboundDocumentType  nvarchar(50),
   LocationId           int,
   Location             nvarchar(15),
   CreateDate           datetime,
   Rating               int
  );
  
  if @InboundShipmentId = -1
    set @InboundShipmentId = null
  
  if @InboundDocumentTypeId = -1
    set @InboundDocumentTypeId = null
  
  insert @TableResult
        (ReceiptId,
         id.InboundDocumentId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         DeliveryDate,
         StatusId,
         Status,
         InboundDocumentType,
         LocationId,
         Location,
         CreateDate,
         Rating)
  select r.ReceiptId,
         id.InboundDocumentId,
         id.OrderNumber,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         r.DeliveryDate,
         r.StatusId,
         s.Status,
         idt.InboundDocumentType,
         l.LocationId,
         l.Location,
         id.CreateDate,
         ec.Rating
    from InboundDocument     id  (nolock)
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    join ExternalCompany     ec  (nolock) on id.ExternalCompanyId     = ec.ExternalCompanyId
    join Receipt             r   (nolock) on id.InboundDocumentId     = r.InboundDocumentId
    join Status              s   (nolock) on r.StatusId               = s.StatusId
    left outer
    join Location            l   (nolock) on r.LocationId             = l.LocationId
   where id.InboundDocumentTypeId    = isnull(@InboundDocumentTypeId, id.InboundDocumentTypeId)
     and ec.ExternalCompanyCode   like isnull(@ExternalCompanyCode + '%', ec.ExternalCompanyCode)
     and ec.ExternalCompany       like isnull(@ExternalCompany + '%', ec.ExternalCompany)
     and id.OrderNumber           like isnull(@OrderNumber  + '%', id.OrderNumber)
     and isnull(r.DeliveryDate, id.DeliveryDate) between @FromDate and @ToDate
     and s.Type                    = 'R'
     and s.StatusCode             in ('W','C','D') -- Waiting, Confirmed, Delivered
  
  insert @TableResult
        (InboundShipmentId,
         DeliveryDate,
         OrderNumber)
  select InboundShipmentId,
         ins.ShipmentDate,
         'No Orders Linked'
    from InboundShipment ins
  where not exists(select 1 from InboundShipmentReceipt isr where ins.InboundShipmentId = isr.InboundShipmentId)
    and ins.InboundShipmentId = isnull(@InboundShipmentId, ins.InboundShipmentId)
    and ins.ShipmentDate        between @FromDate and @ToDate
  
  update @TableResult
     set InboundShipmentId = isr.InboundShipmentId
    from @TableResult t
    join InboundShipmentReceipt isr on t.ReceiptId = isr.ReceiptId
  
  update @TableResult
     set NumberOfLines = (select count(1)
                            from InboundLine il
                           where t.InboundDocumentId = il.InboundDocumentId)
    from @TableResult t
  
  select InboundShipmentId,
         ReceiptId,
         OrderNumber,
         ExternalCompanyCode,
         ExternalCompany,
         NumberOfLines,
         DeliveryDate,
         StatusId,
         Status,
         InboundDocumentType,
         LocationId,
         Location,
         CreateDate,
         Rating
    from @TableResult
   where InboundShipmentId is not null
end
