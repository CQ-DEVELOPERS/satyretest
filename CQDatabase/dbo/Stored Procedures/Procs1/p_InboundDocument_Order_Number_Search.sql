﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Order_Number_Search
  ///   Filename       : p_InboundDocument_Order_Number_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 25 Feb 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Order_Number_Search
(
 @WarehouseId int
)

as
begin
	 set nocount on;
  
  select OrderNumber
    from InboundDocument      id (nolock)
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
   where WarehouseId = @WarehouseId
     and InboundDocumentTypeCode = 'RCP'
end
