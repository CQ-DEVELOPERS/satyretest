﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Comment_Link_Select
  ///   Filename       : p_Comment_Link_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Jan 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Comment_Link_Select
(
 @tableId         int,
 @tableName       sysname = null,
 @operatorId      int = null
)

as
begin
	 set nocount on;
  
  select c.CommentId,
         c.Comment,
         o.Operator,
         CreateDate
    from Comment               c (nolock)
    left
    join Operator              o (nolock) on c.OperatorId = o.OperatorId
    left
    join CommentLink          cl (nolock) on c.CommentId = cl.CommentId
   where cl.TableId   = @tableId
     and cl.TableName = @tableName
  order by c.CreateDate desc
end
