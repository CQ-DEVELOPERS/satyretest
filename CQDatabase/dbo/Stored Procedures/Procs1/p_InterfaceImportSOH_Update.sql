﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOH_Update
  ///   Filename       : p_InterfaceImportSOH_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:11
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportSOH table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSOHId int = null,
  ///   @Location nvarchar(20) = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @UnitPrice numeric(13,3) = null,
  ///   @ProcessedDate datetime = null,
  ///   @RecordStatus char(1) = null,
  ///   @RecordType nvarchar(40) = null,
  ///   @InsertDate datetime = null,
  ///   @SKUCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOH_Update
(
 @InterfaceImportSOHId int = null,
 @Location nvarchar(20) = null,
 @ProductCode nvarchar(60) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @UnitPrice numeric(13,3) = null,
 @ProcessedDate datetime = null,
 @RecordStatus char(1) = null,
 @RecordType nvarchar(40) = null,
 @InsertDate datetime = null,
 @SKUCode nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
  if @InterfaceImportSOHId = '-1'
    set @InterfaceImportSOHId = null;
  
	 declare @Error int
 
  update InterfaceImportSOH
     set Location = isnull(@Location, Location),
         ProductCode = isnull(@ProductCode, ProductCode),
         Batch = isnull(@Batch, Batch),
         Quantity = isnull(@Quantity, Quantity),
         UnitPrice = isnull(@UnitPrice, UnitPrice),
         ProcessedDate = isnull(@ProcessedDate, ProcessedDate),
         RecordStatus = isnull(@RecordStatus, RecordStatus),
         RecordType = isnull(@RecordType, RecordType),
         InsertDate = isnull(@InsertDate, InsertDate),
         SKUCode = isnull(@SKUCode, SKUCode) 
   where InterfaceImportSOHId = @InterfaceImportSOHId
  
  select @Error = @@Error
  
  
  return @Error
  
end
