﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Check_OperatorGroup
  ///   Filename       : p_Check_OperatorGroup.sql
  ///   Create By      : Karen
  ///   Date Created   : October 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Check_OperatorGroup
(
 @operatorId  int
)

as
begin
  set nocount on;
  
  declare @Bit          bit,
		  @Admin		nvarchar(30)
  
 
  select @Admin = og.OperatorGroupCode 
    from Operator o
    join OperatorGroup og (nolock) on og.OperatorGroupId = o.OperatorGroupId
   where o.OperatorId = @operatorId
   
 
  if @Admin in ('A','S')
    set @bit = 1
  else
    set @bit = 0  
  
  select @bit
  return @bit
end
