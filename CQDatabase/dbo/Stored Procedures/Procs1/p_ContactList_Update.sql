﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContactList_Update
  ///   Filename       : p_ContactList_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Apr 2014 11:18:13
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ContactList table.
  /// </remarks>
  /// <param>
  ///   @ContactListId int = null,
  ///   @ExternalCompanyId int = null,
  ///   @OperatorId int = null,
  ///   @ContactPerson nvarchar(510) = null,
  ///   @Telephone nvarchar(510) = null,
  ///   @Fax nvarchar(510) = null,
  ///   @EMail nvarchar(510) = null,
  ///   @Alias varchar(50) = null,
  ///   @Type nvarchar(40) = null,
  ///   @ReportTypeId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContactList_Update
(
 @ContactListId int = null,
 @ExternalCompanyId int = null,
 @OperatorId int = null,
 @ContactPerson nvarchar(510) = null,
 @Telephone nvarchar(510) = null,
 @Fax nvarchar(510) = null,
 @EMail nvarchar(510) = null,
 @Alias varchar(50) = null,
 @Type nvarchar(40) = null,
 @ReportTypeId int = null 
)

as
begin
	 set nocount on;
  
  if @ContactListId = '-1'
    set @ContactListId = null;
  
  if @ExternalCompanyId = '-1'
    set @ExternalCompanyId = null;
  
  if @OperatorId = '-1'
    set @OperatorId = null;
  
  if @ReportTypeId = '-1'
    set @ReportTypeId = null;
  
	 declare @Error int
 
  update ContactList
     set ExternalCompanyId = isnull(@ExternalCompanyId, ExternalCompanyId),
         OperatorId = isnull(@OperatorId, OperatorId),
         ContactPerson = isnull(@ContactPerson, ContactPerson),
         Telephone = isnull(@Telephone, Telephone),
         Fax = isnull(@Fax, Fax),
         EMail = isnull(@EMail, EMail),
         Alias = isnull(@Alias, Alias),
         Type = isnull(@Type, Type),
         ReportTypeId = isnull(@ReportTypeId, ReportTypeId) 
   where ContactListId = @ContactListId
  
  select @Error = @@Error
  
  
  return @Error
  
end
