﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportDetail_Insert
  ///   Filename       : p_InterfaceImportDetail_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:23
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceImportDetail table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportHeaderId int = null,
  ///   @ForeignKey nvarchar(60) = null,
  ///   @LineNumber int = null,
  ///   @ProductCode nvarchar(60) = null,
  ///   @Product nvarchar(100) = null,
  ///   @SKUCode nvarchar(60) = null,
  ///   @Batch nvarchar(100) = null,
  ///   @Quantity float = null,
  ///   @Weight float = null,
  ///   @RetailPrice decimal(13,2) = null,
  ///   @NetPrice decimal(13,2) = null,
  ///   @LineTotal decimal(13,2) = null,
  ///   @Volume decimal(13,3) = null,
  ///   @Additional1 nvarchar(510) = null,
  ///   @Additional2 nvarchar(510) = null,
  ///   @Additional3 nvarchar(510) = null,
  ///   @Additional4 nvarchar(510) = null,
  ///   @Additional5 nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   InterfaceImportDetail.InterfaceImportHeaderId,
  ///   InterfaceImportDetail.ForeignKey,
  ///   InterfaceImportDetail.LineNumber,
  ///   InterfaceImportDetail.ProductCode,
  ///   InterfaceImportDetail.Product,
  ///   InterfaceImportDetail.SKUCode,
  ///   InterfaceImportDetail.Batch,
  ///   InterfaceImportDetail.Quantity,
  ///   InterfaceImportDetail.Weight,
  ///   InterfaceImportDetail.RetailPrice,
  ///   InterfaceImportDetail.NetPrice,
  ///   InterfaceImportDetail.LineTotal,
  ///   InterfaceImportDetail.Volume,
  ///   InterfaceImportDetail.Additional1,
  ///   InterfaceImportDetail.Additional2,
  ///   InterfaceImportDetail.Additional3,
  ///   InterfaceImportDetail.Additional4,
  ///   InterfaceImportDetail.Additional5 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportDetail_Insert
(
 @InterfaceImportHeaderId int = null,
 @ForeignKey nvarchar(60) = null,
 @LineNumber int = null,
 @ProductCode nvarchar(60) = null,
 @Product nvarchar(100) = null,
 @SKUCode nvarchar(60) = null,
 @Batch nvarchar(100) = null,
 @Quantity float = null,
 @Weight float = null,
 @RetailPrice decimal(13,2) = null,
 @NetPrice decimal(13,2) = null,
 @LineTotal decimal(13,2) = null,
 @Volume decimal(13,3) = null,
 @Additional1 nvarchar(510) = null,
 @Additional2 nvarchar(510) = null,
 @Additional3 nvarchar(510) = null,
 @Additional4 nvarchar(510) = null,
 @Additional5 nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceImportDetail
        (InterfaceImportHeaderId,
         ForeignKey,
         LineNumber,
         ProductCode,
         Product,
         SKUCode,
         Batch,
         Quantity,
         Weight,
         RetailPrice,
         NetPrice,
         LineTotal,
         Volume,
         Additional1,
         Additional2,
         Additional3,
         Additional4,
         Additional5)
  select @InterfaceImportHeaderId,
         @ForeignKey,
         @LineNumber,
         @ProductCode,
         @Product,
         @SKUCode,
         @Batch,
         @Quantity,
         @Weight,
         @RetailPrice,
         @NetPrice,
         @LineTotal,
         @Volume,
         @Additional1,
         @Additional2,
         @Additional3,
         @Additional4,
         @Additional5 
  
  select @Error = @@Error
  
  
  return @Error
  
end
