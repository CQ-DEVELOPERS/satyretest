﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundDocument_Select
  ///   Filename       : p_InboundDocument_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Feb 2013 14:47:32
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InboundDocument table.
  /// </remarks>
  /// <param>
  ///   @InboundDocumentId int = null 
  /// </param>
  /// <returns>
  ///   InboundDocument.InboundDocumentId,
  ///   InboundDocument.InboundDocumentTypeId,
  ///   InboundDocument.ExternalCompanyId,
  ///   InboundDocument.StatusId,
  ///   InboundDocument.WarehouseId,
  ///   InboundDocument.OrderNumber,
  ///   InboundDocument.DeliveryDate,
  ///   InboundDocument.CreateDate,
  ///   InboundDocument.ModifiedDate,
  ///   InboundDocument.ReferenceNumber,
  ///   InboundDocument.FromLocation,
  ///   InboundDocument.ToLocation,
  ///   InboundDocument.ReasonId,
  ///   InboundDocument.Remarks,
  ///   InboundDocument.StorageUnitId,
  ///   InboundDocument.BatchId,
  ///   InboundDocument.DivisionId,
  ///   InboundDocument.PrincipalId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundDocument_Select
(
 @InboundDocumentId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InboundDocument.InboundDocumentId
        ,InboundDocument.InboundDocumentTypeId
        ,InboundDocument.ExternalCompanyId
        ,InboundDocument.StatusId
        ,InboundDocument.WarehouseId
        ,InboundDocument.OrderNumber
        ,InboundDocument.DeliveryDate
        ,InboundDocument.CreateDate
        ,InboundDocument.ModifiedDate
        ,InboundDocument.ReferenceNumber
        ,InboundDocument.FromLocation
        ,InboundDocument.ToLocation
        ,InboundDocument.ReasonId
        ,InboundDocument.Remarks
        ,InboundDocument.StorageUnitId
        ,InboundDocument.BatchId
        ,InboundDocument.DivisionId
        ,InboundDocument.PrincipalId
    from InboundDocument
   where isnull(InboundDocument.InboundDocumentId,'0')  = isnull(@InboundDocumentId, isnull(InboundDocument.InboundDocumentId,'0'))
  order by OrderNumber
  
end
