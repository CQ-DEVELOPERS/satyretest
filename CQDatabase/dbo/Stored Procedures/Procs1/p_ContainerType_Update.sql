﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ContainerType_Update
  ///   Filename       : p_ContainerType_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:30
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the ContainerType table.
  /// </remarks>
  /// <param>
  ///   @ContainerTypeId int = null,
  ///   @ContainerType nvarchar(100) = null,
  ///   @ContainerTypeCode nvarchar(20) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ContainerType_Update
(
 @ContainerTypeId int = null,
 @ContainerType nvarchar(100) = null,
 @ContainerTypeCode nvarchar(20) = null 
)

as
begin
	 set nocount on;
  
  if @ContainerTypeId = '-1'
    set @ContainerTypeId = null;
  
  if @ContainerType = '-1'
    set @ContainerType = null;
  
  if @ContainerTypeCode = '-1'
    set @ContainerTypeCode = null;
  
	 declare @Error int
 
  update ContainerType
     set ContainerType = isnull(@ContainerType, ContainerType),
         ContainerTypeCode = isnull(@ContainerTypeCode, ContainerTypeCode) 
   where ContainerTypeId = @ContainerTypeId
  
  select @Error = @@Error
  
  
  return @Error
  
end
