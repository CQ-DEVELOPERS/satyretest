﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSOH_Search
  ///   Filename       : p_InterfaceImportSOH_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:11
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportSOH table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportSOHId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportSOH.InterfaceImportSOHId,
  ///   InterfaceImportSOH.Location,
  ///   InterfaceImportSOH.ProductCode,
  ///   InterfaceImportSOH.Batch,
  ///   InterfaceImportSOH.Quantity,
  ///   InterfaceImportSOH.UnitPrice,
  ///   InterfaceImportSOH.ProcessedDate,
  ///   InterfaceImportSOH.RecordStatus,
  ///   InterfaceImportSOH.RecordType,
  ///   InterfaceImportSOH.InsertDate,
  ///   InterfaceImportSOH.SKUCode 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSOH_Search
(
 @InterfaceImportSOHId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceImportSOHId = '-1'
    set @InterfaceImportSOHId = null;
  
 
  select
         InterfaceImportSOH.InterfaceImportSOHId
        ,InterfaceImportSOH.Location
        ,InterfaceImportSOH.ProductCode
        ,InterfaceImportSOH.Batch
        ,InterfaceImportSOH.Quantity
        ,InterfaceImportSOH.UnitPrice
        ,InterfaceImportSOH.ProcessedDate
        ,InterfaceImportSOH.RecordStatus
        ,InterfaceImportSOH.RecordType
        ,InterfaceImportSOH.InsertDate
        ,InterfaceImportSOH.SKUCode
    from InterfaceImportSOH
   where isnull(InterfaceImportSOH.InterfaceImportSOHId,'0')  = isnull(@InterfaceImportSOHId, isnull(InterfaceImportSOH.InterfaceImportSOHId,'0'))
  
end
