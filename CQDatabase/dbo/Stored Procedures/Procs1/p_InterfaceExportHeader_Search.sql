﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceExportHeader_Search
  ///   Filename       : p_InterfaceExportHeader_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:02
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceExportHeader table.
  /// </remarks>
  /// <param>
  ///   @InterfaceExportHeaderId int = null output 
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceExportHeader.InterfaceExportHeaderId,
  ///   InterfaceExportHeader.IssueId,
  ///   InterfaceExportHeader.PrimaryKey,
  ///   InterfaceExportHeader.OrderNumber,
  ///   InterfaceExportHeader.RecordType,
  ///   InterfaceExportHeader.RecordStatus,
  ///   InterfaceExportHeader.CompanyCode,
  ///   InterfaceExportHeader.Company,
  ///   InterfaceExportHeader.Address,
  ///   InterfaceExportHeader.FromWarehouseCode,
  ///   InterfaceExportHeader.ToWarehouseCode,
  ///   InterfaceExportHeader.Route,
  ///   InterfaceExportHeader.DeliveryNoteNumber,
  ///   InterfaceExportHeader.ContainerNumber,
  ///   InterfaceExportHeader.SealNumber,
  ///   InterfaceExportHeader.DeliveryDate,
  ///   InterfaceExportHeader.Remarks,
  ///   InterfaceExportHeader.NumberOfLines,
  ///   InterfaceExportHeader.Additional1,
  ///   InterfaceExportHeader.Additional2,
  ///   InterfaceExportHeader.Additional3,
  ///   InterfaceExportHeader.Additional4,
  ///   InterfaceExportHeader.Additional5,
  ///   InterfaceExportHeader.Additional6,
  ///   InterfaceExportHeader.Additional7,
  ///   InterfaceExportHeader.Additional8,
  ///   InterfaceExportHeader.Additional9,
  ///   InterfaceExportHeader.Additional10,
  ///   InterfaceExportHeader.ProcessedDate,
  ///   InterfaceExportHeader.InsertDate,
  ///   InterfaceExportHeader.WebServiceMsg 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceExportHeader_Search
(
 @InterfaceExportHeaderId int = null output,
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
  if @InterfaceExportHeaderId = '-1'
    set @InterfaceExportHeaderId = null;
  
 
  select
         InterfaceExportHeader.InterfaceExportHeaderId
        ,InterfaceExportHeader.IssueId
        ,InterfaceExportHeader.PrimaryKey
        ,InterfaceExportHeader.OrderNumber
        ,InterfaceExportHeader.RecordType
        ,InterfaceExportHeader.RecordStatus
        ,InterfaceExportHeader.CompanyCode
        ,InterfaceExportHeader.Company
        ,InterfaceExportHeader.Address
        ,InterfaceExportHeader.FromWarehouseCode
        ,InterfaceExportHeader.ToWarehouseCode
        ,InterfaceExportHeader.Route
        ,InterfaceExportHeader.DeliveryNoteNumber
        ,InterfaceExportHeader.ContainerNumber
        ,InterfaceExportHeader.SealNumber
        ,InterfaceExportHeader.DeliveryDate
        ,InterfaceExportHeader.Remarks
        ,InterfaceExportHeader.NumberOfLines
        ,InterfaceExportHeader.Additional1
        ,InterfaceExportHeader.Additional2
        ,InterfaceExportHeader.Additional3
        ,InterfaceExportHeader.Additional4
        ,InterfaceExportHeader.Additional5
        ,InterfaceExportHeader.Additional6
        ,InterfaceExportHeader.Additional7
        ,InterfaceExportHeader.Additional8
        ,InterfaceExportHeader.Additional9
        ,InterfaceExportHeader.Additional10
        ,InterfaceExportHeader.ProcessedDate
        ,InterfaceExportHeader.InsertDate
        ,InterfaceExportHeader.WebServiceMsg
    from InterfaceExportHeader
   where isnull(InterfaceExportHeader.InterfaceExportHeaderId,'0')  = isnull(@InterfaceExportHeaderId, isnull(InterfaceExportHeader.InterfaceExportHeaderId,'0'))
  
end
