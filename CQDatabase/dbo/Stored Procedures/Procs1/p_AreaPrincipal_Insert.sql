﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaPrincipal_Insert
  ///   Filename       : p_AreaPrincipal_Insert.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2013
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the AreaPrincipal table.
  /// </remarks>
  /// <param>

  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaPrincipal_Insert
(	@PrincipalId		int = null,
	@AreaId 			    int = null
)
    
    

as
begin
	 set nocount on;
  
  declare @Error VARCHAR(255)
  
 
	  if not exists(select top 1 1 from AreaPrincipal ap where @PrincipalId = ap.PrincipalId and @AreaId = ap.AreaId)
	  insert	AreaPrincipal
			   (AreaId,
				PrincipalId)
	  select	@AreaId,
				@PrincipalId	  
    
  SELECT @Error
end
