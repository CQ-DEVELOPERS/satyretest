﻿CREATE procedure [dbo].[p_Interface_WebService_IORSEND]
(
  @XMLBody NVARCHAR(MAX) OUTPUT 
, @PrincipalCode NVARCHAR(30) = NULL
)
--WITH ENCRYPTION
AS
BEGIN

DECLARE @GETDATE DATETIME

SELECT @GETDATE = dbo.ufn_GetDate()

UPDATE de
SET ProcessedDate = @GETDATE
FROM InterfaceExportShipmentDelivery de
INNER JOIN InterfaceExportShipmentDetail dl on de.InterfaceExportShipmentDeliveryId = dl.InterfaceExportShipmentDeliveryId
WHERE RecordStatus = 'N'
AND (ProcessedDate IS NULL OR ProcessedDate < DATEADD(HH, -1, GETDATE()))
AND RecordType IN ( 'DI' )
AND (PrincipalCode = @PrincipalCode OR @PrincipalCode IS NULL)
AND CheckQuantity > 0

SELECT @XMLBody = '<?xml version="1.0" encoding="utf-16"?>' +
(SELECT
(SELECT
'CQuential' AS 'Source'
,'' AS 'Target'
,GETDATE() AS 'CreateDate'
,'POConfirmation' AS 'FileType'
FOR XML PATH('Header'), TYPE)
,(SELECT
(SELECT
'DI' AS 'ItemType'
,'N' AS 'ItemStatus'
,'InterfaceExportShipmentDelivery' AS 'InterfaceTable'
,H.InterfaceExportShipmentDeliveryId AS 'InterfaceTableId'
,H.ForeignKey AS 'PrimaryKey'
,H.WaybillNo AS 'OrderNumber'
,H.InvoiceNumber AS 'InvoiceNumber'
,H.ConsigneeCode AS 'CompanyCode'
,H.ConsigneeName AS 'Company'
,isnull(H.Street,'') + ' ' + isnull(H.Suburb,'') + ' ' + isnull(H.Town,'') + ' ' + isnull(H.Country,'')  AS 'Address'
,H.PrincipalCode AS 'FromWarehouseCode'
,ISNULL( null, '' ) AS 'ToWarehouseCode'
,H2.RouteCode AS 'Route'
,H.DeliveryNoteNumber AS 'DeliveryNoteNumber'
,'' AS 'ContainerNumber'
,H.SealNumber AS 'SealNumber'
,H.DeliveryDate AS 'DeliveryDate'
,'' AS 'Remarks'
,'' AS 'NumberOfLines'
,'' AS 'Additional1'
,'' AS 'Additional2'
,'' AS 'Additional3'
,'' AS 'Additional4'
,'' AS 'Additional5'
,'' AS 'Additional6'
,'' AS 'Additional7'
,'' AS 'Additional8'
,'' AS 'Additional9'
,'' AS 'Additional10'
,0	AS 'FinalDelivery'
,WaveId		AS	'WaveId'
	,(SELECT
		'InterfaceExportShipmentDetail' AS 'InterfaceTable'
	,	ROW_NUMBER() OVER (Order By D1.JobId) AS 'InterfaceTableId'
	--,	H1.ForeignKey AS 'ForeignKey'
	--,	D1.LineNumber AS 'LineNumber'
	,	D1.JobId AS 'JobId'
	,	D1.ProductCode AS 'ProductCode'
	,	D1.Product AS 'Product'
	,	D1.SKUCode AS 'SKUCode'
	,	CASE WHEN LEN(ISNULL(D1.Batch, '')) = 0 THEN 'Default' ELSE D1.Batch END AS 'Batch'
	,	SUM( ISNULL(D1.CheckQuantity,0)) AS 'Quantity'
	,	SUM( D1.[Weight] ) AS 'Weight'
	,	D1.ReferenceNumber	AS	'ReferenceNumber'
	,0	AS 'Volume'
	,'' AS 'Additional1'
	,'' AS 'Additional2'
	,'' AS 'Additional3'
	,'' AS 'Additional4'
	,'' AS 'Additional5'
	,'' AS 'Additional6'
	,'' AS 'Additional7'
	,'' AS 'Additional8'
	,'' AS 'Additional9'
	,'' AS 'Additional10'
	FROM InterfaceExportShipmentDelivery H1
	INNER JOIN InterfaceExportShipmentDetail D1 on H1.InterfaceExportShipmentDeliveryId = D1.InterfaceExportShipmentDeliveryId
	WHERE H1.InterfaceExportShipmentDeliveryId = H.InterfaceExportShipmentDeliveryId
	AND (H1.PrincipalCode = @PrincipalCode OR @PrincipalCode IS NULL)
	AND D1.CheckQuantity > 0
	GROUP BY  D1.JobId, Product, ProductCode, SKUCode, ReferenceNumber,
	CASE WHEN LEN(ISNULL(D1.Batch, '')) = 0 THEN 'Default' ELSE D1.Batch END
	FOR XML PATH('ItemLine'), TYPE)
FROM InterfaceExportShipmentDelivery H
INNER JOIN InterfaceExportShipment H2 on H.InterfaceExportShipmentId = H2.InterfaceExportShipmentId
WHERE H.RecordStatus = 'N'
AND H.RecordType IN ( 'DI' )
AND H.ProcessedDate = @GETDATE
AND (H.PrincipalCode = @PrincipalCode OR @PrincipalCode IS NULL)
FOR XML PATH('Item'), TYPE)
FOR XML PATH('Body'), TYPE)
FOR XML PATH('root'))

UPDATE InterfaceExportShipmentDelivery
SET RecordStatus = 'Y'
WHERE RecordStatus = 'N'
AND ProcessedDate = @GETDATE
AND RecordType IN ( 'DI' )
AND (PrincipalCode = @PrincipalCode OR @PrincipalCode IS NULL)

END

