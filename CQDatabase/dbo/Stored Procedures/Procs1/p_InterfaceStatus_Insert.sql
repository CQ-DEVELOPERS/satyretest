﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceStatus_Insert
  ///   Filename       : p_InterfaceStatus_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:42
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceStatus table.
  /// </remarks>
  /// <param>
  ///   @StatusId int = null,
  ///   @StatusCode nvarchar(20) = null,
  ///   @StatusDescription nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   InterfaceStatus.StatusId,
  ///   InterfaceStatus.StatusCode,
  ///   InterfaceStatus.StatusDescription 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceStatus_Insert
(
 @StatusId int = null,
 @StatusCode nvarchar(20) = null,
 @StatusDescription nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceStatus
        (StatusId,
         StatusCode,
         StatusDescription)
  select @StatusId,
         @StatusCode,
         @StatusDescription 
  
  select @Error = @@Error
  
  
  return @Error
  
end
