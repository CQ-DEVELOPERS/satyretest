﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_LinkedToPrincipal
  ///   Filename       : p_Area_LinkedToPrincipal.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2013
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_LinkedToPrincipal
( 
@PrincipalId	int
)


as
begin
	 set nocount on;
  
  select a.AreaId,
		 a.Area,
		 a.AreaCode
    from Area a
    join AreaPrincipal ap (nolock) on ap.AreaId = a.AreaId
    where ap.PrincipalId = @PrincipalId
	order by Area
end
