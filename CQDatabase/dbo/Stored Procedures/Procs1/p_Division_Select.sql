﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Division_Select
  ///   Filename       : p_Division_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:23:06
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Division table.
  /// </remarks>
  /// <param>
  ///   @DivisionId int = null 
  /// </param>
  /// <returns>
  ///   Division.DivisionId,
  ///   Division.DivisionCode,
  ///   Division.Division 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Division_Select
(
 @DivisionId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Division.DivisionId
        ,Division.DivisionCode
        ,Division.Division
    from Division
   where isnull(Division.DivisionId,'0')  = isnull(@DivisionId, isnull(Division.DivisionId,'0'))
  
end
