﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InboundLine_Insert
  ///   Filename       : p_InboundLine_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 14 Jan 2014 08:48:43
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InboundLine table.
  /// </remarks>
  /// <param>
  ///   @InboundLineId int = null output,
  ///   @InboundDocumentId int = null,
  ///   @StorageUnitId int = null,
  ///   @StatusId int = null,
  ///   @LineNumber int = null,
  ///   @Quantity float = null,
  ///   @BatchId int = null,
  ///   @UnitPrice numeric(13,2) = null,
  ///   @ReasonId int = null,
  ///   @ChildStorageUnitId int = null,
  ///   @BOELineNumber nvarchar(100) = null,
  ///   @TariffCode nvarchar(100) = null,
  ///   @CountryofOrigin nvarchar(200) = null,
  ///   @Reference1 nvarchar(100) = null,
  ///   @Reference2 nvarchar(100) = null 
  /// </param>
  /// <returns>
  ///   InboundLine.InboundLineId,
  ///   InboundLine.InboundDocumentId,
  ///   InboundLine.StorageUnitId,
  ///   InboundLine.StatusId,
  ///   InboundLine.LineNumber,
  ///   InboundLine.Quantity,
  ///   InboundLine.BatchId,
  ///   InboundLine.UnitPrice,
  ///   InboundLine.ReasonId,
  ///   InboundLine.ChildStorageUnitId,
  ///   InboundLine.BOELineNumber,
  ///   InboundLine.TariffCode,
  ///   InboundLine.CountryofOrigin,
  ///   InboundLine.Reference1,
  ///   InboundLine.Reference2 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InboundLine_Insert
(
 @InboundLineId int = null output,
 @InboundDocumentId int = null,
 @StorageUnitId int = null,
 @StatusId int = null,
 @LineNumber int = null,
 @Quantity float = null,
 @BatchId int = null,
 @UnitPrice numeric(13,2) = null,
 @ReasonId int = null,
 @ChildStorageUnitId int = null,
 @BOELineNumber nvarchar(100) = null,
 @TariffCode nvarchar(100) = null,
 @CountryofOrigin nvarchar(200) = null,
 @Reference1 nvarchar(100) = null,
 @Reference2 nvarchar(100) = null 
)

as
begin
	 set nocount on;
  
  if @InboundLineId = '-1'
    set @InboundLineId = null;
  
  if @InboundDocumentId = '-1'
    set @InboundDocumentId = null;
  
  if @StorageUnitId = '-1'
    set @StorageUnitId = null;
  
  if @StatusId = '-1'
    set @StatusId = null;
  
  if @BatchId = '-1'
    set @BatchId = null;
  
  if @ReasonId = '-1'
    set @ReasonId = null;
  
  if @ChildStorageUnitId = '-1'
    set @ChildStorageUnitId = null;
  
	 declare @Error int
 
  insert InboundLine
        (InboundDocumentId,
         StorageUnitId,
         StatusId,
         LineNumber,
         Quantity,
         BatchId,
         UnitPrice,
         ReasonId,
         ChildStorageUnitId,
         BOELineNumber,
         TariffCode,
         CountryofOrigin,
         Reference1,
         Reference2)
  select @InboundDocumentId,
         @StorageUnitId,
         @StatusId,
         @LineNumber,
         @Quantity,
         @BatchId,
         @UnitPrice,
         @ReasonId,
         @ChildStorageUnitId,
         @BOELineNumber,
         @TariffCode,
         @CountryofOrigin,
         @Reference1,
         @Reference2 
  
  select @Error = @@Error, @InboundLineId = scope_identity()
  
  if @Error = 0
    exec @Error = p_InboundLineHistory_Insert
         @InboundLineId = @InboundLineId,
         @InboundDocumentId = @InboundDocumentId,
         @StorageUnitId = @StorageUnitId,
         @StatusId = @StatusId,
         @LineNumber = @LineNumber,
         @Quantity = @Quantity,
         @BatchId = @BatchId,
         @UnitPrice = @UnitPrice,
         @ReasonId = @ReasonId,
         @ChildStorageUnitId = @ChildStorageUnitId,
         @BOELineNumber = @BOELineNumber,
         @TariffCode = @TariffCode,
         @CountryofOrigin = @CountryofOrigin,
         @Reference1 = @Reference1,
         @Reference2 = @Reference2,
         @CommandType = 'Insert'
  
  return @Error
  
end
