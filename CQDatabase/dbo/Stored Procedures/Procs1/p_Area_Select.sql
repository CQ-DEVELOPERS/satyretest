﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Area_Select
  ///   Filename       : p_Area_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 28 Jul 2014 09:46:59
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Area table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null 
  /// </param>
  /// <returns>
  ///   Area.AreaId,
  ///   Area.WarehouseId,
  ///   Area.Area,
  ///   Area.AreaCode,
  ///   Area.StockOnHand,
  ///   Area.AreaSequence,
  ///   Area.AreaType,
  ///   Area.WarehouseCode,
  ///   Area.Replenish,
  ///   Area.MinimumShelfLife,
  ///   Area.Undefined,
  ///   Area.STEKPI,
  ///   Area.BatchTracked,
  ///   Area.StockTake,
  ///   Area.NarrowAisle,
  ///   Area.Interleaving,
  ///   Area.PandD,
  ///   Area.SmallPick,
  ///   Area.AutoLink,
  ///   Area.WarehouseCode2 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Area_Select
(
 @AreaId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Area.AreaId
        ,Area.WarehouseId
        ,Area.Area
        ,Area.AreaCode
        ,Area.StockOnHand
        ,Area.AreaSequence
        ,Area.AreaType
        ,Area.WarehouseCode
        ,Area.Replenish
        ,Area.MinimumShelfLife
        ,Area.Undefined
        ,Area.STEKPI
        ,Area.BatchTracked
        ,Area.StockTake
        ,Area.NarrowAisle
        ,Area.Interleaving
        ,Area.PandD
        ,Area.SmallPick
        ,Area.AutoLink
        ,Area.WarehouseCode2
    from Area
   where isnull(Area.AreaId,'0')  = isnull(@AreaId, isnull(Area.AreaId,'0'))
  order by Area
  
end
