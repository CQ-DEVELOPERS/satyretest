﻿create procedure dbo.p_Interface_WebService_CheckGet
(
	@doc nvarchar(max) output
)
--with encryption
as
begin
	 declare @ReferenceNumber     varchar(50),
	         @idoc                int,
	         @xml                 nvarchar(max),
	         @JobId               int,
	         @WarehouseId         int,
	         @StorageUnitId       int,
	         @ProductId           int,
	         @SKUId               int,
	         @BatchId             int,
	         @ProductCode         nvarchar(30),
	         @Product             nvarchar(255),
	         @SKUCode             nvarchar(20),
	         @Batch               nvarchar(50),
	         @ConfirmedQuantity   float,
	         @Barcode             nvarchar(50)
	 
	 EXEC sp_xml_preparedocument @idoc OUTPUT, @doc

	 SELECT @ReferenceNumber = ReferenceNumber
		 FROM  OPENXML (@idoc, '/root/Header',1)
            WITH (ReferenceNumber varchar(50) 'ReferenceNumber')
  
  IF LEFT(@ReferenceNumber, 2) = 'J:'
  BEGIN
        select
             @JobId = JobId 
            ,@WarehouseId = WarehouseId
        from Job (nolock)
       where JobId = CONVERT(Int, SUBSTRING(@ReferenceNumber, 3, LEN(@ReferenceNumber) - 2))
  END
  ELSE
  BEGIN
      select @JobId = JobId
            ,@WarehouseId = WarehouseId
        from Job    j (nolock)
        join Status s (nolock) on j.StatusId = s.StatusId
       where j.ReferenceNumber = @ReferenceNumber
         and s.StatusCode      = 'CK' --Checking
  END
  
  IF (ISNULL(@JobId, 0) = 0)
    SELECT @XML = '<?xml version="1.0" encoding="utf-16"?><root>Reference not found</root>'
  ELSE
  BEGIN
    
    SELECT @XML = '<?xml version="1.0" encoding="utf-16"?>' +
        (SELECT
             @JobId                                 As 'JobId'
            ,@ReferenceNumber                       As 'ReferenceNumber'
            ,(SELECT
                  sub.StorageUnitBatchId
                 ,p.ProductCode
                 ,p.Product
                 ,sk.SKUCode
                 ,b.Batch
                 ,SUM(convert(numeric(13,6), i.Quantity))                   AS 'ExpectedQuantity'
                 ,sum(convert(numeric(13,6), i.ConfirmedQuantity))          AS 'PickedQuantity'
                 ,SUM(ISNULL(convert(numeric(13,6), i.CheckQuantity),0))    AS 'CheckedQuantity'
                 ,(SELECT distinct 
                     ISNULL(k.Barcode, p.ProductCode) As 'Barcode'
                    ,convert(int, k.Quantity) as 'Quantity'
                   from Pack k
                   where k.StorageUnitId = su.StorageUnitId
                     and (k.Barcode is not null
                          or k.Quantity = 1)
                   FOR XML PATH('Barcodes'), TYPE)
            from Job                j (nolock)
            join Instruction        i (nolock) on j.JobId              = i.JobId
            join StorageUnitBatch sub (nolock) on i.StorageUnitBatchId = sub.StorageUnitBatchId
            join StorageUnit       su (nolock) on sub.StorageUnitId    = su.StorageUnitId
            join Product            p (nolock) on p.ProductId = su.ProductId
            join SKU               sk (nolock) on sk.SKUId = su.SKUId
            join Batch              b (nolock) on sub.BatchId = b.BatchId
            where j.JobId = @JobId
            group by 
                  p.ProductCode
                 ,p.Product
                 ,sk.SKUCode
                 ,b.Batch
                 ,su.StorageUnitId
                 ,sub.StorageUnitBatchId
            FOR XML PATH('Instruction'), TYPE)  
        FOR XML PATH('root'))
    END
  set @doc = @xml
end
