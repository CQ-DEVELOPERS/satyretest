﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportProduct_Delete
  ///   Filename       : p_InterfaceImportProduct_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:03
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the InterfaceImportProduct table.
  /// </remarks>
  /// <param>
  ///   @InterfaceImportProductId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportProduct_Delete
(
 @InterfaceImportProductId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete InterfaceImportProduct
     where InterfaceImportProductId = @InterfaceImportProductId
  
  select @Error = @@Error
  
  
  return @Error
  
end
