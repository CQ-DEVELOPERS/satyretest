﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportLotStatus_Update
  ///   Filename       : p_InterfaceImportLotStatus_Update.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:32:46
  /// </summary>
  /// <remarks>
  ///   Updates a rows in the InterfaceImportLotStatus table.
  /// </remarks>
  /// <param>
  ///   @StatusDescription nvarchar(510) = null,
  ///   @StatusID int = null,
  ///   @HostId varchar(10) = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportLotStatus_Update
(
 @StatusDescription nvarchar(510) = null,
 @StatusID int = null,
 @HostId varchar(10) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  update InterfaceImportLotStatus
     set StatusDescription = isnull(@StatusDescription, StatusDescription),
         StatusID = isnull(@StatusID, StatusID),
         HostId = isnull(@HostId, HostId) 
  
  select @Error = @@Error
  
  
  return @Error
  
end
