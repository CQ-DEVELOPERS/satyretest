﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Interface_Export_SO_Insert
  ///   Filename       : p_Interface_Export_SO_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 06 Jan 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Interface_Export_SO_Insert
(
 @IssueId int,
 @ZeroLines		bit = null
)

as
begin
	 set nocount on;
  
  declare @Error                     int,
          @Errormsg                  varchar(500),
          @GetDate                   datetime,
          @OrderNumber               varchar(30),
          @OutboundDocumentId        int,
          @Delivery                  int,
          @InterfaceImportHeaderId int,
          @InterfaceExportHeaderId int,
          @rowcount                  int,
          @ExternalCompanyId         int,
          @Backorder                 bit,
          @LoadIndicator             bit
  
  select @GetDate = dbo.ufn_Getdate()
  
  set @Errormsg = 'Error executing p_Interface_Export_SO_Insert'
  
  if exists(select * from OutboundDocument      od (nolock) 
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    join Issue                  i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
   where i.IssueId = @IssueId
     and odt.OutboundDocumentTypeCode = 'RET')
     Begin
     insert into InterfaceExportSOHeader(OrderNumber)Values('Credit Note')
     return
     End
  
  select @OrderNumber        = od.OrderNumber,
         @OutboundDocumentId = od.OutboundDocumentId,
         @Delivery           = i.Delivery,
         @ExternalCompanyId  = od.ExternalCompanyId,
         @LoadIndicator      = i.LoadIndicator
    from OutboundDocument      od (nolock) 
    join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
    join Issue                  i (nolock) on od.OutboundDocumentId = i.OutboundDocumentId
   where i.IssueId = @IssueId
     and odt.OutboundDocumentTypeCode not like 'CR%'
  
  select @Backorder = Backorder
    from ExternalCompany (nolock)
   where ExternalCompanyId = @ExternalCompanyId
  
  if @Backorder is null set @Backorder = 0
  if @LoadIndicator is null set @LoadIndicator = 0
  
  begin transaction
  
  if not exists(select top 1 1
                  from InterfaceExportHeader
                 where IssueId = @IssueId
                 AND RecordType != 'Allocated')
  begin
    if @LoadIndicator = 0 and @Backorder = 1 and exists(select top 1 1 from IssueLine where IssueId = @IssueId and (Quantity - ConfirmedQuatity) > 0)
    begin
      exec @Error = p_Outbound_Backorder_Insert
       @IssueId            = @IssueId
      
      if @Error <> 0
        goto error
    end
    
    select @InterfaceImportHeaderId = max(InterfaceImportHeaderId)
      from InterfaceImportHeader
     where RecordStatus = 'C'
       and OrderNumber = @OrderNumber
    
    if @InterfaceImportHeaderId is null
      select @InterfaceImportHeaderId = max(InterfaceImportHeaderId)
        from InterfaceImportHeader
       where OrderNumber = @OrderNumber
    
    insert InterfaceExportHeader
          (IssueId,
           PrimaryKey,
           OrderNumber,
           RecordType,
           RecordStatus,
           CompanyCode,
           Company,
           DeliveryDate,
           Remarks)
    select distinct @IssueId,
           od.OrderNumber,
           od.OrderNumber,
           odt.OutboundDocumentTypeCode,
           'N',
           ec.ExternalCompanyCode,
           ec.ExternalCompany,
           i.DeliveryDate,
           i.Remarks
      from OutboundDocument        od (nolock)
      join OutboundDocumentType   odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
      join Issue                    i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
      left
      join ExternalCompany         ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
     where i.IssueId = @IssueId
    
    select @InterfaceExportHeaderId = scope_identity(), @rowcount = @@rowcount, @Error = @@Error, @Errormsg = 'p_Interface_Export_SO_Insert - insert InterfaceExportHeader'
    
    if @Error <> 0
      goto error
    
    if @InterfaceImportHeaderId is not null
    begin
      update export
         set PrimaryKey        = import.PrimaryKey,
             RecordType        = import.RecordType,
             Address           = import.Address,
             FromWarehouseCode = import.FromWarehouseCode,
             ToWarehouseCode   = import.ToWarehouseCode,
             NumberOfLines     = import.NumberOfLines,
             Additional1       = import.Additional1,
             Additional2       = import.Additional2,
             Additional3       = import.Additional3,
             Additional4       = import.Additional4,
             Additional5       = import.Additional5,
             Additional6       = import.Additional6,
             Additional7       = import.Additional7,
             Additional8       = import.Additional8,
             Additional9       = import.Additional9,
             Additional10      = import.Additional10
        from InterfaceImportHeader import,
             InterfaceExportHeader export
       where InterfaceImportHeaderId = @InterfaceImportHeaderId
         and InterfaceExportHeaderId = @InterfaceExportHeaderId
      
      select @Error = @@Error, @Errormsg = 'p_Interface_Export_SO_Insert - update InterfaceExportHeader'
      
      if @Error <> 0
        goto error
    end
    
    if @rowcount > 0
    begin
      insert InterfaceExportDetail
            (InterfaceExportHeaderId,
             ForeignKey,
             LineNumber,
             ProductCode,
             Product,
             SKUCode,
             Batch,
             Quantity,
             Weight,
             Additional5)
      select @InterfaceExportHeaderId,
             od.OrderNumber,
             ol.LineNumber,
             vs.ProductCode,
             vs.Product,
             vs.SKUCode,
             vs.Batch,
             case when @ZeroLines = 1
                        then 0
                        when sr.SerialNumber is not null
                        then 1
                        else il.ConfirmedQuatity
                        end, 
             --il.ConfirmedQuatity,
             null,
             sr.SerialNumber
        from OutboundDocument od (nolock)
        join OutboundLine     ol (nolock) on od.OutboundDocumentId = ol.OutboundDocumentId
        join IssueLine        il (nolock) on ol.OutboundLineId     = il.OutboundLineId
        join viewStock        vs (nolock) on il.StorageUnitBatchId = vs.StorageUnitBatchId
   left join SerialNumber     sr (nolock) on il.issuelineid        = sr.issuelineid
       where il.IssueId = @IssueId
       --AND il.IssueLineId IN (SELECT IssueLineId FROM Instruction i (nolock) )
      
      select @Error = @@Error
      
      if @Error <> 0
        goto error
      
      if @InterfaceImportHeaderId is not null
      begin
        update export
           set ForeignKey  = import.ForeignKey,
               Additional1 = import.Additional1,
               Additional2 = import.Additional2,
               Additional3 = import.Additional3,
               Additional4 = import.Additional4
               --Additional5 = import.Additional5
          from InterfaceImportDetail import
          join InterfaceExportDetail export on import.LineNumber = export.LineNumber
         where InterfaceImportHeaderId = @InterfaceImportHeaderId
           and InterfaceExportHeaderId = @InterfaceExportHeaderId
        
        select @Error = @@Error
        
        if @Error <> 0
          goto error
        
        -- Sigma change for lines which are not valid
--        insert InterfaceExportDetail
--              (ForeignKey,
--               LineNumber,
--               ProductCode,
--               Product,
--               SKUCode,
--               Batch,
--               Quantity,
--               Weight,
--               Additional1,
--               Additional2,
--               Additional3,
--               Additional4,
--               Additional5)
--        select distinct d.ForeignKey,
--               d.LineNumber,
--               d.ProductCode,
--               d.Product,
--               d.SKUCode,
--               d.Batch,
--               d.Quantity,
--               d.Weight,
--               d.Additional1,
--               d.Additional2,
--               d.Additional3,
--               d.Additional4,
--               d.Additional5
--          from InterfaceImportDetail d
--         where InterfaceImportHeaderId = @InterfaceImportHeaderId
--           and not exists(select 1 from InterfaceExportDetail x where x.InterfaceExportHeaderId = @InterfaceExportHeaderId and d.ForeignKey = x.ForeignKey and d.LineNumber = x.LineNumber)
        
        select @Error = @@Error
        
        if @Error <> 0
          goto error
      end
    end
  end
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, @Errormsg);
    rollback transaction
    return @Error
end



 
 
 
 
