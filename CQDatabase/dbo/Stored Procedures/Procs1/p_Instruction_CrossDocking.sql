﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Instruction_CrossDocking
  ///   Filename       : p_Instruction_CrossDocking.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 20 Nov 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Instruction_CrossDocking
(
 @InstructionId int,
 @OperatorId    int,
 @PalletId      int
)

as
begin
	 set nocount on;
  
  declare @TableSOH as table
  (
   WarehouseId       int,
   StorageUnitId     int,
   Quantity          numeric(13,6),
   AllocatedQuantity numeric(13,6)
  )
  
  declare @Error                 int,
          @Errormsg              nvarchar(500),
          @GetDate               datetime,
          @StorageUnitId         int,
          @WarehouseId           int,
          @Quantity              float,
          @PickInstructionId     int,
          @PickStoreLocationId   int,
          @PutawayPickLocationId int,
          @Type                  char(2),
          @JobId                 int,
          @StorageUnitBatchId    int
  
  select @GetDate = dbo.ufn_Getdate()
  
  select @JobId         = i.JobId,
         @StorageUnitId = sub.StorageUnitId,
         @WarehouseId   = i.WarehouseId,
         @Quantity      = i.Quantity,
         @PutawayPickLocationId = i.PickLocationId,
         @StorageUnitBatchId    = i.StorageUnitBatchId
    from Instruction i
    join StorageUnitBatch sub on i.StorageUnitBatchId = sub.StorageUnitBatchId
   where i.InstructionId = @InstructionId
  
  -- Check if there is a Release Pick for this Instruction
  select @PickInstructionId   = i.InstructionId,
         @PickStoreLocationId = i.StoreLocationId,
         @Type                = sj.Type
    from Instruction                   i
    join InstructionType              it (nolock) on i.InstructionTypeId    = it.InstructionTypeId
    join Job                           j (nolock) on i.JobId                = j.JobId
    join Status                       sj (nolock) on j.StatusId             = sj.StatusId
    join Status                       si (nolock) on i.StatusId             = si.StatusId
    join StorageUnitBatch            sub (nolock) on i.StorageUnitBatchId   = sub.StorageUnitBatchId
    join IssueLineInstruction        ili (nolock) on i.InstructionId      = ili.InstructionId
    join Issue                       iss (nolock) on ili.IssueId          = iss.IssueId
    join Status                        s (nolock) on iss.StatusId         = s.StatusId
   where j.WarehouseId                    = @WarehouseId
     and isnull(j.OperatorId,@OperatorId) = @OperatorId
     and sj.StatusCode                   in ('RL','NS')
     and si.StatusCode                    = 'NS'
     and it.InstructionTypeCode           = 'P'
     and sub.StorageUnitId                = @StorageUnitId
     and i.Quantity                       = @Quantity
     and s.StatusCode                     = 'RL'
  
  if @@ROWCOUNT = 0 -- Find XDock Pick
  begin
    -- Get the Orders which Require Stock
    insert @TableSOH
          (WarehouseId,
           StorageUnitId,
           Quantity)
    select Iss.WarehouseId, 
           sub.StorageUnitId,
           SUM(il.Quantity)
      from Issue                       iss (nolock)
      join Status                        s (nolock) on iss.StatusId          = s.StatusId
      join IssueLine                    il (nolock) on iss.IssueId           = il.IssueId
      join StorageUnitBatch            sub (nolock) on il.StorageUnitBatchId = sub.StorageUnitBatchId
     where iss.WarehouseId   = @WarehouseId
       and sub.StorageUnitId = @StorageUnitId
       and il.Quantity       > 0
       and s.StatusCode      = 'OH'
    group by Iss.WarehouseId, 
             sub.StorageUnitId
    
    -- Update how much stock is there or on it's way
    update soh
       set AllocatedQuantity = va.ActualQuantity + va.AllocatedQuantity - va.ReservedQuantity
      from @TableSOH soh
      join viewAvailableArea va on va.WarehouseId        = soh.WarehouseId
                           and va.StorageUnitId      = soh.StorageUnitId
                           and (va.AreaType           = 'XDock'
                           or   va.Area               = 'XDock')
    
    if exists(select top 1 1
                from @TableSOH
               where Quantity - isnull(AllocatedQuantity, 0) > 0)
    begin
      select @PickStoreLocationId = l.LocationId
        from Area          a (nolock)
        join AreaLocation al (nolock) on a.AreaId = al.AreaId
        join Location      l (nolock) on al.LocationId = l.LocationId
       where (a.AreaType    = 'XDock'
         or   a.Area        = 'XDock')
         and a.StockOnHand = 1
         and a.WarehouseId = @WarehouseId
    end
  end
  
  begin transaction
  
  if @PickInstructionId is not null -- Update the pick job and send to putaway to the despatch area
  begin
    update j
       set OperatorId      = @OperatorId,
           StatusId        = s.StatusId,
           ReferenceNumber = 'I:' + convert(nvarchar(10), @InstructionId)
      from Job      j,
           [Status] s (nolock)
     where j.JobId      = @JobId
       and s.Type       = @Type
       and s.StatusCode = 'S' -- Started
       and isnull(j.OperatorId, @OperatorId) = @OperatorId -- Should stop duplicates, if another users get this one first
    
    -- Deallocate the Pick
    exec @Error = p_StorageUnitBatchLocation_Deallocate
     @InstructionId = @PickInstructionId
    
    if @Error <> 0
      goto error
    
    -- Update the confirmed quantity incase of no-stock and palletId
    exec @Error = p_Instruction_Update
     @InstructionId      = @PickInstructionId,
     @ConfirmedQuantity  = @Quantity,
     @PalletId           = @PalletId,
     @PickLocationId     = @PutawayPickLocationId,
     @StorageUnitBatchId = @StorageUnitBatchId
    
    if @Error <> 0
      goto error
    
    exec @Error = p_Instruction_Started
     @InstructionId = @PickInstructionId,
     @OperatorId    = @OperatorId
    
    if @Error <> 0
      goto error
  end
  
  -- Deallocate the Putaway
  exec @Error = p_StorageUnitBatchLocation_Deallocate
   @InstructionId = @InstructionId
  
  if @Error <> 0
    goto error
  
  -- Change the Putaway Location to same as the Pick and reference the two instructions
  exec @Error = p_Instruction_Update
   @InstructionId    = @InstructionId,
   @StoreLocationId  = @PickStoreLocationId,
   @InstructionRefId = @PickInstructionId
  
  if @Error <> 0
    goto error
  
  -- Allocate the new Locations
  exec @Error = p_StorageUnitBatchLocation_Reserve
   @InstructionId = @InstructionId
  
  if @Error <> 0
    goto error
  
  -- Note: When putaway is finished the Pick is also completed in p_Instruction_Finish
  
  commit transaction
  return
  
  error:
    RAISERROR (900000,-1,-1, 'Error executing p_Instruction_CrossDocking'); 
    rollback transaction
    return @Error
end
