﻿ 
/*
/// <summary>
///   Procedure Name : p_Interface_WebService_GetSchemas
///   Filename       : p_Interface_WebService_GetSchemas.sql
///   Create By      : Grant Schultz
///   Date Created   : 14 Mar 2013
/// </summary>
/// <remarks>
///
/// </remarks>
/// <param>
///
/// </param>
/// <returns>
///
/// </returns>
/// <newpara>
///   Modified by    :
///   Modified Date  :
///   Details        :
/// </newpara>
*/
CREATE procedure [dbo].[p_Interface_WebService_GetSchemas]
(
@XMLBody  nvarchar(max) output,
@PrincipalCode nvarchar(30)
)
--with encryption
as
begin
  
  declare @PrincipalId                  INT
  
  SELECT @PrincipalId = PrincipalId
    From Principal
   Where PrincipalCode = @PrincipalCode
  
  SELECT @XMLBody = '<?xml version="1.0" encoding="utf-16"?>' +
         (SELECT
         (SELECT
         (SELECT
         idt.InterfaceDocumentType  AS 'DocumentType'
         ,idt.InterfaceDocumentTypeCode AS 'DocumentTypeCode'
         ,idt.RecordType     AS 'RecordType'
         ,idt.InterfaceType    AS 'InterfaceType'
         ,imf.SubDirectory
         ,isnull(imf.FilePrefix, idt.FilePrefix) as 'FilePrefix'
         ,idt.InDirectory    AS 'Inbound'
         ,idt.OutDirectory    AS 'Outbound'
         ,imf.PrincipalFTPSite   AS 'FTPSite'
         ,imf.FTPUserName
         ,imf.FTPPassword
         ,ift.InterfaceFileTypeCode  AS 'FileFormat'
         ,isnull(isnull(imf.FileExtension, idt.FileExtension),'txt') as 'FileExtension'
         --,case when idt.InterfaceDocumentTypeCode like 'Ship%'
         --      then 'Outbound'
         --      when idt.InterfaceDocumentTypeCode like 'ASN%'
         --      then 'Inbound'
         --      else 'Master'
         --      end                               as 'Module'
         ,(SELECT CASE ift.InterfaceFileTypeCode
                  WHEN 'FIX' THEN
                  (SELECT
                  (SELECT
                  HeaderRow  AS 'HeaderRow'
                  ,(SELECT
                  inf2.InterfaceField    AS 'FieldName'
                  ,inf2.InterfaceFieldCode   AS 'FieldCode'
                  ,inf2.DataType      AS 'DataType'
                  ,ISNULL(imc2.InterfaceMappingColumnCode, inf2.InterfaceFieldCode) AS 'ColumnCode'
                  ,ISNULL(imc2.interfaceMappingColumn, inf2.InterfaceField) AS 'Column'
                  ,isnull(imc2.StartPosition,'')     AS 'StartPosition'
                  ,isnull(imc2.EndPostion,'-1')                    AS 'Length'
                  ,isnull(imc2.Format,'')      AS 'Format'
             FROM InterfaceField inf2
             LEFT JOIN InterfaceMappingColumn imc2 ON inf2.InterfaceFieldId = imc2.InterfaceFieldId
                                                  and imc2.InterfaceMappingFileId = imf.InterfaceMappingFileId
            WHERE imf.InterfaceFileTypeId = (SELECT InterfaceFileTypeId FROM InterfaceFileType WHERE InterfaceFileTypeCode = 'FIX')
              and inf2.InterfaceDocumentTypeId = imf.InterfaceDocumentTypeId
           FOR XML PATH('Field'), TYPE)
  FOR XML PATH('Fields'), TYPE)
  FOR XML PATH('FixedWidthSchema'), TYPE)
  WHEN 'TXT' THEN
  (SELECT Delimiter AS 'Delimiter'
  , HeaderRow  AS 'HeaderRow'
  ,(SELECT
  
    (select 'Module'    AS 'FieldName'
           ,'Module' 'FieldCode'
           ,'string'      AS 'DataType'
           ,'Module' AS 'ColumnCode'
           ,'Module' 'Column'
           ,-1      AS 'ColumnNumber'
           ,case when idt.InterfaceDocumentTypeCode like 'Ship%'
                    then 'Outbound'
                    when idt.InterfaceDocumentTypeCode like 'ASN%'
                    then 'Inbound'
                    else 'Master'
                    end                               as 'Format'
    FOR XML PATH('Field'), TYPE)
    ,(SELECT inf2.InterfaceField    AS 'FieldName'
           ,inf2.InterfaceFieldCode   AS 'FieldCode'
           ,inf2.DataType      AS 'DataType'
   ,ISNULL(imc2.InterfaceMappingColumnCode, inf2.InterfaceFieldCode) AS 'ColumnCode'
           ,ISNULL(imc2.interfaceMappingColumn, inf2.InterfaceField) AS 'Column'
           ,ISNULL(imc2.ColumnNumber, -1)  AS 'ColumnNumber'
           ,imc2.Format      AS 'Format'
      FROM InterfaceField inf2
       LEFT JOIN InterfaceMappingColumn imc2 ON inf2.InterfaceFieldId = imc2.InterfaceFieldId
                                            and imc2.InterfaceMappingFileId = imf.InterfaceMappingFileId
     WHERE imf.InterfaceFileTypeId = (SELECT InterfaceFileTypeId FROM InterfaceFileType WHERE InterfaceFileTypeCode = 'TXT')
       and inf2.InterfaceDocumentTypeId = imf.InterfaceDocumentTypeId
    FOR XML PATH('Field'), TYPE)
  FOR XML PATH('Fields'), TYPE)
    FROM InterfaceFileType ift1
   WHERE ift1.InterfaceFileTypeId = ift.InterfaceFileTypeId
 FOR XML PATH('DelimitedSchema'), TYPE)
  WHEN 'XML' THEN
  (SELECT StyleSheet AS 'StyleSheet'
    FROM InterfaceMappingFile imf1
   WHERE imf1.InterfaceMappingFileId = imf.InterfaceMappingFileId
  FOR XML PATH('XMLSchema'), TYPE)
END)
,imf.HeaderRow
,imf.Delimiter
,imf.StyleSheet
  from InterfaceDocumentType idt
       inner join InterfaceMappingFile imf on idt.InterfaceDocumentTypeId = imf.InterfaceDocumentTypeId
       inner join InterfaceFileType ift on imf.InterfaceFileTypeId = ift.InterfaceFileTypeId
 WHERE imf.PrincipalId = @PrincipalId
   --and idt.InterfaceDocumentTypeId = 10
FOR XML PATH('Document'), TYPE)
FOR XML PATH('Documents'), TYPE)
FOR XML PATH('root'))
END
