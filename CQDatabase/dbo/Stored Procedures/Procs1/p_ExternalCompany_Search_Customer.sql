﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_ExternalCompany_Search_Customer
  ///   Filename       : p_ExternalCompany_Search_Customer.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 9 Jul 2007
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the ExternalCompany table.
  /// </remarks>
  /// <param>
  ///   @ExternalCompany nvarchar(50) = null,
  ///   @ExternalCompanyCode nvarchar(10) = null 
  /// </param>
  /// <returns>
  ///   ExternalCompanyId,
  ///   ExternalCompany,
  ///   ExternalCompanyCode
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_ExternalCompany_Search_Customer
(
 @ExternalCompany nvarchar(255) = null,
 @ExternalCompanyCode nvarchar(30) = null
)

as
begin
  set nocount on;

 select ec.ExternalCompanyId,
        ec.ExternalCompany     as 'Customer',
        ec.ExternalCompanyCode as 'CustomerCode'
   from ExternalCompany     ec  (nolock)
   join ExternalCompanyType ect (nolock) on ect.ExternalCompanyTypeId = ec.ExternalCompanyTypeId
  where isnull(ec.ExternalCompany,'%')  like '%' + isnull(@ExternalCompany, isnull(ec.ExternalCompany,'%')) + '%'
    and isnull(ec.ExternalCompanyCode,'%')  like '%' + isnull(@ExternalCompanyCode, isnull(ec.ExternalCompanyCode,'%')) + '%'
    and ect.ExternalCompanyTypeCode = 'CUST'
end
