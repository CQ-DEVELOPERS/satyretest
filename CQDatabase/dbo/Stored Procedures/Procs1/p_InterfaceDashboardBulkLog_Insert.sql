﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceDashboardBulkLog_Insert
  ///   Filename       : p_InterfaceDashboardBulkLog_Insert.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:31:54
  /// </summary>
  /// <remarks>
  ///   Inserts a row into the InterfaceDashboardBulkLog table.
  /// </remarks>
  /// <param>
  ///   @FileKeyId int = null,
  ///   @FileType nvarchar(200) = null,
  ///   @ProcessedDate datetime = null,
  ///   @FileName nvarchar(100) = null,
  ///   @ErrorCode char(5) = null,
  ///   @ErrorDescription nvarchar(510) = null 
  /// </param>
  /// <returns>
  ///   InterfaceDashboardBulkLog.FileKeyId,
  ///   InterfaceDashboardBulkLog.FileType,
  ///   InterfaceDashboardBulkLog.ProcessedDate,
  ///   InterfaceDashboardBulkLog.FileName,
  ///   InterfaceDashboardBulkLog.ErrorCode,
  ///   InterfaceDashboardBulkLog.ErrorDescription 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceDashboardBulkLog_Insert
(
 @FileKeyId int = null,
 @FileType nvarchar(200) = null,
 @ProcessedDate datetime = null,
 @FileName nvarchar(100) = null,
 @ErrorCode char(5) = null,
 @ErrorDescription nvarchar(510) = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  insert InterfaceDashboardBulkLog
        (FileKeyId,
         FileType,
         ProcessedDate,
         FileName,
         ErrorCode,
         ErrorDescription)
  select @FileKeyId,
         @FileType,
         @ProcessedDate,
         @FileName,
         @ErrorCode,
         @ErrorDescription 
  
  select @Error = @@Error
  
  
  return @Error
  
end
