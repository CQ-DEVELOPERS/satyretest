﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceImportSODetail_Search
  ///   Filename       : p_InterfaceImportSODetail_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:09
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceImportSODetail table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceImportSODetail.InterfaceImportSOHeaderId,
  ///   InterfaceImportSODetail.ForeignKey,
  ///   InterfaceImportSODetail.LineNumber,
  ///   InterfaceImportSODetail.ProductCode,
  ///   InterfaceImportSODetail.Product,
  ///   InterfaceImportSODetail.SKUCode,
  ///   InterfaceImportSODetail.Batch,
  ///   InterfaceImportSODetail.Quantity,
  ///   InterfaceImportSODetail.Weight,
  ///   InterfaceImportSODetail.Additional1,
  ///   InterfaceImportSODetail.Additional2,
  ///   InterfaceImportSODetail.Additional3,
  ///   InterfaceImportSODetail.Additional4,
  ///   InterfaceImportSODetail.Additional5 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceImportSODetail_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceImportSODetail.InterfaceImportSOHeaderId
        ,InterfaceImportSODetail.ForeignKey
        ,InterfaceImportSODetail.LineNumber
        ,InterfaceImportSODetail.ProductCode
        ,InterfaceImportSODetail.Product
        ,InterfaceImportSODetail.SKUCode
        ,InterfaceImportSODetail.Batch
        ,InterfaceImportSODetail.Quantity
        ,InterfaceImportSODetail.Weight
        ,InterfaceImportSODetail.Additional1
        ,InterfaceImportSODetail.Additional2
        ,InterfaceImportSODetail.Additional3
        ,InterfaceImportSODetail.Additional4
        ,InterfaceImportSODetail.Additional5
    from InterfaceImportSODetail
  
end
