﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Intergration_Get_Column_Mapping
  ///   Filename       : p_Intergration_Get_Column_Mapping.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 18 Mar 2014
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Intergration_Get_Column_Mapping
(
 @interfaceMappingFileId int
)

as
begin
  set nocount on;
  
  declare @TableResult as table
  (
   InterfaceMappingColumnId   int,
   InterfaceMappingColumnCode nvarchar(100),
   InterfaceMappingColumn     nvarchar(100),
   StartPosition              int,
   EndPosition                int,
   FixedWidth                 bit,
   Format                     nvarchar(50),
   ColumnNumber               int,
   Datatype                   nvarchar(50),
   Mandatory                  bit
  )
  
  insert @TableResult
        (InterfaceMappingColumnId,
         InterfaceMappingColumnCode,
         InterfaceMappingColumn,
         StartPosition,
         EndPosition,
         FixedWidth,
         Format,
         ColumnNumber,
         Datatype,
         Mandatory)
  select imc.InterfaceMappingColumnId,
         REPLACE(REPLACE(REPLACE(imc.InterfaceMappingColumnCode, 'Interface',''), 'Import',''), 'Export',''),
         imc.InterfaceMappingColumn,
         imc.StartPosition,
         imc.EndPostion,
         CONVERT(bit, case when ift.InterfaceFileTypeCode = 'FIX'
                           then 1
                           else 0
                           end),
         imc.Format,
         imc.ColumnNumber,
         ifd.Datatype,
         isnull(ifd.Mandatory,0)
    from InterfaceMappingColumn imc (nolock)
    join InterfaceField         ifd (nolock) on imc.InterfaceFieldId = ifd.InterfaceFieldId
    join InterfaceMappingFile   imf (nolock) on imc.InterfaceMappingFileId = imf.InterfaceMappingFileId
    join InterfaceFileType      ift (nolock) on imf.InterfaceFileTypeId = ift.InterfaceFileTypeId
   where imc.InterfaceMappingFileId = @interfaceMappingFileId
  
  if (select COUNT(distinct(InterfaceMappingColumnCode)) from @TableResult where InterfaceMappingColumnCode != 'Place Holder') > 1
    update @TableResult
       set InterfaceMappingColumn = InterfaceMappingColumnCode + ' - ' + InterfaceMappingColumn
  
  select InterfaceMappingColumnId,
         InterfaceMappingColumn,
         StartPosition,
         EndPosition,
         FixedWidth,
         Format,
         Datatype,
         Mandatory
    from @TableResult
  order by ColumnNumber
end
