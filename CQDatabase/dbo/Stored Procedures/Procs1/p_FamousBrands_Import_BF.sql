﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Import_BF
  ///   Filename       : p_FamousBrands_Import_BF.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 27 May 2009
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_FamousBrands_Import_BF

as
begin
	 set nocount on;
  
  declare @Error              int,
          @Errormsg           varchar(500),
          @GetDate            datetime,
          @InterfaceBOMId     int,
          @ParentProductCode  varchar(50),
          @OrderDescription   varchar(255),
          @CreateDate         varchar(20),
          @PlannedDate        varchar(20),
          @Revision           varchar(20),
          @BuildQuantity      varchar(20),
          @HeaderUnits        varchar(20),
          @Location           varchar(20),
          @ProcessedDate      datetime,
          @RecordStatus       char(1),
          @LineNumber         int,
          @ProductCode        varchar(50) ,
          @ProductDescription varchar(255),
          @Quantity           numeric(13,3),
          @Units              numeric(13,3),
          @WarehouseId        int,
          @StorageUnitId      int,
          @StorageUnitBatchId int
  
  select @GetDate = dbo.ufn_Getdate()
  
  update d
     set InterfaceBOMId = (select max(h.InterfaceBOMId)
                             from InterfaceBOM h
                            where d.ParentProductCode = h.ParentProductCode)
    from InterfaceBOMLine d
   where d.InterfaceBOMId is null
  
--  update InterfaceBOM
--     set RecordType = 'BB'
--   where RecordType is null
  
  update h
     set ProcessedDate = @Getdate
    from InterfaceBOM h
   where exists(select 1 from InterfaceBOMLine d where h.InterfaceBOMId = d.InterfaceBOMId)
     and RecordStatus in ('N','U')
     and ProcessedDate is null
  
  declare header_cursor cursor for
   select InterfaceBOMId,
          ParentProductCode,
          OrderDescription,
          CreateDate,
          PlannedDate,
          Revision,
          BuildQuantity,
          Units,
          Location,
          ProcessedDate,
          RecordStatus
     from InterfaceBOM
    where ProcessedDate = @Getdate
   order by	ParentProductCode
  
  open header_cursor
  
  fetch header_cursor
   into @InterfaceBOMId,
        @ParentProductCode,
        @OrderDescription,
        @CreateDate,
        @PlannedDate,
        @Revision,
        @BuildQuantity,
        @HeaderUnits,
        @Location,
        @ProcessedDate,
        @RecordStatus
  
  while (@@fetch_status = 0)
  begin
    begin transaction xml_import
    
    select @WarehouseId = WarehouseId
      from Warehouse
     where WarehouseCode = @Location
    
    if @WarehouseId is null
      set @WarehouseId = 1
    
    set rowcount 1
    update InterfaceBOM
       set RecordStatus = 'C'
     where InterfaceBOMId = @InterfaceBOMId
    set rowcount 0
    
    exec @Error = p_FamousBrands_Insert_Product
     @ProductCode        = @ParentProductCode,
     @Product            = @OrderDescription,
     @WarehouseId        = @WarehouseId,
     @SKUCode            = @HeaderUnits,
     @SKU                = @HeaderUnits,
     @StorageUnitId      = @StorageUnitId output,
     @StorageUnitBatchId = @StorageUnitBatchId output
    
    if @Error != 0 or @StorageUnitBatchId is null
    begin
      select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
      goto error_detail
    end
        
    declare detail_cursor cursor for
     select InterfaceBOMId,
            ParentProductCode,
            LineNumber,
            ProductCode,
            ProductDescription,
            Quantity,
            Units
       from InterfaceBOMLine
      where InterfaceBOMId = @InterfaceBOMId
    
    open detail_cursor
    
    fetch detail_cursor
     into @InterfaceBOMId,
          @ParentProductCode,
          @LineNumber,
          @ProductCode,
          @ProductDescription,
          @Quantity,
          @Units
    
    while (@@fetch_status = 0)
    begin
      exec @Error = p_FamousBrands_Insert_Product
       @ProductCode        = @ProductCode,
       @Product            = @ProductDescription,
       @WarehouseId        = @WarehouseId,
       @SKUCode            = @Units,
       @SKU                = @Units,
       @StorageUnitId      = @StorageUnitId output,
       @StorageUnitBatchId = @StorageUnitBatchId output
      
      if @Error != 0 or @StorageUnitBatchId is null
      begin
        select @ErrorMsg = 'Error executing p_interface_xml_Product_Insert'
        goto error_detail
      end
      
      fetch detail_cursor
       into @InterfaceBOMId,
            @ParentProductCode,
            @LineNumber,
            @ProductCode,
            @ProductDescription,
            @Quantity,
            @Units
    end
    close detail_cursor
    deallocate detail_cursor
        
    error_detail:
    error_header:
    
    if @Error = 0
    begin
      --select @OrderNumber as 'OrderNumber', 'Commit 1', @@trancount as '@@trancount'
      if @@trancount > 0
        commit transaction xml_import
      --select @OrderNumber as 'OrderNumber', 'Commit 2', @@trancount as '@@trancount'
    end
    else
    begin
      --select @OrderNumber as 'OrderNumber', 'Error 1', @@trancount as '@@trancount'
      if @@trancount > 0
        rollback transaction xml_import
      --select @OrderNumber as 'OrderNumber', 'Error 2', @@trancount as '@@trancount'
      update InterfaceBOM
         set RecordStatus = 'E'
       where InterfaceBOMId = @InterfaceBOMId
    end
    
    fetch header_cursor
     into @InterfaceBOMId,
          @ParentProductCode,
          @OrderDescription,
          @CreateDate,
          @PlannedDate,
          @Revision,
          @BuildQuantity,
          @HeaderUnits,
          @Location,
          @ProcessedDate,
          @RecordStatus
  end
  
  close header_cursor
  deallocate header_cursor
  
  return
end
