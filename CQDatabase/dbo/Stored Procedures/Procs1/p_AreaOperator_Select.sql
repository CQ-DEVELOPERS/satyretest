﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_AreaOperator_Select
  ///   Filename       : p_AreaOperator_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:18:46
  /// </summary>
  /// <remarks>
  ///   Selects rows from the AreaOperator table.
  /// </remarks>
  /// <param>
  ///   @AreaId int = null,
  ///   @OperatorId int = null 
  /// </param>
  /// <returns>
  ///   AreaOperator.AreaId,
  ///   AreaOperator.OperatorId 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_AreaOperator_Select
(
 @AreaId int = null,
 @OperatorId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         AreaOperator.AreaId
        ,AreaOperator.OperatorId
    from AreaOperator
   where isnull(AreaOperator.AreaId,'0')  = isnull(@AreaId, isnull(AreaOperator.AreaId,'0'))
     and isnull(AreaOperator.OperatorId,'0')  = isnull(@OperatorId, isnull(AreaOperator.OperatorId,'0'))
  
end
