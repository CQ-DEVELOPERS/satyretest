﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Configuration_GetSwitch_RecipetLine
  ///   Filename       : p_Configuration_GetSwitch_RecipetLine.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 10 Sep 2007
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Configuration_GetSwitch_RecipetLine
(
 @receiptLineId int
)

as
begin
  declare @result bit
  
  select @result = case when su.ProductCategory = 'P'
                        then 1
                        else 0
                        end
    from ReceiptLine       rl (nolock)
    join StorageUnitBatch sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit       su (nolock) on sub.StorageUnitId = su.StorageUnitId
   where rl.ReceiptLineId = @receiptLineId
  
  select @result
end
