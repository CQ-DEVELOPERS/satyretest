﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Dashboard_Inbound_Putaway_Achieved
  ///   Filename       : p_Dashboard_Inbound_Putaway_Achieved.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 07 Nov 2012
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Dashboard_Inbound_Putaway_Achieved
(
 @WarehouseId int = null,
 @Summarise   bit = 0
)

as
begin
	 set nocount on;
	 
	 if @Summarise = 0
	 begin
	   select convert(nvarchar(2), d.CreateDate, 108) + 'H00' as 'Legend',
           sum(d.Value) as 'Value',
           sum(Pallets) as 'Pallets',
           o.Operator
      from DashboardInboundPutawayAchieved d
      join Operator o on d.OperatorId = o.OperatorId
     where d.WarehouseId = @WarehouseId
    group by convert(nvarchar(2), d.CreateDate, 108) + 'H00',
           o.Operator
	 end
	 else
	 begin
	   truncate table DashboardInboundPutawayAchieved
	   
	   insert DashboardInboundPutawayAchieved
	         (WarehouseId,
           OperatorId,
           CreateDate,
           Legend,
           Value,
           Pallets,
           KPI)
	   select i.WarehouseId,
           i.OperatorId,
           i.EndDate,
           null,
           sum(i.ConfirmedQuantity),
           count(distinct(i.JobId)),
           60
	     from Instruction      i (nolock)
	     join InstructionType it (nolock) on i.InstructionTypeId = it.InstructionTypeId
	                                     and it.InstructionTypeCode in ('PR','S','SM')
	     join Status           s (nolock) on i.StatusId = s.StatusId
	    where i.WarehouseId = isnull(@WarehouseId, i.WarehouseId)
	      and s.StatusCode = 'F'
	      --and j.CheckedDate >= CONVERT(nvarchar(10), getdate(), 120)
	   group by i.WarehouseId,
             i.OperatorId,
             i.EndDate
	 end
end
