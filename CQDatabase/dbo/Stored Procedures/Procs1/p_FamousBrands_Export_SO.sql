﻿/*
  /// <summary>
  ///   Procedure Name : p_FamousBrands_Export_SO
  ///   Filename       : p_FamousBrands_Export_SO.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 30 Oct 2008
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
create procedure p_FamousBrands_Export_SO
(
 @FileName varchar(30) = null output
)
as
begin
  set nocount on
  
  declare @TableResult as table
  (
   Id   int identity,
   Data varchar(500)
  )
  
  declare @Error                int,
          @Errormsg             varchar(500),
          @GetDate              datetime,
          @InterfaceExportSOHeaderId int,
          @OrderNumber          varchar(30),
          @DeliveryDate         varchar(20),
          @ProcessedDate        varchar(20),
          @FromWarehouseCode    varchar(10),
          @LineNumber           varchar(10),
          @ProductCode          varchar(30),
          @Quantity             varchar(10),
          -- Internal
          @fetch_status_header  int,
          @fetch_status_detail  int,
          @print_string         varchar(255)
  
  select @GetDate = dbo.ufn_Getdate()
  
  declare extract_header_cursor cursor for
  select top 1
         InterfaceExportSOHeaderId,
         PrimaryKey,
         convert(varchar(20), isnull(DeliveryDate, @Getdate), 120)
    from InterfaceExportSOHeader
   where RecordStatus = 'N'
     and RecordType in ('EXP','DEL','FAR','COL','SAL','3RD','NAC')
  order by PrimaryKey
  
  open extract_header_cursor
  
  fetch extract_header_cursor into @InterfaceExportSOHeaderId,
                                   @OrderNumber,
                                   @DeliveryDate
  
  select @fetch_status_header = @@fetch_status
  
  while (@fetch_status_header = 0)
  begin
    update InterfaceExportSOHeader
       set RecordStatus = 'Y',
           ProcessedDate = @GetDate
     where InterfaceExportSOHeaderId = @InterfaceExportSOHeaderId
    
    set @FileName = @OrderNumber + '(' + convert(varchar(10), @InterfaceExportSOHeaderId) + ')'
    
    insert @TableResult (Data) select '<?xml version="1.0" encoding="Windows-1252" standalone="yes" ?>'
    insert @TableResult (Data) select '<root>'
    insert @TableResult (Data) select '<SalesOrder>'
    insert @TableResult (Data) select '		<OrderNumber>' + @OrderNumber + '</OrderNumber>'
    insert @TableResult (Data) select '		<DeliveryDate>' + @DeliveryDate + '</DeliveryDate>'
    
    declare extract_detail_cursor cursor for
    select convert(varchar(10), LineNumber),
           isnull(ProductCode, ''),
           isnull(Additional1, ''),
           convert(varchar(10), Quantity)
      from InterfaceExportSODetail
     where InterfaceExportSOHeaderId = @InterfaceExportSOHeaderId
     order by LineNumber
      
    open extract_detail_cursor
    
    fetch extract_detail_cursor into @LineNumber,
                                     @ProductCode,
                                     @FromWarehouseCode,
                                     @Quantity
    
    select @fetch_status_detail = @@fetch_status
    
    while (@fetch_status_detail = 0)
    begin
      insert @TableResult (Data) select '		<SalesOrderLine>'
      insert @TableResult (Data) select '			<LineNumber>' + @LineNumber + '</LineNumber>'
      insert @TableResult (Data) select '			<ProductCode>' + @ProductCode + '</ProductCode>'
      insert @TableResult (Data) select '			<Location>' + @FromWarehouseCode + '</Location>'
      insert @TableResult (Data) select '			<Quantity>' + @Quantity + '</Quantity>'
      insert @TableResult (Data) select '		</SalesOrderLine>'
      
      fetch extract_detail_cursor into @LineNumber,
                                       @ProductCode,
        @FromWarehouseCode,
                                       @Quantity
      
      select @fetch_status_detail = @@fetch_status
    end
    
    close extract_detail_cursor
    deallocate extract_detail_cursor
    
    insert @TableResult (Data) select '</SalesOrder>'
    insert @TableResult (Data) select '</root>'
    
    fetch extract_header_cursor into @InterfaceExportSOHeaderId,
                                     @OrderNumber,
                                     @DeliveryDate
    
    select @fetch_status_header = @@fetch_status
  end
  
  close extract_header_cursor
  deallocate extract_header_cursor
  
  select Data
    from @TableResult
  order by Id
end
