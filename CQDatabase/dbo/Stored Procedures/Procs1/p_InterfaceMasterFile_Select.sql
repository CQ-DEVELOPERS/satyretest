﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceMasterFile_Select
  ///   Filename       : p_InterfaceMasterFile_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:29:21
  /// </summary>
  /// <remarks>
  ///   Selects rows from the InterfaceMasterFile table.
  /// </remarks>
  /// <param>
  ///   @MasterFileId int = null 
  /// </param>
  /// <returns>
  ///   InterfaceMasterFile.MasterFileId,
  ///   InterfaceMasterFile.MasterFile,
  ///   InterfaceMasterFile.MasterFileDescription,
  ///   InterfaceMasterFile.LastDate,
  ///   InterfaceMasterFile.SendFlag 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceMasterFile_Select
(
 @MasterFileId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         InterfaceMasterFile.MasterFileId
        ,InterfaceMasterFile.MasterFile
        ,InterfaceMasterFile.MasterFileDescription
        ,InterfaceMasterFile.LastDate
        ,InterfaceMasterFile.SendFlag
    from InterfaceMasterFile
   where isnull(InterfaceMasterFile.MasterFileId,'0')  = isnull(@MasterFileId, isnull(InterfaceMasterFile.MasterFileId,'0'))
  
end
