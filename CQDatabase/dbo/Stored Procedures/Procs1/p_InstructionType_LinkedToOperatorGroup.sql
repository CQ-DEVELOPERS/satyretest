﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InstructionType_LinkedToOperatorGroup
  ///   Filename       : p_InstructionType_LinkedToOperatorGroup.sql
  ///   Create By      : Karen
  ///   Date Created   : December 2011
  /// </summary>
  /// <remarks>
  ///   
  /// </remarks>
  /// <param>
  ///   
  /// </param>
  /// <returns>
  ///   
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InstructionType_LinkedToOperatorGroup
( 
@OperatorGroupId	int
)


as
begin
	 set nocount on;
  
  select it.InstructionType,
		 it.InstructionTypeCode,
		 it.InstructionTypeId
    from InstructionType it
    join OperatorGroupInstructionType ogi (nolock) on ogi.InstructionTypeId = it.InstructionTypeId
    where ogi.OperatorGroupId = @OperatorGroupId
	order by InstructionType
end
