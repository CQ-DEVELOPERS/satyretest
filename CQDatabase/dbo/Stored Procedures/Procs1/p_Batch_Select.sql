﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Batch_Select
  ///   Filename       : p_Batch_Select.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 09 Sep 2014 15:23:49
  /// </summary>
  /// <remarks>
  ///   Selects rows from the Batch table.
  /// </remarks>
  /// <param>
  ///   @BatchId int = null 
  /// </param>
  /// <returns>
  ///   Batch.BatchId,
  ///   Batch.StatusId,
  ///   Batch.WarehouseId,
  ///   Batch.Batch,
  ///   Batch.CreateDate,
  ///   Batch.ExpiryDate,
  ///   Batch.IncubationDays,
  ///   Batch.ShelfLifeDays,
  ///   Batch.ExpectedYield,
  ///   Batch.ActualYield,
  ///   Batch.ECLNumber,
  ///   Batch.BatchReferenceNumber,
  ///   Batch.ProductionDateTime,
  ///   Batch.FilledYield,
  ///   Batch.COACertificate,
  ///   Batch.StatusModifiedBy,
  ///   Batch.StatusModifiedDate,
  ///   Batch.CreatedBy,
  ///   Batch.ModifiedBy,
  ///   Batch.ModifiedDate,
  ///   Batch.Prints,
  ///   Batch.ParentProductCode,
  ///   Batch.RI,
  ///   Batch.Density,
  ///   Batch.BOELineNumber,
  ///   Batch.BillOfEntry,
  ///   Batch.ReferenceNumber 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Batch_Select
(
 @BatchId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  select
         Batch.BatchId
        ,Batch.StatusId
        ,Batch.WarehouseId
        ,Batch.Batch
        ,Batch.CreateDate
        ,Batch.ExpiryDate
        ,Batch.IncubationDays
        ,Batch.ShelfLifeDays
        ,Batch.ExpectedYield
        ,Batch.ActualYield
        ,Batch.ECLNumber
        ,Batch.BatchReferenceNumber
        ,Batch.ProductionDateTime
        ,Batch.FilledYield
        ,Batch.COACertificate
        ,Batch.StatusModifiedBy
        ,Batch.StatusModifiedDate
        ,Batch.CreatedBy
        ,Batch.ModifiedBy
        ,Batch.ModifiedDate
        ,Batch.Prints
        ,Batch.ParentProductCode
        ,Batch.RI
        ,Batch.Density
        ,Batch.BOELineNumber
        ,Batch.BillOfEntry
        ,Batch.ReferenceNumber
    from Batch
   where isnull(Batch.BatchId,'0')  = isnull(@BatchId, isnull(Batch.BatchId,'0'))
  order by Batch
  
end
