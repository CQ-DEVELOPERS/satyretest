﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_Address_Delete
  ///   Filename       : p_Address_Delete.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 01 Sep 2012 23:22:20
  /// </summary>
  /// <remarks>
  ///   Deletes a row from the Address table.
  /// </remarks>
  /// <param>
  ///   @AddressId int = null 
  /// </param>
  /// <returns>
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_Address_Delete
(
 @AddressId int = null 
)

as
begin
	 set nocount on;
  
	 declare @Error int
 
  delete Address
     where AddressId = @AddressId
  
  select @Error = @@Error
  
  
  return @Error
  
end
