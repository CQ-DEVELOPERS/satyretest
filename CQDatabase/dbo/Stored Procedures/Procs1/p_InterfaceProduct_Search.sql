﻿ 
/*
  /// <summary>
  ///   Procedure Name : p_InterfaceProduct_Search
  ///   Filename       : p_InterfaceProduct_Search.sql
  ///   Create By      : Grant Schultz
  ///   Date Created   : 26 Aug 2012 16:33:39
  /// </summary>
  /// <remarks>
  ///   Searches for rows from the InterfaceProduct table.
  /// </remarks>
  /// <param>
  ///   @PageSize int = 10
  ///   @PageNumber int = 1
  /// </param>
  /// <returns>
  ///   InterfaceProduct.HostId,
  ///   InterfaceProduct.ProductCode,
  ///   InterfaceProduct.ProductDescription,
  ///   InterfaceProduct.Barcode,
  ///   InterfaceProduct.OuterCartonBarcode,
  ///   InterfaceProduct.PalletQuantity,
  ///   InterfaceProduct.Class,
  ///   InterfaceProduct.Modified,
  ///   InterfaceProduct.ProcessedDate,
  ///   InterfaceProduct.RecordStatus,
  ///   InterfaceProduct.CaseQuantity,
  ///   InterfaceProduct.InsertDate 
  /// </returns>
  /// <newpara>
  ///   Modified by    : 
  ///   Modified Date  : 
  ///   Details        : 
  /// </newpara>
*/
CREATE procedure p_InterfaceProduct_Search
(
 @PageSize int = 10,
 @PageNumber int = 0
)

as
begin
	 set nocount on;
  
	 declare @RowStart int,
	         @RowEnd   int
  
  set @RowStart   = @PageSize * @PageNumber + 1;
  set @RowEnd     = @RowStart + @PageSize - 1;
  
 
  select
         InterfaceProduct.HostId
        ,InterfaceProduct.ProductCode
        ,InterfaceProduct.ProductDescription
        ,InterfaceProduct.Barcode
        ,InterfaceProduct.OuterCartonBarcode
        ,InterfaceProduct.PalletQuantity
        ,InterfaceProduct.Class
        ,InterfaceProduct.Modified
        ,InterfaceProduct.ProcessedDate
        ,InterfaceProduct.RecordStatus
        ,InterfaceProduct.CaseQuantity
        ,InterfaceProduct.InsertDate
    from InterfaceProduct
  
end
