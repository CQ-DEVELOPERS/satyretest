﻿
create view viewSUEC
as
  select vp.StorageUnitId,
         vp.ProductCode,
         vp.Product,
         vp.SKUCode,
         ec.ExternalCompanyId,
         ec.ExternalCompanyCode,
         ec.ExternalCompany,
         suec.AllocationCategory,
         suec.MinimumShelfLife,
         vp.ShelfLifeDays,
         suec.ProductCode as 'AlternateProductCode',
         suec.SKUCode     as 'AlternateSKUCode',
         suec.DefaultQC,
         suec.SendToQC
    from StorageUnitExternalCompany suec
    join ExternalCompany              ec on suec.ExternalCompanyId = ec.ExternalCompanyId
    join viewProduct                  vp on suec.StorageUnitId = vp.StorageUnitId
