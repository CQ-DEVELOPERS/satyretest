﻿
create view viewWS
as
select ws.WebServiceLogId,
       ws.UserName,
       ws.Password,
       ws.XmlType,
       ws.XML,
       ws.Result,
       ws.InsertDate,
       'exec CQCommon..p_UserOperator_Insert_Xml @UserName = N''' + ws.UserName + ''',@Password = N''' + ws.Password + ''',@XmlType = N''' + ws.XmlType + ''',@Xml= N''' + ws.xml + '''' as 'CQCommon',
       'declare @xml nvarchar(max) exec p_Interface_Webservice_' + ws.XmlType + ' @xml output select @xml' as 'Reprocess'
  from CQCommon..WebServiceLog ws (nolock)
  join Operator                 o (nolock) on ws.UserName = o.Operator
