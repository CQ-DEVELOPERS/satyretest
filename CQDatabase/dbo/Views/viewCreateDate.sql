﻿CREATE VIEW dbo.viewCreateDate
AS
select CreateDate,
       OrderNumber
  from OutboundDocument
 where CreateDate > convert(varchar(10), Getdate(), 120)
union
select CreateDate,
       OrderNumber
  from InboundDocument
 where CreateDate > convert(varchar(10), Getdate(), 120)
