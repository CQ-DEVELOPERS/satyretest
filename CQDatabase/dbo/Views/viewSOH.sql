﻿CREATE VIEW dbo.viewSOH
AS
  select distinct a.WarehouseId,
         a.AreaId,
         a.AreaCode,
         a.Area,
         a.AreaType,
         a.AreaSequence,
         a.WarehouseCode,
         a.StockOnHand,
         a.Replenish,
         lt.LocationType,
         l.Location,
         l.LocationId,
         l.SecurityCode,
         p.HostId,
         sub.StorageUnitBatchId,
         su.StorageUnitId,
         sub.BatchId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         b.BillOfEntry,
         b.BOELineNumber,
         b.ExpiryDate,
         datediff(dd, getdate(), ExpiryDate) as 'DaysToExpiry',
         round(case when p.ShelfLifeDays > 0
              then (convert(float, datediff(dd, getdate(), ExpiryDate)) / convert(float, p.ShelfLifeDays)) * 100
              else 0
              end,3) as 'ShelfLifePercentage',
         s.Status,
         s.StatusCode,
         (select max(pk.Quantity)
            from Pack                       pk (nolock)
            left
            join PackType                   pt (nolock) on pk.PackTypeId          = pt.PackTypeId
                                          and pt.OutboundSequence    = 1
           where su.StorageUnitId       = pk.StorageUnitId
             and a.WarehouseId          = pk.WarehouseId)
          as 'PalletQuantity',
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity,
         l.StockTakeInd,
         l.ActiveBinning,
         l.ActivePicking,
         l.Used,
         pn.PrincipalCode
    from StorageUnitBatch          sub (nolock)
    join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId           = p.ProductId
    left
    join Principal                  pn (nolock) on p.PrincipalId          = pn.PrincipalId
    join SKU                       sku (nolock) on su.SKUId               = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId            = b.BatchId
    join Status                      s (nolock) on b.StatusId             = s.StatusId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join Location                    l (nolock) on subl.LocationId        = l.LocationId
    join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
    join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
   where a.StockOnHand = 1
