﻿CREATE VIEW dbo.viewIssue
AS
  select osi.OutboundShipmentId,
         i.WarehouseId,
         od.OutboundDocumentId,
         i.IssueId,
         i.Delivery,
         i.DropSequence,
         od.OrderNumber,
         od.PrincipalId,
         pn.PrincipalCode,
         r.Route,
         p.Priority,
         i.AreaType,
         ec.ExternalCompanyCode as 'CustomerCode',
         ec.ExternalCompany     as 'CustomerName',
         odt.OutboundDocumentType,
         odt.OutboundDocumentTypeCode,
         si.Status,
         si.StatusCode,
         i.Remarks,
         od.CreateDate,
         i.DeliveryDate,
         i.Weight,
         i.Total,
         i.Complete,
         i.WaveId
  from OutboundDocument      od (nolock)
  left
  join Principal             pn (nolock) on od.PrincipalId            = pn.PrincipalId
  join ExternalCompany       ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
  join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
  join Status                si (nolock) on i.StatusId                = si.StatusId
  join Priority               p (nolock) on i.PriorityId              = p.PriorityId
  left outer
  join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
  left
  join Route                   r (nolock) on i.RouteId = r.RouteId
