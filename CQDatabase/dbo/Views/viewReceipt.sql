﻿CREATE VIEW dbo.viewReceipt
AS
  select isr.InboundShipmentId,
         r.WarehouseId,
         idt.InboundDocumentType,
         idt.InboundDocumentTypeCode,
         id.OrderNumber,
         r.Delivery,
         r.DeliveryNoteNumber,
         r.LocationId,
         l.Location,
         s.StatusCode,
         s.Status,
         ec.ExternalCompanyCode as 'SupplierCode',
         ec.ExternalCompany     as 'SupplierName',
         r.Interfaced,
         id.CreateDate,
         r.DeliveryDate,
         r.ReceiptId,
         id.InboundDocumentId,
         id.PrincipalId,
         p.PrincipalCode
    from Receipt               r (nolock)
    join Status                s (nolock) on r.StatusId = s.StatusId
    join InboundDocument      id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    join InboundDocumentType idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    left
    join ExternalCompany      ec (nolocK) on id.ExternalCompanyId = ec.ExternalCompanyId
    left
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
    left
    join Principal             p (nolock) on id.PrincipalId = p.PrincipalId
    left
    join Location              l (nolock) on r.LocationId = l.LocationId
