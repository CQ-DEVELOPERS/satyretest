﻿
create view viewJH
as
select jh.JobId,
       jh.PriorityId,
       jh.OperatorId,
       o.Operator,
       jh.StatusId,
       s.Status,
       jh.WarehouseId,
       jh.ReceiptLineId,
       jh.IssueLineId,
       jh.ContainerTypeId,
       jh.ReferenceNumber,
       jh.TareWeight,
       jh.Weight,
       jh.CommandType,
       jh.InsertDate
  from JobHistory         jh (nolock)
  join Job                 j (nolock) on jh.JobId      = j.JobId
  left
  join Status              s (nolock) on jh.StatusId   = s.StatusId
  left
  join Operator            o (nolock) on jh.OperatorId = o.OperatorId
 
