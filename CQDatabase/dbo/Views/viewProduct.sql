﻿CREATE VIEW dbo.viewProduct
AS
  select p.ProductId,
         su.StorageUnitId,
         p.ProductCode,
         p.Product,
         p.Barcode as 'ProductBarcode',
         sku.SKUCode,
         sku.SKU,
         p.ShelfLifeDays,
         su.ProductCategory,
         p.ProductType,
         s.Status,
         p.ParentProductCode,
         pr.PrincipalId,
         pr.PrincipalCode
--         pk.Weight,
--         pk.Volume
    from StorageUnit       su (nolock)
    join Product            p (nolock) on su.ProductId          = p.ProductId
    left
    join Principal         pr (nolock) on p.PrincipalId         = pr.PrincipalId
    join SKU              sku (nolock) on su.SKUId              = sku.SKUId
    join Status             s (nolock) on p.StatusId            = s.StatusId
--    join Pack              pk (nolock) on su.StorageUnitId      = pk.StorageUnitId
--   where pk.PackTypeId = 2
