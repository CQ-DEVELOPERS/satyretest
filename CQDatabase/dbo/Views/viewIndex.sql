﻿create view viewIndex
as

select object_name(i.object_id) as 'TableName',
       i.name as 'IndexName',
       case when i.index_id = 1 then 'clustered' else 'nonclustered' end as 'IndexType',
       i.data_space_id,
       case when i.ignore_dup_key <> 0 then 'Ignore Duplicate Keys' else '' end as 'IgnoreDuplicateKeys',
       case when i.is_unique <> 0 then 'Unique' else '' end 'IsUnique',
       case when i.is_hypothetical <> 0 then ', hypothetical' else '' end 'IsHypothetical',
       case when i.is_primary_key <> 0 then 'Primary Key' else '' end 'IsPrimaryKey',
       case when i.is_unique_constraint <> 0 then 'Unique Key' else '' end as 'IsUniqueKey',
       case when s.auto_created <> 0 then 'Auto Create' else '' end as 'IsAutoCreate',
       case when s.no_recompute <> 0 then 'Stats No Recompute' else '' end as 'IsNoRecompute',
       index_col(object_name(i.object_id), ic.index_id, ic.index_column_id) as 'IndexColumn',
       ic.index_column_id as 'ColumnNumber'
 	from sys.indexes i
 	join sys.index_columns ic on i.object_id = ic.object_id
 	                         and i.index_id = ic.index_id
 	join sys.stats          s on i.object_id = s.object_id
 	                         and i.index_id = s.stats_id
 --where i.object_id = object_id('Instruction')
