﻿CREATE VIEW dbo.viewSUL
AS
  select a.WarehouseId,
         su.StorageUnitId,
         l.LocationId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         a.AreaId,
         a.Area,
         a.AreaCode,
         a.AreaType,
         l.Location
    from StorageUnit          su (nolock)
    join Product               p (nolock) on su.ProductId     = p.ProductId
    join SKU                 sku (nolock) on su.SKUId         = sku.SKUId
    join StorageUnitLocation sul (nolock) on su.StorageUnitId = sul.StorageUnitId
    join Location              l (nolock) on sul.LocationId   = l.LocationId
    join AreaLocation         al (nolock) on l.LocationId     = al.LocationId
    join Area                  a (nolock) on al.AreaId        = a.AreaId
