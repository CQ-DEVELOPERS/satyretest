﻿CREATE VIEW dbo.viewAS
AS
select sequence.AdjustmentType,
       pick.WarehouseId    as 'PickWarehouseId',
       pick.AreaId         as 'PickAreaId',
       pick.Area           as 'PickArea',
       pick.WarehouseCode  as 'PickWarehouseCode',
       store.WarehouseId   as 'StoreWarehouseId',
       store.AreaId        as 'StoreAreaId',
       store.Area          as 'StoreArea',
       store.WarehouseCode as 'StoreWarehouseCode'
  from AreaSequence sequence
  join Area             pick on sequence.PickAreaId = pick.AreaId
  join Area            store on sequence.StoreAreaId = store.AreaId
