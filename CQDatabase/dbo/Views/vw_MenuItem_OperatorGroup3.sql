﻿create view [dbo].[vw_MenuItem_OperatorGroup3] as 
  Select distinct case when mi.ParentMenuItemId is null then
                   i.MenuItemId else
                   mi.menuItemId end as MenuItemId3,
         mi.MenuId,
         case when mi.ParentMenuItemId is null then
                   'All - ' + i.MenuItem else
                   m.Menu  + ' \ ' + mi.MenuItem end as MenuItem3,
                   
         --isnull(mi.MenuItemText,'All - ' + i.MenuItemText) as MenuItemText,
         isnull(mi.ParentMenuItemId,i.MenuItemId) as ParentMenuItemId,
		  o.operatorgroupid,
		  g.operatorGroup,
  		
		  case when mi.ParentMenuItemId is null then
                   o1.Access else
                   o.Access end as Access
  	
  from Menuitem	mi
  Left join Menu m on mi.MenuId = m.MenuId
		Left join OperatorGroupMenuItem o	(nolock)	on o.menuItemId = mi.MenuItemid
		Left Join operatorgroup g on g.operatorgroupid = o.operatorgroupid	
  full outer join MenuItem i on i.ParentMenuItemId = mi.MenuItemId
  Left join OperatorGroupMenuItem o1 (nolock) on o1.menuItemId = i.MenuItemid and o1.operatorGroupid = o.operatorGroupId

  where g.OperatorGroup is not null

