﻿CREATE VIEW dbo.viewJob
AS
select j.JobId,
       s.StatusCode,
       j.PriorityId,
       j.OperatorId,
       j.StatusId,
       j.WarehouseId,
       j.ReferenceNumber,
       j.Weight,
       j.CheckedBy,
       j.CheckedDate,
       j.DropSequence,
       j.Pallets,
       'update Job set StatusId = dbo.ufn_StatusId(''IS'',''CK'') where JobId = ' + convert(nvarchar(10), JobId) as 'Update'
  from Job    j (nolock)
  join Status s (nolock) on j.StatusId = s.StatusId
