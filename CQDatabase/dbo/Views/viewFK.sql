﻿create view viewFK
as
select so.type,
       object_name(so.parent_object_id) as 'TableName',
       fk.name as 'ForeignKey',
       col_name(fk.parent_object_id, fkc.parent_column_id) as 'ColumnName',
       object_name(fk.referenced_object_id) as 'ReferencedTable',
       col_name(fk.referenced_object_id, fkc.referenced_column_id) as 'ReferencedColumn',
       'alter table ' + object_name(so.parent_object_id) + ' drop constraint ' + fk.name 'Drop'
  from sys.objects so
  join sys.foreign_keys fk on fk.object_id = so.object_id
  join sys.foreign_key_columns fkc on fkc.constraint_object_id = so.object_id
 where so.type in ('C ','PK','UQ','F ', 'D ')
   --and so.parent_object_id = object_id('IssueLine')
