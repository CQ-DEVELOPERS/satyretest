﻿CREATE VIEW dbo.viewMS
AS
select og.OperatorGroupId,
       og.OperatorGroup,
       pick.WarehouseId    as 'PickWarehouseId',
       pick.AreaId         as 'PickAreaId',
       pick.Area           as 'PickArea',
       pick.WarehouseCode  as 'PickWarehouseCode',
       stage.WarehouseId   as 'StageWarehouseId',
       stage.AreaId        as 'StageAreaId',
       stage.Area          as 'StageArea',
       store.WarehouseId   as 'StoreWarehouseId',
       store.AreaId        as 'StoreAreaId',
       store.Area          as 'StoreArea',
       store.WarehouseCode as 'StoreWarehouseCode'
  from MovementSequence sequence (nolock)
  join Area                 pick (nolock) on sequence.PickAreaId = pick.AreaId
  join Area                store (nolock) on sequence.StoreAreaId = store.AreaId
  join Area                stage (nolock) on sequence.StageAreaId = stage.AreaId
  join OperatorGroup          og (nolock) on sequence.OperatorGroupId = og.OperatorGroupId

