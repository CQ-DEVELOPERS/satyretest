﻿CREATE VIEW dbo.viewAOG
AS
select a.WarehouseId,
       a.AreaId,
       a.Area,
       a.AreaCode,
       a.AreaType,
       og.OperatorGroupId,
       og.OperatorGroup,
       og.OperatorGroupCode
  from AreaOperatorGroup oga (nolock)
  join Area                a (nolock) on oga.AreaId          = a.AreaId
  join OperatorGroup      og (nolock) on oga.OperatorGroupId = og.OperatorGroupId
