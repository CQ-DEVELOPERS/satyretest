﻿CREATE VIEW dbo.viewPalletQty
AS
  select pk.WarehouseId,
         pk.PackId,
         pt.PackTypeId,
         pt.PackType,
         vs.*,
         pk.BarCode as 'PackBarcode',
         pk.Quantity,
         pk.TareWeight,
         pk.NettWeight,
         pk.Weight,
         pk.Volume,
         pt.InboundSequence,
         pt.OutboundSequence
    from viewProduct vs
    join Pack      pk on vs.StorageUnitId = pk.StorageUnitId
    join PackType  pt on pk.PackTypeId = pt.PackTypeId
   where pt.OutboundSequence = 1
