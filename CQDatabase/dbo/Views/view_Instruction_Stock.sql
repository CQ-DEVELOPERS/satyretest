﻿


Create View [dbo].[view_Instruction_Stock] as 
Select Distinct it.InstructionType,
				InstructionTypeCode,
				ar.Area,
				ar.Areaid,
				s.status as Status,
				--Case when (s.status = 'Finished') and (ConfirmedQuantity is null) then 'Cancelled'
					
				--Else
				--		s.Status
				--End as Status,
				dbo.DateFormatNoTime(CreateDate) as CreateDate,
				PicklocationId,
				i.warehouseId
From Instruction i
	Join InstructionType it ON i.InstructionTypeId = it.InstructionTypeId
	Join AreaLocation a on a.LocationId = i.PickLocationId
	Join Area ar on ar.AreaId = a.AreaId
	Join Status s on s.Statusid = i.Statusid
WHERE     (it.InstructionTypeCode in ('STL','STA','STP','STE')) 



