﻿


Create View [dbo].[view_Instruction_Stock_Ref] as 
Select Distinct it.InstructionType,
				InstructionTypeCode,
				ar.Area,
				ar.Areaid,
				s.status as Status,
				--Case when (s.status = 'Finished') and (ConfirmedQuantity is null) then 'Cancelled'
					
				--Else
				--		s.Status
				--End as Status,
				dbo.DateFormatNoTime(CreateDate) as CreateDate,
				PicklocationId,
				i.warehouseId,
				strj.StockTakeReferenceId
From Instruction i
	Join InstructionType it ON i.InstructionTypeId = it.InstructionTypeId
	Join AreaLocation a on a.LocationId = i.PickLocationId
	Join Area ar on ar.AreaId = a.AreaId
	Join Status s on s.Statusid = i.Statusid
	join StockTakeReferenceJob strj on i.JobId = strj.JobId
WHERE     (it.InstructionTypeCode in ('STL','STA','STP','STE')) 



