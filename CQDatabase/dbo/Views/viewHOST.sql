﻿CREATE VIEW dbo.viewHOST
AS
select h.ProcessedDate,
       w.ParentWarehouseId as 'WarehouseId',
       w.WarehouseCode,
       su.StorageUnitId,
       b.BatchId,
       p.ProductCode,
       p.Product,
       sku.SKUCode,
       h.Quantity,
       null as 'ActualCost'
  from InterfaceImportSOH h
  join Product         p on h.ProductCode = p.HostId
  join StorageUnit    su on p.ProductId   = su.ProductId
  join SKU           sku on su.SKUId      = sku.SKUId
  join Batch           b on h.Batch       = b.Batch
  join Warehouse       w on h.Location    = w.HostId
