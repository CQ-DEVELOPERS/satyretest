﻿CREATE VIEW dbo.viewIssueLine
AS
  select i.WaveId,
         osi.OutboundShipmentId,
         i.WarehouseId,
         od.OutboundDocumentId,
         i.IssueId,
         i.Delivery,
         od.OrderNumber,
         ec.ExternalCompanyCode as 'CustomerCode',
         ec.ExternalCompany     as 'CustomerName',
         odt.OutboundDocumentTypeCode,
         sil.Status,
         sil.StatusCode,
         i.Remarks,
         od.CreateDate,
         i.DeliveryDate,
         ol.OutboundLineId,
         il.IssueLineId,
         ol.LineNumber,
         vs.*,
         il.Quantity as 'OrderedQuantity',
         il.ConfirmedQuatity,
         il.DeliveredQuantity
  from OutboundDocument      od (nolock)
  join ExternalCompany       ec (nolock) on od.ExternalCompanyId      = ec.ExternalCompanyId
  join OutboundDocumentType odt (nolock) on od.OutboundDocumentTypeId = odt.OutboundDocumentTypeId
  join Issue                  i (nolock) on od.OutboundDocumentId     = i.OutboundDocumentId
  join IssueLine             il (nolock) on i.IssueId                 = il.IssueId
  join Status               sil (nolock) on il.StatusId               = sil.StatusId
  join OutboundLine          ol (nolock) on od.OutboundDocumentId     = ol.OutboundDocumentId
                               and il.OutboundLineId         = ol.OutboundLineId
  join viewStock             vs on il.StorageUnitBatchId     = vs.StorageUnitBatchId
  left outer
  join OutboundShipmentIssue osi (nolock) on i.IssueId = osi.IssueId
