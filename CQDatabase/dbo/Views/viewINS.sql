﻿CREATE VIEW dbo.viewINS
AS
select i.WarehouseId,
       it.InstructionTypeCode,
       i.JobId,
       j.ReferenceNumber,
       i.PalletId,
       i.InstructionId,
       i.InstructionRefId,
       s.StatusCode  as 'JobCode',
       si.StatusCode as 'InstructionCode',
       o.Operator,
       vs.ProductCode,
       vs.Product,
       vs.SKUCode,
       vs.Batch,
       pick.AreaCode  as 'PickAreaCode',
       pick.Location  as 'PickLocation',
       store.AreaCode as 'StoreAreaCode',
       store.Location as 'StoreLocation',
       --pk.Quantity    as 'PalletQuantity',
       i.Quantity,
       i.ConfirmedQuantity,
       i.CheckQuantity,
       i.CreateDate,
       i.StartDate,
       i.EndDate,
       isnull(i.EndDate, isnull(i.StartDate, i.CreateDate)) 'MostRecent'
  from Instruction i (nolock)
  left
  join viewStock         vs (nolock) on i.StorageUnitBatchId = vs.StorageUnitBatchId
  join Status            si (nolock) on i.StatusId           = si.StatusId
  join InstructionType   it (nolock) on i.InstructionTypeId  = it.InstructionTypeId
  join Job                j (nolock) on i.JobId              = j.JobId
  join Status             s (nolock) on j.StatusId           = s.StatusId
  left
  join viewLocation    pick (nolock) on i.PickLocationId     = pick.LocationId
  left
  join viewLocation   store (nolock) on i.StoreLocationId    = store.LocationId
  left
  join Operator           o (nolock) on j.OperatorId         = o.OperatorId
  --left
  --join Pack              pk (nolock) on vs.StorageUnitId     = pk.StorageUnitId
  --                                  and i.WarehouseId        = pk.WarehouseId
  --join PackType          pt (nolock) on pk.PackTypeId        = pt.PackTypeId
  --                                  and pt.OutboundSequence  = 1
