﻿
Create View viewReceiptLine
as
  Select isr.InboundShipmentId,
         id.WarehouseId,
         idt.InboundDocumentType,
         idt.InboundDocumentTypeCode,
         pn.PrincipalCode,
         r.Delivery,
         id.OrderNumber,
         r.AdditionalText1,
         r.AdditionalText2,
         r.DeliveryNoteNumber,
         r.BOE,
         il.BOELineNumber,
         r.LocationId,
         l.Location,
         il.LineNumber,
         id.CreateDate,
         r.DeliveryDate,
         s.StatusCode,
         s.Status,
         ec.ExternalCompanyCode as 'SupplierCode',
         ec.ExternalCompany     as 'SupplierName',
         p.ProductCode,
         p.Product,
         sk.SKUCode,
         isnull(rl.RequiredQuantity,0) as 'RequiredQuantity',
         isnull(rl.ReceivedQuantity,0) as 'ReceivedQuantity',
         isnull(rl.AcceptedQuantity,0) as 'AcceptedQuantity',
         isnull(rl.RejectQuantity,0) as 'RejectQuantity',
         isnull(rl.DeliveryNoteQuantity,0) as 'DeliveryNoteQuantity',
         isnull(rl.SampleQuantity,0) as 'SampleQuantity',
         isnull(rl.AssaySamples,0) as 'AssaySamples',
         p.RetentionSamples,
         b.Batch,
         b.ExpiryDate,
         rl.OperatorId,
         id.InboundDocumentId,
         il.InboundLineId,
         rl.ReceiptId,
         rl.ReceiptLineId,
         sub.StorageUnitBatchId,
         sub.StorageUnitId,
         b.BatchId
    from ReceiptLine             rl
    join Status                   s (nolock) on rl.StatusId = s.StatusId
    join Receipt                  r (nolock) on rl.ReceiptId = r.ReceiptId
    join InboundDocument         id (nolock) on r.InboundDocumentId = id.InboundDocumentId
    left
    join Principal               pn (nolock) on id.PrincipalId = pn.PrincipalId
    join InboundDocumentType    idt (nolock) on id.InboundDocumentTypeId = idt.InboundDocumentTypeId
    join InboundLine             il (nolock) on rl.InboundLineId = il.InboundLineId
    left
    join ExternalCompany         ec (nolock) on id.ExternalCompanyId = ec.ExternalCompanyId
    join StorageUnitBatch       sub (nolock) on rl.StorageUnitBatchId = sub.StorageUnitBatchId
    join StorageUnit             su (nolock) on sub.StorageUnitId = su.StorageUnitId
    join Sku                     sk (nolock) on su.SkuId = sk.SkuId
    join Product                  p (nolock) on su.ProductId = p.ProductId
    join Batch                    b (nolock) on sub.BatchId = b.BatchId
    left
    join InboundShipmentReceipt isr (nolock) on r.ReceiptId = isr.ReceiptId
    left
    join Location                 l (nolock) on r.LocationId = l.LocationId
