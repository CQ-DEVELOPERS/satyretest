﻿CREATE VIEW dbo.viewOrderNumber
AS
select WarehouseId,
       OrderNumber
  from OutboundDocument (nolock)
union
select WarehouseId,
       OrderNumber
  from InboundDocument (nolock)
