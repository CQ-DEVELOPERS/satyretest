﻿CREATE VIEW dbo.viewQTY
AS  
  select a.WarehouseId,
         a.AreaId,
         a.AreaCode,
         a.Area,
         a.AreaType,
         a.WarehouseCode,
         a.StockOnHand,
         lt.LocationType,
         l.Location,
         l.LocationId,
         sub.StorageUnitBatchId,
         su.StorageUnitId,
         sub.BatchId,
         p.ProductCode,
         p.Product,
         sku.SKUCode,
         sku.SKU,
         b.Batch,
         s.Status,
         s.StatusCode,
         isnull(pk.Quantity, sku.Quantity) as 'PalletQuantity',
         subl.ActualQuantity,
         subl.AllocatedQuantity,
         subl.ReservedQuantity,
         l.StockTakeInd,
         l.ActiveBinning,
         l.ActivePicking,
         l.Used
    from StorageUnitBatch          sub (nolock)
    join StorageUnit                su (nolock) on sub.StorageUnitId      = su.StorageUnitId
    join Product                     p (nolock) on su.ProductId           = p.ProductId
    join SKU                       sku (nolock) on su.SKUId               = sku.SKUId
    join Batch                       b (nolock) on sub.BatchId            = b.BatchId
    join Status                      s (nolock) on b.StatusId             = s.StatusId
    join StorageUnitBatchLocation subl (nolock) on sub.StorageUnitBatchId = subl.StorageUnitBatchId
    join Location                    l (nolock) on subl.LocationId        = l.LocationId
    join LocationType               lt (nolock) on l.LocationTypeId       = lt.LocationTypeId
    join AreaLocation               al (nolock) on l.LocationId           = al.LocationId
    join Area                        a (nolock) on al.AreaId              = a.AreaId
    left
    join Pack                       pk (nolock) on su.StorageUnitId       = pk.StorageUnitId
                                               and a.WarehouseId          = pk.WarehouseId
                                               and pk.PackTypeId          = 1
