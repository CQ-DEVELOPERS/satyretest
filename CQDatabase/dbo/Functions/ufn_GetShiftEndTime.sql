﻿Create Function [dbo].[ufn_GetShiftEndTime]()
Returns int
AS
Begin
Return (Select top 1 EndTime
from Shifts where dbo.ufn_GetCurrentMinutes() > StartTime and dbo.ufn_GetCurrentMinutes() < EndTime)
End
