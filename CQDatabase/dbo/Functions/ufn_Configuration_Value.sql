﻿CREATE FUNCTION [dbo].[ufn_Configuration_Value](@ConfigurationId int, @WarehouseId int)
RETURNS int
AS
BEGIN
  declare @Value int
  
  select @Value = convert(int, [Value])
    from Configuration (nolock)
   where ConfigurationId = @ConfigurationId
     and WarehouseId = @WarehouseId
  
  return @Value
END
