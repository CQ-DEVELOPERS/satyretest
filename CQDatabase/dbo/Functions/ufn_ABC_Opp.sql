﻿CREATE FUNCTION [dbo].[ufn_ABC_Opp] (@ABC char(1), @Int int)
RETURNS numeric
AS
BEGIN
declare @MaxColumn int
select @MaxColumn = max([Column]) from viewLocation where Ailse = @ABC and Location like 'W02%'
    
    select @Int = @Int where @ABC = 'A'
    select @Int = @Int where @ABC = 'B'

    select @Int = @Int where @ABC = 'C'
    select @Int = @MaxColumn - @Int where @ABC = 'D'
    
    select @Int = @Int where @ABC = 'E'
    select @Int = @MaxColumn - @Int where @ABC = 'F'

    select @Int = @MaxColumn - @Int where @ABC = 'G'
    select @Int = @Int where @ABC = 'H'
    
    select @Int = @Int where @ABC = 'I'
    select @Int = @MaxColumn - @Int where @ABC = 'J'

    select @Int = @MaxColumn - @Int where @ABC = 'K'
    select @Int = @Int where @ABC = 'L'
    
    select @Int = @Int where @ABC = 'M'
    select @Int = @MaxColumn - @Int where @ABC = 'N'

    select @Int = @MaxColumn - @Int where @ABC = 'O'
    select @Int = @Int where @ABC = 'P'
    
    select @Int = @Int where @ABC = 'Q'
    select @Int = @MaxColumn - @Int where @ABC = 'R'

    select @Int = @MaxColumn - @Int where @ABC = 'S'
    select @Int = @Int where @ABC = 'T'
    
    select @Int = @Int where @ABC = 'U'
    select @Int = @MaxColumn - @Int where @ABC = 'V'

    select @Int = @MaxColumn - @Int where @ABC = 'W'
    select @Int = @Int where @ABC = 'X'
    
    select @Int = @Int where @ABC = 'Y'
    select @Int = @MaxColumn - @Int where @ABC = 'Z'
    
--    select @Int = 100 - @Int where @ABC = 'G'
--    select @Int = 100 - @Int where @ABC = 'H'
--    select @Int = @Int where @ABC = 'I'
--    select @Int = @Int where @ABC = 'J'
    
    RETURN @INT
END
