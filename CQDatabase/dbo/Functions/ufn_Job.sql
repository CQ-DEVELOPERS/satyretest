﻿CREATE FUNCTION [dbo].[ufn_Job] (@JobId int = null)
RETURNS TABLE
AS
RETURN 
(
    select JobId,
           JobId as 'Job'
      from Job
     where JobId = isnull(@JobId, JobId)
);
