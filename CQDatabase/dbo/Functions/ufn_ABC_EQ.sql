﻿CREATE FUNCTION [dbo].[ufn_ABC_EQ] (@ABC char(1))  
RETURNS numeric  
AS  
BEGIN  
    declare @Int int  
      
    select @Int = 6 where @ABC = 'A'  
    select @Int = 6 where @ABC = 'B'  
    select @Int = 5 where @ABC = 'C'  
    select @Int = 5 where @ABC = 'D'  
    select @Int = 4 where @ABC = 'E'  
    select @Int = 4 where @ABC = 'F'  
    select @Int = 7 where @ABC = 'G'  
    select @Int = 8 where @ABC = 'H'  
    select @Int = 9 where @ABC = 'I'  
    select @Int = 10 where @ABC = 'J'  
    select @Int = 11 where @ABC = 'K'  
    select @Int = 12 where @ABC = 'L'  
    select @Int = 13 where @ABC = 'M'  
    select @Int = 14 where @ABC = 'N'  
    select @Int = 15 where @ABC = 'O'  
    select @Int = 16 where @ABC = 'P'  
    select @Int = 17 where @ABC = 'Q'  
    select @Int = 18 where @ABC = 'R'  
    select @Int = 19 where @ABC = 'S'  
    select @Int = 20 where @ABC = 'T'  
    select @Int = 21 where @ABC = 'U'  
    select @Int = 22 where @ABC = 'V'  
    select @Int = 23 where @ABC = 'W'  
    select @Int = 24 where @ABC = 'X'  
    select @Int = 25 where @ABC = 'Y'  
    select @Int = 26 where @ABC = 'Z'  
      
    RETURN @INT  
END
