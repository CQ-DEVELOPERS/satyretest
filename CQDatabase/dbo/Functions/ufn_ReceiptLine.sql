﻿CREATE FUNCTION [dbo].[ufn_ReceiptLine] (@ReceiptLineId int = null)
RETURNS TABLE
AS
RETURN 
(
    select ReceiptLineId,
           ReceiptLineId as 'ReceiptLine'
      from ReceiptLine
     where ReceiptLineId = isnull(@ReceiptLineId, ReceiptLineId)
);
