﻿CREATE FUNCTION [dbo].[ufn_Configuration_Description](@ConfigurationId int, @WarehouseId int)
RETURNS varchar(50)
AS
BEGIN
  declare @Configuration varchar(50)
  
  select @Configuration = Configuration
    from Configuration (nolock)
   where ConfigurationId = @ConfigurationId
     and WarehouseId = @WarehouseId
  
  return @Configuration
END
