﻿CREATE FUNCTION [dbo].[ufn_StorageUnit] (@StorageUnitId int = null)
RETURNS TABLE
AS
RETURN 
(
    select su.StorageUnitId, p.ProductCode + ', ' + sku.SKUCode as 'StorageUnit'
      from dbo.StorageUnit su  (nolock)
      join dbo.SKU         sku (nolock) on su.SKUId = sku.SKUId
      join dbo.Product     p   (nolock) on su.ProductId = p.ProductId
     where su.StorageUnitId = isnull(@StorageUnitId, su.StorageUnitId)
);
