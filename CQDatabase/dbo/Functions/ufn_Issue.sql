﻿CREATE FUNCTION [dbo].[ufn_Issue] (@IssueId int = null)
RETURNS TABLE
AS
RETURN 
(
    select IssueId,
           DeliveryNoteNumber as 'Issue'
      from Issue
     where IssueId = isnull(@IssueId, IssueId)
);
