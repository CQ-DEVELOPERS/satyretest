﻿CREATE FUNCTION dbo.ufn_Getdate()
RETURNS datetime
AS
BEGIN
    declare @getdate      datetime = getdate(),
            @TimeZoneCode nvarchar(10)
    
    select @TimeZoneCode = convert(nvarchar(10), Value)
      from Configuration
     where ConfigurationId = 442
       and Value is not null
    
    select @getdate = DATEADD(hh, convert(numeric(13,3), Conversion), @getdate)
      from TimeZone
     where TimeZoneCode = @TimeZoneCode
    
    return @getdate
END
