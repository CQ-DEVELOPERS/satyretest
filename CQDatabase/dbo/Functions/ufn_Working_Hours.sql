﻿CREATE FUNCTION dbo.ufn_Working_Hours(@StartDate datetime, @EndDate datetime)
RETURNS int
AS
BEGIN
  declare @Hours int
  
  select @Hours = DATEDIFF(mi, @StartDate, @EndDate)
  
  return @Hours
END
