﻿CREATE FUNCTION [dbo].[fn_StorageUnit] (@StorageUnitId int = null)
RETURNS TABLE
AS
RETURN 
(
    select su.StorageUnitId, p.ProductCode + ', ' + sku.SKUCode as 'StorageUnit'
      from dbo.StorageUnit su
      join dbo.SKU         sku on su.SKUId = sku.SKUId
      join dbo.Product     p   on su.ProductId = p.ProductId
     where su.StorageUnitId = isnull(@StorageUnitId, su.StorageUnitId)
);
