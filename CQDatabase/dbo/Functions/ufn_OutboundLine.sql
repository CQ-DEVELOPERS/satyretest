﻿CREATE FUNCTION [dbo].[ufn_OutboundLine] (@OutboundLineId int = null)
RETURNS TABLE
AS
RETURN 
(
    select OutboundLineId,
           LineNumber as 'OutboundLine'
      from OutboundLine
     where OutboundLineId = isnull(@OutboundLineId, OutboundLineId)
);
