﻿CREATE FUNCTION dbo.ufn_StatusId(@Type varchar(2), @StatusCode varchar(10))
RETURNS int
AS
BEGIN
  declare @StatusId int
  
  select @StatusId = StatusId
    from Status (nolock)
   where StatusCode = @StatusCode
     and Type       = @Type
  
  return @StatusId
END
