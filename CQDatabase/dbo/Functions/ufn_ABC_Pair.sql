﻿CREATE FUNCTION dbo.ufn_ABC_Pair (@ABC char(1))
RETURNS numeric
AS
BEGIN
    declare @Int int
    
    select @Int = 1 where @ABC = 'A'
    select @Int = 1 where @ABC = 'B'
    select @Int = 2 where @ABC = 'C'
    select @Int = 2 where @ABC = 'D'
    select @Int = 3 where @ABC = 'E'
    select @Int = 3 where @ABC = 'F'
    select @Int = 4 where @ABC = 'G'
    select @Int = 4 where @ABC = 'H'
    select @Int = 5 where @ABC = 'I'
    select @Int = 5 where @ABC = 'J'
    select @Int = 6 where @ABC = 'K'
    select @Int = 6 where @ABC = 'L'
    select @Int = 7 where @ABC = 'M'
    select @Int = 7 where @ABC = 'N'
    select @Int = 8 where @ABC = 'O'
    select @Int = 8 where @ABC = 'P'
    select @Int = 9 where @ABC = 'Q'
    select @Int = 9 where @ABC = 'R'
    select @Int = 10 where @ABC = 'S'
    select @Int = 10 where @ABC = 'T'
    select @Int = 11 where @ABC = 'U'
    select @Int = 11 where @ABC = 'V'
    select @Int = 12 where @ABC = 'W'
    select @Int = 12 where @ABC = 'X'
    select @Int = 13 where @ABC = 'Y'
    select @Int = 13 where @ABC = 'Z'
    
    RETURN @INT
END
