﻿CREATE function [dbo].[ConvertTo128] (@i nvarchar(50))
returns nvarchar(50)
as
begin

declare @str1 nvarchar(50)
declare @str2 nvarchar(50)
declare @str3 nvarchar(50)
declare @cnt1 int
declare @cnt2 int
	
	set @str1 = char(154)
	set @cnt1 = 1
	set @cnt2 = 104
	While len(@i) > 0 
	begin
		set @str2 = left(@i,1)
		set @str3 = (Select Char(AsciiNumber) from code128 where ascii(@str2) = AsciiNumber)
		set @str1 = @str1 + @str3 --+ ' ' + convert(char(10),@cnt2)
		set @cnt2 = @cnt2 + ((Select code128 from code128 where ascii(@str2) = AsciiNumber) * @cnt1)
		set @cnt1 = @cnt1 + 1
		set @i = right(@i,len(@i)-1)
	end
	set @str1 = @str1 + (Select Char(AsciiNumber) from code128 where code128 = (@cnt2 % 103))
	set @str1 = @str1 + Char(156)
	return (@str1)
end

