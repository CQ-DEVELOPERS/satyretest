﻿Create Function dbo.ufn_DateDiff_Days	(@StartDate datetime,
										@EndDate Datetime)
Returns varchar(20)
as
Begin
return rtrim(convert(char(3),DATEDIFF(DD,@startDate,@EndDate))) + ' Days ' + 
		right('0' + rtrim(convert(char(2),Datediff(hh,@startDate,@EndDate) % 24)),2) + 'h' + 
			  right('0' + rtrim(convert(char(2),Datediff(MI,@startDate,@EndDate) % 60)),2) + 'm'
end 			  
