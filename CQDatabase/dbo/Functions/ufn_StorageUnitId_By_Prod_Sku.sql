﻿CREATE FUNCTION [dbo].[ufn_StorageUnitId_By_Prod_Sku] (@ProductCode varchar(30) = null, @SKUCode varchar(10) = null)
RETURNS TABLE
AS
RETURN 
(
    select su.StorageUnitId
      from dbo.StorageUnitBatch sub (nolock)
      join dbo.Batch            b   (nolock) on sub.BatchId = b.BatchId
      join dbo.StorageUnit      su  (nolock) on sub.StorageUnitId = su.StorageUnitId
      join dbo.SKU              sku (nolock) on su.SKUId = sku.SKUId
      join dbo.Product          p   (nolock) on su.ProductId = p.ProductId
     where p.ProductCode like isnull(@ProductCode, p.ProductCode) + '%'
       and sku.SKUCode   like isnull(@SKUCode, sku.SKUCode) + '%'
       and b.Batch = 'Default'
);
