﻿create function [dbo].[Code128Barcode](@input varchar(3997), @table char(1) = 'B')
returns table
with schemabinding
as
--=======================================================================--
--== Function to build the required data string for a code 128 barcode ==--
--== Code written from a description of the specification by           ==--
--== Mister Magoo - find me on SSC or twitter @mistermag00             ==--
--== NOTE: this code does not validate that you are passing in valid   ==--
--==       data for code 128 barcodes. SISO                            ==--
--=======================================================================--

return (
		-- build an inline fast tally table
		with n10(n) as (
			select 1 union all select 1 union all 
			select 1 union all select 1 union all 
			select 1 union all select 1 union all 
			select 1 union all select 1 union all 
			select 1 union all select 1 
			),
		n100(n) as (
			select 1 from n10 a, n10 b
			),
		n10000(n) as (
			select 1 from n100 a, n100 b
			),
		tally(n) as (
			-- limit the tally table to the exact length of the input string
			select top(len(@input)) row_number() over(order by @@spid) as n from n10000 order by n
			)

		-- Now the query that does the "work"
		select 
			   -- in Code 128, the "table" is denoted by one of three START codes
			   case @table 
				when 'A' then char(203) 
				when 'B' then char(204) 
				when 'C' then char(205) 
			   end -- START

			 + @input -- Original string

			   -- The code 128 Checksum is made up by taking the sum of each 
			   -- of the input string's characters numbers (ASCII - 32)
			   -- multiplied by it's position in the string , plus the character
			   -- number of the START character, then taking MODULUS 103
			   -- This number is converted to ASCII by adding 32

			 + char(case when (sum(n*(ascii(substring(@input,n,1))-32))+case @table when 'A' then 103 when 'B' then 104 when 'C' then 105 end)%103>94 then 100
						 else 32
					end+(sum(n*(ascii(substring(@input,n,1))-32))+case @table when 'A' then 103 when 'B' then 104 when 'C' then 105 end)%103) 

			   -- The STOP character never changes and denotes the end of the barcode.

			 + CHAR(206) -- STOP

			   -- we need an alias for the column
			   AS encoded_for_128
		from tally
	)