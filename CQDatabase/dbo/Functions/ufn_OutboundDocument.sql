﻿CREATE FUNCTION [dbo].[ufn_OutboundDocument] (@OutboundDocumentId int = null)
RETURNS TABLE
AS
RETURN 
(
    select OutboundDocumentId,
           OrderNumber as 'OutboundDocument'
      from OutboundDocument
     where OutboundDocumentId = isnull(@OutboundDocumentId, OutboundDocumentId)
);
