﻿CREATE FUNCTION [dbo].[ufn_StorageUnitBatch] (@StorageUnitBatchId int = null)
RETURNS TABLE
AS
RETURN 
(
    select sub.StorageUnitBatchId, p.ProductCode + ', ' + sku.SKUCode as 'StorageUnitBatch'
      from dbo.StorageUnitBatch sub (nolock)
      join dbo.Batch            b   (nolock) on sub.BatchId = b.BatchId
      join dbo.StorageUnit      su  (nolock) on sub.StorageUnitId = su.StorageUnitId
      join dbo.SKU              sku (nolock) on su.SKUId = sku.SKUId
      join dbo.Product          p   (nolock) on su.ProductId = p.ProductId
     where sub.StorageUnitBatchId = isnull(@StorageUnitBatchId, sub.StorageUnitBatchId)
);
