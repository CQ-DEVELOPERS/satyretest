﻿CREATE FUNCTION [dbo].[ufn_Receipt] (@ReceiptId int = null)
RETURNS TABLE
AS
RETURN 
(
    select ReceiptId,
           DeliveryNoteNumber as 'Receipt'
      from Receipt
     where ReceiptId = isnull(@ReceiptId, ReceiptId)
);
