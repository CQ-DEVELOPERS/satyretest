<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="InboundDocumentUpload.aspx.cs" Inherits="Housekeeping_InboundDocumentUpload"
    Title="<%$ Resources:Default, ReportTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabDownload" HeaderText="<%$ Resources:Default, Upload%>">
            <ContentTemplate>
                <asp:Label ID="LabelFileUpload" runat="server" Text="Click Browse to find the file to upload:"></asp:Label>
                <asp:UpdatePanel ID="UpdatePanelFileUpload1" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="500" ToolTip="Click Browse to find the file to upload"/>
                            </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <br />
                                    <asp:Button ID="ButtonUpload" runat="server" Text="Save" OnClick="ButtonUpload_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <triggers>
                        <asp:postbacktrigger ControlID="ButtonUpload" />
                    </triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Download%>">
            <ContentTemplate>
                <asp:Label ID="LabelPrint" runat="server" Text="View and download current Inbound master:"></asp:Label>
                <asp:Button ID="ButtonPrint" runat="server" Text="View Master" OnClick="ButtonPrint_Click" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>

