<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="MovementMaintenance.aspx.cs" Inherits="Housekeeping_MovementMaintenance"
    Title="<%$ Resources:Default, MovementTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, MovementTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, MovementAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:DropDownList ID="DropDownListInstructionType" runat="server" DataSourceID="ObjectDataSourceInstructionType"
        DataTextField="InstructionType" DataValueField="InstructionTypeId">
    </asp:DropDownList>
    <asp:ObjectDataSource ID="ObjectDataSourceInstructionType" runat="server" TypeName="InstructionType"
        SelectMethod="GetInstructionTypesByCode">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:Parameter Name="InstructionTypeCode" DefaultValue="M" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    &nbsp;&nbsp;<telerik:RadButton ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>"
        OnClick="ButtonSearch_Click" />
    <br />
    <br />
    <telerik:RadButton ID="ButtonSelect" runat="server" Text="<%$ Resources:Default, SelectAll%>"
        OnClick="ButtonSelect_Click" />
    <telerik:RadButton ID="ButtonCreateMovement" runat="server" Text="<%$ Resources:Default, CreateMovement%>" OnClick="ButtonCreateMovement_Click" Visible="false"></telerik:RadButton>
    <telerik:RadButton ID="ButtonElectronic" runat="server" Text="<%$ Resources:Default, ElectronicRelease%>" OnClick="ButtonElectronic_Click"></telerik:RadButton>
    <telerik:RadButton ID="ButtonPauseJob" runat="server" OnClick="ButtonPauseJob_Click" Text="<%$ Resources:Default, ButtonPause%>"></telerik:RadButton>
    <telerik:RadButton ID="ButtonCheckSheet" runat="server" Text="<%$ Resources:Default, CheckSheet%>" OnClick="ButtonCheckSheet_Click" Visible="false"></telerik:RadButton>
    <telerik:RadButton ID="ButtonLabel" runat="server" Text="<%$ Resources:Default, Label%>" OnClick="ButtonLabel_Click"></telerik:RadButton>
    <telerik:RadButton ID="ButtonSerialNumbers" runat="server" Text="<%$ Resources:Default, SerialNumbers%>" OnClick="ButtonSerialNumbers_Click" Visible="false"></telerik:RadButton>
    <telerik:RadButton ID="ButtonReset" runat="server" Text="<%$ Resources:Default, ResetStatus%>" OnClick="ButtonReset_Click"></telerik:RadButton>
    <telerik:RadButton ID="ButtonManualLocations" runat="server" Text="<%$ Resources:Default, ChangeLocation%>" OnClick="ButtonManualLocations_Click"></telerik:RadButton>
    <telerik:RadButton ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, Delete%>" OnClick="ButtonDelete_Click"></telerik:RadButton>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonDelete" runat="server" TargetControlID="ButtonDelete" ConfirmText="<%$ Resources:Default, ConfirmButtonDelete %>">
    </ajaxToolkit:ConfirmButtonExtender>
    <telerik:RadButton ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, Edit%>" OnClick="ButtonEdit_Click"></telerik:RadButton>
    <telerik:RadButton ID="ButtonSave" runat="server" Text="<%$ Resources:Default, Save%>" OnClick="ButtonSave_Click"></telerik:RadButton>
    <br />
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
        <ContentTemplate>
            <asp:GridView ID="GridViewInstruction" runat="server" AllowPaging="True" PageSize="30" AllowSorting="true"
                AutoGenerateColumns="False" DataKeyNames="InstructionId,JobId" DataSourceID="ObjectDataSourceInstruction"
                OnRowUpdating="GridViewInstruction_OnRowUpdating">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="JobId" ReadOnly="True" HeaderText="<%$ Resources:Default, JobId %>"
                        SortExpression="JobId">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <%--<asp:BoundField DataField="ReferenceNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, ReferenceNumber %>"
                        SortExpression="ReferenceNumber">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>--%>
                    <asp:BoundField DataField="Operator" ReadOnly="True" HeaderText="<%$ Resources:Default, Operator %>"
                        SortExpression="Operator">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                        <EditItemTemplate>
                            <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Wrap="False" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>"
                        SortExpression="Status">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="PalletId" ReadOnly="True" HeaderText="<%$ Resources:Default, PalletId %>"
                        SortExpression="PalletId">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>"
                        SortExpression="ProductCode">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>"
                        SortExpression="Product">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>"
                        SortExpression="SKUCode">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Batch" ReadOnly="True" HeaderText="<%$ Resources:Default, Batch %>"
                        SortExpression="Batch">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                        SortExpression="Quantity">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="PickLocation" ReadOnly="True" HeaderText="<%$ Resources:Default, PickLocation %>"
                        SortExpression="PickLocation">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="StoreLocation" ReadOnly="True" HeaderText="<%$ Resources:Default, StoreLocation %>"
                        SortExpression="StoreLocation">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="CreateDate" ReadOnly="True" HeaderText="<%$ Resources:Default, CreateDate %>"
                        SortExpression="CreateDate">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ECLNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, ECLNumber %>"
                        SortExpression="ECLNumber">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="ManualPalletise"
                SelectMethod="GetMovementByInstructionType" UpdateMethod="UpdateMovements" OnUpdating="ObjectDataSourceInstruction_OnUpdating"
                DeleteMethod="DeleteMovements">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="String" />
                    <asp:ControlParameter Name="InstructionTypeId" Type="Int32" ControlID="DropDownListInstructionType" PropertyName="SelectedValue" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="priorityId" Type="Int32" />
                    <asp:Parameter Name="quantity" Type="Decimal" />
                </UpdateParameters>
                <DeleteParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="InstructionId" Type="Int32" />
                    <asp:Parameter Name="JobId" Type="Int32" />
                    <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
                </DeleteParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" TypeName="Priority"
                SelectMethod="GetPriorities">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonElectronic" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonLabel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonPauseJob" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
