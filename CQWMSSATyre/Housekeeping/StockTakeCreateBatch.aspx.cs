using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_StockTakeCreateBatch : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateProduct" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";

        try
        {
            GridViewProductSearch.DataBind();
            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateProduct" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSearch_Click"
    
    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewProductSearch.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            GetProducts();

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateProduct" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region ButtonDeselect_Click
    protected void ButtonDeselect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselect_Click";
        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewProductSearch.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = false;
            }

            Session["ProductList"] = null;

            Master.MsgText = "Deselect"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateProduct" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselect_Click

    #region GetProducts
    protected void GetProducts()
    {
        theErrMethod = "GetProducts";
        try
        {
            ArrayList productList = new ArrayList();
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewProductSearch.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    productList.Add(GridViewProductSearch.DataKeys[row.RowIndex].Values["StorageUnitBatchId"]);
            }

            if (productList.Count > 0)
                Session["ProductList"] = productList;
            else
                Session["ProductList"] = null;

            Master.MsgText = "Get Product List"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateProduct" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GetProducts

    #region ButtonSelectLocation_Click
    protected void ButtonSelectLocation_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectLocation_Click";
        try
        {
            ArrayList locationList = new ArrayList();

            foreach (ListItem item in CheckBoxListLocation.Items)
            {
                item.Selected = true;
                locationList.Add(item.Value);
            }

            Session["LocationList"] = locationList;

            Master.MsgText = "Select Location"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateProduct" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectLocation_Click

    #region ButtonDeselectLocation_Click
    protected void ButtonDeselectLocation_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectLocation_Click";
        try
        {
            foreach (ListItem item in CheckBoxListLocation.Items)
            {
                item.Selected = false;
            }

            Session["LocationList"] = null;

            Master.MsgText = "Deselect Location"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateProduct" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectLocation_Click

    #region Wizard1_NextButtonClick
    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
        theErrMethod = "Wizard1_NextButtonClick";
        try
        {
            //if (Session["ProductList"] == null)
                GetProducts();

            if (Wizard1.ActiveStepIndex == 0 || Wizard1.ActiveStepIndex == 1)
            {
                if (Wizard1.ActiveStepIndex == 1)
                {
                    StockTake stockTake = new StockTake();
                    string jobId;
                    string referenceNumber;

                    if (Session["ReferenceNumber"] == null)
                        referenceNumber = "-1";
                    else
                        referenceNumber = Session["ReferenceNumber"].ToString();

                    jobId = stockTake.CreateStockTakeJob(Session["ConnectionStringName"].ToString(),
                                                            int.Parse(Session["WarehouseId"].ToString()),
                                                            int.Parse(Session["OperatorId"].ToString()),
                                                            "STL",
                                                            referenceNumber,
                                                            int.Parse(Session["StockTakeReferenceId"].ToString())).ToString();

                    if (jobId == "-1")
                        return;
                    else
                        Session["JobId"] = jobId;

                    if (Session["ReferenceNumber"] == null)
                        Session["ReferenceNumber"] = "Job " + jobId;

                    foreach (ListItem item in CheckBoxListLocation.Items)
                    {
                        if (item.Selected)
                            if (!stockTake.CreateStockTakeBatch(Session["ConnectionStringName"].ToString(),
                                                                    int.Parse(Session["WarehouseId"].ToString()),
                                                                    int.Parse(Session["OperatorId"].ToString()),
                                                                    int.Parse(Session["JobId"].ToString()),
                                                                    int.Parse(item.Value),
                                                                    int.Parse(Session["StorageUnitBatchId"].ToString()),
                                                                    int.Parse(Session["StockTakeReferenceId"].ToString())))
                            {
                                LabelErrorMsg.Text = "Create Stock Take Failed.";
                                break;
                            }
                    }
                }

                ArrayList productList = (ArrayList)Session["ProductList"];

                foreach (int storageUnitBatchId in productList)
                {
                    Session["StorageUnitBatchId"] = storageUnitBatchId;
                    productList.Remove(storageUnitBatchId);
                    CheckBoxListLocation.DataBind();

                    if (CheckBoxListLocation.Items.Count < 1)
                        Wizard1.ActiveStepIndex = 0;

                    break;
                }

                GridView1.DataBind();
            }

            Master.MsgText = "Next"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateProduct" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Wizard1_NextButtonClick

    #region Wizard1_FinishButtonClick
    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        theErrMethod = "Wizard1_FinishButtonClick";
        try
        {
            Session.Remove("ReferenceNumber");
            Response.Redirect("StockTakeMaintenance.aspx");

        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateProduct" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Wizard1_FinishButtonClic

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_StockTakeCreateProduct", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_StockTakeCreateProduct", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling       

}
