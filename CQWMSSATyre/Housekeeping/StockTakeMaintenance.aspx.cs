using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_StockTakeMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {

            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }

        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewSearch.DataBind();


            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSearch_Click"

    #region ButtonRelease_Click
    protected void ButtonRelease_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRelease_Click";
        try
        {
            StatusChange(-1, "RL");
            GridViewSearch.DataBind();
            
            Master.MsgText = "Release"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonRelease_Click"

    #region ButtonCancel_Click
    protected void ButtonCancel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRelease_Click";
        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            StockTake stockTake = new StockTake();

            while (index < GridViewSearch.Rows.Count)
            {
                GridViewRow checkedRow = GridViewSearch.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    if (!stockTake.Cancel(  Session["ConnectionStringName"].ToString(),
                                            int.Parse(GridViewSearch.DataKeys[index].Values["JobId"].ToString()),
                                            (int)Session["OperatorId"]))
                        return;
                }

                index++;
            }
            
            GridViewSearch.DataBind();

            Master.MsgText = "Cancel"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonCancel_Click"

    #region ButtonOperatorRelease_Click
    protected void ButtonOperatorRelease_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonOperatorRelease_Click";
        try
        {
            int operatorId = -1;

            if (int.TryParse(DropDownListOperator.SelectedValue, out operatorId))
                StatusChange(operatorId, "RL");

            GridViewSearch.DataBind();


            Master.MsgText = "Release"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonOperatorRelease_Click"

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";
        try
        {
            Session["FromURL"] = "~/Housekeeping/StockTakeMaintenance.aspx";

            Session["ReportName"] = "Count Sheet";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[4];

            // Create the ConnectionString report parameter
            string strReportConnectionString = Session["ReportConnectionString"].ToString();
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ConnectionString", strReportConnectionString);
          


            // Create the JobId report parameter
            string jobId = Session["JobId"].ToString();
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("JobId", jobId);

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

       

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPrint_Click

    #region GridViewSearch_SelectedIndexChanged
    protected void GridViewSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewSearch_SelectedIndexChanged";
        try
        {
            if (GridViewSearch.SelectedIndex == -1)
                Session["JobId"] = -1;
            else
            {
                Session["JobId"] = GridViewSearch.SelectedDataKey["JobId"];
                ChangeTab(2);
            }


            Master.MsgText = "Select Changed"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewSearch_SelectedIndexChanged"

    #region GridViewUpdate_OnRowEditing
    protected void GridViewUpdate_OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        theErrMethod = "GridViewUpdate_SelectedIndexChanged";
        try
        {
            GridViewUpdate.EditIndex = e.NewEditIndex;

            if (GridViewUpdate.EditIndex == -1)
                Session["InstructionId"] = -1;
            else
            {
                Session["InstructionId"] = GridViewUpdate.DataKeys[e.NewEditIndex].Values["InstructionId"];

                Session["checkedList"] = null;

                ArrayList checkedList = new ArrayList();

                checkedList.Add(GridViewUpdate.DataKeys[e.NewEditIndex].Values["InstructionId"]);

                if (checkedList.Count > 0)
                    Session["checkedList"] = checkedList;
            }

            Master.MsgText = "Edit Changed " + Session["InstructionId"].ToString(); Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewUpdate_OnRowEditing"

    #region GridViewUpdate_Updated
    protected void GridViewUpdate_Updated(object sender, GridViewUpdatedEventArgs e)
    {
        theErrMethod = "GridViewUpdate_Updated";
        try
        {
            GridViewUpdate.DataBind();
            
            if (GridViewUpdate.Rows.Count == 0)
            {
                GridViewSearch.SelectedIndex = -1;
                GridViewSearch.DataBind();
                ChangeTab(0);
            }

            Master.MsgText = "Edit Changed " + Session["InstructionId"].ToString(); Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewUpdate_Updated"

    #region StatusChange
    protected void StatusChange(int operatorId, string statusCode)
    {
        theErrMethod = "StatusChange";
        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            StockTake stockTake = new StockTake();

            while (index < GridViewSearch.Rows.Count)
            {
                GridViewRow checkedRow = GridViewSearch.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    if (!stockTake.Release(Session["ConnectionStringName"].ToString(), int.Parse(GridViewSearch.DataKeys[index].Values["JobId"].ToString()),
                                     operatorId,
                                     statusCode))
                        return;
                }

                index++;
            }


            Master.MsgText = "Status Change"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "StatusChange"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";

        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewUpdate.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    rowList.Add(GridViewUpdate.DataKeys[row.RowIndex].Values["InstructionId"]);
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndex"

    #region SetEditIndex
    protected void SetEditIndex()
    {
        theErrMethod = "SetEditIndex";

        try
        {
            if (Session["checkedList"] != null)
            {
                ArrayList checkedList = (ArrayList)Session["checkedList"];
                string instructionId = checkedList[0].ToString();
                checkedList.Remove(checkedList[0]);

                foreach (GridViewRow row in GridViewUpdate.Rows)
                {
                    theErrMethod = row.ToString();
                    if (GridViewUpdate.DataKeys[row.RowIndex].Values["InstructionId"].ToString() == instructionId)
                    {
                        Session["InstructionId"] = int.Parse(instructionId);
                        GridViewUpdate.EditIndex = row.RowIndex;
                        break;
                    }
                }

                Session["checkedList"] = checkedList;
            }

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "SetEditIndex"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            if (Tabs.ActiveTabIndex == 0)
            {
                foreach (GridViewRow row in GridViewSearch.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    cb.Checked = true;
                }
            }
            else
            {
                foreach (GridViewRow row in GridViewUpdate.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    cb.Checked = true;
                }
            }
            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region ButtonEdit_Click
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";
       
        try
        {
            GetEditIndex();
            SetEditIndex();

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonEdit_Click

    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";
        try
        {
            GridViewUpdate.UpdateRow(GridViewUpdate.EditIndex, false);

            SetEditIndex();

            Master.MsgText = "Save"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSave_Click

    #region ButtonInsert_Click
    protected void ButtonInsert_Click(object sender, EventArgs e)
    {
        StockTake st = new StockTake();
        ArrayList checkedList = new ArrayList();
        int instructionId = -1;

        instructionId = st.ProductInsert(Session["ConnectionStringName"].ToString(),
                            (int)Session["OperatorId"],
                            (int)Session["InstructionId"],
                            (int)Session["StorageUnitBatchId"],
                            -1,
                            0);
        if (instructionId != -1)
        {
            if (Session["checkedList"] != null)
                checkedList = (ArrayList)Session["checkedList"];

            checkedList.Insert(0, instructionId);

            if (GridViewUpdate.EditIndex != -1)
            {
                checkedList.Insert(0, GridViewUpdate.DataKeys[GridViewUpdate.EditIndex].Values["InstructionId"]);
                GridViewUpdate.EditIndex = -1;
                GridViewProductSearch.SelectedIndex = -1;

                ChangeTab(2);
            }

            Session["checkedList"] = checkedList;

            //GridViewUpdate.UpdateRow(GridViewUpdate.EditIndex, false);
            GridViewUpdate.DataBind();
            SetEditIndex();
        }
    }
    #endregion ButtonInsert_Click

    #region DetailsProductInsert_Updated
    protected void DetailsProductInsert_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        ArrayList checkedList = new ArrayList();

        if (Session["checkedList"] != null)
            checkedList = (ArrayList)Session["checkedList"];

        checkedList.Insert(0, e.ReturnValue);

        if (GridViewUpdate.EditIndex != -1)
        {
            checkedList.Insert(0, GridViewUpdate.DataKeys[GridViewUpdate.EditIndex].Values["InstructionId"]);
            GridViewUpdate.EditIndex = -1;
        }

        Session["checkedList"] = checkedList;

        //GridViewUpdate.UpdateRow(GridViewUpdate.EditIndex, false);
        GridViewUpdate.DataBind();
        SetEditIndex();
    }
    #endregion DetailsProductInsert_Updated

    #region GridViewProductSearch_OnSelectedIndexChanged
    protected void GridViewProductSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["StorageUnitBatchId"] = GridViewProductSearch.SelectedDataKey["StorageUnitBatchId"];
        Session["Product"] = GridViewProductSearch.SelectedDataKey["Product"];
    }
    #endregion GridViewProductSearch_OnSelectedIndexChanged

    #region GridViewProductSearch_PageIndexChanging
    protected void GridViewProductSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewProductSearch.PageIndex = e.NewPageIndex;
            GridViewProductSearch_DataBind();
        }
        catch { }
    }
    #endregion "GridViewProductSearch_PageIndexChanging"

    #region ButtonSearchProduct_Click
    protected void ButtonSearchProduct_Click(object sender, EventArgs e)
    {
        GridViewProductSearch.PageIndex = 0;

        GridViewProductSearch_DataBind();
    }
    #endregion "ButtonSearchProduct_Click"

    #region GridViewProductSearch_DataBind
    protected void GridViewProductSearch_DataBind()
    {
        Product product = new Product();

        GridViewProductSearch.DataSource = product.SearchProductsStorageUnitBatch(Session["ConnectionStringName"].ToString(), TextBoxProductCode.Text, TextBoxProduct.Text, TextBoxSKUCode.Text, TextBoxSKU.Text, TextBoxBatch.Text, TextBoxECLNumber.Text);

        GridViewProductSearch.DataBind();
    }
    #endregion GridViewProductSearch_DataBind

    #region ChangeTab
    protected void ChangeTab(int tabIndex)
    {
        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(" + tabIndex.ToString() + ");";
        ScriptManager.RegisterClientScriptBlock(UpdatePanelGridViewUpdate, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }
    #endregion ChangeTab

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_StockTakeMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_StockTakeMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
