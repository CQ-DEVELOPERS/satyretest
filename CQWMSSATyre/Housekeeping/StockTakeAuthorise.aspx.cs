using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_StockTakeAuthorise : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region ButtomRefresh_Click
    protected void ButtomRefresh_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtomRefresh_Click";

        try
        {
            GridViewAuthorise.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtomRefresh_Click

    #region ButtomAccept_Click
    protected void ButtomAccept_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtomAccept_Click";

        try
        {
            RadioButtonList rbl = new RadioButtonList();

            foreach (GridViewRow row in GridViewAuthorise.Rows)
            {
                rbl = (RadioButtonList)row.FindControl("rbAccept");

                rbl.SelectedValue = "A";
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtomAccept_Click"

    #region ButtonRecount_Click
    protected void ButtonRecount_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRecount_Click";

        try
        {

            RadioButtonList rbl = new RadioButtonList();

            foreach (GridViewRow row in GridViewAuthorise.Rows)
            {
                rbl = (RadioButtonList)row.FindControl("rbAccept");

                rbl.SelectedValue = "RC";
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonRecount_Click"

    #region ButtomSave_Click
    protected void ButtomSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtomAccept_Click";

        try
        {
            StockTake stockTake = new StockTake();
            RadioButtonList rbl = new RadioButtonList();
            String statusCode = "";

            foreach (GridViewRow row in GridViewAuthorise.Rows)
            {
                rbl = (RadioButtonList)row.FindControl("rbAccept");

                statusCode = rbl.SelectedValue;

                if (statusCode == "A" || statusCode == "RC")
                    if (!stockTake.Authorise(Session["ConnectionStringName"].ToString(),
                                                int.Parse(GridViewAuthorise.DataKeys[row.RowIndex].Values["InstructionId"].ToString()),
                                                 int.Parse(Session["OperatorId"].ToString()),
                                                 statusCode))
                        break;
            }

            GridViewAuthorise.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtomSave_Click"

    #region StatusChange
    protected void StatusChange(string statusCode)
    {
        theErrMethod = "StatusChange";

        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            StockTake stockTake = new StockTake();

            while (index < GridViewAuthorise.Rows.Count)
            {
                GridViewRow checkedRow = GridViewAuthorise.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    if (!stockTake.Authorise(Session["ConnectionStringName"].ToString(),
                                                int.Parse(GridViewAuthorise.DataKeys[index].Values["InstructionId"].ToString()),
                                                 int.Parse(Session["OperatorId"].ToString()),
                                                 statusCode))
                        return;
                }

                index++;
            }


            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "StatusChange"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_StockTakeAuthorise", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_StockTakeAuthorise", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
