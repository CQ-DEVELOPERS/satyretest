﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="StockTakeGroupLocation.aspx.cs" Inherits="Housekeeping_StockTakeGroupLocation"
    Title="<%$ Resources:Default, StockTakeReferenceTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeCreateTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Link">
            <HeaderTemplate>
                Link
            </HeaderTemplate>
            <ContentTemplate>
                <table border="solid" cellpadding="5">
                    <tr>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text="Linked Locations" Font-Bold="True"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label7" runat="server" Text="Locations" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel ID="updatePanel_StockTake" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="" />
                                    <asp:GridView ID="GridViewStockTake" DataSourceID="ObjectDataSourceStockTakeGroupLoc"
                                        DataKeyNames="StockTakeGroupLocId" runat="server" AllowPaging="True" AllowSorting="True"
                                        PageSize="15" AutoGenerateColumns="False" EmptyDataText="There are currently no Stock Take Groups..."
                                        OnSelectedIndexChanged="GridViewStockTake_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="False" ShowEditButton="False" ShowDeleteButton="True" />
                                            <asp:BoundField HeaderText="StockTakeGroupLocId" DataField="StockTakeGroupLocId"
                                                SortExpression="StockTakeGroupLocId" ReadOnly="True" Visible="False" />
                                            <asp:BoundField HeaderText="StockTakeGroupId" DataField="StockTakeGroupId" SortExpression="StockTakeGroupId"
                                                ReadOnly="True" Visible="False" />
                                            <asp:BoundField HeaderText="StockTakeGroup" DataField="StockTakeGroup" SortExpression="StockTakeGroup" />
                                            <asp:BoundField HeaderText="LocationId" DataField="LocationId" SortExpression="LocationId"
                                                Visible="False" />
                                            <asp:BoundField HeaderText="Location" DataField="Location" SortExpression="Location" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:DetailsView Visible="false" ID="DetailsViewInsertStockTakeGroupLoc" runat="server"
                                        AutoGenerateRows="False" DataSourceID="ObjectDataSourceStockTakeGroupLoc" DefaultMode="Insert"
                                        OnLoad="DetailsViewInsertStockTakeGroupLoc_Load">
                                        <Fields>
                                            <asp:TemplateField HeaderText="StockTakeGroupLocId" InsertVisible="False" SortExpression="StockTakeGroupLocId">
                                                <InsertItemTemplate>
                                                    <asp:Button ID="AddStockTakeGroupLoc" runat="server" CommandName="Insert" Text="Add" />
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockTakeGroupId">
                                                <InsertItemTemplate>
                                                    <asp:TextBox ID="DetailTextStockTakeGroupId" runat="server" Text='<%# Bind("StockTakeGroupId") %>'>
                                                    </asp:TextBox>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="LocationId">
                                                <InsertItemTemplate>
                                                    <asp:DropDownList ID="DropDownListLocationIdInsert" runat="server" DataSourceID="ObjectDataSourceLocation"
                                                        DataTextField="LocationId" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                                    </asp:DropDownList>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <InsertItemTemplate>
                                                    <asp:LinkButton ID="btnInsert" runat="server" CommandName="Insert" Text="Insert">
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                                        Text="Cancel" OnClick="btnCancel_Click">
                                                    </asp:LinkButton>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceStockTakeGroupLoc" runat="server" TypeName="StockTake"
                                        SelectMethod="StockTakeGroupLoc_Search" InsertMethod="StockTakeGroupLoc_Add"
                                        DeleteMethod="StockTakeGroupLoc_Delete" OnInserting="ObjectDataSourceStockTakeGroupLoc_Inserting"
                                        OnInserted="ObjectDataSourceStockTakeGroupLoc_Inserted">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="StockTakeGroupId" SessionField="StockTakeGroupId" Type="Int32" />
                                        </SelectParameters>
                                        <UpdateParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:Parameter Name="StockTakeGroupId" Type="Int32" />
                                            <asp:Parameter Name="LocationId" Type="Int32" />
                                        </UpdateParameters>
                                        <InsertParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:Parameter Name="StockTakeGroupId" Type="Int32" />
                                            <asp:Parameter Name="LocationId" Type="Int32" />
                                        </InsertParameters>
                                        <DeleteParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                        </DeleteParameters>
                                    </asp:ObjectDataSource>
                                    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" TypeName="Location" SelectMethod="ListLocations">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <div style="clear: left;">
                            </div>
                            <br />
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:CheckBoxList ID="CheckBoxListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                        DataTextField="Location" DataValueField="LocationId" RepeatColumns="10">
                                    </asp:CheckBoxList>
                                    <asp:Label ID="LabelErrorMsg" runat="server" Text="">
                                    </asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonSelectLocation" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ButtonDeselectLocation" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                                SelectMethod="SearchLocationByAisleLevel">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                    <asp:SessionParameter Name="location" SessionField="Location" Type="String" />
                                    <asp:SessionParameter Name="stockTakeGroupId" SessionField="StockTakeGroupId" Type="Int32" />
                                    <asp:SessionParameter Name="FromAisle" SessionField="FromAisle" Type="String" />
                                    <asp:SessionParameter Name="ToAisle" SessionField="ToAisle" Type="String" />
                                    <asp:SessionParameter Name="FromLevel" SessionField="FromLevel" Type="String" />
                                    <asp:SessionParameter Name="ToLevel" SessionField="ToLevel" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:Button ID="ButtonSelectLocation" runat="server" Text="Select All" OnClick="ButtonSelectLocation_Click" />
                            <asp:Button ID="ButtonDeselectLocation" runat="server" Text="Deselect All" OnClick="ButtonDeselectLocation_Click" />
                            <asp:Button ID="ButtonLinkLocation" runat="server" Text="Link Locations" OnClick="ButtonLinkLocation_Click" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
