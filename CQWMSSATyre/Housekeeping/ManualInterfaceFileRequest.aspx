﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPage.master"
    StylesheetTheme="Default" AutoEventWireup="true" CodeFile="ManualInterfaceFileRequest.aspx.cs"
    Inherits="Housekeeping_ManualInterfaceFileRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="Manual File Request"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Missing Interface Files Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelManual" runat="server">
                <table>
                    <tr valign="top">
                        <td>
                            <asp:RadioButtonList ID="RadioButtonListFiles" runat="server" DataSourceID="ObjectDSFileTypes"
                                DataTextField="Description" DataValueField="ManualFileRequestTypeId" OnSelectedIndexChanged="RadioButtonListFiles_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:RadioButtonList>
                            <asp:ObjectDataSource ID="ObjectDSFileTypes" runat="server" SelectMethod="ManualFileTypeSearch"
                                TypeName="Housekeeping">
                                <SelectParameters>
                                    <asp:SessionParameter DefaultValue="" Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                        <td>
                            <asp:GridView ID="GridViewFileRequest" runat="server" ShowHeader="False" DataSourceID="ObjectDSFileColumns"
                                AutoGenerateColumns="False" DataKeyNames="ColumnName" onrowdatabound="GridViewFileRequest_RowDataBound" 
                                >
                                <Columns>
                                    <asp:BoundField DataField="ColumnDescription" HeaderText="Column" ReadOnly="True" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtValue" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorValue" runat="server" ErrorMessage="*" ControlToValidate="txtValue"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDSFileColumns" runat="server" SelectMethod="ManualFileColumnSearch"
                                TypeName="Housekeeping">
                                <SelectParameters>
                                    <asp:SessionParameter DefaultValue="" Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:ControlParameter ControlID="RadioButtonListFiles" Name="ManualFileRequestTypeId"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" /><br />
			    <asp:Label ID="LabelSubmit" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                TargetControlID="PanelManual" Radius="5" Color="239, 239, 236" BorderColor="64, 64, 64"
                Enabled="True" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
