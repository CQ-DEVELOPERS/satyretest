using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.Services.Protocols;

public partial class Housekeeping_TransferUpload : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion Page_Load

    #region ButtonUpload_Click
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        Master.MsgText = "";
        Master.ErrorText = "";

        if (FileUpload1.HasFile)
        {
            try
            {
                string xml = "<root>";
                System.IO.Stream s = FileUpload1.PostedFile.InputStream;
                using (StreamReader sr = new StreamReader(s))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] tmpArr = line.Split(Convert.ToChar(","));
                        if (tmpArr.Length >= 4)
                        {
                            string FromWarehouseCode = tmpArr[0];
                            string ProductCode = tmpArr[1];
                            string ToWarehouseCode = tmpArr[2];
                            string Quantity = tmpArr[3];
                            if ((FromWarehouseCode.Length > 0) && (ProductCode.Length > 0) && (ToWarehouseCode.Length > 0) && (Quantity.Length > 0))
                            {
                                xml = xml + "<Line>";
                                xml = xml + "<FromWarehouseCode>" + FromWarehouseCode + "</FromWarehouseCode>";
                                xml = xml + "<ProductCode>" + ProductCode + "</ProductCode>";
                                xml = xml + "<ToWarehouseCode>" + ToWarehouseCode + "</ToWarehouseCode>";
                                xml = xml + "<Quantity>" + Quantity + "</Quantity>";
                                xml = xml + "</Line>";
                            };
                        }
                        else
                        {
                            //Master.ErrorText = "Length:" + tmpArr.Length.ToString();
                        }
                    }
                }
                xml = xml + "</root>";
                s.Close();
                BulkImport import = new BulkImport();

                if (!import.ImportTransfers(Session["ConnectionStringName"].ToString(), xml))
                {
                    Master.ErrorText = "There was an processing the file : " + Server.MapPath(FileUpload1.FileName).ToString();
                }
                else
                {
                    Master.MsgText = "The file was successfully imported.";
                }
            }
            catch (Exception ex)
            {
                Master.ErrorText = "There was an error saving the file : " + ex.Message.ToString();
            }
        }
        else
        {
            Master.ErrorText = "There was no file selected to upload";
        }
    }
    #endregion ButtonUpload_Click
}
