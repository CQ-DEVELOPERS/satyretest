<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Housekeeping_Default" Title="<%$ Resources:Default, DefaultHousekeepingTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultHousekeepingTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultHousekeepingAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <table>
        <tr valign="top">
            <td>
                <telerik:RadButton ID="RadButtonExceptionStockTake" runat="server" Text="Exception Stock Take" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChartExceptionStockTake" runat="server" ChartTitle-TextBlock-Text="Exception Stock Takes" Skin="DeepBlue"
                    DataGroupColumn="Type" DefaultType="Bar" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourceExceptionStockTake" SeriesOrientation="Horizontal" Width="500px">
                    <PlotArea>
                        <XAxis DataLabelsColumn="Area" >
                        </XAxis>
                    </PlotArea>
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourceExceptionStockTake" runat="server" TypeName="Dashboard" SelectMethod="ExceptionStockTake">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonReplenishmentTime" runat="server" Text="ReplenishmentTime" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChartReplenishmentTime" runat="server" ChartTitle-TextBlock-Text="Replenishment Time" Skin="DeepBlue"
                    DataGroupColumn="Type" DefaultType="Bar" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourceReplenishmentTime" SeriesOrientation="Horizontal" Width="500px">
                    <PlotArea>
                        <XAxis DataLabelsColumn="Area">
                        </XAxis>
                    </PlotArea>
                </telerik:RadChart>
                <asp:ObjectDataSource ID="ObjectDataSourceReplenishmentTime" runat="server" TypeName="Dashboard" SelectMethod="ReplenishmentTime">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <telerik:RadButton ID="RadButtonUtilisation" runat="server" Text="Space Utilisation" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChartUtilisation" runat="server" ChartTitle-TextBlock-Text="Space Utilisation" Skin="DeepBlue"
                    DataGroupColumn="Legend" DefaultType="StackedBar100" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourceSpaceUtilisation" Width="500px">
                    <PlotArea>
                        <XAxis DataLabelsColumn="Area">
                        </XAxis>
                    </PlotArea>
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourceSpaceUtilisation" runat="server" TypeName="Dashboard" SelectMethod="SpaceUtilisation">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonReplenishmentWaiting" runat="server" Text="Replenishments Waiting" Skin="Default" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridReplenishmentWaiting" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceReplenishmentWaiting" Skin="Default" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceReplenishmentWaiting">
                        <Columns>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Area" HeaderText="<%$ Resources:Default, Area %>"
                                SortExpression="Area" UniqueName="Area">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Count" HeaderText="Replenishments Waiting"
                                SortExpression="Count" UniqueName="Count">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="KPI" HeaderText="<%$ Resources:Default, KPI %>"
                                SortExpression="KPI" UniqueName="KPI">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceReplenishmentWaiting" runat="server" TypeName="Dashboard" SelectMethod="ReplenishmentWaiting">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
