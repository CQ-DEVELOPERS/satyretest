<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="Movement.aspx.cs" Inherits="Housekeeping_Movement" Title="<%$ Resources:Default, MovementTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">

    <script type="text/javascript" language="JavaScript">

        function openLocationWindow() {
            window.open("../StaticInfo/BatchMaintenance.aspx", "_blank",
      "height=600px width=450px top=200 left=200 resizable=no scrollbars=no ");
        }


    </script>

    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, MovementTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, HousekeepingModule%>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, SearchforStock%>">
            <ContentTemplate>
                <asp:Panel ID="PanelSearch" runat="server" Width="450px" BackColor="#EFEFEC">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="LabelProductCode" runat="server" Text='<%$ Resources:Default,ProductCode %>'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="LabelProduct" runat="server" Text='<%$ Resources:Default,Product %>'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelSKUCode" runat="server" Text='<%$ Resources:Default,SKUCode %>'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxSKUCode" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="LabelBatch" runat="server" Text='<%$ Resources:Default,Batch %>'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelPickLocation" runat="server" Text='<%$ Resources:Default,PickLocation %>'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxPickLocation" runat="server"></asp:TextBox>
                            </td>
                            <td colspan="2" align="right">
                                <asp:Button ID="ButtonSearch" runat="server" Text='<%$ Resources:Default,Search %>'
                                    OnClick="ButtonSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                    TargetControlID="PanelSearch" Radius="10" Color="#EFEFEC" BorderColor="#404040"
                    Corners="All" />
                <br />
                <asp:UpdatePanel ID="UpdatePanelSearch" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="ButtonManualLocations" OnClick="ButtonManualLocations_Click" runat="server"
                            Text="<%$ Resources:Default, ManualAllocation%>" style="Width:auto;"></asp:Button>
                        <asp:Button ID="ButtonSelect" runat="server" Text='<%$ Resources:Default,ButtonSelectAll %>'
                            OnClick="ButtonSelect_Click" />
                        <asp:Button ID="ButtonMoveAll" runat="server" Text='<%$ Resources:Default,MoveAll %>'
                            OnClick="MoveAll" />
                        <asp:GridView ID="GridViewSearch" runat="server" DataKeyNames="StorageUnitBatchId,PickLocationId"
                            AutoGenerateColumns="false" AllowPaging="true" DataSourceID="ObjectDataSourceSearch">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Location" HeaderText='<%$ Resources:Default,Location %>'
                                    SortExpression="Location" />
                                <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>'
                                    SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>'
                                    SortExpression="Product">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ProductStatus" HeaderText='<%$ Resources:Default,ProductStatus %>'
                                    SortExpression="ProductStatus" />
                                <asp:BoundField DataField="SKUCode" HeaderText='<%$ Resources:Default,SKUCode %>'
                                    SortExpression="SKUCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKU" HeaderText='<%$ Resources:Default,SKU %>' SortExpression="SKU">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Batch" HeaderText='<%$ Resources:Default,Batch %>' SortExpression="Batch" />
                                <asp:BoundField DataField="BatchStatus" HeaderText='<%$ Resources:Default,BatchStatus %>'
                                    SortExpression="BatchStatus" />
                                <asp:BoundField DataField="ActualQuantity" HeaderText='<%$ Resources:Default,ActualQuantity %>'
                                    SortExpression="ActualQuantity" />
                                <asp:BoundField DataField="AllocatedQuantity" HeaderText='<%$ Resources:Default,AllocatedQuantity %>'
                                    SortExpression="AllocatedQuantity" />
                                <asp:BoundField DataField="ReservedQuantity" HeaderText='<%$ Resources:Default,ReservedQuantity %>'
                                    SortExpression="ReservedQuantity" />
                                <asp:BoundField DataField="StocktakeInd" HeaderText='<%$ Resources:Default,StocktakeInd %>'
                                    SortExpression="StocktakeInd" />
                                <asp:BoundField DataField="ActiveBinning" HeaderText='<%$ Resources:Default,ActiveBinning %>'
                                    SortExpression="ActiveBinning" />
                                <asp:BoundField DataField="ActivePicking" HeaderText='<%$ Resources:Default,ActivePicking %>'
                                    SortExpression="ActivePicking" />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceSearch" runat="server" TypeName="Housekeeping"
                            SelectMethod="MovementSearch">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:QueryStringParameter Name="instructionTypeCode" Type="string" QueryStringField="Type"
                                    DefaultValue="M" />
                                <asp:ControlParameter Name="productCode" Type="string" ControlID="TextBoxProductCode"
                                    DefaultValue="%" />
                                <asp:ControlParameter Name="product" Type="string" ControlID="TextBoxProduct" DefaultValue="%" />
                                <asp:ControlParameter Name="skuCode" Type="string" ControlID="TextBoxSKUCode" DefaultValue="%" />
                                <asp:ControlParameter Name="batch" Type="string" ControlID="TextBoxBatch" DefaultValue="%" />
                                <asp:ControlParameter Name="pickLocation" Type="string" ControlID="TextBoxPickLocation"
                                    DefaultValue="%" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
