<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="ForceUpload.aspx.cs" Inherits="Housekeeping_ForceUpload" Title="<%$ Resources:Default, StockOnHandTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ForceUploadTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ForceUploadAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber%>"></asp:Label>
    <asp:TextBox ID="TextBoxOrderNumber" runat="server"></asp:TextBox>
    <asp:Button ID="ButtonSearch" runat="server" Text='<%$ Resources:Default,ButtonSearch %>' OnClick="ButtonSearch_Click" />
    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewAuthorise" runat="server" DataSourceID="ObjectDataSourceAuthorise" DataKeyNames="InterfaceExportHeaderId" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PageSize="30" OnSelectedIndexChanged="GridViewAuthorise_SelectedIndexChanged">
                <Columns>
                    <asp:CommandField SelectText="<%$ Resources:Default, Update%>" ShowSelectButton="True" />
                    <asp:BoundField DataField="RecordStatus" HeaderText='<%$ Resources:Default,RecordStatus %>' SortExpression="RecordStatus" />
                    <asp:BoundField DataField="RecordType" HeaderText='<%$ Resources:Default,RecordType %>' SortExpression="RecordType" />
                    <asp:BoundField DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>' SortExpression="OrderNumber" />
                    <asp:BoundField DataField="CompanyCode" HeaderText='<%$ Resources:Default,CompanyCode %>' SortExpression="CompanyCode" />
                    <asp:BoundField DataField="Company" HeaderText='<%$ Resources:Default,Company %>' SortExpression="Company" />
                    <asp:BoundField DataField="FromWarehouseCode" HeaderText='<%$ Resources:Default,FromWarehouseCode %>' SortExpression="FromWarehouseCode" />
                    <asp:BoundField DataField="ToWarehouseCode" HeaderText='<%$ Resources:Default,ToWarehouseCode %>' SortExpression="ToWarehouseCode" />
                    <asp:BoundField DataField="InsertDate" HeaderText='<%$ Resources:Default,InsertDate %>' SortExpression="InsertDate" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceAuthorise" runat="server" TypeName="Housekeeping" SelectMethod="ForceUploadSearch">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="String" />
                    <asp:ControlParameter Name="orderNumber" ControlID="TextBoxOrderNumber" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

