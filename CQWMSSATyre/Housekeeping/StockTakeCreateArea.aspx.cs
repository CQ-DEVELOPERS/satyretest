using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_StockTakeCreateArea : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Init";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateArea" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselect_Click";
        try
        {
            foreach (ListItem item in CheckBoxListArea.Items)
            {
                item.Selected = true;
            }

            GetAreas();

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateArea" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ButtonDeselect_Click
    protected void ButtonDeselect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselect_Click";

        try
        {
            foreach (ListItem item in CheckBoxListArea.Items)
            {
                item.Selected = false;
            }

            Session["AreaList"] = null;
            
            Master.MsgText = "Deselect"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateArea" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselect_Click

    #region GetAreas
    protected void GetAreas()
    {
        theErrMethod = "GetAreas";

        try
        {
            ArrayList areaList = new ArrayList();

            foreach (ListItem item in CheckBoxListArea.Items)
            {
                if (item.Selected)
                    areaList.Add(item.Value);
            }

            if (areaList.Count > 0)
                Session["AreaList"] = areaList;
            else
                Session["AreaList"] = null;
            
            
            Master.MsgText = "Get Area"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateArea" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GetAreas

    #region Wizard1_NextButtonClick
    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
        theErrMethod = "Wizard1_NextButtonClick";

        try
        {
            if (Wizard1.ActiveStepIndex == 0)
            {
                Session["ReferenceNumber"] = null;
                GetAreas();
            }

            if (Session["AreaList"] == null)
                return;

            if (Wizard1.ActiveStepIndex == 1)
            {
                StockTake stockTake = new StockTake();
                string jobId;
                string referenceNumber;

                if (Session["ReferenceNumber"] == null)
                    referenceNumber = "-1";
                else
                    referenceNumber = Session["ReferenceNumber"].ToString();

                jobId = stockTake.CreateStockTakeJob(Session["ConnectionStringName"].ToString(),
                                                        int.Parse(Session["WarehouseId"].ToString()),
                                                        int.Parse(Session["OperatorId"].ToString()),
                                                        "STL",
                                                        referenceNumber,
                                                        int.Parse(Session["StockTakeReferenceId"].ToString())).ToString();

                if (jobId == "-1")
                    return;
                else
                    Session["JobId"] = jobId;

                if (Session["ReferenceNumber"] == null)
                    Session["ReferenceNumber"] = "Job " + jobId;

                ArrayList areaList = (ArrayList)Session["AreaList"];

                bool areaOrAisle = RadioButtonListAreaOrAisle.Items[1].Selected;
                int split = int.Parse(DropDownListSplits.SelectedValue);
                bool columnOrLevel = RadioButtonListColumnOrLevel.Items[1].Selected;

                foreach (String areaId in areaList)
                {
                    Session["AreaId"] = int.Parse(areaId);
                    //areaList.Remove(areaId);

                    if (!stockTake.CreateStockTakeArea(Session["ConnectionStringName"].ToString(),
                                                        int.Parse(Session["WarehouseId"].ToString()),
                                                        int.Parse(Session["OperatorId"].ToString()),
                                                        int.Parse(Session["JobId"].ToString()),
                                                        int.Parse(areaId),
                                                        areaOrAisle,
                                                        split,
                                                        columnOrLevel,
                                                        int.Parse(Session["StockTakeReferenceId"].ToString())))
                    {
                        LabelErrorMsg.Text = "Create Stock Take Failed.";
                        break;
                    }
                }

                GridView1.DataBind();
            }

            Master.MsgText = "Next"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateArea" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Wizard1_NextButtonClick

    #region Wizard1_FinishButtonClick
    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        theErrMethod = "Wizard1_FinishButtonClick";

        try
        {
            Session.Remove("ReferenceNumber");
            Response.Redirect("StockTakeMaintenance.aspx");

            Master.MsgText = "Finish"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateArea" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Wizard1_FinishButtonClick

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_StockTakeCreateArea", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_StockTakeCreateArea", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
