using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.Services.Protocols;
using Microsoft.Reporting.WebForms;

public partial class Housekeeping_OutboundDocumentUpload : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";
    }
    #endregion Page_Load

    #region ButtonUpload_Click
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        Master.MsgText = "The file was successfully imported.";
        Master.ErrorText = "";

        if (FileUpload1.HasFile)
        {
            try
            {
                string xml = "";
                System.IO.Stream s = FileUpload1.PostedFile.InputStream;
                BulkImport import = new BulkImport();

                using (StreamReader sr = new StreamReader(s))
                {
                    string line;
                    //Skip the First Line
                    sr.ReadLine();

                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] tmpArr = line.Split(Convert.ToChar(","));
                        if (tmpArr.Length >= 13)
                        {
                            xml = "<root>";

                            xml = xml + "<Line>";
                            int count = 0;

                            xml = xml + "<Facility>" + tmpArr[count] + "</Facility>";
                            count++;
                            xml = xml + "<ShipmentOrderNumber>" + tmpArr[count] + "</ShipmentOrderNumber>";
                            count++;
                            xml = xml + "<OrderType>" + tmpArr[count] + "</OrderType>";
                            count++;
                            xml = xml + "<Owner>" + tmpArr[count] + "</Owner>";
                            count++;
                            xml = xml + "<ScheduledShipDate>" + tmpArr[count] + "</ScheduledShipDate>";
                            count++;
                            xml = xml + "<Consignee>" + tmpArr[count] + "</Consignee>";
                            count++;
                            xml = xml + "<ShipTo>" + tmpArr[count] + "</ShipTo>";
                            count++;
                            xml = xml + "<CustomerOrderReference>" + tmpArr[count] + "</CustomerOrderReference>";
                            count++;
                            xml = xml + "<BillOfEntry><![CDATA[" + tmpArr[count] + "]]></BillOfEntry>";

                            count++;
                            xml = xml + "<DeliveryAddress1>" + tmpArr[count] + "</DeliveryAddress1>";
                            count++;
                            xml = xml + "<DeliveryAddress2>" + tmpArr[count] + "</DeliveryAddress2>";
                            count++;
                            xml = xml + "<DeliveryAddress3>" + tmpArr[count] + "</DeliveryAddress3>";
                            count++;
                            xml = xml + "<DeliveryAddress4>" + tmpArr[count] + "</DeliveryAddress4>";
                            count++;
                            xml = xml + "<ContactPerson>" + tmpArr[count] + "</ContactPerson>";
                            count++;
                            xml = xml + "<ContactNumber>" + tmpArr[count] + "</ContactNumber>";
                            count++;
                            xml = xml + "<Reference1>" + tmpArr[count] + "</Reference1>";
                            count++;
                            xml = xml + "<Reference2>" + tmpArr[count] + "</Reference2>";
                            count++;
                            xml = xml + "<Incoterms>" + tmpArr[count] + "</Incoterms>";
                            count++;

                            xml = xml + "<OrderLineNumber>" + tmpArr[count] + "</OrderLineNumber>";
                            count++;
                            xml = xml + "<ProductCode><![CDATA[" + tmpArr[count] + "]]></ProductCode>";
                            count++;
                            xml = xml + "<Product><![CDATA[" + tmpArr[count] + "]]></Product>";
                            count++;
                            xml = xml + "<SkuCode><![CDATA[" + tmpArr[count] + "]]></SkuCode>";
                            count++;
                            xml = xml + "<Sku><![CDATA[" + tmpArr[count] + "]]></Sku>";
                            count++;
                            xml = xml + "<RequestedQuantity>" + tmpArr[count] + "</RequestedQuantity>";
                            count++;
                            xml = xml + "<RequestedUOM>" + tmpArr[count] + "</RequestedUOM>";
                            count++;
                            xml = xml + "<RequestedBatch>" + tmpArr[count] + "</RequestedBatch>";
                            count++;

                            xml = xml + "<BOELineNumber><![CDATA[" + tmpArr[count] + "]]></BOELineNumber>";
                            count++;
                            xml = xml + "<AlternativeSKU><![CDATA[" + tmpArr[count] + "]]></AlternativeSKU>";
                            count++;
                            xml = xml + "<UnitPrice><![CDATA[" + tmpArr[count] + "]]></UnitPrice>";
                            count++;
                            xml = xml + "<TariffCode><![CDATA[" + tmpArr[count] + "]]></TariffCode>";
                            count++;
                            xml = xml + "<ExpiryDate><![CDATA[" + tmpArr[count] + "]]></ExpiryDate>";
                            count++;

                            xml = xml + "</Line>";

                            xml = xml + "</root>";

                            if (!import.OutboundDocumentImport(Session["ConnectionStringName"].ToString(), xml))
                            {
                                Master.MsgText = "";
                                Master.ErrorText = "There was an processing the file : " + Server.MapPath(FileUpload1.FileName).ToString() + " " + xml.ToString();
                                break;
                            }
                        }
                    }
                }
                s.Close();
            }
            catch (Exception ex)
            {
                Master.ErrorText = "There was an error saving the file : " + ex.Message.ToString();
            }
        }
        else
        {
            Master.ErrorText = "There was no file selected to upload";
        }
    }
    #endregion ButtonUpload_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            //Read the Excel file in a byte array. here pck is the Excelworkbook              
            string pck = "Facility,Shipment Order Number,Order Type,Owner,Scheduled Ship Date,Consignee,Ship-To,Customer Order Reference,BillOfEntry,DeliveryAddress1,DeliveryAddress2,DeliveryAddress3,DeliveryAddress4,ContactPerson,ContactNumber,Reference1,Reference2,Incoterms,Order Line Number,ProductCode,Product,Sku Code,SKU Description,Requested Quantity,Requested UOM,Requested Batch,BOELineNumber,AlternativeSKU,UnitPrice,TariffCode,ExpiryDate"
                            + Environment.NewLine + ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,";

            Byte[] fileBytes = System.Text.Encoding.Unicode.GetBytes(pck);;
            //Byte[] fileBytes = pck.GetAsByteArray();

            //Clear the response               
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Cookies.Clear();
            //Add the header & other information      
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.CacheControl = "private";
            Response.Charset = System.Text.UTF8Encoding.UTF8.WebName;
            Response.ContentEncoding = System.Text.UTF8Encoding.UTF8;
            Response.AppendHeader("Content-Length", fileBytes.Length.ToString());
            Response.AppendHeader("Pragma", "cache");
            Response.AppendHeader("Expires", "60");
            Response.AppendHeader("Content-Disposition",
            "attachment; " +
            "filename=\"OutboundDocument.csv\"; " +
            "size=" + fileBytes.Length.ToString() + "; " +
            "creation-date=" + DateTime.Now.ToString("R") + "; " +
            "modification-date=" + DateTime.Now.ToString("R") + "; " +
            "read-date=" + DateTime.Now.ToString("R"));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Write it back to the client    
            Response.BinaryWrite(fileBytes);
            Response.End();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocumentSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
