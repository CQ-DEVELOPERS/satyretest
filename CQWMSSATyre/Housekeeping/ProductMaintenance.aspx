<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductMaintenance.aspx.cs" Inherits="Housekeeping_ProductMaintenance"
    Title="<%$ Resources:Default, MovementTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<%@ Register Src="~/Common/LocationSearch.ascx" TagName="Location" TagPrefix="uc1" %>
<%@ Register Src="~/Common/AreaSearch.ascx" TagName="Area" TagPrefix="uc1" %>
<%@ Register Src="~/Common/BatchSearch.ascx" TagName="Batch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ProductMaintenance %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, HousekeepingModule%>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabProducts" HeaderText="<%$ Resources:Default, Product%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="updatePanel_Products" runat="server" EnableViewState="False">
                    <ContentTemplate>
                        <asp:Panel ID="PanelSearch" runat="server" Width="450px" BackColor="#EFEFEC" DefaultButton="ButtonSearch">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelProductCode" runat="server" Text='<%$ Resources:Default,ProductCode %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelProduct" runat="server" Text='<%$ Resources:Default,Product %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelBarcode" runat="server" Text='<%$ Resources:Default,Barcode %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxBarcode" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default, SKUCode%>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxSKUCode" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSKU" runat="server" Text="<%$ Resources:Default, SKU%>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxSKU" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal %>"></asp:Label>
                                    </td>
                                    <td>
                                        <telerik:RadDropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                            DataTextField="Principal" DataValueField="PrincipalId" Width="150px">
                                        </telerik:RadDropDownList>
                                        <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                            SelectMethod="GetPrincipalParameter">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                    Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        <asp:Button ID="ButtonSearch" runat="server" Text='<%$ Resources:Default,Search %>'
                                            OnClick="ButtonSearch_Click" />
                                        <asp:Button ID="ButtonAddProduct" runat="server" Text="<%$ Resources:Default, Add%>"
                                            OnClick="ButtonAddProduct_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                            TargetControlID="PanelSearch" Radius="10" Color="239, 239, 236" BorderColor="64, 64, 64"
                            Enabled="True" />
                        <br />
                        <asp:GridView ID="GridViewProduct" DataSourceID="ObjectDataSourceProduct" DataKeyNames="ProductId"
                            runat="server" AllowPaging="True" AllowSorting="True" PageSize="10" AutoGenerateColumns="False"
                            OnSelectedIndexChanged="GridViewProduct_SelectedIndexChanged" ShowFooter="false"
                            EmptyDataText="<%$ Resources:Default, CurrentlyNoProducts%>">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <HeaderTemplate>
                                        <asp:Label ID="labelclick" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                            Text="<%$ Resources:Default, Update%>" SkinID="linkButtonBlack"></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                            Text="<%$ Resources:Default, Cancel%>" SkinID="linkButtonBlack"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Select"
                                            Text="<%$ Resources:Default, Select%>" SkinID="linkButtonBlack"></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                            Text="<%$ Resources:Default, Edit%>" SkinID="linkButtonBlack"></asp:LinkButton>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:LinkButton ID="lbInsert" runat="server" CommandName="Insert" OnClick="lbInsert_Click"
                                            SkinID="linkButtonBlack">Insert</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButtonCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                            Text="<%$ Resources:Default, Cancel%>" OnClick="LinkButtonCancel_Click" SkinID="linkButtonBlack"></asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="ProductId">
                                    <HeaderTemplate>
                                        <asp:Label ID="labelPID" runat="server" Text="<%$ Resources:Default, Product%>" SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="Labelproductcode" runat="server" Text='<%# Eval("ProductCode") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Labelproductcode" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="Textboxproductcode" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredNewProductCode" runat="server" ControlToValidate="Textboxproductcode"
                                            ErrorMessage="Please type in Product Code"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="Status">
                                    <HeaderTemplate>
                                        <asp:Label ID="labelS" runat="server" Text="<%$ Resources:Default, Status %>" SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceStatus"
                                            DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="DropDownListStatusNew" runat="server" DataSourceID="ObjectDataSourceStatus"
                                            DataTextField="Status" DataValueField="StatusId">
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="Product">
                                    <HeaderTemplate>
                                        <asp:Label ID="labelPD" runat="server" Text="<%$ Resources:Default, Product %>" SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxProduct" runat="server" Columns="80" Text='<%# Bind("Product") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("Product") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxProductNew" runat="server" Columns="80"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredNewProductDesc" runat="server" ControlToValidate="TextBoxProductNew"
                                            ErrorMessage="Please type in Product Description"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="BarCode">
                                    <HeaderTemplate>
                                        <asp:Label ID="labelB" runat="server" Text="<%$ Resources:Default, Barcode %>" SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxbarcode" runat="server" Text='<%# Bind("Barcode") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelBarcode" runat="server" Text='<%# Bind("Barcode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxbarcodeNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelMQ" runat="server" Text="<%$ Resources:Default, MinimumQuantity %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxMinQuantity" runat="server" Text='<%# Bind("MinimumQuantity") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelMinQuantity" runat="server" Text='<%# Bind("MinimumQuantity") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxMinQuantityNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelRC" runat="server" Text="<%$ Resources:Default, ReorderQuantity %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxReorder" runat="server" Text='<%# Bind("ReorderQuantity") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelReorder" runat="server" Text='<%# Bind("ReorderQuantity") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxReorderNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelMaxQ" runat="server" Text="<%$ Resources:Default, MaximumQuantity %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxMaxQuan" runat="server" Text='<%# Bind("MaximumQuantity") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelMaxQuan" runat="server" Text='<%# Bind("MaximumQuantity") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxMaxQuanNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelCP" runat="server" Text="<%$ Resources:Default, CuringPeriod %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxCurPer" runat="server" Text='<%# Bind("CuringPeriodDays") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelCurPer" runat="server" Text='<%# Bind("CuringPeriodDays") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxCurPerNew" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelSH" runat="server" Text="<%$ Resources:Default, ShelfLife %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxShelfLife" runat="server" Text='<%# Bind("ShelfLifeDays") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelShelfLife" runat="server" Text='<%# Bind("ShelfLifeDays") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxShelfLifeNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelQA" runat="server" Text="<%$ Resources:Default, QAIndicator %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkQualityAssuranceIndicator" runat="server" Checked='<%# Bind("QualityAssuranceIndicator") %>'></asp:CheckBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelQualityAssuranceIndicator" runat="server" Text='<%# Bind("QualityAssuranceIndicator") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:CheckBox ID="chkQualityAssuranceIndicatorNew" runat="server"></asp:CheckBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelPT" runat="server" Text="<%$ Resources:Default, ProductType %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxProductType" runat="server" Text='<%# Bind("ProductType") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelProductType" runat="server" Text='<%# Bind("ProductType") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxProductTypeNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelOR" runat="server" Text="<%$ Resources:Default, OverReceipt %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxOverReceipt" runat="server" Text='<%# Bind("OverReceipt") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelOverReceipt" runat="server" Text='<%# Bind("OverReceipt") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxOverReceiptNew" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelHI" runat="server" Text="<%$ Resources:Default, HostId %>" SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxHostId" runat="server" Text='<%# Bind("HostId") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelHostId" runat="server" Text='<%# Bind("HostId") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxHostIdNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelRS" runat="server" Text="<%$ Resources:Default, RetentionSamples %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxRetentionSamples" runat="server" Text='<%# Bind("RetentionSamples") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelRetentionSamples" runat="server" Text='<%# Bind("RetentionSamples") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxRetentionSamplesNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelSA" runat="server" Text="<%$ Resources:Default, Samples %>" SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxSamples" runat="server" Text='<%# Bind("Samples") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelSamples" runat="server" Text='<%# Bind("Samples") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxSamplesNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelAS" runat="server" Text="<%$ Resources:Default, AssaySamples %>" SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxAssaySamples" runat="server" Text='<%# Bind("AssaySamples") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelAssaySamples" runat="server" Text='<%# Bind("AssaySamples") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxAssaySamplesNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelCT" runat="server" Text="<%$ Resources:Default, Category %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxCategory" runat="server" Text='<%# Bind("Category") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxCategoryNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelPP" runat="server" Text="<%$ Resources:Default, ParentProductCode %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxParentProductCode" runat="server" Text='<%# Bind("ParentProductCode") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelParentProductCode" runat="server" Text='<%# Bind("ParentProductCode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TextBoxParentProductCodeNew" runat="server"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="DangerousGoods">
                                    <HeaderTemplate>
                                        <asp:Label ID="labelDG" runat="server" Text="<%$ Resources:Default, DangerousGoods %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListDangerousGoodsId" runat="server" DataSourceID="ObjectDataSourceDangerousGoods"
                                            DataTextField="DangerousGoods" DataValueField="DangerousGoodsId" SelectedValue='<%# Bind("DangerousGoodsId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelDangerousGoods" runat="server" Text='<%# Bind("DangerousGoods") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="DropDownListDangerousGoodsNew" runat="server" DataSourceID="ObjectDataSourceDangerousGoods"
                                            DataTextField="DangerousGoods" DataValueField="DangerousGoodsId">
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="Principal">
                                    <HeaderTemplate>
                                        <asp:Label ID="labelPRI" runat="server" Text="<%$ Resources:Default, Principal %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPrincipalId" runat="server" DataSourceID="ObjectDataSourcePrincipal2"
                                            DataTextField="Principal" DataValueField="PrincipalId" SelectedValue='<%# Bind("PrincipalId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelPrincipal" runat="server" Text='<%# Bind("Principal") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="DropDownListPrinciplalNew" runat="server" DataSourceID="ObjectDataSourcePrincipal2"
                                            DataTextField="Principal" DataValueField="PrincipalId">
                                        </asp:DropDownList>
                                    </FooterTemplate>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:DetailsView ID="DetailsViewInsertProduct" runat="server" AutoGenerateRows="False"
                                    DataSourceID="ObjectDataSourceProduct" DefaultMode="Insert">
                                    <Fields>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, ProductID%>" InsertVisible="False"
                                            SortExpression="ProductID">
                                            <InsertItemTemplate>
                                                <asp:Button ID="AddProduct" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Add%>" />
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, Product%>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextboxproductcode" runat="server" Text='<%# Bind("ProductCode") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredProductCode" runat="server" ControlToValidate="DetailTextboxproductcode"
                                                    ErrorMessage="Please type in Product Code"></asp:RequiredFieldValidator>
                                            </InsertItemTemplate>
                                            <FooterStyle Wrap="False" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, Status %>">
                                            <InsertItemTemplate>
                                                <asp:DropDownList ID="DetailDropDownListStatus" runat="server" DataSourceID="ObjectDataSourceStatus"
                                                    DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'>
                                                </asp:DropDownList>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, Product %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxProduct" runat="server" Columns="80" Text='<%# Bind("Product") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredProductDesc" runat="server" ControlToValidate="DetailTextBoxProduct"
                                                    ErrorMessage="Please type in Product Description"></asp:RequiredFieldValidator>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, Barcode %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxbarcode" runat="server" Text='<%# Bind("Barcode") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, MinimumQuantity %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxMinQuantity" runat="server" Text='<%# Bind("MinimumQuantity") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, ReorderQuantity %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxReorder" runat="server" Text='<%# Bind("ReorderQuantity") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, MaximumQuantity %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxMaxQuan" runat="server" Text='<%# Bind("MaximumQuantity") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, CuringPeriod %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxCurPer" runat="server" Text='<%# Bind("CuringPeriodDays") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, ShelfLife %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxShelfLife" runat="server" Text='<%# Bind("ShelfLifeDays") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, QAIndicator %>">
                                            <InsertItemTemplate>
                                                <asp:CheckBox ID="DetailchkQualityAssuranceIndicator" runat="server" Checked='<%# Bind("QualityAssuranceIndicator") %>'></asp:CheckBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, ProductType %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxProductType" runat="server" Text='<%# Bind("ProductType") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, OverReceipt %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxOverReceipt" runat="server" Text='<%# Bind("OverReceipt") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, HostId %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxHostId" runat="server" Text='<%# Bind("HostId") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, RetentionSamples %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxRetentionSamples" runat="server" Text='<%# Bind("RetentionSamples") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, Samples %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxSamples" runat="server" Text='<%# Bind("Samples") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, AssaySamples %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxAssaySamples" runat="server" Text='<%# Bind("AssaySamples") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, ParentProductCode %>">
                                            <InsertItemTemplate>
                                                <asp:TextBox ID="DetailTextBoxParentProductCode" runat="server" Text='<%# Bind("ParentProductCode") %>'></asp:TextBox>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, DangerousGoods %>">
                                            <InsertItemTemplate>
                                                <asp:DropDownList ID="DetailDropDownListDangerousGoods" runat="server" DataSourceID="ObjectDataSourceDangerousGoods"
                                                    DataTextField="DangerousGoods" DataValueField="DangerousGoodsId" SelectedValue='<%# Bind("DangerousGoodsId") %>'>
                                                </asp:DropDownList>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Default, Principal %>">
                                            <InsertItemTemplate>
                                                <asp:DropDownList ID="DetailDropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal2"
                                                    DataTextField="Principal" DataValueField="PrincipalId" SelectedValue='<%# Bind("PrincipalId") %>'>
                                                </asp:DropDownList>
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ShowInsertButton="true" />
                                    </Fields>
                                </asp:DetailsView>
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="StaticInfo"
                            SelectMethod="product_Search" UpdateMethod="product_Update" InsertMethod="product_Add"
                            OnInserting="ObjectDataSourceProduct_Inserting" OnInserted="ObjectDataSourceProduct_Inserted">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:ControlParameter ControlID="TextBoxProductCode" PropertyName="Text" Name="productCode"
                                    Type="String" />
                                <asp:ControlParameter ControlID="TextBoxProduct" PropertyName="Text" Name="product"
                                    Type="String" />
                                <asp:ControlParameter ControlID="TextBoxSKUCode" PropertyName="Text" Name="SKUCode"
                                    Type="String" />
                                <asp:ControlParameter ControlID="TextBoxSKU" PropertyName="Text" Name="SKU"
                                    Type="String" />
                                <asp:ControlParameter ControlID="TextBoxBarcode" PropertyName="Text" Name="barcode"
                                    Type="String" />
                                <asp:ControlParameter Name="PrincipalId" ControlID="DropDownListPrincipal" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="statusId" Type="Int32" />
                                <asp:Parameter Name="product" Type="String" />
                                <asp:Parameter Name="productCode" Type="String" />
                                <asp:Parameter Name="barcode" Type="String" />
                                <asp:Parameter Name="minimumQuantity" Type="Decimal" />
                                <asp:Parameter Name="reorderQuantity" Type="Decimal" />
                                <asp:Parameter Name="maximumQuantity" Type="Decimal" />
                                <asp:Parameter Name="curingPeriodDays" Type="Int32" />
                                <asp:Parameter Name="shelfLifeDays" Type="Int32" />
                                <asp:Parameter Name="qualityAssuranceIndicator" Type="String" />
                                <asp:Parameter Name="productType" Type="String" />
                                <asp:Parameter Name="overReceipt" Type="Decimal" />
                                <asp:Parameter Name="hostId" Type="String" />
                                <asp:Parameter Name="retentionSamples" Type="Decimal" />
                                <asp:Parameter Name="samples" Type="Decimal" />
                                <asp:Parameter Name="assaySamples" Type="Decimal" />
								<asp:Parameter Name="category" Type="String" />
                                <asp:Parameter Name="parentProductCode" Type="String" />
                                <asp:Parameter Name="dangerousGoodsId" Type="Int32" />
                                <asp:Parameter Name="principalId" Type="Int32" />
                                <asp:Parameter Name="productId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="statusId" Type="Int32" />
                                <asp:Parameter Name="productCode" Type="String" />
                                <asp:Parameter Name="product" Type="String" />
                                <asp:Parameter Name="barcode" Type="String" />
                                <asp:Parameter Name="minimumQuantity" Type="Decimal" DefaultValue="0" />
                                <asp:Parameter Name="reorderQuantity" Type="Decimal" DefaultValue="0" />
                                <asp:Parameter Name="maximumQuantity" Type="Decimal" DefaultValue="0" />
                                <asp:Parameter Name="curingPeriodDays" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="shelfLifeDays" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="qualityAssuranceIndicator" Type="String" />
                                <asp:Parameter Name="productType" Type="String" />
                                <asp:Parameter Name="overReceipt" Type="Decimal" DefaultValue="0" />
                                <asp:Parameter Name="hostId" Type="String" />
                                <asp:Parameter Name="retentionSamples" Type="Decimal" DefaultValue="0" />
                                <asp:Parameter Name="samples" Type="Decimal" DefaultValue="0" />
                                <asp:Parameter Name="assaySamples" Type="Decimal" DefaultValue="0" />
								<asp:Parameter Name="category" Type="String" />
                                <asp:Parameter Name="parentProductCode" Type="String" />
                                <asp:Parameter Name="dangerousGoodsId" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="principalId" Type="Int32" DefaultValue="-1" />
                            </InsertParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status"
                            SelectMethod="GetStatus">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="type" DefaultValue="P" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceDangerousGoods" runat="server" TypeName="DangerousGoods"
                            SelectMethod="GetDangerousGoodsId">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourcePrincipal2" runat="server" TypeName="Principal"
                            SelectMethod="GetPrincipalParameter">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonAddProduct" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabSku" HeaderText="<%$ Resources:Default, SKUCode %>"
            runat="server">
            <ContentTemplate>
                <asp:UpdatePanel ID="Updatepanel_sku" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnl_Sku" runat="server" Visible="false">
                            <asp:DropDownList ID="dropdownlistskus" runat="server" DataSourceID="ObjectDataSourceSkusList"
                                DataTextField="SKU" DataValueField="SKUId">
                            </asp:DropDownList>
                            <br />
                            <asp:Button ID="buttonaddproductsku" runat="server" Text="<%$ Resources:Default, AddSKU%>"
                                OnClick="buttonaddproductsku_Click" />
                            <asp:GridView ID="GridViewSku" DataSourceID="ObjectDataSourceSku" DataKeyNames="StorageUnitId"
                                runat="server" AllowPaging="True" AllowSorting="True" PageSize="30" AutoGenerateColumns="False"
                                OnSelectedIndexChanged="GridViewSku_SelectedIndexChanged">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" ShowDeleteButton="true" ShowEditButton="true" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropdownlistskusedit" runat="server" DataSourceID="ObjectDataSourceSkusList"
                                                DataTextField="SKUCode" DataValueField="SKUId" SelectedValue='<%# Bind("SKUId") %>'>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSKU" runat="server" Text='<%# Bind("SKUCode") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, SKU %>" DataField="SKU" SortExpression="SKU"
                                        ReadOnly="true" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, ProductCode %>" DataField="ProductCode"
                                        SortExpression="ProductCode" ReadOnly="true" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Product %>" DataField="Product"
                                        SortExpression="Product" ReadOnly="true" />
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelPC" runat="server" Text="<%$ Resources:Default, PackingCategory %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxPackingCategory" runat="server" Text='<%# Bind("PackingCategory") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPackingCategory" runat="server" Text='<%# Bind("PackingCategory") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxPackingCategoryNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelPR" runat="server" Text="<%$ Resources:Default, ProductCategory %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxProductCategory" runat="server" Text='<%# Bind("ProductCategory") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelProductCategory" runat="server" Text='<%# Bind("ProductCategory") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxProductCategoryNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelPE" runat="server" Text="<%$ Resources:Default, PickEmpty %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkPickEmpty" runat="server" Checked='<%# Bind("PickEmpty") %>'></asp:CheckBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPickEmpty" runat="server" Text='<%# Bind("PickEmpty") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkPickEmptyNew" runat="server"></asp:CheckBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelSC" runat="server" Text="<%$ Resources:Default, StackingCategory %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxStackingCategory" runat="server" Text='<%# Bind("StackingCategory") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelStackingCategory" runat="server" Text='<%# Bind("StackingCategory") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxStackingCategoryNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelMC" runat="server" Text="<%$ Resources:Default, MovementCategory %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxMovementCategory" runat="server" Text='<%# Bind("MovementCategory") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelMovementCategory" runat="server" Text='<%# Bind("MovementCategory") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxMovementCategoryNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelVC" runat="server" Text="<%$ Resources:Default, ValueCategory %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxValueCategory" runat="server" Text='<%# Bind("ValueCategory") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelValueCategory" runat="server" Text='<%# Bind("ValueCategory") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxValueCategoryNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelSTC" runat="server" Text="<%$ Resources:Default, StoringCategory %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxStoringCategory" runat="server" Text='<%# Bind("StoringCategory") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelStoringCategory" runat="server" Text='<%# Bind("StoringCategory") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxStoringCategoryNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelPPP" runat="server" Text="<%$ Resources:Default, PickPartPallet %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxPickPartPallet" runat="server" Text='<%# Bind("PickPartPallet") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPickPartPallet" runat="server" Text='<%# Bind("PickPartPallet") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxPickPartPalletNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelMIQ" runat="server" Text="<%$ Resources:Default, MinimumQuantity %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxMinimumQuantity" runat="server" Text='<%# Bind("MinimumQuantity") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelMinimumQuantity" runat="server" Text='<%# Bind("MinimumQuantity") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxMinimumQuantityNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelRQ" runat="server" Text="<%$ Resources:Default, ReorderQuantity  %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxReorderQuantity" runat="server" Text='<%# Bind("ReorderQuantity") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelReorderQuantity" runat="server" Text='<%# Bind("ReorderQuantity") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxReorderQuantityNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelMAQ" runat="server" Text="<%$ Resources:Default, MaximumQuantity %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxMaximumQuantity" runat="server" Text='<%# Bind("MaximumQuantity") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelMaximumQuantity" runat="server" Text='<%# Bind("MaximumQuantity") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxMaximumQuantityNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelQC" runat="server" Text="<%$ Resources:Default, QAIndicator %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkQC" runat="server" Checked='<%# Bind("DefaultQC") %>'></asp:CheckBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelDefaultQC" runat="server" Text='<%# Bind("DefaultQC") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkPickDefaultQC" runat="server"></asp:CheckBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelUP" runat="server" Text="<%$ Resources:Default, UnitPrice %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxUnitPrice" runat="server" Text='<%# Bind("UnitPrice") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelUnitPrice" runat="server" Text='<%# Bind("UnitPrice") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxUnitPriceNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelSizeHeader" runat="server" Text="<%$ Resources:Default, Size %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxSize" runat="server" Text='<%# Bind("Size") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSize" runat="server" Text='<%# Bind("Size") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxSizeNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelColourHeader" runat="server" Text="<%$ Resources:Default, Colour %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxColour" runat="server" Text='<%# Bind("Colour") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelColour" runat="server" Text='<%# Bind("Colour") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxColourNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelStyleHeader" runat="server" Text="<%$ Resources:Default, Style %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxStyle" runat="server" Text='<%# Bind("Style") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelStyle" runat="server" Text='<%# Bind("Style") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxStyleNew" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelLotAttributeRuleHeader" runat="server" Text="<%$ Resources:Default, Batch %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropdownlistLotAttributeRule" runat="server" DataSourceID="ObjectDataSourceLotAttributeRule"
                                                SelectedValue='<%# Bind("LotAttributeRule") %>'>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelLotAttributeRule" runat="server" Text='<%# Bind("LotAttributeRule") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="TextBoxLotAttributeRule" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="labelSerialTracked" runat="server" Text="<%$ Resources:Default, SerialTracked %>"
                                            SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkSerialTracked" runat="server" Checked='<%# Bind("SerialTracked") %>'></asp:CheckBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelSerialTracked" runat="server" Text='<%# Bind("SerialTracked") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:CheckBox ID="chkSerialTrackedNew" runat="server"></asp:CheckBox>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                    <%--<asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="labelSerialTracked" runat="server" Text="<%$ Resources:Default, SerialTracked %>"
                                                SkinID="GridviewTitle"></asp:Label>
                                        </HeaderTemplate>
                                        <EditItemTemplate>
                                            <asp:CheckBox ID="chkSerialTracked" runat="server" Checked='<%# Bind("DefaultSerialTracked") %>'></asp:CheckBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelDefaultSerialTracked" runat="server" Text='<%# Bind("DefaultSerialTracked") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkPickDefaultSerialTracked" runat="server"></asp:CheckBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceSku" runat="server" TypeName="StaticInfo"
                                SelectMethod="sku_Search" DeleteMethod="sku_Delete" UpdateMethod="sku_Update"
                                InsertMethod="sku_Add">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="productId" SessionField="ProductId" Type="Int32" DefaultValue="-1" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:Parameter Name="storageUnitId" />
                                </DeleteParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:Parameter Name="storageUnitId" />
                                    <asp:Parameter Name="skuId" />
                                    <asp:Parameter Name="packingCategory" Type="String" />
                                    <asp:Parameter Name="productCategory" Type="String" />
                                    <asp:Parameter Name="pickEmpty" Type="String" />
                                    <asp:Parameter Name="stackingCategory" Type="Int32" />
                                    <asp:Parameter Name="movementCategory" Type="Int32" />
                                    <asp:Parameter Name="valueCategory" Type="Int32" />
                                    <asp:Parameter Name="storingCategory" Type="Int32" />
                                    <asp:Parameter Name="pickPartPallet" Type="Int32" />
                                    <asp:Parameter Name="minimumQuantity" Type="Decimal" />
                                    <asp:Parameter Name="reorderQuantity" Type="Decimal" />
                                    <asp:Parameter Name="maximumQuantity" Type="Decimal" />
                                    <asp:Parameter Name="DefaultQC" Type="String" />
                                    <asp:Parameter Name="UnitPrice" Type="Decimal" />
                                    <asp:Parameter Name="Size" Type="String" />
                                    <asp:Parameter Name="Colour" Type="String" />
                                    <asp:Parameter Name="Style" Type="String" />
                                    <asp:Parameter Name="SerialTracked" Type="String" />
                                </UpdateParameters>
                                <InsertParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:Parameter Name="storageUnitId" />
                                    <asp:Parameter Name="skuId" />
                                    <asp:Parameter Name="packingCategory" Type="String" />
                                    <asp:Parameter Name="productCategory" Type="String" />
                                    <asp:Parameter Name="pickEmpty" Type="String" />
                                    <asp:Parameter Name="stackingCategory" Type="Int32" />
                                    <asp:Parameter Name="movementCategory" Type="Int32" />
                                    <asp:Parameter Name="valueCategory" Type="Int32" />
                                    <asp:Parameter Name="storingCategory" Type="Int32" />
                                    <asp:Parameter Name="pickPartPallet" Type="Int32" />
                                    <asp:Parameter Name="minimumQuantity" Type="Decimal" />
                                    <asp:Parameter Name="reorderQuantity" Type="Decimal" />
                                    <asp:Parameter Name="maximumQuantity" Type="Decimal" />
                                    <asp:Parameter Name="DefaultQC" Type="String" />
                                    <asp:Parameter Name="UnitPrice" Type="Decimal" />
                                    <asp:Parameter Name="Size" Type="String" />
                                    <asp:Parameter Name="Colour" Type="String" />
                                    <asp:Parameter Name="Style" Type="String" />
                                    <asp:Parameter Name="SerialTracked" Type="String" />
                                </InsertParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSourceSkusList" runat="server" TypeName="StaticInfo"
                                SelectMethod="GetSku">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSourceLotAttributeRule" runat="server" TypeName="StaticInfo"
                                SelectMethod="GetLotAttributeRules">
                            </asp:ObjectDataSource>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPack" HeaderText="<%$ Resources:Default, Packs %>" runat="server">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel_Pack" runat="server" Visible="false">
                            <asp:Button ID="buttonPack" runat="server" Text="<%$ Resources:Default, AddPack%>"
                                OnClick="buttonPack_Click" />
                            <asp:GridView ID="GridViewPacks" DataSourceID="ObjectDataSourcePacks" DataKeyNames="PackId"
                                runat="server" AllowPaging="True" AllowSorting="True" PageSize="30" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:CommandField ShowEditButton="true" ShowDeleteButton="true" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, PackType%>" SortExpression="PackType">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DropDownListPackType" runat="server" DataSourceID="ObjectDataSourcePackType"
                                                DataTextField="PackType" DataValueField="PackTypeId" SelectedValue='<%# Bind("PackTypeId") %>'>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPackType" runat="server" Text='<%# Bind("PackType") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Quantity %>" DataField="Quantity"></asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Barcode %>" DataField="Barcode"></asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Length %>" DataField="Length"></asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Width %>" DataField="Width"></asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Height %>" DataField="Height"></asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Volume %>" DataField="Volume"></asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, WeightDetails %>" DataField="Weight"></asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, NettWeight %>" DataField="NettWeight"></asp:BoundField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, TareWeight %>" DataField="TareWeight"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourcePacks" runat="server" TypeName="StaticInfo"
                                SelectMethod="pack_Search" UpdateMethod="pack_Update" DeleteMethod="pack_Delete">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                                        DefaultValue="-1" />
                                    <asp:SessionParameter Name="wareHouseId" SessionField="WareHouseId" Type="int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32" />
                                    <asp:Parameter Name="packTypeId" Type="Int32" />
                                    <asp:Parameter Name="quantity" Type="Decimal" />
                                    <asp:Parameter Name="barcode" Type="String" />
                                    <asp:Parameter Name="length" Type="Decimal" />
                                    <asp:Parameter Name="width" Type="Decimal" />
                                    <asp:Parameter Name="height" Type="Decimal" />
                                    <asp:Parameter Name="volume" Type="Decimal" />
                                    <asp:Parameter Name="weight" Type="Decimal" />
                                    <asp:Parameter Name="nettWeight" Type="Decimal" />
                                    <asp:Parameter Name="tareWeight" Type="Decimal" />
                                    <asp:Parameter Name="packId" Type="Int32" />
                                </UpdateParameters>
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:Parameter Name="packId" Type="Int32" />
                                </DeleteParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSourcePackType" runat="server" TypeName="StaticInfo"
                                SelectMethod="GetPackType">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabBatches" HeaderText="<%$ Resources:Default, Batch %>"
            runat="server">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel_Batch" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch%>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelChangeLevel" runat="server" Text="<%$ Resources:Default, ChangeLevel%>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxChangeLevel" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="right">
                                        <asp:Button ID="ButtonSearchStorage" runat="server" Text="<%$ Resources:Default, Search%>"
                                            OnClick="ButtonSearchStorage_Click" />
                                    </td>
                                </tr>
                            </table>
                            <asp:GridView ID="GridViewBatchSearch" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                AutoGenerateSelectButton="True" DataSourceID="ObjectDataSourceBatchStorage" DataKeyNames="BatchId,Batch"
                                OnSelectedIndexChanged="GridViewBatchSearch_OnSelectedIndexChanged">
                                <Columns>
                                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch%>" />
                                    <asp:BoundField DataField="ECLNumber" HeaderText="<%$ Resources:Default, ChangeLevel%>" />
                                    <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate%>" />
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceBatchStorage" runat="server" TypeName="StaticInfo"
                                EnablePaging="true" MaximumRowsParameterName="PageSize" StartRowIndexParameterName="StartRow"
                                SelectMethod="SearchBatchesByStorageUnit" SelectCountMethod="CountBatchesByStorageUnit">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:ControlParameter Name="batch" ControlID="TextBoxBatch" Type="String" />
                                    <asp:ControlParameter Name="eCLNumber" ControlID="TextBoxChangeLevel" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:Button ID="buttonstorageUnitBatch" runat="server" Text="<%$ Resources:Default, AddBatch%>"
                                OnClick="buttonstorageUnitBatch_Click" />
                            <asp:GridView ID="GridViewBatch" DataSourceID="ObjectDataSourceBatch" DataKeyNames="StorageUnitBatchId"
                                runat="server" AllowPaging="True" AllowSorting="True" PageSize="30" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:CommandField ShowDeleteButton="true" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, StorageUnitBatch%>" DataField="StorageUnitBatchId" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Batch%>" DataField="Batch" />
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="StaticInfo"
                                SelectMethod="storageUnitBatch_Search" DeleteMethod="storageUnitBatch_Delete">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                                        DefaultValue="-1" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:Parameter Name="storageUnitBatchId" Type="Int32" />
                                </DeleteParameters>
                            </asp:ObjectDataSource>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabArea" HeaderText="<%$ Resources:Default, Area %>" runat="server">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel_Area" runat="server" Visible="false">
                            <uc1:Area ID="Area1" runat="server" />
                            <asp:Button ID="buttonAddstorageArea" runat="server" Text="<%$ Resources:Default, AddArea%>"
                                OnClick="buttonAddstorageArea_Click" />
                            <asp:GridView ID="GridViewArea" DataSourceID="ObjectDataSourceArea" DataKeyNames="StorageUnitId,AreaId"
                                runat="server" AllowPaging="True" AllowSorting="True" PageSize="30" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:CommandField ShowEditButton="true" ShowDeleteButton="true" />
                                    <asp:BoundField ReadOnly="true" DataField="Area" HeaderText="<%$ Resources:Default, Area%>"
                                        SortExpression="AreaId"></asp:BoundField>
                                    <asp:BoundField DataField="StoreOrder" HeaderText="<%$ Resources:Default, StoreOrder %>"></asp:BoundField>
                                    <asp:BoundField DataField="PickOrder" HeaderText="<%$ Resources:Default, PickOrder %>"></asp:BoundField>
                                    <asp:BoundField DataField="MinimumQuantity" HeaderText="<%$ Resources:Default, MinimumQuantity %>"></asp:BoundField>
                                    <asp:BoundField DataField="ReorderQuantity" HeaderText="<%$ Resources:Default, ReorderQuantity %>"></asp:BoundField>
                                    <asp:BoundField DataField="MaximumQuantity" HeaderText="<%$ Resources:Default, MaximumQuantity %>"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="StaticInfo"
                                SelectMethod="storageUnitArea_Search" UpdateMethod="storageUnitArea_Update" DeleteMethod="storageUnitArea_Delete">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                                        DefaultValue="-1" />
                                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:Parameter Name="storeOrder" Type="Int32" />
                                    <asp:Parameter Name="pickOrder" Type="Int32" />
                                    <asp:Parameter Name="storageUnitId" Type="Int32" />
                                    <asp:Parameter Name="areaId" Type="Int32" />
                                    <asp:Parameter Name="minimumQuantity" Type="Decimal" />
                                    <asp:Parameter Name="reorderQuantity" Type="Decimal" />
                                    <asp:Parameter Name="maximumQuantity" Type="Decimal" />
                                </UpdateParameters>
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                                        DefaultValue="-1" />
                                    <asp:Parameter Name="areaId" Type="Int32" />
                                </DeleteParameters>
                            </asp:ObjectDataSource>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabLocation" HeaderText="<%$ Resources:Default, Location %>"
            runat="server">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel_Location" runat="server" Visible="false">
                            <uc1:Location ID="location1" runat="server" />
                            <asp:Button ID="addStorageLocation" runat="server" Text="<%$ Resources:Default, AddLocation%>"
                                OnClick="addStorageLocation_Click" />
                            <asp:GridView ID="GridViewLocation" DataSourceID="ObjectDataSourceStorageLocation"
                                DataKeyNames="LocationId" runat="server" AllowPaging="True" AllowSorting="True"
                                PageSize="30" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:CommandField ShowEditButton="true" ShowDeleteButton="true" />
                                    <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>"
                                        ReadOnly="true" />
                                    <asp:BoundField DataField="MinimumQuantity" HeaderText="<%$ Resources:Default, MinimumQuantity %>" />
                                    <asp:BoundField DataField="HandlingQuantity" HeaderText="<%$ Resources:Default, HandlingQuantity %>" />
                                    <asp:BoundField DataField="MaximumQuantity" HeaderText="<%$ Resources:Default, MaximumQuantity %>" />
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceStorageLocation" runat="server" TypeName="StaticInfo"
                                SelectMethod="storageUnitLocation_Search" DeleteMethod="storageUnitLocation_Delete"
                                UpdateMethod="storageUnitLocation_Update">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                                        DefaultValue="-1" />
                                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                                        DefaultValue="-1" />
                                    <asp:Parameter Name="locationId" Type="Int32" />
                                </DeleteParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32"
                                        DefaultValue="-1" />
                                    <asp:Parameter Name="minimumQuantity" Type="Decimal" DefaultValue="0" />
                                    <asp:Parameter Name="handlingQuantity" Type="Decimal" DefaultValue="0" />
                                    <asp:Parameter Name="maximumQuantity" Type="Decimal" DefaultValue="0" />
                                    <asp:Parameter Name="locationId" Type="Int32" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
