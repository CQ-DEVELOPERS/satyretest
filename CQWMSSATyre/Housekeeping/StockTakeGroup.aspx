﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="StockTakeGroup.aspx.cs" Inherits="Housekeeping_StockTakeGroup" Title="<%$ Resources:Default, StockTakeReferenceTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeCreateTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Link">
            <ContentTemplate>
                <asp:UpdatePanel ID="updatePanel_StockTake" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="Button1" runat="server" Text="Add New Group" OnClick="ButtonAddStockTakeGroup_Click" />
                        <asp:Button ID="Button2" runat="server" Text="Link Locations" OnClick="ButtonNext_Click" />
                        <asp:Label ID="LabelError" runat="server" ForeColor="Red" Text="" />
                        <%--<asp:Button ID="ButtonSkip" runat="server" Text="Skip Reference Add" 
                             onclick="ButtonSkip_Click" />--%>
                        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="" />
                        <asp:GridView ID="GridViewStockTake" DataSourceID="ObjectDataSourceStockTakeGroup"
                            DataKeyNames="StockTakeGroupId" runat="server" AllowPaging="True" AllowSorting="True"
                            PageSize="15" AutoGenerateColumns="False" EmptyDataText="There are currently no Stock Take Groups..."
                            OnSelectedIndexChanged="GridViewStockTake_SelectedIndexChanged">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ShowEditButton="True" ShowDeleteButton="True" />
                                <asp:BoundField HeaderText="StockTakeGroupId" DataField="StockTakeGroupId" SortExpression="StockTakeGroupId"
                                    ReadOnly="True" Visible="False" />
                                <asp:BoundField HeaderText="StockTakeGroupCode" DataField="StockTakeGroupCode" SortExpression="StockTakeGroupCode" />
                                <asp:BoundField HeaderText="StockTakeGroup" DataField="StockTakeGroup" SortExpression="StockTakeGroup" />
                            </Columns>
                        </asp:GridView>
                        <asp:DetailsView Visible="false" ID="DetailsViewInsertStockTakeGroup" runat="server"
                            AutoGenerateRows="False" DataSourceID="ObjectDataSourceStockTakeGroup" DefaultMode="Insert"
                            OnLoad="DetailsViewInsertStockTakeGroup_Load">
                            <Fields>
                                <asp:TemplateField HeaderText="StockTakeGroupId" InsertVisible="False" SortExpression="StockTakeGroupId">
                                    <InsertItemTemplate>
                                        <asp:Button ID="AddStockTakeGroup" runat="server" CommandName="Insert" Text="Add" />
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="StockTakeGroupCode">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextStockTakeGroupCode" runat="server" Text='<%# Bind("StockTakeGroupCode") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="StockTakeGroup">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextStockTakeGroup" runat="server" Text='<%# Bind("StockTakeGroup") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <InsertItemTemplate>
                                        <asp:LinkButton ID="btnInsert" runat="server" CommandName="Insert" Text="Insert"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                            Text="Cancel" OnClick="btnCancel_Click"></asp:LinkButton>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                            </Fields>
                        </asp:DetailsView>
                        <asp:ObjectDataSource ID="ObjectDataSourceStockTakeGroup" runat="server" TypeName="StockTake"
                            SelectMethod="StockTakeGroup_Search" UpdateMethod="StockTakeGroup_Update" InsertMethod="StockTakeGroup_Add"
                            DeleteMethod="StockTakeGroup_Delete" OnInserting="ObjectDataSourceStockTakeGroup_Inserting"
                            OnInserted="ObjectDataSourceStockTakeGroup_Inserted">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="stockTakeGroupCode" Type="String" />
                                <asp:Parameter Name="stockTakeGroup" Type="String" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="stockTakeGroupCode" Type="string" />
                                <asp:Parameter Name="stockTakeGroup" Type="string" />
                            </InsertParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="stockTakeGroupId" Type="string" />
                            </DeleteParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
