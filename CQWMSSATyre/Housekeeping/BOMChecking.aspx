﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true" CodeFile="BOMChecking.aspx.cs" Inherits="Housekeeping_BOMChecking"
    StylesheetTheme="Default" Theme="Default" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text='<%$ Resources:Default,ReferenceNumber %>'></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <asp:Label ID="LabelReferenceNumber" runat="server" Text="<%$ Resources:Default, ReferenceNumber %>"></asp:Label>
                    <asp:TextBox ID="TextBoxReferenceNumber" runat="server"></asp:TextBox>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <asp:GridView ID="gvwBOMList" runat="server" AutoGenerateSelectButton="True" AllowPaging="True"
                        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="BOMInstructionId"
                        DataSourceID="dsBOMList" OnRowDataBound="gvwBOMList_RowDataBound"
                        OnSelectedIndexChanged="gvwBOMList_SelectedIndexChanged" 
                        ondatabound="gvwBOMList_DataBound">
                        <Columns>
                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                            <asp:BoundField DataField="SKUCode" HeaderText="SKU Code" />
                            <asp:BoundField DataField="ProductBarcode" HeaderText="Barcode" />
                            <asp:BoundField DataField="RequestedSets" HeaderText="Requested Sets" />
                            <asp:BoundField DataField="CheckableSets" HeaderText="Checkable Sets" />
                            <asp:TemplateField HeaderText="Completed Sets" >
                                <ItemTemplate>
                                    <asp:Label ID="lblCompletedSets" runat="server" Text='<%# Bind("CompletedSets") %>'></asp:Label>
                                    <asp:HiddenField ID="hdnChecked" runat="server" Value='<%# Bind("Checked") %>'/>
                                </ItemTemplate>
                            </asp:TemplateField >
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="dsBOMList" runat="server" SelectMethod="ConfirmBOMPallet"
                        TypeName="ProductCheck">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                Type="String" />
                            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32"
                                DefaultValue="-1" />
                            <asp:ControlParameter ControlID="TextBoxReferenceNumber" DefaultValue="" Name="referenceNumber"
                                PropertyName="Text" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <br />
                    <asp:Label ID="lblBOMBarcode" runat="server" Text="Barcode"></asp:Label>
                    <asp:TextBox ID="txtBOMBarcode" runat="server"></asp:TextBox>
                </asp:View>
                <asp:View ID="View3" runat="server">
                    <asp:GridView ID="gvwBOMItems" runat="server" AutoGenerateSelectButton="True" AllowPaging="True"
                        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="StorageUnitId"
                        DataSourceID="dsBOMItems" 
                        OnSelectedIndexChanged="gvwBOMItems_SelectedIndexChanged" 
                        ondatabound="gvwBOMItems_DataBound">
                        <Columns>
                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                            <asp:BoundField DataField="SKUCode" HeaderText="SKUCode" />
                            <asp:BoundField DataField="RequestedSets" HeaderText="Requested Sets" />
                            <asp:BoundField DataField="PartsPerSet" HeaderText="Parts Per Set" />
                            <asp:BoundField DataField="CheckQuantity" HeaderText="CheckQuantity" />
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="dsBOMItems" runat="server" SelectMethod="ConfirmBOMProductDetails"
                        TypeName="ProductCheck">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                Type="String" />
                            <asp:SessionParameter Name="BOMInstructionId" SessionField="BOMInstructionId" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <br />
                    <asp:Label ID="lblItemBarcode" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
                    <asp:TextBox ID="txtItemBarcode" runat="server"></asp:TextBox>
                </asp:View>
                <asp:View ID="View4" runat="server">
                    <asp:DetailsView ID="DetailsViewBatch" runat="server" DataKeyNames="StorageUnitBatchId"
                        AutoGenerateRows="False" AllowPaging="True" DataSourceID="dsBOMItemDetails">
                        <Fields>
                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                            <asp:BoundField DataField="CheckableSets" HeaderText="Checkable Sets" ApplyFormatInEditMode="true"
                                DataFormatString="{0:G0}" />
                            <asp:BoundField DataField="PartsPerSet" HeaderText="Parts Per Set" ApplyFormatInEditMode="true"
                                DataFormatString="{0:G0}" />
                            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                            <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>"
                                ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                            <asp:BoundField DataField="Checkable" HeaderText="Checkable" ApplyFormatInEditMode="true"
                                DataFormatString="{0:G0}" />
                            <asp:BoundField DataField="CheckQuantity" HeaderText="<%$ Resources:Default, CheckQuantity %>"
                                ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                        </Fields>
                    </asp:DetailsView>
                    <asp:ObjectDataSource ID="dsBOMItemDetails" runat="server" SelectMethod="ConfirmBOMBatch"
                        TypeName="ProductCheck">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                Type="String" />
                            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                            <asp:SessionParameter DefaultValue="" Name="BOMInstructionId" SessionField="BOMInstructionId"
                                Type="Int32" />
                            <asp:Parameter Name="storageUnitId" Type="Int32" />
                            <asp:Parameter DefaultValue="Default" Name="batch" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <br />
                    <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
                    <asp:TextBox ID="TextBoxQuantity" runat="server"></asp:TextBox>
                </asp:View>
            </asp:MultiView>
            <p>
                <asp:Button ID="LinkButtonAccept" runat="server" Text="<%$ Resources:Default, ButtonAccept %>"
                    OnClick="LinkButtonAccept_Click" Style="height: 20px"></asp:Button>
                <asp:Button ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, Back %>"
                    Visible="false" onclick="LinkButtonBack_Click"></asp:Button>
                <asp:Button ID="LinkButtonFinished" runat="server" Text="<%$ Resources:Default, ButtonFinish %>"
                    Visible="false" OnClick="LinkButtonFinished_Click"></asp:Button>
                <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
                    Visible="false"></asp:Button>
                <asp:Button ID="ButtonQuit" runat="server" Text="<%$ Resources:Default, ButtonQuit %>"
                    OnClick="ButtonQuit_Click"></asp:Button>
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
