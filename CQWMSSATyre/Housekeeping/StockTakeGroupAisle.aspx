﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="StockTakeGroupAisle.aspx.cs" Inherits="Housekeeping_StockTakeGroupAisle"
    Title="<%$ Resources:Default, StockTakeCreateAreaTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeCreateTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Link">
            <HeaderTemplate>
                Link
            </HeaderTemplate>
            <ContentTemplate>
                <table border="solid" cellpadding="5">
                    <tr>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text="Linked Locations" Font-Bold="True"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label7" runat="server" Text="Areas" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel ID="updatePanel_StockTake" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="" />
                                    <asp:GridView ID="GridViewStockTake" DataSourceID="ObjectDataSourceStockTakeGroupLoc"
                                        DataKeyNames="StockTakeGroupLocId" runat="server" AllowPaging="True" AllowSorting="True"
                                        PageSize="15" AutoGenerateColumns="False" EmptyDataText="There are currently no Stock Take Groups..."
                                        OnSelectedIndexChanged="GridViewStockTake_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="False" ShowEditButton="False" ShowDeleteButton="True" />
                                            <asp:BoundField HeaderText="StockTakeGroupLocId" DataField="StockTakeGroupLocId"
                                                SortExpression="StockTakeGroupLocId" ReadOnly="True" Visible="False" />
                                            <asp:BoundField HeaderText="StockTakeGroupId" DataField="StockTakeGroupId" SortExpression="StockTakeGroupId"
                                                ReadOnly="True" Visible="False" />
                                            <asp:BoundField HeaderText="StockTakeGroup" DataField="StockTakeGroup" SortExpression="StockTakeGroup" />
                                            <asp:BoundField HeaderText="LocationId" DataField="LocationId" SortExpression="LocationId"
                                                Visible="False" />
                                            <asp:BoundField HeaderText="Location" DataField="Location" SortExpression="Location" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:DetailsView Visible="false" ID="DetailsViewInsertStockTakeGroupLoc" runat="server"
                                        AutoGenerateRows="False" DataSourceID="ObjectDataSourceStockTakeGroupLoc" DefaultMode="Insert"
                                        OnLoad="DetailsViewInsertStockTakeGroupLoc_Load">
                                        <Fields>
                                            <asp:TemplateField HeaderText="StockTakeGroupLocId" InsertVisible="False" SortExpression="StockTakeGroupLocId">
                                                <InsertItemTemplate>
                                                    <asp:Button ID="AddStockTakeGroupLoc" runat="server" CommandName="Insert" Text="Add" />
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="StockTakeGroupId">
                                                <InsertItemTemplate>
                                                    <asp:TextBox ID="DetailTextStockTakeGroupId" runat="server" Text='<%# Bind("StockTakeGroupId") %>'>
                                                    </asp:TextBox>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="LocationId">
                                                <InsertItemTemplate>
                                                    <asp:DropDownList ID="DropDownListLocationIdInsert" runat="server" DataSourceID="ObjectDataSourceLocation"
                                                        DataTextField="LocationId" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                                    </asp:DropDownList>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <InsertItemTemplate>
                                                    <asp:LinkButton ID="btnInsert" runat="server" CommandName="Insert" Text="Insert">
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                                        Text="Cancel" OnClick="btnCancel_Click">
                                                    </asp:LinkButton>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceStockTakeGroupLoc" runat="server" TypeName="StockTake"
                                        SelectMethod="StockTakeGroupLoc_Search" InsertMethod="StockTakeGroupLoc_Add"
                                        DeleteMethod="StockTakeGroupLoc_Delete" OnInserting="ObjectDataSourceStockTakeGroupLoc_Inserting"
                                        OnInserted="ObjectDataSourceStockTakeGroupLoc_Inserted">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="StockTakeGroupId" SessionField="StockTakeGroupId" Type="Int32" />
                                        </SelectParameters>
                                        <UpdateParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:Parameter Name="StockTakeGroupId" Type="Int32" />
                                            <asp:Parameter Name="LocationId" Type="Int32" />
                                        </UpdateParameters>
                                        <InsertParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:Parameter Name="StockTakeGroupId" Type="Int32" />
                                            <asp:Parameter Name="LocationId" Type="Int32" />
                                        </InsertParameters>
                                        <DeleteParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                        </DeleteParameters>
                                    </asp:ObjectDataSource>
                                    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" TypeName="Location" SelectMethod="ListLocations">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <div style="clear: left;">
                            </div>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelFromAisle" runat="server" Text="From Aisle"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListFromAisle" runat="server" DataSourceID="ObjectDataSourceAisle"
                                            DataValueField="Aisle" DataTextField="Aisle">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelToAisle" runat="server" Text="To Aisle"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListToAisle" runat="server" DataSourceID="ObjectDataSourceAisle"
                                            DataValueField="Aisle" DataTextField="Aisle">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelFromLevel" runat="server" Text="From Level:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListFromLevel" runat="server" DataSourceID="ObjectDataSourceLevel"
                                            DataValueField="Level" DataTextField="Level">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelToLevel" runat="server" Text="To Level:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListToLevel" runat="server" DataSourceID="ObjectDataSourceLevel"
                                            DataValueField="Level" DataTextField="Level">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <asp:ObjectDataSource ID="ObjectDataSourceAisle" runat="server" TypeName="StockTake"
                                SelectMethod="AislesList">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSourceLevel" runat="server" TypeName="StockTake"
                                SelectMethod="LevelsList">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:Button ID="ButtonLinkLocation" runat="server" Text="Next" OnClick="ButtonLinkLocation_Click" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
