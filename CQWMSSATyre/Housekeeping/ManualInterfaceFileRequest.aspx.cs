﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class Housekeeping_ManualInterfaceFileRequest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        //Dictionary<string, string> colValues = new Dictionary<string,string>();
        StringBuilder xml = new StringBuilder();

        xml.Append("<root>");
        xml.Append("<docTypeId>" + RadioButtonListFiles.SelectedValue + "</docTypeId>");

        foreach (GridViewRow row in GridViewFileRequest.Rows)
        {
            xml.Append(string.Format("<{0}>{1}</{0}>", GridViewFileRequest.DataKeys[row.DataItemIndex].Value, ((TextBox)row.FindControl("txtValue")).Text));
        }
        xml.Append("</root>");
        Housekeeping.ManualFileRequest_Send(Session["ConnectionStringName"].ToString(), xml.ToString());
        LabelSubmit.Text = "Request successfully sent at " + DateTime.Now.ToString();
    }
    protected void RadioButtonListFiles_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewFileRequest.DataBind();
    }
    protected void GridViewFileRequest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItem != null)
        {
            ((TextBox)e.Row.FindControl("txtValue")).MaxLength = (int)((System.Data.DataRowView)(e.Row.DataItem)).Row["ColumnLength"];
        }
    }
}
