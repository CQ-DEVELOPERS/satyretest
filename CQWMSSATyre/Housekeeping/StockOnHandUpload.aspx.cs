using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_StockOnHandUpload : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockOnHandUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";

        try
        {
            GridViewAuthorise.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockOnHandUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewAuthorise.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region ButtonReject_Click
    protected void ButtonReject_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReject_Click";

        try
        {
            CheckBox cb = new CheckBox();
            StockTake st = new StockTake();
            
            foreach(GridViewRow row in GridViewAuthorise.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    st.SOHDetailsReject(Session["ConnectionStringName"].ToString(),
                                            (int)Session["WarehouseId"],
                                            (DateTime)GridViewAuthorise.DataKeys[row.RowIndex].Values["ComparisonDate"],
                                            (int)GridViewAuthorise.DataKeys[row.RowIndex].Values["StorageUnitId"],
                                            (int)GridViewAuthorise.DataKeys[row.RowIndex].Values["BatchId"]);
            }

            GridViewAuthorise.DataBind();
            
            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockOnHandUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonReject_Click"

    #region ButtomAccept_Click
    protected void ButtomAccept_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtomAccept_Click";

        try
        {
            CheckBox cb = new CheckBox();
            StockTake st = new StockTake();

            foreach (GridViewRow row in GridViewAuthorise.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    st.SOHDetailsAccept(Session["ConnectionStringName"].ToString(),
                                            (int)Session["WarehouseId"],
                                            (DateTime)GridViewAuthorise.DataKeys[row.RowIndex].Values["ComparisonDate"],
                                            (int)GridViewAuthorise.DataKeys[row.RowIndex].Values["StorageUnitId"],
                                            (int)GridViewAuthorise.DataKeys[row.RowIndex].Values["BatchId"]);
            }

            GridViewAuthorise.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockOnHandUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtomAccept_Click"

    #region ButtomReload_Click
    protected void ButtomReload_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtomReload_Click";

        try
        {
            StockTake st = new StockTake();

            st.SOHReload(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"]);
            
            GridViewAuthorise.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockOnHandUpload" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtomReload_Click"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_StockOnHandUpload", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_StockOnHandUpload", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
