<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductAreaMasterUpload.aspx.cs" Inherits="Housekeeping_ProductAreaMasterUpload"
    Title="<%$ Resources:Default, ReportTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabDownload" HeaderText="<%$ Resources:Default, Upload%>">
            <ContentTemplate>
                <asp:Label ID="LabelFileUpload" runat="server" Text="Click Browse to find the file to upload:"></asp:Label>
                <asp:UpdatePanel ID="UpdatePanelFileUpload1" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="500" ToolTip="Click Browse to find the file to upload"/>
                            </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <br />
                                    <asp:Button ID="ButtonUpload" runat="server" Text="Save" OnClick="ButtonUpload_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <triggers>
                        <asp:postbacktrigger ControlID="ButtonUpload" />
                    </triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Download%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                DataTextField="Principal" DataValueField="PrincipalId">
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                SelectMethod="GetPrincipalParameter">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelArea" runat="server" Text="<%$ Resources:Default, Area%>"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownAreaList" runat="server" DataSourceID="ObjectDataSourceAreaList"
                                DataTextField="Area" DataValueField="AreaId" />
                            <asp:ObjectDataSource ID="ObjectDataSourceAreaList" runat="server" TypeName="StaticInfo"
                                SelectMethod="Area_Search">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelLinked" runat="server" Text="Linked"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbLinked" runat="server" Checked="true" />
                        </td>
                    </tr>
                </table>

                <asp:Label ID="LabelPrint" runat="server" Text="View and download the product area master:"></asp:Label>
                <asp:Button ID="ButtonPrint" runat="server" Text="View Master" OnClick="ButtonPrint_Click" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>

