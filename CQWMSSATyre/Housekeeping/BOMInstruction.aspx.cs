﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Housekeeping_BOMInstruction : System.Web.UI.Page
{
    private int _setCount = 0;
    private int _totalSets = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["BOMHeaderId"] != null)
            Session["BOMHeaderId"] = null;
        }
    }
    

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewBOMs.DataBind();
    }

   
    protected void LinkButtonProduct_Click(object sender, EventArgs e)
    {
        PanelBOMInstrustions.Visible = false;
        PanelPopup.Visible = true;
    }

    protected void ButtonCreateInstruction_Click(object sender, EventArgs e)
    {
        if (Session["BOMHeaderId"] != null)
        {
            int BOMHeaderId = Convert.ToInt32(Session["BOMHeaderId"]);
            StaticInfo.BOMInstruction_Add(Session["ConnectionStringName"].ToString(), BOMHeaderId, Convert.ToDecimal(TextBoxQuantity.Text), TextBoxOrderNumber.Text);

            GridViewInstruction.DataBind();
        }
        else
        {
            //error
        }
    }

    protected void ButtonReleaseInstruction_Click(object sender, EventArgs e)
    {
        if (Session["BOMInstructionId"] != null)
        {
            StaticInfo.BOMInstruction_Release(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["BOMInstructionId"]));
            Session["BOMInstructionId"] = null;
            GridViewInstruction.DataBind();
            GridViewBOMInstructionLines.DataBind();
        }
        else
        {
            LabelError.Text = "Please select an instruction";
        }
    }

    protected void GridViewBOMs_SelectedIndexChanged(object sender, EventArgs e)
    {
        LinkButtonProduct.Text = GridViewBOMs.SelectedDataKey["ProductCode"].ToString();
        Session["BOMHeaderId"] = GridViewBOMs.SelectedDataKey["BOMHeaderId"];
        PanelPopup.Visible = false;
        PanelBOMInstrustions.Visible = true;
        GridViewInstruction.DataBind();
    }

    protected void GridViewInstruction_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["BOMHeaderId"] = GridViewInstruction.SelectedDataKey["BOMHeaderId"];
        Session["BOMInstructionId"] = GridViewInstruction.SelectedDataKey["BOMInstructionId"];
        GridViewBOMInstructionLines.DataBind();
        LabelError.Text = "";
    }

    protected void GridViewBOMInstructionLines_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnl_InsLines.Visible = false;
        pnl_InsEdit.Visible = true;
        GridViewSubstitutions.DataBind();
    }
    protected void GridViewSubstitutions_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridView GridViewProducts = (GridView)e.Row.FindControl("GridViewProducts");
            int BOMLineId = Convert.ToInt32(((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[6]);
            int LineNumber = Convert.ToInt32(((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0]);
            _setCount += Convert.ToInt32(((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[2]);
            _totalSets = Convert.ToInt32(((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[3]);

            GridViewProducts.DataSource = StaticInfo.BOMInstructionSubstitutionProduct_Search(Session["ConnectionStringName"].ToString(), BOMLineId, LineNumber);
            GridViewProducts.DataBind();
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[2].Text = "Total:";
            e.Row.Cells[3].Text = _setCount.ToString() + " of " + _totalSets.ToString();
            if (_setCount != _totalSets)
            {
                ButtonUpdateProduct.Enabled = false;
            }
            else
            {
                ButtonUpdateProduct.Enabled = true;
            }
        }
    }

    protected void ButtonUpdateProduct_Click(object sender, EventArgs e)
    {
        pnl_InsLines.Visible = true;
        pnl_InsEdit.Visible = false;
        GridViewBOMInstructionLines.DataBind();
    }

    protected void ButtonCancel_Click(object sender, EventArgs e)
    {
        PanelBOMInstrustions.Visible = true;
        PanelPopup.Visible = false;
    }
}
