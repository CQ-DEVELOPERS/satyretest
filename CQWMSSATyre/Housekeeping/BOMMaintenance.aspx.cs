using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Housekeeping_BOMMaintenance : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        ProductSearchControl.OnSelectedIndexChange = new EventHandler(ProductSearchControl_OnSelectedIndexChange);
    }


    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewBOM.DataBind();
    }

    protected void ButtonAddBOM_Click(object sender, EventArgs e)
    {
        PanelProductSearch.Visible = true;
    }

    protected void ButtonAddBOMLine_Click(object sender, EventArgs e)
    {
        PanelProductSearch.Visible = true;
    }

    protected void ButtonAddSubstitution_Click(object sender, EventArgs e)
    {
        PanelProductSearch.Visible = true;
    }

    protected void ProductSearchControl_OnSelectedIndexChange(object sender, EventArgs e)
    {
        switch (Tabs.ActiveTabIndex)
        {
            case 0:
                FormViewBOM.ChangeMode(FormViewMode.Insert);
                FormViewBOM.DataBind(); 
                FormViewBOM.Visible = true;
                break;
            case 1:
                FormViewBOMLines.ChangeMode(FormViewMode.Insert);
                FormViewBOMLines.DataBind();
                FormViewBOMLines.Visible = true;
                break;
            case 2:
                FormViewSubstitution.ChangeMode(FormViewMode.Insert);
                FormViewSubstitution.DataBind();
                FormViewSubstitution.Visible = true;
                break;
        }
        PanelProductSearch.Visible = false;
    }
    
    protected void GridViewBOM_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["BOMHeaderId"] = GridViewBOM.SelectedDataKey["BOMHeaderId"];
        pnl_BOMSKUs.Visible = true;
        GridViewBOMSKUs.DataBind();

    }
     
    protected void GridViewBOMSKUs_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["BOMLineId"] = GridViewBOMSKUs.SelectedDataKey["BOMLineId"];
        PanelReplacements.Visible = true;
        GridViewSubstitutions.DataBind();
    }   
    
    protected void FormViewBOMLines_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        FormViewBOMLines.Visible = false;
    }

    protected void FormViewSubstitution_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        FormViewSubstitution.Visible = false;
    }
}
