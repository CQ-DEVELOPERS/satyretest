﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Housekeeping_StockTakeClose : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewStockTake_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewStockTake_SelectedIndexChanged";
        try
        {
            Session["StockTakeReferenceId"] = GridViewStockTake.SelectedDataKey["StockTakeReferenceId"];

            //       ObjectDataSourceLocation.DataBind();
            //      GridViewLocation.SelectedIndex = -1;
            //       PanelLocation.Visible = true;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }

    protected void ButtonAddStockTake_Click(object sender, EventArgs e)
    {
        DetailsViewInsertStockTake.Visible = true;
        GridViewStockTake.Visible = false;
    }

    protected void ObjectDataSourceStockTake_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsViewInsertStockTake.Visible = false;
        GridViewStockTake.Visible = true;
        //Session["StockTakeReferenceId"] = int.Parse(e.ReturnValue.ToString());
        //      PanelLocation.Visible = true;
        LabelError.Text = e.ReturnValue.ToString();
        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_StockTake, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }
    protected void ObjectDataSourceStockTake_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        string WarehouseId = ((TextBox)DetailsViewInsertStockTake.FindControl("DetailTextWarehouseId")).Text;
        string OperatorId = Session["OperatorId"].ToString();
        string StockTakeType = ((RadioButtonList)DetailsViewInsertStockTake.FindControl("RadioButtonStockTakeType")).SelectedValue;
        string StartDate = ((TextBox)DetailsViewInsertStockTake.FindControl("TextBoxStartDateInsert")).Text;


        e.InputParameters["warehouseId"] = int.Parse(WarehouseId);
        e.InputParameters["stockTakeType"] = StockTakeType;
        e.InputParameters["operatorId"] = int.Parse(OperatorId);
        e.InputParameters["startDate"] = StartDate;


    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        //ObjectDataSourceArea.Insert();

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertStockTake.Visible = false;
        GridViewStockTake.Visible = true;
    }

    protected void DetailsViewInsertStockTake_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((TextBox)DetailsViewInsertStockTake.FindControl("DetailTextWarehouseId")).Text = Session["WarehouseId"].ToString();
        }
    }
    protected void ButtonNext_Click(object sender, EventArgs e)
    {
        if (GridViewStockTake.SelectedIndex >= 0)
        {
            Session.Add("stockTakeReferenceId", GridViewStockTake.SelectedDataKey.Value);
            Response.Redirect("~/Housekeeping/StockTakeCreate.aspx");
            LabelError.Text = "";
        }
        else
        {
            LabelError.Text = "Please select a stock take reference number.";
        }
    }
}
