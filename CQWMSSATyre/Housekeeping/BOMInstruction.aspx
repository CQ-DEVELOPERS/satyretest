﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true" CodeFile="BOMInstruction.aspx.cs" Inherits="Housekeeping_BOMInstruction"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<% Resources:Default,BOMInstruction %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel ID="updatePanel_BOM" runat="server" EnableViewState="True">
        <ContentTemplate>
            <ajaxToolkit:TabContainer ID="TabContainerBOM" runat="server" ActiveTabIndex="0">
                <ajaxToolkit:TabPanel ID="TabPanelInstruction" runat="server" HeaderText='<%$ Resources:Default,Instruction %>'>
                    <ContentTemplate>
                        <asp:Panel ID="PanelBOMInstrustions" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelInstructionProductCode" Text='<%$ Resources:Default,ProductCode %>'
                                            runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="LinkButtonProduct" runat="server" Text="Select" OnClick="LinkButtonProduct_Click"
                                            CausesValidation="false"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" Text='<%$ Resources:Default,Quantity %>' runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxQuantity" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxQuantity"
                                            ErrorMessage="*" ValidationGroup="NewBom"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" Text='<%$ Resources:Default,OrderNumber %>' runat="server"></asp:Label>                                        
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxOrderNumber" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxOrderNumber"
                                            ErrorMessage="*" ValidationGroup="NewBom"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="ButtonCreateInstruction" ValidationGroup="NewBom" runat="server" Text="Create" OnClick="ButtonCreateInstruction_Click" />
                                        <asp:Button ID="ButtonReleaseInstruction" runat="server" Text="Release" OnClick="ButtonReleaseInstruction_Click" />
                                        <asp:Label ID="LabelError" runat="server" Text="" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:GridView ID="GridViewInstruction" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                DataSourceID="ObjectDataSourceBOMIns" DataKeyNames="BOMInstructionId,BOMHeaderId"
                                OnSelectedIndexChanged="GridViewInstruction_SelectedIndexChanged">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:CommandField ShowEditButton="True" />
                                    <asp:BoundField DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>' />
                                    <asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:Default,Quantity %>' />
                                    <asp:BoundField DataField="Description" HeaderText='<%$ Resources:Default,Description %>'
                                        ReadOnly="True" />
                                    <asp:BoundField DataField="Remarks" HeaderText='<%$ Resources:Default,Remarks %>'
                                        ReadOnly="True" />
                                    <asp:BoundField DataField="BOMType" HeaderText='<%$ Resources:Default,BOMType %>'
                                        ReadOnly="True" />
                                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>'
                                        ReadOnly="True" />
                                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>'
                                        ReadOnly="True" />
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceBOMIns" runat="server" SelectMethod="BOMInstruction_Search"
                                TypeName="StaticInfo" DeleteMethod="BOMInstruction_Delete" OldValuesParameterFormatString="original_{0}"
                                UpdateMethod="BOMInstruction_Update">
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:Parameter Name="original_BOMInstructionId" Type="Int32" />
                                </DeleteParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />                                    
                                </UpdateParameters>
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:Panel>
                        <asp:Panel ID="PanelPopup" runat="server" Visible="False">
                            <br />
                            <asp:Panel ID="PanelProductSearch" runat="server" Width="400px" BackColor="#EFEFEC">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelProductCode" runat="server" Text='<%$ Resources:Default,ProductCode %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelProduct" runat="server" Text='<%$ Resources:Default,Product %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelSKUCode" runat="server" Text='<%$ Resources:Default,Barcode %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxBarcode" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <asp:Button ID="ButtonSearch" runat="server" Text='<%$ Resources:Default,Search %>'
                                                OnClick="ButtonSearch_Click" />
                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                                TargetControlID="PanelProductSearch" Color="239, 239, 236" BorderColor="64, 64, 64"
                                Enabled="True" />
                            <br />
                            <asp:GridView ID="GridViewBOMs" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                DataKeyNames="ProductCode,BOMHeaderId" DataSourceID="ObjectDataSourceBOM" OnSelectedIndexChanged="GridViewBOMs_SelectedIndexChanged">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' />
                                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' />
                                    <asp:BoundField DataField="BarCode" HeaderText='<%$ Resources:Default,Barcode %>' />
                                    <asp:BoundField DataField="Description" HeaderText='<%$ Resources:Default,Description %>' />
                                    <asp:BoundField DataField="BOMType" HeaderText='<%$ Resources:Default,BOMType %>' />
                                    <asp:TemplateField HeaderText='<%$ Resources:Default,Quantity %>'>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelQuantity" runat="server" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxQuantity" runat="server" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                        <asp:ObjectDataSource ID="ObjectDataSourceBOM" runat="server" SelectMethod="BOMHeader_Search"
                            TypeName="StaticInfo">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:ControlParameter ControlID="TextBoxProductCode" Name="productCode" PropertyName="Text"
                                    Type="String" />
                                <asp:ControlParameter ControlID="TextBoxProduct" Name="product" PropertyName="Text"
                                    Type="String" />
                                <asp:ControlParameter ControlID="TextBoxBarcode" Name="barcode" PropertyName="Text"
                                    Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="TabPanelInstructionLine" runat="server" HeaderText='<%$ Resources:Default,Lines %>'>
                    <ContentTemplate>
                        <br />
                        <asp:Panel ID="pnl_InsLines" runat="server">
                            <asp:GridView ID="GridViewBOMInstructionLines" runat="server" AllowPaging="True"
                                DataKeyNames="BOMLineId,BOMInstructionId" AllowSorting="True" AutoGenerateColumns="False"
                                EmptyDataText="There are currently no BOM instruction lines..." DataSourceID="ObjectDataSourceInsLines"
                                OnSelectedIndexChanged="GridViewBOMInstructionLines_SelectedIndexChanged">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" SelectText='<%$ Resources:Default,BOMReplacements %>' />
                                    <asp:BoundField DataField="LineNumber" HeaderText='<%$ Resources:Default,LineNumber %>' />
                                    <asp:BoundField DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>' />
                                    <asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:Default,Quantity %>' />
                                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' />
                                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>"
                                        SortExpression="AvailablePercentage"></asp:BoundField>
                                    <asp:BoundField ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>"
                                        SortExpression="AvailableQuantity"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceInsLines" runat="server" SelectMethod="BOMInstructionLine_Search"
                                TypeName="StaticInfo">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="BOMInstructionId" SessionField="BOMInstructionId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:Panel>
                        <asp:Panel ID="pnl_InsEdit" runat="server">
                            <asp:GridView ID="GridViewSubstitutions" runat="server" AutoGenerateColumns="False"
                                ShowFooter="True" OnRowDataBound="GridViewSubstitutions_RowDataBound" DataSourceID="ObjectDataSourceSubstitutes">
                                <Columns>
                                    <asp:CommandField ShowEditButton="True" />
                                    <asp:TemplateField HeaderText='<%$ Resources:Default,LineNumber %>'>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelLineNumber" Text='<%#Bind("LineNumber") %>' runat="server" />
                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("BOMLineId") %>'>
                                            </asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Product Groups">
                                        <ItemTemplate>
                                            <asp:GridView ID="GridViewProducts" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' />
                                                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' />
                                                    <asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:Default,Quantity %>' />
                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sets">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("CurrentSets") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CurrentSets") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceSubstitutes" runat="server" SelectMethod="BOMInstructionSubstitutions_Search"
                                TypeName="StaticInfo" OldValuesParameterFormatString="original_{0}" UpdateMethod="BOMInstructionLine_Update">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="BOMInstructionId" SessionField="BOMInstructionId" Type="Int32" />
                                    <asp:ControlParameter ControlID="GridViewBOMInstructionLines" Name="BOMLineId" PropertyName="SelectedValue"
                                        Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="BOMInstructionId" SessionField="BOMInstructionId" Type="Int32" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                            <br />
                            <asp:Button ID="ButtonUpdateProduct" runat="server" Text="Return" OnClick="ButtonUpdateProduct_Click" />
                        </asp:Panel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
