using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_StockTakeCreateSimple : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";
        
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["ParameterLocationId"] == null)
                    Session["ParameterLocationId"] = -1;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion Page_Load

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewLocation.PageIndex = 0;

        GridViewLocation_DataBind();
    }
    #endregion "ButtonSearch_Click"

    protected void GridViewLocation_DataBind()
    {
        Location loc = new Location();

        GridViewLocation.DataSource = loc.SearchLocationStockTake(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], TextBoxLocation.Text);

        GridViewLocation.DataBind();
    }

    #region GridViewLocation_PageIndexChanging
    protected void GridViewLocation_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewLocation.PageIndex = e.NewPageIndex;
            GridViewLocation_DataBind();
        }
        catch { }
    }
    #endregion "GridViewLocation_PageIndexChanging"

    #region Wizard1_NextButtonClick
    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
        theErrMethod = "Wizard1_NextButtonClick";

        try
        {
            StockTake stockTake = new StockTake();
            int jobId;
            string referenceNumber;

            if (Session["ReferenceNumber"] == null)
                referenceNumber = "-1";
            else
                referenceNumber = Session["ReferenceNumber"].ToString();

            jobId = stockTake.CreateStockTakeJob(Session["ConnectionStringName"].ToString(),
                                                    int.Parse(Session["WarehouseId"].ToString()),
                                                    int.Parse(Session["OperatorId"].ToString()),
                                                    "STL",
                                                    referenceNumber,
                                                    int.Parse(Session["StockTakeReferenceId"].ToString()));

            if (jobId == -1)
                return;
            else
                Session["JobId"] = jobId;

            if (Session["ReferenceNumber"] == null)
                Session["ReferenceNumber"] = "Job " + jobId.ToString();


            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewLocation.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    if (!stockTake.CreateStockTakeLocation(Session["ConnectionStringName"].ToString(),
                                                            (int)Session["WarehouseId"],
                                                            (int)Session["OperatorId"],
                                                            (int)Session["JobId"],
                                                            (int)GridViewLocation.DataKeys[row.RowIndex].Values["LocationId"],
                                                            (int)Session["StockTakeReferenceId"]))
                    {
                        Master.ErrorText = "Create Stock Take Failed.";
                        break;
                    }
            }

            GridView1.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Wizard1_NextButtonClick

    #region Wizard1_FinishButtonClick
    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        theErrMethod = "Wizard1_FinishButtonClick";

        try
        {
            Session.Remove("ReferenceNumber");
            Response.Redirect("StockTakeMaintenance.aspx");

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Wizard1_FinishButtonClick

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "StockTakeCreateLocation", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "StockTakeCreateLocation", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
