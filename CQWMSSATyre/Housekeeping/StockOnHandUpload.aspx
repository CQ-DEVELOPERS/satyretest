<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="StockOnHandUpload.aspx.cs" Inherits="Housekeeping_StockOnHandUpload" Title="<%$ Resources:Default, StockOnHandTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockOnHandTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StockOnHandAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode%>"></asp:Label>
    <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
    <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Product%>"></asp:Label>
    <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
    <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch%>"></asp:Label>
    <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
    <asp:Button ID="ButtonSearch" runat="server" Text='<%$ Resources:Default,ButtonSearch %>' OnClick="ButtonSearch_Click" />
    <br />
    <br />
    <asp:Button ID="ButtonSelect" runat="server" Text='<%$ Resources:Default,ButtonSelectAll %>' OnClick="ButtonSelect_Click" />
    <asp:Button ID="ButtonReject" runat="server" Text='<%$ Resources:Default,ButtonReject %>' OnClick="ButtonReject_Click" />
    <asp:Button ID="ButtonAccept" runat="server" Text='<%$ Resources:Default,ButtonAccept %>' OnClick="ButtomAccept_Click" />
    <asp:Button ID="ButtonReload" runat="server" Text='<%$ Resources:Default, Reload%>' OnClick="ButtomReload_Click" />
    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewAuthorise" runat="server" DataSourceID="ObjectDataSourceAuthorise" DataKeyNames="ComparisonDate,StorageUnitId,BatchId" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PageSize="30">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ComparisonDate" HeaderText='<%$ Resources:Default,ComparisonDate %>' SortExpression="ComparisonDate" />
                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' SortExpression="ProductCode" />
                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' SortExpression="Product" />
                    <asp:BoundField DataField="SKUCode" HeaderText='<%$ Resources:Default,SKUCode %>' SortExpression="SKUCode" />
                    <asp:BoundField DataField="Batch" HeaderText='<%$ Resources:Default,Batch %>' SortExpression="Batch" />
                    <asp:BoundField DataField="CurrentQuantity" HeaderText='<%$ Resources:Default,CurrentQuantity %>' SortExpression="CurrentQuantity" />
                    <asp:BoundField DataField="PreviousQuantity" HeaderText='<%$ Resources:Default,PreviousQuantity %>' SortExpression="PreviousQuantity" />
                    <asp:BoundField DataField="Variance" HeaderText='<%$ Resources:Default,Variance %>' SortExpression="Variance" />
                    <asp:BoundField DataField="CurrentValue" HeaderText='<%$ Resources:Default,CurrentValue %>' SortExpression="CurrentValue" />
                    <asp:BoundField DataField="PreviousValue" HeaderText='<%$ Resources:Default,PreviousValue %>' SortExpression="PreviousValue" />
                    <asp:BoundField DataField="PriceVariance" HeaderText='<%$ Resources:Default,PriceVariance %>' SortExpression="PriceVariance" />
                    <asp:BoundField DataField="Sent" HeaderText='<%$ Resources:Default,Sent %>' SortExpression="Sent" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceAuthorise" runat="server" TypeName="StockTake" SelectMethod="ViewSOHDetails">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="String" />
                    <asp:ControlParameter Name="productCode" ControlID="TextBoxProductCode" Type="String" />
                    <asp:ControlParameter Name="product" ControlID="TextBoxProduct" Type="String" />
                    <asp:ControlParameter Name="batch" ControlID="TextBoxBatch" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonReject" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonAccept" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonReload" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

