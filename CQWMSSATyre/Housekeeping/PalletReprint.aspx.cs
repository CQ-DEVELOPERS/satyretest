using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_PalletReprint : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            if (!Page.IsPostBack)
            {
                //Sets the menu for Housekeeping
                Session["MenuId"] = 5;
            }
            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Pallet Reprint " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewSearch.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Pallet Reprint " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSearch_Click"

    #region ButtonRePrint_Click
    protected void ButtonRePrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRePrint_Click";
        try
        {
            string instructionTypeCode = Request.QueryString["Type"];

            if (instructionTypeCode == "" || instructionTypeCode == null)
                instructionTypeCode = "M";

            GetEditIndex();

            Session["LabelName"] = "Pallet Reprint.lbl";

            Session["FromURL"] = "~/Housekeeping/PalletReprint.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletReprint " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonRePrint_Click"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";

        try
        {
            int index = 0;

            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();
            Housekeeping hw = new Housekeeping();
            int palletid;
            while (index < GridViewSearch.Rows.Count)
            {
                GridViewRow checkedRow = GridViewSearch.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {

                    palletid = hw.PalletInsert(Session["ConnectionStringName"].ToString(), int.Parse(GridViewSearch.DataKeys[index].Values["StorageUnitBatchId"].ToString()), int.Parse(GridViewSearch.DataKeys[index].Values["PickLocationId"].ToString()));
                    if (int.Parse(palletid.ToString()) != -1)
                    {
                        rowList.Add(int.Parse(palletid.ToString()));
                    }
                    //insert into db and get the palletid to be returned...
                    //.ToString());
                    //int.Parse(GridViewSearch.DataKeys[index].Values["PickLocationId"].ToString());
                    

                }
                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletReprint " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndex"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Pallet_Reprint", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Pallet_Reprint", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
