﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true" CodeFile="BOMInstructionSO.aspx.cs" Inherits="Housekeeping_BOMInstruction"
    Theme="Default" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="../Common/DateRange.ascx" tagname="DateRange" tagprefix="uc1" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Title='<% Resources:Default,BOMInstruction %>'></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel ID="updatePanel_BOM" runat="server" EnableViewState="True">
        <ContentTemplate>
            <ajaxToolkit:TabContainer ID="TabContainerBOM" runat="server" ActiveTabIndex="0">
                <ajaxToolkit:TabPanel ID="TabPanelInstruction" runat="server" HeaderText='<%$ Resources:Default,Instruction %>'>
                    <ContentTemplate>
                        <asp:Panel ID="PanelBOMInstrustions" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelInstructionProductCode" Text='<%$ Resources:Default,ProductCode %>'
                                            runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="LinkButtonProduct" runat="server" Text="Select" OnClick="LinkButtonProduct_Click"
                                            CausesValidation="False"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" Text='<%$ Resources:Default,Quantity %>' runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxQuantity" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxQuantity"
                                            ErrorMessage="*" ValidationGroup="NewBom"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" Text='<%$ Resources:Default,OrderNumber %>' runat="server"></asp:Label>                                        
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxOrderNumber" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxOrderNumber"
                                            ErrorMessage="*" ValidationGroup="NewBom"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="ButtonCreateInstruction" ValidationGroup="NewBom" 
                                            runat="server" Text="Create" OnClick="ButtonCreateInstruction_Click" 
                                            Enabled="False" />
                                        <asp:Label ID="LabelError" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:GridView ID="GridViewInstruction" runat="server" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="true"
                                DataSourceID="ObjectDataSourceBOMIns" DataKeyNames="BOMInstructionId,BOMHeaderId"
                                OnSelectedIndexChanged="GridViewInstruction_SelectedIndexChanged" OnRowCommand="GridViewInstruction_RowCommand"
                                OnRowDataBound="GridViewInstruction_RowDataBound">
                                <Columns>
                                    <asp:CommandField ShowEditButton="True" />
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:ButtonField ButtonType="Button" CommandName="Release" Text='<%$ Resources:Default,Release %>' HeaderText='<%$ Resources:Default,Release %>' />
                                    <asp:BoundField DataField="ReleaseEnabled" ReadOnly="true" HeaderText="Packaging" />
                                    <asp:BoundField DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>' SortExpression="OrderNumber" />
                                    <asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:Default,Quantity %>' SortExpression="Quantity" />
                                    <asp:BoundField DataField="MaximumQuantity" HeaderText='<%$ Resources:Default,MaximumQuantity %>' ReadOnly="True" SortExpression="MaximumQuantity" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Remarks %>" SortExpression="Remarks">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="LabelRemarks" runat="server" Text='<%#Bind("Remarks") %>'></asp:Label>--%>
                                            <asp:TextBox ID="LabelRemarks" runat="server" Text='<%#Bind("Remarks") %>' TextMode="MultiLine" MaxLength="4000" Wrap="true" Height="100px" Width="300px" Enabled="false"></asp:TextBox>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxRemarks" runat="server" Text='<%#Bind("Remarks") %>' TextMode="MultiLine" MaxLength="4000" Wrap="true" Height="100px" Width="300px"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="BOMType" HeaderText='<%$ Resources:Default,BOMType %>' ReadOnly="True" SortExpression="BOMType" />
                                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' ReadOnly="True" SortExpression="ProductCode" />
                                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' ReadOnly="True" SortExpression="Product" />
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceBOMIns" runat="server" SelectMethod="BOMInstruction_Search"
                                TypeName="StaticInfo" DeleteMethod="BOMInstruction_Delete" OldValuesParameterFormatString="original_{0}"
                                UpdateMethod="BOMInstruction_Update">
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:Parameter Name="original_BOMInstructionId" Type="Int32" />
                                </DeleteParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:Parameter Name="original_BOMInstructionId" Type="Int32" />
                                    <asp:Parameter Name="original_BOMHeaderId" Type="Int32" />
                                    <asp:Parameter Name="OrderNumber" Type="String" />
                                    <asp:Parameter Name="Remarks" Type="String" />
                                    <asp:Parameter Name="Quantity" Type="Decimal" />
                                </UpdateParameters>
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:Panel>
                        <asp:Panel ID="PanelPopup" runat="server" Visible="False">
                            <br />
                            <asp:Panel ID="PanelProductSearch" runat="server" Width="600px" BackColor="#EFEFEC">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelOrderNumberSearch" runat="server" Text='<%$ Resources:Default, OrderNumber %>' Width="100px"></asp:Label>
                                            <asp:TextBox ID="TextBoxOrderNumberSearch" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelBarcode" runat="server" Text='<%$ Resources:Default,Barcode %>' Width="100px"></asp:Label>
                                            <asp:TextBox ID="TextBoxBarcode" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelProductCode" runat="server" Text='<%$ Resources:Default,ProductCode %>' Width="100px"></asp:Label>
                                            <asp:TextBox ID="TextBoxProductCode" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelProduct" runat="server" Text='<%$ Resources:Default,Product %>' Width="100px"></asp:Label>
                                            <asp:TextBox ID="TextBoxProduct" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <uc1:DateRange ID="DateRange1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <asp:Button ID="ButtonSearch" runat="server" Text='<%$ Resources:Default,Search %>'
                                                OnClick="ButtonSearch_Click" />
                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                                TargetControlID="PanelProductSearch" Color="239, 239, 236" BorderColor="64, 64, 64"
                                Enabled="True" />
                            <br />
                            
                            <asp:Button ID="ButtonCombine" runat="server" Text="<%$ Resources:Default, Combine %>" OnClick="ButtonCombine_Click" />
                            <asp:Button ID="ButtonSplit" runat="server" Text="<%$ Resources:Default, Split %>" OnClick="ButtonSplit_Click" />
                            <asp:Button ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, Delete %>" OnClick="ButtonDelete_Click" />
                            <asp:GridView ID="GridViewBOMs" runat="server" AllowPaging="True" AllowSorting="true"
                                AutoGenerateColumns="False" BackImageUrl="~/App_Themes/Default/menu.png" EnableTheming="True"
                                DataKeyNames="ProductCode,BOMHeaderId,OutboundShipmentId,IssueId,BOMOrderNumber,ShipmentTotal"
                                DataSourceID="ObjectDataSourceBOM" 
                                OnSelectedIndexChanged="GridViewBOMs_SelectedIndexChanged" 
                                OnRowCommand="GridViewBOMs_RowCommand">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Select %>">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="OutboundShipmentId" HeaderText='<%$ Resources:Default,MO %>' SortExpression="OutboundShipmentId" />
                                    <asp:BoundField DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>' SortExpression="OrderNumber" />
                                    <asp:BoundField DataField="ExternalCompany" HeaderText='<%$ Resources:Default,Customer %>' SortExpression="ExternalCompany" />
                                    <asp:BoundField DataField="Status" HeaderText='<%$ Resources:Default,Status %>' SortExpression="Status" />
                                    <asp:BoundField DataField="DeliveryDate" HeaderText='<%$ Resources:Default,DeliveryDate %>' ApplyFormatInEditMode="true" DataFormatString="{0:d}" SortExpression="DeliveryDate" />
                                    <asp:ButtonField CommandName="ViewSOH" Text="View" />
                                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' SortExpression="ProductCode" />
                                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' SortExpression="Product" />
                                    <asp:BoundField DataField="Description" HeaderText='<%$ Resources:Default,Description %>' SortExpression="Description" />
                                    <asp:BoundField DataField="SOH" HeaderText='<%$ Resources:Default,SOH %>' SortExpression="SOH" />
                                    <asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:Default,Quantity %>' SortExpression="Quantity" />
                                    <asp:BoundField DataField="MaximumQuantity" HeaderText='<%$ Resources:Default,MaximumQuantity %>' SortExpression="MaximumQuantity" />
                                    <asp:BoundField DataField="Total" HeaderText='<%$ Resources:Default,Total %>' SortExpression="Total" />
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                        <asp:ObjectDataSource ID="ObjectDataSourceBOM" runat="server" TypeName="BOM" 
                            SelectMethod="SearchOrders" DeleteMethod="DeleteOrder">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:ControlParameter ControlID="TextBoxOrderNumberSearch" Name="orderNumber" PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="TextBoxProductCode" Name="productCode" PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="TextBoxProduct" Name="product" PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="TextBoxBarcode" Name="barcode" PropertyName="Text" Type="String" />
                                <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="ProductCode" Type="String" />
                                <asp:Parameter Name="BOMHeaderId" Type="Int32" />
                                <asp:Parameter Name="OutboundShipmentId" Type="Int32" />
                                <asp:Parameter Name="IssueId" Type="Int32" />
                                <asp:Parameter Name="BOMOrderNumber" Type="String" />
                                <asp:Parameter Name="ShipmentTotal" Type="Decimal" />
                            </DeleteParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="TabPanelInstructionLine" runat="server" HeaderText='<%$ Resources:Default,Lines %>'>
                    <ContentTemplate>
                        <br />
                        <asp:Panel ID="pnl_InsLines" runat="server">
                            <asp:GridView ID="GridViewBOMInstructionLines" runat="server" AllowPaging="false"
                                DataKeyNames="BOMLineId,BOMInstructionId" AllowSorting="True" AutoGenerateColumns="False"
                                EmptyDataText="There are currently no BOM instruction lines..." DataSourceID="ObjectDataSourceInsLines"
                                OnSelectedIndexChanged="GridViewBOMInstructionLines_SelectedIndexChanged">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" SelectText='<%$ Resources:Default,BOMReplacements %>' />
                                    <asp:BoundField DataField="LineNumber" HeaderText='<%$ Resources:Default,LineNumber %>' />
                                    <asp:BoundField DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>' />
                                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' />
                                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField ReadOnly="True" DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, RequiredQuantity %>" SortExpression="RequiredQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                    <asp:BoundField ReadOnly="True" DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>" SortExpression="AllocatedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                    <asp:BoundField ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>" SortExpression="AvailableQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                    <asp:BoundField ReadOnly="True" DataField="ManufacturingQuantity" HeaderText="<%$ Resources:Default, ManufacturingQuantity %>" SortExpression="ManufacturingQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                    <asp:BoundField ReadOnly="True" DataField="RawQuantity" HeaderText="<%$ Resources:Default, RawQuantity %>" SortExpression="RawQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                    <asp:BoundField ReadOnly="True" DataField="ReceivingQuantity" HeaderText="<%$ Resources:Default, RawReceivingQuantity %>" SortExpression="ReceivingQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                    <asp:BoundField ReadOnly="True" DataField="ReplenishmentQuantity" HeaderText="<%$ Resources:Default, ReplenishmentQuantity %>" SortExpression="ReplenishmentQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                    <asp:BoundField ReadOnly="True" DataField="StockOnOrder" HeaderText="<%$ Resources:Default, StockOnOrder %>" SortExpression="StockOnOrder" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                    <asp:BoundField ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>" SortExpression="AvailablePercentage" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceInsLines" runat="server" SelectMethod="BOMInstructionLine_Search"
                                TypeName="BOM">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                    <asp:SessionParameter Name="BOMInstructionId" SessionField="BOMInstructionId" Type="Int32" />
                                    <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" />
                                    <asp:SessionParameter Name="IssueId" SessionField="IssueId" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:Panel>
                        <asp:Panel ID="pnl_InsEdit" runat="server" Visible="false">
                            <asp:GridView ID="GridViewSubstitutions" runat="server" AutoGenerateColumns="False"
                                ShowFooter="True" OnRowDataBound="GridViewSubstitutions_RowDataBound" DataSourceID="ObjectDataSourceSubstitutes">
                                <Columns>
                                    <asp:CommandField ShowEditButton="True" />
                                    <asp:TemplateField HeaderText='<%$ Resources:Default,LineNumber %>'>
                                        <ItemTemplate>
                                            <asp:Label ID="LabelLineNumber" Text='<%#Bind("LineNumber") %>' runat="server" />
                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("BOMLineId") %>'>
                                            </asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Product Groups">
                                        <ItemTemplate>
                                            <asp:GridView ID="GridViewProducts" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' />
                                                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' />
                                                    <asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:Default,Quantity %>' />
                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sets">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("CurrentSets") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CurrentSets") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceSubstitutes" runat="server" SelectMethod="BOMInstructionSubstitutions_Search"
                                TypeName="StaticInfo" OldValuesParameterFormatString="original_{0}" UpdateMethod="BOMInstructionLine_Update">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="BOMInstructionId" SessionField="BOMInstructionId" Type="Int32" />
                                    <asp:ControlParameter ControlID="GridViewBOMInstructionLines" Name="BOMLineId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="BOMInstructionId" SessionField="BOMInstructionId" Type="Int32" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                            <br />
                            <asp:Button ID="ButtonUpdateProduct" runat="server" Text="Return" OnClick="ButtonUpdateProduct_Click" />
                        </asp:Panel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText='<%$ Resources:Default,Packaging %>'>
                    <ContentTemplate>
                        <ajaxToolkit:Accordion ID="AccordionRegisterPallet" runat="server" SelectedIndex="0"
                            HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
                            FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
                            SuppressHeaderPostbacks="true">
                            <Panes>
                                <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                                    <Header>
                                        <a href="" class="accordionLink">1. Select a Product</a></Header>
                                    <Content>
                                        <asp:UpdatePanel runat="server" ID="UpdatePanelProductSearch">
                                            <ContentTemplate>
                                                <uc2:ProductSearch ID="ProductSearch1" runat="server" />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </Content>
                                </ajaxToolkit:AccordionPane>
                                <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                                    <Header>
                                        <a href="" class="accordionLink">2. Enter Quantity</a></Header>
                                    <Content>
                                        <asp:UpdatePanel runat="server" ID="UpdatePanelTextBoxQuantity">
                                            <ContentTemplate>
                                                <asp:Label ID="LabelQuantity" runat="server"></asp:Label>
                                                <asp:TextBox ID="TextBoxPackagingQty" runat="server"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Numbers" TargetControlID="TextBoxPackagingQty"></ajaxToolkit:FilteredTextBoxExtender>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </Content>
                                </ajaxToolkit:AccordionPane>
                            </Panes>
                        </ajaxToolkit:Accordion>
                        <asp:Button ID="ButtonInsertLine" runat="server" Text="Insert" OnClick="ButtonInsertLine_Click" />
                        <asp:Button ID="ButtonDeleteLine" runat="server" Text="Delete" OnClick="ButtonDeleteLine_Click" />
                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonDeleteLine" runat="server" ConfirmText="Press OK if you are sure you want to delete the selected row." TargetControlID="ButtonDeleteLine"></ajaxToolkit:ConfirmButtonExtender>
                        <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInboundLine">
                            <ContentTemplate>
                                <asp:Label ID="LabelErrorMsg" runat="server" Text="" EnableTheming="false" ForeColor="Red"></asp:Label>
                                <asp:GridView ID="GridViewInboundLine" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                    AutoGenerateSelectButton="true" AutoGenerateEditButton="true" DataKeyNames="BOMPackagingId"
                                    DataSourceID="ObjectDataSourceInboundLine">
                                    <Columns>
                                        <asp:BoundField DataField="ProductCode" ReadOnly="true" HeaderText="<%$ Resources:Default, ProductCode %>" />
                                        <asp:BoundField DataField="Product" ReadOnly="true" HeaderText="<%$ Resources:Default, Product %>" />
                                        <asp:BoundField DataField="SKUCode" ReadOnly="true" HeaderText="<%$ Resources:Default, SKUCode %>" />
                                        <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" />
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ButtonDeleteLine" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:ObjectDataSource ID="ObjectDataSourceInboundLine" runat="server" TypeName="BOM"
                            SelectMethod="SearchPackaging" UpdateMethod="UpdatePackaging" DeleteMethod="DeletePackaging">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Type="Int32" Name="BOMInstructionId" SessionField="BOMInstructionId" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="BOMPackagingId" Type="Int32"></asp:Parameter>
                                <asp:Parameter Name="Quantity" Type="Decimal"></asp:Parameter>
                            </UpdateParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="BOMPackagingId" Type="Int32"></asp:Parameter>
                            </DeleteParameters>
                        </asp:ObjectDataSource>
                        
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
