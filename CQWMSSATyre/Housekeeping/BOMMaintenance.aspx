<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="BOMMaintenance.aspx.cs" Inherits="Housekeeping_BOMMaintenance" Title=""
    StylesheetTheme="Default" Theme="Default" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="ucProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text='<%$ Resources:Default,BOMMaintenance %>'></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel ID="GroupUpdate" runat="server">
        <ContentTemplate>
            <ajaxToolkit:TabContainer runat="server" ID="Tabs" ActiveTabIndex="0">
                <ajaxToolkit:TabPanel runat="server" ID="TabBOMs" HeaderText='<%$ Resources:Default,BOM %>'>
                    <ContentTemplate>
                        <asp:UpdatePanel ID="updatePanel_BOM" runat="server" EnableViewState="False">
                            <ContentTemplate>
                                <asp:Panel ID="PanelSearch" runat="server" Width="450px" BackColor="#EFEFEC">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="LabelProductCode" runat="server" Text='<%$ Resources:Default,ProductCode %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="LabelProduct" runat="server" Text='<%$ Resources:Default,Product %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="LabelSKUCode" runat="server" Text='<%$ Resources:Default,Barcode %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxBarcode" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <asp:Button ID="ButtonAddBOM" runat="server" Text="Add" OnClick="ButtonAddBOM_Click" />
                                                <asp:Button ID="ButtonSearch" runat="server" Text='<%$ Resources:Default,Search %>'
                                                    OnClick="ButtonSearch_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                                    TargetControlID="PanelSearch" Radius="5" Color="239, 239, 236" BorderColor="64, 64, 64"
                                    Enabled="True" />
                                <br />
                                <asp:GridView ID="GridViewBOM" DataSourceID="ObjectDataSourceBOM" runat="server"
                                    DataKeyNames="BOMHeaderId" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                                    EmptyDataText="There are currently no BOMs..." OnSelectedIndexChanged="GridViewBOM_SelectedIndexChanged">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" />
                                        <asp:CommandField ShowEditButton="True" />
                                        <asp:BoundField DataField="productCode" HeaderText='<%$ Resources:Default,ProductCode %>'
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="product" HeaderText='<%$ Resources:Default,Product %>'
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="barcode" HeaderText='<%$ Resources:Default,Barcode %>'
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="description" HeaderText='<%$ Resources:Default,Description %>' />
                                        <asp:BoundField DataField="remarks" HeaderText='<%$ Resources:Default,Remarks %>' />
                                        <asp:BoundField DataField="instruction" HeaderText='<%$ Resources:Default,Instruction %>' />
                                        <asp:BoundField DataField="BOMType" HeaderText='<%$ Resources:Default,BOMType %>' />
                                        <asp:CommandField ShowDeleteButton="True" />
                                    </Columns>
                                </asp:GridView>
                                <asp:FormView ID="FormViewBOM" runat="server" DataSourceID="ObjectDataSourceBOM"
                                    Visible="False" DefaultMode="Insert">
                                    <InsertItemTemplate>
                                        <br />
                                        <p>
                                            Enter new BOM Line details:</p>
                                        <table style="font-size: medium;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelProductCode" Text='<%$ Resources:Default,ProductCode %>' runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="TextBoxProductCode" Text='<%# Session["ProductCode"] %>' runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelDescription" Text='<%$ Resources:Default,Description %>' runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TextBoxDescription" Text='<%# Bind("Description") %>' runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelRemarks" Text='<%$ Resources:Default,Remarks %>' runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TextBoxRemarks" Text='<%# Bind("Remarks") %>' runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelInstruction" Text='<%$ Resources:Default,Instruction %>' runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TextBoxInstruction" Text='<%# Bind("Instruction") %>' runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelBOMType" Text='<%$ Resources:Default,BOMType %>' runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TextBoxBOMType" Text='<%# Bind("BOMType") %>' runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <asp:Button ID="ButtonInsert" CommandName="Insert" Text="Insert" runat="server" />
                                                    <asp:Button ID="ButtonCancel" CommandName="Cancel" Text="Cancel" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </InsertItemTemplate>
                                </asp:FormView>
                                <asp:ObjectDataSource ID="ObjectDataSourceBOM" runat="server" TypeName="StaticInfo"
                                    SelectMethod="BOMHeader_Search" OldValuesParameterFormatString="original_{0}"
                                    InsertMethod="BOMHeader_Add" DeleteMethod="BOMHeader_Delete" 
                                    UpdateMethod="BOMHeader_Update">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="TextBoxProductCode" Type="String" Name="productCode" />
                                        <asp:ControlParameter ControlID="TextBoxProduct" Type="String" Name="product" />
                                        <asp:ControlParameter ControlID="TextBoxBarcode" Type="String" Name="barcode" />
                                    </SelectParameters>
                                    <DeleteParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                            Type="String" />
                                    </DeleteParameters>
                                    <UpdateParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                            Type="String" />
                                        <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32" />
                                    </UpdateParameters>
                                    <InsertParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                            Type="String" />
                                        <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32" />
                                    </InsertParameters>
                                </asp:ObjectDataSource>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonAddBOM" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="TabBOMSKUs" HeaderText='<%$ Resources:Default,BOMProducts %>' runat="server">
                    <ContentTemplate>
                        <asp:UpdatePanel ID="Updatepanel_BOMSKUs" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnl_BOMSKUs" runat="server" Visible="false"> 
                                    <br />
                                    <asp:GridView ID="GridViewBOMSKUs" DataSourceID="ObjectDataSourceBOMSKUs" runat="server"
                                        DataKeyNames="BOMLineId" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                                        EmptyDataText="There are currently no BOMLines..." OnSelectedIndexChanged="GridViewBOMSKUs_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" />
                                            <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>'
                                                ReadOnly="True" />
                                            <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>'
                                                ReadOnly="True" />
                                            <asp:BoundField DataField="Barcode" HeaderText='<%$ Resources:Default,Barcode %>'
                                                ReadOnly="True" />
                                            <asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:Default,Quantity %>' />
                                            <asp:CommandField ShowDeleteButton="True" />
                                        </Columns>
                                    </asp:GridView>
                                    <br />
                                    <asp:Button ID="ButtonAddBOMLine" runat="server" Text="Add" OnClick="ButtonAddBOMLine_Click" />
                                    <asp:FormView ID="FormViewBOMLines" runat="server" Visible="False" DefaultMode="Insert"
                                        DataSourceID="ObjectDataSourceBOMSKUs" OnItemCommand="FormViewBOMLines_ItemCommand">
                                        <InsertItemTemplate>
                                            <br />
                                            <p>
                                                Enter new BOM line details:</p>
                                            <table style="font-size: medium;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelProductCode" Text='<%$ Resources:Default,ProductCode %>' runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="TextBoxProductCode" Text='<%# Session["ProductCode"] %>' runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelQuantity" Text='<%$ Resources:Default,Quantity %>' runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxQuantity" Text='<%# Bind("quantity") %>' runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <asp:Button ID="ButtonInsert" CommandName="Insert" Text="Insert" runat="server" />
                                                        <asp:Button ID="ButtonCancel" CommandName="Cancel" Text="Cancel" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </InsertItemTemplate>
                                    </asp:FormView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceBOMSKUs" runat="server" SelectMethod="BOMLine_Search"
                                        TypeName="StaticInfo" InsertMethod="BOMLine_Add" DeleteMethod="BOMLine_Delete"
                                        OldValuesParameterFormatString="original_{0}">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="BOMHeaderId" SessionField="BOMHeaderId" Type="Int32" />
                                        </SelectParameters>
                                        <InsertParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32" />
                                            <asp:SessionParameter Name="BOMHeaderId" SessionField="BOMHeaderId" Type="Int32" />
                                        </InsertParameters>
                                        <DeleteParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                        </DeleteParameters>
                                    </asp:ObjectDataSource>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonAddBOMLine" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="TabReplacements" HeaderText='<%$ Resources:Default,BOMReplacements %>'
                    runat="server">
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatepanelReplacements" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="PanelReplacements" runat="server" Visible="false">
                                    <br />
                                    <asp:GridView ID="GridViewSubstitutions" DataSourceID="ObjectDataSourceSubstitutions"
                                        runat="server" DataKeyNames="BOMLineId, StorageUnitId, LineNumber" AllowPaging="True"
                                        AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="There are currently no substitutions...">
                                        <Columns>
                                            <asp:CommandField ShowEditButton="True" />
                                            <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' ReadOnly="True" />
                                            <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' ReadOnly="True" />
                                            <asp:BoundField DataField="Barcode" HeaderText='<%$ Resources:Default,Barcode %>' ReadOnly="True" />
                                            <asp:BoundField DataField="LineNumber" HeaderText='<%$ Resources:Default,LineNumber %>' />
                                            <asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:Default,Quantity %>' />
                                            <asp:CommandField ShowDeleteButton="True" />
                                        </Columns>
                                    </asp:GridView>
                                    <br />
                                    <asp:Button ID="ButtonAddSubstitution" runat="server" Text="Add" OnClick="ButtonAddSubstitution_Click" />
                                    <asp:FormView ID="FormViewSubstitution" runat="server" Visible="False" DefaultMode="Insert"
                                        DataSourceID="ObjectDataSourceSubstitutions" OnItemCommand="FormViewSubstitution_ItemCommand">
                                        <InsertItemTemplate>
                                            <br />
                                            <p>
                                                Enter new replacement details:</p>
                                            <table style="font-size: medium;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelProductCode" Text='<%$ Resources:Default,ProductCode %>' runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="TextBoxProductCode" Text='<%# Session["ProductCode"] %>' runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelLineNumber" Text='<%$ Resources:Default,LineNumber %>' runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxLineNumber" Text='<%# Bind("lineNumber") %>' runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelQuantity" Text='<%$ Resources:Default,Quantity %>' runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxQuantity" Text='<%# Bind("quantity") %>' runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <asp:Button ID="ButtonInsert" CommandName="Insert" Text="Insert" runat="server" />
                                                        <asp:Button ID="ButtonCancel" CommandName="Cancel" Text="Cancel" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </InsertItemTemplate>
                                    </asp:FormView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceSubstitutions" runat="server" SelectMethod="BOMProduct_Search"
                                        TypeName="StaticInfo" InsertMethod="BOMProduct_Add" UpdateMethod="BOMProduct_Update"
                                        DeleteMethod="BOMProduct_Delete" OldValuesParameterFormatString="original_{0}">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="BOMLineId" SessionField="BOMLineId" Type="Int32" />
                                        </SelectParameters>
                                        <InsertParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32" />
                                            <asp:SessionParameter Name="BOMLineId" SessionField="BOMLineId" Type="Int32" />
                                        </InsertParameters>
                                        <UpdateParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                        </UpdateParameters>
                                        <DeleteParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                        </DeleteParameters>
                                    </asp:ObjectDataSource>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonAddSubstitution" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
            <br />
            <asp:UpdatePanel ID="PanelProductSearch" runat="server" Visible="false">
                <ContentTemplate>
                    <ucProduct:ProductSearch ID="ProductSearchControl" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ProductSearchControl" />
                </Triggers>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
