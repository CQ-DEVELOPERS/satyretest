﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="TakeOnStock.aspx.cs" Inherits="Housekeeping_TakeOnStock"
    Title="<%$ Resources:Default, ReportTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabDownload" HeaderText="<%$ Resources:Default, Upload%>">
            <ContentTemplate>
                <asp:Label ID="LabelFileUpload" runat="server" Text="Click Browse to find the file to upload:"></asp:Label>
                <asp:UpdatePanel ID="UpdatePanelFileUpload1" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="500" ToolTip="Click Browse to find the file to upload"/>
                            </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <br />
                                    <asp:Button ID="ButtonUpload" runat="server" Text="Save" OnClick="ButtonUpload_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <triggers>
                        <asp:postbacktrigger ControlID="ButtonUpload" />
                    </triggers>
                </asp:UpdatePanel>
                 <telerik:RadGrid ID="RadGridTakeOnStock" runat="server"
                            AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceTakeOnStock"
                            PageSize="30" ShowGroupPanel="True" Skin="Metro"
                            OnItemCommand="RadGridTakeOnStock_ItemCommand">
                            <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                            <MasterTableView DataKeyNames="InterfaceId,InterfaceTableId,Status" DataSourceID="ObjectDataSourceTakeOnStock"
                                AutoGenerateColumns="False" EnableHeaderContextMenu="true" AllowFilteringByColumn="true">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" SortExpression="Status" UniqueName="Status"></telerik:GridBoundColumn>
                                    <telerik:GridButtonColumn Text="Reprocess" HeaderText="Reprocess" CommandName="Reprocess"></telerik:GridButtonColumn>
                                    <telerik:GridBoundColumn DataField="InterfaceMessage" HeaderText="InterfaceMessage" SortExpression="InterfaceMessage" UniqueName="InterfaceMessage"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InterfaceMessageCode" HeaderText="InterfaceMessageCode" SortExpression="InterfaceMessageCode" UniqueName="InterfaceMessageCode"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InterfaceTableId" HeaderText="InterfaceTableId" SortExpression="InterfaceTableId" UniqueName="InterfaceTableId"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InterfaceId" HeaderText="InterfaceId" SortExpression="InterfaceId" UniqueName="InterfaceId"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="RecordType" HeaderText="RecordType" SortExpression="RecordType" UniqueName="RecordType"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="RecordStatus" HeaderText="RecordStatus" SortExpression="RecordStatus" UniqueName="RecordStatus"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" UniqueName="ProductCode"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Location" HeaderText="Location" SortExpression="Location" UniqueName="Location"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Batch" HeaderText="Batch" SortExpression="Batch" UniqueName="Batch"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InsertDate" HeaderText="InsertDate" SortExpression="InsertDate" UniqueName="InsertDate"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProcessedDate" HeaderText="ProcessedDate" SortExpression="ProcessedDate" UniqueName="ProcessedDate"></telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                                        <Resizing AllowColumnResize="true"></Resizing>
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                    <GroupingSettings ShowUnGroupButton="True" />
                        </telerik:RadGrid>
                        <asp:ObjectDataSource ID="ObjectDataSourceTakeOnStock" runat="server" TypeName="InterfaceConsole"
                            SelectMethod="SearchTakeOnStockUpload">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Download%>">
            <ContentTemplate>
                <asp:Label ID="LabelPrint" runat="server" Text="View and download current Inbound master:"></asp:Label>
                <asp:Button ID="ButtonPrint" runat="server" Text="View Master" OnClick="ButtonPrint_Click" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>

