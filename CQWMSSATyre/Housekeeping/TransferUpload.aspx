<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="TransferUpload.aspx.cs" Inherits="Housekeeping_TransferUpload"
    Title="<%$ Resources:Default, ReportTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Label ID="LabelFileUpload" runat="server" Text="Click Browse to find the file to upload:"></asp:Label>
    <asp:UpdatePanel ID="UpdatePanelFileUpload1" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="500" ToolTip="Click Browse to find the file to upload"/>
                </td>
                </tr>
                <tr>
                    <td align="right">
                        <br />
                        <asp:Button ID="ButtonUpload" runat="server" Text="Save" OnClick="ButtonUpload_Click" />
                        <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server" TargetControlID="ButtonUpload" ConfirmText="Press OK to import Tranfer Order File." />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <triggers>
            <asp:postbacktrigger ControlID="ButtonUpload" />
        </triggers>
    </asp:UpdatePanel>
</asp:Content>

