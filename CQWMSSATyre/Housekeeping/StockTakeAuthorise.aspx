<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="StockTakeAuthorise.aspx.cs" Inherits="Housekeeping_StockTakeAuthorise" Title="<%$ Resources:Default, StockTakeAuthoriseTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockTakeAuthoriseTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StockTakeAuthoriseAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">

    <asp:Label ID="LabelJob" runat="server" Text="<%$ Resources:Default, JobId %>"></asp:Label>
    <asp:TextBox ID="TextBoxJob" runat="server" Text=""></asp:TextBox>

    <asp:DropDownList ID="DropDownListArea" runat="server" DataSourceID="ObjectDataSourceArea"
        DataTextField="Area" DataValueField="AreaId" AutoPostBack="true">
    </asp:DropDownList>
    <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="StockTake"
        SelectMethod="GetAuthoriseAreas">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Button ID="ButtomRefresh" runat="server" Text="<%$ Resources:Default, Refresh%>" OnClick="ButtomRefresh_Click" />
    <asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, Save %>" OnClick="ButtomSave_Click" />
    <asp:Button ID="ButtonAccept" runat="server" Text="<%$ Resources:Default, Accept%>" OnClick="ButtomAccept_Click" />
    <asp:Button ID="ButtonRecount" runat="server" Text="<%$ Resources:Default, Recount%>" OnClick="ButtonRecount_Click" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewAuthorise" runat="server" DataSourceID="ObjectDataSourceAuthorise" DataKeyNames="InstructionId" AutoGenerateColumns="false" AllowPaging="true" AllowSorting="true" PageSize="20">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                        <ItemTemplate>
                            <%--<asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>--%>
                            <asp:RadioButtonList ID="rbAccept" runat="server" RepeatColumns="2">
                                <asp:ListItem Text="Accept" Value="A"></asp:ListItem>
                                <asp:ListItem Text="Recount" Value="RC"></asp:ListItem>
                            </asp:RadioButtonList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="JobId" HeaderText='<%$ Resources:Default,JobId %>' SortExpression="JobId">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="LineNumber" HeaderText='<%$ Resources:Default,LineNumber %>' SortExpression="LineNumber">
                        <ItemStyle Wrap="false"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Location" HeaderText='<%$ Resources:Default,Location %>' SortExpression="Location">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' SortExpression="ProductCode">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' SortExpression="Product">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="SKUCode" HeaderText='<%$ Resources:Default,SKUCode %>' SortExpression="SKUCode">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Batch" HeaderText='<%$ Resources:Default,Batch %>' SortExpression="Batch">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ExpectedQuantity" HeaderText='<%$ Resources:Default,ExpectedQuantity %>' SortExpression="ExpectedQuantity">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="PreviousCount2" HeaderText='<%$ Resources:Default,PreviousCount2 %>' SortExpression="PreviousCount2">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="PreviousCount1" HeaderText='<%$ Resources:Default,PreviousCount1 %>' SortExpression="PreviousCount1">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="LatestCount" HeaderText='<%$ Resources:Default,LatestCount %>' SortExpression="LatestCount">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Variance" HeaderText='<%$ Resources:Default,Variance %>' SortExpression="Variance">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceAuthorise" runat="server" TypeName="StockTake" SelectMethod="ViewAuthoriseDetails">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="String" />
                    <asp:ControlParameter ControlID="DropDownListArea" DefaultValue="" Name="AreaId" PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter Name="jobId" ControlID="TextBoxJob" Type="String" />
                    <%--<asp:Parameter Name="TextBoxJob" Type="string" />--%>
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="InstructionId" Type="int32" />
                    <asp:Parameter Name="Status" Type="string" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtomRefresh" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonRecount" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonAccept" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="DropDownListArea" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

