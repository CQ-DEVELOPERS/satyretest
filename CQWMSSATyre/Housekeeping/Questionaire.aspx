<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="Questionaire.aspx.cs" Inherits="Housekeeping_Questionaire" Title="<%$ Resources:Default, MovementTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<ajaxToolkit:Tabcontainer runat="server" id="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabQuestionaires" HeaderText="Questionaires">
        <ContentTemplate>
        <asp:UpdatePanel ID="updatePanel_Questionaires"  runat="server" EnableViewState="false">
            <ContentTemplate>
                <asp:Panel ID="PanelSearch" runat="server" Width="450px" BackColor="#EFEFEC">
                <table>
                        <tr>
                            
                            
                            <td>
                                <asp:Label ID="LabelType" runat="server" Text='Type'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxQuestionaireType" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text='Description'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxDescription" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                          <tr>
                            <td colspan="2" align="right">
                                <asp:Button ID="ButtonAddQuestionaire" runat="server" Text="Add" OnClick="ButtonAddQuestionaire_Click" />
                            </td>
                        </tr>
                </table>
                </asp:Panel>    
                
                <asp:GridView ID="GridViewQuestionaire" DataSourceID="ObjectDataSourceQuestionaire" DataKeyNames="QuestionaireId"  runat="server"  
                            AllowPaging="True" AllowSorting="True" 
                            PageSize="10" AutoGenerateColumns="False" 
                            OnSelectedIndexChanged="GridViewQuestionaire_SelectedIndexChanged" ShowFooter="false" EmptyDataText="There are currently no questionaires...">
                            <Columns>
                            
                                <asp:TemplateField ShowHeader="False">
                                    <HeaderTemplate>
                                    <asp:Label ID="labelclick" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                            Text="Update" SkinID="linkButtonBlack"></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                            Text="Cancel" SkinID="linkButtonBlack"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Select"
                                            Text="Select" SkinID="linkButtonBlack"></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                            Text="Edit" SkinID="linkButtonBlack"></asp:LinkButton>
                                        
                                    </ItemTemplate>
                        
                            
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="QuestionaireId">
                                  <HeaderTemplate>
                                    <asp:Label ID="labelId" runat="server" Text="Questionaire Id" SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="LabelQuestionaireId" runat="server" Text='<%# Eval("QuestionaireId") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelQuestionaireId" runat="server" Text='<%# Bind("QuestionaireId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField SortExpression="QuestionaireType">
                                  <HeaderTemplate>
                                    <asp:Label ID="labelQT" runat="server" Text="Questionaire Type" SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="LabelQuestionaireType" runat="server" Text='<%# Bind("QuestionaireType") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelQuestionaireType" runat="server" Text='<%# Bind("QuestionaireType") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  
                                <asp:TemplateField  SortExpression="QuestionaireDescription">
                                  <HeaderTemplate>
                                    <asp:Label ID="labelS" runat="server" Text="Description" SkinID="GridviewTitle"></asp:Label>
                                    </HeaderTemplate>
                                    <EditItemTemplate>
                                         <asp:TextBox ID="TextBoxQuestionaireDescription" runat="server" Text='<%# Bind("QuestionaireDesc") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelQuestionaireDescription" runat="server" Text='<%# Bind("QuestionaireDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    
                               <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                
                            </Columns>
                            
                        </asp:GridView>
                        
                    <asp:ObjectDataSource ID="ObjectDataSourceQuestionaire" runat="server" TypeName="Questionaire"
                             SelectMethod="GetQuestionaire" UpdateMethod="UpdateQuestionaire" >
                    
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="int32" />
                    </SelectParameters>

                    <UpdateParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="int32" />
                            <asp:Parameter Name="QuestionaireId" Type="int32" />
                            <asp:Parameter Name="QuestionaireType" Type="string" />
                            <asp:Parameter Name="QuestionaireDesc" Type="string" />
                            
                     </UpdateParameters>
                        
                     <InsertParameters>
                     <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="statusId" Type="Int32" />
                    <asp:Parameter Name="productCode" Type="String" />
                    <asp:Parameter Name="product" Type="String" />
                    <asp:Parameter Name="barcode" Type="String" />
                    <asp:Parameter Name="minimumQuantity" Type="Decimal" DefaultValue="0" />
                    <asp:Parameter Name="reorderQuantity" Type="Decimal" DefaultValue="0" />
                    <asp:Parameter Name="maximumQuantity" Type="Decimal" DefaultValue="0"/>
                    <asp:Parameter Name="curingPeriodDays" Type="Int32" DefaultValue="0"/>
                    <asp:Parameter Name="shelfLifeDays" Type="Int32" DefaultValue="0"/>
                    <asp:Parameter Name="qualityAssuranceIndicator" Type="String" />
                     </InsertParameters>   
                        
                </asp:ObjectDataSource>
                
               
               <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                    TargetControlID="PanelSearch"
                    Radius="10"
                    Color="239, 239, 236"
                    BorderColor="64, 64, 64" Enabled="True" />
               <br />
                    
            </ContentTemplate>
        
          </asp:UpdatePanel>
          
        
          </ContentTemplate>
        
          </ajaxToolkit:TabPanel>
          <ajaxToolkit:TabPanel ID="Questions" HeaderText="Questions" runat="server">
        <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text='Category'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCategory" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server" Text='Code'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCode" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label6" runat="server" Text='Sequence'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxSequence" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" Text='Question Text'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxQuestionText" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label8" runat="server" Text='Question Type'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxType" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    
                          <tr>
                            <td colspan="2" align="right">
                                <asp:Button ID="Button1" runat="server" Text="Add" OnClick="ButtonAddQuestion_Click" />
                            </td>
                        </tr>
                </table>
        <asp:UpdatePanel ID="updatePanel1"  runat="server" EnableViewState="false">
            <ContentTemplate>
                <asp:GridView ID="GridviewQuestions" runat="server" DataSourceID="ObjectDataSourceQuestions" AutoGenerateColumns="false" >
                <Columns>
                                    
                        <asp:TemplateField ShowHeader="False">
                            <HeaderTemplate>
                            <asp:Label ID="labelclick" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                    Text="Update" SkinID="linkButtonBlack"></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                    Text="Cancel" SkinID="linkButtonBlack"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Select"
                                    Text="Select" SkinID="linkButtonBlack"></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                    Text="Edit" SkinID="linkButtonBlack"></asp:LinkButton>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField SortExpression="QuestionId">
                              <HeaderTemplate>
                                <asp:Label ID="labelId" runat="server" Text="Question Id" SkinID="GridviewTitle"></asp:Label>
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    
                                    <asp:Label ID="QuestionId" runat="server" Text='<%# Bind("QuestionId") %>'></asp:Label>
                                </EditItemTemplate>
                                <ItemTemplate>  
                                    <asp:Label ID="QuestionId" runat="server" Text='<%# Bind("QuestionId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField SortExpression="Category">
                              <HeaderTemplate>
                                <asp:Label ID="labelCategory" runat="server" Text="Category" SkinID="GridviewTitle"></asp:Label>
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    
                                    <asp:TextBox ID="LabelCategory" runat="server" Text='<%# Bind("Category") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LabelCategory2" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField SortExpression="Code">
                              <HeaderTemplate>
                                <asp:Label ID="labelCode" runat="server" Text="Code" SkinID="GridviewTitle"></asp:Label>
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    
                                    <asp:TextBox ID="TextBoxCode" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LabelCode2" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField SortExpression="Sequence">
                              <HeaderTemplate>
                                <asp:Label ID="labelSequence" runat="server" Text="Sequence" SkinID="GridviewTitle"></asp:Label>
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBoxSequence" runat="server" Text='<%# Bind("Sequence") %>'></asp:TextBox>
                                    
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LabelSequence" runat="server" Text='<%# Bind("Sequence") %>'></asp:Label>
                                </ItemTemplate>
                                
                            </asp:TemplateField>
                            
                            <asp:TemplateField SortExpression="Active">
                              <HeaderTemplate>
                                <asp:Label ID="labelActive" runat="server" Text="Active" SkinID="GridviewTitle"></asp:Label>
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox ID="checkBoxActive" runat="server" Checked='<%# Bind("Active") %>' />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="checkBoxActive" runat="server" Text='<%# Bind("Active") %>' Enabled="false" />
                                </ItemTemplate>
                                
                            </asp:TemplateField>
                            
                            <asp:TemplateField SortExpression="QuestionText">
                              <HeaderTemplate>
                                <asp:Label ID="labelQT" runat="server" Text="Question Text" SkinID="GridviewTitle"></asp:Label>
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    
                                    <asp:Textbox ID="TextBoxQuestionText" runat="server" Text='<%# Bind("QuestionText") %>'></asp:Textbox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LabelQuestionText" runat="server" Text='<%# Bind("QuestionText") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField SortExpression="QuestionType">
                              <HeaderTemplate>
                                <asp:Label ID="labelQType" runat="server" Text="Question Type" SkinID="GridviewTitle"></asp:Label>
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    
                                    <asp:Textbox ID="TextBoxQuestionType" runat="server" Text='<%# Bind("QuestionType") %>'></asp:Textbox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LabelQuestionType" runat="server" Text='<%# Bind("QuestionType") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:ObjectDataSource ID="ObjectDataSourceQuestions" runat="server" TypeName="Questionaire" 
                             SelectMethod="GetQuestions" UpdateMethod="UpdateQuestion" >
                    
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="int32" />
                        <asp:SessionParameter Name="QuestionaireId" SessionField="QuestionaireId" type="int32"/>
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="connectionStringName" Type="string" />
                        <asp:SessionParameter Name="QuestionaireId" SessionField="QuestionaireId" Type="int32" />
                        <asp:Parameter Name="QuestionId" Type="int32" />
                        <asp:Parameter Name="Category" Type="string" />
                        <asp:Parameter Name="Code" Type="string" />
                        <asp:Parameter Name="Sequence" Type="Int32" />
                        <asp:Parameter Name="Active" Type="int32" />
                        <asp:Parameter Name="QuestionText" Type="string" />
                        <asp:Parameter Name="QuestionType" Type="string" />
                       
                    </UpdateParameters>

                        
                </asp:ObjectDataSource>
         </ContentTemplate>
                </ajaxToolkit:TabPanel>
                
                </ajaxToolkit:Tabcontainer>
               
</asp:Content>