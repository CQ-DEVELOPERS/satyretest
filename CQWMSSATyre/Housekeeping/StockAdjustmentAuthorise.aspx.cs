﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class HousekeepingStockAdjustmentAuthorise : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    #endregion Page_Load

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
    }
    #endregion Page_LoadComplete

    #region RadButtonSearch_Click
    protected void RadButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            RadGridStockAdjustmentAuthorise.DataSource = SearchDetails();
        }
        catch (Exception ex)
        {
        }
    }
    #endregion RadButtonSearch_Click

    #region SearchDetails
    public DataSet SearchDetails()
    {
        string connectionStringName = Session["ConnectionStringName"].ToString();
        int warehouseId = int.Parse(Session["WarehouseId"].ToString());


        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StockAdjAuth_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, Session["FromDate"].ToString());
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, Session["ToDate"].ToString());

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchDetails
    protected void RadGridStockAdjustmentAuthorise_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "Accept")
        {
            Status status = new Status();
            LinkButton lb = e.CommandSource as LinkButton;

            foreach (GridDataItem item in RadGridStockAdjustmentAuthorise.Items)
            {
                if (item.RowIndex == e.Item.RowIndex)
                {
                    Session["InterfaceExportStockAdjustmentId"] = item.GetDataKeyValue("InterfaceExportStockAdjustmentId");

                    if (status.StockAdjustment(Session["ConnectionStringName"].ToString(), (int)Session["InterfaceExportStockAdjustmentId"], "A"))
                    {
                        RadGridStockAdjustmentAuthorise.DataBind();

                        Master.MsgText = "Received"; Master.ErrorText = "";
                    }
                    else
                    {
                        result = SendErrorNow("Received" + "_" + "Error p_Receipt_Update_Status_Received");
                        Master.MsgText = "";
                        Master.ErrorText = "Exception, please make sure there are no default batches and there is a delivery note number";
                    }

                    break;
                }
            }
        }
        if (e.CommandName == "Reject")
        {
            Status status = new Status();
            LinkButton lb = e.CommandSource as LinkButton;

            foreach (GridDataItem item in RadGridStockAdjustmentAuthorise.Items)
            {
                if (item.RowIndex == e.Item.RowIndex)
                {
                    Session["InterfaceExportStockAdjustmentId"] = item.GetDataKeyValue("InterfaceExportStockAdjustmentId");

                    if (status.StockAdjustment(Session["ConnectionStringName"].ToString(), (int)Session["InterfaceExportStockAdjustmentId"], "RJ"))
                    {
                        RadGridStockAdjustmentAuthorise.DataBind();

                        Master.MsgText = "Received"; Master.ErrorText = "";
                    }
                    else
                    {
                        result = SendErrorNow("Received" + "_" + "Error p_Receipt_Update_Status_Received");
                        Master.MsgText = "";
                        Master.ErrorText = "Exception, please make sure there are no default batches and there is a delivery note number";
                    }

                    break;
                }
            }
        }
    }

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocument", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocument", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}