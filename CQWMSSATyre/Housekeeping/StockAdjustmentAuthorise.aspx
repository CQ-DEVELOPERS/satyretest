﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="StockAdjustmentAuthorise.aspx.cs" Inherits="HousekeepingStockAdjustmentAuthorise" Title="<%$ Resources:Default, MovementTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Common/DateRange.ascx" TagPrefix="uc1" TagName="DateRange" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StockAdjustmentAuthoriseTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, HousekeepingModule%>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Orders%>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Lines%>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Jobs%>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Details%>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                <uc1:DateRange runat="server" ID="DateRange" />
                <telerik:RadButton ID="RadButtonSearch" runat="server" Text="<%$ Resources:Default, RecordType %>" OnClick="RadButtonSearch_Click"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridStockAdjustmentAuthorise" runat="server" Skin="Metro" GridLines="None"
                        AllowAutomaticUpdates="true" AllowMultiRowSelection="true" ClientSettings-Selecting-AllowRowSelect="true" AllowFilteringByColumn="false"
                        AllowSorting="True" AllowPaging="true" PageSize="30" ShowFooter="true" EnableLinqExpressions="true"
                        Width="1200px" OnItemCommand="RadGridStockAdjustmentAuthorise_ItemCommand">
                        <MasterTableView DataKeyNames="InterfaceExportStockAdjustmentId" CommandItemDisplay="TopAndBottom" AutoGenerateColumns="False" EnableHeaderContextMenu="true" AllowFilteringByColumn="true">
                            <Columns>
                                <telerik:GridButtonColumn DataTextField="Complete" Text="<%$ Resources:Default, Accept %>" HeaderText="<%$ Resources:Default, Accept %>" CommandName="Accept"></telerik:GridButtonColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="InterfaceExportStockAdjustmentId" HeaderText="Id" SortExpression="InterfaceExportStockAdjustmentId"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="RecordType" HeaderText="<%$ Resources:Default, RecordType %>" SortExpression="RecordType"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="RecordStatus" HeaderText="<%$ Resources:Default, RecordStatus %>" SortExpression="RecordStatus"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" SortExpression="Quantity"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="Weight" HeaderText="<%$ Resources:Default, Weight %>" SortExpression="Weight"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="Additional1" HeaderText="<%$ Resources:Default, FromWarehouseCode %>" SortExpression="Additional1"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="Additional2" HeaderText="<%$ Resources:Default, ToWarehouseCode %>" SortExpression="Additional2"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="InterfaceMessage" HeaderText="<%$ Resources:Default, remarks %>" SortExpression="InterfaceMessage"></telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, CreatedBy %>" ReadOnly="false" DataType="System.Int32" DataField="OperatorId">
                                <telerik:GridButtonColumn DataTextField="Reject" Text="<%$ Resources:Default, Reject %>" HeaderText="<%$ Resources:Default, Reject %>" CommandName="Reject"></telerik:GridButtonColumn>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                                            DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblOperator" runat="server" Text='<%# Bind("CreatedBy") %>'></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn ReadOnly="False" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings AllowKeyboardNavigation="true"></ClientSettings>
                    </telerik:RadGrid>

                    <asp:ObjectDataSource ID="ObjectDataSourceCreatedBy" runat="server" TypeName="CreatedBy" SelectMethod="GetCreatedBy">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </telerik:RadPageView>
        </telerik:RadMultiPage>
        </div>
</asp:Content>

