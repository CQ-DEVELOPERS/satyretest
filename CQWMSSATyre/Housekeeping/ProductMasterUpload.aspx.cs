using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Web.Services.Protocols;
using Microsoft.Reporting.WebForms;

public partial class Housekeeping_ProductMasterUpload : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";
    }
    #endregion Page_Load

    #region ButtonUpload_Click
    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        Master.MsgText = "The file was successfully imported.";
        Master.ErrorText = "";

        if (FileUpload1.HasFile)
        {
            try
            {
                string xml = "";
                System.IO.Stream s = FileUpload1.PostedFile.InputStream;
                BulkImport import = new BulkImport();

                using (StreamReader sr = new StreamReader(s))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] tmpArr = line.Split(Convert.ToChar(","));
                        if (tmpArr.Length >= 36)
                        {
                            xml = "<root>";

                            xml = xml + "<Line>";
                            int count = 0;

                            xml = xml + "<ProductCode>" + tmpArr[count] + "</ProductCode>";
                            count++;
                            xml = xml + "<Product>" + tmpArr[count].Replace("&", "&amp;") + "</Product>";
                            count++;
                            xml = xml + "<SKUCode>" + tmpArr[count] + "</SKUCode>";
                            count++;
                            xml = xml + "<SKU>" + tmpArr[count] + "</SKU>";
                            count++;
                            xml = xml + "<PalletQuantity>" + tmpArr[count] + "</PalletQuantity>";
                            count++;
                            xml = xml + "<Barcode>" + tmpArr[count] + "</Barcode>";
                            count++;
                            xml = xml + "<MinimumQuantity>" + tmpArr[count] + "</MinimumQuantity>";
                            count++;
                            xml = xml + "<ReorderQuantity>" + tmpArr[count] + "</ReorderQuantity>";
                            count++;
                            xml = xml + "<MaximumQuantity>" + tmpArr[count] + "</MaximumQuantity>";
                            count++;
                            xml = xml + "<CuringPeriodDays>" + tmpArr[count] + "</CuringPeriodDays>";
                            count++;
                            xml = xml + "<ShelfLifeDays>" + tmpArr[count] + "</ShelfLifeDays>";
                            count++;
                            xml = xml + "<QualityAssuranceIndicator>" + tmpArr[count] + "</QualityAssuranceIndicator>";
                            count++;
                            xml = xml + "<ProductType>" + tmpArr[count] + "</ProductType>";
                            count++;
                            xml = xml + "<ProductCategory>" + tmpArr[count] + "</ProductCategory>";
                            count++;
                            xml = xml + "<OverReceipt>" + tmpArr[count] + "</OverReceipt>";
                            count++;
                            xml = xml + "<RetentionSamples>" + tmpArr[count] + "</RetentionSamples>";
                            count++;
                            xml = xml + "<HostId>" + tmpArr[count] + "</HostId>";
                            count++;
                            xml = xml + "<PackCode>" + tmpArr[count] + "</PackCode>";
                            count++;
                            xml = xml + "<PackDescription>" + tmpArr[count] + "</PackDescription>";
                            count++;
                            xml = xml + "<Quantity>" + tmpArr[count] + "</Quantity>";
                            count++;
                            xml = xml + "<PackBarcode>" + tmpArr[count] + "</PackBarcode>";
                            count++;
                            xml = xml + "<Length>" + tmpArr[count] + "</Length>";
                            count++;
                            xml = xml + "<Width>" + tmpArr[count] + "</Width>";
                            count++;
                            xml = xml + "<Height>" + tmpArr[count] + "</Height>";
                            count++;
                            xml = xml + "<Volume>" + tmpArr[count] + "</Volume>";
                            count++;
                            xml = xml + "<NettWeight>" + tmpArr[count] + "</NettWeight>";
                            count++;
                            xml = xml + "<GrossWeight>" + tmpArr[count] + "</GrossWeight>";
                            count++;
                            xml = xml + "<TareWeight>" + tmpArr[count] + "</TareWeight>";
                            count++;
                            xml = xml + "<ProductCategory>" + tmpArr[count] + "</ProductCategory>";
                            count++;
                            xml = xml + "<PackingCategory>" + tmpArr[count] + "</PackingCategory>";
                            count++;
                            xml = xml + "<PickEmpty>" + tmpArr[count] + "</PickEmpty>";
                            count++;
                            xml = xml + "<StackingCategory>" + tmpArr[count] + "</StackingCategory>";
                            count++;
                            xml = xml + "<MovementCategory>" + tmpArr[count] + "</MovementCategory>";
                            count++;
                            xml = xml + "<ValueCategory>" + tmpArr[count] + "</ValueCategory>";
                            count++;
                            xml = xml + "<StoringCategory>" + tmpArr[count] + "</StoringCategory>";
                            count++;
                            xml = xml + "<PickPartPallet>" + tmpArr[count] + "</PickPartPallet>";

                            xml = xml + "</Line>";

                            xml = xml + "</root>";

                            if (!import.ProductMasterImport(Session["ConnectionStringName"].ToString(), xml))
                            {
                                Master.MsgText = "";
                                Master.ErrorText = "There was an processing the file : " + Server.MapPath(FileUpload1.FileName).ToString() + " " + xml.ToString();
                                break;
                            }
                        }
                    }
                }
                s.Close();

                if (!import.ProductMasterExecute(Session["ConnectionStringName"].ToString()))
                {
                    Master.MsgText = "";
                    Master.ErrorText = "There was an processing importing the record(s) - please confirm all data was imported";
                }
                return;
            }
            catch (Exception ex)
            {
                Master.ErrorText = "There was an error saving the file : " + ex.Message.ToString();
            }
        }
        else
        {
            Master.ErrorText = "There was no file selected to upload";
        }
    }
    #endregion ButtonUpload_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            Session["FromURL"] = "~/Housekeeping/ProductMasterUpload.aspx";

            Session["ReportName"] = "Product Master";

            ReportParameter[] RptParameters = new ReportParameter[3];

            // Create the ServerName report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocumentSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
