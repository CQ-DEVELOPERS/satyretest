using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Housekeeping_ProductMaintenance : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion Page_Load

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewProduct.DataBind();
    }
    #endregion ButtonSearch_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region GridViewProduct_SelectedIndexChanged
    protected void GridViewProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewProduct_SelectedIndexChanged";
        try
        {
            Session["ProductId"] = GridViewProduct.SelectedDataKey["ProductId"];
            
            ObjectDataSourceSku.DataBind();
            GridViewSku.SelectedIndex = -1;
            pnl_Sku.Visible = true;

            Session["StorageUnitId"] = "-1";
            ObjectDataSourcePacks.DataBind();
            ObjectDataSourceBatch.DataBind();
            ObjectDataSourceArea.DataBind();
            ObjectDataSourceStorageLocation.DataBind();

            Panel_Pack.Visible = false;
            Panel_Batch.Visible = false;
            Panel_Area.Visible = false;
            Panel_Location.Visible = false;


            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewProduct_SelectedIndexChanged

    #region GridViewSku_SelectedIndexChanged
    protected void GridViewSku_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewSku_SelectedIndexChanged";
        try
        {
            Session["StorageUnitId"] = GridViewSku.SelectedDataKey["StorageUnitId"];
            ObjectDataSourcePacks.DataBind();
            ObjectDataSourceBatch.DataBind();
            ObjectDataSourceArea.DataBind();
            ObjectDataSourceStorageLocation.DataBind();

            Panel_Pack.Visible = true;
            Panel_Batch.Visible = true;
            Panel_Area.Visible = true;
            Panel_Location.Visible = true;
            


            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewSku_SelectedIndexChanged

    protected void addStorageLocation_Click(object sender, EventArgs e)
    {
        StaticInfo.storageUnitLocation_Add(Session["ConnectionStringName"].ToString(), int.Parse(Session["StorageUnitId"].ToString()), int.Parse(Session["ParameterLocationId"].ToString()));
        //ObjectDataSourceStorageLocation.DataBind();
        GridViewLocation.DataBind();
    }

    protected void buttonAddstorageArea_Click(object sender, EventArgs e)
    {
        Session["MinimumQuantity"] = 0;
        Session["ReorderQuantity"] = 0;
        Session["MaximumQuantity"] = 0;

        StaticInfo.storageUnitArea_Add(Session["ConnectionStringName"].ToString(), int.Parse(Session["StorageUnitId"].ToString()), int.Parse(Session["AreaId"].ToString()), Decimal.Parse(Session["MinimumQuantity"].ToString()), Decimal.Parse(Session["ReorderQuantity"].ToString()), Decimal.Parse(Session["MaximumQuantity"].ToString()));
        GridViewArea.DataBind();
    }

    protected void buttonstorageUnitBatch_Click(object sender, EventArgs e)
    {
        StaticInfo.storageUnitBatch_Add(Session["ConnectionStringName"].ToString(), int.Parse(Session["StorageUnitId"].ToString()), int.Parse(Session["BatchId"].ToString()));
        
        GridViewBatch.DataBind();
    }

    protected void buttonPack_Click(object sender, EventArgs e)
    {
        int PackId = StaticInfo.pack_Add(Session["ConnectionStringName"].ToString(), int.Parse(Session["StorageUnitId"].ToString()), (int)Session["WarehouseId"]);
        GridViewPacks.DataBind();


        for (int i = 0; i < GridViewPacks.Rows.Count; i++)
        {
            if (GridViewPacks.DataKeys[i].Value.ToString() == PackId.ToString())
            {
                GridViewPacks.EditIndex = i;
            }
        }
        
    }

    protected void buttonaddproductsku_Click(object sender, EventArgs e)
    {
        StaticInfo.sku_Add(Session["ConnectionStringName"].ToString(), int.Parse(dropdownlistskus.SelectedValue.ToString()), int.Parse(Session["ProductId"].ToString()));
        GridViewSku.DataBind();
    }  

    protected void ButtonAddProduct_Click(object sender, EventArgs e)
    {
        GridViewProduct.ShowFooter = true;
    }

    protected void lbInsert_Click(object sender, EventArgs e)
    {
        ObjectDataSourceProduct.Insert();
        
        GridViewProduct.ShowFooter = false;
        
    }

    protected void LinkButtonCancel_Click(object sender, EventArgs e)
    {
        GridViewProduct.ShowFooter = false;
    }

    protected void ObjectDataSourceProduct_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {

        if (GridViewProduct.Rows.Count == 0)
            // We are inserting through the DetailsView in the EmptyDataTemplate
            return;

        string statusId = ((DropDownList)GridViewProduct.FooterRow.FindControl("DropDownListStatusNew")).SelectedValue.ToString();
        string productCode = ((TextBox)GridViewProduct.FooterRow.FindControl("Textboxproductcode")).Text;
        string product = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxProductNew")).Text;
        string barcode = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxbarcodeNew")).Text;
        string minimumQuantity = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxMinQuantityNew")).Text;
        string reorderQuantity = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxReorderNew")).Text;
        string maximumQuantity = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxMaxQuanNew")).Text;
        string curingPeriodDays = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxCurPerNew")).Text;
        string shelfLifeDays = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxShelfLifeNew")).Text;
        string qualityAssuranceIndicator = ((CheckBox)GridViewProduct.FooterRow.FindControl("chkQualityAssuranceIndicatorNew")).Checked.ToString();
        string productType = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxProductTypeNew")).Text;
        string overReceipt = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxOverReceiptNew")).Text;
        string hostId = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxHostIdNew")).Text;
        string retentionSamples = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxRetentionSamplesNew")).Text;
        string samples = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxSamplesNew")).Text;
        string parentProductCode = ((TextBox)GridViewProduct.FooterRow.FindControl("TextBoxParentProductCodeNew")).Text;
        string dangerousGoodsId = ((DropDownList)GridViewProduct.FooterRow.FindControl("DropDownListDangerousGoodsNew")).SelectedValue.ToString();
        string principalId = ((DropDownList)GridViewProduct.FooterRow.FindControl("DropDownListPrinciplalNew")).SelectedValue.ToString();

        e.InputParameters["statusId"] = int.Parse(statusId);
        e.InputParameters["productCode"] = productCode;
        e.InputParameters["product"] = product;
        e.InputParameters["barcode"] = barcode;

        Decimal? CminimumQuantity = null;
        if (!string.IsNullOrEmpty(minimumQuantity))
            CminimumQuantity = Decimal.Parse(minimumQuantity);
        e.InputParameters["minimumQuantity"] = CminimumQuantity;

        Decimal? CreorderQuantity = null;
        if (!string.IsNullOrEmpty(reorderQuantity))
            CreorderQuantity = Decimal.Parse(reorderQuantity);
        e.InputParameters["reorderQuantity"] = CreorderQuantity;

        Decimal? CmaximumQuantity = null;
        if (!string.IsNullOrEmpty(maximumQuantity))
            CmaximumQuantity = Decimal.Parse(maximumQuantity);
        e.InputParameters["maximumQuantity"] = CmaximumQuantity;

        int? CcuringPeriodDays = null;
        if (!string.IsNullOrEmpty(curingPeriodDays))
            CcuringPeriodDays = int.Parse(curingPeriodDays);
        e.InputParameters["curingPeriodDays"] = CcuringPeriodDays;

        int? CshelfLifeDays = null;
        if (!string.IsNullOrEmpty(shelfLifeDays))
            CshelfLifeDays = int.Parse(shelfLifeDays);
        e.InputParameters["shelfLifeDays"] = CshelfLifeDays;
        
        e.InputParameters["qualityAssuranceIndicator"] = qualityAssuranceIndicator;

        e.InputParameters["productType"] = productType;

        decimal? CoverReceipt = null;
        if (!string.IsNullOrEmpty(overReceipt))
            CoverReceipt = decimal.Parse(overReceipt);
        e.InputParameters["overReceipt"] = CoverReceipt;

        e.InputParameters["hostId"] = hostId;

        Decimal? CretentionSamples = null;
        if (!string.IsNullOrEmpty(retentionSamples))
            CretentionSamples = Decimal.Parse(retentionSamples);
        e.InputParameters["retentionSamples"] = CretentionSamples;

        Decimal? Csamples = null;
        if (!string.IsNullOrEmpty(samples))
            Csamples = Decimal.Parse(samples);
        e.InputParameters["samples"] = Csamples;

        e.InputParameters["parentProductCode"] = parentProductCode;

        int? CdangerousGoodsId = null;
        if (!string.IsNullOrEmpty(dangerousGoodsId))
            CdangerousGoodsId = int.Parse(dangerousGoodsId);
        e.InputParameters["dangerousGoodsId"] = CdangerousGoodsId;

        int? CprincipalId = null;
        if (!string.IsNullOrEmpty(principalId))
            CprincipalId = int.Parse(principalId);
        e.InputParameters["principalId"] = CprincipalId; 
        
    }

    protected void ObjectDataSourceProduct_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        Session["ProductId"] = int.Parse(e.ReturnValue.ToString());
        pnl_Sku.Visible = true;
        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_Products, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }

    #region GridViewBatchSearch_OnSelectedIndexChanged
    protected void GridViewBatchSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["BatchId"] = GridViewBatchSearch.SelectedDataKey["BatchId"];
        Session["Batch"] = GridViewBatchSearch.SelectedDataKey["Batch"];
    }
    #endregion GridViewBatchSearch_OnSelectedIndexChanged

    #region ButtonSearchStorage_Click
    protected void ButtonSearchStorage_Click(object sender, EventArgs e)
    {
        GridViewBatchSearch.PageIndex = 0;
        GridViewBatchSearch.DataBind();
    }
    #endregion "ButtonSearchStorage_Click"


    protected void ObjectDataSourceProduct_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {

    }
}
