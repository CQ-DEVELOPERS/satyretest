<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="PalletReprint.aspx.cs" Inherits="Housekeeping_PalletReprint" Title="<%$ Resources:Default, MovementTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PalletLabelReprint %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="Search for Stock">
            <ContentTemplate>
                <asp:Panel ID="PanelSearch" runat="server" Width="450px" BackColor="#EFEFEC">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="LabelProductCode" runat="server" Text='<%$ Resources:Default,ProductCode %>'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="LabelProduct" runat="server" Text='<%$ Resources:Default,Product %>'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelSKUCode" runat="server" Text='<%$ Resources:Default,SKUCode %>'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxSKUCode" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="LabelBatch" runat="server" Text='<%$ Resources:Default,Batch %>'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelPickLocation" runat="server" Text='<%$ Resources:Default,PickLocation %>'></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxPickLocation" runat="server"></asp:TextBox>
                            </td>
                            <td colspan="2" align="right">
                                <asp:Button ID="ButtonSearch" runat="server" Text='<%$ Resources:Default,Search %>' OnClick="ButtonSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                    TargetControlID="PanelSearch"
                    Radius="10"
                    Color="#EFEFEC"
                    BorderColor="#404040"
                    Corners="All" />
                <br />
                <asp:UpdatePanel ID="UpdatePanelSearch" runat="server">
                    <ContentTemplate>
                        <asp:Button id="ButtonRePrint" onclick="ButtonRePrint_Click" runat="server" Text="Print"></asp:Button>
                        <asp:GridView ID="GridViewSearch" runat="server" DataKeyNames="StorageUnitBatchId,PickLocationId" AutoGenerateColumns="false" AllowPaging="true" DataSourceID="ObjectDataSourceSearch">
                            <Columns>
                                <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Location" HeaderText='<%$ Resources:Default,Location %>' SortExpression="Location" />
                                <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' SortExpression="Product">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" HeaderText='<%$ Resources:Default,SKUCode %>' SortExpression="SKUCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKU" HeaderText='<%$ Resources:Default,SKU %>' SortExpression="SKU">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Batch" HeaderText='<%$ Resources:Default,Batch %>' SortExpression="Batch" />
                                
                                <asp:BoundField DataField="ActualQuantity" HeaderText='<%$ Resources:Default,ActualQuantity %>' SortExpression="ActualQuantity" />
                                <asp:BoundField DataField="AllocatedQuantity" HeaderText='<%$ Resources:Default,AllocatedQuantity %>' SortExpression="AllocatedQuantity" />
                                <asp:BoundField DataField="ReservedQuantity" HeaderText='<%$ Resources:Default,ReservedQuantity %>' SortExpression="ReservedQuantity" />
                             
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceSearch" runat="server"
                            TypeName="Housekeeping"
                            SelectMethod="PalletReprint">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:QueryStringParameter Name="instructionTypeCode" Type="string" QueryStringField="Type" DefaultValue="M" />
                                <asp:ControlParameter Name="productCode" Type="string" ControlID="TextBoxProductCode" DefaultValue="%" />
                                <asp:ControlParameter Name="product" Type="string" ControlID="TextBoxProduct" DefaultValue="%" />
                                <asp:ControlParameter Name="skuCode" Type="string" ControlID="TextBoxSKUCode" DefaultValue="%" />
                                <asp:ControlParameter Name="batch" Type="string" ControlID="TextBoxBatch" DefaultValue="%" />
                                <asp:ControlParameter Name="pickLocation" Type="string" ControlID="TextBoxPickLocation" DefaultValue="%" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>

