﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_StockTakeCreateCategory : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Init";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateCategory" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselect_Click";
        try
        {
            foreach (ListItem item in CheckBoxListCategory.Items)
            {
                item.Selected = true;
            }

            GetCategorys();

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateCategory" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ButtonDeselect_Click
    protected void ButtonDeselect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselect_Click";

        try
        {
            foreach (ListItem item in CheckBoxListCategory.Items)
            {
                item.Selected = false;
            }

            Session["CategoryList"] = null;

            Master.MsgText = "Deselect"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateCategory" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselect_Click

    #region GetCategorys
    protected void GetCategorys()
    {
        theErrMethod = "GetCategorys";

        try
        {
            ArrayList CategoryList = new ArrayList();

            foreach (ListItem item in CheckBoxListCategory.Items)
            {
                if (item.Selected)
                    CategoryList.Add(item.Value);
            }

            if (CategoryList.Count > 0)
                Session["CategoryList"] = CategoryList;
            else
                Session["CategoryList"] = null;


            Master.MsgText = "Get Category"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateCategory" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion GetCategorys

    #region Wizard1_NextButtonClick
    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
        theErrMethod = "Wizard1_NextButtonClick";
        try
        {
            if (Session["CategoryList"] == null)
            GetCategorys();

            if (Wizard1.ActiveStepIndex == 0 || Wizard1.ActiveStepIndex == 1)
            {
                if (Wizard1.ActiveStepIndex == 1)
                {
                    StockTake stockTake = new StockTake();
                    string jobId;
                    string referenceNumber;

                    if (Session["ReferenceNumber"] == null)
                        referenceNumber = "-1";
                    else
                        referenceNumber = Session["ReferenceNumber"].ToString();

                    jobId = stockTake.CreateStockTakeJob(Session["ConnectionStringName"].ToString(),
                                                            int.Parse(Session["WarehouseId"].ToString()),
                                                            int.Parse(Session["OperatorId"].ToString()),
                                                            "STP",
                                                            referenceNumber,
                                                            int.Parse(Session["StockTakeReferenceId"].ToString())).ToString();

                    if (jobId == "-1")
                        return;
                    else
                        Session["JobId"] = jobId;

                    Session["Category"] = TextBoxCategory;

                    if (Session["ReferenceNumber"] == null)
                        Session["ReferenceNumber"] = "Job " + jobId;

                    foreach (ListItem item in CheckBoxListCategory.Items)
                    {
                        if (item.Selected)
                            if (!stockTake.CreateStockTakeCategory(Session["ConnectionStringName"].ToString(),
                                                                    int.Parse(Session["WarehouseId"].ToString()),
                                                                    int.Parse(Session["OperatorId"].ToString()),
                                                                    int.Parse(Session["JobId"].ToString()),
                                                                    //Session["Category"].ToString(),
                                                                    item.Value,
                                                                    //int.Parse(Session["StorageUnitId"].ToString()),
                                                                    int.Parse(Session["StockTakeReferenceId"].ToString())))
                            {
                                //LabelErrorMsg.Text = "Create Stock Take Failed.";
                                break;
                            }
                    }
                }

                ArrayList categoryList = (ArrayList)Session["CategoryList"];
                
                if (Session["CategoryList"] != null)
                {
                    foreach (string Category in categoryList)
                    {
                        Session["Category"] = Category;
                        categoryList.Remove(Category);
                        CheckBoxListCategory.DataBind();

                        //if (CheckBoxListLocation.Items.Count < 1)
                        if (Session["JobId"] == null)
                            Wizard1.ActiveStepIndex = 3;
                        break;
                    }
                }

                GridView1.DataBind();
            }

            Master.MsgText = "Next"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateCategory" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Wizard1_NextButtonClick

    //#region GetProducts
    //protected void GetProducts()
    //{
    //    theErrMethod = "GetProducts";
    //    try
    //    {
    //        ArrayList productList = new ArrayList();
    //        CheckBox cb = new CheckBox();

    //        foreach (GridViewRow row in GridViewProductSearch.Rows)
    //        {
    //            cb = (CheckBox)row.FindControl("CheckBoxEdit");

    //            if (cb.Checked)
    //                productList.Add(GridViewProductSearch.DataKeys[row.RowIndex].Values["StorageUnitId"]);
    //        }

    //        if (productList.Count > 0)
    //            Session["ProductList"] = productList;
    //        else
    //            Session["ProductList"] = null;

    //        Master.MsgText = "Get Product List"; Master.ErrorText = "";
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("StockTakeCreateProduct" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }

    //}
    //#endregion GetProducts

    #region Wizard1_FinishButtonClick
    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        theErrMethod = "Wizard1_FinishButtonClick";
        try
        {
            Session.Remove("ReferenceNumber");
            Response.Redirect("StockTakeMaintenance.aspx");

        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateCategory" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Wizard1_FinishButtonClick

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_StockTakeCreateCategory", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_StockTakeCreateCategory", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
