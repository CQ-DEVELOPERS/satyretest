﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class Housekeeping_StockTakeGroup : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                //Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewStockTake_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewStockTakeGroup_SelectedIndexChanged";
        try
        {
            Session["StockTakeGroupId"] = GridViewStockTake.SelectedDataKey["StockTakeGroupId"];

            //Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeGroupMaintenance" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }
    }

    protected void ButtonAddStockTakeGroup_Click(object sender, EventArgs e)
    {
        DetailsViewInsertStockTakeGroup.Visible = true;
        GridViewStockTake.Visible = false;
    }

    protected void ObjectDataSourceStockTakeGroup_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsViewInsertStockTakeGroup.Visible = false;
        GridViewStockTake.Visible = true;
        //Session["StockTakeReferenceId"] = int.Parse(e.ReturnValue.ToString());
        //      PanelLocation.Visible = true;
        LabelError.Text = e.ReturnValue.ToString();
        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_StockTake, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }

    protected void ObjectDataSourceStockTakeGroup_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        string StockTakeGroupCode = ((TextBox)DetailsViewInsertStockTakeGroup.FindControl("DetailTextStockTakeGroupCode")).Text;
        string StockTakeGroup = ((TextBox)DetailsViewInsertStockTakeGroup.FindControl("DetailTextStockTakeGroup")).Text;

        e.InputParameters["stockTakeGroupCode"] = StockTakeGroupCode;
        e.InputParameters["stockTakeGroup"] = StockTakeGroup;

    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        //ObjectDataSourceArea.Insert();

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertStockTakeGroup.Visible = false;
        GridViewStockTake.Visible = true;
    }

    protected void DetailsViewInsertStockTakeGroup_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //((TextBox)DetailsViewInsertStockTakeGroup.FindControl("DetailTextStockTakeGroupCode")).Text = Session["StockTakeGroupCode"].ToString();
        }
    }
    protected void ButtonNext_Click(object sender, EventArgs e)
    {
        if (GridViewStockTake.SelectedIndex >= 0)
        {
            Session.Add("stockTakeGroupId", GridViewStockTake.SelectedDataKey.Value);
            Response.Redirect("~/Housekeeping/StockTakeGroupAisle.aspx");
            LabelError.Text = "";
        }
        else
        {
            LabelError.Text = "Please select a stock take group before proceeding";
        }
    }
    //protected void ButtonSkip_Click(object sender, EventArgs e)
    //{


    //    //Session.Add("stockTakeReferenceId", 0);
    //    //Response.Redirect("~/Housekeeping/StockTakeCreate.aspx");
    //    //LabelError.Text = "";

    //}
}
