<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManualPalletise.aspx.cs" Inherits="Housekeeping_ManualPalletise"
    Title="<%$ Resources:Default, ManualPalletiseTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ManualPalletiseTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ManualPalletiseAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewMovementLine">
        <ContentTemplate>
            <asp:GridView ID="GridViewMovementLine" runat="server"
                AllowPaging="true"
                DataKeyNames="StorageUnitBatchId,PickLocationId"
                AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ActualQuantity" HeaderText="<%$ Resources:Default, ActualQuantity %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ReservedQuantity" HeaderText="<%$ Resources:Default, ReservedQuantity %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonCreateLines" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonGetNextMovement" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <br />
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelLines" runat="server" Text="<%$ Resources:Default, LabelLines %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxLines" runat="server"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtenderTextBoxLines" runat="server" FilterType="Numbers" TargetControlID="TextBoxLines">
                </ajaxToolkit:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, LabelQuantity %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxQuantity" runat="server"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtenderTextBoxQuantity" runat="server" FilterType="Custom" ValidChars="0123456789." TargetControlID="TextBoxQuantity">
                </ajaxToolkit:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceReason"
                    DataTextField="Reason" DataValueField="ReasonId">
                </asp:DropDownList>
                
                <asp:ObjectDataSource id="ObjectDataSourceReason" runat="server"
                    TypeName="Reason"
                    SelectMethod="GetReasonsByType">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="MOW" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <asp:Button ID="ButtonCreateLines" runat="server" Text="Create Lines" OnClick="ButtonCreateLines_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonCreateLines" runat="server" TargetControlID="ButtonCreateLines" ConfirmText="<%$ Resources:Default, ConfirmButtonCreateLines %>">
                </ajaxToolkit:ConfirmButtonExtender>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="ButtonAutoLocations" runat="server" Text="Auto Allocation" OnClick="ButtonAutoLocations_Click" />
    <asp:Button ID="ButtonManualLocations" runat="server" Text="Manual Allocation" OnClick="ButtonManualLocations_Click" />
    <asp:Button ID="ButtonGetNextMovement" runat="server" Text="Next Movement" OnClick="ButtonGetNextMovement_Click" />
    <asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonDelete" runat="server" TargetControlID="ButtonDelete" ConfirmText="<%$ Resources:Default, ConfirmButtonDelete %>">
    </ajaxToolkit:ConfirmButtonExtender>
    <asp:Button id="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click" />
    <asp:Button id="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" />
    <asp:Button ID="ButtonElectronic" runat="server" Text="Electronic Release" OnClick="ButtonElectronic_Click" />
    <br />
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
        <ContentTemplate>
            <asp:GridView ID="GridViewInstruction" runat="server" AllowPaging="True" DataKeyNames="InstructionId,JobId" AutoGenerateColumns="False"
                DataSourceID="ObjectDataSourceInstruction" OnRowUpdating="GridViewInstruction_OnRowUpdating">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>">
                        <EditItemTemplate>
                            <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemStyle Wrap="False" />
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="PickLocation" ReadOnly="True" HeaderText="<%$ Resources:Default, PickLocation %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="StoreLocation" ReadOnly="True" HeaderText="<%$ Resources:Default, StoreLocation %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="PalletId" ReadOnly="True" HeaderText="<%$ Resources:Default, PalletId %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="CreateDate" ReadOnly="True" HeaderText="<%$ Resources:Default, CreateDate %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Batch" ReadOnly="True" HeaderText="<%$ Resources:Default, Batch %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="ECLNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, ECLNumber %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            
            <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server"
                TypeName="ManualPalletise"
                SelectMethod="GetMovements" OnSelecting="ObjectDataSourceInstruction_OnSelecting"
                UpdateMethod="UpdateMovements" OnUpdating="ObjectDataSourceInstruction_OnUpdating"
                DeleteMethod="DeleteMovements">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="InstructionId" Type="Int32" />
                    <asp:Parameter Name="JobId" Type="Int32" />  
                    <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
                    <asp:Parameter Name="PickLocationId" Type="Int32" />  
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="priorityId" Type="Int32" />
                    <asp:Parameter Name="quantity" Type="Decimal" />
                </UpdateParameters>
                <DeleteParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="InstructionId" Type="Int32" />
                    <asp:Parameter Name="JobId" Type="Int32" />  
                </DeleteParameters>
            </asp:ObjectDataSource>
            
            <asp:ObjectDataSource id="ObjectDataSourcePriority" runat="server"
                TypeName="Priority"
                SelectMethod="GetPriorities">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonCreateLines" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonAutoLocations" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonGetNextMovement" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
