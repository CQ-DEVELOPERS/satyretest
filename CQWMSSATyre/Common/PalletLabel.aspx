<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="PalletLabel.aspx.cs"
    Inherits="Common_PalletLabel"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="StorageUnitBatchIdSearch.ascx" TagName="StorageUnitBatchIdSearch"
    TagPrefix="uc1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default,ProductCode %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default,Product %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default,SKUCode %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxSKUCode" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelSKU" runat="server" Text="<%$ Resources:Default,SKU %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxSKU" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default,Batch %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelFromLocation" runat="server" Text="<%$ Resources:Default, FromLocation%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxFromLocation" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelToLocation" runat="server" Text="<%$ Resources:Default, ToLocation%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxToLocation" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td align="right">
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default,Search %>" OnClick="ButtonSearch_Click" />
            </td>
        </tr>
    </table>
    <asp:Button ID="ButtonSelect" runat="server" Text="<%$ Resources:Default, SelectAll%>" OnClick="ButtonSelect_Click" />
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, Print%>" OnClick="ButtonPrint_Click" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewProductSearch"
                runat="server"
                AllowPaging="True"
                AutoGenerateColumns="False"
                AutoGenerateSelectButton="False" 
                DataKeyNames="StorageUnitBatchId,LocationId,ActualQuantity"
                OnSelectedIndexChanged="GridViewProductSearch_OnSelectedIndexChanged"
                OnPageIndexChanging="GridViewProductSearch_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="StorageUnitBatchId" Visible="false" />
                    <asp:BoundField DataField="LocationId" Visible="false" />
                    <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default,Area %>" />
                    <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default,Location %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default,Product %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default,ProductCode %>" />
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default,SKUCode %>" />
                    <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default,SKU %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default,Batch %>" />
                    <asp:BoundField DataField="ActualQuantity" HeaderText="<%$ Resources:Default,ActualQuantity %>" />
                    <asp:BoundField DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default,AllocatedQuantity %>" />
                    <asp:BoundField DataField="ReservedQuantity" HeaderText="<%$ Resources:Default,ReservedQuantity %>" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

