using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_BatchNumberSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["BatchId"] == null)
                Session["BatchId"] = -1;
        }
    }
    #endregion Page_Load

    #region GridViewBatchSearch_OnSelectedIndexChanged
    protected void GridViewBatchSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["BatchId"] = GridViewBatchSearch.SelectedDataKey["BatchId"];
        Session["Batch"] = GridViewBatchSearch.SelectedDataKey["Batch"];
    }
    #endregion GridViewBatchSearch_OnSelectedIndexChanged

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewBatchSearch.PageIndex = 0;
        GridViewBatchSearch.DataBind();
    }
    #endregion "ButtonSearch_Click"
}
