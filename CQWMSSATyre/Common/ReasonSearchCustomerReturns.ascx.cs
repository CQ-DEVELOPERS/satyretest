﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ReasonSearchCustomerReturns : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["Reason"] == null)
                Session["Reason"] = -1;
            if (Session["ReasonCode"] == null)
                Session["ReasonCode"] = "";
        }
    }
    #endregion Page_Load

    #region GridViewReasonSearch_OnSelectedIndexChanged
    protected void GridViewReasonSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["Reason"] = GridViewReasonSearch.SelectedRow.Cells[1].Text;
        Session["ReasonCode"] = GridViewReasonSearch.SelectedRow.Cells[2].Text;
        Session["ReasonId"] = GridViewReasonSearch.SelectedDataKey["ReasonId"];
    }
    #endregion GridViewReasonSearch_OnSelectedIndexChanged

    #region GridViewReasonSearch_PageIndexChanging
    protected void GridViewReasonSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewReasonSearch.PageIndex = e.NewPageIndex;
            GridViewReasonSearch_DataBind();
        }
        catch { }
    }
    #endregion "GridViewReasonSearch_PageIndexChanging"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewReasonSearch.PageIndex = 0;

        GridViewReasonSearch_DataBind();
    }
    #endregion "ButtonSearch_Click"

    protected void GridViewReasonSearch_DataBind()
    {
        Reason Reason = new Reason();

        if (Session["ReasonId"] == null)
            Session["ReasonId"] = -1;

        GridViewReasonSearch.DataSource = Reason.GetReasonsByType(Session["ConnectionStringName"].ToString(), "CR%");

        GridViewReasonSearch.DataBind();
    }
}
