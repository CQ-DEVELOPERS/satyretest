using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_UnlockUser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ButtonUnlock_Click(object sender, EventArgs e)
    {
        try
        {
            LogonCredentials lc = new LogonCredentials();

            if (lc.UnlockUser(DropDownListOperator.SelectedItem.ToString()))
            {
                Master.ErrorText = "";
                Master.MsgText = "User succefully unlocked";
            }
            else
            {
                Master.MsgText = "";
                Master.ErrorText = "There was an error unlocking the user.";
            }

        }
        catch { }
    }
}
