using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using System.Web.Services.Protocols;
using ReportingService2010;
using System.Text;
using System.Windows.Forms;


public partial class Common_DatePeriod : System.Web.UI.UserControl
{

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        //this Gets the session parameters.
        CreateSubscriptionParameters();
    }
    #endregion

    #region DataSubscription Methods


    protected void CreateSubscriptionParameters() 
    {
        //New parameters from Session
        //Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = (Microsoft.Reporting.WebForms.ReportParameter[])Session["ReportParameters"];


        //get the report path
        string strReportPath = ConfigurationManager.AppSettings.Get("ReportPath").ToString();
        string strReportName = Session["ReportName"].ToString();
        string strReport = strReportPath + strReportName;

        //initialize Webservice 
        ReportingService2010.ReportingService2010 rs = new ReportingService2010.ReportingService2010();
        //get credentials
        rs.Credentials = System.Net.CredentialCache.DefaultCredentials;


        string report = strReport;
        
        bool forRendering = false;
        string historyID = null;
        ParameterValue[] values = null;
        
        ReportingService2010.DataSourceCredentials[] credentials = null;
        ReportingService2010.ItemParameter[] parameters = null;

        try
        {
            //get Reports Parameter Types via web service.
            parameters = rs.GetItemParameters(report, historyID, forRendering, values, credentials);


            ParameterValueOrFieldReference[] _rptparameters = new ParameterValueOrFieldReference[parameters.GetLength(0)];
            int i = 0;
            if (parameters != null)
            {
                foreach (ReportingService2010.ItemParameter rp in parameters)
                {
                    //check to see if report has any datetime fields
                    if (rp.ParameterTypeName != "DateTime")
                    {
                        ParameterValue paraVal = new ParameterValue();
                        paraVal.Name = rp.Name;
                       // paraVal.Value = RptParameters[i].Values[0].ToString();
                        _rptparameters[i] = paraVal;
                    }

                        //if so then remove the session parameters and set to Datasource alias field
                    else
                    {
                        pnl_Radiolist.Visible = true;
                        ParameterFieldReference paraVal = new ParameterFieldReference();
                        paraVal.ParameterName = rp.Name;
                        if (rp.Name == "FromDate")
                            paraVal.FieldAlias = "DateFrom";
                        else
                            paraVal.FieldAlias = "DateTo";
                        _rptparameters[i] = paraVal;

                    }
                    i++;
                }
                //store the new parameters list into a new session to be used later
                Session["ScheduleParameters"] = _rptparameters;
            }
        }

        catch (SoapException ex)
        {
            Response.Write(ex.Detail.InnerXml.ToString());
        }
    
    
    }

    //Create XML Schedule String.
    protected string CreateSchedule() 
    {
        string daynow = DateTime.Now.Day.ToString();
        string monthnow = DateTime.Now.Month.ToString();
        string yearnow = DateTime.Now.Year.ToString();


        if (DateTime.Now.Month < 10) 
        {
            monthnow = "0" + DateTime.Now.Month.ToString();
        }

        if (DateTime.Now.Day < 10)
        {
            daynow = "0" + DateTime.Now.Day.ToString();
        }
        //2008-06-25T03:00:00.000+02:00

        StringBuilder myXml = new StringBuilder();

        myXml.Append("<ScheduleDefinition>");
        myXml.Append("<StartDateTime>");
        myXml.Append(yearnow + "-" + monthnow + "-" + daynow + "T");

        string sthour;

        switch (rad_Schedule.SelectedValue)
        {
            //Done!!
            case "Hour":

                if (rad_hourly_AP.SelectedValue == "PM" && int.Parse(txt_Hourly_stHour.Text) < 12)
                    sthour = (int.Parse(txt_Hourly_stHour.Text) + 12).ToString();
                else
                {
                    sthour = int.Parse(txt_Hourly_stHour.Text).ToString();

                    if (int.Parse(txt_Hourly_stHour.Text) < 10)
                        sthour = "0" + int.Parse(txt_Hourly_stHour.Text).ToString();
                    

                }

                myXml.Append(sthour.ToString()+":"+txt_Hourly_stMin.Text+":00.000+02:00");
                myXml.Append("</StartDateTime>");

                myXml.Append("<MinuteRecurrence>");
                myXml.Append("<MinutesInterval>");
                myXml.Append(((int.Parse(txt_Hourly_Hour.Text) * 60) + int.Parse(txt_Hourly_Minute.Text)).ToString());
                myXml.Append("</MinutesInterval></MinuteRecurrence>");
                break;

            //Done!!
            case "Day":

                if (rad_Day_AP.SelectedValue == "PM" && int.Parse(txt_day_stHour.Text) < 12)
                    sthour = (int.Parse(txt_day_stHour.Text) + 12).ToString();
                else
                {
                    sthour = int.Parse(txt_day_stHour.Text).ToString();
                    if (int.Parse(txt_day_stHour.Text) < 10)
                        sthour = "0" + int.Parse(txt_day_stHour.Text).ToString();
                }

                myXml.Append(sthour.ToString() + ":" + txt_day_stMin.Text + ":00.000+02:00");
                myXml.Append("</StartDateTime>");
                

                if (rad_day_ffg.Checked) 
                {

                    myXml.Append("<WeeklyRecurrence>");
                    myXml.Append("<WeeksInterval>");
                    myXml.Append("1");
                    myXml.Append("</WeeksInterval>");

                    myXml.Append("<DaysOfWeek>");

                    for (int i = 0; i < chk_day_days.Items.Count; i++)
                    {
                        if (chk_day_days.Items[i].Selected)
                        {
                            myXml.Append("<" + chk_day_days.Items[i].Value.ToString() + ">true</" + chk_day_days.Items[i].Value.ToString() + ">");
                        }
                    }

                    myXml.Append("</DaysOfWeek>");
                    myXml.Append("</WeeklyRecurrence>");
                }

                if (rad_day_all.Checked)
                {
                    myXml.Append("<WeeklyRecurrence>");
                    myXml.Append("<WeeksInterval>");
                    myXml.Append("1");
                    myXml.Append("</WeeksInterval>");

                    myXml.Append("<DaysOfWeek>");                  
                    myXml.Append("<Monday>true</Monday>"); 
                    myXml.Append("<Tuesday>true</Tuesday>"); 
                    myXml.Append("<Wednesday>true</Wednesday>"); 
                    myXml.Append("<Thursday>true</Thursday>");
                    myXml.Append("<Friday>true</Friday>"); 
                    myXml.Append("</DaysOfWeek>");
                    myXml.Append("</WeeklyRecurrence>");
                
                }

                if (rad_day_repeat.Checked)
                {
                myXml.Append("<DailyRecurrence>");
                myXml.Append("<DaysInterval>" + txt_day_repeat.Text + "</DaysInterval>");
                myXml.Append("</DailyRecurrence>");
                }

                break;
                //Done!!
            case "Week":

                if (rad_week_AP.SelectedValue == "PM" && int.Parse(txt_week_stHour.Text) < 12)
                    sthour = (int.Parse(txt_week_stHour.Text) + 12).ToString();
                else
                {
                    sthour = int.Parse(txt_week_stHour.Text).ToString();
                    if (int.Parse(txt_week_stHour.Text) < 10)
                        sthour = "0" + int.Parse(txt_week_stHour.Text).ToString();
                }

                myXml.Append(sthour.ToString() + ":" + txt_week_stMin.Text + ":00.000+02:00");
                myXml.Append("</StartDateTime>");

                myXml.Append("<WeeklyRecurrence>");
                myXml.Append("<WeeksInterval>");
                myXml.Append(txt_week_repeat.Text);
                myXml.Append("</WeeksInterval>");

                myXml.Append("<DaysOfWeek>");

                for (int i = 0; i < chk_week_days.Items.Count; i++) 
                {
                    if (chk_week_days.Items[i].Selected) 
                    { 
                        myXml.Append("<"+chk_week_days.Items[i].Value.ToString()+">true</"+chk_week_days.Items[i].Value.ToString()+">");
                    }
                }

                myXml.Append("</DaysOfWeek>");
                myXml.Append("</WeeklyRecurrence>");
                break;

            case "Month":

                if (rad_month_AP.SelectedValue == "PM" && int.Parse(txt_month_stHour.Text) < 12)
                    sthour = (int.Parse(txt_month_stHour.Text) + 12).ToString();
                else
                {
                    sthour = int.Parse(txt_month_stHour.Text).ToString();
                    if (int.Parse(txt_month_stHour.Text) < 10)
                        sthour = "0" + int.Parse(txt_month_stHour.Text).ToString();
                }


                myXml.Append(sthour.ToString() + ":" + txt_month_stMin.Text + ":00.000+02:00");
                myXml.Append("</StartDateTime>");
                

                string mDOW = "";

                if (Rad_month_Week.Checked) 
                { 
                    myXml.Append("<MonthlyDOWRecurrence>");
                    myXml.Append("<WhichWeek>" + dd_month_week.SelectedValue.ToString() + "</WhichWeek>");
                    myXml.Append("<DaysOfWeek>");

                    for (int i = 0; i < chk_month_days.Items.Count; i++)
                    {
                        if (chk_month_days.Items[i].Selected)
                        {
                            myXml.Append("<" + chk_month_days.Items[i].Value.ToString() + ">true</" + chk_month_days.Items[i].Value.ToString() + ">");
                        }
                    }

                    myXml.Append("</DaysOfWeek>");
                    mDOW = "DOW";
                }

                if (rad_month_calDays.Checked)
                {
                    myXml.Append("<MonthlyRecurrence>");
                    myXml.Append("<Days>" + txt_month_calDays.Text + "</Days>");
                }

                myXml.Append("<MonthsOfYear>");
                for (int i = 0; i < chk_month_months.Items.Count; i++)
                {
                    if (chk_month_months.Items[i].Selected)
                    {
                        myXml.Append("<" + chk_month_months.Items[i].Value.ToString() + ">true</" + chk_month_months.Items[i].Value.ToString() + ">");
                    }
                }

                myXml.Append("</MonthsOfYear>");
                myXml.Append("</Monthly" + mDOW + "Recurrence>");
                break;
                //Done!!
            case "Once":

                if (rad_once_AP.SelectedValue == "PM" && int.Parse(txt_once_stHour.Text) < 12)
                    sthour = (int.Parse(txt_once_stHour.Text) + 12).ToString();
                else
                {
                    sthour = int.Parse(txt_once_stHour.Text).ToString();
                    if (int.Parse(txt_once_stHour.Text) < 10)
                        sthour = "0"+int.Parse(txt_once_stHour.Text).ToString();
                }
                myXml.Append(sthour.ToString() + ":" + txt_once_stMin.Text + ":00.000+02:00");
                myXml.Append("</StartDateTime>");
                break;
                
        }

        myXml.Append("</ScheduleDefinition>");

        return myXml.ToString();
    }

    //Create DataSubscription
    protected void CreateSubscription(string timeSchedule) 
    {
        //SetDatasourceConnectionString();

        ReportingService2010.ReportingService2010 rs = new ReportingService2010.ReportingService2010();
        rs.Credentials = System.Net.CredentialCache.DefaultCredentials;

        string strReportPath = ConfigurationManager.AppSettings.Get("ReportPath").ToString();
        string strReportName = Session["ReportName"].ToString();
        string strReport = strReportPath + strReportName;
        string report = strReport;
        string description = txt_Subject.Text + " " + txt_to.Text;

        // Set the extension setting as report server email.
        ExtensionSettings settings = new ExtensionSettings();
        settings.Extension = "Report Server Email";

        // Set the extension parameter values.
        ParameterValueOrFieldReference[] extensionParams = new ParameterValueOrFieldReference[8];

        ParameterValue to = new ParameterValue(); 
        to.Name = "TO";
        to.Value = txt_to.Text;
        extensionParams[0] = to;

        ParameterValue replyTo = new ParameterValue();
        replyTo.Name = "ReplyTo";
        replyTo.Value = txt_repto.Text ;
        extensionParams[1] = replyTo;

        ParameterValue includeReport = new ParameterValue();
        includeReport.Name = "IncludeReport";
        includeReport.Value = "True";
        extensionParams[2] = includeReport;

        ParameterValue renderFormat = new ParameterValue();
        renderFormat.Name = "RenderFormat";
        renderFormat.Value = "HTML4.0";
        extensionParams[3] = renderFormat;

        ParameterValue priority = new ParameterValue();
        priority.Name = "Priority";
        priority.Value = "NORMAL";
        extensionParams[4] = priority;

        ParameterValue subject = new ParameterValue();
        subject.Name = "Subject";
        subject.Value = txt_Subject.Text;
        extensionParams[5] = subject;

        ParameterValue comment = new ParameterValue();
        comment.Name = "Comment";
        comment.Value = txt_Comment.Text;
        extensionParams[6] = comment;

        ParameterValue includeLink = new ParameterValue();
        includeLink.Name = "IncludeLink";
        includeLink.Value = "True";
        extensionParams[7] = includeLink;

        settings.ParameterValues = extensionParams;

        // Create the data source for the delivery query.
        DataSource delivery = new DataSource();

        delivery.Name = "";

        DataSourceDefinition dataSourceDefinition = new DataSourceDefinition();
        dataSourceDefinition.ConnectString = Session["ReportConnectionString"].ToString();
        dataSourceDefinition.CredentialRetrieval = CredentialRetrievalEnum.Store;
        dataSourceDefinition.Enabled = true;
        dataSourceDefinition.EnabledSpecified = true;
        dataSourceDefinition.Extension = "SQL";
        dataSourceDefinition.ImpersonateUserSpecified = false;
        dataSourceDefinition.UserName = "sa";
        dataSourceDefinition.Password = "password";
        delivery.Item = dataSourceDefinition;

        // Create the fields list.
        Field[] fieldsList = new Field[2];
        fieldsList[0] = new Field();
        fieldsList[0].Name = "DateFrom";
        fieldsList[0].Alias = "DateFrom";
        fieldsList[1] = new Field();
        fieldsList[1].Name = "DateTo";
        fieldsList[1].Alias = "DateTo";

        // Create the data set for the delivery query.
        DataSetDefinition dataSetDefinition = new DataSetDefinition();
        dataSetDefinition.AccentSensitivitySpecified = false;
        dataSetDefinition.CaseSensitivitySpecified = false;
        dataSetDefinition.KanatypeSensitivitySpecified = false;
        dataSetDefinition.WidthSensitivitySpecified = false;
        dataSetDefinition.Fields = fieldsList;

        QueryDefinition queryDefinition = new QueryDefinition();

        if (pnl_Radiolist.Visible == true)
        {
            queryDefinition.CommandText = "EXECUTE p_SelectDatePeriod_byID " + rad_DatePeriod.SelectedValue;
        }
        else
            queryDefinition.CommandText = "EXECUTE p_SelectDatePeriod_byID 1";

        queryDefinition.CommandType = CommandType.Text.ToString();
        queryDefinition.Timeout = 45;
        queryDefinition.TimeoutSpecified = true;
        dataSetDefinition.Query = queryDefinition;

        DataSetDefinition results = new DataSetDefinition();

        bool changed;
        string[] paramNames;

        try
        {
            results = rs.PrepareQuery(delivery, dataSetDefinition, out changed, out paramNames);
        }
        catch (SoapException ex)
        {
            lbl_Message.Text =ex.Detail.InnerText.ToString();
        }


        DataRetrievalPlan dataRetrieval = new DataRetrievalPlan();
        dataRetrieval.DataSet = results;
        dataRetrieval.Item = dataSourceDefinition;
        // Set the event type and match data for the delivery.
        string eventType = "TimedSubscription";
        
        string matchData = timeSchedule;

        // Set the report parameter values.
        ParameterValueOrFieldReference[] parameters = (ParameterValueOrFieldReference[])Session["ScheduleParameters"];

        try
        {
            string subscriptionID = rs.CreateDataDrivenSubscription(report, settings, dataRetrieval, description, eventType, matchData, parameters);
        }
        catch (SoapException ex)
        {
            lbl_Message.Text =ex.Detail.InnerText.ToString();
        }
    }

#endregion

    #region Control Methods
    protected void rad_Schedule_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnl_Once.Visible= false;
        pnl_Month.Visible= false;
        pnl_Week.Visible= false;
        pnl_Day.Visible= false;
        pnl_Hourly.Visible = false;
        lbl_Message.Text = "";

        switch (rad_Schedule.SelectedValue)
        {
            case "Hour":
                  pnl_Hourly.Visible = true;
                break;
                
            case "Day":
                 pnl_Day.Visible = true;
                break;
                
            case "Week":
                 pnl_Week.Visible = true;
                break;
                
            case "Month":
                 pnl_Month.Visible = true;
                break;

            case "Once":
                pnl_Once.Visible = true;
                break;
        }
    }

    protected void but_CreateSubscription_Click(object sender, EventArgs e)
    {
        Boolean validate = ValidateControls();
        Boolean chkDates = true;
        lbl_ErrorMessage.Text = "";
        lbl_Message.Text = "";
        if (pnl_Radiolist.Visible == true) 
        { 
           chkDates = validateRadioList(rad_DatePeriod,lbl_ErrorMessage,"Please Select a date period");
        }

        if (!(validate && chkDates))
        {
            return;
        }

        //lbl_ErrorMessage.Text = "";
        string TimedSchedule = CreateSchedule();
        CreateSubscription(TimedSchedule);

        lbl_ErrorMessage.Text = "Thank you. Your Subscription has been created successfully.";
    }
    #endregion

    #region Validation Methods
    
        protected Boolean validateRadioList(System.Web.UI.WebControls.RadioButtonList rad_But, System.Web.UI.WebControls.Label SummaryGroup, string ErrorMessage) 
        {
            Boolean error = false;
            foreach (ListItem Li in rad_But.Items) 
            {
                if (Li.Selected == true)
                {
                    error = true; 
                   // break;
                }
            }
            if (!error)
            {
                SummaryGroup.Text += "</br>" + ErrorMessage;
            }
            
            return error;
        }

        protected Boolean validateTextBox(System.Web.UI.WebControls.TextBox txt_Field, System.Web.UI.WebControls.Label lbl_Field, System.Web.UI.WebControls.Label SummaryGroup, string ErrorMessage)
        {
            Boolean error = true;
            
            if (txt_Field.Text == "")
            {
                error = false;
            }
            
            if (!error)
            {
                SummaryGroup.Text += "</br> Please enter a value into the " + lbl_Field.Text +  ErrorMessage +" field";
            }
            
            return error;
        }

        protected Boolean validateCheckBoxList(System.Web.UI.WebControls.CheckBoxList chk_But, System.Web.UI.WebControls.Label SummaryGroup, string ErrorMessage)
        {
            
            Boolean error = false;
            
            foreach (ListItem Li in chk_But.Items)
            {
                if (Li.Selected == true)
                {
                    error = true;  
                   // break;
                }
            }
            if (!error) 
            {
                SummaryGroup.Text += "</br>" + ErrorMessage;
            }
            
            
            return error;

        }

        protected Boolean validateDropdownList(System.Web.UI.WebControls.DropDownList dd_List, System.Web.UI.WebControls.Label SummaryGroup, string ErrorMessage)
        {
            Boolean error = false ;

            foreach (ListItem Li in dd_List.Items)
            {
                if (Li.Selected == true)
                {
                    error = true;
                    // break;
                }
            }
            if (!error)
            {
                SummaryGroup.Text += "</br>" + ErrorMessage;
            }


            return error;
        }

        protected Boolean ValidateControls() 
        {
            Boolean error = false;

            switch (rad_Schedule.SelectedValue)
            {
                case "Hour":

                    error = (validateTextBox(txt_Hourly_Hour, lbl_hourly_Hour, lbl_Message, "") 
                            && validateTextBox(txt_Hourly_Minute, lbl_hourly_Minutes, lbl_Message, "")
                            && validateTextBox(txt_Hourly_stHour, lbl_hourly_stTime, lbl_Message, "hours")
                            && validateTextBox(txt_Hourly_stMin, lbl_hourly_stTime, lbl_Message, "minutes")
                            && validateRadioList(rad_hourly_AP, lbl_Message, "Please select AM/PM"));
                    
                    break;

                case "Day":
                    if (rad_day_ffg.Checked) 
                    {
                        error = (validateCheckBoxList(chk_day_days, lbl_Message, "Please Select day(s)")
                            && validateTextBox(txt_day_stHour, lbl_day_stTime, lbl_Message, "hours")
                            && validateTextBox(txt_day_stMin, lbl_day_stTime, lbl_Message, "minutes")
                            && validateRadioList(rad_Day_AP, lbl_Message, "Please select AM/PM"));

                    }
                    if (rad_day_all.Checked)
                    {
                        error = (validateTextBox(txt_day_stHour, lbl_day_stTime, lbl_Message, "hours")
                            && validateTextBox(txt_day_stMin, lbl_day_stTime, lbl_Message, "minutes")
                            && validateRadioList(rad_Day_AP, lbl_Message, "Please select AM/PM"));
                    }
                    if (rad_day_repeat.Checked)
                    {
                        error = (validateTextBox(txt_day_repeat, lbl_day_repeat, lbl_Message, "Please Select day(s)")
                            && validateTextBox(txt_day_stHour, lbl_day_stTime, lbl_Message, "hours")
                            && validateTextBox(txt_day_stMin, lbl_day_stTime, lbl_Message, "minutes")
                            && validateRadioList(rad_Day_AP, lbl_Message, "Please select AM/PM"));
                    }


                    break;

                case "Week":
                    error = (validateTextBox(txt_week_repeat, lbl_week_repeat, lbl_Message, "")
                        && validateCheckBoxList(chk_week_days,lbl_Message,"Please Select day(s)")
                          && validateTextBox(txt_week_stHour, lbl_week_stTime, lbl_Message, "hours")
                          && validateTextBox(txt_week_stMin, lbl_week_stTime, lbl_Message, "minutes")
                          && validateRadioList(rad_week_AP, lbl_Message, "Please select AM/PM"));
                    
                    break;

                case "Month":
                    error = validateCheckBoxList(chk_month_months, lbl_Message, "Please select months(s)");

                    if (Rad_month_Week.Checked)
                    {
                        error = (error && validateDropdownList(dd_month_week, lbl_Message, "Please select a week number")
                            && validateCheckBoxList(chk_month_days,lbl_Message,"Please select day(s)"));
                    }

                    if (rad_month_calDays.Checked)
                    {
                        error = (error && 
                                validateTextBox(txt_month_calDays,lbl_month_calDays,lbl_Message,""));
                    }

                    error = (error && validateTextBox(txt_month_stHour, lbl_month_stTime, lbl_Message, "hours")
                         && validateTextBox(txt_month_stMin, lbl_month_stTime, lbl_Message, "minutes")
                         && validateRadioList(rad_month_AP, lbl_Message, "Please select AM/PM"));
                    break;

                case "Once":
                    error = (error && validateTextBox(txt_once_stHour, lbl_Once_stTime, lbl_Message, "hours")
                        && validateTextBox(txt_once_stMin, lbl_Once_stTime, lbl_Message, "minutes")
                        && validateRadioList(rad_once_AP, lbl_Message, "Please select AM/PM"));
                    break;
            }
            return error;
        
        }


    #endregion


}
