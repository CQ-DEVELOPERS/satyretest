<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="BatchLabel.aspx.cs" Inherits="Common_ProductBatchLabel" Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/StorageUnitBatchIdSearch.ascx" TagName="StorageUnitBatchIdSearch"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default,ProductCode %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default,Product %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default,SKUCode %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxSKUCode" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelSKU" runat="server" Text="<%$ Resources:Default,SKU %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxSKU" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default,Batch %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default,Search %>"
                    OnClick="ButtonSearch_Click" />
            </td>
        </tr>
    </table>
    <asp:Button ID="ButtonSelect" runat="server" Text="<%$ Resources:Default, SelectAll%>"
        OnClick="ButtonSelect_Click" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewProductSearch" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                AutoGenerateSelectButton="False" DataKeyNames="StorageUnitBatchId,BatchId" OnSelectedIndexChanged="GridViewProductSearch_OnSelectedIndexChanged"
                OnPageIndexChanging="GridViewProductSearch_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                            <asp:HiddenField ID="HiddenNumberOfPallets" runat="server" Value='<%# Bind("NumberOfPallets") %>' />
                            <asp:HiddenField ID="HiddenReceiptLineId" runat="server" Value='<%# Bind("ReceiptLineId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="StorageUnitBatchId" Visible="false" />
                    <asp:BoundField DataField="BatchId" Visible="false" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default,Product %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default,ProductCode %>" />
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default,SKUCode %>" />
                    <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default,SKU %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default,Batch %>" />
                    <asp:BoundField DataField="NumberOfPallets" HeaderText="<%$ Resources:Default,NumberOfPallets %>" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Label ID="LabelQty" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
    <asp:TextBox ID="TextBoxQty" runat="server" Text="1"></asp:TextBox>
    <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" FilterType="Numbers"
        TargetControlID="TextBoxQty">
    </ajaxToolkit:FilteredTextBoxExtender>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, Print%>"
        OnClick="ButtonPrint_Click" />
</asp:Content>
