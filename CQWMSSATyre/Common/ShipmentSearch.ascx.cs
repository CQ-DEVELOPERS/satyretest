﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ShipmentSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                DropDownListOutboundDocumentType.DataBind();

                if (Session["ParameterShipmentId2"] == null)
                    Session["ParameterShipmentId2"] = -1;
                else
                    TextBoxOutboundShipmentId2.Text = Session["ParameterShipmentId2"].ToString();

                if (Session["OutboundDocumentTypeId"] == null)
                    Session["OutboundDocumentTypeId"] = int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString());
                else
                    DropDownListOutboundDocumentType.SelectedValue = Session["OutboundDocumentTypeId"].ToString();
            }

            if (TextBoxOutboundShipmentId2.Text == "{All}")
            {
                Session["ParameterShipmentId2"] = "-1";
                TextBoxOutboundShipmentId2.Text = "";
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxOutboundShipmentId2_TextChanged(object sender, EventArgs e)
    {
        Session["ParameterShipmentId2"] = TextBoxOutboundShipmentId2.Text;
    }

    protected void DropDownListOutboundDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["OutboundDocumentTypeId"] = int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString());
    }
}
