<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BatchSearch.ascx.cs" Inherits="Common_BatchSearch" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelChangeLevel" runat="server" Text="<%$ Resources:Default, ChangeLevel%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxChangeLevel" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
        </td>
    </tr>
</table>
<asp:GridView ID="GridViewBatchSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataSourceID="ObjectDataSourceBatch"
    DataKeyNames="BatchId,Batch"
    OnSelectedIndexChanged="GridViewBatchSearch_OnSelectedIndexChanged">
    <Columns>
        <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch%>" />
        <asp:BoundField DataField="ECLNumber" HeaderText="<%$ Resources:Default, ChangeLevel%>" />
        <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate%>" />
    </Columns>
</asp:GridView>

<asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch" SelectMethod="SearchBatchesByStorageUnit">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="StorageUnitId" SessionField="StorageUnitId" Type="Int32" />
        <asp:ControlParameter Name="batch" ControlID="TextBoxBatch" Type="String" />
        <asp:ControlParameter Name="eCLNumber" ControlID="TextBoxChangeLevel" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>