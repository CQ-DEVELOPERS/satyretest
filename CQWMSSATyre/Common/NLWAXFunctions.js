﻿//=======================================================================================
//  NLWAXFunctions.js handles sending print stream to client side printer.
//  Calls are made to NLWAX.ocx passing appropriate parameters.
//=======================================================================================


// convenience function for commonnly used getElementById call
function $(id) { return document.getElementById(id) }
//=======================================================================================

//var WAX = $('NLWAXForm');
var printerList = $('NLWAX1_hdfPrinterList');
var portList = $('NLWAX1_hdfPortList');
var isPrinting = $('NLWAX1_hdfIsPrinting');

//Check for null printer or port list when client's Print Spooler Service is not started 
if (printerList == null || portList == null) {
    alert('Failed retrieving client printers!\r\nPrinting will be disabled.')
}
else {

    // Retrieve client-side printer/port lists
    LoadPrinterList();
    LoadPortList();

    // hdfIsPrinting let's us know if printing has been requested  
    if (isPrinting.value == 'true') {

        var sendTo = $('NLWAX1_hdfSendTo');
        var label = $('NLWAX1_hdfLabel');
        var file = $('NLWAX1_hdfFile');
        var cBaseUrl = $('NLWAX1_hdfBaseURL');

        // Pass BaseURL to NLWAX.ocx
        if (BrowserDetect.browser == 'Explorer') {
            document.NLWAXForm.baseURL = cBaseUrl.value;
        } else {
            document.NLWAXForm.Set_BaseURL(cBaseUrl.value)
        }

        switch (sendTo.value) {
            case 'To Printer':
                PrintViaDriver();
                break;
            case 'To Port':
                PrintViaPort();
                break;
            default:
        }
        // Reset hdfIsPrinting
        isPrinting.value = 'false';
    }
}
//=======================================================================================

function LoadPrinterList() {
    // Calls NLWAX.oxc's GetPrinterList to retrieve list of client-side printers.
    // The returned data is stored in a HiddenField control
    printerList.value = document.NLWAXForm.GetPrinterList();
}
//=======================================================================================

function LoadPortList() {
    // Calls NLWAX.oxc's GetPortList to retrieve list of client-side ports.
    // The returned data is stored in a HiddenField control
    portList.value = document.NLWAXForm.GetPortList();
}
//=======================================================================================

function PrintViaDriver() {
    try {
        var printer = $('NLWAX1_hdfPrinter');
        //Pass label name, print stream file name, and driver name to NLWAX.ocx
        document.NLWAXForm.SendToPrinter(label.value, file.value, printer.value);
        window.location.reload()
    }
    catch (e) {
        alert('NLWAX error sending data to ' + printer.value + ' Printer\r\n' + e.message)
    }
}
//=======================================================================================

function PrintViaPort() {
    try {
        var port = $('NLWAX1_hdfPort');
        //Pass label name, print stream file name, port name, and TCP port.
        //TCP Port value is necessary when using IP printer, otherwise pass '0'
        document.NLWAXForm.SendToPort(label.value, file.value, port.value, 0);
        window.location.reload()
    }
    catch (e) {
        alert('NLWAX error sending data to ' + port.value + ' Port\r\n' + e.message)
    }
}
//=======================================================================================


//=============================================================
//Browser detection
var BrowserDetect = {
    init: function() {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function(data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function(dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    },
    dataBrowser: [
		{ string: navigator.userAgent,
		    subString: "OmniWeb",
		    versionSearch: "OmniWeb/",
		    identity: "OmniWeb"
		},
		{
		    string: navigator.vendor,
		    subString: "Apple",
		    identity: "Safari"
		},
		{
		    prop: window.opera,
		    identity: "Opera"
		},
		{
		    string: navigator.vendor,
		    subString: "iCab",
		    identity: "iCab"
		},
		{
		    string: navigator.vendor,
		    subString: "KDE",
		    identity: "Konqueror"
		},
		{
		    string: navigator.userAgent,
		    subString: "Firefox",
		    identity: "Firefox"
		},
		{
		    string: navigator.vendor,
		    subString: "Camino",
		    identity: "Camino"
		},
		{		// for newer Netscapes (6+)
		    string: navigator.userAgent,
		    subString: "Netscape",
		    identity: "Netscape"
		},
		{
		    string: navigator.userAgent,
		    subString: "MSIE",
		    identity: "Explorer",
		    versionSearch: "MSIE"
		},
		{
		    string: navigator.userAgent,
		    subString: "Gecko",
		    identity: "Mozilla",
		    versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
		    string: navigator.userAgent,
		    subString: "Mozilla",
		    identity: "Netscape",
		    versionSearch: "Mozilla"
		}
	],
    dataOS: [
		{
		    string: navigator.platform,
		    subString: "Win",
		    identity: "Windows"
		},
		{
		    string: navigator.platform,
		    subString: "Mac",
		    identity: "Mac"
		},
		{
		    string: navigator.platform,
		    subString: "Linux",
		    identity: "Linux"
		}
	]

};
BrowserDetect.init();
//=============================================================

//======= End of script =================================================================