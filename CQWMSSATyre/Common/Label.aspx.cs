﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Common_Label : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LabelClose"] != null)
        {
            Session["LabelClose"] = null;
            Session["FromURL"] = "~/Common/Label.aspx";
            Response.Redirect("~/Common/NLPrint.aspx");
        }
        else
            ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "ClosePopup", "window.close()", true);
    }
}