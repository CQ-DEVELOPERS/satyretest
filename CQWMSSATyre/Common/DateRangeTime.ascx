<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateRangeTime.ascx.cs" Inherits="Common_DateRangeTime" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Label ID="LabelFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>" Width="100px"></asp:Label>
<asp:TextBox ID="TextBoxFromDate" runat="server" OnTextChanged="TextBoxFromDate_TextChanged" Width="100px"></asp:TextBox>
<asp:TextBox ID="TextBoxFromTime" runat="server" Width="50px"></asp:TextBox>



<asp:Label ID="LabelToDate" runat="server" Text="<%$ Resources:Default, ToDate %>" Width="100px"></asp:Label>
<asp:TextBox ID="TextBoxToDate" runat="server" OnTextChanged="TextBoxToDate_TextChanged" Width="100px"></asp:TextBox>
<asp:TextBox ID="TextBoxToTime" runat="server" Width="50px"></asp:TextBox>

<ajaxToolkit:CalendarExtender
    ID="CalendarExtenderFromDate"
    runat="server"
    Animated="true"
    Format="<%$ Resources:Default,DateFormat %>"
    TargetControlID="TextBoxFromDate">
</ajaxToolkit:CalendarExtender>

<ajaxToolkit:MaskedEditExtender
    ID="MaskedEditExtenderFromDate"
    runat="server"
    Mask="<%$ Resources:Default,DateMask %>"
    MaskType="Date"
    CultureName="<%$ Resources:Default,CultureCode %>"
    MessageValidatorTip="true"
    TargetControlID="TextBoxFromDate">
</ajaxToolkit:MaskedEditExtender>

<ajaxToolkit:MaskedEditValidator
    ID="MaskedEditValidatorFromDate"
    runat="server"
    ControlExtender="MaskedEditExtenderFromDate"
    ControlToValidate="TextBoxFromDate"
    Display="Dynamic"
    EmptyValueMessage="From Date is required"
    InvalidValueMessage="From Date is invalid"
    IsValidEmpty="False"
    TooltipMessage="Input a From Date">
</ajaxToolkit:MaskedEditValidator>

<ajaxToolkit:CalendarExtender
    ID="CalendarExtenderToDate"
    runat="server"
    Animated="true"
    Format="<%$ Resources:Default,DateFormat %>"
    TargetControlID="TextBoxToDate">
</ajaxToolkit:CalendarExtender>

<ajaxToolkit:MaskedEditExtender
    ID="MaskedEditExtenderToDate"
    runat="server"
    Mask="<%$ Resources:Default,DateMask %>"
    MaskType="Date"
    CultureName="<%$ Resources:Default,CultureCode %>"
    MessageValidatorTip="true"
    TargetControlID="TextBoxToDate">
</ajaxToolkit:MaskedEditExtender>

<ajaxToolkit:MaskedEditValidator
    ID="MaskedEditValidatorToDate"
    runat="server"
    ControlExtender="MaskedEditExtenderToDate"
    ControlToValidate="TextBoxToDate"
    Display="Dynamic"
    EmptyValueMessage="From Date is required"
    InvalidValueMessage="From Date is invalid"
    IsValidEmpty="False"
    TooltipMessage="Input a From Date">
</ajaxToolkit:MaskedEditValidator>