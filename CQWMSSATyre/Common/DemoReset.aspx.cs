using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_DemoReset : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e) 
    {
        try
        {
            if (!Page.IsPostBack)
            {
            }
        }
        catch { }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Demo d = new Demo();

            if(d.ResetDatabase(Session["ConnectionStringName"].ToString()))
            {
                Master.MsgText = "Database reset successful."; Master.ErrorText = "";
            }
                else
            {
                Master.MsgText = ""; Master.ErrorText = "Database reset failed.";
            }

            
        }
        catch
        {
            Master.MsgText = ""; Master.ErrorText = "There was an error trying to reset the database.";
        }
    }
}
