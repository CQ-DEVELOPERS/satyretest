<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="ClothingLabel.aspx.cs"
    Inherits="Common_TicketingLabel"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="StorageUnitBatchIdSearch.ascx" TagName="StorageUnitBatchIdSearch"
    TagPrefix="uc1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default,ProductCode %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default,Product %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default,SKUCode %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxSKUCode" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelSKU" runat="server" Text="<%$ Resources:Default,SKU %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxSKU" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelPackType" runat="server" Text="<%$ Resources:Default, PackType%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxPackType" runat="server" Text=""></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxBatch" runat="server" Text=""></asp:TextBox>
            </td>
            <td align="right">
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default,Search %>" OnClick="ButtonSearch_Click" />
            </td>
        </tr>
    </table>
    <asp:Button ID="ButtonSelect" runat="server" Text="<%$ Resources:Default, SelectAll%>" OnClick="ButtonSelect_Click" />
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, Print%>" OnClick="ButtonPrint_Click" />
    <asp:Label ID="LabelQty" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
    <asp:TextBox ID="TextBoxQty" runat="server" Text="1"></asp:TextBox>
    <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" FilterType="Numbers" TargetControlID="TextBoxQty"></ajaxToolkit:FilteredTextBoxExtender>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewProductSearch"
                runat="server"
                AllowPaging="True"
                AutoGenerateColumns="False"
                AutoGenerateSelectButton="False" 
                DataKeyNames="PackId,BatchId"
                OnPageIndexChanging="GridViewProductSearch_PageIndexChanging"
                PageSize="30" AllowSorting="true">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PackId" Visible="false" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default,ProductCode %>" SortExpression="ProductCode" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default,Product %>" SortExpression="Product" />
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default,SKUCode %>" SortExpression="SKUCode" />
                    <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default,SKU %>" SortExpression="SKU" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default,Batch %>" SortExpression="Batch" />
                    <asp:BoundField DataField="PackType" HeaderText="<%$ Resources:Default,PackType %>" SortExpression="PackType" />
                    <asp:BoundField DataField="Barcode" HeaderText="<%$ Resources:Default,Barcode %>" SortExpression="Barcode" />
                    <asp:BoundField DataField="LabelQuantity" HeaderText="Label Quantity" SortExpression="LabelQuantity" DataFormatString="{0:G0}"/>
                    <asp:BoundField DataField="POQuantity" HeaderText="PO Quantity" SortExpression="POQuantity" DataFormatString="{0:G0}"/>
                    <asp:BoundField DataField="Quantity" HeaderText="Pack Quantity" SortExpression="Quantity" DataFormatString="{0:G0}"/>
                    <asp:BoundField DataField="LabelType" HeaderText="<%$ Resources:Default,Size %>" SortExpression="LabelType" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="click" />
            <asp:AsyncPostBackTrigger ControlID="ButtonPrint" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

