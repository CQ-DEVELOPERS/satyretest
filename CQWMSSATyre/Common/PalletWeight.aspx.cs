using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_PalletWeight : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

                TextBoxBarcode.Focus();
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region ButtonEnter_Click
    protected void ButtonEnter_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEnter_Click";

        try
        {
            GetDetails();

            MultiView1.ActiveViewIndex++;

            ScriptManager.GetCurrent(this.Page).SetFocus(tbTareWeight);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonEnter_Click

    #region ButtonUpdate_Click
    protected void ButtonUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            theErrMethod = "ButtonUpdate_Click";
            Master.MsgText = "";
            Master.ErrorText = "";

            PalletWeight pw = new PalletWeight();

            int palletId = -1;
            Decimal tareWeight = -1;
            Decimal nettWeight = -1;
            Decimal grossWeight = -1;

            int.TryParse(Session["Barcode"].ToString().ToUpper().Replace("P:", ""), out palletId);
            Decimal.TryParse(tbTareWeight.Text, out tareWeight);
            Decimal.TryParse(tbNettWeight.Text, out nettWeight);
            Decimal.TryParse(tbGrossWeight.Text, out grossWeight);

            if (palletId > -1)
            {

                if (pw.UpdatePallet(Session["ConnectionStringName"].ToString(), palletId, tareWeight, nettWeight, grossWeight))
                {
                    GetDetails();

                    ScriptManager.GetCurrent(this.Page).SetFocus(TextBoxBarcode);

                    MultiView1.ActiveViewIndex--;
                }
                else
                    Master.ErrorText = Resources.Default.WeightDetails + " " + Resources.ResMessages.InvalidField;
            }
            else
            {
                Master.ErrorText = Resources.ResMessages.InvalidPalletId;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonUpdate_Click

    #region ButtonCancel_Click
    protected void ButtonCancel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCancel_Click";

        try
        {
            GetDetails();

            MultiView1.ActiveViewIndex--;

            ScriptManager.GetCurrent(this.Page).SetFocus(TextBoxBarcode);

            Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonCancel_Click

    #region GetDetails
    protected void GetDetails()
    {
        theErrMethod = "GetDetails";

        try
        {
            Master.ErrorText = "";

            Session["Barcode"] = TextBoxBarcode.Text;
            TextBoxBarcode.Text = "";
            tbTareWeight.Text = "";
            tbNettWeight.Text = "";
            tbGrossWeight.Text = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GetDetails

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceiveIntoWarehouse", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceiveIntoWarehouse", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling
}
