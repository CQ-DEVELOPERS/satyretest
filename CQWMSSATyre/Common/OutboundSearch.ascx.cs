using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_OutboundSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                DropDownListOutboundDocumentType.DataBind();
                DropDownListPrincipal.DataBind();

                //Session["FromDate"] = DateRange.GetFromDate();
                //Session["ToDate"] = DateRange.GetToDate();

                //rdpFromDate.SelectedDate = DateRange.GetFromDate();
                //rdpToDate.SelectedDate = DateRange.GetToDate().AddDays(1);

                if (Session["OutboundShipmentId"] == null)
                    Session["OutboundShipmentId"] = -1;

                if (Session["OrderNumber"] == null)
                    Session["OrderNumber"] = "";
                else
                    TextBoxOrderNumber.Text = Session["OrderNumber"].ToString();

                if (Session["ExternalCompanyCode"] == null)
                    Session["ExternalCompanyCode"] = "";
                else
                    TextBoxCustomerCode.Text = Session["ExternalCompanyCode"].ToString();

                if (Session["ExternalCompany"] == null)
                    Session["ExternalCompany"] = "";
                else
                    TextBoxCustomerName.Text = Session["ExternalCompany"].ToString();

                if (Session["OutboundDocumentTypeId"] == null)
                    Session["OutboundDocumentTypeId"] = int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString());
                else
                    DropDownListOutboundDocumentType.SelectedValue = Session["OutboundDocumentTypeId"].ToString();

                if (Session["PrincipalId"] == null)
                    try { Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString()); }
                    catch { Session["PrincipalId"] = -1; }
                else
                    DropDownListPrincipal.SelectedValue = Session["PrincipalId"].ToString();
            }

            if (TextBoxOrderNumber.Text == "{All}")
            {
                Session["OrderNumber"] = "";
                TextBoxOrderNumber.Text = "";
            }

            if (TextBoxCustomerCode.Text == "{All}")
            {
                Session["ExternalCompanyCode"] = "";
                TextBoxCustomerCode.Text = "";
            }

            if (TextBoxCustomerName.Text == "{All}")
            {
                Session["ExternalCompany"] = "";
                TextBoxCustomerName.Text = "";
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxOrderNumber_TextChanged(object sender, EventArgs e)
    {
        Session["OrderNumber"] = TextBoxOrderNumber.Text;
    }
    protected void TextBoxCustomerCode_TextChanged(object sender, EventArgs e)
    {
        Session["ExternalCompanyCode"] = TextBoxCustomerCode.Text;
    }
    protected void TextBoxCustomerName_TextChanged(object sender, EventArgs e)
    {
        Session["ExternalCompany"] = TextBoxCustomerName.Text;
    }
    protected void DropDownListOutboundDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["OutboundDocumentTypeId"] = int.Parse(DropDownListOutboundDocumentType.SelectedValue.ToString());
    }
    protected void DropDownListPrincipal_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["PrincipalId"] = int.Parse(DropDownListPrincipal.SelectedValue.ToString());
    }

    //#region TextBoxFromDate_TextChanged
    //protected void TextBoxFromDate_TextChanged(object sender, EventArgs e)
    //{
    //    Session["FromDate"] = rdpFromDate.SelectedDate.ToString();
    //}
    //#endregion "TextBoxFromDate_TextChanged"

    //#region TextBoxToDate_TextChanged
    //protected void TextBoxToDate_TextChanged(object sender, EventArgs e)
    //{
    //    Session["ToDate"] = rdpToDate.SelectedDate.ToString();
    //}
    //#endregion "TextBoxToDate_TextChanged"
}
