using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_RegisterSerialNumberPallet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //FormViewSerialNumber.DataBind();
        //RepeaterSerialNumber.DataBind();

        //if (FormViewSerialNumber.DataItemCount < 1)
            //GetNextInstruction();

        if (!Page.IsPostBack)
        {
            try
            {
                GetNextInstruction();
            }
            catch { }
        }
    }
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        LabelError.Text = "";

        SerialNumber ds = new SerialNumber();

        int instructionId = (int)FormViewSerialNumber.DataKey.Values["InstructionId"];
        int storageUnitId = (int)FormViewSerialNumber.DataKey.Values["StorageUnitId"];

        if (ds.RegisterSerialNumberInstruction(Session["ConnectionStringName"].ToString(), instructionId, TextBoxSerialNumber.Text, storageUnitId) == false)
            LabelError.Text = "There was an error inserting the Serial Number.";
        else
            LabelError.Text = "";

        RepeaterSerialNumber.DataBind();
    }
    protected void ObjectDataSourceDetails_OnLoad(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (Session["InstructionId"] == null)
            Session["InstructionId"] = -1;

        e.InputParameters["instructionId"] = int.Parse(Session["InstructionId"].ToString());
    }
    protected void ObjectDataSourceRepeater_OnLoad(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["instructionId"] = FormViewSerialNumber.DataKey.Values["InstructionId"];
    }
    protected void GetNextInstruction()
    {
        ArrayList rowList = (ArrayList)Session["checkedList"];

        if (rowList == null)
        {
            Session.Remove("checkedList");

            if (Session["FromURL"] != null)
                Response.Redirect(Session["FromURL"].ToString());
        }
        else
        {
            int index = rowList.Count;

            if (index < 1)
            {
                Session.Remove("checkedList");

                if (Session["FromURL"] != null)
                    Response.Redirect(Session["FromURL"].ToString());
            }
            else
            {
                if (index > 0)
                {
                    Session["InstructionId"] = rowList[0];
                    rowList.Remove(rowList[0]);

                    index--;
                }

                FormViewSerialNumber.DataBind();
                RepeaterSerialNumber.DataBind();
                TextBoxSerialNumber.Text = "";
            }
        }
    }
    protected void ButtonGetNextInsruction_Click(object sender, EventArgs e)
    {
        GetNextInstruction();
    }
}
