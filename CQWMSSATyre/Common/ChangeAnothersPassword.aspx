<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="ChangeAnothersPassword.aspx.cs"
    Inherits="Common_ChangeAnothersPassword"
    Title="Change Another Persons Password" 
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelTitle" runat="server" Text="Change Another Persons Password" SkinID="PageTitle"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" Text="User Credentials" SkinID="AgendaTitle"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:Label ID="LabelOperatorName" runat="server" Text="<%$ Resources:Default, Operator %>"></asp:Label>
            <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                DataTextField="Operator" DataValueField="OperatorId">
            </asp:DropDownList>
            <asp:Label runat="server" ID="LabelPassword" Text="Password"></asp:Label>
            <asp:TextBox runat="server" ID="TextBoxPassword"></asp:TextBox>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonChange" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Button runat="server" ID="ButtonChange" Text="Change" OnClick="ButtonChange_Click" />
    <ajaxToolkit:ConfirmButtonExtender ID="cbeButtonManualLocations" runat="server" TargetControlID="ButtonChange" ConfirmText="Press OK to change the selected users password"></ajaxToolkit:ConfirmButtonExtender>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
        SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

