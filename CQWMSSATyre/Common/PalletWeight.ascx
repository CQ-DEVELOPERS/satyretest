<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PalletWeight.ascx.cs" Inherits="Common_PalletWeight" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:UpdatePanel runat="server" ID="UpdatePanel1">
    <ContentTemplate>
        <asp:DetailsView ID="DetailsViewCaptureWeight" runat="server" SkinID="ReceiveIntoWarehouse" DataSourceID="ObjectDataSourceJobWeight" DataKeyNames="JobId"
            AutoGenerateRows="False" AutoGenerateDeleteButton="False" AutoGenerateEditButton="True" AutoGenerateInsertButton="False"
            Height="200" Width="300">
            <Fields>
                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" SortExpression="JobId" ReadOnly="true" />
                <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" SortExpression="ReferenceNumber" ReadOnly="true" />
                
<%--                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" ReadOnly="true" />
                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product" ReadOnly="true" />
                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" ReadOnly="true" />
                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" SortExpression="Quantity" ReadOnly="true" />
                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" ReadOnly="true" />
                <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate" ReadOnly="true" />
                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch" ReadOnly="true" />--%>
                
                <asp:BoundField DataField="TareWeight" HeaderText="<%$ Resources:Default, TareWeight %>" SortExpression="TareWeight" ReadOnly="false" />
                <asp:BoundField DataField="NettWeight" HeaderText="<%$ Resources:Default, NettWeight %>" SortExpression="NettWeight" ReadOnly="false" />
                <asp:BoundField DataField="GrossWeight" HeaderText="<%$ Resources:Default, GrossWeight %>" SortExpression="GrossWeight" ReadOnly="false" />
            </Fields>
        </asp:DetailsView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ButtonPack" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelPackaging" runat="server" Text="<%$ Resources:Default, Packaging %>"></asp:Label>
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                <ContentTemplate>
                    <asp:GridView ID="gvPackaging" runat="server" DataSourceID="ObjectDataSourcePackaging" DataKeyNames="StorageUnitBatchId,Quantity" AutoGenerateColumns="false" AutoGenerateSelectButton="true" AutoPostBack="true"  OnSelectedIndexChanged="gvPackaging_OnSelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="Packaging" HeaderText="<%$ Resources:Default, Packaging %>" />
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="UpdatePanel3">
                <ContentTemplate>
                    <asp:TextBox ID="TextBoxQuantity" runat="server" Text=""></asp:TextBox>
                    <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="TextBoxQuantity" Width="120" Minimum = "1" Maximum = "1000" ></ajaxToolkit:NumericUpDownExtender>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="gvPackaging" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>

<asp:Button ID="ButtonPack" runat="server" Text="<%$ Resources:Default, AddPack%>" OnClick="ButtonPack_Click"/>
<br />

<asp:UpdatePanel runat="server" ID="UpdatePanelGridViewPackagingLines">
    <ContentTemplate>
        <asp:GridView ID="GridViewPackagingLines" runat="server" SkinID="ReceiveIntoWarehouse" DataSourceID="ObjectDataSourcePackaingLines" AutoGenerateColumns="false"
            DataKeyNames="PackageLineId" AllowSorting="true" AllowPaging="true">
            <Columns>
                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>">
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="AcceptedQuantity" HeaderText="<%$ Resources:Default, AcceptedQuantity %>">
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:BoundField>
            </Columns>
        </asp:GridView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ButtonPack" EventName="Click" />
    </Triggers>
    </asp:UpdatePanel>

<asp:ObjectDataSource ID="ObjectDataSourceJobWeight" runat="server"
    TypeName="PalletWeight" SelectMethod="GetWeights" InsertMethod="InsertWeights" UpdateMethod="UpdateWeights">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="int32" /> 
    </SelectParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="jobId" Type="Int32" />
        <asp:Parameter Name="tareWeight" Type="Decimal" />
        <asp:Parameter Name="nettWeight" Type="Decimal" />
        <asp:Parameter Name="grossWeight" Type="Decimal" />
    </UpdateParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="ObjectDataSourcePackaging" runat="server" TypeName="PalletWeight" SelectMethod="GetPackaging">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="ObjectDataSourcePackaingLines" runat="server" TypeName="PalletWeight" SelectMethod="SearchPackaging">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>