<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="ProductLabelUser.aspx.cs"
    Inherits="Common_ProductLabelUser"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Label ID="LabelTitle" runat="server" Text="<%$ Resources:Default, EnterTitleLabel%>"></asp:Label>
    <asp:TextBox ID="TextBoxTitle" runat="server"></asp:TextBox>
    <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, EnterBarcodeLabel%>"></asp:Label>
    <asp:TextBox ID="TextBoxBarcode" runat="server"></asp:TextBox>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, Print%>" OnClick="ButtonPrint_Click" />
</asp:Content>

