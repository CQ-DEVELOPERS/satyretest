using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_PalletWeight : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["JobId"] != null)
                    DetailsViewCaptureWeight.DataBind();
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region ButtonPack_Click
    protected void ButtonPack_Click(object sender, EventArgs e)
    {
        int storageUnitBatchId = -1;
        Decimal quantity = 0;

        if (Session["JobId"] != null)
            if (int.TryParse(gvPackaging.SelectedDataKey["StorageUnitBatchId"].ToString(), out storageUnitBatchId))
                if (Decimal.TryParse(TextBoxQuantity.Text, out quantity))
                {
                    PalletWeight pw = new PalletWeight();

                    if (pw.InsertPackaging(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], storageUnitBatchId, quantity, (int)Session["OperatorId"]))
                    {
                        GridViewPackagingLines.DataBind();
                        DetailsViewCaptureWeight.DataBind();
                        TextBoxQuantity.Text = "1";
                        gvPackaging.SelectedIndex = -1;
                    }
                }
    }
    #endregion ButtonPack_Click

    #region gvPackaging_OnSelectedIndexChanged
    protected void gvPackaging_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            TextBoxQuantity.Text = gvPackaging.SelectedDataKey["Quantity"].ToString();
        }
        catch { }
    }
    #endregion "gvPackaging_OnSelectedIndexChanged"
}
