﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InboundStationSheetSearch.ascx.cs" Inherits="Common_InboundStationSheetSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>

<%--<div style="float:left;">
    <asp:Label ID="LabelShowOrHide" runat="server"></asp:Label>
</div>

<div style="float:left;">--%>
<asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
<uc1:DateRange ID="DateRange1" runat="server" />
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxProductCode" runat="server" Width="150px" OnTextChanged="TextBoxProductCode_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxBatch" runat="server" Width="150px" OnTextChanged="TextBoxBatch_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>" Width="100px"></asp:Label>
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxOrderNumber" runat="server" Width="150px" OnTextChanged="TextBoxOrderNumber_TextChanged" EmptyMessage="{All}"></telerik:RadTextBox>
            </td>
            <td>
            </td>
        </tr>

    </table>
   
</asp:Panel>
<%--</div>--%>
<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch"
    Radius="10"
    Color="#EFEFEC"
    BorderColor="#404040"
    Corners="All" />

<%--<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtenderSearch" runat="Server"
    TargetControlID="PanelSearch"
    ExpandControlID="LabelShowOrHide"
    CollapseControlID="LabelShowOrHide" 
    Collapsed="False"
    TextLabelID="LabelShowOrHide"
    ExpandedText="<"
    CollapsedText=">>"
    SuppressPostBack="true" />--%>

<%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderJobNumber" runat="server"
    TargetControlID="TextBoxOrderNumber" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
    TargetControlID="TextBoxProductCode" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
    TargetControlID="TextBoxBatch" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>--%>