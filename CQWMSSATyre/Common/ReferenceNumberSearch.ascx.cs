﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ReferenceNumberSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ParameterOrderNumber"] == null)
                Session["ParameterOrderNumber"] = -1;
        }
    }
    #endregion Page_Load


    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        ReferenceNumber refno = new ReferenceNumber();
        DataSet ds = refno.ListReferenceNumber(Session["ConnectionStringName"].ToString(), TextBoxReferenceNumber.Text);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataRow row = ds.Tables[0].Rows[0];

            Session["OrderNumber"] = row["OrderNumber"];
            Session["IssueId"] = row["IssueId"];
            Session["OutboundShipmentId"] = row["OutboundShipmentId"];
            {
                if (Session["OutboundShipmentId"] == null)
                    Session["OutboundShipmentId"] = -1;
            }
        }
      
    }
    #endregion "ButtonSearch_Click"

   
}