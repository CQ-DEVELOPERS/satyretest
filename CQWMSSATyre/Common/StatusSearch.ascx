﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StatusSearch.ascx.cs" Inherits="Common_StatusSearch" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelStatusId" runat="server" Text="<%$ Resources:Default, StatusId%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxStatusId" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelStatus" runat="server" Text="<%$ Resources:Default, Status%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxStatus" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
        </td>
    </tr>
</table>

<asp:GridView ID="GridViewStatusSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataKeyNames="StatusId,Status"
    OnSelectedIndexChanged="GridViewStatusSearch_OnSelectedIndexChanged"
    OnPageIndexChanging="GridViewStatusSearch_PageIndexChanging">
    <Columns>
        <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status%>" />
        <asp:BoundField DataField="StatusId" HeaderText="<%$ Resources:Default, StatusId%>" />
    </Columns>
    <EmptyDataTemplate>
        <h5>No rows</h5>
    </EmptyDataTemplate>
</asp:GridView>
