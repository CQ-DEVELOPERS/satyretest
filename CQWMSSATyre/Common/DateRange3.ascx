<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateRange3.ascx.cs" Inherits="Common_DateRange3" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Label ID="LabelFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>" Width="100px"></asp:Label>
<asp:TextBox ID="TextBoxFromDate" runat="server" OnTextChanged="TextBoxFromDate_TextChanged" Width="150px"></asp:TextBox>
<br />

<ajaxToolkit:CalendarExtender
    ID="CalendarExtenderFromDate"
    runat="server"
    Animated="true"
    Format="<%$ Resources:Default,DateFormat %>"
    TargetControlID="TextBoxFromDate">
</ajaxToolkit:CalendarExtender>

<ajaxToolkit:MaskedEditExtender
    ID="MaskedEditExtenderFromDate"
    runat="server"
    Mask="<%$ Resources:Default,DateMask %>"
    MaskType="Date"
    CultureName="<%$ Resources:Default,CultureCode %>"
    MessageValidatorTip="true"
    TargetControlID="TextBoxFromDate">
</ajaxToolkit:MaskedEditExtender>

<ajaxToolkit:MaskedEditValidator
    ID="MaskedEditValidatorFromDate"
    runat="server"
    ControlExtender="MaskedEditExtenderFromDate"
    ControlToValidate="TextBoxFromDate"
    Display="Dynamic"
    EmptyValueMessage="From Date is required"
    InvalidValueMessage="From Date is invalid"
    IsValidEmpty="False"
    TooltipMessage="Input a From Date">
</ajaxToolkit:MaskedEditValidator>
