<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SupplierSearch.ascx.cs" Inherits="Common_SupplierSearch" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelExternalCompany" runat="server" Text="<%$ Resources:Default, Supplier %>" Width="100px"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxExternalCompany" runat="server" Width="150px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelExternalCompanyCode" runat="server" Text="<%$ Resources:Default, SupplierCode %>" Width="100px"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxExternalCompanyCode" runat="server" Width="150px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Select%>" OnClick="ButtonSearch_Click"></asp:Button>
        </td>
    </tr>
</table>
<asp:GridView ID="GridViewExternalCompany" 
                runat="server" 
                DataSourceID="ObjectDataSourceExternalCompany"
                AutoGenerateColumns="False" 
                AutoGenerateSelectButton="True" 
                AllowPaging="true"
                OnSelectedIndexChanged="GridViewExternalCompany_OnSelectedIndexChanged"
                DataKeyNames="ExternalCompanyId,ExternalCompany">
    <Columns>
        <asp:BoundField ReadOnly="True" DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Supplier %>">
        </asp:BoundField>
        <asp:BoundField ReadOnly="True" DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, SupplierCode %>"></asp:BoundField>
    </Columns>
</asp:GridView>
<asp:ObjectDataSource ID="ObjectDataSourceExternalCompany" runat="server" TypeName="ExternalCompany" SelectMethod="SearchExternalCompanies">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:ControlParameter Name="externalCompany" ControlID="TextBoxExternalCompany" Type="String" />
        <asp:ControlParameter Name="externalCompanyCode" ControlID="TextBoxExternalCompanyCode" Type="String" />
        <asp:Parameter Name="externalCompanyTypeCode" DefaultValue="SUPP" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>