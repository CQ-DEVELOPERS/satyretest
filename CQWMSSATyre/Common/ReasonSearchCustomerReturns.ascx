﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReasonSearchCustomerReturns.ascx.cs" Inherits="Common_ReasonSearchCustomerReturns" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelReasonCode" runat="server" Text="<%$ Resources:Default, ReasonCode%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxReasonCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelReason" runat="server" Text="<%$ Resources:Default, Reason%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxReason" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button ID="ButtonSearchReason" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
        </td>
    </tr>
</table>

<asp:GridView ID="GridViewReasonSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataKeyNames="ReasonId"
    OnSelectedIndexChanged="GridViewReasonSearch_OnSelectedIndexChanged"
    OnPageIndexChanging="GridViewReasonSearch_PageIndexChanging">
    <Columns>
        <asp:BoundField DataField="Reason" HeaderText="<%$ Resources:Default, Reason%>" />
        <asp:BoundField DataField="ReasonCode" HeaderText="<%$ Resources:Default, ReasonCode%>" />
    </Columns>
    <EmptyDataTemplate>
        <h5>No rows</h5>
    </EmptyDataTemplate>
</asp:GridView>
