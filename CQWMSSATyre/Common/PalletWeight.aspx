<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PalletWeight.aspx.cs" Inherits="Common_PalletWeight" Title="<%$ Resources:Default, PalletWeight %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PalletWeight %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceiveIntoWarehouseAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <div style="text-align: center; width: 800px">
        <table>
            <tr>
                <td valign="top">
                    <asp:DetailsView ID="DetailsViewInstruction" runat="server" SkinID="ReceiveIntoWarehouse"
                        DataKeyNames="PalletId" DataSourceID="ObjectDataSourcePallet"
                        AutoGenerateRows="False">
                        <Fields>
                            <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                        </Fields>
                    </asp:DetailsView>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="view1" runat="server">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="ButtonEnter">
                                <asp:TextBox ID="TextBoxBarcode" runat="server" EnableTheming="false" Width="200px"
                                    Font-Size="X-Large"></asp:TextBox>
                                <br />
                                <asp:Button ID="ButtonEnter" runat="server" Text="<%$ Resources:Default, ButtonEnter %>" OnClick="ButtonEnter_Click" EnableTheming="false" Height="80px" Width="200px" Font-Size="X-Large" />
                                <asp:Button ID="Button1" runat="server" Visible="false" />
                            </asp:Panel>
                        </asp:View>
                        <asp:View ID="view2" runat="server">
                            <asp:Label ID="lblTareWeight" runat="server" Text="<%$ Resources:Default, TareWeight %>"></asp:Label>
                            <asp:TextBox ID="tbTareWeight" runat="server"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblNettWeight" runat="server" Text="<%$ Resources:Default, NettWeight %>"></asp:Label>
                            <asp:TextBox ID="tbNettWeight" runat="server"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblGrossWeight" runat="server" Text="<%$ Resources:Default, GrossWeight %>"></asp:Label>
                            <asp:TextBox ID="tbGrossWeight" runat="server"></asp:TextBox>
                            <br />
                            <asp:Button ID="ButtonUpdate" runat="server" Text="<%$ Resources:Default, ButtonEnter %>" OnClick="ButtonUpdate_Click" EnableTheming="false" Height="80px" Width="200px" Font-Size="X-Large" />
                            <asp:Button ID="ButtonCancel" runat="server" Text="<%$ Resources:Default, ButtonCancel %>" OnClick="ButtonCancel_Click" EnableTheming="false" Height="80px" Width="200px" Font-Size="X-Large" />
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
        </table>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSourcePallet" runat="server" TypeName="PalletWeight"
        SelectMethod="GetPallet" UpdateMethod="UpdatePallet">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="barcode" SessionField="Barcode" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="palletId" Type="Int32" />
            <asp:Parameter Name="tareWeight" Type="Decimal" />
            <asp:Parameter Name="nettWeight" Type="Decimal" />
            <asp:Parameter Name="grossWeight" Type="Decimal" />
        </UpdateParameters>
    </asp:ObjectDataSource>
</asp:Content>
