using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_GenericLabel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e) 
    {


    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        Location loc = new Location();
        DataSet ds = loc.GetLocations(Session["connectionStringName"].ToString(),
                    int.Parse(Session["warehouseId"].ToString()),ddlStartLocation.Text,ddlEndLocation.Text);

        
        Session["LabelName"] = "Multi Purpose Label.lbl";
        Session["FromURL"] = "~/Common/LocationPrint.aspx";

        if (Session["Printer"] == null)
            Session["Printer"] = "";

        

        int index = -1;
        foreach (DataRow dr in ds.Tables[0].Rows)
            try
            {
                index++;
                Session["Title"] = dr["Location"].ToString();
                Session["Barcode"] = dr["Location"].ToString();
                if (Session["Printer"].ToString() == "")
                    Response.Redirect("~/Common/NLLabels.aspx");
                else
                {
                    Session["Printing"] = true;
                    Response.Redirect("~/Common/NLPrint.aspx");
                }
            }

            catch { }
    }
}
