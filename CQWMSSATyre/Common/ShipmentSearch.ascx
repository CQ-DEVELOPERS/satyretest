﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShipmentSearch.ascx.cs"
    Inherits="Common_ShipmentSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange4.ascx" TagName="DateRange" TagPrefix="ucdr1" %>
<%--<div style="float:left;">
    <asp:Label ID="LabelShowOrHide" runat="server"></asp:Label>
</div>

<div style="float:left;">--%>
<asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelOutboundShipmentId" runat="server" Text="<%$ Resources:Default, OutboundShipmentId %>"
                    Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxOutboundShipmentId2" runat="server" Width="150px" OnTextChanged="TextBoxOutboundShipmentId2_TextChanged"></asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelDocumentType" runat="server" Text="<%$ Resources:Default, OutboundDocumentType %>"
                    Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataTextField="OutboundDocumentType"
                    DataValueField="OutboundDocumentTypeId" DataSourceID="ObjectDataSourceOutboundDocumentTypeId"
                    Width="150px" OnSelectedIndexChanged="DropDownListOutboundDocumentType_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <ucdr1:DateRange ID="DateRange1" runat="server" />
</asp:Panel>
<%--</div>--%>
<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch" Radius="10" Color="#EFEFEC" BorderColor="#404040"
    Corners="All" />
<%--<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtenderSearch" runat="Server"
    TargetControlID="PanelSearch"
    ExpandControlID="LabelShowOrHide"
    CollapseControlID="LabelShowOrHide" 
    Collapsed="False"
    TextLabelID="LabelShowOrHide"
    ExpandedText="<<<"
    CollapsedText=">>"
    SuppressPostBack="true" />--%>
<asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentTypeId" runat="server"
    SelectMethod="GetOutboundDocumentTypes" TypeName="OutboundDocumentType">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderOutboundShipmentId"
    runat="server" TargetControlID="TextBoxOutboundShipmentId2" WatermarkCssClass="watermarked"
    WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>
