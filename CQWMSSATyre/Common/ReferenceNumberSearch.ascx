﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReferenceNumberSearch.ascx.cs" Inherits="Common_ReferenceNumberSearch" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelReferenceNumber" runat="server" Text="<%$ Resources:Default, ReferenceNumber%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxReferenceNumber" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
        </td>
    </tr>
</table>
