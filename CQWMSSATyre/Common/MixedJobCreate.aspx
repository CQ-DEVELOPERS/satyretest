<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="MixedJobCreate.aspx.cs" Inherits="Common_MixedJobCreate" Title="<%$ Resources:Default, MixedJobCreateTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange2.ascx" TagName="DateRange2" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, MixedJobCreateTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, MixedJobCreateAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, PalletManipulation%>">
            <ContentTemplate>
                <table border="solid" cellpadding="5">
                    <tr>
                        <td valign="top">
                            <asp:Label ID="LabelJobs" runat="server" Text="<%$ Resources:Default, Jobs%>" Font-Bold="true"></asp:Label>
                            <br />
                            <asp:Panel ID="PanelSearch" runat="server">
                                <asp:Label ID="LabelJobNumber" runat="server" Text="<%$ Resources:Default, JobId%>"
                                    Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxJobNumber" runat="server" Width="150px"></asp:TextBox>
                                <br />
                                <uc1:DateRange2 ID="DateRange21" runat="server" />
                            </asp:Panel>
                            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                                OnClick="ButtonSearch_Click" />
                            <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, PrintPutAwaySheet%>"
                                OnClick="ButtonPrint_Click" />
                        </td>
                        <td valign="top">
                            <asp:Label ID="LabelLinkedInstructions" runat="server" Text="<%$ Resources:Default, LinkedInstructions%>"
                                Font-Bold="true"></asp:Label>
                            <asp:UpdatePanel runat="server" ID="UpdatePanelDetails">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewDetails" runat="server" DataSourceID="ObjectDataSourceDetails"
                                        AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, QuantityDetails %>">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Weight" HeaderText="<%$ Resources:Default, WeightDetails %>">
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="GridViewLinked" EventName="SelectedIndexChanged">
                                    </asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" TypeName="MixedJobCreate"
                                SelectMethod="GetDetails">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="string" />
                                    <asp:SessionParameter Name="jobId" SessionField="JobId" Type="int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:Button ID="ButtonPlanningComplete" runat="server" Text="<%$ Resources:Default, PlanningComplete%>"
                                OnClick="ButtonPlanningComplete_Click" />
                        </td>
                        <td valign="top">
                            <asp:Label ID="LabelUnlinked" runat="server" Text="<%$ Resources:Default, UnlinkedInstructions%>"
                                Font-Bold="true"></asp:Label>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode%>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Product%>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelLocation" runat="server" Text="<%$ Resources:Default, Location%>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxLocation" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="right">
                                        <asp:Button ID="ButtonUnlinkedSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                                            OnClick="ButtonUnlinkedSearch_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewAvailable">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewAvailable" runat="server" DataSourceID="ObjectDataSourceAvailable"
                                        DataKeyNames="JobId" AllowPaging="true" AutoGenerateColumns="false" AutoGenerateSelectButton="True"
                                        OnSelectedIndexChanged="GridViewAvailable_SelectedIndexChanged">
                                        <Columns>
                                            <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" SortExpression="JobId">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                                                SortExpression="InstructionType"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceAvailable" runat="server" TypeName="MixedJobCreate"
                                        SelectMethod="GetAvailable">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="string" />
                                            <asp:SessionParameter Name="InstructionTypeCode" SessionField="InstructionTypeCode"
                                                Type="string" />
                                            <asp:ControlParameter Name="JobId" ControlID="TextBoxJobNumber" Type="int32" DefaultValue="-1" />
                                            <asp:SessionParameter Name="ShipmentId" SessionField="ShipmentId" Type="int32" DefaultValue="-1" />
                                            <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String"
                                                DefaultValue="-1" />
                                            <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                            <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td valign="top">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewLinked">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewLinked" runat="server" DataSourceID="ObjectDataSourceLinked"
                                        DataKeyNames="InstructionId" AllowPaging="true" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewLinked_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField SelectText="<%$ Resources:Default, Remove%>" ShowSelectButton="True" />
                                            <asp:BoundField DataField="InstructionId" HeaderText="<%$ Resources:Default, InstructionId %>"
                                                SortExpression="InstructionId" Visible="False" ReadOnly="True" />
                                            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                                SortExpression="SKUCode" />
                                            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                                SortExpression="Quantity" />
                                            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                                SortExpression="OrderNumber"></asp:BoundField>
                                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                                SortExpression="ProductCode" />
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonUnlinkedSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="GridViewLinked" EventName="SelectedIndexChanged">
                                    </asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:ObjectDataSource ID="ObjectDataSourceLinked" runat="server" TypeName="MixedJobCreate"
                                SelectMethod="GetLinked">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="string" />
                                    <asp:SessionParameter Name="jobId" SessionField="JobId" Type="int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                        <td valign="top">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewUnlinked">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewUnlinked" runat="server" DataSourceID="ObjectDataSourceUnlinked"
                                        AutoGenerateColumns="false" DataKeyNames="InstructionId" OnSelectedIndexChanged="GridViewUnlinked_SelectedIndexChanged"
                                        AllowPaging="true">
                                        <Columns>
                                            <asp:CommandField SelectText="<%$ Resources:Default, Add%>" ShowSelectButton="True" />
                                            <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" SortExpression="JobId">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                                SortExpression="SKUCode"></asp:BoundField>
                                            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                                SortExpression="Quantity"></asp:BoundField>
                                            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                                SortExpression="OrderNumber"></asp:BoundField>
                                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                                SortExpression="ProductCode"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonUnlinkedSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:ObjectDataSource ID="ObjectDataSourceUnlinked" runat="server" TypeName="MixedJobCreate"
                                SelectMethod="GetUnlinked">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="string" />
                                    <asp:SessionParameter Name="InstructionTypeCode" SessionField="InstructionTypeCode"
                                        Type="string" />
                                    <asp:SessionParameter Name="jobId" SessionField="JobId" Type="int32" />
                                    <asp:SessionParameter Name="ShipmentId" SessionField="ShipmentId" Type="int32" DefaultValue="-1" />
                                    <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String"
                                        DefaultValue="-1" />
                                    <asp:ControlParameter Name="ProductCode" ControlID="TextBoxProductCode" Type="string"
                                        ConvertEmptyStringToNull="true" />
                                    <asp:ControlParameter Name="Product" ControlID="TextBoxProduct" Type="string" ConvertEmptyStringToNull="true" />
                                    <asp:ControlParameter Name="Location" ControlID="TextBoxLocation" Type="string" ConvertEmptyStringToNull="true" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
