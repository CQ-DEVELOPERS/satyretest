<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReceivingBay.ascx.cs" Inherits="Common_ReceivingBay" %>
<asp:DropDownList ID="DropDownListBay" runat="server" DataSourceID="ObjectDataSourceBay"
    DataValueField="LocationId" DataTextField="<%$ Resources:Default, Location%>" OnSelectedIndexChanged="DropDownListBay_SelectedIndexChanged">
</asp:DropDownList>
<asp:ObjectDataSource ID="ObjectDataSourceBay" runat="server"
    TypeName="Location" SelectMethod="GetLocationsByAreaCode">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="areaCode" Type="string" DefaultValue="R" />
    </SelectParameters>
</asp:ObjectDataSource>
