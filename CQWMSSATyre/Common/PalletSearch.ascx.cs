using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_PalletSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                DropDownListStatus.DataBind();

                if (Session["JobId"] == null)
                    Session["JobId"] = -1;

                if (Session["ProductCode"] == null)
                    Session["ProductCode"] = "%";
                else
                    TextBoxProductCode.Text = Session["ProductCode"].ToString();

                if (Session["Product"] == null)
                    Session["Product"] = "%";
                else
                    TextBoxProduct.Text = Session["Product"].ToString();

                if (Session["SKUCode"] == null)
                    Session["SKUCode"] = "%";
                else
                    TextBoxSKUCode.Text = Session["SKUCode"].ToString();

                if (Session["Batch"] == null)
                    Session["Batch"] = "%";
                else
                    TextBoxBatch.Text = Session["Batch"].ToString();

                if (Session["JobId"] == null)
                    Session["JobId"] = -1;
                else
                    TextBoxJobId.Text = Session["JobId"].ToString();

                if (Session["JobId"].ToString() == "-1")
                    TextBoxJobId.Text = "";

                if (Session["PalletId"] == null)
                    Session["PalletId"] = -1;
                else
                    TextBoxPalletId.Text = Session["PalletId"].ToString();

                if (Session["PalletId"].ToString() == "-1")
                    TextBoxPalletId.Text = "";

                if (Session["StatusId"] == null)
                    Session["StatusId"] = int.Parse(DropDownListStatus.SelectedValue.ToString());
                else
                    DropDownListStatus.SelectedValue = Session["StatusId"].ToString();
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxProductCode_TextChanged(object sender, EventArgs e)
    {
        Session["ProductCode"] = TextBoxProductCode.Text;
    }

    protected void TextBoxProduct_TextChanged(object sender, EventArgs e)
    {
        Session["Product"] = TextBoxProduct.Text;
    }

    protected void TextBoxSKUCode_TextChanged(object sender, EventArgs e)
    {
        Session["SKUCode"] = TextBoxSKUCode.Text;
    }

    protected void TextBoxBatch_TextChanged(object sender, EventArgs e)
    {
        Session["Batch"] = TextBoxBatch.Text;
    }

    protected void TextBoxPalletId_TextChanged(object sender, EventArgs e)
    {
        if (TextBoxPalletId.Text == "")
            Session["PalletId"] = -1;
        else
            Session["PalletId"] = TextBoxPalletId.Text;
    }

    protected void TextBoxJobId_TextChanged(object sender, EventArgs e)
    {
        if (TextBoxJobId.Text == "")
            Session["JobId"] = -1;
        else
            Session["JobId"] = TextBoxJobId.Text;
    }

    protected void DropDownListStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["StatusId"] = int.Parse(DropDownListStatus.SelectedValue.ToString());
    }
}
