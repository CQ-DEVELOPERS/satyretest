using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_TransderOrderSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                DropDownListLocation.DataBind();

                if (Session["ProductCode"] == null)
                    Session["ProductCode"] = "";
                else
                    TextBoxProductCode.Text = Session["ProductCode"].ToString();

                if (Session["Product"] == null)
                    Session["Product"] = "";
                else
                    TextBoxProduct.Text = Session["Product"].ToString();

                if (Session["LocationId"] == null)
                    Session["LocationId"] = int.Parse(DropDownListLocation.SelectedValue.ToString());
                else
                    DropDownListLocation.SelectedValue = Session["LocationId"].ToString();
            }

            if (TextBoxProductCode.Text == "{All}")
            {
                Session["ProductCode"] = "";
                TextBoxProductCode.Text = "";
            }

            if (TextBoxProduct.Text == "{All}")
            {
                Session["Product"] = "";
                TextBoxProduct.Text = "";
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxProductCode_TextChanged(object sender, EventArgs e)
    {
        Session["ProductCode"] = TextBoxProductCode.Text;
    }

    protected void TextBoxProduct_TextChanged(object sender, EventArgs e)
    {
        Session["Product"] = TextBoxProduct.Text;
    }

    protected void DropDownListLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["LocationId"] = int.Parse(DropDownListLocation.SelectedValue.ToString());
    }
}
