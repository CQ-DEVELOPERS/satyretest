using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ManualPalletiseSplit : System.Web.UI.Page
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                if (GridViewReceiptLine.Rows.Count < 1)
                    GetNextInstruction();
            }
            catch { }
        }
    }
    #endregion Page_Load

    #region GetNextInstruction
    protected void GetNextInstruction()
    {
        ArrayList rowList = (ArrayList)Session["checkedList"];

        if (rowList == null)
        {
            Session.Remove("checkedList");

            if (Session["FromURL"] != null)
                Response.Redirect(Session["FromURL"].ToString());
        }
        else
        {
            int index = rowList.Count;

            if (index < 1)
            {
                Session.Remove("checkedList");

                if (Session["FromURL"] != null)
                    Response.Redirect(Session["FromURL"].ToString());
            }
            else
            {
                ManualPalletiseSplit ds = new ManualPalletiseSplit();

                if (index > 0)
                {
                    GridViewReceiptLine.DataSource = ds.GetInstructionToSplit(Session["ConnectionStringName"].ToString(), Convert.ToInt32(rowList[0].ToString()));

                    Session["InstructionId"] = Convert.ToInt32(rowList[0].ToString());

                    rowList.Remove(rowList[0]);

                    index--;
                }

                GridViewReceiptLine.DataBind();

                if (rowList.Count == 0)
                    Session.Remove("checkedList");

                if (GridViewReceiptLine.Rows.Count > 0)
                {
                    GridViewInstruction.DataBind();
                    GridViewQuantity.DataBind();
                    TextBoxLines.Enabled = true;
                    TextBoxLines.Text = "1";
                    ButtonCreateLines.Enabled = true;
                }
                else
                    GetNextInstruction();
            }
        }
    }
    #endregion GetNextInstruction

    #region ButtonCreateLines_Click
    protected void ButtonCreateLines_Click(object sender, EventArgs e)
    {
        if (TextBoxLines.Text == "" || TextBoxLines.Text == "0")
            return;

        ManualPalletiseSplit ds = new ManualPalletiseSplit();

        if (ds.CreateLines(Session["ConnectionStringName"].ToString(),
                            int.Parse(GridViewReceiptLine.Rows[0].Cells[1].Text),
                            int.Parse(TextBoxLines.Text)) == true)
        {
            //GridViewInstruction.DataSource = ds.GetInstructions(int.Parse(GridViewReceiptLine.Rows[0].Cells[0].Text));
            GridViewInstruction.DataBind();
            GridViewQuantity.DataBind();
            TextBoxLines.Enabled = false;
            ButtonCreateLines.Enabled = false;
        }
        else
            Response.Write("There was an error!!!" + GridViewReceiptLine.Rows[0].Cells[0].Text);
    }
    #endregion ButtonCreateLines_Click

    #region GetEditIndex
    protected void GetEditIndex()
    {
        int index = 0;
        CheckBox cb = new CheckBox();
        ArrayList rowList = new ArrayList();
        try
        {
            while (index < GridViewInstruction.Rows.Count)
            {
                GridViewRow checkedRow = GridViewInstruction.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    rowList.Add(GridViewInstruction.DataKeys[index].Values["InstructionId"]);
                }

                index++;
            }
            if (rowList.Count > 0)
                Session["editList"] = rowList;
        }
        catch { }
    }
    #endregion GetEditIndex

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        try
        {
            GetEditIndex();
            ArrayList rowList = (ArrayList)Session["editList"];

            if (rowList == null)
                Session.Remove("editList");
            else
            {
                int index = rowList.Count;

                if (index < 1)
                {
                    Session.Remove("editList");
                }
                else
                {
                    ManualPalletiseSplit ds = new ManualPalletiseSplit();

                    while (index > 0)
                    {
                        if (ds.AutoLocationAllocate(Session["ConnectionStringName"].ToString(), Convert.ToInt32(rowList[0])) == false)
                            Response.Write("There was an error!!!");

                        rowList.Remove(rowList[0]);

                        index--;
                    }

                    //Session.Remove("editList");

                    //GridViewInstruction.DataSource = ds.GetInstructions(int.Parse(GridViewReceiptLine.Rows[0].Cells[0].Text));
                    GridViewInstruction.DataBind();
                }
            }
        }
        catch { }
    }
    #endregion ButtonAutoLocations_Click

    #region ButtonManualLocations_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Common/ManualPalletiseSplit.aspx";
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");
        }
        catch { }
    }
    #endregion ButtonManualLocations_Click

    #region GridViewInstruction_RowUpdated
    protected void GridViewInstruction_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        int jobId = int.Parse(GridViewReceiptLine.Rows[0].Cells[0].Text);
        
        try
        {
            GridViewQuantity.DataBind();
        }
        catch
        { }


    }
    #endregion GridViewInstruction_RowUpdated

    #region GridViewInstruction_RowUpdating
    protected void GridViewInstruction_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
    }
    #endregion GridViewInstruction_RowUpdating

    #region ButtonGetNext_Click
    protected void ButtonGetNext_Click(object sender, EventArgs e)
    {
        try
        {
            GetNextInstruction();
        }
        catch { }
    }
    #endregion ButtonGetNext_Click

    #region ButtonSerial_Click
    protected void ButtonSerial_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Common/ManualPalletiseSplit.aspx";
            Response.Redirect("~/Common/RegisterSerialNumberJob.aspx");
        }
        catch { }
    }
    #endregion ButtonSerial_Click

    #region ButtonReturn_Click
    protected void ButtonReturn_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["checkedList"] != null)
                Session.Remove("checkedList");

            if (Session["FromURL"] != null)
                Response.Redirect(Session["FromURL"].ToString());
        }
        catch { }
    }
    #endregion ButtonReturn_Click
}