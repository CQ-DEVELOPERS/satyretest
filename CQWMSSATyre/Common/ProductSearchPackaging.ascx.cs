using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ProductSearchPackaging : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["StorageUnitId"] == null)
                Session["StorageUnitId"] = -1;
            if (Session["Product"] == null)
                Session["Product"] = -1;
            if (Session["ProductCode"] == null)
                Session["ProductCode"] = ""; 
        }
    }
    #endregion Page_Load

    #region GridViewProductSearch_OnSelectedIndexChanged
    protected void GridViewProductSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["StorageUnitId"] = GridViewProductSearch.SelectedDataKey["StorageUnitId"];
        Session["PassStorageUnitBatchId"] = GridViewProductSearch.SelectedDataKey["StorageUnitBatchId"];
        Session["StorageUnitIdPass"] = GridViewProductSearch.SelectedDataKey["StorageUnitId"];
        Session["Product"] = GridViewProductSearch.SelectedDataKey["Product"];
        Session["ProductCode"] = GridViewProductSearch.SelectedRow.Cells[2].Text;
    }
    #endregion GridViewProductSearch_OnSelectedIndexChanged

    #region GridViewProductSearch_PageIndexChanging
    protected void GridViewProductSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewProductSearch.PageIndex = e.NewPageIndex;
            GridViewProductSearch_DataBind();
        }
        catch { }
    }
    #endregion "GridViewProductSearch_PageIndexChanging"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewProductSearch.PageIndex = 0;

        GridViewProductSearch_DataBind();
    }
    #endregion "ButtonSearch_Click"

    protected void GridViewProductSearch_DataBind()
    {
        Product product = new Product();

        GridViewProductSearch.DataSource = product.SearchPackageProducts(Session["ConnectionStringName"].ToString(), TextBoxProductCode.Text, TextBoxProduct.Text);

        GridViewProductSearch.DataBind();
    }
}
