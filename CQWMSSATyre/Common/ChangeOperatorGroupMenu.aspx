<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ChangeOperatorGroupMenu.aspx.cs" Inherits="Common_Change_Operator_Group_Menu"
    Title="Menu Administration" StylesheetTheme="Default" Theme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelTitle" runat="server" Text="Menu Administration" SkinID="PageTitle"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <br />
    <table>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="Label2" runat="server" SkinID="PageTitle" Text="Manage Access By"
                    Width="228px"></asp:Label></td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" ID="Label1" SkinID="AgendaTitle" Text="Operator Group"></asp:Label></td>
            <td align="center">
                <asp:Label runat="server" ID="lblAccessMenu" SkinID="AgendaTitle" Text="Menu Item"></asp:Label></td>
        </tr>
        <tr>
            <td valign="top">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataTextField="OperatorGroup"
                            DataValueField="OperatorGroupId" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:TreeView ID="TreeView1" runat="server" DataSourceID="XmlDataSource1" ShowLines="True"
                            ExpandDepth="4" Width="250px" OnSelectedNodeChanged="TreeView1_SelectedNodeChanged" >
                            <DataBindings>
                                <asp:TreeNodeBinding DataMember="p" SelectAction="Expand" TextField="OperatorGroup"
                                    ValueField="OperatorGroupId" />
                                <asp:TreeNodeBinding DataMember="i" SelectAction="Expand" TextField="ParentItemText"
                                    ValueField="ParentItemId" />
                                <asp:TreeNodeBinding DataMember="c" ImageUrlField="c.access" PopulateOnDemand="True"
                                    TextField="MenuItem" ValueField="MenuItemid" />
                                <asp:TreeNodeBinding DataMember="d" ImageUrlField="d.access" PopulateOnDemand="True"
                                    TextField="MenuItem3" ValueField="MenuItemid3" />
                            </DataBindings>
                        </asp:TreeView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        &nbsp;<asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" DataTextField="MenuItem"
                            DataValueField="MenuItemId" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"
                            Width="220px">
                        </asp:DropDownList>
                        <asp:TreeView ID="TreeView2" runat="server" DataSourceID="XmlDataSource2" ShowLines="True"
                            ExpandDepth="2" Width="250px" OnSelectedNodeChanged="TreeView2_SelectedNodeChanged">
                            <DataBindings>
                                <asp:TreeNodeBinding DataMember="i" SelectAction="Expand" TextField="ParentItemText"
                                    ValueField="ParentItemId" />
                                <asp:TreeNodeBinding DataMember="c" SelectAction="Expand" TextField="MenuItem"
                                    ValueField="MenuItemid" />
                                <asp:TreeNodeBinding DataMember="p" ImageUrlField="c.access" SelectAction="SelectExpand"
                                    TextField="OperatorGroup" ValueField="OperatorGroupId" />
                            </DataBindings>
                            <ParentNodeStyle ForeColor="#C0C0FF" />
                            <RootNodeStyle Font-Bold="True" />
                        </asp:TreeView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <table style="width: 445px">
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblAddOperator" runat="server" Text="Add Operator Group" SkinID="PageTitle"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 88px">
                        <asp:Label ID="lblDescription" runat="server" Font-Bold="False" Font-Names="Verdana"
                            Font-Size="0.8em" Style="font-size: 0.9em; color: black; font-family: Verdana"
                            Text="Description"></asp:Label></td>
                    <td style="width: 125px">
                        <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox></td>
                    <td style="width: 6px">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription"
                            ErrorMessage="RequiredFieldValidator" SetFocusOnError="True" Width="93px">*Required Field</asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td style="width: 88px">
                        <asp:Label ID="Label3" runat="server" Font-Bold="False" Font-Names="Verdana" Font-Size="0.8em"
                            Style="font-size: 0.9em; color: black; font-family: Verdana" Text="Copy From Group"
                            Width="129px"></asp:Label></td>
                    <td style="width: 125px">
                        <asp:DropDownList ID="ddlAddNew" runat="server" DataTextField="OperatorGroup" DataValueField="OperatorGroupId"
                            Width="156px">
                        </asp:DropDownList></td>
                    <td style="width: 6px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 88px">
                        <asp:Button ID="buttonAddNew" runat="server" Text="Add New" OnClick="buttonAddNew_Click"
                            SkinID="ButtonLarge" /></td>
                    <td style="width: 125px">
                    </td>
                    <td style="width: 6px">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <br />
    <br />
    <br />
    <br />
    <asp:XmlDataSource ID="XmlDataSource2" runat="server" EnableCaching="False" ></asp:XmlDataSource>
    <br />
    <asp:XmlDataSource ID="XmlDataSource1" runat="server" EnableCaching="False"></asp:XmlDataSource>
</asp:Content>
