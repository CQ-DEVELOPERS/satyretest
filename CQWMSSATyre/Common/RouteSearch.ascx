﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RouteSearch.ascx.cs" Inherits="Common_RouteSearch" %>

<asp:GridView ID="GridViewRouteSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataSourceID="ObjectDataSourceRoute"
    DataKeyNames="RouteId,Route"
    OnSelectedIndexChanged="GridViewRouteSearch_OnSelectedIndexChanged">
    <Columns>
        <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route%>" />
    </Columns>
</asp:GridView>

<asp:ObjectDataSource ID="ObjectDataSourceRoute" runat="server" TypeName="Route" SelectMethod="GetRoutes">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>