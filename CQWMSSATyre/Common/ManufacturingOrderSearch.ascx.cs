using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ManufacturingOrderSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                DropDownListLocation.DataBind();

                if (Session["ParameterShipmentId"] == null)
                    Session["ParameterShipmentId"] = -1;
                else
                    TextBoxOutboundShipmentId.Text = Session["ParameterShipmentId"].ToString();

                if (Session["OrderNumber"] == null)
                    Session["OrderNumber"] = "";
                else
                    TextBoxOrderNumber.Text = Session["OrderNumber"].ToString();

                if (Session["Batch"] == null)
                    Session["Batch"] = "";
                else
                    TextBoxBatch.Text = Session["Batch"].ToString();

                if (Session["ProductCode"] == null)
                    Session["ProductCode"] = "";
                else
                    TextBoxProductCode.Text = Session["ProductCode"].ToString();

                if (Session["Product"] == null)
                    Session["Product"] = "";
                else
                    TextBoxProduct.Text = Session["Product"].ToString();

                if (Session["LocationId"] == null)
                    Session["LocationId"] = int.Parse(DropDownListLocation.SelectedValue.ToString());
                else
                    DropDownListLocation.SelectedValue = Session["LocationId"].ToString();
            }

            if (TextBoxOutboundShipmentId.Text == "{All}")
            {
                Session["ParameterShipmentId"] = "-1";
                TextBoxOutboundShipmentId.Text = "";
            }

            if (TextBoxOrderNumber.Text == "{All}")
            {
                Session["OrderNumber"] = "";
                TextBoxOrderNumber.Text = "";
            }

            if (TextBoxBatch.Text == "{All}")
            {
                Session["Batch"] = "";
                TextBoxBatch.Text = "";
            }
        }
        catch { }
    }
    #endregion "Page_Load"

    protected void TextBoxOrderNumber_TextChanged(object sender, EventArgs e)
    {
        Session["OrderNumber"] = TextBoxOrderNumber.Text;
    }
    protected void TextBoxOutboundShipmentId_TextChanged(object sender, EventArgs e)
    {
        Session["ParameterShipmentId"] = TextBoxOutboundShipmentId.Text;
    }
    protected void TextBoxProductCode_TextChanged(object sender, EventArgs e)
    {
        Session["ProductCode"] = TextBoxProductCode.Text;
    }
    protected void TextBoxProduct_TextChanged(object sender, EventArgs e)
    {
        Session["Product"] = TextBoxProduct.Text;
    }
    protected void TextBoxBatch_TextChanged(object sender, EventArgs e)
    {
        Session["Batch"] = TextBoxBatch.Text;
    }
    protected void DropDownListLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["LocationId"] = int.Parse(DropDownListLocation.SelectedValue.ToString());
    }
}
