using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_CreateUser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        string userName = CreateUserWizard1.UserName;
        string password = CreateUserWizard1.Password;

        Operator op = new Operator();

        int operatorId = op.InsertOperator(Session["ConnectionStringName"].ToString(),
                                            1,
                                            (int)Session["WarehouseId"],
                                            userName,
                                            userName,
                                            password,
                                            true,
                                            DateTime.Today);

        Session["NewOperatorId"] = operatorId;
    }

    //// Activate event fires when the user hits "next" in the CreateUserWizard
    //public void AssignUserToRoles_Activate(object sender, EventArgs e)
    //{
    //}

    // Deactivate event fires when user hits "next" in the CreateUserWizard 
    public void AssignUserToRoles_Deactivate(object sender, EventArgs e)
    {
        int operatorId = (int)Session["NewOperatorId"];
        int operatorGroupId = int.Parse(DropDownListOperatorGroups.SelectedValue);
        string userName = CreateUserWizard1.UserName;

        Operator op = new Operator();
        LogonCredentials lc = new LogonCredentials();

        if (op.UpdateOperatorGroupId(Session["ConnectionStringName"].ToString(),
                                            operatorId,
                                            operatorGroupId))
            lc.EnableUserInCommonDB(Session["ConnectionStringName"].ToString(), userName);
    }
}
