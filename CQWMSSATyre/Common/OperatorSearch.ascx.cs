using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_OperatorSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ParameterOperatorId"] == null)
                Session["ParameterOperatorId"] = -1;
        }
    }
    #endregion Page_Load

    #region GridViewOperatorSearch_OnSelectedIndexChanged
    protected void GridViewOperatorSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["ParameterOperatorId"] = GridViewOperatorSearch.SelectedDataKey["OperatorId"];
    }
    #endregion GridViewOperatorSearch_OnSelectedIndexChanged

    #region GridViewOperatorSearch_PageIndexChanging
    protected void GridViewOperatorSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewOperatorSearch.PageIndex = e.NewPageIndex;
            GridViewOperatorSearch_DataBind();
        }
        catch { }
    }
    #endregion "GridViewOperatorSearch_PageIndexChanging"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewOperatorSearch.PageIndex = 0;

        GridViewOperatorSearch_DataBind();
    }
    #endregion "ButtonSearch_Click"

    protected void GridViewOperatorSearch_DataBind()
    {
        Operator op = new Operator();

        GridViewOperatorSearch.DataSource = op.GetOperatorList(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"]);

        GridViewOperatorSearch.DataBind();
    }
}
