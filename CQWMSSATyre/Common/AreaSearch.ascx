<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AreaSearch.ascx.cs" Inherits="Common_AreaSearch" %>

<asp:GridView ID="GridViewAreaSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataSourceID="ObjectDataSourceArea"
    DataKeyNames="AreaId,Area"
    OnSelectedIndexChanged="GridViewAreaSearch_OnSelectedIndexChanged">
    <Columns>
        <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default, Area%>" />
    </Columns>
</asp:GridView>

<asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="Area" SelectMethod="GetAreasByWarehouse">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>