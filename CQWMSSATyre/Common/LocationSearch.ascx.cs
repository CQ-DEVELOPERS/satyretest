using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_LocationSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["ParameterLocationId"] == null)
                Session["ParameterLocationId"] = -1;
        }
    }
    #endregion Page_Load

    #region GridViewLocationSearch_OnSelectedIndexChanged
    protected void GridViewLocationSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["ParameterLocationId"] = GridViewLocationSearch.SelectedDataKey["LocationId"];
    }
    #endregion GridViewLocationSearch_OnSelectedIndexChanged

    #region GridViewLocationSearch_PageIndexChanging
    protected void GridViewLocationSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewLocationSearch.PageIndex = e.NewPageIndex;
            GridViewLocationSearch_DataBind();
        }
        catch { }
    }
    #endregion "GridViewLocationSearch_PageIndexChanging"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewLocationSearch.PageIndex = 0;

        GridViewLocationSearch_DataBind();
    }
    #endregion "ButtonSearch_Click"

    protected void GridViewLocationSearch_DataBind()
    {
        Location loc = new Location();

        GridViewLocationSearch.DataSource = loc.SearchLocation(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], TextBoxLocation.Text);

        GridViewLocationSearch.DataBind();
    }
}
