﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateRange4.ascx.cs" Inherits="Common_DateRange4" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Label ID="LabelFromDate2" runat="server" Text="<%$ Resources:Default, FromDate %>" Width="100px"></asp:Label>
<asp:TextBox ID="TextBoxFromDate2" runat="server" OnTextChanged="TextBoxFromDate2_TextChanged" Width="150px"></asp:TextBox>

<asp:Label ID="LabelToDate2" runat="server" Text="<%$ Resources:Default, ToDate %>" Width="100px"></asp:Label>
<asp:TextBox ID="TextBoxToDate2" runat="server" OnTextChanged="TextBoxToDate2_TextChanged" Width="150px"></asp:TextBox>

<ajaxToolkit:CalendarExtender
    ID="CalendarExtenderFromDate2"
    runat="server"
    Animated="true"
    Format="yyyy/MM/dd"
    TargetControlID="TextBoxFromDate2">
</ajaxToolkit:CalendarExtender>

<ajaxToolkit:MaskedEditExtender
    ID="MaskedEditExtenderFromDate2"
    runat="server"
    Mask="9999/99/99"
    MaskType="Date"
    CultureName="en-ZA"
    MessageValidatorTip="true"
    TargetControlID="TextBoxFromDate2">
</ajaxToolkit:MaskedEditExtender>

<ajaxToolkit:MaskedEditValidator
    ID="MaskedEditValidatorFromDate2"
    runat="server"
    ControlExtender="MaskedEditExtenderFromDate2"
    ControlToValidate="TextBoxFromDate2"
    Display="Dynamic"
    EmptyValueMessage="From Date is required"
    InvalidValueMessage="From Date is invalid"
    IsValidEmpty="False"
    TooltipMessage="Input a From Date">
</ajaxToolkit:MaskedEditValidator>

<ajaxToolkit:CalendarExtender
    ID="CalendarExtenderToDate2"
    runat="server"
    Animated="true"
    Format="yyyy/MM/dd"
    TargetControlID="TextBoxToDate2">
</ajaxToolkit:CalendarExtender>

<ajaxToolkit:MaskedEditExtender
    ID="MaskedEditExtenderToDate2"
    runat="server"
    Mask="9999/99/99"
    MaskType="Date"
    CultureName="en-ZA"
    MessageValidatorTip="true"
    TargetControlID="TextBoxToDate2">
</ajaxToolkit:MaskedEditExtender>

<ajaxToolkit:MaskedEditValidator
    ID="MaskedEditValidatorToDate2"
    runat="server"
    ControlExtender="MaskedEditExtenderToDate2"
    ControlToValidate="TextBoxToDate2"
    Display="Dynamic"
    EmptyValueMessage="To Date is required"
    InvalidValueMessage="To Date is invalid"
    IsValidEmpty="False"
    TooltipMessage="Input a To Date">
</ajaxToolkit:MaskedEditValidator>
