<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="LocationPrint.aspx.cs"
    Inherits="Common_GenericLabel"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Label ID="LabelTitle" runat="server" Text="<%$ Resources:Default, EnterStartLocation%>">
    </asp:Label>
    
    <asp:DropDownList ID="ddlStartLocation"  runat="server" DataSourceID="DataSourceLocations" DataTextField="<%$ Resources:Default, Location%>"></asp:DropDownList>
    
    <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, EnterEndLocation%>"></asp:Label>
    <asp:DropDownList ID="ddlEndLocation"  runat="server" DataSourceID="DataSourceLocations"  DataTextField="<%$ Resources:Default, Location%>" ></asp:DropDownList>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, Print%>" OnClick="ButtonPrint_Click" /><br />
   
    <asp:ObjectDataSource ID="DataSourceLocations"  runat="server" TypeName="Location" SelectMethod="GetLocations">
    
    <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="warehouseId" Type="Int32" />
            <asp:SessionParameter Name="StartLocation" SessionField="StartLocation" Type="String" />
            <asp:SessionParameter Name="EndLocation" SessionField="EndLocation" Type="String" />
            
    </SelectParameters>
    </asp:ObjectDataSource>
    
</asp:Content>

