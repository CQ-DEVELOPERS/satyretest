using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_LocationLabels : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e) 
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["locationList"] != null)
                    PrintLabels();
            }
        }
        catch { }
    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["RadioButtonListType"] = RadioButtonListType.SelectedValue;

            Location loc = new Location();
            DataSet ds = loc.GetLocations(Session["connectionStringName"].ToString(),
                                            int.Parse(Session["warehouseId"].ToString()),
                                            DropDownListFromAisle.SelectedValue,
                                            DropDownListToAisle.SelectedValue,
                                            DropDownListFromLevel.SelectedValue,
                                            DropDownListToLevel.SelectedValue);


            Session["LabelName"] = RadioButtonListType.SelectedValue + " " + RadioButtonListLabelSize.SelectedValue + ".lbl";
            Session["FromURL"] = "~/Common/LocationLabels.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            KeepValues(GridViewViewLocations);

            //CheckBox cb = new CheckBox();

            //foreach (GridViewRow row in GridViewViewLocations.Rows)
            //{
            //    cb = (CheckBox)row.FindControl("CheckBoxEdit");
            //    if (cb.Checked)
            //    {
            //        switch (RadioButtonListType.SelectedValue)
            //        {
            //            case "Pickface Label":
            //                Session["Title"] = GridViewViewLocations.DataKeys[row.RowIndex].Values["Location"].ToString();
            //                Session["Barcode"] = GridViewViewLocations.DataKeys[row.RowIndex].Values["Level"].ToString();
            //                break;
            //            case "Racking Label":
            //                Session["Title"] = GridViewViewLocations.DataKeys[row.RowIndex].Values["Level"].ToString();
            //                break;
            //            case "Security Code Label":
            //                Session["Title"] = GridViewViewLocations.DataKeys[row.RowIndex].Values["SecurityCode"].ToString();
            //                break;
            //        }

            //        if (Session["Printer"].ToString() == "")
            //            Response.Redirect("~/Common/NLLabels.aspx");
            //        else
            //        {
            //            Session["Printing"] = true;
            //            Response.Redirect("~/Common/NLPrint.aspx");
            //        }

            //        GridViewViewLocations.DeleteRow(row.RowIndex);

            //        if (GridViewViewLocations.Rows.Count > 0)
            //            Session["GridViewViewLocations"] = GridViewViewLocations;
            //        else
            //            Session["GridViewViewLocations"] = null;

            //        break;
            //    }
            //}
        }
        //foreach (DataRow dr in ds.Tables[0].Rows)
        //    try
        //    {
        //        switch (RadioButtonListType.SelectedValue)
        //        {
        //            case "Pickface Label":
        //                Session["Title"] = dr["Location"].ToString();
        //                Session["Barcode"] = dr["Level"].ToString();
        //                break;
        //            case "Racking Label":
        //                Session["Title"] = dr["Level"].ToString();
        //                break;
        //            case "Security Code Label":
        //                Session["Title"] = dr["SecurityCode"].ToString();
        //                break;
        //        }

        //        if (Session["Printer"].ToString() == "")
        //            Response.Redirect("~/Common/NLLabels.aspx");
        //        else
        //        {
        //            Session["Printing"] = true;
        //            Response.Redirect("~/Common/NLPrint.aspx");
        //        }
        //    }

        catch { }
    }

    protected void KeepValues(GridView gv)
    {
        CheckBox cb = new CheckBox();
        ArrayList locationList = new ArrayList();
        ArrayList levelList = new ArrayList();
        ArrayList securityCodeList = new ArrayList();

        foreach (GridViewRow row in gv.Rows)
        {
            cb = (CheckBox)row.FindControl("CheckBoxEdit");
            if (cb.Checked)
            {
                locationList.Add(gv.DataKeys[row.RowIndex].Values["Location"].ToString());
                levelList.Add(gv.DataKeys[row.RowIndex].Values["Level"].ToString());
                securityCodeList.Add(gv.DataKeys[row.RowIndex].Values["SecurityCode"].ToString());

                //switch (RadioButtonListType.SelectedValue)
                //{
                //    case "Pickface Label":
                //        Session["Title"] = gv.DataKeys[row.RowIndex].Values["Location"].ToString();
                //        Session["Barcode"] = gv.DataKeys[row.RowIndex].Values["Level"].ToString();
                //        break;
                //    case "Racking Label":
                //        Session["Title"] = gv.DataKeys[row.RowIndex].Values["Level"].ToString();
                //        break;
                //    case "Security Code Label":
                //        Session["Title"] = gv.DataKeys[row.RowIndex].Values["SecurityCode"].ToString();
                //        break;
                //}

                //gv.DeleteRow(row.RowIndex);

                //if (gv.Rows.Count > 0)
                //    Session["GridViewViewLocations"] = gv;
                //else
                //    Session["GridViewViewLocations"] = null;

                //if (Session["Printer"].ToString() == "")
                //    Response.Redirect("~/Common/NLLabels.aspx");
                //else
                //{
                //    Session["Printing"] = true;
                //    Response.Redirect("~/Common/NLPrint.aspx");
                //}

                //break;
            }
        }
        if (locationList.Count > 0)
        {
            Session["locationList"] = locationList;
            Session["levelList"] = levelList;
            Session["securityCodeList"] = securityCodeList;

            PrintLabels();
        }
    }

    #region PrintLabels
    protected void PrintLabels()
    {
        ArrayList locationList = (ArrayList)Session["locationList"];
        ArrayList levelList = (ArrayList)Session["levelList"];
        ArrayList securityCodeList = (ArrayList)Session["securityCodeList"];

        switch (Session["RadioButtonListType"].ToString())
        {
            case "Pickface Label":
                Session["Title"] = locationList[0].ToString();
                Session["Barcode"] = levelList[0].ToString();
                break;
            case "Second Level Label":
                Session["Title"] = locationList[0].ToString();
                Session["Barcode"] = levelList[0].ToString();
                break;
            case "Second Level Plain Label":
                Session["Title"] = locationList[0].ToString();
                Session["Barcode"] = levelList[0].ToString();
                break;
            case "Racking Label":
                Session["Title"] = levelList[0].ToString();
                break;
            case "Security Code Label":
                Session["Title"] = securityCodeList[0].ToString();
                break;
            case "Left Location Label":
                Session["Title"] = locationList[0].ToString();
                Session["Barcode"] = levelList[0].ToString();
                break;
            case "Right Location Label":
                Session["Title"] = locationList[0].ToString();
                Session["Barcode"] = levelList[0].ToString();
                break;
            case "Product Bin Down Label":
                Session["Title"] = locationList[0].ToString();
                Session["Barcode"] = levelList[0].ToString();
                break;
            case "Product Bin Up Label":
                Session["Title"] = locationList[0].ToString();
                Session["Barcode"] = levelList[0].ToString();
                break;
        }

        locationList.RemoveAt(0);
        levelList.RemoveAt(0);
        securityCodeList.RemoveAt(0);

        if (locationList.Count > 0)
        {
            Session["locationList"] = locationList;
            Session["levelList"] = levelList;
            Session["securityCodeList"] = securityCodeList;
        }
        else
        {
            Session["locationList"] = null;
            Session["levelList"] = null;
            Session["securityCodeList"] = null;
        }

        if (Session["Printer"].ToString() == "")
            Response.Redirect("~/Common/NLLabels.aspx");
        else
        {
            Session["Printing"] = true;
            Response.Redirect("~/Common/NLPrint.aspx");
        }
    }
    #endregion PrintLabels

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {

        }
        catch { }
    }
}
