using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ManageOperators : System.Web.UI.Page
{   Operator op = new Operator();

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewOperators.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            Master.MsgText = "";
            Master.ErrorText = ex.Message.ToString();
        }

    }
    #endregion ButtonSelect_Click
    
    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList checkList = new ArrayList();

            Session["checkedList"] = null;

            foreach (GridViewRow row in GridViewOperators.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    checkList.Add(GridViewOperators.DataKeys[row.RowIndex].Values["OperatorId"]);
            }

            if (checkList.Count > 0)
                Session["checkedList"] = checkList;

            Session["LabelName"] = "Sign In Card.lbl";

            Session["FromURL"] = "~/Common/ManageOperators.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch (Exception ex)
        {
            Master.MsgText = "";
            Master.ErrorText = ex.Message.ToString();
        }
    }
    #endregion ButtonPrint_Click
}
