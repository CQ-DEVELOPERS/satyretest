﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DriverSearch.ascx.cs" Inherits="Common_DriverSearch" %>

<asp:GridView ID="GridViewContactSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataSourceID="ObjectDataSourceContact"
    DataKeyNames="ContactListId,ContactPerson"
    OnSelectedIndexChanged="GridViewContactSearch_OnSelectedIndexChanged">
    <Columns>
        <asp:BoundField DataField="ContactPerson" HeaderText="<%$ Resources:Default, ContactPerson2%>" />
    </Columns>
</asp:GridView>

<asp:ObjectDataSource ID="ObjectDataSourceContact" runat="server" TypeName="Contact" SelectMethod="GetDriverByContactId">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>