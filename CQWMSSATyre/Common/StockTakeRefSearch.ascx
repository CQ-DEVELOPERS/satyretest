﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StockTakeRefSearch.ascx.cs" Inherits="Common_WebUserControl" %>

<asp:GridView ID="GridViewStockTakeRefSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataSourceID="ObjectDataSourceStockTakeRef"
    DataKeyNames="StockTakeReferenceId"
    OnSelectedIndexChanged="GridViewStockTakeRefSearch_OnSelectedIndexChanged">
    <Columns>
        <asp:BoundField DataField="StockTakeReferenceId" HeaderText="<%$ Resources:Default, StockTakeReference%>" />
        <asp:BoundField DataField="WarehouseId" HeaderText="<%$ Resources:Default, Warehouse%>" />
        <asp:BoundField DataField="StartDate" HeaderText="<%$ Resources:Default, StartDate%>" />
        <asp:BoundField DataField="EndDate" HeaderText="<%$ Resources:Default, EndDate%>" />
    </Columns>
</asp:GridView>

<asp:ObjectDataSource ID="ObjectDataSourceStockTakeRef" runat="server" TypeName="StockTakeRef" SelectMethod="GetStockTakeRefByWarehouse">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
    </SelectParameters>
</asp:ObjectDataSource>