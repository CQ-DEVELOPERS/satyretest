﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_RouteSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["RouteId"] == null)
                Session["RouteId"] = -1;
        }
    }
    #endregion Page_Load

    #region GridViewRouteSearch_OnSelectedIndexChanged
    protected void GridViewRouteSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["RouteId"] = GridViewRouteSearch.SelectedDataKey["RouteId"];
        Session["Route"] = GridViewRouteSearch.SelectedDataKey["Route"];
    }
    #endregion GridViewContactSearch_OnSelectedIndexChanged
}
