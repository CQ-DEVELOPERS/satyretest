using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ChangeAnothersPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ButtonChange_Click(object sender, EventArgs e)
    {
        try
        {
            LogonCredentials lc = new LogonCredentials();

            Operator o = new Operator();
            
            if (lc.ChangePassword(DropDownListOperator.SelectedItem.ToString(), TextBoxPassword.Text))
            {
                if (o.UpdateOperatorPassword(Session["ConnectionStringName"].ToString(), int.Parse(DropDownListOperator.SelectedValue), TextBoxPassword.Text))
                {
                    Master.ErrorText = "";
                    Master.MsgText = "Users password succefully changed";
                }
                else
                {
                    Master.MsgText = "";
                    Master.ErrorText = "There was an error changing the user password.";
                }
            }
            else
            {
                Master.MsgText = "";
                Master.ErrorText = "There was an error changing the user password.";
            }

        }
        catch { }
    }
}
