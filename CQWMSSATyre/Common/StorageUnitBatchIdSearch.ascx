<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageUnitBatchIdSearch.ascx.cs" Inherits="Common_StorageUnitBatchIdSearch" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default,ProductCode %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default,Product %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default,SKUCode %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxSKUCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelSKU" runat="server" Text="<%$ Resources:Default,SKU %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxSKU" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default,Batch %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelECLNumber" runat="server" Text="<%$ Resources:Default,ECLNumber %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxECLNumber" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default,Search %>" OnClick="ButtonSearch_Click" />
        </td>
    </tr>
</table>

<asp:GridView ID="GridViewProductSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataKeyNames="StorageUnitBatchId,Product"
    OnSelectedIndexChanged="GridViewProductSearch_OnSelectedIndexChanged"
    OnPageIndexChanging="GridViewProductSearch_PageIndexChanging">
    <Columns>
        <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default,Product %>" />
        <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default,ProductCode %>" />
        <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default,SKUCode %>" />
        <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default,SKU %>" />
        <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default,Batch %>" />
        <asp:BoundField DataField="ECLNumber" HeaderText="<%$ Resources:Default,ECLNumber %>" />
        <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default,PalletQuantity %>" />
        <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default,Location %>" />
        <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default,Area %>" />
        <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default,Status %>" />
        <asp:BoundField DataField="Yield" HeaderText="<%$ Resources:Default,Yield %>" />
    </Columns>
</asp:GridView>