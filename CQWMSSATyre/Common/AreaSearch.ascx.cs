using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_AreaSearch : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["AreaId"] == null)
                Session["AreaId"] = -1;
        }
    }
    #endregion Page_Load

    #region GridViewAreaSearch_OnSelectedIndexChanged
    protected void GridViewAreaSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["AreaId"] = GridViewAreaSearch.SelectedDataKey["AreaId"];
        Session["Area"] = GridViewAreaSearch.SelectedDataKey["Area"];
    }
    #endregion GridViewAreaSearch_OnSelectedIndexChanged
}
