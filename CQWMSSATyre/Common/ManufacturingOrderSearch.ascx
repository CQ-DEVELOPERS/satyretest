<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManufacturingOrderSearch.ascx.cs" Inherits="Common_ManufacturingOrderSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>

<asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelOutboundShipmentId" runat="server" Text="<%$ Resources:Default, OutboundShipmentId %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxOutboundShipmentId" runat="server" Width="150px" OnTextChanged="TextBoxOutboundShipmentId_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxOrderNumber" runat="server" Width="150px" OnTextChanged="TextBoxOrderNumber_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text='<%$ Resources:Default,ProductCode %>' Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProductCode" runat="server" Width="150px" OnTextChanged="TextBoxProductCode_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelProduct" runat="server" Text='<%$ Resources:Default,Product %>' Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProduct" runat="server" Width="150px" OnTextChanged="TextBoxProduct_TextChanged"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxBatch" runat="server" Width="150px" OnTextChanged="TextBoxBatch_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="LabelLocation" runat="server" Text="<%$ Resources:Default, Location %>" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListLocation" runat="server" DataTextField="<%$ Resources:Default, Location%>"
                    DataValueField="LocationId" DataSourceID="ObjectDataSourceLocation" Width="150px" OnSelectedIndexChanged="DropDownListLocation_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <uc1:DateRange ID="DateRange1" runat="server" />
</asp:Panel>

<ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
    TargetControlID="PanelSearch"
    Radius="10"
    Color="#EFEFEC"
    BorderColor="#404040"
    Corners="All" />

<asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
    SelectMethod="GetLocationsByAreaCode">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="String" />
        <asp:Parameter Name="AreaCode" DefaultValue="R" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
    TargetControlID="TextBoxOutboundShipmentId" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
    TargetControlID="TextBoxOrderNumber" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
    TargetControlID="TextBoxProductCode" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server"
    TargetControlID="TextBoxProduct" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server"
    TargetControlID="TextBoxBatch" WatermarkCssClass="watermarked" WatermarkText="{All}">
</ajaxToolkit:TextBoxWatermarkExtender>