<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DivisionSearch.ascx.cs" Inherits="Common_DivisionsSearch" %>

<table>
    <tr>
        <td>
            <asp:Label ID="LabelDivisionCode" runat="server" Text="<%$ Resources:Default, DivisionCode%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxDivisionCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelDivision" runat="server" Text="<%$ Resources:Default, Division%>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxDivision" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
        </td>
    </tr>
</table>
<asp:GridView ID="GridViewDivisionSearch"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataSourceID="ObjectDataSourceDivision"
    DataKeyNames="DivisionId,DivisionCode"
    OnSelectedIndexChanged="GridViewDivisionSearch_OnSelectedIndexChanged">
    <Columns>
        <asp:BoundField DataField="DivisionCode" HeaderText="<%$ Resources:Default, DivisionCode%>" />
        <asp:BoundField DataField="Division" HeaderText="<%$ Resources:Default, Division%>" />
    </Columns>
</asp:GridView>

<asp:ObjectDataSource ID="ObjectDataSourceDivision" runat="server" TypeName="InboundDocument" SelectMethod="SearchDivision">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:ControlParameter Name="DivisionCode" ControlID="TextBoxDivisionCode" Type="String" />
        <asp:ControlParameter Name="Division" ControlID="TextBoxDivision" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>