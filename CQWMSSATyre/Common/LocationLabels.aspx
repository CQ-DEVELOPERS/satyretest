<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="LocationLabels.aspx.cs"
    Inherits="Common_LocationLabels"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <table style="text-align:left; vertical-align:top">
        <tr>
            <td valign="top">
                <asp:Label ID="LabelType" runat="server" Text="<%$ Resources:Default, LabelType%>"></asp:Label>
            </td>
            <td valign="top">
                <asp:RadioButtonList runat="server" ID="RadioButtonListType">
                    <asp:ListItem Text="<%$ Resources:Default, PickfaceLabel%>" Value="Pickface Label" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Default, SecondLevelLabel%>" Value="Second Level Label"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Default, SecondLevelPlainLabel%>" Value="Second Level Plain Label"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Default, RackingLabel%>" Value="Racking Label"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Default, SecurityCodeLabel%>" Value="Security Code Label"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Default, LeftLocationLabel%>" Value="Left Location Label"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Default, RightLocationLabel%>" Value="Right Location Label"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Default, ProductBinDownLabel%>" Value="Product Bin Down Label"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Default, ProductBinUpLabel%>" Value="Product Bin Up Label"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td valign="top">
                <asp:Label ID="LabelSize" runat="server" Text="<%$ Resources:Default, LabelSize%>"></asp:Label>
            </td>
            <td valign="top">
                <asp:RadioButtonList runat="server" ID="RadioButtonListLabelSize">
                    <asp:ListItem Text="<%$ Resources:Default, Small%>" Value="Small" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Default, Large%>" Value="Large"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="LabelFromAisle" runat="server" Text="<%$ Resources:Default, FromAisle%>"></asp:Label>
            </td>
            <td valign="top">
                <asp:DropDownList ID="DropDownListFromAisle" runat="server" DataSourceID="ObjectDataSourceAisle"
                    DataValueField="Aisle" DataTextField="<%$ Resources:Default, Aisle%>">
                </asp:DropDownList>
            </td>
            <td valign="top">
                <asp:Label ID="LabelToAisle" runat="server" Text="<%$ Resources:Default, ToAisle%>"></asp:Label>
            </td>
            <td valign="top">
                <asp:DropDownList ID="DropDownListToAisle" runat="server" DataSourceID="ObjectDataSourceAisle"
                    DataValueField="Aisle" DataTextField="<%$ Resources:Default, Aisle%>">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="LabelFromLevel" runat="server" Text="<%$ Resources:Default, FromLevel%>"></asp:Label>
            </td>
            <td valign="top">
                <asp:DropDownList ID="DropDownListFromLevel" runat="server" DataSourceID="ObjectDataSourceLevel"
                    DataValueField="Level" DataTextField="<%$ Resources:Default, Level%>">
                </asp:DropDownList>
            </td>
            <td valign="top">
                <asp:Label ID="LabelToLevel" runat="server" Text="<%$ Resources:Default, ToLevel%>"></asp:Label>
            </td>
            <td valign="top">
                <asp:DropDownList ID="DropDownListToLevel" runat="server" DataSourceID="ObjectDataSourceLevel"
                    DataValueField="Level" DataTextField="<%$ Resources:Default, Level%>">
                </asp:DropDownList>
            </td>
            <td valign="top">
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" /><br />
            </td>
        </tr>
    </table>
    <asp:ObjectDataSource ID="ObjectDataSourceAisle" runat="server" TypeName="StockTake"
        SelectMethod="AislesList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceLevel" runat="server" TypeName="StockTake"
        SelectMethod="LevelsList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, Print%>" OnClick="ButtonPrint_Click" />
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:GridView runat="server" ID="GridViewViewLocations" DataKeyNames="Location,Aisle,Column,Level,SecurityCode" DataSourceID="ObjectDataSourceViewLocations" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Location" HeaderText='<%$ Resources:Default,Location %>' />
                    <asp:BoundField DataField="Aisle" HeaderText='<%$ Resources:Default,Aisle %>' />
                    <asp:BoundField DataField="Column" HeaderText='<%$ Resources:Default,Column %>' />
                    <asp:BoundField DataField="Level" HeaderText='<%$ Resources:Default,Level %>' />
                    <asp:BoundField DataField="SecurityCode" HeaderText='<%$ Resources:Default,SecurityCode %>' />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID="ObjectDataSourceViewLocations"  runat="server" TypeName="Location" SelectMethod="GetLocations" DeleteMethod="DeleteLocations">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="warehouseId" Type="Int32" />
            <asp:ControlParameter Name="fromAisle" Type="String" ControlID="DropDownListFromAisle" PropertyName="SelectedValue" />
            <asp:ControlParameter Name="toAisle" Type="String" ControlID="DropDownListToAisle" PropertyName="SelectedValue" />
            <asp:ControlParameter Name="fromLevel" Type="String" ControlID="DropDownListFromLevel" PropertyName="SelectedValue" />
            <asp:ControlParameter Name="toLevel" Type="String" ControlID="DropDownListToLevel" PropertyName="SelectedValue" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="Location" Type="String" />
            <asp:Parameter Name="Aisle" Type="String" />
            <asp:Parameter Name="Column" Type="String" />
            <asp:Parameter Name="Level" Type="String" />
            <asp:Parameter Name="SecurityCode" Type="String" />
        </DeleteParameters>
    </asp:ObjectDataSource>
</asp:Content>

