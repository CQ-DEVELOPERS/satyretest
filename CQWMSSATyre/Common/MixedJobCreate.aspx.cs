using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Common_MixedJobCreate : System.Web.UI.Page
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                if (Session["InstructionTypeCode"] == null)
                    Session["InstructionTypeCode"] = "SM";

                if (Session["InstructionTypeCode"].ToString() == "SM")
                {
                    LabelAgenda.Text = "Inbound - Preparation for Put Away";
                }
                else
                {
                    LabelAgenda.Text = "Outbound";
                    ButtonPrint.Text = "Print Pick Sheet";
                }

                if (Session["OutboundShipmentId"] != null)
                    if(Session["OutboundShipmentId"].ToString() != "-1")
                        PanelSearch.Visible = false;
            }
            catch
            {
                //BusinessLayerValidation.SqlErrorMessage(ex.Message & this.Form.ID & "Hello this is me");
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewAvailable.DataBind();
    }
    #endregion "ButtonSearch_Click"

    #region GridViewAvailable_SelectedIndexChanged
    protected void GridViewAvailable_SelectedIndexChanged(object sender, EventArgs e)
    {
        int jobId = -1;

        if (int.TryParse(GridViewAvailable.SelectedDataKey.Value.ToString(), out jobId))
        {
            Session["JobId"] = jobId;

            GridViewLinked.DataBind();
            GridViewDetails.DataBind();
        }
    }
    #endregion "GridViewAvailable_SelectedIndexChanged"

    #region ButtonUnlinkedSearch_Click
    protected void ButtonUnlinkedSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (GridViewAvailable.SelectedIndex != -1)
                GridViewUnlinked.DataBind();
        }
        catch { }
    }
    #endregion "ButtonUnlinkedSearch_Click"

    #region GridViewLinked_SelectedIndexChanged
    protected void GridViewLinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        MixedJobCreate ds = new MixedJobCreate();

        if (ds.Remove(Session["ConnectionStringName"].ToString(),
                      Convert.ToInt32(GridViewAvailable.SelectedDataKey.Value.ToString()),
                      Convert.ToInt32(GridViewLinked.SelectedDataKey.Value.ToString())) == true)
        {
            GridViewAvailable.DataBind();
            GridViewUnlinked.DataBind();
            GridViewLinked.DataBind();
            GridViewDetails.DataBind();
        }
        else
            Response.Write("There was an error!!!");
    }
    #endregion "GridViewLinked_SelectedIndexChanged"

    #region GridViewUnlinked_SelectedIndexChanged
    protected void GridViewUnlinked_SelectedIndexChanged(object sender, EventArgs e)
    {
        MixedJobCreate ds = new MixedJobCreate();

        if (ds.Transfer(Session["ConnectionStringName"].ToString(),
                        Convert.ToInt32(GridViewAvailable.SelectedDataKey.Value.ToString()),
                        Convert.ToInt32(GridViewUnlinked.SelectedDataKey.Value.ToString())) == true)
        {
            GridViewAvailable.DataBind();
            GridViewUnlinked.DataBind();
            GridViewLinked.DataBind();
            GridViewDetails.DataBind();
        }
        else
            Response.Write("There was an error!!!");
    }
    #endregion "GridViewUnlinked_SelectedIndexChanged"

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            //Session["FromURL"] = "~/Common/MixedJobCreate.aspx";

            //if (Session["InstructionTypeCode"].ToString() == "SM")
            //    Session["ReportName"] = "Putaway Sheet";
            //else
            //    Session["ReportName"] = "Job Pick Sheet";

            ////ReportParameter[] RptParameters = new ReportParameter[4];

            ////// Create the JobId report parameter
            ////string jobId = Session["JobId"].ToString();
            ////RptParameters[0] = new ReportParameter("JobId", jobId);

            //RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            //RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            //RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());
            
            //Session["ReportParameters"] = RptParameters;

            //Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
    #endregion "ButtonPrint_Click"

    #region ButtonPlanningComplete_Click
    protected void ButtonPlanningComplete_Click(object sender, EventArgs e)
    {
        try
        {
            Status st = new Status();

            if (st.PlanningComplete(Session["ConnectionStringName"].ToString(), Convert.ToInt32(GridViewAvailable.SelectedDataKey.Value.ToString())))
            {
                Master.ErrorText = "";
                Master.MsgText = "Planning Complete Successful";

                GridViewAvailable.DataBind();
                GridViewUnlinked.DataBind();
                GridViewLinked.DataBind();
                GridViewDetails.DataBind();
            }
            else
            {
                Master.ErrorText = "Planning Complete Failed";
                Master.MsgText = "";
            }
        }
        catch { }
    }
    #endregion ButtonPlanningComplete_Click
}
