<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocationType.ascx.cs" Inherits="Common_LocationType" %>
<asp:DropDownList ID="DropDownListLocationType" runat="server" DataSourceID="ObjectDataSourceLocationType"
    DataValueField="LocationTypeId" DataTextField="LocationType" OnSelectedIndexChanged="DropDownListLocationType_SelectedIndexChanged">
</asp:DropDownList>
<asp:ObjectDataSource ID="ObjectDataSourceLocationType" runat="server"
    TypeName="LocationType" SelectMethod="GetLocationTypes">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
