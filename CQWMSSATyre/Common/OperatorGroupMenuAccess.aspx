<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="OperatorGroupMenuAccess.aspx.cs" Inherits="Common_Change_Operator_Group_Menu"
    Title="Menu Administration" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelTitle" runat="server" Text="Menu Administration" SkinID="PageTitle"></asp:Label>
    <br />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridAccess">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridAccess" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="DropDownList1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridAccess" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="DropDownList2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridAccess" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadButtonSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridAccess" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="buttonAddNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DropDownList1" />
                    <telerik:AjaxUpdatedControl ControlID="DropDownList2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <table>
        <tr>
            <td>
                <asp:Label runat="server" ID="Label1" Text="Operator Group"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataTextField="OperatorGroup"
                    DataValueField="OperatorGroupId" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label runat="server" ID="lblAccessMenu" Text="Menu Item"></asp:Label>
                <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" DataTextField="Menu"
                    DataValueField="MenuId" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"
                    Width="220px">
                </asp:DropDownList>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonSave" OnClick="RadButtonSave_Click" runat="server" Text="<%$ Resources:Default, ButtonSave %>">
                    <Icon PrimaryIconCssClass="rbSave" />
                </telerik:RadButton>
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="RadGridAccess" runat="server" Skin="Metro" AllowAutomaticUpdates="true"
        AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30"
        AutoGenerateColumns="False" DataSourceID="ObjectDataSourceProductChecking">
        <MasterTableView DataKeyNames="OperatorGroupId, MenuItemId, Access" 
            DataSourceID="ObjectDataSourceProductChecking" AutoGenerateColumns="False" EnableHeaderContextMenu="true" ShowGroupFooter="true">
            <Columns>
                <%--<telerik:GridBoundColumn ReadOnly="True" DataField="Menu" HeaderText="Menu" SortExpression="Menu"></telerik:GridBoundColumn>--%>
                <telerik:GridBoundColumn ReadOnly="True" DataField="MenuItem1" HeaderText="Level 1" SortExpression="MenuItem1"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="MenuItem2" HeaderText="Level 2" SortExpression="MenuItem2"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="MenuItem3" HeaderText="Level 3" SortExpression="MenuItem3"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="MenuItem4" HeaderText="Level 4" SortExpression="MenuItem4"></telerik:GridBoundColumn>
                <telerik:GridCheckBoxColumn ReadOnly="true" DataField="Access" HeaderText="Current Access" SortExpression="Access"></telerik:GridCheckBoxColumn>
                <telerik:GridTemplateColumn UniqueName="Access" HeaderText="New Value">
                    <ItemTemplate>
                        <asp:CheckBox ID="cbAccess" runat="server" Checked='<%# Bind("Access") %>' Width="80px"></asp:CheckBox>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
            <GroupByExpressions>
                <telerik:GridGroupByExpression>
                <SelectFields>
                    <telerik:GridGroupByField FieldAlias="Menu" FieldName="Menu" />
                </SelectFields>
                <GroupByFields>
                    <telerik:GridGroupByField FieldAlias="Menu" FieldName="Menu" />
                </GroupByFields>
                </telerik:GridGroupByExpression>
            </GroupByExpressions>
        </MasterTableView>
        <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
            <Resizing AllowColumnResize="true"></Resizing>
        </ClientSettings>
        <GroupingSettings ShowUnGroupButton="True" />
    </telerik:RadGrid>
    <asp:ObjectDataSource ID="ObjectDataSourceProductChecking" runat="server" TypeName="MenuItemsDynamic"
        SelectMethod="SearchMenuItemAccess" UpdateMethod="UpdateMenuItemAccess">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:ControlParameter Name="OperatorGroupId" ControlID="DropDownList1" Type="Int32" />
            <asp:ControlParameter Name="MenuId" ControlID="DropDownList2" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
            <asp:Parameter Name="MenuItemId" Type="Int32" />
            <asp:Parameter Name="Access" Type="Boolean" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <table style="width: 445px">
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblAddOperator" runat="server" Text="Add Operator Group" SkinID="PageTitle"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 88px">
                        <asp:Label ID="lblDescription" runat="server" Font-Bold="False" Font-Names="Verdana"
                            Font-Size="0.8em" Style="font-size: 0.9em; color: black; font-family: Verdana"
                            Text="Description"></asp:Label></td>
                    <td style="width: 125px">
                        <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox></td>
                    <td style="width: 6px">
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription"
                            ErrorMessage="RequiredFieldValidator" SetFocusOnError="True" Width="93px">*Required Field</asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td style="width: 88px">
                        <asp:Label ID="Label3" runat="server" Text="Copy From Group"></asp:Label></td>
                    <td style="width: 125px">
                        <asp:DropDownList ID="ddlAddNew" runat="server" DataTextField="OperatorGroup" DataValueField="OperatorGroupId">
                        </asp:DropDownList></td>
                    <td style="width: 6px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 88px">
                        <asp:Button ID="buttonAddNew" runat="server" Text="Add New" OnClick="buttonAddNew_Click"
                            SkinID="ButtonLarge" /></td>
                    <td style="width: 125px">
                    </td>
                    <td style="width: 6px">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
