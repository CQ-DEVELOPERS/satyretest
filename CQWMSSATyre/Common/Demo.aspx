<%@ Page
    Language="C#"
    MasterPageFile="~/MasterPages/Demo.master"
    AutoEventWireup="true"
    CodeFile="Demo.aspx.cs"
    Inherits="_Demo"
    Title="Change Password" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:ToolkitScriptManager EnablePartialRendering="true"  runat="Server" ID="ScriptManager1" />
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
    <%--<asp:Image ID="imgbg" runat="server" ImageUrl="../Images/Demo/Body.gif" />--%>
    <marquee id="MARQUEE1" style="text-align: left; " bgcolor="white" behavior="scroll" direction="up" width="600" height="30" scrollamount="1" scrolldelay="10" onmouseover="this.stop();" onmouseout="this.start();">
        <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
            <ContentTemplate>
                <asp:GridView ID="GridViewInstruction" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceOperators" DataKeyNames="OperatorId" PageSize="30" 
                    EnableTheming="false" ShowHeader="false" BorderStyle="None" BorderWidth="0">
                    <Columns>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                            </ItemTemplate>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>" SortExpression="Operator" />
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </marquee>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:Image ID="ticker_tile" runat="server" ImageUrl="../Images/Demo/ticker_header.gif" />
            </td>
            <td style="background-image: url(../Images/Demo/ticker_tile.gif); width:100%">
                <asp:RadioButtonList ID="rblOperators" runat="server" RepeatDirection="Horizontal" DataSourceID="ObjectDataSourceOperators" DataKeyNames="OperatorId" 
                              DataTextField="Operator" DataValueField="OperatorId">
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:Image ID="Image1" runat="server" ImageUrl="../Images/Demo/ticker_close.gif" />
            </td>
        </tr>
    </table>
    
    
    <asp:ObjectDataSource ID="ObjectDataSourceOperators" runat="server" TypeName="Demo"
        SelectMethod="GetOperators">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>