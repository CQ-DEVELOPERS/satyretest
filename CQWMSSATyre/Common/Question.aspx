<%@ Page
    Language="C#"
    MasterPageFile="~/MasterPages/Empty.master"
    AutoEventWireup="true"
    CodeFile="Question.aspx.cs"
    Inherits="Common_Question"
    Title="Question"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/Empty.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
         <asp:View ID="View1" runat="server">
            <asp:Label ID="QuestionText" runat="server" Text="<%$ Resources:Default, QuestionYn%>"></asp:Label>
            <asp:RadioButtonList ID="QuestionValueRadio" runat="server">
            <asp:ListItem Text="<%$ Resources:Default, Yes%>"></asp:ListItem> 
            <asp:ListItem Text="<%$ Resources:Default, No%>"></asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:LinkButton ID="LinkButton1" runat="server" TabIndex="6" Text="<%$ Resources:Default, Next%>" OnClick="LinkButtonView1_Click"></asp:LinkButton>
            
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:Label ID="QuestionTextBox" runat="server" Text="<%$ Resources:Default, QuestionBox%>"></asp:Label>
            <asp:TextBox ID="QuestionValueTextBox" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:LinkButton ID="LinkButton3" runat="server" TabIndex="6" Text="<%$ Resources:Default, Next%>" OnClick="LinkButtonView2_Click"></asp:LinkButton>
            
            
        </asp:View>        
   </asp:MultiView>
</asp:Content>
        