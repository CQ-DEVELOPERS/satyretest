<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="TaskMaster.aspx.cs" Inherits="Common_TaskMaster"
    Title="Task Master" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundSearch"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="Task Master"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Adminsitration"></asp:Label>
    <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Orders%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>" />
                        <asp:GridView ID="GridViewInstruction" runat="server" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceTaskList"
                            DataKeyNames="TaskListId" OnSelectedIndexChanged="GridViewInstruction_SelectedIndexChanged"
                            PageSize="60">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" ShowSelectButton="True"></asp:CommandField>
                                <asp:BoundField ReadOnly="true" DataField="TaskListId" HeaderText="#" SortExpression="TaskListId"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>                                   
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="true" DataField="Task" HeaderText="Task" SortExpression="Task"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Billable" HeaderText="Billable" SortExpression="Billable"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="ProjectName" HeaderText="ProjectName" SortExpression="ProjectName"></asp:BoundField>                                
                                <asp:BoundField ReadOnly="true" DataField="SponsorCode" HeaderText="SponsorCode" SortExpression="SponsorCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="DeveloperCode" HeaderText="DeveloperCode" SortExpression="DeveloperCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="AssistCode" HeaderText="AssistCode" SortExpression="AssistCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="TesterCode" HeaderText="TesterCode" SortExpression="TesterCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="ApprovedCode" HeaderText="ApprovedCode" SortExpression="ApprovedCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="EstimateHours" HeaderText="EstimateHours" SortExpression="EstimateHours"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="ActualHours" HeaderText="ActualHours" SortExpression="ActualHours"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="TargetDate" HeaderText="TargetDate" SortExpression="TargetDate"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Comments" HeaderText="Comments" SortExpression="Comments"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Percentage" HeaderText="Percentage" SortExpression="Percentage"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Cycle" HeaderText="Cycle" SortExpression="Cycle"></asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="OrderBy" HeaderText="OrderBy" SortExpression="OrderBy"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceTaskList" runat="server" TypeName="Tasks"
                    SelectMethod="SearchTasks" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringCommon" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringCommon" Type="String" />
                        <asp:Parameter Name="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" TypeName="Priority" SelectMethod="GetPriorities">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringCommon"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
                    SelectMethod="GetOperatorList">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringCommon"
                            Type="String" />
                        <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Projects">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelProjects">
                    <ContentTemplate>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="Users">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelUsers">
                    <ContentTemplate>
                    </ContentTemplate>
                    <Triggers>
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel4" HeaderText="Priorities">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelPriorities" runat="server">
                    <ContentTemplate>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
