using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ReceivingBay : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["LocationId"] = -1;
    }
    protected void DropDownListBay_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["LocationId"] = int.Parse(DropDownListBay.SelectedValue);
    }
}
