using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Common_ProductBatchLabel : System.Web.UI.Page
{
    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }

    #region GridViewProductSearch_OnSelectedIndexChanged
    protected void GridViewProductSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Session["StorageUnitBatchId"] = GridViewProductSearch.SelectedDataKey["StorageUnitBatchId"];
        Session["Product"] = GridViewProductSearch.SelectedDataKey["Product"];
    }
    #endregion GridViewProductSearch_OnSelectedIndexChanged

    #region GridViewProductSearch_PageIndexChanging
    protected void GridViewProductSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridViewProductSearch.PageIndex = e.NewPageIndex;
            GridViewProductSearch_DataBind();
        }
        catch { }
    }
    #endregion "GridViewProductSearch_PageIndexChanging"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewProductSearch.PageIndex = 0;

        GridViewProductSearch_DataBind();
    }
    #endregion "ButtonSearch_Click"

    protected void GridViewProductSearch_DataBind()
    {
        Product product = new Product();

        GridViewProductSearch.DataSource = product.SearchProductsBatchId(Session["ConnectionStringName"].ToString(), TextBoxProductCode.Text, TextBoxProduct.Text, TextBoxSKUCode.Text, TextBoxSKU.Text, TextBoxBatch.Text);

        GridViewProductSearch.DataBind();
    }

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewProductSearch.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletLabel" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();
            string nop;
            int numberToPrint;
            string rli;
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewProductSearch.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    checkedList.Add((int)GridViewProductSearch.DataKeys[row.RowIndex].Values["StorageUnitBatchId"]);
                }
                Exceptions ex = new Exceptions();
                rli = ((HiddenField)row.FindControl("HiddenReceiptLineId")).Value;
                Session["rli"] = int.Parse(rli);
                int count = ex.CountGreenPrint(Session["ConnectionStringName"].ToString(), (int)Session["rli"]);
                nop = ((HiddenField)row.FindControl("HiddenNumberOfPallets")).Value;
                numberToPrint = int.Parse(nop) - count;

                if (int.Parse(TextBoxQty.Text) > numberToPrint)
                {
                    Session["LabelCopies"] = numberToPrint;
                    result = SendErrorNow("Some lines exceeded number of allowed labels - defaulted to number of pallets");
                    Master.ErrorText = result;
                }
                else
                {
                    Session["LabelCopies"] = int.Parse(TextBoxQty.Text);
                }

                if (ex.CreateExceptionGreen(Session["ConnectionStringName"].ToString(), (int)Session["rli"], (int)Session["LabelCopies"], (int)Session["OperatorId"]) == false)
                {
                    return;
                }
            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 199))
                Session["LabelName"] = "Verify Batch Label Small.lbl";
            else
                Session["LabelName"] = "Verify Batch Label Large Version 2.lbl";

            Session["FromURL"] = "~/Common/BatchLabel.aspx";

            //Session["LabelCopies"] = int.Parse(TextBoxQty.Text);

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("../Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch { }
    }

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "PutawayMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "PutawayMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
