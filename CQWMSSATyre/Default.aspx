<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Title="CQuential Solutions" StylesheetTheme="Default" Theme="Default"  %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Charting" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultAgenda %>"></asp:Label>
    <br />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
</asp:Content>

