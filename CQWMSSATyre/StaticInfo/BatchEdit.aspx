<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="BatchEdit.aspx.cs" Inherits="StaticInfo_BatchEdit" Title="Batch Maintenance"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/BatchNumberSearch.ascx" TagName="BatchNumberSearch" TagPrefix="uc1" %>
<%@ Register Src="../Common/BatchMaintenace.ascx" TagName="BatchMaintenace" TagPrefix="uc2" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, Batch%>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, HousekeepingModule%>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, SelectaBatch%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <uc1:BatchNumberSearch ID="BatchNumberSearch1" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="<%$ Resources:Default, UpdateaBatch%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <uc2:BatchMaintenace ID="BatchMaintenace1" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel3" HeaderText="<%$ Resources:Default, LinkProducts%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <h4>
                                Search</h4>
                        </td>
                        <td>
                            <h4>
                                Linked</h4>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <asp:UpdatePanel ID="UpdatePanelProductSearch" runat="server">
                                <ContentTemplate>
                                    <uc3:ProductSearch ID="ProductSearch1" runat="server" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonAdd" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:Button ID="ButtonAdd" runat="server" Text="<%$ Resources:Default, Add%>" OnClick="ButtonAdd_Click" />
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <asp:GridView ID="GridViewProductSearch" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        DataSourceID="ObjectDataSourceProduct">
                                        <Columns>
                                            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product%>" />
                                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode%>" />
                                            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode%>" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <h5>
                                                No rows</h5>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonAdd" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <%--
                <asp:UpdatePanel ID="UpdatePanelBatchFields" runat="server">
                    <ContentTemplate>
                        <asp:DetailsView ID="DetailsViewInsertBatchFields" runat="server" AutoGenerateRows="TRUE" DataSourceID="ObjectDataSourceBatchFields" DefaultMode="Insert"  >
                            <Fields>
                            </Fields>
                        </asp:DetailsView>
                    </ContentTemplate>
                </asp:UpdatePanel>--%>
    <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="Batch"
        SelectMethod="LinkedProducts">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="String" />
            <asp:SessionParameter Name="BatchId" SessionField="BatchId" Type="int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <%--    <asp:ObjectDataSource ID="ObjectDataSourceBatchFields" runat="server" TypeName="Batch" SelectMethod="GetBatchFields">   
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>   --%>
</asp:Content>
