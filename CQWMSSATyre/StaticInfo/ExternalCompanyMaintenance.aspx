﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ExternalCompanyMaintenance.aspx.cs" Inherits="StaticInfo_ExternalCompanyMaintenance"
    Title="<%$ Resources:Default, MovementTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ExternalCompanyMaintenance %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, HousekeepingModule%>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, ExternalCompany%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="updatePanel_ExternalCompany" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="PanelExternalCompany" runat="server" Width="450px" Visible="false">
                            <asp:Button ID="ButtonExternalCompany" runat="server" Text="<%$ Resources:Default, AddExternalCompany%>"
                                OnClick="ButtonAddExternalCompany_Click" />
                            <asp:GridView ID="GridViewExternalCompany" DataSourceID="ObjectDataSourceExternalCompany"
                                DataKeyNames="ExternalCompanyId" runat="server" AllowPaging="True" AllowSorting="True"
                                PageSize="15" AutoGenerateColumns="False" EmptyDataText="<%$ Resources:Default, NoSuppliers%>">
                                <Columns>
                                    <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ExternalCompanyType%>" SortExpression="ExternalCompanyType">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DropDownListExternalCompanyType" runat="server" DataSourceID="ObjectdatsourceExternalCompanyType"
                                                DataTextField="ExternalCompanyType" DataValueField="ExternalCompanyTypeId" SelectedValue='<%# Bind("ExternalCompanyTypeId") %>'>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ExternalCompanyType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, RouteId%>" DataField="RouteId"
                                        SortExpression="RouteId" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, ExternalCompany%>" DataField="ExternalCompany"
                                        SortExpression="ExternalCompany" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, ExternalCompanyCode%>" DataField="ExternalCompanyCode"
                                        SortExpression="ExternalCompanyCode" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Rating%>" DataField="Rating" SortExpression="Rating" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, RedeliveryIndicator%>" DataField="RedeliveryIndicator"
                                        SortExpression="RedeliveryIndicator" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, QAIndicator%>" DataField="QualityAssuranceIndicator"
                                        SortExpression="QualityAssuranceIndicator" />
                                </Columns>
                            </asp:GridView>
                            <asp:DetailsView Visible="false" ID="DetailsViewInsertExternalCompany" runat="server"
                                AutoGenerateRows="False" DataSourceID="ObjectDataSourceExternalCompany" DefaultMode="Insert">
                                <Fields>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ExternalCompanyId%>" InsertVisible="False"
                                        SortExpression="ExternalCompanyId">
                                        <InsertItemTemplate>
                                            <asp:Button ID="AddArea" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Add%>" />
                                        </InsertItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ExternalCompanyType%>">
                                        <InsertItemTemplate>
                                            <asp:DropDownList ID="DropDownListExternalCompanyType" runat="server" DataSourceID="ObjectdatsourceExternalCompanyType"
                                                DataTextField="ExternalCompanyType" DataValueField="ExternalCompanyTypeId" SelectedValue='<%# Bind("ExternalCompanyTypeId") %>'>
                                            </asp:DropDownList>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, RouteId%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxRouteId" runat="server" Text='<%# Bind("RouteId") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ExternalCompany%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxExternalCompany" runat="server" Text='<%# Bind("ExternalCompany") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ExternalCompanyCode%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxExternalCompanyCode" runat="server" Text='<%# Bind("ExternalCompanyCode") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Rating%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxRating" runat="server" Text='<%# Bind("Rating") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, RedeliveryIndicator%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxRedeliveryIndicator" runat="server" Text='<%# Bind("RedeliveryIndicator") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, QAIndicator%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxQualityAssuranceIndicator" runat="server" Text='<%# Bind("QualityAssuranceIndicator") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <InsertItemTemplate>
                                            <asp:LinkButton ID="btnlocInsert" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Insert%>"
                                                OnClick="btnlocInsert_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="btnlocCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                                Text="<%$ Resources:Default, Cancel%>" OnClick="btnlocCancel_Click"></asp:LinkButton>
                                        </InsertItemTemplate>
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>
                        </asp:Panel>
                        <asp:ObjectDataSource ID="ObjectDataSourceExternalCompany" runat="server" TypeName="ExternalCompany"
                            SelectMethod="SearchExternalCompanies" UpdateMethod="UpdateExternalCompany" DeleteMethod="DeleteExternalCompany"
                            InsertMethod="InsertExternalCompany" OnInserting="ObjectDataSourceExternalCompany_Inserting"
                            OnInserted="ObjectDataSourceExternalCompany_Inserted">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="externalCompanyId" SessionField="ExternalCompanyId" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="externalCompanyTypeId" Type="Int32" />
                                <asp:Parameter Name="routeId" Type="Int32" />
                                <asp:Parameter Name="externalCompany" Type="string" />
                                <asp:Parameter Name="externalCompanyCode" Type="string" />
                                <asp:Parameter Name="rating" Type="Int32" />
                                <asp:Parameter Name="redeliveryIndicator" Type="Boolean" />
                                <asp:Parameter Name="qualityAssuranceIndicator" Type="Boolean" />
                                <asp:Parameter Name="externalCompanyId" Type="Int32" />
                            </UpdateParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="externalCompanyId" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="externalCompanyTypeId" Type="Int32" />
                                <asp:Parameter Name="routeId" Type="Int32" />
                                <asp:Parameter Name="externalCompany" Type="string" />
                                <asp:Parameter Name="externalCompanyCode" Type="string" />
                                <asp:Parameter Name="rating" Type="Int32" />
                                <asp:Parameter Name="redeliveryIndicator" Type="Boolean" />
                                <asp:Parameter Name="qualityAssuranceIndicator" Type="Boolean" />
                                <asp:Parameter Name="externalCompanyId" Type="Int32" />
                            </InsertParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectdatsourceExternalCompanyType" runat="server" TypeName="StaticInfoExternalCompanyType"
                            SelectMethod="GetExternalCompanyType">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="externalCompanyTypeId" SessionField="ExternalCompanyTypeId"
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
