using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using Microsoft.Reporting.WebForms;

public partial class StaticInfo_InstanceWizard : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "";
                Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstanceWizard" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception ex)
        {
            result = SendErrorNow("InstanceWizard" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_LoadComplete

    #region Wizard1_NextButtonClick
    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
        theErrMethod = "Wizard1_NextButtonClick";

        try
        {
            if (Wizard1.ActiveStepIndex == 0)
            {
            }

            if (Wizard1.ActiveStepIndex == 2)
            {
                Wizard w = new Wizard();
                String versionCode = rblVersion.SelectedValue.ToString();

                if (w.SaveInstanceDetails(rbtEmail.Text, rtbFirstName.Text, rtbLastName.Text, rtbCompany.Text, rtbTelephone.Text, rtbClientURL.Text, versionCode))
                {
                    Master.MsgText = "Next"; Master.ErrorText = "";
                }
                else
                    throw new Exception("There was an error saving the details.");
            }

            Master.MsgText = "Next"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateAisle" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Wizard1_NextButtonClick

    #region Wizard1_FinishButtonClick
    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        theErrMethod = "Wizard1_FinishButtonClick";

        try
        {
            Session.Remove("ReferenceNumber");
            Response.Redirect("http://www.cquential.com");

            Master.MsgText = "Finish"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateAisle" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Wizard1_FinishButtonClick

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "ContainerPlanning", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "ContainerPlanning", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

    protected void rbCheckAvailability_Click(object sender, EventArgs e)
    {
        theErrMethod = "rbCheckAvailability_Click";

        try
        {
            Wizard w = new Wizard();

            if (w.CheckInstanceAvailability(rtbClientURL.Text))
            {
                rtbClientURL.ForeColor = System.Drawing.Color.LimeGreen;
                Master.MsgText = "Available";
                Master.ErrorText = "";
            }
            else
            {
                rtbClientURL.ForeColor = System.Drawing.Color.Red;
                Master.MsgText = ""; Master.ErrorText = "Sorry this instance is not aviable";
            }
        }
        catch { }
    }
}


