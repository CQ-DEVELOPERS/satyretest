using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

public partial class StaticInfo_LocationMaintenance : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
        #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewArea_SelectedIndexChanged";
        try
        {
            Session["AreaId"] = GridViewArea.SelectedDataKey["AreaId"];
            
            ObjectDataSourceLocation.DataBind();
            GridViewLocation.SelectedIndex = -1;
            PanelLocation.Visible = true;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LocationMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }

    protected void ButtonAddArea_Click(object sender, EventArgs e)
    {
        DetailsViewInsertArea.Visible = true;
        GridViewArea.Visible = false;
    }

    protected void ButtonAddLocation_Click(object sender, EventArgs e)
    {
        DetailsViewInsertLocation.Visible = true;
        GridViewLocation.Visible = false;
    }

    #region ButtonSelectArea_Click
    protected void ButtonSelectArea_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectArea_Click";
        try
        {
            foreach (ListItem item in CheckBoxListArea.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectArea_Click

    #region ButtonDeselectArea_Click
    protected void ButtonDeselectArea_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectArea_Click";

        try
        {
            foreach (ListItem item in CheckBoxListArea.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectArea_Click

    #region ButtonLinkAreaUnsel_Click

    protected void ButtonLinkAreaUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectArea_Click";
        try
        {
            List<int> areaList = new List<int>();

            foreach (ListItem item in CheckBoxListArea.Items)
            {
                if (item.Selected == true)
                {
                    areaList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int area in areaList)
            {
                Area.AreaPrincipal_Delete(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["PrincipalId"]), area);
            }

            CheckBoxListAreaUnsel.DataBind();
            CheckBoxListArea.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("AreaPrincipal" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkAreaUnsel_Click

    #region ButtonLinkArea_Click

    protected void ButtonLinkArea_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectArea_Click";
        try
        {
            List<int> areaList = new List<int>();

            foreach (ListItem item in CheckBoxListAreaUnsel.Items)
            {
                if (item.Selected == true)
                {
                    areaList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int area in areaList)
            {
                Area.AreaPrincipal_Add(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["PrincipalId"]), area);
            }

            CheckBoxListAreaUnsel.DataBind();
            CheckBoxListArea.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("AreaPrincipal" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkArea_Click

    #region ButtonSelectAreaUnsel_Click
    protected void ButtonSelectAreaUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectAreaUnsel_Click";
        try
        {
            foreach (ListItem item in CheckBoxListAreaUnsel.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectAreaUnsel_Click

    #region ButtonDeselectAreaUnsel_Click
    protected void ButtonDeselectAreaUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectAreaUnsel_Click";

        try
        {
            foreach (ListItem item in CheckBoxListAreaUnsel.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectAreaUnsel_Click


    protected void ObjectDataSourceArea_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsViewInsertArea.Visible = false;
        GridViewArea.Visible = true;
        Session["AreaId"] = int.Parse(e.ReturnValue.ToString());
        PanelLocation.Visible = true;
        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_Area, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }

    protected void ObjectDataSourceArea_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {

        string area = ((TextBox)DetailsViewInsertArea.FindControl("DetailTextArea")).Text;
        string areaCode = ((TextBox)DetailsViewInsertArea.FindControl("DetailTextAreaCode")).Text;
        string stockOnHand = ((CheckBox)DetailsViewInsertArea.FindControl("CheckBoxStockOnHand")).Checked.ToString();
        int areaId = int.Parse("0");

        e.InputParameters["area"] = area;
        e.InputParameters["areaCode"] = areaCode;
        e.InputParameters["stockOnHand"] = stockOnHand;
        e.InputParameters["areaId"] = areaId;
    }

    protected void GridViewPrincipal_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewPrincipal_SelectedIndexChanged";
        try
        {
            Session["PrincipalId"] = GridViewPrincipal.SelectedDataKey["PrincipalId"];

        }
        catch (Exception ex)
        {
            result = SendErrorNow("AreaPrincipalMaintenance" + "_" + ex.Message.ToString());
        }
    }

    protected void DetailsViewInsertAreaPrincipal_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        //ObjectDataSourceArea.Insert();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertArea.Visible = false;
        GridViewArea.Visible = true;
    }

    protected void btnlocInsert_Click(object sender, EventArgs e)
    {
       // ObjectDataSourceLocation.Insert();
    }

    protected void btnlocCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertLocation.Visible = false;
        GridViewLocation.Visible = true;
    }

    protected void ObjectDataSourceLocation_Inserting(object sender, ObjectDataSourceMethodEventArgs e) 
    {
        int locationTypeId = 1;
        int.TryParse(((DropDownList)DetailsViewInsertLocation.FindControl("DropDownListLocationType")).SelectedValue.ToString(),out locationTypeId);
        if (locationTypeId == 0)
            locationTypeId = 1;

        string location = ((TextBox)DetailsViewInsertLocation.FindControl("TextboxLocation")).Text;
        string ailse = ((TextBox)DetailsViewInsertLocation.FindControl("TextboxAilse")).Text;
        string column = ((TextBox)DetailsViewInsertLocation.FindControl("TextboxColumn")).Text;
        string level = ((TextBox)DetailsViewInsertLocation.FindControl("TextboxLevel")).Text;
        
        int palletQuantity = -1;
        int.TryParse(((TextBox)DetailsViewInsertLocation.FindControl("TextboxPalletQuantity")).Text, out palletQuantity);

        int securityCode = 99;
        int.TryParse(((TextBox)DetailsViewInsertLocation.FindControl("TextboxSecurityCode")).Text, out securityCode);

        decimal relativeValue = 0;
        decimal.TryParse(((TextBox)DetailsViewInsertLocation.FindControl("TextboxRelativeValue")).Text, out relativeValue);

        int numberofSub = -1;
        int.TryParse(((TextBox)DetailsViewInsertLocation.FindControl("TextboxNumberOfSUB")).Text, out numberofSub);

        string stockTakeInd = ((CheckBox)DetailsViewInsertLocation.FindControl("CheckBoxStocktakeInd")).Checked.ToString();
        string activeBinning = ((CheckBox)DetailsViewInsertLocation.FindControl("CheckBoxActiveBinning")).Checked.ToString();
        string activePicking = ((CheckBox)DetailsViewInsertLocation.FindControl("CheckBoxActivePicking")).Checked.ToString();
        string used = ((CheckBox)DetailsViewInsertLocation.FindControl("CheckBoxUsed")).Checked.ToString();
        
        int locationId = int.Parse("0");
        int areaId = int.Parse(Session["AreaId"].ToString());

        e.InputParameters["locationTypeId"] = locationTypeId;
        e.InputParameters["location"] = location;
        e.InputParameters["ailse"] = ailse;
        e.InputParameters["column"] = column;
        e.InputParameters["level"] = level;
        e.InputParameters["palletQuantity"] = palletQuantity;
        e.InputParameters["securityCode"] = securityCode;
        e.InputParameters["relativeValue"] = relativeValue;
        e.InputParameters["numberofSub"] = numberofSub;
        e.InputParameters["stockTakeInd"] = stockTakeInd;
        e.InputParameters["activeBinning"] = activeBinning;
        e.InputParameters["activePicking"] = activePicking;
        e.InputParameters["used"] = used;
        e.InputParameters["locationId"] = locationId;
        e.InputParameters["areaId"] = areaId;

    }

    protected void ObjectDataSourceLocation_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        ObjectDataSourceLocation.DataBind();
        DetailsViewInsertLocation.Visible = false;
        GridViewLocation.Visible = true;

    }
}
