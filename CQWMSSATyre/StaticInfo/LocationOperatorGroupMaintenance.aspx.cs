﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

public partial class StaticInfo_LocationOperatorGroupMaintenance : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewOperatorGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOperatorGroupGroupLoc_SelectedIndexChanged";
        try
        {
            Session["OperatorGroupId"] = GridViewOperatorGroup.SelectedDataKey["OperatorGroupId"];

        }
        catch (Exception ex)
        {
            result = SendErrorNow("LocationOperatorGroupMaintenance" + "_" + ex.Message.ToString());
        }
    }


    protected void ObjectDataSourceOperatorGroup_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsViewInsertLocationOperatorGroup.Visible = false;
        GridViewOperatorGroup.Visible = true;

        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_OperatorGroup, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }

    protected void ObjectDataSourceOperatorGroup_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        string OperatorGroupCode = ((TextBox)DetailsViewInsertLocationOperatorGroup.FindControl("DetailTextOperatorGroupCode")).Text;
        string RestrictedAreaIndicator = ((TextBox)DetailsViewInsertLocationOperatorGroup.FindControl("DetailTextRestrictedAreaIndicator")).Text;

        e.InputParameters["OperatorGroupCode"] = OperatorGroupCode;
        e.InputParameters["RestrictedAreaIndicator"] = RestrictedAreaIndicator;

    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertLocationOperatorGroup.Visible = false;
        GridViewOperatorGroup.Visible = true;
    }

    protected void DetailsViewInsertLocationOperatorGroup_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    #region ButtonSelectLocation_Click
    protected void ButtonSelectLocation_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectLocation_Click";
        try
        {
            foreach (ListItem item in CheckBoxListLocation.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectLocation_Click

    #region ButtonSelectLocationUnsel_Click
    protected void ButtonSelectLocationUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectLocationUnsel_Click";
        try
        {
            foreach (ListItem item in CheckBoxListLocationUnsel.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectLocationUnsel_Click

    #region ButtonDeselectLocation_Click
    protected void ButtonDeselectLocation_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectLocation_Click";

        try
        {
            foreach (ListItem item in CheckBoxListLocation.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectLocationUnsel_Click

    #region ButtonDeselectLocationUnsel_Click
    protected void ButtonDeselectLocationUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectLocationUnsel_Click";

        try
        {
            foreach (ListItem item in CheckBoxListLocationUnsel.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectLocationUnsel_Click

    #region ButtonLinkLocation_Click

    protected void ButtonLinkLocation_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectLocation_Click";
        try
        {
            List<int> locationList = new List<int>();

            foreach (ListItem item in CheckBoxListLocationUnsel.Items)
            {
                if (item.Selected == true)
                {
                    locationList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int location in locationList)
            {
                Location.LocationOperatorGroup_Add(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["OperatorGroupId"]), location);
            }

            CheckBoxListLocationUnsel.DataBind();
            CheckBoxListLocation.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LocationOperatorGroup" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkLocation_Click

    #region ButtonLinkLocationUnsel_Click

    protected void ButtonLinkLocationUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectLocation_Click";
        try
        {
            List<int> locationList = new List<int>();

            foreach (ListItem item in CheckBoxListLocation.Items)
            {
                if (item.Selected == true)
                {
                    locationList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int location in locationList)
            {
                Location.LocationOperatorGroup_Delete(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["OperatorGroupId"]), location);
            }

            CheckBoxListLocationUnsel.DataBind();
            CheckBoxListLocation.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LocationOperatorGroup" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkLocationUnsel_Click

    #region ButtonDisplayLocation_Click

    protected void ButtonDisplayLocation_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDisplayLocation_Click";

        try
        {
            Session["fromAisle"] = DropDownListFromAisle.SelectedValue;
            Session["toAisle"] = DropDownListToAisle.SelectedValue;
            Session["fromLevel"] = DropDownListFromLevel.SelectedValue;
            Session["toLevel"] = DropDownListToLevel.SelectedValue;
            //Response.Redirect("StockTakeGroupLocation.aspx");

            Master.MsgText = "Next"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateAisle" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDisplayLocation_Click

}
