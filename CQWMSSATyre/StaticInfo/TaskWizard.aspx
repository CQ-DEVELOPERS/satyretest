<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="TaskWizard.aspx.cs" Inherits="StaticInfo_TaskWizard"
    Title="<%$ Resources:Default, PrincipalWizardTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="~/UserControls/RequestTypeGridView.ascx" TagPrefix="uc1" TagName="RequestTypeGridView" %>
<%@ Register Src="~/UserControls/BillableGridView.ascx" TagPrefix="uc1" TagName="BillableGridView" %>
<%@ Register Src="~/UserControls/CycleGridView.ascx" TagPrefix="uc1" TagName="CycleGridView" %>
<%@ Register Src="~/UserControls/TaskListGridView.ascx" TagPrefix="uc1" TagName="TaskListGridView" %>
<%@ Register Src="~/UserControls/ProjectGridView.ascx" TagPrefix="uc1" TagName="ProjectGridView" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PrincipalWizardTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PrincipalWizardAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ProjectGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ProjectGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="TaskListGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="TaskListGridView" />
                    <telerik:AjaxUpdatedControl ControlID="RequestType" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RequestTypeGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RequestTypeGridView" />
                    <telerik:AjaxUpdatedControl ControlID="BillableGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="BillableGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="BillableGridView" />
                    <telerik:AjaxUpdatedControl ControlID="CycleGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabSContainer1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <%--<telerik:RadTab Text="<%$ Resources:Default, Principal %>"></telerik:RadTab>--%>
                <telerik:RadTab Text="Project"></telerik:RadTab>
                <telerik:RadTab Text="TaskList"></telerik:RadTab>
                <telerik:RadTab Text="RequestType"></telerik:RadTab>
                <telerik:RadTab Text="Billable"></telerik:RadTab>
                <telerik:RadTab Text="Cycle"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabsContainer and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
                
            <telerik:RadPageView runat="Server" ID="RadPageView1">
                <uc1:ProjectGridView runat="server" ID="ProjectGridView" />
            </telerik:RadPageView>
                
            <telerik:RadPageView runat="Server" ID="RadPageView2">
                <uc1:TaskListGridView runat="server" ID="TaskListGridView" />
            </telerik:RadPageView>

            <telerik:RadPageView runat="server" ID="RadPageView3">
                <uc1:RequestTypeGridView runat="server" ID="RequestTypeGridView" />
            </telerik:RadPageView>

            <telerik:RadPageView runat="server" ID="RadPageView4">
                <uc1:BillableGridView runat="server" ID="BillableGridView" />
            </telerik:RadPageView>
                
            <telerik:RadPageView runat="Server" ID="RadPageView5">
                <uc1:CycleGridView runat="server" ID="CycleGridView" />
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Content>
