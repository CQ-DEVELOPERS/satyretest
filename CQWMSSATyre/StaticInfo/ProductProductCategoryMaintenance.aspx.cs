﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

public partial class StaticInfo_ProductProductCategoryMaintenance : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    protected void GridViewProductCategories_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewProductCategoriesGroupLoc_SelectedIndexChanged";
        try
        {
            Session["ProductCategoryId"] = GridViewProductCategories.SelectedDataKey["ProductCategoryId"];

        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductOperatorGroupMaintenance" + "_" + ex.Message.ToString());
        }
    }


    protected void ObjectDataSourceProductCategories_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DetailsViewInsertProductProductCategories.Visible = false;
        GridViewProductCategories.Visible = true;

        string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
        ScriptManager.RegisterClientScriptBlock(updatePanel_ProductCategories, typeof(UpdatePanel), "changeactivetab", sScript, true);
    }

    protected void ObjectDataSourceProductCategories_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        string ProductCategoryType = ((TextBox)DetailsViewInsertProductProductCategories.FindControl("DetailTextProductCategoryType")).Text;
        string PackingCategory = ((TextBox)DetailsViewInsertProductProductCategories.FindControl("DetailTextPackingCategory")).Text;

        e.InputParameters["ProductCategoryType"] = ProductCategoryType;
        e.InputParameters["PackingCategory"] = PackingCategory;

    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertProductProductCategories.Visible = false;
        GridViewProductCategories.Visible = true;
    }

    protected void DetailsViewInsertProductProductCategories_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    #region ButtonSelectProduct_Click
    protected void ButtonSelectProduct_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectProduct_Click";
        try
        {
            foreach (ListItem item in CheckBoxListProduct.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectProduct_Click

    #region ButtonSelectProductUnsel_Click
    protected void ButtonSelectProductUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectProductUnsel_Click";
        try
        {
            foreach (ListItem item in CheckBoxListProductUnsel.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelectProductUnsel_Click

    #region ButtonDeselectProduct_Click
    protected void ButtonDeselectProduct_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectProduct_Click";

        try
        {
            foreach (ListItem item in CheckBoxListProduct.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectProductUnsel_Click

    #region ButtonDeselectProductUnsel_Click
    protected void ButtonDeselectProductUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectProductUnsel_Click";

        try
        {
            foreach (ListItem item in CheckBoxListProductUnsel.Items)
            {
                item.Selected = false;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeCreateLocation" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonDeselectProductUnsel_Click

    #region ButtonLinkProduct_Click

    protected void ButtonLinkProduct_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectProduct_Click";
        try
        {
            List<int> ProductList = new List<int>();

            foreach (ListItem item in CheckBoxListProductUnsel.Items)
            {
                if (item.Selected == true)
                {
                    ProductList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int product in ProductList)
            {
                Product.ProductProductCategory_Add(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["ProductCategoryId"]), product);
            }

            CheckBoxListProductUnsel.DataBind();
            CheckBoxListProduct.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductOperatorGroup" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkProduct_Click

    #region ButtonLinkProductUnsel_Click

    protected void ButtonLinkProductUnsel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectProduct_Click";
        try
        {
            List<int> ProductList = new List<int>();

            foreach (ListItem item in CheckBoxListProduct.Items)
            {
                if (item.Selected == true)
                {
                    ProductList.Add(Convert.ToInt32(item.Value));
                }
            }


            foreach (int product in ProductList)
            {
                Product.ProductProductCategory_Delete(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["ProductCategoryId"]), product);
            }

            CheckBoxListProductUnsel.DataBind();
            CheckBoxListProduct.DataBind();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductOperatorGroup" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLinkProductUnsel_Click
}
