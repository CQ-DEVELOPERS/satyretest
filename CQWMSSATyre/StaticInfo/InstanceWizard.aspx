<%@ Page Language="C#" MasterPageFile="~/MasterPages/Blank.master" AutoEventWireup="true"
    CodeFile="InstanceWizard.aspx.cs" Inherits="StaticInfo_InstanceWizard"
    Title="<%$ Resources:Default, InstanceWizardTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ MasterType VirtualPath="~/MasterPages/Blank.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, InstanceWizardTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, InstanceWizardAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="PrincipalGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PrincipalGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="ContactListGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ContactListGridView" />
                    <telerik:AjaxUpdatedControl ControlID="AddressGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <%--<telerik:AjaxSetting AjaxControlID="rbOption1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rbOption1" />
                    <telerik:AjaxUpdatedControl ControlID="rbOption2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="rbOption2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rbOption1" />
                    <telerik:AjaxUpdatedControl ControlID="rbOption2" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <asp:Wizard ID="Wizard1" runat="server" Width="100%" Height="650px" OnNextButtonClick="Wizard1_NextButtonClick" OnFinishButtonClick="Wizard1_FinishButtonClick">
        <WizardSteps>
            <asp:WizardStep ID="WizardStep1" runat="server" Title="Welcome" StepType="Start">
                <asp:Label ID="LabelInstanceWizardStartTitle" runat="server" Text="<%$ Resources:Default, InstanceWizardStartTitle %>" Font-Bold="true" Font-Size="Large"></asp:Label>
                <hr />
                <asp:Label ID="LabelInstanceWizardStartMessage" runat="server" Text="<%$ Resources:Default, InstanceWizardStartMessage %>"></asp:Label>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStep2" runat="server" Title="Register" StepType="Step">
                <table>
                    <tr>
                        <th colspan="2">
                            <asp:Label ID="LabelRegisterEvaluation" runat="server" Text="<%$ Resources:Default, RegisterEvaluation %>" Font-Bold="true" Font-Size="Large"></asp:Label>
                            <hr />
                        </th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <asp:Label ID="LabelAccountInformation" runat="server" Text="<%$ Resources:Default, AccountInformation %>" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rbtEmail" runat="server" Label="<%$ Resources:Default, Email %>" EmptyMessage="email@example.com" Width="250px"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfv1" runat="server" Display="Dynamic" ControlToValidate="rbtEmail" ErrorMessage="The textbox can not be empty!" >
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtbFirstName" runat="server" Label="<%$ Resources:Default, FirstName %>" EmptyMessage="" Width="250px"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfv2" runat="server" Display="Dynamic" ControlToValidate="rtbFirstName" ErrorMessage="The textbox can not be empty!"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <telerik:RadTextBox ID="rtbLastName" runat="server" Label="<%$ Resources:Default, LastName %>" EmptyMessage="" Width="250px"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfv3" runat="server" Display="Dynamic" ControlToValidate="rtbLastName" ErrorMessage="The textbox can not be empty!"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <telerik:RadTextBox ID="rtbCompany" runat="server" Label="<%$ Resources:Default, Company %>" EmptyMessage="Company Name" Width="250px"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfv4" runat="server" Display="Dynamic" ControlToValidate="rtbCompany" ErrorMessage="The textbox can not be empty!"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <telerik:RadTextBox ID="rtbTelephone" runat="server" Label="<%$ Resources:Default, Telephone %>" EmptyMessage="011 123 4567" Width="250px"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfv5" runat="server" Display="Dynamic" ControlToValidate="rtbTelephone" ErrorMessage="The textbox can not be empty!"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <asp:Label ID="LabelApplicationPreferences" runat="server" Text="<%$ Resources:Default, ApplicationPreferences %>" Font-Bold="true"></asp:Label>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="LabelApplicationPreferencesNotes" runat="server" Text="<%$ Resources:Default, ApplicationPreferencesNotes %>"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <asp:Label ID="LabelClientURL" runat="server" Text="<%$ Resources:Default, ClientURL %>"></asp:Label>
                            <telerik:RadTextBox ID="rtbClientURL" runat="server" EmptyMessage="*" Width="250px"></telerik:RadTextBox>
                            <telerik:RadButton ID="rbCheckAvailability" runat="server" Text="<%$ Resources:Default, CheckAvailability %>" OnClick="rbCheckAvailability_Click"></telerik:RadButton>
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStep3" runat="server" Title="Engagment Type" StepType="Step">
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, InstanceWizardChooseTitle %>" Font-Bold="true" Font-Size="Large"></asp:Label>
                <hr />
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Default, InstanceWizardChooseMessage %>"></asp:Label>
                <br />
                <br />
                <asp:RadioButtonList ID="rblVersion" runat="server">
                    <asp:ListItem Selected="True" Text="30 Day Trial" Value="Trial"></asp:ListItem>
                    <asp:ListItem Text="Buy" Value="Buy"></asp:ListItem>
                </asp:RadioButtonList>
                <br />
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStep4" runat="server" Title="Finished" StepType="Finish">
                <asp:Label ID="LabelInstanceWizardEndTitle" runat="server" Text="<%$ Resources:Default, InstanceWizardEndTitle %>" Font-Bold="true" Font-Size="Large"></asp:Label>
                <hr />
                <asp:Label ID="LabelInstanceWizardEndMessage" runat="server" Text="<%$ Resources:Default, InstanceWizardEndMessage %>"></asp:Label>
            </asp:WizardStep>
        </WizardSteps>
    </asp:Wizard>
</asp:Content>
