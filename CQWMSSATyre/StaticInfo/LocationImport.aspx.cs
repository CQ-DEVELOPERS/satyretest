using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
//using Microsoft.SqlServer.Dts.Runtime;




public partial class StaticInfo_LocationImport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //ScriptManager.GetCurrent(this).RegisterPostBackControl(FileUpload1);
    }

    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
            /*
                code here to truncate table
            */
        LocationImport.LocationImport_Truncate(Session["ConnectionStringName"].ToString());
        System.Threading.Thread.Sleep(2000);

        if (FileUpload1.HasFile)
        {
            
            //FileUpload1.SaveAs(@"\\Cqserver0001\Import\" + FileUpload1.FileName);
            FileUpload1.SaveAs(@"\\KAREN\Users\Import\" + FileUpload1.FileName);
            System.Threading.Thread.Sleep(2000);

        }
        //
        // Load package from SQL Server
        //

        //Package package2 = app.LoadFromSqlServer(
        //    "ExamplePackage", "server_name", "sa", "your_password", null);

        //DTSExecResult result2 = package2.Execute();

        //    GridView1.DataBind();
        
    }

    protected void Button_AddLocation_Click(object sender, EventArgs e)
    {
        bool result =  LocationImport.LocationImport_CheckError(Session["ConnectionStringName"].ToString());
            /*
            *  Have a check here for errors.
            *  if HasError ----- (do not truncate table)
            */

        if (result) 
        {
            System.Windows.Forms.MessageBox.Show("Data has errors");
        }
        else
        {
            LocationImport.LocationImport_Add(Session["ConnectionStringName"].ToString());
            System.Threading.Thread.Sleep(5000);
            LocationImport.LocationImport_Truncate(Session["ConnectionStringName"].ToString());
            System.Threading.Thread.Sleep(2000);
        }

        GridView1.DataBind();

    }
}
