<%@ Page Language="C#" MasterPageFile="~/MasterPages/Empty.master" AutoEventWireup="true" CodeFile="PalletWeight.aspx.cs" Inherits="StaticInfo_PalletWeight" 
    Title="Pallet Weight"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/PalletWeight.ascx" TagName="PalletWeight" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/Empty.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></asp:ScriptManager>--%>
    <ajaxToolkit:ToolkitScriptManager runat="Server" EnablePartialRendering="true" ID="ScriptManager1" />
    <uc1:PalletWeight ID="PalletWeight1" runat="server" />
</asp:Content>