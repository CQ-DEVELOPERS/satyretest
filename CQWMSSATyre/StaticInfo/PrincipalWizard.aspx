<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PrincipalWizard.aspx.cs" Inherits="StaticInfo_PrincipalWizard"
    Title="<%$ Resources:Default, PrincipalWizardTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc2" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/PrincipalGridView.ascx" TagPrefix="uc1" TagName="PrincipalGridView" %>
<%@ Register Src="~/UserControls/ContactListGridView.ascx" TagPrefix="uc1" TagName="ContactListGridView" %>
<%@ Register Src="~/UserControls/AddressGridView.ascx" TagPrefix="uc1" TagName="AddressGridView" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PrincipalWizardTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PrincipalWizardAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="PrincipalGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PrincipalGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="ContactListGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ContactListGridView" />
                    <telerik:AjaxUpdatedControl ControlID="AddressGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="AddressGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AddressGridView" />
                    <telerik:AjaxUpdatedControl ControlID="ContactListGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabSContainer1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Principal %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, ContactList %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Address %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabsContainer and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="RadPageView1">
                <uc1:PrincipalGridView runat="server" ID="PrincipalGridView" />
                
                <asp:Label ID="LabelArea" runat="server" Text="<%$ Resources:Default, Area %>" Font-Bold="true"></asp:Label>
                <br />
                <telerik:RadButton ID="RadButtonArea" runat="server" Text="<%$ Resources:Default, ButtonViewMaintain %>" OnClick="RadButtonArea_Click" Width="125px"></telerik:RadButton>
                <br />
                <telerik:RadButton ID="RadButtonLocationMaster" runat="server" Text="<%$ Resources:Default, Upload %>" OnClick="RadButtonLocationMaster_Click" Width="125px"></telerik:RadButton>
                <br />
                <br />
                <asp:Label ID="LabelProducts" runat="server" Text="<%$ Resources:Default, Products %>" Font-Bold="true"></asp:Label>
                <br />
                <telerik:RadButton ID="RadButtonStockOnHand" runat="server" Text="<%$ Resources:Default, ButtonView %>" OnClick="RadButtonStockOnHand_Click" Width="125px"></telerik:RadButton>
                <br />
                <telerik:RadButton ID="RadButtonProducts" runat="server" Text="<%$ Resources:Default, ButtonMaintain %>" OnClick="RadButtonProducts_Click" Width="125px"></telerik:RadButton>
                <br />
                <telerik:RadButton ID="RadButtonProductMaster" runat="server" Text="<%$ Resources:Default, Upload %>" OnClick="RadButtonProductMaster_Click" Width="125px"></telerik:RadButton>
                <br />
                <br />
                <asp:Label ID="LabelEDI" runat="server" Text="<%$ Resources:Default, EDI %>" Font-Bold="true"></asp:Label>
                <br />
                <telerik:RadButton ID="RadButtonInegration" runat="server" Text="<%$ Resources:Default, ButtonViewMaintain %>" OnClick="RadButtonInegration_Click" Width="125px"></telerik:RadButton>
                <br />
                <telerik:RadButton ID="RadButtonInterface" runat="server" Text="<%$ Resources:Default, ButtonViewMaintain %>" OnClick="RadButtonInterface_Click" Width="125px"></telerik:RadButton>
                <br />
                <br />
                <asp:Label ID="LabelBillingCategories" runat="server" Text="<%$ Resources:Default, BillingCategories %>" Font-Bold="true" Visible="false"></asp:Label>
                <br />
                <telerik:RadButton ID="RadButtonBilling" runat="server" Text="<%$ Resources:Default, ButtonViewMaintain %>" OnClick="RadButtonBilling_Click" Width="125px" Visible="false"></telerik:RadButton>
            </telerik:RadPageView>

            <telerik:RadPageView runat="server" ID="RadPageView2">
                <uc1:ContactListGridView runat="server" ID="ContactListGridView" />
            </telerik:RadPageView>
                
            <telerik:RadPageView runat="Server" ID="RadPageView3">
                <uc1:AddressGridView runat="server" ID="AddressGridView" />
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Content>
