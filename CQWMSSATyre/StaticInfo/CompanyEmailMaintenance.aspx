﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="CompanyEmailMaintenance.aspx.cs" Inherits="StaticInfo_CompanyEmailMaintenance"
    Title="<%$ Resources:Default, EmailMaintenance %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, CompanyEmailMaintenance %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, HousekeepingModule%>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabCompany" HeaderText="<%$ Resources:Default, ExternalCompany%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="updatePanel_Company" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="PanelSearch" runat="server" Width="450px" BackColor="#EFEFEC" DefaultButton="ButtonSearch">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelExternalCompanyCode" runat="server" Text='<%$ Resources:Default,ExternalCompanyCode %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxExternalCompanyCode" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelExternalCompany" runat="server" Text='<%$ Resources:Default,ExternalCompany %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxExternalCompany" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        <asp:Button ID="ButtonSearch" runat="server" Text='<%$ Resources:Default,Search %>'
                                            OnClick="ButtonSearch_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                            TargetControlID="PanelSearch" Radius="10" Color="239, 239, 236" BorderColor="64, 64, 64"
                            Enabled="True" />
                        <asp:GridView ID="GridViewExternalCompany" DataSourceID="ObjectDataSourceCompany"
                            DataKeyNames="ExternalCompanyId" runat="server" AllowPaging="True" AllowSorting="True"
                            PageSize="15" AutoGenerateColumns="False" EmptyDataText="<%$ Resources:Default, CurrentlyNoExternalCompanys%>"
                            OnSelectedIndexChanged="GridViewExternalCompany_SelectedIndexChanged">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ShowEditButton="False" ShowDeleteButton="False" />
                                <asp:BoundField HeaderText="<%$ Resources:Default, ExternalCompany%>" DataField="ExternalCompany"
                                    SortExpression="ExternalCompany" />
                                <asp:BoundField HeaderText="<%$ Resources:Default, ExternalCompanyCode%>" DataField="ExternalCompanyCode"
                                    SortExpression="ExternalCompanyCode" />
                            </Columns>
                        </asp:GridView>
                        <asp:DetailsView Visible="false" ID="DetailsViewInsertExternalCompany" runat="server"
                            AutoGenerateRows="False" DataSourceID="ObjectDataSourceCompany" DefaultMode="Insert">
                            <Fields>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, ExternalCompanyId%>" InsertVisible="False"
                                    SortExpression="ExternalCompanyId">
                                    <InsertItemTemplate>
                                        <asp:Button ID="AddExternalCompany" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Add%>" />
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, ExternalCompany%>">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextExternalCompany" runat="server" Text='<%# Bind("ExternalCompany") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                    <FooterStyle Wrap="False" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, ExternalCompanyCode %>">
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="DetailTextExternalCompanyCode" runat="server" Text='<%# Bind("ExternalCompanyCode") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <InsertItemTemplate>
                                        <asp:LinkButton ID="btnInsert" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Insert%>"
                                            OnClick="btnInsert_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                            Text="<%$ Resources:Default, Cancel%>" OnClick="btnCancel_Click"></asp:LinkButton>
                                    </InsertItemTemplate>
                                </asp:TemplateField>
                            </Fields>
                        </asp:DetailsView>
                        <asp:ObjectDataSource ID="ObjectDataSourceCompany" runat="server" TypeName="StaticInfoExternalCompany"
                            SelectMethod="ExternalCompanySearch" UpdateMethod="ExternalCompany_Update">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:ControlParameter ControlID="TextBoxExternalCompany" PropertyName="Text" Name="ExternalCompany"
                                    Type="String" />
                                <asp:ControlParameter ControlID="TextBoxExternalCompanyCode" PropertyName="Text"
                                    Name="ExternalCompanyCode" Type="String" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:Parameter Name="ExternalCompany" Type="string" />
                                <asp:Parameter Name="ExternalCompanyCode" Type="string" />
                                <asp:Parameter Name="ExternalCompanyId" Type="Int32" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabContact" HeaderText="<%$ Resources:Default, Contact%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="updatePanel_Contact" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="PanelContact" runat="server" Width="450px" Visible="false">
                            <asp:Button ID="ButtonContact" runat="server" Text="<%$ Resources:Default, AddContact%>"
                                OnClick="ButtonAddContact_Click" />
                            <asp:GridView ID="GridViewContact" DataSourceID="ObjectDataSourceContact" DataKeyNames="ContactListId"
                                runat="server" AllowPaging="True" AllowSorting="True" PageSize="15" AutoGenerateColumns="False"
                                EmptyDataText="<%$ Resources:Default, NoContactsForCustomer%>">
                                <Columns>
                                    <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Contact%>" DataField="ContactPerson"
                                        SortExpression="ContactPerson" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, ReportType%>" DataField="ReportType"
                                        SortExpression="ReportType" ReadOnly="True" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ReportType%>" SortExpression="ReportType">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="DropDownListReportType" runat="server" DataSourceID="ObjectdatsourceReportType"
                                                DataTextField="ReportType" DataValueField="ReportTypeId" SelectedValue='<%# Bind("ReportTypeId") %>'>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="TextBoxReportType" runat="server" Text='<%# Bind("ReportTypeId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Telephone%>" DataField="Telephone"
                                        SortExpression="Telephone" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, Fax%>" DataField="Fax" SortExpression="Fax" />
                                    <asp:BoundField HeaderText="<%$ Resources:Default, EMail%>" DataField="EMail" SortExpression="EMail" />
                                </Columns>
                            </asp:GridView>
                            <asp:DetailsView Visible="false" ID="DetailsViewInsertContact" runat="server" AutoGenerateRows="False"
                                DataSourceID="ObjectDataSourceContact" DefaultMode="Insert">
                                <Fields>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Contact%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxContactPerson" runat="server" Text='<%# Bind("ContactPerson") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, ReportType%>">
                                        <InsertItemTemplate>
                                            <asp:DropDownList ID="DropDownListReportType" runat="server" DataSourceID="ObjectdatsourceReportType"
                                                DataTextField="ReportType" DataValueField="ReportTypeId" SelectedValue='<%# Bind("ReportTypeId") %>'>
                                            </asp:DropDownList>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Telephone%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxTelephone" runat="server" Text='<%# Bind("Telephone") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, Fax%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxFax" runat="server" Text='<%# Bind("Fax") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Default, EMail%>">
                                        <InsertItemTemplate>
                                            <asp:TextBox ID="TextboxEMail" runat="server" Text='<%# Bind("EMail") %>'></asp:TextBox>
                                        </InsertItemTemplate>
                                        <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <InsertItemTemplate>
                                            <asp:LinkButton ID="btnlocInsert" runat="server" CommandName="Insert" Text="<%$ Resources:Default, Insert%>"
                                                OnClick="btnlocInsert_Click"></asp:LinkButton>
                                            <asp:LinkButton ID="btnlocCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                                Text="<%$ Resources:Default, Cancel%>" OnClick="btnlocCancel_Click"></asp:LinkButton>
                                        </InsertItemTemplate>
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>
                        </asp:Panel>
                        <asp:ObjectDataSource ID="ObjectDataSourceContact" runat="server" TypeName="Contact"
                            SelectMethod="Contact_Search_By_ExternalCompanyId" UpdateMethod="Contact_Edit"
                            DeleteMethod="Contact_Delete" InsertMethod="Contact_Add" OnInserting="ObjectDataSourceContact_Inserting"
                            OnInserted="ObjectDataSourceContact_Inserted">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="ExternalCompanyId" SessionField="ExternalCompanyId" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="ContactListId" Type="Int32" />
                                <asp:Parameter Name="ContactPerson" Type="string" />
                                <asp:Parameter Name="Telephone" Type="string" />
                                <asp:Parameter Name="Fax" Type="string" />
                                <asp:Parameter Name="EMail" Type="string" />
                                <asp:Parameter Name="ReportTypeId" Type="Int32" />
                            </UpdateParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="ContactListId" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="ContactListId" Type="Int32" />
                                <asp:Parameter Name="ExternalCompanyId" Type="Int32" />
                                <asp:Parameter Name="ContactPerson" Type="string" />
                                <asp:Parameter Name="Telephone" Type="string" />
                                <asp:Parameter Name="Fax" Type="string" />
                                <asp:Parameter Name="EMail" Type="string" />
                                <asp:Parameter Name="ReportTypeId" Type="Int32" />
                            </InsertParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectdatsourceReportType" runat="server" TypeName="StaticInfo"
                            SelectMethod="GetReportType">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
