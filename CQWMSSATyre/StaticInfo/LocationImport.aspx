<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="LocationImport.aspx.cs" Inherits="StaticInfo_LocationImport" Title="<%$ Resources:Default, LocationMaintenance %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, LocationMaintenance %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
        ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40"
        TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPanePeriod" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a file to Upload</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPeriod" runat="server">
                        <ContentTemplate>
                            <asp:FileUpload ID="FileUpload1" runat="server" Width="500" />
                            <asp:Button ID="ButtonUpload" runat="server" Text="Save" OnClick="ButtonUpload_Click" />
                            <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server" TargetControlID="ButtonUpload"
                                ConfirmText="Please note that all previous data upload data will be truncated." />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ButtonUpload" />
                        </Triggers>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridView1" runat="server" DataKeyNames="LocationImportId" DataSourceID="ObjectdtasourceLocationImport"
                AutoGenerateColumns="False">
                <Columns>
                    <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" />
                    <asp:BoundField DataField="WarehouseCode" HeaderText="Warehouse Code" />
                    <asp:BoundField DataField="Area" HeaderText="Area" />
                    <asp:BoundField DataField="AreaCode" HeaderText="AreaC ode" />
                    <asp:BoundField DataField="Location" HeaderText="Location" />
                    <asp:BoundField DataField="Ailse" HeaderText="Ailse" />
                    <asp:BoundField DataField="Column" HeaderText="Column" />
                    <asp:BoundField DataField="Level" HeaderText="Level" />
                    <asp:BoundField DataField="PalletQuantity" HeaderText="Pallet Quantity" />
                    <asp:BoundField DataField="SecurityCode" HeaderText="Security Code" />
                    <asp:BoundField DataField="RelativeValue" HeaderText="RelativeV alue" />
                    <asp:BoundField DataField="NumberOfSUB" HeaderText="Number Of SUB" />
                    <asp:BoundField DataField="Width" HeaderText="Width" />
                    <asp:BoundField DataField="Height" HeaderText="Height" />
                    <asp:BoundField DataField="Length" HeaderText="Length" />
                    <asp:TemplateField HeaderText="Error">
                        <ItemTemplate>
                            <asp:Label ID="LabelErrorFlag" runat="server" ToolTip='<%# "Error Messages :" + Environment.NewLine + Eval("ErrorLog").ToString().Replace(@"\\n", Environment.NewLine) %>'
                                Text='<%# Eval("HasError").ToString() == "True" ? Eval("HasError").ToString()  : "False" %>'
                                ForeColor="White" BackColor='<%# Eval("HasError").ToString() == "True" ? System.Drawing.Color.FromName("Red")  : System.Drawing.Color.FromName("Green") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Error Message">
                        <ItemTemplate>
                            <asp:Label ID="LabelErrorMessage" runat="server" Text='<%# "Error Messages :" +  Eval("ErrorLog").ToString().Replace(@"\\n", ",") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectdtasourceLocationImport" runat="server" TypeName="LocationImport"
                SelectMethod="LocationImport_Search" UpdateMethod="LocationImport_Update" DeleteMethod="LocationImport_Delete">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="warehouseCode" Type="String" />
                    <asp:Parameter Name="area" Type="String" />
                    <asp:Parameter Name="areaCode" Type="String" />
                    <asp:Parameter Name="location" Type="String" />
                    <asp:Parameter Name="ailse" Type="String" />
                    <asp:Parameter Name="column" Type="String" />
                    <asp:Parameter Name="level" Type="String" />
                    <asp:Parameter Name="palletQuantity" Type="Decimal" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="securityCode" Type="Int32" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="relativeValue" Type="Decimal" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="numberOfSUB" Type="Int32" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="width" Type="Decimal" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="height" Type="Decimal" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="length" Type="Decimal" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="locationImportId" Type="Int32" />
                </UpdateParameters>
                <DeleteParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="locationImportId" Type="Int32" />
                </DeleteParameters>
            </asp:ObjectDataSource>
            <asp:Button ID="Button_AddLocation" runat="server" Text="Add Locations" OnClick="Button_AddLocation_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
