<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="InterfaceWizard.aspx.cs" Inherits="StaticInfo_InterfaceWizard"
    Title="<%$ Resources:Default, InterfaceWizardTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc2" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="~/UserControls/InterfaceFileTypeGridView.ascx" TagPrefix="uc1" TagName="InterfaceFileTypeGridView" %>
<%@ Register Src="~/UserControls/InterfaceDocumentTypeGridView.ascx" TagPrefix="uc1" TagName="InterfaceDocumentTypeGridView" %>
<%@ Register Src="~/UserControls/InterfaceFieldGridView.ascx" TagPrefix="uc1" TagName="InterfaceFieldGridView" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, InterfaceWizardTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, InterfaceWizardAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="InterfaceFileTypeGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceFileTypeGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="InterfaceDocumentTypeGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceDocumentTypeGridView" />
                    <telerik:AjaxUpdatedControl ControlID="InterfaceFieldGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="InterfaceFieldGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceFieldGridView" />
                    <telerik:AjaxUpdatedControl ControlID="InterfaceDocumentTypeGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabSContainer1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs" Skin="Metro">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, FileTypes %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, DocumentTypes %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Fields %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabsContainer and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="RadPageView1">
                <uc1:InterfaceFileTypeGridView runat="server" ID="InterfaceFileTypeGridView" />
            </telerik:RadPageView>

            <telerik:RadPageView runat="server" ID="RadPageView2">
                <uc1:InterfaceDocumentTypeGridView runat="server" ID="InterfaceDocumentTypeGridView" />
            </telerik:RadPageView>
                
            <telerik:RadPageView runat="Server" ID="RadPageView3">
                <uc1:InterfaceFieldGridView runat="server" ID="InterfaceFieldGridView" />
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Content>
