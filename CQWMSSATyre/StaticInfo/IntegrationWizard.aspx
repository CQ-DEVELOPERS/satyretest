<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="IntegrationWizard.aspx.cs" Inherits="StaticInfo_IntegrationWizard"
    Title="<%$ Resources:Default, IntegrationWizardTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/InterfaceDocumentTypeGridView.ascx" TagPrefix="uc1" TagName="InterfaceDocumentTypeGridView" %>
<%@ Register Src="~/UserControls/InterfaceMappingFileGridView.ascx" TagPrefix="uc1" TagName="InterfaceMappingFileGridView" %>
<%@ Register Src="~/UserControls/PrincipalGridView.ascx" TagPrefix="uc1" TagName="PrincipalGridView" %>
<%@ Register Src="~/StaticInfo/IntergrationDelimiter.ascx" TagPrefix="uc1" TagName="IntergrationDelimiter" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, IntegrationWizardTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, IntegrationWizardAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="PrincipalGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PrincipalGridView" />
                    <telerik:AjaxUpdatedControl ControlID="InterfaceMappingFileGridView" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="InterfaceMappingFileGridView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceMappingFileGridView" />
                    <telerik:AjaxUpdatedControl ControlID="IntergrationDelimiter" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="IntergrationDelimiter">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="IntergrationDelimiter" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabSContainer1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs" Skin="Metro">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Principal %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Files %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Columns %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabsContainer and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="RadPageView1">
                <uc1:PrincipalGridView runat="server" ID="PrincipalGridView" />
            </telerik:RadPageView>
            <telerik:RadPageView runat="server" ID="RadPageView2">
                <uc1:InterfaceMappingFileGridView runat="server" ID="InterfaceMappingFileGridView" />
            </telerik:RadPageView>
                
            <telerik:RadPageView runat="Server" ID="RadPageView3">
                <br />
                <uc1:IntergrationDelimiter runat="server" ID="IntergrationDelimiter" />
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Content>
