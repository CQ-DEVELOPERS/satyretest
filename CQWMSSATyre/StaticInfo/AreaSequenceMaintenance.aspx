﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="AreaSequenceMaintenance.aspx.cs" Inherits="StaticInfo_AreaSequenceMaintenance"
    Title="<%$ Resources:Default, AreaOperatorGroupTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, AreaOperatorGroupTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Area Operator Group">
            <HeaderTemplate>
                Area Operator Group
            </HeaderTemplate>
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table border="solid" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="Area" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="Pick Area" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Store Area" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="updatePanel_AreaAll" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="" />
                                            <asp:GridView ID="GridViewAreaAll" DataSourceID="ObjectDataSourceAreaAll" DataKeyNames="AreaId"
                                                runat="server" AllowPaging="True" AllowSorting="True" PageSize="15" AutoGenerateColumns="False"
                                                EmptyDataText="There are currently no Areas..." OnSelectedIndexChanged="GridViewAreaAll_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True" ShowEditButton="False" ShowDeleteButton="False"
                                                        ShowCancelButton="False" />
                                                    <asp:BoundField HeaderText="AreaId" DataField="AreaId" SortExpression="AreaId" ReadOnly="True"
                                                        Visible="False" />
                                                    <asp:BoundField HeaderText="AreaCode" DataField="AreaCode" SortExpression="AreaCode"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="Area" DataField="Area" SortExpression="Area" />
                                                    <asp:BoundField HeaderText="RestrictedAreaIndicator" DataField="RestrictedAreaIndicator"
                                                        SortExpression="RestrictedAreaIndicator" Visible="False" />
                                                </Columns>
                                            </asp:GridView>
                                            <asp:DetailsView Visible="false" ID="DetailsViewInsertAreaAll" runat="server" AutoGenerateRows="False"
                                                DataSourceID="ObjectDataSourceAreaAll" OnLoad="DetailsViewInsertAreaAll_Load">
                                                <Fields>
                                                    <asp:TemplateField HeaderText="AreaId" InsertVisible="False" SortExpression="AreaId">
                                                        <InsertItemTemplate>
                                                            <asp:Button ID="AddArea" runat="server" CommandName="Insert" Text="Add" />
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AreaCode" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextAreaCode" runat="server" Text='<%# Bind("AreaCode") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="RestrictedAreaIndicator" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextRestrictedAreaIndicator" runat="server" Text='<%# Bind("RestrictedAreaIndicator") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                </Fields>
                                            </asp:DetailsView>
                                            <asp:ObjectDataSource ID="ObjectDataSourceAreaAll" runat="server" TypeName="Area"
                                                SelectMethod="GetAreasByWarehouse">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceAreaPick" runat="server" TypeName="Area"
                                                SelectMethod="GetAreasPick">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="AreaId" SessionField="AreaId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceAreaStore" runat="server" TypeName="Area"
                                                SelectMethod="GetAreasStore">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="AreaId" SessionField="AreaId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <%--<asp:Button ID="Button4" runat="server" Text=">>" OnClick="ButtonLinkAreaStore_Click" />--%>
                                    <br />
                                    <br />
                                    <asp:Button ID="Button3" runat="server" Text="Add Links" OnClick="ButtonLinkAreaPick_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanelPickArea" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListAreaPick" runat="server" DataSourceID="ObjectDataSourceAreaPick"
                                                DataTextField="Area" DataValueField="AreaId" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="LabelErrorMsg" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectPickArea" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectPickArea" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectPickArea" runat="server" Text="Select All" OnClick="ButtonSelectPickArea_Click" />
                                    <asp:Button ID="ButtonDeselectPickArea" runat="server" Text="Deselect All" OnClick="ButtonDeselectPickArea_Click" />
                                </td>
                                <%--<td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:Button ID="Button4" runat="server" Text=">>" OnClick="ButtonLinkAreaStore_Click" />
                                    <br />
                                    <br />
                                    <asp:Button ID="Button3" runat="server" Text="<<" OnClick="ButtonLinkAreaPick_Click" />
                                </td>--%>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanelStoreArea" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListAreaStore" runat="server" DataSourceID="ObjectDataSourceAreaStore"
                                                DataTextField="Area" DataValueField="AreaId" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="Label3" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectStoreArea" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectStoreArea" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectStoreArea" runat="server" Text="Select All" OnClick="ButtonSelectStoreArea_Click" />
                                    <asp:Button ID="ButtonDeselectStoreArea" runat="server" Text="Deselect All" OnClick="ButtonDeselectStoreArea_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <%--<asp:AsyncPostBackTrigger ControlID="Button4" EventName="Click" />--%>
                        <asp:AsyncPostBackTrigger ControlID="Button3" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
