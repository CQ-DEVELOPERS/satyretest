using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class StaticInfo_BatchMaintenance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
     
       
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        if (Session["Batch"] != null)
            {
                DetailsViewInsertBatch.Visible = true;
                LabelProductSelect.Visible = false;
                
                ObjectDataSourceBatch.DataBind();
                DetailsViewInsertBatch.DataBind();

                
            }

    }



    protected void ObjectDataSourceBatch_Updating(object sender, ObjectDataSourceMethodEventArgs e)
    {
        int statusId = int.Parse(((DropDownList)DetailsViewInsertBatch.FindControl("DropDownBatchStatus")).SelectedValue.ToString());

        if (((DropDownList)DetailsViewInsertBatch.FindControl("DropDownBatchNumber")).Visible)
        {
            ((TextBox)DetailsViewInsertBatch.FindControl("TextBoxBatchNumber")).Text = ((DropDownList)DetailsViewInsertBatch.FindControl("DropDownBatchNumber")).SelectedItem.Text;
        }

        string batch = (((TextBox)DetailsViewInsertBatch.FindControl("TextBoxBatchNumber")).Text);
        string batchReferenceNumber = (((TextBox)DetailsViewInsertBatch.FindControl("TextBoxBatchReference")).Text);
        int productionLine = int.Parse(((DropDownList)DetailsViewInsertBatch.FindControl("DropDownProductionLine")).SelectedValue.ToString());
        string productionDateTime = (((TextBox)DetailsViewInsertBatch.FindControl("TextBoxProdDate")).Text);
        string createDate = (((TextBox)DetailsViewInsertBatch.FindControl("TextBoxCreateDateTime")).Text);
        string expiryDate = (((TextBox)DetailsViewInsertBatch.FindControl("TextBoxExpiryDate")).Text);
        int incubationDays = 0;
        int shelfLifeDays = int.Parse(((TextBox)DetailsViewInsertBatch.FindControl("TextBoxShelfLife")).Text);
        int expectedYield = int.Parse(((TextBox)DetailsViewInsertBatch.FindControl("TextBoxPlannedYield")).Text);
        int actualYield = int.Parse(((TextBox)DetailsViewInsertBatch.FindControl("TextBoxActualYield")).Text);
        int filledYield = int.Parse(((TextBox)DetailsViewInsertBatch.FindControl("TextBoxActualFillingYield")).Text);
        string eCLNumber = "0";
        string cOACertificate = (((RadioButtonList)DetailsViewInsertBatch.FindControl("RadioCOACert")).SelectedValue.ToString());

        e.InputParameters["statusId"] = statusId;
        e.InputParameters["batch"] = batch;
        e.InputParameters["batchReferenceNumber"] = batchReferenceNumber;
        e.InputParameters["productionLine"] = productionLine;
        e.InputParameters["productionDateTime"] = productionDateTime;
        e.InputParameters["createDate"] = createDate;
        e.InputParameters["expiryDate"] = expiryDate;
        e.InputParameters["incubationDays"] = incubationDays;
        e.InputParameters["shelfLifeDays"] = shelfLifeDays;
        e.InputParameters["expectedYield"] = expectedYield;
        e.InputParameters["actualYield"] = actualYield;
        e.InputParameters["filledYield"] = filledYield;
        e.InputParameters["eCLNumber"] = eCLNumber;
        e.InputParameters["cOACertificate"] = cOACertificate;

    }

    
    
}
