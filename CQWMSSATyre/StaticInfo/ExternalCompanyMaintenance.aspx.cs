﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class StaticInfo_ExternalCompanyMaintenance : System.Web.UI.Page
{
    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    //protected void GridViewArea_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    theErrMethod = "GridViewArea_SelectedIndexChanged";
    //    try
    //    {
    //        Session["AreaId"] = GridViewArea.SelectedDataKey["AreaId"];

    //        ObjectDataSourceExternalCompany.DataBind();
    //        GridViewExternalCompany.SelectedIndex = -1;
    //        PanelLocation.Visible = true;

    //        Master.MsgText = ""; Master.ErrorText = "";
    //    }
    //    catch (Exception ex)
    //    {
    //        result = SendErrorNow("LocationMaintenance" + "_" + ex.Message.ToString());
    //        Master.ErrorText = result;
    //    }
    //}

    //protected void ButtonAddArea_Click(object sender, EventArgs e)
    //{
    //    DetailsViewInsertArea.Visible = true;
    //    GridViewArea.Visible = false;
    //}

    protected void ButtonAddExternalCompany_Click(object sender, EventArgs e)
    {
        DetailsViewInsertExternalCompany.Visible = true;
        GridViewExternalCompany.Visible = false;
    }

    //protected void ObjectDataSourceArea_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    //{
    //    DetailsViewInsertArea.Visible = false;
    //    GridViewArea.Visible = true;
    //    Session["AreaId"] = int.Parse(e.ReturnValue.ToString());
    //    PanelLocation.Visible = true;
    //    string sScript = "var tab = $find(\"" + Tabs.ClientID + "\").set_activeTabIndex(1);";
    //    ScriptManager.RegisterClientScriptBlock(updatePanel_Area, typeof(UpdatePanel), "changeactivetab", sScript, true);
    //}

    //protected void ObjectDataSourceArea_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    //{

    //    string area = ((TextBox)DetailsViewInsertArea.FindControl("DetailTextArea")).Text;
    //    string areaCode = ((TextBox)DetailsViewInsertArea.FindControl("DetailTextAreaCode")).Text;
    //    string stockOnHand = ((CheckBox)DetailsViewInsertArea.FindControl("CheckBoxStockOnHand")).Checked.ToString();
    //    int areaId = int.Parse("0");

    //    e.InputParameters["area"] = area;
    //    e.InputParameters["areaCode"] = areaCode;
    //    e.InputParameters["stockOnHand"] = stockOnHand;
    //    e.InputParameters["areaId"] = areaId;
    //}

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        //ObjectDataSourceArea.Insert();
    }

    //protected void btnCancel_Click(object sender, EventArgs e)
    //{
    //    DetailsViewInsertArea.Visible = false;
    //    GridViewArea.Visible = true;
    //}

    protected void btnlocInsert_Click(object sender, EventArgs e)
    {
        // ObjectDataSourceExternalCompany.Insert();
    }

    protected void btnlocCancel_Click(object sender, EventArgs e)
    {
        DetailsViewInsertExternalCompany.Visible = false;
        GridViewExternalCompany.Visible = true;
    }

    protected void ObjectDataSourceExternalCompany_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        int externalCompanyTypeId = int.Parse(((DropDownList)DetailsViewInsertExternalCompany.FindControl("DropDownListExternalCompanyType")).SelectedValue.ToString());
        int routeId = int.Parse(((TextBox)DetailsViewInsertExternalCompany.FindControl("TextboxRouteId")).Text);
        string externalCompany = ((TextBox)DetailsViewInsertExternalCompany.FindControl("TextboxExternalCompany")).Text;
        string externalCompanyCode = ((TextBox)DetailsViewInsertExternalCompany.FindControl("TextboxExternalCompanyCode")).Text;
        int rating = int.Parse(((TextBox)DetailsViewInsertExternalCompany.FindControl("TextboxRating")).Text);
        string redeliveryIndicator = ((TextBox)DetailsViewInsertExternalCompany.FindControl("TextboxRedeliveryIndicator")).Text;
        string qualityAssuranceIndicator = (((TextBox)DetailsViewInsertExternalCompany.FindControl("TextboxPalletQuantity")).Text);
        int externalCompanyId = int.Parse("0");


        e.InputParameters["externalCompanyTypeId"] = externalCompanyTypeId;
        e.InputParameters["routeId"] = routeId;
        e.InputParameters["externalCompany"] = externalCompany;
        e.InputParameters["externalCompanyCode"] = externalCompanyCode;
        e.InputParameters["rating"] = rating;
        e.InputParameters["redeliveryIndicator"] = redeliveryIndicator;
        e.InputParameters["qualityAssuranceIndicator"] = qualityAssuranceIndicator;
        e.InputParameters["externalCompanyId"] = externalCompanyId;
       
    }

    protected void ObjectDataSourceExternalCompany_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        ObjectDataSourceExternalCompany.DataBind();
        DetailsViewInsertExternalCompany.Visible = false;
        GridViewExternalCompany.Visible = true;

    }
}
