﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductProductCategoryMaintenance.aspx.cs" Inherits="StaticInfo_ProductProductCategoryMaintenance"
    Title="<%$ Resources:Default, ProductProductCategoryTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ProductProductCategoryTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Product - Product Category">
            <HeaderTemplate>
                Product - Product Category
            </HeaderTemplate>
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table border="solid" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="Product Categories" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="Linked Products" Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text=" " Font-Bold="True"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Unlinked Products" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="updatePanel_ProductCategories" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="" />
                                            <asp:GridView ID="GridViewProductCategories" DataSourceID="ObjectDataSourceProductCategories"
                                                DataKeyNames="ProductCategoryId" runat="server" AllowPaging="True" AllowSorting="True"
                                                PageSize="15" AutoGenerateColumns="False" EmptyDataText="There are currently no Product Categories..."
                                                OnSelectedIndexChanged="GridViewProductCategories_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True" ShowEditButton="False" ShowDeleteButton="False"
                                                        ShowCancelButton="False" />
                                                    <asp:BoundField HeaderText="ProductCategoryId" DataField="ProductCategoryId" SortExpression="ProductCategoryId"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="ProductCategoryType" DataField="ProductCategoryType" SortExpression="ProductCategoryType"
                                                        ReadOnly="True" Visible="False" />
                                                    <asp:BoundField HeaderText="ProductType" DataField="ProductType" SortExpression="ProductType" />
                                                    <asp:BoundField HeaderText="PackingCategory" DataField="PackingCategory"
                                                        SortExpression="PackingCategory" Visible="False" />
                                                </Columns>
                                            </asp:GridView>
                                            <asp:DetailsView Visible="false" ID="DetailsViewInsertProductProductCategories" runat="server"
                                                AutoGenerateRows="False" DataSourceID="ObjectDataSourceProductCategories" OnLoad="DetailsViewInsertProductProductCategories_Load">
                                                <Fields>
                                                    <asp:TemplateField HeaderText="ProductCategoryId" InsertVisible="False" SortExpression="ProductCategoryId">
                                                        <InsertItemTemplate>
                                                            <asp:Button ID="AddProductCategories" runat="server" CommandName="Insert" Text="Add" />
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ProductCategoryType" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextProductCategoryType" runat="server" Text='<%# Bind("ProductCategoryType") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PackingCategory" InsertVisible="False">
                                                        <InsertItemTemplate>
                                                            <asp:TextBox ID="DetailTextPackingCategory" runat="server" Text='<%# Bind("PackingCategory") %>'>
                                                            </asp:TextBox>
                                                        </InsertItemTemplate>
                                                    </asp:TemplateField>
                                                </Fields>
                                            </asp:DetailsView>
                                            <asp:ObjectDataSource ID="ObjectDataSourceProductCategories" runat="server" TypeName="Product"
                                                SelectMethod="ListProductCategories">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="Product" SelectMethod="GetProductsSel">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="ProductCategoryId" SessionField="ProductCategoryId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <asp:ObjectDataSource ID="ObjectDataSourceProductUnsel" runat="server" TypeName="Product"
                                                SelectMethod="GetProductsUnsel">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="ProductCategoryId" SessionField="ProductCategoryId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListProduct" runat="server" DataSourceID="ObjectDataSourceProduct"
                                                DataTextField="Product" DataValueField="ProductId" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="LabelErrorMsg" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectProduct" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectProduct" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectProduct" runat="server" Text="Select All" OnClick="ButtonSelectProduct_Click" />
                                    <asp:Button ID="ButtonDeselectProduct" runat="server" Text="Deselect All" OnClick="ButtonDeselectProduct_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:Button ID="Button4" runat="server" Text=">>" OnClick="ButtonLinkProductUnsel_Click" />
                                    <br />
                                    <br />
                                    <asp:Button ID="Button3" runat="server" Text="<<" OnClick="ButtonLinkProduct_Click" />
                                </td>
                                <td>
                                    <div style="clear: left;">
                                    </div>
                                    <br />
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBoxList ID="CheckBoxListProductUnsel" runat="server" DataSourceID="ObjectDataSourceProductUnsel"
                                                DataTextField="Product" DataValueField="ProductId" RepeatColumns="2">
                                            </asp:CheckBoxList>
                                            <asp:Label ID="Label3" runat="server" Text="">
                                            </asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ButtonSelectProductUnsel" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="ButtonDeselectProductUnsel" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:Button ID="ButtonSelectProductUnsel" runat="server" Text="Select All" OnClick="ButtonSelectProductUnsel_Click" />
                                    <asp:Button ID="ButtonDeselectProductUnsel" runat="server" Text="Deselect All" OnClick="ButtonDeselectProductUnsel_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Button4" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="Button3" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
