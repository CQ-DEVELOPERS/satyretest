using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class StaticInfo_ProductImport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        /*
            code here to truncate table
        */
        ProductImport.ProductImport_Truncate(Session["ConnectionStringName"].ToString());
        System.Threading.Thread.Sleep(2000);

        if (FileUpload1.HasFile)
        {

            FileUpload1.SaveAs(@"\\KAREN\Users\Import\" + FileUpload1.FileName);
            //FileUpload1.SaveAs(@"\\Cqserver0001\Import\" + FileUpload1.FileName);
            System.Threading.Thread.Sleep(2000);

        }


        GridView1.DataBind();

    }

    protected void Button_AddProduct_Click(object sender, EventArgs e)
    {
        bool result = ProductImport.ProductImport_CheckError(Session["ConnectionStringName"].ToString());
        /*
        *  Have a check here for errors.
        *  if HasError ----- (do not truncate table)
        */

        if (result)
        {
            System.Windows.Forms.MessageBox.Show("Data has errors");
        }
        else
        {
            ProductImport.ProductImport_Add(Session["ConnectionStringName"].ToString());
            System.Threading.Thread.Sleep(5000);
            ProductImport.ProductImport_Truncate(Session["ConnectionStringName"].ToString());
            System.Threading.Thread.Sleep(2000);
        }

        GridView1.DataBind();
    }
}
