﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IntergrationDelimiter.ascx.cs" Inherits="StaticInfo_IntergrationDelimiter" %>
 
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<table>
    <tr style="vertical-align:top">
        <td>
            <telerik:RadListBox ID="RadListBox1" CssClass="RadListBox1" runat="server" Width="450px" Height="500px"
                SelectionMode="Multiple" AllowTransfer="true" TransferToID="RadListBox2" AutoPostBackOnTransfer="true"
                AllowReorder="true" AutoPostBackOnReorder="true" EnableDragAndDrop="true" OnTransferred="RadListBox1_Transferred" Skin="Metro"
                DataSourceID="ObjectDataSourceFields" DataValueField="InterfaceFieldId" DataTextField="InterfaceFieldId">
                <HeaderTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblHeaderColumn" runat="server" Text="Column" Width="200px"></asp:Label>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Datatype" Width="100px"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Mandatory" Width="10px"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="rtbValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "InterfaceField") %>' Width="200px"></asp:Label>
                            </td>
                           <td>
                               <asp:Label ID="lblDatatype" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Datatype") %>' Width="100px"></asp:Label>
                            </td>
                            <td>
                                <telerik:RadButton ID="btnMan" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton" Checked='<%# DataBinder.Eval(Container.DataItem, "Mandatory") %>' Enabled="false"></telerik:RadButton>
                            </td>
                         </tr>
                    </table>
                </ItemTemplate>
            </telerik:RadListBox>
            <asp:ObjectDataSource ID="ObjectDataSourceFields" runat="server" TypeName="Intergration"
                SelectMethod="GetFileFields">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="InterfaceMappingFileId" SessionField="InterfaceMappingFileId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadListBox ID="RadListBox2" CssClass="RadListBox2" runat="server" Width="550px" Height="500px"
                SelectionMode="Multiple" AllowReorder="true" AutoPostBackOnReorder="true" EnableDragAndDrop="true"
                Skin="Metro" OnDeleted="RadListBox2_Deleted"
                DataSourceID="ObjectDataSourceMapping" DataValueField="InterfaceMappingColumnId" DataTextField="InterfaceMappingColumn" DataKeyField="InterfaceMappingColumnId">
                <HeaderTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblHeaderColumn" runat="server" Text="Column" Width="200px"></asp:Label>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Datatype" Width="100px"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Mandatory" Width="10px"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="rtbValue" runat="server" Text='<%# DataBinder.Eval(Container, "Text") %>' Width="200px"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDatatype" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Datatype") %>' Width="100px"></asp:Label>
                            </td>
                            <td>
                                <telerik:RadButton ID="btnMan" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton" Checked='<%# DataBinder.Eval(Container.DataItem, "Mandatory") %>' Enabled="false"></telerik:RadButton>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="rtbStartPos" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "StartPosition") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "FixedWidth") %>' Width="35px" EmptyMessage="Position"></telerik:RadTextBox>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="rtbEndPos" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EndPosition") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "FixedWidth") %>' Width="35px" EmptyMessage="Length"></telerik:RadTextBox>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="rtbFormat" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Format") %>' Width="120px" MaxLength="30" EmptyMessage="Optional Format"></telerik:RadTextBox>
                            </td>
                        </tr>
                    </table>
            </ItemTemplate> 
            </telerik:RadListBox>
            <asp:ObjectDataSource ID="ObjectDataSourceMapping" runat="server" TypeName="Intergration"
                SelectMethod="GetMappingColumn">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="InterfaceMappingFileId" SessionField="InterfaceMappingFileId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
            <br />
            <telerik:RadButton ID="RadButtonSave" runat="server" Text="<%$ Resources:Default, ButtonSave %>" OnClick="RadButtonSave_Click" Width="150px">
                <Icon PrimaryIconCssClass="rbSave" />
            </telerik:RadButton>
            <br />
            <telerik:RadButton ID="RadButtonLayout" runat="server" Text="<%$ Resources:Default, Layout %>" OnClick="RadButtonLayout_Click" Width="150px">
                <Icon PrimaryIconCssClass="rbConfig" />
            </telerik:RadButton>
            <br />
            <telerik:RadButton ID="RadButtonDownload" runat="server" Text="<%$ Resources:Default, Download %>" OnClick="RadButtonDownload_Click" Width="150px">
                <Icon PrimaryIconCssClass="rbDownload" />
            </telerik:RadButton>
        </td>
    </tr>
</table>
<telerik:RadTextBox ID="txtData" runat="server" TextMode="MultiLine"></telerik:RadTextBox>
