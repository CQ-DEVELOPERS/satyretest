<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="IntergrationFileExplorer.aspx.cs" Inherits="StaticInfo_IntegrationWizard"
    Title="<%$ Resources:Default, IntegrationWizardTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/StaticInfo/IntergrationFileExplorer.ascx" TagPrefix="uc1" TagName="IntergrationFileExplorer" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, IntegrationWizardTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, IntegrationWizardAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
<%--        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="IntergrationFileExplorer">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="IntergrationFileExplorer" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>--%>
    </telerik:RadAjaxManager>
    
    <uc1:IntergrationFileExplorer runat="server" ID="IntergrationFileExplorer" EnableTheming="true" Skin="Metro">
    </uc1:IntergrationFileExplorer>
</asp:Content>
