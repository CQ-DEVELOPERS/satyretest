<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="StorageUnitLabelImport.aspx.cs" Inherits="StaticInfo_StorageUnitLabelImport"
    Title="<%$ Resources:Default, ReportTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
 <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabDownload" HeaderText="<%$ Resources:Default, Upload%>">
            <ContentTemplate>
                <asp:Label ID="LabelFileUpload" runat="server" Text="Click Browse to find the file to upload:"></asp:Label>
                <asp:UpdatePanel ID="UpdatePanelFileUpload1" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="500" ToolTip="Click Browse to find the file to upload"/>
                            </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <br />
                                    <asp:Button ID="ButtonUpload" runat="server" Text="Save" OnClick="ButtonUpload_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <triggers>
                        <asp:postbacktrigger ControlID="ButtonUpload" />
                    </triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Download%>">
            <ContentTemplate>
                <asp:Label ID="LabelPrint" runat="server" Text="View and download current label category list:"></asp:Label>
                <asp:Button ID="ButtonPrint" runat="server" Text="View Master" OnClick="ButtonPrint_Click" />
                <asp:UpdatePanel ID="UpdatePanelDownload" runat="server">
                    <ContentTemplate>
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="600px"></rsweb:ReportViewer>
                    </ContentTemplate>
                    <triggers>
                        <asp:postbacktrigger ControlID="ButtonPrint" />
                    </triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>

