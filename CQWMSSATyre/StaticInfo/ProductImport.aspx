<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductImport.aspx.cs" Inherits="StaticInfo_ProductImport" Title="<%$ Resources:Default, ProductMaintenance %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ProductMaintenance %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
        ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40"
        TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPanePeriod" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a file to Upload</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPeriod" runat="server">
                        <ContentTemplate>
                            <asp:FileUpload ID="FileUpload1" runat="server" Width="500" />
                            <asp:Button ID="ButtonUpload" runat="server" Text="Save" OnClick="ButtonUpload_Click" />
                            <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server" TargetControlID="ButtonUpload"
                                ConfirmText="Please note that all previous data upload data will be truncated." />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ButtonUpload" />
                        </Triggers>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridView1" runat="server" DataKeyNames="ProductImportId" DataSourceID="ObjectdtasourceProductImport"
                AutoGenerateColumns="False">
                <Columns>
                    <asp:CommandField ShowEditButton="true" ShowDeleteButton="true" />
                    <asp:BoundField HeaderText="ProductCode" DataField="ProductCode" />
                    <asp:BoundField HeaderText="SKUCode" DataField="SKUCode" />
                    <asp:BoundField HeaderText="Product" DataField="Product" />
                    <asp:BoundField HeaderText="ProductBarcode" DataField="ProductBarcode" />
                    <asp:BoundField HeaderText="MinimumQuantity" DataField="MinimumQuantity" />
                    <asp:BoundField HeaderText="ReorderQuantity" DataField="ReorderQuantity" />
                    <asp:BoundField HeaderText="MaximumQuantity" DataField="MaximumQuantity" />
                    <asp:BoundField HeaderText="CuringPeriodDays" DataField="CuringPeriodDays" />
                    <asp:BoundField HeaderText="ShelfLifeDays" DataField="ShelfLifeDays" />
                    <asp:BoundField HeaderText="QualityAssuranceIndicator" DataField="QualityAssuranceIndicator" />
                    <asp:BoundField HeaderText="ProductType" DataField="ProductType" />
                    <asp:BoundField HeaderText="PackType" DataField="PackType" />
                    <asp:BoundField HeaderText="PackQuantity" DataField="PackQuantity" />
                    <asp:BoundField HeaderText="PackBarcode" DataField="PackBarcode" />
                    <asp:BoundField HeaderText="PackVolume" DataField="PackVolume" />
                    <asp:BoundField HeaderText="PackWeight" DataField="PackWeight" />
                    <asp:TemplateField HeaderText="Error">
                        <ItemTemplate>
                            <asp:Label ID="LabelErrorFlag" runat="server" ToolTip='<%# "Error Messages :" + Environment.NewLine + Eval("ErrorLog").ToString().Replace(@"\\n", Environment.NewLine) %>'
                                Text='<%# Eval("HasError").ToString() == "True" ? Eval("HasError").ToString()  : "False" %>'
                                ForeColor="White" BackColor='<%# Eval("HasError").ToString() == "True" ? System.Drawing.Color.FromName("Red")  : System.Drawing.Color.FromName("Green") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Error Message">
                        <ItemTemplate>
                            <asp:Label ID="LabelErrorMessage" runat="server" Text='<%# "Error Messages :" +  Eval("ErrorLog").ToString().Replace(@"\\n", ",") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectdtasourceProductImport" runat="server" TypeName="ProductImport"
                SelectMethod="ProductImport_Search" UpdateMethod="ProductImport_Update" DeleteMethod="ProductImport_Delete">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="productCode" />
                    <asp:Parameter Name="skuCode" />
                    <asp:Parameter Name="product" />
                    <asp:Parameter Name="productBarcode" />
                    <asp:Parameter Name="minimumQuantity" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="reorderQuantity" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="maximumQuantity" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="curingPeriodDays" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="shelfLifeDays" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="qualityAssuranceIndicator" />
                    <asp:Parameter Name="productType" />
                    <asp:Parameter Name="packType" />
                    <asp:Parameter Name="packQuantity" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="packBarcode" />
                    <asp:Parameter Name="packVolume" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="packWeight" ConvertEmptyStringToNull="true" />
                    <asp:Parameter Name="productImportId" />
                </UpdateParameters>
                <DeleteParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="productImportId" />
                </DeleteParameters>
            </asp:ObjectDataSource>
            <asp:Button ID="Button_AddProduct" runat="server" Text="Add Products" OnClick="Button_AddProduct_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
