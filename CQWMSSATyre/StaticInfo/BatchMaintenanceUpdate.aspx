<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="BatchMaintenanceUpdate.aspx.cs" Inherits="StaticInfo_BatchMaintenance" 
Title="Batch Maintenance"
    StylesheetTheme="Default"
    Theme="Default" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<%@ Register Src="~/Common/BatchNumberSearch.ascx" TagName="BatchSearch" TagPrefix="uc1"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="Batch"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Housekeeping Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
 <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneProduct" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Batch</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelBatchSearch" runat="server">
                        <ContentTemplate>
                            <uc1:BatchSearch ID="BatchSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneBatch" runat="server" >
                <Header>
                    <a href="" class="accordionLink">Update Batch</a></Header>
                <Content>
                        <asp:UpdatePanel ID="UpdatePanelBatch" runat="server">
                        <ContentTemplate>
                       <asp:Label ID="LabelProductSelect" runat="server" Text="Please Select a Product" Visible="true"></asp:Label>  
                     <asp:DetailsView Visible="false" ID="DetailsViewInsertBatch" runat="server" AutoGenerateRows="False" DataSourceID="ObjectDataSourceBatch"
                               DefaultMode="Edit" >
                                <Fields>

                                  <asp:TemplateField HeaderText="Product Code">
                                    <ItemTemplate>
                                         <asp:Label id="LabelProductCode" runat="server" Text='<%# Bind("ProductCode")%>' ></asp:Label>
                                    </ItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Product Name">
                                    <ItemTemplate>
                                         <asp:Label id="LabelProductName" runat="server" Text='<%# Bind("Product")%>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:TemplateField HeaderText="Batch Number">
                                    
                                    <ItemTemplate>
                                        <asp:Label id="TextBoxBatchNumber" runat="server" Text='<%# Bind("Batch")%>' Visible="true"></asp:Label> 
                                     </ItemTemplate>   
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Batch Reference Number">
                                    <ItemTemplate>
                                         <asp:TextBox id="TextBoxBatchReference" runat="server"  Text='<%# Bind("BatchReferenceNumber")%>' ></asp:TextBox> 
                                    </ItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Production Line">
                                    <EditItemTemplate>
                                          <asp:DropDownList ID="DropDownProductionLine" runat="server" DataSourceID="ObjectDataSourceLocation" DataTextField="Location" DataValueField="LocationId" >
                                            </asp:DropDownList>
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Production Date and Time">
                                    <EditItemTemplate>
                                          <asp:TextBox id="TextBoxProdDate" runat="server" Width="150px" Text='<%# Bind("ProductionDateTime")%>'></asp:TextBox>              
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Expiry Date">
                                    <EditItemTemplate>
                                         <asp:TextBox id="TextBoxExpiryDate" Width="150px" runat="server" Text='<%# Bind("ExpiryDate")%>' ></asp:TextBox>
                                         <asp:RequiredFieldValidator ID="REQExpiryDate" ControlToValidate="TextBoxExpiryDate" Text="Please Select Expiry Date" runat="server"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Shelf Life">
                                    <EditItemTemplate>
                                         <asp:TextBox id="TextBoxShelfLife" runat="server"  Text='<%# Bind("ShelfLifeDays")%>'></asp:TextBox>
                                         <asp:RequiredFieldValidator ID="REQShelfLife" ControlToValidate="TextBoxShelfLife" Text="Please Enter Shelf Life" runat="server"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Certificate supplied">
                                    <EditItemTemplate>
                                                              <asp:RadioButtonList id="RadioCOACert" runat="server" RepeatDirection="Horizontal" >
                                                              <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                                                              <asp:ListItem Text="N" Value="N"></asp:ListItem>
                                                              </asp:RadioButtonList>   
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Batch Status">
                                    <EditItemTemplate>
                                            <asp:DropDownList ID="DropDownBatchStatus" runat="server" DataSourceID="ObjectDataSourceStatus" DataTextField="Status" DataValueField="StatusId" >
                                            </asp:DropDownList>
                                         <asp:RequiredFieldValidator ID="REQBatchStatus" ControlToValidate="DropDownBatchStatus" Text="Please Select a Batch Status" runat="server"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Status Update Operator ID">
                                    <ItemTemplate>                                                                              
                                         <asp:TextBox id="TextBoxStatusOperatorID" runat="server" ReadOnly="true" Text='<%# (Eval("StatusUpdatebyId").ToString() == "" ? Session["UserName"].ToString() : Eval("StatusUpdatebyId")) %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Status Update Date">
                                    <EditItemTemplate>
                                         <asp:TextBox id="TextBoxStatusUpdateDate" runat="server" Text='<%# (Eval("StatusUpdateDate").ToString() == "" ? DateTime.Now.ToString() : Eval("StatusUpdateDate")) %>' ReadOnly="true" Width="150px"  ></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Planned Production Yield">
                                    <EditItemTemplate>
                                         <asp:TextBox id="TextBoxPlannedYield" runat="server" Text='<%# Bind("ExpectedYield")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Actual Production Yield">
                                    <EditItemTemplate>
                                         <asp:TextBox id="TextBoxActualYield" runat="server" Text='<%# Bind("ActualYield")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Actual Filling Yield">
                                    <EditItemTemplate>
                                         <asp:TextBox id="TextBoxActualFillingYield" runat="server" Text='<%# Bind("FilledYield")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Create Date and Time">
                                    <EditItemTemplate>
                                         <asp:TextBox id="TextBoxCreateDateTime" runat="server" Text='<%# DateTime.Now %>' ReadOnly="true" Width="150px"  ></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Update by">
                                    <ItemTemplate>
                                         <asp:TextBox id="TextBoxupdateOperatorID" runat="server" ReadOnly="true" Text='<%# Session["UserName"].ToString() %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Update Date">
                                    <EditItemTemplate>
                                         <asp:TextBox id="TextBoxUpdateDate" runat="server" Text='<%# DateTime.Now.ToString() %>' ReadOnly="true" Width="150px"  ></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    </asp:TemplateField>
                                                                      
                                  <asp:TemplateField>
                               <EditItemTemplate>
                                        <asp:LinkButton ID="btnlocInsert" runat="server" CommandName="Edit" Text="Update" ></asp:LinkButton>
                                        <asp:LinkButton ID="btnlocCancel" runat="server" CausesValidation="false" CommandName="Cancel" Text="Cancel" ></asp:LinkButton>
                               </EditItemTemplate>
                               </asp:TemplateField>
                                               
                                </Fields>
                            </asp:DetailsView>
</ContentTemplate>
</asp:UpdatePanel>
                    
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneBatchFields" runat="server" >
                         <Header>
                            <a href="" class="accordionLink">Batch Information</a></Header>
                        <Content>
                        <asp:UpdatePanel ID="UpdatePanelBatchFields" runat="server">
                        <ContentTemplate>
                        
                        
                        <asp:DetailsView ID="DetailsViewInsertBatchFields" runat="server" AutoGenerateRows="TRUE" DataSourceID="ObjectDataSourceBatchFields" DefaultMode="Insert"  >
                                <Fields>
                                                                    
                                    
                        </Fields>
                        </asp:DetailsView>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        </Content>
                        
                        </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
   
                <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch" 
                    SelectMethod="SearchBatchesByBatch"
                    UpdateMethod ="UpdateBatch" OnUpdating="ObjectDataSourceBatch_Updating">
                    
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="batch"  SessionField="Batch" Type="String" />
                        <asp:Parameter Name="batchReferenceNumber" DefaultValue="" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                        <asp:SessionParameter Name="batchId" SessionField="BatchId" Type="int32" /> 
                                        <asp:Parameter Name="statusId" Type="int32" />
                                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="int32" /> 
                                        <asp:SessionParameter Name="statusUpdateById" SessionField="OperatorId" Type="int32" />
                                        <asp:Parameter Name="batch" Type="String" />
                                        <asp:Parameter Name="batchReferenceNumber" Type="String"  />
                                        <asp:Parameter Name="productionLine" Type="int32"  />
                                        <asp:Parameter Name="productionDateTime" Type="String"  />
                                        <asp:Parameter Name="createDate" Type="String" />
                                        <asp:Parameter Name="expiryDate" Type="String" />
                                        <asp:Parameter Name="incubationDays" Type="int32" />
                                        <asp:Parameter Name="shelfLifeDays" Type="int32" />
                                        <asp:Parameter Name="expectedYield" Type="int32" />
                                        <asp:Parameter Name="actualYield" Type="int32" />
                                        <asp:Parameter Name="filledYield" Type="int32" />
                                        <asp:Parameter Name="eCLNumber" Type="String" />
                                        <asp:Parameter Name="cOACertificate" Type="String" />
                                        <asp:SessionParameter Name="modifiedBy" SessionField="OperatorId" Type="int32" />
                                        <asp:Parameter Name="batchId" Type="int32" />
                                        <asp:Parameter Name="locationId" Type="int32" />
                    </UpdateParameters>                   
                   
                </asp:ObjectDataSource>
                
                <asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status" SelectMethod="GetStatus">   
                        <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="type" DefaultValue="P" Type="String" /> 
                        </SelectParameters>
                </asp:ObjectDataSource>
                
                <asp:ObjectDataSource ID="ObjectDataSourceBatchFields" runat="server" TypeName="Batch" SelectMethod="GetBatchFields">   
                        <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                         
                        </SelectParameters>
                </asp:ObjectDataSource>            
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location" SelectMethod="GetLocationsByAreaCode">   
                        <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="int32" /> 
                        <asp:Parameter Name="areaCode" Type="string" DefaultValue="R" />
                         
                        </SelectParameters>
                </asp:ObjectDataSource>
               
                  
</asp:Content>

