<%@ Control Language="C#" AutoEventWireup="false" CodeFile="NLWAX.ascx.cs" Inherits="NLWAX" %>

<!-- Using HiddenFields to persist client side data -->
<asp:HiddenField ID="hdfPrinterList" runat="server" />
<asp:HiddenField ID="hdfPortList" runat="server" />
<asp:HiddenField ID="hdfLabel" runat="server" />
<asp:HiddenField ID="hdfFile" runat="server" />
<asp:HiddenField ID="hdfPrinter" runat="server" />
<asp:HiddenField ID="hdfPort" runat="server" />
<asp:HiddenField ID="hdfSendTo" runat="server" Value="To Printer" />
<asp:HiddenField ID="hdfIsPrinting" runat="server" />
<asp:HiddenField ID="hdfBaseURL" runat="server" />
<br />
<script type="text/javascript" language="javascript" src="javascript/NLWAXFunctions.js"></script>
<script type="text/javascript" language="javascript" src="javascript/global.js"></script>
<!-- NLWAX ActiveX control -->
<%--<object classid="clsid:ECBB0B73-FDF4-4DBD-B7C9-8450C2D9FB66" 
    codebase="./NLWAX.ocx#version=1,0,0,34" 
    width="1" 
    height="1" 
    id="NLWAXForm" 
    name="NLWAXForm">
</object>--%>
<script type="text/javascript">
    if (BrowserDetect.browser == 'Explorer') {
        //<!-- NLWAX ActiveX control -->
        document.write(
        "<object classid=\"clsid:ECBB0B73-FDF4-4DBD-B7C9-8450C2D9FB66\""
            + "codebase=\"./NLWAX.ocx#version=1,0,0,34\""
            + "width=\"1\""
            + "height=\"1\""
            + "id=\"NLWAXForm\""
            + "name=\"NLWAXForm\">"
        + "</object>");
    } else {
        document.write(
        "<applet name=\"NLWAXForm\""
            + "code=\"nlwax.UserInterface\""
            + "WIDTH=0 HEIGHT=0 "
            + "ARCHIVE=\"./nlwax.jar\">"
        + "</applet>");
    }
</script>    
<!--  Include external JavaScript file-->
