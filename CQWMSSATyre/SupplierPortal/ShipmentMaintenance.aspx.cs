using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;
using System.IO;

public partial class SupplierPortal_ShipmentMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["MenuId"] == null)
                    Session["MenuId"] = 1;

                Session["FromDate"] = DateRange.GetFromDate();
                Session["ToDate"] = DateRange.GetToDate();

                rdpFromDate.SelectedDate = DateRange.GetFromDate();
                rdpToDate.SelectedDate = DateRange.GetToDate().AddDays(1);

                rdpFromDate2.SelectedDate = DateRange.GetFromDate();
                rdpToDate2.SelectedDate = DateRange.GetToDate().AddDays(1);

                RadGridShipmentSearch.DataBind();
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["ShipmentHeaderId"] != null)
                {
                    int ShipmentHeaderId = (int)Session["ShipmentHeaderId"];

                    foreach (GridDataItem item in RadGridShipmentSearch.Items)
                    {
                        theErrMethod = "Page_LoadComplete";
                        if (item.GetDataKeyValue("ShipmentHeaderId").ToString() == ShipmentHeaderId.ToString())
                        {
                            RadGridShipmentSearch.SelectedIndexes.Add(item.ItemIndex);
                            break;
                        }
                    }

                    if (RadGridShipmentSearch.SelectedIndexes.Count == 0)
                    {
                        RadGridShipmentSearch.SelectedIndexes.Clear();
                        RadGridComments.SelectedIndexes.Clear();
                        Session["ShipmentHeaderId"] = null;

                        RadGridLinked.DataBind();
                    }
                }
            }
        }
        catch { }
    }
    #endregion Page_LoadComplete

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            RadGridShipmentSearch.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region ButtonSearch2_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch2_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            RadGridUnlinked.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch2_Click

    #region RadGridComments_OnSelectedIndexChanged
    protected void RadGridComments_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridComments_OnSelectedIndexChanged";

        try
        {
            Session["JobId"] = -1;

            foreach (GridDataItem item in RadGridComments.Items)
            {
                if (item.Selected)
                    Session["JobId"] = (int)item.GetDataKeyValue("JobId");
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "RadGridComments_OnSelectedIndexChanged"

    #region RadGridShipmentSearch_SelectedIndexChanged
    protected void RadGridShipmentSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridShipmentSearch_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in RadGridShipmentSearch.Items)
            {
                if (item.Selected)
                {
                    Session["ShipmentHeaderId"] = item.GetDataKeyValue("ShipmentHeaderId");
                    RadGridLinked.DataBind();
                    RadGridComments.DataBind();
                    break;
                }
            }
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridShipmentSearch_SelectedIndexChanged

    #region RadGridShipmentSequence_SelectedIndexChanged
    protected void RadGridShipmentSequence_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridShipmentSequence_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in RadGridShipmentSequence.Items)
            {
                if (item.Selected)
                {
                    Session["UploadTableId"] = item.GetDataKeyValue("ShipmentSequenceId");
                    Session["UploadTableName"] = "ShipmentSequence";
                    break;
                }
            }
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridShipmentSequence_SelectedIndexChanged

    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";
        try
        {
            foreach (GridDataItem item in RadGridLinked.Items)
            {
                if (item.Selected)
                    e.InputParameters["storageUnitId"] = (int)item.GetDataKeyValue("StorageUnitId");
            }
            

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ObjectDataSourceBatch_OnSelecting"

    #region ObjectDataSourceLinked_Selecting
    protected void ObjectDataSourceLinked_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceLinked_Selecting";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ObjectDataSourceLinked_Selecting

    #region RadGridLinked_SelectedIndexChanging
    protected void RadGridLinked_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        theErrMethod = "RadGridLinked_SelectedIndexChanging";

        try
        {
            foreach (GridDataItem item in RadGridLinked.Items)
            {
                if(item.Selected)
                    Session["StorageUnitId"] = item.GetDataKeyValue("StorageUnitId");
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridLinked_SelectedIndexChanging

    #region RadGridUnlinked_OnItemUpdated
    protected void RadGridUnlinked_OnItemUpdated(object sender, GridUpdatedEventArgs e)
    {
        RadGridLinked.DataBind();
    }
    #endregion RadGridUnlinked_OnItemUpdated

    #region RadGridLinked_OnItemUpdated
    protected void RadGridLinked_OnItemUpdated(object sender, GridUpdatedEventArgs e)
    {
        RadGridUnlinked.DataBind();
    }
    #endregion RadGridLinked_OnItemUpdated

    #region RadGridLinked_OnItemDeleted
    protected void RadGridLinked_OnItemDeleted(object sender, GridDeletedEventArgs e)
    {
        RadGridUnlinked.DataBind();
    }
    #endregion RadGridLinked_OnItemDeleted

    #region RadGridShipmentSearch_ItemCommand
    protected void RadGridShipmentSearch_ItemCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.SelectCommandName)
        {
            foreach (GridDataItem item in RadGridShipmentSearch.Items)
            {
                if (item.RowIndex == e.Item.RowIndex)
                {
                    Session["ShipmentHeaderId"] = item.GetDataKeyValue("ShipmentHeaderId");

                    //Response.Redirect("~/SupplierPortal/ShipmentQuery.aspx");
                    break;
                }
            }
        }
    }
    #endregion RadGridShipmentSearch_ItemCommand

    #region RadGridShipmentSequence_ItemCommand
    protected void RadGridShipmentSequence_ItemCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.SelectCommandName)
        {
            foreach (GridDataItem item in RadGridShipmentSequence.Items)
            {
                if (item.Selected)
                {
                    Session["UploadTableId"] = item.GetDataKeyValue("ShipmentSequenceId");
                    Session["UploadTableName"] = "ShipmentSequence";

                    ScriptManager.RegisterStartupScript(this.Page,
                                                        this.Page.GetType(),
                                                        "newWindow",
                                                        "window.open('../SupplierPortal/UploadFile.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=450,height=500,top=200,left=200');",
                                                        true);
                    break;
                }
            }
        }
    }
    #endregion RadGridShipmentSequence_ItemCommand

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region Render
    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(writer);
        Profile.Pet = new Pet();
        Profile.Pet.SavePetNames(RadGridShipmentSearch, "ShipmentMaintenanceRadGridShipmentSearch");
        Profile.Pet.SavePetNames(RadGridLinked, "ShipmentMaintenanceRadGridLinked");
        Profile.Pet.SavePetNames(RadGridUnlinked, "ShipmentMaintenanceRadGridUnlinked");
        Profile.Pet.SavePetNames(RadGridComments, "ShipmentMaintenanceRadGridComments");
        Profile.Pet.SavePetNames(RadGridShipmentSequence, "ShipmentMaintenanceRadGridShipmentSequence");
    }
    #endregion Render

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
            if (Profile.Pet.Names.Count > 0)
            {
                /*This Grid is an AutoGeneratedColumn grid. At this point in the page life cycle the AutoGeneratedColumns array 
                is not initililized and cannot load the settings for the grid. There is a CreateColumnSet() method that I thought would work 
                to make the columns available at this point but this did not work. I also tried to put LoadSettings call in the PreRender event
                but this would occur after columns were made invisible thus making the columns visible again by loading the previous settings.
                The only place that you might be able to do this is on the ColumnCreated event or the ItemCreated event.	 
                For grid columns that are declaratively set at design time or added manually in the Page_Init event this will work perfectly.*/
                if (Profile.Pet.Names.ContainsKey("ShipmentMaintenanceRadGridShipmentSearch"))
                {
                    //RadGrid1.MasterTableView.GenerateColumnsCreateColumnSet(true);
                    GridSettings settings = new GridSettings(RadGridShipmentSearch);
                    settings.LoadSettings(Profile.Pet.Names["ShipmentMaintenanceRadGridShipmentSearch"].ToString());
                }

                /*Columns declared on this grid are done so at design time*/
                if (Profile.Pet.Names.ContainsKey("ShipmentMaintenanceRadGridLinked"))
                {
                    GridSettings settings = new GridSettings(RadGridLinked);
                    settings.LoadSettings(Profile.Pet.Names["ShipmentMaintenanceRadGridLinked"].ToString());
                }

                /*Columns declared on this grid are done so at design time*/
                if (Profile.Pet.Names.ContainsKey("ShipmentMaintenanceRadGridUnlinked"))
                {
                    GridSettings settings = new GridSettings(RadGridUnlinked);
                    settings.LoadSettings(Profile.Pet.Names["ShipmentMaintenanceRadGridUnlinked"].ToString());
                }

                /*Columns declared on this grid are done so at design time*/
                if (Profile.Pet.Names.ContainsKey("ShipmentMaintenanceRadGridComments"))
                {
                    GridSettings settings = new GridSettings(RadGridComments);
                    settings.LoadSettings(Profile.Pet.Names["ShipmentMaintenanceRadGridComments"].ToString());
                }

                /*Columns declared on this grid are done so at design time*/
                if (Profile.Pet.Names.ContainsKey("ShipmentMaintenanceRadGridShipmentSequence"))
                {
                    GridSettings settings = new GridSettings(RadGridShipmentSequence);
                    settings.LoadSettings(Profile.Pet.Names["ShipmentMaintenanceRadGridShipmentSequence"].ToString());
                }
            }
    }
    #endregion Page_Init
}