<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ShipmentMaintenance.aspx.cs" Inherits="SupplierPortal_ShipmentMaintenance"
    Title="<%$ Resources:Default, ShipmentMaintenanceTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ShipmentMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ShipmentMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <%--<telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="Textbox" />--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Thumbnail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridShipmentSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridShipmentSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinked" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridUnlinked" />
                    <telerik:AjaxUpdatedControl ControlID="FormViewShipmentHeader" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridComments" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridShipmentSequence" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridShipmentSearch" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridLinked">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridShipmentSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinked" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridUnlinked" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridUnlinked">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinked" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridUnlinked" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridFiles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridFiles" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridShipmentSequence" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridShipmentSequence">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridFiles" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridShipmentSequence" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="ButtonSearch2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridUnlinked" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridComments">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridShipmentSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridComments" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSelect" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridComments" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridDetails">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Search %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Maintain %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Comments %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Shipping %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                <table>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtShipmentNumber" runat="server" Label="Call Off No:" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtxtOrderNumber" runat="server" Label="Order No:" Width="250px"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtProductCode" runat="server" Label="Product Code:" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtxtProduct" runat="server" Label="Description:" Width="250px"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtSupplierCode" runat="server" Label="Supplier Code:" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtxtSupplier" runat="server" Label="Supplier Name:" Width="250px"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>" Width="100px"></asp:Label>
                            <telerik:RadDatePicker ID="rdpFromDate" runat="server"></telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:Label ID="labeToDate" runat="server" Text="<%$ Resources:Default, ToDate %>" Width="100px"></asp:Label>
                            <telerik:RadDatePicker ID="rdpToDate" runat="server"></telerik:RadDatePicker>
                        </td>
                        <td>
                            <telerik:RadButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"></telerik:RadButton>
                        </td>
                    </tr>
                </table>
                <telerik:RadGrid ID="RadGridShipmentSearch" runat="server" OnRowDataBound="RadGridShipmentSearch_RowDataBound"
                    AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true"
                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePlanning"
                    OnSelectedIndexChanged="RadGridShipmentSearch_SelectedIndexChanged" PageSize="30" ShowGroupPanel="True" Height="500px" Skin="Metro"
                    OnItemCommand="RadGridShipmentSearch_ItemCommand">
                    <ExportSettings IgnorePaging="True" OpenInNewWindow="True">
                        <Pdf PageHeight="210mm" PageWidth="297mm" PageTitle="SushiBar menu" DefaultFontFamily="Arial Unicode MS"
                            PageBottomMargin="20mm" PageTopMargin="20mm" PageLeftMargin="20mm" PageRightMargin="20mm">
                        </Pdf>
                    </ExportSettings>
                    <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                    <MasterTableView DataKeyNames="ShipmentHeaderId" DataSourceID="ObjectDataSourcePlanning"
                        CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" 
                                FilterControlAltText="Filter EditCommandColumn column"></telerik:GridEditCommandColumn>
                            <telerik:GridButtonColumn DataTextField="ShipmentNumber" Text="<%$ Resources:Default, Upload %>" HeaderText="<%$ Resources:Default, ShipmentNumber %>" CommandName="Select"></telerik:GridButtonColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="PurchaseOrder" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                SortExpression="PurchaseOrder"
                                UniqueName="PurchaseOrder">
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn DataField="RequiredDate" HeaderText="<%$ Resources:Default, RequiredDate %>" UniqueName="RequiredDate" SortExpression="RequiredDate"></telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                SortExpression="Status"
                                UniqueName="Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListPriorityId" 
                                ListTextField="Priority" ListValueField="PriorityId"
                                DataSourceID="ObjectDataSourcePriority" DataField="PriorityId" 
                                HeaderText="<%$ Resources:Default, Priority %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListTransportModeId" 
                                ListTextField="TransportMode" ListValueField="TransportModeId"
                                DataSourceID="ObjectDataSourceTransportMode" DataField="TransportModeId" 
                                HeaderText="<%$ Resources:Default, TransportMode %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListExternalCompanyId" 
                                ListTextField="ExternalCompany" ListValueField="ExternalCompanyId"
                                DataSourceID="ObjectDataSourceExternalCompany" DataField="ExternalCompanyId" 
                                HeaderText="<%$ Resources:Default, Participant %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListNotificationMethodId"
                                ListTextField="NotificationMethod" ListValueField="NotificationMethodId"
                                DataSourceID="ObjectDataSourceNotificationMethod" DataField="NotificationMethodId" 
                                HeaderText="<%$ Resources:Default, NotificationMethod %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>"
                                SortExpression="Remarks"
                                UniqueName="Remarks">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="NumberOfOrders" HeaderText="<%$ Resources:Default, NumberOfOrders %>" SortExpression="NumberOfOrders" UniqueName="NumberOfOrders"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>" SortExpression="NumberOfLines" UniqueName="NumberOfLines"></telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="ConfirmReceipt" HeaderText="<%$ Resources:Default, ConfirmReceipt %>" SortExpression="ConfirmReceipt" UniqueName="ConfirmReceipt"></telerik:GridCheckBoxColumn>
                            <telerik:GridCheckBoxColumn DataField="ConfirmAccpeted" HeaderText="<%$ Resources:Default, ConfirmAccpeted %>" SortExpression="ConfirmAccpeted" UniqueName="ConfirmAccpeted"></telerik:GridCheckBoxColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                        ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                        AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        <Resizing AllowColumnResize="True" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="Shipment"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateShipmentHeader" InsertMethod="InsertShipmentHeader" DeleteMethod="DeleteShipmentHeader">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:ControlParameter Name="shipmentNumber" ControlID="rtxtShipmentNumber" />
                        <asp:ControlParameter Name="OrderNumber" ControlID="rtxtOrderNumber" />
                        <asp:ControlParameter Name="ProductCode" ControlID="rtxtProductCode" />
                        <asp:ControlParameter Name="Product" ControlID="rtxtProduct" />
                        <asp:ControlParameter Name="ExternalCompanyCode" ControlID="rtxtSupplierCode" />
                        <asp:ControlParameter Name="ExternalCompany" ControlID="rtxtSupplier" />
                        <asp:ControlParameter Name="FromDate" ControlID="rdpFromDate" />
                        <asp:ControlParameter Name="ToDate" ControlID="rdpToDate" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="shipmentHeaderId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="shipmentNumber" Type="String"></asp:Parameter>
                        <asp:Parameter Name="priorityId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="externalCompanyId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="transportModeId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="notificationMethodId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="requiredDate" Type="DateTime"></asp:Parameter>
                        <asp:Parameter Name="confirmReceipt" Type="Boolean"></asp:Parameter>
                        <asp:Parameter Name="confirmAccpeted" Type="Boolean"></asp:Parameter>
                        <asp:Parameter Name="remarks" Type="String"></asp:Parameter>
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="shipmentHeaderId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="shipmentNumber" Type="String"></asp:Parameter>
                        <asp:Parameter Name="priorityId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="externalCompanyId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="transportModeId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="notificationMethodId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="requiredDate" Type="DateTime"></asp:Parameter>
                        <asp:Parameter Name="confirmReceipt" Type="Boolean"></asp:Parameter>
                        <asp:Parameter Name="confirmAccpeted" Type="Boolean"></asp:Parameter>
                        <asp:Parameter Name="remarks" Type="String"></asp:Parameter>
                    </InsertParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="shipmentHeaderId" Type="Int32"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel2">
                <telerik:RadDockLayout runat="server" ID="RadDockLayout1">
                    <table>
                        <tr>
                            <td valign="top">
                                <telerik:RadDockZone runat="server" ID="RadDockZone1" MinHeight="800" Width="600" Skin="Metro">
                                    <telerik:RadDock runat="server" ID="RadDockLinked" Title="Call-Off Order" DockMode="Docked" Skin="Metro">
                                        <ContentTemplate>
                                            <asp:FormView ID="FormViewShipmentHeader" runat="server" AutoGenerateRows="false"
                                                 DataSourceID="ObjectDataSourceShipmentDetailsView" DataKeyNames="ShipmentHeaderId" >
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <telerik:RadTextBox ID="LabelShipmentNumber" runat="server" Label="<%$ Resources:Default, ShipmentNumber %>" Text='<%# Bind("ShipmentNumber") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                                            </td>
                                                            <td>
                                                                <telerik:RadTextBox ID="LabelTransportMode" runat="server" Label="<%$ Resources:Default, TransportMode %>" Text='<%# Bind("TransportMode") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <telerik:RadTextBox ID="LabelWarehouse" runat="server" Label="<%$ Resources:Default, Warehouse %>" Text='<%# Bind("Warehouse") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                                            </td>
                                                            <td>
                                                                <telerik:RadTextBox ID="LabelNotificationMethod" runat="server" Label="<%$ Resources:Default, NotificationMethod %>" Text='<%# Bind("NotificationMethod") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <telerik:RadTextBox ID="LabelPriority" runat="server" Label="<%$ Resources:Default, Priority %>" Text='<%# Bind("Priority") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                                            </td>
                                                            <td>
                                                                <telerik:RadTextBox ID="LabelRequiredDate" runat="server" Label="<%$ Resources:Default, RequiredDate %>" Text='<%# Bind("RequiredDate") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <telerik:RadTextBox ID="LabelConfirmAccpeted" runat="server" Label="<%$ Resources:Default, ConfirmAccpeted %>" Text='<%# Bind("ConfirmAccpeted") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                                            </td>
                                                            <td>
                                                                <telerik:RadTextBox ID="LabelConfirmReceipt" runat="server" Label="<%$ Resources:Default, ConfirmReceipt %>" Text='<%# Bind("ConfirmReceipt") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                                            </td>
                                                            <%--<td>
                                                                <telerik:RadTextBox ID="LabelStatus" runat="server" Label="<%$ Resources:Default, Status %>" Text='<%# Bind("Status") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                                            </td>--%>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:FormView>
                                            <asp:ObjectDataSource ID="ObjectDataSourceShipmentDetailsView" runat="server" TypeName="Shipment" SelectMethod="SelectOrder">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                                    <asp:SessionParameter Type="Int32" Name="shipmentHeaderId" SessionField="ShipmentHeaderId"></asp:SessionParameter>
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                            <telerik:RadGrid ID="RadGridLinked" runat="server" DataSourceID="ObjectDataSourceLinked" AutoGenerateDeleteColumn="true"
                                                AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                                                OnItemDeleted="RadGridLinked_OnItemDeleted" OnItemUpdated="RadGridLinked_OnItemUpdated" Skin="Metro">
                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                <MasterTableView DataKeyNames="ShipmentDetailId">
                                                    <Columns>
                                                        <telerik:GridEditCommandColumn EditText="<%$ Resources:Default, Edit %>" UpdateText="<%$ Resources:Default, Update %>" CancelText="<%$ Resources:Default, Cancel %>"></telerik:GridEditCommandColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                                            SortExpression="ProductCode"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                                            SortExpression="Product"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                                            SortExpression="SKUCode"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="false" DataField="NewShipmentQuantity" HeaderText="<%$ Resources:Default, NewShipmentQuantity %>"
                                                            SortExpression="NewShipmentQuantity"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Resizing AllowColumnResize="True" />
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:ObjectDataSource ID="ObjectDataSourceLinked" runat="server" TypeName="Shipment"
                                                SelectMethod="SearchLinked" UpdateMethod="UpdateLinked" DeleteMethod="DeleteLinked">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="shipmentHeaderId" Type="Int32" SessionField="ShipmentHeaderId" DefaultValue="-1" />
                                                </SelectParameters>
                                                <UpdateParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:Parameter Name="shipmentDetailId" Type="Int32"></asp:Parameter>
                                                    <asp:Parameter Name="NewShipmentQuantity" Type="Decimal"></asp:Parameter>
                                                </UpdateParameters>
                                                <DeleteParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:Parameter Name="shipmentDetailId" Type="Int32"></asp:Parameter>
                                                </DeleteParameters>
                                            </asp:ObjectDataSource>
                                        </ContentTemplate>
                                    </telerik:RadDock>
                                </telerik:RadDockZone>
                            </td>
                            <td valign="top">
                                <telerik:RadDockZone runat="server" ID="RadDockZone2" MinHeight="800" Width="1000" DockMode="Docked" Skin="Metro">
                                    <telerik:RadDock runat="server" ID="RadDockUnlinked" Title="Call Off's" Skin="Metro">
                                        <ContentTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <telerik:RadComboBox ID="DropDownListDocumentTypeId" runat="server" Label="Document Type:"
                                                            DataSourceID="ObjectDataSourceDocumentTypeId" DataValueField="InboundDocumentTypeId" DataTextField="InboundDocumentType" Skin="Metro">
                                                        </telerik:RadComboBox>
                                                        <asp:ObjectDataSource ID="ObjectDataSourceDocumentTypeId" TypeName="InboundDocumentType" SelectMethod="GetInboundDocumentTypeParameters" runat="server">
                                                            <SelectParameters>
                                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                    </td>
                                                    <td>
                                                        <telerik:RadTextBox ID="rtxtOrderNumber2" runat="server" Label="Order No:" Width="250px"></telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <telerik:RadTextBox ID="rtxtProductCode2" runat="server" Label="Product Code:" Width="250px"></telerik:RadTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadTextBox ID="rtxtProduct2" runat="server" Label="Description:" Width="250px"></telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <telerik:RadTextBox ID="rtxtSupplierCode2" runat="server" Label="Supplier Code:" Width="250px"></telerik:RadTextBox>
                                                    </td>
                                                    <td>
                                                        <telerik:RadTextBox ID="rtxtSupplier2" runat="server" Label="Supplier Name:" Width="250px"></telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelFromDate2" runat="server" Text="<%$ Resources:Default, FromDate %>" Width="100px"></asp:Label>
                                                        <telerik:RadDatePicker ID="rdpFromDate2" runat="server" Skin="Metro"></telerik:RadDatePicker>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="labeToDate2" runat="server" Text="<%$ Resources:Default, ToDate %>" Width="100px"></asp:Label>
                                                        <telerik:RadDatePicker ID="rdpToDate2" runat="server"></telerik:RadDatePicker>
                                                    </td>
                                                    <td>
                                                        <telerik:RadButton ID="ButtonSearch2" OnClick="ButtonSearch2_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"></telerik:RadButton>
                                                    </td>
                                                </tr>
                                            </table>
                                            <telerik:RadGrid ID="RadGridUnlinked" runat="server" DataSourceID="ObjectDataSourceUnlinked"
                                                AllowAutomaticUpdates="true" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                                                OnItemUpdated="RadGridUnlinked_OnItemUpdated" Skin="Metro">
                                                <PagerStyle Mode="NumericPages"></PagerStyle>
                                                <MasterTableView DataKeyNames="CallOffDetailId">
                                                    <Columns>
                                                        <telerik:GridEditCommandColumn EditText="<%$ Resources:Default, ButtonAdd %>" UpdateText="<%$ Resources:Default, ButtonAdd %>"></telerik:GridEditCommandColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                                            SortExpression="ShipmentNumber"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                                            SortExpression="ProductCode"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                                            SortExpression="Product"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                                            SortExpression="SKUCode"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="PendingDeliveryQuantity" HeaderText="<%$ Resources:Default, PendingDeliveryQuantity %>"
                                                            SortExpression="PendingDeliveryQuantity"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="AwaitingShipmentQuantity" HeaderText="<%$ Resources:Default, AwaitingShipmentQuantity %>"
                                                            SortExpression="AwaitingShipmentQuantity"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="OriginalOrderQuantity" HeaderText="<%$ Resources:Default, OriginalOrderQuantity %>"
                                                            SortExpression="OriginalOrderQuantity"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="false" DataField="NewShipmentQuantity" HeaderText="<%$ Resources:Default, NewShipmentQuantity %>"
                                                            SortExpression="NewShipmentQuantity"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn ReadOnly="true" DataField="IdealShippingMultiples" HeaderText="<%$ Resources:Default, IdealShippingMultiples %>"
                                                            SortExpression="IdealShippingMultiples"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Resizing AllowColumnResize="True" />
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:ObjectDataSource ID="ObjectDataSourceUnlinked" runat="server" TypeName="Shipment"
                                                SelectMethod="SearchUnlinked" UpdateMethod="UpdateUnlinked">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="String" />
                                                    <asp:ControlParameter Name="DocumentTypeId" ControlID="DropDownListDocumentTypeId" />
                                                    <asp:ControlParameter Name="OrderNumber" ControlID="rtxtOrderNumber2" />
                                                    <asp:ControlParameter Name="ProductCode" ControlID="rtxtProductCode2" />
                                                    <asp:ControlParameter Name="Product" ControlID="rtxtProduct2" />
                                                    <asp:ControlParameter Name="ExternalCompanyCode" ControlID="rtxtSupplierCode2" />
                                                    <asp:ControlParameter Name="ExternalCompany" ControlID="rtxtSupplier2" />
                                                    <asp:ControlParameter Name="FromDate" ControlID="rdpFromDate2" />
                                                    <asp:ControlParameter Name="ToDate" ControlID="rdpToDate2" />
                                                </SelectParameters>
                                                <UpdateParameters>
                                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                        Type="String" />
                                                    <asp:SessionParameter Name="shipmentHeaderId" SessionField="ShipmentHeaderId" Type="Int32" />
                                                    <asp:Parameter Name="callOffDetailId" Type="Int32"></asp:Parameter>
                                                    <asp:Parameter Name="newShipmentQuantity" Type="Decimal"></asp:Parameter>
                                                </UpdateParameters>
                                            </asp:ObjectDataSource>
                                        </ContentTemplate>
                                    </telerik:RadDock>
                                </telerik:RadDockZone>
                            </td>
                        </tr>
                    </table>
                </telerik:RadDockLayout>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel3">
                <telerik:RadGrid ID="RadGridComments" runat="server" DataSourceID="ObjectDataSourceComents"
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" AllowAutomaticInserts="true" Skin="Metro">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="CommentId"
                        CommandItemDisplay="Top" DataSourceID="ObjectDataSourceComents"
                        AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridBoundColumn DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" ReadOnly="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>" ReadOnly="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Comment" HeaderText="<%$ Resources:Default, Comments %>"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceComents" runat="server" TypeName="Shipment"
                    SelectMethod="SelectComment" InsertMethod="InsertComment" UpdateMethod="UpdateComment" DeleteMethod="DeleteComment">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="shipmentHeaderId" Type="Int32" SessionField="ShipmentHeaderId" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="shipmentHeaderId" Type="Int32" SessionField="ShipmentHeaderId" />
                        <asp:Parameter Type="String" Name="comment"></asp:Parameter>
                        <asp:SessionParameter Type="Int32" Name="operatorId" SessionField="OperatorId"></asp:SessionParameter>
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="commentId"></asp:Parameter>
                        <asp:Parameter Type="String" Name="comment"></asp:Parameter>
                        <asp:SessionParameter Type="Int32" Name="operatorId" SessionField="OperatorId"></asp:SessionParameter>
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="commentId"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel4">
                <telerik:RadGrid ID="RadGridShipmentSequence" runat="server" DataSourceID="ObjectDataSourceShipmentSequence" OnSelectedIndexChanged="RadGridShipmentSequence_SelectedIndexChanged"
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" Skin="Metro"
                    OnItemCommand="RadGridShipmentSequence_ItemCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="ShipmentSequenceId"
                        Width="100%" CommandItemDisplay="Top" DataSourceID="ObjectDataSourceShipmentSequence"
                        AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridEditCommandColumn InsertText="<%$ Resources:Default, Insert %>" EditText="<%$ Resources:Default, Edit %>" UpdateText="<%$ Resources:Default, Update %>" CancelText="<%$ Resources:Default, Cancel %>"></telerik:GridEditCommandColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListTransportModeId" 
                                ListTextField="TransportMode" ListValueField="TransportModeId"
                                DataSourceID="ObjectDataSourceTransportMode" DataField="TransportModeId" 
                                HeaderText="<%$ Resources:Default, TransportMode %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListExternalCompanyId" 
                                ListTextField="ExternalCompany" ListValueField="ExternalCompanyId"
                                DataSourceID="ObjectDataSourceExternalCompany" DataField="ExternalCompanyId" 
                                HeaderText="<%$ Resources:Default, Participant %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListNotificationMethodId"
                                ListTextField="NotificationMethod" ListValueField="NotificationMethodId"
                                DataSourceID="ObjectDataSourceNotificationMethod" DataField="NotificationMethodId" 
                                HeaderText="<%$ Resources:Default, NotificationMethod %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridDateTimeColumn DataField="CollectionDate" HeaderText="<%$ Resources:Default, CollectionDate %>"></telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn DataField="ReceivedDate" HeaderText="<%$ Resources:Default, ReceivedDate %>"></telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn DataField="DespatchDate" HeaderText="<%$ Resources:Default, DespatchedDate %>"></telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn DataField="PlannedDelivery" HeaderText="<%$ Resources:Default, PlannedDeliveryDate %>"></telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>"></telerik:GridBoundColumn>
                            <telerik:GridButtonColumn Text="<%$ Resources:Default, Upload %>" HeaderText="<%$ Resources:Default, Upload %>" CommandName="Select"></telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceShipmentSequence" runat="server" TypeName="Shipment"
                    SelectMethod="SelectSequence" InsertMethod="InsertSequence" UpdateMethod="UpdateSequence" DeleteMethod="DeleteSequence">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="shipmentHeaderId" Type="Int32" SessionField="ShipmentHeaderId" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="shipmentSequenceId"></asp:Parameter>
                        <asp:SessionParameter Name="shipmentHeaderId" Type="Int32" SessionField="ShipmentHeaderId" />
                        <asp:Parameter Type="Int32" Name="ExternalCompanyId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="TransportModeId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="NotificationMethodId"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="CollectionDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="ReceivedDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="DespatchDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="PlannedDelivery"></asp:Parameter>
                        <asp:Parameter Type="String" Name="Remarks"></asp:Parameter>
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="shipmentSequenceId"></asp:Parameter>
                        <asp:SessionParameter Name="shipmentHeaderId" Type="Int32" SessionField="ShipmentHeaderId" />
                        <asp:Parameter Type="Int32" Name="ExternalCompanyId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="TransportModeId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="NotificationMethodId"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="CollectionDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="ReceivedDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="DespatchDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="PlannedDelivery"></asp:Parameter>
                        <asp:Parameter Type="String" Name="Remarks"></asp:Parameter>
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="shipmentSequenceId"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSourceTransportMode" TypeName="Shipment" SelectMethod="GetTransportModes" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceExternalCompany" TypeName="Shipment" SelectMethod="GetParticipants" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceNotificationMethod" TypeName="Shipment" SelectMethod="GetNotificationMethods" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
        TypeName="Priority">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
