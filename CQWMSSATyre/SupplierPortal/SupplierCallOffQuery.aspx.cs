using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;
using System.IO;

public partial class SupplierPortal_POQuery : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["MenuId"] == null)
                    Session["MenuId"] = 1;

                Session["FromDate"] = DateRange.GetFromDate();
                Session["ToDate"] = DateRange.GetToDate();

                rdpFromDate.SelectedDate = DateRange.GetFromDate();
                rdpToDate.SelectedDate = DateRange.GetToDate().AddDays(1);

                RadGridCallOffSearch.DataBind();
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["CallOffHeaderId"] != null)
                {
                    int callOffHeaderId = (int)Session["CallOffHeaderId"];

                    foreach (GridDataItem item in RadGridCallOffSearch.Items)
                    {
                        theErrMethod = "Page_LoadComplete";
                        if (item.GetDataKeyValue("CallOffHeaderId").ToString() == callOffHeaderId.ToString())
                        {
                            RadGridCallOffSearch.SelectedIndexes.Add(item.ItemIndex);
                            break;
                        }
                    }

                    if (RadGridCallOffSearch.SelectedIndexes.Count == 0)
                    {
                        RadGridCallOffSearch.SelectedIndexes.Clear();
                        Session["CallOffHeaderId"] = null;

                        RadGridLinked.DataBind();
                    }
                }
            }
        }
        catch { }
    }
    #endregion Page_LoadComplete

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            RadGridCallOffSearch.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region RadGridCallOffSearch_SelectedIndexChanged
    protected void RadGridCallOffSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridCallOffSearch_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in RadGridCallOffSearch.Items)
            {
                if (item.Selected)
                {
                    Session["CallOffHeaderId"] = item.GetDataKeyValue("CallOffHeaderId");
                    RadGridLinked.DataBind();
                    break;
                }
            }
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridCallOffSearch_SelectedIndexChanged

    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";
        try
        {
            foreach (GridDataItem item in RadGridLinked.Items)
            {
                if (item.Selected)
                    e.InputParameters["storageUnitId"] = (int)item.GetDataKeyValue("StorageUnitId");
            }
            

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ObjectDataSourceBatch_OnSelecting"

    #region ObjectDataSourceLinked_Selecting
    protected void ObjectDataSourceLinked_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceLinked_Selecting";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ObjectDataSourceLinked_Selecting

    #region RadGridLinked_SelectedIndexChanging
    protected void RadGridLinked_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        theErrMethod = "RadGridLinked_SelectedIndexChanging";

        try
        {
            foreach (GridDataItem item in RadGridLinked.Items)
            {
                if(item.Selected)
                    Session["StorageUnitId"] = item.GetDataKeyValue("StorageUnitId");
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridLinked_SelectedIndexChanging

    #region RadGridCallOffSearch_ItemCommand
    protected void RadGridCallOffSearch_ItemCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.SelectCommandName)
        {
            foreach (GridDataItem item in RadGridCallOffSearch.Items)
            {
                if (item.RowIndex == e.Item.RowIndex)
                {
                    Session["CallOffHeaderId"] = item.GetDataKeyValue("CallOffHeaderId");

                    Response.Redirect("~/SupplierPortal/CallOffMaintenance.aspx");
                    break;
                }
            }
        }
    }
    #endregion RadGridCallOffSearch_ItemCommand

    #region RadGridLinked_ItemCommand
    protected void RadGridLinked_ItemCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.SelectCommandName)
        {
            Shipment sh = new Shipment();
            string statusCode = "";

            foreach (GridDataItem item in RadGridLinked.Items)
            {
                if (item.RowIndex == e.Item.RowIndex)
                {
                    Session["CallOffDetailId"] = item.GetDataKeyValue("CallOffDetailId");
                    statusCode = item.GetDataKeyValue("StatusCode").ToString();

                    sh.UpdateOrderLineStatus(Session["ConnectionStringName"].ToString(), (int)Session["CallOffHeaderId"], (int)Session["CallOffDetailId"], statusCode);

                    RadGridLinked.DataBind();
                    break;
                }
            }
        }
    }
    #endregion RadGridLinked_ItemCommand

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region Render
    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(writer);
        Profile.Pet = new Pet();
        Profile.Pet.SavePetNames(RadGridCallOffSearch, "CallOffQueryRadGridCallOffSearch");
        Profile.Pet.SavePetNames(RadGridLinked, "CallOffQueryRadGridLinked");
    }
    #endregion Render

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
            if (Profile.Pet.Names.Count > 0)
            {
                /*This Grid is an AutoGeneratedColumn grid. At this point in the page life cycle the AutoGeneratedColumns array 
                is not initililized and cannot load the settings for the grid. There is a CreateColumnSet() method that I thought would work 
                to make the columns available at this point but this did not work. I also tried to put LoadSettings call in the PreRender event
                but this would occur after columns were made invisible thus making the columns visible again by loading the previous settings.
                The only place that you might be able to do this is on the ColumnCreated event or the ItemCreated event.	 
                For grid columns that are declaratively set at design time or added manually in the Page_Init event this will work perfectly.*/
                if (Profile.Pet.Names.ContainsKey("CallOffQueryRadGridCallOffSearch"))
                {
                    //RadGrid1.MasterTableView.GenerateColumnsCreateColumnSet(true);
                    GridSettings settings = new GridSettings(RadGridCallOffSearch);
                    settings.LoadSettings(Profile.Pet.Names["CallOffQueryRadGridCallOffSearch"].ToString());
                }

                /*Columns declared on this grid are done so at design time*/
                if (Profile.Pet.Names.ContainsKey("CallOffQueryRadGridLinked"))
                {
                    GridSettings settings = new GridSettings(RadGridLinked);
                    settings.LoadSettings(Profile.Pet.Names["CallOffQueryRadGridLinked"].ToString());
                }
            }
    }
    #endregion Page_Init
}