using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;

public partial class SupplierPortal_BayScheduler : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["ConnectionStringName"] == null)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            return;
        }
    }

    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Init";

        try
        {
            string fromDate = DateRange.GetFromDate().ToString();
            string toDate = DateRange.GetToDate().ToString();

            if (Session["FromDate"] == null)
                Session["FromDate"] = Convert.ToDateTime(fromDate);

            if (Session["ToDate"] == null)
                Session["ToDate"] = Convert.ToDateTime(toDate);


            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Default" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page Load

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            RadGridOrders.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region RadGridOrders_SelectedIndexChanged
    protected void RadGridOrders_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridOrders_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in RadGridOrders.SelectedItems)
            {
                    Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
                    Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");
                    RadGridPlan.DataBind();
                    RadGridResult.DataBind();
                    ObjectDataSourceVehicleLocation.DataBind();
                    RadSchedulerAllBays.Rebind();
                    break;
            }

            RadGridOrderLines.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridOrders_SelectedIndexChanged

    #region RadGridOrders_ItemCommand
    protected void RadGridOrders_ItemCommand(object source, GridCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "Redelivery")
            {
                Receiving r = new Receiving();

                foreach (GridDataItem item in RadGridOrders.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
                        Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

                        r.RedeliveryInsert(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"]);

                        RadGridOrders.DataBind();

                        break;
                    }
                }
            }
            if (e.CommandName == "Complete")
            {
                Status status = new Status();

                foreach (GridDataItem item in RadGridOrders.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
                        Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

                        if (status.InboundStatusChange(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"], -1, "RC"))
                        {
                            RadGridOrders.DataBind();

                            Master.MsgText = "Received"; Master.ErrorText = "";
                        }
                        else
                        {
                            result = SendErrorNow("Received" + "_" + "Error p_Receipt_Update_Status_Received");
                            Master.MsgText = "";
                            Master.ErrorText = "Exception, please make sure there are no default batches and there is a delivery note number";
                        }

                        break;
                    }
                }
            }
            if (e.CommandName == "DeliveryConformance")
            {
                foreach (GridDataItem item in RadGridOrders.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
                        Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

                        Session["Sequence"] = 0;
                        Session["QuestionaireId"] = 5;
                        Session["QuestionaireType"] = "Supplier Load Conformance";

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Common/Question.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=200,height=200,top=100,left=100');",
                                                            true);
                        break;
                    }
                }
            }
            if (e.CommandName == "CheckSheet")
            {
                foreach (GridDataItem item in RadGridOrders.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
                        Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

                        Session["FromURL"] = "~/Reports/ReceivingCheckSheet.aspx";

                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
                            Session["ReportName"] = "Receiving Check Sheet Batches";
                        else
                            Session["ReportName"] = "Receiving Check Sheet";

                        ReportParameter[] RptParameters = new ReportParameter[7];

                        // Create the ConnectionString report parameter
                        string strReportConnectionString = Session["ReportConnectionString"].ToString();
                        RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

                        // Create the InboundShipmentId report parameter
                        string inboundShipmentId = Session["InboundShipmentId"].ToString();

                        if (inboundShipmentId == "")
                            inboundShipmentId = "-1";

                        RptParameters[1] = new ReportParameter("InboundShipmentId", inboundShipmentId);

                        // Create the ReceiptId report parameter
                        string receiptId = Session["ReceiptId"].ToString();
                        RptParameters[2] = new ReportParameter("ReceiptId", receiptId);

                        // Create the ReceiptLineId report parameter
                        RptParameters[3] = new ReportParameter("ReceiptLineId", "-1");

                        // Create the ServerName report parameter
                        RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                        // Create the DatabaseName report parameter
                        RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        // Create the UserName report parameter
                        RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                        break;
                    }
                }
            }
            if (e.CommandName == "PrintGRN")
            {

                foreach (GridDataItem item in RadGridOrders.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
                        Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

                        Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
                        Session["ReportName"] = "Goods Received Note";

                        ReportParameter[] RptParameters = new ReportParameter[5];

                        // Create the ConnectionString report parameter
                        //string strReportConnectionString = Session["ReportConnectionString"].ToString();
                        //RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

                        // Create the InboundShipmentId report parameter
                        string inboundShipmentId = Session["InboundShipmentId"].ToString();
                        RptParameters[0] = new ReportParameter("InboundShipmentId", inboundShipmentId);

                        // Create the ReceiptId report parameter
                        string receiptId = Session["ReceiptId"].ToString();
                        RptParameters[1] = new ReportParameter("ReceiptId", receiptId);

                        RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                        RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Received" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridOrders_ItemCommand

    #region RadGridOrders_RowDataBound
    protected void RadGridOrders_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex != -1)
        {
            if (e.Row.Cells[09].Text == "On Hold")
            {
                e.Row.Cells[0].Enabled = false;
            }
        }
    }
    #endregion

    #region ObjectDataSourcePackaging_OnSelecting
    protected void ObjectDataSourcePackaging_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourcePackaging_OnSelecting";

        try
        {
            //   e.InputParameters["inboundShipmentId"] = (int)Session["InboundShipmentId"];
            e.InputParameters["receiptId"] = (int)Session["ReceiptId"];
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourcePackaging_OnSelecting"

    #region ObjectDataSourceReceiptLine_OnSelecting
    protected void ObjectDataSourceReceiptLine_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceReceiptLine_OnSelecting";

        try
        {
            e.InputParameters["inboundShipmentId"] = (int)Session["InboundShipmentId"];
            e.InputParameters["receiptId"] = (int)Session["ReceiptId"];
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceReceiptLine_OnSelecting"

    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";

        try
        {
            e.InputParameters["storageUnitId"] = (int)Session["StorageUnitId"];

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceBatch_OnSelecting"

    #region ObjectDataSourceReceiptLine_OnUpdating
    protected void ObjectDataSourceReceiptLine_OnUpdating(object sender, ObjectDataSourceMethodEventArgs e)
    {

        theErrMethod = "ObjectDataSourceReceiptLine_OnUpdating";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];

            try { e.InputParameters["storageUnitBatchId"] = (int)Session["StorageUnitBatchId"]; }
            catch { e.InputParameters["storageUnitBatchId"] = -1; }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ObjectDataSourceReceiptLine_OnUpdating"

    #region ObjectDataSourceReceiptLine_Updated
    protected void ObjectDataSourceReceiptLine_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        switch ((int)e.ReturnValue)
        {
            case -1:
                Master.MsgText = "";
                Master.ErrorText = "Over receipt not allowed.";
                break;
            case -2:
                Master.MsgText = "";
                Master.ErrorText = "Default batch not allowed.";
                break;
            case -3:
                Master.MsgText = "";
                Master.ErrorText = "You cannot change quantities after a redelivery has been requested.";
                break;
            default:
                Master.MsgText = "Update successful.";
                Master.ErrorText = "";
                break;
        }
    }
    #endregion ObjectDataSourceReceiptLine_Updated

    #region RadGridOrderLines_ItemCommand
    protected void RadGridOrderLines_ItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "CheckSheet")
            {
                foreach (GridDataItem item in RadGridOrderLines.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["ReceiptLineId"] = item.GetDataKeyValue("ReceiptLineId");

                        Session["FromURL"] = "~/Reports/ReceivingCheckSheet.aspx";

                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
                            Session["ReportName"] = "Receiving Check Sheet Batches";
                        else
                            Session["ReportName"] = "Report Receiving Check Sheet With Capture Sheets";

                        ReportParameter[] RptParameters = new ReportParameter[7];

                        // Create the ConnectionString report parameter
                        string strReportConnectionString = Session["ReportConnectionString"].ToString();
                        RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

                        // Create the InboundShipmentId report parameter
                        string inboundShipmentId = Session["InboundShipmentId"].ToString();

                        if (inboundShipmentId == "")
                            inboundShipmentId = "-1";

                        RptParameters[1] = new ReportParameter("InboundShipmentId", inboundShipmentId);

                        // Create the ReceiptId report parameter
                        string receiptId = Session["ReceiptId"].ToString();
                        RptParameters[2] = new ReportParameter("ReceiptId", receiptId);

                        // Create the ReceiptLineId report parameter
                        string receiptLineId = Session["ReceiptLineId"].ToString();
                        RptParameters[3] = new ReportParameter("ReceiptLineId", receiptLineId);

                        // Create the ServerName report parameter
                        RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                        // Create the DatabaseName report parameter
                        RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        // Create the UserName report parameter
                        RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                        break;
                    }
                }
            }
            if (e.CommandName == "SerialNumber")
            {
                //Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
                //Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

                //Response.Redirect("RegisterSerialNumber.aspx");

                //Master.MsgText = "Link"; Master.ErrorText = "";
                foreach (GridDataItem item in RadGridOrderLines.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["ReceiptLineId"] = item.GetDataKeyValue("ReceiptLineId");

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Inbound/RegisterSerialNumber.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=400,height=525,top=100,left=100');",
                                                            true);
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Received" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridOrderLines_ItemCommand

    #region RadGridOrderLines_EditCommand
    protected void RadGridOrderLines_EditCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridOrderLines.Items)
            {
                if (item.RowIndex == e.Item.RowIndex)
                {
                    Session["StorageUnitId"] = item.GetDataKeyValue("StorageUnitId");
                    ObjectDataSourceBatch.DataBind();
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Received" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridOrderLines_EditCommand

    #region ButtonPrintLabel_Click
    protected void ButtonPrintLabel_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();
            int quantity = 1;
            string nop;
            int numberToPrint;
            string rli;
            bool verifyLabel = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 159);
            bool useReceiptLineId = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 178);
            bool customerLabel = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 320);
            bool utiLabel = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 431);

            foreach (GridDataItem item in RadGridOrderLines.SelectedItems)
            {
                if (verifyLabel || useReceiptLineId)
                    checkedList.Add((int)item.GetDataKeyValue("ReceiptLineId"));
                else
                    checkedList.Add((int)item.GetDataKeyValue("BatchId"));

                quantity = Convert.ToInt32(item.GetDataKeyValue("Boxes"));

                Exceptions ex = new Exceptions();
                rli = ((HiddenField)item.FindControl("HiddenReceiptLineId")).Value;
                Session["rli"] = int.Parse(rli);
                int count = ex.CountRedPrint(Session["ConnectionStringName"].ToString(), (int)Session["rli"]);
                nop = ((HiddenField)item.FindControl("HiddenNumberOfPallets")).Value;
                numberToPrint = int.Parse(nop) - count;

                if (TextBoxQty.Text != "")
                    int.TryParse(TextBoxQty.Text, out quantity);

                if (quantity > numberToPrint)
                {
                    Session["LabelCopies"] = numberToPrint;
                    Master.MsgText = "";
                    Master.ErrorText = "Some lines exceeded number of allowed labels - defaulted to number of pallets";
                    if (numberToPrint < 1)
                    {
                        Master.MsgText = "";
                        Master.ErrorText = "Maximum number of labels have already been printed";
                        return;
                    }
                    //break;
                }
                else
                {
                    Session["LabelCopies"] = quantity;
                }

                if (ex.CreateException(Session["ConnectionStringName"].ToString(), (int)Session["rli"], (int)Session["LabelCopies"], (int)Session["OperatorId"]) == false)
                {
                    return;
                }

            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            if (utiLabel)
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 432))
                    Session["LabelName"] = "UTI Product Label Small.lbl";
                else
                    Session["LabelName"] = "UTI Product Label Large.lbl";
            else
                if (customerLabel)
                    Session["LabelName"] = "Customer Product Label Small.lbl";
                else
                    if (verifyLabel)
                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 199))
                            Session["LabelName"] = "Receiving Batch Label Small.lbl";
                        else
                            Session["LabelName"] = "Receiving Batch Label Large.lbl";
                    else
                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
                            Session["LabelName"] = "Product Batch Label Small.lbl";
                        else
                            Session["LabelName"] = "Product Batch Label Large.lbl";

            Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                //Response.Redirect("~/Common/NLPrint.aspx");
                Session["LabelClose"] = true;
                ScriptManager.RegisterStartupScript(this.Page,
                                                    this.Page.GetType(),
                                                    "newWindow",
                                                    "window.open('../Common/Label.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=200,height=200,top=100,left=100');",
                                                    true);
            }
        }
        catch { }
    }
    #endregion ButtonPrintLabel_Click

    #region ButtonTicketLabel_Click
    protected void ButtonTicketLabel_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();
            int quantity = 1;

            foreach (GridDataItem item in RadGridOrderLines.Items)
            {
                if (item.Selected)
                {
                    checkedList.Add((int)item.GetDataKeyValue("StorageUnitId") * -1);
                    quantity = Convert.ToInt32(item.GetDataKeyValue("Boxes"));
                }
            }

            if (TextBoxTicket.Text != "")
                if (!int.TryParse(TextBoxTicket.Text, out quantity))
                    quantity = 1;

            Session["LabelCopies"] = quantity;

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            Session["LabelName"] = "Moresport Ticket.lbl";

            Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                //Response.Redirect("~/Common/NLPrint.aspx");
                Session["LabelClose"] = true;
                ScriptManager.RegisterStartupScript(this.Page,
                                                    this.Page.GetType(),
                                                    "newWindow",
                                                    "window.open('../Common/Label.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=200,height=200,top=100,left=100');",
                                                    true);
            }

        }
        catch { }
    }
    #endregion ButtonTicketLabel_Click

    #region ButtonPrintSample_Click
    protected void ButtonPrintSample_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();
            int quantity = 1;
            bool useReceiptLineId = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 178);

            foreach (GridDataItem item in RadGridOrderLines.Items)
            {
                if (item.Selected)
                {
                    if (useReceiptLineId)
                        checkedList.Add((int)item.GetDataKeyValue("ReceiptLineId"));
                    else
                        checkedList.Add((int)item.GetDataKeyValue("BatchId"));

                    quantity = Convert.ToInt32(item.GetDataKeyValue("Boxes"));
                }
            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
                Session["LabelName"] = "Sample Label Small.lbl";
            else
                Session["LabelName"] = "Sample Label Large.lbl";

            Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
            Session["ActiveTabIndex"] = int.Parse(Tabs.SelectedIndex.ToString());

            if (TextBoxSample.Text != "")
                if (int.TryParse(TextBoxSample.Text, out quantity))
                    quantity = 1;

            Session["LabelCopies"] = quantity;

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                //Response.Redirect("~/Common/NLPrint.aspx");
                Session["LabelClose"] = true;
                ScriptManager.RegisterStartupScript(this.Page,
                                                    this.Page.GetType(),
                                                    "newWindow",
                                                    "window.open('../Common/Label.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=200,height=200,top=100,left=100');",
                                                    true);
            }
        }
        catch { }
    }
    #endregion ButtonPrintSample_Click

    #region RadSchedulerAllBays_AppointmentDataBound
    protected void RadSchedulerAllBays_AppointmentDataBound(object sender, SchedulerEventArgs e)
    {
        //rsCategoryDarkBlue
        //rsCategoryBlue
        //rsCategoryDarkGreen
        //rsCategoryGreen
        //rsCategoryDarkRed
        //rsCategoryOrange
        //rsCategoryPink
        //rsCategoryRed
        //rsCategoryViolet
        //rsCategoryYellow

        if (e.Appointment.Description == "Confirmed")
            e.Appointment.CssClass = "rsCategoryGreen";
        
        if (e.Appointment.Description == "Unconfirmed")
            e.Appointment.CssClass = "rsCategoryBlue";

        if (e.Appointment.Description == "Late")
            e.Appointment.CssClass = "rsCategoryRed";
    }
    #endregion RadSchedulerAllBays_AppointmentDataBound

    #region RadGridPlan_SelectedIndexChanged
    protected void RadGridPlan_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridPlan_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in RadGridPlan.Items)
            {
                if (item.Selected)
                {
                    Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
                    Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

                    RadGridResult.DataBind();
                    break;
                }
            }
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_Scheduler" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridPlan_SelectedIndexChanged

    #region RadGridResult_SelectedIndexChanged
    protected void RadGridResult_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridResult_SelectedIndexChanged";
        try
        {
            DockSchedule ds = new DockSchedule();

            foreach (GridDataItem item in RadGridResult.Items)
            {
                if (item.Selected)
                {
                    Session["InboundShipmentId"] = int.Parse(item.GetDataKeyValue("InboundShipmentId").ToString());
                    Session["InstructionTypeId"] = int.Parse(item.GetDataKeyValue("InstructionTypeId").ToString());
                    Session["OperatorGroupId"] = int.Parse(item.GetDataKeyValue("OperatorGroupId").ToString());

                    if (!ds.AssignTeam(Session["ConnectionStringName"].ToString(),
                                     (int)Session["InboundShipmentId"],
                                     (int)Session["InstructionTypeId"],
                                     (int)Session["OperatorGroupId"]))
                    {
                        throw new Exception("AssignTeam Failed.");
                    }
                }
            }
            RadGridPlan.DataBind();
            RadGridResult.DataBind();
            //RadGridResult.SelectedIndexes.Clear();
            //foreach (GridDataItem item in RadGridResult.Items)
            //{
            //    if (item.GetDataKeyValue("Planned").ToString().ToLower() == "true")
            //    {
            //        RadGridResult.SelectedIndexes.Add(item.ItemIndex);
            //    }
            //}
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_Scheduler" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridResult_SelectedIndexChanged

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();
                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "Default", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "Default", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

    #region Render
    protected override void Render(HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);
            SavePetNames();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Render

    #region SavePetNames
    protected void SavePetNames()
    {
        try
        {
            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridOrders);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrders, "ReceivingDocumentRadGridOrders", (int)Session["OperatorId"]);
            }
            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridOrderLines);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrderLines, "ReceivingDocumentRadGridOrderLines", (int)Session["OperatorId"]);
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion SavePetNames

    #region ButtonSaveSettings_Click
    protected void ButtonSaveSettings_Click(object sender, EventArgs e)
    {
        try
        {
            SavePetNames();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSaveSettings_Click

    #region DropDownListBatch_OnSelectedIndexChanged
    protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddl = (DropDownList)sender;
            Session["StorageUnitBatchId"] = int.Parse(ddl.SelectedValue);
        }
        catch { }
    }
    #endregion "DropDownListBatch_OnSelectedIndexChanged"

    #region ButtonDefaultDelivery_Click
    protected void ButtonDefaultDelivery_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDefaultDelivery_Click";
        try
        {
            Receiving r = new Receiving();

            r.SetDefaultDeliveryNoteQty(Session["ConnectionStringName"].ToString(), (int)Session["InboundShipmentId"], (int)Session["ReceiptId"]);

            this.RadGridOrderLines.DataBind();

            Master.MsgText = "Delivery"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonDefaultDelivery_Click"
}
