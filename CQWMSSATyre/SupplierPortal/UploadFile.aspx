<%@ Page Language="C#" MasterPageFile="~/MasterPages/Empty.master" AutoEventWireup="true" CodeFile="UploadFile.aspx.cs" Inherits="SupplierPortal_UploadFile" 
    Title="<%$ Resources:Default, Upload %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ MasterType VirtualPath="~/MasterPages/Empty.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:FileUpload ID="fileUploadDocument" runat="server" /> 
    <telerik:RadButton ID="ButtonUpload" runat="server" Text="Upload" onclick="ButtonUpload_Click"></telerik:RadButton>
    <br />
    <br />
    <telerik:RadGrid ID="RadGridFiles" runat="server" DataSourceID="ObjectDataSourceFiles"
        AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" Skin="Metro"
        OnItemCommand="RadGridFiles_ItemCommand" OnItemCreated="RadGridFiles_ItemCreated">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView DataKeyNames="SavedFileId">
            <Columns>
                <%--<telerik:GridButtonColumn DataTextField="Download" CommandName="Download" Text='<%$ Resources:Default, Download %>'></telerik:GridButtonColumn>--%>
                <telerik:GridAttachmentColumn DataSourceID="ObjectDataSourceCallOffSequence" MaxFileSize="1048576"
                    EditFormHeaderTextFormat="Upload File:" HeaderText="<%$ Resources:Default, Download %>" AttachmentDataField="Data"
                    AttachmentKeyFields="SavedFileId" FileNameTextField="SavedFile" DataTextField="SavedFile"
                    UniqueName="AttachmentColumn">
                </telerik:GridAttachmentColumn>
                <telerik:GridBoundColumn DataField="SavedFileType" HeaderText="Type"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CreateDate" HeaderText="CreateDate"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Resizing AllowColumnResize="True" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
    </telerik:RadGrid>
    <asp:ObjectDataSource ID="ObjectDataSourceFiles" runat="server" TypeName="CallOff"
        SelectMethod="SelectFiles">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="UploadTableId" Type="Int32" SessionField="UploadTableId" />
            <asp:SessionParameter Name="UploadTableName" Type="String" SessionField="UploadTableName" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <%--<br />
    <telerik:RadButton ID="ButtonDownload" runat="server" Text="Download" onclick="ButtonDownload_Click"></telerik:RadButton>--%>
    <br />
    <telerik:RadButton ID="ButtonClose" runat="server" Text="<%$ Resources:Default, Close %>" OnClick="ButtonClose_Click"></telerik:RadButton>
</asp:Content>