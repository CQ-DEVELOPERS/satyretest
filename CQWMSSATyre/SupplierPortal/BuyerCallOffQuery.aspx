<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="BuyerCallOffQuery.aspx.cs" Inherits="SupplierPortal_CallOffQuery"
    Title="<%$ Resources:Default, CallOffQueryTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, CallOffQueryTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, CallOffQueryAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <%--<telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="Textbox" />--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Thumbnail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridCallOffSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinked" />
                    <telerik:AjaxUpdatedControl ControlID="FormViewCallOffHeader" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridComments" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSequence" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridLinked">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinked" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridCallOffSequence">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSequence" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridComments">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridComments" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSelect" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridComments" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Search %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Lines %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Comments %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Shipping %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                <table>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtcallOffNumber" runat="server" Label="Call Off No:" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtxtOrderNumber" runat="server" Label="Order No:" Width="250px"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtProductCode" runat="server" Label="Product Code:" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtxtProduct" runat="server" Label="Description:" Width="250px"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadTextBox ID="rtxtSupplierCode" runat="server" Label="Supplier Code:" Width="250px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="rtxtSupplier" runat="server" Label="Supplier Name:" Width="250px"></telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>" Width="100px"></asp:Label>
                            <telerik:RadDatePicker ID="rdpFromDate" runat="server"></telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:Label ID="labeToDate" runat="server" Text="<%$ Resources:Default, ToDate %>" Width="100px"></asp:Label>
                            <telerik:RadDatePicker ID="rdpToDate" runat="server"></telerik:RadDatePicker>
                        </td>
                        <td>
                            <telerik:RadButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"></telerik:RadButton>
                        </td>
                    </tr>
                </table>
                <telerik:RadGrid ID="RadGridCallOffSearch" runat="server" OnRowDataBound="RadGridCallOffSearch_RowDataBound"
                    AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true"
                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePlanning"
                    OnSelectedIndexChanged="RadGridCallOffSearch_SelectedIndexChanged" PageSize="30" ShowGroupPanel="True" Height="500px" Skin="Metro"
                    OnItemCommand="RadGridCallOffSearch_ItemCommand">
                    <ExportSettings IgnorePaging="True" OpenInNewWindow="True">
                        <Pdf PageHeight="210mm" PageWidth="297mm" PageTitle="SushiBar menu" DefaultFontFamily="Arial Unicode MS"
                            PageBottomMargin="20mm" PageTopMargin="20mm" PageLeftMargin="20mm" PageRightMargin="20mm">
                        </Pdf>
                    </ExportSettings>
                    <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                    <MasterTableView DataKeyNames="CallOffHeaderId,CallOffDetailId" DataSourceID="ObjectDataSourcePlanning"
                        CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" FilterControlAltText="Filter EditCommandColumn column"></telerik:GridEditCommandColumn>
                            <telerik:GridButtonColumn DataTextField="CallOffNumber" Text="<%$ Resources:Default, Upload %>" HeaderText="<%$ Resources:Default, CallOffNumber %>" CommandName="Select"></telerik:GridButtonColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="PurchaseOrder" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="PurchaseOrder" UniqueName="PurchaseOrder"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="LineNumber" HeaderText="<%$ Resources:Default, LineNumber %>" SortExpression="LineNumber" UniqueName="LineNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="CommunicationUpdate" HeaderText="<%$ Resources:Default, CommunicationUpdate %>" SortExpression="CommunicationUpdate" UniqueName="CommunicationUpdate"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="RequiredDate" HeaderText="<%$ Resources:Default, RequiredDate %>" SortExpression="RequiredDate" UniqueName="RequiredDate"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" UniqueName="Status"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority" UniqueName="Priority"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="TransportMode" HeaderText="<%$ Resources:Default, TransportMode %>" SortExpression="TransportMode" UniqueName="TransportMode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ExternalCompany" HeaderText="<%$ Resources:Default, ExternalCompany %>" SortExpression="ExternalCompany" UniqueName="ExternalCompany"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" UniqueName="ProductCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product" UniqueName="Product"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" UniqueName="SKUCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>" SortExpression="OrderQuantity" UniqueName="OrderQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Remainder" HeaderText="<%$ Resources:Default, Remainder %>" SortExpression="Remainder" UniqueName="Remainder"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RequestQuantity" HeaderText="<%$ Resources:Default, RequestQuantity %>" SortExpression="RequestQuantity" UniqueName="RequestQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="SupplierConfirmedQuantity" HeaderText="<%$ Resources:Default, SupplierConfirmedQuantity %>" SortExpression="SupplierConfirmedQuantity" UniqueName="SupplierConfirmedQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ReceivedQuantity" HeaderText="<%$ Resources:Default, ReceivedQuantity %>" SortExpression="ReceivedQuantity" UniqueName="ReceivedQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ShipmentNumber" HeaderText="<%$ Resources:Default, ShipmentNumber %>" SortExpression="ShipmentNumber" UniqueName="ShipmentNumber"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                        ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                        AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        <Resizing AllowColumnResize="True" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="CallOff"
                    SelectMethod="SearchOrderQuery" UpdateMethod="UpdateOrderQuery">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:ControlParameter Name="CallOffNumber" ControlID="rtxtCallOffNumber" />
                        <asp:ControlParameter Name="OrderNumber" ControlID="rtxtOrderNumber" />
                        <asp:ControlParameter Name="ProductCode" ControlID="rtxtProductCode" />
                        <asp:ControlParameter Name="Product" ControlID="rtxtProduct" />
                        <asp:ControlParameter Name="ExternalCompanyCode" ControlID="rtxtSupplierCode" />
                        <asp:ControlParameter Name="ExternalCompany" ControlID="rtxtSupplier" />
                        <asp:ControlParameter Name="FromDate" ControlID="rdpFromDate" />
                        <asp:ControlParameter Name="ToDate" ControlID="rdpToDate" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="callOffDetailId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="requestQuantity" Type="Decimal"></asp:Parameter>
                    </UpdateParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel2">
                <asp:FormView ID="FormViewCallOffHeader" runat="server" AutoGenerateRows="false"
                     DataSourceID="ObjectDataSourceCallOffDetailsView" DataKeyNames="CallOffHeaderId" >
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <telerik:RadTextBox ID="LabelCallOffNumber" runat="server" Label="<%$ Resources:Default, CallOffNumber %>" Text='<%# Bind("CallOffNumber") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="LabelTransportMode" runat="server" Label="<%$ Resources:Default, TransportMode %>" Text='<%# Bind("TransportMode") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadTextBox ID="LabelWarehouse" runat="server" Label="<%$ Resources:Default, Warehouse %>" Text='<%# Bind("Warehouse") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="LabelNotificationMethod" runat="server" Label="<%$ Resources:Default, NotificationMethod %>" Text='<%# Bind("NotificationMethod") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadTextBox ID="LabelPriority" runat="server" Label="<%$ Resources:Default, Priority %>" Text='<%# Bind("Priority") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="LabelRequiredDate" runat="server" Label="<%$ Resources:Default, RequiredDate %>" Text='<%# Bind("RequiredDate") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadTextBox ID="LabelConfirmAccpeted" runat="server" Label="<%$ Resources:Default, ConfirmAccpeted %>" Text='<%# Bind("ConfirmAccpeted") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="LabelConfirmReceipt" runat="server" Label="<%$ Resources:Default, ConfirmReceipt %>" Text='<%# Bind("ConfirmReceipt") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>
                                <%--<td>
                                    <telerik:RadTextBox ID="LabelStatus" runat="server" Label="<%$ Resources:Default, Status %>" Text='<%# Bind("Status") %>' ReadOnly="true" Width="250px"></telerik:RadTextBox>
                                </td>--%>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:FormView>
                <asp:ObjectDataSource ID="ObjectDataSourceCallOffDetailsView" runat="server" TypeName="CallOff" SelectMethod="SelectOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Type="Int32" Name="CallOffHeaderId" SessionField="CallOffHeaderId"></asp:SessionParameter>
                    </SelectParameters>
                </asp:ObjectDataSource>
                <telerik:RadGrid ID="RadGridLinked" runat="server" DataSourceID="ObjectDataSourceLinked" 
                    AllowAutomaticUpdates="true" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" Skin="Metro"
                    OnItemCommand="RadGridLinked_ItemCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="CallOffDetailId,StatusCode">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" FilterControlAltText="Filter EditCommandColumn column"></telerik:GridEditCommandColumn>
                            <telerik:GridButtonColumn DataTextField="CallOffNumber" Text="<%$ Resources:Default, Upload %>" HeaderText="<%$ Resources:Default, CallOffNumber %>" CommandName="Select"></telerik:GridButtonColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="PurchaseOrder" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="PurchaseOrder" UniqueName="PurchaseOrder"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="LineNumber" HeaderText="<%$ Resources:Default, LineNumber %>" SortExpression="LineNumber" UniqueName="LineNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="RequiredDate" HeaderText="<%$ Resources:Default, RequiredDate %>" SortExpression="RequiredDate" UniqueName="RequiredDate"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" UniqueName="ProductCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product" UniqueName="Product"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" UniqueName="SKUCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>" SortExpression="OrderQuantity" UniqueName="OrderQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Remainder" HeaderText="<%$ Resources:Default, Remainder %>" SortExpression="Remainder" UniqueName="Remainder"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="RequestQuantity" HeaderText="<%$ Resources:Default, RequestQuantity %>" SortExpression="RequestQuantity" UniqueName="RequestQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="SupplierConfirmedQuantity" HeaderText="<%$ Resources:Default, SupplierConfirmedQuantity %>" SortExpression="SupplierConfirmedQuantity" UniqueName="SupplierConfirmedQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ShipmentNumber" HeaderText="<%$ Resources:Default, ShipmentNumber %>" SortExpression="ShipmentNumber" UniqueName="ShipmentNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ShipmentDate" HeaderText="<%$ Resources:Default, ShipmentDate %>" SortExpression="ShipmentDate" UniqueName="ShipmentDate"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="DeliveryAdvice" HeaderText="<%$ Resources:Default, DeliveryAdvice %>" SortExpression="DeliveryAdvice" UniqueName="DeliveryAdvice"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" UniqueName="Status"></telerik:GridBoundColumn>
                            <telerik:GridButtonColumn DataTextField="NextStatus" HeaderText="<%$ Resources:Default, NextStatus %>" CommandName="Select"></telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceLinked" runat="server" TypeName="CallOff"
                    SelectMethod="SearchQueryOrderLine">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="CallOffHeaderId" Type="Int32" SessionField="CallOffHeaderId" DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel3">
                <telerik:RadGrid ID="RadGridComments" runat="server" DataSourceID="ObjectDataSourceComents"
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" AllowAutomaticInserts="true" Skin="Metro">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="CommentId"
                        CommandItemDisplay="Top" DataSourceID="ObjectDataSourceComents"
                        AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridBoundColumn DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" ReadOnly="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>" ReadOnly="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Comment" HeaderText="<%$ Resources:Default, Comments %>"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceComents" runat="server" TypeName="CallOff"
                    SelectMethod="SelectComment" InsertMethod="InsertComment" UpdateMethod="UpdateComment" DeleteMethod="DeleteComment">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="callOffHeaderId" Type="Int32" SessionField="callOffHeaderId" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="callOffHeaderId" Type="Int32" SessionField="callOffHeaderId" />
                        <asp:Parameter Type="String" Name="comment"></asp:Parameter>
                        <asp:SessionParameter Type="Int32" Name="operatorId" SessionField="OperatorId"></asp:SessionParameter>
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="commentId"></asp:Parameter>
                        <asp:Parameter Type="String" Name="comment"></asp:Parameter>
                        <asp:SessionParameter Type="Int32" Name="operatorId" SessionField="OperatorId"></asp:SessionParameter>
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="commentId"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel4">
                <telerik:RadGrid ID="RadGridCallOffSequence" runat="server" DataSourceID="ObjectDataSourceCallOffSequence" OnSelectedIndexChanged="RadGridCallOffSequence_SelectedIndexChanged"
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" Skin="Metro"
                    OnItemCommand="RadGridCallOffSequence_ItemCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="CallOffSequenceId"
                        Width="100%" CommandItemDisplay="Top" DataSourceID="ObjectDataSourceCallOffSequence"
                        AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridEditCommandColumn InsertText="<%$ Resources:Default, Insert %>" EditText="<%$ Resources:Default, Edit %>" UpdateText="<%$ Resources:Default, Update %>" CancelText="<%$ Resources:Default, Cancel %>"></telerik:GridEditCommandColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListTransportModeId" 
                                ListTextField="TransportMode" ListValueField="TransportModeId"
                                DataSourceID="ObjectDataSourceTransportMode" DataField="TransportModeId" 
                                HeaderText="<%$ Resources:Default, TransportMode %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListExternalCompanyId" 
                                ListTextField="ExternalCompany" ListValueField="ExternalCompanyId"
                                DataSourceID="ObjectDataSourceExternalCompany" DataField="ExternalCompanyId" 
                                HeaderText="<%$ Resources:Default, Participant %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListNotificationMethodId"
                                ListTextField="NotificationMethod" ListValueField="NotificationMethodId"
                                DataSourceID="ObjectDataSourceNotificationMethod" DataField="NotificationMethodId" 
                                HeaderText="<%$ Resources:Default, NotificationMethod %>">
                            </telerik:GridDropDownColumn>
                            <telerik:GridDateTimeColumn DataField="CollectionDate" HeaderText="<%$ Resources:Default, CollectionDate %>"></telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn DataField="ReceivedDate" HeaderText="<%$ Resources:Default, ReceivedDate %>"></telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn DataField="DespatchDate" HeaderText="<%$ Resources:Default, DespatchedDate %>"></telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn DataField="PlannedDelivery" HeaderText="<%$ Resources:Default, PlannedDeliveryDate %>"></telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>"></telerik:GridBoundColumn>
                            <telerik:GridButtonColumn Text="<%$ Resources:Default, Upload %>" HeaderText="<%$ Resources:Default, Upload %>" CommandName="Select"></telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceCallOffSequence" runat="server" TypeName="CallOff"
                    SelectMethod="SelectSequence" InsertMethod="InsertSequence" UpdateMethod="UpdateSequence" DeleteMethod="DeleteSequence">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="callOffHeaderId" Type="Int32" SessionField="CallOffHeaderId" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="CallOffSequenceId"></asp:Parameter>
                        <asp:SessionParameter Name="callOffHeaderId" Type="Int32" SessionField="CallOffHeaderId" />
                        <asp:Parameter Type="Int32" Name="ExternalCompanyId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="TransportModeId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="NotificationMethodId"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="CollectionDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="ReceivedDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="DespatchDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="PlannedDelivery"></asp:Parameter>
                        <asp:Parameter Type="String" Name="Remarks"></asp:Parameter>
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="CallOffSequenceId"></asp:Parameter>
                        <asp:SessionParameter Name="callOffHeaderId" Type="Int32" SessionField="CallOffHeaderId" />
                        <asp:Parameter Type="Int32" Name="ExternalCompanyId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="TransportModeId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="NotificationMethodId"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="CollectionDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="ReceivedDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="DespatchDate"></asp:Parameter>
                        <asp:Parameter Type="DateTime" Name="PlannedDelivery"></asp:Parameter>
                        <asp:Parameter Type="String" Name="Remarks"></asp:Parameter>
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="CallOffSequenceId"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSourceTransportMode" TypeName="CallOff" SelectMethod="GetTransportModes" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceExternalCompany" TypeName="CallOff" SelectMethod="GetParticipants" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceNotificationMethod" TypeName="CallOff" SelectMethod="GetNotificationMethods" runat="server">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
