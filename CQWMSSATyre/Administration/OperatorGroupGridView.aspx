<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OperatorGroupGridView.aspx.cs" Inherits="Administration_OperatorGroupGridView" Title="OperatorGroup" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/OperatorGroupGridView.ascx" TagName="OperatorGroupGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="OperatorGroup Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="OperatorGroup">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="OperatorGroup" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:OperatorGroupGridView ID="OperatorGroupGridView1" runat="server" />
</asp:Content>
