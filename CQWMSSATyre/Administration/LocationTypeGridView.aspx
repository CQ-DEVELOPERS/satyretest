<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LocationTypeGridView.aspx.cs" Inherits="Administration_LocationTypeGridView" Title="LocationType" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/LocationTypeGridView.ascx" TagName="LocationTypeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="LocationType Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="LocationType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="LocationType" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:LocationTypeGridView ID="LocationTypeGridView1" runat="server" />
</asp:Content>
