<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InterfaceMasterFileGridView.aspx.cs" Inherits="Administration_InterfaceMasterFileGridView" Title="InterfaceMasterFile" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/InterfaceMasterFileGridView.ascx" TagName="InterfaceMasterFileGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="InterfaceMasterFile Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="InterfaceMasterFile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceMasterFile" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:InterfaceMasterFileGridView ID="InterfaceMasterFileGridView1" runat="server" />
</asp:Content>
