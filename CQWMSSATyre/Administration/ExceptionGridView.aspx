<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExceptionGridView.aspx.cs" Inherits="Administration_ExceptionGridView" Title="Exception" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/ExceptionGridView.ascx" TagName="ExceptionGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="Exception Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:ExceptionGridView ID="ExceptionGridView1" runat="server" />
</asp:Content>
