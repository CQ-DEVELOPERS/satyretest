<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransportModeGridView.aspx.cs" Inherits="Administration_TransportModeGridView" Title="TransportMode" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/TransportModeGridView.ascx" TagName="TransportModeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="TransportMode Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:TransportModeGridView ID="TransportModeGridView1" runat="server" />
</asp:Content>
