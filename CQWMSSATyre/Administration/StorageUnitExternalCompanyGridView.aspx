<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StorageUnitExternalCompanyGridView.aspx.cs" Inherits="Administration_StorageUnitExternalCompanyGridView" Title="StorageUnitExternalCompany" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/StorageUnitExternalCompanyGridView.ascx" TagName="StorageUnitExternalCompanyGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="StorageUnitExternalCompany Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="StorageUnitExternalCompany">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="StorageUnitExternalCompany" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:StorageUnitExternalCompanyGridView ID="StorageUnitExternalCompanyGridView1" runat="server" />
</asp:Content>
