<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LabelingCategoryGridView.aspx.cs" Inherits="Administration_LabelingCategoryGridView" Title="LabelingCategory" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/LabelingCategoryGridView.ascx" TagName="LabelingCategoryGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="LabelingCategory Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="LabelingCategory">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="LabelingCategory" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:LabelingCategoryGridView ID="LabelingCategoryGridView1" runat="server" />
</asp:Content>
