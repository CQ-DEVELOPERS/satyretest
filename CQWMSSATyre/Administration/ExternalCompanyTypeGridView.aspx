<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExternalCompanyTypeGridView.aspx.cs" Inherits="Administration_ExternalCompanyTypeGridView" Title="ExternalCompanyType" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/ExternalCompanyTypeGridView.ascx" TagName="ExternalCompanyTypeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="ExternalCompanyType Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ExternalCompanyType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ExternalCompanyType" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:ExternalCompanyTypeGridView ID="ExternalCompanyTypeGridView1" runat="server" />
</asp:Content>
