<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StorageUnitGridView.aspx.cs" Inherits="Administration_StorageUnitGridView" Title="StorageUnit" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/StorageUnitGridView.ascx" TagName="StorageUnitGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="StorageUnit Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="StorageUnit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="StorageUnit" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:StorageUnitGridView ID="StorageUnitGridView1" runat="server" />
</asp:Content>
