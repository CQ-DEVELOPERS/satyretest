<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InstructionTypeGridView.aspx.cs" Inherits="Administration_InstructionTypeGridView" Title="InstructionType" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/InstructionTypeGridView.ascx" TagName="InstructionTypeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="InstructionType Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="InstructionType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InstructionType" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:InstructionTypeGridView ID="InstructionTypeGridView1" runat="server" />
</asp:Content>
