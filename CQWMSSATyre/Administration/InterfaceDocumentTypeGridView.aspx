<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InterfaceDocumentTypeGridView.aspx.cs" Inherits="Administration_InterfaceDocumentTypeGridView" Title="InterfaceDocumentType" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/InterfaceDocumentTypeGridView.ascx" TagName="InterfaceDocumentTypeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="InterfaceDocumentType Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="InterfaceDocumentType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceDocumentType" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:InterfaceDocumentTypeGridView ID="InterfaceDocumentTypeGridView1" runat="server" />
</asp:Content>
