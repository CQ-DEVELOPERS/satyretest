<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InterfaceFieldGridView.aspx.cs" Inherits="Administration_InterfaceFieldGridView" Title="InterfaceField" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/InterfaceFieldGridView.ascx" TagName="InterfaceFieldGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="InterfaceField Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="InterfaceField">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceField" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:InterfaceFieldGridView ID="InterfaceFieldGridView1" runat="server" />
</asp:Content>
