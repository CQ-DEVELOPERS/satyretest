<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StatusGridView.aspx.cs" Inherits="Administration_StatusGridView" Title="Status" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/StatusGridView.ascx" TagName="StatusGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="Status Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Status">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Status" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:StatusGridView ID="StatusGridView1" runat="server" />
</asp:Content>
