<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PackExternalCompanyGridView.aspx.cs" Inherits="Administration_PackExternalCompanyGridView" Title="PackExternalCompany" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/PackExternalCompanyGridView.ascx" TagName="PackExternalCompanyGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="PackExternalCompany Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="PackExternalCompany">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PackExternalCompany" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:PackExternalCompanyGridView ID="PackExternalCompanyGridView1" runat="server" />
</asp:Content>
