<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InterfaceFileTypeGridView.aspx.cs" Inherits="Administration_InterfaceFileTypeGridView" Title="InterfaceFileType" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/InterfaceFileTypeGridView.ascx" TagName="InterfaceFileTypeGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="InterfaceFileType Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="InterfaceFileType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceFileType" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:InterfaceFileTypeGridView ID="InterfaceFileTypeGridView1" runat="server" />
</asp:Content>
