<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TimeBoxGridView.aspx.cs" Inherits="Administration_TimeBoxGridView" Title="TimeBox" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/TimeBoxGridView.ascx" TagName="TimeBoxGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="TimeBox Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="TimeBox">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="TimeBox" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:TimeBoxGridView ID="TimeBoxGridView1" runat="server" />
</asp:Content>
