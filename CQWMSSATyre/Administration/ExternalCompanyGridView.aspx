<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExternalCompanyGridView.aspx.cs" Inherits="Administration_ExternalCompanyGridView" Title="ExternalCompany" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/ExternalCompanyGridView.ascx" TagName="ExternalCompanyGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="ExternalCompany Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ExternalCompany">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ExternalCompany" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:ExternalCompanyGridView ID="ExternalCompanyGridView1" runat="server" />
</asp:Content>
