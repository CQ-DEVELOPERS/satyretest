<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InterfaceMappingFileGridView.aspx.cs" Inherits="Administration_InterfaceMappingFileGridView" Title="InterfaceMappingFile" MasterPageFile="~/MasterPages/MasterPage.master" Theme="Default" %>
 
<%@ Register Src="../UserControls/InterfaceMappingFileGridView.ascx" TagName="InterfaceMappingFileGridView" TagPrefix="uc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Static Maintenance"></asp:Label>
    <br />
    <asp:Label ID="LabelTitle" runat="server" SkinID="PageTitle" Text="InterfaceMappingFile Administration"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="InterfaceMappingFile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterfaceMappingFile" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:InterfaceMappingFileGridView ID="InterfaceMappingFileGridView1" runat="server" />
</asp:Content>
