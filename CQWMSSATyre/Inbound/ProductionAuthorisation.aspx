<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductionAuthorisation.aspx.cs" Inherits="Inbound_ProductionAuthorisation"
    Title="<%$ Resources:Default, ReceivingDocumentSearchTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/InboundStationSheetSearch.ascx" TagName="InboundSearch"
    TagPrefix="uc1" %>
<%@ Register Src="../Common/ProductSearchPackaging.ascx" TagName="ProductSearch"
    TagPrefix="ucpsp" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">

    <script type="text/javascript" language="JavaScript">

        function openNewWindowBatch() {
            window.open("../StaticInfo/BatchMaintenance.aspx", "_blank",
      "height=600px width=450px top=200 left=200 resizable=no scrollbars=no ");
        }

    </script>

    <ajaxToolkit:TabContainer runat="server" ID="Tabs" ActiveTabIndex="0">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Orders%>">
            <ContentTemplate>
                <div style="float: left;">
                    <uc1:InboundSearch ID="InboundSearch1" runat="server"></uc1:InboundSearch>
                </div>
                <asp:Button ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                    OnClick="ButtonDocumentSearch_Click" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:Button ID="ButtonSelect" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                    OnClick="ButtonSelect_Click"></asp:Button>
                <asp:Button ID="ButtonReceived" runat="server" Text="<%$ Resources:Default, PostResults%>"
                    OnClick="ButtonReceived_Click"></asp:Button>
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonReceived" runat="server"
                    TargetControlID="ButtonReceived" ConfirmText="<%$ Resources:Default, PressOKPostSelected%>">
                </ajaxToolkit:ConfirmButtonExtender>
                <asp:Image ID="ImageGreen3" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                <asp:Label ID="LabelGreen3" runat="server" Text="<%$ Resources:Default, ProdNotRegLT12Hrs%>"></asp:Label>
                <asp:Image ID="ImageYellow3" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                <asp:Label ID="LabelYellow3" runat="server" Text="<%$ Resources:Default, ProdGT12Hrs%>"></asp:Label>
                <asp:Image ID="ImageRed3" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                <asp:Label ID="LabelRed3" runat="server" Text="<%$ Resources:Default, ProdNotRegGT24Hrs%>"></asp:Label>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptDocument">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewReceiptDocument" runat="server" DataSourceID="ObjectDataSourceReceiptDocument"
                            DataKeyNames="InboundShipmentId,ReceiptId" AutoGenerateColumns="false" AllowPaging="true"
                            OnRowDataBound="GridViewReceiptDocument_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, ManufacturingOrders%>"
                                    ReadOnly="true" />
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product%>"
                                    ReadOnly="true" />
                                <asp:BoundField DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, PlannedYield%>"
                                    ReadOnly="true" />
                                <asp:BoundField DataField="DeliveryNoteQuantity" HeaderText="<%$ Resources:Default, FilledYield%>"
                                    ReadOnly="true" />
                                <asp:BoundField DataField="AcceptedQuantity" HeaderText="<%$ Resources:Default, ConfirmedYield%>"
                                    ReadOnly="true" />
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, OrderStatus%>"
                                    ReadOnly="true" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, DwellTime%>">
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="AdjustmentQuantity" HeaderText="<%$ Resources:Default, AdjustmentQuantity%>"
                                    ReadOnly="true" />
                                <asp:BoundField DataField="CreateDate" HeaderText='<%$ Resources:Default,CreateDate %>'
                                    ReadOnly="true" />
                                <asp:BoundField DataField="PlannedDeliveryDate" HeaderText="<%$ Resources:Default, PlannedManufactureDate%>"
                                    ApplyFormatInEditMode="False" ReadOnly="True" />
                                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, BatchNumber%>"
                                    ReadOnly="true" />
                                <asp:TemplateField HeaderText='<%$ Resources:Default,Location %>'>
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="LabelLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DDLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDocumentSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonReceived" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewReceiptDocument" EventName="SelectedIndexChanged">
                        </asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceReceiptDocument" runat="server" TypeName="Production"
                    SelectMethod="GetReceivingDocuments">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ProductCode" SessionField="ProductCode" Type="String" />
                        <asp:SessionParameter Name="Batch" SessionField="Batch" Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                    SelectMethod="GetLocationsByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="AreaCode" Type="String" DefaultValue="R" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
