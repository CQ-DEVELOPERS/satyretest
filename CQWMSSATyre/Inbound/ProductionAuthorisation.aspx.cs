using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Inbound_ProductionAuthorisation : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            { }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }

    #endregion "Page_Load"

    #region ButtonDocumentSearch_Click
    protected void ButtonDocumentSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentSearch_Click";
        try
        {

            GridViewReceiptDocument.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDocumentSearch_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in  GridViewReceiptDocument.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ButtonReceived_Click
    protected void ButtonReceived_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReceived_Click";
        try
        {
            int index = 0;
            int inboundShipmentId = 0;
            int receiptId = 0;
            CheckBox cb = new CheckBox(); 
            Status status = new Status();

            while (index < GridViewReceiptDocument.Rows.Count)
            {
                GridViewRow checkedRow = GridViewReceiptDocument.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    inboundShipmentId = int.Parse(GridViewReceiptDocument.DataKeys[index].Values["InboundShipmentId"].ToString());
                    receiptId = int.Parse(GridViewReceiptDocument.DataKeys[index].Values["ReceiptId"].ToString());

                    if (!status.InboundStatusChange(Session["ConnectionStringName"].ToString(), inboundShipmentId, receiptId, -1, "RC"))
                    {
                        result = SendErrorNow("Received" + "_" + "Error p_Receipt_Update_Status_Received");
                        Master.MsgText = "";
                        Master.ErrorText = "Exception, please make sure there are no default batches";
                        break;
                    }
                }
                index++;
            }

            GridViewReceiptDocument.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Received" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonReceived_Click"

    #region GridViewReceiptDocument_RowDataBound
    protected void GridViewReceiptDocument_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // This line will get the reference to the underlying row
            DataRowView _row = (DataRowView)e.Row.DataItem;
            if (_row != null)
            {
                DateTime deliveryDate = Convert.ToDateTime(_row.Row["PlannedDeliveryDate"]);
                DateTime endDate = DateTime.Now;
                TimeSpan dateDifference = endDate.Subtract(deliveryDate);
                int days = dateDifference.Days;
                
                if (days == 0)
                    e.Row.Cells[10].ForeColor = System.Drawing.Color.Green;

                if (days < 0)
                    e.Row.Cells[10].ForeColor = System.Drawing.Color.RoyalBlue;

                if (days > 0)
                    e.Row.Cells[10].ForeColor = System.Drawing.Color.Red;

            }
        }
    }
    #endregion GridViewReceiptDocument_RowDataBound

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocumentSearch", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
