<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="CreditAdviceReject.aspx.cs"
    Inherits="Inbound_CreditAdviceReject"
    Title="<%$ Resources:Default, CreditAdviceRejectTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
    
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, CreditAdviceRejectTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, CreditAdviceRejectAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:Button ID="ButtonPrint" runat="Server" Text="Print CA" OnClick="ButtonPrint_Click" />
    <asp:Button ID="ButtonEmail" runat="Server" Text="Email" OnClick="ButtonEmail_Click" />
    <asp:Button ID="ButtonNext" runat="Server" Text="Next" OnClick="ButtonNext_Click" />
    <asp:UpdatePanel ID="UpdatePanelDetailsViewCreditAdvice" runat="server">
        <ContentTemplate>
            <asp:DetailsView ID="DetailsViewCreditAdvice" runat="server" DataSourceID="ObjectDataSourceCreditAdvice"
                DataKeyNames="ReceiptLineId" AutoGenerateEditButton="True" AutoGenerateRows="False">
                <Fields>
                    <asp:BoundField DataField="SupplierCode" HeaderText="SupplierCode" ReadOnly="True" />
                    <asp:BoundField DataField="Supplier" HeaderText="Supplier" ReadOnly="True" />
                    <asp:BoundField DataField="OrderNumber" HeaderText="OrderNumber" ReadOnly="True" />
                    <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" ReadOnly="True" />
                    <asp:BoundField DataField="Product" HeaderText="Product" ReadOnly="True" />
                    <asp:BoundField DataField="DeliveryDate" HeaderText="DeliveryDate" ReadOnly="True" />
                    <asp:BoundField DataField="DeliveryNoteNumber" HeaderText="DeliveryNoteNumber" ReadOnly="True" />
                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" ReadOnly="True" />
                    <asp:BoundField DataField="DeliveryNoteQuantity" HeaderText="DeliveryNoteQuantity"
                        ReadOnly="True" />
                    <asp:BoundField DataField="ReceivedQuantity" HeaderText="ReceivedQuantity" ReadOnly="True" />
                    <asp:BoundField DataField="RejectQuantity" HeaderText="RejectQuantity" />
                    <asp:BoundField DataField="Exception" HeaderText="Exception" />
                    <asp:TemplateField HeaderText="Reason">
                        <EditItemTemplate>
                            <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceReason"
                                DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="TextBoxBindReasonId" runat="server" Text='<%# Bind("ReasonId") %>'></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LabelBindReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>
            </asp:DetailsView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID="ObjectDataSourceCreditAdvice" runat="server" TypeName="CreditAdvice"
        SelectMethod="GetCreditAdviceReject" OnSelecting="ObjectDataSourceCreditAdvice_OnSelecting"
        UpdateMethod="CreditAdviceRejectUpdate">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="receiptLineId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="receiptLineId" Type="Int32" />
            <asp:Parameter Name="rejectQuantity" Type="int32" />
            <asp:Parameter Name="reasonId" Type="Int32" />
            <asp:Parameter Name="exception" Type="String" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
        SelectMethod="GetReasonsByType">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="REJ" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Button ID="ButtonCancel" runat="Server" Text="Cancel" OnClick="ButtonCancel_Click" />
</asp:Content>
