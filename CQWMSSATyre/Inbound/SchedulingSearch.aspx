<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="SchedulingSearch.aspx.cs" Inherits="Scheduling_SchedulingSearch" Title="<%$ Resources:Default, SchedulingSearchTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/InboundSearch.ascx" TagName="InboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, SchedulingSearchTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, SchedulingSearchAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <div style="float: left;">
                    <uc1:InboundSearch ID="InboundSearch1" runat="server"></uc1:InboundSearch>
                </div>
                <asp:Button ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                    OnClick="ButtonDocumentSearch_Click" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInboundDocument">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewInboundDocument" runat="server" DataSourceID="ObjectDataSourceInboundDocument"
                            AllowPaging="true" PageIndex="0" DataKeyNames="InboundShipmentId,ReceiptId" AutoGenerateColumns="false"
                            AutoGenerateSelectButton="true" OnSelectedIndexChanged="GridViewInboundDocument_SelectedIndexChanged">
                            <Columns>
                                <asp:BoundField DataField="InboundShipmentId" ReadOnly="True" HeaderText="<%$ Resources:Default, InboundShipmentId %>"
                                    SortExpression="InboundShipmentId" />
                                <asp:BoundField DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber" />
                                <asp:BoundField DataField="SupplierCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SupplierCode %>"
                                    SortExpression="SupplierCode" />
                                <asp:BoundField DataField="Supplier" ReadOnly="True" HeaderText="<%$ Resources:Default, Supplier %>"
                                    SortExpression="Supplier" />
                                <asp:BoundField DataField="NumberOfLines" ReadOnly="True" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                    SortExpression="NumberOfLines" />
                                <asp:BoundField DataField="PlannedDeliveryDate" ReadOnly="True" HeaderText="<%$ Resources:Default, PlannedDeliveryDate %>"
                                    SortExpression="PlannedDeliveryDate" />
                                <asp:BoundField DataField="ActualDeliveryDate" ReadOnly="True" HeaderText="<%$ Resources:Default, ActualDeliveryDate %>"
                                    SortExpression="ActualDeliveryDate" />
                                <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status" />
                                <asp:BoundField DataField="Location" ReadOnly="True" HeaderText="<%$ Resources:Default, Location %>"
                                    SortExpression="Location" />
                                <asp:BoundField DataField="Rating" ReadOnly="True" HeaderText="<%$ Resources:Default, Rating %>"
                                    SortExpression="Rating" />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceInboundDocument" runat="server" TypeName="Scheduling"
                            SelectMethod="SearchOrders">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:SessionParameter Name="InboundDocumentTypeId" SessionField="InboundDocumentTypeId"
                                    Type="Int32" />
                                <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                                <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                                <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                                    Type="String" />
                                <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDocumentSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentType" runat="server" TypeName="InboundDocumentType"
                    SelectMethod="GetInboundDocumentTypes">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, Update%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelUpdate">
                    <ContentTemplate>
                        <asp:Panel ID="properties_ContentPanel" runat="server" Style="overflow: hidden;"
                            Height="0px">
                            <pre>&lt;ajaxToolkit:MaskedEditExtender
    TargetControlID="TextBox2" 
    Mask="99,999.99"
    <em>MessageValidatorTip</em>="true" 
    <em>OnFocusCssClass</em>="MaskedEditFocus" 
    <em>OnInvalidCssClass</em>="MaskedEditError"
    <em>MaskType</em>="Number" 
    <em>InputDirection</em>="RightToLeft" 
    <em>AcceptNegative</em>="Left" 
    <em>DisplayMoney</em>="Left"/&gt;</pre>
                        </asp:Panel>
                        <asp:DetailsView ID="DetailsViewUpdate" runat="server" DataKeyNames="InboundShipmentId,ReceiptId"
                            DataSourceID="ObjectDataSourceUpdate" AutoGenerateRows="False" AutoGenerateEditButton="True">
                            <Fields>
                                <asp:BoundField DataField="InboundDocumentType" ReadOnly="True" HeaderText="<%$ Resources:Default, InboundDocumentType %>" />
                                <asp:BoundField DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>" />
                                <asp:BoundField DataField="SupplierCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SupplierCode %>" />
                                <asp:BoundField DataField="Supplier" ReadOnly="True" HeaderText="<%$ Resources:Default, Supplier %>" />
                                <asp:BoundField DataField="NumberOfLines" ReadOnly="True" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, PlannedDeliveryDate %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxPlannedDeliveryDateEdit" runat="server" Text='<%# Bind("PlannedDeliveryDate") %>'
                                            Width="50px"></asp:TextBox>
                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="tbwePlannedDeliveryDateEdit" runat="server" TargetControlID="TextBoxPlannedDeliveryDateEdit" WatermarkText="<%$ Resources:Default,DateFormat %>"></ajaxToolkit:TextBoxWatermarkExtender>--%>
                                        <ajaxToolkit:MaskedEditExtender ID="meeDeliveryDate" runat="server" TargetControlID="TextBoxPlannedDeliveryDateEdit"
                                            Mask="<%$ Resources:Default,DateMask %>" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                            OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                            CultureName="<%$ Resources:Default,CultureCode %>" />
                                        <ajaxToolkit:MaskedEditValidator ID="mevDeliveryDate" runat="server" ControlExtender="meeDeliveryDate"
                                            ControlToValidate="TextBoxPlannedDeliveryDateEdit" IsValidEmpty="False" EmptyValueMessage="Date is required"
                                            InvalidValueMessage="Date is invalid" ValidationGroup="Demo1" Display="Dynamic"
                                            TooltipMessage="Input a Date" />
                                        <asp:TextBox ID="TextBoxPlannedDeliveryTimeEdit" runat="server" Text='<%# Bind("PlannedDeliveryTime") %>'
                                            Width="40px"></asp:TextBox>
                                        <ajaxToolkit:MaskedEditExtender ID="meePlannedDeliveryTimeEdit" runat="server" TargetControlID="TextBoxPlannedDeliveryTimeEdit"
                                            Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                            MaskType="Time" AcceptAMPM="true" CultureName="<%$ Resources:Default,CultureCode %>" />
                                        <ajaxToolkit:MaskedEditValidator ID="mevPlannedDeliveryTimeEdit" runat="server" ControlExtender="meePlannedDeliveryTimeEdit"
                                            ControlToValidate="TextBoxPlannedDeliveryTimeEdit" IsValidEmpty="False" EmptyValueMessage="Time is required"
                                            InvalidValueMessage="Time is invalid" ValidationGroup="Demo1" Display="Dynamic"
                                            TooltipMessage="Input a time" />
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="TextBoxPlannedDeliveryDateInsert" runat="server" Text='<%# Bind("PlannedDeliveryDate") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="TextBoxPlannedDeliveryDateItem" runat="server" Text='<%# Bind("PlannedDeliveryDate") %>'
                                            Enabled="false" Width="50px"></asp:TextBox>
                                        <ajaxToolkit:MaskedEditExtender ID="meePlannedDeliveryDateItem" runat="server" TargetControlID="TextBoxPlannedDeliveryDateItem"
                                            Mask="<%$ Resources:Default,DateMask %>" MessageValidatorTip="false" OnFocusCssClass="MaskedEditFocus"
                                            OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                            CultureName="<%$ Resources:Default,CultureCode %>" />
                                        <asp:TextBox ID="TextBoxPlannedDeliveryTimeItem" runat="server" Text='<%# Bind("PlannedDeliveryTime") %>'
                                            Enabled="false" Width="40px"></asp:TextBox>
                                        <ajaxToolkit:MaskedEditExtender ID="meePlannedDeliveryTimeItem" runat="server" TargetControlID="TextBoxPlannedDeliveryTimeItem"
                                            Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                            MaskType="Time" AcceptAMPM="True" CultureName="<%$ Resources:Default,CultureCode %>" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, ActualDeliveryDate %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxActualDeliveryDateEdit" runat="server" Text='<%# Bind("ActualDeliveryDate") %>'
                                            Width="50px"></asp:TextBox>
                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="tbweDeliveryDateEdit" runat="server" TargetControlID="TextBoxActualDeliveryDateEdit" WatermarkText="<%$ Resources:Default,DateFormat %>"></ajaxToolkit:TextBoxWatermarkExtender>--%>
                                        <ajaxToolkit:MaskedEditExtender ID="meeActualDate" runat="server" TargetControlID="TextBoxActualDeliveryDateEdit"
                                            Mask="<%$ Resources:Default,DateMask %>" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                                            OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                            CultureName="<%$ Resources:Default,CultureCode %>" />
                                        <ajaxToolkit:MaskedEditValidator ID="mevActualDate" runat="server" ControlExtender="meeActualDate"
                                            ControlToValidate="TextBoxActualDeliveryDateEdit" IsValidEmpty="True" EmptyValueMessage="Date is required"
                                            InvalidValueMessage="Date is invalid" ValidationGroup="Demo1" Display="Dynamic"
                                            TooltipMessage="Input a Date" />
                                        <asp:TextBox ID="TextBoxActualDeliveryTimeEdit" runat="server" Text='<%# Bind("ActualDeliveryTime") %>'
                                            Width="40px"></asp:TextBox>
                                        <ajaxToolkit:MaskedEditExtender ID="meeActualDeliveryTimeEdit" runat="server" TargetControlID="TextBoxActualDeliveryTimeEdit"
                                            Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                            MaskType="Time" AcceptAMPM="true" CultureName="<%$ Resources:Default,CultureCode %>" />
                                        <ajaxToolkit:MaskedEditValidator ID="mevActualDeliveryTimeEdit" runat="server" ControlExtender="meeActualDeliveryTimeEdit"
                                            ControlToValidate="TextBoxActualDeliveryTimeEdit" IsValidEmpty="True" EmptyValueMessage="Time is required"
                                            InvalidValueMessage="Time is invalid" ValidationGroup="Demo1" Display="Dynamic"
                                            TooltipMessage="Input a time" />
                                        <%--<ajaxToolkit:FilteredTextBoxExtender ID="fteActualDeliveryDateEdit" runat="server" FilterType="Custom" TargetControlID="TextBoxActualDeliveryDateEdit" ValidChars="0123456789/"></ajaxToolkit:FilteredTextBoxExtender>--%>
                                        <%--<ajaxToolkit:CalendarExtender ID="CalendarActualDeliveryDateEdit" runat="server" Animated="true"
                                            Format="<%$ Resources:Default,DateFormat %>" TargetControlID="TextBoxActualDeliveryDateEdit"></ajaxToolkit:CalendarExtender>--%>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorActualDeliveryDateEdit" runat="server" ControlToValidate="TextBoxActualDeliveryDateEdit" ErrorMessage="Please enter Delivery Date"></asp:RequiredFieldValidator>--%>
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="TextBoxActualDeliveryDateInsert" runat="server" Text='<%# Bind("ActualDeliveryDate") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="TextBoxActualDeliveryDateItem" runat="server" Text='<%# Bind("ActualDeliveryDate") %>'
                                            Enabled="false" Width="50px"></asp:TextBox>
                                        <ajaxToolkit:MaskedEditExtender ID="meeActualDeliveryDateItem" runat="server" TargetControlID="TextBoxActualDeliveryDateItem"
                                            Mask="<%$ Resources:Default,DateMask %>" MessageValidatorTip="false" OnFocusCssClass="MaskedEditFocus"
                                            OnInvalidCssClass="MaskedEditError" MaskType="Date" DisplayMoney="Left" AcceptNegative="Left"
                                            CultureName="<%$ Resources:Default,CultureCode %>" />
                                        <asp:TextBox ID="TextBoxActualDeliveryTimeItem" runat="server" Text='<%# Bind("ActualDeliveryTime") %>'
                                            Enabled="false" Width="40px"></asp:TextBox>
                                        <ajaxToolkit:MaskedEditExtender ID="meeActualDeliveryTimeItem" runat="server" TargetControlID="TextBoxActualDeliveryTimeItem"
                                            Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                            MaskType="Time" AcceptAMPM="True" CultureName="<%$ Resources:Default,CultureCode %>" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Status %>">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListStatus" runat="server" DataValueField="StatusId"
                                            DataTextField="Status" SelectedValue='<%# Bind("StatusId") %>' DataSourceID="ObjectDataSourceStatus">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="TextBoxStatusId" runat="server" Text='<%# Bind("StatusId") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Location %>">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListLocation" runat="server" DataValueField="LocationId"
                                            DataTextField="Location" SelectedValue='<%# Bind("LocationId") %>' DataSourceID="ObjectDataSourceLocation">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <asp:TextBox ID="TextBoxLocationId" runat="server" Text='<%# Bind("LocationId") %>'></asp:TextBox>
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Rating" ReadOnly="True" HeaderText="<%$ Resources:Default, Rating %>" />
                            </Fields>
                        </asp:DetailsView>
                        <asp:ObjectDataSource ID="ObjectDataSourceUpdate" runat="server" TypeName="Scheduling"
                            SelectMethod="GetOrder" UpdateMethod="UpdateOrder" OnUpdating="ObjectDataSourceInboundDocument_OnUpdating">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="ReceiptId" SessionField="ReceiptId" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="InboundShipmentId" Type="Int32" />
                                <asp:Parameter Name="ReceiptId" Type="Int32" />
                                <asp:Parameter Name="PlannedDeliveryDate" Type="DateTime" />
                                <asp:Parameter Name="PlannedDeliveryTime" Type="String" />
                                <asp:Parameter Name="ActualDeliveryDate" Type="DateTime" />
                                <asp:Parameter Name="ActualDeliveryTime" Type="String" />
                                <asp:Parameter Name="LocationId" Type="Int32" />
                                <asp:Parameter Name="StatusId" Type="Int32" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status"
                            SelectMethod="GetStatusScheduling">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                            SelectMethod="GetLocationsByAreaCode">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:Parameter Name="AreaCode" DefaultValue="R" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Lines%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelOrderLine">
                    <ContentTemplate>
                        <asp:Image ID="ImageRed2" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                        <asp:Label ID="LabelRed2" runat="server" Text="<%$ Resources:Default, Undefinded%>"></asp:Label>
                        <asp:Image ID="ImageGreen2" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                        <asp:Label ID="LabelGreen2" runat="server" Text="<%$ Resources:Default, Setup%>"></asp:Label>
                        <asp:GridView ID="GridViewLineUpdate" runat="server" DataSourceID="ObjectDataSourceOrderLines"
                            AutoGenerateColumns="False" AllowPaging="False">
                            <Columns>
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product" />
                                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch" />
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode" />
                                <asp:BoundField DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                    SortExpression="RequiredQuantity" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Pickface%>">
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "Pickface", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, PutawayRules%>">
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "PutawayRules", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceOrderLines" runat="server" TypeName="Scheduling"
                            SelectMethod="GetOrderLines">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="ReceiptId" SessionField="ReceiptId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
