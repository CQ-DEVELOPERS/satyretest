<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="DashboardPutaway.aspx.cs" Inherits="Inbound_DashboardPutaway" Title="<%$ Resources:Default, DefaultOutboundTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultOutboundTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultOutboundAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <table>
        <tr valign="top">
            <td colspan="2">
                <telerik:RadButton ID="RadButtonPutawayWorkload" runat="server" Text="Put Away Workload" Skin="Windows7" Width="500px"></telerik:RadButton>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonPutawayAchieved" runat="server" Text="Putaway Achieved" Skin="Windows7" Width="500px"></telerik:RadButton>
            </td>
        </tr>
        <tr valign="top">
            <td>
                <telerik:RadChart ID="RadChartPutawayWorkloadFull" runat="server" ChartTitle-TextBlock-Text="Full Pallets" Skin="DeepBlue"
                    DataGroupColumn="GroupBy" DefaultType="Pie"  AutoTextWrap="true" DataSourceID="ObjectDataSourcePutawayWorkloadFull" SeriesOrientation="Horizontal" Width="250px">
                    <Series>
                        <telerik:ChartSeries Name="Series 1" Type="Pie" DataYColumn="Value" DataLabelsColumn="Legend">
                            <Appearance LegendDisplayMode="ItemLabels">
                            </Appearance>
                        </telerik:ChartSeries>
                     </Series>
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourcePutawayWorkloadFull" runat="server" TypeName="Dashboard" SelectMethod="InboundPutawayWorkload">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="Type" DefaultValue="Mixed" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <telerik:RadChart ID="RadChartPutawayWorkloadMixed" runat="server" ChartTitle-TextBlock-Text="Mixed Pallets" Skin="DeepBlue"
                    DataGroupColumn="GroupBy" DefaultType="Pie"  AutoTextWrap="true" DataSourceID="ObjectDataSourcePutawayWorkloadMixed" SeriesOrientation="Horizontal" Width="250px">
                    <Series>
                        <telerik:ChartSeries Name="Series 1" Type="Pie" DataYColumn="Value" DataLabelsColumn="Legend">
                            <Appearance LegendDisplayMode="ItemLabels">
                            </Appearance>
                        </telerik:ChartSeries>
                     </Series>
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourcePutawayWorkloadMixed" runat="server" TypeName="Dashboard" SelectMethod="InboundPutawayWorkload">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="Type" DefaultValue="Mixed" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
                <asp:ObjectDataSource ID="ObjectDataSourcePutawayWorkload" runat="server" TypeName="Dashboard" SelectMethod="InboundPutawayWorkload">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="Type" DefaultValue="-1" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <telerik:RadChart ID="RadChartPutawayAchieved" runat="server" ChartTitle-TextBlock-Text="Putaway Achieved per hour" Skin="DeepBlue"
                     DataGroupColumn="Legend" DefaultType="Bar" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourcePutawayAchieved" SeriesOrientation="Vertical" Width="500px" Legend-Visible="false">
                    <PlotArea>
                        <XAxis DataLabelsColumn="Legend">
                        </XAxis>
                    </PlotArea>
                    <Series>
                        <telerik:ChartSeries Name="Series 1" Type="Bar" DataYColumn="Value">
                            <Appearance LegendDisplayMode="ItemLabels">
                            </Appearance>
                        </telerik:ChartSeries>
                    </Series>
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourcePutawayAchieved" runat="server" TypeName="Dashboard" SelectMethod="OutboundPutawayAchieved">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
                <telerik:RadButton ID="RadButtonPutawayWorkloadGrid" runat="server" Text="Putaway Workload" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridPutawayWorkloadGrid" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePutawayWorkload" Skin="Outlook" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourcePutawayWorkload">
                        <Columns>
                            <telerik:GridBoundColumn DataField="GroupBy" HeaderText="" UniqueName="GroupBy"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Legend" HeaderText="Type" UniqueName="Legend"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Value %>" UniqueName="Value"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Jobs" HeaderText="Jobs" UniqueName="Jobs"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <br />
                <telerik:RadButton ID="RadButtonDockToStock" runat="server" Text="Dock to Stock" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridDockToStock" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePutaway" Skin="Outlook" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceDockToStock">
                        <Columns>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="InboundDocumentType" HeaderText="<%$ Resources:Default, InboundDocumentType %>"
                                SortExpression="InboundDocumentType" UniqueName="InboundDocumentType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Lines" HeaderText="<%$ Resources:Default, Lines %>"
                                SortExpression="Lines" UniqueName="Lines">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Units" HeaderText="<%$ Resources:Default, Units %>"
                                SortExpression="Units" UniqueName="Units">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Minutes" HeaderText="<%$ Resources:Default, Minutes %>"
                                SortExpression="Minutes" UniqueName="Minutes">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="KPI" HeaderText="<%$ Resources:Default, KPI %>"
                                SortExpression="KPI" UniqueName="KPI">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourcePutaway" runat="server" TypeName="Dashboard" SelectMethod="PutawayPriority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceDockToStock" runat="server" TypeName="Dashboard" SelectMethod="DockToStock">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonPutawayAchievedKPI" runat="server" Text="Target / Hour" Skin="Outlook" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridPutawayAchievedKPI" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePutawayAchievedKPI" Skin="Default" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourcePutawayAchievedKPI">
                        <Columns>
                            <telerik:GridBoundColumn DataField="KPI" HeaderText="<%$ Resources:Default, KPI %>"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="Achieved"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                
                <asp:ObjectDataSource ID="ObjectDataSourcePutawayAchievedKPI" runat="server" TypeName="Dashboard" SelectMethod="OutboundPutawayAchievedKPI">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <br />
                <telerik:RadButton ID="RadButtonPutawayAchievedGrid" runat="server" Text="Putaway Achieved" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridPutawayAchievedGrid" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePutawayAchieved" Skin="Outlook" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourcePutawayAchieved" ShowGroupFooter="true" GroupsDefaultExpanded="false">
                        <Columns>
                            <telerik:GridBoundColumn DataField="Legend" HeaderText="Hour" UniqueName="Legend"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Units %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total : "></telerik:GridBoundColumn>
                        </Columns>
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                            <SelectFields>
                                <telerik:GridGroupByField FieldAlias="Operator" FieldName="Operator" />
                                <telerik:GridGroupByField FieldAlias="Value" FieldName="Value" Aggregate="Sum" />
                            </SelectFields>
                            <GroupByFields>
                                <telerik:GridGroupByField FieldAlias="Operator" FieldName="Operator" />
                            </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Content>