<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManualPalletiseSplit.aspx.cs" Inherits="Inbound_ManualPalletiseSplit"
    Title="Document Search" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="Manual Palletisation"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Inbound"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptLine">
        <ContentTemplate>
            <table>
                <tr>
                    <td rowspan="4">
                        <asp:GridView id="GridViewReceiptLine" runat="server" AutoGenerateColumns="false" DataKeyNames="JobId,InstructionId" AllowPaging="true">
                            <Columns>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default,JobId %>" SortExpression="ProductCode" />
                                <asp:BoundField DataField="InstructionId" HeaderText="<%$ Resources:Default,InstructionId %>" SortExpression="ProductCode" />
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default,ProductCode %>" SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default,Product %>" SortExpression="Product" />
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default,SKUCode %>" SortExpression="SKUCode" />
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default,Quantity %>" SortExpression="Quantity" />
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td colSpan=2>
                        <asp:Label id="Label5" runat="server" Text="Total Quantity Split Difference" Width="100%" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label id="Label2" runat="server" Text="Difference" Width="100px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label id="lblSplitTotal" runat="server" Font-Size="Medium" Font-Italic="False" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label id="Label3" runat="server" Text="Order Qty" Width="100px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label id="lblOrder" runat="server" Font-Size="Medium" Font-Italic="False" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label id="Label4" runat="server" Text="Order" Width="100px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label id="lblQty" runat="server" Font-Size="Medium" Font-Italic="False" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonGetNext" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <br />
    <asp:Label ID="LabelQuantity" runat="server" Text="Enter number of put away lines to create:"></asp:Label>
    <asp:TextBox ID="TextBoxLines" runat="server"></asp:TextBox>
    <asp:Button ID="ButtonCreateLines" runat="server" Text="Create Lines" OnClick="ButtonCreateLines_Click" />
    <br />
    <br />
    <asp:Button ID="ButtonAutoLocations" runat="server" Text="Auto Allocation" OnClick="ButtonAutoLocations_Click" />
    <asp:Button ID="ButtonManualLocations" runat="server" Text="Manual Allocation" OnClick="ButtonManualLocations_Click" />
    <asp:Button ID="ButtonSerial" runat="server" Text="Link Serial Numbers" OnClick="ButtonGetNext_Click" PostBackUrl="RegisterSerialNumberJob.aspx" /><br />
    <asp:Button ID="ButtonGetNext" runat="server" Text="Get Next" OnClick="ButtonGetNext_Click"></asp:Button>
    <br />
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
        <ContentTemplate>
            <asp:GridView id="GridViewInstruction" runat="server" AllowPaging="True" DataKeyNames="InstructionId" AutoGenerateColumns="False" OnRowUpdating="GridViewInstruction_RowUpdating" OnRowUpdated="GridViewInstruction_RowUpdated" AllowSorting="True">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server" Checked="true"></asp:CheckBox>                        
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="InstructionId"  ReadOnly="True" SortExpression="InstructionId" HeaderText="<%$ Resources:Default, InstructionId %>"></asp:BoundField>
                    <asp:BoundField DataField="Quantity" SortExpression="Quantity" HeaderText="<%$ Resources:Default,Quantity %>"></asp:BoundField>
                    <asp:BoundField DataField="Status"  ReadOnly="True" SortExpression="Status" HeaderText="<%$ Resources:Default,Status %>"></asp:BoundField>
                    <asp:BoundField DataField="StoreLocation"  ReadOnly="True" SortExpression="StoreLocation" HeaderText="<%$ Resources:Default,StoreLocation %>"></asp:BoundField>
                    <asp:TemplateField HeaderText="Total Quantity Split">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("SplitQuantity") %>' Load="SayHelloWorld"></asp:Label>
                        </EditItemTemplate>
                        <ItemStyle Wrap="False" />
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("SplitQuantity") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True" ></asp:CommandField>
                </Columns>
            </asp:GridView>

            <asp:GridView id="GridViewQuantity" runat="server" DataKeyNames="SplitQuantity" Visible="False">
            </asp:GridView>
            <asp:Label id="Label1" runat="server" Text="The Remainder to put away is:" Width="228px"></asp:Label>
            <asp:Button id="cmdPrint" onclick="cmdPrint_Click" runat="server" Text="Print"></asp:Button>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonCreateLines" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonAutoLocations" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonGetNext" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
