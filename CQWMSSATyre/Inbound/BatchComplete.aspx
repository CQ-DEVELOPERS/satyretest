<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="BatchComplete.aspx.cs" Inherits="Inbound_BatchComplete" Title="<%$ Resources:Default, BatchCompleteTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, BatchCompleteTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, BatchCompleteAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <div style="text-align: center; width: 800px">
        <table>
            <tr>
                <td>
                    <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, BatchCompleteScan %>"></asp:Label>
                    <br />
                    <asp:TextBox ID="TextBoxBarcode" runat="server" EnableTheming="false" Width="200px"
                        Font-Size="X-Large"></asp:TextBox>
                    <br />
                    <br />
                    <br />
                    <asp:Button ID="ButtonAccept" runat="server" Text="<%$ Resources:Default, BatchCompleteAccept %>"
                        OnClick="ButtonAccept_Click" EnableTheming="false" Height="80px" Width="200px"
                        Font-Size="X-Large" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
