using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Inbound_PutawayMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                string errors = Request.QueryString["Errors"];

                if (errors != "" || errors != null)
                {
                    if (errors == "Yes")
                    {
                        Status s = new Status();

                        Session["PMStatusId"] = s.GetStatusId(Session["ConnectionStringName"].ToString(), "PR", "RE");
                    }
                }

                if (Session["PMStatusId"] != null)
                    DropDownListStatus.SelectedValue = Session["PMStatusId"].ToString();
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "Page_Load"
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";

        try
        {
            Session["PMStatusId"] = DropDownListStatus.SelectedValue.ToString();

            GridViewPutAway.DataBind();

            Master.MsgText = "Document Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonSearch_Click"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";

        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            while (index < GridViewPutAway.Rows.Count)
            {
                GridViewRow checkedRow = GridViewPutAway.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    rowList.Add(GridViewPutAway.DataKeys[index].Values["InstructionId"]);

                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndex"

    #region GetEditIndexRowNumber
    protected void GetEditIndexRowNumber()
    {
        theErrMethod = "GetEditIndexRowNumber";

        try
        {
            if (Session["rowList"] == null)
            {
                int index = 0;
                CheckBox cb = new CheckBox();
                ArrayList rowList = new ArrayList();

                while (index < GridViewPutAway.Rows.Count)
                {
                    GridViewRow checkedRow = GridViewPutAway.Rows[index];
                    cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                    if (cb.Checked == true)
                        rowList.Add(index);

                    index++;
                }
                if (rowList.Count > 0)
                    Session["rowList"] = rowList;
            }

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GetEditIndexRowNumber"

    #region SetEditIndexRowNumber
    protected void SetEditIndexRowNumber()
    {
        theErrMethod = "SetEditIndexRowNumber";

        try
        {
            ArrayList rowList = (ArrayList)Session["rowList"];
            if (rowList == null)
                Session.Remove("rowList");
            else
            {
                if (GridViewPutAway.EditIndex != -1)
                {
                    GridViewPutAway.UpdateRow(GridViewPutAway.EditIndex, true);
                }

                if (rowList.Count < 1)
                {
                    Session.Remove("rowList");
                    GridViewPutAway.EditIndex = -1;
                }
                else
                {
                    GridViewPutAway.SelectedIndex = (int)rowList[0];
                    GridViewPutAway.EditIndex = (int)rowList[0];

                    rowList.Remove(rowList[0]);
                }
            }
            Master.MsgText = "Edit Index"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "SetEditIndexRowNumber"

    #region ButtonSplit_Click
    protected void ButtonSplit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSplit_Click";

        try
        {
            GetEditIndex();

            Session["FromURL"] = "~/Inbound/PutawayMaintenance.aspx";
            Response.Redirect("~/Common/ManualPalletiseSplit.aspx");
        
            Master.MsgText = "Split"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonSplit_Click"

    #region ButtonEdit_Click
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndex();

            if (Session["checkedList"] != null)
            {
                ArrayList checkedList = (ArrayList)Session["checkedList"];
                string instructionId = checkedList[0].ToString();
                checkedList.Remove(checkedList[0]);

                foreach (GridViewRow row in GridViewPutAway.Rows)
                {
                    theErrMethod = row.ToString();
                    if (GridViewPutAway.DataKeys[row.RowIndex].Values["InstructionId"].ToString() == instructionId)
                    {
                        GridViewPutAway.EditIndex = row.RowIndex;
                        break;
                    }
                }

                Session["checkedList"] = checkedList;
            }

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonEdit_Click
	
    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";
        
        try
        {
            Status status = new Status();
            int editIndex = GridViewPutAway.EditIndex;

            if (editIndex != -1)
            {
				GridViewPutAway.UpdateRow(editIndex, true);

                if (!status.Finished(Session["ConnectionStringName"].ToString(), int.Parse(GridViewPutAway.DataKeys[editIndex].Values["InstructionId"].ToString())))
                    Master.ErrorText = "Please try again";
            }

            GridViewPutAway.DataBind();

            if (Session["checkedList"] != null)
            {
                ArrayList checkedList = (ArrayList)Session["checkedList"];
                
                if (checkedList.Count == 0)
                    return;

                string instructionId = checkedList[0].ToString();
                checkedList.Remove(checkedList[0]);

                foreach (GridViewRow row in GridViewPutAway.Rows)
                {
                    theErrMethod = row.ToString();
                    if (GridViewPutAway.DataKeys[row.RowIndex].Values["InstructionId"].ToString() == instructionId)
                    {
                        GridViewPutAway.EditIndex = row.RowIndex;
                        break;
                    }
                }

                Session["checkedList"] = checkedList;
            }

            Master.MsgText = "Save"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = ex.Message.ToString();
        }

    }
    #endregion "ButtonSave_Click"

    #region ButtonReset_Click
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReset_Click";

        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            Status status = new Status();


            while (index < GridViewPutAway.Rows.Count)
            {
                GridViewRow checkedRow = GridViewPutAway.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");
                Master.MsgText = "Reset"; 

                if (cb.Checked == true)
                    if (!status.ResetStatus(Session["ConnectionStringName"].ToString(), int.Parse(GridViewPutAway.DataKeys[index].Values["InstructionId"].ToString())))
                        Master.ErrorText = "Please try again";

                index++;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonReset_Click

    #region ButtonPrintLabel_Click
    protected void ButtonPrintLabel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintLabel_Click";

        try
        {
            CheckBox cb = new CheckBox();
            Status status = new Status();

            foreach (GridViewRow row in GridViewPutAway.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                Master.MsgText = "Print";

                if (cb.Checked == true)
                    if (!status.Print(Session["ConnectionStringName"].ToString(), int.Parse(GridViewPutAway.DataKeys[row.RowIndex].Values["InstructionId"].ToString()), (int)Session["OperatorId"]))
                        Master.ErrorText = "Please try again";
            }

            GetEditIndex();

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 390))
                Session["LabelName"] = "Pallet Label Small.lbl";
            else
                Session["LabelName"] = "Pallet Label.lbl";

            Session["FromURL"] = "~/Inbound/PutawayMaintenance.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPrintLabel_Click

    #region GetJobs
    protected void GetJobs()
    {
        theErrMethod = "GetJobs";
        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            Session["ReportJobId"] = ",";

            while (index < GridViewPutAway.Rows.Count)
            {
                GridViewRow checkedRow = GridViewPutAway.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    Session["ReportJobId"] = Session["ReportJobId"].ToString() + GridViewPutAway.DataKeys[index].Values["JobId"].ToString() + ",";

                index++;
            }

            Master.MsgText = "Fetched"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetJobs"

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";
        try
        {
            GetJobs();

            Session["FromURL"] = "~/Inbound/PutawayMaintenance.aspx";

            Session["ReportName"] = "Putaway Sheet";

            ReportParameter[] RptParameters = new ReportParameter[4];

            // Create the JobId report parameter
            string jobId = Session["ReportJobId"].ToString();
            RptParameters[0] = new ReportParameter("JobId", jobId);

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportJobId"] = null;

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "ButtonPrint_Click"

    #region ButtonSerial_Click
    protected void ButtonSerial_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSerial_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Inbound/PutawayMaintenance.aspx";
            Response.Redirect("~/Common/RegisterSerialNumberPallet.aspx");
            
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSerial_Click

    #region ButtonLocation_Click
    protected void ButtonLocation_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonLocation_Click";

        try
        {
            GetEditIndex();

            Session["FromURL"] = "~/Inbound/PutawayMaintenance.aspx";

            Response.Redirect("~/Common/ManualLocationAllocation.aspx");


        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonLocation_Click"

    #region ButtonComplete_Click
    protected void ButtonComplete_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonComplete_Click";

        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            Status status = new Status();


            while (index < GridViewPutAway.Rows.Count)
            {
                GridViewRow checkedRow = GridViewPutAway.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");
                Master.MsgText = "Reset";

                if (cb.Checked == true)
                    if (!status.Finished(Session["ConnectionStringName"].ToString(), int.Parse(GridViewPutAway.DataKeys[index].Values["InstructionId"].ToString())))
                        Master.ErrorText = "Please try again";

                index++;
            }

            GridViewPutAway.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonComplete_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewPutAway.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSelect_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "PutawayMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "PutawayMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
