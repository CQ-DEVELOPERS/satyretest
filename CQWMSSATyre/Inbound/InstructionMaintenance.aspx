<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="InstructionMaintenance.aspx.cs" Inherits="Inbound_InstructionMaintenance"
    Title="<%$ Resources:Default, InstructionMaintenanceTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, InstructionMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, InstructionMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, InstructionMaintenanceTitle %>">
            <ContentTemplate>
                <asp:Panel ID="PanelSearch" runat="server" Width="650px" BackColor="#EFEFEC">
                    <table>
                        <tr>
                            <td>
                                <uc1:DateRange ID="DateRange1" runat="server" />
                                <asp:Button ID="ButtonSearch" runat="server" OnClick="ButtonSearch_Click" Text="<%$ Resources:Default, Search%>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default, ProductCode%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxProductCode" runat="server" Width="150px"></asp:TextBox>
                                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default, Description%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxProduct" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default, SKU%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxSKUCode" runat="server" Width="150px"></asp:TextBox>
                                <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxBatch" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelPalletId" runat="server" Text="<%$ Resources:Default, PalletId%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxPalletId" runat="server" Width="150px"></asp:TextBox>
                                <asp:Label ID="LabelJobId" runat="server" Text="<%$ Resources:Default, JobId%>" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxJobId" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelStatus" runat="server" Text="<%$ Resources:Default, Status%>" Width="100px"></asp:Label>
                                <asp:DropDownList ID="DropDownListStatus" runat="server" DataTextField="Status" DataValueField="StatusId"
                                    DataSourceID="ObjectDataSourceStatus" Width="150px">
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status"
                                    SelectMethod="GetStatus">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                        <asp:Parameter Name="Type" Type="string" DefaultValue="PR" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" 
                    FilterType="Numbers" TargetControlID="TextBoxPalletId">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE2" runat="server" 
                    FilterType="Numbers" TargetControlID="TextBoxJobId">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                    TargetControlID="PanelSearch"
                    Radius="10"
                    Color="#EFEFEC"
                    BorderColor="#404040"
                    Corners="All" />
                <br />
                <asp:Button ID="ButtonSelect" runat="server" OnClick="ButtonSelect_Click" Text="<%$ Resources:Default, SelectAll%>"></asp:Button>
                <asp:Button ID="ButtonAutoLocations" runat="server" Text="<%$ Resources:Default, AutoAllocation%>" OnClick="ButtonAutoLocations_Click" style="Width:auto;"/>
                <asp:Button ID="ButtonManualLocations" runat="server" Text="<%$ Resources:Default, ManualAllocation%>" OnClick="ButtonManualLocations_Click" style="Width:auto;"/>
                <asp:Button ID="ButtonPrintLabel" runat="server" Text="<%$ Resources:Default, PrintPalletLabels%>" OnClick="ButtonPrintLabel_Click" style="Width:auto;"/>
                <asp:Button ID="ButtonMix" runat="server" Text="<%$ Resources:Default, CreateMixedPallet%>" OnClick="ButtonMix_Click" style="Width:auto;"/>
                <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, PrintPutAwaySheet%>" OnClick="ButtonPrint_Click" />
                <asp:Button ID="ButtonLink" runat="server" Text="<%$ Resources:Default, LinkSerial%>" OnClick="ButtonLink_Click" />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewInstruction" runat="server" DataSourceID="ObjectDataSourceInstruction" AutoGenerateColumns="False"
                            DataKeyNames="InstructionId,JobId,ReceiptLineId,BatchId" AllowSorting="true" AllowPaging="True" OnPageIndexChanging="GridViewInstruction_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                        <asp:HiddenField ID="HiddenNumberOfPallets" runat="server" Value='<%# Bind("NumberOfPallets") %>' />
                                        <asp:HiddenField ID="HiddenReceiptLineId" runat="server" Value='<%# Bind("ReceiptLineId") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" SortExpression="Quantity">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" SortExpression="JobId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>" SortExpression="PickLocation">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="StoreLocation" HeaderText="<%$ Resources:Default, StoreLocation %>" SortExpression="StoreLocation">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>" SortExpression="PalletId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ECLNumber" HeaderText="<%$ Resources:Default, ECLNumber %>" SortExpression="ECLNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InboundShipmentId" HeaderText="<%$ Resources:Default, InboundShipmentId %>" SortExpression="InboundShipmentId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>" SortExpression="InstructionType">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>" SortExpression="Operator">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="NumberOfPallets" ReadOnly="True" HeaderText="<%$ Resources:Default, NumberOfPallets %>" />
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonAutoLocations" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewInstruction" EventName="SelectedIndexChanged">
                        </asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Label ID="LabelQty" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
                <asp:TextBox ID="TextBoxQty" runat="server" Text="1"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                    TargetControlID="TextBoxQty">
                </ajaxToolkit:FilteredTextBoxExtender>
                <asp:Button ID="ButtonPrintProductLabel" runat="server" OnClick="ButtonPrintProductLabel_Click"
                    Text="<%$ Resources:Default, PrintLabel%>" />
                <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="InstructionMaintenance"
                    SelectMethod="GetInstructions">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
                        <asp:ControlParameter ControlID="TextBoxPalletId" Name="palletId" ConvertEmptyStringToNull="true" DefaultValue="-1" PropertyName="Text" Type="Int32" />
                        <asp:ControlParameter ControlID="TextBoxJobId" Name="jobId" ConvertEmptyStringToNull="true" DefaultValue="-1" PropertyName="Text" Type="Int32" />
                        <asp:SessionParameter Name="FromDate" Type="DateTime" SessionField="FromDate" />
                        <asp:SessionParameter Name="ToDate" Type="DateTime" SessionField="ToDate" />
                        <asp:ControlParameter ControlID="DropDownListStatus" DefaultValue="0" Name="statusId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="TextBoxProductCode" Name="productCode" DefaultValue="%" PropertyName="Text" Type="string" />
                        <asp:ControlParameter ControlID="TextBoxProduct" Name="product" DefaultValue="%" PropertyName="Text" Type="string" />
                        <asp:ControlParameter ControlID="TextBoxSKUCode" Name="skuCode" DefaultValue="%" PropertyName="Text" Type="string" />
                        <asp:ControlParameter ControlID="TextBoxBatch" Name="batch" DefaultValue="%" PropertyName="Text" Type="string" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
