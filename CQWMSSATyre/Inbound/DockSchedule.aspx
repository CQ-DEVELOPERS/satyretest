<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="DockSchedule.aspx.cs" Inherits="Outbound_DockSchedule" Title="<%$ Resources:Default, DefaultOutboundTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>


<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultOutboundTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultOutboundAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadSchedulerAllBays">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadSchedulerAllBays" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadScheduler ID="RadSchedulerAllBays" runat="server" DataEndField="PlannedEnd" GroupBy="Location"
        DataKeyField="DockScheduleId" DataRecurrenceField="RecurrenceRule" DayEndTime="00:00"
        DataRecurrenceParentKeyField="RecurrenceParentID" DataSourceID="ObjectDataSourceDockSchedulerAllBays"
        DataDescriptionField="Description" OnAppointmentDataBound="RadSchedulerAllBays_AppointmentDataBound"
        DataStartField="PlannedStart" DataSubjectField="Subject" CssClass='<%# Bind("CssClass") %>' Skin="Metro" Height="700px">
        <AdvancedForm Modal="true" />
        <TimelineView UserSelectable="false"  />
        <%--<TimelineView SlotDuration="00:15" NumberOfSlots="96" ColumnHeaderDateFormat="hh:mm"/>--%>
        <ResourceTypes>
            <telerik:ResourceType DataSourceID="ObjectDataSourceLocation" ForeignKeyField="LocationId" 
                KeyField="LocationId" Name="Location" TextField="Location" />
            <telerik:ResourceType DataSourceID="ObjectDataSourceCssClass" ForeignKeyField="CssClass" 
                KeyField="CssClass" Name="CssClass" TextField="CssClass" />
        </ResourceTypes>
        <ResourceStyles>
            <telerik:ResourceStyleMapping Type="CssClass" Text="rsCategoryBlue" ApplyCssClass="rsCategoryBlue" />
            <telerik:ResourceStyleMapping Type="CssClass" Text="rsCategoryGreen" ApplyCssClass="rsCategoryGreen" />
            <telerik:ResourceStyleMapping Type="CssClass" Text="rsCategoryGray" ApplyCssClass="rsCategoryGray" />
            <telerik:ResourceStyleMapping Type="CssClass" Text="rsCategoryRed" ApplyCssClass="rsCategoryRed" />
            <telerik:ResourceStyleMapping Type="CssClass" Text="rsCategoryLime" ApplyCssClass="rsCategoryLime" />
        </ResourceStyles>
    </telerik:RadScheduler>
    <asp:ObjectDataSource ID="ObjectDataSourceDockSchedulerAllBays" runat="server" TypeName="DockSchedule"
        SelectMethod="DockScheduleSelect" InsertMethod="DockScheduleInsert" UpdateMethod="DockScheduleUpdate" DeleteMethod="DockScheduleDelete">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="PlannedStart" Type="DateTime" />
            <asp:Parameter Name="PlannedEnd" Type="DateTime" />
            <asp:Parameter Name="RecurrenceRule" Type="String" />
            <asp:Parameter Name="RecurrenceParentID" Type="Int32" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="LocationId" Type="Int32" />
            <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="PlannedStart" Type="DateTime" />
            <asp:Parameter Name="PlannedEnd" Type="DateTime" />
            <asp:Parameter Name="RecurrenceRule" Type="String" />
            <asp:Parameter Name="RecurrenceParentID" Type="Int32" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="LocationId" Type="Int32" />
            <asp:Parameter Name="DockScheduleId" Type="Int32" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="DockScheduleId" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
        SelectMethod="GetLocationsByAreaCode">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:Parameter Name="AreaCode" Type="String" DefaultValue="R" />

        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceCssClass" runat="server" TypeName="DockSchedule"
        SelectMethod="CssClass">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>