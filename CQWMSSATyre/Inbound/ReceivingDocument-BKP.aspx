<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReceivingDocument.aspx.cs" Inherits="Telerik.GridExamplesCSharp.Programming.SavingGridSettingsOnPerUserBasis.DefaultCS"
    Title="<%$ Resources:Default, ReceivingDocumentSearchTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/InboundShipmentSearch.ascx" TagName="InboundShipmentSearch" TagPrefix="uc1" %>
<%@ Register Src="../Common/ProductSearchPackaging.ascx" TagName="ProductSearch" TagPrefix="ucpsp" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">

    <script type="text/javascript" language="JavaScript">

        function openNewWindowBatch() {
            window.open("../StaticInfo/BatchMaintenance.aspx", "_blank",
      "height=600px width=525px top=200 left=200 resizable=no scrollbars=no ");
        }


    </script>
    
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridOrders">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridJobs" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridDetails" />
                    <telerik:AjaxUpdatedControl ControlID="ButtonPalletise" />
                    <telerik:AjaxUpdatedControl ControlID="ButtonReject" />
                    <telerik:AjaxUpdatedControl ControlID="DetailsLinesReceived" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDocumentSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridOrderLines">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                    <telerik:AjaxUpdatedControl ControlID="DetailsLinesReceived" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonPalletise" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonAutoLocations" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDefaultDelivery" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDefaultReceived" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDefaultZero" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonPrintLabel" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonTicketLabel" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSample" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDefaultReceiveds">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DetailsLinesReceived" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDefaultDeliverys">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DetailsLinesReceived" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDefaultZeros">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DetailsLinesReceived" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridJobs">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridJobs" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGridDetails">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
    <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
        SelectedIndex="0" MultiPageID="Tabs">
        <Tabs>
            <telerik:RadTab Text="<%$ Resources:Default, Orders%>"></telerik:RadTab>
            <telerik:RadTab Text="<%$ Resources:Default, Lines%>"></telerik:RadTab>
            <telerik:RadTab Text="<%$ Resources:Default, Jobs%>"></telerik:RadTab>
            <telerik:RadTab Text="<%$ Resources:Default, Details%>"></telerik:RadTab>
            <telerik:RadTab Text="<%$ Resources:Default, Packaging%>"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip><!--no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
    --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
        <telerik:RadPageView runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <uc1:InboundShipmentSearch ID="InboundShipmentSearch1" runat="server"></uc1:InboundShipmentSearch>
            <telerik:RadButton ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search %>" OnClick="ButtonDocumentSearch_Click" />
            <%--<telerik:RadButton ID="ButtonDeliveryConformance" runat="server" Text="<%$ Resources:Default, DeliveryConformance %>"
                OnClick="ButtonDeliveryConformance_Click" Style="width: auto;" />--%>
            <telerik:RadGrid ID="RadGridOrders" runat="server" Skin="Metro" AllowAutomaticUpdates="true"
                AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30"
                DataSourceID="ObjectDataSourceReceiptDocument" OnSelectedIndexChanged="RadGridOrders_SelectedIndexChanged"
                OnRowDataBound="RadGridOrders_RowDataBound" OnItemCommand="RadGridOrders_ItemCommand">
                <%--<PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>--%>
                <MasterTableView DataKeyNames="InboundShipmentId,ReceiptId,AllowPalletise,OrderNumber" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn DataField="InboundShipmentId" HeaderText='<%$ Resources:Default,InboundShipmentId %>' ReadOnly="true" SortExpression="InboundShipmentId" />
                        <telerik:GridBoundColumn DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>' ReadOnly="true" SortExpression="OrderNumber" />
                        <telerik:GridBoundColumn DataField="ReferenceNumber" HeaderText='<%$ Resources:Default,ReferenceNumber %>' ReadOnly="true" SortExpression="ReferenceNumber" />
                        <telerik:GridBoundColumn DataField="PrincipalCode" HeaderText='<%$ Resources:Default,PrincipalCode %>' ReadOnly="true" SortExpression="PrincipalCode" />
                        <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default,Delivery %>' SortExpression="Delivery">
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelDelivery" runat="server" Text='<%# Bind("Delivery") %>' Font-Bold="true"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="SupplierCode" HeaderText='<%$ Resources:Default,SupplierCode %>' ReadOnly="true" SortExpression="SupplierCode" />
                        <telerik:GridBoundColumn DataField="Supplier" HeaderText='<%$ Resources:Default,Supplier %>' ReadOnly="true" SortExpression="Supplier" />
                        <telerik:GridBoundColumn DataField="NumberOfLines" HeaderText='<%$ Resources:Default,Lines %>' ReadOnly="true" SortExpression="NumberOfLines" />
                        <telerik:GridBoundColumn DataField="PlannedDeliveryDate" HeaderText='<%$ Resources:Default,PlannedDeliveryDate %>' ReadOnly="true" SortExpression="PlannedDeliveryDate" />
                        <telerik:GridBoundColumn DataField="DeliveryDate" HeaderText='<%$ Resources:Default,DeliveryDate %>' SortExpression="DeliveryDate" DataFormatString="{0:d}" />
                        <telerik:GridBoundColumn DataField="Status" HeaderText='<%$ Resources:Default,Status %>' ReadOnly="true" SortExpression="Status" />
                        <telerik:GridBoundColumn DataField="InboundDocumentType" HeaderText='<%$ Resources:Default,InboundDocumentType %>' ReadOnly="true" SortExpression="InboundDocumentType" />
                        <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default,Location %>' SortExpression="Location">
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                    DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Rating" HeaderText='<%$ Resources:Default,Rating %>' ReadOnly="true" SortExpression="Rating" />

                        <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default,DeliveryNoteNumber %>' SortExpression="DeliveryNoteNumber">
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelDeliveryNoteNumber" runat="server" Text='<%# Bind("DeliveryNoteNumber") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBoxDeliveryNoteNumber" runat="server" Text='<%# Bind("DeliveryNoteNumber")%>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxDeliveryNoteNumber" ForeColor="Transparent" FontColor="Red" CssClass="requiredFieldValidator" Text="* Delivery Note Number Required"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <%--<telerik:GridBoundColumn DataField="DeliveryNoteNumber" HeaderText='<%$ Resources:Default,DeliveryNoteNumber %>' SortExpression="DeliveryNoteNumber" />--%>
                        <telerik:GridBoundColumn DataField="SealNumber" HeaderText='<%$ Resources:Default,SealNumber %>' SortExpression="SealNumber" />
                        <telerik:GridBoundColumn DataField="GRN" HeaderText='<%$ Resources:Default,GRN %>' SortExpression="GRN" />
                        <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default,Priority %>' SortExpression="Priority">
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelPriority" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                    DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="VehicleRegistration" HeaderText='<%$ Resources:Default,VehicleRegistration %>' SortExpression="VehicleRegistration" />
                        <telerik:GridBoundColumn DataField="ContainerNumber" HeaderText='<%$ Resources:Default,ContainerNumber %>' SortExpression="ContainerNumber" />
                        <telerik:GridBoundColumn DataField="ContainerSize" HeaderText='ContainerSize' SortExpression="ContainerSize" />
                        <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default,VehicleType %>' SortExpression="VehicleType">
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelVehicleType" runat="server" Text='<%# Bind("VehicleType") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDVehicleType" runat="server" DataSourceID="ObjectDataSourceVehicleType"
                                    DataTextField="VehicleType" DataValueField="VehicleTypeId" SelectedValue='<%# Bind("VehicleTypeId") %>'>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText='<%$ Resources:Default,ShippingAgent %>' SortExpression="ShippingAgent">
                            <ItemTemplate>
                                <div align="center">
                                    <asp:Label ID="LabelShippingAgent" runat="server" Text='<%# Bind("ShippingAgent") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDShippingAgent" runat="server" DataSourceID="ObjectDataSourceShippingAgent"
                                    DataTextField="ShippingAgent" DataValueField="ShippingAgentId" SelectedValue='<%# Bind("ShippingAgentId") %>'>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, CheckSheet %>" HeaderText="<%$ Resources:Default, CheckSheet %>" CommandName="CheckSheet"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn DataTextField="Redelivery" Text="<%$ Resources:Default, Redelivery %>" HeaderText="<%$ Resources:Default, Redelivery %>" CommandName="Redelivery"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn DataTextField="Complete" Text="<%$ Resources:Default, Complete %>" HeaderText="<%$ Resources:Default, Complete %>" CommandName="Complete"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, DeliveryConformance %>" HeaderText="<%$ Resources:Default, DeliveryConformance %>" CommandName="DeliveryConformance"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, PrintGRN %>" HeaderText="<%$ Resources:Default, PrintGRN %>" CommandName="PrintGRN"></telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Remarks" HeaderText='<%$ Resources:Default,Remarks %>' SortExpression="Remarks" />
                        <telerik:GridBoundColumn DataField="BOE" HeaderText='<%$ Resources:Default,BOE %>' SortExpression="BOE" />
                        <telerik:GridBoundColumn DataField="AdditionalText1" HeaderText='<%$ Resources:Default,AdditionalText1 %>' SortExpression="AdditionalText1" />
                        <telerik:GridBoundColumn DataField="AdditionalText2" HeaderText='<%$ Resources:Default,AdditionalText2 %>' SortExpression="AdditionalText2" />
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                    <Resizing AllowColumnResize="true"></Resizing>
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="True" />
            </telerik:RadGrid>
            <asp:ObjectDataSource ID="ObjectDataSourceReceiptDocument" runat="server" TypeName="Receiving"
                SelectMethod="GetReceivingDocuments" UpdateMethod="UpdateReceipt" OnUpdating="ObjectDataSourceReceiptDocument_Updating">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="InboundDocumentTypeId" SessionField="InboundDocumentTypeId" Type="Int32" />
                    <asp:SessionParameter Name="InboundShipmentId" DefaultValue="-1" Type="Int32" SessionField="ParameterInboundShipmentId" />
                    <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                    <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                    <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                    <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="inboundShipmentId" Type="Int32" />
                    <asp:Parameter Name="receiptId" DefaultValue="-1" Type="Int32" />
                    <asp:Parameter Name="allowPalletise" Type="Boolean" />
                    <asp:Parameter Name="deliveryDate" Type="DateTime" />
                    <asp:Parameter Name="locationId" DefaultValue="-1" Type="Int32" />
                    <asp:Parameter Name="deliveryNoteNumber" Type="String" />
                    <asp:Parameter Name="sealNumber" Type="String" />
                    <asp:Parameter Name ="GRN" Type="String" />
                    <asp:Parameter Name="priorityId" DefaultValue="-1" Type="Int32" />
                    <asp:Parameter Name="vehicleRegistration" Type="String" />
                    <asp:Parameter Name="vehicleTypeId" Type="Int32" />
                    <asp:Parameter Name="remarks" Type="String" />
                    <asp:Parameter Name="delivery" DefaultValue="-1" Type="Int32" />
                    <asp:Parameter Name="orderNumber" DefaultValue="-1" Type="String" />
                    <asp:Parameter Name="containerNumber" Type="String" />
                    <asp:Parameter Name="containerSize" Type="String" />
                    <asp:Parameter Name="shippingAgentId" Type="Int32" />
                    <asp:Parameter Name="boe" Type="String" />
                    <asp:Parameter Name="additionalText1" Type="String" />
                    <asp:Parameter Name="additionalText2" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                SelectMethod="GetLocationsByAreaCode">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:Parameter Name="AreaCode" Type="String" DefaultValue="R" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" TypeName="Priority"
                SelectMethod="GetPriorities">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceShippingAgent" runat="server" TypeName="ExternalCompany"
                    SelectMethod="ExternalCompaniesByType">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="externalCompanyTypeCode" DefaultValue="SHAG" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceVehicleType" runat="server" TypeName="VehicleType"
                    SelectMethod="GetVehicleTypes">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
        </telerik:RadPageView>
        <telerik:RadPageView runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, ReceiveLines%>">
            <table>
                <tr>
                    <td>
                        <telerik:RadButton ID="ButtonDefaultDelivery" runat="server" visible="False" Text="<%$ Resources:Default, Delivery%>" OnClick="ButtonDefaultDelivery_Click"></telerik:RadButton>
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonDefaultReceived" runat="server" visible="False"  Text="<%$ Resources:Default, Received%>" OnClick="ButtonDefaultActual_Click"></telerik:RadButton>
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonDefaultZero" runat="server" visible="False" Text="<%$ Resources:Default, ZeroLines%>" OnClick="ButtonDefaultZero_Click"></telerik:RadButton>
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonPalletise" runat="server" visible="False" Text="<%$ Resources:Default, Palletise%>" OnClick="ButtonPalletise_Click"></telerik:RadButton>
                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="ButtonPalletise" ConfirmText="<%$ Resources:Default, PressOKPalletise%>">
                        </ajaxToolkit:ConfirmButtonExtender>
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonAutoLocations" visible="False" runat="server" Text="<%$ Resources:Default, AutoLocate%>" OnClick="ButtonAutoLocations_Click"></telerik:RadButton>
                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="ButtonAutoLocations" ConfirmText="<%$ Resources:Default, PressOKAllocateLocations%>">
                        </ajaxToolkit:ConfirmButtonExtender>
                    </td>
                    <td>
                        <telerik:RadButton ID="ButtonCreditAdvice" visible="False" runat="server" Text="<%$ Resources:Default, CreditAdviceIndicator%>" OnClick="ButtonCreditAdvice_Click"></telerik:RadButton>
                    </td>
                    <td>
                        <asp:DetailsView ID="DetailsLinesReceived" runat="server" DataKeyNames="LinesReceived,TotalLines"
                            DataSourceID="ObjectDataSourceLinesReceived" AutoGenerateRows="False">
                            <Fields>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, LinesReceived %>">
                                    <ItemTemplate>
                                        <asp:Label ID="TextBoxLinesReceived" runat="server" Text='<%# Bind("LinesReceived") %>'></asp:Label>
                                        <asp:Label ID="LabelOf" runat="server" Text="<%$ Resources:Default, Of%>"></asp:Label>
                                        <asp:Label ID="TextBoxTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Fields>
                        </asp:DetailsView>
                        <asp:ObjectDataSource ID="ObjectDataSourceLinesReceived" runat="server" TypeName="Receiving"
                            SelectMethod="LinesReceived">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="inboundShipmentId" SessionField="InboundShipmentId" Type="Int32" />
                                <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="RadGridOrderLines" runat="server" Skin="Metro" DataSourceID="ObjectDataSourceReceiptLine"
                AllowAutomaticUpdates="true" AllowMultiRowSelection="true" OnItemCommand="RadGridOrderLines_ItemCommand" OnEditCommand="RadGridOrderLines_EditCommand"
                AllowFilteringByColumn="false" AllowSorting="True" AllowPaging="true" PageSize="30" ShowFooter="true" EnableLinqExpressions="true"
                OnDataBound="RadGridOrderLines_DataBound" Width="1200px">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView DataKeyNames="ReceiptLineId,StorageUnitId,BatchId,Boxes" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                <GroupByExpressions>
                  <telerik:GridGroupByExpression>
                    <SelectFields>
                      <telerik:GridGroupByField FieldAlias="OrderedQuantity" Aggregate="Sum" FieldName="RequiredQuantity" />
                      <telerik:GridGroupByField FieldAlias="DeliveryNoteQty" Aggregate="Sum" FieldName="DeliveryNoteQuantity" />
                      <telerik:GridGroupByField FieldAlias="ReceivedQty" Aggregate="Sum" FieldName="ReceivedQuantity" />
                    </SelectFields>
                    <GroupByFields>
                      <telerik:GridGroupByField FieldAlias="LineNumber" FieldName="LineNumber" />
                      <telerik:GridGroupByField FieldAlias="ProductCode" FieldName="ProductCode" />
                      <telerik:GridGroupByField FieldAlias="SKUCode" FieldName="SKUCode" />
                    </GroupByFields>
                  </telerik:GridGroupByExpression>
                </GroupByExpressions>
                <Columns>
                    <telerik:GridTemplateColumn Visible="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenNumberOfPallets" runat="server" Value='<%# Bind("NumberOfPallets") %>' />
                            <asp:HiddenField ID="HiddenReceiptLineId" runat="server" Value='<%# Bind("ReceiptLineId") %>' />
                            <asp:HiddenField ID="HiddenReceivedQuantity" runat="server" Value='<%# Bind("ReceivedQuantity") %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn DataField="LineNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, LineNumber %>" SortExpression="LineNumber" />
                    <telerik:GridBoundColumn DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" />
                    <telerik:GridBoundColumn DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product" />
                    <telerik:GridBoundColumn DataField="ChildProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ChildProductCode %>" SortExpression="ChildProductCode" />
                    <telerik:GridBoundColumn DataField="ChildProduct" ReadOnly="True" HeaderText="<%$ Resources:Default, ChildProduct %>" SortExpression="ChildProduct" />
                    <%--<telerik:GridDropDownColumn HeaderText="<%$ Resources:Default, Batch %>" UniqueName="StorageUnitBatchId" ListTextField="Batch" ListValueField="StorageUnitBatchId" DataField="StorageUnitBatchId" DataSourceID="ObjectDataSourceBatch"></telerik:GridDropDownColumn>--%>
                    <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Batch %>">
                        <EditItemTemplate>
                            <asp:Button ID="ButtonBatchPopup" runat="server" Text="<%$ Resources:Default, ButtonBatchPopup %>"
                                Visible='<%# Eval("BatchButton") %>' CausesValidation="false" OnClientClick="javascript:openNewWindowBatch();" />
                            <asp:DropDownList ID="DropDownListBatch" runat="server" DataSourceID="ObjectDataSourceBatch"
                                DataTextField="Batch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>'
                                Visible='<%# Eval("BatchSelect") %>' OnSelectedIndexChanged="DropDownListBatch_OnSelectedIndexChanged">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBatch" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" />
                    <telerik:GridBoundColumn DataField="SKU" ReadOnly="True" HeaderText="<%$ Resources:Default, SKU %>" SortExpression="SKU" />
                    <telerik:GridBoundColumn DataField="Boxes" ReadOnly="True" HeaderText="<%$ Resources:Default, Packs %>" SortExpression="Boxes" />
                    <telerik:GridBoundColumn DataField="RequiredQuantity" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderQuantity %>" SortExpression="RequiredQuantity" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="<%$ Resources:Default, OrderQuantity %>" />
                    <telerik:GridBoundColumn DataField="DeliveryNoteQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>" SortExpression="DeliveryNoteQuantity" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="<%$ Resources:Default, DeliveryNoteQuantity %>" />
                    <telerik:GridBoundColumn DataField="ReceivedQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, ReceivedQuantity %>" SortExpression="ReceivedQuantity" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="<%$ Resources:Default, ReceivedQuantity %>" />
                    <%--
                    <telerik:GridBoundColumn DataField="AcceptedQuantity" ReadOnly="True" HeaderText="<%$ Resources:Default, AcceptedQuantity %>"
                        SortExpression="AcceptedQuantity" DataFormatString="{0:G0}" />
                    --%>
                    <telerik:GridBoundColumn DataField="AcceptedWeight" ReadOnly="False" HeaderText="<%$ Resources:Default, AcceptedWeight %>" SortExpression="AcceptedWeight" DataFormatString="{0:G0}" />
                    <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, AlternateBatch %>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxAlternateBatch" runat="server" Checked='<%# Bind("AlternateBatch") %>'>
                            </asp:CheckBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="SampleQuantity" HeaderText="<%$ Resources:Default, SampleQuantity %>" SortExpression="SampleQuantity" DataFormatString="{0:G0}" />
                    <telerik:GridBoundColumn DataField="AssaySamples" HeaderText="<%$ Resources:Default, AssaySamples %>" SortExpression="AssaySamples" DataFormatString="{0:G0}" />
                    <telerik:GridBoundColumn DataField="RejectQuantity" HeaderText="<%$ Resources:Default, RejectQuantity %>" SortExpression="RejectQuantity" DataFormatString="{0:G0}" />
                    <telerik:GridDropDownColumn HeaderText="<%$ Resources:Default, Reason %>" UniqueName="DropDownListReasonId" ListTextField="Reason" ListValueField="ReasonId" DataField="ReasonId" DataSourceID="ObjectDataSourceReason"></telerik:GridDropDownColumn>
                    <telerik:GridBoundColumn DataField="CreditAdviceIndicator" ReadOnly="True" HeaderText="<%$ Resources:Default, CreditAdviceIndicator %>" SortExpression="CreditAdviceIndicator" />
                    <telerik:GridBoundColumn DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber" />
                    <telerik:GridBoundColumn DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" />
                    <telerik:GridBoundColumn DataField="NumberOfPallets" ReadOnly="True" HeaderText="<%$ Resources:Default, NumberOfPallets %>" />
                    <telerik:GridButtonColumn Text="<%$ Resources:Default, CheckSheet %>" HeaderText="<%$ Resources:Default, CheckSheet %>" CommandName="CheckSheet"></telerik:GridButtonColumn>
                    <telerik:GridButtonColumn Text="<%$ Resources:Default, SerialNumber %>" HeaderText="<%$ Resources:Default, SerialNumber %>" CommandName="SerialNumber"></telerik:GridButtonColumn>
                    <telerik:GridBoundColumn DataField="PalletId" ReadOnly="True" HeaderText="<%$ Resources:Default, PalletId %>" />
                    <telerik:GridBoundColumn DataField="BOELineNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, BOELineNumber %>" />
                    <telerik:GridBoundColumn DataField="CountryofOrigin" ReadOnly="True" HeaderText="<%$ Resources:Default, CountryofOrigin %>" />
                    <telerik:GridBoundColumn DataField="TariffCode" ReadOnly="True" HeaderText="<%$ Resources:Default, TariffCode %>" />
                    <telerik:GridBoundColumn DataField="Reference1" ReadOnly="True" HeaderText="<%$ Resources:Default, Reference1 %>" />
                    <telerik:GridBoundColumn DataField="Reference2" ReadOnly="True" HeaderText="<%$ Resources:Default, Reference2 %>" />
                    <telerik:GridBoundColumn DataField="UnitPrice" ReadOnly="True" HeaderText="<%$ Resources:Default, UnitPrice %>" />
                </Columns>
            </MasterTableView>
            <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                <Resizing AllowColumnResize="true"></Resizing>
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings ShowUnGroupButton="True" />
        </telerik:RadGrid>
            <asp:ObjectDataSource ID="ObjectDataSourceReceiptLine" runat="server" TypeName="Receiving"
                SelectMethod="GetReceiptLines" OnSelecting="ObjectDataSourceReceiptLine_OnSelecting"
                UpdateMethod="SetReceiptLines" OnUpdating="ObjectDataSourceReceiptLine_OnUpdating"
                OnUpdated="ObjectDataSourceReceiptLine_Updated">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="inboundShipmentId" Type="Int32" />
                    <asp:Parameter Name="receiptId" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="StorageUnitBatchId" SessionField="StorageUnitBatchId" Type="Int32" />
                    <asp:Parameter Name="Batch" Type="String" />
                    <asp:Parameter Name="OperatorId" Type="Int32" />
                    <asp:Parameter Name="ReceivedQuantity" Type="Decimal" />
                    <asp:Parameter Name="DeliveryNoteQuantity" Type="Decimal" />
                    <asp:Parameter Name="SampleQuantity" Type="Decimal" />
                    <asp:Parameter Name="AssaySamples" Type="Decimal" />
                    <asp:Parameter Name="AlternateBatch" Type="Boolean" />
                    <asp:Parameter Name="ReasonId" Type="Int32" />
                    <asp:Parameter Name="AcceptedWeight" Type="Decimal" />
                    <asp:Parameter Name="RejectQuantity" Type="Decimal" />
                    <asp:Parameter Name="ReceiptLineId" Type="Int32" />
                    <asp:Parameter Name="StorageUnitId" Type="Int32" />
                    <asp:Parameter Name="BatchId" Type="Int32" />
                    <asp:Parameter Name="Boxes" Type="Decimal" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
                SelectMethod="GetReasonsByType">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="RW" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                SelectMethod="GetBatchesNotExpired" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="storageUnitId" SessionField="StorageUntiId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

            <asp:Label ID="LabelQty" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
            <telerik:RadNumericTextBox ID="TextBoxQty" runat="server" DisplayText="Default" MinValue="1" MaxValue="500" showspinbuttons="true" incrementsettings-interceptarrowkeys="true" incrementsettings-interceptmousewheel="true"></telerik:RadNumericTextBox>
            <telerik:RadButton ID="ButtonPrintLabel" runat="server" OnClick="ButtonPrintLabel_Click" Text="<%$ Resources:Default, PrintProduct%>" />

            <asp:Label ID="LabelTicket" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
            <telerik:RadNumericTextBox ID="TextBoxTicket" runat="server" DisplayText="Default" MinValue="1" MaxValue="500" showspinbuttons="true" incrementsettings-interceptarrowkeys="true" incrementsettings-interceptmousewheel="true"></telerik:RadNumericTextBox>
            <telerik:RadButton ID="ButtonTicketLabel" runat="server" OnClick="ButtonTicketLabel_Click" Text="<%$ Resources:Default, PrintTicket%>" />

            <asp:Label ID="LabelSample" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
            <telerik:RadNumericTextBox ID="TextBoxSample" runat="server" DisplayText="Default" MinValue="1" MaxValue="500" showspinbuttons="true" incrementsettings-interceptarrowkeys="true" incrementsettings-interceptmousewheel="true"></telerik:RadNumericTextBox>
            <telerik:RadButton ID="ButtonSample" runat="server" OnClick="ButtonPrintSample_Click" Text="<%$ Resources:Default, PrintSample%>" />
        </telerik:RadPageView>
        <telerik:RadPageView runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Jobs%>">
            <%--<telerik:RadButton ID="ButtonPrintJob" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
                OnClick="ButtonPrintJob_Click" />--%>
            <telerik:RadButton ID="ButtonPrintCheckSheet" runat="server" Text="<%$ Resources:Default, ButtonPrintCheckSheet %>" OnClick="ButtonPrintCheckSheet_Click" />
            <telerik:RadButton ID="ButtonPalletWeight" runat="server" Text="<%$ Resources:Default, PalletWeight%>" OnClick="ButtonPalletWeight_Click" />
            <telerik:RadGrid ID="RadGridJobs" runat="server" Skin="Metro" DataSourceID="ObjectDataSourceJobs" AllowMultiRowSelection="true" 
                AllowAutomaticUpdates="true" AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30"
                OnSelectedIndexChanged="RadGridJobs_OnSelectedIndexChanged" OnItemCommand="RadGridJobs_ItemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="JobId,ReferenceNumber" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                    <Columns>
                        <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
                        <telerik:GridBoundColumn DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:Default, Status %>"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>"></telerik:GridBoundColumn>
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, CheckSheet %>" HeaderText="<%$ Resources:Default, CheckSheet %>" CommandName="CheckSheet"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, ReferenceNumber %>" HeaderText="<%$ Resources:Default, Print %>" CommandName="ReferenceNumber"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, PalletWeight %>" HeaderText="<%$ Resources:Default, PalletWeight %>" CommandName="PalletWeight"></telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                    <Resizing AllowColumnResize="true"></Resizing>
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="True" />
            </telerik:RadGrid>
            <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="Receiving"
                SelectMethod="SearchPickingJobs">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="InboundShipmentId" Type="Int32" SessionField="InboundShipmentId" />
                    <asp:SessionParameter Name="ReceiptId" Type="Int32" SessionField="ReceiptId" />
                    <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </telerik:RadPageView>
        <telerik:RadPageView runat="Server" ID="TabPanel4" HeaderText="<%$ Resources:Default, Details%>">
            <%--<telerik:RadButton ID="ButtonPrintPallet" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
                OnClick="ButtonPrintPallet_Click" />--%>
            <asp:Image ID="ImageLocationStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
            <asp:Label ID="LabelLocationStandard" runat="server" Text="<%$ Resources:Default, NoLocation%>"></asp:Label>
            <asp:Image ID="ImageLocationBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
            <asp:Label ID="LabelLocationBlue" runat="server" Text="<%$ Resources:Default, Allocated%>"></asp:Label>
            <telerik:RadGrid ID="RadGridDetails" runat="server" Skin="Metro" DataSourceID="ObjectDataSourceDetails"
                AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30"
                AllowMultiRowSelection="true" AllowAutomaticUpdates="true" OnItemCommand="RadGridDetails_ItemCommand">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="InstructionId" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                    <Columns>
                        <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
                        <telerik:GridTemplateColumn>
                            <ItemTemplate>
                                <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                            SortExpression="OrderNumber"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                            SortExpression="ProductCode"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                            SortExpression="Product"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                            SortExpression="Batch"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                            SortExpression="SKUCode"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>"
                            SortExpression="SKU"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                            SortExpression="Quantity" DataFormatString="{0:G0}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="False" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>"
                            SortExpression="ConfirmedQuantity" DataFormatString="{0:G0}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                            SortExpression="InstructionType"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>"
                            SortExpression="PickLocation"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="StoreLocation" HeaderText="<%$ Resources:Default, StoreLocation %>"
                            SortExpression="StoreLocation"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                            SortExpression="Status"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>"
                            SortExpression="Operator"></telerik:GridBoundColumn>
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, PalletLabel %>" HeaderText="<%$ Resources:Default, Print %>" CommandName="PalletLabel"></telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                    <Resizing AllowColumnResize="true"></Resizing>
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="True" />
            </telerik:RadGrid>
            <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" TypeName="Receiving"
                SelectMethod="SearchLinesByJob" UpdateMethod="UpdateLine">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="InstructionId" Type="Int32" />
                    <asp:Parameter Name="ConfirmedQuantity" Type="Decimal" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </telerik:RadPageView>
        <telerik:RadPageView runat="Server" ID="TabPanel5" HeaderText="<%$ Resources:Default, Packaging%>">
            <table>
                <tr>
                    <td>
                        <asp:Panel ID="Panel8" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small"
                            Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                            <asp:Panel ID="Panel10" runat="server">
                                <telerik:RadButton ID="ButtonPackageComplete" runat="server" Text="<%$ Resources:Default, Complete%>"
                                    OnClick="ButtonPackageReceived_Click" Enabled="false"></telerik:RadButton>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                    <td>
                        <asp:DetailsView ID="DetailsPackageLinesReceived" runat="server" DataKeyNames="LinesReceived,TotalLines"
                            DataSourceID="ObjectDataSourcePackageLinesReceived" AutoGenerateRows="False">
                            <Fields>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, LinesReceived %>">
                                    <ItemTemplate>
                                        <asp:Label ID="TextBoxLinesReceived" runat="server" Text='<%# Bind("LinesReceived") %>'></asp:Label>
                                        <asp:Label ID="LabelOf" runat="server" Text=" <%$ Resources:Default, Of%> "></asp:Label>
                                        <asp:Label ID="TextBoxTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Fields>
                        </asp:DetailsView>
                        <asp:ObjectDataSource ID="ObjectDataSourcePackageLinesReceived" runat="server" TypeName="Receiving"
                            SelectMethod="PackageLinesReceived">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:Panel ID="PanelProductSearch" runat="server" Font-Names="Tahoma" 
                            Font-Size="Small" ForeColor="#EFEFEC" 
                            GroupingText="<%$ Resources:Default, AddProduct%>" HorizontalAlign="Center" 
                            Width="380">
                            <ucpsp:ProductSearch ID="ProductSearch2" runat="server" />
                            <telerik:RadButton ID="ButtonProductAdd" runat="server" 
                                OnClick="ButtonProductAdd_Click" Text="<%$ Resources:Default, Add%>" />
                        </asp:Panel>
                    </td>
                    <td>
                        <asp:Panel ID="Panel15" runat="server" Font-Names="Tahoma" Font-Size="Small" 
                            ForeColor="#EFEFEC" GroupingText="<%$ Resources:Default, DefaultQuantities%>" 
                            HorizontalAlign="Center" Width="180">
                            <telerik:RadButton ID="ButtonPackageDefaultDelivery" runat="server" 
                                OnClick="ButtonDefaultDelivery_Click" Text="<%$ Resources:Default, Delivery%>">
                            </telerik:RadButton>
                            <telerik:RadButton ID="ButtonPackageDefaultActual" runat="server" 
                                OnClick="ButtonDefaultActual_Click" Text="<%$ Resources:Default, Actual%>">
                            </telerik:RadButton>
                            <telerik:RadButton ID="ButtonPackageDefaultZero" runat="server" 
                                OnClick="ButtonDefaultZero_Click" Text="<%$ Resources:Default, ZeroLines%>">
                            </telerik:RadButton>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="GridViewPackageLines" runat="server" Skin="Metro" DataSourceID="ObjectDataSourceDetails"
                AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" AutoGenerateEditButton="true">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="PackageLineId" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                    <Columns>
                        <telerik:GridBoundColumn DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>"
                            SortExpression="ProductCode" />
                        <telerik:GridBoundColumn DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>"
                            SortExpression="Product" />
                        <telerik:GridBoundColumn DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>"
                            SortExpression="SKUCode" />
                        <telerik:GridBoundColumn DataField="DeliveryNoteQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>"
                            SortExpression="DeliveryNoteQuantity" DataFormatString="{0:G0}" />
                        <telerik:GridBoundColumn DataField="AcceptedQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, AcceptedQuantity %>"
                            SortExpression="AcceptedQuantity" DataFormatString="{0:G0}" />
                        <telerik:GridBoundColumn DataField="RejectQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, RejectQuantity %>"
                            SortExpression="RejectQuantity" DataFormatString="{0:G0}" />
                        <telerik:GridBoundColumn DataField="CreditAdviceIndicator" ReadOnly="True" HeaderText="<%$ Resources:Default, CreditAdviceIndicator %>"
                            SortExpression="CreditAdviceIndicator" />
                        <telerik:GridBoundColumn DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>"
                            SortExpression="OrderNumber" />
                        <telerik:GridBoundColumn DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>"
                            SortExpression="Status" />
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                    <Resizing AllowColumnResize="true"></Resizing>
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="True" />
            </telerik:RadGrid>
            <br />
            <telerik:RadGrid ID="GridViewPackageLines2" runat="server" Skin="Metro" DataSourceID="ObjectDataSourceDetails"
                AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True">
                <PagerStyle Mode="NumericPages"></PagerStyle>
                <MasterTableView DataKeyNames="PackageLineId" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                    <Columns>
                        <telerik:GridBoundColumn DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>"
                            SortExpression="ProductCode" />
                        <telerik:GridBoundColumn DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>"
                            SortExpression="Product" />
                        <telerik:GridBoundColumn DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>"
                            SortExpression="SKUCode" />
                        <telerik:GridBoundColumn DataField="DeliveryNoteQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>"
                            SortExpression="DeliveryNoteQuantity" DataFormatString="{0:G0}" />
                        <telerik:GridBoundColumn DataField="AcceptedQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, AcceptedQuantity %>"
                            SortExpression="AcceptedQuantity" DataFormatString="{0:G0}" />
                        <telerik:GridBoundColumn DataField="RejectQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, RejectQuantity %>"
                            SortExpression="RejectQuantity" DataFormatString="{0:G0}" />
                        <telerik:GridBoundColumn DataField="CreditAdviceIndicator" ReadOnly="True" HeaderText="<%$ Resources:Default, CreditAdviceIndicator %>"
                            SortExpression="CreditAdviceIndicator" />
                        <telerik:GridBoundColumn DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>"
                            SortExpression="OrderNumber" />
                        <telerik:GridBoundColumn DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>"
                            SortExpression="Status" />
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                    <Resizing AllowColumnResize="true"></Resizing>
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="True" />
            </telerik:RadGrid>
            <asp:ObjectDataSource ID="ObjectDataSourcePackageLines2" runat="server" TypeName="Receiving"
                SelectMethod="GetPackageLinesComplete" OnSelecting="ObjectDataSourcePackaging_OnSelecting">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="receiptId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourcePackageLines" runat="server" TypeName="Receiving"
                SelectMethod="GetPackageLines" OnSelecting="ObjectDataSourcePackaging_OnSelecting"
                UpdateMethod="SetPackageLines" OnUpdating="ObjectDataSourcePackaging_OnUpdating"
                OnUpdated="ObjectDataSourcePackaging_Updated">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:Parameter Name="receiptId" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="StorageUnitBatchId" SessionField="StorageUnitBatchId"
                        Type="Int32" />
                    <asp:Parameter Name="OperatorId" Type="Int32" />
                    <asp:Parameter Name="AcceptedQuantity" Type="Decimal" />
                    <asp:Parameter Name="DeliveryNoteQuantity" Type="Decimal" />
                    <asp:Parameter Name="RejectQuantity" Type="Decimal" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <telerik:RadButton ID="ButtonSaveSettings" runat="server" Text="Save Layout Settings" OnClick="ButtonSaveSettings_Click" />
</asp:Content>
