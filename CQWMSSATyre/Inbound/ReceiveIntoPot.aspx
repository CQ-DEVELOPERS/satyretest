<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="ReceiveIntoPot.aspx.cs"
    Inherits="Inbound_ReceiveIntoPot"
    Title="<%$ Resources:Default, ReceiveIntoPotTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/InboundSearch.ascx" TagName="InboundSearch" TagPrefix="uc1" %>
<%@ Register Src="../Common/ProductSearchPackaging.ascx" TagName="ProductSearch" TagPrefix="ucpsp" %>


<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReceiveIntoPotTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceiveIntoPotAgenda %>"></asp:Label>
    <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
<script type="text/javascript" language="JavaScript">
 
function openNewWindowBatch()
{
   window.open("../StaticInfo/BatchMaintenance.aspx", "_blank",
      "height=600px width=450px top=200 left=200 resizable=no scrollbars=no ");
}


</script>

    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="Search">
            <ContentTemplate>
                <div style="float:left;">
                    <uc1:InboundSearch id="InboundSearch1" runat="server">
                    </uc1:InboundSearch>
                </div>
                <asp:Button ID="ButtonDocumentSearch" runat="server" Text="Search" OnClick="ButtonDocumentSearch_Click" />
                <asp:Button ID="ButtonDeliveryConformance" runat="server" Text="Delivery Confomance" OnClick="ButtonDeliveryConformance_Click" />
                <div style="clear:left;"></div>
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptDocument">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewReceiptDocument" runat="server" DataSourceID="ObjectDataSourceReceiptDocument"
                            DataKeyNames="ReceiptId,AllowPalletise,OrderNumber"   AutoGenerateSelectButton="true" AutoGenerateColumns="false" AllowPaging="true"
                            OnSelectedIndexChanged="GridViewReceiptDocument_SelectedIndexChanged" AutoGenerateEditButton="true"
                            >
                            <Columns>
                                <asp:BoundField DataField="InboundShipmentId" HeaderText='<%$ Resources:Default,InboundShipmentId %>' ReadOnly="true" />
                                <asp:BoundField DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>' ReadOnly="true" />
                                <asp:TemplateField HeaderText='<%$ Resources:Default,Delivery %>'>
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="LabelDelivery" runat="server" Text='<%# Bind("Delivery") %>' Font-Bold="true"></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SupplierCode" HeaderText='<%$ Resources:Default,SupplierCode %>' ReadOnly="true" />
                                <asp:BoundField DataField="Supplier" HeaderText='<%$ Resources:Default,Supplier %>' ReadOnly="true" />
                                <asp:BoundField DataField="NumberOfLines" HeaderText='<%$ Resources:Default,NumberOfLines %>' ReadOnly="true" />
                                <asp:BoundField DataField="PlannedDeliveryDate" HeaderText='<%$ Resources:Default,PlannedDeliveryDate %>' ReadOnly="true" />
                                <asp:BoundField DataField="DeliveryDate" HeaderText='<%$ Resources:Default,DeliveryDate %>' />
                                <asp:BoundField DataField="Status" HeaderText='<%$ Resources:Default,Status %>' ReadOnly="true" />
                                <asp:BoundField DataField="InboundDocumentType" HeaderText='<%$ Resources:Default,InboundDocumentType %>' ReadOnly="true" />
                                
                                <asp:TemplateField HeaderText='<%$ Resources:Default,ProductionLocation %>'>
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="LabelLocation" runat="server" Text='<%# Bind("Location") %>'  ></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                            <asp:DropDownList ID="DDLocation" runat="server" DataSourceID="ObjectDataSourceLocation" DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' ></asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>                                

                                <asp:BoundField DataField="Rating" HeaderText='<%$ Resources:Default,Rating %>' ReadOnly="true" />			
                                <asp:BoundField DataField="DeliveryNoteNumber" HeaderText='<%$ Resources:Default,DeliveryNoteNumber %>' />
                                <asp:BoundField DataField="SealNumber" HeaderText='<%$ Resources:Default,SealNumber %>' />
                                
                                <asp:TemplateField HeaderText='<%$ Resources:Default,Priority %>'>
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="LabelPriority" runat="server" Text='<%# Bind("Priority") %>'  ></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                            <asp:DropDownList ID="DDPriority" runat="server" DataSourceID="ObjectDataSourcePriority" DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'></asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>                                   
                                <asp:BoundField DataField="VehicleRegistration" HeaderText='<%$ Resources:Default,VehicleRegistration %>' />
                                <asp:BoundField DataField="Remarks" HeaderText='<%$ Resources:Default,Remarks %>' />

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDocumentSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewReceiptDocument" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>

                <asp:ObjectDataSource ID="ObjectDataSourceReceiptDocument" runat="server"
                    TypeName="Receiving"
                    SelectMethod="GetReceivingDocuments"
                    UpdateMethod="UpdateReceipt"     
                    >
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="InboundDocumentTypeId" SessionField="InboundDocumentTypeId" Type="Int32" />
                        <asp:SessionParameter Name="InboundShipmentId" DefaultValue="-1" Type="Int32" SessionField="InboundShipmentId" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="allowPalletise" Type="Boolean" />
                        <asp:Parameter Name="deliveryDate" ConvertEmptyStringToNull="true" Type="DateTime" />
                        <asp:Parameter Name="locationId" DefaultValue="-1"  Type="Int32" />
                        <asp:Parameter Name="deliveryNoteNumber" ConvertEmptyStringToNull="true"   Type="String" />
                        <asp:Parameter Name="sealNumber" ConvertEmptyStringToNull="true"   Type="String"  />
                        <asp:Parameter Name="priorityId" ConvertEmptyStringToNull="true" DefaultValue="-1"  Type="Int32" />
                        <asp:Parameter Name="vehicleRegistration"  ConvertEmptyStringToNull="true"  Type="String" />
                        <asp:Parameter Name="remarks"  ConvertEmptyStringToNull="true"  Type="String" />
                        <asp:Parameter Name="receiptId" ConvertEmptyStringToNull="true" DefaultValue="-1" Type="Int32" />
                        <asp:Parameter Name="delivery" ConvertEmptyStringToNull="true" DefaultValue="-1" Type="Int32" />
                        <asp:Parameter Name="orderNumber" ConvertEmptyStringToNull="true" DefaultValue="-1" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>

                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location" SelectMethod="GetLocationsByAreaCode">
                <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="AreaCode" Type="String" DefaultValue="R" />
                    </SelectParameters>
                </asp:ObjectDataSource>


                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server"
                    TypeName="Priority"
                    SelectMethod="GetPriorities">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>

            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Receive Lines">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="border:1px 1px">
                            <asp:Panel ID="Panel1" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="180" HorizontalAlign="Center" DefaultButton="ButtonSelect">
                                <asp:Button ID="ButtonSelect" runat="server" Text="Select All" OnClick="ButtonSelect_Click"></asp:Button>
                                <asp:Button ID="ButtonPrint" runat="server" Text="Check Sheet" OnClick="ButtonPrint_Click" ToolTip="To view only one line, select that line first and then click Check Sheet..."></asp:Button>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:Panel ID="Panel2" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel4">
                                    <ContentTemplate>
                                        <asp:Panel ID="PanelEdit" runat="server">
                                            <asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click"></asp:Button>
                                            <asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"></asp:Button>
                                            <br />
                                            <br />
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel runat="server" ID="UpdatePanelButtonRedelivery">
                                    <ContentTemplate>
                                        <asp:Panel ID="PanelLinesReceived" runat="server">
                                            <asp:Button ID="ButtonRedelivery" runat="server" Text="Redelivery" OnClick="ButtonRedelivery_Click" Enabled="false"></asp:Button>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonRedelivery" runat="server" TargetControlID="ButtonRedelivery" ConfirmText="Press OK to Confirm for Redelivery"></ajaxToolkit:ConfirmButtonExtender>
                                            <asp:Button ID="ButtonReceived" runat="server" Text="Complete" OnClick="ButtonReceived_Click" Enabled="false"></asp:Button>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonReceived" runat="server" TargetControlID="ButtonReceived" ConfirmText="Redelivery not requested yet. Press OK to Confirm as Received"></ajaxToolkit:ConfirmButtonExtender>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultDelivery" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultActual" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultZero" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:Panel ID="Panel3" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                                    <ContentTemplate>
                                        <asp:Panel ID="Panel6" runat="server">
                                            <asp:Button ID="ButtonReject" runat="server" Text="Reject" OnClick="ButtonReject_Click"></asp:Button>
                                            <asp:Button ID="ButtonCreditAdvice" runat="server" Text="Credit Advice" OnClick="ButtonCreditAdvice_Click"></asp:Button>
                                            <br />
                                            <br />
                                            <asp:Button ID="ButtonPalletise" runat="server" Text="Palletise" OnClick="ButtonPalletise_Click" Enabled="false"></asp:Button>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="ButtonPalletise" ConfirmText="Press OK to Palletise"></ajaxToolkit:ConfirmButtonExtender>
                                            <asp:Button ID="ButtonAutoLocations" runat="server" Text="Auto Locate" OnClick="ButtonAutoLocations_Click" Enabled="false"></asp:Button>
                                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="ButtonAutoLocations" ConfirmText="Press OK to Allocate Locations"></ajaxToolkit:ConfirmButtonExtender>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultDelivery" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultActual" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultZero" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:Panel ID="Panel4" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                                <asp:Button ID="ButtonManualLocations" Visible="False" runat="server" Text="Manual Allocation" OnClick="ButtonManualLocations_Click" Enabled="false"></asp:Button>
                                <asp:Button ID="ButtonLink" runat="server" Text="Link Serial #" OnClick="ButtonLink_Click"></asp:Button>
                                <br />
                                <br />
                                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                    <ContentTemplate>
                                        <asp:Panel ID="Panel5" runat="server">
                                            <asp:Button ID="ButtonPOD" runat="server" Text="Print POD" OnClick="ButtonPOD_Click" Enabled="false"></asp:Button>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="UpdatePanelLinesReceived">
                                <ContentTemplate>
                                    <asp:DetailsView ID="DetailsLinesReceived" runat="server" DataKeyNames="LinesReceived,TotalLines" DataSourceID="ObjectDataSourceLinesReceived" AutoGenerateRows="False">
                                        <Fields>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default, LinesReceived %>">
                                                <ItemTemplate>
                                                    <asp:Label ID="TextBoxLinesReceived" runat="server" Text='<%# Bind("LinesReceived") %>'></asp:Label>
                                                    <asp:Label ID="LabelOf" runat="server" Text=" Of "></asp:Label>
                                                    <asp:Label ID="TextBoxTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceLinesReceived" runat="server"
                                        TypeName="Receiving"
                                        SelectMethod="LinesReceived">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                            <asp:SessionParameter Name="inboundShipmentId" SessionField="InboundShipmentId" Type="Int32" />
                                            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel runat="server" ID="UpdatePanel3">
                    <ContentTemplate>
                        <asp:Panel ID="PanelDefault" runat="server" GroupingText="Default Quantities" ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                            <asp:Button ID="ButtonDefaultDelivery" runat="server" Text="Delivery" OnClick="ButtonDefaultDelivery_Click"></asp:Button>
                            <asp:Button ID="ButtonDefaultActual" runat="server" Text="Actual" OnClick="ButtonDefaultActual_Click"></asp:Button>
                            <asp:Button ID="ButtonDefaultZero" runat="server" Text="Zero Lines" OnClick="ButtonDefaultZero_Click"></asp:Button>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultDelivery" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultActual" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultZero" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptLine">
                    <ContentTemplate>
                        <asp:GridView id="GridViewReceiptLine" runat="server" DataKeyNames="ReceiptLineId,StorageUnitId,BatchId" AllowPaging="true" AutoGenerateSelectButton="False" PageSize="30">
                            <Columns>
                                <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product" />                   
                                
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Batch %>">
                                    <EditItemTemplate>
                                        <asp:Button ID="ButtonBatchPopup" runat="server" Text="<%$ Resources:Default, ButtonBatchPopup %>" Visible='<%# Eval("BatchButton") %>' CausesValidation="false" OnClientClick="javascript:openNewWindowBatch();" />
                                        <asp:DropDownList ID="DropDownListBatch" runat="server" DataSourceID="ObjectDataSourceBatch"
                                            DataTextField="Batch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' Visible='<%# Eval("BatchSelect") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                   <ItemTemplate>
                                     <asp:Label id="lblBatch" runat="server" Text='<%# Bind("Batch") %>' ></asp:Label>
                                   </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" />
                                <asp:BoundField DataField="Quantity" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderQuantity %>" SortExpression="Quantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="DeliveryNoteQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>" SortExpression="DeliveryNoteQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="AcceptedQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, AcceptedQuantity %>" SortExpression="AcceptedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, AlternateBatch %>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxAlternateBatch" runat="server" Checked='<%# Bind("AlternateBatch") %>'></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SampleQuantity" HeaderText="<%$ Resources:Default, SampleQuantity %>" SortExpression="SampleQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="RejectQuantity" ReadOnly="True" HeaderText="<%$ Resources:Default, RejectQuantity %>" SortExpression="RejectQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="CreditAdviceIndicator" ReadOnly="True" HeaderText="<%$ Resources:Default, CreditAdviceIndicator %>" SortExpression="CreditAdviceIndicator" />
                                <asp:BoundField DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber" />
                                <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" />
                            </Columns>
                        </asp:GridView>
                        
                        <asp:ObjectDataSource id="ObjectDataSourceReceiptLine" runat="server"
                            TypeName="Receiving"
                            SelectMethod="GetReceiptLines" OnSelecting="ObjectDataSourceReceiptLine_OnSelecting"
                            UpdateMethod="SetReceiptLines" OnUpdating="ObjectDataSourceReceiptLine_OnUpdating" OnUpdated="ObjectDataSourceReceiptLine_Updated">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="inboundShipmentId" Type="Int32" />
                                <asp:Parameter Name="receiptId" Type="Int32" />  
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="StorageUnitBatchId" SessionField="StorageUnitBatchId" Type="Int32" />
                                <asp:Parameter Name="Batch" Type="String" />
                                <asp:Parameter Name="OperatorId" Type="Int32" />
                                <asp:Parameter Name="AcceptedQuantity" Type="Decimal" />
                                <asp:Parameter Name="DeliveryNoteQuantity" Type="Decimal" />
                                <asp:Parameter Name="SampleQuantity" Type="Decimal" />
                                <asp:Parameter Name="AlternateBatch" Type="Boolean" />
                                <asp:Parameter Name="ReceiptLineId" Type="Int32" />
                                <asp:Parameter Name="StorageUnitId" Type="Int32" />
                                <asp:Parameter Name="BatchId" Type="Int32" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                        
                        <asp:ObjectDataSource id="ObjectDataSourceBatch" runat="server"
                            TypeName="Batch"
                            SelectMethod="GetBatchesNotExpired" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="storageUnitId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonPalletise" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonAutoLocations" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultDelivery" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultActual" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDefaultZero" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                
                <asp:Label ID="LabelQty" runat="server" Text="Copies:"></asp:Label>
                <asp:TextBox ID="TextBoxQty" runat="server" Text="1"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" FilterType="Numbers" TargetControlID="TextBoxQty"></ajaxToolkit:FilteredTextBoxExtender>
                <asp:Button ID="ButtonPrintLabel" runat="server" OnClick="ButtonPrintLabel_Click" Text="Print Label" />
                
                <asp:Label ID="LabelSample" runat="server" Text="Copies:"></asp:Label>
                <asp:TextBox ID="TextBoxSample" runat="server" Text="1"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE2" runat="server" FilterType="Numbers" TargetControlID="TextBoxSample"></ajaxToolkit:FilteredTextBoxExtender>
                <asp:Button ID="ButtonSample" runat="server" OnClick="ButtonPrintSample_Click" Text="Print Sample" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="Jobs">
            <ContentTemplate>
                <asp:Button ID="ButtonSelectJobs" runat="server" Text="Select All" OnClick="ButtonSelectJobs_Click"></asp:Button>
                <asp:Button ID="ButtonPrintJob" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrintJob_Click" />
                <asp:Button ID="ButtonPrintCheckSheet" runat="server" Text="<%$ Resources:Default, ButtonPrintCheckSheet %>" OnClick="ButtonPrintCheckSheet_Click" />
                <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged"
                            DataKeyNames="JobId,ReferenceNumber">
                            <Columns>
                                <asp:TemplateField HeaderText="Label">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField SelectText="Select" ShowSelectButton="True" />
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="Receiving"
                            SelectMethod="SearchPickingJobs">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="InboundShipmentId" Type="Int32" SessionField="InboundShipmentId" />
                                <asp:SessionParameter Name="ReceiptId" Type="Int32" SessionField="ReceiptId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelectJobs" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPrintJob" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel4" HeaderText="Details">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelDetails" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSelectPallet" runat="server" Text="Select All" OnClick="ButtonSelectPallet_Click"></asp:Button>
                        <asp:Button ID="ButtonPrintPallet" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrintPallet_Click" />
                        <asp:Image ID="ImageLocationStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                        <asp:Label ID="LabelLocationStandard" runat="server" Text="No Location"></asp:Label>
                        <asp:Image ID="ImageLocationBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                        <asp:Label ID="LabelLocationBlue" runat="server" Text="Allocated"></asp:Label>
                        <asp:GridView ID="GridViewDetails" runat="server" 
                            DataSourceID="ObjectDataSourceDetails"
                            AutoGenerateColumns="False" 
                            DataKeyNames="InstructionId" 
                            AllowPaging="True"              
                            AllowSorting="True"
                            AutoGenerateEditButton="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Check">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product">
                                </asp:BoundField>                    
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" SortExpression="Quantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="False" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" SortExpression="ConfirmedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>" SortExpression="InstructionType">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>" SortExpression="PickLocation">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StoreLocation" HeaderText="<%$ Resources:Default, StoreLocation %>" SortExpression="StoreLocation">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>" SortExpression="Operator">
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ButtonSelectPallet" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ButtonPrintPallet" EventName="Click" />
                </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceDetails" 
                    runat="server" 
                    TypeName="Receiving"
                    SelectMethod="SearchLinesByJob"
                    UpdateMethod="UpdateLine">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="InstructionId" Type="Int32" />
                        <asp:Parameter Name="ConfirmedQuantity" Type="Decimal" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
          <ajaxToolkit:TabPanel runat="Server" ID="TabPanel5" HeaderText="Packaging">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel8" runat="server" GroupingText="-" ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                                
                                <asp:UpdatePanel runat="server" ID="UpdatePanel6">
                                    <ContentTemplate>
                                        <asp:Panel ID="Panel10" runat="server">
                                            <asp:Button ID="ButtonPackageComplete" runat="server" Text="Complete" OnClick="ButtonPackageReceived_Click" Enabled="false"></asp:Button>
                                        </asp:Panel>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultDelivery" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultActual" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultZero" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                        
                        <td>
                            <asp:UpdatePanel runat="server" ID="UpdatePanel9">
                                <ContentTemplate>
                                    <asp:DetailsView ID="DetailsPackageLinesReceived" runat="server" DataKeyNames="LinesReceived,TotalLines" 
                                                DataSourceID="ObjectDataSourcePackageLinesReceived" AutoGenerateRows="False">
                                        <Fields>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default, LinesReceived %>">
                                                <ItemTemplate>
                                                    <asp:Label ID="TextBoxLinesReceived" runat="server" Text='<%# Bind("LinesReceived") %>'></asp:Label>
                                                    <asp:Label ID="LabelOf" runat="server" Text=" Of "></asp:Label>
                                                    <asp:Label ID="TextBoxTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                    <asp:ObjectDataSource ID="ObjectDataSourcePackageLinesReceived" runat="server"
                                        TypeName="Receiving"
                                        SelectMethod="PackageLinesReceived">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
                                            
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                                
                            </asp:UpdatePanel>
                        </td>
                        
                    </tr>
                </table>
                
                <table>
                 
                 <td>
                    <asp:UpdatePanel runat="server" ID="UpdatePanelProductSearch" >
                        <ContentTemplate>
                            <asp:Panel ID="PanelProductSearch" runat="server" GroupingText="Add Product" ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="380" HorizontalAlign="Center">
                                <ucpsp:ProductSearch ID="ProductSearch2" runat="server" />
                                <asp:Button ID="ButtonProductAdd" runat="server" Text="Add" OnClick="ButtonProductAdd_Click" />
                            </asp:Panel>
                         </ContentTemplate>
                     </asp:UpdatePanel>
                        
                 </td>
                 <td>
                  <asp:UpdatePanel runat="server" ID="UpdatePanel10">
                    <ContentTemplate>
                        <asp:Panel ID="Panel15" runat="server" GroupingText="Default Quantities" ForeColor="#EFEFEC" Font-Size="Small" Font-Names="Tahoma" Width="180" HorizontalAlign="Center">
                            <asp:Button ID="ButtonPackageDefaultDelivery" runat="server" Text="Delivery" OnClick="ButtonDefaultDelivery_Click"></asp:Button>
                            <asp:Button ID="ButtonPackageDefaultActual" runat="server" Text="Actual" OnClick="ButtonDefaultActual_Click"></asp:Button>
                            <asp:Button ID="ButtonPackageDefaultZero" runat="server" Text="Zero Lines" OnClick="ButtonDefaultZero_Click"></asp:Button>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultDelivery" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultActual" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultZero" EventName="Click" />
                     </Triggers>
                  </asp:UpdatePanel>
                 </td>
                </table>
                        
                <asp:UpdatePanel runat="server" ID="UpdatePanel11">
                    <ContentTemplate>
                        
                        
                  
                        <asp:GridView ID="GridViewPackageLines" runat="server" DataKeyNames="PackageLineId" AllowPaging="true" AutoGenerateEditButton="true"  AutoGenerateSelectButton="False" PageSize="30">
                            <Columns>

                                <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product" />                   
                                
                                <asp:BoundField DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" />
                                <asp:BoundField DataField="DeliveryNoteQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>" SortExpression="DeliveryNoteQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="AcceptedQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, AcceptedQuantity %>" SortExpression="AcceptedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                           
                                <asp:BoundField DataField="RejectQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, RejectQuantity %>" SortExpression="RejectQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="CreditAdviceIndicator" ReadOnly="True" HeaderText="<%$ Resources:Default, CreditAdviceIndicator %>" SortExpression="CreditAdviceIndicator" />
                                <asp:BoundField DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber" />
                                <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" />
                            </Columns>
                        </asp:GridView>
                        <br />
                        
                        <asp:GridView ID="GridViewPackageLines2" runat="server" DataKeyNames="PackageLineId" AllowPaging="true" AutoGenerateEditButton="False"  AutoGenerateSelectButton="False" PageSize="30">
                            <Columns>

                                <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product" />                   
                                
                                <asp:BoundField DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" />
                                <asp:BoundField DataField="DeliveryNoteQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, DeliveryNoteQuantity %>" SortExpression="DeliveryNoteQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="AcceptedQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, AcceptedQuantity %>" SortExpression="AcceptedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                           
                                <asp:BoundField DataField="RejectQuantity" ReadOnly="False" HeaderText="<%$ Resources:Default, RejectQuantity %>" SortExpression="RejectQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField DataField="CreditAdviceIndicator" ReadOnly="True" HeaderText="<%$ Resources:Default, CreditAdviceIndicator %>" SortExpression="CreditAdviceIndicator" />
                                <asp:BoundField DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber" />
                                <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status" />
                            </Columns>
                        </asp:GridView>
                        
                        <asp:ObjectDataSource id="ObjectDataSourcePackageLines2" runat="server"
                            TypeName="Receiving"
                            SelectMethod="GetPackageLinesComplete" OnSelecting="ObjectDataSourcePackaging_OnSelecting">
                            
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="receiptId" Type="Int32" />  
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        
                        <asp:ObjectDataSource id="ObjectDataSourcePackageLines" runat="server"
                            TypeName="Receiving"
                            SelectMethod="GetPackageLines" OnSelecting="ObjectDataSourcePackaging_OnSelecting"
                            UpdateMethod="SetPackageLines" OnUpdating="ObjectDataSourcePackaging_OnUpdating" OnUpdated="ObjectDataSourcePackaging_Updated" >
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Name="receiptId" Type="Int32" />  
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="StorageUnitBatchId" SessionField="StorageUnitBatchId" Type="Int32" />
                                <asp:Parameter Name="OperatorId" Type="Int32" />
                                <asp:Parameter Name="AcceptedQuantity" Type="Decimal" />
                                <asp:Parameter Name="DeliveryNoteQuantity" Type="Decimal" />
                                <asp:Parameter Name="RejectQuantity" Type="Decimal" />
                                
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                        
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultDelivery" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultActual" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPackageDefaultZero" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
