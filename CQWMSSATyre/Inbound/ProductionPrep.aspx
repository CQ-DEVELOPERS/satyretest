<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductionPrep.aspx.cs" Inherits="Inbound_ProductionPrep" Title="<%$ Resources:Default, ProductionPrepTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/PalletSearch.ascx" TagName="PalletSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ProductionPrepTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ProductionPrepAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, ProductionPrepTitle %>">
            <ContentTemplate>
                <uc1:PalletSearch ID="PalletSearch1" runat="server" />
                <asp:Button ID="ButtonSearch" runat="server" OnClick="ButtonSearch_Click" Text="<%$ Resources:Default, Search%>" />
                <%--                <asp:Panel ID="PanelSearch" runat="server" Width="650px" BackColor="#EFEFEC">
                    <table>
                        <tr>
                            <td>
                                <uc1:DateRange ID="DateRange1" runat="server" />
                                <asp:Button ID="ButtonSearch" runat="server" OnClick="ButtonSearch_Click" Text="Search" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelProductCode" runat="server" Text="Product Code:" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxProductCode" runat="server" Width="150px"></asp:TextBox>
                                <asp:Label ID="LabelProduct" runat="server" Text="Description:" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxProduct" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelSKUCode" runat="server" Text="SKU:" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxSKUCode" runat="server" Width="150px"></asp:TextBox>
                                <asp:Label ID="LabelBatch" runat="server" Text="Batch:" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxBatch" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelPalletId" runat="server" Text="Pallet Id:" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxPalletId" runat="server" Width="150px"></asp:TextBox>
                                <asp:Label ID="LabelJobId" runat="server" Text="Job Number:" Width="100px"></asp:Label>
                                <asp:TextBox ID="TextBoxJobId" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelStatus" runat="server" Text="Status:" Width="100px"></asp:Label>
                                <asp:DropDownList ID="DropDownListStatus" runat="server" DataTextField="Status" DataValueField="StatusId"
                                    DataSourceID="ObjectDataSourceStatus" Width="150px">
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="ObjectDataSourceStatus" runat="server" TypeName="Status"
                                    SelectMethod="GetStatus">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                        <asp:Parameter Name="Type" Type="string" DefaultValue="PR" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" 
                    FilterType="Numbers" TargetControlID="TextBoxPalletId">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE2" runat="server" 
                    FilterType="Numbers" TargetControlID="TextBoxJobId">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                    TargetControlID="PanelSearch"
                    Radius="10"
                    Color="#EFEFEC"
                    BorderColor="#404040"
                    Corners="All" />
                <br />--%>
                <asp:Button ID="ButtonSelect" runat="server" OnClick="ButtonSelect_Click" Text="<%$ Resources:Default, SelectAll%>">
                </asp:Button>
                <asp:Button ID="ButtonEdit" runat="server" OnClick="ButtonEdit_Click" Text="<%$ Resources:Default, Edit%>" />
                <asp:Button ID="ButtonSave" runat="server" OnClick="ButtonSave_Click" Text="<%$ Resources:Default, Save%>" />
                <asp:Button ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, Delete%>"
                    OnClick="ButtonDelete_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="cbeButtonDelete" runat="server" ConfirmText="<%$ Resources:Default, PressOKtodelete%>"
                    TargetControlID="ButtonDelete">
                </ajaxToolkit:ConfirmButtonExtender>
                <asp:Button ID="ButtonPrint" runat="server" OnClick="ButtonPrint_Click" Text="<%$ Resources:Default, Print%>" />
                <asp:Button ID="ButtonRegister" runat="server" OnClick="ButtonRegister_Click" Text="<%$ Resources:Default, Register%>" />
                <asp:Button ID="ButtonPalletWeight" runat="server" OnClick="ButtonPalletWeight_Click"
                    Text="<%$ Resources:Default, PalletWeight%>
" />
                <asp:Button ID="ButtonLocation" runat="server" OnClick="ButtonLocation_Click" Text="<%$ Resources:Default, ChangeLocation%>"
                    Visible="false" />
                <asp:Button ID="ButtonSerial" runat="server" OnClick="ButtonSerial_Click" Text="<%$ Resources:Default, SerialNumbers%>"
                    Visible="false" />
                <asp:Button ID="ButtonReset" runat="server" OnClick="ButtonReset_Click" Text="<%$ Resources:Default, ResetStatus%>" />
                <ajaxToolkit:ConfirmButtonExtender ID="cbeButtonReset" runat="server" ConfirmText="<%$ Resources:Default, PressOKreset%>."
                    TargetControlID="ButtonReset">
                </ajaxToolkit:ConfirmButtonExtender>
                <asp:Button Visible="false" ID="ButtonComplete" runat="server" OnClick="ButtonComplete_Click"
                    Text="<%$ Resources:Default, Complete%>" />
                <br />
                <br />
                <asp:UpdatePanel ID="UpdatePanelGridViewPutAway" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewPutAway" runat="server" AllowPaging="true" AllowSorting="true"
                            AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePutAway" DataKeyNames="InstructionId,JobId"
                            OnPageIndexChanged="GridViewPutAway_PageIndexChanged">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="OrderNumber" ReadOnly="True" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ProductCode" ReadOnly="True" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Product" ReadOnly="True" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" ReadOnly="True" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Batch" ReadOnly="True" HeaderText="<%$ Resources:Default, Batch %>"
                                    SortExpression="Batch">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Quantity" ReadOnly="True" HeaderText="<%$ Resources:Default, Quantity %>"
                                    SortExpression="Quantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>"
                                    SortExpression="ConfirmedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="NettWeight" ReadOnly="True" HeaderText="<%$ Resources:Default, NettWeight %>"
                                    SortExpression="NettWeight" ApplyFormatInEditMode="true" DataFormatString="{0:G0}">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Weight" ReadOnly="True" HeaderText="<%$ Resources:Default, GrossWeight %>"
                                    SortExpression="Weight" ApplyFormatInEditMode="true" DataFormatString="{0:G0}">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" ReadOnly="True" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PalletId" ReadOnly="True" HeaderText="<%$ Resources:Default, PalletId %>"
                                    SortExpression="PalletId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" ReadOnly="True" HeaderText="<%$ Resources:Default, JobId %>"
                                    SortExpression="JobId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="DwellTime" ReadOnly="True" HeaderText="<%$ Resources:Default, DwellTime %>"
                                    SortExpression="DwellTime">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="CreateDate" ReadOnly="True" HeaderText="<%$ Resources:Default, CreateDate %>"
                                    SortExpression="CreateDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="OperatorId" Visible="false"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Operator %>" SortExpression="Operator">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                                            DataTextField="Operator" DataValueField="OperatorId">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Operator") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourcePutAway" runat="server" TypeName="Production"
                            SelectMethod="ProductionLines" UpdateMethod="UpdateProductionLines" DeleteMethod="DeleteProductionLines">
                            <SelectParameters>
                                <%--<asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
                                <asp:ControlParameter ControlID="TextBoxPalletId" Name="palletId" ConvertEmptyStringToNull="true" DefaultValue="-1" PropertyName="Text" Type="Int32" />
                                <asp:ControlParameter ControlID="TextBoxJobId" Name="jobId" ConvertEmptyStringToNull="true" DefaultValue="-1" PropertyName="Text" Type="Int32" />
                                <asp:SessionParameter Name="FromDate" Type="DateTime" SessionField="FromDate" />
                                <asp:SessionParameter Name="ToDate" Type="DateTime" SessionField="ToDate" />
                                <asp:ControlParameter ControlID="DropDownListStatus" DefaultValue="0" Name="statusId" PropertyName="SelectedValue" Type="Int32" />
                                <asp:ControlParameter ControlID="TextBoxProductCode" Name="productCode" DefaultValue="%" PropertyName="Text" Type="string" />
                                <asp:ControlParameter ControlID="TextBoxProduct" Name="product" DefaultValue="%" PropertyName="Text" Type="string" />
                                <asp:ControlParameter ControlID="TextBoxSKUCode" Name="skuCode" DefaultValue="%" PropertyName="Text" Type="string" />
                                <asp:ControlParameter ControlID="TextBoxBatch" Name="batch" DefaultValue="%" PropertyName="Text" Type="string" />--%>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
                                <asp:SessionParameter Name="palletId" Type="Int32" SessionField="PalletId" />
                                <asp:SessionParameter Name="jobId" Type="Int32" SessionField="JobId" />
                                <asp:SessionParameter Name="fromDate" Type="DateTime" SessionField="FromDate" />
                                <asp:SessionParameter Name="toDate" Type="DateTime" SessionField="ToDate" />
                                <asp:SessionParameter Name="statusId" Type="Int32" SessionField="StatusId" />
                                <asp:SessionParameter Name="productCode" Type="string" SessionField="ProductCode" />
                                <asp:SessionParameter Name="product" Type="string" SessionField="Product" />
                                <asp:SessionParameter Name="skuCode" Type="string" SessionField="SKUCode" />
                                <asp:SessionParameter Name="batch" Type="string" SessionField="Batch" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="instructionId" Type="Int32" />
                                <asp:Parameter Name="JobId" Type="Int32" />
                                <asp:Parameter Name="confirmedQuantity" Type="Decimal" />
                                <asp:Parameter Name="operatorId" Type="Int32" />
                            </UpdateParameters>
                            <DeleteParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="InstructionId" Type="Int32" />
                                <asp:Parameter Name="JobId" Type="Int32" />
                                <asp:SessionParameter Name="operatorId" Type="Int32" SessionField="operatorId" />
                            </DeleteParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
                            SelectMethod="GetOperators">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonRegister" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonReset" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonComplete" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSplit" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:TextBox ID="TextBoxQuantity" runat="server"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="fteQuantity" runat="server" FilterType="Numbers"
                    TargetControlID="TextBoxQuantity">
                </ajaxToolkit:FilteredTextBoxExtender>
                <ajaxToolkit:NumericUpDownExtender ID="nudeQuantity" runat="server" TargetControlID="TextBoxQuantity"
                    Width="120" Minimum="1" Maximum="1000000">
                </ajaxToolkit:NumericUpDownExtender>
                <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="ObjectDataSourceReason"
                    DataTextField="Reason" DataValueField="ReasonId">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
                    SelectMethod="GetReasonsByType">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="PR" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Button ID="ButtonSplit" runat="server" OnClick="ButtonSplit_Click" Text="<%$ Resources:Default, SplitLine%>"
                    Visible="true" />
                <ajaxToolkit:ConfirmButtonExtender ID="cbeSplitLine" runat="server" ConfirmText="<%$ Resources:Default, PressOKtoSplit%>."
                    TargetControlID="ButtonSplit">
                </ajaxToolkit:ConfirmButtonExtender>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
