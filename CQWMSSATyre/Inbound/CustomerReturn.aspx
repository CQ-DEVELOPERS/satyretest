<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="CustomerReturn.aspx.cs" Inherits="Inbound_CustomerReturn" Title="<%$ Resources:Default, InboundDocumentTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Src="../Common/ProductSearchCustomerReturn.ascx" TagName="ProductSearch" TagPrefix="uc4" %>
<%@ Register Src="../Common/BatchSearch.ascx" TagName="BatchSearch" TagPrefix="uc3" %>
<%@ Register Src="../Common/CustomerSearch.ascx" TagName="ExternalCompanySearch" TagPrefix="uc2" %>
<%@ Register Src="../Common/InboundSearch.ascx" TagName="InboundSearch" TagPrefix="uc1" %>
<%@ Register Src="../Common/ReasonSearchCustomerReturns.ascx" TagName="ReasonSearch" TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, InboundDocumentTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, InboundDocumentAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs" ActiveTabIndex="1">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <uc1:InboundSearch ID="InboundSearch1" runat="server"></uc1:InboundSearch>
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInboundDocument" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewInboundDocument" runat="server" DataSourceID="ObjectDataSourceInboundDocument"
                            AllowPaging="true" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewInboundDocument_OnSelectedIndexChanged"
                            DataKeyNames="InboundDocumentId,ExternalCompanyId,ReferenceNumber">
                            <Columns>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:BoundField DataField="ExternalCompanyCode" SortExpression="ExternalCompanyCode" HeaderText="<%$ Resources:Default, ExternalCompanyCode %>" />
                                <asp:BoundField DataField="ExternalCompany" SortExpression="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>" />
                                <asp:BoundField DataField="ExternalCompanyId"  Visible="False" /> 
                                <asp:BoundField DataField="NumberOfLines" SortExpression="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                                <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" />
                                <asp:BoundField DataField="ReferenceNumber" SortExpression="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" />
                                <asp:BoundField DataField="DeliveryDate" SortExpression="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>" />
                                <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="<%$ Resources:Default, Status %>" />
                                <asp:BoundField DataField="InboundDocumentType" SortExpression="InboundDocumentType" HeaderText="<%$ Resources:Default, InboundDocumentType %>" />
                                <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="<%$ Resources:Default, Remarks %>" />
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>

                <asp:ObjectDataSource ID="ObjectDataSourceInboundDocument" runat="server" SelectMethod="SearchInboundDocument"
                    TypeName="InboundDocument">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="InboundDocumentTypeId" SessionField="InboundDocumentTypeId" Type="Int32" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:SessionParameter Name="ReturnType" SessionField="ReturnType" Type="String" DefaultValue="RET" />
                        <asp:SessionParameter Name="ContactListId" SessionField="ContactListId" Type="Int32"  />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, EditInsert%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelDetailsViewInboundDocument" RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:Button ID="ButtonChangeMode" runat="server" Text="<%$ Resources:Default, NewDocument%>" OnClick="ButtonChangeMode_Click" Visible="false" />
                                    <asp:DetailsView ID="DetailsViewInboundDocument"
                                                     runat="server"
                                                     DataSourceID="ObjectDataSourceInboundDocumentUpdate"
                                                     DataKeyNames="InboundDocumentId"
                                                     AutoGenerateEditButton="true"
                                                     AutoGenerateInsertButton="true"
                                                     AutoGenerateDeleteButton="true"
                                                     AutoGenerateRows="false"
                                                     OnModeChanging="DetailsViewInboundDocument_OnModeChanging"
                                                     OnItemInserting="DetailsViewInboundDocument_OnItemInserting"
                                                     OnDataBound="DetailsViewInboundDocument_OnDataBound">
                                        <Fields>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,InboundDocumentType %>">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="DropDownListInboundDocumentType" runat="server" DataSourceID="ObjectDataSourceInboundDocumentType"
                                                        DataValueField="InboundDocumentTypeId" DataTextField="InboundDocumentType" SelectedValue='<%# Bind("InboundDocumentTypeId") %>'>
                                                    </asp:DropDownList>
                                                    <asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentType" runat="server" TypeName="InboundDocumentType"
                                                        SelectMethod="GetInboundDocumentTypeReturn">
                                                        <SelectParameters>
                                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                                        </SelectParameters>
                                                    </asp:ObjectDataSource>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:DropDownList ID="DropDownListInboundDocumentType" runat="server" DataSourceID="ObjectDataSourceInboundDocumentType"
                                                        DataValueField="InboundDocumentTypeId" DataTextField="InboundDocumentType" SelectedValue='<%# Bind("InboundDocumentTypeId") %>'>
                                                    </asp:DropDownList>
                                                    <asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentType" runat="server" TypeName="InboundDocumentType"
                                                        SelectMethod="GetInboundDocumentTypeReturn">
                                                        <SelectParameters>
                                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                                        </SelectParameters>
                                                    </asp:ObjectDataSource>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("InboundDocumentType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="<%$ Resources:Default,Principal %>">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                                        DataValueField="PrincipalId" DataTextField="Principal" SelectedValue='<%# Bind("PrincipalId") %>'>
                                                    </asp:DropDownList>
                                                    <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                                        SelectMethod="GetPrincipalParameter">
                                                        <SelectParameters>
                                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                                        </SelectParameters>
                                                    </asp:ObjectDataSource>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                                        DataValueField="PrincipalId" DataTextField="Principal" SelectedValue='<%# Bind("PrincipalId") %>'>
                                                    </asp:DropDownList>
                                                    <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                                        SelectMethod="GetPrincipalParameter">
                                                        <SelectParameters>
                                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                                Type="String" />
                                                        </SelectParameters>
                                                    </asp:ObjectDataSource>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelPrincipal" runat="server" Text='<%# Bind("Principal") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default, DocumentNo %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBoxOrderNumberEdit" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="REQOrderNumberEdit" ControlToValidate="TextBoxOrderNumberEdit" Text="*" runat="server"></asp:RequiredFieldValidator>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:TextBox ID="TextBoxOrderNumberInsert" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="REQOrderNumberInsert" ControlToValidate="TextBoxOrderNumberInsert" Text="*" runat="server"></asp:RequiredFieldValidator>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelOrderNumber" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,ReferenceNumber %>">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LabelUnappliedCredit" runat="server" Text="<%$ Resources:Default, UnappliedCredit%>" Visible="false"></asp:Label>
                                                    <asp:Label ID="LabelReferenceNumberEdit" runat="server" Text='<%# Bind("ReferenceNumber") %>'></asp:Label>
                                                    <%--<asp:RequiredFieldValidator ID="REQReferenceNumberEdit" ControlToValidate="TextBoxReferenceNumberEdit" Text="*" runat="server"></asp:RequiredFieldValidator>--%>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:Label ID="LabelReferenceNumberInsert" runat="server" Text="<%$ Resources:Default, UnappliedCredit%>" Visible="false"></asp:Label>
                                                    <asp:TextBox ID="TextBoxReferenceNumberInsert" runat="server" Text='<%# Bind("ReferenceNumber") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="REQReferenceNumberInsert" ControlToValidate="TextBoxReferenceNumberInsert" Text="*" runat="server"></asp:RequiredFieldValidator>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelReferenceNumber" runat="server" Text='<%# Bind("ReferenceNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField ReadOnly="True" DataField="ExternalCompanyId" Visible="False"></asp:BoundField>
                                            <asp:BoundField ReadOnly="True" InsertVisible="false" DataField="ExternalCompany" HeaderText='<%$ Resources:Default,Customer %>'></asp:BoundField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,Reason %>">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="DropDownListReasonEdit" runat="server" DataSourceID="ObjectDataSourceReason"
                                                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>'>
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:DropDownList ID="DropDownListReasonInsert" runat="server" DataSourceID="ObjectDataSourceReason"
                                                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>'>
                                                    </asp:DropDownList>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelBindReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,ContactPerson %>">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="DropDownListSalesRepEdit" runat="server" DataSourceID="ObjectDataSourceContactListId"
                                                        DataTextField="ContactPerson" DataValueField="ContactListId" SelectedValue='<%# Bind("ContactListId") %>'>
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:DropDownList ID="DropDownListSalesRepInsert" runat="server" DataSourceID="ObjectDataSourceContactListId"
                                                        DataTextField="ContactPerson" DataValueField="ContactListId" SelectedValue='<%# Bind("ContactListId") %>'>
                                                    </asp:DropDownList>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelBindContactPerson" runat="server" Text='<%# Bind("ContactPerson") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                          
                                            
                                            <asp:BoundField DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>" />
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,DeliveryDate %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBoxDeliveryDateEdit" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderFromDateEdit" runat="server" Animated="true"
                                                        Format="<%$ Resources:Default,DateFormat %>" TargetControlID="TextBoxDeliveryDateEdit">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeliveryDateInsert" runat="server" ControlToValidate="TextBoxDeliveryDateEdit" ErrorMessage="Please enter Delivery Date"></asp:RequiredFieldValidator>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:TextBox ID="TextBoxDeliveryDateInsert" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderFromDateInsert" runat="server" Animated="true"
                                                        Format="<%$ Resources:Default,DateFormat %>" TargetControlID="TextBoxDeliveryDateInsert">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeliveryDateInsert" runat="server" ControlToValidate="TextBoxDeliveryDateInsert" ErrorMessage="Please enter Delivery Date"></asp:RequiredFieldValidator>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonChangeMode" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            
                            <asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentUpdate" runat="server" SelectMethod="GetInboundDocument"
                                TypeName="InboundDocument" UpdateMethod="UpdateInboundDocument" InsertMethod="CreateInboundDocument"
                                DeleteMethod="DeleteInboundDocument" OnInserted="ObjectDataSourceInboundDocumentUpdate_OnInserted">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Type="Int32" Name="InboundDocumentId" SessionField="InboundDocumentId"></asp:SessionParameter>
                                </SelectParameters>
                                <%--<UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:Parameter Name="InboundDocumentId" Type="Int32"></asp:Parameter>
                                    <asp:Parameter Name="InboundDocumentTypeId" Type="Int32"></asp:Parameter>
                                    <asp:SessionParameter Name="ExternalCompanyId" SessionField="ExternalCompanyId" Type="Int32" />
                                    <asp:Parameter Name="OrderNumber" Type="String"></asp:Parameter>
                                    <asp:Parameter Name="DeliveryDate" Type="DateTime"></asp:Parameter>
                                    <asp:Parameter Name="ContactListId" Type="Int32"></asp:Parameter>
                                </UpdateParameters>--%>
                                 <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:Parameter Name="InboundDocumentId" Type="Int32"></asp:Parameter>
                                    <asp:Parameter Name="PrincipalId" Type="Int32"></asp:Parameter>
                                    <asp:Parameter Name="InboundDocumentTypeId" Type="Int32"></asp:Parameter>
                                    <asp:SessionParameter Name="ExternalCompanyId" SessionField="ExternalCompanyId" Type="Int32" />
                                    <asp:Parameter Name="OrderNumber" Type="String"></asp:Parameter>
                                    <asp:Parameter Name="DeliveryDate" Type="DateTime"></asp:Parameter>
                                    <asp:Parameter Name="ContactListId" Type="Int32"></asp:Parameter>
                                </UpdateParameters>
                                <%--<InsertParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                    <asp:Parameter Name="InboundDocumentTypeId" Type="Int32"></asp:Parameter>
                                    <asp:SessionParameter Name="ExternalCompanyId" SessionField="ExternalCompanyId" Type="Int32" />
                                    <asp:Parameter Name="OrderNumber" Type="String"></asp:Parameter>
                                    <asp:Parameter Name="DeliveryDate" Type="DateTime"></asp:Parameter>
                                    <asp:Parameter Name="ContactListId" Type="Int32"></asp:Parameter>
                                </InsertParameters>--%>
                                <InsertParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                    <asp:Parameter Name="PrincipalId" Type="Int32"></asp:Parameter>
                                    <asp:Parameter Name="InboundDocumentTypeId" Type="Int32"></asp:Parameter>
                                    <asp:SessionParameter Name="ExternalCompanyId" SessionField="ExternalCompanyId" Type="Int32" />
                                    <asp:Parameter Name="OrderNumber" Type="String"></asp:Parameter>
                                    <asp:Parameter Name="DeliveryDate" Type="DateTime"></asp:Parameter>
                                    <asp:Parameter Name="ContactListId" Type="Int32"></asp:Parameter>
                                </InsertParameters>
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:Parameter Name="InboundDocumentId" Type="Int32"></asp:Parameter>
                                </DeleteParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:Label ID="LabelErrorTab2" runat="server" EnableTheming="false" ForeColor="red"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonChangeMode" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <br />
                                <asp:UpdatePanel runat="server" ID="UpdatePanelExternalCompanySearch" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:Panel ID="PanelCustomer" runat="server" GroupingText="<%$ Resources:Default, SelectCustomer%>">
                                            <uc2:ExternalCompanySearch ID="ExternalCompanySearch1" runat="server"></uc2:ExternalCompanySearch>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
                    SelectMethod="GetReasonsByType">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="CR" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceContactListId" runat="server" TypeName="SalesRep"
                    SelectMethod="GetSalesRepByContactId">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="ContactListId" Type="String"  />
                    </SelectParameters>
                </asp:ObjectDataSource>
              
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Lines%>" Height="100%">
            <ContentTemplate>
            
            <table border="1">
                <tr>
                    <th style="background-color:#00489C; color:White">1. Select a Product<asp:Label ID="LabelShowOrHide" runat="server" SkinID="GridviewTitle"></asp:Label></th>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelProductSearch">
                                <ContentTemplate>
                                    <uc4:ProductSearch ID="ProductSearch" runat="server" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        
                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtenderSearch" runat="Server"
                            TargetControlID="PanelSearch"
                            ExpandControlID="LabelShowOrHide"
                            CollapseControlID="LabelShowOrHide" 
                            Collapsed="False"
                            TextLabelID="LabelShowOrHide"
                            ExpandedText="<<<"
                            CollapsedText=">>"
                            SuppressPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <th style="background-color:#00489C; color:White">2. Select a Batch (Optional)<asp:Label ID="LabelShowOrHideBatch" runat="server" SkinID="GridviewTitle"></asp:Label></th>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelSearchBatch" runat="server" Width="550px" BackColor="#EFEFEC">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelBatchSearch">
                                <ContentTemplate>
                                    <uc3:BatchSearch ID="BatchSearch" runat="server" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        
                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="Server"
                            TargetControlID="PanelSearchBatch"
                            ExpandControlID="LabelShowOrHideBatch"
                            CollapseControlID="LabelShowOrHideBatch" 
                            Collapsed="True"
                            TextLabelID="LabelShowOrHideBatch"
                            ExpandedText="<<<"
                            CollapsedText=">>"
                            SuppressPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <th style="background-color:#00489C; color:White">3. Enter Quantity</th>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel runat="server" ID="UpdatePanelTextBoxQuantity">
                            <ContentTemplate>
                                <asp:Label ID="LabelQuantity" runat="server"></asp:Label>
                                <asp:TextBox ID="TextBoxQuantity" runat="server"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Numbers" TargetControlID="TextBoxQuantity"></ajaxToolkit:FilteredTextBoxExtender>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <th style="background-color:#00489C; color:White">4. Enter Reason</th>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelSearchReason" runat="server" Width="550px" BackColor="#EFEFEC">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelReasonSearch">
                                <ContentTemplate>
                                    <uc5:ReasonSearch ID="ReasonSearch" runat="server" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        
                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="Server"
                            TargetControlID="PanelSearchReason"
                            ExpandControlID="LabelShowOrHide"
                            CollapseControlID="LabelShowOrHide" 
                            Collapsed="False"
                            TextLabelID="LabelShowOrHide"
                            ExpandedText="<<<"
                            CollapsedText=">>"
                            SuppressPostBack="true" />
                    </td>
                </tr>
            </table>
            
<%--                <ajaxToolkit:Accordion ID="AccordionLineInsert" runat="server" SelectedIndex="0"
                    HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
                    FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
                    SuppressHeaderPostbacks="true">
                    <Panes>
                        <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                            <Header>
                                <a href="" class="accordionLink">1. Select a Product</a></Header>
                            <Content>
                                <asp:UpdatePanel runat="server" ID="UpdatePanelProductSearch">
                                    <ContentTemplate>
                                        <uc4:ProductSearch ID="ProductSearch" runat="server" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                        <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                            <Header>
                                <a href="" class="accordionLink">2. Select a Batch (Optional)</a></Header>
                            <Content>
                                <asp:UpdatePanel runat="server" ID="UpdatePanelBatchSearch">
                                    <ContentTemplate>
                                        <uc3:BatchSearch ID="BatchSearch" runat="server" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                        <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
                            <Header>
                                <a href="" class="accordionLink">3. Enter Quantity</a></Header>
                            <Content>
                                <asp:UpdatePanel runat="server" ID="UpdatePanelTextBoxQuantity">
                                    <ContentTemplate>
                                        <asp:Label ID="LabelQuantity" runat="server"></asp:Label>
                                        <asp:TextBox ID="TextBoxQuantity" runat="server"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Numbers" TargetControlID="TextBoxQuantity"></ajaxToolkit:FilteredTextBoxExtender>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                    </Panes>
                </ajaxToolkit:Accordion>--%>
                
                <asp:Button ID="ButtonInsertLine" runat="server" Text="<%$ Resources:Default, Insert%>" OnClick="ButtonInsertLine_Click" />
                <asp:Button ID="ButtonDeleteLine" runat="server" Text="<%$ Resources:Default, Delete%>" OnClick="ButtonDeleteLine_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonDeleteLine" runat="server" ConfirmText="<%$ Resources:Default, PressOkDelete%>" TargetControlID="ButtonDeleteLine"></ajaxToolkit:ConfirmButtonExtender>
                <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                <asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, ProductNotOnInvoice%>"></asp:Label>
                <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                <asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, QuantityMoreThanOrder%>"></asp:Label>
                <asp:Image ID="ImageGreen" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                <asp:Label ID="LabelGreen" runat="server" Text="<%$ Resources:Default, Correct%>"></asp:Label>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInboundLine">
                    <ContentTemplate>
                        <asp:Label ID="LabelErrorMsg" runat="server" Text="" EnableTheming="false" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="GridViewInboundLine" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                            AutoGenerateSelectButton="true" AutoGenerateEditButton="true" DataKeyNames="InboundLineId"
                            DataSourceID="ObjectDataSourceInboundLine">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "Remarks", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ProductCode" ReadOnly="true" HeaderText="<%$ Resources:Default, ProductCode %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Product" ReadOnly="true" HeaderText="<%$ Resources:Default, Product %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" ReadOnly="true" HeaderText="<%$ Resources:Default, SKUCode %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Batch" ReadOnly="true" HeaderText="<%$ Resources:Default, Batch %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="ECLNumber" ReadOnly="true" HeaderText="<%$ Resources:Default, ECLNumber %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" ReadOnly="true" HeaderText="<%$ Resources:Default, Status %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="UnitPrice" HeaderText="<%$ Resources:Default, UnappliedCredit %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="ReasonId" HeaderText="<%$ Resources:Default, ReasonId %>" Visible="False">
                                </asp:BoundField>
                                <%--<asp:BoundField DataField="Reason" HeaderText="<%$ Resources:Default, Reason %>" ReadOnly="True">
                                </asp:BoundField>--%>
                                <asp:TemplateField HeaderText="<%$ Resources:Default,Reason %>">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="DropDownListReasonEdit" runat="server" DataSourceID="ObjectDataSourceReason2"
                                                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>'>
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:DropDownList ID="DropDownListReasonInsert" runat="server" DataSourceID="ObjectDataSourceReason"
                                                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>'>
                                                    </asp:DropDownList>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelBindReason" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                                                </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDeleteLine" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceInboundLine" runat="server" TypeName="InboundDocument"
                    SelectMethod="SearchInboundLine" UpdateMethod="UpdateInboundLine" InsertMethod="CreateInboundLine"
                    DeleteMethod="DeleteInboundLine" >
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Type="Int32" Name="InboundDocumentId" SessionField="InboundDocumentId" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="InboundLineId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="Quantity" Type="Decimal"></asp:Parameter>
                        <asp:Parameter Name="UnitPrice" Type="Decimal"></asp:Parameter>  
                        <asp:Parameter Name="ReasonId" Type="Int32"></asp:Parameter>                      
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
                        <asp:SessionParameter Name="InboundDocumentId" SessionField="InboundDocumentId" Type="Int32" />
                        <asp:SessionParameter Name="StorageUnitId" SessionField="StorageUnitId" Type="Int32" />
                        <asp:ControlParameter Name="Quantity" ControlID="TextBoxQuantity" Type="Decimal" />
                        <asp:SessionParameter Name="BatchId" SessionField="BatchId" Type="Int32" />
                        <asp:SessionParameter Name="ReasonId" SessionField="ReasonId" Type="Int32" />
                    </InsertParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="InboundLineId" Type="Int32"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceReason2" runat="server" TypeName="Reason"
                    SelectMethod="GetReasonsByType">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="CR" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, PrintUpliftment%>"
                                OnClick="ButtonPrint_Click" style="Width:auto;"/>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
