using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Inbound_InboundShipmentCreate : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            //Sets the menu for Receiving
            Session["MenuId"] = 3;

            if (TextBoxShipmentDate.Text == "")
                TextBoxShipmentDate.Text = DateTime.Today.ToShortDateString();

            if (TextBoxShipmentTime.Text == "")
                TextBoxShipmentTime.Text = DateTime.Today.TimeOfDay.ToString();

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page Load

    #region ButtonAdd_Click
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAdd_Click";

        try
        {
            InboundShipment ds = new InboundShipment();

            DateTime shipmentDate = Convert.ToDateTime(TextBoxShipmentDate.Text + " " + TextBoxShipmentTime.Text);
            //DateTime shipmentDate = Convert.ToDateTime(DateTime.Today.ToShortDateString());
            string remarks = TextBoxRemarks.Text;

            if (ds.InsertInboundShipment(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], shipmentDate, remarks) == true)
                Response.Redirect("InboundShipmentLink.aspx");
            else
                Response.Write("There was an error!");

            Master.MsgText = "Added"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("InboundDocumentSearch" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonAdd_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                //result = cqexception.GenericInboundErrorHandling(0, ex, "101");
                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "InboundDocumentSearch", theErrMethod, ex);
            }

            // //LiteralMsg.Text = result;
            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;
            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;
                CQExceptionLayer cqexception = new CQExceptionLayer();

                //result = SendErrorNow("InboundDocumentCreate" + "_" + exMsg.Message.ToString());
                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "InboundDocumentSearch", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return result;
            }
            else
            {
                //LiteralMsg.Text = "WARNING";  
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}