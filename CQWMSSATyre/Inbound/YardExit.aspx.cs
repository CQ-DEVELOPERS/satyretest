using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;
using System.IO;

namespace Telerik.GridExamplesCSharp.Programming.SavingGridSettingsOnPerUserBasis
{
    public partial class DefaultCS : System.Web.UI.Page
    {
        #region InitializeCulture
        protected virtual void InitializeCulture()
        {
            try
            {
                if (Session["CultureName"] != null)
                {
                    System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                }
            }
            catch { }
        }
        #endregion "InitializeCulture"

        #region Private Variables
        private string result = "";
        private string theErrMethod = "";
        #endregion Private Variables

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            theErrMethod = "Page Load";

            try
            {
                if (!Page.IsPostBack)
                {
                }

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
                Master.ErrorText = result;
            }

        }
        #endregion "Page_Load"

        #region Page_LoadComplete
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            try
            {
            }
            catch { }
        }
        #endregion Page_LoadComplete

        #region ShowAlertMessage
        public static void ShowAlertMessage(string error)
        {
            Page page = HttpContext.Current.Handler as Page;

            if (page != null)
            {
                error = error.Replace("'", "\'");

                ScriptManager.RegisterStartupScript(page, page.GetType(), "err_msg", "alert('" + error + "');", true);
            }
        }
        #endregion

        #region ButtonOut_Click
        protected void ButtonOut_Click(object sender, EventArgs e)
        {
            try
            {
                YardManagement ym = new YardManagement();

                if(ym.ContainerOut(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], RadTextBoxBarcode.Text) == 0)
                    ShowAlertMessage("Container scanned out successfully.");
                else
                    ShowAlertMessage("Container match not found!");

                RadTextBoxBarcode.Text = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
                Master.ErrorText = result;
            }
        }
        #endregion ButtonOut_Click

        #region ErrorHandling
        private string SendErrorNow(string ex)
        {

            try
            {
                int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

                if (loopPrevention == 0)
                {
                    CQExceptionLayer cqexception = new CQExceptionLayer();

                    result = cqexception.GenericInboundErrorHandling(5, "ReceivingDocument", theErrMethod, ex);
                }

                Session["countLoopsToPreventInfinLoop"] = "0";

                // throw new System.Exception();  
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

            }
            catch (Exception exMsg)
            {

                int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

                if (loopPrevention == 0)
                {
                    CQExceptionLayer cqexception = new CQExceptionLayer();

                    Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                    result = cqexception.GenericInboundErrorHandling(3, "ReceivingDocument", theErrMethod, exMsg.Message.ToString());

                    Master.ErrorText = result;

                    loopPrevention++;

                    Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                    return result;
                }
                else
                {
                    loopPrevention++;

                    Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                    return "Please refresh this page.";
                }

            }

            return result;
        }
        #endregion ErrorHandling
    }
}