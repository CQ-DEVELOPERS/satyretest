using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;

public partial class Outbound_DockSchedule : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["ConnectionStringName"] == null)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            return;
        }
    }

    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Init";

        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Default" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page Load

    #region RadSchedulerAllBays_AppointmentDataBound
    protected void RadSchedulerAllBays_AppointmentDataBound(object sender, SchedulerEventArgs e)
    {
        //rsCategoryDarkBlue
        //rsCategoryBlue
        //rsCategoryDarkGreen
        //rsCategoryGreen
        //rsCategoryDarkRed
        //rsCategoryOrange
        //rsCategoryPink
        //rsCategoryRed
        //rsCategoryViolet
        //rsCategoryYellow

        //if (e.Appointment.Description == "Confirmed" || e.Appointment.Description == "Container Scanned In")
        //    e.Appointment.CssClass = "rsCategoryGreen";

        //if (e.Appointment.Description == "Unconfirmed")
        //    e.Appointment.CssClass = "rsCategoryBlue";

        //if (e.Appointment.Description == "Late")
        //    e.Appointment.CssClass = "rsCategoryRed";

        //if (e.Appointment.CssClass = ""
    }
    #endregion RadSchedulerAllBays_AppointmentDataBound

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();
                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "Default", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "Default", theErrMethod, exMsg.Message.ToString());
                
                Master.ErrorText = result;
  
                loopPrevention++;
  
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
        
}
