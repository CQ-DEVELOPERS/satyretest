<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReceiveIntoWarehouse.aspx.cs" Inherits="Inbound_ReceiveIntoWarehouse"
    Title="<%$ Resources:Default, ReceiveIntoWarehouseTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReceiveIntoWarehouseTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceiveIntoWarehouseAgenda %>"></asp:Label>
    <br />
    <%--    <script type="text/javascript">       
        function myKeyHandler() 
        {
            //alert(event.keyCode);
            
            if (event.keyCode != null)        
            {
                if (event.keyCode == 13) //Enter
                {
                    document.getElementById("ctl00_ContentPlaceHolderBody_Button1").click();
                }
                if (event.keyCode == 27) //Esc
                {
                    document.getElementById("ctl00_ContentPlaceHolderBody_ButtonAccept").click();
                }
                if (event.keyCode == 32) //Space Bar
                {
                    document.getElementById("ctl00_ContentPlaceHolderBody_ButtonReject").click();
                }
            }
        }
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:HiddenField ID="HiddenFieldProduct" runat="server" />
    <asp:HiddenField ID="HiddenFieldBatch" runat="server" />
    <asp:HiddenField ID="HiddenFieldQuantity" runat="server" />
    <asp:HiddenField ID="HiddenFieldWeight" runat="server" />
    <div style="text-align: center; width: 800px">
        <table>
            <tr>
                <td>
                    <asp:DetailsView ID="DetailsViewInstruction" runat="server" SkinID="ReceiveIntoWarehouse"
                        DataKeyNames="InstructionId,JobId" DataSourceID="ObjectDataSourceInstruction"
                        AutoGenerateRows="False">
                        <Fields>
                            <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                        </Fields>
                    </asp:DetailsView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="ButtonEnter">
                        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                            <asp:View ID="View1" runat="server">
                                <asp:TextBox ID="TextBoxBarcode" runat="server" EnableTheming="false" Width="200px"
                                    Font-Size="X-Large"></asp:TextBox>
                                <br />
                                <asp:Button ID="Button1" runat="server" Visible="false" />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <asp:Label ID="LabelProdut" runat="server" Text="<%$ Resources:Default, ProductCode %>"></asp:Label>
                                <asp:TextBox ID="TextBoxProduct" runat="server" Visible="true" OnTextChanged="TextBoxProduct_TextChanged"></asp:TextBox>
                                <br />
                            </asp:View>
                            <asp:View ID="View3" runat="server">
                                <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch %>"></asp:Label>
                                <asp:TextBox ID="TextBoxBatch" runat="server" Visible="true" OnTextChanged="TextBoxBatch_TextChanged"></asp:TextBox>
                                <br />
                            </asp:View>
                            <asp:View ID="View4" runat="server">
                                <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
                                <asp:TextBox ID="TextBoxQuantity" runat="server" Visible="true" OnTextChanged="TextBoxQuantity_TextChanged"></asp:TextBox>
                                <br />
                            </asp:View>
                            <asp:View ID="View5" runat="server">
                                <asp:Label ID="LabelWeight" runat="server" Text="<%$ Resources:Default, Weight %>"></asp:Label>
                                <asp:TextBox ID="TextBoxWeight" runat="server" Visible="true" OnTextChanged="TextBoxWeight_TextChanged"></asp:TextBox>
                                <br />
                            </asp:View>
                            <asp:View ID="View6" runat="server">
                                <asp:Button ID="ButtonAccept" runat="server" Text="<%$ Resources:Default, ButtonAccept %>"
                                    OnClick="ButtonAccept_Click" EnableTheming="false" Height="80px" Width="200px"
                                    Font-Size="X-Large" BackColor="GreenYellow" />
                            </asp:View>
                            <asp:View ID="View7" runat="server">
                                <asp:RadioButtonList ID="RadioButtonListReason" runat="server" DataSourceID="ObjectDataSourceReason"
                                    DataValueField="ReasonId" DataTextField="Reason" RepeatColumns="2" RepeatDirection="Horizontal"
                                    RepeatLayout="Table">
                                </asp:RadioButtonList>
                                <asp:Button ID="ButtonRejectReason" runat="server" Text="<%$ Resources:Default, ButtonReject %>"
                                    OnClick="ButtonRejectReason_Click" EnableTheming="false" Height="80px" Width="200px"
                                    Font-Size="X-Large" BackColor="Red" />
                            </asp:View>
                        </asp:MultiView>
                        <asp:Button ID="ButtonEnter" runat="server" Text="<%$ Resources:Default, ButtonEnter %>"
                            OnClick="ButtonEnter_Click" EnableTheming="false" Height="80px" Width="200px"
                            Font-Size="X-Large" />
                        <asp:Button ID="ButtonReject" runat="server" Text="<%$ Resources:Default, ButtonReject %>"
                            OnClick="ButtonReject_Click" EnableTheming="false" Height="80px" Width="200px"
                            Font-Size="X-Large" BackColor="Red" />
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="ReceiveIntoWarehouse"
        SelectMethod="GetInstructions">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:ControlParameter ControlID="TextBoxBarcode" Name="barcode" ConvertEmptyStringToNull="true"
                DefaultValue="-1" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
        SelectMethod="GetReasonsByType">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="RW" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
