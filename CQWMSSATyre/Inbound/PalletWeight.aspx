<%@ Page Language="C#" MasterPageFile="~/MasterPages/Empty.master" AutoEventWireup="true"
    CodeFile="PalletWeight.aspx.cs" Inherits="Inbound_ReceiveIntoWarehouse" Title="<%$ Resources:Default, ReceiveIntoWarehouseTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/Empty.master" %>
<%@ Register Src="../Common/PalletWeight.ascx" TagName="PalletWeight" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <%--<asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReceiveIntoWarehouseTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceiveIntoWarehouseAgenda %>"></asp:Label>
    <br />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <div style="text-align: center; width: 500px">
        <table>
            <tr>
                <td valign="top">
                    <asp:DetailsView ID="DetailsViewInstruction" runat="server" SkinID="ReceiveIntoWarehouse"
                        DataKeyNames="InstructionId,JobId" DataSourceID="ObjectDataSourceInstruction"
                        AutoGenerateRows="False">
                        <Fields>
                            <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                        </Fields>
                    </asp:DetailsView>
                </td>
                <td valign="top">
                    <uc1:PalletWeight ID="PalletWeight1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="ButtonEnter">
                        <asp:TextBox ID="TextBoxBarcode" runat="server" EnableTheming="false" Width="200px"
                            Font-Size="X-Large"></asp:TextBox>
                        <br />
                        <asp:Button ID="ButtonEnter" runat="server" Text="<%$ Resources:Default, ButtonEnter %>"
                            OnClick="ButtonEnter_Click" EnableTheming="false" Height="80px" Width="200px"
                            Font-Size="X-Large" />
                        <asp:Button ID="ButtonNext" runat="server" Text="<%$ Resources:Default, ButtonNext %>"
                            OnClick="ButtonNext_Click" EnableTheming="false" Height="80px" Width="200px"
                            Font-Size="X-Large" />
                        <asp:Button ID="Button1" runat="server" Visible="false" />
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="PalletWeight"
        SelectMethod="GetInstructions">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="barcode" SessionField="Barcode" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
