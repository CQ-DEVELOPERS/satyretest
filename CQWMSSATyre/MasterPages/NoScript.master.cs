using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPages_MasterPage : System.Web.UI.MasterPage
{
    #region MsgText
    public String MsgText
    {
        get
        {
            return Msg.Text;
        }
        set
        {
            Msg.Text = value;
        }
    }
    #endregion MsgText

    #region ErrorText
    public String ErrorText
    {
        get
        {
            return ErrorMesg.Text;
        }
        set
        {
            ErrorMesg.Text = value;
        }
    }
    #endregion ErrorText

    #region Page_PreRender
    protected void Page_PreRender(object sender, EventArgs e)
    {
        // Make it Safari and Chrome compatible
        if (Request.UserAgent != null && (Request.UserAgent.IndexOf("AppleWebKit") > 0))
        {
            Request.Browser.Adapters.Clear();
        }
    }
    #endregion Page_PreRender

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Context.Session.IsNewSession)
            {
                FormsAuthentication.SignOut();
                Response.Redirect("~/Security/Login.aspx");
            }
            else
            {
                // Acquire Auth Ticket from the FormsIdentity object
                FormsAuthenticationTicket ticket = ((FormsIdentity)Context.User.Identity).Ticket;

                // Manually slide the expiration
                FormsAuthentication.RenewTicketIfOld(ticket);
            }

            if (!Page.IsPostBack)
            {
                //if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
                //{
                //    Request.Browser.Adapters.Clear();
                //}

                if (Session["WarehouseId"] != null)
                    DropDownListWarehouse.SelectedValue = Session["WarehouseId"].ToString();

                if (Session["ConnectionStringName"] == null)
                {
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                    return;
                }
                else
                    LabelDatabase.Text = Session["ConnectionStringName"].ToString();

                if (Session["OperatorGroupCode"] != null)
                    if (Session["OperatorGroupCode"].ToString() == "A" || Session["OperatorGroupCode"].ToString() == "S")
                        DropDownListWarehouse.Enabled = true;

                MenuItemsDynamic ds = new MenuItemsDynamic();

                int operatorId;

                if (Session["MenuId"] == null)
                    Session["MenuId"] = 1;

                if (Session["OperatorId"] == null)
                    operatorId = -1;
                else
                    operatorId = (int)Session["OperatorId"];

                xmlDataSource.Data = ds.GetMenuItems(Session["ConnectionStringName"].ToString(), (int)Session["MenuId"], operatorId, "Desktop").GetXml();
                xmlDataSource.EnableCaching = false;
                xmlDataSource.DataBind();
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region xmlDataSource_Transforming
    protected void xmlDataSource_Transforming(object sender, EventArgs e)
    {
    }
    #endregion xmlDataSource_Transforming

    #region DropDownListWarehouse_OnSelectedIndexChanged
    protected void DropDownListWarehouse_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Warehouse w = new Warehouse();
            Session["WarehouseId"] = int.Parse(DropDownListWarehouse.SelectedValue);

            w.SetWarehouseForOperator(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"]);

            MsgText = "";
            ErrorMesg.Text = "";
        }
        catch (Exception ex)
        {
            ErrorMesg.Text = ex.Message.ToString();
        }
    }
    #endregion DropDownListWarehouse_OnSelectedIndexChanged

    #region LoginStatus1_LoggingOut
    protected void LoginStatus1_LoggingOut(Object sender, System.EventArgs e)
    {
        // Perform any post-logout processing, such as setting the
        // user's last logout time or clearing a per-user cache of 
        // objects here.
        try
        {
            LogonCredentials lc = new LogonCredentials();

            lc.UserLoggedOut(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], "Desktop");
        }
        catch (Exception ex)
        {
            Session["LogonErrorMessage"] = ex.Message;

            FormsAuthentication.SignOut();
            Session.Abandon();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
    #endregion LoginStatus1_LoggingOut

    #region LoginStatus1_LoggedOut
    protected void LoginStatus1_LoggedOut(Object sender, System.EventArgs e)
    {
        // Perform any post-logout processing, such as setting the
        // user's last logout time or clearing a per-user cache of 
        // objects here.
        try
        {
            Session.Abandon();
        }
        catch (Exception ex)
        {
            Session["LogonErrorMessage"] = ex.Message;

            FormsAuthentication.SignOut();
            Session.Abandon();
            FormsAuthentication.RedirectToLoginPage();
        }
    }
    #endregion LoginStatus1_LoggedOut
}
