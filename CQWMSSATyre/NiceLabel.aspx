<%@ Page Language="C#" AutoEventWireup="false" CodeFile="NiceLabel.aspx.cs" Inherits="_NiceLabel"
    ValidateRequest="false" EnableEventValidation="false" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="Common\NLWAX.ascx" TagName="NLWAX" TagPrefix="nl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Label Preview & Print</title>
    <link rel="SHORTCUT ICON" href="images/label.ico" />

    <script type="text/javascript" language="javascript" src="javascript/global.js"></script>

    <script type="text/javascript" language="javascript" src="javascript/port.js"></script>

    <script type="text/javascript" language="javascript" src="javascript/printer.js"></script>

</head>
<body onload="AddClientPrinters()">
    <form id="form1" runat="server">
        <!-- Add the NLWAX1 User Control to form -->
        <nl:NLWAX ID="NLWAX1" runat="server" />
        
        
        <table>
            <tr valign="top">
                <td>
                    <h3>Available Printers</h3>
                    <asp:ListBox ID="lstPrinters" runat="server" />
                </td>
                <td>
                    <h3>Printing Options</h3>
                    <table class="innerTable">
                        <tr>
                            <td>
                                Label:
                            </td>
                            <td class="textRight">
                                <asp:Label ID="lblLabel" ForeColor="red" runat="server" Text="No Label Selected" />
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 21px">
                                Printer:
                            </td>
                            <td class="textRight" style="height: 21px">
                                <span id="spPrinter" style="color: Red;">No Printer Selected</span>
                            </td>
                        </tr>
                        <tr>
                            <td id="spPort">
                                Port
                            </td>
                            <td class="textRight">
                                <div id="divLabel" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Print Quantity
                                <asp:TextBox Width="40" ID="txtQuant" Font-Size="10pt" runat="server" Text="1" />
                            </td>
                            <td class="textRight">
                                <asp:Button ID="btnReprint" runat="server" Text="Reprint" Visible="false" OnClick="btnReprint_Click" />
                                <asp:Button ID="btnPrint" runat="server" Text="Print Label" Enabled="false" OnClick="btnPrint_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td class="textRight">
                                <asp:Button ID="btnDirectPrint" runat="server" Text="Print to Server printer" OnClick="btnDirectPrint_Click" />
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblError" ForeColor="Red" runat="server" Text="No error" Visible="False" />
                </td>
            </tr>
            <tr valign="top">
                <td>
                    <h3>Labels</h3>
                    <asp:TreeView ID="trvLabels" runat="server" CollapseImageUrl="~/images/OPENFOLD.BMP"
                        ExpandImageUrl="~/images/CLSDFOLD.BMP" NoExpandImageUrl="~/images/Label.bmp" OnSelectedNodeChanged="trvLabels_SelectedNodeChanged">
                    </asp:TreeView>
                </td>
                <td>
                    <h3>Label Preview</h3>
                    <asp:Image ID="imgPreview" AlternateText="No image loaded" runat="server" Height="314px"
                        Width="382px" EnableViewState="true" />
                </td>
            </tr>
        </table>
        <asp:Label ID="lblVariables" Text="Label Variables - No Variables Present" runat="server" />
        <asp:Button ID="btnExpand" runat="server" Text="-" ToolTip="Hide Extended Properties" OnClick="btnExpand_Click" />
        <asp:GridView ID="grvVars" runat="server" AutoGenerateColumns="False" EnableViewState="true">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" />
                <asp:BoundField DataField="Prompt" HeaderText="Prompt" ReadOnly="True" />
                <asp:TemplateField HeaderText="Format" Visible="true" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("FormatID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Length" HeaderText="Length" ReadOnly="True" Visible="true"
                    ItemStyle-HorizontalAlign="center" />
                <asp:CheckBoxField DataField="FixedLength" HeaderText="Fixed Length" ReadOnly="true"
                    Visible="true" ItemStyle-HorizontalAlign="center" />
                <asp:CheckBoxField DataField="ValueRequired" HeaderText="Required" ReadOnly="true"
                    Visible="true" ItemStyle-HorizontalAlign="center" />
                <asp:TemplateField HeaderText="Value" ItemStyle-Wrap="true">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Value") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditVal" runat="server" Text='<%# Bind("Value") %>'
                            Width="98%" />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:CommandField EditText="Edit" CancelText="Cancel" UpdateText="Update" HeaderText=""
                    ShowCancelButton="true" ShowEditButton="true" />
            </Columns>
        </asp:GridView>
        
        
        <table class="main">
            <tr>
                <td class="cols">
                    <table class="innerTable">
                        <tr>
                            <td colspan="2">
                                <table class="innerTable">
                                    <tr>
                                        <td class="header">
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="content">
</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="cols">
                                <table class="innerTable">
                                    <tr>
                                        <td class="header">
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="content">

                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="cols">
                                <table class="innerTable">
                                    <tr>
                                        <td class="header">
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="content">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="cols">
                    <table class="innerTable">
                        <tr>
                            <td>
                                <table class="innerTable">
                                    <tr>
                                        <td class="header">
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="content" style="text-align: center;">

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="innerTable">
                        <tr>
                            <td class="header">
                                <table class="innerTable">
                                    <tr>
                                        <td>
                                            
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="content">

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
