﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Billing_BillProcessing : System.Web.UI.Page
{
  protected void Page_Load( object sender, EventArgs e )
  {

  }
  protected void ButtonDocumentSearch_Click( object sender, EventArgs e )
  {
    BillingDatasource.DataBind();
  }

  protected void btnCreateBill_Click( object sender, EventArgs e )
  {
    var ids = new List<object>();

    foreach ( Telerik.Web.UI.GridDataItem item in dgBill.Items )
    {
      var id = int.Parse( item.GetDataKeyValue( "Id" ).ToString() );
      ids.Add( id );
    }

    try
    {
      // Only Create the BilledTransaction if we have records
      if ( ids.Count > 0 )
      {
        var tranId = Billing.CreateBilledTransaction( ids, dtFromDate.SelectedDate.Value, dtToDate.SelectedDate.Value );

        Session["FromUrl"] = HttpContext.Current.Request.Url;
        Session["ReportName"] = "Bill Processing";

        // Create the Report Parameters holder
        var reportParameters = new List<ReportParameter>();

        // Create the BilledTransactionId (Id) report parameter
        reportParameters.Add(new ReportParameter( "Id", tranId.ToString() ) );
        reportParameters.Add(new ReportParameter("ServerName", Session["ServerName"].ToString()));
        reportParameters.Add(new ReportParameter("DatabaseName", Session["DatabaseName"].ToString()));
        reportParameters.Add(new ReportParameter("UserName", Session["UserName"].ToString()));
        Session["ReportParameters"] = reportParameters.ToArray();

        Response.Redirect( "~/Reports/Report.aspx" );
      }

    }
    catch ( Exception ex )
    {
      System.Windows.Forms.MessageBox.Show( ex.Message, "Error" );
    }


  }
}