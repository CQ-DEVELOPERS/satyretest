﻿<%@ Page Language="C#"
  AutoEventWireup="true"
  MasterPageFile="~/MasterPages/MasterPage.master"
  CodeFile="BillProcessing.aspx.cs"
  Inherits="Billing_BillProcessing" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
  <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, BillingTitle %>"></asp:Label>
  <br />
  <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchAgenda %>"></asp:Label>
  <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">

  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="RadGridBills">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridBill" />
          <telerik:AjaxUpdatedControl ControlID="RadGridBillDetails" />
        </UpdatedControls>
      </telerik:AjaxSetting>

      <%--      
      <telerik:AjaxSetting AjaxControlID="ButtonDocumentSearch" EventName="Click">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      
      <telerik:AjaxSetting AjaxControlID="RadGridOrderLines">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="ButtonPalletise" EventName="Click">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="ButtonAutoLocations" EventName="Click">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="ButtonDefaultDelivery" EventName="Click">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="ButtonDefaultReceived" EventName="Click">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="ButtonDefaultZero" EventName="Click">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="ButtonPrintLabel" EventName="Click">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="ButtonTicketLabel" EventName="Click">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="ButtonSample" EventName="Click">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="DetailsLinesReceived" EventName="Click">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="ButtonPODs" />
          <telerik:AjaxUpdatedControl ControlID="ButtonAutoLocations" />
          <telerik:AjaxUpdatedControl ControlID="ButtonPalletises" />
          <telerik:AjaxUpdatedControl ControlID="ButtonManualLocationss" />
          <telerik:AjaxUpdatedControl ControlID="PanelDefaults" />
          <telerik:AjaxUpdatedControl ControlID="ButtonDefaultReceiveds" />
          <telerik:AjaxUpdatedControl ControlID="ButtonDefaultDeliverys" />
          <telerik:AjaxUpdatedControl ControlID="ButtonDefaultZeros" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="RadGridJobs">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridJobs" />
          <telerik:AjaxUpdatedControl ControlID="RadGridDetails" />
        </UpdatedControls>
      </telerik:AjaxSetting>
      <telerik:AjaxSetting AjaxControlID="RadGridDetails">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridDetails" />
        </UpdatedControls>
      </telerik:AjaxSetting>--%>
    </AjaxSettings>
  </telerik:RadAjaxManager>

  <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
  </telerik:RadAjaxLoadingPanel>

  <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
      function onSelectedIndexChanged(sender, eventArgs) {
        var selectedItem = eventArgs.get_item();
        var selectedItemText = selectedItem != null ? selectedItem.get_text() : sender.get_text();
        __doPostBack('', '');
      }
    </script>

  </telerik:RadScriptBlock>

  <div style="float: left;">
    <asp:Panel ID="PanelSearch" runat="server" Width="100%" BackColor="#EFEFEC">
      <table>
        <tr>
          <td style="height: 53px">
            <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>" Width="100px"></asp:Label>
          </td>
          <td style="height: 53px">
            <telerik:RadComboBox ID="cboPrincipal" Width="80%" runat="server"
              DataSourceID="ObjectDataSourcePrincipal" DataTextField="Principal" DataValueField="PrincipalId" AutoPostBack="False">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal" SelectMethod="GetAllPrincipals" OldValuesParameterFormatString="original_{0}">
            </asp:ObjectDataSource>
          </td>
          <td style="height: 53px">
            <asp:Label ID="lblFromDate" runat="server" Text="<%$ Resources:Default, FromDate%>" Width="100px"></asp:Label>
          </td>
          <td style="height: 53px">
            <telerik:RadDatePicker ID="dtFromDate" runat="server" Width="80%" Culture="en-ZA">
            </telerik:RadDatePicker>

          </td>
          <td style="height: 53px">
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, ToDate%>" Width="100px"></asp:Label>
          </td>
          <td style="height: 53px">
            <telerik:RadDatePicker ID="dtToDate" runat="server" Width="80%">
            </telerik:RadDatePicker>
          </td>

        </tr>

        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="dtFromDate" ErrorMessage="From Date is required"></asp:RequiredFieldValidator>
          </td>
          <td></td>
          <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="dtToDate" ErrorMessage="To Date is required"></asp:RequiredFieldValidator>
          </td>
        </tr>
      </table>
    </asp:Panel>

    <telerik:RadButton ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search %>" OnClick="ButtonDocumentSearch_Click" AutoPostBack="true" />
    <telerik:RadButton ID="btnCreateBill" runat="server" Text="<%$ Resources:Default, CreateBill%>" OnClick="btnCreateBill_Click" />

    <telerik:RadGrid runat="server" AutoGenerateColumns="False" CellSpacing="0" DataSourceID="BillingDatasource" GridLines="None" ID="dgBill" AllowMultiRowSelection="True" AllowSorting="True">

      <ExportSettings>
        <Pdf>
          <PageHeader>
            <LeftCell Text=""></LeftCell>

            <MiddleCell Text=""></MiddleCell>

            <RightCell Text=""></RightCell>
          </PageHeader>

          <PageFooter>
            <LeftCell Text=""></LeftCell>

            <MiddleCell Text=""></MiddleCell>

            <RightCell Text=""></RightCell>
          </PageFooter>
        </Pdf>
      </ExportSettings>

      <ClientSettings>
        <Selecting AllowRowSelect="True" />
      </ClientSettings>

      <MasterTableView DataSourceID="BillingDatasource" DataKeyNames="Id" GroupsDefaultExpanded="false">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
          <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
          <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

        <GroupByExpressions>
          <telerik:GridGroupByExpression>
            <SelectFields>
              <telerik:GridGroupByField FormatString="<strong style='color:blue'>{0}</strong>" FieldName="TranType" HeaderText="Type" />
              <telerik:GridGroupByField FieldAlias="Total" Aggregate="Sum" FieldName="Total" FormatString="<strong >{0:N2}</strong>" />
            </SelectFields>
            <GroupByFields>
              <telerik:GridGroupByField FieldName="TranType" SortOrder="Ascending" />
            </GroupByFields>
          </telerik:GridGroupByExpression>
        </GroupByExpressions>


        <Columns>
          <telerik:GridBoundColumn DataField="TranType" FilterControlAltText="Filter TranType column" HeaderText="Bill Type" ReadOnly="True" SortExpression="TranType" UniqueName="TranType">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" Display="False" FilterControlAltText="Filter Id column" HeaderText="UniqueId" ReadOnly="True" SortExpression="Id" UniqueName="Id" Visible="False">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="BillId" FilterControlAltText="Filter BillId column" HeaderText="Document Id" SortExpression="BillId" UniqueName="BillId">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="Shipment" FilterControlAltText="Filter Shipment column" HeaderText="Shipment" SortExpression="Shipment" UniqueName="Shipment">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="ReferenceNumber" FilterControlAltText="Filter ReferenceNumber column" HeaderText="Reference No." SortExpression="ReferenceNumber" UniqueName="ReferenceNumber">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="DocumentType" FilterControlAltText="Filter DocumentType column" HeaderText="Document Type" SortExpression="DocumentType" UniqueName="DocumentType">
          </telerik:GridBoundColumn>
          <telerik:GridBoundColumn DataField="VechicalType" FilterControlAltText="Filter VehicleType column" HeaderText="Vehicle Type" SortExpression="VehicleType" UniqueName="VehicleType">
          </telerik:GridBoundColumn>
          <telerik:GridNumericColumn NumericType="Currency" DecimalDigits="2" DataField="Total" DataType="System.Double" FilterControlAltText="Filter Total column" HeaderText="Total" ReadOnly="True" SortExpression="Total" UniqueName="Total">
          </telerik:GridNumericColumn>
          <telerik:GridBoundColumn DataField="TransactedOn" DataType="System.DateTime" FilterControlAltText="Filter TransactedOn column" HeaderText="Created On" SortExpression="TransactedOn" UniqueName="TransactedOn">
          </telerik:GridBoundColumn>
        </Columns>

        <EditFormSettings>
          <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>

        <BatchEditingSettings EditType="Cell"></BatchEditingSettings>

        <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
      </MasterTableView>

      <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

      <FilterMenu EnableImageSprites="False"></FilterMenu>

    </telerik:RadGrid>
    <asp:ObjectDataSource ID="BillingDatasource" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetBillableTransactions" TypeName="Billing">
      <SelectParameters>
        <asp:ControlParameter ControlID="cboPrincipal" Name="PrincipalId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="dtFromDate" Name="fromDate" PropertyName="SelectedDate" Type="DateTime" />
        <asp:ControlParameter ControlID="dtToDate" Name="toDate" PropertyName="SelectedDate" Type="DateTime" />
        <asp:Parameter Name="status" Type="Object" DefaultValue="0" />
      </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <telerik:RadButton ID="ButtonSaveSettings" runat="server" Text="Save Layout Settings" />
  </div>
</asp:Content>

