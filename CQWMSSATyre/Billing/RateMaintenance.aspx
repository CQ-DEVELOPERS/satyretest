﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RateMaintenance.aspx.cs" Inherits="Billing_RateMaintenance"
  Title="<%$ Resources:Default, RateDefaultMaintenanceTitle %>" StylesheetTheme="Default" Theme="Default"
  MasterPageFile="~/MasterPages/MasterPage.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
  <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, RateDefault %>"></asp:Label>
  <br />
  <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReceivingDocumentSearchAgenda %>"></asp:Label>
  <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">

  <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="RadGridrateDefaults">
        <UpdatedControls>
          <telerik:AjaxUpdatedControl ControlID="RadGridrateDefault" />
        </UpdatedControls>
      </telerik:AjaxSetting>
    </AjaxSettings>
  </telerik:RadAjaxManager>

  <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
  </telerik:RadAjaxLoadingPanel>

  <asp:Panel runat="server">
    <table>
      <tr>
        <td style="height: 54px">
          <asp:Label ID="LabelPrincipal" runat="server" Text="<%$ Resources:Default, Principal%>" Width="100px"></asp:Label>
        </td>
        <td style="height: 54px">
          <telerik:RadComboBox ID="cboPrincipal" Width="80%" runat="server" DataSourceID="ObjectDataSourcePrincipal" DataTextField="Principal" DataValueField="PrincipalId" AutoPostBack="False" />
          <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal" SelectMethod="GetAllPrincipals" OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>
        </td>
        <td style="height: 54px">
          <asp:Label ID="lblBillingCategory" runat="server" Text="<%$ Resources:Default, BillingCategoryId%>" Width="100px"></asp:Label>
        </td>
        <td style="height: 54px">
          <telerik:RadComboBox ID="cboBillingCategory" runat="server" DataSourceID="BillingCategoryDataSource"
            DataTextField="Name" DataValueField="Id" Width="80%">
          </telerik:RadComboBox>
          <asp:ObjectDataSource ID="BillingCategoryDataSource" runat="server" TypeName="BillingCategory" SelectMethod="GetBillingCategoryByPrincipal" OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
              <asp:ControlParameter ControlID="cboPrincipal" Name="principalId" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
          </asp:ObjectDataSource>
        </td>
          <td>
              <telerik:RadButton ID="RadButton1" runat="server" Text="Search" />
          </td>
      </tr>
    </table>

  </asp:Panel>

  <%--</asp:Panel>--%>

  <telerik:RadGrid ID="gridRate" runat="server" DataSourceID="rateDataSource" AllowSorting="True" CellSpacing="0"
    AutoGenerateColumns="False" GroupingEnabled="False" GridLines="None" OnSelectedIndexChanged="gridRate_SelectedIndexChanged">
    <MasterTableView Width="100%" CommandItemDisplay="Top" HorizontalAlign="Left" DataKeyNames="Id" DataSourceID="rateDataSource">
      <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>
      <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>
      <Columns>
        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Select" Display="true" Text="Select" FilterControlAltText="Filter editColumn column" UniqueName="editColumn" />
        <telerik:GridBoundColumn DataField="Id" HeaderText="Id" UniqueName="idColumn" Visible="False" FilterControlAltText="Filter Id column" ReadOnly="True" Display="False" InsertVisiblityMode="AlwaysHidden" />
        <telerik:GridBoundColumn DataField="RateId" HeaderText="Rate Id" UniqueName="rateIdColumn" Visible="True" FilterControlAltText="Filter Rate Id column" ReadOnly="False" Display="True" />
        <telerik:GridDropDownColumn DataField="PrincipalId" DataSourceID="ObjectDataSourcePrincipal" FilterControlAltText="Filter Principal column" HeaderText="Principal" ListTextField="Principal" ListValueField="PrincipalId" UniqueName="cboPrincipalColumn" ReadOnly="False"></telerik:GridDropDownColumn>
        <telerik:GridDropDownColumn DataField="BillingCategoryId" DataSourceID="billingCategoryDataSource" FilterControlAltText="Filter Billing Category column" HeaderText="Billing Category" ListTextField="Name" ListValueField="Id" UniqueName="cboBillingCategoryColumn"></telerik:GridDropDownColumn>

        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Edit" Display="true" Text="Edit" FilterControlAltText="Filter editColumn column" UniqueName="editColumn" />
        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Delete" Display="true" Text="Delete" ItemStyle-HorizontalAlign="Left" FilterControlAltText="Filter deleteColumn column" UniqueName="deleteColumn">
          <ItemStyle HorizontalAlign="Left"></ItemStyle>
        </telerik:GridButtonColumn>
      </Columns>

      <EditFormSettings ColumnNumber="8" CaptionFormatString="Edit details for Rate Default">
        <FormTableItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True"></FormTableItemStyle>
        <FormCaptionStyle CssClass="EditFormHeader"></FormCaptionStyle>
        <FormMainTableStyle CellSpacing="0" CellPadding="1" HorizontalAlign="Left" />
        <FormTableStyle CellSpacing="0" CellPadding="1" CssClass="module"
          Height="100%" />
        <FormTableAlternatingItemStyle Wrap="True" HorizontalAlign="Left"></FormTableAlternatingItemStyle>
        <FormStyle Width="100%" BackColor="#EEF2EA"></FormStyle>
        <EditColumn UpdateText="Save Changes" UniqueName="EditCommandColumn1" CancelText="Cancel edit" />
        <FormTableButtonRowStyle HorizontalAlign="Left" VerticalAlign="Middle" CssClass="EditFormButtonRow" />
      </EditFormSettings>

      <BatchEditingSettings EditType="Cell"></BatchEditingSettings>

      <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

    </MasterTableView>

    <ClientSettings AllowColumnsReorder="True">
      <ClientEvents></ClientEvents>
    </ClientSettings>

    <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

    <FilterMenu EnableImageSprites="False"></FilterMenu>
  </telerik:RadGrid>


  <asp:ObjectDataSource runat="server" ID="rateDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetRateByPrincipal" TypeName="Rate">

    <SelectParameters>
      <asp:ControlParameter ControlID="cboPrincipal" Name="principalId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
  </asp:ObjectDataSource>
  <asp:ObjectDataSource runat="server" ID="documentTypesDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDocumentTypes" TypeName="DocumentType" />
  <asp:ObjectDataSource runat="server" ID="vehicleTypesDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetVehicleTypes" TypeName="VehicleType" />
  <asp:ObjectDataSource runat="server" ID="areaDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetAreasByWarehouse" TypeName="Area">
    <SelectParameters>
      <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:SessionParameter Name="warehouseId" SessionField="warehouseId" Type="Int32" />
    </SelectParameters>
  </asp:ObjectDataSource>
  <asp:ObjectDataSource runat="server" ID="billingEventDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetBillingEvents" TypeName="BillingEvent" />

  <telerik:RadTabStrip runat="server" ID="tabsRateDetail"
    SelectedIndex="0" MultiPageID="Tabs" OnTabClick="tabsRateDetail_TabClick">
    <Tabs>
      <telerik:RadTab Text="<%$ Resources:Default, RateMaintenance_Inbound%>" TabIndex="1" Selected="True" />
      <telerik:RadTab Text="<%$ Resources:Default, RateMaintenance_Outbound%>" TabIndex="2" />
      <telerik:RadTab Text="<%$ Resources:Default, RateMaintenance_Storage%>" TabIndex="3" />
    </Tabs>
  </telerik:RadTabStrip>
  <!--no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace -->
  <telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt" Visible="False">
    <telerik:RadPageView runat="Server" ID="tpnlInbound" HeaderText="<%$ Resources:Default, RateMaintenance_Inbound%>">
      <asp:Panel ID="InboundPanel" runat="server">
        <asp:ObjectDataSource runat="server" ID="rateDetailInboundDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetByRateIdAndType" TypeName="RateDetail" InsertMethod="Update" UpdateMethod="Update">
          <InsertParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter Name="shipment" Type="Double" />
            <asp:Parameter Name="Documentation" Type="Double" />
            <asp:SessionParameter Name="rateId" SessionField="RateId" Type="Int32" />
            <asp:Parameter DefaultValue="1" Name="type" Type="Int32" />
            <asp:Parameter Name="storageDays" Type="Byte" DefaultValue="0" />
          </InsertParameters>
          <SelectParameters>
            <asp:SessionParameter Name="rateId" SessionField="RateId" Type="Int32" />
            <asp:Parameter DefaultValue="1" Name="type" Type="Int32" />
          </SelectParameters>
          <UpdateParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter DefaultValue="1" Name="type" Type="Int32" />
            <asp:Parameter Name="shipment" Type="Double" />
            <asp:Parameter Name="documentation" Type="Double" />
            <asp:SessionParameter SessionField="RateId" Name="RateId" />
            <asp:Parameter Name="storageDays" Type="Byte" DefaultValue="0" />
          </UpdateParameters>
        </asp:ObjectDataSource>
        <asp:DetailsView ID="DetailsViewInbound" DataKeyNames="Id" runat="server" Width="250px" DataSourceID="rateDetailInboundDataSource" EnableModelValidation="True" AutoGenerateEditButton="True" AutoGenerateRows="False" SkinID="ReceiveIntoWarehouse" OnDataBound="DetailsViewInbound_DataBound">
          <Fields>
            <asp:BoundField DataField="Shipment" DataFormatString="{0:N2}" HeaderText="Shipment" />
            <asp:BoundField DataField="Documentation" DataFormatString="{0:N2}" HeaderText="Documentation" />
            <asp:BoundField DataField="Type" HeaderText="Type" Visible="false" ShowHeader="false" />
            <asp:BoundField DataField="RateId" HeaderText="RateId" Visible="false" ShowHeader="false" />
          </Fields>
        </asp:DetailsView>
        <asp:Panel runat="server">

          <asp:ObjectDataSource runat="server" ID="rateDetailLineInboundDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetByRateDetailId" TypeName="RateDetailLine" InsertMethod="Update" UpdateMethod="Update">
            <InsertParameters>
              <asp:Parameter Name="id" Type="Int32" />
              <asp:Parameter Name="weightInKg" Type="Double" />
              <asp:Parameter Name="numberOfUnits" Type="Double" />
              <asp:Parameter Name="cubeInMeters" Type="Double" />
              <asp:Parameter Name="numberOfPallets" Type="Double" />
              <asp:Parameter Name="numberOfCartons" Type="Double" />
              <asp:Parameter Name="flatRateAmount" Type="Double" />
              <asp:Parameter Name="minimumWeightInKg" Type="Double" />
              <asp:SessionParameter Name="rateDetailId" Type="Int32" SessionField="RATEDETAIL_ID_INBOUND" />
              <asp:Parameter Name="billingEventId" Type="Int32" />
              <asp:Parameter Name="areaId" Type="Int32" />
              <asp:Parameter Name="original_Id" Type="Int32" />
            </InsertParameters>
            <SelectParameters>
              <asp:SessionParameter Name="rateDetailId" SessionField="RATEDETAIL_ID_INBOUND" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
              <asp:Parameter Name="id" Type="Int32" />
              <asp:Parameter Name="weightInKg" Type="Double" />
              <asp:Parameter Name="numberOfUnits" Type="Double" />
              <asp:Parameter Name="cubeInMeters" Type="Double" />
              <asp:Parameter Name="numberOfPallets" Type="Double" />
              <asp:Parameter Name="numberOfCartons" Type="Double" />
              <asp:Parameter Name="flatRateAmount" Type="Double" />
              <asp:Parameter Name="minimumWeightInKg" Type="Double" />
              <asp:SessionParameter Name="rateDetailId" Type="Int32" SessionField="RATEDETAIL_ID_INBOUND" />
              <asp:Parameter Name="billingEventId" Type="Int32" />
              <asp:Parameter Name="areaId" Type="Int32" />
            </UpdateParameters>
          </asp:ObjectDataSource>


          <telerik:RadGrid ID="gridDetailLineInbound" runat="server" CellSpacing="0" DataSourceID="rateDetailLineInboundDataSource" AllowSorting="True" AutoGenerateColumns="False" GroupingEnabled="False" GridLines="None" AutoGenerateEditColumn="true" AutoGenerateDeleteColumn="true">
            <MasterTableView DataSourceID="rateDetailLineInboundDataSource" CommandItemDisplay="Top" HorizontalAlign="Left" DataKeyNames="Id" AllowAutomaticInserts="true" AllowAutomaticUpdates="true">
              <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px" />
              </RowIndicatorColumn>
              <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                <HeaderStyle Width="20px" />
              </ExpandCollapseColumn>
              <Columns>
                <telerik:GridBoundColumn DataField="Id" FilterControlAltText="Filter Id column" UniqueName="Idcolumn" Display="false" Visible="false" ReadOnly="true" InsertVisiblityMode="AlwaysHidden" />
                <telerik:GridDropDownColumn DataField="AreaId" DataSourceID="areaDataSource" ListTextField="Area" ListValueField="AreaId" HeaderText="Area" />
                <telerik:GridDropDownColumn DataField="BillingEventId" DataSourceID="billingEventDataSource" ListTextField="Name" ListValueField="Id" HeaderText="Billing Event" />
                <telerik:GridNumericColumn DataField="WeightInKg" HeaderText="Weight In Kg" DecimalDigits="2" FilterControlAltText="Filter Weight In Kg column" UniqueName="WeightInKgColumn" />
                <telerik:GridNumericColumn DataField="MinimumWeightInKg" HeaderText="Minimum Weight In Kg" DecimalDigits="2" FilterControlAltText="Filter Minimum Weight In Kg column" UniqueName="MinimumWeightInKgColumn" />
                <telerik:GridNumericColumn DataField="NumberOfUnits" HeaderText="Number Of Units" DecimalDigits="2" FilterControlAltText="Filter Number Of Units column" UniqueName="NumberOfUnitsColumn" />
                <telerik:GridNumericColumn DataField="CubeInMeters" HeaderText="Cube In Meters" DecimalDigits="2" FilterControlAltText="Filter Cube In Meters column" UniqueName="CubeInMetersColumn" />
                <telerik:GridNumericColumn DataField="NumberOfPallets" HeaderText="Number Of Pallets" DecimalDigits="2" FilterControlAltText="Filter Number Of Pallets column" UniqueName="NumberOfPalletsColumn" />
                <telerik:GridNumericColumn DataField="NumberOfCartons" HeaderText="Number Of Cartons" DecimalDigits="2" FilterControlAltText="Filter Number Of Cartons column" UniqueName="NumberOfCartonsColumn" />
                <telerik:GridNumericColumn DataField="FlatRateAmount" HeaderText="Flat-rate Amount" DecimalDigits="2" FilterControlAltText="Filter Flat Rate Amount column" UniqueName="FlatRateAmountColumn" />
              </Columns>
              <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
              </EditFormSettings>
              <BatchEditingSettings EditType="Cell" />
              <PagerStyle PageSizeControlType="RadComboBox" />
            </MasterTableView><PagerStyle PageSizeControlType="RadComboBox" />
            <FilterMenu EnableImageSprites="False"></FilterMenu>
          </telerik:RadGrid>
        </asp:Panel>
      </asp:Panel>
    </telerik:RadPageView>
    <telerik:RadPageView runat="Server" ID="tpnlOutbound" HeaderText="<%$ Resources:Default, RateMaintenance_Outbound%>">
      <asp:Panel ID="OutboundPanel" runat="server">

        <asp:ObjectDataSource runat="server" ID="rateDetailOutboundDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetByRateIdAndType" TypeName="RateDetail" InsertMethod="Update" UpdateMethod="Update">
          <InsertParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter Name="shipment" Type="Double" />
            <asp:Parameter Name="Documentation" Type="Double" />
            <asp:SessionParameter Name="rateId" SessionField="RateId" Type="Int32" />
            <asp:Parameter DefaultValue="2" Name="type" Type="Int32" />
            <asp:Parameter Name="storageDays" Type="Byte" DefaultValue="0" />
          </InsertParameters>
          <SelectParameters>
            <asp:SessionParameter Name="rateId" SessionField="RateId" Type="Int32" />
            <asp:Parameter DefaultValue="2" Name="type" Type="Int32" />
          </SelectParameters>
          <UpdateParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter DefaultValue="2" Name="type" Type="Int32" />
            <asp:Parameter Name="shipment" Type="Double" />
            <asp:Parameter Name="documentation" Type="Double" />
            <asp:SessionParameter SessionField="RateId" Name="RateId" />
            <asp:Parameter Name="storageDays" Type="Byte" DefaultValue="0" />
          </UpdateParameters>
        </asp:ObjectDataSource>

        <asp:DetailsView ID="DetailsViewOutbound" DataKeyNames="Id" runat="server" Width="250px" DataSourceID="rateDetailOutboundDataSource" EnableModelValidation="True" AutoGenerateEditButton="True" AutoGenerateRows="False" SkinID="ReceiveIntoWarehouse" OnDataBound="DetailsViewOutbound_DataBound">
          <Fields>
            <asp:BoundField DataField="Shipment" DataFormatString="{0:N2}" HeaderText="Shipment" />
            <asp:BoundField DataField="Documentation" DataFormatString="{0:N2}" HeaderText="Documentation" />
          </Fields>
        </asp:DetailsView>
        <asp:Panel ID="Panel1" runat="server">

          <asp:ObjectDataSource runat="server" ID="rateDetailLineOutboundDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetByRateDetailId" TypeName="RateDetailLine" InsertMethod="Update" UpdateMethod="Update">
            <InsertParameters>
              <asp:Parameter Name="id" Type="Int32" />
              <asp:Parameter Name="weightInKg" Type="Double" />
              <asp:Parameter Name="numberOfUnits" Type="Double" />
              <asp:Parameter Name="cubeInMeters" Type="Double" />
              <asp:Parameter Name="numberOfPallets" Type="Double" />
              <asp:Parameter Name="numberOfCartons" Type="Double" />
              <asp:Parameter Name="flatRateAmount" Type="Double" />
              <asp:Parameter Name="minimumWeightInKg" Type="Double" />
              <asp:SessionParameter Name="rateDetailId" SessionField="RATEDETAIL_ID_OUTBOUND" Type="Int32" />
              <asp:Parameter Name="billingEventId" Type="Int32" />
              <asp:Parameter Name="areaId" Type="Int32" />
              <asp:Parameter Name="original_Id" Type="Int32" />
            </InsertParameters>
            <SelectParameters>
              <asp:SessionParameter Name="rateDetailId" SessionField="RATEDETAIL_ID_OUTBOUND" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
              <asp:Parameter Name="id" Type="Int32" />
              <asp:Parameter Name="weightInKg" Type="Double" />
              <asp:Parameter Name="numberOfUnits" Type="Double" />
              <asp:Parameter Name="cubeInMeters" Type="Double" />
              <asp:Parameter Name="numberOfPallets" Type="Double" />
              <asp:Parameter Name="numberOfCartons" Type="Double" />
              <asp:Parameter Name="flatRateAmount" Type="Double" />
              <asp:Parameter Name="minimumWeightInKg" Type="Double" />
              <asp:SessionParameter Name="rateDetailId" SessionField="RATEDETAIL_ID_OUTBOUND" Type="Int32" />
              <asp:Parameter Name="billingEventId" Type="Int32" />
              <asp:Parameter Name="areaId" Type="Int32" />
            </UpdateParameters>
          </asp:ObjectDataSource>

          <telerik:RadGrid ID="gridDetailLineOutbound" runat="server" CellSpacing="0" DataSourceID="rateDetailLineOutboundDataSource" GridLines="None" AutoGenerateDeleteColumn="True" AutoGenerateEditColumn="True" AutoGenerateColumns="False">
            <MasterTableView DataSourceID="rateDetailLineOutboundDataSource" CommandItemDisplay="Top" HorizontalAlign="Left" DataKeyNames="Id" AllowAutomaticDeletes="true" AllowAutomaticInserts="true" AllowAutomaticUpdates="true">
              <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px" />
              </RowIndicatorColumn>
              <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                <HeaderStyle Width="20px" />
              </ExpandCollapseColumn>
              <Columns>
                <telerik:GridBoundColumn DataField="Id" FilterControlAltText="Filter Id column" UniqueName="Idcolumn" Display="false" Visible="false" InsertVisiblityMode="AlwaysHidden" />
                <telerik:GridDropDownColumn DataField="AreaId" DataSourceID="areaDataSource" ListTextField="Area" ListValueField="AreaId" HeaderText="Area" />
                <telerik:GridDropDownColumn DataField="BillingEventId" DataSourceID="billingEventDataSource" ListTextField="Name" ListValueField="Id" HeaderText="Billing Event" />
                <telerik:GridNumericColumn DataField="WeightInKg" HeaderText="Weight In Kg" DecimalDigits="2" FilterControlAltText="Filter Weight In Kg column" UniqueName="WeightInKgColumn" />
                <telerik:GridNumericColumn DataField="MinimumWeightInKg" HeaderText="Minimum Weight In Kg" DecimalDigits="2" FilterControlAltText="Filter Minimum Weight In Kg column" UniqueName="MinimumWeightInKgColumn" />
                <telerik:GridNumericColumn DataField="NumberOfUnits" HeaderText="Number Of Units" DecimalDigits="2" FilterControlAltText="Filter Number Of Units column" UniqueName="NumberOfUnitsColumn" />
                <telerik:GridNumericColumn DataField="CubeInMeters" HeaderText="Cube In Meters" DecimalDigits="2" FilterControlAltText="Filter Cube In Meters column" UniqueName="CubeInMetersColumn" />
                <telerik:GridNumericColumn DataField="NumberOfPallets" HeaderText="Number Of Pallets" DecimalDigits="2" FilterControlAltText="Filter Number Of Pallets column" UniqueName="NumberOfPalletsColumn" />
                <telerik:GridNumericColumn DataField="NumberOfCartons" HeaderText="Number Of Cartons" DecimalDigits="2" FilterControlAltText="Filter Number Of Cartons column" UniqueName="NumberOfCartonsColumn" />
                <telerik:GridNumericColumn DataField="FlatRateAmount" HeaderText="Flat-rate Amount" DecimalDigits="2" FilterControlAltText="Filter Flat Rate Amount column" UniqueName="FlatRateAmountColumn" />
              </Columns>
              <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
              </EditFormSettings>
              <BatchEditingSettings EditType="Cell" />
              <PagerStyle PageSizeControlType="RadComboBox" />
            </MasterTableView><PagerStyle PageSizeControlType="RadComboBox" />
            <FilterMenu EnableImageSprites="False"></FilterMenu>
          </telerik:RadGrid>
        </asp:Panel>
      </asp:Panel>
    </telerik:RadPageView>
    <telerik:RadPageView runat="Server" ID="tpnlStorage" HeaderText="<%$ Resources:Default, RateMaintenance_Storage%>">
      <asp:Panel ID="StoragePanel" runat="server">

        <asp:DetailsView ID="DetailsViewStorage" DataKeyNames="Id" runat="server" Width="250px" DataSourceID="rateDetailStorageDataSource" EnableModelValidation="True" AutoGenerateEditButton="True" AutoGenerateRows="False" SkinID="ReceiveIntoWarehouse" OnDataBound="DetailsViewStorage_DataBound">
          <Fields>
            <asp:BoundField DataField="StorageDays" DataFormatString="{0:N2}" HeaderText="Storage Days" />
          </Fields>
        </asp:DetailsView>

        <asp:ObjectDataSource runat="server" ID="rateDetailStorageDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetByRateIdAndType" TypeName="RateDetail" InsertMethod="Update" UpdateMethod="Update">
          <InsertParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter Name="shipment" Type="Double" />
            <asp:Parameter Name="documentation" Type="Double" />
            <asp:Parameter Name="StorageDays" Type="Byte" />
            <asp:SessionParameter Name="RateId" SessionField="RateId" Type="Int32" />
            <asp:Parameter DefaultValue="3" Name="type" Type="Int32" />
            <asp:Parameter Name="original_Id" Type="Int32" />
          </InsertParameters>
          <SelectParameters>
            <asp:SessionParameter Name="rateId" SessionField="RateId" Type="Int32" />
            <asp:Parameter DefaultValue="3" Name="type" Type="Int32" />
          </SelectParameters>
          <UpdateParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter DefaultValue="3" Name="type" Type="Int32" />
            <asp:Parameter Name="shipment" Type="Double" DefaultValue="0.0" />
            <asp:Parameter Name="documentation" Type="Double" DefaultValue="0.0" />
            <asp:Parameter Name="StorageDays" Type="Byte" DefaultValue="0" />
            <asp:SessionParameter SessionField="RateId" Name="RateId" />
            <asp:Parameter Name="original_Id" Type="Int32" />
          </UpdateParameters>
        </asp:ObjectDataSource>


        <asp:Panel ID="Panel2" runat="server">

          <asp:ObjectDataSource runat="server" ID="rateDetailLineStorageDataSource" OldValuesParameterFormatString="original_{0}" SelectMethod="GetByRateDetailId" TypeName="RateDetailLine" InsertMethod="Update" UpdateMethod="Update">
            <InsertParameters>
              <asp:Parameter Name="original_Id" Type="Int32" />
              <asp:Parameter Name="id" Type="Int32" />
              <asp:Parameter Name="weightInKg" Type="Double" />
              <asp:Parameter Name="numberOfUnits" Type="Double" />
              <asp:Parameter Name="cubeInMeters" Type="Double" />
              <asp:Parameter Name="numberOfPallets" Type="Double" />
              <asp:Parameter Name="numberOfCartons" Type="Double" />
              <asp:Parameter Name="flatRateAmount" Type="Double" />
              <asp:Parameter Name="minimumWeightInKg" Type="Double" />
              <asp:SessionParameter Name="rateDetailId" SessionField="RATEDETAIL_ID_STORAGE" Type="Int32" />
              <asp:Parameter Name="billingEventId" Type="Int32" />
              <asp:Parameter Name="areaId" Type="Int32" />
            </InsertParameters>
            <SelectParameters>
              <asp:SessionParameter Name="rateDetailId" SessionField="RATEDETAIL_ID_STORAGE" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
              <asp:Parameter Name="original_Id" Type="Int32" />
              <asp:Parameter Name="id" Type="Int32" />
              <asp:Parameter Name="weightInKg" Type="Double" />
              <asp:Parameter Name="numberOfUnits" Type="Double" />
              <asp:Parameter Name="cubeInMeters" Type="Double" />
              <asp:Parameter Name="numberOfPallets" Type="Double" />
              <asp:Parameter Name="numberOfCartons" Type="Double" />
              <asp:Parameter Name="flatRateAmount" Type="Double" />
              <asp:Parameter Name="minimumWeightInKg" Type="Double" />
              <asp:SessionParameter Name="rateDetailId" SessionField="RATEDETAIL_ID_STORAGE" Type="Int32" />
              <asp:Parameter Name="billingEventId" Type="Int32" />
              <asp:Parameter Name="areaId" Type="Int32" />
            </UpdateParameters>
          </asp:ObjectDataSource>

          <telerik:RadGrid ID="gridDetailLineStorage" runat="server" CellSpacing="0" DataSourceID="rateDetailLineStorageDataSource" GridLines="None" AutoGenerateDeleteColumn="True" AutoGenerateEditColumn="True" AutoGenerateColumns="False">
            <MasterTableView DataSourceID="rateDetailLineStorageDataSource" CommandItemDisplay="Top" HorizontalAlign="Left" DataKeyNames="Id" AllowAutomaticDeletes="true" AllowAutomaticInserts="true" AllowAutomaticUpdates="true">
              <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px" />
              </RowIndicatorColumn>
              <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                <HeaderStyle Width="20px" />
              </ExpandCollapseColumn>
              <Columns>
                <telerik:GridBoundColumn DataField="Id" FilterControlAltText="Filter Id column" UniqueName="Idcolumn" Display="false" Visible="false" InsertVisiblityMode="AlwaysHidden" />
                <telerik:GridDropDownColumn DataField="AreaId" DataSourceID="areaDataSource" ListTextField="Area" ListValueField="AreaId" HeaderText="Area" />
                <telerik:GridDropDownColumn DataField="BillingEventId" DataSourceID="billingEventDataSource" ListTextField="Name" ListValueField="Id" HeaderText="Billing Event" />
                <telerik:GridNumericColumn DataField="WeightInKg" HeaderText="Weight In Kg" DecimalDigits="2" FilterControlAltText="Filter Weight In Kg column" UniqueName="WeightInKgColumn" />
                <telerik:GridNumericColumn DataField="MinimumWeightInKg" HeaderText="Minimum Weight In Kg" DecimalDigits="2" FilterControlAltText="Filter Minimum Weight In Kg column" UniqueName="MinimumWeightInKgColumn" />
                <telerik:GridNumericColumn DataField="NumberOfUnits" HeaderText="Number Of Units" DecimalDigits="2" FilterControlAltText="Filter Number Of Units column" UniqueName="NumberOfUnitsColumn" />
                <telerik:GridNumericColumn DataField="CubeInMeters" HeaderText="Cube In Meters" DecimalDigits="2" FilterControlAltText="Filter Cube In Meters column" UniqueName="CubeInMetersColumn" />
                <telerik:GridNumericColumn DataField="NumberOfPallets" HeaderText="Number Of Pallets" DecimalDigits="2" FilterControlAltText="Filter Number Of Pallets column" UniqueName="NumberOfPalletsColumn" />
                <telerik:GridNumericColumn DataField="NumberOfCartons" HeaderText="Number Of Cartons" DecimalDigits="2" FilterControlAltText="Filter Number Of Cartons column" UniqueName="NumberOfCartonsColumn" />
                <telerik:GridNumericColumn DataField="FlatRateAmount" HeaderText="Flat-rate Amount" DecimalDigits="2" FilterControlAltText="Filter Flat Rate Amount column" UniqueName="FlatRateAmountColumn" />
              </Columns>
              <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
              </EditFormSettings>
              <BatchEditingSettings EditType="Cell" />
              <PagerStyle PageSizeControlType="RadComboBox" />
            </MasterTableView><PagerStyle PageSizeControlType="RadComboBox" />
            <FilterMenu EnableImageSprites="False"></FilterMenu>
          </telerik:RadGrid>
        </asp:Panel>
      </asp:Panel>
    </telerik:RadPageView>
  </telerik:RadMultiPage>
  <telerik:RadButton ID="ButtonSaveSettings" runat="server" Text="Save Layout Settings" />
</asp:Content>
