﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Billing_RateDefaultMaintenance : System.Web.UI.Page
{
  protected void Page_Load( object sender, EventArgs e )
  {
    
  }

  /// <summary>
  /// Handles the Inserted event of the rateDefaultDataSource control.
  /// </summary>
  /// <param name="sender">The source of the event.</param>
  /// <param name="e">The <see cref="ObjectDataSourceStatusEventArgs"/> instance containing the event data.</param>
  protected void rateDefaultDataSource_Inserted( object sender, ObjectDataSourceStatusEventArgs e )
  {
    if ( e.Exception != null )
    {
      Master.ErrorText = GetLastErrorMessage( e.Exception );
      e.ExceptionHandled = true;
    }
  }

  /// <summary>
  /// Gets the last error message from the Exception Stack
  /// </summary>
  /// <param name="exception">The exception.</param>
  /// <returns></returns>
  private string GetLastErrorMessage( Exception exception )
  {
    string str = exception.Message;

    var innEx = exception.InnerException;
    while ( innEx != null )
    {
      str = innEx.Message;
      innEx = innEx.InnerException;
    }

    return str;
  }
}
