<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Inbound_Default" Title="<%$ Resources:Default, DefaultOutboundTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultOutboundTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultOutboundAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <table>
        <tr valign="top">
            <td>
                <telerik:RadButton ID="RadButtonOrderStatus" runat="server" Text="Order Status" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChartOrderStatus" runat="server" ChartTitle-TextBlock-Text="Order Status" Skin="DeepBlue"
                    DataGroupColumn="Legend" DefaultType="Bar" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourceOrderStatus" SeriesOrientation="Horizontal" Width="500px">
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourceOrderStatus" runat="server" TypeName="Dashboard" SelectMethod="OutboundOrderStatus">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonShortPicks" runat="server" Text="Short Picks" Skin="Outlook" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridDockShortPicks" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceShortPicks" Skin="Default" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceShortPicks">
                        <Columns>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" UniqueName="ProductCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" UniqueName="Product"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" UniqueName="SKUCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="ShortQuantity" HeaderText="<%$ Resources:Default, Quantity %>" UniqueName="Quantity"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceShortPicks" runat="server" TypeName="Dashboard" SelectMethod="OutboundShortPicks">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadButton ID="RadButtonOutstandingLines" runat="server" Text="Outstanding Lines" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChartOutstandingLines" runat="server" ChartTitle-TextBlock-Text="Outstanding Lines" Skin="DeepBlue"
                    DataGroupColumn="Legend" DefaultType="Bar" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourceOutstandingLines" SeriesOrientation="Horizontal" Width="500px">
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourceOutstandingLines" runat="server" TypeName="Dashboard" SelectMethod="OutboundOutstandingLines">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonOutstandingJobs" runat="server" Text="Outstanding Jobs" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChart1" runat="server" ChartTitle-TextBlock-Text="Outstanding Jobs" Skin="DeepBlue"
                    DataGroupColumn="Legend" DefaultType="Bar" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourceOutstandingJobs" SeriesOrientation="Horizontal" Width="500px">
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourceOutstandingJobs" runat="server" TypeName="Dashboard" SelectMethod="OutboundOutstandingJobs">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        
    </table>
</asp:Content>