<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="OrderQuery.aspx.cs" Inherits="Outbound_WavePlanning"
    Title="<%$ Resources:Default, OutboundShipmentLinkTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc2" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="Container Planning"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Delivery and Returns Module"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
                <asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
                    <table>
                        <tr>
                            <td>
                                <telerik:RadTextBox ID="TextBoxOrderNumber" runat="server" Label="<%$ Resources:Default, OrderNumber %>" Width="250px"></telerik:RadTextBox>
                            </td>
                            <td>
                                <telerik:RadDatePicker ID="rdpFromDate" runat="server" DateInput-DisplayText="<%$ Resources:Default, FromDate %>"></telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadTextBox ID="TextBoxInvoiceNumber" runat="server" Label="<%$ Resources:Default, InvoiceNumber %>" Width="250px"></telerik:RadTextBox>
                            </td>
                            <td>
                                <telerik:RadDatePicker ID="rdpToDate" runat="server" DateInput-DisplayText="<%$ Resources:Default, ToDate %>"></telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rblStatus" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Text="All" Value="All"></asp:ListItem>
                                    <asp:ListItem Text="On time" Value="OnTime"></asp:ListItem>
                                    <asp:ListItem Text="Over Due" Value="OverDue"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td align="right">
                                <telerik:RadButton ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search %>" OnClick="ButtonDocumentSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                    TargetControlID="PanelSearch" Radius="10" Color="239, 239, 236" BorderColor="64, 64, 64"
                    Enabled="True" />
                <br />
                
                
                <telerik:RadBinaryImage ID="rbiGreen" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                <asp:Label ID="LabelGreen" runat="server" Text="On Time - Complete"></asp:Label>
                
                <telerik:RadBinaryImage ID="rbiYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                <asp:Label ID="LabelYellow" runat="server" Text="On Time - Incomplete"></asp:Label>
                
                <telerik:RadBinaryImage ID="rbiOrange" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                <asp:Label ID="LabelOrange" runat="server" Text="Overdue - Complete"></asp:Label>
                
                <telerik:RadBinaryImage ID="rbiRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                <asp:Label ID="LabelRed" runat="server" Text="Overdue - Incomplete"></asp:Label>
                <br />

                <telerik:RadGrid ID="RadGridWave" runat="server" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true"
                    AllowMultiRowSelection="True" OnItemCommand="RadGridWave_ItemCommand" EnableHeaderContextMenu="true"
                    OnSelectedIndexChanged="RadGridWave_SelectedIndexChanged" AllowPaging="true" PageSize="30" Height="500px" Skin="Metro">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="OutboundShipmentId,IssueId" DataSourceID="ObjectDataSourceWave" AutoGenerateColumns="false">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridBoundColumn ReadOnly="false" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber"></telerik:GridBoundColumn>
                            <%--<telerik:GridBoundColumn ReadOnly="false" DataField="InvoiceNumber" HeaderText="<%$ Resources:Default, InvoiceNumber %>" SortExpression="InvoiceNumber"></telerik:GridBoundColumn>--%>
                            <telerik:GridBoundColumn ReadOnly="false" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status"></telerik:GridBoundColumn>
                            <%--<telerik:GridBoundColumn ReadOnly="false" DataField="PODStatus" HeaderText="POD" SortExpression="PODStatus"></telerik:GridBoundColumn>--%>
                            <%--<telerik:GridBoundColumn ReadOnly="false" DataField="PODDate" HeaderText="POD Date" SortExpression="PODDate"></telerik:GridBoundColumn>--%>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>" SortExpression="OutboundDocumentType"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="DeliveryMethod" HeaderText="<%$ Resources:Default, DeliveryMethod %>" SortExpression="DeliveryMethod"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>" SortExpression="ExternalCompany"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>" SortExpression="ExternalCompanyCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="false" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>" SortExpression="DeliveryDate" DataFormatString="{0:d}"></telerik:GridBoundColumn>
                            <%--<telerik:GridTemplateColumn UniqueName="ImageDeliveryDate">
                                <ItemTemplate>
                                    <img alt="Image" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "ImageDeliveryDate", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            --%>
                            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, CreateDate %>">
                                <ItemTemplate>
                                    <asp:Label ID="LabelCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                                    <br />
                                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "ImageCreateDate", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, PlanningComplete %>">
                                <ItemTemplate>
                                    <asp:Label ID="LabelPlanningComplete" runat="server" Text='<%# Bind("PlanningComplete") %>'></asp:Label>
                                    <br />
                                    <img alt="Image" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "ImagePlanningComplete", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Release %>">
                                <ItemTemplate>
                                    <asp:Label ID="LabelRelease" runat="server" Text='<%# Bind("Release") %>'></asp:Label>
                                    <br />
                                    <img alt="Image" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "ImageRelease", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Checking %>">
                                <ItemTemplate>
                                    <asp:Label ID="LabelChecking" runat="server" Text='<%# Bind("Checking") %>'></asp:Label>
                                    <br />
                                    <img alt="Image" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "ImageChecking", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Checked %>">
                                <ItemTemplate>
                                    <asp:Label ID="LabelChecked" runat="server" Text='<%# Bind("Checked") %>'></asp:Label>
                                    <br />
                                    <img alt="Image" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "ImageChecked", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <%--<telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Despatched %>">
                                <ItemTemplate>
                                    <asp:Label ID="LabelDespatch" runat="server" Text='<%# Bind("Despatch") %>'></asp:Label>
                                    <br />
                                    <img alt="Image" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "ImageDespatch", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, DespatchChecked %>">
                                <ItemTemplate>
                                    <asp:Label ID="LabelDespatchChecked" runat="server" Text='<%# Bind("DespatchChecked") %>'></asp:Label>
                                    <br />
                                    <img alt="Image" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "ImageDespatchChecked", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="<%$ Resources:Default, Complete %>">
                                <ItemTemplate>
                                    <asp:Label ID="LabelComplete" runat="server" Text='<%# Bind("Complete") %>'></asp:Label>
                                    <br />
                                    <img alt="Image" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "ImageComplete", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>--%>
                            <telerik:GridBoundColumn ReadOnly="false" DataField="PickPercentage" HeaderText="<%$ Resources:Default, PercentagePicked %>" SortExpression="PickPercentage"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="false" DataField="CheckingPercentage" HeaderText="<%$ Resources:Default, PercentageChecked %>" SortExpression="CheckingPercentage"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="false" DataField="TotalQuantity" HeaderText="<%$ Resources:Default, TotalQuantity %>" SortExpression="TotalQuantity"></telerik:GridBoundColumn>
                            <%--<telerik:GridBoundColumn ReadOnly="false" DataField="Extracted" HeaderText="<%$ Resources:Default, Extracted %>" SortExpression="Extracted"></telerik:GridBoundColumn>--%>
                            <%--<telerik:GridButtonColumn Text="<%$ Resources:Default, Invoice %>" HeaderText="<%$ Resources:Default, Invoice %>" CommandName="Invoice"></telerik:GridButtonColumn>--%>
                            <%--<telerik:GridButtonColumn Text="<%$ Resources:Default, ShortPicks %>" HeaderText="<%$ Resources:Default, ShortPicks %>" CommandName="ShortPicks"></telerik:GridButtonColumn>--%>
                            <%--<telerik:GridButtonColumn Text="<%$ Resources:Default, DeliveryNote %>" HeaderText="<%$ Resources:Default, DeliveryNote %>" CommandName="DeliveryNote"></telerik:GridButtonColumn>--%>
                            <%--<telerik:GridButtonColumn Text="<%$ Resources:Default, Containers %>" HeaderText="<%$ Resources:Default, Containers %>" CommandName="Containers"></telerik:GridButtonColumn>--%>
                            <%--<telerik:GridBoundColumn ReadOnly="false" DataField="ReceivedFromHost" HeaderText="<%$ Resources:Default, ReceivedFromHost %>" SortExpression="ReceivedFromHost"></telerik:GridBoundColumn>--%>
                            <%--<telerik:GridBoundColumn ReadOnly="false" DataField="Data" HeaderText="<%$ Resources:Default, Data %>" SortExpression="Data"></telerik:GridBoundColumn>--%>
                            <%--<telerik:GridBoundColumn ReadOnly="false" DataField="URL" HeaderText="<%$ Resources:Default, URL %>" SortExpression="URL"></telerik:GridBoundColumn>--%>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                        ReorderColumnsOnClient="True" AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        <Resizing AllowColumnResize="True" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceWave" runat="server" TypeName="OrderQuery"
                    SelectMethod="SearchOrder" InsertMethod="InsertOrder" UpdateMethod="UpdateOrder" DeleteMethod="DeleteOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="operatorId" SessionField="operatorId" Type="Int32" />
                        <asp:ControlParameter Name="orderNumber" ControlID="TextBoxOrderNumber" Type="String" />
                        <asp:ControlParameter Name="invoiceNumber" ControlID="TextBoxInvoiceNumber" Type="String" />
                        <asp:ControlParameter Name="FromDate" ControlID="rdpFromDate" Type="DateTime" />
                        <asp:ControlParameter Name="ToDate" ControlID="rdpToDate" Type="DateTime" />
                        <asp:ControlParameter Name="Status" ControlID="rblStatus" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Thumbnail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridWave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridWave" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridComments" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="ButtonDocumentSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridWave" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabSContainer1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs" Visible="false">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Dashboard %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Search %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Comments %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabsContainer and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="RadPageView1" Visible="false">
                <telerik:RadButton ID="RadButtonUnreadCommunication" runat="server" Text="Unread Communication" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridUnreadCommunication" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceUnreadCommunication" Skin="Outlook" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceUnreadCommunication">
                        <Columns>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="true" DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" UniqueName="OrderNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Comment" HeaderText="Comment" UniqueName="Comment"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceUnreadCommunication" runat="server" TypeName="Dashboard" SelectMethod="UnreadCommunicationIssue">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="server" ID="RadPageView2">


            </telerik:RadPageView>
                
            <telerik:RadPageView runat="Server" ID="RadPageView3" Visible="false">
                <telerik:RadGrid ID="RadGridComments" runat="server" DataSourceID="ObjectDataSourceComents"
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" AllowAutomaticInserts="true" Skin="Metro">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="CommentId"
                        CommandItemDisplay="Top" DataSourceID="ObjectDataSourceComents"
                        AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridBoundColumn DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" ReadOnly="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>" ReadOnly="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Comment" HeaderText="<%$ Resources:Default, Comments %>"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceComents" runat="server" TypeName="Comment"
                    SelectMethod="SelectComment" InsertMethod="InsertComment" UpdateMethod="UpdateComment" DeleteMethod="DeleteComment">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="tableId" Type="Int32" SessionField="tableId" />
                        <asp:SessionParameter Name="tableName" Type="String" SessionField="tableName" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="tableId" Type="Int32" SessionField="tableId" />
                        <asp:SessionParameter Name="tableName" Type="String" SessionField="tableName" />
                        <asp:Parameter Type="String" Name="comment"></asp:Parameter>
                        <asp:SessionParameter Type="Int32" Name="operatorId" SessionField="OperatorId"></asp:SessionParameter>
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="commentId"></asp:Parameter>
                        <asp:Parameter Type="String" Name="comment"></asp:Parameter>
                        <asp:SessionParameter Type="Int32" Name="operatorId" SessionField="OperatorId"></asp:SessionParameter>
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="commentId"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Content>
