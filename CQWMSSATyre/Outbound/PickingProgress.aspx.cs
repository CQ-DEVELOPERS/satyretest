using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;

public partial class Outbound_PickingProgress : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;
                ButtonAutoAllocation.Visible = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 33);

                Configuration config = new Configuration();

                int config44 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 44);
                int config45 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 45);
                int config46 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 46);
                int config47 = config.GetIntValue(Session["ConnectionStringName"].ToString(), 47);

                LabelRed.Text = "Overdue";
                LabelOrange.Text = "Required in " + config45.ToString() + " hours";
                LabelYellow.Text = "Required in " + config46.ToString() + " hours";
                LabelStandard.Text = "Required > " + config47.ToString() + " hours";

                MenuItemsDynamic ds = new MenuItemsDynamic();

                int operatorId;

                if (Session["MenuId"] == null)
                    Session["MenuId"] = 1;

                if (Session["OperatorId"] == null)
                    operatorId = -1;
                else
                    operatorId = (int)Session["OperatorId"];
            }

            if (Session["WarehouseId"] == null)
                Response.Redirect("~/Security/Login.aspx");

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["IssueId"] != null)
                {
                    int issueId = (int)Session["IssueId"];

                    foreach (GridDataItem item in RadGridOrders.Items)
                    {
                        theErrMethod = "Page_LoadComplete";
                        if (item.GetDataKeyValue("IssueId").ToString() == issueId.ToString())
                        {
                            RadGridOrders.SelectedIndexes.Add(item.ItemIndex);
                            break;
                        }
                    }

                    if (RadGridOrders.SelectedIndexes.Count == 0)
                    {
                        RadGridOrders.SelectedIndexes.Clear();
                        RadGridJobs.SelectedIndexes.Clear();
                        Session["OutboundShipmentId"] = null;
                        Session["IssueId"] = null;
                        Session["Jobid"] = null;

                        RadGridOrderLines.DataBind();
                        RadGridJobs.DataBind();
                        RadGridDetails.DataBind();
                    }
                }
            }
        }
        catch { }
    }
    #endregion Page_LoadComplete

    #region ButtonOperatorRelease_Click
    protected void ButtonOperatorRelease_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonOperatorRelease_Click";
        try
        {
            int operatorId = -1;
            Status status = new Status();

            if (int.TryParse(DropDownListOperator.SelectedValue, out operatorId))
            {
                foreach (GridDataItem item in RadGridOrders.SelectedItems)
                {
                    status.SetReleaseOperator(Session["ConnectionStringName"].ToString(),
                                                int.Parse(item.GetDataKeyValue("OutboundShipmentId").ToString()),
                                                int.Parse(item.GetDataKeyValue("IssueId").ToString()),
                                                operatorId);
                }

                RadGridOrders.DataBind();
                Master.MsgText = "Release"; Master.ErrorText = "";
            }

        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonOperatorRelease_Click"

    #region ButtonPriority_Click
    protected void ButtonPriority_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPriority_Click";
        try
        {
            int priorityId = -1;
            Priority p = new Priority();

            if (int.TryParse(DropDownListPriority.SelectedValue, out priorityId))
            {
                foreach (GridDataItem item in RadGridOrders.SelectedItems)
                {
                    p.PriorityUpdate(Session["ConnectionStringName"].ToString(),
                                                int.Parse(item.GetDataKeyValue("OutboundShipmentId").ToString()),
                                                int.Parse(item.GetDataKeyValue("IssueId").ToString()),
                                                priorityId);
                }

                RadGridOrders.DataBind();
                Master.MsgText = "Release"; Master.ErrorText = "";
            }

        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonPriority_Click"    

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            RadGridOrders.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region RadGridJobs_OnSelectedIndexChanged
    protected void RadGridJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridJobs_OnSelectedIndexChanged";

        try
        {
            Session["JobId"] = -1;

            foreach (GridDataItem item in RadGridJobs.Items)
            {
                if (item.Selected)
                    Session["JobId"] = (int)item.GetDataKeyValue("JobId");
            }

            //RadGridJobs.DataBind();
            RadGridDetails.DataBind();

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "RadGridJobs_OnSelectedIndexChanged"

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            OutboundWIP plan = new OutboundWIP();

            foreach (GridDataItem item in RadGridOrders.SelectedItems)
            {
                if (!plan.AutoLocationAllocateIssue(Session["ConnectionStringName"].ToString(), (int)item.GetDataKeyValue("IssueId")))
                {
                    Master.ErrorText = "Auto Allocation Not Complete";
                    break; ;
                }

                RadGridDetails.DataBind();

                Master.MsgText = ""; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAutoLocations_Click

    #region ButtonPalletise_Click
    protected void ButtonPalletise_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPalletise_Click";

        try
        {
            OutboundWIP plan = new OutboundWIP();

            foreach (GridDataItem item in RadGridOrders.SelectedItems)
            {
                if (plan.Palletise(Session["ConnectionStringName"].ToString(), int.Parse(item.GetDataKeyValue("IssueId").ToString()), -1))
                {
                    Master.MsgText = "Palletised"; Master.ErrorText = "";
                }
                else
                {
                    Master.ErrorText = "Palletisation Failed";
                    break;
                }

                RadGridOrderLines.DataBind();
                RadGridDetails.DataBind();
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPalletise_Click

    #region RadGridOrders_RowDataBound
    protected void RadGridOrders_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // This line will get the reference to the underlying row
            DataRowView _row = (DataRowView)e.Row.DataItem;
            if (_row != null)
            {
                switch (_row.Row["Status"].ToString())
                {
                    case "Pause":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.OrangeRed;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Planning Complete":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.RoyalBlue;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Release":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.Green;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Manual":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.LightSteelBlue;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Checking":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.SaddleBrown;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Quality Assurance":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                }
            }
        }
    }
    #endregion RadGridOrders_RowDataBound

    #region RadGridOrders_SelectedIndexChanged
    protected void RadGridOrders_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetKey();
    }
    #endregion RadGridOrders_SelectedIndexChanged

    #region GetKey
    protected void GetKey()
    {
        theErrMethod = "RadGridOrders_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in RadGridOrders.Items)
            {
                if (item.Selected)
                {
                    Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId");
                    Session["IssueId"] = item.GetDataKeyValue("IssueId");
                    ObjectDataSourceOrderLines.DataBind();

                    RadGridJobs.SelectedIndexes.Clear();
                    Session["Jobid"] = null;
                    RadGridDetails.DataBind();
                    break;
                }
            }
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GetKey

    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";
        try
        {
            foreach (GridDataItem item in RadGridOrderLines.Items)
            {
                if (item.Selected)
                    e.InputParameters["storageUnitId"] = (int)item.GetDataKeyValue("StorageUnitId");
            }
            

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ObjectDataSourceBatch_OnSelecting"

    #region ObjectDataSourceOrderLines_Selecting
    protected void ObjectDataSourceOrderLines_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceOrderLines_Selecting";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ObjectDataSourceOrderLines_Selecting

    #region RadGridOrderLines_SelectedIndexChanging
    protected void RadGridOrderLines_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        theErrMethod = "RadGridOrderLines_SelectedIndexChanging";

        try
        {
            foreach (GridDataItem item in RadGridOrderLines.Items)
            {
                if(item.Selected)
                    Session["StorageUnitId"] = item.GetDataKeyValue("StorageUnitId");
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridOrderLines_SelectedIndexChanging

    #region DropDownListBatch_OnSelectedIndexChanged
    protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "DropDownListBatch_OnSelectedIndexChanged";

        try
        {
            DropDownList dpList = (DropDownList)sender;
            Session["StorageUnitId"] = int.Parse(dpList.SelectedValue);

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "DropDownListBatch_OnSelectedIndexChanged"

    #region ButtonDeallocate_Click
    protected void ButtonDeallocate_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeallocate_Click";
        try
        {
            OutboundWIP wip = new OutboundWIP();

            foreach (GridDataItem item in RadGridOrders.SelectedItems)
            {
                if (wip.PalletiseDeallocate(Session["ConnectionStringName"].ToString(), (int)item.GetDataKeyValue("OutboundShipmentId"), (int)item.GetDataKeyValue("IssueId")))
                {
                    Master.MsgText = "Reverse Successful"; Master.ErrorText = "";
                }
                else
                {
                    Master.MsgText = ""; Master.ErrorText = "Document cannot be reversed";
                    break;
                }
            }

            RadGridOrders.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonDeallocate_Click

    #region ButtonMix_Click
    protected void ButtonMix_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonMix_Click";
        try
        {
            Session["InstructionTypeCode"] = "PM";
            Response.Redirect("~/Common/MixedJobCreate.aspx");
            Master.MsgText = "Mix"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonMix_Click

    #region ButtonRelease_Click
    protected void ButtonRelease_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRelease_Click";
        try
        {
            StatusChange("RL");

            RadGridOrders.DataBind();

            Master.MsgText = "Release"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonRelease_Click"

    #region ButtonReleaseJob_Click
    protected void ButtonReleaseJob_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReleaseJob_Click";
        try
        {
            Status status = new Status();
            int operatorId = -1;
            
            if (!int.TryParse(DropDownListOperator.SelectedValue, out operatorId))
                operatorId = -1;

            foreach (GridDataItem item in RadGridJobs.Items)
            {
                if (item.Selected)
                {
                    status.ReleaseJob(Session["ConnectionStringName"].ToString(),
                                      int.Parse(item.GetDataKeyValue("JobId").ToString()),
                                      operatorId,
                                      "RL");
                }
            }

            RadGridJobs.DataBind();

            Master.MsgText = "Job Released"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonReleaseJob_Click"

    #region ButtonPauseJob_Click
    protected void ButtonPauseJob_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPauseJob_Click";
        try
        {
            Status status = new Status();

            foreach (GridDataItem item in RadGridJobs.Items)
            {
                if (item.Selected)
                {
                    status.ReleaseJob(Session["ConnectionStringName"].ToString(),
                                      int.Parse(item.GetDataKeyValue("JobId").ToString()),
                                      int.Parse(Session["OperatorId"].ToString()),
                                      "PS");
                }
            }

            RadGridJobs.DataBind();

            Master.MsgText = "Job Paused"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonPauseJob_Click"

    #region ButtonManual_Click
    protected void ButtonManual_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManual_Click";
        try
        {
            StatusChange("M");

            RadGridOrders.DataBind();

            Master.MsgText = "Status Manual"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonManual_Click"

    #region ButtonReleaseNoStock_Click
    protected void ButtonReleaseNoStock_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReleaseNoStock_Click";
        try
        {
            int outboundShipmentId = -1;
            int issueId = -1;
            int operatorId = (int)Session["OperatorId"];

            CheckBox cb = new CheckBox();
            OutboundWIP wip = new OutboundWIP();

            foreach (GridDataItem item in RadGridOrders.Items)
            {
                if (item.Selected)
                {
                    outboundShipmentId = int.Parse(item.GetDataKeyValue("OutboundShipmentId").ToString());
                    issueId = int.Parse(item.GetDataKeyValue("IssueId").ToString());

                    if (outboundShipmentId != -1)
                        issueId = -1;

                    if (wip.ReleaseNoStock(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId, operatorId))
                    {
                        Master.MsgText = "Re-release of no-stock's successful"; Master.ErrorText = "";
                    }
                    else
                    {
                        Master.MsgText = ""; Master.ErrorText = "Re-release of no-stock's not complete";

                        RadGridOrders.SelectedIndexes.Add(item.ItemIndex);
                        GetKey();
                        break;
                    }
                }
            }

            RadGridJobs.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonReleaseNoStock_Click"

    #region StatusChange
    protected void StatusChange(string statusCode)
    {
        theErrMethod = "StatusChange";
        try
        {
            Status status = new Status();

            foreach (GridDataItem item in RadGridOrders.Items)
            {
                if (item.Selected)
                {
                    if (item.GetDataKeyValue("OutboundShipmentId").ToString() != "-1")
                        status.SetRelease(Session["ConnectionStringName"].ToString(),
                                          int.Parse(item.GetDataKeyValue("OutboundShipmentId").ToString()),
                                          -1,
                                          -1,
                                          statusCode);
                    else
                        status.SetRelease(Session["ConnectionStringName"].ToString(),
                                          int.Parse(item.GetDataKeyValue("OutboundShipmentId").ToString()),
                                          int.Parse(item.GetDataKeyValue("IssueId").ToString()),
                                          -1,
                                          statusCode);
                }
            }

            Master.MsgText = "Status Change"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadRelease" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "StatusChange"

    #region ButtonPause_Click
    protected void ButtonPause_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPause_Click";
        try
        {
            StatusChange("PS");

            RadGridOrders.DataBind();

            Master.MsgText = "Release"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonPause_Click"

    #region ButtonPrintJob_Click
    protected void ButtonPrintJob_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintJob_Click";
        try
        {
            Session["FromURL"] = "~/Outbound/PlanningMaintenance.aspx";

            Session["ReportName"] = "Job Pick Sheet";

            ReportParameter[] RptParameters = new ReportParameter[4];

            // Create the JobId report parameter
            string jobId = Session["JobId"].ToString();
            RptParameters[0] = new ReportParameter("JobId", jobId);

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonPrintJob_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            foreach (GridDataItem item in RadGridJobs.Items)
            {
                item.Selected = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ButtonPrint_Click

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            ArrayList rowList = new ArrayList();

            foreach (GridDataItem item in RadGridJobs.Items)
                if (item.Selected)
                    rowList.Add((int) item.GetDataKeyValue("JobId"));

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 351))
                Session["LabelName"] = "Despatch By Route Label.lbl";
            else
                Session["LabelName"] = "Despatch By Order Label Plumblink.lbl";
                //Session["LabelName"] = "Despatch By Order Label.lbl";

            Session["FromURL"] = "~/Outbound/PlanningMaintenance.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = string.Empty;

            if (Session["Printer"].ToString() == string.Empty)
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message);

            Master.ErrorText = result;
        }
    }

    #endregion ButtonPrint_Click

    #region ButtonPrintLabel_Click
    protected void ButtonPrintLabel_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();
            
            foreach (GridDataItem item in RadGridJobs.Items)
            {
                if(item.Selected)
                {
                    checkedList.Add((int)item.GetDataKeyValue("JobId"));
                }
            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
                Session["LabelName"] = "Picking Label Small.lbl";
            else
                Session["LabelName"] = "Picking Label Large.lbl";

            Session["FromURL"] = "~/Outbound/PlanningMaintenance.aspx";

            Session["LabelCopies"] = int.Parse(TextBoxQty.Text);

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("../Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch { }
    }
    #endregion ButtonPrintLabel_Click

    #region ButtonPalletWeight
    protected void ButtonPalletWeight_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPalletWeight";

        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();

            foreach (GridDataItem item in RadGridJobs.Items)
            {
                if (item.Selected)
                {
                    checkedList.Add((int)item.GetDataKeyValue("JobId"));
                }
            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            Session["FromURL"] = "~/Outbound/PlanningMaintenance.aspx";

            Response.Redirect("~/Inbound/PalletWeight.aspx");

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonPalletWeight"

    #region ButtonWavePlanning_Click
    protected void ButtonWavePlanning_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonWavePlanning_Click";

        try
        {
            Response.Redirect("~/Outbound/WavePlanning.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonWavePlanning_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region Render
    protected override void Render(HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);
            SavePetNames();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Render

    #region SavePetNames
    protected void SavePetNames()
    {
        try
        {
            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridOrders);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrders, "PickingProgressRadGridOrders", (int)Session["OperatorId"]);
            }
            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridOrderLines);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrderLines, "PickingProgressRadGridOrderLines", (int)Session["OperatorId"]);
            }

            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridJobs);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridJobs, "PickingProgressRadGridJobs", (int)Session["OperatorId"]);
            }
            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridDetails);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridDetails, "PickingProgressRadGridDetails", (int)Session["OperatorId"]);
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion SavePetNames

    #region ButtonSaveSettings_Click
    protected void ButtonSaveSettings_Click(object sender, EventArgs e)
    {
        try
        {
            SavePetNames();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSaveSettings_Click

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            if (!Page.IsPostBack)
            {
                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridOrders);
                    settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrders, "PickingProgressRadGridOrders", (int)Session["OperatorId"]);
                }
                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridOrderLines);
                    settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrderLines, "PickingProgressRadGridOrderLines", (int)Session["OperatorId"]);
                }

                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridJobs);
                    settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridJobs, "PickingProgressRadGridJobs", (int)Session["OperatorId"]);
                }
                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridDetails);
                    settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridDetails, "PickingProgressRadGridDetails", (int)Session["OperatorId"]);
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingProgress" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Init

    #region RadGridOrders_ItemCommand
    protected void RadGridOrders_ItemCommand(object source, GridCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "PrintCollectionSlip")
            {
                foreach (GridDataItem item in RadGridOrders.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId");
                        Session["IssueId"] = item.GetDataKeyValue("IssueId");

                        Session["FromUrl"] = HttpContext.Current.Request.Url;

                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 156))
                            Session["ReportName"] = "Receiving Check Sheet Batches";
                        else
                            Session["ReportName"] = "Collection Slip";

                        ReportParameter[] RptParameters = new ReportParameter[7];

                        // Create the Report Parameters holder
                        var reportParameters = new List<ReportParameter>();

                        // Create the BilledTransactionId (Id) report parameter
                        reportParameters.Add(new ReportParameter("ServerName", Session["ServerName"].ToString()));
                        reportParameters.Add(new ReportParameter("DatabaseName", Session["DatabaseName"].ToString()));
                        reportParameters.Add(new ReportParameter("UserName", Session["UserName"].ToString()));
                        reportParameters.Add(new ReportParameter("OutboundShipmentId", Session["OutboundShipmentId"].ToString()));
                        reportParameters.Add(new ReportParameter("IssueId", Session["IssueId"].ToString()));

                        Session["ReportParameters"] = reportParameters.ToArray();

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                        break;
                    }
                }
            }
            if (e.CommandName == "PrintGRN")
            {

                foreach (GridDataItem item in RadGridOrders.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["InboundShipmentId"] = item.GetDataKeyValue("InboundShipmentId");
                        Session["ReceiptId"] = item.GetDataKeyValue("ReceiptId");

                        Session["FromURL"] = "~/Inbound/ReceivingDocument.aspx";
                        //Session["ReportName"] = "Goods Received Note";

                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 424))
                            Session["ReportName"] = "GRN and Serial Numbers";
                        else
                            Session["ReportName"] = "Goods Received Note";

                        ReportParameter[] RptParameters = new ReportParameter[5];

                        // Create the ConnectionString report parameter
                        //string strReportConnectionString = Session["ReportConnectionString"].ToString();
                        //RptParameters[0] = new ReportParameter("ConnectionString", strReportConnectionString);

                        // Create the InboundShipmentId report parameter
                        string inboundShipmentId = Session["InboundShipmentId"].ToString();
                        RptParameters[0] = new ReportParameter("InboundShipmentId", inboundShipmentId);

                        // Create the ReceiptId report parameter
                        string receiptId = Session["ReceiptId"].ToString();
                        RptParameters[1] = new ReportParameter("ReceiptId", receiptId);

                        RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                        RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Received" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridOrders_ItemCommand
}