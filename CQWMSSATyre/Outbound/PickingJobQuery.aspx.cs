using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_PickingJobQuery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        GridViewJob.DataBind();
    }

    #region GetEditIndex
    protected void GetEditIndex()
    {
        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    rowList.Add(GridViewInstruction.DataKeys[row.RowIndex].Values["InstructionId"]);

                index++;
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch { }

    }
    #endregion "GetEditIndex"

    #region ButtonReset_Click
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            Status status = new Status();
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    if (!status.ResetStatus(Session["ConnectionStringName"].ToString(), int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["InstructionId"].ToString())))
                    {
                        Master.ErrorText = "Please try again.";
                        break;
                    }
            }

            GridViewInstruction.DataBind();

            Master.MsgText = "Reset";
        }
        catch { }
    }
    #endregion "ButtonReset_Click"

    #region ButtonNoStock_Click
    protected void ButtonNoStock_Click(object sender, EventArgs e)
    {
        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            Status status = new Status();
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewInstruction.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    if (!status.SetNoStock(Session["ConnectionStringName"].ToString(), int.Parse(GridViewInstruction.DataKeys[row.RowIndex].Values["InstructionId"].ToString())))
                    {
                        Master.ErrorText = "Please try again.";
                        break;
                    }
            }

            GridViewInstruction.DataBind();

            Master.MsgText = "No-Stock";
        }
        catch { }
    }
    #endregion "ButtonNoStock_Click"

    #region ButtonManualLocations_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        try
        {
            if (GridViewInstruction.EditIndex == -1)
            {
                GetEditIndex();
            }
            else
            {
                ArrayList rowList = new ArrayList();

                rowList.Add(GridViewInstruction.DataKeys[GridViewInstruction.EditIndex].Values["InstructionId"]);

                Session["checkedList"] = rowList;
            }

            Session["FromURL"] = "~/Outbound/PickingJobQuery.aspx";
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");

            Master.MsgText = "Manual Location"; Master.ErrorText = "";
        }
        catch { }
    }
    #endregion "ButtonManualLocations_Click"
}
