<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="OutboundPOLink.aspx.cs" Inherits="Outbound_OutboundPOLink" Title="<%$ Resources:Default, OutboundDocumentTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="uc4" %>
<%@ Register Src="../Common/CustomerSearch.ascx" TagName="ExternalCompanySearch"
    TagPrefix="uc2" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ Register Src="../Common/InboundSearch.ascx" TagName="InboundSearch" TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, OutboundDocumentTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, OutboundDocumentAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                    OnClick="ButtonSearch_Click" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundDocument" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOutboundDocument" runat="server" DataSourceID="ObjectDataSourceOutboundDocument"
                            AllowPaging="true" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewOutboundDocument_OnSelectedIndexChanged"
                            DataKeyNames="OutboundDocumentId,ExternalCompanyId">
                            <Columns>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:BoundField DataField="ExternalCompanyCode" SortExpression="ExternalCompanyCode"
                                    HeaderText="<%$ Resources:Default, CustomerCode %>"></asp:BoundField>
                                <asp:BoundField DataField="ExternalCompany" SortExpression="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="NumberOfLines" SortExpression="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" SortExpression="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="<%$ Resources:Default, Status %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="OutboundDocumentType" SortExpression="OutboundDocumentType"
                                    HeaderText="<%$ Resources:Default, OutboundDocumentType %>"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocument" runat="server" SelectMethod="SearchOutboundDocument"
                    TypeName="OutboundDocument">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, InsertEdit%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelDetailsViewOutboundDocument">
                                <ContentTemplate>
                                    <asp:Button ID="ButtonChangeMode" runat="server" Text="<%$ Resources:Default, Insert%>"
                                        OnClick="ButtonChangeMode_Click" Visible="false" />
                                    <asp:DetailsView ID="DetailsViewOutboundDocument" runat="server" DataSourceID="ObjectDataSourceOutboundDocumentUpdate"
                                        DataKeyNames="OutboundDocumentId" AutoGenerateEditButton="true" AutoGenerateInsertButton="true"
                                        AutoGenerateDeleteButton="true" AutoGenerateRows="false" OnModeChanging="DetailsViewOutboundDocument_OnModeChanging"
                                        OnItemInserting="DetailsViewOutboundDocument_OnItemInserting" OnDataBound="DetailsViewOutboundDocument_OnDataBound">
                                        <Fields>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,OutboundDocumentType %>">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataSourceID="ObjectDataSourceOutboundDocumentType"
                                                        DataValueField="OutboundDocumentTypeId" DataTextField="OutboundDocumentType"
                                                        SelectedValue='<%# Bind("OutboundDocumentTypeId") %>'>
                                                    </asp:DropDownList>
                                                    <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType" runat="server" TypeName="OutboundDocumentType"
                                                        SelectMethod="GetOutboundDocumentTypeParameters">
                                                        <SelectParameters>
                                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                                Type="String" />
                                                        </SelectParameters>
                                                    </asp:ObjectDataSource>
                                                    <asp:HiddenField ID="HiddenOperatorId" runat="server" Value='<%# Bind("OperatorId") %>' />
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataSourceID="ObjectDataSourceOutboundDocumentType"
                                                        DataValueField="OutboundDocumentTypeId" DataTextField="OutboundDocumentType"
                                                        SelectedValue='<%# Bind("OutboundDocumentTypeId") %>'>
                                                    </asp:DropDownList>
                                                    <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType" runat="server" TypeName="OutboundDocumentType"
                                                        SelectMethod="GetOutboundDocumentTypeParameters">
                                                        <SelectParameters>
                                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                                Type="String" />
                                                        </SelectParameters>
                                                    </asp:ObjectDataSource>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("OutboundDocumentType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField ReadOnly="True" DataField="ExternalCompanyId" Visible="False"></asp:BoundField>
                                            <asp:BoundField ReadOnly="True" InsertVisible="false" DataField="ExternalCompany"
                                                HeaderText='<%$ Resources:Default,Customer %>'></asp:BoundField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,OrderNumber %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBoxOrderNumberEdit" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="REQOrderNumberEdit" runat="server" ControlToValidate="TextBoxOrderNumberEdit"
                                                        ErrorMessage="Please enter Order Number"></asp:RequiredFieldValidator>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:TextBox ID="TextBoxOrderNumberInsert" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="REQOrderNumberInsert" runat="server" ControlToValidate="TextBoxOrderNumberInsert"
                                                        ErrorMessage="Please enter Order Number"></asp:RequiredFieldValidator>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,DeliveryDate %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBoxDeliveryDateEdit" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeliveryDateInsert" runat="server"
                                                        ControlToValidate="TextBoxDeliveryDateEdit" ErrorMessage="Please enter Delivery Date"></asp:RequiredFieldValidator>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:TextBox ID="TextBoxDeliveryDateInsert" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderFromDate" runat="server" Animated="true"
                                                        Format="<%$ Resources:Default,DateFormat %>" TargetControlID="TextBoxDeliveryDateInsert">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeliveryDateInsert" runat="server"
                                                        ControlToValidate="TextBoxDeliveryDateInsert" ErrorMessage="Please enter Delivery Date"></asp:RequiredFieldValidator>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonChangeMode" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentUpdate" runat="server"
                                SelectMethod="GetOutboundDocument" TypeName="OutboundDocument" UpdateMethod="UpdateOutboundDocument"
                                InsertMethod="CreateOutboundDocument" DeleteMethod="DeleteOutboundDocument" OnInserted="ObjectDataSourceOutboundDocumentUpdate_OnInserted">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Type="Int32" Name="OutboundDocumentId" SessionField="OutboundDocumentId">
                                    </asp:SessionParameter>
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:Parameter Name="OutboundDocumentId" Type="Int32"></asp:Parameter>
                                    <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32"></asp:Parameter>
                                    <asp:SessionParameter Name="ExternalCompanyId" SessionField="ExternalCompanyId" Type="Int32" />
                                    <asp:Parameter Name="OrderNumber" Type="String"></asp:Parameter>
                                    <asp:Parameter Name="DeliveryDate" Type="DateTime"></asp:Parameter>
                                </UpdateParameters>
                                <InsertParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                    <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32"></asp:Parameter>
                                    <asp:SessionParameter Name="ExternalCompanyId" SessionField="ExternalCompanyId" Type="Int32" />
                                    <asp:Parameter Name="OrderNumber" Type="String"></asp:Parameter>
                                    <asp:Parameter Name="DeliveryDate" Type="DateTime"></asp:Parameter>
                                    <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
                                </InsertParameters>
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                    <asp:Parameter Name="OutboundDocumentId" Type="Int32"></asp:Parameter>
                                </DeleteParameters>
                            </asp:ObjectDataSource>
                            <br />
                            <asp:Button ID="ButtonQuickRelease" runat="server" Text="<%$ Resources:Default, QuickRelease%>"
                                OnClick="ButtonQuickRelease_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:Label ID="LabelErrorTab2" runat="server" EnableTheming="false" ForeColor="red"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonChangeMode" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <br />
                            <asp:Panel ID="PanelCustomer" runat="server" GroupingText="<%$ Resources:Default, SelectCustomer%>">
                                <asp:UpdatePanel runat="server" ID="UpdatePanelExternalCompanySearch" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc2:ExternalCompanySearch ID="ExternalCompanySearch1" runat="server"></uc2:ExternalCompanySearch>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Lines%>"
            Height="100%">
            <ContentTemplate>
                <ajaxToolkit:Accordion ID="AccordionLineInsert" runat="server" SelectedIndex="0"
                    HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
                    FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
                    SuppressHeaderPostbacks="true">
                    <Panes>
                        <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                            <Header>
                                <a href="" class="accordionLink">1. Select a PO</a></Header>
                            <Content>
                                <asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtOrderNumber" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblType" runat="server" Text="<%$ Resources:Default, InboundDocumentType %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlInboundDocumentType" runat="server" DataTextField="InboundDocumentType"
                                                    DataValueField="InboundDocumentTypeId" DataSourceID="ObjectDataSourceInboundDocumentTypeId"
                                                    Width="150px" OnSelectedIndexChanged="ddlInboundDocumentType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSupplier" runat="server" Text="<%$ Resources:Default, Supplier %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSupplier" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDivision" runat="server" Text="<%$ Resources:Default, Division %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDivision" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblToDate" runat="server" Text="<%$ Resources:Default, ToDate %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentTypeId" runat="server" SelectMethod="GetInboundDocumentTypes"
                                    TypeName="InboundDocumentType">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                            Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                                <ajaxToolkit:RoundedCornersExtender ID="RoundedCornersExtenderSearch" runat="server"
                                    TargetControlID="PanelSearch" Radius="10" Color="#EFEFEC" BorderColor="#404040"
                                    Corners="All" />
                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderJobNumber" runat="server"
                                    TargetControlID="txtOrderNumber" WatermarkCssClass="watermarked" WatermarkText="{All}">
                                </ajaxToolkit:TextBoxWatermarkExtender>
                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                    TargetControlID="txtSupplier" WatermarkCssClass="watermarked" WatermarkText="{All}">
                                </ajaxToolkit:TextBoxWatermarkExtender>
                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                    TargetControlID="txtDivision" WatermarkCssClass="watermarked" WatermarkText="{All}">
                                </ajaxToolkit:TextBoxWatermarkExtender>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtenderFromDate" runat="server" Animated="true"
                                    Format="yyyy/MM/dd" TargetControlID="txtFromDate">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtenderFromDate" runat="server" Mask="9999/99/99"
                                    MaskType="Date" CultureName="en-ZA" MessageValidatorTip="true" TargetControlID="txtFromDate">
                                </ajaxToolkit:MaskedEditExtender>
                                <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidatorFromDate" runat="server"
                                    ControlExtender="MaskedEditExtenderFromDate" ControlToValidate="txtFromDate"
                                    Display="Dynamic" EmptyValueMessage="From Date is required" InvalidValueMessage="From Date is invalid"
                                    IsValidEmpty="False" TooltipMessage="Input a From Date">
                                </ajaxToolkit:MaskedEditValidator>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtenderToDate" runat="server" Animated="true"
                                    Format="yyyy/MM/dd" TargetControlID="txtToDate">
                                </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtenderToDate" runat="server" Mask="9999/99/99"
                                    MaskType="Date" CultureName="en-ZA" MessageValidatorTip="true" TargetControlID="txtToDate">
                                </ajaxToolkit:MaskedEditExtender>
                                <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidatorToDate" runat="server" ControlExtender="MaskedEditExtenderToDate"
                                    ControlToValidate="txtToDate" Display="Dynamic" EmptyValueMessage="To Date is required"
                                    InvalidValueMessage="To Date is invalid" IsValidEmpty="False" TooltipMessage="Input a To Date">
                                </ajaxToolkit:MaskedEditValidator>
                                <br />
                                <div style="clear: left;">
                                </div>
                                <asp:Button ID="ButtonPO" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonPOSearch_Click" />
                                <br />
                                <asp:UpdatePanel runat="server" ID="UpdatePanelP0Search">
                                    <ContentTemplate>
                                        <asp:GridView ID="GridViewInboundDocument" runat="server" DataSourceID="ObjectDataSourceInboundDocument"
                                            AllowPaging="true" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewInboundDocument_OnSelectedIndexChanged"
                                            DataKeyNames="InboundDocumentId,ExternalCompanyId">
                                            <Columns>
                                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                                <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ExternalCompany" SortExpression="ExternalCompany" HeaderText="<%$ Resources:Default, Supplier %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Division" SortExpression="Division" HeaderText="<%$ Resources:Default, Division %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NumberOfLines" SortExpression="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DeliveryDate" SortExpression="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="<%$ Resources:Default, Status %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="InboundDocumentType" SortExpression="OutboundDocumentType"
                                                    HeaderText="<%$ Resources:Default, OutboundDocumentType %>"></asp:BoundField>
                                                <asp:BoundField DataField="ReferenceNumber" SortExpression="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DeliveryNoteNumber" SortExpression="DeliveryNoteNumber"
                                                    HeaderText="<%$ Resources:Default, DeliveryNoteNumber %>"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:ObjectDataSource ID="ObjectDataSourceInboundDocument" runat="server" SelectMethod="SearchInboundDocumentLink"
                                    TypeName="InboundDocument">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                            Type="String" />
                                        <asp:SessionParameter Name="InboundDocumentTypeId" SessionField="InboundDocumentTypeId"
                                            Type="Int32" />
                                        <asp:ControlParameter Name="ExternalCompany" ControlID="txtSupplier" Type="String" />
                                        <asp:ControlParameter Name="Division" ControlID="txtDivision" Type="String" />
                                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                        <asp:ControlParameter Name="OrderNumber" ControlID="txtOrderNumber" Type="String" />
                                        <asp:ControlParameter Name="FromDate" ControlID="txtFromDate" Type="DateTime" />
                                        <asp:ControlParameter Name="ToDate" ControlID="txtToDate" Type="DateTime" />
                                        <asp:SessionParameter Name="ReturnType" SessionField="ReturnType" Type="String" DefaultValue="RET" />
                                        <asp:SessionParameter Name="ContactListId" SessionField="ContactListId" Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                        <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                            <Header>
                                <a href="" class="accordionLink">2. Select Products</a></Header>
                            <Content>
                                <asp:UpdatePanel runat="server" ID="UpdatePanelProductSearch">
                                    <ContentTemplate>
                                        <asp:GridView runat="server" ID="GridViewLinkedProducts" DataSourceID="ObjectDataSourceLinkedProducts"
                                            AllowPaging="true" AutoGenerateColumns="False" DataKeyNames="StorageUnitId, BatchId">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="CheckBoxProductSelectAll" AutoPostBack="true" runat="server"
                                                        OnCheckedChanged="CheckBoxProductSelectAll_CheckedChanged" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBoxLinkProduct" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ProductCode" SortExpression="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Product" SortExpression="Product" HeaderText="<%$ Resources:Default, Product %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Barcode" SortExpression="Barcode" HeaderText="<%$ Resources:Default, Barcode %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Batch" SortExpression="Batch" HeaderText="<%$ Resources:Default, Batch %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SKUCode" SortExpression="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AvailableQuantity" SortExpression="AvailableQuantity"
                                                    HeaderText="<%$ Resources:Default, AvailableQuantity %>"></asp:BoundField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Default, RequiredQuantity %>">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBoxRequiredQuantity" runat="server" Text='<%# Bind("AvailableQuantity") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:ObjectDataSource ID="ObjectDataSourceLinkedProducts" runat="server" SelectMethod="SearchInboundDocumentLinkedProducts"
                                    TypeName="InboundDocument">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                            Type="String" />
                                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                        <asp:SessionParameter Name="InboundDocumentId" SessionField="InboundDocumentId" Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                    </Panes>
                </ajaxToolkit:Accordion>
                <asp:Button ID="ButtonInsertLine" runat="server" Text="<%$ Resources:Default, Insert%>"
                    OnClick="ButtonInsertLine_Click" />
                <asp:Button ID="ButtonInsertAllLines" runat="server" Text="<%$ Resources:Default, AllLines%>"
                    OnClick="ButtonInsertAllLines_Click" />                    
                <asp:Button ID="ButtonDeleteLine" runat="server" Text="<%$ Resources:Default, Delete%>"
                    OnClick="ButtonDeleteLine_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonDeleteLine" runat="server"
                    ConfirmText="<%$ Resources:Default, PressOkDelete%>" TargetControlID="ButtonDeleteLine">
                </ajaxToolkit:ConfirmButtonExtender>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundLine">
                    <ContentTemplate>
                        <asp:Label ID="LabelErrorMsg" runat="server" Text="" EnableTheming="false" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="GridViewOutboundLine" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                            AutoGenerateSelectButton="true" AutoGenerateEditButton="true" DataKeyNames="OutboundLineId"
                            DataSourceID="ObjectDataSourceOutboundLine">
                            <Columns>
                                <asp:BoundField DataField="ProductCode" ReadOnly="true" HeaderText="<%$ Resources:Default, ProductCode %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Product" ReadOnly="true" HeaderText="<%$ Resources:Default, Product %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" ReadOnly="true" HeaderText="<%$ Resources:Default, SKUCode %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Batch" ReadOnly="true" HeaderText="<%$ Resources:Default, Batch %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="ECLNumber" ReadOnly="true" HeaderText="<%$ Resources:Default, ECLNumber %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" ReadOnly="true" HeaderText="<%$ Resources:Default, Status %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceOutboundLine" runat="server" TypeName="OutboundDocument"
                    SelectMethod="SearchOutboundLine" UpdateMethod="UpdateOutboundLine" InsertMethod="CreateOutboundLine"
                    DeleteMethod="DeleteOutboundLine">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Type="Int32" Name="OutboundDocumentId" SessionField="OutboundDocumentId" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="OutboundLineId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="Quantity" Type="Int32"></asp:Parameter>
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentId" SessionField="OutboundDocumentId"
                            Type="Int32" />
                        <asp:SessionParameter Name="StorageUnitId" SessionField="StorageUnitId" Type="Int32" />
                        <asp:ControlParameter Name="Quantity" ControlID="TextBoxQuantity" Type="Int32" />
                        <asp:SessionParameter Name="BatchId" SessionField="BatchId" Type="Int32" />
                    </InsertParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="OutboundLineId" Type="Int32"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
