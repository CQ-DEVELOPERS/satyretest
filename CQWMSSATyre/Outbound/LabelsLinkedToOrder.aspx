﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="LabelsLinkedToOrder.aspx.cs" Inherits="Outbound_LabelsLinkedToOrder" Title="<%$ Resources:Default, PlanningMaintenanceTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/ReferenceNumberSearch.ascx" TagName="ReferenceNumberSearch" TagPrefix="rn1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PlanningMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PlanningMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="Label Link">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <rn1:ReferenceNumberSearch ID="ReferenceNumberSearch" runat="server">
                            </rn1:ReferenceNumberSearch>
                        </td>
                    </tr>
                </table>
               <%-- <table>
                    <tr>
                        <td>
                            <asp:Button ID="Button1" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"/></asp:Button>
                        </td>
                    </tr>
                </table>--%>
                <asp:Button ID="ButtonSelect" runat="server" Text="Select All" OnClick="ButtonSelect_Click"></asp:Button>
                <asp:Button ID="ButtonButtonPrint" OnClick="ButtonPrint_Click" runat="server" Text="<%$ Resources:Default, ButtonLabel %>"/>
                <asp:Button ID="ButtonPrintJob" OnClick="ButtonPrintJob_Click" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"/>
                    <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                        <ContentTemplate>
                            <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                                    AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged"
                                    DataKeyNames="JobId">
                                <Columns>
                                    <asp:TemplateField HeaderText="Label">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxEdit" runat="server" Enabled='<%# Bind("Enabled") %>'>
                                            </asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField SelectText="Select" ShowSelectButton="True" />
                                        <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                            <ItemStyle Wrap="False">
                                            </ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                            <ItemStyle Wrap="False">
                                            </ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                            <ItemStyle Wrap="False">
                                            </ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                            <ItemStyle Wrap="False">
                                            </ItemStyle>
                                        </asp:BoundField>
                                         <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                            <ItemStyle Wrap="False">
                                            </ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                            <ItemStyle Wrap="False">
                                            </ItemStyle>
                                        </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="OutboundWIP"
                                        SelectMethod="SearchPickingJobs">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                                    <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                                    <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="ButtonButtonPrint" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="ButtonPrintJob" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
        <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator" SelectMethod="GetOperatorList">
            <SelectParameters>
                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:Content>
