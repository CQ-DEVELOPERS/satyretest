<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="DespatchCheck.aspx.cs" Inherits="Outbound_DespatchCheck" Title="<%$ Resources:Default, DespatchCheckTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DespatchCheckTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DespatchCheckAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                <asp:Button ID="ButtonSearch" runat="server" 
                    Text="<%$ Resources:Default, Search %>" OnClick="ButtonSearch_Click" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:Button ID="ButtonCheckSheet" runat="server" 
                    Text="<%$ Resources:Default, PrintCheckSheet %>" 
                    OnClick="ButtonCheckSheet_Click" style="Width:auto;" />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundDocument">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOutboundDocument" runat="server" DataSourceID="ObjectDataSourceOutboundDocument"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewOutboundDocument_OnSelectedIndexChanged"
                            DataKeyNames="OutboundShipmentId,IssueId">
                            <Columns>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, DespatchLocation %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocument" runat="server" TypeName="DespatchChecking"
                            SelectMethod="SearchPickingDocuments">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
                                <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                                <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                                <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                                <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonCheckSheet" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, Jobs%>">
            <ContentTemplate>
                <asp:Button ID="ButtonSelect" runat="server" Text="<%$ Resources:Default, SelectAll%>" OnClick="ButtonSelect_Click"/>
                <asp:Button ID="ButtonDespatched" runat="server" Text="<%$ Resources:Default, Despatched%>" OnClick="ButtonDespatched_Click" />
                <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                            AutoGenerateColumns="False" DataKeyNames="JobId">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDespatched" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="DespatchChecking"
                    SelectMethod="SearchPickingJobs">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                        <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                        <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>

