<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="Boxing.aspx.cs" Inherits="Outbound_Boxing"
    Title="<%$ Resources:Default, BoxingTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundShipmentWaveSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, BoxingTitle  %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, BoxingAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <script type="text/javascript">
        //<![CDATA[

        //Standard Window.confirm
        function FinishConfirm(sender, args) {
            args.set_cancel(!window.confirm("Are you sure you want to finish the job?"));
        }
        function DeleteConfirm(sender, args) {
            args.set_cancel(!window.confirm("Are you sure you want to delete the job?"));
        }
    </script>
    <telerik:radajaxloadingpanel runat="server" id="LoadingPanel1">
    </telerik:radajaxloadingpanel>
    <telerik:radajaxmanager id="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridOrders">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGridOrders" />--%>
                    <telerik:AjaxUpdatedControl ControlID="RadGridJobs" />
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGridDetails" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonSearch" EventName="Click">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGridOrders" />--%>
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinkedJobs" />
                    <telerik:AjaxUpdatedControl ControlID="ddJobs" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonRadButtonConsolidationSlip" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonSave" EventName="Click">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGridOrders" />--%>
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinkedJobs" />
                    <telerik:AjaxUpdatedControl ControlID="ddJobs" />
                </UpdatedControls>
            </telerik:AjaxSetting>
           
            <telerik:AjaxSetting AjaxControlID="RadGridLinkedJobs">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinkedJobs" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadButtonPrint" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinkedJobs" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadButtonFinished" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadButtonRadButtonConsolidationSlip" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadButtonDefault" EventName="Click">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGridOrders" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadButtonDefaultPicked" EventName="Click">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGridOrders" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadButtonPrint" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinkedJobs" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadButtonDelete" EventName="Click">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGridOrders" />--%>
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinkedJobs" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="ddJobs">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="RadGridOrders" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:radajaxmanager>
    <br />
    <table>
        <tr>
            <td>
                <telerik:radtextbox id="TextBoxReferenceNumber" runat="server" label="Reference:" text=""></telerik:radtextbox>
            </td>
            <td>
                <telerik:radbutton id="RadButtonSearch" onclick="RadButtonSearch_Click" runat="server" text="<%$ Resources:Default, ButtonSearch %>">
                    <Icon PrimaryIconCssClass="rbSearch" />
                </telerik:radbutton>
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td>
                <telerik:radbutton id="RadButtonDefault" onclick="RadButtonDefault_Click" runat="server" text="Default Box Qty">
                    <Icon PrimaryIconCssClass="rbDownload" />
                </telerik:radbutton>
            </td>
            <td>
                <telerik:radbutton id="RadButtonDefaultPicked" onclick="RadButtonDefaultPicked_Click" runat="server" text="Default Pick Qty">
                    <Icon PrimaryIconCssClass="rbDownload" />
                </telerik:radbutton>
            </td>
            <td>
                <telerik:radcombobox id="ddJobs" runat="server" datasourceid="ObjectDataSourceJobsList" skin="Metro"
                    datatextfield="ReferenceNumber" datavaluefield="JobId" autopostback="false" onselectedindexchanged="ddJobs_OnSelectedIndexChanged">
                </telerik:radcombobox>
                <asp:ObjectDataSource ID="ObjectDataSourceJobsList" runat="server" TypeName="ProductCheck"
                    SelectMethod="JobsList">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="jobId" SessionField="jobId" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <%--  <td>
                <telerik:RadTextBox ID="TextBoxWeight" runat="server" Label="<%$ Resources:Default, Weight %>" Text="0"></telerik:RadTextBox>
            </td>--%>
            <td>
                <telerik:radbutton id="RadButtonSave" onclick="RadButtonSave_Click" runat="server" text="<%$ Resources:Default, ButtonSave %>">
                    <Icon PrimaryIconCssClass="rbSave" />
                </telerik:radbutton>
            </td>
            <td>
                <telerik:radbutton id="RadButtonFinished" runat="server" text="<%$ Resources:Default, ButtonFinish %>" onclick="RadButtonFinished_Click" onclientclicking="FinishConfirm">
                    <Icon PrimaryIconCssClass="rbUpload" />
                </telerik:radbutton>
            </td>
            <td>
                <telerik:radbutton id="RadButtonRadButtonConsolidationSlip" runat="server" text="Consolidation Slip" onclick="RadButtonConsolidationSlip_Click">
                    <Icon PrimaryIconCssClass="rbPrint" />
                </telerik:radbutton>
            </td>
        </tr>
    </table>
    <br />
    <asp:UpdatePanel runat="server" ID="UpdatePanel" RenderMode="Inline">
        <ContentTemplate>
            <telerik:RadGrid ID="RadGridOrders" runat="server" Skin="Metro" AllowAutomaticUpdates="true"
                AllowFilteringByColumn="false" allowsorting="True" AllowPaging="true" PageSize="15"
                OnItemDataBound="RadGridOrders_ItemDataBound"
                OnPageIndexChanged="RadGridOrders_PageIndexChanged">
        <PagerStyle Mode="NumericPages" PageSizeControlType="None" Position="TopAndBottom"></PagerStyle>
        <MasterTableView DataKeyNames="JobId, InstructionId, PickedQuantity, BoxQuantity, Boxes, IssueLineId, OrderNumber"
             AutoGenerateColumns="False" EnableHeaderContextMenu="true" ShowGroupFooter="true">
            <Columns>
                <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>" SortExpression="SKU"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="PickedQuantity" HeaderText="<%$ Resources:Default, PickedQuantity %>" SortExpression="PickedQuantity" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total Picked : "></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="CheckedQuantity" HeaderText="<%$ Resources:Default, CheckQuantity %>" SortExpression="CheckedQuantity" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total Checked : "></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Variance" HeaderText="<%$ Resources:Default, Variance %>" SortExpression="Variance" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total Variance : "></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn UniqueName="PackQuantity" HeaderText="<%$ Resources:Default, PackQuantity %>">
                    <ItemTemplate>
                        <telerik:RadTextBox ID="rtbPackQuantityItem" runat="server" Text='<%# Bind("PackQuantity") %>' Width="80px"></telerik:RadTextBox>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="Boxes" HeaderText="Boxes">
                    <ItemTemplate>
                        <telerik:RadNumericTextBox ID="rntbBoxes" runat="server" Width="80px" Value="1" MinValue="1" MaxValue="100000" showspinbuttons="true" incrementsettings-interceptarrowkeys="true" incrementsettings-interceptmousewheel="true">
                            <NumberFormat AllowRounding="false" DecimalDigits="0" />
                        </telerik:RadNumericTextBox>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <%--<telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>--%>
                <telerik:GridBoundColumn ReadOnly="True" DataField="JobQuantity" HeaderText="<%$ Resources:Default, JobQuantity %>" SortExpression="JobQuantity" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total Job Quantity : "></telerik:GridBoundColumn>
            </Columns>
         <%--   <GroupByExpressions>
                <telerik:GridGroupByExpression>
                <SelectFields>
                    <telerik:GridGroupByField FieldAlias="JobId" FieldName="JobId" />
                </SelectFields>
                <GroupByFields>
                    <telerik:GridGroupByField FieldAlias="JobId" FieldName="JobId" />
                </GroupByFields>
                </telerik:GridGroupByExpression>
            </GroupByExpressions>--%>
        </MasterTableView>
        <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
            <Resizing AllowColumnResize="true"></Resizing>
        </ClientSettings>
        <%--<GroupingSettings ShowUnGroupButton="True" />--%>
    </telerik:RadGrid>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="RadButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>

    <asp:ObjectDataSource ID="ObjectDataSourceProductChecking" runat="server" TypeName="ProductCheck"
        SelectMethod="SearchJob" UpdateMethod="UpdateJob">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
            <asp:ControlParameter Name="ReferenceNumber" ControlID="TextBoxReferenceNumber" Type="String" />
            <asp:SessionParameter Name="NewJobId" SessionField="NewJobId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
            <asp:Parameter Name="JobId" Type="Int32" />
            <asp:Parameter Name="InstructionId" Type="Int32" />
            <asp:Parameter Name="PackQuantity" Type="Decimal" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <br />
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelLabels" runat="server" Text="Labels:"></asp:Label>
            </td>
            <td>
                <telerik:radnumerictextbox id="TextBoxLabels" runat="server" value="1" minvalue="1" maxvalue="5" showspinbuttons="true" incrementsettings-interceptarrowkeys="true" incrementsettings-interceptmousewheel="true">
                    <NumberFormat AllowRounding="false" DecimalDigits="0" />
                </telerik:radnumerictextbox>
            </td>
            <td>
                <telerik:radbutton id="RadButtonPrint" runat="server" text="<%$ Resources:Default, ButtonPrint %>" onclick="RadButtonPrint_Click">
                    <Icon PrimaryIconCssClass="rbPrint"></Icon>
                </telerik:radbutton>
            </td>
            <td>
                <telerik:radbutton id="RadButtonDelete" runat="server" text="<%$ Resources:Default, ButtonDelete %>" onclick="RadButtonDelete_Click" onclientclicking="DeleteConfirm">
                    <Icon PrimaryIconCssClass="rbRemove"></Icon>
                </telerik:radbutton>
            </td>
        </tr>
    </table>
    <br />
    <telerik:radgrid id="RadGridLinkedJobs" runat="server" onitemcommand="RadGridLinkedJobs_ItemCommand" skin="Metro" width="500px"
        allowpaging="True" autogeneratecolumns="False" datasourceid="ObjectDataSourceLinkedJobs" allowmultirowselection="True">
        <MasterTableView DataKeyNames="JobId" DataSourceID="ObjectDataSourceLinkedJobs">
            <Columns>
                <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" UniqueName="JobId"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" UniqueName="ReferenceNumber"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" UniqueName="OrderNumber"></telerik:GridBoundColumn>
                <%--<telerik:GridBoundColumn ReadOnly="True" DataField="Weight" HeaderText="<%$ Resources:Default, Weight %>" UniqueName="Weight"></telerik:GridBoundColumn>--%>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Prints" HeaderText="<%$ Resources:Default, Copies %>" UniqueName="Prints"></telerik:GridBoundColumn>
                <telerik:GridButtonColumn Text="<%$ Resources:Default, ButtonPackingSlip %>" HeaderText="<%$ Resources:Default, ButtonPackingSlip %>" CommandName="PackingSlip"></telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
            ReorderColumnsOnClient="True" AllowDragToGroup="True" 
            AllowColumnsReorder="True">
            <Selecting AllowRowSelect="True" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            <Resizing AllowColumnResize="True" />
        </ClientSettings>
    </telerik:radgrid>
    <asp:ObjectDataSource ID="ObjectDataSourceLinkedJobs" runat="server" TypeName="ProductCheck"
        SelectMethod="LinkedJobs">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="jobId" SessionField="jobId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
