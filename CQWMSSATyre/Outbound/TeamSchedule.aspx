<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="TeamSchedule.aspx.cs" Inherits="Outbound_TeamSchedule" Title="<%$ Resources:Default, DefaultOutboundTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultOutboundTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultOutboundAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadScheduler1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadScheduler1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadScheduler ID="RadScheduler1" runat="server" DataEndField="PlannedEnd" GroupBy="InstructionType,OperatorGroupId"
        DataKeyField="TeamScheduleId" DataRecurrenceField="RecurrenceRule" 
        DataRecurrenceParentKeyField="RecurrenceParentID" DataSourceID="SchedulerDataSource"
        DataDescriptionField="Description" 
        Height="700px"
        DataStartField="PlannedStart" DataSubjectField="Subject" Skin="Metro">
        <AdvancedForm Modal="true" />
        <TimelineView UserSelectable="false" />
        <ResourceTypes>
            <telerik:ResourceType DataSourceID="InstructionTypesDataSource" ForeignKeyField="InstructionTypeId" 
                KeyField="InstructionTypeId" Name="InstructionType" TextField="InstructionType" />
        </ResourceTypes>
        <ResourceStyles>
            <telerik:ResourceStyleMapping Type="InstructionType" ApplyCssClass="rsCategoryBlue" Text="DSP1" />
            <telerik:ResourceStyleMapping Type="InstructionType" ApplyCssClass="rsCategoryGreen" Text="DSP2" />
            <telerik:ResourceStyleMapping Type="InstructionType" ApplyCssClass="rsCategoryGray" Text="DSP3" />
            <telerik:ResourceStyleMapping Type="InstructionType" ApplyCssClass="rsCategoryRed" Text="DSP4" />
            <telerik:ResourceStyleMapping Type="InstructionType" ApplyCssClass="rsCategoryLime" Text="DSP5" />
        </ResourceStyles>
    </telerik:RadScheduler>
    <asp:SqlDataSource ID="SchedulerDataSource" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ABI %>" 
        DeleteCommand="DELETE FROM [TeamSchedule] WHERE [TeamScheduleId] = @TeamScheduleId" 
        InsertCommand="INSERT INTO [TeamSchedule] ([Subject], [PlannedStart], [PlannedEnd], [RecurrenceRule], [RecurrenceParentID], [Description], [InstructionTypeId]) VALUES (@Subject, @PlannedStart, @PlannedEnd, @RecurrenceRule, @RecurrenceParentID, @Description, @InstructionTypeId)" 
        SelectCommand="SELECT [TeamScheduleId], [Subject], [PlannedStart], [PlannedEnd], [RecurrenceRule], [RecurrenceParentID], [Description], [InstructionTypeId] FROM [TeamSchedule]" 
        UpdateCommand="UPDATE [TeamSchedule] SET [Subject] = @Subject, [PlannedStart] = @PlannedStart, [PlannedEnd] = @PlannedEnd, [RecurrenceRule] = @RecurrenceRule, [RecurrenceParentID] = @RecurrenceParentID, [Description] = @Description, [InstructionTypeId] = @InstructionTypeId WHERE [TeamScheduleId] = @TeamScheduleId">
        <DeleteParameters>
            <asp:Parameter Name="TeamScheduleId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="PlannedStart" Type="DateTime" />
            <asp:Parameter Name="PlannedEnd" Type="DateTime" />
            <asp:Parameter Name="RecurrenceRule" Type="String" />
            <asp:Parameter Name="RecurrenceParentID" Type="Int32" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="InstructionTypeId" Type="Int32" />
            <asp:Parameter Name="TeamScheduleId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="Subject" Type="String" />
            <asp:Parameter Name="PlannedStart" Type="DateTime" />
            <asp:Parameter Name="PlannedEnd" Type="DateTime" />
            <asp:Parameter Name="RecurrenceRule" Type="String" />
            <asp:Parameter Name="RecurrenceParentID" Type="Int32" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="InstructionTypeId" Type="Int32" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="InstructionTypesDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ABI %>" 
        SelectCommand="SELECT [InstructionTypeId], [InstructionType] FROM [InstructionType] where [InstructionTypeCode] in ('P','PM','PS')">
    </asp:SqlDataSource>
</asp:Content>