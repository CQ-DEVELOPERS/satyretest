using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_PickJobWeightCheck : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";
        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickJobWeightCheck " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion Page Load

    #region ButtonSearch
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickJobWeightCheck " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonSearch

    #region ButtonReadScale_Click
    protected void ButtonReadScale_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReadScale_Click";
        try
        {

            Master.MsgText = "Read Scale"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickJobWeightCheck " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonReadScale_Click

    #region ButtonAccept_Click
    protected void ButtonAccept_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAccept_Click";

        Despatch d = new Despatch();
        string barcode = "";
        decimal tareWeight = 0;
        decimal weight = 0;

        try
        {
            barcode = TextBoxBarcode.Text;

            if (decimal.TryParse(TextBoxTareWeight.Text, out tareWeight) && decimal.TryParse(TextBoxWeight.Text, out weight))
                if (d.AcceptPallet(Session["ConnectionStringName"].ToString(), barcode, tareWeight, weight))
                    ButtonPrint.Enabled = true;
                else
                    ButtonPrint.Enabled = false;


            Master.MsgText = "Accept"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickJobWeightCheck " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonAccept_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "Print";
        try
        {
            string barcode = TextBoxBarcode.Text;
            int jobId = -1;

            TextBoxBarcode.Text = "";
            TextBoxTareWeight.Text = "";
            TextBoxWeight.Text = "";

            ArrayList printList = new ArrayList();

            if (barcode.Substring(0, 2) == "J:")
                barcode = barcode.Substring(3);

            if (!int.TryParse(barcode, out jobId))
                return;

            printList.Add(jobId);

            Session["checkedList"] = printList;

            Session["LabelName"] = "Despatch By Route Label.lbl";

            Session["FromURL"] = "~/Outbound/PickJobWeightCheck.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }    

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickJobWeightCheck " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion ButtonPrint_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PickJobWeightCheck", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PickJobWeightCheck", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
