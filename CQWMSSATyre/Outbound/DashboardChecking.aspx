<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="DashboardChecking.aspx.cs" Inherits="Outbound_DashboardChecking" Title="<%$ Resources:Default, DefaultOutboundTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Charting" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DefaultOutboundTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DefaultOutboundAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridCheckingAchievedGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCheckingAchievedGrid" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <table>
        <tr valign="top">
            <td>
                <telerik:RadButton ID="RadButtonCheckingWorkload" runat="server" Text="Checking Workload" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChartCheckingWorkload" runat="server" ChartTitle-TextBlock-Text="Checking Workload Units" Skin="DeepBlue"
                    DefaultType="Pie"  AutoTextWrap="true" DataSourceID="ObjectDataSourceCheckingWorkload" SeriesOrientation="Horizontal" Width="500px">
                    <Series>
                        <telerik:ChartSeries Name="Series 1" Type="Pie" DataYColumn="Value" DataLabelsColumn="Legend">
                            <Appearance LegendDisplayMode="ItemLabels">
                            </Appearance>
                        </telerik:ChartSeries>
                     </Series>
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourceCheckingWorkload" runat="server" TypeName="Dashboard" SelectMethod="OutboundCheckingWorkload">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <br />
                <telerik:RadButton ID="RadButtonCheckingWorkloadGrid" runat="server" Text="Checking Workload" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridCheckingWorkloadGrid" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceCheckingWorkload" Skin="Outlook" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceCheckingWorkload">
                        <Columns>
                            <telerik:GridBoundColumn DataField="Legend" HeaderText="" UniqueName="Legend"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Units %>" UniqueName="Units"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Jobs" HeaderText="<%$ Resources:Default, Jobs %>" UniqueName="Jobs"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <br />
                <telerik:RadButton ID="RadButtonCheckingQA" runat="server" Text="Checking QA" Skin="Outlook" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridCheckingQA" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceCheckingQA" Skin="Default" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceCheckingQA">
                        <Columns>
                            <telerik:GridBoundColumn DataField="KPI" HeaderText="<%$ Resources:Default, KPI %>"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="Today"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                
                <asp:ObjectDataSource ID="ObjectDataSourceCheckingQA" runat="server" TypeName="Dashboard" SelectMethod="OutboundCheckingQA">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonCheckingAchieved" runat="server" Text="Checking Achieved" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadChart ID="RadChartCheckingAchieved" runat="server" ChartTitle-TextBlock-Text="Checking Achieved per hour" Skin="DeepBlue"
                    DefaultType="Bar" AutoLayout="true" AutoTextWrap="true" DataSourceID="ObjectDataSourceCheckingAchievedGroup" SeriesOrientation="Vertical" Width="500px" Legend-Visible="false">
                    <PlotArea>
                        <XAxis DataLabelsColumn="Legend">
                        </XAxis>
                    </PlotArea>
                    <Series>
                        <telerik:ChartSeries Name="Series 1" Type="Bar" DataYColumn="Value">
                            <Appearance LegendDisplayMode="ItemLabels">
                            </Appearance>
                        </telerik:ChartSeries>
                    </Series>
                </telerik:RadChart>
                
                <asp:ObjectDataSource ID="ObjectDataSourceCheckingAchievedGroup" runat="server" TypeName="Dashboard" SelectMethod="OutboundCheckingAchievedGroup">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
                <asp:ObjectDataSource ID="ObjectDataSourceCheckingAchieved" runat="server" TypeName="Dashboard" SelectMethod="OutboundCheckingAchieved">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <br />
                <%--<telerik:RadButton ID="RadButtonCheckingAchievedKPI" runat="server" Text="Warehouse KPI" Skin="Outlook" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridCheckingAchievedKPI" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceCheckingAchievedKPI" Skin="Default" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceCheckingAchievedKPI">
                        <Columns>
                            <telerik:GridBoundColumn DataField=CheckingAchieved HeaderText="Target / Hour"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="Achieved"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                
                <asp:ObjectDataSource ID="ObjectDataSourceCheckingAchievedKPI" runat="server" TypeName="Dashboard" SelectMethod="OutboundCheckingAchievedKPI">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>--%>
                
                <br />
                <telerik:RadButton ID="RadButtonCheckingAchievedKPIWarehouse" runat="server" Text="Warehouse KPI" Skin="Outlook" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridCheckingAchievedKPIWarehouse" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceCheckingAchieved" Skin="Default" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceCheckingAchieved" ShowGroupFooter="true" GroupsDefaultExpanded="false" ShowHeader="false">
                        <GroupHeaderTemplate>
                            <!-- use the AggregatesValues collection to access the totals for each group header -->
                            <telerik:RadTextBox runat="server" ID="LabelKPI" Text='<%# Eval("WarehouseKPI") %>'  Label="Target / Hour" LabelWidth="125px">
                            </telerik:RadTextBox>
                            <telerik:RadTextBox runat="server" ID="LabelValue" Text='<%# (((GridGroupHeaderItem)Container).AggregatesValues["Value"]) %>' Label="Current Performance" LabelWidth="125px"
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["Value"]) != null)%>'>
                            </telerik:RadTextBox>
                        </GroupHeaderTemplate>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Legend" HeaderText="Hour" UniqueName="Legend"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Units %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total : "></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="WarehouseKPI" HeaderText="<%$ Resources:Default, KPI %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="KPI : "></telerik:GridBoundColumn>
                        </Columns>
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldAlias="WarehouseKPI" FieldName="WarehouseKPI" />
                                    <telerik:GridGroupByField FieldAlias="Value" FieldName="Value" Aggregate="Sum" />
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldAlias="WarehouseKPI" FieldName="WarehouseKPI" />
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                    </MasterTableView>
                </telerik:RadGrid>
                <br />
                <telerik:RadButton ID="RadButtonCheckingAchievedKPIOperator" runat="server" Text="Checker KPI" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridCheckingAchievedKPIOperator" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceCheckingAchieved" Skin="Outlook" EnableTheming="true" Width="500px">
                    <MasterTableView DataSourceID="ObjectDataSourceCheckingAchieved" ShowGroupFooter="true" GroupsDefaultExpanded="false" ShowHeader="false">
                        <GroupHeaderTemplate>
                            <!-- use the AggregatesValues collection to access the totals for each group header -->
                            <telerik:RadTextBox runat="server" ID="LabelKPI" Text='<%# Eval("OperatorKPI") %>'  Label="Units / Hour" LabelWidth="125px"
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["OperatorKPI"]) != null)%>'>
                            </telerik:RadTextBox>
                            <telerik:RadTextBox runat="server" ID="LabelValue" Text='<%# (((GridGroupHeaderItem)Container).AggregatesValues["Value"]) %>' Label="Average / Hour" LabelWidth="125px"
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["Value"]) != null)%>'>
                            </telerik:RadTextBox>
                        </GroupHeaderTemplate>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Legend" HeaderText="Hour" UniqueName="Legend"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Units %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total : "></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="OperatorKPI" HeaderText="<%$ Resources:Default, KPI %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="KPI : "></telerik:GridBoundColumn>
                        </Columns>
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldAlias="OperatorKPI" FieldName="OperatorKPI" />
                                    <telerik:GridGroupByField FieldAlias="Value" FieldName="Value" Aggregate="Avg" />
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldAlias="OperatorKPI" FieldName="OperatorKPI" />
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                    </MasterTableView>
                </telerik:RadGrid>
                <br />
                <telerik:RadButton ID="RadButtonCheckingAchievedGrid" runat="server" Text="Checking Achieved" Skin="Windows7" Width="500px"></telerik:RadButton>
                <telerik:RadGrid ID="RadGridCheckingAchievedGrid" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceCheckingAchieved" Skin="Outlook" EnableTheming="true" Width="500px"
                    OnPreRender="RadButtonCheckingAchievedGrid_PreRender">
                    <MasterTableView DataSourceID="ObjectDataSourceCheckingAchieved" ShowGroupFooter="true" GroupsDefaultExpanded="false">
                        <GroupHeaderTemplate>
                            <!-- use the AggregatesValues collection to access the totals for each group header -->
                            <asp:Label runat="server" ID="LabelOperator" Text='<%# "Operator name: "+Eval("Operator") %>'
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["Operator"]) != null)%>'></asp:Label>
                            <asp:Label runat="server" ID="LabelValue" Text='<%# (((GridGroupHeaderItem)Container).AggregatesValues["Value"]) %>'
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["Value"]) != null)%>'>
                            </asp:Label>
                            <asp:Label runat="server" ID="LabelKPI" Text='<%# Eval("OperatorKPI") %>'
                                Visible='<%# ((((GridGroupHeaderItem)Container).AggregatesValues["OperatorKPI"]) != null)%>'></asp:Label>
                        </GroupHeaderTemplate>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Legend" HeaderText="Hour" UniqueName="Legend"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Value" HeaderText="<%$ Resources:Default, Units %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="Total : "></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="OperatorKPI" HeaderText="<%$ Resources:Default, KPI %>" DataFormatString="{0:G0}" Aggregate="Sum" FooterText="KPI : " Visible="false"></telerik:GridBoundColumn>
                        </Columns>
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldAlias="Operator" FieldName="Operator" />
                                    <telerik:GridGroupByField FieldAlias="Value" FieldName="Value" Aggregate="Sum" />
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldAlias="Operator" FieldName="Operator" />
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Content>