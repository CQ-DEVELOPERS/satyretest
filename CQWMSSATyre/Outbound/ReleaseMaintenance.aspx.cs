using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_ReleaseMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"
    
    #region PRIVATE CONSTANTS
        //private const string DEFAULT_STATUS = "IS";
        StringBuilder strBuilder = new StringBuilder();
        private string result = "";
        private string theErrMethod = "";
    #endregion

    #region Page_Load
     protected void Page_Load(object sender, EventArgs e)
     {
         Session["countLoopsToPreventInfinLoop"] = 0;
         theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;
            }

            if(Session["WarehouseId"] == null)
                Response.Redirect("~/Security/Login.aspx");

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReleaseMaintenance" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
     }
    #endregion Page_Load

    #region ButtonSearch_Click
    /// <summary>
     /// 
     /// </summary>
     /// <param name="sender"></param>
     /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
     {
         theErrMethod = "ButtonSearch_Click";
         try
         {
             if (rblShowOrders.SelectedValue != "true")
             {
                 GridViewShipment.Columns[2].Visible = false;
                 GridViewShipment.Columns[3].Visible = false;
                 GridViewShipment.Columns[4].Visible = false;
             }
             else
             {
                 GridViewShipment.Columns[2].Visible = true;
                 GridViewShipment.Columns[3].Visible = true;
                 GridViewShipment.Columns[4].Visible = true;
             }
             GridViewShipment.DataBind();

             Master.MsgText = ""; Master.ErrorText = "";
         }
         catch (Exception ex)
         {
             result = SendErrorNow("ReleaseMaintenance" + "_" + ex.Message.ToString()); 
             Master.ErrorText = result;
         }
     }
    #endregion ButtonSearch_Click

    #region GridViewShipment_RowDataBound
    protected void GridViewShipment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // This line will get the reference to the underlying row
            DataRowView _row = (DataRowView)e.Row.DataItem;
            if (_row != null)
            {
                switch (_row.Row["Status"].ToString())
                {
                    case "Pause":
                        e.Row.Cells[12].ForeColor = System.Drawing.Color.OrangeRed;
                        e.Row.Cells[12].Font.Bold = true;
                        break;
                    case "Planning Complete":
                        e.Row.Cells[12].ForeColor = System.Drawing.Color.RoyalBlue;
                        e.Row.Cells[12].Font.Bold = true;
                        break;
                    case "Release":
                        e.Row.Cells[12].ForeColor = System.Drawing.Color.Green;
                        e.Row.Cells[12].Font.Bold = true;
                        break;
                    case "Manual":
                        e.Row.Cells[12].ForeColor = System.Drawing.Color.LightSteelBlue;
                        e.Row.Cells[12].Font.Bold = true;
                        break;
                    case "Checking":
                        e.Row.Cells[12].ForeColor = System.Drawing.Color.SaddleBrown;
                        e.Row.Cells[12].Font.Bold = true;
                        break;
                    case "Quality Assurance":
                        e.Row.Cells[12].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[12].Font.Bold = true;
                        break;
                }
            }
        }
    }
    #endregion GridViewShipment_RowDataBound

    #region GridViewShipment_SelectedIndexChanged
    protected void GridViewShipment_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewShipment_SelectedIndexChanged";
        try
        {
            Session["OutboundShipmentId"] = GridViewShipment.SelectedDataKey["OutboundShipmentId"];
            Session["IssueId"] = GridViewShipment.SelectedDataKey["IssueId"];

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReleaseMaintenance" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion GridViewShipment_SelectedIndexChanged

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReleaseMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region GridViewJobs_OnSelectedIndexChanged
    protected void GridViewJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewJobs_OnSelectedIndexChanged";

        try
        {
            if (GridViewJobs.SelectedDataKey["JobId"].ToString() == "")
                Session["JobId"] = -1;
            else
                Session["JobId"] = int.Parse(GridViewJobs.SelectedDataKey["JobId"].ToString());

            GridViewJobs.DataBind();

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReleaseMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewJobs_OnSelectedIndexChanged"

    #region ButtonReleaseNoStock_Click
    protected void ButtonReleaseNoStock_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReleaseNoStock_Click";
        try
        {
            Master.MsgText = "Re-release No-stock"; Master.ErrorText = "";

            int index = GridViewShipment.SelectedIndex;

            if (index != -1)
            {
                OutboundWIP wip = new OutboundWIP();

                int outboundShipmentId = int.Parse(GridViewShipment.DataKeys[index].Values["OutboundShipmentId"].ToString());
                int issueId = int.Parse(GridViewShipment.DataKeys[index].Values["IssueId"].ToString());
                int operatorId = (int)Session["OperatorId"];

                if (outboundShipmentId != -1)
                    issueId = -1;

                if (wip.ReleaseNoStock(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId, operatorId))
                {
                    Master.MsgText = "Re-release of no-stock's successful"; Master.ErrorText = "";

                    GridViewShipment.DataBind();
                }
                else
                {
                    Master.MsgText = ""; Master.ErrorText = "Re-release of no-stock's not complete";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonReleaseNoStock_Click"

    #region ButtonReleaseNoStock2_Click
    protected void ButtonReleaseNoStock2_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReleaseNoStock_Click";
        try
        {
            Master.MsgText = "Re-release No-stock"; Master.ErrorText = "";

            int index = GridViewShipment.SelectedIndex;

            if (index != -1)
            {
                OutboundWIP wip = new OutboundWIP();

                int outboundShipmentId = int.Parse(GridViewShipment.DataKeys[index].Values["OutboundShipmentId"].ToString());
                int issueId = int.Parse(GridViewShipment.DataKeys[index].Values["IssueId"].ToString());
                int operatorId = (int)Session["OperatorId"];
                int storageUnitId = -1;

                if (outboundShipmentId != -1)
                    issueId = -1;

                try
                {
                    CheckBox cb = new CheckBox();

                    foreach (GridViewRow row in GridViewJobs.Rows)
                    {
                        cb = (CheckBox)row.FindControl("CheckBoxEdit");

                        if (cb.Checked)
                            storageUnitId = (int)GridViewJobs.DataKeys[row.RowIndex].Values["StorageUnitId"];

                        if (wip.ReleaseNoStock(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId, operatorId, storageUnitId))
                        {
                            Master.MsgText = "Re-release of no-stock's successful"; Master.ErrorText = "";

                            GridViewJobs.DataBind();
                        }
                        else
                        {
                            Master.MsgText = ""; Master.ErrorText = "Re-release of no-stock's not complete";
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = SendErrorNow("ReleaseMaintenance" + "_" + ex.Message.ToString());
                    Master.ErrorText = result;
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonReleaseNoStock2_Click"

    #region ButtonBackorder_Click
    protected void ButtonBackorder_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonOperatorRelease_Click";
        try
        {
            int index = GridViewShipment.SelectedIndex;

            if (index != -1)
            {
                Backorders back = new Backorders();

                back.BackorderInsert(Session["ConnectionStringName"].ToString(),
                                            int.Parse(GridViewShipment.DataKeys[index].Values["OutboundShipmentId"].ToString()),
                                            int.Parse(GridViewShipment.DataKeys[index].Values["IssueId"].ToString()),
                                            (int)Session["OperatorId"]);

                GridViewShipment.DataBind();
                Master.MsgText = "Release"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_Backorders" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonBackorder_Click"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "ReleaseMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "ReleaseMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
 #endregion ErrorHandling
}