<%@ Page Language="C#" MasterPageFile="~/MasterPages/NoScript.master" AutoEventWireup="true"
    CodeFile="PickingProgress.aspx.cs" Inherits="Outbound_PickingProgress"
    Title="<%$ Resources:Default, PlanningMaintenanceTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundShipmentWaveSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/NoScript.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PlanningMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PlanningMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridOrders">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridJobs" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonDeallocate" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonAutoAllocation" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonRelease" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonManual" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonPause" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonReleaseNoStock" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonOperatorRelease" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridOrderLines">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrderLines" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridJobs">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridJobs" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSelect" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridJobs" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridDetails">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridDetails" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Orders%>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Lines%>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Jobs%>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Details%>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                &nbsp;<table>
                    <tr>
                        <td>
                            <uc1:OutboundSearch ID="OutboundSearch1" runat="server">
                            </uc1:OutboundSearch>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Label ID="LabelNotPicked" runat="server" Text="<%$ Resources:Default, NotPicked %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, Overdue %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="ImageOrange" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelOrange" runat="server" Text="<%$ Resources:Default, Requiredin2hours %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, Requiredin24hours %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelStandard" runat="server" Text="<%$ Resources:Default, RequiredGT24hours %>"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Label ID="LabelPicked" runat="server" Text="<%$ Resources:Default, Picked %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelBlue" runat="server" Text="<%$ Resources:Default, NotAllocatedRoute %>"
                                            Width="110px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        <asp:Image ID="ImageGreen" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelGreen" runat="server" Text="<%$ Resources:Default, NotAllocatedGT1Day %>"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <telerik:RadButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>">
                    <Icon PrimaryIconCssClass="rbSearch" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                </telerik:RadButton>
                <telerik:RadButton ID="ButtonDeallocate" runat="server" Text="<%$ Resources:Default, DeAllocate %>" OnClick="ButtonDeallocate_Click">
                    <Icon PrimaryIconCssClass="rbConfig" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                </telerik:RadButton>
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderDeallocate" runat="server"
                    TargetControlID="ButtonDeallocate" ConfirmText="<%$ Resources:Default, PressOKtoconfirmreverse %>"
                    Enabled="True">
                </ajaxToolkit:ConfirmButtonExtender>
                <telerik:RadButton ID="ButtonRelease" runat="server" Text="<%$ Resources:Default, ButtonRelease %>" OnClick="ButtonRelease_Click">
                    <Icon PrimaryIconCssClass="rbOpen" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                </telerik:RadButton>
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderRelease" runat="server"
                    TargetControlID="ButtonRelease" ConfirmText="<%$ Resources:Default, PressOKtoconfirmrelease %>"
                    Enabled="True">
                </ajaxToolkit:ConfirmButtonExtender>
                <telerik:RadButton ID="ButtonManual" runat="server" Text="<%$ Resources:Default, ButtonManual %>"
                    OnClick="ButtonManual_Click">
                    <Icon PrimaryIconCssClass="rbCart" PrimaryIconLeft="4" PrimaryIconTop="4"></Icon>
                </telerik:RadButton>
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderManual" runat="server"
                    TargetControlID="ButtonManual" ConfirmText="<%$ Resources:Default, PressOKconfirmmanualrelease %>"
                    Enabled="True">
                </ajaxToolkit:ConfirmButtonExtender>
                <telerik:RadButton ID="ButtonPause" runat="server" Text="<%$ Resources:Default, ButtonPause %>"
                    OnClick="ButtonPause_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderPause" runat="server"
                    TargetControlID="ButtonPause" ConfirmText="<%$ Resources:Default, PressOKtopauseload %>"
                    Enabled="True">
                </ajaxToolkit:ConfirmButtonExtender>
                <telerik:RadButton ID="ButtonAutoAllocation" runat="server" Text="<%$ Resources:Default, ButtonAutoAllocation %>"
                    OnClick="ButtonAutoLocations_Click" />
                <telerik:RadButton ID="ButtonReleaseNoStock" runat="server" Text="<%$ Resources:Default, ButtonReleaseNoStock %>"
                    OnClick="ButtonReleaseNoStock_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderReleaseNoStock" runat="server"
                    TargetControlID="ButtonReleaseNoStock" ConfirmText="<%$ Resources:Default, PressOKconfirmrerelease %>">
                </ajaxToolkit:ConfirmButtonExtender>
                <!--<asp:LinkButton CssClass="lnkSubmit" ID="ButtonLoadPlanning" runat="server" Text="<%$ Resources:Default, ButtonLoadPlanning %>" Enabled="false" />-->
                <telerik:RadButton ID="ButtonWavePlanning" runat="server" Text="<%$ Resources:Default, ButtonWavePlanning %>" OnClick="ButtonWavePlanning_Click" />
                <asp:Label ID="LabelOperator" runat="server" Text="<%$ Resources:Default,Operator %>"></asp:Label>
                <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                    DataTextField="Operator" DataValueField="OperatorId">
                </asp:DropDownList>
                <telerik:RadButton ID="ButtonOperatorRelease" runat="server" Text="<%$ Resources:Default, OperatorRelease %>"
                    OnClick="ButtonOperatorRelease_Click" />
                <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                    DataTextField="Priority" DataValueField="PriorityId">
                </asp:DropDownList>
                <telerik:RadButton ID="ButtonPriority" runat="server" Text="<%$ Resources:Default, Priority %>"
                    OnClick="ButtonPriority_Click" />                
                <br />
                <br />
                <telerik:RadGrid ID="RadGridOrders" runat="server" Skin="Metro" AllowAutomaticUpdates="true"
                    AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30"
                    AutoGenerateColumns="False" AllowMultiRowSelection="True"
                    DataSourceID="ObjectDataSourcePlanning" OnRowDataBound="RadGridOrders_RowDataBound"
                    OnSelectedIndexChanged="RadGridOrders_SelectedIndexChanged" OnItemCommand="RadGridOrders_ItemCommand">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="OutboundShipmentId,IssueId" DataSourceID="ObjectDataSourcePlanning" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton" 
                                FilterControlAltText="Filter EditCommandColumn column"></telerik:GridEditCommandColumn>
                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                                UniqueName="TemplateColumn">
                                <ItemTemplate>
                                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Wave" HeaderText="<%$ Resources:Default, Wave %>" SortExpression="Wave"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                SortExpression="OutboundShipmentId" 
                                FilterControlAltText="Filter OutboundShipmentId column" 
                                UniqueName="OutboundShipmentId">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                SortExpression="OrderNumber" 
                                FilterControlAltText="Filter OrderNumber column" UniqueName="OrderNumber">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                SortExpression="CustomerCode" 
                                FilterControlAltText="Filter CustomerCode column" UniqueName="CustomerCode">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>"
                                SortExpression="Customer" FilterControlAltText="Filter Customer column" 
                                UniqueName="Customer">
                                <ItemStyle Wrap="False" Width="100px"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListPriorityId" 
                                ListTextField="Priority" ListValueField="PriorityId"
                                DataSourceID="ObjectDataSourcePriority" DataField="PriorityId" 
                                HeaderText="<%$ Resources:Default, Priority %>" 
                                FilterControlAltText="Filter DropDownListPriorityId column">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="PercentageComplete" HeaderText="<%$ Resources:Default, PercentagePicked %>"
                                SortExpression="PercentageComplete" 
                                FilterControlAltText="Filter PercentageComplete column" 
                                UniqueName="PercentageComplete">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                SortExpression="NumberOfLines" 
                                FilterControlAltText="Filter NumberOfLines column" UniqueName="NumberOfLines">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Units" HeaderText="<%$ Resources:Default, Units %>"
                                SortExpression="Units" FilterControlAltText="Filter Units column" 
                                UniqueName="Units">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="ShortPicks" HeaderText="<%$ Resources:Default, ShortPicks %>"
                                SortExpression="ShortPicks" 
                                FilterControlAltText="Filter ShortPicks column" UniqueName="ShortPicks">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Releases" HeaderText="<%$ Resources:Default, Releases %>"
                                SortExpression="Releases" FilterControlAltText="Filter Releases column" 
                                UniqueName="Releases">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                SortExpression="CreateDate" 
                                FilterControlAltText="Filter CreateDate column" UniqueName="CreateDate">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                SortExpression="DeliveryDate" 
                                FilterControlAltText="Filter DeliveryDate column" UniqueName="DeliveryDate">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                SortExpression="Status" FilterControlAltText="Filter Status column" 
                                UniqueName="Status">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridButtonColumn Text="<%$ Resources:Default, PrintCollectionSlip %>" HeaderText="<%$ Resources:Default, PrintCollectionSlip %>" CommandName="PrintCollectionSlip" DataTextField="PrintCollectionSlip"></telerik:GridButtonColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="CheckingCount" HeaderText="<%$ Resources:Default, CheckingCount %>"
                                SortExpression="CheckingCount" 
                                FilterControlAltText="Filter CheckingCount column" 
                                UniqueName="CheckingCount">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>"
                                SortExpression="OutboundDocumentType" 
                                FilterControlAltText="Filter OutboundDocumentType column" 
                                UniqueName="OutboundDocumentType">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListLocationId" 
                                ListTextField="Location" ListValueField="LocationId"
                                DataSourceID="ObjectDataSourceLocation" DataField="LocationId" 
                                HeaderText="<%$ Resources:Default, Location %>" 
                                FilterControlAltText="Filter DropDownListLocationId column" ReadOnly="True">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Route" HeaderText="<%$ Resources:Default, Route %>"
                                SortExpression="Route" FilterControlAltText="Filter Route column" 
                                UniqueName="Route">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>"
                                SortExpression="Rating" FilterControlAltText="Filter Rating column" 
                                UniqueName="Rating">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Weight" HeaderText="<%$ Resources:Default, Weight %>"
                                SortExpression="Weight" FilterControlAltText="Filter Weight column" 
                                UniqueName="Weight">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>"
                                SortExpression="Remarks" FilterControlAltText="Filter Remarks column" 
                                UniqueName="Remarks">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Comment" HeaderText="<%$ Resources:Default, Comments %>"
                                SortExpression="Comment"		>
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn ButtonType="PushButton" 
                                FilterControlAltText="Filter EditCommandColumn1 column" 
                                UniqueName="EditCommandColumn1">
                            </EditColumn>
                        </EditFormSettings>
                        <CommandItemTemplate>
                            <asp:Button ID="DownloadPDF" runat="server" Width="100%" CommandName="ExportToPdf"
                            CssClass="pdfButton"></asp:Button>
                            <asp:Image ID="Image1" runat="server" ImageUrl="Images/Backgr.jpg" AlternateText="Sushi Bar"
                            Width="100%"></asp:Image>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                        <Resizing AllowColumnResize="true"></Resizing>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                    SelectMethod="GetLocationsByIssue">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="OutboundWIP"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
                        <asp:SessionParameter Name="WaveId" SessionField="WaveId" Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
                    TypeName="Priority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel2">
                <asp:Image ID="ImageRed2" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                <asp:Label ID="LabelRed2" runat="server" Text="<%$ Resources:Default, NoStock%>"></asp:Label>
                <asp:Image ID="ImageYellow2" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                <asp:Label ID="LabelYellow2" runat="server" Text="<%$ Resources:Default, StockShort%>"></asp:Label>
                <asp:Image ID="ImageGreen2" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                <asp:Label ID="LabelGreen2" runat="server" Text="<%$ Resources:Default, StockAvailable%>"></asp:Label>
                
                <telerik:RadGrid ID="RadGridOrderLines" runat="server" Skin="Metro" DataSourceID="ObjectDataSourceOrderLines"
                    AllowAutomaticUpdates="true" AllowMultiRowSelection="true" 
                    AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30" ShowFooter="true" EnableLinqExpressions="true">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="IssueLineId,StorageUnitId" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                        <Columns>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                SortExpression="OrderNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                SortExpression="ProductCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                SortExpression="Product"></telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListBatchId" ListTextField="Batch" ListValueField="StorageUnitBatchId"
                                DataSourceID="ObjectDataSourceBatch" DataField="StorageUnitBatchId" HeaderText="<%$ Resources:Default, Batch %>" DropDownControlType="RadComboBox">
                            </telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                SortExpression="SKUCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>"
                                SortExpression="OrderQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                SortExpression="Status"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>"
                                SortExpression="AvailablePercentage"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>"
                                SortExpression="AvailableQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="LocationsAllocated" HeaderText="<%$ Resources:Default, LocationsAllocated %>"
                                SortExpression="LocationsAllocated"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                        <Resizing AllowColumnResize="true"></Resizing>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                    SelectMethod="GetBatchesByStorageUnit" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:ControlParameter ControlID="RadGridOrderLines" Name="StorageUnitId" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceOrderLines" runat="server" TypeName="OutboundWIP"
                    SelectMethod="GetOrderLines" UpdateMethod="UpdateOrderLine">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId"
                            DefaultValue="-1" />
                        <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" DefaultValue="-1" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Type="Int32" Name="issueLineId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="storageUnitBatchId"></asp:Parameter>
                        <asp:Parameter Type="Int32" Name="storageUnitId"></asp:Parameter>
                    </UpdateParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel3">
                <table>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
                <telerik:RadButton ID="ButtonSelect" OnClick="ButtonSelect_Click" runat="server" Text="<%$ Resources:Default, SelectAll%>" />
                <telerik:RadButton ID="ButtonButtonPrint" OnClick="ButtonPrint_Click" runat="server" Text="<%$ Resources:Default, ButtonLabel %>" />
                <telerik:RadButton ID="ButtonPrintJob" OnClick="ButtonPrintJob_Click" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" />
                <telerik:RadButton ID="ButtonPalletWeight" runat="server" OnClick="ButtonPalletWeight_Click" Text="<%$ Resources:Default, PalletWeight%>" />
                <telerik:RadButton ID="ButtonPauseJob" runat="server" OnClick="ButtonPauseJob_Click" Text="<%$ Resources:Default, ButtonPause%>" />
                <telerik:RadButton ID="ButtonReleaseJob" runat="server" OnClick="ButtonReleaseJob_Click" Text="<%$ Resources:Default, Release%>" />
                <asp:Label ID="LabelQty" runat="server" Text="<%$ Resources:Default, Copies%>"></asp:Label>
                <asp:TextBox ID="TextBoxQty" runat="server" Text="1"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" FilterType="Numbers" TargetControlID="TextBoxQty">
                </ajaxToolkit:FilteredTextBoxExtender>
                <telerik:RadButton ID="ButtonPrintLabel" runat="server" OnClick="ButtonPrintLabel_Click" Text="<%$ Resources:Default, PrintLabel%>" />
                
                <telerik:RadGrid ID="RadGridJobs" runat="server" Skin="Metro" DataSourceID="ObjectDataSourceJobs" AllowMultiRowSelection="true" 
                    AllowAutomaticUpdates="true" AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30"
                    OnSelectedIndexChanged="RadGridJobs_OnSelectedIndexChanged">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="JobId" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                            <telerik:GridBoundColumn DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                        <Resizing AllowColumnResize="true"></Resizing>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="OutboundWIP"
                    SelectMethod="SearchPickingJobs">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                        <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                        <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel4">
                <asp:Image ID="ImageLocationStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                <asp:Label ID="LabelLocationStandard" runat="server" Text="<%$ Resources:Default, NoLocation%>"></asp:Label>
                <asp:Image ID="ImageLocationBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                <asp:Label ID="LabelLocationBlue" runat="server" Text="<%$ Resources:Default, Allocated%>"></asp:Label>
                <telerik:RadGrid ID="RadGridDetails" runat="server" Skin="Metro"
                    AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30"
                    AllowMultiRowSelection="true" AllowAutomaticUpdates="true" DataSourceID="ObjectDataSourceDetails">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="InstructionId" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
                        <Columns>
                            <telerik:GridTemplateColumn>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                SortExpression="OrderNumber"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                SortExpression="ProductCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                SortExpression="Product"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                                SortExpression="Batch"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                SortExpression="SKUCode"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                SortExpression="Quantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>"
                                SortExpression="ConfirmedQuantity"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                                SortExpression="InstructionType"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>"
                                SortExpression="PickLocation"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                SortExpression="Status"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>"
                                SortExpression="Operator"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                        <Resizing AllowColumnResize="true"></Resizing>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" TypeName="OutboundWIP"
                    SelectMethod="SearchLinesByJob">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
        </div>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
        SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <telerik:RadButton ID="ButtonSaveSettings" runat="server" Text="Save Layout Settings" OnClick="ButtonSaveSettings_Click" />
</asp:Content>
