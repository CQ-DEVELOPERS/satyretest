using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_SupplierReturn : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;

        theErrMethod = "Page Load";

        if (!Page.IsPostBack)
        {
            try
            {
                Session["ExternalCompanyId"] = null;

                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                string pageType = Request.QueryString["pageType"];

                if (pageType == "" || pageType == null)
                    pageType = "Edit";

                if (pageType == "Insert")
                {
                    Session["OutboundDocumentId"] = null;
                    Title = "Outbound Document Insert";
                    LabelHeading.Text = "Outbound Document Insert";
                    TabPanel1.HeaderText = "";
                    TabPanel2.HeaderText = "Insert";
                    TabPanel1.Visible = false;

                    DetailsViewOutboundDocument.ChangeMode(DetailsViewMode.Insert);
                }

                Master.MsgText = "Page load successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString()); 
                Master.MsgText = result; Master.ErrorText = result;
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";

        try
        {
            GridViewOutboundDocument.DataBind();

            Master.MsgText = "GridView Succussful"; 
            Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonSearch_Click"

    #region ButtonInsertLine_Click
    protected void ButtonInsertLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInsertLine_Click";

        try
        {
            if (Session["StorageUnitId"] == null || (int)Session["StorageUnitId"] == -1)
            {
                Master.ErrorText = "Please select a Product.";
                LabelErrorMsg.Text = "Please select a Product.";
                return;
            }

            if (TextBoxQuantity.Text == "" || TextBoxQuantity.Text == "0")
            {
                Master.ErrorText = "Please enter a Quantity.";
                LabelErrorMsg.Text = "Please enter a Quantity.";
                return;
            }

            Decimal quantity = Decimal.Parse(TextBoxQuantity.Text);

            if (Session["OutboundDocumentId"] == null)
            {
                Master.ErrorText = "Please select a Document.";
                LabelErrorMsg.Text = "Please select a Document.";
                return;
            }

            OutboundDocument outboundDocument = new OutboundDocument();

            if (Session["BatchId"] == null)
                Session["BatchId"] = -1;

            if (outboundDocument.CreateOutboundLine(Session["ConnectionStringName"].ToString(),
                                                int.Parse(Session["OperatorId"].ToString()),
                                                int.Parse(Session["OutboundDocumentId"].ToString()),
                                                int.Parse(Session["StorageUnitId"].ToString()),
                                                quantity,
                                                int.Parse(Session["BatchId"].ToString())))
            {
                TextBoxQuantity.Text = "";
                Session["StorageUnitId"] = null;
                Session["BatchId"] = null;
                Master.ErrorText = "";
                LabelErrorMsg.Text = "";

                GridViewOutboundLine.DataBind();

                Master.MsgText = "InsertLine";
                Master.ErrorText = "";
            }
            else
            {
                Master.ErrorText = "May not enter duplicate Product /  Batch Combination";
                LabelErrorMsg.Text = "May not enter duplicate Product /  Batch Combination";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "ButtonInsertLine_Click"

    #region ButtonDeleteLine_Click
    protected void ButtonDeleteLine_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeleteLine_Click";
        try
        {
            if (GridViewOutboundLine.SelectedIndex != -1)
                GridViewOutboundLine.DeleteRow(GridViewOutboundLine.SelectedIndex);

            Master.MsgText = "Deleted"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonDeleteLine_Click"

    #region GridViewOutboundDocument_OnSelectedIndexChanged
    protected void GridViewOutboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewOutboundDocument_OnSelectedIndexChanged";
        try
        {
            if (GridViewOutboundDocument.SelectedIndex != -1)
            {
                Session["OutboundDocumentId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["OutboundDocumentId"].ToString());
                Session["ExternalCompanyId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["ExternalCompanyId"].ToString());
            }

            DetailsViewOutboundDocument.DataBind();

            DetailsViewOutboundDocument.ChangeMode(DetailsViewMode.ReadOnly);

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion "GridViewOutboundDocument_OnSelectedIndexChanged"

    #region DetailsViewOutboundDocument_OnModeChanging
    protected void DetailsViewOutboundDocument_OnModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        theErrMethod = "DetailsViewOutboundDocument_OnModeChanging";
        try
        {
            if (e.NewMode == DetailsViewMode.Insert)
                Session["ExternalCompanyId"] = null;

            Master.MsgText = "Document Changed"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion DetailsViewOutboundDocument_OnModeChanging

    #region DetailsViewOutboundDocument_OnItemInserting
    protected void DetailsViewOutboundDocument_OnItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        theErrMethod = "DetailsViewOutboundDocument_OnItemInserting";
        try
        {
            if (Session["ExternalCompanyId"] == null || (int)Session["ExternalCompanyId"] == -1)
            {
                Master.ErrorText = "Please select a Supplier first.";
                LabelErrorTab2.Text = "Please select a Supplier first";
                e.Cancel = true;
            }
            else
                LabelErrorTab2.Text = "";

            Master.MsgText = "Item Insert"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.MsgText = result; Master.ErrorText = result;
        }
    }
    #endregion #region DetailsViewOutboundDocument_OnItemInserting
    
    #region ObjectDataSourceOutboundDocumentUpdate_OnInserted
    protected void ObjectDataSourceOutboundDocumentUpdate_OnInserted(object source, ObjectDataSourceStatusEventArgs e)
    {
        theErrMethod = "ObjectDataSourceOutboundDocumentUpdate_OnInserted";
        try
        {
            if (e.ReturnValue.ToString() != "0")
            {
                Session["OutboundDocumentId"] = e.ReturnValue;
                DetailsViewOutboundDocument.ChangeMode(DetailsViewMode.ReadOnly);
                DetailsViewOutboundDocument.DataBind();
            }
            else
            {
                ButtonChangeMode.Visible = true;
                LabelErrorTab2.Text = "Duplicate Order Number and Supplier Combination.";
            }
            Master.MsgText = "Updated"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ObjectDataSourceOutboundDocumentUpdate_OnInserted"

    #region ButtonChangeMode_Click
    protected void ButtonChangeMode_Click(object sender, EventArgs e)
    {
        DetailsViewOutboundDocument.ChangeMode(DetailsViewMode.Insert);
        ButtonChangeMode.Visible = false;
        LabelErrorTab2.Text = "";
    }
    #endregion ButtonChangeMode_Click

    #region ButtonComplete_Click
    protected void ButtonComplete_Click(object sender, EventArgs e)
    {
        try
        {
            OutboundDocument od = new OutboundDocument();

            if (od.SupplierReturnComplete(Session["ConnectionStringName"].ToString(), (int)Session["OutboundDocumentId"], (int)Session["OperatorId"]))
            {
                Master.MsgText = "Supplier Return Complete"; Master.ErrorText = "";
            }
            else
            {
                Master.MsgText = ""; Master.ErrorText = "Supplier Return Failed";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_OutboundDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonComplete_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "OutboundDocument", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;
                result = SendErrorNow("Outbound_OutboundDocument" + "_" + exMsg.Message.ToString());
                Master.ErrorText = result;
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return result;
            }
            else
            {
                //LiteralMsg.Text = "WARNING";  
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
