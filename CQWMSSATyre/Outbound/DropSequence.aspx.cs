using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_DropSequence : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";
        try
        {
            if (!Page.IsPostBack)
            {
                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("DropSequence" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page load

    #region GridViewJobs_OnSelectedIndexChanged
    protected void GridViewJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewJobs_OnSelectedIndexChanged"; 
        try
        {
            if (GridViewJobs.SelectedDataKey["JobId"].ToString() == "")
                Session["JobId"] = -1;
            else
                Session["JobId"] = int.Parse(GridViewJobs.SelectedDataKey["JobId"].ToString());

            GridViewJobs.DataBind();


            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("DropSequence" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "GridViewJobs_OnSelectedIndexChanged"

    #region ButtonReturn_Click
    protected void ButtonReturn_Click(object sender, EventArgs e)
    {
        if (Session["FromURL"] != null)
        {
            string fromURL = Session["FromURL"].ToString();

            Session.Remove("FromURL");

            Response.Redirect(fromURL);
        }
    }
    #endregion ButtonReturn_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
    
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();
                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "DropSequence", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "DropSequence", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
