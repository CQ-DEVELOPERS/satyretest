<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="Routing.aspx.cs" Inherits="Outbound_Routing" Title="<%$ Resources:Default, OutboundPlanningTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, OutboundPlanningTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, OutboundPlanningAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, OrdersAvailable%>">
            <ContentTemplate>
                <table>
                    <tr valign="top">
                        <td valign="bottom">
                            <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                            <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, Search%>" />
                        </td>
                    </tr>
                </table>
                <asp:Button ID="ButtonSelectAvailable" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                    OnClick="ButtonSelectAvailable_Click" />
                <asp:Button ID="ButtonAddToRoute" runat="server" Text="<%$ Resources:Default, AddtoRoute%>"
                    OnClick="ButtonAddToRoute_Click" />
                <asp:Label ID="LabelNotPicked" runat="server" Text="<%$ Resources:Default, NotPicked%>"></asp:Label>
                <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                <asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, Overdue%>"></asp:Label>
                <asp:Image ID="ImageOrange" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                <asp:Label ID="LabelOrange" runat="server" Text="<%$ Resources:Default, Requiredin2hours%>"></asp:Label>
                <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                <asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, Requiredin24hours%>"></asp:Label>
                <asp:Image ID="ImageStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                <asp:Label ID="LabelStandard" runat="server" Text="<%$ Resources:Default, RequiredGT24hours%>"></asp:Label>
                <br />
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelAvailable">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewAvailable" runat="server" DataSourceID="ObjectDataSourceAvailable"
                            AllowPaging="True" PageSize="15" AllowSorting="True" DataKeyNames="IssueId,OutboundShipmentId"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewAvailable_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Route%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField ShowEditButton="True" ShowSelectButton="True"></asp:CommandField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Due%>">
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                    SortExpression="OutboundShipmentId"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                    SortExpression="CustomerCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>"
                                    SortExpression="Customer"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                    SortExpression="NumberOfLines"></asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                    SortExpression="DeliveryDate"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>"
                                    SortExpression="OutboundDocumentType"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Location %>" SortExpression="Location">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>">
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListRoute" runat="server" DataSourceID="ObjectDataSourceRouting"
                                            DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoute" runat="server" Text='<%# Bind("Route") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                    SortExpression="CreateDate"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Remarks %>" SortExpression="Remarks">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelectAvailable" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonAddToRoute" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel runat="server" ID="UpdatePanelAvailableLines">
                    <ContentTemplate>
                        <asp:Image ID="ImageRed2" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                        <asp:Label ID="LabelRed2" runat="server" Text="<%$ Resources:Default, NoStock%>"></asp:Label>
                        <asp:Image ID="ImageYellow2" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                        <asp:Label ID="LabelYellow2" runat="server" Text="<%$ Resources:Default, StockShort%>"></asp:Label>
                        <asp:Image ID="ImageGreen2" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                        <asp:Label ID="LabelGreen2" runat="server" Text="<%$ Resources:Default, StockAvailable%>"></asp:Label>
                        <asp:GridView ID="GridViewAvailableLines" runat="server" DataSourceID="ObjectDataSourceAvailableLines"
                            DataKeyNames="IssueLineId" AutoGenerateColumns="False" AllowPaging="True" PageSize="15"
                            AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber" />
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product" />
                                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch" />
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode" />
                                <asp:BoundField DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>"
                                    SortExpression="OrderQuantity" />
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status" />
                                <asp:BoundField DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>"
                                    SortExpression="AvailablePercentage" />
                                <asp:BoundField DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>"
                                    SortExpression="AvailableQuantity" />
                                <asp:BoundField DataField="LocationsAllocated" HeaderText="<%$ Resources:Default, LocationsAllocated %>"
                                    SortExpression="LocationsAllocated" />
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridViewAvailable" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceAvailable" runat="server" TypeName="Routing"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:QueryStringParameter Name="outboundShipmentId" QueryStringField="OutboundShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="outboundShipmentId" Type="Int32" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                        <asp:Parameter Name="OrderNumber" Type="String" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                        <asp:Parameter Name="RouteId" Type="Int32" />
                        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
                        <asp:Parameter Name="Remarks" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceAvailableLines" runat="server" TypeName="Routing"
                    SelectMethod="GetOrderLines">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:ControlParameter Name="IssueId" ControlID="GridViewAvailable" PropertyName="SelectedValue"
                            DefaultValue="-1" />
                        <asp:ControlParameter Name="OutboundShipmentId" ControlID="GridViewAvailable" PropertyName="SelectedValue"
                            DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="<%$ Resources:Default, Notsent%>">
            <ContentTemplate>
                <asp:Button ID="ButtonSelectPending" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                    OnClick="ButtonSelectPending_Click" />
                <asp:Button ID="ButtonRemoveFromRoute" runat="server" Text="<%$ Resources:Default, Remove%>"
                    OnClick="ButtonRemoveFromRoute_Click" />
                <asp:Button ID="ButtonSend" runat="server" Text="<%$ Resources:Default, SendFlo%>"
                    OnClick="ButtonSend_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="cbeButtonSend" runat="server" TargetControlID="ButtonSend"
                    ConfirmText="<%$ Resources:Default, PressOKSendOrders%>">
                </ajaxToolkit:ConfirmButtonExtender>
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Default, NotPicked%>"></asp:Label>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Default, Overdue%>"></asp:Label>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Default, Requiredin2hours%>"></asp:Label>
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Default, Requiredin24hours%>"></asp:Label>
                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Default, RequiredGT24hours%>"></asp:Label>
                <br />
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelPending">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewPending" runat="server" DataSourceID="ObjectDataSourcePending"
                            AllowPaging="True" PageSize="15" AllowSorting="True" DataKeyNames="IssueId,OutboundShipmentId"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewPending_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Route%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField ShowEditButton="True" ShowSelectButton="True"></asp:CommandField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Due%>">
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                    SortExpression="OutboundShipmentId"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                    SortExpression="CustomerCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>"
                                    SortExpression="Customer"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                    SortExpression="NumberOfLines"></asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                    SortExpression="DeliveryDate"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>"
                                    SortExpression="OutboundDocumentType"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Location %>" SortExpression="Location">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>">
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListRoute" runat="server" DataSourceID="ObjectDataSourceRouting"
                                            DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoute" runat="server" Text='<%# Bind("Route") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                    SortExpression="CreateDate"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Remarks %>" SortExpression="Remarks">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelectPending" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonRemoveFromRoute" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSend" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel runat="server" ID="UpdatePanelPendingLines">
                    <ContentTemplate>
                        <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                        <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Default, NoStock%>"></asp:Label>
                        <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                        <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Default, StockShort%>"></asp:Label>
                        <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                        <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Default, StockAvailable%>"></asp:Label>
                        <asp:GridView ID="GridViewPendingLines" runat="server" DataSourceID="ObjectDataSourcePendingLines"
                            DataKeyNames="IssueLineId" AutoGenerateColumns="False" AllowPaging="True" PageSize="15"
                            AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber" />
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product" />
                                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch" />
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode" />
                                <asp:BoundField DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>"
                                    SortExpression="OrderQuantity" />
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status" />
                                <asp:BoundField DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>"
                                    SortExpression="AvailablePercentage" />
                                <asp:BoundField DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>"
                                    SortExpression="AvailableQuantity" />
                                <asp:BoundField DataField="LocationsAllocated" HeaderText="<%$ Resources:Default, LocationsAllocated %>"
                                    SortExpression="LocationsAllocated" />
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelectPending" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonRemoveFromRoute" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonSend" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="GridViewPending" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourcePending" runat="server" TypeName="Routing"
                    SelectMethod="GetPendingOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="outboundShipmentId" Type="Int32" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                        <asp:Parameter Name="OrderNumber" Type="String" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                        <asp:Parameter Name="RouteId" Type="Int32" />
                        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
                        <asp:Parameter Name="Remarks" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePendingLines" runat="server" TypeName="Routing"
                    SelectMethod="GetOrderLines">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:ControlParameter Name="IssueId" ControlID="GridViewPending" PropertyName="SelectedValue"
                            DefaultValue="-1" />
                        <asp:ControlParameter Name="OutboundShipmentId" ControlID="GridViewPending" PropertyName="SelectedValue"
                            DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
        TypeName="Priority">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
        SelectMethod="GetLocationsByIssue">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceRouting" runat="server" SelectMethod="GetRoutes"
        TypeName="Route">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
