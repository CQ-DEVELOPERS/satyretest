<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="StretchWrap.aspx.cs" Inherits="Outbound_StretchWrap" Title="<%$ Resources:Default, StretchWrapTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StretchWrapTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StretchWrapAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:HiddenField ID="HiddenFieldProduct" runat="server" />
    <asp:HiddenField ID="HiddenFieldBatch" runat="server" />
    <asp:HiddenField ID="HiddenFieldQuantity" runat="server" />
    <asp:HiddenField ID="HiddenFieldWeight" runat="server" />
    <div style="text-align: center; width: 800px">
        <table>
            <tr>
                <td>
                    <asp:GridView ID="GridViewInstruction" runat="server" SkinID="StretchWrap" OnRowDataBound="GridViewInstruction_RowDataBound"
                        AutoGenerateColumns="false" DataKeyNames="InstructionId,JobId,StatusCode,OneProductPerPallet" DataSourceID="ObjectDataSourceInstruction">
                        <Columns>
                            <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="GridViewProducts" runat="server" AutoGenerateColumns="false" DataKeyNames="ProductCode,Product"
                        DataSourceID="ObjectDataSourceProducts">
                        <Columns>
                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>">
                                <ItemStyle Wrap="False"></ItemStyle>
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="View1" runat="server">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="ButtonEnter">
                                <telerik:RadTextBox ID="TextBoxBarcode" runat="server" EnableTheming="false" Width="200px"
                                    Font-Size="X-Large"></telerik:RadTextBox>
                                <br />
                                <%--<asp:Button ID="ButtonEnter" runat="server" Text="<%$ Resources:Default, ButtonEnter %>"  OnClick="ButtonEnter_Click" EnableTheming="false" Height="80px" Width="200px" Font-Size="X-Large" />--%>
                                <asp:Button ID="Button1" runat="server" Visible="false" />
                            </asp:Panel>
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                            <telerik:RadTextBox ID="TextBoxProduct" runat="server" Visible="false" OnTextChanged="TextBoxProduct_TextChanged"></telerik:RadTextBox>
                        </asp:View>
                        <asp:View ID="View3" runat="server">
                            <telerik:RadTextBox ID="TextBoxBatch" runat="server" Visible="false" OnTextChanged="TextBoxBatch_TextChanged"></telerik:RadTextBox>
                        </asp:View>
                        <asp:View ID="View4" runat="server">
                            <telerik:RadTextBox ID="TextBoxQuantity" runat="server" Visible="false" OnTextChanged="TextBoxQuantity_TextChanged"></telerik:RadTextBox>
                        </asp:View>
                        <asp:View ID="View5" runat="server">
                            <asp:Label ID="LabelWeight" runat="server" Text="<%$ Resources:Default, Weight %>"></asp:Label>
                            <telerik:RadTextBox ID="TextBoxWeight" runat="server" Visible="true" OnTextChanged="TextBoxWeight_TextChanged"></telerik:RadTextBox>
                            <br />
                        </asp:View>
                        <asp:View ID="View6" runat="server">
                            <asp:Button ID="ButtonAccept" runat="server" Text="<%$ Resources:Default, ButtonAccept %>"
                                OnClick="ButtonAccept_Click" EnableTheming="false" Height="80px" Width="220px"
                                Font-Size="X-Large" BackColor="GreenYellow" />
                            <asp:Button ID="ButtonReject" runat="server" Text="<%$ Resources:Default, ButtonReject %>"
                                OnClick="ButtonReject_Click" EnableTheming="false" Height="80px" Width="200px"
                                Font-Size="X-Large" BackColor="Red" Visible="false" />
                            <br />
                            <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, Label%>"
                                OnClick="ButtonPrint_Click" EnableTheming="false" Height="80px" Width="220px"
                                Font-Size="X-Large" BackColor="White" Visible="false" />
                            <asp:Button ID="ButtonPrintProduct" runat="server" Text="<%$ Resources:Default, LabelProduct%>"
                                OnClick="ButtonPrintProduct_Click" EnableTheming="false" Height="80px" Width="220px"
                                Font-Size="X-Large" BackColor="White" Visible="false" />
                            <asp:Button ID="ButtonPrintCheckSheet" runat="server" Text="<%$ Resources:Default, ButtonPrintCheckSheet %>"
                                OnClick="ButtonPrintCheckSheet_Click" EnableTheming="false" Height="80px" Width="220px"
                                Font-Size="X-Large" BackColor="White" Visible="false" />
                        </asp:View>
                        <asp:View ID="View7" runat="server">
                            <asp:RadioButtonList ID="RadioButtonListReason" runat="server" DataSourceID="ObjectDataSourceReason"
                                DataValueField="ReasonId" DataTextField="Reason" RepeatColumns="2" RepeatDirection="Horizontal"
                                RepeatLayout="Table">
                            </asp:RadioButtonList>
                            <asp:Button ID="ButtonRejectReason" runat="server" Text="<%$ Resources:Default, ButtonReject %>"
                                OnClick="ButtonRejectReason_Click" EnableTheming="false" Height="80px" Width="200px"
                                Font-Size="X-Large" BackColor="Red" />
                        </asp:View>
                    </asp:MultiView>
                    <asp:Button ID="ButtonEnter" runat="server" Text="<%$ Resources:Default, ButtonEnter %>"
                        OnClick="ButtonEnter_Click" EnableTheming="false" Height="80px" Width="200px"
                        Font-Size="X-Large" Visible="false" />
                    <asp:Button ID="ButtonReject2" runat="server" Text="<%$ Resources:Default, ButtonReject %>"
                        OnClick="ButtonReject_Click" EnableTheming="false" Height="80px" Width="200px"
                        Font-Size="X-Large" BackColor="Red" Visible="false" />
                </td>
            </tr>
        </table>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="StretchWrap"
        SelectMethod="GetInstructions">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:ControlParameter ControlID="TextBoxBarcode" Name="barcode" ConvertEmptyStringToNull="true"
                DefaultValue="-1" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceProducts" runat="server" TypeName="StretchWrap"
        SelectMethod="GetProducts">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:ControlParameter ControlID="TextBoxBarcode" Name="barcode" ConvertEmptyStringToNull="true"
                DefaultValue="-1" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceReason" runat="server" TypeName="Reason"
        SelectMethod="GetReasonsByType">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:Parameter Name="ReasonCode" Type="String" DefaultValue="RW" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
