<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="AutoAllocatePickSlip.aspx.cs" Inherits="Outbound_AutoAllocatePickSlip" Title="<%$ Resources:Default, AutoAllocatePickSlipTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, AutoAllocatePickSlipTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, AutoAllocatePickSlipAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true" Width="880">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneScanOperator" runat="server">
                <Header>
                    <a href="" class="accordionLink">Scan Your Operator Id</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelScanOperator" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelScanOperator" runat="server" Text="<%$ Resources:Default, LabelScanOperator %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxOperator" runat="server" OnTextChanged="TextBoxOperator_TextChanged"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonPrint1" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint1_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select Operator from list</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelSelectOperator" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSelectOperator" runat="server" Text="<%$ Resources:Default, LabelSelectOperator %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator" DataValueField="OperatorId" DataTextField="Operator"></asp:DropDownList>
                                        <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server"
                                            TypeName="Operator" SelectMethod="GetOperators">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                                <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonPrint2" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint2_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <br />
    <br />
</asp:Content>

