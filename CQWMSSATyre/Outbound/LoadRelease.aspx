<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="LoadRelease.aspx.cs" Inherits="Outbound_LoadRelease"
    Title="<%$ Resources:Default, LoadReleaseTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, LoadReleaseTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, LoadReleaseAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <div style="float:left;">
                    <uc1:OutboundSearch id="OutboundSearch1" runat="server">
                    </uc1:OutboundSearch>
                </div>
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
                <div style="clear:left;"></div>
                <br />
                <asp:Button ID="ButtonRelease" runat="server" Text="<%$ Resources:Default, ButtonRelease %>" OnClick="ButtonRelease_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderRelease" runat="server" TargetControlID="ButtonRelease" ConfirmText="<%$ Resources:Default, PressOKtoconfirmrelease%>"></ajaxToolkit:ConfirmButtonExtender>
                <asp:Button ID="ButtonManual" runat="server" Text="<%$ Resources:Default, ButtonManual %>" OnClick="ButtonManual_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderManual" runat="server" TargetControlID="ButtonManual" ConfirmText="<%$ Resources:Default, PressOKManualRelease%>"></ajaxToolkit:ConfirmButtonExtender>
                <asp:UpdatePanel runat="server" ID="UpdatePanelOutboundDocumentSearch">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOutboundDocument" runat="server" 
                                      DataSourceID="ObjectDataSourceOutboundDocument"
                                      AutoGenerateColumns="False"
                                      AllowPaging="true"
                                      OnSelectedIndexChanged="GridViewOutboundDocument_OnSelectedIndexChanged"
                                      DataKeyNames="OutboundShipmentId,IssueId">
                            <Columns>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, DespatchLocation %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocument" 
                                              runat="server"
                                              TypeName="Planning"
                                              SelectMethod="SearchReleaseDocuments">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
                                <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                                <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                                <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                                <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonRelease" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonManual" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, Details%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewLines">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewLines" runat="server" 
                                      DataSourceID="ObjectDataSourceLines"
                                      AutoGenerateColumns="False" 
                                      AllowPaging="true"
                                      DataKeyNames="JobId,InstructionId">
                            <Columns>
                                <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default, Area %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceLines" 
                                              runat="server"
                                              TypeName="Planning"
                                              SelectMethod="SearchReleaseLines">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter SessionField="OutboundShipmentId" Type="Int32" Name="OutboundShipmentId"></asp:SessionParameter>
                                <asp:SessionParameter SessionField="IssueId" Type="Int32" Name="IssueId"></asp:SessionParameter>
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
            <%--            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>--%>
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
