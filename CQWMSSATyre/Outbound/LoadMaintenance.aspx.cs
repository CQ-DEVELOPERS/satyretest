using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_LoadMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"
    
    #region PRIVATE CONSTANTS
        //private const string DEFAULT_STATUS = "IS";
        StringBuilder strBuilder = new StringBuilder();
        private string result = "";
        private string theErrMethod = "";
    #endregion

    #region Page_Load
     protected void Page_Load(object sender, EventArgs e)
     {
         Session["countLoopsToPreventInfinLoop"] = 0;
         theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;

                if (Session["OperatorGroupCode"] != null)
                    if (Session["OperatorGroupCode"].ToString() != "A" && Session["OperatorGroupCode"].ToString() != "S")
                        ButtonClose.Enabled = false;
            }

            if(Session["WarehouseId"] == null)
                Response.Redirect("~/Security/Login.aspx");

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
     }
    #endregion Page_Load

    #region ButtonSearch_Click
    /// <summary>
     /// 
     /// </summary>
     /// <param name="sender"></param>
     /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
     {
         theErrMethod = "ButtonSearch_Click";
         try
         {
             if (rblShowOrders.SelectedValue != "true")
             {
                 GridViewShipment.Columns[2].Visible = false;
                 GridViewShipment.Columns[3].Visible = false;
                 GridViewShipment.Columns[4].Visible = false;
             }
             else
             {
                 GridViewShipment.Columns[2].Visible = true;
                 GridViewShipment.Columns[3].Visible = true;
                 GridViewShipment.Columns[4].Visible = true;
             }
             GridViewShipment.DataBind();

             Master.MsgText = ""; Master.ErrorText = "";
         }
         catch (Exception ex)
         {
             result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString()); 
             Master.ErrorText = result;
         }
     }
    #endregion ButtonSearch_Click

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewDetails.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if(cb.Checked)
                    rowList.Add(GridViewDetails.DataKeys[row.RowIndex].Values["InstructionId"]);
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetEditIndex:

    #region GridViewShipment_RowDataBound
    protected void GridViewShipment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // This line will get the reference to the underlying row
            DataRowView _row = (DataRowView)e.Row.DataItem;
            if (_row != null)
            {
                switch (_row.Row["Status"].ToString())
                {
                    case "Pause":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.OrangeRed;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                    case "Planning Complete":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.RoyalBlue;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                    case "Release":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.Green;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                    case "Manual":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.LightSteelBlue;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                    case "Checking":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.SaddleBrown;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                    case "Quality Assurance":
                        e.Row.Cells[11].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[11].Font.Bold = true;
                        break;
                }
            }
        }
    }
    #endregion GridViewShipment_RowDataBound

    #region GridViewShipment_SelectedIndexChanged
    protected void GridViewShipment_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewShipment_SelectedIndexChanged";
        try
        {
            Session["OutboundShipmentId"] = GridViewShipment.SelectedDataKey["OutboundShipmentId"];
            Session["IssueId"] = GridViewShipment.SelectedDataKey["IssueId"];

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion GridViewShipment_SelectedIndexChanged

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region GridViewJobs_OnSelectedIndexChanged
    protected void GridViewJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewJobs_OnSelectedIndexChanged";

        try
        {
            if (GridViewJobs.SelectedDataKey["JobId"].ToString() == "")
                Session["JobId"] = -1;
            else
                Session["JobId"] = int.Parse(GridViewJobs.SelectedDataKey["JobId"].ToString());

            GridViewJobs.DataBind();
            GridViewDetails.DataBind();

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewJobs_OnSelectedIndexChanged"

    #region ButtonLoad_Click
    protected void ButtonLoad_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonLoad_Click";
        try
        {
            CheckBox cb = new CheckBox();
            LoadMaintenance lm = new LoadMaintenance();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    lm.UpdateLoaded(Session["ConnectionStringName"].ToString(), (int)GridViewJobs.DataKeys[row.RowIndex].Values["JobId"], "Load");
            }

            GridViewJobs.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonLoad_Click"

    #region ButtonUnload_Click
    protected void ButtonUnload_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonUnload_Click";
        try
        {
            CheckBox cb = new CheckBox();
            LoadMaintenance lm = new LoadMaintenance();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    lm.UpdateLoaded(Session["ConnectionStringName"].ToString(), (int)GridViewJobs.DataKeys[row.RowIndex].Values["JobId"], "Unload");
            }

            GridViewJobs.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonUnload_Click"

    #region Print
    protected void Print()
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            Session["FromURL"] = "~/Outbound/LoadMaintenance.aspx";

            Session["ReportName"] = "Load Sheet";

            ReportParameter[] RptParameters = new ReportParameter[5];

            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            // Create the JobId report parameter
            string outboundShipmentId = Session["OutboundShipmentId"].ToString();
            RptParameters[3] = new ReportParameter("OutboundShipmentId", outboundShipmentId);

            // Create the JobId report parameter
            string issueId = Session["IssueId"].ToString();
            RptParameters[4] = new ReportParameter("IssueId", issueId);

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Print

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            Print();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonPrint_Click"

    #region ButtonInvoice_Click
    protected void ButtonInvoice_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonInvoice_Click";
        try
        {
            if (Session["OutboundShipmentId"] != null && Session["IssueId"] != null)
            {
                LoadMaintenance lm = new LoadMaintenance();

                lm.Invoice(Session["ConnectionStringName"].ToString(), (int)Session["OutboundShipmentId"], (int)Session["IssueId"]);

                GridViewShipment.DataBind();
                GridViewJobs.DataBind();
                Session["JobId"] = null;
                GridViewDetails.DataBind();

                Print();
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonInvoice_Click"

    #region ButtonClose_Click
    protected void ButtonClose_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonClose_Click";
        try
        {
            if (Session["OutboundShipmentId"] != null && Session["IssueId"] != null)
            {
                LoadMaintenance lm = new LoadMaintenance();

                lm.Close(Session["ConnectionStringName"].ToString(), (int)Session["OutboundShipmentId"], (int)Session["IssueId"]);

                GridViewShipment.DataBind();
                GridViewJobs.DataBind();
                Session["JobId"] = null;
                GridViewDetails.DataBind();

                Print();
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonClose_Click"

    #region ButtonCheckSheet_Click
    protected void ButtonCheckSheet_Click(object sender, EventArgs e)
    {
        try
        {
            theErrMethod = "ButtonCheckSheet_Click";

            Session["FromURL"] = "~/Outbound/LoadMaintenance.aspx";

            Session["ReportName"] = "Pick Check Sheet";

            ReportParameter[] RptParameters = new ReportParameter[5];

            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            // Create the InboundShipmentId report parameter
            string strOutboundShipmentId = GridViewShipment.SelectedDataKey["OutboundShipmentId"].ToString();
            RptParameters[3] = new ReportParameter("OutboundShipmentId", strOutboundShipmentId);

            // Create the ReceiptId report parameter
            string strIssueId = GridViewShipment.SelectedDataKey["IssueId"].ToString();
            RptParameters[4] = new ReportParameter("IssueId", strIssueId);

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonCheckSheet_Click"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "LoadMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "LoadMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
 #endregion ErrorHandling
}