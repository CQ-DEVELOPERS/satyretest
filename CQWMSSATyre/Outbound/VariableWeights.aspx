<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="VariableWeights.aspx.cs" Inherits="Outbound_VariableWeights" Title="<%$ Resources:Default, StretchWrapTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, StretchWrapTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, StretchWrapAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <div style="text-align: center; width: 800px">
        <asp:UpdatePanel ID="upProducts" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewInstruction" runat="server" SkinID="StretchWrap" AutoGenerateColumns="false"
                    DataKeyNames="InstructionId,JobId,StatusCode" DataSourceID="ObjectDataSourceInstruction">
                    <Columns>
                        <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="TareWeight" HeaderText="<%$ Resources:Default, TareWeight %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="NettWeight" HeaderText="<%$ Resources:Default, NettWeight %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="GrossWeight" HeaderText="<%$ Resources:Default, GrossWeight %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <br />
                <br />
                <asp:GridView ID="GridViewProducts" runat="server" AutoGenerateColumns="false" DataKeyNames="JobId,StorageUnitId"
                    DataSourceID="ObjectDataSourceProducts" OnRowUpdated="GridViewUpdated" OnRowEditing="GridView_RowEditing">
                    <Columns>
                        <asp:CommandField ShowEditButton="true" />
                        <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                            ReadOnly="true">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                            ReadOnly="true">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                            ReadOnly="true">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>"
                            ReadOnly="true">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="NettWeight" HeaderText="<%$ Resources:Default, NettWeight %>"
                            ReadOnly="true">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ConfirmedWeight" HeaderText="<%$ Resources:Default, GrossWeight %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="<%$ Resources:Default, Packaging %>" SortExpression="StorageUnitBatchId">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DropDownListPackaging" runat="server" DataSourceID="ObjectDataSourcePackaging"
                                    DataTextField="Packaging" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>'>
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="ObjectDataSourcePackaging" runat="server" TypeName="PalletWeight"
                                    SelectMethod="GetPackagingNull">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                            Type="String" />
                                        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" />
                                        <asp:SessionParameter Name="storageUnitId" SessionField="StorageUnitId" Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </EditItemTemplate>
                            <ItemStyle Wrap="False" />
                            <ItemTemplate>
                                <asp:Label ID="LabelStorageUnitBatchId" runat="server" Text='<%# Bind("Packaging") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="PackagingQuantity" HeaderText="<%$ Resources:Default, PackagingQuantity %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <br />
        <asp:Panel ID="Panel1" runat="server" DefaultButton="ButtonEnter">
            <asp:TextBox ID="TextBoxBarcode" runat="server" EnableTheming="false" Width="200px"
                Font-Size="X-Large"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="ButtonEnter" runat="server" Text="<%$ Resources:Default, ButtonEnter %>"
                OnClick="ButtonEnter_Click" EnableTheming="false" Height="80px" Width="200px"
                Font-Size="X-Large" />
            <asp:Button ID="Button1" runat="server" Visible="false" />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="VariableWeights"
        SelectMethod="GetInstructions">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:ControlParameter ControlID="TextBoxBarcode" Name="barcode" ConvertEmptyStringToNull="true"
                DefaultValue="-1" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceProducts" runat="server" TypeName="VariableWeights"
        SelectMethod="GetProducts" UpdateMethod="UpdateProducts">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:ControlParameter ControlID="TextBoxBarcode" Name="barcode" ConvertEmptyStringToNull="true"
                DefaultValue="-1" PropertyName="Text" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:Parameter Name="jobId" Type="Int32" />
            <asp:Parameter Name="StorageUnitId" Type="Int32" />
            <asp:Parameter Name="ConfirmedWeight" Type="Decimal" />
            <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
            <asp:Parameter Name="PackagingQuantity" Type="Decimal" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
        </UpdateParameters>
    </asp:ObjectDataSource>
</asp:Content>
