using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_StretchWrap : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    private int jobId = -1;
    private int instructionId = -1;
    private int reasonId = -1;
    private string statusCode = "";
    private string oneProductPerPallet = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (!Page.IsPostBack)
            {
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 64))
                {
                    ButtonPrint.Visible = true;
                    ButtonPrintProduct.Visible = true;
                }
                else
                {
                    ButtonPrint.Visible = true;
                    ButtonPrintProduct.Visible = true;

                }

                ButtonReject.Visible = false;
                ButtonEnter.Visible = true;

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

                //Form.Attributes.Add("onKeyPress", "myKeyHandler()");
                HiddenFieldProduct.Value = "false";
                HiddenFieldBatch.Value = "false";
                HiddenFieldQuantity.Value = "false";
                HiddenFieldWeight.Value = "false";
                
                TextBoxBarcode.Focus();

                if (Session["PrintLabel"] != null && Session["JobId"] != null)
                    if ((Boolean)Session["PrintLabel"])
                    {
                        oneProductPerPallet = "false";
                        
                        ArrayList checkedList = new ArrayList();

                        checkedList.Add((int)Session["JobId"]);

                        Session["checkedList"] = checkedList;

                        PrintLabel();
                    }
            }
            GridViewInstruction.DataBind();
            if (GridViewInstruction.Rows.Count > 0)
            {
                jobId = int.Parse(GridViewInstruction.DataKeys[0].Values["JobId"].ToString());
                instructionId = int.Parse(GridViewInstruction.DataKeys[0].Values["InstructionId"].ToString());
                statusCode = GridViewInstruction.DataKeys[0].Values["StatusCode"].ToString();
                oneProductPerPallet = GridViewInstruction.DataKeys[0].Values["OneProductPerPallet"].ToString();
                Session["OneProductPerPallet"] = GridViewInstruction.DataKeys[0].Values["OneProductPerPallet"].ToString().ToLower();
                Master.MsgText = oneProductPerPallet.ToLower();
                if (statusCode == "QA")
                {
                    ButtonAccept.Text = Resources.Default.ButtonPrintCheckSheet;
                    ButtonReject.Text = Resources.Default.ButtonPrintRejectLabel;
                    //Master.ErrorText = "Pallet in QA Status";
                    //TextBoxBarcode.Text = "";
                    //GridViewInstruction.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion Page_Load

    #region GridViewInstruction_RowDataBound
    protected void GridViewInstruction_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // This line will get the reference to the underlying row
            DataRowView _row = (DataRowView)e.Row.DataItem;
            if (_row != null)
            {
                switch (_row.Row["Status"].ToString())
                {
                    case "Checking":
                        e.Row.Cells[2].ForeColor = System.Drawing.Color.SaddleBrown;
                        e.Row.Cells[2].Font.Bold = true;
                        break;
                    case "Quality Assurance":
                        e.Row.Cells[2].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[2].Font.Bold = true;
                        break;
                }
            }
        }
    }
    #endregion GridViewInstruction_RowDataBound

    #region ResetPage
    protected void ResetPage()
    {
        HiddenFieldProduct.Value = "false";
        HiddenFieldBatch.Value = "false";
        HiddenFieldQuantity.Value = "false";
        HiddenFieldWeight.Value = "false";

        TextBoxBarcode.Text = "";
        MultiView1.ActiveViewIndex = 0;
        GridViewInstruction.Visible = false;
        GridViewProducts.Visible = false;
    }
    #endregion ResetPage

    #region ButtonEnter_Click
    protected void ButtonEnter_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEnter_Click";

        try
        {
            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"] , 63))
            {
                GridViewInstruction.Visible = false;
                GridViewProducts.Visible = false;
                GridViewInstruction.DataBind();
            }
            else
            {
                GridViewInstruction.Visible = true;
                GridViewProducts.Visible = true;
                GridViewInstruction.DataBind();
            }
            if (GridViewInstruction.Rows.Count > 0)
            {
                ButtonReject.Visible = true;
                CheckNext();

                statusCode = GridViewInstruction.DataKeys[0].Values["StatusCode"].ToString();

                if (statusCode != "CD" && statusCode != "D" && statusCode != "DC" && statusCode != "C")
                {
                    ButtonPrint.Visible = false;
                    ButtonPrintCheckSheet.Visible = false;
                    ButtonPrintProduct.Visible = false;
                }
                else
                {
                    ButtonPrint.Visible = true;
                    ButtonPrintCheckSheet.Visible = true;
                    ButtonPrintProduct.Visible = true;
                }
            }
            else
                Master.ErrorText = Resources.Messages.ErrorRetrieving;
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonEnter_Click

    #region ButtonAccept_Click
    protected void ButtonAccept_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAccept_Click";

        try
        {
            if (statusCode == "C" || statusCode == "DC" || statusCode == "QA")
            // Karen 2017-03-13  Automatically print the despatch label i.s.o. the check sheet
            //PrintAccept();
            {
                ArrayList checkedList = new ArrayList();

                checkedList.Add(jobId);

                Session["checkedList"] = checkedList;

                PrintLabel();
            }
            else
            {
                StretchWrap rec = new StretchWrap();

                if (rec.Accept(Session["ConnectionStringName"].ToString(), jobId, (int)Session["OperatorId"]))
                {
                    // Karen 2017-03-13  Automatically print the despatch label i.s.o. the check sheet
                    //PrintAccept();
                    ArrayList checkedList = new ArrayList();

                    checkedList.Add(jobId);

                    Session["checkedList"] = checkedList;

                    PrintLabel();
                    ResetPage();
                }
                else
                {
                    Master.ErrorText = Resources.Messages.ErrorAccepting;
                }
            }

            TextBoxBarcode.Focus();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAccept_Click

    #region ButtonReject_Click
    protected void ButtonReject_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReject_Click";

        try
        {
            GridViewInstruction.Visible = false;
            GridViewProducts.Visible = false;
            ButtonReject.Visible = false;
            MultiView1.ActiveViewIndex = 6;
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonReject_Click

    #region ButtonRejectReason_Click
    protected void ButtonRejectReason_Click(object sender, EventArgs e)
    {
        theErrMethod = "Reject";

        try
        {
            StretchWrap rec = new StretchWrap();

            reasonId = int.Parse(RadioButtonListReason.SelectedValue.ToString());

            if (rec.Reject(Session["ConnectionStringName"].ToString(), jobId, reasonId, (int)Session["OperatorId"]))
            {
                PrintReject();
                ResetPage();
            }
            else
            {
                Master.ErrorText = Resources.Messages.ErrorRejecting;
            }

            MultiView1.ActiveViewIndex = 0;

            TextBoxBarcode.Focus();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonRejectReason_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";

        try
        {
            ArrayList checkedList = new ArrayList();

            checkedList.Add(jobId);

            Session["checkedList"] = checkedList;

            PrintLabel();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPrint_Click

    #region ButtonPrintProduct_Click
    protected void ButtonPrintProduct_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintProduct_Click";

        try
        {
            ArrayList rowList = new ArrayList();

            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewProducts.Rows)
            {
                rowList.Add(GridViewProducts.DataKeys[row.RowIndex].Values["ProductCode"].ToString() + "," + GridViewProducts.DataKeys[row.RowIndex].Values["Product"].ToString());
            }
            
            if (rowList.Count > 0)
                Session["ProductList"] = rowList;

            ArrayList checkedList = new ArrayList();
            checkedList.Add(0);

            Session["checkedList"] = checkedList;

            Session["LabelName"] = "Product Label Small.lbl";

            Session["FromURL"] = "~/Common/ProductLabel.aspx";

            Exceptions ex = new Exceptions();
            jobId = int.Parse(GridViewInstruction.DataKeys[0].Values["JobId"].ToString());
            instructionId = int.Parse(GridViewInstruction.DataKeys[0].Values["InstructionId"].ToString());
            if (ex.CreateLabelAuditException(Session["ConnectionStringName"].ToString(), jobId, instructionId, (int)Session["OperatorId"], Session["LabelName"].ToString()) == false)
            {

            }

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PalletLabel " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPrintProduct_Click

    #region ButtonPrintCheckSheet_Click
    protected void ButtonPrintCheckSheet_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintCheckSheet_Click";

        try
        {
            ReportParameter[] RptParameters = new ReportParameter[4];

            Session["FromURL"] = "~/Outbound/StretchWrap.aspx";

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 295))
                Session["ReportName"] = "Packing Slip";
            else
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 361))
                        Session["ReportName"] = "Packing Slip";
                    else
                        Session["ReportName"] = "Job Check Sheet";                      

            // Create the JobId report parameter
            RptParameters[0] = new ReportParameter("JobId", jobId.ToString());

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Session["AutoPrint"] = true;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPrintCheckSheet_Click

    #region PrintAccept
    protected void PrintAccept()
    {
        theErrMethod = "PrintAccept";

        try
        {
            ArrayList checkedList = new ArrayList();
            ReportParameter[] RptParameters = new ReportParameter[4];

            checkedList.Add(jobId);

            Session["JobId"] = jobId;

            Session["checkedList"] = checkedList;

            switch (statusCode)
            {
                case "QA":
                    Session["FromURL"] = "~/Outbound/StretchWrap.aspx";

                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 361))
                        Session["ReportName"] = "Packing Slip";
                    else
                        Session["ReportName"] = "Job Check Sheet";

                    // Create the JobId report parameter
                    RptParameters[0] = new ReportParameter("JobId", jobId.ToString());

                    RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                    RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                    RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                    Session["ReportParameters"] = RptParameters;

                    Session["AutoPrint"] = true;

                    Response.Redirect("~/Reports/Report.aspx");

                    break;
                case "CK":
                    Session["FromURL"] = "~/Outbound/StretchWrap.aspx";

                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 361))
                        Session["ReportName"] = "Packing Slip";
                    else
                        Session["ReportName"] = "Job Check Sheet"; 

                    // Create the JobId report parameter
                    RptParameters[0] = new ReportParameter("JobId", jobId.ToString());

                    RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                    RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                    RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                    Session["ReportParameters"] = RptParameters;

                    Session["AutoPrint"] = true;

                    Response.Redirect("~/Reports/Report.aspx");

                    break;
                case "CD":
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 83))
                    {
                        Session["PrintLabel"] = true;
                    }

                    Session["FromURL"] = "~/Outbound/StretchWrap.aspx";

                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 361))
                        Session["ReportName"] = "Packing Slip";
                    else
                        Session["ReportName"] = "Job Check Sheet"; 

                    // Create the JobId report parameter
                    RptParameters[0] = new ReportParameter("JobId", jobId.ToString());

                    RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                    RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                    RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                    Session["ReportParameters"] = RptParameters;

                    Session["AutoPrint"] = true;

                    Response.Redirect("~/Reports/Report.aspx");

                    break;
                default:
                    PrintLabel();

                    break;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion PrintAccept

    #region PrintReject
    protected void PrintReject()
    {
        theErrMethod = "PrintReject";

        try
        {
            ArrayList checkedList = new ArrayList();
            checkedList.Add(jobId);

            Session["checkedList"] = checkedList;

            Session["LabelName"] = "Reject Label.lbl";

            Session["FromURL"] = "~/Outbound/StretchWrap.aspx";

            Exceptions ex = new Exceptions();
            jobId = int.Parse(GridViewInstruction.DataKeys[0].Values["JobId"].ToString());
            instructionId = int.Parse(GridViewInstruction.DataKeys[0].Values["InstructionId"].ToString());
            if (ex.CreateLabelAuditException(Session["ConnectionStringName"].ToString(), jobId, instructionId, (int)Session["OperatorId"], Session["LabelName"].ToString()) == false)
            {
   
            }

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion PrintReject

	#region PrintLabel

    protected void PrintLabel()
    {
        Session["PrintLabel"] = false;

        if (Session["OneProductPerPallet"] != null)
            oneProductPerPallet = Session["OneProductPerPallet"].ToString();

        if (oneProductPerPallet.ToLower() == "true")
        {
            Session["LabelName"] = "Builders Product Label Large.lbl";

            Session["PrintLabel"] = true;

            Session["OneProductPerPallet"] = false;
        }
        else
        {
            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 351))
                Session["LabelName"] = "Despatch By Route Label.lbl";
            else
                Session["LabelName"] = "Despatch By Order Label Plumblink.lbl";
                //Session["LabelName"] = "Despatch By Order Label.lbl";
        }

        Session["FromURL"] = "~/Outbound/StretchWrap.aspx";

        if (Session["Printer"] == null)
            Session["Printer"] = string.Empty;

        try
        {
            Exceptions ex = new Exceptions();

            if (GridViewInstruction.Rows.Count > 0)
            {
                DataKey dataKey = GridViewInstruction.DataKeys[0];

                if (dataKey != null)
                    if (dataKey.Values != null) jobId = int.Parse(dataKey.Values["JobId"].ToString());

                //DataKey key = GridViewInstruction.DataKeys[0];

                if (dataKey != null)
                    if (dataKey.Values != null) instructionId = int.Parse(dataKey.Values["InstructionId"].ToString());
            }
            else
            {
                jobId = (int)Session["JobId"];

                instructionId = -1;
            }

            ex.CreateLabelAuditException(Session["ConnectionStringName"].ToString(), jobId, instructionId,
                (int) Session["OperatorId"], Session["LabelName"].ToString());
        }
        catch (Exception)
        {
            // ignored
        }

        if (Session["Printer"].ToString() == string.Empty)
            Response.Redirect("~/Common/NLLabels.aspx");
        else
        {
            Session["Printing"] = true;

            Response.Redirect("~/Common/NLPrint.aspx");
        }
        
    }

    #endregion PrintLabel

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "StretchWrap", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "StretchWrap", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region CheckNext
    protected void CheckNext()
    {
        theErrMethod = "CheckNext";

        try
        {
            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 64))
            {
                if (HiddenFieldProduct.Value == "false")
                {
                    MultiView1.ActiveViewIndex = 1;
                    TextBoxProduct.Focus();
                    return;
                }
            }
            else
            {
                HiddenFieldProduct.Value = "true";
            }

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 65))
            {
                if (HiddenFieldBatch.Value == "false")
                {
                    MultiView1.ActiveViewIndex = 2;
                    TextBoxBatch.Focus();
                    return;
                }
            }
            else
            {
                HiddenFieldBatch.Value = "true";
            }

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 66))
            {
                if (HiddenFieldQuantity.Value == "false")
                {
                    MultiView1.ActiveViewIndex = 3;
                    TextBoxQuantity.Focus();
                    return;
                }
                //else
                //{
                //    MultiView1.ActiveViewIndex = 4;
                //}
            }
            else
            {
                HiddenFieldQuantity.Value = "true";
                MultiView1.ActiveViewIndex = 4;
            }

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 196))
            {
                if (HiddenFieldWeight.Value == "false")
                {
                    MultiView1.ActiveViewIndex = 4;
                    TextBoxWeight.Focus();
                }
                else
                {
                    MultiView1.ActiveViewIndex = 5;
                    ButtonEnter.Visible = false;
                }
            }
            else
            {
                HiddenFieldWeight.Value = "true";
                MultiView1.ActiveViewIndex = 5;
                ButtonEnter.Visible = false;
                ButtonReject2.Visible = false;
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion CheckNext

    #region TextBoxProduct_TextChanged
    protected void TextBoxProduct_TextChanged(object sender, EventArgs e)
    {
        theErrMethod = "TextBoxProduct_TextChanged";

        try
        {
            Transact tran = new Transact();
            if (tran.ConfirmProduct(Session["ConnectionStringName"].ToString(), instructionId, TextBoxProduct.Text) == 0)
            {
                Master.MsgText = Resources.Messages.ConfirmProductPassed;
                Master.ErrorText = "";
                HiddenFieldProduct.Value = "true";
                TextBoxProduct.Enabled = false;
                CheckNext();
            }
            else
            {
                Master.MsgText = "";
                Master.ErrorText = Resources.Messages.ConfirmProductFailed;
                HiddenFieldProduct.Value = "false";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion TextBoxProduct_TextChanged

    #region TextBoxBatch_TextChanged
    protected void TextBoxBatch_TextChanged(object sender, EventArgs e)
    {
        theErrMethod = "TextBoxBatch_TextChanged";

        try
        {
            Transact tran = new Transact();
            if (tran.ConfirmBatch(Session["ConnectionStringName"].ToString(), instructionId, TextBoxBatch.Text) == 0)
            {
                Master.MsgText = Resources.Messages.ConfirmBatchPassed;
                Master.ErrorText = "";
                HiddenFieldBatch.Value = "true";
                TextBoxBatch.Enabled = false;
                CheckNext();
            }
            else
            {
                Master.MsgText = "";
                Master.ErrorText = Resources.Messages.ConfirmBatchFailed;
                HiddenFieldBatch.Value = "false";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion TextBoxBatch_TextChanged

    #region TextBoxQuantity_TextChanged
    protected void TextBoxQuantity_TextChanged(object sender, EventArgs e)
    {
        theErrMethod = "TextBoxQuantity_TextChanged";

        try
        {
            Transact tran = new Transact();
            int quantity = 0;

            if (int.TryParse(TextBoxQuantity.Text, out quantity))
            {
                if (tran.ConfirmQuantity(Session["ConnectionStringName"].ToString(), instructionId, quantity) == 0)
                {
                    Master.MsgText = Resources.Messages.ConfirmQuantityPassed;
                    Master.ErrorText = "";
                    HiddenFieldQuantity.Value = "true";
                    TextBoxQuantity.Enabled = false;
                    CheckNext();
                }
                else
                {
                    Master.MsgText = "";
                    Master.ErrorText = Resources.Messages.ConfirmQuantityFailed;
                    HiddenFieldQuantity.Value = "false";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StretchWrap " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion TextBoxQuantity_TextChanged

    #region TextBoxWeight_TextChanged
    protected void TextBoxWeight_TextChanged(object sender, EventArgs e)
    {
        theErrMethod = "TextBoxWeight_TextChanged";

        try
        {
            Transact tran = new Transact();
            Decimal weight = 0;

            if (Decimal.TryParse(TextBoxWeight.Text, out weight))
            {
                if (tran.ConfirmPalletWeight(Session["ConnectionStringName"].ToString(), instructionId, weight) == 0)
                {
                    Master.MsgText = Resources.Messages.ConfirmWeightPassed;
                    Master.ErrorText = "";
                    HiddenFieldWeight.Value = "true";
                    TextBoxWeight.Enabled = false;
                    CheckNext();
                }
                else
                {
                    Master.MsgText = "";
                    Master.ErrorText = Resources.Messages.ConfirmWeightFailed;
                    HiddenFieldWeight.Value = "false";
                }
            }

            TextBoxWeight.Text = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceiveIntoWarehouse " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion TextBoxWeight_TextChanged
}
