using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;

public partial class Screens_ProductChecking : System.Web.UI.Page
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                //DwellTime dwell = new DwellTime();

                //if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                //{
                //    Session["ReturnURL"] = "~/Screens/ProductChecking.aspx";
                //    Response.Redirect("~/Screens/DwellTime.aspx");
                //}
                Master.MsgText = Resources.ResMessages.Successful;
                TextBoxBarcode.Text = "";
                TextBoxBatch.Text = "";
                TextBoxQuantity.Text = "";
                Session["Mismatch"] = null;
                Session["ReferenceNumber"] = null;

                ScriptManager.GetCurrent(this.Page).SetFocus(TextBoxReferenceNumber);

                if (Request.QueryString["JobId"] != null)
                {
                    ArrayList checkedList = new ArrayList();
                    int jobId = -1;

                    if (int.TryParse(Request.QueryString["JobId"], out jobId))
                    {
                        Session["JobId"] = jobId;
                        //PrintLabel();

                        //checkedList.Add(jobId);

                        //Session["checkedList"] = checkedList;

                        //Session["PrintLabel"] = false;

                        //Session["LabelName"] = "Despatch By Route Label.lbl";

                        //Session["FromURL"] = "~/Outbound/ProductChecking.aspx";

                        //if (Session["Printer"] == null)
                        //    Session["Printer"] = "";

                        //if (Session["Printer"].ToString() == "")
                        //    Response.Redirect("~/Common/NLLabels.aspx");
                        //else
                        //{
                        //    Session["Printing"] = true;
                        //    Response.Redirect("~/Common/NLPrint.aspx");
                        //}
                    }
                }

                RadButtonNewBox.Enabled = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 292);

                if (Session["ReferenceNumber"] != null)
                {
                    TextBoxReferenceNumber.Text = Session["ReferenceNumber"].ToString();
                    RadButtonAccept_Click(null, null);
                }
                else
                    Session["JobId"] = null;

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                if (Session["ParameterReferenceNumber"] != null)
                {
                    TextBoxReferenceNumber.Text = Session["ParameterReferenceNumber"].ToString();
                    Session["ParameterReferenceNumber"] = null;
                }
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            GridViewProducts.Visible = true;

            switch (MultiView1.ActiveViewIndex)
            {
                case 0: // Scan Reference Number
                    SetFocus(TextBoxReferenceNumber);

                    RadButtonAccept.Visible = true;
                    RadButtonBack.Visible = false;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = false;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = false;
                    RadButtonClose.Visible = false;
                    break;
                case 1: // Scan Product Barcode
                    SetFocus(TextBoxBarcode);
                    RadButtonFinished.Visible = true;
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 309))
                    {
                        RadButtonChangeBox.Visible = true;
                    }

                    RadButtonAccept.Visible = true;
                    RadButtonBack.Visible = true;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = false;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = false;
                    RadButtonClose.Visible = true;
                    break;
                case 2: // Scan Batch
                    SetFocus(TextBoxBatch);

                    RadButtonAccept.Visible = true;
                    RadButtonBack.Visible = true;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = false;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = false;
                    RadButtonClose.Visible = false;
                    break;
                case 3: // Enter Quantity
                    DetailsViewBatch_DataBind();
                    SetFocus(TextBoxQuantity);

                    Session["ExternalCompanyId"] = int.Parse(DetailsViewJob.DataKey["ExternalCompanyId"].ToString());

                    if (!SerialNumber.IsTrackedStorageUnitId(Session["ConnectionStringName"].ToString(), (int)Session["StorageUnitId"], (int)Session["ExternalCompanyId"]))
                    {
                        SerialNumbers1.Visible = false;
                        Session["SerialNumber"] = false;
                    }
                    else
                    {
                        SerialNumbers1.Visible = true;
                        Session["KeyId"] = (int)Session["IssueLineId"];
                        Session["KeyType"] = "IssueLineId";
                        Session["KeyStorageUnitId"] = (int)Session["StorageUnitId"];
                    }

                    RadButtonAccept.Visible = true;
                    RadButtonBack.Visible = true;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = false;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = false;
                    RadButtonClose.Visible = false;
                    break;
                case 4: // Used to be Quantity
                    SetFocus(RadButtonNext);

                    RadButtonAccept.Visible = false;
                    RadButtonBack.Visible = false;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = false;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = false;
                    RadButtonClose.Visible = false;
                    break;
                case 5: // New Box
                    SetFocus(TextBoxNewReferenceNumber);

                    RadButtonAccept.Visible = true;
                    RadButtonBack.Visible = false;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = false;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = false;
                    RadButtonClose.Visible = false;
                    break;
                case 6: // Labels
                    SetFocus(TextBoxNewReferenceNumber);

                    GridViewProducts.Visible = false;

                    RadButtonAccept.Visible = false;
                    RadButtonBack.Visible = false;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = true;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = true;
                    RadButtonClose.Visible = false;
                    break;
                case 7: // Container Type
                    SetFocus(ddlContainerType);

                    RadButtonAccept.Visible = false;
                    RadButtonBack.Visible = false;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = false;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = false;
                    RadButtonClose.Visible = false;
                    break;
                case 8: // Weight
                    SetFocus(TextBoxWeight);

                    RadButtonCreateJobs.Enabled = true;
                    RadButtonAccept.Visible = true;
                    RadButtonBack.Visible = false;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = false;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = false;
                    RadButtonClose.Visible = false;
                    break;
                case 9: // Store Location
                    SetFocus(TextBoxStoreLocation);

                    RadButtonAccept.Visible = true;
                    RadButtonBack.Visible = true;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = false;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = false;
                    RadButtonClose.Visible = false;
                    break;
                case 10: // Store Location
                    RadButtonAccept.Visible = false;
                    RadButtonBack.Visible = false;
                    RadButtonNext.Visible = false;
                    RadButtonNewBox.Visible = false;
                    RadButtonChangeBox.Visible = false;
                    RadButtonFinished.Visible = true;
                    RadButtonClose.Visible = false;
                    break;
            }
        }
        catch { }
    }
    #endregion Page_LoadComplete

    #region RadButtonAccept_Click
    protected void RadButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            ProductCheck chk = new ProductCheck();
            Master.MsgText = Resources.ResMessages.Successful;

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    DetailsViewJob.DataBind();

                    if (DetailsViewJob.Rows.Count > 1)
                    {
                        Session["JobId"] = int.Parse(DetailsViewJob.DataKey["JobId"].ToString());

                        if (DetailsViewJob.DataKey["Finished"].ToString() == "1")
                        {
                            RadButtonFinished.Visible = true;
                        }

                        RadButtonNewBox.Visible = true;
                        RadButtonBack.Visible = true;
                        //System.Media.SystemSounds.Beep.Play();
                        System.Console.Beep();

                        //Product Checking - Desktop label print
                        //if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 342))
                        //{
                        //    PrintLabel();
                        //}

                        //Product Checking - Container Type
                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 309) && chk.GetContainerType(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"])) == 0)
                        {
                            MultiView1.ActiveViewIndex = 7;
                            return;
                        }

                        //Product Checking - Scan Store Loc
                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 365))
                        {
                            TextBoxStoreLocation.Text = chk.StoreLocationGet(Session["ConnectionStringName"].ToString(), (int)Session["JobId"]);
                            MultiView1.ActiveViewIndex = 9;
                            ScriptManager.GetCurrent(this.Page).SetFocus(TextBoxStoreLocation);
                            return;
                        }

                        MultiView1.ActiveViewIndex++;
                    }
                    else
                    {
                        //System.Media.SystemSounds.Exclamation.Play();
                        System.Console.Beep();
                        Master.MsgText = Resources.ResMessages.InvalidField;
                        TextBoxBarcode.Text = "";
                    }

                    break;
                case 1:
                    DetailsViewProduct.DataBind();

                    if (DetailsViewProduct.Rows.Count > 1)
                    {
                        Session["StorageUnitId"] = int.Parse(DetailsViewProduct.DataKey["StorageUnitId"].ToString());
						Session["IssueLineId"] = int.Parse(DetailsViewProduct.DataKey["IssueLineId"].ToString());
                        MultiView1.ActiveViewIndex++;
                        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 34)
                            || Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 169)
                            || !Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 338))
                        {
                            TextBoxBatch.Text = "Default";
                            DetailsViewBatch.DataBind();

                            if (DetailsViewBatch.Rows.Count > 1)
                            {
                                Session["StorageUnitBatchId"] = int.Parse(DetailsViewBatch.DataKey["StorageUnitBatchId"].ToString());

                                if (cbScanMode.Checked)
                                {
                                    //TextBoxQuantity.Text = "1";
                                    // ** Start
                                    //KS May 2012 - Get default quantity

                                    ProductCheck pr = new ProductCheck();

                                    Decimal defqty = 0;

                                    defqty = pr.DefaultQuantity(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], TextBoxBarcode.Text);

                                    TextBoxQuantity.Text = string.Concat(defqty);

                                    //int.Parse(defqty);
                                    // ** End

                                    DetailsViewQuantity.DataBind();

                                    if (DetailsViewQuantity.DataKey["StorageUnitBatchId"].ToString() == "-1")
                                    {
                                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                                    }
                                    TextBoxBarcode.Text = "";

                                    Next();
                                    MultiView1.ActiveViewIndex = 1;
                                    GridViewProducts.DataBind();
                                }
                                else
                                    MultiView1.ActiveViewIndex++;

                                // ** Start
                                //KS April 2012 - proceed to label print if already checked
                                ProductCheck pc = new ProductCheck();

                                int chkd = 0;

                                chkd = pc.GetLineStatus(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["JobId"], (int)Session["StorageUnitId"]);

                                if (chkd == 1)
                                {
                                    MultiView1.ActiveViewIndex = 6;
                                }
                                // ** End

                            }
                            else
                            {
                                Master.MsgText = Resources.ResMessages.InvalidBatch;
                                TextBoxBatch.Text = "";
                            }
                        }
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidProduct;
                        System.Media.SystemSounds.Exclamation.Play();
                        TextBoxBarcode.Text = "";

                    }

                    break;
                case 2:
                    DetailsViewBatch.DataBind();

                    if (DetailsViewBatch.Rows.Count > 1)
                    {
                        Session["StorageUnitBatchId"] = int.Parse(DetailsViewBatch.DataKey["StorageUnitBatchId"].ToString());
                        MultiView1.ActiveViewIndex++;
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidBatch;
                        TextBoxBatch.Text = "";
                    }
                    break;
                case 3:
                    DetailsViewQuantity.DataBind();

                    if (DetailsViewQuantity.DataKey["StorageUnitBatchId"].ToString() == "-1")
                    {
                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                        TextBoxQuantity.Text = "";

                        if (Session["Mismatch"] == null)
                            Session["Mismatch"] = true;
                        else
                        {
                            if (chk.ConfirmFinished(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["OperatorId"]) == 0)
                            {
                                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 398))
                                {
                                    MultiView1.ActiveViewIndex = 8;
                                    return;
                                }
                                else
                                {
                                    TextBoxReferenceNumber.Text = "";
                                    Reset();
                                    MultiView1.ActiveViewIndex = 0;
                                }
                            }
                            else
                            {
                                TextBoxReferenceNumber.Text = "";
                                Reset();
                                Master.MsgText = Resources.ResMessages.SkuCheckQuantityQA;
                                MultiView1.ActiveViewIndex = 0;
                            }
                            Session["JobId"] = null;
                            Session["ReferenceNumber"] = null;
                            GridViewProducts.DataBind();
                        }
                    }
                    else
                    {
                        NextOrFinish();
                        GridViewProducts.DataBind();
                        DetailsViewJob.DataBind();
                    }
                    break;
                case 4:
                    break;
                case 5:
                    //if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 398))
                    //{
                    //    chk.ConfirmWeight(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]), Convert.ToDecimal(TextBoxNBWeight.Text));
                    //}
                    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 366))
                    {
                        PrintLabel();
                    }

                    int jobId = 0;

                    jobId = chk.NewBox(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["OperatorId"], TextBoxNewReferenceNumber.Text);

                    if (jobId > 0)
                    {
                        TextBoxReferenceNumber.Text = TextBoxNewReferenceNumber.Text;
                        TextBoxNewReferenceNumber.Text = "";
                        LabelOldReferenceNumber2.Text = "";

                        MultiView1.ActiveViewIndex = 0;

                        DetailsViewJob.DataBind();

                        if (DetailsViewJob.Rows.Count > 1)
                        {
                            Session["JobId"] = int.Parse(DetailsViewJob.DataKey["JobId"].ToString());
                            RadButtonBack.Visible = true;
                            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 309) && chk.GetContainerType(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"])) == 0)
                            {
                                MultiView1.ActiveViewIndex = 7;
                            }
                            else
                            {
                                MultiView1.ActiveViewIndex++;
                            }
                            Session["ReferenceNumber"] = TextBoxReferenceNumber.Text;

                            System.Media.SystemSounds.Beep.Play();

                            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 342))
                            {
                                PrintLabel();
                            }
                        }
                        else
                        {
                            Master.MsgText = Resources.ResMessages.InvalidField;
                            System.Media.SystemSounds.Exclamation.Play();
                            TextBoxBarcode.Text = "";
                        }

                        break;
                    }
                    else
                    {
                        switch (jobId)
                        {
                            case -2:
                                Master.MsgText = Resources.ResMessages.RefInUse;
                                TextBoxNewReferenceNumber.Text = "";
                                break;
                            case -3:
                                Master.MsgText = Resources.ResMessages.NothingLeft;
                                TextBoxNewReferenceNumber.Text = "";
                                break;
                            default:
                                Master.MsgText = Resources.ResMessages.InvalidBarcode;
                                TextBoxNewReferenceNumber.Text = "";
                                break;
                        }
                    }
                    break;
                case 7:
                    //set containertype
                    chk.SetContainerType(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]), int.Parse(ddlContainerType.SelectedValue));
                    if (DetailsViewJob.DataKey["Finished"].ToString() == "1")
                    {
                        RadButtonFinished.Visible = true;
                    }

                    MultiView1.ActiveViewIndex = 1;
                    break;
                case 8:
                    // Confirm Weight
                    if (chk.ConfirmWeight(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]), Convert.ToDecimal(TextBoxWeight.Text)))
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        RadGridLinkedJobs.DataBind();
                        MultiView1.ActiveViewIndex = 6;
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidEmptyWeight;
                    }
                    break;
                case 9:
                    // Scan StoreLocation
                    if (chk.StoreLocation(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]), TextBoxStoreLocation.Text))
                    {
                        MultiView1.ActiveViewIndex = 1;
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                        Master.MsgText = Resources.ResMessages.StoreLocationError;
                    break;
            }
        }
        catch { }
    }
    #endregion RadButtonAccept_Click

    #region DetailsViewBatch_PageIndexChanged
    protected void DetailsViewBatch_PageIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DetailsViewBatch.DataBind();

            Session["StorageUnitBatchId"] = int.Parse(DetailsViewBatch.DataKey["StorageUnitBatchId"].ToString());
            //DetailsViewStock1.PageIndex = e.NewPageIndex;
            //DetailsViewStock2.PageIndex = e.NewPageIndex;
            //DetailsViewStock_DataBind();
        }
        catch { }
    }
    #endregion DetailsViewBatch_PageIndexChanged

    #region DetailsViewBatch_DataBind
    protected void DetailsViewBatch_DataBind()
    {
        DetailsViewBatch.DataBind();

        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 318))
            DetailsViewBatch.Rows[5].Visible = false;
    }
    #endregion DetailsViewBatch_DataBind

    #region MultiView1_ActiveViewChanged
    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
    }
    #endregion MultiView1_ActiveViewChanged

    #region RadButtonBack_Click
    protected void RadButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                break;
            case 1:
                RadButtonBack.Visible = false;
                MultiView1.ActiveViewIndex--;
                break;
            case 7:
                MultiView1.ActiveViewIndex = 1;
                break;
            case 9:
                MultiView1.ActiveViewIndex = 0;
                break;
            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }
    #endregion RadButtonBack_Click

    #region RadButtonNext_Click
    protected void RadButtonNext_Click(object sender, EventArgs e)
    {
        Next();
    }
    #endregion RadButtonNext_Click

    #region RadButtonFinished_Click
    protected void RadButtonFinished_Click(object sender, EventArgs e)
    {
        try
        {
            if (MultiView1.ActiveViewIndex == 10)
            {
                MultiView1.ActiveViewIndex = 0;

                Reset();
                Finish();
                TextBoxReferenceNumber.Text = "";
                Session["JobId"] = null;
                Session["ReferenceNumber"] = null;
                GridViewProducts.DataBind();
            }
            else
            {
                ProductCheck chk = new ProductCheck();

                if (chk.ConfirmFinished(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], (int)Session["OperatorId"]) == 0)
                {
                    //if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 398))
                    //{
                    //    if (MultiView1.ActiveViewIndex != 8)
                    //    {
                    //        MultiView1.ActiveViewIndex = 8;
                    //        RadButtonAccept.Visible = false;
                    //        return;
                    //    }
                    //    else
                    //    {
                    //        if (chk.ConfirmWeight(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]), Convert.ToDecimal(TextBoxWeight.Text)))
                    //        {
                    //            Master.MsgText = Resources.ResMessages.Successful;
                    //        }
                    //        else
                    //        {
                    //            Master.MsgText = Resources.ResMessages.InvalidEmptyWeight;
                    //            return;
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    MultiView1.ActiveViewIndex = 6;
                    //}

                    // Consolidation Slip
                    int issueId = chk.DespatchAdvice(Session["ConnectionStringName"].ToString(), (int)Session["JobId"]);

                    if (issueId > 0)
                        MultiView1.ActiveViewIndex = 10; // Consolidation Slip
                    else
                    {
                        MultiView1.ActiveViewIndex = 0; // Next Job
                        Reset();
                        Finish();
                    }
                }
                else
                {
                    Master.MsgText = Resources.ResMessages.SkuCheckQuantityQA;
                    MultiView1.ActiveViewIndex = 6;
                }

                //Product Checking - Desktop label print
                if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 342))
                {
                    PrintLabel();
                }
                //// Print Despatch Advice
                //int issueId = chk.DespatchAdvice(Session["ConnectionStringName"].ToString(), (int)Session["JobId"]);

                //if (issueId > 0)
                //{
                //    OpenWindow.Report("JobLabel.aspx", Session["JobId"].ToString());

                //    Session["FromURL"] = "~/Outbound/ProductChecking.aspx";

                //    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 333))
                //        Session["ReportName"] = "Despatch Advice Ctrl";
                //    else
                //        Session["ReportName"] = "Despatch Advice";

                //    ReportParameter[] RptParameters = new ReportParameter[6];

                //    RptParameters[0] = new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString());
                //    RptParameters[1] = new ReportParameter("OutboundShipmentId", "-1");
                //    RptParameters[2] = new ReportParameter("IssueId", issueId.ToString());
                //    RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());
                //    RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());
                //    RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                //    Session["ReportParameters"] = RptParameters;

                //    Response.Redirect("~/Reports/Report.aspx");
                //}




                //// Print Despatch Advice
                //int issueId = chk.DespatchAdvice(Session["ConnectionStringName"].ToString(), (int)Session["JobId"]);

                //if (issueId > 0)
                //{
                //    Session["FromURL"] = "~/Reports/Report.aspx";

                //    if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 333))
                //        Session["ReportName"] = "Despatch Advice Ctrl";
                //    else
                //        Session["ReportName"] = "Despatch Advice";

                //    ReportParameter[] RptParameters = new ReportParameter[6];

                //    RptParameters[0] = new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString());
                //    RptParameters[1] = new ReportParameter("OutboundShipmentId", "-1");
                //    RptParameters[2] = new ReportParameter("IssueId", issueId.ToString());
                //    RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());
                //    RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());
                //    RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                //    Session["ReportParameters"] = RptParameters;

                //    ArrayList checkedList = new ArrayList();

                //    checkedList.Add(Session["JobId"]);

                //    Session["checkedList"] = checkedList;

                //    Session["PrintLabel"] = false;

                //    Session["LabelName"] = "Despatch By Route Label.lbl";

                //    if (Session["Printer"] == null)
                //        Session["Printer"] = "";

                //    if (Session["Printer"].ToString() == "")
                //        Response.Redirect("~/Common/NLLabels.aspx");
                //    else
                //    {
                //        Session["Printing"] = true;
                //        Response.Redirect("~/Common/NLPrint.aspx");
                //    }
                //}
                //else
                //{
                //    Session["FromURL"] = "~/Outbound/ProductChecking.aspx";

                //    PrintLabel();
                //}

                //// Print Despatch Advice
                //int issueId = chk.DespatchAdvice(Session["ConnectionStringName"].ToString(), (int)Session["JobId"]);

                //if (issueId > 0)
                //{
                //    Session["FromURL"] = "~/Outbound/ProductChecking.aspx?JobId=" + Session["JobId"].ToString();

                //    Session["ReportName"] = "Consolidation Slip";

                //    ReportParameter[] RptParameters = new ReportParameter[5];

                //    RptParameters[0] = new ReportParameter("OutboundShipmentId", "-1");
                //    RptParameters[1] = new ReportParameter("IssueId", issueId.ToString());
                //    RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());
                //    RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());
                //    RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                //    Session["ReportParameters"] = RptParameters;

                //    Session["JobId"] = null;
                //    Session["ReferenceNumber"] = null;

                //    Response.Redirect("~/Reports/Report.aspx");
                //}


                //if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 293))
                //    Finish();
            }
        }
        catch { }
    }
    #endregion RadButtonFinished_Click

    #region RadButtonClose_Click
    protected void RadButtonClose_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 8;

        //if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 366))
        //{
        //    PrintLabel();
        //}
    }
    #endregion RadButtonClose_Click

    #region RadButtonNewBox_Click
    protected void RadButtonNewBox_Click(object sender, EventArgs e)
    {
        ProductCheck chk = new ProductCheck();

        LabelOldReferenceNumber2.Text = TextBoxReferenceNumber.Text;

        LabelVariables label = new LabelVariables();
        int value = label.SetReferenceNumber(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"]);

        if (value > 0)
            TextBoxNewReferenceNumber.Text = "NB:" + value.ToString();

        MultiView1.ActiveViewIndex = 5;

        //if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 366))
        //{
        //    PrintLabel();
        //}
    }
    #endregion RadButtonNewBox_Click

    #region NextOrFinish
    protected void NextOrFinish()
    {
        Reset();
        MultiView1.ActiveViewIndex = 1;
    }
    #endregion NextOrFinish

    #region Next
    protected void Next()
    {
        try
        {
            DetailsViewJob.DataBind();

            MultiView1.ActiveViewIndex++;
        }
        catch { }
    }
    #endregion Next

    #region Finish
    protected void Finish()
    {
        try
        {
            TextBoxReferenceNumber.Text = "";

            MultiView1.ActiveViewIndex = 0;

            Session["JobId"] = null;
            Session["ReferenceNumber"] = null;
            GridViewProducts.DataBind();
        }
        catch { }
    }
    #endregion Finish

    #region Reset
    protected void Reset()
    {
        Master.MsgText = Resources.ResMessages.Successful;
        TextBoxBarcode.Text = "";
        TextBoxBatch.Text = "";
        TextBoxQuantity.Text = "";
        TextBoxQuantity.Text = "";
        TextBoxWeight.Text = "";
        Session["Mismatch"] = null;
    }
    #endregion Reset

   #region RadButtonPrint_Click

    protected void RadButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["ParameterReferenceNumber"] = TextBoxReferenceNumber.Text;

            ArrayList checkedList = new ArrayList();

            foreach (GridDataItem item in RadGridLinkedJobs.Items)
            {
                if (item.Selected)
                    checkedList.Add((int)item.GetDataKeyValue("JobId"));
            }

            int copies;

            int.TryParse(TextBoxLabels.Text, out copies);

            if (copies > 0)
                Session["LabelCopies"] = copies;

            if (checkedList.Count == 0)
                return;

            Session["checkedList"] = checkedList;

            Session["PrintLabel"] = false;

            Session["LabelName"] = "Despatch By Order Label Plumblink.lbl";
            //Session["LabelName"] = "Despatch By Order Label.lbl";

            Session["FromURL"] = "~/Outbound/ProductChecking.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = string.Empty;

            if (Session["Printer"].ToString() == string.Empty)
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                //Response.Redirect("~/Common/NLPrint.aspx");
                Session["LabelClose"] = true;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newWindow",
                    "window.open('../Common/Label.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=200,height=200,top=100,left=100');",
                    true);
            }

            //PrintLabel();
            //Reset();
            //Finish();
        }
        catch (Exception exception)
        {
            System.Diagnostics.EventLog appLog = new System.Diagnostics.EventLog {Source = "Plumblink"};

            appLog.WriteEntry(exception.Message);

            //System.Diagnostics.Debug.WriteLine(ex.Message);
        }
        //catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.Message); }
    }

    #endregion RadButtonPrint_Click

    #region PrintLabel

    protected void PrintLabel()
    {
        try
        {
            // ReSharper disable once UseObjectOrCollectionInitializer
            ArrayList checkedList = new ArrayList();

            checkedList.Add(Session["JobId"]);

            Session["checkedList"] = checkedList;

            Session["PrintLabel"] = false;

            Session["LabelName"] = "Despatch By Order Label Plumblink.lbl";
            //Session["LabelName"] = "Despatch By Order Label.lbl";

            Session["FromURL"] = "~/Outbound/ProductChecking.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = string.Empty;

            if (Session["Printer"].ToString() == string.Empty)
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                //Response.Redirect("~/Common/NLPrint.aspx");
                Session["LabelClose"] = true;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newWindow",
                    "window.open('../Common/Label.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=200,height=200,top=100,left=100');",
                    true);
            }
        }
        catch (Exception)
        {
            // ignored
        }
    }

    #endregion PrintLabel

    #region RadButtonPackSlip_Click
    protected void RadButtonPackSlip_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["JobId"] != null)
            {
                Session["Blank"] = null; // Hide the menu on the report.aspx page

                //OpenWindow.Report("PackingSlip.aspx", Session["JobId"].ToString());

                Session["ReportName"] = "Packing Slip";

                ReportParameter[] RptParameters = new ReportParameter[4];

                // Create the JobId report parameter
                RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("JobId", Session["JobId"].ToString());

                RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                Session["ReportParameters"] = RptParameters;

                //Response.Redirect("~/Reports/Report.aspx");

                ScriptManager.RegisterStartupScript(this.Page,
                                                    this.Page.GetType(),
                                                    "newWindow",
                                                    "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                    true);
            }
        }
        catch { }
    }
    #endregion RadButtonPackSlip_Click

    #region RadGridLinkedJobs_ItemCommand
    protected void RadGridLinkedJobs_ItemCommand(object source, GridCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "PackingSlip")
            {

                foreach (GridDataItem item in RadGridLinkedJobs.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["FromURL"] = "~/Outbound/ProductChecking.aspx";
                        
                        Session["ReportName"] = "Packing Slip";

                        ReportParameter[] RptParameters = new ReportParameter[4];

                        // Create the JobId report parameter
                        RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("JobId", item.GetDataKeyValue("JobId").ToString());

                        RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                        RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                        RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                        Session["ReportParameters"] = RptParameters;

                        //Response.Redirect("~/Reports/Report.aspx");

                        ScriptManager.RegisterStartupScript(this.Page,
                                                            this.Page.GetType(),
                                                            "newWindow",
                                                            "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                            true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //result = SendErrorNow("Received" + "_" + ex.Message.ToString());
            //Master.ErrorText = result;
        }
    }
    #endregion RadGridLinkedJobs_ItemCommand

    #region RadButtonCreateJobs_Click
    protected void RadButtonCreateJobs_Click(object sender, EventArgs e)
    {
        try
        {
            ProductCheck chk = new ProductCheck();
            int quantity = 0;

            

            // Consolidation Slip
            if(int.TryParse(TextBoxCreateJobs.Text, out quantity))
                if(chk.CreateJobs(Session["ConnectionStringName"].ToString(), (int)Session["JobId"], quantity))
                {
                    RadButtonCreateJobs.Enabled = false;
                    RadGridLinkedJobs.DataBind();
                }
        }
        catch { }
    }
    #endregion RadButtonCreateJobs_Click

    #region RadButtonConsolidationSlip_Click
    protected void RadButtonConsolidationSlip_Click(object sender, EventArgs e)
    {
        try
        {
            ProductCheck chk = new ProductCheck();

            // Consolidation Slip
            int issueId = chk.DespatchAdvice(Session["ConnectionStringName"].ToString(), (int)Session["JobId"]);

            if (issueId > 0)
            {
                Session["Blank"] = null; // Hide the menu on the report.aspx page

                Session["FromURL"] = "~/Outbound/ProductChecking.aspx?JobId=" + Session["JobId"].ToString();

                Session["ReportName"] = "Consolidation Slip";

                ReportParameter[] RptParameters = new ReportParameter[5];

                RptParameters[0] = new ReportParameter("OutboundShipmentId", "-1");
                RptParameters[1] = new ReportParameter("IssueId", issueId.ToString());
                RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());
                RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());
                RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                Session["ReportParameters"] = RptParameters;

                Session["JobId"] = null;
                Session["ReferenceNumber"] = null;

                //Response.Redirect("~/Reports/Report.aspx");

                ScriptManager.RegisterStartupScript(this.Page,
                                                    this.Page.GetType(),
                                                    "newWindow",
                                                    "window.open('../Reports/Report.aspx','_blank','status=0,toolbar=0,menubar=0,location=0,scrollbars=1,resizable=1,width=1000,height=800,top=100,left=100');",
                                                    true);
            }
        }
        catch { }
    }
    #endregion RadButtonConsolidationSlip_Click

    #region RadButtonChangeBox_Click
    protected void RadButtonChangeBox_Click(object sender, EventArgs e)
    {
        ProductCheck chk = new ProductCheck();

        int containerTypeId = chk.GetContainerType(Session["ConnectionStringName"].ToString(), Convert.ToInt32(Session["JobId"]));
        if (containerTypeId != 0)
        {
            ddlContainerType.SelectedValue = containerTypeId.ToString();
        }

        MultiView1.ActiveViewIndex = 7;
    }
    #endregion RadButtonChangeBox_Click
}
