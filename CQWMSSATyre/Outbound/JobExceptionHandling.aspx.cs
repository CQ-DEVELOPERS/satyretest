using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_JobExceptionHandling : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";


        if (!Page.IsPostBack)
        {
            try
            {
                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("JobExceptionHandling" + "_" + ex.Message.ToString());
                Master.ErrorText = result;
            }

        }
    }
    #endregion "Page_Load"

    #region GridViewJobs_OnSelectedIndexChanged
    protected void GridViewJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewJobs_OnSelectedIndexChanged";

        try
        {
            if (GridViewJobs.SelectedDataKey["JobId"].ToString() == "")
                Session["JobId"] = -1;
            else
                Session["JobId"] = int.Parse(GridViewJobs.SelectedDataKey["JobId"].ToString());

            GridViewJobs.DataBind();
            GridViewMixedLines.DataBind();

            Master.MsgText = "View Job"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("JobExceptionHandling" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "GridViewJobs_OnSelectedIndexChanged"

    #region ButtonEdit_Click
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndexRowNumber();
            SetEditIndexRowNumber();

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("JobExceptionHandling" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonEdit_Click

    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";

        try
        {

            if (GridViewMixedLines.EditIndex != -1)
                GridViewMixedLines.UpdateRow(GridViewMixedLines.EditIndex, true);

            SetEditIndexRowNumber();

            Master.MsgText = "Save"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("JobExceptionHandling" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSave_Click

    #region GetEditIndexRowNumber
    protected void GetEditIndexRowNumber()
    {
        theErrMethod = "GetEditIndexRowNumber";

        try
        {
            if (Session["rowList"] == null)
            {
                int index = 0;
                CheckBox cb = new CheckBox();
                ArrayList rowList = new ArrayList();

                while (index < GridViewMixedLines.Rows.Count)
                {
                    GridViewRow checkedRow = GridViewMixedLines.Rows[index];

                    cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                    if (cb.Checked == true)
                        rowList.Add(index);

                    index++;
                }

                if (rowList.Count > 0)
                    Session["rowList"] = rowList;
            }

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("JobExceptionHandling" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetEditIndexRowNumber"

    #region SetEditIndexRowNumber
    protected void SetEditIndexRowNumber()
    {
        theErrMethod = "SetEditIndexRowNumber";

        try
        {
            ArrayList rowList = (ArrayList)Session["rowList"];

            if (rowList == null)
                Session.Remove("rowList");
            else
            {
                if (GridViewMixedLines.EditIndex != -1)
                    GridViewMixedLines.UpdateRow(GridViewMixedLines.EditIndex, true);

                if (rowList.Count < 1)
                {
                    Session.Remove("rowList");
                    GridViewMixedLines.EditIndex = -1;
                }
                else
                {
                    GridViewMixedLines.EditIndex = (int)rowList[0];

                    rowList.Remove(rowList[0]);
                }
            }

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("JobExceptionHandling" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "SetEditIndexRowNumber"

    #region ButtonCheckSheet_Click
    protected void ButtonCheckSheet_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCheckSheet_Click";

        try
        {
            Master.MsgText = "Check Sheet"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("JobExceptionHandling" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonCheckSheet_Click"

    #region ButtonDespatchLabel_Click
    protected void ButtonDespatchLabel_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDespatchLabel_Click";

        try
        {
            Master.MsgText = "Check Sheet"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("JobExceptionHandling" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonDespatchLabel_Click"
    
    #region ButtonPackSlip_Click
    protected void ButtonPackSlip_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPackSlip_Click";

        try
        {
            Master.MsgText = "Pack Slip"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("JobExceptionHandling" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonPackSlip_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewMixedLines.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region Login1_OnLoggedIn
    protected void Login1_OnLoggedIn(object sender, EventArgs e)
    {
        theErrMethod = "Login1_OnLoggedIn";

        try
        {
            Status st = new Status();

            if (st.Authorise(Session["ConnectionStringName"].ToString(), (int)Session["JobId"]))
            {
                Master.MsgText = "Check Sheet"; Master.ErrorText = "Status update successful";
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                Master.MsgText = "Check Sheet"; Master.ErrorText = "Status update failed";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("JobExceptionHandling" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Login1_OnLoggedIn

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "JobExceptionHandling", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "JobExceptionHandling", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
