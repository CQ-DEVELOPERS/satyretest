<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="ProductChecking.aspx.cs" Inherits="Screens_ProductChecking" Title="Product Checking" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../UserControls/SerialNumbers.ascx" TagName="SerialNumbers" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<%--<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </ajaxToolkit:ToolkitScriptManager>--%>
<%--    <script type="text/javascript" language="JavaScript">
        function openNewWindowReport()
        {
           window.open("../Reports/PackingSlip.aspx", "_blank",
              "height=600px width=450px top=200 left=200 resizable=no scrollbars=no ");
        }
    </script>--%>
    <telerik:RadFormDecorator ID="FormDecorator1" runat="server" DecoratedControls="All" Skin="Metro">
    </telerik:RadFormDecorator>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="MultiView1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadButtonCreateJobs" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinkedJobs" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonPackSlip" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonAccept" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonBack" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNext" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNewBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonChangeBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonFinished" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonPrint" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonClose" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonAccept" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinkedJobs" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonAccept" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonBack" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNext" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNewBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonChangeBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonFinished" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonPrint" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonClose" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonBack" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonAccept" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonBack" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNext" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNewBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonChangeBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonFinished" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonPrint" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonClose" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonNext" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonAccept" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonBack" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNext" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNewBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonChangeBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonFinished" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonPrint" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonClose" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonNewBox" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonAccept" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonBack" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNext" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNewBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonChangeBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonFinished" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonPrint" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonClose" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonChangeBox" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonAccept" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonBack" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNext" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNewBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonChangeBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonFinished" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonPrint" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonClose" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonFinished" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonAccept" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonBack" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNext" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNewBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonChangeBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonFinished" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonPrint" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonClose" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonPrint" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonAccept" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonBack" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNext" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNewBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonChangeBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonFinished" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonPrint" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonClose" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadButtonClose" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MultiView1" />
                    <telerik:AjaxUpdatedControl ControlID="GridViewProducts" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonAccept" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonBack" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNext" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonNewBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonChangeBox" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonFinished" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonPrint" />
                    <telerik:AjaxUpdatedControl ControlID="RadButtonClose" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View0" runat="server">
            <br />
            <asp:Label ID="LabelReferenceNumber" runat="server" Text="<%$ Resources:Default, ReferenceNumber %>"></asp:Label>
            <telerik:RadTextBox ID="TextBoxReferenceNumber" runat="server"></telerik:RadTextBox>
        </asp:View>
        <asp:View ID="View1" runat="server">
            <asp:DetailsView ID="DetailsViewJob" runat="server" DataKeyNames="JobId,Finished,ExternalCompanyId" DataSourceID="ObjectDataSourceJob" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" />
                    <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" />
                    <asp:BoundField DataField="Lines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, OrderQuantity %>" />
                    <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" />
                    <asp:BoundField DataField="CheckQuantity" HeaderText="<%$ Resources:Default, CheckQuantity %>" />
                </Fields>
            </asp:DetailsView>
            <br />
            <asp:CheckBox ID="cbScanMode" runat="server" Text="<%$ Resources:Default, ScanMode%>" Checked="false"/>
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
            <telerik:RadTextBox ID="TextBoxBarcode" runat="server"></telerik:RadTextBox>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:DetailsView ID="DetailsViewProduct" runat="server" DataKeyNames="StorageUnitId, IssueLineId" DataSourceID="ObjectDataSourceProduct" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="Lines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="ErrorMsg" HeaderText="" ItemStyle-ForeColor="Red" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="ProductCheck"
                SelectMethod="ConfirmProduct">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="JobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
                    <asp:ControlParameter Name="Barcode" ControlID="TextBoxBarcode" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <asp:Label ID="LabelBatch" runat="server" Text="<%$ Resources:Default, Batch %>"></asp:Label>
            <telerik:RadTextBox ID="TextBoxBatch" runat="server"></telerik:RadTextBox>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <asp:DetailsView ID="DetailsViewBatch" runat="server" DataKeyNames="StorageUnitBatchId" DataSourceID="ObjectDataSourceBatch" AutoGenerateRows="False" PageIndex="0" AllowPaging="true" OnPageIndexChanged="DetailsViewBatch_PageIndexChanged">
                <Fields>
                    <asp:BoundField DataField="Lines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField DataField="ExpiryDate" HeaderText="<%$ Resources:Default, ExpiryDate %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                    <asp:BoundField DataField="ErrorMsg" HeaderText="" ItemStyle-ForeColor="Red" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="ProductCheck"
                SelectMethod="ConfirmBatch">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="JobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="StorageUnitId" SessionField="StorageUnitId" Type="Int32" DefaultValue="-1" />
                    <asp:ControlParameter Name="Batch" ControlID="TextBoxBatch" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <uc1:SerialNumbers ID="SerialNumbers1" runat="server" />
            <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
            <telerik:RadTextBox ID="TextBoxQuantity" runat="server" Font-Size="Medium"></telerik:RadTextBox>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <asp:DetailsView ID="DetailsViewQuantity" runat="server" DataKeyNames="StorageUnitBatchId" DataSourceID="ObjectDataSourceQuantity" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="Lines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField DataField="ExpiryDate" HeaderText="<%$ Resources:Default, ExpiryDate %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="ObjectDataSourceQuantity" runat="server" TypeName="ProductCheck"
                SelectMethod="ConfirmQuantity">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="JobId" SessionField="JobId" Type="Int32" DefaultValue="-1" />
                    <asp:SessionParameter Name="StorageUnitBatchId" SessionField="StorageUnitBatchId" Type="Int32" DefaultValue="-1" />
                    <asp:ControlParameter Name="quantity" ControlID="TextBoxQuantity" Type="String" />
                    <asp:ControlParameter Name="scanMode" ControlID="cbScanMode" Type="Boolean" />
                    <asp:ControlParameter Name="Barcode" ControlID="TextBoxBarcode" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <%--<asp:Label ID="Label1" runat="server" Text="Quantity:"></asp:Label>
            <telerik:RadTextBox ID="TextBox1" runat="server"></telerik:RadTextBox>--%>
        </asp:View>
        <asp:View ID="View5" runat="server">
            <br />
            <asp:Label ID="LabelOldReferenceNumber1" runat="server" Text="Old Number:"></asp:Label>
            <asp:Label ID="LabelOldReferenceNumber2" runat="server" Text=""></asp:Label>
            <%--<telerik:RadButton ID="RadButtonPackSlip1" runat="server" Text="<%$ Resources:Default, ButtonPackingSlip %>" OnClick="RadButtonPackSlip_Click" />
            <br />
            <br />
            <br />
            <asp:Label ID="LabelNBWeight" runat="server" Text="<%$ Resources:Default, Weight %>"></asp:Label>
            <telerik:RadTextBox ID="TextBoxNBWeight" runat="server" ></telerik:RadTextBox>--%>
            <br />
            <br />
	        <br />
            <asp:Label ID="LabelNewReferenceNumber" runat="server" Text="New Number:"></asp:Label>
            <telerik:RadTextBox ID="TextBoxNewReferenceNumber" runat="server"></telerik:RadTextBox>
        </asp:View>
        <asp:View ID="View6" runat="server">
            <br />
            <telerik:RadNumericTextBox ID="TextBoxCreateJobs" runat="server" Value="1" MinValue="1" MaxValue="100" showspinbuttons="true" incrementsettings-interceptarrowkeys="true" incrementsettings-interceptmousewheel="true">
                <NumberFormat AllowRounding="false" DecimalDigits="0" />
            </telerik:RadNumericTextBox>
            <telerik:RadButton ID="RadButtonCreateJobs" runat="server" Text="Create Jobs" OnClick="RadButtonCreateJobs_Click"></telerik:RadButton>
            <telerik:RadGrid ID="RadGridLinkedJobs" runat="server" OnItemCommand="RadGridLinkedJobs_ItemCommand"
                AllowPaging="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceLinkedJobs" PageSize="30" AllowMultiRowSelection="True">
                <MasterTableView DataKeyNames="JobId" DataSourceID="ObjectDataSourceLinkedJobs" >
                    <Columns>
                        <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" UniqueName="JobId"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn ReadOnly="True" DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" UniqueName="ReferenceNumber"></telerik:GridBoundColumn>
                        <telerik:GridButtonColumn Text="<%$ Resources:Default, ButtonPackingSlip %>" HeaderText="<%$ Resources:Default, ButtonPackingSlip %>" CommandName="PackingSlip"></telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                    ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                    AllowColumnsReorder="True">
                    <Selecting AllowRowSelect="True" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
            </telerik:RadGrid>
            <%--<asp:GridView ID="GridViewLinkedJobs" runat="server" DataSourceID="ObjectDataSourceLinkedJobs" AutoGenerateColumns="false" >
                <Columns>
                    <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" />
                    <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" />
                </Columns>
            </asp:GridView>--%>
            <asp:ObjectDataSource ID="ObjectDataSourceLinkedJobs" runat="server" TypeName="ProductCheck"
                SelectMethod="LinkedJobs">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="jobId" SessionField="jobId" Type="Int32" DefaultValue="-1" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <table>
                <%--<tr>
                    <td></td>
                    <td></td>
                    <td>
                        <telerik:RadButton ID="RadButtonPackSlip" runat="server" Text="<%$ Resources:Default, ButtonPackingSlip %>" OnClick="RadButtonPackSlip_Click" />
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        <asp:Label ID="LabelLabels" runat="server" Text="Labels:"></asp:Label>
                    </td>
                    <td>
                        <telerik:RadNumericTextBox ID="TextBoxLabels" runat="server" Value="1" MinValue="1" MaxValue="5" showspinbuttons="true" incrementsettings-interceptarrowkeys="true" incrementsettings-interceptmousewheel="true"></telerik:RadNumericTextBox>
                    </td>
                    <td>
                        <telerik:RadButton ID="RadButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="RadButtonPrint_Click"></telerik:RadButton>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <br />
        </asp:View>
        <asp:View ID="View7" runat="server">
            <br />
            <asp:Label ID="LabelContainerType" runat="server" Text="<%$ Resources:Default, ContainerType %>"></asp:Label>
            <asp:DropDownList ID="ddlContainerType" runat="server" DataSourceId="ObjectDataSourceContainerType"
             DataTextField="Description" DataValueField="ContainerTypeId">
            </asp:DropDownList>
            <br />
            <asp:ObjectDataSource ID="ObjectDataSourceContainerType" runat="server" TypeName="ProductCheck"
                SelectMethod="SearchContainerTypes">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="View8" runat="server">
            <asp:DetailsView ID="DetailsView2" runat="server" DataKeyNames="JobId,Finished" DataSourceID="ObjectDataSourceJob" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" />
                    <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" />
                    <asp:BoundField DataField="Lines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, OrderQuantity %>" />
                    <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" />
                    <asp:BoundField DataField="CheckQuantity" HeaderText="<%$ Resources:Default, CheckQuantity %>" />
                </Fields>
            </asp:DetailsView>
            <br />
            <asp:Label ID="LabelWeight" runat="server" Text="<%$ Resources:Default, Weight %>"></asp:Label>
            <telerik:RadTextBox ID="TextBoxWeight" runat="server" ></telerik:RadTextBox>
        </asp:View>
        <asp:View ID="View9" runat="server">
            <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="JobId,Finished" DataSourceID="ObjectDataSourceJob" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" />
                    <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" />
                    <asp:BoundField DataField="Lines" HeaderText="<%$ Resources:Default, NumberOfLines %>" />
                    <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, OrderQuantity %>" />
                    <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" />
                    <asp:BoundField DataField="CheckQuantity" HeaderText="<%$ Resources:Default, CheckQuantity %>" />
                </Fields>
            </asp:DetailsView>
            <br />
            <asp:Label ID="LabelStoreLocation" runat="server" Text="<%$ Resources:Default, StoreLocation %>"></asp:Label>
            <telerik:RadTextBox ID="TextBoxStoreLocation" runat="server" Text=""></telerik:RadTextBox>
        </asp:View>
        <asp:View ID="View10" runat="server">
            <telerik:RadButton ID="RadButtonRadButtonConsolidationSlip" runat="server" Text="Consolidation Slip" OnClick="RadButtonConsolidationSlip_Click" />
        </asp:View>
    </asp:MultiView>
    <br />
    <br />
    <table>
        <tr>
            <td>
                <telerik:RadButton ID="RadButtonAccept" runat="server" Text="<%$ Resources:Default, ButtonAccept %>" OnClick="RadButtonAccept_Click"></telerik:RadButton>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonBack" runat="server" Text="<%$ Resources:Default, Back %>" OnClick="RadButtonBack_Click" Visible="false"></telerik:RadButton>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonNext" runat="server" Text="<%$ Resources:Default, ButtonNext %>" OnClick="RadButtonNext_Click" Visible="false"></telerik:RadButton>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonNewBox" runat="server" Text="<%$ Resources:Default, ButtonNewBox %>" OnClick="RadButtonNewBox_Click" Visible="false"></telerik:RadButton>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonChangeBox" runat="server" Text="<%$ Resources:Default, ButtonChangeBox %>" OnClick="RadButtonChangeBox_Click" Visible="false"></telerik:RadButton>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonFinished" runat="server" Text="<%$ Resources:Default, ButtonFinish %>" OnClick="RadButtonFinished_Click" Visible="false"></telerik:RadButton>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClick="RadButtonClose_Click"></telerik:RadButton>
            </td>
        </tr>
    </table>
    <%--<telerik:RadButton ID="RadButtonQuit" runat="server" Text="<%$ Resources:Default, ButtonQuit %>" PostBackUrl="~/Default.aspx"></telerik:RadButton>--%>
    <br />
    <br />
    <br />
    <asp:GridView ID="GridViewProducts" runat="server" DataSourceID="ObjectDataSourceProducts" AutoGenerateColumns="false" >
        <Columns>
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
            <%--<asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, OrderQuantity %>" />
            <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" />--%>
            <asp:BoundField DataField="CheckQuantity" HeaderText="<%$ Resources:Default, CheckQuantity %>" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSourceProducts" runat="server" TypeName="ProductCheck"
        SelectMethod="ProductDetails">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="jobId" SessionField="jobId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceJob" runat="server" TypeName="ProductCheck"
        SelectMethod="ConfirmPallet">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" DefaultValue="-1" />
            <asp:ControlParameter Name="ReferenceNumber" ControlID="TextBoxReferenceNumber" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>