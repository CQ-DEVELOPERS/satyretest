<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="TransferOrder.aspx.cs" Inherits="Outbound_TransferOrder" Title="<%$ Resources:Default, TransferOrderTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/TransferOrderSearch.ascx" TagName="TransferOrderSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, TransferOrderTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, TransferOrderAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="Orders">
            <ContentTemplate>
                <uc1:TransferOrderSearch ID="TransferOrderSearch1" runat="server"></uc1:TransferOrderSearch>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonPalletise" runat="server" Text="Auto Palletise" OnClick="ButtonPalletise_Click" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonDeallocate" runat="server" Text="Deallocate" OnClick="ButtonDeallocate_Click" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderDeallocate" runat="server" TargetControlID="ButtonDeallocate" ConfirmText="Press OK to confirm reverse."></ajaxToolkit:ConfirmButtonExtender>
                        </td>
                        <td>
                            <asp:Button ID="ButtonRelease" runat="server" Text="<%$ Resources:Default, ButtonRelease %>" OnClick="ButtonRelease_Click" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderRelease" runat="server" TargetControlID="ButtonRelease" ConfirmText="Press OK to confirm release."></ajaxToolkit:ConfirmButtonExtender>
                        </td>
                        <td>
                            <asp:Button ID="ButtonManual" runat="server" Text="<%$ Resources:Default, ButtonManual %>" OnClick="ButtonManual_Click" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderManual" runat="server" TargetControlID="ButtonManual" ConfirmText="Press OK to confirm manual release."></ajaxToolkit:ConfirmButtonExtender>
                        </td>
                        <td>
                            <asp:Button ID="ButtonPause" runat="server" Text="<%$ Resources:Default, ButtonPause %>" OnClick="ButtonPause_Click" />
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderPause" runat="server" TargetControlID="ButtonPause" ConfirmText="Press OK to pause load."></ajaxToolkit:ConfirmButtonExtender>
                        </td>
                        <td>
                            <asp:Button ID="ButtonAutoAllocation" runat="server" Text="<%$ Resources:Default, ButtonAutoAllocation %>" OnClick="ButtonAutoLocations_Click" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonReleaseNoStock" runat="server" Text="<%$ Resources:Default, ButtonReleaseNoStock %>" OnClick="ButtonReleaseNoStock_Click" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonCombine" runat="server" Text="<%$ Resources:Default, Combine %>" OnClick="ButtonCombine_Click" />
                        </td>
                        <td>
                            <asp:Label ID="LabelOperator" runat="server" Text="<%$ Resources:Default,Operator %>"></asp:Label>
                            <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                            DataTextField="Operator" DataValueField="OperatorId">
                                </asp:DropDownList>
                             <asp:Button ID="ButtonOperatorRelease" runat="server" Text="Operator Release" OnClick="ButtonOperatorRelease_Click" />
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Image ID="ImageGreen3" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                <asp:Label ID="LabelGreen3" runat="server" Text="Raw material avaible in production"></asp:Label>
                <asp:Image ID="ImageYellow3" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                <asp:Label ID="LabelYellow3" runat="server" Text="Raw material avaible - replenishment required"></asp:Label>
                <asp:Image ID="ImageRed3" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                <asp:Label ID="LabelRed3" runat="server" Text="Raw material not avaible"></asp:Label>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewInstruction" runat="server" OnRowDataBound="GridViewInstruction_RowDataBound" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="False" 
                            DataSourceID="ObjectDataSourcePlanning" DataKeyNames="OutboundShipmentId,IssueId" OnSelectedIndexChanged="GridViewInstruction_SelectedIndexChanged" PageSize="30">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" ShowEditButton="true"></asp:CommandField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>" SortExpression="OutboundDocumentType">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, RequiredQuantity %>" SortExpression="RequiredQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="UOM" HeaderText="<%$ Resources:Default, UOM %>" SortExpression="UOM" />
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                
                                <asp:TemplateField HeaderText="<%$ Resources:Default, ManufacturingLocation %>" SortExpression="ManufacturingLocation">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListManufacturingLocation" runat="server" DataSourceID="ObjectDataSourceManufacturingLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("ManufacturingId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblManufacturingLocation" runat="server" Text='<%# Bind("ManufacturingLocation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>" SortExpression="Remarks">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" SortExpression="ConfirmedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" SortExpression="JobId">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, RawStaging %>" SortExpression="RawStaging">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListRawStaging" runat="server" DataSourceID="ObjectDataSourceRawStaging"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("RawStagingId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRawStaging" runat="server" Text='<%# Bind("RawStaging") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, ProductionStaging %>" SortExpression="ProductionStaging">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListProductionStaging" runat="server" DataSourceID="ObjectDataSourceProductionStaging"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("ProductionStagingId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblProductionStaging" runat="server" Text='<%# Bind("ProductionStaging") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="DwellTime" HeaderText="<%$ Resources:Default, DwellTime %>" SortExpression="DwellTime">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status">
                                <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPalletise" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonDeallocate" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonRelease" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonManual" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPause" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonReleaseNoStock" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonOperatorRelease" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonCombine" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="GridViewInstruction" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="TransferOrder"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="LocationId" SessionField="LocationId" Type="Int32" />
                        <asp:SessionParameter Name="ProductCode" SessionField="ProductCode" Type="String" />
                        <asp:SessionParameter Name="Product" SessionField="Product" Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="Batch" Type="String" />
                        <asp:Parameter Name="ManufacturingId" Type="Int32" />
                        <asp:Parameter Name="RawStagingId" Type="Int32" />
                        <asp:Parameter Name="ProductionStagingId" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceManufacturingLocation" runat="server" TypeName="Location" SelectMethod="GetLocationsByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="areaCode" DefaultValue="S" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceRawStaging" runat="server" TypeName="Location" SelectMethod="GetLocationsByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="areaCode" DefaultValue="RAW" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceProductionStaging" runat="server" TypeName="Location" SelectMethod="GetLocationsByAreaCode">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:Parameter Name="areaCode" DefaultValue="PRD" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
                    TypeName="Priority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Lines">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelOrderLine">
                    <ContentTemplate>
                        <asp:Image ID="ImageGreen4" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                        <asp:Label ID="LabelGreen4" runat="server" Text="Raw material avaible in production"></asp:Label>
                        <asp:Image ID="ImageYellow4" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                        <asp:Label ID="LabelYellow4" runat="server" Text="Raw material avaible - replenishment required"></asp:Label>
                        <asp:Image ID="ImageRed4" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                        <asp:Label ID="LabelRed4" runat="server" Text="Raw material not avaible"></asp:Label>
                        <br />
                        <asp:GridView ID="GridViewLineUpdate" runat="server" DataSourceID="ObjectDataSourceOrderLines"
                            DataKeyNames="IssueLineId" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product">
                                </asp:BoundField>
                                <%--<asp:TemplateField HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListBatch" runat="server" OnSelectedIndexChanged="DropDownListBatch_OnSelectedIndexChanged"
                                            DataSourceID="ObjectDataSourceBatch" DataTextField="Batch" DataValueField="StorageUnitBatchId"
                                            SelectedValue='<%# Bind("StorageUnitBatchId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBatch" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode" />
                                <asp:BoundField ReadOnly="True" DataField="RequiredQuantity" HeaderText="<%$ Resources:Default, RequiredQuantity %>" SortExpression="RequiredQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>" SortExpression="AllocatedQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>" SortExpression="AvailableQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="ManufacturingQuantity" HeaderText="<%$ Resources:Default, ManufacturingQuantity %>" SortExpression="ManufacturingQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="RawQuantity" HeaderText="<%$ Resources:Default, RawQuantity %>" SortExpression="RawQuantity" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                                <asp:BoundField ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>" SortExpression="AvailablePercentage" ApplyFormatInEditMode="true" DataFormatString="{0:G0}" />
                            </Columns>
                        </asp:GridView>
                        <%--<asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                            SelectMethod="GetBatchesByStorageUnit" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:ControlParameter ControlID="GridViewLineUpdate" Name="StorageUnitId" PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>--%>
                        <asp:ObjectDataSource ID="ObjectDataSourceOrderLines" runat="server" TypeName="TransferOrder"
                            SelectMethod="GetOrderLines" UpdateMethod="UpdateOrderLine">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" DefaultValue="-1" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" DefaultValue="-1" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:Parameter Type="Int32" Name="issueLineId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitBatchId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitId"></asp:Parameter>
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="Jobs">
            <ContentTemplate>
                <asp:Button ID="ButtonSelect" runat="server" Text="Select All" OnClick="ButtonSelect_Click"></asp:Button>
                <asp:Button ID="ButtonButtonPrint" OnClick="ButtonPrint_Click" runat="server" Text="<%$ Resources:Default, ButtonLabel %>"/>
                <asp:Button ID="ButtonPrintJob" OnClick="ButtonPrintJob_Click" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"/>
                <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged"
                            DataKeyNames="JobId">
                            <Columns>
                                <asp:TemplateField HeaderText="Label">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField SelectText="Select" ShowSelectButton="True" />
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="TransferOrder"
                            SelectMethod="SearchPickingJobs">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonButtonPrint" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPrintJob" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel4" HeaderText="Details">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelDetails" runat="server">
                    <ContentTemplate>
                        <asp:Image ID="ImageLocationStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                        <asp:Label ID="LabelLocationStandard" runat="server" Text="No Location"></asp:Label>
                        <asp:Image ID="ImageLocationBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                        <asp:Label ID="LabelLocationBlue" runat="server" Text="Allocated"></asp:Label>
                        <asp:GridView ID="GridViewDetails" runat="server" 
                            DataSourceID="ObjectDataSourceDetails"
                            AutoGenerateColumns="False" 
                            DataKeyNames="InstructionId" 
                            AllowPaging="True"              
                            AllowSorting="True">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>" SortExpression="Product">
                                </asp:BoundField>                    
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" SortExpression="SKUCode">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" SortExpression="Quantity">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>" SortExpression="ConfirmedQuantity">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>" SortExpression="InstructionType">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>" SortExpression="PickLocation">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>" SortExpression="Operator">
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceDetails" 
                    runat="server" 
                    TypeName="TransferOrder"
                    SelectMethod="SearchLinesByJob">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" />
                    </SelectParameters>                   
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator" SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
