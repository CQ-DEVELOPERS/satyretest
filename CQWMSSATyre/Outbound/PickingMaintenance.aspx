<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PickingMaintenance.aspx.cs" Inherits="Outbound_PickingMaintenance"
    Title="<%$ Resources:Default, PickingMaintenanceTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PickingMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PickingMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                    OnClick="ButtonSearch_Click" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundDocument" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOutboundDocument" runat="server" DataSourceID="ObjectDataSourceOutboundDocument"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewOutboundDocument_OnSelectedIndexChanged"
                            DataKeyNames="OutboundShipmentId,IssueId" AllowSorting="true">
                            <Columns>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>"
                                    SortExpression="DocumentNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>"
                                    SortExpression="Priority">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                    SortExpression="CustomerCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>"
                                    SortExpression="Customer">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, DespatchLocation %>"
                                    SortExpression="Location">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                    SortExpression="DeliveryDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocument" runat="server" TypeName="PickingMaintenance"
                            SelectMethod="SearchPickingDocuments">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                                    Type="Int32" />
                                <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                                <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                                <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                                    Type="String" />
                                <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, FullLines%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewFullLines">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSelect1" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                            OnClick="ButtonSelect_Click"></asp:Button>
                        <asp:Button ID="ButtonEdit1" runat="server" Text="<%$ Resources:Default, Edit%>"
                            OnClick="ButtonEdit_Click" />
                        <asp:Button ID="ButtonSave1" runat="server" Text="<%$ Resources:Default, Save%>"
                            OnClick="ButtonSave_Click" />
                        <asp:Button ID="ButtonSplit1" runat="server" Text="<%$ Resources:Default, SplitLine%>"
                            OnClick="ButtonSplit_Click" />
                        <asp:Button ID="ButtonManualLocations1" runat="server" Text="<%$ Resources:Default, AlternateLocation%>"
                            OnClick="ButtonManualLocations_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonReset1" runat="server" Text="<%$ Resources:Default, ResetStatus%>"
                            OnClick="ButtonReset_Click" />
                        <asp:Button ID="ButtonPrint1" runat="server" Text="<%$ Resources:Default, PrintPickSlip%>"
                            OnClick="ButtonPrint_Click" />
                        <asp:Button ID="ButtonPickLabel1" runat="server" Text="<%$ Resources:Default, PrintPickLabel%>"
                            OnClick="ButtonPickLabel_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonJobLabel1" runat="server" Text="<%$ Resources:Default, PrintJobLabel%>"
                            OnClick="ButtonJobLabel_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonSerial1" runat="server" Text="<%$ Resources:Default, AllocateSerialNumbers%>"
                            OnClick="ButtonSerial_Click" style="Width:auto;"/>
                        <asp:GridView ID="GridViewFullLines" runat="server" DataSourceID="ObjectDataSourceFullLines"
                            AutoGenerateColumns="False" DataKeyNames="JobId,InstructionId">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--                                <asp:BoundField ReadOnly="true" DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>--%>
                                <asp:BoundField ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, PickLocation %>">
                                    <EditItemTemplate>
                                        <asp:Label ID="LabelPickLocationEdit" runat="server" Text='<%# Eval("PickLocation") %>'></asp:Label>
                                        <asp:Button ID="ButtonManualLocations3" runat="server" Text="<%$ Resources:Default, AlternateLocation%>"
                                            OnClick="ButtonManualLocations_Click" style="Width:auto;"/>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelPickLocationItem" runat="server" Text='<%# Eval("PickLocation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="true" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Operator %>">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                                            DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelOperator" runat="server" Text='<%# Bind("Operator") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Area" HeaderText="<%$ Resources:Default, Area %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceFullLines" runat="server" TypeName="PickingMaintenance"
                            SelectMethod="SearchPickingLines" UpdateMethod="UpdateLine">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="P" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="jobId" Type="Int32" />
                                <asp:Parameter Name="instructionId" Type="Int32" />
                                <asp:Parameter Name="confirmedQuantity" Type="Decimal" />
                                <asp:Parameter Name="palletId" Type="Int32" />
                                <asp:Parameter Name="operatorId" Type="Int32" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, MixedJobs%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged"
                            DataKeyNames="JobId">
                            <Columns>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="PickingMaintenance"
                            SelectMethod="SearchPickingJobs">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <%--                            <asp:AsyncPostBackTrigger ControlID="Tabs" EventName="Load" />--%>
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel4" HeaderText="<%$ Resources:Default, MixedLines%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewMixedLines">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSelect2" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                            OnClick="ButtonSelect_Click"></asp:Button>
                        <asp:Button ID="ButtonEdit2" runat="server" Text="<%$ Resources:Default, Edit%>"
                            OnClick="ButtonEdit_Click" />
                        <asp:Button ID="ButtonSave2" runat="server" Text="<%$ Resources:Default, Save%>"
                            OnClick="ButtonSave_Click" />
                        <asp:Button ID="ButtonSplit2" runat="server" Text="<%$ Resources:Default, SplitLine%>"
                            OnClick="ButtonSplit_Click" />
                        <asp:Button ID="ButtonManualLocations2" runat="server" Text="<%$ Resources:Default, AlternateLocation%>"
                            OnClick="ButtonManualLocations_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonReset2" runat="server" Text="<%$ Resources:Default, ResetStatus%>"
                            OnClick="ButtonReset_Click" />
                        <asp:Button ID="ButtonPrint2" runat="server" Text="<%$ Resources:Default, PrintPickSlip%>"
                            OnClick="ButtonPrint_Click" />
                        <asp:Button ID="ButtonPickLabel2" runat="server" Text="<%$ Resources:Default, PrintPickLabel%>"
                            OnClick="ButtonPickLabel_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonJobLabel2" runat="server" Text="<%$ Resources:Default, PrintJobLabel%>"
                            OnClick="ButtonJobLabel_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonSerial2" runat="server" Text="<%$ Resources:Default, AllocateSerialNumbers%>"
                            OnClick="ButtonSerial_Click" style="Width:auto;"/>
                        <asp:GridView ID="GridViewMixedLines" runat="server" DataSourceID="ObjectDataSourceMixedLines"
                            AutoGenerateColumns="False" DataKeyNames="JobId,InstructionId">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--                                <asp:BoundField ReadOnly="true" DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>--%>
                                <asp:BoundField ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, PickLocation %>">
                                    <EditItemTemplate>
                                        <asp:Label ID="LabelPickLocationEdit" runat="server" Text='<%# Eval("PickLocation") %>'></asp:Label>
                                        <asp:Button ID="ButtonManualLocations3" runat="server" Text="<%$ Resources:Default, AlternateLocation%>"
                                            OnClick="ButtonManualLocations_Click" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelPickLocationItem" runat="server" Text='<%# Eval("PickLocation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="true" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Operator %>">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceOperator"
                                            DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelOperator" runat="server" Text='<%# Bind("Operator") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Area" HeaderText="<%$ Resources:Default, Area %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceMixedLines" runat="server" TypeName="PickingMaintenance"
                            SelectMethod="SearchLinesByJob" UpdateMethod="UpdateLine">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="JobId" Type="Int32" SessionField="JobId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="jobId" Type="Int32" />
                                <asp:Parameter Name="instructionId" Type="Int32" />
                                <asp:Parameter Name="confirmedQuantity" Type="Decimal" />
                                <asp:Parameter Name="palletId" Type="Int32" />
                                <asp:Parameter Name="operatorId" Type="Int32" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
        SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
