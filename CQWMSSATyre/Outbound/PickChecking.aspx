<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PickChecking.aspx.cs" Inherits="Outbound_PickChecking" Title="<%$ Resources:Default, PickCheckingTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PickCheckingTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PickCheckingAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                    OnClick="ButtonSearch_Click" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:Button ID="ButtonCheckSheet" runat="server" Text="<%$ Resources:Default, PrintCheckSheet%>"
                    OnClick="ButtonCheckSheet_Click" style="Width:auto;"/>
                <asp:Button ID="ButtonDespatchLabel" runat="server" Text="<%$ Resources:Default, PrintDespatchLabel%>"
                    OnClick="ButtonDespatchLabel_Click" style="Width:auto;"/>
                <asp:Button ID="ButtonComplete" runat="server" Text="<%$ Resources:Default, CheckingComplete%>"
                    OnClick="ButtonComplete_Click" style="Width:auto;"/>
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderComplete" runat="server"
                    TargetControlID="ButtonComplete" ConfirmText="<%$ Resources:Default, PressOKtoconfirmchecked%>."
                    Enabled="True">
                </ajaxToolkit:ConfirmButtonExtender>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundDocument">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOutboundDocument" runat="server" DataSourceID="ObjectDataSourceOutboundDocument"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewOutboundDocument_OnSelectedIndexChanged"
                            DataKeyNames="OutboundShipmentId,IssueId">
                            <Columns>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, DespatchLocation %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocument" runat="server" TypeName="PickChecking"
                            SelectMethod="SearchPickingDocuments">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                                    Type="Int32" />
                                <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                                <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                                <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                                    Type="String" />
                                <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonComplete" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, FullLines%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewFullLines">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSelect1" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                            OnClick="ButtonSelect_Click"></asp:Button>
                        <asp:Button ID="ButtonEdit1" runat="server" Text="<%$ Resources:Default, Edit%>"
                            OnClick="ButtonEdit_Click" />
                        <asp:Button ID="ButtonSave1" runat="server" Text="<%$ Resources:Default, Save%>"
                            OnClick="ButtonSave_Click" />
                        <asp:Button ID="ButtonDefaultQuantity1" runat="server" Text="<%$ Resources:Default, DefaultCheckQuantities%>"
                            OnClick="ButtonDefaultQuantity_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonDespatchLabel1" runat="server" Text="<%$ Resources:Default, PrintDespatchLabel%>"
                            OnClick="ButtonDespatchLabel_Click" />
                        <asp:Button ID="ButtonPackSlip1" runat="server" Text="<%$ Resources:Default, PrintPackingSlip%>"
                            OnClick="ButtonPackSlip_Click" style="Width:auto;"/>
                        <asp:GridView ID="GridViewFullLines" runat="server" DataSourceID="ObjectDataSourceFullLines"
                            AutoGenerateColumns="False" DataKeyNames="JobId,InstructionId">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="true" DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="CheckQuantity" HeaderText="<%$ Resources:Default, CheckQuantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Area" HeaderText="<%$ Resources:Default, Area %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceFullLines" runat="server" TypeName="PickChecking"
                            SelectMethod="SearchPickingLines" UpdateMethod="UpdateLine">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="P" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="jobId" Type="Int32" />
                                <asp:Parameter Name="instructionId" Type="Int32" />
                                <asp:Parameter Name="checkQuantity" Type="Decimal" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, MixedJobs%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                            AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged"
                            DataKeyNames="JobId">
                            <Columns>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="PickChecking"
                            SelectMethod="SearchPickingJobs">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel4" HeaderText="<%$ Resources:Default, MixedLines%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewMixedLines">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSelect2" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                            OnClick="ButtonSelect_Click"></asp:Button>
                        <asp:Button ID="ButtonEdit2" runat="server" Text="<%$ Resources:Default, Edit%>"
                            OnClick="ButtonEdit_Click" />
                        <asp:Button ID="ButtonSave2" runat="server" Text="<%$ Resources:Default, Save%>"
                            OnClick="ButtonSave_Click" />
                        <asp:Button ID="ButtonButtonDefaultQuantity2" runat="server" Text="<%$ Resources:Default, DefaultCheckQuantities%>"
                            OnClick="ButtonDefaultQuantity_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonButtonDespatchLabel2" runat="server" Text="<%$ Resources:Default, PrintDespatchLabel%>"
                            OnClick="ButtonDespatchLabel_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonButtonPackSlip2" runat="server" Text="<%$ Resources:Default, PrintPackingSlip%>"
                            OnClick="ButtonPackSlip_Click" style="Width:auto;"/>
                        <asp:GridView ID="GridViewMixedLines" runat="server" DataSourceID="ObjectDataSourceMixedLines"
                            AutoGenerateColumns="False" DataKeyNames="JobId,InstructionId">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="true" DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="CheckQuantity" HeaderText="<%$ Resources:Default, CheckQuantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="Area" HeaderText="<%$ Resources:Default, Area %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="true" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceMixedLines" runat="server" TypeName="PickChecking"
                            SelectMethod="SearchLinesByJob" UpdateMethod="UpdateLine">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="JobId" Type="Int32" SessionField="JobId" />
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Name="jobId" Type="Int32" />
                                <asp:Parameter Name="instructionId" Type="Int32" />
                                <asp:Parameter Name="checkQuantity" Type="Decimal" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
