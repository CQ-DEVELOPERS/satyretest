<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="DropSequence.aspx.cs" Inherits="Outbound_DropSequence"
    Title="<%$ Resources:Default, DropSequenceTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, DropSequenceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="Label2" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, DropSequenceAgenda %>"></asp:Label>
    <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="Drop Sequence">
            <ContentTemplate>
                <asp:Button ID="ButtonReturn" runat="server" Text="<%$ Resources:Default, ButtonReturn %>" OnClick="ButtonReturn_Click" />
                <asp:GridView ID="GridViewShipment" runat="server" AutoGenerateColumns="False"
                    DataKeyNames="OutboundShipmentId" DataSourceID="ObjectDataSourceShipment">
                    <Columns>
                        <asp:BoundField DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ShipmentDate" HeaderText="<%$ Resources:Default, ShipmentDate %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, DespatchLocation %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                            <ItemStyle Wrap="False"></ItemStyle>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                
                <asp:ObjectDataSource ID="ObjectDataSourceShipment" runat="server"
                    TypeName="OutboundShipment"
                    SelectMethod="GetShipmentDetails">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
                <br />
                
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundShipment">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewDropSequence" runat="server" DataKeyNames="IssueId"
                            AutoGenerateColumns="False" AutoGenerateEditButton="True" DataSourceID="ObjectDataSourceDropSequence">
                            <Columns>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" ReadOnly="True">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompanyCode" HeaderText="<%$ Resources:Default, CustomerCode %>" ReadOnly="True">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>" ReadOnly="True">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>" ReadOnly="True">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="DefaultDropSequence" HeaderText="<%$ Resources:Default, DefaultDropSequence %>" ReadOnly="True">
                                    <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, CurrentDropSequence %>">
                                    <EditItemTemplate>
                                        <div align="center">
                                            <asp:DropDownList ID="DropDownListDropSequence" runat="server" DataSourceID="ObjectDataSourceDropSequenceDDL"
                                                DataTextField="DropSequence" DataValueField="DropSequence" SelectedValue='<%# Bind("DropSequence") %>'>
                                            </asp:DropDownList>
                                        </div>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <div align="center">
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("DropSequence") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
                <asp:ObjectDataSource ID="ObjectDataSourceDropSequence" runat="server"
                    TypeName="OutboundShipment"
                    SelectMethod="GetDropSequenceDetails"
                    UpdateMethod="UpdateDropSequenceDetails">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="DropSequence" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                
                <asp:ObjectDataSource ID="ObjectDataSourceDropSequenceDDL" runat="server"
                    TypeName="OutboundShipment"
                    SelectMethod="GetDropSequence">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId" Type="Int32" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="Full Line">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewFullLines">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewFullLines" runat="server" 
                                      DataSourceID="ObjectDataSourceFullLines"
                                      AutoGenerateColumns="False" 
                                      DataKeyNames="JobId,InstructionId">
                            <Columns>
                                <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default, Area %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceFullLines" 
                                              runat="server"
                                              TypeName="PickingMaintenance"
                                              SelectMethod="SearchPickingLines">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId"></asp:SessionParameter>
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId"></asp:SessionParameter>
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="P"></asp:Parameter>
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="Mixed Jobs" >
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewJobs" runat="server" 
                                      DataSourceID="ObjectDataSourceJobs"
                                      AutoGenerateColumns="False"
                                      OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged"
                                      DataKeyNames="JobId">
                            <Columns>
                                <asp:CommandField SelectText="Select" ShowSelectButton="True" />
                                <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceJobs" 
                                              runat="server"
                                              TypeName="PickingMaintenance"
                                              SelectMethod="SearchPickingJobs">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId"></asp:SessionParameter>
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId"></asp:SessionParameter>
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM"></asp:Parameter>
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel4" HeaderText="Mixed Lines" >
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewMixedLines">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewMixedLines" runat="server" 
                                      DataSourceID="ObjectDataSourceMixedLines"
                                      AutoGenerateColumns="False" 
                                      DataKeyNames="JobId,InstructionId">
                            <Columns>
                                <asp:BoundField DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default, Area %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceMixedLines" 
                                              runat="server"
                                              TypeName="PickingMaintenance"
                                              SelectMethod="SearchLinesByJob">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="JobId" Type="Int32" SessionField="JobId"></asp:SessionParameter>
                                <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM"></asp:Parameter>
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>