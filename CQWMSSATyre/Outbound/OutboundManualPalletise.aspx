<%@ Page Language="C#" MasterPageFile="~/MasterPages/Blank.master" AutoEventWireup="true" CodeFile="OutboundManualPalletise.aspx.cs" Inherits="Outbound_OutboundManualPalletise" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="Manual Palletisation"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="Inbound"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewReceiptLine">
        <ContentTemplate>
            <asp:ObjectDataSource ID="objInstruction" runat="server" TypeName="ManualPalletiseSplit"
                UpdateMethod="SetQuantityFieldSplit" SelectMethod="GetInstructionToSplit">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="InstructionId" SessionField="InstructionIdSplit" DefaultValue="" Type="int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="Quantity" SessionField="Quantity" DefaultValue="" Type="Decimal" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <table>
                <tr>
                    <td>
                        <asp:GridView ID="GridViewReceiptLine" runat="server" AutoGenerateColumns="false"
                            DataSourceID="objInstruction" DataKeyNames="JobId,InstructionId" AllowPaging="true">
                            <Columns>
                                <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default,JobId %>" SortExpression="ProductCode" />
                                <asp:BoundField DataField="InstructionId" HeaderText="<%$ Resources:Default,InstructionId %>"
                                    SortExpression="ProductCode" />
                                <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default,ProductCode %>"
                                    SortExpression="ProductCode" />
                                <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default,Product %>"
                                    SortExpression="Product" />
                                <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default,SKUCode %>"
                                    SortExpression="SKUCode" />
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default,Quantity %>"
                                    SortExpression="Quantity" />
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td valign="top">
                        <table>
                            <tr>
                                <td style="height: 21px" colspan="2">
                                    <asp:Label ID="Label5" runat="server" Text="Total Quantity Split Difference" Width="100%"
                                        Font-Bold="True"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 103px; height: 21px">
                                    <asp:Label ID="Label2" runat="server" Text="Difference" Width="100px"></asp:Label></td>
                                <td style="height: 21px">
                                    <asp:Label ID="lblSplitTotal" runat="server" Font-Size="Medium" Font-Italic="False"
                                        Font-Bold="True"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 103px">
                                    <asp:Label ID="Label3" runat="server" Text="Order Qty" Width="100px"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblOrder" runat="server" Font-Size="Medium" Font-Italic="False" Font-Bold="True"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 103px; height: 21px">
                                    <asp:Label ID="Label4" runat="server" Text="Order" Width="100px"></asp:Label></td>
                                <td style="height: 21px">
                                    <asp:Label ID="lblQty" runat="server" Font-Size="Medium" Font-Italic="False" Font-Bold="True"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonGetNextReceipt" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <br />
    <asp:Label ID="LabelQuantity" runat="server" Text="Enter number of put away lines to create:"></asp:Label>
    <asp:TextBox ID="TextBoxLines" runat="server"></asp:TextBox>
    <asp:Button ID="ButtonCreateLines" runat="server" Text="Create Lines" OnClick="ButtonCreateLines_Click" />
    <br />
    <br />
    <asp:Button ID="ButtonAutoLocations" runat="server" Text="Auto Allocation" OnClick="ButtonAutoLocations_Click" />
    <asp:Button ID="ButtonManualLocations" runat="server" Text="Manual Allocation" OnClick="ButtonManualLocations_Click" />
    <asp:Button ID="ButtonGetNextReceipt" runat="server" Text="Next Receipt" OnClick="GetNextInstruction_Click" />
    <asp:Button ID="Button1" runat="server" Text="Link Serial Numbers" OnClick="GetNextInstruction_Click"
        PostBackUrl="RegisterSerialNumberJob.aspx" /><br />
    <br />
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
        <ContentTemplate>
            <asp:ObjectDataSource ID="objManualPalletiseSplit" runat="server" SelectMethod="GetInstructions"
                UpdateMethod="SetQuantityFieldSplit" TypeName="ManualPalletiseSplit" OnSelecting="objManualPalletiseSplit_OnSelecting">
                <UpdateParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:Parameter Type="Int32" Name="instructionId"></asp:Parameter>
                    <asp:Parameter Type="Decimal" Name="quantity"></asp:Parameter>
                </UpdateParameters>
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter SessionField="instructionIdSplit" Type="Int32" DefaultValue="" Name="JobId"></asp:SessionParameter>
                </SelectParameters>
            </asp:ObjectDataSource>
            <table>
                <tr>
                    <td>
                        <asp:GridView ID="GridViewInstruction" runat="server" AllowPaging="True" DataKeyNames="InstructionId"
                            DataSourceID="objManualPalletiseSplit" AutoGenerateColumns="False" OnRowUpdating="GridViewInstruction_RowUpdating"
                            OnRowUpdated="GridViewInstruction_RowUpdated" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server" Checked="true"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="InstructionId" ReadOnly="True" SortExpression="InstructionId"
                                    HeaderText="<%$ Resources:Default, InstructionId %>"></asp:BoundField>
                                <asp:BoundField DataField="Quantity" SortExpression="Quantity" HeaderText="<%$ Resources:Default,Quantity %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" ReadOnly="True" SortExpression="Status" HeaderText="<%$ Resources:Default,Status %>">
                                </asp:BoundField>
                                <asp:BoundField DataField="StoreLocation" ReadOnly="True" SortExpression="StoreLocation"
                                    HeaderText="<%$ Resources:Default,StoreLocation %>"></asp:BoundField>
                                <asp:TemplateField HeaderText="Total Quantity Split">
                                    <EditItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("SplitQuantity") %>' Load="SayHelloWorld"></asp:Label>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SplitQuantity") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField ShowEditButton="True"></asp:CommandField>
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td>
                    </td>
                    <td valign="top">
                    </td>
                </tr>
                <tr>
                    <td style="height: 16px">
                    </td>
                    <td style="height: 16px">
                    </td>
                    <td style="height: 16px">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td style="width: 160px; height: 18px">
                        &nbsp;<asp:GridView ID="GridViewQuantity" runat="server" DataKeyNames="SplitQuantity"
                            Visible="False">
                        </asp:GridView>
                    </td>
                    <td style="height: 18px">
                        &nbsp;&nbsp;&nbsp;
                    </td>
                    <td style="width: 4px; height: 18px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; height: 17px">
                        <asp:Label ID="Label1" runat="server" Text="The Remainder to put away is:" Width="228px"></asp:Label></td>
                    <td style="height: 17px">
                        &nbsp;</td>
                    <td style="width: 4px; height: 17px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; height: 17px">
                    </td>
                    <td style="height: 17px">
                    </td>
                    <td style="width: 4px; height: 17px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; height: 17px">
                    </td>
                    <td style="height: 17px">
                    </td>
                    <td style="width: 4px; height: 17px">
                    </td>
                </tr>
            </table>
            <asp:Button ID="cmdPrint" OnClick="cmdPrint_Click" runat="server" Text="Print"></asp:Button>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonCreateLines" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonAutoLocations" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="ButtonGetNextReceipt" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
