<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PickJobWeightCheck.aspx.cs" Inherits="Outbound_PickJobWeightCheck"
    Title="<%$ Resources:Default, PickJobWeightCheckTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PickJobWeightCheckTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PickJobWeightCheckAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <asp:UpdatePanel ID="UpdatePanelTable" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, ScanPalletJob%>"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxBarcode" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, DocumentSearch%>"
                            OnClick="ButtonSearch_Click" style="Width:auto;"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelTareWeight" runat="server" Text="<%$ Resources:Default, TareWeight%>"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxTareWeight" runat="server"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="Filteredtextboxextender1" runat="server"
                            TargetControlID="TextBoxTareWeight" FilterType="Numbers">
                        </ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelWeight" runat="server" Text="<%$ Resources:Default, ActualWeight%>"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxWeight" runat="server"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="Filteredtextboxextender2" runat="server"
                            TargetControlID="TextBoxWeight" FilterType="Numbers">
                        </ajaxToolkit:FilteredTextBoxExtender>
                    </td>
                    <td>
                        <asp:Button ID="ButtonReadScale" runat="server" Text="<%$ Resources:Default, ReadScale%>"
                            OnClick="ButtonReadScale_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonPrint" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <table>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="ButtonAccept" runat="server" Text="<%$ Resources:Default, Accept%>"
                            OnClick="ButtonAccept_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, PrintLabel%>"
                            OnClick="ButtonPrint_Click" Enabled="false" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonAccept" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
