<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="JobMaintenance.aspx.cs" Inherits="Outbound_JobMaintenance" Title="<%$ Resources:Default, JobMaintenanceTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundSearch"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, JobMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, JobMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Shipment%>">
            <ContentTemplate>
                <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>" />
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblShowOrders" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="<%$ Resources:Default, Orders%>" Value="true" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Default, Shipment%>" Value="false"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:Button ID="ButtonPrint" OnClick="ButtonPrint_Click" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonInvoice" OnClick="ButtonInvoice_Click" runat="server" Text="<%$ Resources:Default, ButtonInvoice %>" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonCheckSheet" runat="server" Text="<%$ Resources:Default, PrintCheckSheet%>"
                                OnClick="ButtonCheckSheet_Click" style="Width:auto;"/>
                        </td>
                        <td>
                            <asp:Button ID="ButtonClose" OnClick="ButtonClose_Click" runat="server" Text="<%$ Resources:Default, ButtonClose %>" />
                            <ajaxToolkit:ConfirmButtonExtender ID="cbeClose" runat="server" TargetControlID="ButtonClose"
                                ConfirmText="<%$ Resources:Default, PressOKToCloseShipment%>">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewShipment">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewShipment" runat="server" OnRowDataBound="GridViewShipment_RowDataBound"
                            AllowPaging="true" AllowSorting="true" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePlanning"
                            DataKeyNames="OutboundShipmentId,IssueId" OnSelectedIndexChanged="GridViewShipment_SelectedIndexChanged"
                            PageSize="30">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" ShowSelectButton="True"></asp:CommandField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                    SortExpression="OutboundShipmentId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                    SortExpression="CustomerCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>"
                                    SortExpression="Customer">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, Lines %>"
                                    SortExpression="NumberOfLines" />
                                <asp:BoundField ReadOnly="True" DataField="FirstLoaded" HeaderText="<%$ Resources:Default, FirstLoaded %>"
                                    SortExpression="FirstLoaded" />
                                <asp:BoundField ReadOnly="True" DataField="Loaded" HeaderText="<%$ Resources:Default, Loaded %>"
                                    SortExpression="Loaded">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Pallets" HeaderText="<%$ Resources:Default, Pallets %>"
                                    SortExpression="Pallets">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                    SortExpression="CreateDate" DataFormatString="{0:yyyy-MM-dd}">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ShipmentDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                    SortExpression="ShipmentDate" DataFormatString="{0:yyyy-MM-dd}">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>"
                                    SortExpression="OutboundDocumentType">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Location" HeaderText="<%$ Resources:Default, Location %>"
                                    SortExpression="Location">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Route" HeaderText="<%$ Resources:Default, Route %>"
                                    SortExpression="Route">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="VehicleRegistration" HeaderText="<%$ Resources:Default, VehicleRegistration %>"
                                    SortExpression="VehicleRegistration">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>"
                                    SortExpression="Remarks">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonPrint" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="GridViewShipment" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="LoadMaintenance"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:ControlParameter ControlID="rblShowOrders" DefaultValue="0" Name="ShowOrders"
                            PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                        <asp:Parameter Name="vehicleRegistration" Type="String" />
                        <asp:Parameter Name="shipmentDate" Type="DateTime" />
                        <asp:Parameter Name="remarks" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
                    TypeName="Priority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Jobs%>">
            <ContentTemplate>
                <table>
                    <tr valign="top">
                        <td>
                            <asp:Button ID="ButtonSelect" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                                OnClick="ButtonSelect_Click"></asp:Button>
                            <asp:Button ID="ButtonLoad" OnClick="ButtonLoad_Click" runat="server" Text="<%$ Resources:Default, ButtonLoad %>" />
                            <asp:Button ID="ButtonUnload" OnClick="ButtonUnload_Click" runat="server" Text="<%$ Resources:Default, ButtonUnload %>" />
                            <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                                        AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged"
                                        DataKeyNames="OutboundShipmentId,JobId">
                                        <Columns>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:CommandField SelectText="<%$ Resources:Default, Remove%>" ShowSelectButton="True" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="LoadMaintenance"
                                        SelectMethod="SearchPickingJobs">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                                            <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                                            <asp:Parameter Name="InstructionTypeCode" Type="String" DefaultValue="PM" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ButtonLoad" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ButtonUnload" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <%--<asp:Button ID="ButtonLink" OnClick="ButtonLink_Click" runat="server" Text="<%$ Resources:Default, Link %>" />
                            <asp:Button ID="ButtonUnlink" OnClick="ButtonUnlink_Click" runat="server" Text="<%$ Resources:Default, Unlink %>" />--%>
                            <asp:Button ID="ButtonLink" OnClick="ButtonLink_Click" runat="server" Text="<%$ Resources:Default, ButtonAdd %>" />
                            <asp:Button ID="ButtonUnlink" OnClick="ButtonUnlink_Click" runat="server" Text="<%$ Resources:Default, ButtonRemove %>" />
                        </td>
                        <td>
                            <uc1:OutboundSearch ID="OutboundSearch2" runat="server"></uc1:OutboundSearch>
                            <asp:Button ID="ButtonSearchUnlink" OnClick="ButtonSearchUnlink_Click" runat="server" Text="<%$ Resources:Default, Search %>" />
                            <asp:UpdatePanel runat="server" ID="UpdatePanelUnlinked">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewUnlinked" runat="server" DataSourceID="ObjectDataSourceUnlinked"
                                        AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewUnlinked_OnSelectedIndexChanged"
                                        DataKeyNames="OutboundShipmentId,JobId">
                                        <Columns>
                                            <asp:CommandField SelectText="<%$ Resources:Default, Add%>" ShowSelectButton="True" />
                                            <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Priority" HeaderText="<%$ Resources:Default, Priority %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>">
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceUnlinked" runat="server" TypeName="LoadMaintenance" SelectMethod="SearchUnlinkedJobs">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                            <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId" Type="Int32" DefaultValue="-1" />
                                            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                            <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
                                            <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                                            <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                                            <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                                            <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                                            <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonSearchUnlink" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ButtonLink" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ButtonUnlink" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel4" HeaderText="<%$ Resources:Default, Details%>
">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelDetails" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewDetails" runat="server" DataSourceID="ObjectDataSourceDetails"
                            AutoGenerateColumns="False" DataKeyNames="InstructionId" AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                                    SortExpression="Batch"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                    SortExpression="Quantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, ConfirmedQuantity %>"
                                    SortExpression="ConfirmedQuantity"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" TypeName="LoadMaintenance"
                    SelectMethod="SearchLinesByJob">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
        SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
