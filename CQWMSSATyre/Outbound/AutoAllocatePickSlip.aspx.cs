using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Outbound_AutoAllocatePickSlip : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PutawayMaintenance " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region ButtonPrint1_Click
    protected void ButtonPrint1_Click(object sender, EventArgs e)
    {
    }
    #endregion ButtonPrint1_Click

    #region ButtonPrint2_Click
    protected void ButtonPrint2_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint2_Click";

        try
        {
            int operatorId = int.Parse(DropDownListOperator.SelectedValue);

            Print(operatorId);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_AutoAllocatePickSlip" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPrint2_Click

    #region TextBoxOperator_TextChanged
    protected void TextBoxOperator_TextChanged(object sender, EventArgs e)
    {
        theErrMethod = "TextBoxOperator_TextChanged";

        try
        {
            Operator op = new Operator();
            int operatorId = op.GetOperatorId(Session["ConnectionStringName"].ToString(),
                                                TextBoxOperator.Text);

            if (operatorId != -1)
                Print(operatorId);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_AutoAllocatePickSlip" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion TextBoxOperator_TextChanged

    #region Print
    protected void Print(int operatorId)
    {
        theErrMethod = "Print";
        try
        {
            Int32 jobId = -1;

            Operator nextPick = new Operator();

            jobId = nextPick.GetNextPickingJob(Session["ConnectionStringName"].ToString(), operatorId, "PM", "-1");

            if (jobId != -1)
            {
                Session["FromURL"] = "~/Outbound/AutoAllocatePickSlip.aspx";

                Session["ReportName"] = "Job Pick Sheet";

                Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[4];

                // Create the JobId report parameter
                RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("JobId", jobId.ToString());

                // Create the ServerName report parameter
                RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                // Create the DatabaseName report parameter
                RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                // Create the UserName report parameter
                RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                Session["ReportParameters"] = RptParameters;

                Response.Redirect("~/Reports/Report.aspx");
            }

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("Outbound_AutoAllocatePickSlip" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Print

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "AutoAllocatePickSlip", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "AutoAllocatePickSlip", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling
}
