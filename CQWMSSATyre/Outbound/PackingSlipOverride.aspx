<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PackingSlipOverride.aspx.cs" Inherits="Outbound_PackingSlipOverride"
    Title="<%$ Resources:Default, PackingSlipOverrideTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, PackingSlipOverrideTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, PackingSlipOverrideAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
    <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>"
        OnClick="ButtonSearch_Click" />
    <div style="clear: left;">
    </div>
    <br />
    <asp:Button ID="ButtonSendOverride" runat="server" Text="<%$ Resources:Default, Send%>"
        OnClick="ButtonSendOverride_Click" />
    <asp:DropDownList ID="ddlDeliveryMethods" runat="server" >
        <asp:ListItem Text="Express" Value="Express" ></asp:ListItem>
        <asp:ListItem Text="Logistics" Value="Logistics" ></asp:ListItem>
    </asp:DropDownList>
    <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundDocument">
        <ContentTemplate>
            <br />
            <asp:GridView ID="GridViewPackingSlip" runat="server" DataSourceID="ObjectDataSourcePackingSlip"
                AllowPaging="true" AutoGenerateColumns="False" DataKeyNames="OrderNumber">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Edit%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField ReadOnly="true" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="true" DataField="DeliveryNoteNumber" HeaderText="<%$ Resources:Default, DeliveryNoteNumber %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="true" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="true" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="true" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="true" DataField="DeliveryMethod" HeaderText="<%$ Resources:Default, DeliveryMethod %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="true" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="true" DataField="ManagerOverride" HeaderText="<%$ Resources:Default, Override %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="true" DataField="Manager" HeaderText="<%$ Resources:Default, UserName %>">
                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourcePackingSlip" runat="server" TypeName="DespatchConfirmation"
                SelectMethod="SearchPackingSlip">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                        Type="String" />
                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                        Type="Int32" />
                    <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                    <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                    <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                        Type="String" />
                    <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                    <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSendOverride" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
