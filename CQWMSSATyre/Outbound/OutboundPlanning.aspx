<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="OutboundPlanning.aspx.cs" Inherits="Outbound_OutboundPlanning" Title="<%$ Resources:Default, OutboundPlanningTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, OutboundPlanningTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, OutboundPlanningAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Orders%>">
            <ContentTemplate>
                <table>
                    <tr valign="top">
                        <td valign="bottom">
                            <table>
                                <tr>
                                    <td>
                                        <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanelBuild" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="PanelBuild" runat="server" GroupingText="<%$ Resources:Default, PalletBuildResults%>"
                                                    Font-Size="X-Small">
                                                    <asp:GridView ID="GridViewBuild" runat="server" DataSourceID="ObjectDataSourceBuild"
                                                        AutoGenerateColumns="true">
                                                        <RowStyle Font-Size="X-Small" />
                                                        <HeaderStyle Font-Size="X-Small" />
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ButtonPalletise" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:ObjectDataSource ID="ObjectDataSourceBuild" runat="server" TypeName="Planning"
                                            SelectMethod="GetBuildResults">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                    Type="String" />
                                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                                    Type="Int32" DefaultValue="-1" />
                                                <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanelAllocation" runat="server">
                                            <ContentTemplate>
                                                <asp:Panel ID="PanelAllocation" runat="server" GroupingText="<%$ Resources:Default, StockAllocationResults%>"
                                                    Font-Size="X-Small">
                                                    <asp:GridView ID="GridViewAllocation" runat="server" DataSourceID="ObjectDataSourceAllocation"
                                                        AutoGenerateColumns="true">
                                                        <RowStyle Font-Size="X-Small" />
                                                        <HeaderStyle Font-Size="X-Small" />
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="ButtonPalletise" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:ObjectDataSource ID="ObjectDataSourceAllocation" runat="server" TypeName="Planning"
                                            SelectMethod="GetAllocationResults">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                    Type="String" />
                                                <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                                                    Type="Int32" DefaultValue="-1" />
                                                <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <asp:Label ID="LabelNotPicked" runat="server" Text="<%$ Resources:Default, NotPicked%>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="ImageRed" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelRed" runat="server" Text="<%$ Resources:Default, Overdue%>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="ImageOrange" runat="server" ImageUrl="~/Images/Indicators/Orange.gif" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelOrange" runat="server" Text="<%$ Resources:Default, Requiredin2hours%>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Image ID="ImageYellow" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelYellow" runat="server" Text="<%$ Resources:Default, Requiredin24hours%>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Image ID="ImageStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelStandard" runat="server" Text="<%$ Resources:Default, RequiredGT24hours%>"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, Search%>" />
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonPalletise" runat="server" Text="<%$ Resources:Default, AutoPalletise%>"
                                            OnClick="ButtonPalletise_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonAutoAllocation" runat="server" Text="<%$ Resources:Default, AutoAllocation%>"
                                            OnClick="ButtonAutoLocations_Click" style="Width:auto;"/>
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonDeallocate" runat="server" Text="<%$ Resources:Default, DeAllocate%>"
                                            OnClick="ButtonDeallocate_Click" />
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonPlanningComplete" runat="server" Text="<%$ Resources:Default, PlanningComplete%>"
                                            OnClick="ButtonPlanningComplete_Click" style="Width:auto;"/>
                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderRelease" runat="server"
                                            TargetControlID="ButtonPlanningComplete" ConfirmText="<%$ Resources:Default, PressOKtoconfirmplanned%>"
                                            Enabled="True">
                                        </ajaxToolkit:ConfirmButtonExtender>
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonLoadPlanning" runat="server" Text="<%$ Resources:Default, LoadPlanningTitle%>"
                                            Enabled="False" />
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonWavePlanning" runat="server" Text="<%$ Resources:Default, WavePlanning%>"
                                            Enabled="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewInstruction">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewInstruction" runat="server" DataSourceID="ObjectDataSourcePlanning"
                            OnPageIndexChanging="GridViewInstruction_PageIndexChanging" AllowPaging="True"
                            AllowSorting="True" DataKeyNames="OutboundShipmentId,IssueId,OrderNumber" AutoGenerateColumns="False"
                            OnRowUpdating="GridViewInstruction_RowUpdating" OnSelectedIndexChanged="GridViewInstruction_SelectedIndexChanged">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" ShowSelectButton="True"></asp:CommandField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, RequiredIn %>">
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Availability %>">
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "Availability", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>" SortExpression="OutboundShipmentId"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PrincipalCode" HeaderText="<%$ Resources:Default, Principal %>" SortExpression="PrincipalCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>" SortExpression="CustomerCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>" SortExpression="Customer"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>" SortExpression="NumberOfLines"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, DeliveryDate %>" SortExpression="DeliveryDate">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxDeliveryDate" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtenderDeliveryDate" runat="server" Animated="true"
                                            Format="<%$ Resources:Default,DateFormat %>" TargetControlID="TextBoxDeliveryDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtenderDeliveryDate" runat="server"
                                            Mask="<%$ Resources:Default,DateMask %>" MaskType="Date" CultureName="<%$ Resources:Default,CultureCode %>" MessageValidatorTip="true"
                                            TargetControlID="TextBoxDeliveryDate">
                                        </ajaxToolkit:MaskedEditExtender>
                                        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidatorDeliveryDate" runat="server"
                                            ControlExtender="MaskedEditExtenderDeliveryDate" ControlToValidate="TextBoxDeliveryDate"
                                            Display="Dynamic" EmptyValueMessage="Manufacturing Date is required" InvalidValueMessage="Manufacturing Date is invalid"
                                            IsValidEmpty="False" TooltipMessage="Input a Manufacturing Date">
                                        </ajaxToolkit:MaskedEditValidator>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="LabelDeliveryDate" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, AreaType %>" SortExpression="AreaType">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListAreaType" runat="server" DataSourceID="ObjectDataSourceAreaType"
                                            DataTextField="AreaType" DataValueField="AreaType" SelectedValue='<%# Bind("AreaType") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("AreaType") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>" SortExpression="OutboundDocumentType"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Location %>" SortExpression="Location">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                            DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListRoute" runat="server" DataSourceID="ObjectDataSourceRouting" DataTextField="Route" DataValueField="RouteId" SelectedValue='<%# Bind("RouteId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoute" runat="server" Text='<%# Bind("Route") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Remarks %>" SortExpression="Remarks">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:BoundField DataField="AddressLine1" HeaderText="Address Line 1"></asp:BoundField>
                                <asp:BoundField DataField="AddressLine2" HeaderText="Address Line 2"></asp:BoundField>
                                <asp:BoundField DataField="AddressLine3" HeaderText="Address Line 3"></asp:BoundField>
                                <asp:BoundField DataField="AddressLine4" HeaderText="Address Line 4"></asp:BoundField>
                                <asp:BoundField DataField="AddressLine5" HeaderText="Address Line 5"></asp:BoundField>

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonPalletise" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonPlanningComplete" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDeallocate" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="GridViewInstruction" EventName="SelectedIndexChanged">
                        </asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="Planning"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:QueryStringParameter Name="outboundShipmentId" QueryStringField="OutboundShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="outboundShipmentId" Type="Int32" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="LocationId" Type="Int32" />
                        <asp:Parameter Name="OrderNumber" Type="String" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                        <asp:Parameter Name="RouteId" Type="Int32" />
                        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
                        <asp:Parameter Name="Remarks" Type="String" />
                        <asp:Parameter Name="AddressLine1" Type="String" />
                        <asp:Parameter Name="AddressLine2" Type="String" />
                        <asp:Parameter Name="AddressLine3" Type="String" />
                        <asp:Parameter Name="AddressLine4" Type="String" />
                        <asp:Parameter Name="AddressLine5" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
                    TypeName="Priority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceAreaType" runat="server" TypeName="Area"
                    SelectMethod="GetAreaTypes">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
                    SelectMethod="GetLocationsByIssue">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceRouting" runat="server" SelectMethod="GetRoutes"
                    TypeName="Route">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="<%$ Resources:Default, Lines%>">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="UpdatePanelOrderLine">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSelect1" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                            OnClick="ButtonSelect_Click"></asp:Button>
                        <asp:Button ID="ButtonEdit" OnClick="ButtonEdit_Click" runat="server" Text="<%$ Resources:Default, Edit%>">
                        </asp:Button>
                        <asp:Button ID="ButtonSave" OnClick="ButtonSave_Click" runat="server" Text="<%$ Resources:Default, Save%>">
                        </asp:Button>
                        <asp:Button ID="ManualPalletise" OnClick="ButtonManualPalletise_Click" runat="server"
                            Text="<%$ Resources:Default, ManualAllocation%>" Width="106px" style="Width:auto;"></asp:Button>
                        <asp:Image ID="ImageRed2" runat="server" ImageUrl="~/Images/Indicators/Red.gif" />
                        <asp:Label ID="LabelRed2" runat="server" Text="<%$ Resources:Default, NoStock%>"></asp:Label>
                        <asp:Image ID="ImageYellow2" runat="server" ImageUrl="~/Images/Indicators/Yellow.gif" />
                        <asp:Label ID="LabelYellow2" runat="server" Text="<%$ Resources:Default, StockShort%>"></asp:Label>
                        <asp:Image ID="ImageGreen2" runat="server" ImageUrl="~/Images/Indicators/Green.gif" />
                        <asp:Label ID="LabelGreen2" runat="server" Text="<%$ Resources:Default, StockAvailable%>"></asp:Label>
                        <asp:GridView ID="GridViewLineUpdate" runat="server" DataSourceID="ObjectDataSourceOrderLines"
                            DataKeyNames="IssueLineId,StorageUnitId" AutoGenerateColumns="False" AllowPaging="True"
                            AllowSorting="True">
                            <Columns>
                                <asp:CommandField></asp:CommandField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>">
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Batch %>" SortExpression="Batch">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListBatch" runat="server" OnSelectedIndexChanged="DropDownListBatch_OnSelectedIndexChanged"
                                            DataSourceID="ObjectDataSourceBatch" DataTextField="Batch" DataValueField="StorageUnitBatchId"
                                            SelectedValue='<%# Bind("StorageUnitBatchId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBatch" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>"
                                    SortExpression="SKU"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>"
                                    SortExpression="OrderQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="AvailablePercentage" HeaderText="<%$ Resources:Default, AvailablePercentage %>"
                                    SortExpression="AvailablePercentage"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="AvailableQuantity" HeaderText="<%$ Resources:Default, AvailableQuantity %>"
                                    SortExpression="AvailableQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OtherAreaTypeQuantity" HeaderText="Other Areas"
                                    SortExpression="AvailableQuantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="LocationsAllocated" HeaderText="<%$ Resources:Default, LocationsAllocated %>"
                                    SortExpression="LocationsAllocated"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PalletQuantity" HeaderText="<%$ Resources:Default, PalletQuantity %>"
                                    SortExpression="PalletQuantity"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="Batch"
                            SelectMethod="GetBatchesByStorageUnit" OnSelecting="ObjectDataSourceBatch_OnSelecting">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:ControlParameter ControlID="GridViewLineUpdate" Name="StorageUnitId" PropertyName="SelectedValue"
                                    Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ObjectDataSource ID="ObjectDataSourceOrderLines" runat="server" TypeName="Planning"
                            SelectMethod="GetOrderLines" UpdateMethod="UpdateOrderLine">
                            <UpdateParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:Parameter Type="Int32" Name="issueLineId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitBatchId"></asp:Parameter>
                                <asp:Parameter Type="Int32" Name="storageUnitId"></asp:Parameter>
                            </UpdateParameters>
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="outboundShipmentId" Type="Int32" DefaultValue="-1" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Details%>">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanelDetails" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="ButtonSelect2" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                            OnClick="ButtonSelect_Click"></asp:Button>
                        <asp:Button ID="ButtonAutoAllocation2" runat="server" Text="<%$ Resources:Default, AutoAllocation%>"
                            OnClick="ButtonAutoLocations2_Click" />
                        <asp:Button ID="ButtonMix" runat="server" Text="<%$ Resources:Default, CreateMixedJob%>"
                            OnClick="ButtonMix_Click" style="Width:auto;"/>
                        <asp:Button ID="ButtonManualLocations2" runat="server" Text="<%$ Resources:Default, AlternateLocation%>"
                            OnClick="ButtonManualLocations_Click" style="Width:auto;"/>
                        <asp:Image ID="ImageLocationStandard" runat="server" ImageUrl="~/Images/Indicators/Standard.gif" />
                        <asp:Label ID="LabelLocationStandard" runat="server" Text="<%$ Resources:Default, NoLocation%>"></asp:Label>
                        <asp:Image ID="ImageLocationBlue" runat="server" ImageUrl="~/Images/Indicators/Blue.gif" />
                        <asp:Label ID="LabelLocationBlue" runat="server" Text="<%$ Resources:Default, Allocated%>"></asp:Label>
                        <asp:GridView ID="GridViewDetails" runat="server" DataSourceID="ObjectDataSourceDetails"
                            AutoGenerateColumns="False" DataKeyNames="InstructionId" AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>" SortExpression="XXX">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>"
                                    SortExpression="StorageUnitId"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                                    SortExpression="Batch"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>"
                                    SortExpression="SKU"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                    SortExpression="Quantity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>"
                                    SortExpression="JobId"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>"
                                    SortExpression="InstructionType"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>"
                                    SortExpression="PickLocation"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonAutoAllocation2" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceDetails" runat="server" TypeName="Planning"
                    SelectMethod="GetOrderDetails">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
