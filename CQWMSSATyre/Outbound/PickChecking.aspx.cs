using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_PickChecking: System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        if (!Page.IsPostBack)
        {
            try
            {
                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;

                Master.MsgText = "Page_Load"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
                Master.ErrorText = result;
            }

        }
    }
    #endregion "Page_Load"

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            GridViewOutboundDocument.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonSearch_Click"

    #region ButtonComplete_Click
    protected void ButtonComplete_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonComplete_Click";
        try
        {
            Status st = new Status();

            if (st.CheckingComplete(Session["ConnectionStringName"].ToString(),
                                (int)Session["WarehouseId"],
                                int.Parse(GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString()),
                                int.Parse(GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString())))
            {
                GridViewOutboundDocument.DataBind();

                Master.MsgText = "Checking Complete Update Successful"; Master.ErrorText = "";
            }
            else
            {
                Master.MsgText = "Checking Complete Update Failed"; Master.ErrorText = "";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonComplete_Click"

    #region GridViewOutboundDocument_OnSelectedIndexChanged
    protected void GridViewOutboundDocument_OnSelectedIndexChanged(object sender, EventArgs e)
    {
       theErrMethod = "GridViewOutboundDocument_OnSelectedIndexChanged";
        try

        {
            if (GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString() == "")
                Session["OutboundShipmentId"] = -1;
            else
                Session["OutboundShipmentId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString());

            if (GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString() == "")
                Session["IssueId"] = -1;
            else
                Session["IssueId"] = int.Parse(GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString());

            Master.MsgText = "Changed Selection"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "GridViewOutboundDocument_OnSelectedIndexChanged"

    #region GridViewJobs_OnSelectedIndexChanged
    protected void GridViewJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewJobs_OnSelectedIndexChanged";
        try
        {
            if (GridViewJobs.SelectedDataKey["JobId"].ToString() == "")
                Session["JobId"] = -1;
            else
                Session["JobId"] = int.Parse(GridViewJobs.SelectedDataKey["JobId"].ToString());

            GridViewJobs.DataBind();
            GridViewMixedLines.DataBind();

            Master.MsgText = "JobId Changed"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }


    }
    #endregion "GridViewJobs_OnSelectedIndexChanged"

    #region ButtonEdit_Click
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonEdit_Click";

        try
        {
            GetEditIndexRowNumber();
            SetEditIndexRowNumber();

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonEdit_Click

    #region ButtonSave_Click
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSave_Click";
        try
        {
            if(Tabs.ActiveTabIndex == 1)
            {
                if (GridViewFullLines.EditIndex != -1)
                   GridViewFullLines.UpdateRow(GridViewFullLines.EditIndex, true);
            }

            if (Tabs.ActiveTabIndex == 3)
            {
                if (GridViewMixedLines.EditIndex != -1)
                    GridViewMixedLines.UpdateRow(GridViewMixedLines.EditIndex, true);
            }
            
            SetEditIndexRowNumber();

            Master.MsgText = "Save"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSave_Click

    #region GetEditIndexRowNumber
    protected void GetEditIndexRowNumber()
    {
        theErrMethod = "GetEditIndexRowNumber";
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            if (Tabs.ActiveTabIndex == 1)
                foreach (GridViewRow row in GridViewFullLines.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");

                    if (cb.Checked == true)
                        rowList.Add(row.RowIndex);
                }

            if (Tabs.ActiveTabIndex == 3)
                foreach (GridViewRow row in GridViewMixedLines.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");

                    if (cb.Checked == true)
                        rowList.Add(row.RowIndex);
                }

            if (rowList.Count > 0)
                Session["rowList"] = rowList;

            Master.MsgText = "Selection Changed"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetEditIndexRowNumber"

    #region SetEditIndexRowNumber
    protected void SetEditIndexRowNumber()
    {
        theErrMethod = "SetEditIndexRowNumber";
        try
        {
            ArrayList rowList = (ArrayList)Session["rowList"];
            
            if (rowList == null)
                Session.Remove("rowList");
            else
            {
                if (Tabs.ActiveTabIndex == 1)
                {
                    if (GridViewFullLines.EditIndex != -1)
                        GridViewFullLines.UpdateRow(GridViewFullLines.EditIndex, true);

                    if (rowList.Count < 1)
                    {
                        Session.Remove("rowList");
                        GridViewFullLines.EditIndex = -1;
                    }
                    else
                    {
                        GridViewFullLines.EditIndex = (int)rowList[0];

                        rowList.Remove(rowList[0]);
                    }
                }

                if (Tabs.ActiveTabIndex == 3)
                {
                    if (GridViewMixedLines.EditIndex != -1)
                        GridViewMixedLines.UpdateRow(GridViewMixedLines.EditIndex, true);

                    if (rowList.Count < 1)
                    {
                        Session.Remove("rowList");
                        GridViewMixedLines.EditIndex = -1;
                    }
                    else
                    {
                        GridViewMixedLines.EditIndex = (int)rowList[0];

                        rowList.Remove(rowList[0]);
                    }
                }
            }

            Master.MsgText = "Edit"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "SetEditIndexRowNumber"

    #region ButtonCheckSheet_Click
    protected void ButtonCheckSheet_Click(object sender, EventArgs e)
    {
        try
        {
            theErrMethod = "ButtonCheckSheet_Click";

            Session["FromURL"] = "~/Outbound/PickChecking.aspx";

            Session["ReportName"] = "Pick Check Sheet";

            ReportParameter[] RptParameters = new ReportParameter[5];

            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            // Create the InboundShipmentId report parameter
            string strOutboundShipmentId = GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString();
            RptParameters[3] = new ReportParameter("OutboundShipmentId", strOutboundShipmentId);

            // Create the ReceiptId report parameter
            string strIssueId = GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString();
            RptParameters[4] = new ReportParameter("IssueId", strIssueId);

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

            Master.MsgText = "Print"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

            
    }
    #endregion "ButtonCheckSheet_Click"

	#region ButtonDespatchLabel_Click

    protected void ButtonDespatchLabel_Click(object sender, EventArgs e)
    {
        try
        {
            theErrMethod = "ButtonDespatchLabel_Click";

            ArrayList printList = new ArrayList();

            if (Tabs.ActiveTabIndex == 1)
                foreach (GridViewRow row in GridViewFullLines.Rows)
                {
                    var cb = (CheckBox)row.FindControl("CheckBoxEdit");

                    if (cb.Checked)
                    {
                        DataKey dataKey = GridViewFullLines.DataKeys[row.RowIndex];

                        if (dataKey != null)
                            if (dataKey.Values != null) printList.Add(dataKey.Values["JobId"]);
                    }
                }

            if (Tabs.ActiveTabIndex == 3)
            {
                //foreach (GridViewRow row in GridViewMixedLines.Rows)
                //{
                //    cb = (CheckBox)row.FindControl("CheckBoxEdit");

                //    if (cb.Checked == true)
                //        printList.Add(GridViewMixedLines.DataKeys[row.RowIndex].Values["JobId"]);
                //}

                printList.Add((int)Session["JobId"]);
            }

            if (printList.Count > 0)
            {
                Session["checkedList"] = printList;

                //Session["LabelName"] = "Despatch By Order Label.lbl";

                Session["LabelName"] = "Despatch By Order Label Plumblink.lbl";

                Session["FromURL"] = "~/Outbound/PickChecking.aspx";

                if (Session["Printer"] == null)
                    Session["Printer"] = string.Empty;

                if (Session["Printer"].ToString() == string.Empty)
                    Response.Redirect("~/Common/NLLabels.aspx");
                else
                {
                    Session["Printing"] = true;

                    Response.Redirect("~/Common/NLPrint.aspx");
                }
            }

            Master.MsgText = "Despatch";

            Master.ErrorText = string.Empty;
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message);

            Master.ErrorText = result;
        }
    }

    #endregion ButtonDespatchLabel_Click
    
    #region ButtonDefaultQuantity_Click
    protected void ButtonDefaultQuantity_Click(object sender, EventArgs e)
    {
        try
        {
            theErrMethod = "ButtonDefaultQuantity_Click";

            PickChecking pc = new PickChecking();

            if (pc.DefaultCheckQuantity(Session["ConnectionStringName"].ToString(),
                                    int.Parse(GridViewOutboundDocument.SelectedDataKey["OutboundShipmentId"].ToString()),
                                    int.Parse(GridViewOutboundDocument.SelectedDataKey["IssueId"].ToString())))
            {
                GridViewFullLines.DataBind();
                GridViewMixedLines.DataBind();

                Master.MsgText = "Quantity Update Successful"; Master.ErrorText = "";
            }
            else
            {
                Master.MsgText = "Quantity Update Failed"; Master.ErrorText = ""; 
            }

        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonDefaultQuantity_Click"

    #region ButtonPackSlip_Click
    protected void ButtonPackSlip_Click(object sender, EventArgs e)
    {
        try
        {
            theErrMethod = "ButtonPackSlip_Click";

            Master.MsgText = "Pack Slip"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickChecking" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonPackSlip_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            if (Tabs.ActiveTabIndex == 1)
                foreach (GridViewRow row in GridViewFullLines.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    cb.Checked = true;
                }

            if (Tabs.ActiveTabIndex == 3)
                foreach (GridViewRow row in GridViewMixedLines.Rows)
                {
                    cb = (CheckBox)row.FindControl("CheckBoxEdit");
                    cb.Checked = true;
                }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PickingMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PickChecking", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PickChecking", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
