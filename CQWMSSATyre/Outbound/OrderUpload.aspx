<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="OrderUpload.aspx.cs" Inherits="Outbound_OrderUpload"
    Title="<%$ Resources:Default, ReportTitle %>" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabUpload" HeaderText="<%$ Resources:Default, Upload%>">
            <ContentTemplate>
                <asp:Label ID="LabelFileUpload" runat="server" Text="Click Browse to find the file to upload:"></asp:Label>
                <asp:UpdatePanel ID="UpdatePanelFileUpload1" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="500" ToolTip="Click Browse to find the file to upload"/>
                            </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <br />
                                    <asp:Button ID="ButtonUpload" runat="server" Text="Save" OnClick="ButtonUpload_Click" />
                                </td>
                            </tr>
                        </table>
                        <telerik:RadGrid ID="RadGridStockAdjustment" runat="server"
                            AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceInterfaceImportOrders"
                            PageSize="30" ShowGroupPanel="True" Skin="Metro">
                            <MasterTableView DataKeyNames="PrimaryKey" DataSourceID="ObjectDataSourceInterfaceImportOrders"
                                AutoGenerateColumns="False" EnableHeaderContextMenu="true" AllowFilteringByColumn="true">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="OrderNumber" HeaderText="OrderNumber" SortExpression="OrderNumber"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProcessedDate" HeaderText="ProcessedDate" SortExpression="ProcessedDate"></telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                                <Resizing AllowColumnResize="true"></Resizing>
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <GroupingSettings ShowUnGroupButton="True" />
                        </telerik:RadGrid>
                        <asp:ObjectDataSource ID="ObjectDataSourceInterfaceImportOrders" runat="server" TypeName="InterfaceConsole"
                            SelectMethod="SearchInterfaceImportOrders">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <triggers>
                        <asp:postbacktrigger ControlID="ButtonUpload" />
                    </triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>

