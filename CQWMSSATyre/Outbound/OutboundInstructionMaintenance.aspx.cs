using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


// ============================================
// CLASS : Outbound_OutboundDocumentSearch
// ============================================
/// <summary>
/// Inscrution Maintenance happens on this screen.
/// </summary>
public partial class Outbound_OutboundInstructionMaintenance : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        if (!Page.IsPostBack)
        {
            try
            {
                if (!BusinessLayerValidation.Validate())
                    Response.Redirect("");

                Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
            }
            catch (Exception ex)
            {
                result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
                Master.ErrorText = result;
            }
        }
    }
    #endregion "Page_Load"

    #region ButtonDocumentSearch_Click
    /// <summary> 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonDocumentLineSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDocumentLineSearch_Click";

        try
        {
            GridViewLineUpdate.DataBind();

            Master.MsgText = "Search"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonDocumentSearch_Click

    #region GridViewLineUpdate_OnSelectedIndexChanged
    protected void GridViewLineUpdate_OnSelectedIndexChanged(object sender, System.EventArgs e)
    {
        theErrMethod = "GridViewLineUpdate_OnSelectedIndexChanged";
        try
        {
            Session["IssueLineId"] = GridViewLineUpdate.SelectedDataKey["IssueLineId"].ToString();

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewLineUpdate_OnSelectedIndexChanged"

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";
        try
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            while (index < GridViewLineUpdate.Rows.Count)
            {
                GridViewRow checkedRow = GridViewLineUpdate.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                {
                    rowList.Add(GridViewLineUpdate.DataKeys[index].Values["IssueLineId"]);
                }

                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = "Updated"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GetEditIndex

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            GetEditIndex();
            ArrayList rowList = (ArrayList)Session["checkedList"];

            if (rowList == null)
                Session.Remove("checkedList");
            else
            {
                int index = rowList.Count;

                if (index < 1)
                {
                    Session.Remove("checkedList");
                }
                else
                {
                    Planning ds = new Planning();

                    while (index > 0)
                    {
                        if (ds.AutoLocationAllocate(Session["ConnectionStringName"].ToString(), Convert.ToInt32(rowList[0])) == false)
                            Master.ErrorText = "Auto Allocation Not Complete";

                        rowList.Remove(rowList[0]);

                        index--;
                    }

                    Session.Remove("checkedList");

                    GridViewLineUpdate.DataBind();
                }
            }
            Master.MsgText = "Update"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAutoLocations_Click

    #region ButtonMix_Click
    protected void ButtonMix_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonMix_Click";
        try
        {
            Session["InstructionTypeCode"] = "PM";
            Response.Redirect("~/Common/MixedJobCreate.aspx");
            Master.MsgText = "Mix"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonMix_Click
    
    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "OutboundInstructionMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "OutboundInstructionMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}