<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="PalletRebuild.aspx.cs" Inherits="Outbound_PalletRebuild" Title="<%$ Resources:Default, OutboundShipmentLinkTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc2" %>
<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundShipmentSearch"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, OutboundShipmentLinkTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, OutboundShipmentLinkAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="<%$ Resources:Default, SearchLoads%>">
            <HeaderTemplate>
                Search Loads
            </HeaderTemplate>
            <ContentTemplate>
                <asp:Panel ID="PanelSearch" runat="server" Width="550px" BackColor="#EFEFEC">
                    <asp:UpdatePanel ID="UpdatePanelOutboundShipmentId" runat="server">
                        <ContentTemplate>
                            <uc1:OutboundShipmentSearch ID="OutboundShipmentSearch1" runat="server"></uc1:OutboundShipmentSearch>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ButtonDocumentSearch" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <br />
                </asp:Panel>
                <asp:Button ID="ButtonDocumentSearch" runat="server" Text="<%$ Resources:Default, Search%>"
                    OnClick="ButtonDocumentSearch_Click" />
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundShipment">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOutboundShipment" runat="server" AllowPaging="true" AllowSorting="true"
                            AutoGenerateColumns="False" DataSourceID="ObjectDataSourceOutboundShipment" DataKeyNames="OutboundShipmentId,IssueId"
                            OnSelectedIndexChanged="GridViewOutboundShipment_SelectedIndexChanged" PageSize="30">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                    SortExpression="OutboundShipmentId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                    SortExpression="CustomerCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>"
                                    SortExpression="Customer">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                    SortExpression="NumberOfLines">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                    SortExpression="CreateDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                    SortExpression="DeliveryDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="PercentageComplete" HeaderText="<%$ Resources:Default, PercentageComplete %>"
                                    SortExpression="PercentageComplete">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>"
                                    SortExpression="OutboundDocumentType">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Location" HeaderText="<%$ Resources:Default, Location %>"
                                    SortExpression="Location">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Route" HeaderText="<%$ Resources:Default, Route %>"
                                    SortExpression="Route">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>"
                                    SortExpression="Rating">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>"
                                    SortExpression="Remarks">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonDocumentSearch" EventName="click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceOutboundShipment" runat="server" TypeName="PalletRebuild"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="<%$ Resources:Default, RebuildJobs%>">
            <ContentTemplate>
                <table border="solid" cellpadding="5">
                    <tr>
                        <td>
                            <asp:Label ID="LabelLinkedJobs" runat="server" Text="<%$ Resources:Default, LinkedJobs%>"
                                Font-Bold="true"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="LabelAmountToMove" runat="server" Text="<%$ Resources:Default, AmountToMove%>"
                                Font-Bold="true"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LabelUnlinkedJobs" runat="server" Text="<%$ Resources:Default, JobsUnlinked%>"
                                Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewLinked">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlJobLinked" runat="server" DataSourceID="odsJobsLinked" DataTextField="JobId"
                                        DataValueField="JobId" AutoPostBack="true" OnSelectedIndexChanged="ddlJobsLinked_OnSelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:ObjectDataSource ID="odsJobsLinked" runat="server" TypeName="PalletRebuild"
                                        SelectMethod="GetLinkedJobs">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId"
                                                Type="Int32" />
                                            <asp:SessionParameter Name="IssueId" SessionField="IssueId" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                    <asp:GridView ID="GridViewLinked" runat="server" DataSourceID="ObjectDataSourceLinked"
                                        DataKeyNames="IssueLineId,InstructionId,ConfirmedQuantity" AllowSorting="true"
                                        AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewLinked_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True"
                                                HeaderText="<%$ Resources:Default, Transfer%>" />
                                            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                                SortExpression="OrderNumber" />
                                            <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>"
                                                SortExpression="ExternalCompany" />
                                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                                SortExpression="ProductCode" />
                                            <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                                SortExpression="ConfirmedQuantity" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceLinked" runat="server" TypeName="PalletRebuild"
                                        SelectMethod="GetLinkedInstructions">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:ControlParameter Name="JobId" Type="Int32" ControlID="ddlJobLinked" PropertyName="SelectedValue" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="GridViewLinked" EventName="selectedindexchanged">
                                    </asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                <ContentTemplate>
                                    <div style="text-align: center">
                                        <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
                                        <asp:TextBox ID="TextBoxQuantity" runat="server"></asp:TextBox>
                                        <asp:Button ID="ButtonTransferLinked" runat="server" Text="<%$ Resources:Default, MoveLinked%>"
                                            OnClick="ButtonTransferLinked_Click" Enabled="false" />
                                        <asp:Button ID="ButtonTransferUnlinked" runat="server" Text="<%$ Resources:Default, MoveUnlinked%>"
                                            OnClick="ButtonTransferUnlinked_Click" Enabled="false" />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <asp:TextBox ID="TextBoxComments" runat="server" TextMode="MultiLine" MaxLength="255"></asp:TextBox>
                                        <asp:Button ID="ButtonInstructionLinked" runat="server" Text="<%$ Resources:Default, InstructionLinked%>"
                                            OnClick="ButtonInstructionLinked_Click" Enabled="false" />
                                        <asp:Button ID="ButtonInstructionUnlinked" runat="server" Text="<%$ Resources:Default, InstructionUnlinked%>"
                                            OnClick="ButtonInstructionUnlinked_Click" Enabled="false" />
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="GridViewLinked" EventName="selectedindexchanged">
                                    </asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="GridViewUnlinked" EventName="selectedindexchanged">
                                    </asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                            <%--<ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="TextBoxQuantity" Width="120" Minimum = "1" Maximum = "1000" ></ajaxToolkit:NumericUpDownExtender>--%>
                        </td>
                        <td valign="top" width="100%">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewUnlinked">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlJobsUnlinked" runat="server" DataSourceID="odsJobsUnlinked"
                                        DataTextField="JobId" DataValueField="JobId" AutoPostBack="true" OnSelectedIndexChanged="ddlJobsUnlinked_OnSelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:ObjectDataSource ID="odsJobsUnlinked" runat="server" TypeName="PalletRebuild"
                                        SelectMethod="GetUnlinkedJobs">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:SessionParameter Name="OutboundShipmentId" SessionField="OutboundShipmentId"
                                                Type="Int32" />
                                            <asp:SessionParameter Name="IssueId" SessionField="IssueId" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                    <%--<asp:Button ID="ButtonUnlinkedSearch" runat="server" Text="Search" OnClick="ButtonUnlinkedSearch_Click" />--%>
                                    <asp:GridView ID="GridViewUnlinked" runat="server" DataSourceID="ObjectDataSourceUnlinked"
                                        DataKeyNames="IssueLineId,InstructionId,ConfirmedQuantity" AllowSorting="true"
                                        AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewUnlinked_SelectedIndexChanged">
                                        <Columns>
                                            <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True"
                                                HeaderText="<%$ Resources:Default, Transfer%>" />
                                            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                                SortExpression="OrderNumber" />
                                            <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>"
                                                SortExpression="ExternalCompany" />
                                            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                                SortExpression="ProductCode" />
                                            <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, Quantity %>"
                                                SortExpression="ConfirmedQuantity" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceUnlinked" runat="server" TypeName="PalletRebuild"
                                        SelectMethod="GetLinkedInstructions">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                                Type="String" />
                                            <asp:ControlParameter Name="JobId" Type="Int32" ControlID="ddlJobsUnlinked" PropertyName="SelectedValue" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType" runat="server" TypeName="OutboundDocumentType"
                    SelectMethod="GetOutboundDocumentTypes">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>
