<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReleaseMaintenance.aspx.cs" Inherits="Outbound_ReleaseMaintenance"
    Title="<%$ Resources:Default, ReleaseMaintenanceTitle %>" StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundSearch"
    TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReleaseMaintenanceTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReleaseMaintenanceAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Shipment%>">
            <ContentTemplate>
                <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" Text="<%$ Resources:Default, ButtonSearch %>" />
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblShowOrders" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="<%$ Resources:Default, Orders%>" Value="true" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Default, Shipment%>" Value="false"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:Button ID="ButtonBackorder" runat="server" Text="<%$ Resources:Default, ButtonBackorder %>"
                                OnClick="ButtonBackorder_Click" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonReleaseNoStock" runat="server" Text="<%$ Resources:Default, ButtonReleaseNoStock %>"
                                OnClick="ButtonReleaseNoStock_Click" style="Width:auto;"/>
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderReleaseNoStock" runat="server"
                                TargetControlID="ButtonReleaseNoStock" ConfirmText="<%$ Resources:Default, PressOKconfirmrerelease%>">
                            </ajaxToolkit:ConfirmButtonExtender>
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewShipment">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewShipment" runat="server" OnRowDataBound="GridViewShipment_RowDataBound"
                            AllowPaging="true" AllowSorting="true" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePlanning"
                            DataKeyNames="OutboundShipmentId,IssueId" OnSelectedIndexChanged="GridViewShipment_SelectedIndexChanged"
                            PageSize="30">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" ShowSelectButton="True"></asp:CommandField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>"
                                    SortExpression="OutboundShipmentId">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"
                                    SortExpression="OrderNumber">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>"
                                    SortExpression="CustomerCode">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>"
                                    SortExpression="Customer">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"
                                    SortExpression="NumberOfLines">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Units" HeaderText="<%$ Resources:Default, Units %>"
                                    SortExpression="Units">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="ShortPicks" HeaderText="<%$ Resources:Default, ShortPicks %>"
                                    SortExpression="ShortPicks">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Releases" HeaderText="<%$ Resources:Default, Releases %>"
                                    SortExpression="Releases">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>"
                                    SortExpression="CreateDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ShipmentDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"
                                    SortExpression="ShipmentDate">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Priority %>" SortExpression="Priority">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                            DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>"
                                    SortExpression="Status">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>"
                                    SortExpression="OutboundDocumentType">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Location" HeaderText="<%$ Resources:Default, Location %>"
                                    SortExpression="Location">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="Route" HeaderText="<%$ Resources:Default, Route %>"
                                    SortExpression="Route">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="VehicleRegistration" HeaderText="<%$ Resources:Default, VehicleRegistration %>"
                                    SortExpression="VehicleRegistration">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>"
                                    SortExpression="Remarks">
                                    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="GridViewShipment" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="ReleaseMaintenance"
                    SelectMethod="SearchOrders" UpdateMethod="UpdateOrder">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId"
                            Type="Int32" DefaultValue="-1" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId"
                            Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                            Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                        <asp:ControlParameter ControlID="rblShowOrders" DefaultValue="0" Name="ShowOrders"
                            PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                        <asp:Parameter Name="OutboundShipmentId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="IssueId" Type="Int32" />
                        <asp:Parameter Name="PriorityId" Type="Int32" />
                        <asp:Parameter Name="vehicleRegistration" Type="String" />
                        <asp:Parameter Name="shipmentDate" Type="DateTime" />
                        <asp:Parameter Name="remarks" Type="String" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
                    TypeName="Priority">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Details%>">
            <ContentTemplate>
                <asp:Button ID="ButtonSelect" runat="server" Text="<%$ Resources:Default, SelectAll%>"
                    OnClick="ButtonSelect_Click"></asp:Button>
                <asp:Button ID="ButtonReleaseNoStock2" OnClick="ButtonReleaseNoStock2_Click" runat="server"
                    Text="<%$ Resources:Default, ButtonReleaseNoStock %>" style="Width:auto;"/>
                <asp:UpdatePanel runat="server" ID="UpdatePanelJobs">
                    <ContentTemplate>
                        <asp:GridView ID="GridViewJobs" runat="server" DataSourceID="ObjectDataSourceJobs"
                            DataKeyNames="StorageUnitId" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewJobs_OnSelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Default, Label%>">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="DocumentNumber" HeaderText="<%$ Resources:Default, DocumentNumber %>"
                                    SortExpression="DocumentNumber" />
                                <asp:BoundField ReadOnly="True" DataField="StorageUnitId" Visible="False" HeaderText="<%$ Resources:Default, StorageUnitId %>" />
                                <asp:BoundField ReadOnly="True" DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>"
                                    SortExpression="ProductCode" />
                                <asp:BoundField ReadOnly="True" DataField="Product" HeaderText="<%$ Resources:Default, Product %>"
                                    SortExpression="Product" />
                                <asp:BoundField ReadOnly="True" DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>"
                                    SortExpression="SKUCode" />
                                <asp:BoundField ReadOnly="True" DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>"
                                    SortExpression="Batch" />
                                <asp:BoundField ReadOnly="True" DataField="OrderQuantity" HeaderText="<%$ Resources:Default, OrderQuantity %>"
                                    SortExpression="OrderQuantity" />
                                <asp:BoundField ReadOnly="True" DataField="ShortQuantity" HeaderText="<%$ Resources:Default, ShortQuantity %>"
                                    SortExpression="ShortQuantity" />
                                <asp:BoundField ReadOnly="True" DataField="SOH" HeaderText="<%$ Resources:Default, SOH %>"
                                    SortExpression="SOH" />
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSourceJobs" runat="server" TypeName="ReleaseMaintenance"
                            SelectMethod="SearchDetails">
                            <SelectParameters>
                                <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                    Type="String" />
                                <asp:SessionParameter Name="OutboundShipmentId" Type="Int32" SessionField="OutboundShipmentId" />
                                <asp:SessionParameter Name="IssueId" Type="Int32" SessionField="IssueId" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSelect" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ButtonReleaseNoStock2" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:ObjectDataSource ID="ObjectDataSourceOperator" runat="server" TypeName="Operator"
        SelectMethod="GetOperatorList">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="warehouseId" Type="Int32" SessionField="WarehouseId" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
