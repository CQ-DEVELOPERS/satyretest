<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="SupplierReturn.aspx.cs" Inherits="Outbound_SupplierReturn" Title="<%$ Resources:Default, OutboundDocumentTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="uc4" %>
<%@ Register Src="../Common/BatchSearch.ascx" TagName="BatchSearch" TagPrefix="uc3" %>
<%@ Register Src="../Common/SupplierSearch.ascx" TagName="ExternalCompanySearch" TagPrefix="uc2" %>
<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, OutboundDocumentTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, OutboundDocumentAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:TabContainer runat="server" ID="Tabs">
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel1" HeaderText="<%$ Resources:Default, Search%>">
            <ContentTemplate>
                <uc1:OutboundSearch ID="OutboundSearch1" runat="server"></uc1:OutboundSearch>
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Search%>" OnClick="ButtonSearch_Click" />
                <div style="clear: left;">
                </div>
                <br />
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundDocument" >
                    <ContentTemplate>
                        <asp:GridView ID="GridViewOutboundDocument" runat="server" DataSourceID="ObjectDataSourceOutboundDocument"
                            AllowPaging="true" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewOutboundDocument_OnSelectedIndexChanged"
                            DataKeyNames="OutboundDocumentId,ExternalCompanyId">
                            <Columns>
                                <asp:CommandField SelectText="<%$ Resources:Default, Select%>" ShowSelectButton="True" />
                                <asp:BoundField DataField="ExternalCompanyCode" SortExpression="ExternalCompanyCode"
                                    HeaderText="<%$ Resources:Default, CustomerCode %>"></asp:BoundField>
                                <asp:BoundField DataField="ExternalCompany" SortExpression="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>"></asp:BoundField>
                                <asp:BoundField DataField="NumberOfLines" SortExpression="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>"></asp:BoundField>
                                <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>"></asp:BoundField>
                                <asp:BoundField DataField="DeliveryDate" SortExpression="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>"></asp:BoundField>
                                <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="<%$ Resources:Default, Status %>"></asp:BoundField>
                                <asp:BoundField DataField="OutboundDocumentType" SortExpression="OutboundDocumentType"
                                    HeaderText="<%$ Resources:Default, OutboundDocumentType %>"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>

                <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocument" runat="server" SelectMethod="SearchOutboundDocument"
                    TypeName="OutboundDocument">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
                        <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
                        <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
                        <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                        <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
                        <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
                        <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel2" HeaderText="<%$ Resources:Default, Edit%>">
            <ContentTemplate>
                <table>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel runat="server" ID="UpdatePanelDetailsViewOutboundDocument">
                                <ContentTemplate>
                                    <asp:Button ID="ButtonChangeMode" runat="server" Text="<%$ Resources:Default, TryAgain%>" OnClick="ButtonChangeMode_Click" Visible="false" />
                                    <asp:DetailsView ID="DetailsViewOutboundDocument"
                                        runat="server"
                                        DataSourceID="ObjectDataSourceOutboundDocumentUpdate"
                                        DataKeyNames="OutboundDocumentId"
                                        AutoGenerateEditButton="true"
                                        AutoGenerateInsertButton="true"
                                        AutoGenerateDeleteButton="true"
                                        AutoGenerateRows="false"
                                        OnModeChanging="DetailsViewOutboundDocument_OnModeChanging"
                                        OnItemInserting="DetailsViewOutboundDocument_OnItemInserting">
                                        <Fields>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,OutboundDocumentType %>">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataSourceID="ObjectDataSourceOutboundDocumentType"
                                                        DataValueField="OutboundDocumentTypeId" DataTextField="OutboundDocumentType" SelectedValue='<%# Bind("OutboundDocumentTypeId") %>'>
                                                    </asp:DropDownList>
                                                    <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType" runat="server" TypeName="OutboundDocumentType"
                                                        SelectMethod="GetOutboundDocumentTypeReturn">
                                                        <SelectParameters>
                                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                                        </SelectParameters>
                                                    </asp:ObjectDataSource>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataSourceID="ObjectDataSourceOutboundDocumentType"
                                                        DataValueField="OutboundDocumentTypeId" DataTextField="OutboundDocumentType" SelectedValue='<%# Bind("OutboundDocumentTypeId") %>'>
                                                    </asp:DropDownList>
                                                    <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType" runat="server" TypeName="OutboundDocumentType"
                                                        SelectMethod="GetOutboundDocumentTypeReturn">
                                                        <SelectParameters>
                                                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                                        </SelectParameters>
                                                    </asp:ObjectDataSource>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("OutboundDocumentType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,OrderNumber %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBoxOrderNumberEdit" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="REQOrderNumberEdit" ControlToValidate="TextBoxOrderNumberEdit" Text="*" runat="server"></asp:RequiredFieldValidator>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:TextBox ID="TextBoxOrderNumberInsert" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="REQOrderNumberInsert" ControlToValidate="TextBoxOrderNumberInsert" Text="*" runat="server"></asp:RequiredFieldValidator>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelOrderNumber" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,ReferenceNumber %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBoxReferenceNumberEdit" runat="server" Text='<%# Bind("ReferenceNumber") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="REQReferenceNumberEdit" ControlToValidate="TextBoxReferenceNumberEdit" Text="*" runat="server"></asp:RequiredFieldValidator>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:TextBox ID="TextBoxReferenceNumberInsert" runat="server" Text='<%# Bind("ReferenceNumber") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="REQReferenceNumberInsert" ControlToValidate="TextBoxReferenceNumberInsert" Text="*" runat="server"></asp:RequiredFieldValidator>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelReferenceNumber" runat="server" Text='<%# Bind("ReferenceNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Default,DeliveryDate %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBoxDeliveryDateEdit" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeliveryDateInsert" runat="server" ControlToValidate="TextBoxDeliveryDateEdit" ErrorMessage="Please enter Delivery Date"></asp:RequiredFieldValidator>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:TextBox ID="TextBoxDeliveryDateInsert" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtenderFromDate" runat="server" Animated="true"
                                                        Format="<%$ Resources:Default,DateFormat %>" TargetControlID="TextBoxDeliveryDateInsert">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeliveryDateInsert" runat="server" ControlToValidate="TextBoxDeliveryDateInsert" ErrorMessage="Please enter Delivery Date"></asp:RequiredFieldValidator>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonChangeMode" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ButtonComplete" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <asp:Button ID="ButtonComplete" runat="server" Text="<%$ Resources:Default, Complete%>" OnClick="ButtonComplete_Click" />

                            <asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentUpdate"
                                runat="server"
                                SelectMethod="GetOutboundDocument"
                                TypeName="OutboundDocument"
                                UpdateMethod="UpdateOutboundDocument"
                                InsertMethod="CreateOutboundDocument"
                                DeleteMethod="DeleteOutboundDocument"
                                OnInserted="ObjectDataSourceOutboundDocumentUpdate_OnInserted">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Type="Int32" Name="OutboundDocumentId" SessionField="OutboundDocumentId"></asp:SessionParameter>
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:Parameter Name="OutboundDocumentId" Type="Int32"></asp:Parameter>
                                    <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32"></asp:Parameter>
                                    <asp:SessionParameter Name="ExternalCompanyId" SessionField="ExternalCompanyId" Type="Int32" />
                                    <asp:Parameter Name="OrderNumber" Type="String"></asp:Parameter>
                                    <asp:Parameter Name="DeliveryDate" Type="DateTime"></asp:Parameter>
                                </UpdateParameters>
                                <InsertParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                                    <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32"></asp:Parameter>
                                    <asp:SessionParameter Name="ExternalCompanyId" SessionField="ExternalCompanyId" Type="Int32" />
                                    <asp:Parameter Name="OrderNumber" Type="String"></asp:Parameter>
                                    <asp:Parameter Name="DeliveryDate" Type="DateTime"></asp:Parameter>
                                </InsertParameters>
                                <DeleteParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                                    <asp:Parameter Name="OutboundDocumentId" Type="Int32"></asp:Parameter>
                                </DeleteParameters>
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:Label ID="LabelErrorTab2" runat="server" EnableTheming="false" ForeColor="red"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonChangeMode" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ButtonComplete" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <br />
                            <asp:Panel ID="PanelSupplier" runat="server" GroupingText="<%$ Resources:Default, SelectSupplier%>">
                                <asp:UpdatePanel runat="server" ID="UpdatePanelExternalCompanySearch" RenderMode="Inline">
                                    <ContentTemplate>
                                        <uc2:ExternalCompanySearch ID="ExternalCompanySearch1" runat="server"></uc2:ExternalCompanySearch>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="Server" ID="TabPanel3" HeaderText="<%$ Resources:Default, Lines%>" Height="100%">
            <ContentTemplate>
                <ajaxToolkit:Accordion ID="AccordionLineInsert" runat="server" SelectedIndex="0"
                    HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
                    FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
                    SuppressHeaderPostbacks="true">
                    <Panes>
                        <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                            <Header>
                                <a href="" class="accordionLink">1. Select a Product</a>
                            </Header>
                            <Content>
                                <asp:UpdatePanel runat="server" ID="UpdatePanelProductSearch">
                                    <ContentTemplate>
                                        <uc4:ProductSearch ID="ProductSearch2" runat="server" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                        <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                            <Header><a href="" class="accordionLink">2. Select a Batch (Optional)</a></Header>
                            <Content>
                                <asp:UpdatePanel runat="server" ID="UpdatePanelBatchSearch2">
                                    <ContentTemplate>
                                        <uc3:BatchSearch ID="BatchSearch2" runat="server" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                        <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
                            <Header><a href="" class="accordionLink">3. Enter Quantity</a></Header>
                            <Content>
                                <asp:UpdatePanel runat="server" ID="UpdatePanelTextBoxQuantity">
                                    <ContentTemplate>
                                        <asp:Label ID="LabelQuantity" runat="server"></asp:Label>
                                        <asp:TextBox ID="TextBoxQuantity" runat="server"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Numbers" TargetControlID="TextBoxQuantity"></ajaxToolkit:FilteredTextBoxExtender>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                    </Panes>
                </ajaxToolkit:Accordion>
                <asp:Button ID="ButtonInsertLine" runat="server" Text="<%$ Resources:Default, Insert%>" OnClick="ButtonInsertLine_Click" />
                <asp:Button ID="ButtonDeleteLine" runat="server" Text="<%$ Resources:Default, Delete%>" OnClick="ButtonDeleteLine_Click" />
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtenderButtonDeleteLine" runat="server" ConfirmText="<%$ Resources:Default, PressOkDelete%>" TargetControlID="ButtonDeleteLine"></ajaxToolkit:ConfirmButtonExtender>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridViewOutboundLine">
                    <ContentTemplate>
                        <asp:Label ID="LabelErrorMsg" runat="server" Text="" EnableTheming="false" ForeColor="Red"></asp:Label>
                        <asp:GridView ID="GridViewOutboundLine" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                            AutoGenerateSelectButton="true" AutoGenerateEditButton="true" DataKeyNames="OutboundLineId"
                            DataSourceID="ObjectDataSourceOutboundLine">
                            <Columns>
                                <asp:BoundField DataField="ProductCode" ReadOnly="true" HeaderText="<%$ Resources:Default, ProductCode %>"></asp:BoundField>
                                <asp:BoundField DataField="Product" ReadOnly="true" HeaderText="<%$ Resources:Default, Product %>"></asp:BoundField>
                                <asp:BoundField DataField="SKUCode" ReadOnly="true" HeaderText="<%$ Resources:Default, SKUCode %>"></asp:BoundField>
                                <asp:BoundField DataField="Batch" ReadOnly="true" HeaderText="<%$ Resources:Default, Batch %>"></asp:BoundField>
                                <asp:BoundField DataField="ECLNumber" ReadOnly="true" HeaderText="<%$ Resources:Default, ECLNumber %>"></asp:BoundField>
                                <asp:BoundField DataField="Status" ReadOnly="true" HeaderText="<%$ Resources:Default, Status %>"></asp:BoundField>
                                <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ButtonInsertLine" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="ObjectDataSourceOutboundLine" runat="server" TypeName="OutboundDocument"
                    SelectMethod="SearchOutboundLine" UpdateMethod="UpdateOutboundLine" InsertMethod="CreateOutboundLine"
                    DeleteMethod="DeleteOutboundLine">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Type="Int32" Name="OutboundDocumentId" SessionField="OutboundDocumentId" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="OutboundLineId" Type="Int32"></asp:Parameter>
                        <asp:Parameter Name="Quantity" Type="Decimal"></asp:Parameter>
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
                        <asp:SessionParameter Name="OutboundDocumentId" SessionField="OutboundDocumentId" Type="Int32" />
                        <asp:SessionParameter Name="StorageUnitId" SessionField="StorageUnitId" Type="Int32" />
                        <asp:ControlParameter Name="Quantity" ControlID="TextBoxQuantity" Type="Decimal" />
                        <asp:SessionParameter Name="BatchId" SessionField="BatchId" Type="Int32" />
                    </InsertParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="OutboundLineId" Type="Int32"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</asp:Content>

