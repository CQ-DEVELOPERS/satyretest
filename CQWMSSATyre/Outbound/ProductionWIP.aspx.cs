using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Outbound_ProductionWIP : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"
    
    #region PRIVATE CONSTANTS
        //private const string DEFAULT_STATUS = "IS";
        StringBuilder strBuilder = new StringBuilder();
        private string result = "";
        private string theErrMethod = "";
    #endregion

    #region Page_Load
     protected void Page_Load(object sender, EventArgs e)
     {
         Session["countLoopsToPreventInfinLoop"] = 0;
         theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
                Session["JobId"] = null;
                
                Session["ProductCode"] = null;
                Session["Product"] = null;
                Session["Batch"] = null;

                ButtonAutoAllocation.Visible = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), 33);
            }

            if(Session["WarehouseId"] == null)
                Response.Redirect("~/Security/Login.aspx");

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
     }
    #endregion Page_Load

    #region ButtonOperatorRelease_Click
    protected void ButtonOperatorRelease_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonOperatorRelease_Click";
        try
        {
            int operatorId = -1;
            int index = GridViewInstruction.SelectedIndex;

            if (index != -1)
            {

                if (int.TryParse(DropDownListOperator.SelectedValue, out operatorId))
                {
                    Status status = new Status();

                    status.SetReleaseOperator(Session["ConnectionStringName"].ToString(),
                                                int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString()),
                                                int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString()),
                                                operatorId);

                    GridViewInstruction.DataBind();
                    Master.MsgText = "Release"; Master.ErrorText = "";
                }
            }
            
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "ButtonOperatorRelease_Click"

    #region ButtonSearch_Click
    /// <summary>
     /// 
     /// </summary>
     /// <param name="sender"></param>
     /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
     {
         theErrMethod = "ButtonSearch_Click";
         try
         {
             GridViewInstruction.DataBind();

             Master.MsgText = ""; Master.ErrorText = "";
         }
         catch (Exception ex)
         {
             result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString()); 
             Master.ErrorText = result;
         }
     }
    #endregion ButtonSearch_Click

    #region GridViewJobs_OnSelectedIndexChanged
    protected void GridViewJobs_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewJobs_OnSelectedIndexChanged";

        try
        {
            if (GridViewJobs.SelectedDataKey["JobId"].ToString() == "")
                Session["JobId"] = -1;
            else
                Session["JobId"] = int.Parse(GridViewJobs.SelectedDataKey["JobId"].ToString());

            GridViewJobs.DataBind();
            GridViewDetails.DataBind();

            Master.MsgText = "Selected"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GridViewJobs_OnSelectedIndexChanged"

    #region ButtonAutoLocations_Click
    protected void ButtonAutoLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            if (GridViewInstruction.SelectedIndex != -1)
            {
                OutboundWIP plan = new OutboundWIP();

                if (!plan.AutoLocationAllocateIssue(Session["ConnectionStringName"].ToString(), (int)GridViewInstruction.SelectedDataKey.Values["IssueId"]))
                {
                    Master.ErrorText = "Auto Allocation Not Complete";
                    return;
                }

                GridViewDetails.DataBind();

                Master.MsgText = ""; Master.ErrorText = "";
            }
            else
                Master.ErrorText = "Please select an Order.";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAutoLocations_Click  

    #region GetEditIndex
    protected void GetEditIndex()
    {
        theErrMethod = "GetEditIndex";
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewDetails.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if(cb.Checked)
                    rowList.Add(GridViewDetails.DataKeys[row.RowIndex].Values["InstructionId"]);
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "GetEditIndex:

    #region ButtonPalletise_Click
    protected void ButtonPalletise_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPalletise_Click";

        try
        {
            if (GridViewInstruction.SelectedIndex != -1)
            {
                OutboundWIP plan = new OutboundWIP();

                if (plan.Palletise(Session["ConnectionStringName"].ToString(), int.Parse(GridViewInstruction.DataKeys[GridViewInstruction.SelectedIndex].Values["IssueId"].ToString()), -1))
                {
                    GridViewInstruction.DataBind();
                    GridViewLineUpdate.DataBind();
                    GridViewDetails.DataBind();

                    Master.MsgText = "Palletised"; Master.ErrorText = "";
                }
                else
                {
                    Master.ErrorText = "Palletisation Failed";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonPalletise_Click

    #region ButtonManualLocation_Click
    protected void ButtonManualLocations_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManualLocations_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Outbound/ProductionWIP.aspx";
            Response.Redirect("~/Common/ManualLocationAllocation.aspx");

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
           result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString()); 
           Master.ErrorText = result;
        }
    }
    #endregion ButtonManualLocation_Click

    #region ButtonLink_Click
    protected void ButtonLink_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonLink_Click";

        try
        {
            GetEditIndex();
            Session["FromURL"] = "~/Outbound/ProductionWIP.aspx";
            Response.Redirect("~/Common/RegisterSerialNumberPallet.aspx");

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
    }
    #endregion ButtonLink_Click

    #region GridViewInstruction_RowDataBound
    protected void GridViewInstruction_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // This line will get the reference to the underlying row
            DataRowView _row = (DataRowView)e.Row.DataItem;
            if (_row != null)
            {
                switch (_row.Row["Status"].ToString())
                {
                    case "Pause":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.OrangeRed;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Planning Complete":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.RoyalBlue;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Release":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.Green;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Manual":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.LightSteelBlue;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Checking":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.SaddleBrown;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Quality Assurance":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                }
            }
        }
    }
    #endregion GridViewInstruction_RowDataBound

    #region GridViewInstruction_SelectedIndexChanged
    protected void GridViewInstruction_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "GridViewInstruction_SelectedIndexChanged";
        try
        {
            Session["OutboundShipmentId"] = GridViewInstruction.SelectedDataKey["OutboundShipmentId"];
            Session["IssueId"] = GridViewInstruction.SelectedDataKey["IssueId"];
            ObjectDataSourceOrderLines.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }

    }
    #endregion GridViewInstruction_SelectedIndexChanged

    #region ObjectDataSourceBatch_OnSelecting
    protected void ObjectDataSourceBatch_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceBatch_OnSelecting";
        try
        {
            e.InputParameters["storageUnitId"] = int.Parse(GridViewLineUpdate.SelectedDataKey["StorageUnitId"].ToString());

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ObjectDataSourceBatch_OnSelecting"
   
    #region ObjectDataSourceOrderLines_Selecting
    protected void ObjectDataSourceOrderLines_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        theErrMethod = "ObjectDataSourceOrderLines_Selecting";

        try
        {
            e.InputParameters["operatorId"] = (int)Session["OperatorId"];

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString()); 
            Master.ErrorText = result;
        }
    }
    #endregion ObjectDataSourceOrderLines_Selecting

    #region GridViewLineUpdate_SelectedIndexChanging
    protected void GridViewLineUpdate_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        theErrMethod = "GridViewLineUpdate_SelectedIndexChanging";

        try
        {
            Session["StorageUnitId"] = GridViewLineUpdate.DataKeys[1].Value;

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion GridViewLineUpdate_SelectedIndexChanging

    #region DropDownListBatch_OnSelectedIndexChanged
    protected void DropDownListBatch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "DropDownListBatch_OnSelectedIndexChanged";

        try
        {
            DropDownList dpList = (DropDownList)sender;
            Session["StorageUnitId"] = int.Parse(dpList.SelectedValue);

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "DropDownListBatch_OnSelectedIndexChanged"

    #region ButtonDeallocate_Click
    protected void ButtonDeallocate_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeallocate_Click";
        try
        {
            int index = GridViewInstruction.SelectedIndex;

            if (index != -1)
            {
                OutboundWIP wip = new OutboundWIP();

                int outboundShipmentId = int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString());
                int issueId = int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString());
                
                if (outboundShipmentId != -1)
                    issueId = -1;

                if (wip.PalletiseDeallocate(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId))
                {
                    Master.MsgText = "Reverse Successful"; Master.ErrorText = "";

                    GridViewInstruction.SelectedIndex = -1;
                    GridViewInstruction.DataBind();
                }
                else
                {
                    Master.MsgText = ""; Master.ErrorText = "Document cannot be reversed";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonDeallocate_Click

    #region ButtonAutoLocations2_Click
    protected void ButtonAutoLocations2_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonAutoLocations_Click";

        try
        {
            CheckBox cb = new CheckBox();
            OutboundWIP plan = new OutboundWIP();

            foreach (GridViewRow row in GridViewDetails.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                if (cb.Checked == true)
                    if (plan.AutoLocationAllocateInstruction(Session["ConnectionStringName"].ToString(), (int)GridViewDetails.DataKeys[row.RowIndex].Values["InstructionId"]) == false)
                    {
                        Master.ErrorText = "Auto Allocation Not Complete";
                        break;
                    }
            }

            GridViewDetails.DataBind();

            Master.MsgText = "Auto Locations"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonAutoLocations2_Click

    #region ButtonMix_Click
    protected void ButtonMix_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonMix_Click";
        try
        {
            Session["InstructionTypeCode"] = "PM";
            Response.Redirect("~/Common/MixedJobCreate.aspx");
            Master.MsgText = "Mix"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonMix_Click

    #region ButtonRelease_Click
    protected void ButtonRelease_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRelease_Click";
        try
        {
            StatusChange("RL");

            GridViewInstruction.DataBind();

            Master.MsgText = "Release"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonRelease_Click"

    #region ButtonManual_Click
    protected void ButtonManual_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonManual_Click";
        try
        {
            StatusChange("M");

            GridViewInstruction.DataBind();

            Master.MsgText = "Status Manual"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonManual_Click"

    #region ButtonReleaseNoStock_Click
    protected void ButtonReleaseNoStock_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReleaseNoStock_Click";
        try
        {
            int index = GridViewInstruction.SelectedIndex;

            if (index != -1)
            {
                OutboundWIP wip = new OutboundWIP();

                int outboundShipmentId = int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString());
                int issueId = int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString());
                int operatorId = (int)Session["OperatorId"];

                if (outboundShipmentId != -1)
                    issueId = -1;

                if (wip.ReleaseNoStock(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId, operatorId))
                {
                    Master.MsgText = "Re-release of no-stock's successful"; Master.ErrorText = "";

                    GridViewJobs.DataBind();
                }
                else
                {
                    Master.MsgText = ""; Master.ErrorText = "Re-release of no-stock's not complete";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonReleaseNoStock_Click"

    #region ButtonWavePlanning_Click
    protected void ButtonWavePlanning_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReleaseNoStock_Click";
        try
        {
            int index = GridViewInstruction.SelectedIndex;

            if (index != -1)
            {
                OutboundWIP wip = new OutboundWIP();

                int outboundShipmentId = int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString());
                int issueId = int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString());
                int operatorId = (int)Session["OperatorId"];

                if (outboundShipmentId != -1)
                    issueId = -1;

                if (wip.ReleaseWave(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId, operatorId))
                {
                    Master.MsgText = "Release of wave successful"; Master.ErrorText = "";

                    GridViewJobs.DataBind();
                }
                else
                {
                    Master.MsgText = ""; Master.ErrorText = "Release of wave unsuccessful";
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundInstructionMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonWavePlanning_Click

    #region StatusChange
    protected void StatusChange(string statusCode)
    {
        theErrMethod = "StatusChange";
        try
        {
            int index = GridViewInstruction.SelectedIndex;

            if (index != -1)
            {
                Status status = new Status();

                if (GridViewInstruction.DataKeys[index].Values["IssueId"].ToString() == "-1")
                    status.SetRelease(Session["ConnectionStringName"].ToString(),
                                      int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString()),
                                      -1,
                                      -1,
                                      statusCode);
                else
                    status.SetRelease(Session["ConnectionStringName"].ToString(),
                                      int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString()),
                                      int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString()),
                                      -1,
                                      statusCode);
            }

            Master.MsgText = "Status Change"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("LoadRelease" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }

    }
    #endregion "StatusChange"

    #region ButtonPause_Click
    protected void ButtonPause_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPause_Click";
        try
        {
            StatusChange("PS");

            GridViewInstruction.DataBind();

            Master.MsgText = "Release"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
#endregion "ButtonPause_Click"

    #region ButtonPrintJob_Click
    protected void ButtonPrintJob_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrintJob_Click";
        try
        {
            Session["FromURL"] = "~/Outbound/ProductionWIP.aspx";

            Session["ReportName"] = "Job Pick Sheet";

            ReportParameter[] RptParameters = new ReportParameter[4];

            // Create the JobId report parameter
            string jobId = Session["JobId"].ToString();
            RptParameters[0] = new ReportParameter("JobId", jobId);

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonPrintJob_Click"

    #region ButtonSelect_Click
    protected void ButtonSelect_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelect_Click";

        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");
                cb.Checked = true;
            }

            Master.MsgText = "Select"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSelect_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonPrint_Click";
        try
        {
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                    rowList.Add(GridViewJobs.DataKeys[row.RowIndex].Values["JobId"]);
            }

            if (rowList.Count > 0)
                Session["checkedList"] = rowList;

            Session["LabelName"] = "Despatch By Route Label.lbl";

            Session["FromURL"] = "~/Outbound/ProductionWIP.aspx";

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("~/Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion "ButtonPrint_Click"

    #region ButtonCheckStock_Click
    protected void ButtonCheckStock_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonCheckStock_Click";

        try
        {
            GridViewLineUpdate.DataBind();

            Master.MsgText = "Check Stock Successful";
            Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            Master.MsgText = "";
            Master.ErrorText = "Check Stock Failed";
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonCheckStock_Click

    #region ButtonReplenish_Click
    protected void ButtonReplenish_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonReplenish_Click";

        try
        {
            ProductionWIP pw = new ProductionWIP();

            if (pw.ReplenishmentRequest(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OutboundShipmentId"], (int)Session["IssueId"]))
            {
                GridViewLineUpdate.DataBind();

                Master.MsgText = "Replenishment Request Successful";
                Master.ErrorText = "";
            }
            else
                throw new Exception("ProcessStock(): Posting InventoryTransaction failed.");
        }
        catch (Exception ex)
        {
            Master.MsgText = "";
            Master.ErrorText = "Replenishment Request Successful Failed";
            result = SendErrorNow("ProductionWIP" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonReplenish_Click

    #region ButtonPrintLabel_Click
    protected void ButtonPrintLabel_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList checkedList = new ArrayList();
            CheckBox cb = new CheckBox();
            
            foreach (GridViewRow row in GridViewJobs.Rows)
            {
                cb = (CheckBox)row.FindControl("CheckBoxEdit");

                if (cb.Checked)
                {
                    checkedList.Add((int)GridViewJobs.DataKeys[row.RowIndex].Values["JobId"]);
                }
            }

            if (checkedList.Count < 1)
                return;

            Session["checkedList"] = checkedList;

            if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 144))
                Session["LabelName"] = "Picking Label Small.lbl";
            else
                Session["LabelName"] = "Picking Label Large.lbl";

            Session["FromURL"] = "~/Outbound/ProductionWIP.aspx";

            Session["LabelCopies"] = int.Parse(TextBoxQty.Text);

            if (Session["Printer"] == null)
                Session["Printer"] = "";

            if (Session["Printer"].ToString() == "")
                Response.Redirect("../Common/NLLabels.aspx");
            else
            {
                Session["Printing"] = true;
                Response.Redirect("~/Common/NLPrint.aspx");
            }
        }
        catch { }
    }
    #endregion ButtonPrintLabel_Click

    #region ButtonPrintBatchCard_Click
    protected void ButtonPrintBatchCard_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/BatchCards.aspx";

            Session["ReportName"] = "Batch Card Details";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[8];

            // Create the ServerName report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            string outboundShipmentId = Session["OutboundShipmentId"].ToString();

            if (outboundShipmentId == "")
                outboundShipmentId = "-1";

            RptParameters[3] = new ReportParameter("OutboundShipmentId", outboundShipmentId);

            // Create the ReceiptId report parameter
            string issueId = Session["IssueId"].ToString();

            if (issueId == "")
                issueId = "-1";

            RptParameters[4] = new ReportParameter("IssueId", issueId);

            //// Create the OrderNumber report parameter
            //RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("OrderNumber", Session["OrderNumber"].ToString());
            // Create the FromDate report parameter
            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
    #endregion ButtonPrintBatchCard_Click

    #region GridViewInstruction_RowCommand
    protected void GridViewInstruction_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        string currentCommand = e.CommandName;
        if (currentCommand == "Up" || currentCommand == "Down")
        {
            int index = Int32.Parse(e.CommandArgument.ToString());

            int outboundShipmentId = int.Parse(GridViewInstruction.DataKeys[index].Values["OutboundShipmentId"].ToString());
            int issueId = int.Parse(GridViewInstruction.DataKeys[index].Values["IssueId"].ToString());
            int rank = int.Parse(GridViewInstruction.DataKeys[index].Values["Rank"].ToString());

            ProductionPlan plan = new ProductionPlan();

            if (currentCommand == "Up")
                rank--;
            else
                rank++;

            if (plan.Update(Session["ConnectionStringName"].ToString(), issueId, rank))
            {
                Master.MsgText = "Rank update successful";
                Master.ErrorText = "";
            }
            else
            {
                Master.MsgText = "";
                Master.ErrorText = "There was an error";
            }

            GridViewInstruction.DataBind();
        }
    }
    #endregion GridViewInstruction_RowCommand

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "ProductionWIP", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "ProductionWIP", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
 #endregion ErrorHandling
}