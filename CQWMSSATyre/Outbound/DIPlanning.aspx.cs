using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;

public partial class Outbound_DIPlanning : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = null;
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["IssueId"] != null)
                {
                    int issueId = (int)Session["IssueId"];

                    foreach (GridDataItem item in RadGridOrders.Items)
                    {
                        theErrMethod = "Page_LoadComplete";
                        if (item.GetDataKeyValue("IssueId").ToString() == issueId.ToString())
                        {
                            RadGridOrders.SelectedIndexes.Add(item.ItemIndex);
                            break;
                        }
                    }

                    if (RadGridOrders.SelectedIndexes.Count == 0)
                    {
                        RadGridOrders.SelectedIndexes.Clear();
                        Session["OutboundShipmentId"] = null;
                        Session["IssueId"] = null;
                        Session["Jobid"] = null;

                        RadGridOrderLines.DataBind();
                    }
                }
            }
        }
        catch { }
    }
    #endregion Page_LoadComplete

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            RadGridOrders.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region RadGridOrders_RowDataBound
    protected void RadGridOrders_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // This line will get the reference to the underlying row
            DataRowView _row = (DataRowView)e.Row.DataItem;
            if (_row != null)
            {
                switch (_row.Row["Status"].ToString())
                {
                    case "Pause":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.OrangeRed;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Planning Complete":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.RoyalBlue;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Release":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.Green;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Manual":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.LightSteelBlue;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Checking":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.SaddleBrown;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                    case "Quality Assurance":
                        e.Row.Cells[17].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[17].Font.Bold = true;
                        break;
                }
            }
        }
    }
    #endregion RadGridOrders_RowDataBound

    #region RadGridOrders_SelectedIndexChanged
    protected void RadGridOrders_SelectedIndexChanged(object sender, EventArgs e)
    {
        theErrMethod = "RadGridOrders_SelectedIndexChanged";
        try
        {
            foreach (GridDataItem item in RadGridOrders.SelectedItems)
            {
                Session["OutboundShipmentId"] = item.GetDataKeyValue("OutboundShipmentId");
                Session["IssueId"] = item.GetDataKeyValue("IssueId");
                ObjectDataSourceOrderLines.DataBind();
                break;
            }
            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridOrders_SelectedIndexChanged


    #region ButtonWavePlanning_Click
    protected void ButtonWavePlanning_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonWavePlanning_Click";

        try
        {
            Response.Redirect("~/Outbound/WavePlanning.aspx");
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonWavePlanning_Click

    #region ButtonCombine_Click
    protected void ButtonCombine_Click(object sender, EventArgs e)
    {
        try
        {
            theErrMethod = "ButtonCombine_Click";

            Wave wav = new Wave();
            
            int waveId = -1;
            int outboundShipmentId = -1;
            int issueId = -1;

            waveId = wav.InsertWave(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"]);

            if (waveId > 0)
            {
                foreach (GridDataItem item in RadGridOrders.SelectedItems)
                {
                    outboundShipmentId = int.Parse(item.GetDataKeyValue("OutboundShipmentId").ToString());
                    issueId = int.Parse(item.GetDataKeyValue("IssueId").ToString());

                    if (!wav.Transfer(Session["ConnectionStringName"].ToString(), waveId, outboundShipmentId, issueId))
                    {
                        Master.MsgText = ""; Master.ErrorText = "There was an error adding to the wave";
                        break;
                    }
                }

                RadGridOrders.DataBind();
            }
        }
        catch (Exception ex)
        {
            Master.ErrorText = ex.Message.ToString();
        }
    }
    #endregion ButtonCombine_Click

    #region ButtonSplit_Click
    protected void ButtonSplit_Click(object sender, EventArgs e)
    {
        try
        {
            theErrMethod = "ButtonSplit_Click";

            Wave wav = new Wave();

            int outboundShipmentId = -1;
            int issueId = -1;

            foreach (GridDataItem item in RadGridOrders.SelectedItems)
            {
                outboundShipmentId = int.Parse(item.GetDataKeyValue("OutboundShipmentId").ToString());
                issueId = int.Parse(item.GetDataKeyValue("IssueId").ToString());

                if (!wav.Remove(Session["ConnectionStringName"].ToString(), outboundShipmentId, issueId))
                {
                    Master.MsgText = ""; Master.ErrorText = "There was an error removing from the Wave";
                    break;
                }
            }

            RadGridOrders.DataBind();
        }
        catch (Exception ex)
        {
            Master.ErrorText = ex.Message.ToString();
        }
    }
    #endregion ButtonSplit_Click

    #region RadGridOrders_ItemCommand
    protected void RadGridOrders_ItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            theErrMethod = "RadGridOrders_ItemCommand";

            Master.MsgText = "";
            Master.ErrorText = "";

            Wave w = new Wave();
            int waveId = -1;

            if (e.CommandName == RadGrid.SelectCommandName)
            {
                foreach (GridDataItem item in RadGridOrders.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        waveId = (int)item.GetDataKeyValue("WaveId");

                        if (w.ReleaseWave(Session["ConnectionStringName"].ToString(),
                                  waveId,
                                  (int)Session["OperatorId"]))
                            Master.MsgText = "Wave released";

                        RadGridOrders.DataBind();
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("OutboundShipmentLink" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
            RadGridOrders.DataBind();
        }
    }
    #endregion RadGridOrders_ItemCommand

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region Render
    protected override void Render(HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);

            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridOrders);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrders, "DIPlanningRadGridOrders", (int)Session["OperatorId"]);
            }
            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridOrderLines);
                settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrderLines, "DIPlanningRadGridOrderLines", (int)Session["OperatorId"]);
            }

        }
        catch (Exception ex)
        {
            result = SendErrorNow("DIPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Render

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            if (!Page.IsPostBack)
            {
                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridOrders);
                    settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrders, "DIPlanningRadGridOrders", (int)Session["OperatorId"]);
                }
                {
                    GridSettingsPersister settings = new GridSettingsPersister(RadGridOrderLines);
                    settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridOrderLines, "DIPlanningRadGridOrderLines", (int)Session["OperatorId"]);
                }

            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("DIPlanning" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Init
}