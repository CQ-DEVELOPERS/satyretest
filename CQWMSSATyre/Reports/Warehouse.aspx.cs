using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_Warehouse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MenuItemsDynamic ds = new MenuItemsDynamic();

            int operatorId;
            int menuId = 1;

            if (Session["OperatorId"] == null)
                operatorId = -1;
            else
                operatorId = (int)Session["OperatorId"];

            xmlDataSource.Data = ds.GetMenuItems(Session["ConnectionStringName"].ToString(), menuId, operatorId, "Desktop").GetXml();
            xmlDataSource.EnableCaching = false;
            xmlDataSource.DataBind();


            Visual vs = new Visual();

            xmlDataSourceReports.Data = vs.GetReports(Session["ConnectionStringName"].ToString()).ToString();
            xmlDataSourceReports.EnableCaching = false;
            xmlDataSourceReports.DataBind();
        }
    }
    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    {
       
    }
}
