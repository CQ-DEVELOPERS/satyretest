using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_InterfaceConsole : System.Web.UI.Page
{
    #region InitializeCultureform
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCultureform"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["StorageUnitId"] = -1;
                Session["ProductCode"] = "";
                Session["OrderNumber"] = "-1";
            }
        }
        catch { }
    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            string orderNumber = TextBoxOrderNumber.Text;
            if (orderNumber != "")
            {
                Session["FromURL"] = "~/Reports/InterfaceConsole.aspx";

                Session["ReportName"] = "Interface Console Order Track Summ";

                Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[4];
             
                // Create the StorageUnitId report parameter
                //string orderNumber = TextBoxOrderNumber.Text;

                if (orderNumber == "")
                    orderNumber = "-1";

                RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("OrderNumber", orderNumber);

                // Create the ServerName report parameter
                RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                // Create the DatabaseName report parameter
                RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                // Create the UserName report parameter
                RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                Session["ReportParameters"] = RptParameters;

                Response.Redirect("~/Reports/Report.aspx");
            }
            else
            {   
                if (Session["StorageUnitId"].ToString() != "-1")
                {
                    Session["FromURL"] = "~/Reports/InterfaceConsole.aspx";

                    Session["ReportName"] = "Interface Console Product Track";

                    Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[6];

                    // Create the ProductId report parameter
                    RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("StorageUnitId", Session["StorageUnitId"].ToString());

                    // Create the FromDate report parameter
                    RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("StartDate", Session["FromDate"].ToString());

                    // Create the ToDate report parameter
                    RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("EndDate", Session["ToDate"].ToString());

                    // Create the ServerName report parameter
                    RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                    // Create the DatabaseName report parameter
                    RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                    // Create the UserName report parameter
                    RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

                    Session["ReportParameters"] = RptParameters;

                    Response.Redirect("~/Reports/Report.aspx");
                    }
                    else
                    {
                    Session["FromURL"] = "~/Reports/InterfaceConsole.aspx";

                    Session["ReportName"] = "Interface Console";

                    Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[4];

                    // Create the FromDate report parameter
                    RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("StartDate", Session["FromDate"].ToString());

                    // Create the ToDate report parameter
                    RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("EndDate", Session["ToDate"].ToString());

                    // Create the ServerName report parameter
                    RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

                    // Create the DatabaseName report parameter
                    RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

                    Session["ReportParameters"] = RptParameters;

                    Response.Redirect("~/Reports/Report.aspx");
                    }
                }
            }
        catch { }
    }
}
