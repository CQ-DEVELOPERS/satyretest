﻿<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="InboundQuery.aspx.cs"
    Inherits="Reports_InboundQuery"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/InboundQuerySearch.ascx" TagName="InboundQuerySearch" TagPrefix="iqs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneInboundQuerySearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Search Criteria</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelInboundQuerySearch" runat="server">
                        <ContentTemplate>
                            <iqs:InboundQuerySearch ID="InboundQuerySearch" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>

        </Panes>
    </ajaxToolkit:Accordion>
    <asp:UpdatePanel ID="UpdatePanelMeasure" runat="server">
        <ContentTemplate>
            <table style="border: 2px 2px" border="1">

                <tr>
                    <td>Select Document Types to View
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="clear: left;">
                        </div>
                        <br />
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:CheckBoxList ID="CheckBoxListInboundDocumentType" runat="server" DataSourceID="ObjectDataSourceInboundDocumentTypeId"
                                    DataTextField="InboundDocumentType" DataValueField="InboundDocumentTypeId" RepeatColumns="4">
                                </asp:CheckBoxList>
                                <asp:Label ID="LabelErrorMsg" runat="server" Text="">
                                </asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonSelectInboundDocumentType" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ButtonDeselectInboundDocumentType" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="ButtonSelectInboundDocumentType" runat="server" Text="Select All" OnClick="ButtonSelectInboundDocumentType_Click" />
                        <asp:Button ID="ButtonDeselectInboundDocumentType" runat="server" Text="Deselect All" OnClick="ButtonDeselectInboundDocumentType_Click" />
                    </td>
                </tr>
                <tr>
                    <tr>
                        <td>Specify Date to be Used for Search
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px 1px">
                            <asp:RadioButtonList ID="RadioButtonDateSearch" runat="server" AutoPostBack="False" DataValueField="Sort"
                                Font-Size="Small">
                                <asp:ListItem Text="Create Date" Value="Create Date" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Planned Delivery Date" Value="Planned Delivery Date"></asp:ListItem>
                                <asp:ListItem Text="Delivery Date" Value="Delivery Date"></asp:ListItem>
                                <asp:ListItem Text="Received Date" Value="Received Date" ></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>

                </tr>
            </table>
            <asp:ObjectDataSource ID="ObjectDataSourceInboundDocumentTypeId" runat="server" SelectMethod="GetInboundDocumentTypes"
                TypeName="InboundDocumentType">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>

    </asp:UpdatePanel>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>


