using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_ShortPicks : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Session["StorageUnitId"] = -1;
            Session["BatchId"] = -1;
            Session["ExternalCompanyId"] = -1;
        }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/ShortPicks.aspx";

            Session["ReportName"] = "Short Picks";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[11];

            // Create the ConnectionString report parameter
            string strReportConnectionString = Session["ReportConnectionString"].ToString();
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ConnectionString", strReportConnectionString);

            // Create the WarehouseId report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            // Create the FromDate report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            // Create the OrderNumber report parameter
            string orderNumber = TextBoxOrderNumber.Text;

            if (orderNumber == "")
                orderNumber = "%";

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("OrderNumber", orderNumber);

            // Create the StorageUnitId report parameter
            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("StorageUnitId", Session["StorageUnitId"].ToString());

            // Create the BatchId report parameter
            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("BatchId", Session["BatchId"].ToString());

            // Create the ExternalCompanyId report parameter
            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("ExternalCompanyId", Session["ExternalCompanyId"].ToString());

            // Create the ServerName report parameter
            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[9] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[10] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
}
