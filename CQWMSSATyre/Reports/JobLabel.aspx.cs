using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_JobLabel : System.Web.UI.Page
{
    #region InitializeCulture

    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());

                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch (Exception)
        {
            // ignored
        }
    }

    #endregion InitializeCulture

    #region Page_Load

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["JobId"] != null)
            {
                ArrayList checkedList = new ArrayList();

                int jobId;

                if (int.TryParse(Request.QueryString["JobId"], out jobId))
                {
                    TextBoxJob.Text = @"J:" + jobId;

                    checkedList.Add(jobId);

                    Session["checkedList"] = checkedList;

                    Session["PrintLabel"] = false;

                    Session["LabelName"] = "Despatch By Order Label Plumblink.lbl";
                    //Session["LabelName"] = "Despatch By Order Label.lbl";

                    Session["FromURL"] = "~/Reports/JobLabel.aspx";

                    if (Session["Printer"] == null)
                        Session["Printer"] = string.Empty;

                    if (Session["Printer"].ToString() == string.Empty)
                        Response.Redirect("~/Common/NLLabels.aspx");
                    else
                    {
                        Session["Printing"] = true;

                        Response.Redirect("~/Common/NLPrint.aspx");
                    }
                }
            }
            else
                ClientScript.RegisterStartupScript(typeof (Page), "closePage", "window.close();", true);
        }
        catch (Exception)
        {
            // ignored
        }
    }

    #endregion Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["JobId"] != null)
            {
                ArrayList checkedList = new ArrayList();
                int jobId = -1;

                if (int.TryParse(Request.QueryString["JobId"], out jobId))
                {
                    TextBoxJob.Text = "J:" + jobId.ToString();

                    checkedList.Add(jobId);

                    Session["checkedList"] = checkedList;

                    Session["PrintLabel"] = false;

					Session["LabelName"] = "Despatch By Order Label Plumblink.lbl";
					//Session["LabelName"] = "Despatch By Order Label.lbl";

                    Session["FromURL"] = "~/Reports/JobLabel.aspx";

                    if (Session["Printer"] == null)
                        Session["Printer"] = "";

                    if (Session["Printer"].ToString() == "")
                        Response.Redirect("~/Common/NLLabels.aspx");
                    else
                    {
                        Session["Printing"] = true;
                        Response.Redirect("~/Common/NLPrint.aspx");
                    }
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }
        catch { }
    }
    #endregion Page_Load
}