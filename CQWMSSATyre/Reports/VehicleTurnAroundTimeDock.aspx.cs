using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;

public partial class Reports_VehicleTurnAroundTimeDock : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = HttpContext.Current.Request.Url;
            Session["ReportName"] = "Vehicle Turn Around Time Dock";

            // Create the report parameters holder
            var reportParameters = new List<ReportParameter>();

            // Create the report parameter
            reportParameters.Add(new ReportParameter("ServerName", Session["ServerName"].ToString()));
            reportParameters.Add(new ReportParameter("DatabaseName", Session["DatabaseName"].ToString()));
            reportParameters.Add(new ReportParameter("UserName", Session["UserName"].ToString()));
            reportParameters.Add(new ReportParameter("WarehouseId", Session["WarehouseId"].ToString()));
            reportParameters.Add(new ReportParameter("ExternalCompanyId", Session["ExternalCompanyId"].ToString()));
            reportParameters.Add(new ReportParameter("LocationId", Session["LocationId"].ToString()));
            reportParameters.Add(new ReportParameter("FromDate", Session["FromDate"].ToString()));
            reportParameters.Add(new ReportParameter("ToDate", Session["ToDate"].ToString()));
            
            Session["ReportParameters"] = reportParameters.ToArray();

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
}
