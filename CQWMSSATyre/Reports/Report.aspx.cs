using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using System.Net;
using System.Security.Principal;
using System.Runtime.InteropServices;

public partial class Reports_Report : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Page_PreInit
    private void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["Blank"] != null)
            this.MasterPageFile = "~/MasterPages/Empty.master";
    }
    #endregion Page_PreInit

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["Blank"] != null)
                {
                    Session["Blank"] = null;
                    ButtonBack.Visible = false;
                    but_CreateSubscription.Visible = false;
                }

                // Set the processing mode for the ReportViewer to Remote
               ReportViewer1.ProcessingMode = ProcessingMode.Remote;

                // Do not prompt user for Parameters
               ReportViewer1.ShowParameterPrompts = false;

                ReportViewerCredentials rvc = new ReportViewerCredentials(  ConfigurationManager.AppSettings.Get("UserName").ToString(),
                                                                            ConfigurationManager.AppSettings.Get("Password").ToString(),
                                                                            ConfigurationManager.AppSettings.Get("Domain").ToString());

                ReportViewer1.ServerReport.ReportServerCredentials = rvc;

                // Set the report server URL and report path
                string reportServerUrl = ConfigurationManager.AppSettings.Get("ReportServerUrl").ToString();
                this.ReportViewer1.ServerReport.ReportServerUrl = new System.Uri(reportServerUrl);

                string strReportPath = ConfigurationManager.AppSettings.Get("ReportPath").ToString();
                string strReportName = Session["ReportName"].ToString();
                string strReport = strReportPath + strReportName;
                this.ReportViewer1.ServerReport.ReportPath = strReport;

                ReportParameter[] RptParameters = (ReportParameter[])Session["ReportParameters"];

                // Set the report parameters for the report
                ReportViewer1.ServerReport.SetParameters(RptParameters);

                // Display the Report
                this.ReportViewer1.ServerReport.Refresh();

                //if(Session["AutoPrint"] != null)
                //    if((bool)Session["AutoPrint"])

                //LabelMessage.Text = ConfigurationManager.AppSettings.Get("UserName").ToString() + " " + ConfigurationManager.AppSettings.Get("Password").ToString() + " " + ConfigurationManager.AppSettings.Get("Domain").ToString();
                //LabelMessage.Text = LabelMessage.Text + " " + Session["ServerName"].ToString() + " " + Session["DatabaseName"].ToString() + " " + Session["UserName"].ToString();
            }
        }
        catch (Exception ex)
        {
            LabelError.Text = ex.Message.ToString();
        }
    }
    #endregion Page_Load

    #region ButtonBack_Click
    protected void ButtonBack_Click(object sender, EventArgs e)
    {
        if (Session["FromURL"] != null)
            if (Session["FromURL"] == "~/Reports/Report.aspx")
                Response.Redirect("~/Outbound/ProductChecking.aspx");
        else
            Response.Redirect(Session["FromURL"].ToString());
    }
    #endregion ButtonBack_Click

    //protected void Button1_Click(object sender, EventArgs e)
    //{
    //    Warning[] warnings;
    //    string[] streamids;
    //    string mimeType;
    //    string encoding;
    //    string extension;
    //    //string deviceInfo;

    //    //deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>";

    //    byte[] bytes = ReportViewer1.ServerReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);

    //    System.IO.FileStream fs = new System.IO.FileStream(@"C:\Users\Grant Schultz\Desktop\output.pdf", System.IO.FileMode.Create);
    //    fs.Write(bytes, 0, bytes.Length);
    //    fs.Close();
    //}

    #region ReportViewerCredentials
    public class ReportViewerCredentials : IReportServerCredentials
    {
        [DllImport("advapi32.dll", SetLastError = true)]
        public extern static bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public extern static bool DuplicateToken(IntPtr ExistingTokenHandle,
            int SECURITY_IMPERSONATION_LEVEL, ref IntPtr DuplicateTokenHandle);

        public ReportViewerCredentials()
        {
        }

        public ReportViewerCredentials(string username)
        {
            this.Username = username;
        }


        public ReportViewerCredentials(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }


        public ReportViewerCredentials(string username, string password, string domain)
        {
            this.Username = username;
            this.Password = password;
            this.Domain = domain;
        }


        public string Username
        {
            get
            {
                return this.username;
            }
            set
            {
                string username = value;
                if (username.Contains("\\"))
                {
                    this.domain = username.Substring(0, username.IndexOf("\\"));
                    this.username = username.Substring(username.IndexOf("\\") + 1);
                }
                else
                {
                    this.username = username;
                }
            }
        }
        private string username;



        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }
        private string password;


        public string Domain
        {
            get
            {
                return this.domain;
            }
            set
            {
                this.domain = value;
            }
        }
        private string domain;




        #region IReportServerCredentials Members

        public bool GetBasicCredentials(out string basicUser, out string basicPassword, out string basicDomain)
        {
            basicUser = username;
            basicPassword = password;
            basicDomain = domain;
            return username != null && password != null && domain != null;
        }

        public bool GetFormsCredentials(out string formsUser, out string formsPassword, out string formsAuthority)
        {
            formsUser = username;
            formsPassword = password;
            formsAuthority = domain;
            return username != null && password != null && domain != null;

        }

        public bool GetFormsCredentials(out Cookie authCookie,
      out string user, out string password, out string authority)
        {
            authCookie = null;
            user = password = authority = null;
            return false;  // Not implemented
        }


        public WindowsIdentity ImpersonationUser
        {
            get
            {

                string[] args = new string[3] { this.Domain.ToString(), this.Username.ToString(), this.Password.ToString() };
                IntPtr tokenHandle = new IntPtr(0);
                IntPtr dupeTokenHandle = new IntPtr(0);

                const int LOGON32_PROVIDER_DEFAULT = 0;
                //This parameter causes LogonUser to create a primary token.
                const int LOGON32_LOGON_INTERACTIVE = 2;
                const int SecurityImpersonation = 2;

                tokenHandle = IntPtr.Zero;
                dupeTokenHandle = IntPtr.Zero;
                try
                {
                    // Call LogonUser to obtain an handle to an access token.
                    bool returnValue = LogonUser(args[1], args[0], args[2],
                        LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT,
                        ref tokenHandle);

                    if (false == returnValue)
                    {
                        Console.WriteLine("LogonUser failed with error code : {0}",
                            Marshal.GetLastWin32Error());
                        return null;
                    }

                    // Check the identity.
                    System.Diagnostics.Trace.WriteLine("Before impersonation: "
                        + WindowsIdentity.GetCurrent().Name);


                    bool retVal = DuplicateToken(tokenHandle, SecurityImpersonation, ref dupeTokenHandle);
                    if (false == retVal)
                    {
                        CloseHandle(tokenHandle);
                        Console.WriteLine("Exception in token duplication.");
                        return null;
                    }


                    // The token that is passed to the following constructor must 
                    // be a primary token to impersonate.
                    WindowsIdentity newId = new WindowsIdentity(dupeTokenHandle);
                    WindowsImpersonationContext impersonatedUser = newId.Impersonate();


                    // Free the tokens.
                    if (tokenHandle != IntPtr.Zero)
                        CloseHandle(tokenHandle);
                    if (dupeTokenHandle != IntPtr.Zero)
                        CloseHandle(dupeTokenHandle);

                    // Check the identity.
                    System.Diagnostics.Trace.WriteLine("After impersonation: "
                        + WindowsIdentity.GetCurrent().Name);

                    return newId;

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception occurred. " + ex.Message);
                }

                return null;
            }
        }

        public ICredentials NetworkCredentials
        {
            get
            {
                return null;  // Not using NetworkCredentials to authenticate.
            }
        }


        #endregion
    }
    #endregion ReportViewerCredentials
    protected void but_CreateSubscription_Click(object sender, EventArgs e)
    {
        Response.Redirect("ReportSubscription.aspx");
    }
}
