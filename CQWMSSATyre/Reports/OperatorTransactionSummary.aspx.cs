﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_OperatorTransactionSummary : System.Web.UI.Page
{
    #region InitializeCultureform
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCultureform"

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/OperatorTransactionSummary.aspx";

            Session["ReportName"] = "Operator Transaction Summary";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[5];

            // Create the ServerName report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            // Create the FromDate report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
}
