<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="ProductMovementSummary.aspx.cs"
    Inherits="Reports_ProductMovementSummary"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/ProductIdSearch.ascx" TagName="ProductIdSearch" TagPrefix="ucprd" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/BatchSearchProduct.ascx" TagName="BatchSearchProduct" TagPrefix="ucbtch" %>
<%@ Register Src="../Common/DateRange3.ascx" TagName="DateRange" TagPrefix="ucdr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPanePrincipal" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Principal</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPrincipal" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                DataTextField="Principal" DataValueField="PrincipalId">
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                SelectMethod="GetPrincipalParameter">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneProductSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Product</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelProductId" runat="server">
                        <ContentTemplate>
                            <ucprd:ProductIdSearch ID="ProductIdSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneBatch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Batch</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelBatch" runat="server">
                        <ContentTemplate>
                            <ucbtch:BatchSearchProduct ID="BatchSearchProduct1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>

