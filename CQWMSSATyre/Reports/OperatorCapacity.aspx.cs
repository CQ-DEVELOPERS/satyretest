using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_OperatorCapacity : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try 
        {
             Session["FromURL"] = "~/Reports/OperatorCapacity.aspx";

            Session["ReportName"] = "Operator Performance Capacity Report";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[9];

            // Create the WarehouseId report parameter
            
            // Create the FromDate report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("StartDate", Session["FromDate"].ToString());

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("EndDate", Session["ToDate"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("OperatorGroupId", DD_OperatorGroupId.SelectedValue.ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("OperatorId","-1");  //DD_OperatorGroupId.SelectedValue.ToString());


            // Create the UOM report parameter
            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UOM", rblUOM.SelectedValue.ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());
            
            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
}
