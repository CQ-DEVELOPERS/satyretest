using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Telerik.Web.UI;
using Telerik.Web;
using System.IO;

public partial class Reports_ViewStockAdjustments : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
    //private const string DEFAULT_STATUS = "IS";
    StringBuilder strBuilder = new StringBuilder();
    private string result = "";
    private string theErrMethod = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page_Load";

        try
        {
            if (!Page.IsPostBack)
            {
                if (Session["MenuId"] == null)
                    Session["MenuId"] = 1;

                DateTime fromDate = DateRange.GetFromDate();
                DateTime toDate = DateRange.GetToDate().AddDays(1);

                rdpFromDate.SelectedDate = fromDate;
                rdpToDate.SelectedDate = toDate;

                RadGridInterfaceConsole.DataBind();
            }

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Load

    #region Page_LoadComplete
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_LoadComplete

    #region ButtonSearch_Click
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSearch_Click";
        try
        {
            RadGridInterfaceConsole.DataBind();

            Master.MsgText = ""; Master.ErrorText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion ButtonSearch_Click

    #region RadGridInterfaceConsole_ItemCommand
    protected void RadGridInterfaceConsole_ItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            Master.MsgText = "";
            Master.ErrorText = "";

            if (e.CommandName == "Reprocess")
            {
                InterfaceConsole ic = new InterfaceConsole();

                foreach (GridDataItem item in RadGridInterfaceConsole.Items)
                {
                    if (item.RowIndex == e.Item.RowIndex)
                    {
                        Session["InterfaceId"] = item.GetDataKeyValue("InterfaceId");
                        Session["InterfaceTableId"] = item.GetDataKeyValue("InterfaceTableId");

                        if (ic.Reprocess(Session["ConnectionStringName"].ToString(), (int)Session["InterfaceId"], (int)Session["InterfaceTableId"]))
                        {
                            RadGridInterfaceConsole.DataBind();
                            Master.MsgText = Resources.Default.Successful;
                        }
                        else
                            Master.ErrorText = "Error";

                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PlanningMaintenance" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion RadGridInterfaceConsole_ItemCommand

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        try
        {
            if (Session["countLoopsToPreventInfinLoop"] == null)
                Session["countLoopsToPreventInfinLoop"] = 0;
            
                int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PlanningMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;
        }
        catch (Exception exMsg)
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PlanningMaintenance", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }
        }

        return result;
    }
    #endregion ErrorHandling

    #region Render
    protected override void Render(HtmlTextWriter writer)
    {
        try
        {
            base.Render(writer);

            GridSettingsPersister settings = new GridSettingsPersister(RadGridInterfaceConsole);
            settings.SaveGridSettings(Session["ConnectionStringname"].ToString(), RadGridInterfaceConsole, "ReportsRadGridInterfaceConsole", (int)Session["OperatorId"]);
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ReceivingDocument" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Render

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                GridSettingsPersister settings = new GridSettingsPersister(RadGridInterfaceConsole);
                settings.LoadGridSettings(Session["ConnectionStringname"].ToString(), RadGridInterfaceConsole, "ReportsRadGridInterfaceConsole", (int)Session["OperatorId"]);
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("ViewStockAdjustment" + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    #endregion Page_Init
}