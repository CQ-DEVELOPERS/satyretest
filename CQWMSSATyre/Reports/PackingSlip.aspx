﻿<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="PackingSlip.aspx.cs"
    Inherits="Reports_PackingSlip"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/OutboundSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Label ID="LabelJob" runat="server" Text="<%$ Resources:Default, JobId %>"></asp:Label>
    <asp:TextBox ID="TextBoxJob" runat="server"></asp:TextBox>
    <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" FilterType="Numbers" TargetControlID="TextBoxJob"></ajaxToolkit:FilteredTextBoxExtender>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>

