using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using System.Diagnostics;
using System.Text;
using System.Collections.Generic;

public partial class Reports_StockOnHandByPrice : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PRIVATE CONSTANTS
        //private const string DEFAULT_STATUS = "IS";
        StringBuilder strBuilder = new StringBuilder();
        //private string result = "";
        private string theErrMethod = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["StorageUnitId"] = -1;
                Session["BatchId"] = -1;
                Session["FromLocationId"] = -1;
                Session["ToLocationId"] = -1;
                Session["LocationTypeId"] = -1;
                Session["DivisionId"] = -1;
            }
        }
        catch { }
    }

    #region ButtonSelectDivision_Click
    protected void ButtonSelectDivision_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonSelectDivision_Click";
        try
        {
            foreach (ListItem item in CheckBoxDivision.Items)
            {
                item.Selected = true;
            }            
        }
        catch (Exception ex)
        {           
        }

    }
    #endregion ButtonSelectDivision_Click

    #region ButtonDeselectDivision_Click
    protected void ButtonDeselectDivision_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonDeselectDivision_Click";

        try
        {
            foreach (ListItem item in CheckBoxDivision.Items)
            {
                item.Selected = false;
            }            
        }
        catch (Exception ex)
        {           
        }

    }
    #endregion ButtonDeselectLocationUnsel_Click


    protected void ButtonPrint_Click(object sender, EventArgs e)
    {

        try
            {
            List<string> divisionList = new List<string>();

            foreach (ListItem item in CheckBoxDivision.Items)
            {
                if (item.Selected == true)
                {
                    divisionList.Add(item.Value);
                }
            }

            string dvlist = String.Join(",", divisionList.ToArray());
            Session["DivisionList"] = "'" + dvlist + "'";


            //CheckBoxDivision.DataBind();            
        }
        catch (Exception ex)
        {
        }
        try
        {
            Session["FromURL"] = "~/Reports/StockOnHandByPrice.aspx";
            Session["ReportName"] = "Stock On Hand By Price";
            ReportParameter[] RptParameters = new ReportParameter[17];

            // Create the ConnectionString report parameter
            RptParameters[0] = new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString());
            // Create the WarehouseId report parameter
            RptParameters[1] = new ReportParameter("WarehouseId", Session["WarehouseId"].ToString());
            // Create the StorageUnitId report parameter
            RptParameters[2] = new ReportParameter("StorageUnitId", Session["StorageUnitId"].ToString());
            // Create the BatchId report parameter
            RptParameters[3] = new ReportParameter("BatchId", Session["BatchId"].ToString());
            // Create the StoreLocationId report parameter
            RptParameters[4] = new ReportParameter("StoreLocationId", Session["FromLocationId"].ToString());
            // Create the PickLocationId report parameter
            RptParameters[5] = new ReportParameter("PickLocationId", Session["ToLocationId"].ToString());
            // Create the LocationTypeId report parameter
            RptParameters[6] = new ReportParameter("LocationTypeId", Session["LocationTypeId"].ToString());
            // Create the DivisionList report parameter
            RptParameters[7] = new ReportParameter("DivisionList", Session["DivisionList"].ToString());
        
            // Create the BatchMore report parameter
            int batchMore = -1;

            if (rbBatchMore.Checked)
                batchMore = 1;

            if (rbBatchLess.Checked)
                batchMore = 0;

            RptParameters[8] = new ReportParameter("BatchMore", batchMore.ToString());

            // Create the BatchQuantity report parameter
            RptParameters[9] = new ReportParameter("BatchQuantity", tbBatchQuantity.Text);

            // Create the AllocatedMore report parameter
            int allocatedMore = -1;

            if (rbAllocatedMore.Checked)
                allocatedMore = 1;

            if (rbAllocatedLess.Checked)
                allocatedMore = 0;

            RptParameters[10] = new ReportParameter("AllocatedMore", allocatedMore.ToString());

            // Create the AllocatedQuantity report parameter
            RptParameters[11] = new ReportParameter("AllocatedQuantity", tbAllocatedQuantity.Text);

            // Create the ReservedMore report parameter
            int reservedMore = -1;

            if (rbReservedMore.Checked)
                reservedMore = 1;

            if (rbReservedLess.Checked)
                reservedMore = 0;

            RptParameters[12] = new ReportParameter("ReservedMore", reservedMore.ToString());

            // Create the ReservedQuantity report parameter
            RptParameters[13] = new ReportParameter("ReservedQuantity", tbReservedQuantity.Text);

            RptParameters[14] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[15] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[16] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch (Exception ex) { Debug.Write(ex.Message); }
    }
}
