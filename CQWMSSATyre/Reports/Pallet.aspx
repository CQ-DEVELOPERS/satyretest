<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="Pallet.aspx.cs" Inherits="Reports_Pallet" Title="<%$ Resources:Default, ReportTitle %>" Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneProductSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Pallet</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelOrderNumber" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelOrderNumber" runat="server" Text="Pallet Id:"></asp:Label>
                            <asp:Textbox ID="txt_PalletId" runat="server"></asp:Textbox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBE1" runat="server" FilterType="Numbers" TargetControlID="txt_PalletId"></ajaxToolkit:FilteredTextBoxExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
       </Panes>
       </ajaxToolkit:Accordion>
      
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click"/>

</asp:Content>

