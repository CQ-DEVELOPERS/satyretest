using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_ProductionTransfer : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["StorageUnitId"] = -1;
                Session["BatchId"] = -1;
                Session["ProductId"] = -1;
                Session["OrderNumber"] = "-1";
                Session["StatusId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/ProductionTransfer.aspx";

            Session["ReportName"] = "Production Transfer Detail";

            ReportParameter[] RptParameters = new ReportParameter[12];

            // Create the ConnectionString report parameter
            RptParameters[0] = new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString());

            // Create the WarehouseId report parameter
            RptParameters[1] = new ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            // Create the Sort report parameter
            string DateSel = RadioButtonList1.SelectedValue;

            // Create the StorageUnitId report parameter
            string orderNumber = TextBoxOrderNumber.Text;

            if (orderNumber == "")
                orderNumber = "-1";

            RptParameters[2] = new ReportParameter("OrderNumber", orderNumber);

            // Create the ProductId report parameter
            RptParameters[3] = new ReportParameter("StorageUnitId", Session["StorageUnitId"].ToString());

            // Create the BatchId report parameter
            RptParameters[4] = new ReportParameter("BatchId", Session["BatchId"].ToString());

            // Create the StatusId report parameter
            RptParameters[5] = new ReportParameter("StatusId", Session["StatusId"].ToString());

            // Create the FromDate report parameter
            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            RptParameters[8] = new ReportParameter("DateSel", DateSel.ToString());

            RptParameters[9] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[10] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[11] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
}
