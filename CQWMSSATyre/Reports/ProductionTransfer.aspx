<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductionTransfer.aspx.cs" Inherits="Reports_ProductionTransfer" Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/BatchSearch.ascx" TagName="BatchSearch" TagPrefix="ucbtch" %>
<%@ Register Src="../Common/ProductSearch.ascx" TagName="ProductSearch" TagPrefix="ucprd" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>
<%@ Register Src="../Common/StatusSearch.ascx" TagName="StatusSearch" TagPrefix="ucss" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0" HeaderCssClass="accordionHeader"
        ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40"
        TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneOrderSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Track an Order Number</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelOrderNumber" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelOrderNumber" runat="server" Text="Order Number:"></asp:Label>
                            <asp:TextBox ID="TextBoxOrderNumber" runat="server"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneProductSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Track a Product</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelProduct" runat="server">
                        <ContentTemplate>
                            <ucprd:ProductSearch ID="ProductSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneBatch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Batch</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelBatch" runat="server">
                        <ContentTemplate>
                            <ucbtch:BatchSearch ID="BatchSearch1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanelStatusSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Status</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelStatus" runat="server">
                        <ContentTemplate>
                            <ucss:StatusSearch ID="StatusSearch" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <table style="border: 2px 2px" border="1">
                <tr>
                    <th>
                        Select Date Option
                    </th>
                </tr>
                <tr>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="False" DataValueField="DateSel"
                            Font-Size="Small">
                            <asp:ListItem Text="Production Date" Value="P" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Warehouse Date" Value="W"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
        OnClick="ButtonPrint_Click" />
</asp:Content>
