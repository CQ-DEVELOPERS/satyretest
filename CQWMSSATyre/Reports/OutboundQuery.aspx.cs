using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_OutboundQuery : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["OutboundShipmentId"] = null;
                Session["IssueId"] = -1;
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region ButtonSearchShipment_Click
    protected void ButtonSearchShipment_Click(object sender, EventArgs e)
    {
        try
        {
            GridView2.DataBind();
        }
        catch { }
    }
    #endregion ButtonSearchShipment_Click

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GridView1.DataBind();
        }
        catch { }
    }
    #endregion ButtonSearch_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/OutboundQuery.aspx";

            Session["ReportName"] = "Outbound Query";

            ReportParameter[] RptParameters = new ReportParameter[9];

            // Create the OutboundShipmentId report parameter
            RptParameters[0] = new ReportParameter("OutboundShipmentId", Session["OutboundShipmentId"].ToString());

            // Create the IssueId report parameter
            RptParameters[1] = new ReportParameter("IssueId", Session["IssueId"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("Barcode", TextBoxBarcode.Text);

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            if (rblSummary.SelectedValue.ToString() == "true")
            {
                Session["ReportName"] = "Outbound Query Summary";
                Session["Bool"] = "true";
            }
            else
            {
                if (rblSummary.SelectedValue.ToString() == "false")
                {
                    Session["ReportName"] = "Outbound Query";
                    Session["Bool"] = "false";
                }
                else
                {
                    Session["ReportName"] = "Outbound Query Order Report";
                    Session["Bool"] = "order";
                }
            }

            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("Summary", Session["Bool"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
    #endregion ButtonPrint_Click

    #region ButtonPrintJob_Click
    protected void ButtonPrintJob_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/OutboundQuery.aspx";

            Session["ReportName"] = "Outbound Query";

            ReportParameter[] RptParameters = new ReportParameter[9];

            // Create the OutboundShipmentId report parameter
            RptParameters[0] = new ReportParameter("OutboundShipmentId", "-1");

            // Create the IssueId report parameter
            RptParameters[1] = new ReportParameter("IssueId", "-1");

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("Barcode", TextBoxBarcode.Text);

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            if (rblSummary.SelectedValue.ToString() == "true")
            {
                Session["ReportName"] = "Outbound Query Summary";
                Session["Bool"] = "true";
            }
            else
            {
                if (rblSummary.SelectedValue.ToString() == "false")
                {
                    Session["ReportName"] = "Outbound Query";
                    Session["Bool"] = "false";
                }
                else
                {
                    Session["ReportName"] = "Outbound Query Order Report";
                    Session["Bool"] = "order";
                }
            }

            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("Summary", Session["Bool"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
    #endregion ButtonPrintJob_Click

    #region ButtonPrintDate_Click
    protected void ButtonPrintDate_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/OutboundQuery.aspx";

            ReportParameter[] RptParameters = new ReportParameter[9];

            // Create the OutboundShipmentId report parameter
            RptParameters[0] = new ReportParameter("OutboundShipmentId", "-1");

            // Create the IssueId report parameter
            RptParameters[1] = new ReportParameter("IssueId", "-1");

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("Barcode", "-1");

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            if (rblSummary.SelectedValue.ToString() == "true")
            {
                Session["ReportName"] = "Outbound Query Summary";
                Session["Bool"] = "true";
            }
            else
            {
                if (rblSummary.SelectedValue.ToString() == "false")
                {
                    Session["ReportName"] = "Outbound Query";
                    Session["Bool"] = "false";
                }
                else
                {
                    Session["ReportName"] = "Outbound Query Order Report";
                    Session["Bool"] = "order";
                }
            }

            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("Summary", Session["Bool"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
    #endregion ButtonPrintDate_Click

    #region ButtonPrintShipment_Click
    protected void ButtonPrintShipment_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/OutboundQuery.aspx";

            Session["ReportName"] = "Outbound Query";

            ReportParameter[] RptParameters = new ReportParameter[9];

            // Create the OutboundShipmentId report parameter
            RptParameters[0] = new ReportParameter("OutboundShipmentId", Session["OutboundShipmentId"].ToString());

            // Create the IssueId report parameter
            RptParameters[1] = new ReportParameter("IssueId", Session["IssueId"].ToString());

            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("Barcode", TextBoxBarcode.Text);

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            if (rblSummary.SelectedValue.ToString() == "true")
            {
                Session["ReportName"] = "Outbound Query Summary";
                Session["Bool"] = "true";
            }
            else
            {
                if (rblSummary.SelectedValue.ToString() == "false")
                {
                    Session["ReportName"] = "Outbound Query";
                    Session["Bool"] = "false";
                }
                else
                {
                    Session["ReportName"] = "Outbound Query Order Report";
                    Session["Bool"] = "order";
                }
            }

            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("Summary", Session["Bool"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
    #endregion ButtonPrintShipment_Click

    #region GridView1_SelectedIndexChanged
    protected void GridView1_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try
        {
            Session["OutboundShipmentId"] = GridView1.SelectedDataKey["OutboundShipmentId"].ToString();
            Session["IssueId"] = GridView1.SelectedDataKey["IssueId"].ToString();
        }
        catch { }
    }
    #endregion GridView1_SelectedIndexChanged

    #region GridView2_SelectedIndexChanged
    protected void GridView2_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try
        {
            Session["OutboundShipmentId"] = GridView2.SelectedDataKey["OutboundShipmentId"].ToString();
            Session["IssueId"] = GridView2.SelectedDataKey["IssueId"].ToString();
        }
        catch { }
    }
    #endregion GridView2_SelectedIndexChanged
}
