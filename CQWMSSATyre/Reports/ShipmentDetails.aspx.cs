﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_ShipmentDetails : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["InboundShipmentId"] = -1;
                Session["ReceiptId"] = -1;
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GridView1.DataBind();
        }
        catch { }
    }
    #endregion ButtonSearch_Click

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/ShipmentDetails.aspx";

            Session["ReportName"] = "Shipment Details";

            ReportParameter[] RptParameters = new ReportParameter[7];

            // Create the ConnectionString report parameter
            string strReportConnectionString = Session["ReportConnectionString"].ToString();
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ConnectionString", strReportConnectionString);

            // Create the InboundShipmentId report parameter
            string inboundShipmentId = Session["InboundShipmentId"].ToString();

            if (inboundShipmentId == "")
                inboundShipmentId = "-1";

            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("InboundShipmentId", inboundShipmentId);

            // Create the ReceiptId report parameter
            string receiptId = Session["ReceiptId"].ToString();
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ReceiptId", receiptId);

            // Create the ReceiptLineId report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ReceiptLineId", "-1");

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
    #endregion ButtonPrint_Click

    #region GridView1_SelectedIndexChanged
    protected void GridView1_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try
        {
            Session["InboundShipmentId"] = GridView1.SelectedDataKey["InboundShipmentId"].ToString();
            Session["ReceiptId"] = GridView1.SelectedDataKey["ReceiptId"].ToString();
        }
        catch { }
    }
    #endregion GridView1_SelectedIndexChanged
}