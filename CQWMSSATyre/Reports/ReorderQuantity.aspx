﻿<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="ReorderQuantity.aspx.cs"
    Inherits="Reports_ReorderQuantity"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
    
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/BatchSearch.ascx" TagName="BatchSearch" TagPrefix="ucbtch" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionProductSearch" runat="server">
                <Header>
                <a href="" class="accordionLink">Enter a Product Code</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdateProductCode" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelProductCode" runat="server" Text="Product Code:"></asp:Label>
                            <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            
            <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                <a href="" class="accordionLink">Select Warehouse</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DropDownListWarehouse" runat="server" DataTextField="Warehouse"
                    DataValueField="WarehouseId" DataSourceID="ObjectDataSourceWarehouse" Width="150px" OnSelectedIndexChanged="DropDownListWarehouse_SelectedIndexChanged">
                </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>   
        </Panes>
    </ajaxToolkit:Accordion>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server"> 
            <ContentTemplate> 
                <table style="border:2px 2px" border="1">
                    <tr>
                        <th>Select Sort Sequence</th>
                    </tr>
                    <tr>
                        <td style="border:1px 1px">
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="False" 
                                DataValueField="Sort" Font-Size="Small"> 
                                <asp:ListItem Text="Highest Consumption" Value="H" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Lowest Days SOH" Value="L"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>                
            </ContentTemplate>
            </asp:UpdatePanel>
    <asp:Button ID="ButtonPrint" runat="server" Text="View" OnClick="ButtonPrint_Click" />
    <asp:ObjectDataSource ID="ObjectDataSourceWarehouse" runat="server" SelectMethod="GetActiveWarehouses" TypeName="Warehouse">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>



