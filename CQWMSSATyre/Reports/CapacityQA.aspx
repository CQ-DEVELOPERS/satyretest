<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="CapacityQA.aspx.cs" Inherits="Reports_CapacityQA" Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
              <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select an Operator Group</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DD_OperatorGroupId" runat="server" DataSourceID="OperatorGroupDataSource"
                            DataTextField="OperatorGroup" DataValueField="OperatorGroupId">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneUOM" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a UOM</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelUOM" runat="server">
                        <ContentTemplate>
                            <asp:RadioButtonList ID="rblUOM" runat="server">
                                <asp:ListItem Selected="True" Text="Units" Value="Units"></asp:ListItem>
                                <asp:ListItem Text="Weight" Value="Weight"></asp:ListItem>
                                <asp:ListItem Text="Jobs" Value="Jobs"></asp:ListItem>
                                <asp:ListItem Text="Pick Lines" Value="Instructions"></asp:ListItem>
                            </asp:RadioButtonList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:ObjectDataSource ID="OperatorGroupDataSource" runat="server" SelectMethod="GetOperatorGroups" TypeName="OperatorGroup" >
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" Type="String" SessionField="ConnectionStringName" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>

