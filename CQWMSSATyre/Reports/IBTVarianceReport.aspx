﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="IBTVarianceReport.aspx.cs" Inherits="Reports_IBTVarianceReport" Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default" Theme="Default" %>

<%@ Register Src="../Common/InboundSearch.ascx" TagName="InboundSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <uc1:InboundSearch ID="InboundSearch1" runat="server" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <table style="border: 2px 2px" border="1">
        <tr>
            <th>
                Select
            </th>
        </tr>
        <tr>
            <td style="border: 1px 1px">
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="False" DataValueField="AllOrVar"
                    Font-Size="Small">
                    <asp:ListItem Text="Show All Records" Value="A" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Show Variances Only" Value="V"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, ButtonSearch %>"
        OnClick="ButtonSearch_Click" />
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>"
        OnClick="ButtonPrint_Click" />
    <br />
    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1" DataKeyNames="OrderNumber"
                AutoGenerateSelectButton="true" AutoGenerateColumns="false" AllowPaging="true"
                OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="InboundShipmentId" HeaderText='<%$ Resources:Default,InboundShipmentId %>'
                        ReadOnly="true" />
                    <asp:BoundField DataField="ReceiptId" HeaderText='<%$ Resources:Default,ReceiptId %>'
                        ReadOnly="true" />
                    <asp:BoundField DataField="OrderNumber" HeaderText='<%$ Resources:Default,OrderNumber %>'
                        ReadOnly="true" />
                    <asp:TemplateField HeaderText='<%$ Resources:Default,Delivery %>'>
                        <ItemTemplate>
                            <div align="center">
                                <asp:Label ID="LabelDelivery" runat="server" Text='<%# Bind("Delivery") %>' Font-Bold="true"></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SupplierCode" HeaderText='<%$ Resources:Default,SupplierCode %>'
                        ReadOnly="true" />
                    <asp:BoundField DataField="Supplier" HeaderText='<%$ Resources:Default,Supplier %>'
                        ReadOnly="true" />
                    <asp:BoundField DataField="NumberOfLines" HeaderText='<%$ Resources:Default,NumberOfLines %>'
                        ReadOnly="true" />
                    <asp:BoundField DataField="PlannedDeliveryDate" HeaderText='<%$ Resources:Default,PlannedDeliveryDate %>'
                        ReadOnly="true" />
                    <asp:BoundField DataField="DeliveryDate" HeaderText='<%$ Resources:Default,DeliveryDate %>' />
                    <asp:BoundField DataField="Status" HeaderText='<%$ Resources:Default,Status %>' ReadOnly="true" />
                    <asp:BoundField DataField="InboundDocumentType" HeaderText='<%$ Resources:Default,InboundDocumentType %>'
                        ReadOnly="true" />
                    <asp:TemplateField HeaderText='<%$ Resources:Default,Location %>'>
                        <ItemTemplate>
                            <div align="center">
                                <asp:Label ID="LabelLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="DDLocation" runat="server" DataSourceID="ObjectDataSourceLocation"
                                DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Rating" HeaderText='<%$ Resources:Default,Rating %>' ReadOnly="true" />
                    <asp:BoundField DataField="DeliveryNoteNumber" HeaderText='<%$ Resources:Default,DeliveryNoteNumber %>' />
                    <asp:BoundField DataField="SealNumber" HeaderText='<%$ Resources:Default,SealNumber %>' />
                    <asp:TemplateField HeaderText='<%$ Resources:Default,Priority %>'>
                        <ItemTemplate>
                            <div align="center">
                                <asp:Label ID="LabelPriority" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="DDPriority" runat="server" DataSourceID="ObjectDataSourcePriority"
                                DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="VehicleRegistration" HeaderText='<%$ Resources:Default,VehicleRegistration %>' />
                    <asp:BoundField DataField="Remarks" HeaderText='<%$ Resources:Default,Remarks %>' />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Receiving"
        SelectMethod="GetIBTDocumentsReports">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="InboundDocumentTypeId" SessionField="InboundDocumentTypeId"
                Type="Int32" />
            <asp:SessionParameter Name="InboundShipmentId" DefaultValue="-1" Type="Int32" SessionField="InboundShipmentId" />
            <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
            <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode"
                Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
            <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
            <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
