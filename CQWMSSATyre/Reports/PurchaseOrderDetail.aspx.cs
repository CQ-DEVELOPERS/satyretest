﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_PurchaseOrderDetail : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["OrderNumber"] = -1;
                //Session["ProductCode"] = -1;
                Session["Batch"] = -1;
                Session["StorageUnitId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/PurchaseOrderDetail.aspx";

            Session["ReportName"] = "Purchase Order Detail";

            ReportParameter[] RptParameters = new ReportParameter[8];

            // Create the ConnectionString report parameter
            //RptParameters[0] = new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString());

            // Create the WarehouseId report parameter
            //RptParameters[1] = new ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            

            // Create the OrderNumber report parameter
            string orderNumber = TextBoxOrderNumber.Text;

            if (orderNumber == "")
                orderNumber = "-1";

            RptParameters[0] = new ReportParameter("OrderNumber", orderNumber);

            // Create the StorageUnitId report parameter
            RptParameters[1] = new ReportParameter("StorageUnitId", Session["StorageUnitId"].ToString());


            //// Create the Product Code report parameter
            //string productCode = TextBoxProductCode.Text;

            //if (productCode == "")
            //    productCode = "-1";

            //RptParameters[1] = new ReportParameter("ProductCode", productCode);

            // Create the Batch report parameter
            RptParameters[2] = new ReportParameter("Batch", Session["Batch"].ToString());

            // Create the FromDate report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch (Exception ex)
        {

        }
    }
}
