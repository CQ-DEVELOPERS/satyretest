using System;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;
using System.Web;

public partial class Reports_StockOnHandQC : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        //Print();
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        Print();
    }

    protected void Print()
    {
        try
        {
            Session["FromUrl"] = HttpContext.Current.Request.Url;

            Session["ReportName"] = "Stock On Hand QC";

            // Create the Report Parameters holder
            var reportParameters = new List<ReportParameter>();

            // Create the BilledTransactionId (Id) report parameter
            reportParameters.Add(new ReportParameter("ServerName", Session["ServerName"].ToString()));
            reportParameters.Add(new ReportParameter("DatabaseName", Session["DatabaseName"].ToString()));
            reportParameters.Add(new ReportParameter("UserName", Session["UserName"].ToString()));
            reportParameters.Add(new ReportParameter("WarehouseId", Session["WarehouseId"].ToString()));
            reportParameters.Add(new ReportParameter("ExternalCompanyId", Session["ExternalCompanyId"].ToString()));
            reportParameters.Add(new ReportParameter("OrderNumber", TextBoxOrderNumber.Text));

            Session["ReportParameters"] = reportParameters.ToArray();

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
}
