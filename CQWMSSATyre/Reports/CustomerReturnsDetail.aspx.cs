using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_CustomerReturnsDetail : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/CustomerReturnsDetail.aspx";
            Session["ReportName"] = "Customer Returns Detail";
            Session["RFCNumber"] = TextBoxRFCNumber.Text;
            Session["InvoiceNumber"] = TextBoxInvoiceNumber.Text;
            Session["Status"] = RadioButtonListStatus.Text;

            if (Session["ExternalCompanyId"] == null)
                Session["ExternalCompanyId"] = -1;

            if (Session["Status"] == null)
                Session["Status"] = null;

            if (Session["RFCNumber"] == null) 
                Session["RFCNumber"] = -1;

            if (Session["InvoiceNumber"] == null)
                Session["InvoiceNumber"] = -1;

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[10];

            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());
            // Create the WarehouseId report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());
            // Create the FromDate report parameter
            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());
            // Create the ToDate report parameter
            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());
            // Create the Customer report parameter
            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("Customer", Session["ExternalCompanyId"].ToString());
            // Create the Status report parameter
            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("Status", Session["Status"].ToString());
            // Create the B report parameter
            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("RFCNumber", Session["RFCNumber"].ToString());
            // Create the B report parameter
            RptParameters[9] = new Microsoft.Reporting.WebForms.ReportParameter("InvoiceNumber", Session["InvoiceNumber"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
}
