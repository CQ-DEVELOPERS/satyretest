﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;


public partial class Reports_InboundSLA : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                //Session["StorageUnitId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {

            Session["FromURL"] = "~/Reports/InboundSLA.aspx";

            string ReportType = RadioButtonListSummaryDetail.SelectedValue;

            if (ReportType == "D")
            {
                Session["ReportName"] = "Inbound SLA Detail";
            }
            else
            {
                Session["ReportName"] = "Inbound SLA";
            }

            ReportParameter[] RptParameters = new ReportParameter[11];

            //  Set up selected radio button values
            string Measure = RadioButtonListMeasure.SelectedValue;
            string Weekend = RadioButtonWeekend.SelectedValue;
            string ReceivingThreshold = RadioButtonToLineReceived.SelectedValue;
            string PutAwayThreshold = RadioButtonToPutAway.SelectedValue;
            string ASNReceiptCloseThreshold = RadioButtonToASNReceiptClose.SelectedValue;

            RptParameters[0] = new ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[1] = new ReportParameter("ToDate", Session["ToDate"].ToString());

            // Create the PrincipalId report parameter
            RptParameters[2] = new ReportParameter("PrincipalId", DropDownListPrincipal.SelectedValue.ToString());

            RptParameters[3] = new ReportParameter("SLAStartDate", Measure.ToString());

            RptParameters[4] = new ReportParameter("WeekendType", Weekend.ToString());

            RptParameters[5] = new ReportParameter("ReceivingThreshold", ReceivingThreshold.ToString());

            RptParameters[6] = new ReportParameter("PutAwayThreshold", PutAwayThreshold.ToString());

            RptParameters[7] = new ReportParameter("ASNReceiptCloseThreshold", ASNReceiptCloseThreshold.ToString());


            RptParameters[8] = new ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[9] = new ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[10] = new ReportParameter("UserName", Session["UserName"].ToString());

           
            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
}
