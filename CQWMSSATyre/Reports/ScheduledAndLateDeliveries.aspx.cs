using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_ScheduledAndLateDeliveries : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["OrderNumber"] = -1;
                Session["StorageUnitId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/ScheduledAndLateDeliveries.aspx";

            Session["ReportName"] = "ScheduledAndLateDeliveries";

            ReportParameter[] RptParameters = new ReportParameter[9];

            // Create the Sort report parameter
            string Selection = RadioButtonSelection.SelectedValue;

            // Create the OrderNumber report parameter
            string orderNumber = TextBoxOrderNumber.Text;

            if (orderNumber == "")
                orderNumber = "-1";

            RptParameters[0] = new ReportParameter("OrderNumber", orderNumber);

            // Create the StorageUnitId report parameter
            RptParameters[1] = new ReportParameter("StorageUnitId", Session["StorageUnitId"].ToString());

           
            // Create the FromDate report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            RptParameters[7] = new ReportParameter("Selection", Selection.ToString());

            RptParameters[8] = new ReportParameter("PrincipalId", DropDownListPrincipal.SelectedValue.ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch (Exception ex)
        {

        }
    }
}