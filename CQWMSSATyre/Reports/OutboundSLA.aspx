﻿<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="OutboundSLA.aspx.cs"
    Inherits="Reports_OutboundSLA"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="ucdr" %>
<%@ Register Src="../Common/StatusSearch.ascx" TagName="StatusSearch" TagPrefix="ucss" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPaneDateRange" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelDateRange" runat="server">
                        <ContentTemplate>
                            <ucdr:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanePrincipal" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Principal</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPrincipal" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="DropDownListPrincipal" runat="server" DataSourceID="ObjectDataSourcePrincipal"
                                DataTextField="Principal" DataValueField="PrincipalId">
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ObjectDataSourcePrincipal" runat="server" TypeName="Principal"
                                SelectMethod="GetPrincipalParameter">
                                <SelectParameters>
                                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                                        Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPanelStatusSearch" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Status</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelStatus" runat="server">
                        <ContentTemplate>
                            <ucss:StatusSearch ID="StatusSearch" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPaneOrderNumber" runat="server">
                <Header>
                    <a href="" class="accordionLink">Enter an Order Number</a>
                </Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelOrderNumber" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="LabelOrderNumber" runat="server" Text="Order Number:"></asp:Label>
                            <asp:TextBox ID="TextBoxOrderNumber" runat="server"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:UpdatePanel ID="UpdatePanelMeasure" runat="server">
        <ContentTemplate>
            <table style="border: 2px 2px" border="1">
                <tr>
                    <td>Requested ShipDate Option
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonListShipDate" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <asp:ListItem Text="Use Date and Time Value" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Use Date Value Only" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    </tr>
                <tr>
                    <td>Choose how SLA must be measured
                    </td>
                    
                </tr>
                <tr>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonSLAMeasured" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <asp:ListItem Text="Requested Ship date to Pick Complete date" Value="1" ></asp:ListItem>
                            <asp:ListItem Text="Requested Ship date to Ship Complete date" Value="2" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td
                </tr>
                <tr>
                    <td>Select Summary or Detail Report
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px 1px">
                        <asp:RadioButtonList ID="RadioButtonListSummaryDetail" runat="server" AutoPostBack="False" DataValueField="Sort"
                            Font-Size="Small">
                            <asp:ListItem Text="Summary" Value="S" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Detail" Value="D"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>  
                </tr>   
            </table>
        </ContentTemplate>
       
    </asp:UpdatePanel>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
</asp:Content>


