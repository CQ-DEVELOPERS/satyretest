<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewOrderConsole.aspx.cs" Inherits="Reports_ViewStockAdjustments"
    Title="<%$ Resources:Default, ReportTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <%--<telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="Textbox" />--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadFilter1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadFilter1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridInterfaceConsole">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridInterfaceConsole" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridInterfaceConsole" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
        <tr align="right">
            <td>
                <asp:Label ID="LabelOrderNumber" runat="server" Text="OrderNumber"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxOrderNumber" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelCompany" runat="server" Text="Company"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxCompany" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelCompanyCode" runat="server" Text="CompanyCode"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxCompanyCode" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
        </tr>
        <tr align="right">
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="ProductCode"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxProductCode" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelProduct" runat="server" Text="Product"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxProduct" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelBatch" runat="server" Text="Batch"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxBatch" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
        </tr>
        <tr align="right">
            <td>
                <asp:Label ID="LabelStatus" runat="server" Text="Status"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxStatus" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelMessage" runat="server" Text="Message"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxMessage" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
            <td>
                <asp:Label ID="LabelRecordType" runat="server" Text="RecordType"></asp:Label>
                <telerik:RadTextBox ID="RadTextBoxRecordType" runat="server" Width="200px"></telerik:RadTextBox>
            </td>
        </tr>
        <tr align="right">
            <td>
                <asp:Label ID="LabelFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>"></asp:Label>
                <telerik:RadDatePicker ID="rdpFromDate" runat="server" Width="200px"></telerik:RadDatePicker>
            </td>
            <td>
                <asp:Label ID="labeToDate" runat="server" Text="<%$ Resources:Default, ToDate %>"></asp:Label>
                <telerik:RadDatePicker ID="rdpToDate" runat="server" Width="200px"></telerik:RadDatePicker>
            </td>
            <td>
                <telerik:RadButton ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"></telerik:RadButton>
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="RadGridInterfaceConsole" runat="server"
        AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceStockAdjustment"
        PageSize="30" ShowGroupPanel="True" Skin="Metro"
        OnItemCommand="RadGridInterfaceConsole_ItemCommand">
        <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
        <MasterTableView DataKeyNames="InterfaceId,InterfaceTableId,Status" DataSourceID="ObjectDataSourceStockAdjustment"
            AutoGenerateColumns="False" EnableHeaderContextMenu="true" AllowFilteringByColumn="true">
            <Columns>
                <telerik:GridBoundColumn DataField="Status" HeaderText="Status" SortExpression="Status" UniqueName="Status"></telerik:GridBoundColumn>
                <telerik:GridButtonColumn Text="Reprocess" HeaderText="Reprocess" CommandName="Reprocess"></telerik:GridButtonColumn>
                <telerik:GridBoundColumn DataField="InterfaceMessage" HeaderText="InterfaceMessage" SortExpression="InterfaceMessage" UniqueName="InterfaceMessage"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="InterfaceMessageCode" HeaderText="InterfaceMessageCode" SortExpression="InterfaceMessageCode" UniqueName="InterfaceMessageCode"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="InterfaceTableId" HeaderText="InterfaceTableId" SortExpression="InterfaceTableId" UniqueName="InterfaceTableId"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="InterfaceTable" HeaderText="InterfaceTable" SortExpression="InterfaceTable" UniqueName="InterfaceTable"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="InterfaceId" HeaderText="InterfaceId" SortExpression="InterfaceId" UniqueName="InterfaceId"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="RecordType" HeaderText="RecordType" SortExpression="RecordType" UniqueName="RecordType"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="RecordStatus" HeaderText="RecordStatus" SortExpression="RecordStatus" UniqueName="RecordStatus"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ProcessedDate" HeaderText="ProcessedDate" SortExpression="ProcessedDate" UniqueName="ProcessedDate"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="InsertDate" HeaderText="InsertDate" SortExpression="InsertDate" UniqueName="InsertDate"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PrimaryKey" HeaderText="PrimaryKey" SortExpression="PrimaryKey" UniqueName="PrimaryKey"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="OrderNumber" HeaderText="OrderNumber" SortExpression="OrderNumber" UniqueName="OrderNumber"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="CompanyCode" SortExpression="CompanyCode" UniqueName="CompanyCode"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Company" HeaderText="Company" SortExpression="Company" UniqueName="Company"></telerik:GridBoundColumn>
                <%--<telerik:GridBoundColumn DataField="ForeignKey" HeaderText="ForeignKey" SortExpression="ForeignKey" UniqueName="ForeignKey"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LineNumber" HeaderText="LineNumber" SortExpression="LineNumber" UniqueName="LineNumber"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" UniqueName="ProductCode"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Product" HeaderText="Product" SortExpression="Product" UniqueName="Product"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Batch" HeaderText="Batch" SortExpression="Batch" UniqueName="Batch"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" UniqueName="Quantity"></telerik:GridBoundColumn>--%>
                <telerik:GridBoundColumn DataField="hAdditional1" HeaderText="hAdditional1" SortExpression="hAdditional1" UniqueName="hAdditional1"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="hAdditional2" HeaderText="hAdditional2" SortExpression="hAdditional2" UniqueName="hAdditional2"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="hAdditional3" HeaderText="hAdditional3" SortExpression="hAdditional3" UniqueName="hAdditional3"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="hAdditional4" HeaderText="hAdditional4" SortExpression="hAdditional4" UniqueName="hAdditional4"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="hAdditional5" HeaderText="hAdditional5" SortExpression="hAdditional5" UniqueName="hAdditional5"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="hAdditional6" HeaderText="hAdditional6" SortExpression="hAdditional6" UniqueName="hAdditional6"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="hAdditional7" HeaderText="hAdditional7" SortExpression="hAdditional7" UniqueName="hAdditional7"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="hAdditional8" HeaderText="hAdditional8" SortExpression="hAdditional8" UniqueName="hAdditional8"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="hAdditional9" HeaderText="hAdditional9" SortExpression="hAdditional9" UniqueName="hAdditional9"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="hAdditional10" HeaderText="hAdditional10" SortExpression="hAdditional10" UniqueName="hAdditional10"></telerik:GridBoundColumn>
                <%--<telerik:GridBoundColumn DataField="dAdditional1" HeaderText="dAdditional1" SortExpression="dAdditional1" UniqueName="dAdditional1"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="dAdditional2" HeaderText="dAdditional2" SortExpression="dAdditional2" UniqueName="dAdditional2"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="dAdditional3" HeaderText="dAdditional3" SortExpression="dAdditional3" UniqueName="dAdditional3"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="dAdditional4" HeaderText="dAdditional4" SortExpression="dAdditional4" UniqueName="dAdditional4"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="dAdditional5" HeaderText="dAdditional5" SortExpression="dAdditional5" UniqueName="dAdditional5"></telerik:GridBoundColumn>--%>
            </Columns>
        </MasterTableView>
                <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
                    <Resizing AllowColumnResize="true"></Resizing>
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="True" />
    </telerik:RadGrid>
    <asp:ObjectDataSource ID="ObjectDataSourceStockAdjustment" runat="server" TypeName="InterfaceConsole"
        SelectMethod="SearchOrders">
        <SelectParameters>
            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            
            <asp:ControlParameter ControlID="RadTextBoxOrderNumber" Name="OrderNumber" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxCompany" Name="Company" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxCompanyCode" Name="CompanyCode" PropertyName="Text" Type="String" />
            
            <asp:ControlParameter ControlID="RadTextBoxProductCode" Name="ProductCode" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxProduct" Name="Product" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxBatch" Name="Batch" PropertyName="Text" Type="String" />

            <asp:ControlParameter ControlID="RadTextBoxStatus" Name="Status" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxMessage" Name="Message" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="RadTextBoxRecordType" Name="RecordType" PropertyName="Text" Type="String" />
            
            <asp:ControlParameter ControlID="rdpFromDate" Name="FromDate" />
            <asp:ControlParameter ControlID="rdpToDate" Name="ToDate"  />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>