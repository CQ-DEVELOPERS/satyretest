using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_LocationAnalysis : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/LocationAnalysis.aspx";

            Session["ReportName"] = "Location Analysis By Transaction";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[9];

            // Create the ConnectionString report parameter
            string strReportConnectionString = Session["ReportConnectionString"].ToString();
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ConnectionString", strReportConnectionString);

            // Create the WarehouseId report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            // Create the FromLocationId report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("FromLocationId", Session["FromLocationId"].ToString());

            // Create the ToLocationId report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ToLocationId", Session["ToLocationId"].ToString());

            // Create the FromDate report parameter
            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
}
