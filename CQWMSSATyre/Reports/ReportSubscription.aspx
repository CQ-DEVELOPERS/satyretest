<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="ReportSubscription.aspx.cs" Inherits="Reports_ReportSubscription" Title="<%$ Resources:Default, ReportTitle %>"  Theme="Default"%>
<%@ Register Src="~/Common/DatePeriod.ascx" TagName="DatePeriod" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPanePeriod" runat="server">
                <Header>
                    <a href="" class="accordionLink">New Report Subscription</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPeriod" runat="server">
                        <ContentTemplate>
                            <uc1:DatePeriod ID="DatePeriod1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>

</asp:Content>

