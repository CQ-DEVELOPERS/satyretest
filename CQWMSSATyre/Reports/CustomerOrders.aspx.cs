using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_CustomerOrders : System.Web.UI.Page
{
    #region InitializeCultureform
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCultureform"

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/CustomerOrders.aspx";

            Session["ReportName"] = "CustomerOrder";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[8];

            // Create the ConnectionString report parameter
            string strReportConnectionString = Session["ReportConnectionString"].ToString();
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ConnectionString", strReportConnectionString);

            //// Create the ExternalClientId report parameter
            //RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ExternalClient", DD_ExternalClient.SelectedValue);

            // Create the OrderNumber report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("OrderNumber", "");

            // Create the FromDate report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());
            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
    protected void ExternalCompanies_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {

    }
}
