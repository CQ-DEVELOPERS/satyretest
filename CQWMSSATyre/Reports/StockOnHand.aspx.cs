using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_StockOnHand : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["StorageUnitId"] = -1;
                Session["BatchId"] = -1;
                Session["FromLocationId"] = -1;
                Session["ToLocationId"] = -1;
                Session["LocationTypeId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/StockOnHand.aspx";
            Session["ReportName"] = "Stock On Hand";
            ReportParameter[] RptParameters = new ReportParameter[16];

            // Create the ConnectionString report parameter
            RptParameters[0] = new ReportParameter("ConnectionString", Session["ReportConnectionString"].ToString());
            // Create the WarehouseId report parameter
            RptParameters[1] = new ReportParameter("WarehouseId", Session["WarehouseId"].ToString());
            // Create the StorageUnitId report parameter
            RptParameters[2] = new ReportParameter("StorageUnitId", Session["StorageUnitId"].ToString());
            // Create the BatchId report parameter
            RptParameters[3] = new ReportParameter("BatchId", Session["BatchId"].ToString());
            // Create the StoreLocationId report parameter
            RptParameters[4] = new ReportParameter("StoreLocationId", Session["FromLocationId"].ToString());
            // Create the PickLocationId report parameter
            RptParameters[5] = new ReportParameter("PickLocationId", Session["ToLocationId"].ToString());
            // Create the LocationTypeId report parameter
            RptParameters[6] = new ReportParameter("LocationTypeId", Session["LocationTypeId"].ToString());



            // Create the BatchMore report parameter
            int batchMore = -1;

            if (rbBatchMore.Checked)
                batchMore = 1;

            if (rbBatchLess.Checked)
                batchMore = 0;

            RptParameters[7] = new ReportParameter("BatchMore", batchMore.ToString());

            // Create the BatchQuantity report parameter
            RptParameters[8] = new ReportParameter("BatchQuantity", tbBatchQuantity.Text);

            // Create the AllocatedMore report parameter
            int allocatedMore = -1;

            if (rbAllocatedMore.Checked)
                allocatedMore = 1;

            if (rbAllocatedLess.Checked)
                allocatedMore = 0;

            RptParameters[9] = new ReportParameter("AllocatedMore", allocatedMore.ToString());

            // Create the AllocatedQuantity report parameter
            RptParameters[10] = new ReportParameter("AllocatedQuantity", tbAllocatedQuantity.Text);

            // Create the ReservedMore report parameter
            int reservedMore = -1;

            if (rbReservedMore.Checked)
                reservedMore = 1;

            if (rbReservedLess.Checked)
                reservedMore = 0;

            RptParameters[11] = new ReportParameter("ReservedMore", reservedMore.ToString());

            // Create the ReservedQuantity report parameter
            RptParameters[12] = new ReportParameter("ReservedQuantity", tbReservedQuantity.Text);

            RptParameters[13] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[14] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[15] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());






            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
}
