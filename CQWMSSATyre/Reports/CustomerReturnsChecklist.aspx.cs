﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_CustomerReturnsChecklist : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (!Page.IsPostBack)
    //        {
    //            Session["ProductId"] = -1;
    //            Session["BatchId"] = -1;
    //        }
    //    }
    //    catch { }
    //}

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/CustomerReturnsChecklist.aspx";

            Session["ReportName"] = "Customer Returns Checklist";

            ReportParameter[] RptParameters = new ReportParameter[9];

            // Create the ReasonId report parameter
            string reasonId = "-1";

            if (Session["ReasonId"] != null)
                reasonId = Session["ReasonId"].ToString();

            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ReasonId", reasonId);

            // Create the ContactListId report parameter
            RptParameters[1] = new ReportParameter("ContactListId", Session["ContactListId"].ToString());
 
            // Create the FromDate report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            // Create the Order Number report parameter
            string orderNumber = TextBoxOrderNumber.Text;

            if (orderNumber == "")
                orderNumber = "-1";

            RptParameters[4] = new ReportParameter("OrderNumber", orderNumber);

            // Create the External Company Id report parameter
            string externalCompanyId = "-1";

            if (Session["ExternalCompanyId"] != null)
                externalCompanyId = Session["ExternalCompanyId"].ToString();

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ExternalCompanyId", externalCompanyId);

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());


            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
}
