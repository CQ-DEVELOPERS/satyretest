<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true" CodeFile="OperatorDwellTime.aspx.cs" Inherits="Reports_OperatorDwellTime" Title="<%$ Resources:Default, ReportTitle %>" Theme="Default"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
<ajaxToolkit:Accordion ID="AccordionSearch" runat="server" SelectedIndex="0"
        HeaderCssClass="accordionHeader" ContentCssClass="accordionContent" FadeTransitions="false"
        FramesPerSecond="40" TransitionDuration="250" AutoSize="None" RequireOpenedPane="false"
        SuppressHeaderPostbacks="true">
        <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPanePeriod" runat="server">
                <Header>
                    <a href="" class="accordionLink">Select a Date Range</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanelPeriod" runat="server">
                        <ContentTemplate>
                            <uc1:DateRange ID="DateRange1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
              <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                    <a href="" class="accordionLink">Elapsed Time</a></Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                           <asp:Label ID="lbl_text" runat="server" Text="All Operators with a Dwell time greater than"></asp:Label>
                           <asp:TextBox ID="txt_amount" runat="server" Columns="4"></asp:TextBox>
                           <asp:DropDownList ID="dd_hms" runat="server">
                           <asp:ListItem Text="Hours/s" Value="H"></asp:ListItem>
                           <asp:ListItem Text="Minutes/s" Value="M"></asp:ListItem>
                           <asp:ListItem Text="Seconds/s" Value="S"></asp:ListItem>
                           </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </ajaxToolkit:AccordionPane>
        </Panes>
    </ajaxToolkit:Accordion>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click"/>
</asp:Content>

