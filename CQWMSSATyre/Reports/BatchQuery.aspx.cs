using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_BatchQuery : System.Web.UI.Page
{
    #region InitializeCultureform
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCultureform"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            

        }

    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/BatchQuery.aspx";

            Session["ReportName"] = "Batch Query";

            if(Session["Product"] == null) 
            {
                Session["Product"] = -1;
            };
            if(Session["ProductCode"] == null) 
            {
                Session["ProductCode"] = -1;

            };

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[9];

            // Create the ServerName report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());
           

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("BatchNumber", TextBoxBatchNumber.Text);


            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("BatchRefNumber", TextBoxBatchRefNumber.Text);

            // Create the OrderNumber report parameter
            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ProductCode",Session["ProductCode"].ToString());

            // Create the FromDate report parameter
            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("Product", Session["Product"].ToString());

            // Create the ToDate report parameter
            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("BatchStatus", DropDownBatchStatus.SelectedValue);

            
            // Create the ToDate report parameter
            RptParameters[8] = new Microsoft.Reporting.WebForms.ReportParameter("OrderNumber", TextBoxPurchaseOrderNumber.Text.ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
}
