﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_InterwarehouseReplenishments : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ProductId"] = -1;
                Session["Sort"] = -1;
            }
            
        }
        catch { }
    }
   
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/InterwarehouseReplenishments.aspx";

            Session["ReportName"] = "Interwarehouse Replenishment Report";

            ReportParameter[] RptParameters = new ReportParameter[7];

            // Create the Sort report parameter
            string Sort = RadioButtonList1.SelectedValue;

            // Create the Sort report parameter
            string days = TextBoxDays.Text;

            //RptParameters[0] = new ReportParameter("ProductCode", productCode);
            RptParameters[0] = new ReportParameter("ProductId", Session["ProductId"].ToString());

            // Create the WarehouseId report parameter
            RptParameters[1] = new ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            RptParameters[2] = new ReportParameter("Sort", Sort.ToString());

            RptParameters[3] = new ReportParameter("Days", days.ToString());

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch (Exception ex)
        {

        }
    }
}
