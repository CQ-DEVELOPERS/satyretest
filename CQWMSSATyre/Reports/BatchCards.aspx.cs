﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_BatchCards : System.Web.UI.Page
{
    #region InitializeCultureform
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCultureform"

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GridView1.DataBind();
        }
        catch { }
    }
    #endregion ButtonSearch_Click

    #region GridView1_SelectedIndexChanged
    protected void GridView1_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try
        {
            Session["OutboundShipmentId"] = GridView1.SelectedDataKey["OutboundShipmentId"].ToString();
            Session["IssueId"] = GridView1.SelectedDataKey["IssueId"].ToString();
        }
        catch { }
    }
    #endregion GridView1_SelectedIndexChanged

    #region ButtonPrint_Click
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/BatchCards.aspx";

            Session["ReportName"] = "Batch Card Details";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[8];

            // Create the ServerName report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            string outboundShipmentId = Session["OutboundShipmentId"].ToString();

            if (outboundShipmentId == "")
                outboundShipmentId = "-1";

            RptParameters[3] = new ReportParameter("OutboundShipmentId", outboundShipmentId);

            // Create the ReceiptId report parameter
            string issueId = Session["IssueId"].ToString();

            if (issueId == "")
                issueId = "-1";

            RptParameters[4] = new ReportParameter("IssueId", issueId);

            //// Create the OrderNumber report parameter
            //RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("OrderNumber", Session["OrderNumber"].ToString());
            // Create the FromDate report parameter
            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
    #endregion ButtonPrint_Click
}
