﻿<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="RTTReport.aspx.cs"
    Inherits="Reports_RTTReport"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/OutboundShipmentSearch.ascx" TagName="OutboundSearch" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <uc1:OutboundSearch ID="OutboundSearch1" runat="server" />
    <br />
    <br />
    <br />
    <br />
    <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, ButtonSearch %>" OnClick="ButtonSearch_Click" />
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, ButtonPrint %>" OnClick="ButtonPrint_Click" />
    <br />
    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1" AllowPaging="True" AllowSorting="True"
                DataKeyNames="OutboundShipmentId,IssueId" AutoGenerateColumns="False" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                <Columns>
                    <asp:CommandField ShowSelectButton="True"></asp:CommandField>
                    <asp:BoundField DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>" SortExpression="OutboundShipmentId">
                    </asp:BoundField>
                    <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber">
                    </asp:BoundField>
                    <asp:BoundField DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>" SortExpression="CustomerCode">
                    </asp:BoundField>
                    <asp:BoundField DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>" SortExpression="Customer">
                    </asp:BoundField>
                    <asp:BoundField DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>" SortExpression="NumberOfLines">
                    </asp:BoundField>
                    <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>" SortExpression="DeliveryDate">
                    </asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status">
                    </asp:BoundField>
                    <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route">
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Reports"
        SelectMethod="SearchDespatchAdvice">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId" Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
            <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
            <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
            <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
            <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
            <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>


