<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/MasterPage.master"
    AutoEventWireup="true"
    CodeFile="OutboundPickList.aspx.cs"
    Inherits="Reports_OutboundPickList"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<%@ Register Src="../Common/OutboundShipmentWaveTextBox.ascx" TagName="OutboundShipmentWaveTextBox" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGridOrders">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridOrders" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <uc1:OutboundShipmentWaveTextBox ID="OutboundShipmentWaveTextBox1" runat="server" />
    <br />
    <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, ButtonSearch %>" OnClick="ButtonSearch_Click" />
    <telerik:RadGrid ID="RadGridOrders" runat="server" Skin="Metro" AllowAutomaticUpdates="false"
        AllowFilteringByColumn="false" AllowSorting="True" ShowGroupPanel="True" AllowPaging="true" PageSize="30"
        AutoGenerateColumns="False" AllowMultiRowSelection="True"
        DataSourceID="ObjectDataSourcePlanning" OnItemCommand="RadGridOrders_ItemCommand">
        <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
        <MasterTableView DataKeyNames="OutboundShipmentId,IssueId" DataSourceID="ObjectDataSourcePlanning" AutoGenerateColumns="False" EnableHeaderContextMenu="true">
            <Columns>
                <telerik:GridClientSelectColumn UniqueName="CheckBox"></telerik:GridClientSelectColumn>
                <telerik:GridEditCommandColumn ButtonType="ImageButton" 
                    FilterControlAltText="Filter EditCommandColumn column"></telerik:GridEditCommandColumn>
                <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                    UniqueName="TemplateColumn">
                    <ItemTemplate>
                        <img alt="Availability Indicator" align="middle" src='<%# DataBinder.Eval(Container.DataItem, "AvailabilityIndicator", "../images/Indicators/{0}.gif") %>' />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn Text="<%$ Resources:Default, Print %>" HeaderText="<%$ Resources:Default, Print %>" CommandName="Print"></telerik:GridButtonColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundShipmentId" HeaderText="<%$ Resources:Default, OutboundShipmentId %>" SortExpression="OutboundShipmentId"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Wave" HeaderText="<%$ Resources:Default, Wave %>" SortExpression="Wave"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="CustomerCode" HeaderText="<%$ Resources:Default, CustomerCode %>" SortExpression="CustomerCode"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Customer" HeaderText="<%$ Resources:Default, Customer %>" SortExpression="Customer"></telerik:GridBoundColumn>
                <telerik:GridDropDownColumn UniqueName="DropDownListPriorityId" 
                    ListTextField="Priority" ListValueField="PriorityId"
                    DataSourceID="ObjectDataSourcePriority" DataField="PriorityId" 
                    HeaderText="<%$ Resources:Default, Priority %>" 
                    FilterControlAltText="Filter DropDownListPriorityId column">
                </telerik:GridDropDownColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="PercentageComplete" HeaderText="<%$ Resources:Default, PercentagePicked %>"
                    SortExpression="PercentageComplete" 
                    FilterControlAltText="Filter PercentageComplete column" 
                    UniqueName="PercentageComplete">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="NumberOfLines" HeaderText="<%$ Resources:Default, NumberOfLines %>" SortExpression="NumberOfLines"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Units" HeaderText="<%$ Resources:Default, Units %>" SortExpression="Units"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="ShortPicks" HeaderText="<%$ Resources:Default, ShortPicks %>" SortExpression="ShortPicks"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Releases" HeaderText="<%$ Resources:Default, Releases %>" SortExpression="Releases"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" SortExpression="CreateDate"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>" SortExpression="DeliveryDate"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Status" HeaderText="<%$ Resources:Default, Status %>" SortExpression="Status"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>" SortExpression="OutboundDocumentType"></telerik:GridBoundColumn>
                <telerik:GridDropDownColumn UniqueName="DropDownListLocationId" 
                    ListTextField="Location" ListValueField="LocationId"
                    DataSourceID="ObjectDataSourceLocation" DataField="LocationId" 
                    HeaderText="<%$ Resources:Default, Location %>" 
                    FilterControlAltText="Filter DropDownListLocationId column">
                </telerik:GridDropDownColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Route" HeaderText="<%$ Resources:Default, Route %>" SortExpression="Route"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Rating" HeaderText="<%$ Resources:Default, Rating %>" SortExpression="Rating"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Weight" HeaderText="<%$ Resources:Default, Weight %>" SortExpression="Weight"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn ReadOnly="True" DataField="Remarks" HeaderText="<%$ Resources:Default, Remarks %>" SortExpression="Remarks"></telerik:GridBoundColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn ButtonType="PushButton" 
                    FilterControlAltText="Filter EditCommandColumn1 column" 
                    UniqueName="EditCommandColumn1">
                </EditColumn>
            </EditFormSettings>
            <CommandItemTemplate>
                <asp:Button ID="DownloadPDF" runat="server" Width="100%" CommandName="ExportToPdf"
                CssClass="pdfButton"></asp:Button>
                <asp:Image ID="Image1" runat="server" ImageUrl="Images/Backgr.jpg" AlternateText="Sushi Bar"
                Width="100%"></asp:Image>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
            <Resizing AllowColumnResize="true"></Resizing>
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings ShowUnGroupButton="True" />
    </telerik:RadGrid>
    <asp:ObjectDataSource ID="ObjectDataSourceLocation" runat="server" TypeName="Location"
        SelectMethod="GetLocationsByIssue">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="outboundShipmentId" SessionField="OutboundShipmentId"
                Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="issueId" SessionField="IssueId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="Reports" SelectMethod="SearchWave">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="outboundShipmentId" SessionField="ParameterShipmentId" Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
            <asp:SessionParameter Name="OutboundDocumentTypeId" SessionField="OutboundDocumentTypeId" Type="Int32" />
            <asp:SessionParameter Name="WaveId" SessionField="WaveId" Type="Int32" />
            <asp:SessionParameter Name="OrderNumber" SessionField="OrderNumber" Type="String" />
            <asp:SessionParameter Name="ExternalCompany" SessionField="ExternalCompany" Type="String" />
            <asp:SessionParameter Name="ExternalCompanyCode" SessionField="ExternalCompanyCode" Type="String" />
            <asp:SessionParameter Name="FromDate" SessionField="FromDate" Type="DateTime" />
            <asp:SessionParameter Name="ToDate" SessionField="ToDate" Type="DateTime" />
            <asp:SessionParameter Name="PrincipalId" SessionField="PrincipalId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" SelectMethod="GetPriorities"
        TypeName="Priority">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

