﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="CertificateOfAnalysis.aspx.cs" Inherits="Reports_CertificateOfAnalysis"
    Title="<%$ Resources:Default, ReportTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/StorageUnitBatchIdSearch.ascx" TagName="StorageUnitBatchIdSearch"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelProductCode" runat="server" Text="<%$ Resources:Default,ProductCode %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProductCode" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelProduct" runat="server" Text="<%$ Resources:Default,Product %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxProduct" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelSKUCode" runat="server" Text="<%$ Resources:Default,SKUCode %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxSKUCode" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelSKU" runat="server" Text="<%$ Resources:Default,SKU %>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxSKU" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, Batch%>"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxBatch" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default,Search %>"
                    OnClick="ButtonSearch_Click" />
            </td>
        </tr>
    </table>
    <asp:Button ID="ButtonPrint" runat="server" Text="<%$ Resources:Default, Print%>"
        OnClick="ButtonPrint_Click" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="GridViewProductSearchCOA" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                AutoGenerateSelectButton="False" DataKeyNames="ProductCode,Product,Batch" OnSelectedIndexChanged="GridViewProductSearchCOA_OnSelectedIndexChanged"
                OnPageIndexChanging="GridViewProductSearchCOA_PageIndexChanging" PageSize="30"
                AllowSorting="true">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, Select%>">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default,ProductCode %>"
                        SortExpression="ProductCode" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default,Product %>"
                        SortExpression="Product" />
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default,SKUCode %>"
                        SortExpression="SKUCode" />
                    <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default,SKU %>" SortExpression="SKU" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default,Batch %>" SortExpression="Batch" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
