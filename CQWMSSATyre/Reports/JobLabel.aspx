<%@ Page Language="C#"
    MasterPageFile="~/MasterPages/Blank.master"
    AutoEventWireup="true"
    CodeFile="JobLabel.aspx.cs"
    Inherits="Reports_JobLabel"
    Title="<%$ Resources:Default, ReportTitle %>"
    StylesheetTheme="Default"
    Theme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, ReportTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, ReportAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <asp:Label ID="LabelJob" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
    <asp:TextBox ID="TextBoxJob" runat="server" Enabled="false"></asp:TextBox>
</asp:Content>


