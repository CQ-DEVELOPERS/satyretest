﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_ThroughputMetrics : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["StorageUnitId"] = -1;
                Session["BatchId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/ThroughputMetrics.aspx";

            Session["ReportName"] = "Throughput Metrics";

            ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[8];


            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("PrincipalId", DropDownListPrincipal.SelectedValue.ToString());


            // Create the FromDate report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("FromDate", Session["FromDate"].ToString());

            // Create the ToDate report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("ToDate", Session["ToDate"].ToString());

            DateTime fromtime = DateTime.Parse(string.Format("{0}:{1}:{2} {3}", TimeSelector1.Hour, TimeSelector1.Minute, TimeSelector1.Second, TimeSelector1.AmPm));

            Session["FromTime"] = fromtime;

            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("FromTime", Session["FromTime"].ToString());

            DateTime totime = DateTime.Parse(string.Format("{0}:{1}:{2} {3}", TimeSelector2.Hour, TimeSelector2.Minute, TimeSelector2.Second, TimeSelector2.AmPm));

            Session["ToTime"] = totime;

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("ToTime", Session["ToTime"].ToString());

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            RptParameters[7] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
}
