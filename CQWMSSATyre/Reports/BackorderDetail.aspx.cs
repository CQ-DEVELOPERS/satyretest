using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Reports_BackorderDetail : System.Web.UI.Page
{
    #region InitializeCultureform
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCultureform"

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/BackorderDetail.aspx";

            Session["ReportName"] = "Backorder Detail";

            Microsoft.Reporting.WebForms.ReportParameter[] RptParameters = new Microsoft.Reporting.WebForms.ReportParameter[7];

            // Create the ServerName report parameter
            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());

            // Create the DatabaseName report parameter
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());

            // Create the UserName report parameter
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());

            // Create the WarehouseId report parameter
            RptParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("WarehouseId", Session["WarehouseId"].ToString());

            // Create the StorageUnitId report parameter
            string storageUnitId = "-1";

            if (Session["StorageUnitId"] != null)
                storageUnitId = Session["StorageUnitId"].ToString();

            RptParameters[4] = new Microsoft.Reporting.WebForms.ReportParameter("StorageUnitId", storageUnitId);

            // Create the StorageUnitId report parameter
            string outboundDocumentTypeId = "-1";

            outboundDocumentTypeId = DropDownListOutboundDocumentType.SelectedValue.ToString();

            RptParameters[5] = new Microsoft.Reporting.WebForms.ReportParameter("OutboundDocumentTypeId", outboundDocumentTypeId);

            // Create the ExternalClientId report parameter
            string externalCompanyId = "-1";

            if (Session["ExternalCompanyId"] != null)
                externalCompanyId = Session["ExternalCompanyId"].ToString();

            RptParameters[6] = new Microsoft.Reporting.WebForms.ReportParameter("ExternalCompanyId", externalCompanyId);

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");
        }
        catch { }
    }
}
