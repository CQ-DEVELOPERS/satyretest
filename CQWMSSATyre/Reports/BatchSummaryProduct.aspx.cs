    using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;

public partial class Reports_StockOnHand : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["StorageUnitId"] = -1;
                Session["BatchId"] = -1;
                Session["FromLocationId"] = -1;
                Session["ToLocationId"] = -1;
                Session["LocationTypeId"] = -1;
            }
        }
        catch { }
    }

    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        try
        {
            Session["FromURL"] = "~/Reports/BatchSummaryProduct.aspx";
            Session["ReportName"] = "Batch Summary Report Product";
            ReportParameter[] RptParameters = new ReportParameter[10];

            RptParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ServerName", Session["ServerName"].ToString());
            RptParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("DatabaseName", Session["DatabaseName"].ToString());
            RptParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("UserName", Session["UserName"].ToString());
            RptParameters[3] = new ReportParameter("WarehouseId", Session["WarehouseId"].ToString());
            RptParameters[4] = new ReportParameter("BatchId", Session["BatchId"].ToString());
            RptParameters[5] = new ReportParameter("FromDate", Session["FromDate"].ToString());
            RptParameters[6] = new ReportParameter("ToDate", Session["ToDate"].ToString());
            RptParameters[7] = new ReportParameter("ManufactureArea", "-1");
            RptParameters[8] = new ReportParameter("ProductCode", Session["ProductCode"].ToString());
            RptParameters[9] = new ReportParameter("Product", Session["Product"].ToString());

            Session["ReportParameters"] = RptParameters;

            Response.Redirect("~/Reports/Report.aspx");

        }
        catch { }
    }
}
