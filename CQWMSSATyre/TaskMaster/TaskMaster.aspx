<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPage.master" AutoEventWireup="true"
    CodeFile="TaskMaster.aspx.cs" Inherits="TaskMaster_TaskMaster"
    Title="<%$ Resources:Default, CallOffQueryTitle %>" StylesheetTheme="Default" Theme="Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Common/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" runat="Server">
    <asp:Label ID="LabelHeading" runat="server" SkinID="PageTitle" Text="<%$ Resources:Default, CallOffQueryTitle %>"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" SkinID="AgendaTitle" Text="<%$ Resources:Default, CallOffQueryAgenda %>"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <style type="text/css">
        .filterDiv {
            margin: 20px 0px 10px 0px;
        }
    </style>
    <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <%--<telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="Textbox" />--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadFilter1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadFilter1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridCallOffSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinked" />
                    <telerik:AjaxUpdatedControl ControlID="FormViewCallOffHeader" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridComments" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSequence" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSearch" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridLinked">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridLinked" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridCallOffSequence">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSequence" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            <telerik:AjaxSetting AjaxControlID="RadGridComments">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridCallOffSearch" />
                    <telerik:AjaxUpdatedControl ControlID="RadGridComments" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ButtonSelect" EventName="Click">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGridComments" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div style="border: medium none #696969">
        <telerik:RadTabStrip runat="server" ID="RadTabStrip1" Orientation="HorizontalTop"
            SelectedIndex="0" MultiPageID="Tabs">
            <Tabs>
                <telerik:RadTab Text="<%$ Resources:Default, Search %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Lines %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Comments %>"></telerik:RadTab>
                <telerik:RadTab Text="<%$ Resources:Default, Shipping %>"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip><!--
            no spaces between the tabstrip and multipage, in order to remove unnecessary whitespace
            --><telerik:RadMultiPage runat="server" ID="Tabs" Height="100%" Width="100%" SelectedIndex="0" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="1pt">
            <telerik:RadPageView runat="server" ID="TabPanel1">
                <table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
                    <tr align="right">
                        <td>
                            <asp:Label ID="LabelComplete" runat="server" Text="% Complete"></asp:Label>
                            <telerik:RadTextBox ID="TextBoxFromPercent" runat="server" Text="0" InputType="Number" Width="90px"></telerik:RadTextBox>
                            <asp:Label ID="LabelTo" runat="server" Text=" to "></asp:Label>
                            <telerik:RadTextBox ID="TextBoxToPercent" runat="server" Text="99" InputType="Number" Width="90px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <asp:Label ID="LabelTaskList" runat="server" Text="Task"></asp:Label>
                            <telerik:RadTextBox ID="TextBoxTaskList" runat="server" Width="200px"></telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourcePriorityId"
                                DataTextField="Priority" DataValueField="PriorityId" Label="PriorityId" Width="200px">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr align="right">
                        <td>
                            <telerik:RadComboBox ID="DropDownListProjectId" runat="server" DataSourceID="ObjectDataSourceProjectId"
                                DataTextField="Project" DataValueField="ProjectId" Label="Project" Width="200px">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="DropDownListRequestTypeId" runat="server" DataSourceID="ObjectDataSourceRequestTypeId"
                                DataTextField="RequestType" DataValueField="RequestTypeId" Label="RequestType" Width="200px">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="DropDownListBillableId" runat="server" DataSourceID="ObjectDataSourceBillableId"
                                DataTextField="Billable" DataValueField="BillableId" Label="Billable" Width="200px">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr align="right">
                        <td>
                            <telerik:RadComboBox ID="DropDownListRaisedBy" runat="server" DataSourceID="ObjectDataSourceOperatorId"
                                DataTextField="Operator" DataValueField="OperatorId" Label="RaisedBy" Width="200px">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="DropDownListApprovedBy" runat="server" DataSourceID="ObjectDataSourceOperatorId"
                                DataTextField="Operator" DataValueField="OperatorId" Label="ApprovedBy" Width="200px">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="DropDownListAllocatedTo" runat="server" DataSourceID="ObjectDataSourceOperatorId"
                                DataTextField="Operator" DataValueField="OperatorId" Label="AllocatedTo" Width="200px">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr align="right">
                        <td>
                            <telerik:RadComboBox ID="DropDownListTester" runat="server" DataSourceID="ObjectDataSourceOperatorId"
                                DataTextField="Operator" DataValueField="OperatorId" Label="Tester" Width="200px">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="DropDownListCycleId" runat="server" DataSourceID="ObjectDataSourceCycleId"
                                DataTextField="Cycle" DataValueField="CycleId" Label="Cycle" Width="200px">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr align="right">
                        <td>
                            <asp:Label ID="LabelFromDate" runat="server" Text="<%$ Resources:Default, FromDate %>"></asp:Label>
                            <telerik:RadDatePicker ID="rdpFromDate" runat="server" Width="200px"></telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:Label ID="labeToDate" runat="server" Text="<%$ Resources:Default, ToDate %>"></asp:Label>
                            <telerik:RadDatePicker ID="rdpToDate" runat="server" Width="200px"></telerik:RadDatePicker>
                        </td>
                        <td>
                            <telerik:RadButton ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"></telerik:RadButton>
                        </td>
                    </tr>
                </table>
                <div class="filterDiv">
                    <telerik:RadFilter runat="server" ID="RadFilter1" FilterContainerID="RadGridCallOffSearch" ShowApplyButton="true">
                        <FieldEditors>
                            <telerik:RadFilterTextFieldEditor FieldName="RequestType" />
                            <telerik:RadFilterTextFieldEditor FieldName="Comments" />
                            <telerik:RadFilterDateFieldEditor FieldName="CreateDate" />
                            <telerik:RadFilterDateFieldEditor FieldName="TargetDate" />
                        </FieldEditors>
                    </telerik:RadFilter>
                </div>
                <telerik:RadGrid ID="RadGridCallOffSearch" runat="server" OnRowDataBound="RadGridCallOffSearch_RowDataBound"
                    AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true"
                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSourcePlanning"
                    OnSelectedIndexChanged="RadGridCallOffSearch_SelectedIndexChanged" PageSize="30" ShowGroupPanel="True" Height="500px" Skin="Metro"
                    OnItemCommand="RadGridCallOffSearch_ItemCommand" AllowFilteringByColumn="true">
                    <ExportSettings IgnorePaging="True" OpenInNewWindow="True">
                        <Pdf PageHeight="210mm" PageWidth="297mm" PageTitle="SushiBar menu" DefaultFontFamily="Arial Unicode MS"
                            PageBottomMargin="20mm" PageTopMargin="20mm" PageLeftMargin="20mm" PageRightMargin="20mm">
                        </Pdf>
                    </ExportSettings>
                    <PagerStyle Mode="NextPrevNumericAndAdvanced" EnableSEOPaging="True"></PagerStyle>
                    <MasterTableView DataKeyNames="TaskListId" DataSourceID="ObjectDataSourcePlanning"
                        CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage" IsFilterItemExpanded="false" AllowFilteringByColumn="true">
                        <CommandItemTemplate>
                            <telerik:RadToolBar runat="server" ID="RadToolBar1" OnButtonClick="RadToolBar1_ButtonClick">
                                <Items>
                                    <telerik:RadToolBarButton Text="Apply filter" CommandName="FilterRadGrid" ImageUrl="<%#GetFilterIcon() %>" ImagePosition="Right"></telerik:RadToolBarButton>
                                </Items>
                            </telerik:RadToolBar>
                        </CommandItemTemplate>
                        <Columns>
                            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn ReadOnly="True" DataField="TaskListId" HeaderText="#" SortExpression="TaskListId" UniqueName="TaskListId" AllowFiltering="true"></telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListPriorityId" ListTextField="Priority" ListValueField="PriorityId" DataSourceID="ObjectDataSourcePriorityId" DataField="PriorityId" HeaderText="Priority"></telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListProjectId" ListTextField="Project" ListValueField="ProjectId" DataSourceID="ObjectDataSourceProjectId" DataField="ProjectId" HeaderText="Project"></telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListRequestTypeId" ListTextField="RequestType" ListValueField="RequestTypeId" DataSourceID="ObjectDataSourceRequestTypeId" DataField="RequestTypeId" HeaderText="Request Type"></telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListBillableId" ListTextField="Billable" ListValueField="BillableId" DataSourceID="ObjectDataSourceBillableId" DataField="BillableId" HeaderText="Billable"></telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn DataField="TaskList" HeaderText="TaskList" SortExpression="TaskList" UniqueName="Task"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Comments" HeaderText="Comments" SortExpression="Comments" UniqueName="Comment"></telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListRaisedBy" ListTextField="Operator" ListValueField="OperatorId" DataSourceID="ObjectDataSourceOperatorId" DataField="RaisedBy" HeaderText="Raised By"></telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListApprovedBy" ListTextField="Operator" ListValueField="OperatorId" DataSourceID="ObjectDataSourceOperatorId" DataField="ApprovedBy" HeaderText="Approved By"></telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListAllocatedTo" ListTextField="Operator" ListValueField="OperatorId" DataSourceID="ObjectDataSourceOperatorId" DataField="AllocatedTo" HeaderText="Allocated To"></telerik:GridDropDownColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListTester" ListTextField="Operator" ListValueField="OperatorId" DataSourceID="ObjectDataSourceOperatorId" DataField="Tester" HeaderText="Tester"></telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn DataField="EstimateHours" HeaderText="Estimate Hrs" SortExpression="EstimateHours" UniqueName="EstimateHours"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ActualHours" HeaderText="Actual Hrs" SortExpression="ActualHours" UniqueName="ActualHours"></telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn DataField="CreateDate" HeaderText="Create Date"></telerik:GridDateTimeColumn>
                            <telerik:GridDateTimeColumn DataField="TargetDate" HeaderText="Target Date"></telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" SortExpression="Percentage" UniqueName="%"></telerik:GridBoundColumn>
                            <telerik:GridDropDownColumn UniqueName="DropDownListCycleId" ListTextField="Cycle" ListValueField="CycleId" DataSourceID="ObjectDataSourceCycleId" DataField="CycleId" HeaderText="Cycle"></telerik:GridDropDownColumn>
                            <telerik:GridBoundColumn DataField="OrderBy" HeaderText="Order By" SortExpression="OrderBy" UniqueName="OrderBy"></telerik:GridBoundColumn>
                            <telerik:GridButtonColumn Text="<%$ Resources:Default, Upload %>" HeaderText="<%$ Resources:Default, Upload %>" CommandName="Upload"></telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowColumnHide="True" EnablePostBackOnRowClick="True" 
                        ReorderColumnsOnClient="True" AllowDragToGroup="True" 
                        AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        <Resizing AllowColumnResize="True" />
                    </ClientSettings>
                    <GroupingSettings ShowUnGroupButton="True" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourcePlanning" runat="server" TypeName="Tasks"
                    DeleteMethod="DeleteTaskList"
                    InsertMethod="InsertTaskList"
                    SelectMethod="SearchTaskList"
                    UpdateMethod="UpdateTaskList">
                    <SelectParameters>
                        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="TaskListId" Type="Int32" DefaultValue="-1" />
                        <asp:ControlParameter ControlID="DropDownListPriorityId" DefaultValue="-1" Name="PriorityId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DropDownListProjectId" DefaultValue="-1" Name="ProjectId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DropDownListRequestTypeId" DefaultValue="-1" Name="RequestTypeId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DropDownListBillableId" DefaultValue="-1" Name="BillableId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="TextBoxTaskList" DefaultValue="%" Name="TaskList" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="DropDownListRaisedBy" DefaultValue="-1" Name="RaisedBy" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DropDownListApprovedBy" DefaultValue="-1" Name="ApprovedBy" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DropDownListAllocatedTo" DefaultValue="-1" Name="AllocatedTo" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DropDownListTester" DefaultValue="-1" Name="Tester" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DropDownListCycleId" DefaultValue="-1" Name="CycleId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="TextBoxFromPercent" DefaultValue="0" Name="FromPercent" PropertyName="Text" Type="Decimal" />
                        <asp:ControlParameter ControlID="TextBoxToPercent" DefaultValue="100" Name="ToPercent" PropertyName="Text" Type="Decimal" />
                        <asp:ControlParameter ControlID="rdpFromDate" Name="FromDate" />
                        <asp:ControlParameter ControlID="rdpToDate" Name="ToDate"  />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="TaskListId" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Name="TaskListId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="ProjectId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="RequestTypeId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="BillableId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="TaskList" Type="String" />
                        <asp:Parameter Name="Comments" Type="String" />
                        <asp:Parameter Name="RaisedBy" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="ApprovedBy" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="AllocatedTo" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="Tester" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="EstimateHours" Type="Double" />
                        <asp:Parameter Name="ActualHours" Type="Double" />
                        <asp:Parameter Name="CreateDate" Type="DateTime" />
                        <asp:Parameter Name="TargetDate" Type="DateTime" />
                        <asp:Parameter Name="Percentage" Type="Double" />
                        <asp:Parameter Name="CycleId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="OrderBy" Type="Int32" DefaultValue="-1" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Direction="InputOutput" Name="TaskListId" Type="Int32" />
                        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="ProjectId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="RequestTypeId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="BillableId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="TaskList" Type="String" />
                        <asp:Parameter Name="Comments" Type="String" />
                        <asp:Parameter Name="RaisedBy" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="ApprovedBy" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="AllocatedTo" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="Tester" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="EstimateHours" Type="Double" />
                        <asp:Parameter Name="ActualHours" Type="Double" />
                        <asp:Parameter Name="CreateDate" Type="DateTime" />
                        <asp:Parameter Name="TargetDate" Type="DateTime" />
                        <asp:Parameter Name="Percentage" Type="Double" />
                        <asp:Parameter Name="CycleId" Type="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="OrderBy" Type="Int32" DefaultValue="-1" />
                    </InsertParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
            <telerik:RadPageView runat="Server" ID="TabPanel2">
                <telerik:RadGrid ID="RadGridComments" runat="server" DataSourceID="ObjectDataSourceComents"
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" AllowAutomaticInserts="true" Skin="Metro">
                    <PagerStyle Mode="NumericPages"></PagerStyle>
                    <MasterTableView DataKeyNames="CommentId"
                        CommandItemDisplay="Top" DataSourceID="ObjectDataSourceComents"
                        AutoGenerateColumns="false" InsertItemDisplay="Top"
                        InsertItemPageIndexAction="ShowItemOnFirstPage">
                        <Columns>
                            <telerik:GridBoundColumn DataField="CreateDate" HeaderText="<%$ Resources:Default, CreateDate %>" ReadOnly="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Operator" HeaderText="<%$ Resources:Default, Operator %>" ReadOnly="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Comment" HeaderText="<%$ Resources:Default, Comments %>"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Resizing AllowColumnResize="True" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:ObjectDataSource ID="ObjectDataSourceComents" runat="server" TypeName="Comment"
                    SelectMethod="SelectComment" InsertMethod="InsertComment" UpdateMethod="UpdateComment" DeleteMethod="DeleteComment">
                    <SelectParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="tableId" Type="Int32" SessionField="tableId" />
                        <asp:SessionParameter Name="tableName" Type="String" SessionField="tableName" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:SessionParameter Name="tableId" Type="Int32" SessionField="tableId" />
                        <asp:SessionParameter Name="tableName" Type="String" SessionField="tableName" />
                        <asp:Parameter Type="String" Name="comment"></asp:Parameter>
                        <asp:SessionParameter Type="Int32" Name="operatorId" SessionField="OperatorId"></asp:SessionParameter>
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="commentId"></asp:Parameter>
                        <asp:Parameter Type="String" Name="comment"></asp:Parameter>
                        <asp:SessionParameter Type="Int32" Name="operatorId" SessionField="OperatorId"></asp:SessionParameter>
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        <asp:Parameter Type="Int32" Name="commentId"></asp:Parameter>
                    </DeleteParameters>
                </asp:ObjectDataSource>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    
    
<asp:ObjectDataSource ID="ObjectDataSourcePriorityId" runat="server" TypeName="StaticInfoPriority"
    SelectMethod="ParameterPriority">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceProjectId" runat="server" TypeName="StaticInfoProject"
    SelectMethod="ParameterProject">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceRequestTypeId" runat="server" TypeName="StaticInfoRequestType"
    SelectMethod="ParameterRequestType">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceBillableId" runat="server" TypeName="StaticInfoBillable"
    SelectMethod="ParameterBillable">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceOperatorId" runat="server" TypeName="StaticInfoOperator"
    SelectMethod="ParameterOperator">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceCycleId" runat="server" TypeName="StaticInfoCycle"
    SelectMethod="ParameterCycle">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
</asp:Content>
