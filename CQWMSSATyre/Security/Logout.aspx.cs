﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Security_Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            LogonCredentials lc = new LogonCredentials();

            lc.UserLoggedOut(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], "Desktop");
            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Redirect("~/Default.aspx");
        }
        catch (Exception ex)
        {
            Session["LogonErrorMessage"] = ex.Message;
        }
    }
}
