<%@ Page
    Language="C#"
    MasterPageFile="~/MasterPages/Blank.master"
    AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs"
    Inherits="Security_ChangePassword"
    Title="Change Password"
    StylesheetTheme="Default"
    Theme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeading" Runat="Server">
    <asp:Label ID="LabelTitle" runat="server" Text="Change Your Password" SkinID="PageTitle"></asp:Label>
    <br />
    <asp:Label ID="LabelAgenda" runat="server" Text="User Credentials" SkinID="AgendaTitle"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolderBody">
    <asp:ChangePassword ID="ChangePassword1" runat="server" ContinueDestinationPageUrl="~/Default.aspx" CancelDestinationPageUrl="~/Default.aspx">
    </asp:ChangePassword>
</asp:Content>

