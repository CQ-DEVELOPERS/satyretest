using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Security_PrintExternalLabel : System.Web.UI.Page
{
    #region Private Variables
    private string result = "";
    private string theErrMethod = "";
    #endregion Private Variables

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {
            if (Context.Session.IsNewSession)
            {
                if (Request.QueryString["UserName"] != null && Request.QueryString["Password"] != null)
                {
                    Response.Redirect("Login.aspx?UserName=" + Request.QueryString["UserName"]
                        + "&Password=" + Request.QueryString["Password"]
                        + "&ReturnUrl=" + Request.Url);
                }
            }

            Master.MsgText = "Page loaded successfully"; Master.ErrorText = "";

            ArrayList checkedList = new ArrayList();

            Session["checkedList"] = checkedList;

            if (Request.QueryString["LabelName"] != null)
            {
                Session["LabelName"] = Request.QueryString["LabelName"];
            }

            if (Request.QueryString["LabelCopies"] != null)
            {
                try {Session["LabelCopies"] = int.Parse(Request.QueryString["LabelCopies"]);}
                catch{Session["LabelCopies"] = 1;}
            }

            if (Request.QueryString["Key"] != null)
            {
                try
                {
                    checkedList.Add(int.Parse(Request.QueryString["Key"].ToString()));
                }
                catch
                {
                    if (checkedList.Count < 1)
                        return;
                }

                Session["FromURL"] = "~/Security/PrintExternalLabel.aspx";

                if (Session["Printer"] == null)
                    Session["Printer"] = "";

                if (Session["Printer"].ToString() == "")
                    Response.Redirect("../Common/NLLabels.aspx");
                else
                {
                    Session["Printing"] = true;
                    Response.Redirect("~/Common/NLPrint.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("PrintExternalLabel " + "_" + ex.Message.ToString());
            Master.ErrorText = result;
        }
    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
    }

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "PrintExternalLabel", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "PrintExternalLabel", theErrMethod, exMsg.Message.ToString());

                Master.ErrorText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
