<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutboundDocumentTypeGridView.ascx.cs" Inherits="UserControls_OutboundDocumentTypeGridView" %>
 
<table style="background-color:lightgray; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <telerik:RadTextBox ID="TextBoxOutboundDocumentTypeCode" runat="server" Label="OutboundDocumentTypeCode" Width="300px" Skin="Metro"></telerik:RadTextBox>
        </td>
        <td>
            <telerik:RadTextBox ID="TextBoxOutboundDocumentType" runat="server" Label="OutboundDocumentType" Width="300px" Skin="Metro"></telerik:RadTextBox>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListPriorityId" runat="server" DataSourceID="ObjectDataSourceDDLPriorityId"
                DataTextField="Priority" DataValueField="PriorityId" Label="Priority" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLPriorityId" runat="server" TypeName="StaticInfoPriority"
                SelectMethod="ParameterPriority">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadComboBox ID="DropDownListCheckingLane" runat="server" DataSourceID="ObjectDataSourceDDLCheckingLane"
                DataTextField="Location" DataValueField="LocationId" Label="Location" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLCheckingLane" runat="server" TypeName="StaticInfoLocation"
                SelectMethod="ParameterLocation">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListDespatchBay" runat="server" DataSourceID="ObjectDataSourceDDLDespatchBay"
                DataTextField="Location" DataValueField="LocationId" Label="Location" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLDespatchBay" runat="server" TypeName="StaticInfoLocation"
                SelectMethod="ParameterLocation">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadButton ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"></telerik:RadButton>
        </td>
    </tr>
</table>
 
<telerik:RadGrid ID="RadGridOutboundDocumentType" runat="server" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AutoGenerateDeleteColumn="true"
    AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="30" DataSourceID="ObjectDataSourceOutboundDocumentType" OnSelectedIndexChanged="RadGridOutboundDocumentType_SelectedIndexChanged" Skin="Metro">
<PagerStyle Mode="NumericPages"></PagerStyle>
<MasterTableView DataKeyNames="
OutboundDocumentTypeId
" DataSourceID="ObjectDataSourceOutboundDocumentType"
    CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
    InsertItemPageIndexAction="ShowItemOnFirstPage">
        <Columns>
            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="OutboundDocumentTypeId" HeaderText="OutboundDocumentTypeId" SortExpression="OutboundDocumentTypeId" UniqueName="OutboundDocumentTypeId"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="OutboundDocumentTypeCode" HeaderText="OutboundDocumentTypeCode" SortExpression="OutboundDocumentTypeCode" UniqueName="OutboundDocumentTypeCode"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="OutboundDocumentType" HeaderText="OutboundDocumentType" SortExpression="OutboundDocumentType" UniqueName="OutboundDocumentType"></telerik:GridBoundColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListPriorityId" ListTextField="Priority" ListValueField="PriorityId" DataSourceID="ObjectDataSourcePriorityId" DataField="PriorityId" HeaderText="Priority">
            </telerik:GridDropDownColumn>
                        <telerik:GridCheckBoxColumn DataField="AlternatePallet" HeaderText="AlternatePallet" SortExpression="AlternatePallet" UniqueName="AlternatePallet"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="SubstitutePallet" HeaderText="SubstitutePallet" SortExpression="SubstitutePallet" UniqueName="SubstitutePallet"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="AutoRelease" HeaderText="AutoRelease" SortExpression="AutoRelease" UniqueName="AutoRelease"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="AddToShipment" HeaderText="AddToShipment" SortExpression="AddToShipment" UniqueName="AddToShipment"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="MultipleOnShipment" HeaderText="MultipleOnShipment" SortExpression="MultipleOnShipment" UniqueName="MultipleOnShipment"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="LIFO" HeaderText="LIFO" SortExpression="LIFO" UniqueName="LIFO"></telerik:GridCheckBoxColumn>
            <telerik:GridBoundColumn DataField="MinimumShelfLife" HeaderText="MinimumShelfLife" SortExpression="MinimumShelfLife" UniqueName="MinimumShelfLife"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="AreaType" HeaderText="AreaType" SortExpression="AreaType" UniqueName="AreaType"></telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="AutoSendup" HeaderText="AutoSendup" SortExpression="AutoSendup" UniqueName="AutoSendup"></telerik:GridCheckBoxColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListCheckingLane" ListTextField="Location" ListValueField="LocationId" DataSourceID="ObjectDataSourceLocationId" DataField="CheckingLane" HeaderText="Location">
            </telerik:GridDropDownColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListDespatchBay" ListTextField="Location" ListValueField="LocationId" DataSourceID="ObjectDataSourceLocationId" DataField="DespatchBay" HeaderText="Location">
            </telerik:GridDropDownColumn>
                        <telerik:GridCheckBoxColumn DataField="PlanningComplete" HeaderText="PlanningComplete" SortExpression="PlanningComplete" UniqueName="PlanningComplete"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="AutoInvoice" HeaderText="AutoInvoice" SortExpression="AutoInvoice" UniqueName="AutoInvoice"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="Backorder" HeaderText="Backorder" SortExpression="Backorder" UniqueName="Backorder"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="AutoCheck" HeaderText="AutoCheck" SortExpression="AutoCheck" UniqueName="AutoCheck"></telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="ReserveBatch" HeaderText="ReserveBatch" SortExpression="ReserveBatch" UniqueName="ReserveBatch"></telerik:GridCheckBoxColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
       </ClientSettings>
    <GroupingSettings ShowUnGroupButton="True" />
</telerik:RadGrid>
<asp:ObjectDataSource ID="ObjectDataSourcePriorityId" runat="server" TypeName="StaticInfoPriority"
    SelectMethod="ParameterPriority">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceCheckingLane" runat="server" TypeName="StaticInfoLocation"
    SelectMethod="ParameterLocation">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceDespatchBay" runat="server" TypeName="StaticInfoLocation"
    SelectMethod="ParameterLocation">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
 
<asp:ObjectDataSource ID="ObjectDataSourceOutboundDocumentType" runat="server" TypeName="StaticInfoOutboundDocumentType"
    DeleteMethod="DeleteOutboundDocumentType"
    InsertMethod="InsertOutboundDocumentType"
    SelectMethod="SearchOutboundDocumentType"
    UpdateMethod="UpdateOutboundDocumentType">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="TextBoxOutboundDocumentTypeCode" DefaultValue="%" Name="OutboundDocumentTypeCode" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="TextBoxOutboundDocumentType" DefaultValue="%" Name="OutboundDocumentType" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="DropDownListPriorityId" DefaultValue="-1" Name="PriorityId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListCheckingLane" DefaultValue="-1" Name="CheckingLane" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListDespatchBay" DefaultValue="-1" Name="DespatchBay" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OutboundDocumentTypeCode" Type="String" />
        <asp:Parameter Name="OutboundDocumentType" Type="String" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AlternatePallet" Type="Boolean" />
        <asp:Parameter Name="SubstitutePallet" Type="Boolean" />
        <asp:Parameter Name="AutoRelease" Type="Boolean" />
        <asp:Parameter Name="AddToShipment" Type="Boolean" />
        <asp:Parameter Name="MultipleOnShipment" Type="Boolean" />
        <asp:Parameter Name="LIFO" Type="Boolean" />
        <asp:Parameter Name="MinimumShelfLife" Type="Decimal" />
        <asp:Parameter Name="AreaType" Type="String" />
        <asp:Parameter Name="AutoSendup" Type="Boolean" />
        <asp:Parameter Name="CheckingLane" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="DespatchBay" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PlanningComplete" Type="Boolean" />
        <asp:Parameter Name="AutoInvoice" Type="Boolean" />
        <asp:Parameter Name="Backorder" Type="Boolean" />
        <asp:Parameter Name="AutoCheck" Type="Boolean" />
        <asp:Parameter Name="ReserveBatch" Type="Boolean" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="OutboundDocumentTypeId" Type="Int32" />
        <asp:Parameter Name="OutboundDocumentTypeCode" Type="String" />
        <asp:Parameter Name="OutboundDocumentType" Type="String" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AlternatePallet" Type="Boolean" />
        <asp:Parameter Name="SubstitutePallet" Type="Boolean" />
        <asp:Parameter Name="AutoRelease" Type="Boolean" />
        <asp:Parameter Name="AddToShipment" Type="Boolean" />
        <asp:Parameter Name="MultipleOnShipment" Type="Boolean" />
        <asp:Parameter Name="LIFO" Type="Boolean" />
        <asp:Parameter Name="MinimumShelfLife" Type="Decimal" />
        <asp:Parameter Name="AreaType" Type="String" />
        <asp:Parameter Name="AutoSendup" Type="Boolean" />
        <asp:Parameter Name="CheckingLane" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="DespatchBay" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="PlanningComplete" Type="Boolean" />
        <asp:Parameter Name="AutoInvoice" Type="Boolean" />
        <asp:Parameter Name="Backorder" Type="Boolean" />
        <asp:Parameter Name="AutoCheck" Type="Boolean" />
        <asp:Parameter Name="ReserveBatch" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
