<%@ Control Language="C#" AutoEventWireup="true" CodeFile="COATestFormView.ascx.cs" Inherits="UserControls_COATestFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>COATest</h3>
<asp:FormView ID="FormViewCOATest" runat="server" DataKeyNames="COATestId" DataSourceID="ObjectDataSourceCOATest" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    COAId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCOAId" runat="server" DataSourceID="ObjectDataSourceDDLCOAId"
                        DataTextField="COA" DataValueField="COAId" SelectedValue='<%# Bind("COAId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCOAId" runat="server" TypeName="StaticInfoCOA"
                        SelectMethod="ParameterCOA">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    TestId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTestId" runat="server" DataSourceID="ObjectDataSourceDDLTestId"
                        DataTextField="Test" DataValueField="TestId" SelectedValue='<%# Bind("TestId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLTestId" runat="server" TypeName="StaticInfoTest"
                        SelectMethod="ParameterTest">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ResultId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListResultId" runat="server" DataSourceID="ObjectDataSourceDDLResultId"
                        DataTextField="Result" DataValueField="ResultId" SelectedValue='<%# Bind("ResultId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLResultId" runat="server" TypeName="StaticInfoResult"
                        SelectMethod="ParameterResult">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    MethodId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMethodId" runat="server" DataSourceID="ObjectDataSourceDDLMethodId"
                        DataTextField="Method" DataValueField="MethodId" SelectedValue='<%# Bind("MethodId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMethodId" runat="server" TypeName="StaticInfoMethod"
                        SelectMethod="ParameterMethod">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ResultCode:
                </td>
                <td>
                    <asp:TextBox ID="ResultCodeTextBox" runat="server" Text='<%# Bind("ResultCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Result:
                </td>
                <td>
                    <asp:TextBox ID="ResultTextBox" runat="server" Text='<%# Bind("Result") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Pass:
                </td>
                <td>
                    <asp:CheckBox ID="PassCheckBox" runat="server" Checked='<%# Bind("Pass") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StartRange:
                </td>
                <td>
                    <asp:TextBox ID="StartRangeTextBox" runat="server" Text='<%# Bind("StartRange") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    EndRange:
                </td>
                <td>
                    <asp:TextBox ID="EndRangeTextBox" runat="server" Text='<%# Bind("EndRange") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    COAId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCOAId" runat="server" DataSourceID="ObjectDataSourceDDLCOAId"
                        DataTextField="COA" DataValueField="COAId" SelectedValue='<%# Bind("COAId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCOAId" runat="server" TypeName="StaticInfoCOA"
                        SelectMethod="ParameterCOA">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    TestId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTestId" runat="server" DataSourceID="ObjectDataSourceDDLTestId"
                        DataTextField="Test" DataValueField="TestId" SelectedValue='<%# Bind("TestId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLTestId" runat="server" TypeName="StaticInfoTest"
                        SelectMethod="ParameterTest">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ResultId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListResultId" runat="server" DataSourceID="ObjectDataSourceDDLResultId"
                        DataTextField="Result" DataValueField="ResultId" SelectedValue='<%# Bind("ResultId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLResultId" runat="server" TypeName="StaticInfoResult"
                        SelectMethod="ParameterResult">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    MethodId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMethodId" runat="server" DataSourceID="ObjectDataSourceDDLMethodId"
                        DataTextField="Method" DataValueField="MethodId" SelectedValue='<%# Bind("MethodId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMethodId" runat="server" TypeName="StaticInfoMethod"
                        SelectMethod="ParameterMethod">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ResultCode:
                </td>
                <td>
                    <asp:TextBox ID="ResultCodeTextBox" runat="server" Text='<%# Bind("ResultCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Result:
                </td>
                <td>
                    <asp:TextBox ID="ResultTextBox" runat="server" Text='<%# Bind("Result") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Pass:
                </td>
                <td>
                    <asp:CheckBox ID="PassCheckBox" runat="server" Checked='<%# Bind("Pass") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StartRange:
                </td>
                <td>
                    <asp:TextBox ID="StartRangeTextBox" runat="server" Text='<%# Bind("StartRange") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    EndRange:
                </td>
                <td>
                    <asp:TextBox ID="EndRangeTextBox" runat="server" Text='<%# Bind("EndRange") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    COA:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCOAId" runat="server" DataSourceID="ObjectDataSourceDDLCOAId"
                        DataTextField="COA" DataValueField="COAId" SelectedValue='<%# Bind("COAId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCOAId" runat="server" TypeName="StaticInfoCOA"
                        SelectMethod="ParameterCOA">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Test:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTestId" runat="server" DataSourceID="ObjectDataSourceDDLTestId"
                        DataTextField="Test" DataValueField="TestId" SelectedValue='<%# Bind("TestId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLTestId" runat="server" TypeName="StaticInfoTest"
                        SelectMethod="ParameterTest">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Result:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListResultId" runat="server" DataSourceID="ObjectDataSourceDDLResultId"
                        DataTextField="Result" DataValueField="ResultId" SelectedValue='<%# Bind("ResultId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLResultId" runat="server" TypeName="StaticInfoResult"
                        SelectMethod="ParameterResult">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Method:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMethodId" runat="server" DataSourceID="ObjectDataSourceDDLMethodId"
                        DataTextField="Method" DataValueField="MethodId" SelectedValue='<%# Bind("MethodId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMethodId" runat="server" TypeName="StaticInfoMethod"
                        SelectMethod="ParameterMethod">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ResultCode:
                </td>
                <td>
                    <asp:Label ID="ResultCodeLabel" runat="server" Text='<%# Bind("ResultCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Result:
                </td>
                <td>
                    <asp:Label ID="ResultLabel" runat="server" Text='<%# Bind("Result") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Pass:
                </td>
                <td>
                    <asp:Label ID="PassLabel" runat="server" Text='<%# Bind("Pass") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StartRange:
                </td>
                <td>
                    <asp:Label ID="StartRangeLabel" runat="server" Text='<%# Bind("StartRange") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    EndRange:
                </td>
                <td>
                    <asp:Label ID="EndRangeLabel" runat="server" Text='<%# Bind("EndRange") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/COATestGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceCOATest" runat="server" TypeName="StaticInfoCOATest"
    SelectMethod="GetCOATest"
    DeleteMethod="DeleteCOATest"
    UpdateMethod="UpdateCOATest"
    InsertMethod="InsertCOATest">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COATestId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COATestId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TestId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MethodId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Result" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Pass" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="StartRange" Type="Double" DefaultValue="-1" />
        <asp:Parameter Name="EndRange" Type="Double" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="COATestId" QueryStringField="COATestId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COATestId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TestId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MethodId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultCode" Type="String" />
        <asp:Parameter Name="Result" Type="String" />
        <asp:Parameter Name="Pass" Type="Boolean" />
        <asp:Parameter Name="StartRange" Type="Double" />
        <asp:Parameter Name="EndRange" Type="Double" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
