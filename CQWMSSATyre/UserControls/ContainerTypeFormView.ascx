<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContainerTypeFormView.ascx.cs" Inherits="UserControls_ContainerTypeFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>ContainerType</h3>
<asp:FormView ID="FormViewContainerType" runat="server" DataKeyNames="ContainerTypeId" DataSourceID="ObjectDataSourceContainerType" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    ContainerType:
                </td>
                <td>
                    <asp:TextBox ID="ContainerTypeTextBox" runat="server" Text='<%# Bind("ContainerType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ContainerTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="ContainerTypeCodeTextBox" runat="server" Text='<%# Bind("ContainerTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    ContainerType:
                </td>
                <td>
                    <asp:TextBox ID="ContainerTypeTextBox" runat="server" Text='<%# Bind("ContainerType") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ContainerTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="ContainerTypeCodeTextBox" runat="server" Text='<%# Bind("ContainerTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    ContainerType:
                </td>
                <td>
                    <asp:Label ID="ContainerTypeLabel" runat="server" Text='<%# Bind("ContainerType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ContainerTypeCode:
                </td>
                <td>
                    <asp:Label ID="ContainerTypeCodeLabel" runat="server" Text='<%# Bind("ContainerTypeCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ContainerTypeGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceContainerType" runat="server" TypeName="StaticInfoContainerType"
    SelectMethod="GetContainerType"
    DeleteMethod="DeleteContainerType"
    UpdateMethod="UpdateContainerType"
    InsertMethod="InsertContainerType">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ContainerTypeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ContainerTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ContainerType" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="ContainerTypeCode" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ContainerTypeId" QueryStringField="ContainerTypeId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ContainerTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ContainerType" Type="String" />
        <asp:Parameter Name="ContainerTypeCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
