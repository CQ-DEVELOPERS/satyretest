<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReceiptFormView.ascx.cs" Inherits="UserControls_ReceiptFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Receipt</h3>
<asp:FormView ID="FormViewReceipt" runat="server" DataKeyNames="ReceiptId" DataSourceID="ObjectDataSourceReceipt" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    InboundDocument:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundDocument" runat="server" DataSourceID="ObjectDataSourceDDLInboundDocument"
                        DataTextField="InboundDocumentId" DataValueField="InboundDocumentId" SelectedValue='<%# Bind("InboundDocumentId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundDocument" runat="server"
                        TypeName="StaticInfoInboundDocument" SelectMethod="ParameterInboundDocument">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListInboundDocument" ErrorMessage="* Please enter InboundDocumentId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourceDDLPriority"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriority" runat="server"
                        TypeName="StaticInfoPriority" SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListPriority" ErrorMessage="* Please enter PriorityId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryNoteNumber:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryNoteNumberTextBox" runat="server" Text='<%# Bind("DeliveryNoteNumber") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryDateTextBox" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SealNumber:
                </td>
                <td>
                    <asp:TextBox ID="SealNumberTextBox" runat="server" Text='<%# Bind("SealNumber") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    VehicleRegistration:
                </td>
                <td>
                    <asp:TextBox ID="VehicleRegistrationTextBox" runat="server" Text='<%# Bind("VehicleRegistration") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Remarks:
                </td>
                <td>
                    <asp:TextBox ID="RemarksTextBox" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CreditAdviceIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="CreditAdviceIndicatorCheckBox" runat="server" Checked='<%# Bind("CreditAdviceIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Delivery:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryTextBox" runat="server" Text='<%# Bind("Delivery") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="DeliveryTextBox" ErrorMessage="* Please enter Delivery"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="DeliveryTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    InboundDocument:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundDocument" runat="server" DataSourceID="ObjectDataSourceDDLInboundDocument"
                        DataTextField="InboundDocument" DataValueField="InboundDocumentId" SelectedValue='<%# Bind("InboundDocumentId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundDocument" runat="server"
                        TypeName="StaticInfoInboundDocument" SelectMethod="ParameterInboundDocument">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListInboundDocument" ErrorMessage="* Please enter InboundDocumentId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourceDDLPriority"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriority" runat="server"
                        TypeName="StaticInfoPriority" SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListPriority" ErrorMessage="* Please enter PriorityId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryNoteNumber:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryNoteNumberTextBox" runat="server" Text='<%# Bind("DeliveryNoteNumber") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryDateTextBox" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    SealNumber:
                </td>
                <td>
                    <asp:TextBox ID="SealNumberTextBox" runat="server" Text='<%# Bind("SealNumber") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    VehicleRegistration:
                </td>
                <td>
                    <asp:TextBox ID="VehicleRegistrationTextBox" runat="server" Text='<%# Bind("VehicleRegistration") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Remarks:
                </td>
                <td>
                    <asp:TextBox ID="RemarksTextBox" runat="server" Text='<%# Bind("Remarks") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    CreditAdviceIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="CreditAdviceIndicatorCheckBox" runat="server" Checked='<%# Bind("CreditAdviceIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Delivery:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryTextBox" runat="server" Text='<%# Bind("Delivery") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="DeliveryTextBox" ErrorMessage="* Please enter Delivery"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="DeliveryTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    InboundDocument:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundDocument" runat="server" DataSourceID="ObjectDataSourceDDLInboundDocument"
                        DataTextField="InboundDocument" DataValueField="InboundDocumentId" SelectedValue='<%# Bind("InboundDocumentId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundDocument" runat="server"
                        TypeName="StaticInfoInboundDocument" SelectMethod="ParameterInboundDocument">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="ObjectDataSourceDDLPriority"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLPriority" runat="server"
                        TypeName="StaticInfoPriority" SelectMethod="ParameterPriority">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListWarehouse" runat="server" DataSourceID="ObjectDataSourceDDLWarehouse"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("WarehouseId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLWarehouse" runat="server"
                        TypeName="StaticInfoWarehouse" SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryNoteNumber:
                </td>
                <td>
                    <asp:Label ID="DeliveryNoteNumberLabel" runat="server" Text='<%# Bind("DeliveryNoteNumber") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:Label ID="DeliveryDateLabel" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    SealNumber:
                </td>
                <td>
                    <asp:Label ID="SealNumberLabel" runat="server" Text='<%# Bind("SealNumber") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    VehicleRegistration:
                </td>
                <td>
                    <asp:Label ID="VehicleRegistrationLabel" runat="server" Text='<%# Bind("VehicleRegistration") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Remarks:
                </td>
                <td>
                    <asp:Label ID="RemarksLabel" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CreditAdviceIndicator:
                </td>
                <td>
                    <asp:Label ID="CreditAdviceIndicatorLabel" runat="server" Text='<%# Bind("CreditAdviceIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Delivery:
                </td>
                <td>
                    <asp:Label ID="DeliveryLabel" runat="server" Text='<%# Bind("Delivery") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ReceiptGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceReceipt" runat="server" TypeName="StaticInfoReceipt"
    SelectMethod="GetReceipt"
    DeleteMethod="DeleteReceipt"
    UpdateMethod="UpdateReceipt"
    InsertMethod="InsertReceipt">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
        <asp:Parameter Name="InboundDocumentId" Type="Int32" />
        <asp:Parameter Name="PriorityId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="DeliveryNoteNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="SealNumber" Type="String" />
        <asp:Parameter Name="VehicleRegistration" Type="String" />
        <asp:Parameter Name="Remarks" Type="String" />
        <asp:Parameter Name="CreditAdviceIndicator" Type="Boolean" />
        <asp:Parameter Name="Delivery" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ReceiptId" QueryStringField="ReceiptId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
        <asp:Parameter Name="InboundDocumentId" Type="Int32" />
        <asp:Parameter Name="PriorityId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="DeliveryNoteNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="SealNumber" Type="String" />
        <asp:Parameter Name="VehicleRegistration" Type="String" />
        <asp:Parameter Name="Remarks" Type="String" />
        <asp:Parameter Name="CreditAdviceIndicator" Type="Boolean" />
        <asp:Parameter Name="Delivery" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
