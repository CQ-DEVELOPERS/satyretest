<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InboundDocumentFormView.ascx.cs" Inherits="UserControls_InboundDocumentFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>InboundDocument</h3>
<asp:FormView ID="FormViewInboundDocument" runat="server" DataKeyNames="InboundDocumentId" DataSourceID="ObjectDataSourceInboundDocument" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    InboundDocumentType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundDocumentType" runat="server" DataSourceID="ObjectDataSourceDDLInboundDocumentType"
                        DataTextField="InboundDocumentType" DataValueField="InboundDocumentTypeId" SelectedValue='<%# Bind("InboundDocumentTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundDocumentType" runat="server"
                        TypeName="StaticInfoInboundDocumentType" SelectMethod="ParameterInboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListInboundDocumentType" ErrorMessage="* Please enter InboundDocumentTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompany" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompany"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompany" runat="server"
                        TypeName="StaticInfoExternalCompany" SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListExternalCompany" ErrorMessage="* Please enter ExternalCompanyId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OrderNumber:
                </td>
                <td>
                    <asp:TextBox ID="OrderNumberTextBox" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="OrderNumberTextBox" ErrorMessage="* Please enter OrderNumber"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryDateTextBox" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DeliveryDateTextBox" ErrorMessage="* Please enter DeliveryDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="CreateDateTextBox" ErrorMessage="* Please enter CreateDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ModifiedDate:
                </td>
                <td>
                    <asp:TextBox ID="ModifiedDateTextBox" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    InboundDocumentType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundDocumentType" runat="server" DataSourceID="ObjectDataSourceDDLInboundDocumentType"
                        DataTextField="InboundDocumentType" DataValueField="InboundDocumentTypeId" SelectedValue='<%# Bind("InboundDocumentTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundDocumentType" runat="server"
                        TypeName="StaticInfoInboundDocumentType" SelectMethod="ParameterInboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListInboundDocumentType" ErrorMessage="* Please enter InboundDocumentTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompany" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompany"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompany" runat="server"
                        TypeName="StaticInfoExternalCompany" SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListExternalCompany" ErrorMessage="* Please enter ExternalCompanyId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OrderNumber:
                </td>
                <td>
                    <asp:TextBox ID="OrderNumberTextBox" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="OrderNumberTextBox" ErrorMessage="* Please enter OrderNumber"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryDateTextBox" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DeliveryDateTextBox" ErrorMessage="* Please enter DeliveryDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="CreateDateTextBox" ErrorMessage="* Please enter CreateDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ModifiedDate:
                </td>
                <td>
                    <asp:TextBox ID="ModifiedDateTextBox" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    InboundDocumentType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundDocumentType" runat="server" DataSourceID="ObjectDataSourceDDLInboundDocumentType"
                        DataTextField="InboundDocumentType" DataValueField="InboundDocumentTypeId" SelectedValue='<%# Bind("InboundDocumentTypeId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundDocumentType" runat="server"
                        TypeName="StaticInfoInboundDocumentType" SelectMethod="ParameterInboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompany" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompany"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompany" runat="server"
                        TypeName="StaticInfoExternalCompany" SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListWarehouse" runat="server" DataSourceID="ObjectDataSourceDDLWarehouse"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("WarehouseId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLWarehouse" runat="server"
                        TypeName="StaticInfoWarehouse" SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OrderNumber:
                </td>
                <td>
                    <asp:Label ID="OrderNumberLabel" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:Label ID="DeliveryDateLabel" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:Label ID="CreateDateLabel" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ModifiedDate:
                </td>
                <td>
                    <asp:Label ID="ModifiedDateLabel" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBack %>" PostBackUrl="~/Administration/InboundDocumentGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceInboundDocument" runat="server" TypeName="StaticInfoInboundDocument"
    SelectMethod="GetInboundDocument"
    DeleteMethod="DeleteInboundDocument"
    UpdateMethod="UpdateInboundDocument"
    InsertMethod="InsertInboundDocument">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InboundDocumentId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InboundDocumentId" Type="Int32" />
        <asp:Parameter Name="InboundDocumentTypeId" Type="Int32" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="OrderNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="ModifiedDate" Type="DateTime" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="InboundDocumentId" QueryStringField="InboundDocumentId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InboundDocumentId" Type="Int32" />
        <asp:Parameter Name="InboundDocumentTypeId" Type="Int32" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="OrderNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="ModifiedDate" Type="DateTime" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
