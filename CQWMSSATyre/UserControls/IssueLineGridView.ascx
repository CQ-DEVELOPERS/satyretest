<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IssueLineGridView.ascx.cs" Inherits="UserControls_IssueLineGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowIssueLine()
{
   window.open("IssueLineFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelIssueId" runat="server" Text="Issue:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListIssue" runat="server" DataSourceID="SqlDataSourceIssueDDL"
                DataTextField="Issue" DataValueField="IssueId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceIssueDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Issue_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelOutboundLineId" runat="server" Text="OutboundLine:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListOutboundLine" runat="server" DataSourceID="SqlDataSourceOutboundLineDDL"
                DataTextField="OutboundLine" DataValueField="OutboundLineId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceOutboundLineDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_OutboundLine_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelStorageUnitBatchId" runat="server" Text="StorageUnitBatch:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="SqlDataSourceStorageUnitBatchDDL"
                DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStorageUnitBatchDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_StorageUnitBatch_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelStatusId" runat="server" Text="Status:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceStatusDDL"
                DataTextField="Status" DataValueField="StatusId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStatusDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Status_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelOperatorId" runat="server" Text="Operator:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="SqlDataSourceOperatorDDL"
                DataTextField="Operator" DataValueField="OperatorId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceOperatorDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Operator_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="<%$ Resources:Default, ButtonNew %>" OnClientClick="javascript:openNewWindowIssueLine();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, ButtonEdit %>" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, ButtonSave %>" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, ButtonDelete %>" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="<%$ Resources:Default, ButtonBack %>" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="<%$ Resources:Default, ButtonPrevious %>" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="<%$ Resources:Default, ButtonNext %>" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="<%$ Resources:Default, ButtonHelp %>"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewIssueLine" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
IssueLineId
"
                DataSourceID="SqlDataSourceIssueLine" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewIssueLine_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="IssueLineId" HeaderText="IssueLineId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="IssueLineId" />
            <asp:TemplateField HeaderText="Issue" meta:resourcekey="Issue" SortExpression="IssueId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListIssue" runat="server" DataSourceID="SqlDataSourceDDLIssue"
                        DataTextField="Issue" DataValueField="IssueId" SelectedValue='<%# Bind("IssueId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLIssue" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Issue_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkIssueId" runat="server" Text='<%# Bind("Issue") %>' NavigateUrl="~/Administration/IssueGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Issue" HeaderText="Issue" ReadOnly="True" SortExpression="Issue" Visible="False" />
            <asp:TemplateField HeaderText="OutboundLine" meta:resourcekey="OutboundLine" SortExpression="OutboundLineId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListOutboundLine" runat="server" DataSourceID="SqlDataSourceDDLOutboundLine"
                        DataTextField="OutboundLine" DataValueField="OutboundLineId" SelectedValue='<%# Bind("OutboundLineId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLOutboundLine" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_OutboundLine_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkOutboundLineId" runat="server" Text='<%# Bind("OutboundLine") %>' NavigateUrl="~/Administration/OutboundLineGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="OutboundLine" HeaderText="OutboundLine" ReadOnly="True" SortExpression="OutboundLine" Visible="False" />
            <asp:TemplateField HeaderText="StorageUnitBatch" meta:resourcekey="StorageUnitBatch" SortExpression="StorageUnitBatchId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="SqlDataSourceDDLStorageUnitBatch"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStorageUnitBatch" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_StorageUnitBatch_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStorageUnitBatchId" runat="server" Text='<%# Bind("StorageUnitBatch") %>' NavigateUrl="~/Administration/StorageUnitBatchGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StorageUnitBatch" HeaderText="StorageUnitBatch" ReadOnly="True" SortExpression="StorageUnitBatch" Visible="False" />
            <asp:TemplateField HeaderText="Status" meta:resourcekey="Status" SortExpression="StatusId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStatus" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Status_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStatusId" runat="server" Text='<%# Bind("Status") %>' NavigateUrl="~/Administration/StatusGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" Visible="False" />
            <asp:TemplateField HeaderText="Operator" meta:resourcekey="Operator" SortExpression="OperatorId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="SqlDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLOperator" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Operator_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkOperatorId" runat="server" Text='<%# Bind("Operator") %>' NavigateUrl="~/Administration/OperatorGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Operator" HeaderText="Operator" ReadOnly="True" SortExpression="Operator" Visible="False" />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                        <asp:BoundField DataField="ConfirmedQuatity" HeaderText="ConfirmedQuatity" SortExpression="ConfirmedQuatity" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceIssueLine" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_IssueLine_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_IssueLine_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_IssueLine_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_IssueLine_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListIssue" DefaultValue="" Name="IssueId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListOutboundLine" DefaultValue="" Name="OutboundLineId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStorageUnitBatch" DefaultValue="" Name="StorageUnitBatchId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStatus" DefaultValue="" Name="StatusId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListOperator" DefaultValue="" Name="OperatorId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="IssueLineId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="IssueId" Type="Int32" />
        <asp:Parameter Name="OutboundLineId" Type="Int32" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="Quantity" Type="Int32" />
        <asp:Parameter Name="ConfirmedQuatity" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="IssueId" Type="Int32" />
        <asp:Parameter Name="OutboundLineId" Type="Int32" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="Quantity" Type="Int32" />
        <asp:Parameter Name="ConfirmedQuatity" Type="Int32" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
