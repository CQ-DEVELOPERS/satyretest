using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_SerialNumbers : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PrivateVariables
    private string result = "";
    private string strresult = "";
    private string theErrMethod = "";
    #endregion PrivateVariables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["countLoopsToPreventInfinLoop"] = 0;

            if (!Page.IsPostBack)
            {
                //Session["KeyId"] = 934;
                //Session["KeyType"] = "StoreInstructionId";
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("SerialNumber" + "_" + ex.Message.ToString());
            //Master.MsgText = result;
        }
    }
    #endregion Page_Load

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
      Page.SetFocus(TextBoxSerialNumber);
    }

    #region ButtonAdd_Click
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        try
        {
            SerialNumber sn = new SerialNumber();
            int keyId = (int)Session["KeyId"];
            String keyType = Session["KeyType"].ToString();

            int returnedValue = sn.SetSerialNumbers(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], keyId, keyType, TextBoxSerialNumber.Text);

            if (returnedValue != 0)
                throw new Exception("Failed to Delete SerialNumber");

            TextBoxSerialNumber.Text = "";

            GridView1.DataBind();
            GridView2.DataBind();

            SerialNumberCountCheck();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("SerialNumber" + "_" + ex.Message.ToString());
            //Master.MsgText = result;
        }
    }
    #endregion ButtonAdd_Click

    #region GridView1_SelectedIndexChanged
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            SerialNumber sn = new SerialNumber();

            int serialNumberId = (int)GridView1.SelectedDataKey["SerialNumberId"];
            int returnedValue = sn.DeleteSerialNumber(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], serialNumberId);

            if (returnedValue != 0)
                throw new Exception("Failed to Delete SerialNumber");

            GridView1.SelectedIndex = -1;

            GridView1.DataBind();
            GridView2.DataBind();

            SerialNumberCountCheck();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("SerialNumber" + "_" + ex.Message.ToString());
            //Master.MsgText = result;
        }
    }
    #endregion GridView1_SelectedIndexChanged

    #region SerialNumberCountCheck
    protected void SerialNumberCountCheck()
    {
        try
        {
            DataSet ds = null;
            SerialNumber sn = new SerialNumber();

            if (Session["KeyStorageUnitId"] == null)
            {
                ds = sn.GetTotal(Session["ConnectionStringName"].ToString(),
                                    int.Parse(Session["KeyId"].ToString()),
                                    Session["KeyType"].ToString());
            }
            else
            {

                ds = sn.GetTotal(Session["ConnectionStringName"].ToString(),
                                    (int)Session["KeyId"],
                                    Session["KeyType"].ToString(),
                                    (int)Session["KeyStorageUnitId"]);
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];

                int count = (int)dr.ItemArray.GetValue(0);
                int total = (int)dr.ItemArray.GetValue(1);

                Session["SerialNumber"] = null;

                if (count == total)
                {
                    Session["SerialNumber"] = true;
                }                
            }
        }
        catch (Exception ex)
        {
            result = SendErrorNow("SerialNumber" + "_" + ex.Message.ToString());
            //Master.MsgText = result;
        }
    }
    #endregion SerialNumberCountCheck

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 5, "PickingMaintenance", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericOutboundErrorHandling(Session["ConnectionStringName"].ToString(), 3, "PickingMaintenance", theErrMethod, exMsg.Message.ToString());

                //Master.MsgText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling
}
