<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AreaOperatorGridView.ascx.cs" Inherits="UserControls_AreaOperatorGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowAreaOperator()
{
   window.open("AreaOperatorFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelAreaId" runat="server" Text="<%$ Resources:Default, LabelArea %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListArea" runat="server" DataSourceID="SqlDataSourceAreaDDL"
                DataTextField="Area" DataValueField="AreaId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceAreaDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Area_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelOperatorId" runat="server" Text="Operator:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="SqlDataSourceOperatorDDL"
                DataTextField="Operator" DataValueField="OperatorId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceOperatorDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Operator_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="<%$ Resources:Default, LabelShow %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="<%$ Resources:Default, ButtonNew %>" OnClientClick="javascript:openNewWindowAreaOperator();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, ButtonEdit %>" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, ButtonSave %>" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, ButtonDelete %>" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="<%$ Resources:Default, ButtonBack %>" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="<%$ Resources:Default, ButtonPrevious %>" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="<%$ Resources:Default, ButtonNext %>" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="<%$ Resources:Default, ButtonHelp %>"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewAreaOperator" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
AreaId
,OperatorId
"
                DataSourceID="SqlDataSourceAreaOperator" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewAreaOperator_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
            <asp:TemplateField HeaderText="Area" meta:resourcekey="Area" SortExpression="AreaId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListArea" runat="server" DataSourceID="SqlDataSourceDDLArea"
                        DataTextField="Area" DataValueField="AreaId" SelectedValue='<%# Bind("AreaId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLArea" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Area_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkAreaId" runat="server" Text='<%# Bind("Area") %>' NavigateUrl="~/Administration/AreaGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Area" HeaderText="Area" ReadOnly="True" SortExpression="Area" Visible="False" />
            <asp:TemplateField HeaderText="Operator" meta:resourcekey="Operator" SortExpression="OperatorId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="SqlDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLOperator" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Operator_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkOperatorId" runat="server" Text='<%# Bind("Operator") %>' NavigateUrl="~/Administration/OperatorGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Operator" HeaderText="Operator" ReadOnly="True" SortExpression="Operator" Visible="False" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceAreaOperator" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_AreaOperator_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_AreaOperator_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_AreaOperator_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_AreaOperator_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListArea" DefaultValue="" Name="AreaId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListOperator" DefaultValue="" Name="OperatorId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="AreaId" Type="Int32" />
        <asp:Parameter Direction="InputOutput" Name="OperatorId" Type="Int32" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
