<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageUnitBatchFormView.ascx.cs" Inherits="UserControls_StorageUnitBatchFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>StorageUnitBatch</h3>
<asp:FormView ID="FormViewStorageUnitBatch" runat="server" DataKeyNames="StorageUnitBatchId" DataSourceID="ObjectDataSourceStorageUnitBatch" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Batch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListBatch" runat="server" DataSourceID="ObjectDataSourceDDLBatch"
                        DataTextField="Batch" DataValueField="BatchId" SelectedValue='<%# Bind("BatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLBatch" runat="server"
                        TypeName="StaticInfoBatch" SelectMethod="ParameterBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListBatch" ErrorMessage="* Please enter BatchId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListStorageUnit" ErrorMessage="* Please enter StorageUnitId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Batch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListBatch" runat="server" DataSourceID="ObjectDataSourceDDLBatch"
                        DataTextField="Batch" DataValueField="BatchId" SelectedValue='<%# Bind("BatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLBatch" runat="server"
                        TypeName="StaticInfoBatch" SelectMethod="ParameterBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListBatch" ErrorMessage="* Please enter BatchId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListStorageUnit" ErrorMessage="* Please enter StorageUnitId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Batch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListBatch" runat="server" DataSourceID="ObjectDataSourceDDLBatch"
                        DataTextField="Batch" DataValueField="BatchId" SelectedValue='<%# Bind("BatchId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLBatch" runat="server"
                        TypeName="StaticInfoBatch" SelectMethod="ParameterBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/StorageUnitBatchGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceStorageUnitBatch" runat="server" TypeName="StaticInfoStorageUnitBatch"
    SelectMethod="GetStorageUnitBatch"
    DeleteMethod="DeleteStorageUnitBatch"
    UpdateMethod="UpdateStorageUnitBatch"
    InsertMethod="InsertStorageUnitBatch">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:Parameter Name="BatchId" Type="Int32" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="StorageUnitBatchId" QueryStringField="StorageUnitBatchId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:Parameter Name="BatchId" Type="Int32" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
