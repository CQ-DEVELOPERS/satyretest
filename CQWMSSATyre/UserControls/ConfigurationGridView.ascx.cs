using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_ConfigurationGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridConfiguration.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridConfiguration_SelectedIndexChanged
    protected void RadGridConfiguration_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridConfiguration.SelectedItems)
            {
                Session["ConfigurationId"] = item.GetDataKeyValue("ConfigurationId");
            }
        }
        catch { }
    }
    #endregion "RadGridConfiguration_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
 
        if (Session["ModuleId"] != null)
          DropDownListModuleId.SelectedValue = Session["ModuleId"].ToString();
    
        RadGridConfiguration.DataBind();
    }
    #endregion BindData
    
}
