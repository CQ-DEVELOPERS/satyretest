<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TestFormView.ascx.cs" Inherits="UserControls_TestFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Test</h3>
<asp:FormView ID="FormViewTest" runat="server" DataKeyNames="TestId" DataSourceID="ObjectDataSourceTest" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    TestCode:
                </td>
                <td>
                    <asp:TextBox ID="TestCodeTextBox" runat="server" Text='<%# Bind("TestCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Test:
                </td>
                <td>
                    <asp:TextBox ID="TestTextBox" runat="server" Text='<%# Bind("Test") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    TestCode:
                </td>
                <td>
                    <asp:TextBox ID="TestCodeTextBox" runat="server" Text='<%# Bind("TestCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Test:
                </td>
                <td>
                    <asp:TextBox ID="TestTextBox" runat="server" Text='<%# Bind("Test") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    TestCode:
                </td>
                <td>
                    <asp:Label ID="TestCodeLabel" runat="server" Text='<%# Bind("TestCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Test:
                </td>
                <td>
                    <asp:Label ID="TestLabel" runat="server" Text='<%# Bind("Test") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/TestGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceTest" runat="server" TypeName="StaticInfoTest"
    SelectMethod="GetTest"
    DeleteMethod="DeleteTest"
    UpdateMethod="UpdateTest"
    InsertMethod="InsertTest">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TestId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TestId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TestCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Test" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="TestId" QueryStringField="TestId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="TestId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="TestCode" Type="String" />
        <asp:Parameter Name="Test" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
