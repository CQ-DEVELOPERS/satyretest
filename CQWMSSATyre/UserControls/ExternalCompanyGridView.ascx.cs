using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_ExternalCompanyGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridExternalCompany.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridExternalCompany_SelectedIndexChanged
    protected void RadGridExternalCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridExternalCompany.SelectedItems)
            {
                Session["ExternalCompanyId"] = item.GetDataKeyValue("ExternalCompanyId");
            }
        }
        catch { }
    }
    #endregion "RadGridExternalCompany_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["ExternalCompanyTypeId"] != null)
          DropDownListExternalCompanyTypeId.SelectedValue = Session["ExternalCompanyTypeId"].ToString();
        if (Session["RouteId"] != null)
          DropDownListRouteId.SelectedValue = Session["RouteId"].ToString();
        if (Session["ContainerTypeId"] != null)
          DropDownListContainerTypeId.SelectedValue = Session["ContainerTypeId"].ToString();
        if (Session["PrincipalId"] != null)
          DropDownListPrincipalId.SelectedValue = Session["PrincipalId"].ToString();
        if (Session["ProcessId"] != null)
          DropDownListProcessId.SelectedValue = Session["ProcessId"].ToString();
        if (Session["PricingCategoryId"] != null)
          DropDownListPricingCategoryId.SelectedValue = Session["PricingCategoryId"].ToString();
        if (Session["LabelingCategoryId"] != null)
          DropDownListLabelingCategoryId.SelectedValue = Session["LabelingCategoryId"].ToString();
    
        RadGridExternalCompany.DataBind();
    }
    #endregion BindData
    
}
