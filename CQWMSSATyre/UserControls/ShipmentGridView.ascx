<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShipmentGridView.ascx.cs" Inherits="UserControls_ShipmentGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowShipment()
{
   window.open("ShipmentFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelStatusId" runat="server" Text="Status:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceStatusDDL"
                DataTextField="Status" DataValueField="StatusId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStatusDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Status_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelLocationId" runat="server" Text="Location:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="SqlDataSourceLocationDDL"
                DataTextField="Location" DataValueField="LocationId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceLocationDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Location_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowShipment();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="Search" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="Previous" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="Next" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="Help"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewShipment" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
ShipmentId
"
                DataSourceID="SqlDataSourceShipment" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewShipment_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="ShipmentId" HeaderText="ShipmentId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="ShipmentId" />
            <asp:TemplateField HeaderText="Status" meta:resourcekey="Status" SortExpression="StatusId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStatus" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Status_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStatusId" runat="server" Text='<%# Bind("Status") %>' NavigateUrl="~/Administration/StatusGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" Visible="False" />
                        <asp:BoundField DataField="Warehouse" HeaderText="Warehouse" ReadOnly="True" SortExpression="Warehouse" Visible="False" />
            <asp:TemplateField HeaderText="Location" meta:resourcekey="Location" SortExpression="LocationId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="SqlDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLLocation" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Location_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkLocationId" runat="server" Text='<%# Bind("Location") %>' NavigateUrl="~/Administration/LocationGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Location" HeaderText="Location" ReadOnly="True" SortExpression="Location" Visible="False" />
                        <asp:TemplateField HeaderText="ShipmentDate" SortExpression="ShipmentDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonShipmentDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('ShipmentDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelShipmentDate" runat="server" Text='<%# Bind("ShipmentDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks" />
                        <asp:BoundField DataField="SealNumber" HeaderText="SealNumber" SortExpression="SealNumber" />
                        <asp:BoundField DataField="VehicleRegistration" HeaderText="VehicleRegistration" SortExpression="VehicleRegistration" />
                        <asp:BoundField DataField="Route" HeaderText="Route" SortExpression="Route" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceShipment" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_Shipment_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_Shipment_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_Shipment_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_Shipment_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStatus" DefaultValue="" Name="StatusId" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListLocation" DefaultValue="" Name="LocationId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="ShipmentId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="ShipmentId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="ShipmentDate" Type="DateTime" />
        <asp:Parameter Name="Remarks" Type="String" />
        <asp:Parameter Name="SealNumber" Type="String" />
        <asp:Parameter Name="VehicleRegistration" Type="String" />
        <asp:Parameter Name="Route" Type="String" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="ShipmentId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="ShipmentDate" Type="DateTime" />
        <asp:Parameter Name="Remarks" Type="String" />
        <asp:Parameter Name="SealNumber" Type="String" />
        <asp:Parameter Name="VehicleRegistration" Type="String" />
        <asp:Parameter Name="Route" Type="String" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
