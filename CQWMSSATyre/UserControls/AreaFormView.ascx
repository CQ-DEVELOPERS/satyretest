<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AreaFormView.ascx.cs" Inherits="UserControls_AreaFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Area</h3>
<asp:FormView ID="FormViewArea" runat="server" DataKeyNames="AreaId" DataSourceID="ObjectDataSourceArea" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Area:
                </td>
                <td>
                    <asp:TextBox ID="AreaTextBox" runat="server" Text='<%# Bind("Area") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="AreaTextBox" ErrorMessage="* Please enter Area"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AreaCode:
                </td>
                <td>
                    <asp:TextBox ID="AreaCodeTextBox" runat="server" Text='<%# Bind("AreaCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="AreaCodeTextBox" ErrorMessage="* Please enter AreaCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StockOnHand:
                </td>
                <td>
                    <asp:CheckBox ID="StockOnHandCheckBox" runat="server" Checked='<%# Bind("StockOnHand") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="<%$ Resources:Default, ButtonUpdate %>" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="<%$ Resources:Default, ButtonCancel %>" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Area:
                </td>
                <td>
                    <asp:TextBox ID="AreaTextBox" runat="server" Text='<%# Bind("Area") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="AreaTextBox" ErrorMessage="* Please enter Area"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AreaCode:
                </td>
                <td>
                    <asp:TextBox ID="AreaCodeTextBox" runat="server" Text='<%# Bind("AreaCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="AreaCodeTextBox" ErrorMessage="* Please enter AreaCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StockOnHand:
                </td>
                <td>
                    <asp:CheckBox ID="StockOnHandCheckBox" runat="server" Checked='<%# Bind("StockOnHand") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="<%$ Resources:Default, ButtonInsert %>"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListWarehouse" runat="server" DataSourceID="ObjectDataSourceDDLWarehouse"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("WarehouseId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLWarehouse" runat="server"
                        TypeName="StaticInfoWarehouse" SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Area:
                </td>
                <td>
                    <asp:Label ID="AreaLabel" runat="server" Text='<%# Bind("Area") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AreaCode:
                </td>
                <td>
                    <asp:Label ID="AreaCodeLabel" runat="server" Text='<%# Bind("AreaCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StockOnHand:
                </td>
                <td>
                    <asp:Label ID="StockOnHandLabel" runat="server" Text='<%# Bind("StockOnHand") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBack %>" PostBackUrl="~/Administration/AreaGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="StaticInfoArea"
    SelectMethod="GetArea"
    DeleteMethod="DeleteArea"
    UpdateMethod="UpdateArea"
    InsertMethod="InsertArea">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AreaId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="Area" Type="String" />
        <asp:Parameter Name="AreaCode" Type="String" />
        <asp:Parameter Name="StockOnHand" Type="Boolean" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="AreaId" QueryStringField="AreaId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="Area" Type="String" />
        <asp:Parameter Name="AreaCode" Type="String" />
        <asp:Parameter Name="StockOnHand" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
