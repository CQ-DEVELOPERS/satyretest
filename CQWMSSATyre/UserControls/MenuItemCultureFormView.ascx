<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuItemCultureFormView.ascx.cs" Inherits="UserControls_MenuItemCultureFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>MenuItemCulture</h3>
<asp:FormView ID="FormViewMenuItemCulture" runat="server" DataKeyNames="MenuItemCultureId" DataSourceID="ObjectDataSourceMenuItemCulture" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    MenuItemId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMenuItemId" runat="server" DataSourceID="ObjectDataSourceDDLMenuItemId"
                        DataTextField="MenuItem" DataValueField="MenuItemId" SelectedValue='<%# Bind("MenuItemId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
                        SelectMethod="ParameterMenuItem">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    CultureId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCultureId" runat="server" DataSourceID="ObjectDataSourceDDLCultureId"
                        DataTextField="Culture" DataValueField="CultureId" SelectedValue='<%# Bind("CultureId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCultureId" runat="server" TypeName="StaticInfoCulture"
                        SelectMethod="ParameterCulture">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    MenuItem:
                </td>
                <td>
                    <asp:TextBox ID="MenuItemTextBox" runat="server" Text='<%# Bind("MenuItem") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ToolTip:
                </td>
                <td>
                    <asp:TextBox ID="ToolTipTextBox" runat="server" Text='<%# Bind("ToolTip") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    MenuItemId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMenuItemId" runat="server" DataSourceID="ObjectDataSourceDDLMenuItemId"
                        DataTextField="MenuItem" DataValueField="MenuItemId" SelectedValue='<%# Bind("MenuItemId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
                        SelectMethod="ParameterMenuItem">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    CultureId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCultureId" runat="server" DataSourceID="ObjectDataSourceDDLCultureId"
                        DataTextField="Culture" DataValueField="CultureId" SelectedValue='<%# Bind("CultureId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCultureId" runat="server" TypeName="StaticInfoCulture"
                        SelectMethod="ParameterCulture">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    MenuItem:
                </td>
                <td>
                    <asp:TextBox ID="MenuItemTextBox" runat="server" Text='<%# Bind("MenuItem") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ToolTip:
                </td>
                <td>
                    <asp:TextBox ID="ToolTipTextBox" runat="server" Text='<%# Bind("ToolTip") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    MenuItem:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMenuItemId" runat="server" DataSourceID="ObjectDataSourceDDLMenuItemId"
                        DataTextField="MenuItem" DataValueField="MenuItemId" SelectedValue='<%# Bind("MenuItemId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
                        SelectMethod="ParameterMenuItem">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Culture:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCultureId" runat="server" DataSourceID="ObjectDataSourceDDLCultureId"
                        DataTextField="Culture" DataValueField="CultureId" SelectedValue='<%# Bind("CultureId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCultureId" runat="server" TypeName="StaticInfoCulture"
                        SelectMethod="ParameterCulture">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    MenuItem:
                </td>
                <td>
                    <asp:Label ID="MenuItemLabel" runat="server" Text='<%# Bind("MenuItem") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ToolTip:
                </td>
                <td>
                    <asp:Label ID="ToolTipLabel" runat="server" Text='<%# Bind("ToolTip") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/MenuItemCultureGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceMenuItemCulture" runat="server" TypeName="StaticInfoMenuItemCulture"
    SelectMethod="GetMenuItemCulture"
    DeleteMethod="DeleteMenuItemCulture"
    UpdateMethod="UpdateMenuItemCulture"
    InsertMethod="InsertMenuItemCulture">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MenuItemCultureId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MenuItemCultureId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="CultureId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItem" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="ToolTip" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="MenuItemCultureId" QueryStringField="MenuItemCultureId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MenuItemCultureId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="CultureId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItem" Type="String" />
        <asp:Parameter Name="ToolTip" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
