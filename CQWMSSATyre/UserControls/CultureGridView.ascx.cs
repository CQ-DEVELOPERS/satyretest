using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_CultureGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridCulture.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridCulture_SelectedIndexChanged
    protected void RadGridCulture_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridCulture.SelectedItems)
            {
                Session["CultureId"] = item.GetDataKeyValue("CultureId");
            }
        }
        catch { }
    }
    #endregion "RadGridCulture_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
    
        RadGridCulture.DataBind();
    }
    #endregion BindData
    
}
