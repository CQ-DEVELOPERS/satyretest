<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MovementSequenceFormView.ascx.cs" Inherits="UserControls_MovementSequenceFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>MovementSequence</h3>
<asp:FormView ID="FormViewMovementSequence" runat="server" DataKeyNames="MovementSequenceId" DataSourceID="ObjectDataSourceMovementSequence" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    PickAreaId:
                </td>
                <td>
                    <asp:TextBox ID="PickAreaIdTextBox" runat="server" Text='<%# Bind("PickAreaId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PickAreaIdTextBox" ErrorMessage="* Please enter PickAreaId"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="PickAreaIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StoreAreaId:
                </td>
                <td>
                    <asp:TextBox ID="StoreAreaIdTextBox" runat="server" Text='<%# Bind("StoreAreaId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="StoreAreaIdTextBox" ErrorMessage="* Please enter StoreAreaId"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="StoreAreaIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StageAreaId:
                </td>
                <td>
                    <asp:TextBox ID="StageAreaIdTextBox" runat="server" Text='<%# Bind("StageAreaId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="StageAreaIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroup" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroup"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroup" runat="server"
                        TypeName="StaticInfoOperatorGroup" SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="DropDownListOperatorGroup" ErrorMessage="* Please enter OperatorGroupId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StatusCode:
                </td>
                <td>
                    <asp:TextBox ID="StatusCodeTextBox" runat="server" Text='<%# Bind("StatusCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="StatusCodeTextBox" ErrorMessage="* Please enter StatusCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    PickAreaId:
                </td>
                <td>
                    <asp:TextBox ID="PickAreaIdTextBox" runat="server" Text='<%# Bind("PickAreaId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PickAreaIdTextBox" ErrorMessage="* Please enter PickAreaId"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="PickAreaIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StoreAreaId:
                </td>
                <td>
                    <asp:TextBox ID="StoreAreaIdTextBox" runat="server" Text='<%# Bind("StoreAreaId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="StoreAreaIdTextBox" ErrorMessage="* Please enter StoreAreaId"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="StoreAreaIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StageAreaId:
                </td>
                <td>
                    <asp:TextBox ID="StageAreaIdTextBox" runat="server" Text='<%# Bind("StageAreaId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="StageAreaIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroup" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroup"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroup" runat="server"
                        TypeName="StaticInfoOperatorGroup" SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="DropDownListOperatorGroup" ErrorMessage="* Please enter OperatorGroupId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StatusCode:
                </td>
                <td>
                    <asp:TextBox ID="StatusCodeTextBox" runat="server" Text='<%# Bind("StatusCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="StatusCodeTextBox" ErrorMessage="* Please enter StatusCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    PickAreaId:
                </td>
                <td>
                    <asp:Label ID="PickAreaIdLabel" runat="server" Text='<%# Bind("PickAreaId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StoreAreaId:
                </td>
                <td>
                    <asp:Label ID="StoreAreaIdLabel" runat="server" Text='<%# Bind("StoreAreaId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StageAreaId:
                </td>
                <td>
                    <asp:Label ID="StageAreaIdLabel" runat="server" Text='<%# Bind("StageAreaId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroup" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroup"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroup" runat="server"
                        TypeName="StaticInfoOperatorGroup" SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    StatusCode:
                </td>
                <td>
                    <asp:Label ID="StatusCodeLabel" runat="server" Text='<%# Bind("StatusCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/MovementSequenceGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceMovementSequence" runat="server" TypeName="StaticInfoMovementSequence"
    SelectMethod="GetMovementSequence"
    DeleteMethod="DeleteMovementSequence"
    UpdateMethod="UpdateMovementSequence"
    InsertMethod="InsertMovementSequence">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MovementSequenceId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MovementSequenceId" Type="Int32" />
        <asp:Parameter Name="PickAreaId" Type="Int32" />
        <asp:Parameter Name="StoreAreaId" Type="Int32" />
        <asp:Parameter Name="StageAreaId" Type="Int32" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" />
        <asp:Parameter Name="StatusCode" Type="String" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="MovementSequenceId" QueryStringField="MovementSequenceId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MovementSequenceId" Type="Int32" />
        <asp:Parameter Name="PickAreaId" Type="Int32" />
        <asp:Parameter Name="StoreAreaId" Type="Int32" />
        <asp:Parameter Name="StageAreaId" Type="Int32" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" />
        <asp:Parameter Name="StatusCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
