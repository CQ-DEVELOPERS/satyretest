<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CycleFormView.ascx.cs" Inherits="UserControls_CycleFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Cycle</h3>
<asp:FormView ID="FormViewCycle" runat="server" DataKeyNames="CycleId" DataSourceID="ObjectDataSourceCycle" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    CycleCode:
                </td>
                <td>
                    <asp:TextBox ID="CycleCodeTextBox" runat="server" Text='<%# Bind("CycleCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Cycle:
                </td>
                <td>
                    <asp:TextBox ID="CycleTextBox" runat="server" Text='<%# Bind("Cycle") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    CycleCode:
                </td>
                <td>
                    <asp:TextBox ID="CycleCodeTextBox" runat="server" Text='<%# Bind("CycleCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Cycle:
                </td>
                <td>
                    <asp:TextBox ID="CycleTextBox" runat="server" Text='<%# Bind("Cycle") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    CycleCode:
                </td>
                <td>
                    <asp:Label ID="CycleCodeLabel" runat="server" Text='<%# Bind("CycleCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Cycle:
                </td>
                <td>
                    <asp:Label ID="CycleLabel" runat="server" Text='<%# Bind("Cycle") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/CycleGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceCycle" runat="server" TypeName="StaticInfoCycle"
    SelectMethod="GetCycle"
    DeleteMethod="DeleteCycle"
    UpdateMethod="UpdateCycle"
    InsertMethod="InsertCycle">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="CycleId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="CycleId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="CycleCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Cycle" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="CycleId" QueryStringField="CycleId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="CycleId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="CycleCode" Type="String" />
        <asp:Parameter Name="Cycle" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
