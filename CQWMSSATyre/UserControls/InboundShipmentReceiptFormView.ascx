<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InboundShipmentReceiptFormView.ascx.cs" Inherits="UserControls_InboundShipmentReceiptFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>InboundShipmentReceipt</h3>
<asp:FormView ID="FormViewInboundShipmentReceipt" runat="server" DataKeyNames="InboundShipmentReceiptId" DataSourceID="ObjectDataSourceInboundShipmentReceipt" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="<%$ Resources:Default, ButtonUpdate %>"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    InboundShipment:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundShipment" runat="server" DataSourceID="ObjectDataSourceDDLInboundShipment"
                        DataTextField="InboundShipment" DataValueField="InboundShipmentId" SelectedValue='<%# Bind("InboundShipmentId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundShipment" runat="server"
                        TypeName="StaticInfoInboundShipment" SelectMethod="ParameterInboundShipment">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListInboundShipment" ErrorMessage="* Please enter InboundShipmentId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Receipt:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceipt" runat="server" DataSourceID="ObjectDataSourceDDLReceipt"
                        DataTextField="Receipt" DataValueField="ReceiptId" SelectedValue='<%# Bind("ReceiptId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceipt" runat="server"
                        TypeName="StaticInfoReceipt" SelectMethod="ParameterReceipt">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListReceipt" ErrorMessage="* Please enter ReceiptId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="<%$ Resources:Default, ButtonInsert %>"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    InboundShipment:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundShipment" runat="server" DataSourceID="ObjectDataSourceDDLInboundShipment"
                        DataTextField="InboundShipment" DataValueField="InboundShipmentId" SelectedValue='<%# Bind("InboundShipmentId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundShipment" runat="server"
                        TypeName="StaticInfoInboundShipment" SelectMethod="ParameterInboundShipment">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Receipt:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceipt" runat="server" DataSourceID="ObjectDataSourceDDLReceipt"
                        DataTextField="Receipt" DataValueField="ReceiptId" SelectedValue='<%# Bind("ReceiptId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceipt" runat="server"
                        TypeName="StaticInfoReceipt" SelectMethod="ParameterReceipt">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBack %>" PostBackUrl="~/Administration/InboundShipmentReceiptGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceInboundShipmentReceipt" runat="server" TypeName="StaticInfoInboundShipmentReceipt"
    SelectMethod="GetInboundShipmentReceipt"
    DeleteMethod="DeleteInboundShipmentReceipt"
    UpdateMethod="UpdateInboundShipmentReceipt"
    InsertMethod="InsertInboundShipmentReceipt">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InboundShipmentReceiptId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InboundShipmentId" Type="Int32" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="InboundShipmentId" QueryStringField="InboundShipmentId" DefaultValue="-1" Type="Int32" />
      <asp:QueryStringParameter Name="ReceiptId" QueryStringField="ReceiptId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="InboundShipmentId" Type="Int32" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
