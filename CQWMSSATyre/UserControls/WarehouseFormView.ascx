<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WarehouseFormView.ascx.cs" Inherits="UserControls_WarehouseFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Warehouse</h3>
<asp:FormView ID="FormViewWarehouse" runat="server" DataKeyNames="WarehouseId" DataSourceID="ObjectDataSourceWarehouse" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    CompanyId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLCompanyId"
                        DataTextField="Company" DataValueField="CompanyId" SelectedValue='<%# Bind("CompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCompanyId" runat="server" TypeName="StaticInfoCompany"
                        SelectMethod="ParameterCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:TextBox ID="WarehouseTextBox" runat="server" Text='<%# Bind("Warehouse") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="WarehouseTextBox"
                        ErrorMessage="* Please enter Warehouse"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    WarehouseCode:
                </td>
                <td>
                    <asp:TextBox ID="WarehouseCodeTextBox" runat="server" Text='<%# Bind("WarehouseCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HostId:
                </td>
                <td>
                    <asp:TextBox ID="HostIdTextBox" runat="server" Text='<%# Bind("HostId") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ParentWarehouseId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListParentWarehouseId" runat="server" DataSourceID="ObjectDataSourceDDLParentWarehouseId"
                        DataTextField="Warehouse" DataValueField="ParentWarehouseId" SelectedValue='<%# Bind("WarehouseId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLParentWarehouseId" runat="server" TypeName="StaticInfoWarehouse"
                        SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    AddressId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListAddressId" runat="server" DataSourceID="ObjectDataSourceDDLAddressId"
                        DataTextField="Address" DataValueField="AddressId" SelectedValue='<%# Bind("AddressId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLAddressId" runat="server" TypeName="StaticInfoAddress"
                        SelectMethod="ParameterAddress">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DesktopMaximum:
                </td>
                <td>
                    <asp:TextBox ID="DesktopMaximumTextBox" runat="server" Text='<%# Bind("DesktopMaximum") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="DesktopMaximumTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DesktopWarning:
                </td>
                <td>
                    <asp:TextBox ID="DesktopWarningTextBox" runat="server" Text='<%# Bind("DesktopWarning") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="DesktopWarningTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MobileWarning:
                </td>
                <td>
                    <asp:TextBox ID="MobileWarningTextBox" runat="server" Text='<%# Bind("MobileWarning") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="MobileWarningTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MobileMaximum:
                </td>
                <td>
                    <asp:TextBox ID="MobileMaximumTextBox" runat="server" Text='<%# Bind("MobileMaximum") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="MobileMaximumTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    CompanyId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLCompanyId"
                        DataTextField="Company" DataValueField="CompanyId" SelectedValue='<%# Bind("CompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCompanyId" runat="server" TypeName="StaticInfoCompany"
                        SelectMethod="ParameterCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:TextBox ID="WarehouseTextBox" runat="server" Text='<%# Bind("Warehouse") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="WarehouseTextBox"
                        ErrorMessage="* Please enter Warehouse"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    WarehouseCode:
                </td>
                <td>
                    <asp:TextBox ID="WarehouseCodeTextBox" runat="server" Text='<%# Bind("WarehouseCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    HostId:
                </td>
                <td>
                    <asp:TextBox ID="HostIdTextBox" runat="server" Text='<%# Bind("HostId") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ParentWarehouseId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListParentWarehouseId" runat="server" DataSourceID="ObjectDataSourceDDLParentWarehouseId"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("ParentWarehouseId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLParentWarehouseId" runat="server" TypeName="StaticInfoWarehouse"
                        SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    AddressId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListAddressId" runat="server" DataSourceID="ObjectDataSourceDDLAddressId"
                        DataTextField="Address" DataValueField="AddressId" SelectedValue='<%# Bind("AddressId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLAddressId" runat="server" TypeName="StaticInfoAddress"
                        SelectMethod="ParameterAddress">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DesktopMaximum:
                </td>
                <td>
                    <asp:TextBox ID="DesktopMaximumTextBox" runat="server" Text='<%# Bind("DesktopMaximum") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="DesktopMaximumTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DesktopWarning:
                </td>
                <td>
                    <asp:TextBox ID="DesktopWarningTextBox" runat="server" Text='<%# Bind("DesktopWarning") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="DesktopWarningTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MobileWarning:
                </td>
                <td>
                    <asp:TextBox ID="MobileWarningTextBox" runat="server" Text='<%# Bind("MobileWarning") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="MobileWarningTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MobileMaximum:
                </td>
                <td>
                    <asp:TextBox ID="MobileMaximumTextBox" runat="server" Text='<%# Bind("MobileMaximum") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="MobileMaximumTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Company:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLCompanyId"
                        DataTextField="Company" DataValueField="CompanyId" SelectedValue='<%# Bind("CompanyId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCompanyId" runat="server" TypeName="StaticInfoCompany"
                        SelectMethod="ParameterCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:Label ID="WarehouseLabel" runat="server" Text='<%# Bind("Warehouse") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    WarehouseCode:
                </td>
                <td>
                    <asp:Label ID="WarehouseCodeLabel" runat="server" Text='<%# Bind("WarehouseCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    HostId:
                </td>
                <td>
                    <asp:Label ID="HostIdLabel" runat="server" Text='<%# Bind("HostId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListParentWarehouseId" runat="server" DataSourceID="ObjectDataSourceDDLParentWarehouseId"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("ParentWarehouseId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLParentWarehouseId" runat="server" TypeName="StaticInfoWarehouse"
                        SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Address:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListAddressId" runat="server" DataSourceID="ObjectDataSourceDDLAddressId"
                        DataTextField="Address" DataValueField="AddressId" SelectedValue='<%# Bind("AddressId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLAddressId" runat="server" TypeName="StaticInfoAddress"
                        SelectMethod="ParameterAddress">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    DesktopMaximum:
                </td>
                <td>
                    <asp:Label ID="DesktopMaximumLabel" runat="server" Text='<%# Bind("DesktopMaximum") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DesktopWarning:
                </td>
                <td>
                    <asp:Label ID="DesktopWarningLabel" runat="server" Text='<%# Bind("DesktopWarning") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    MobileWarning:
                </td>
                <td>
                    <asp:Label ID="MobileWarningLabel" runat="server" Text='<%# Bind("MobileWarning") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    MobileMaximum:
                </td>
                <td>
                    <asp:Label ID="MobileMaximumLabel" runat="server" Text='<%# Bind("MobileMaximum") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/WarehouseGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceWarehouse" runat="server" TypeName="StaticInfoWarehouse"
    SelectMethod="GetWarehouse"
    DeleteMethod="DeleteWarehouse"
    UpdateMethod="UpdateWarehouse"
    InsertMethod="InsertWarehouse">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="WarehouseId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="WarehouseId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="CompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Warehouse" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="WarehouseCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="HostId" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="ParentWarehouseId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AddressId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="DesktopMaximum" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="DesktopWarning" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MobileWarning" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MobileMaximum" Type="Int32" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="WarehouseId" QueryStringField="WarehouseId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="WarehouseId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="CompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Warehouse" Type="String" />
        <asp:Parameter Name="WarehouseCode" Type="String" />
        <asp:Parameter Name="HostId" Type="String" />
        <asp:Parameter Name="ParentWarehouseId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="AddressId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="DesktopMaximum" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="DesktopWarning" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MobileWarning" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MobileMaximum" Type="Int32" DefaultValue="-1" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
