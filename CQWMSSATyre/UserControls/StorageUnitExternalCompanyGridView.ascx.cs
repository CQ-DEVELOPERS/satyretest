using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_StorageUnitExternalCompanyGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridStorageUnitExternalCompany.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridStorageUnitExternalCompany_SelectedIndexChanged
    protected void RadGridStorageUnitExternalCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridStorageUnitExternalCompany.SelectedItems)
            {
                Session["StorageUnitExternalCompanyId"] = item.GetDataKeyValue("StorageUnitExternalCompanyId");
            }
        }
        catch { }
    }
    #endregion "RadGridStorageUnitExternalCompany_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["StorageUnitId"] != null)
          DropDownListStorageUnitId.SelectedValue = Session["StorageUnitId"].ToString();
        if (Session["ExternalCompanyId"] != null)
          DropDownListExternalCompanyId.SelectedValue = Session["ExternalCompanyId"].ToString();
    
        RadGridStorageUnitExternalCompany.DataBind();
    }
    #endregion BindData
    
}
