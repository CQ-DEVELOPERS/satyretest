<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageUnitBatchLocationFormView.ascx.cs" Inherits="UserControls_StorageUnitBatchLocationFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>StorageUnitBatchLocation</h3>
<asp:FormView ID="FormViewStorageUnitBatchLocation" runat="server" DataKeyNames="StorageUnitBatchLocationId" DataSourceID="ObjectDataSourceStorageUnitBatchLocation" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    ActualQuantity:
                </td>
                <td>
                    <asp:TextBox ID="ActualQuantityTextBox" runat="server" Text='<%# Bind("ActualQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ActualQuantityTextBox" ErrorMessage="* Please enter ActualQuantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="ActualQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AllocatedQuantity:
                </td>
                <td>
                    <asp:TextBox ID="AllocatedQuantityTextBox" runat="server" Text='<%# Bind("AllocatedQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="AllocatedQuantityTextBox" ErrorMessage="* Please enter AllocatedQuantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="AllocatedQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReservedQuantity:
                </td>
                <td>
                    <asp:TextBox ID="ReservedQuantityTextBox" runat="server" Text='<%# Bind("ReservedQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ReservedQuantityTextBox" ErrorMessage="* Please enter ReservedQuantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="ReservedQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    StorageUnitBatch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatch"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatch" runat="server"
                        TypeName="StaticInfoStorageUnitBatch" SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListStorageUnitBatch" ErrorMessage="* Please enter StorageUnitBatchId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListLocation" ErrorMessage="* Please enter LocationId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ActualQuantity:
                </td>
                <td>
                    <asp:TextBox ID="ActualQuantityTextBox" runat="server" Text='<%# Bind("ActualQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ActualQuantityTextBox" ErrorMessage="* Please enter ActualQuantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="ActualQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AllocatedQuantity:
                </td>
                <td>
                    <asp:TextBox ID="AllocatedQuantityTextBox" runat="server" Text='<%# Bind("AllocatedQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="AllocatedQuantityTextBox" ErrorMessage="* Please enter AllocatedQuantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="AllocatedQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReservedQuantity:
                </td>
                <td>
                    <asp:TextBox ID="ReservedQuantityTextBox" runat="server" Text='<%# Bind("ReservedQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ReservedQuantityTextBox" ErrorMessage="* Please enter ReservedQuantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="ReservedQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    StorageUnitBatch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatch"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatch" runat="server"
                        TypeName="StaticInfoStorageUnitBatch" SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ActualQuantity:
                </td>
                <td>
                    <asp:Label ID="ActualQuantityLabel" runat="server" Text='<%# Bind("ActualQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AllocatedQuantity:
                </td>
                <td>
                    <asp:Label ID="AllocatedQuantityLabel" runat="server" Text='<%# Bind("AllocatedQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ReservedQuantity:
                </td>
                <td>
                    <asp:Label ID="ReservedQuantityLabel" runat="server" Text='<%# Bind("ReservedQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/StorageUnitBatchLocationGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceStorageUnitBatchLocation" runat="server" TypeName="StaticInfoStorageUnitBatchLocation"
    SelectMethod="GetStorageUnitBatchLocation"
    DeleteMethod="DeleteStorageUnitBatchLocation"
    UpdateMethod="UpdateStorageUnitBatchLocation"
    InsertMethod="InsertStorageUnitBatchLocation">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitBatchLocationId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="ActualQuantity" Type="Int32" />
        <asp:Parameter Name="AllocatedQuantity" Type="Int32" />
        <asp:Parameter Name="ReservedQuantity" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="StorageUnitBatchId" QueryStringField="StorageUnitBatchId" DefaultValue="-1" Type="Int32" />
      <asp:QueryStringParameter Name="LocationId" QueryStringField="LocationId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="ActualQuantity" Type="Int32" />
        <asp:Parameter Name="AllocatedQuantity" Type="Int32" />
        <asp:Parameter Name="ReservedQuantity" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
