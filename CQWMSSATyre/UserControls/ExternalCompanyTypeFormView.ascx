<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExternalCompanyTypeFormView.ascx.cs" Inherits="UserControls_ExternalCompanyTypeFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>ExternalCompanyType</h3>
<asp:FormView ID="FormViewExternalCompanyType" runat="server" DataKeyNames="ExternalCompanyTypeId" DataSourceID="ObjectDataSourceExternalCompanyType" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompanyType:
                </td>
                <td>
                    <asp:TextBox ID="ExternalCompanyTypeTextBox" runat="server" Text='<%# Bind("ExternalCompanyType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ExternalCompanyTypeTextBox"
                        ErrorMessage="* Please enter ExternalCompanyType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompanyTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="ExternalCompanyTypeCodeTextBox" runat="server" Text='<%# Bind("ExternalCompanyTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ExternalCompanyTypeCodeTextBox"
                        ErrorMessage="* Please enter ExternalCompanyTypeCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompanyType:
                </td>
                <td>
                    <asp:TextBox ID="ExternalCompanyTypeTextBox" runat="server" Text='<%# Bind("ExternalCompanyType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ExternalCompanyTypeTextBox"
                        ErrorMessage="* Please enter ExternalCompanyType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompanyTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="ExternalCompanyTypeCodeTextBox" runat="server" Text='<%# Bind("ExternalCompanyTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ExternalCompanyTypeCodeTextBox"
                        ErrorMessage="* Please enter ExternalCompanyTypeCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompanyType:
                </td>
                <td>
                    <asp:Label ID="ExternalCompanyTypeLabel" runat="server" Text='<%# Bind("ExternalCompanyType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompanyTypeCode:
                </td>
                <td>
                    <asp:Label ID="ExternalCompanyTypeCodeLabel" runat="server" Text='<%# Bind("ExternalCompanyTypeCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ExternalCompanyTypeGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceExternalCompanyType" runat="server" TypeName="StaticInfoExternalCompanyType"
    SelectMethod="GetExternalCompanyType"
    DeleteMethod="DeleteExternalCompanyType"
    UpdateMethod="UpdateExternalCompanyType"
    InsertMethod="InsertExternalCompanyType">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExternalCompanyTypeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExternalCompanyTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyType" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyTypeCode" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ExternalCompanyTypeId" QueryStringField="ExternalCompanyTypeId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ExternalCompanyTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyType" Type="String" />
        <asp:Parameter Name="ExternalCompanyTypeCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
