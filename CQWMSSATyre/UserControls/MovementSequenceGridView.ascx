<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MovementSequenceGridView.ascx.cs" Inherits="UserControls_MovementSequenceGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowMovementSequence()
{
   window.open("MovementSequenceFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelOperatorGroupId" runat="server" Text="OperatorGroup:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListOperatorGroup" runat="server" DataSourceID="SqlDataSourceOperatorGroupDDL"
                DataTextField="OperatorGroup" DataValueField="OperatorGroupId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceOperatorGroupDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_OperatorGroup_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowMovementSequence();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="Search" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="Previous" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="Next" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="Help"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewMovementSequence" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
MovementSequenceId
"
                DataSourceID="SqlDataSourceMovementSequence" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewMovementSequence_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="MovementSequenceId" HeaderText="MovementSequenceId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="MovementSequenceId" />
                        <asp:BoundField DataField="PickAreaId" HeaderText="PickAreaId" SortExpression="PickAreaId" />
                        <asp:BoundField DataField="StoreAreaId" HeaderText="StoreAreaId" SortExpression="StoreAreaId" />
                        <asp:BoundField DataField="StageAreaId" HeaderText="StageAreaId" SortExpression="StageAreaId" />
            <asp:TemplateField HeaderText="OperatorGroup" meta:resourcekey="OperatorGroup" SortExpression="OperatorGroupId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListOperatorGroup" runat="server" DataSourceID="SqlDataSourceDDLOperatorGroup"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLOperatorGroup" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_OperatorGroup_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkOperatorGroupId" runat="server" Text='<%# Bind("OperatorGroup") %>' NavigateUrl="~/Administration/OperatorGroupGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="OperatorGroup" HeaderText="OperatorGroup" ReadOnly="True" SortExpression="OperatorGroup" Visible="False" />
                        <asp:BoundField DataField="StatusCode" HeaderText="StatusCode" SortExpression="StatusCode" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceMovementSequence" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_MovementSequence_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_MovementSequence_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_MovementSequence_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_MovementSequence_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListOperatorGroup" DefaultValue="" Name="OperatorGroupId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="MovementSequenceId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="MovementSequenceId" Type="Int32" />
        <asp:Parameter Name="PickAreaId" Type="Int32" />
        <asp:Parameter Name="StoreAreaId" Type="Int32" />
        <asp:Parameter Name="StageAreaId" Type="Int32" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" />
        <asp:Parameter Name="StatusCode" Type="String" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="MovementSequenceId" Type="Int32" />
        <asp:Parameter Name="PickAreaId" Type="Int32" />
        <asp:Parameter Name="StoreAreaId" Type="Int32" />
        <asp:Parameter Name="StageAreaId" Type="Int32" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" />
        <asp:Parameter Name="StatusCode" Type="String" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
