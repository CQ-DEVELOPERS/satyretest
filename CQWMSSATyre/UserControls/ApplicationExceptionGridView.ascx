<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationExceptionGridView.ascx.cs" Inherits="UserControls_ApplicationExceptionGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowApplicationException()
{
   window.open("ApplicationExceptionFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="<%$ Resources:Default, ButtonNew %>" OnClientClick="javascript:openNewWindowApplicationException();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, ButtonEdit %>" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, ButtonSave %>" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, ButtonDelete %>" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="<%$ Resources:Default, ButtonSearch %>" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="<%$ Resources:Default, ButtonPrevious %>" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="<%$ Resources:Default, ButtonNext %>" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="Help"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewApplicationException" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="ApplicationExceptionId"
                DataSourceID="SqlDataSourceApplicationException" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewApplicationException_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="ApplicationExceptionId" HeaderText="ApplicationExceptionId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="ApplicationExceptionId" />
                        <asp:BoundField DataField="PriorityLevel" HeaderText="PriorityLevel" SortExpression="PriorityLevel" />
                        <asp:BoundField DataField="Module" HeaderText="Module" SortExpression="Module" />
                        <asp:BoundField DataField="Page" HeaderText="Page" SortExpression="Page" />
                        <asp:BoundField DataField="Method" HeaderText="Method" SortExpression="Method" />
                        <asp:BoundField DataField="ErrorMsg" HeaderText="ErrorMsg" SortExpression="ErrorMsg" />
                        <asp:TemplateField HeaderText="CreateDate" SortExpression="CreateDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonCreateDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('CreateDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceApplicationException" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_ApplicationException_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_ApplicationException_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_ApplicationException_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_ApplicationException_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="ApplicationExceptionId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="ApplicationExceptionId" Type="Int32" />
        <asp:Parameter Name="PriorityLevel" Type="Int32" />
        <asp:Parameter Name="Module" Type="String" />
        <asp:Parameter Name="Page" Type="String" />
        <asp:Parameter Name="Method" Type="String" />
        <asp:Parameter Name="ErrorMsg" Type="String" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="ApplicationExceptionId" Type="Int32" />
        <asp:Parameter Name="PriorityLevel" Type="Int32" />
        <asp:Parameter Name="Module" Type="String" />
        <asp:Parameter Name="Page" Type="String" />
        <asp:Parameter Name="Method" Type="String" />
        <asp:Parameter Name="ErrorMsg" Type="String" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
