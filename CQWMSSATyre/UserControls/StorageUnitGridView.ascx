<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageUnitGridView.ascx.cs" Inherits="UserControls_StorageUnitGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowStorageUnit()
{
   window.open("StorageUnitFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelSKUId" runat="server" Text="SKU:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSKU" runat="server" DataSourceID="SqlDataSourceSKUDDL"
                DataTextField="SKU" DataValueField="SKUId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceSKUDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_SKU_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelProductId" runat="server" Text="Product:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListProduct" runat="server" DataSourceID="SqlDataSourceProductDDL"
                DataTextField="Product" DataValueField="ProductId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceProductDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Product_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowStorageUnit();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="Search" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="Previous" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="Next" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="Help"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewStorageUnit" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
StorageUnitId
"
                DataSourceID="SqlDataSourceStorageUnit" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewStorageUnit_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="StorageUnitId" HeaderText="StorageUnitId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="StorageUnitId" />
            <asp:TemplateField HeaderText="SKU" meta:resourcekey="SKU" SortExpression="SKUId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListSKU" runat="server" DataSourceID="SqlDataSourceDDLSKU"
                        DataTextField="SKU" DataValueField="SKUId" SelectedValue='<%# Bind("SKUId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLSKU" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_SKU_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkSKUId" runat="server" Text='<%# Bind("SKU") %>' NavigateUrl="~/Administration/SKUGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="SKU" HeaderText="SKU" ReadOnly="True" SortExpression="SKU" Visible="False" />
            <asp:TemplateField HeaderText="Product" meta:resourcekey="Product" SortExpression="ProductId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListProduct" runat="server" DataSourceID="SqlDataSourceDDLProduct"
                        DataTextField="Product" DataValueField="ProductId" SelectedValue='<%# Bind("ProductId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLProduct" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Product_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkProductId" runat="server" Text='<%# Bind("Product") %>' NavigateUrl="~/Administration/ProductGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Product" HeaderText="Product" ReadOnly="True" SortExpression="Product" Visible="False" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceStorageUnit" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_StorageUnit_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_StorageUnit_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_StorageUnit_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_StorageUnit_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListSKU" DefaultValue="" Name="SKUId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListProduct" DefaultValue="" Name="ProductId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="SKUId" Type="Int32" />
        <asp:Parameter Name="ProductId" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="SKUId" Type="Int32" />
        <asp:Parameter Name="ProductId" Type="Int32" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
