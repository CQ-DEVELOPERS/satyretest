using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
 
public partial class UserControls_InboundLineFormView : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.LinkButtonShowError.Text = "";
    }
    protected void FormViewInserted(Object s, FormViewInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            LinkButtonShowError.OnClientClick="javascript:openNewWindow();";
            LinkButtonShowError.ForeColor = System.Drawing.Color.Red;
            LinkButtonShowError.Text = "There was an error inserting the row, click here to view.";
            Session["ErrorMessage"] = e.Exception.ToString();
            e.ExceptionHandled = true;
        }
        else
        {
            LinkButtonShowError.OnClientClick="";
            LinkButtonShowError.ForeColor = System.Drawing.Color.Green;
            LinkButtonShowError.Text = "The row was successfully inserted";
        }
    }
    protected void FormViewUpdated(Object s, FormViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            LinkButtonShowError.OnClientClick="javascript:openNewWindow();";
            LinkButtonShowError.ForeColor = System.Drawing.Color.Red;
            LinkButtonShowError.Text = "There was an error updating the row, click here to view.";
            Session["ErrorMessage"] = e.Exception.ToString();
            e.ExceptionHandled = true;
        }
        else
        {
            LinkButtonShowError.OnClientClick="";
            LinkButtonShowError.ForeColor = System.Drawing.Color.Green;
            LinkButtonShowError.Text = "The row was successfully updated";
        }
    }
}
