using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_InboundDocumentTypeGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridInboundDocumentType.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridInboundDocumentType_SelectedIndexChanged
    protected void RadGridInboundDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridInboundDocumentType.SelectedItems)
            {
                Session["InboundDocumentTypeId"] = item.GetDataKeyValue("InboundDocumentTypeId");
            }
        }
        catch { }
    }
    #endregion "RadGridInboundDocumentType_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["PriorityId"] != null)
          DropDownListPriorityId.SelectedValue = Session["PriorityId"].ToString();
    
        RadGridInboundDocumentType.DataBind();
    }
    #endregion BindData
    
}
