<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BatchFormView.ascx.cs" Inherits="UserControls_BatchFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Batch</h3>
<asp:FormView ID="FormViewBatch" runat="server" DataKeyNames="BatchId" DataSourceID="ObjectDataSourceBatch" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Batch:
                </td>
                <td>
                    <asp:TextBox ID="BatchTextBox" runat="server" Text='<%# Bind("Batch") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="BatchTextBox" ErrorMessage="* Please enter Batch"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="CreateDateTextBox" ErrorMessage="* Please enter CreateDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExpiryDate:
                </td>
                <td>
                    <asp:TextBox ID="ExpiryDateTextBox" runat="server" Text='<%# Bind("ExpiryDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    IncubationDays:
                </td>
                <td>
                    <asp:TextBox ID="IncubationDaysTextBox" runat="server" Text='<%# Bind("IncubationDays") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="IncubationDaysTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ShelfLifeDays:
                </td>
                <td>
                    <asp:TextBox ID="ShelfLifeDaysTextBox" runat="server" Text='<%# Bind("ShelfLifeDays") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="ShelfLifeDaysTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExpectedYield:
                </td>
                <td>
                    <asp:TextBox ID="ExpectedYieldTextBox" runat="server" Text='<%# Bind("ExpectedYield") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="ExpectedYieldTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ActualYield:
                </td>
                <td>
                    <asp:TextBox ID="ActualYieldTextBox" runat="server" Text='<%# Bind("ActualYield") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="ActualYieldTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ECLNumber:
                </td>
                <td>
                    <asp:TextBox ID="ECLNumberTextBox" runat="server" Text='<%# Bind("ECLNumber") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="<%$ Resources:Default, ButtonUpdate %>"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Batch:
                </td>
                <td>
                    <asp:TextBox ID="BatchTextBox" runat="server" Text='<%# Bind("Batch") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="BatchTextBox" ErrorMessage="* Please enter Batch"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="CreateDateTextBox" ErrorMessage="* Please enter CreateDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExpiryDate:
                </td>
                <td>
                    <asp:TextBox ID="ExpiryDateTextBox" runat="server" Text='<%# Bind("ExpiryDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    IncubationDays:
                </td>
                <td>
                    <asp:TextBox ID="IncubationDaysTextBox" runat="server" Text='<%# Bind("IncubationDays") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="IncubationDaysTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ShelfLifeDays:
                </td>
                <td>
                    <asp:TextBox ID="ShelfLifeDaysTextBox" runat="server" Text='<%# Bind("ShelfLifeDays") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="ShelfLifeDaysTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExpectedYield:
                </td>
                <td>
                    <asp:TextBox ID="ExpectedYieldTextBox" runat="server" Text='<%# Bind("ExpectedYield") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="ExpectedYieldTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ActualYield:
                </td>
                <td>
                    <asp:TextBox ID="ActualYieldTextBox" runat="server" Text='<%# Bind("ActualYield") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="ActualYieldTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ECLNumber:
                </td>
                <td>
                    <asp:TextBox ID="ECLNumberTextBox" runat="server" Text='<%# Bind("ECLNumber") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="<%$ Resources:Default, ButtonInsert %>"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListWarehouse" runat="server" DataSourceID="ObjectDataSourceDDLWarehouse"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("WarehouseId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLWarehouse" runat="server"
                        TypeName="StaticInfoWarehouse" SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Batch:
                </td>
                <td>
                    <asp:Label ID="BatchLabel" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:Label ID="CreateDateLabel" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ExpiryDate:
                </td>
                <td>
                    <asp:Label ID="ExpiryDateLabel" runat="server" Text='<%# Bind("ExpiryDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    IncubationDays:
                </td>
                <td>
                    <asp:Label ID="IncubationDaysLabel" runat="server" Text='<%# Bind("IncubationDays") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ShelfLifeDays:
                </td>
                <td>
                    <asp:Label ID="ShelfLifeDaysLabel" runat="server" Text='<%# Bind("ShelfLifeDays") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ExpectedYield:
                </td>
                <td>
                    <asp:Label ID="ExpectedYieldLabel" runat="server" Text='<%# Bind("ExpectedYield") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ActualYield:
                </td>
                <td>
                    <asp:Label ID="ActualYieldLabel" runat="server" Text='<%# Bind("ActualYield") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ECLNumber:
                </td>
                <td>
                    <asp:Label ID="ECLNumberLabel" runat="server" Text='<%# Bind("ECLNumber") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBack %>" PostBackUrl="~/Administration/BatchGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceBatch" runat="server" TypeName="StaticInfoBatch"
    SelectMethod="GetBatch"
    DeleteMethod="DeleteBatch"
    UpdateMethod="UpdateBatch"
    InsertMethod="InsertBatch">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="BatchId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="BatchId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="Batch" Type="String" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="ExpiryDate" Type="DateTime" />
        <asp:Parameter Name="IncubationDays" Type="Int32" />
        <asp:Parameter Name="ShelfLifeDays" Type="Int32" />
        <asp:Parameter Name="ExpectedYield" Type="Int32" />
        <asp:Parameter Name="ActualYield" Type="Int32" />
        <asp:Parameter Name="ECLNumber" Type="String" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="BatchId" QueryStringField="BatchId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="BatchId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="Batch" Type="String" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="ExpiryDate" Type="DateTime" />
        <asp:Parameter Name="IncubationDays" Type="Int32" />
        <asp:Parameter Name="ShelfLifeDays" Type="Int32" />
        <asp:Parameter Name="ExpectedYield" Type="Int32" />
        <asp:Parameter Name="ActualYield" Type="Int32" />
        <asp:Parameter Name="ECLNumber" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
