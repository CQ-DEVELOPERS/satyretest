<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuItemGridView.ascx.cs" Inherits="UserControls_MenuItemGridView" %>
 
<table style="background-color:lightgray; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <telerik:RadComboBox ID="DropDownListMenuId" runat="server" DataSourceID="ObjectDataSourceDDLMenuId"
                DataTextField="Menu" DataValueField="MenuId" Label="Menu" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLMenuId" runat="server" TypeName="StaticInfoMenu"
                SelectMethod="ParameterMenu">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <telerik:RadTextBox ID="TextBoxMenuItem" runat="server" Label="MenuItem" Width="300px" Skin="Metro"></telerik:RadTextBox>
        </td>
        <td>
            <telerik:RadComboBox ID="DropDownListParentMenuItemId" runat="server" DataSourceID="ObjectDataSourceDDLParentMenuItemId"
                DataTextField="MenuItem" DataValueField="MenuItemId" Label="MenuItem" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLParentMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
                SelectMethod="ParameterMenuItem">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
        <td>
            <telerik:RadButton ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"></telerik:RadButton>
        </td>
    </tr>
</table>
 
<telerik:RadGrid ID="RadGridMenuItem" runat="server" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AutoGenerateDeleteColumn="true"
    AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="30" DataSourceID="ObjectDataSourceMenuItem" OnSelectedIndexChanged="RadGridMenuItem_SelectedIndexChanged" Skin="Metro">
<PagerStyle Mode="NumericPages"></PagerStyle>
<MasterTableView DataKeyNames="
MenuItemId
" DataSourceID="ObjectDataSourceMenuItem"
    CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
    InsertItemPageIndexAction="ShowItemOnFirstPage">
        <Columns>
            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="MenuItemId" HeaderText="MenuItemId" SortExpression="MenuItemId" UniqueName="MenuItemId"></telerik:GridBoundColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListMenuId" ListTextField="Menu" ListValueField="MenuId" DataSourceID="ObjectDataSourceMenuId" DataField="MenuId" HeaderText="Menu">
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="MenuItem" HeaderText="MenuItem" SortExpression="MenuItem" UniqueName="MenuItem"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ToolTip" HeaderText="ToolTip" SortExpression="ToolTip" UniqueName="ToolTip"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="NavigateTo" HeaderText="NavigateTo" SortExpression="NavigateTo" UniqueName="NavigateTo"></telerik:GridBoundColumn>
            <telerik:GridDropDownColumn UniqueName="DropDownListParentMenuItemId" ListTextField="MenuItem" ListValueField="MenuItemId" DataSourceID="ObjectDataSourceMenuItemId" DataField="ParentMenuItemId" HeaderText="MenuItem">
            </telerik:GridDropDownColumn>
            <telerik:GridBoundColumn DataField="OrderBy" HeaderText="OrderBy" SortExpression="OrderBy" UniqueName="OrderBy"></telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
       </ClientSettings>
    <GroupingSettings ShowUnGroupButton="True" />
</telerik:RadGrid>
<asp:ObjectDataSource ID="ObjectDataSourceMenuId" runat="server" TypeName="StaticInfoMenu"
    SelectMethod="ParameterMenu">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSourceParentMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
    SelectMethod="ParameterMenuItem">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
 
<asp:ObjectDataSource ID="ObjectDataSourceMenuItem" runat="server" TypeName="StaticInfoMenuItem"
    DeleteMethod="DeleteMenuItem"
    InsertMethod="InsertMenuItem"
    SelectMethod="SearchMenuItem"
    UpdateMethod="UpdateMenuItem">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="DropDownListMenuId" DefaultValue="-1" Name="MenuId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="TextBoxMenuItem" DefaultValue="%" Name="MenuItem" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="DropDownListParentMenuItemId" DefaultValue="-1" Name="ParentMenuItemId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MenuItemId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="MenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItem" Type="String" />
        <asp:Parameter Name="ToolTip" Type="String" />
        <asp:Parameter Name="NavigateTo" Type="String" />
        <asp:Parameter Name="ParentMenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OrderBy" Type="Int16" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="MenuItemId" Type="Int32" />
        <asp:Parameter Name="MenuId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItem" Type="String" />
        <asp:Parameter Name="ToolTip" Type="String" />
        <asp:Parameter Name="NavigateTo" Type="String" />
        <asp:Parameter Name="ParentMenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OrderBy" Type="Int16" />
    </InsertParameters>
</asp:ObjectDataSource>
