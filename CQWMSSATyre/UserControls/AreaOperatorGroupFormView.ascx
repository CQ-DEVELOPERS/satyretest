<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AreaOperatorGroupFormView.ascx.cs" Inherits="UserControls_AreaOperatorGroupFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>AreaOperatorGroup</h3>
<asp:FormView ID="FormViewAreaOperatorGroup" runat="server" DataKeyNames="AreaOperatorGroupId" DataSourceID="ObjectDataSourceAreaOperatorGroup" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="<%$ Resources:Default, ButtonUpdate %>"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Area:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListArea" runat="server" DataSourceID="ObjectDataSourceDDLArea"
                        DataTextField="Area" DataValueField="AreaId" SelectedValue='<%# Bind("AreaId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLArea" runat="server"
                        TypeName="StaticInfoArea" SelectMethod="ParameterArea">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListArea" ErrorMessage="* Please enter AreaId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroup" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroup"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroup" runat="server"
                        TypeName="StaticInfoOperatorGroup" SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListOperatorGroup" ErrorMessage="* Please enter OperatorGroupId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="<%$ Resources:Default, ButtonInsert %>"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="<%$ Resources:Default, ButtonCancel %>"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Area:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListArea" runat="server" DataSourceID="ObjectDataSourceDDLArea"
                        DataTextField="Area" DataValueField="AreaId" SelectedValue='<%# Bind("AreaId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLArea" runat="server"
                        TypeName="StaticInfoArea" SelectMethod="ParameterArea">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroup" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroup"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroup" runat="server"
                        TypeName="StaticInfoOperatorGroup" SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="<%$ Resources:Default, ButtonEdit %>"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="<%$ Resources:Default, ButtonDelete %>"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="<%$ Resources:Default, ButtonNew %>"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="<%$ Resources:Default, ButtonBack %>" PostBackUrl="~/Administration/AreaOperatorGroupGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceAreaOperatorGroup" runat="server" TypeName="StaticInfoAreaOperatorGroup"
    SelectMethod="GetAreaOperatorGroup"
    DeleteMethod="DeleteAreaOperatorGroup"
    UpdateMethod="UpdateAreaOperatorGroup"
    InsertMethod="InsertAreaOperatorGroup">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AreaOperatorGroupId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="AreaId" QueryStringField="AreaId" DefaultValue="-1" Type="Int32" />
      <asp:QueryStringParameter Name="OperatorGroupId" QueryStringField="OperatorGroupId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AreaId" Type="Int32" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="<%$ Resources:Default, ButtonClose %>" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
