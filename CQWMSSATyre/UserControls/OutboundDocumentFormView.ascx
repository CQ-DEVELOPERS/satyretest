<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutboundDocumentFormView.ascx.cs" Inherits="UserControls_OutboundDocumentFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>OutboundDocument</h3>
<asp:FormView ID="FormViewOutboundDocument" runat="server" DataKeyNames="OutboundDocumentId" DataSourceID="ObjectDataSourceOutboundDocument" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocumentType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocumentType"
                        DataTextField="OutboundDocumentType" DataValueField="OutboundDocumentTypeId" SelectedValue='<%# Bind("OutboundDocumentTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocumentType" runat="server"
                        TypeName="StaticInfoOutboundDocumentType" SelectMethod="ParameterOutboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListOutboundDocumentType" ErrorMessage="* Please enter OutboundDocumentTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompany" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompany"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompany" runat="server"
                        TypeName="StaticInfoExternalCompany" SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListExternalCompany" ErrorMessage="* Please enter ExternalCompanyId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OrderNumber:
                </td>
                <td>
                    <asp:TextBox ID="OrderNumberTextBox" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="OrderNumberTextBox" ErrorMessage="* Please enter OrderNumber"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryDateTextBox" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="DeliveryDateTextBox" ErrorMessage="* Please enter DeliveryDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="CreateDateTextBox" ErrorMessage="* Please enter CreateDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ModifiedDate:
                </td>
                <td>
                    <asp:TextBox ID="ModifiedDateTextBox" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocumentType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocumentType"
                        DataTextField="OutboundDocumentType" DataValueField="OutboundDocumentTypeId" SelectedValue='<%# Bind("OutboundDocumentTypeId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocumentType" runat="server"
                        TypeName="StaticInfoOutboundDocumentType" SelectMethod="ParameterOutboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListOutboundDocumentType" ErrorMessage="* Please enter OutboundDocumentTypeId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompany" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompany"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompany" runat="server"
                        TypeName="StaticInfoExternalCompany" SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListExternalCompany" ErrorMessage="* Please enter ExternalCompanyId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OrderNumber:
                </td>
                <td>
                    <asp:TextBox ID="OrderNumberTextBox" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="OrderNumberTextBox" ErrorMessage="* Please enter OrderNumber"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryDateTextBox" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="DeliveryDateTextBox" ErrorMessage="* Please enter DeliveryDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:TextBox ID="CreateDateTextBox" runat="server" Text='<%# Bind("CreateDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="CreateDateTextBox" ErrorMessage="* Please enter CreateDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ModifiedDate:
                </td>
                <td>
                    <asp:TextBox ID="ModifiedDateTextBox" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    OutboundDocumentType:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOutboundDocumentType" runat="server" DataSourceID="ObjectDataSourceDDLOutboundDocumentType"
                        DataTextField="OutboundDocumentType" DataValueField="OutboundDocumentTypeId" SelectedValue='<%# Bind("OutboundDocumentTypeId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOutboundDocumentType" runat="server"
                        TypeName="StaticInfoOutboundDocumentType" SelectMethod="ParameterOutboundDocumentType">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompany" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompany"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompany" runat="server"
                        TypeName="StaticInfoExternalCompany" SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Warehouse:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListWarehouse" runat="server" DataSourceID="ObjectDataSourceDDLWarehouse"
                        DataTextField="Warehouse" DataValueField="WarehouseId" SelectedValue='<%# Bind("WarehouseId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLWarehouse" runat="server"
                        TypeName="StaticInfoWarehouse" SelectMethod="ParameterWarehouse">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OrderNumber:
                </td>
                <td>
                    <asp:Label ID="OrderNumberLabel" runat="server" Text='<%# Bind("OrderNumber") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryDate:
                </td>
                <td>
                    <asp:Label ID="DeliveryDateLabel" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    CreateDate:
                </td>
                <td>
                    <asp:Label ID="CreateDateLabel" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ModifiedDate:
                </td>
                <td>
                    <asp:Label ID="ModifiedDateLabel" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/OutboundDocumentGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceOutboundDocument" runat="server" TypeName="StaticInfoOutboundDocument"
    SelectMethod="GetOutboundDocument"
    DeleteMethod="DeleteOutboundDocument"
    UpdateMethod="UpdateOutboundDocument"
    InsertMethod="InsertOutboundDocument">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundDocumentId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundDocumentId" Type="Int32" />
        <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="OrderNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="ModifiedDate" Type="DateTime" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="OutboundDocumentId" QueryStringField="OutboundDocumentId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OutboundDocumentId" Type="Int32" />
        <asp:Parameter Name="OutboundDocumentTypeId" Type="Int32" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="OrderNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
        <asp:Parameter Name="ModifiedDate" Type="DateTime" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
