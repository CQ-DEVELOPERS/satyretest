<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExceptionGridView.ascx.cs" Inherits="UserControls_ExceptionGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowException()
{
   window.open("ExceptionFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelReceiptLineId" runat="server" Text="<%$ Resources:Default, LabelReceiptLine %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="SqlDataSourceReceiptLineDDL"
                DataTextField="ReceiptLine" DataValueField="ReceiptLineId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceReceiptLineDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_ReceiptLine_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelInstructionId" runat="server" Text="<%$ Resources:Default, LabelInstruction %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListInstruction" runat="server" DataSourceID="SqlDataSourceInstructionDDL"
                DataTextField="InstructionId" DataValueField="InstructionId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceInstructionDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Instruction_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelIssueLineId" runat="server" Text="<%$ Resources:Default, LabelIssueLine %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="SqlDataSourceIssueLineDDL"
                DataTextField="IssueLine" DataValueField="IssueLineId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceIssueLineDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_IssueLine_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelReasonId" runat="server" Text="<%$ Resources:Default, LabelReason %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="SqlDataSourceReasonDDL"
                DataTextField="Reason" DataValueField="ReasonId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceReasonDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Reason_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelException" runat="server" Text="<%$ Resources:Default, LabelException %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxException" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelExceptionCode" runat="server" Text="<%$ Resources:Default, LabelExceptionCode %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxExceptionCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="<%$ Resources:Default, LabelShow %>"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="<%$ Resources:Default, ButtonNew %>" OnClientClick="javascript:openNewWindowException();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, ButtonEdit %>" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, ButtonSave %>" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, ButtonDelete %>" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="<%$ Resources:Default, ButtonBack %>" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="<%$ Resources:Default, ButtonPrevious %>" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="<%$ Resources:Default, ButtonNext %>" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="<%$ Resources:Default, ButtonHelp %>"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewException" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
ExceptionId
"
                DataSourceID="SqlDataSourceException" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewException_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="ExceptionId" HeaderText="ExceptionId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="ExceptionId" />
            <asp:TemplateField HeaderText="ReceiptLine" meta:resourcekey="ReceiptLine" SortExpression="ReceiptLineId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="SqlDataSourceDDLReceiptLine"
                        DataTextField="ReceiptLine" DataValueField="ReceiptLineId" SelectedValue='<%# Bind("ReceiptLineId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLReceiptLine" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_ReceiptLine_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkReceiptLineId" runat="server" Text='<%# Bind("ReceiptLine") %>' NavigateUrl="~/Administration/ReceiptLineGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ReceiptLine" HeaderText="ReceiptLine" ReadOnly="True" SortExpression="ReceiptLine" Visible="False" />
            <asp:TemplateField HeaderText="Instruction" meta:resourcekey="Instruction" SortExpression="InstructionId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListInstruction" runat="server" DataSourceID="SqlDataSourceDDLInstruction"
                        DataTextField="Instruction" DataValueField="InstructionId" SelectedValue='<%# Bind("InstructionId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLInstruction" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Instruction_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkInstructionId" runat="server" Text='<%# Bind("Instruction") %>' NavigateUrl="~/Administration/InstructionGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Instruction" HeaderText="Instruction" ReadOnly="True" SortExpression="Instruction" Visible="False" />
            <asp:TemplateField HeaderText="IssueLine" meta:resourcekey="IssueLine" SortExpression="IssueLineId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="SqlDataSourceDDLIssueLine"
                        DataTextField="IssueLine" DataValueField="IssueLineId" SelectedValue='<%# Bind("IssueLineId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLIssueLine" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_IssueLine_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkIssueLineId" runat="server" Text='<%# Bind("IssueLine") %>' NavigateUrl="~/Administration/IssueLineGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="IssueLine" HeaderText="IssueLine" ReadOnly="True" SortExpression="IssueLine" Visible="False" />
            <asp:TemplateField HeaderText="Reason" meta:resourcekey="Reason" SortExpression="ReasonId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListReason" runat="server" DataSourceID="SqlDataSourceDDLReason"
                        DataTextField="Reason" DataValueField="ReasonId" SelectedValue='<%# Bind("ReasonId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLReason" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Reason_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkReasonId" runat="server" Text='<%# Bind("Reason") %>' NavigateUrl="~/Administration/ReasonGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Reason" HeaderText="Reason" ReadOnly="True" SortExpression="Reason" Visible="False" />
                        <asp:BoundField DataField="Exception" HeaderText="Exception" SortExpression="Exception" />
                        <asp:BoundField DataField="ExceptionCode" HeaderText="ExceptionCode" SortExpression="ExceptionCode" />
                        <asp:TemplateField HeaderText="CreateDate" SortExpression="CreateDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonCreateDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('CreateDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelCreateDate" runat="server" Text='<%# Bind("CreateDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceException" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_Exception_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_Exception_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_Exception_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_Exception_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListReceiptLine" DefaultValue="" Name="ReceiptLineId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListInstruction" DefaultValue="" Name="InstructionId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListIssueLine" DefaultValue="" Name="IssueLineId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListReason" DefaultValue="" Name="ReasonId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="TextBoxException" DefaultValue="%" Name="Exception" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="TextBoxExceptionCode" DefaultValue="%" Name="ExceptionCode" PropertyName="Text" Type="String" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="ExceptionId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="ExceptionId" Type="Int32" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="InstructionId" Type="Int32" />
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="ReasonId" Type="Int32" />
        <asp:Parameter Name="Exception" Type="String" />
        <asp:Parameter Name="ExceptionCode" Type="String" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="ExceptionId" Type="Int32" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="InstructionId" Type="Int32" />
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="ReasonId" Type="Int32" />
        <asp:Parameter Name="Exception" Type="String" />
        <asp:Parameter Name="ExceptionCode" Type="String" />
        <asp:Parameter Name="CreateDate" Type="DateTime" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
