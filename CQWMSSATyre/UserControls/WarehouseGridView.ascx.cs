using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_WarehouseGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridWarehouse.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridWarehouse_SelectedIndexChanged
    protected void RadGridWarehouse_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridWarehouse.SelectedItems)
            {
                Session["WarehouseId"] = item.GetDataKeyValue("WarehouseId");
            }
        }
        catch { }
    }
    #endregion "RadGridWarehouse_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["CompanyId"] != null)
          DropDownListCompanyId.SelectedValue = Session["CompanyId"].ToString();
        if (Session["ParentWarehouseId"] != null)
          DropDownListParentWarehouseId.SelectedValue = Session["ParentWarehouseId"].ToString();
        if (Session["AddressId"] != null)
          DropDownListAddressId.SelectedValue = Session["AddressId"].ToString();
        if (Session["PostalAddressId"] != null)
          DropDownListPostalAddressId.SelectedValue = Session["PostalAddressId"].ToString();
        if (Session["ContactListId"] != null)
          DropDownListContactListId.SelectedValue = Session["ContactListId"].ToString();
    
        RadGridWarehouse.DataBind();
    }
    #endregion BindData
    
}
