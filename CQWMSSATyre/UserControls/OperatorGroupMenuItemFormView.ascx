<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperatorGroupMenuItemFormView.ascx.cs" Inherits="UserControls_OperatorGroupMenuItemFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>OperatorGroupMenuItem</h3>
<asp:FormView ID="FormViewOperatorGroupMenuItem" runat="server" DataKeyNames="OperatorGroupMenuItemId" DataSourceID="ObjectDataSourceOperatorGroupMenuItem" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Access:
                </td>
                <td>
                    <asp:CheckBox ID="AccessCheckBox" runat="server" Checked='<%# Bind("Access") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    OperatorGroupId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroupId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroupId"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroupId" runat="server" TypeName="StaticInfoOperatorGroup"
                        SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListOperatorGroupId"
                        ErrorMessage="* Please enter OperatorGroupId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MenuItemId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMenuItemId" runat="server" DataSourceID="ObjectDataSourceDDLMenuItemId"
                        DataTextField="MenuItem" DataValueField="MenuItemId" SelectedValue='<%# Bind("MenuItemId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
                        SelectMethod="ParameterMenuItem">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListMenuItemId"
                        ErrorMessage="* Please enter MenuItemId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Access:
                </td>
                <td>
                    <asp:CheckBox ID="AccessCheckBox" runat="server" Checked='<%# Bind("Access") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    OperatorGroup:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorGroupId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorGroupId"
                        DataTextField="OperatorGroup" DataValueField="OperatorGroupId" SelectedValue='<%# Bind("OperatorGroupId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorGroupId" runat="server" TypeName="StaticInfoOperatorGroup"
                        SelectMethod="ParameterOperatorGroup">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    MenuItem:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListMenuItemId" runat="server" DataSourceID="ObjectDataSourceDDLMenuItemId"
                        DataTextField="MenuItem" DataValueField="MenuItemId" SelectedValue='<%# Bind("MenuItemId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLMenuItemId" runat="server" TypeName="StaticInfoMenuItem"
                        SelectMethod="ParameterMenuItem">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Access:
                </td>
                <td>
                    <asp:Label ID="AccessLabel" runat="server" Text='<%# Bind("Access") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/OperatorGroupMenuItemGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceOperatorGroupMenuItem" runat="server" TypeName="StaticInfoOperatorGroupMenuItem"
    SelectMethod="GetOperatorGroupMenuItem"
    DeleteMethod="DeleteOperatorGroupMenuItem"
    UpdateMethod="UpdateOperatorGroupMenuItem"
    InsertMethod="InsertOperatorGroupMenuItem">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorGroupMenuItemId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Access" Type="Boolean" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="OperatorGroupId" QueryStringField="OperatorGroupId" DefaultValue="-1" Type="Int32" />
      <asp:QueryStringParameter Name="MenuItemId" QueryStringField="MenuItemId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="OperatorGroupId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="MenuItemId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Access" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
