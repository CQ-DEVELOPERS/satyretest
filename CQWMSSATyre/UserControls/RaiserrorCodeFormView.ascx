<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RaiserrorCodeFormView.ascx.cs" Inherits="UserControls_RaiserrorCodeFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>RaiserrorCode</h3>
<asp:FormView ID="FormViewRaiserrorCode" runat="server" DataKeyNames="RaiserrorCodeId" DataSourceID="ObjectDataSourceRaiserrorCode" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    RaiserrorCode:
                </td>
                <td>
                    <asp:TextBox ID="RaiserrorCodeTextBox" runat="server" Text='<%# Bind("RaiserrorCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RaiserrorMessage:
                </td>
                <td>
                    <asp:TextBox ID="RaiserrorMessageTextBox" runat="server" Text='<%# Bind("RaiserrorMessage") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StoredProcedure:
                </td>
                <td>
                    <asp:TextBox ID="StoredProcedureTextBox" runat="server" Text='<%# Bind("StoredProcedure") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="StoredProcedureTextBox" ErrorMessage="* Please enter StoredProcedure"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    RaiserrorCode:
                </td>
                <td>
                    <asp:TextBox ID="RaiserrorCodeTextBox" runat="server" Text='<%# Bind("RaiserrorCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    RaiserrorMessage:
                </td>
                <td>
                    <asp:TextBox ID="RaiserrorMessageTextBox" runat="server" Text='<%# Bind("RaiserrorMessage") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StoredProcedure:
                </td>
                <td>
                    <asp:TextBox ID="StoredProcedureTextBox" runat="server" Text='<%# Bind("StoredProcedure") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="StoredProcedureTextBox" ErrorMessage="* Please enter StoredProcedure"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    RaiserrorCode:
                </td>
                <td>
                    <asp:Label ID="RaiserrorCodeLabel" runat="server" Text='<%# Bind("RaiserrorCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    RaiserrorMessage:
                </td>
                <td>
                    <asp:Label ID="RaiserrorMessageLabel" runat="server" Text='<%# Bind("RaiserrorMessage") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StoredProcedure:
                </td>
                <td>
                    <asp:Label ID="StoredProcedureLabel" runat="server" Text='<%# Bind("StoredProcedure") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/RaiserrorCodeGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceRaiserrorCode" runat="server" TypeName="StaticInfoRaiserrorCode"
    SelectMethod="GetRaiserrorCode"
    DeleteMethod="DeleteRaiserrorCode"
    UpdateMethod="UpdateRaiserrorCode"
    InsertMethod="InsertRaiserrorCode">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RaiserrorCodeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RaiserrorCodeId" Type="Int32" />
        <asp:Parameter Name="RaiserrorCode" Type="String" />
        <asp:Parameter Name="RaiserrorMessage" Type="String" />
        <asp:Parameter Name="StoredProcedure" Type="String" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="RaiserrorCodeId" QueryStringField="RaiserrorCodeId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RaiserrorCodeId" Type="Int32" />
        <asp:Parameter Name="RaiserrorCode" Type="String" />
        <asp:Parameter Name="RaiserrorMessage" Type="String" />
        <asp:Parameter Name="StoredProcedure" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
