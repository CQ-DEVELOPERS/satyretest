<%@ Control Language="C#" AutoEventWireup="true" CodeFile="COAFormView.ascx.cs" Inherits="UserControls_COAFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>COA</h3>
<asp:FormView ID="FormViewCOA" runat="server" DataKeyNames="COAId" DataSourceID="ObjectDataSourceCOA" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    COACode:
                </td>
                <td>
                    <asp:TextBox ID="COACodeTextBox" runat="server" Text='<%# Bind("COACode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    COA:
                </td>
                <td>
                    <asp:TextBox ID="COATextBox" runat="server" Text='<%# Bind("COA") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnitBatchId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatchId" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatchId"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatchId" runat="server" TypeName="StaticInfoStorageUnitBatch"
                        SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    COACode:
                </td>
                <td>
                    <asp:TextBox ID="COACodeTextBox" runat="server" Text='<%# Bind("COACode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    COA:
                </td>
                <td>
                    <asp:TextBox ID="COATextBox" runat="server" Text='<%# Bind("COA") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnitBatchId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatchId" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatchId"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatchId" runat="server" TypeName="StaticInfoStorageUnitBatch"
                        SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    COACode:
                </td>
                <td>
                    <asp:Label ID="COACodeLabel" runat="server" Text='<%# Bind("COACode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    COA:
                </td>
                <td>
                    <asp:Label ID="COALabel" runat="server" Text='<%# Bind("COA") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnitBatch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatchId" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatchId"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatchId" runat="server" TypeName="StaticInfoStorageUnitBatch"
                        SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/COAGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceCOA" runat="server" TypeName="StaticInfoCOA"
    SelectMethod="GetCOA"
    DeleteMethod="DeleteCOA"
    UpdateMethod="UpdateCOA"
    InsertMethod="InsertCOA">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COAId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="COACode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="COA" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="COAId" QueryStringField="COAId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="COACode" Type="String" />
        <asp:Parameter Name="COA" Type="String" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" DefaultValue="-1" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
