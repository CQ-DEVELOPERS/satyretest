<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResultFormView.ascx.cs" Inherits="UserControls_ResultFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Result</h3>
<asp:FormView ID="FormViewResult" runat="server" DataKeyNames="ResultId" DataSourceID="ObjectDataSourceResult" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    ResultCode:
                </td>
                <td>
                    <asp:TextBox ID="ResultCodeTextBox" runat="server" Text='<%# Bind("ResultCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Result:
                </td>
                <td>
                    <asp:TextBox ID="ResultTextBox" runat="server" Text='<%# Bind("Result") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Pass:
                </td>
                <td>
                    <asp:CheckBox ID="PassCheckBox" runat="server" Checked='<%# Bind("Pass") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StartRange:
                </td>
                <td>
                    <asp:TextBox ID="StartRangeTextBox" runat="server" Text='<%# Bind("StartRange") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    EndRange:
                </td>
                <td>
                    <asp:TextBox ID="EndRangeTextBox" runat="server" Text='<%# Bind("EndRange") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    ResultCode:
                </td>
                <td>
                    <asp:TextBox ID="ResultCodeTextBox" runat="server" Text='<%# Bind("ResultCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Result:
                </td>
                <td>
                    <asp:TextBox ID="ResultTextBox" runat="server" Text='<%# Bind("Result") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Pass:
                </td>
                <td>
                    <asp:CheckBox ID="PassCheckBox" runat="server" Checked='<%# Bind("Pass") %>'></asp:CheckBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    StartRange:
                </td>
                <td>
                    <asp:TextBox ID="StartRangeTextBox" runat="server" Text='<%# Bind("StartRange") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    EndRange:
                </td>
                <td>
                    <asp:TextBox ID="EndRangeTextBox" runat="server" Text='<%# Bind("EndRange") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    ResultCode:
                </td>
                <td>
                    <asp:Label ID="ResultCodeLabel" runat="server" Text='<%# Bind("ResultCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Result:
                </td>
                <td>
                    <asp:Label ID="ResultLabel" runat="server" Text='<%# Bind("Result") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Pass:
                </td>
                <td>
                    <asp:Label ID="PassLabel" runat="server" Text='<%# Bind("Pass") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    StartRange:
                </td>
                <td>
                    <asp:Label ID="StartRangeLabel" runat="server" Text='<%# Bind("StartRange") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    EndRange:
                </td>
                <td>
                    <asp:Label ID="EndRangeLabel" runat="server" Text='<%# Bind("EndRange") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ResultGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceResult" runat="server" TypeName="StaticInfoResult"
    SelectMethod="GetResult"
    DeleteMethod="DeleteResult"
    UpdateMethod="UpdateResult"
    InsertMethod="InsertResult">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ResultId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ResultId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Result" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Pass" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="StartRange" Type="Double" DefaultValue="-1" />
        <asp:Parameter Name="EndRange" Type="Double" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ResultId" QueryStringField="ResultId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ResultId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultCode" Type="String" />
        <asp:Parameter Name="Result" Type="String" />
        <asp:Parameter Name="Pass" Type="Boolean" />
        <asp:Parameter Name="StartRange" Type="Double" />
        <asp:Parameter Name="EndRange" Type="Double" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
