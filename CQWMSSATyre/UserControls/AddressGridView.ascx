<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddressGridView.ascx.cs" Inherits="UserControls_AddressGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowAddress()
{
   window.open("AddressFormView.aspx", "_blank",
      "height=640px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelExternalCompanyId" runat="server" Text="ExternalCompany:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListExternalCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyId"
                DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" >
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyId" runat="server" TypeName="StaticInfoExternalCompany"
                SelectMethod="ParameterExternalCompany">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"/>
        <td>
            <asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowAddress();"/>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Visible="False"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewAddress" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
AddressId
"
                DataSourceID="ObjectDataSourceAddress" OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewAddress_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit" Visible="False">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="False" ShowEditButton="True" />
        <asp:BoundField DataField="AddressId" HeaderText="AddressId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="AddressId" />
            <asp:TemplateField HeaderText="ExternalCompanyId" meta:resourcekey="ExternalCompanyId" SortExpression="ExternalCompanyId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListExternalCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyId"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyId" runat="server" TypeName="StaticInfoExternalCompany"
                        SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkExternalCompanyId" runat="server" Text='<%# Bind("ExternalCompany") %>' NavigateUrl="~/Administration/ExternalCompanyGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Street" HeaderText="Street" SortExpression="Street" />
                        <asp:BoundField DataField="Suburb" HeaderText="Suburb" SortExpression="Suburb" />
                        <asp:BoundField DataField="Town" HeaderText="Town" SortExpression="Town" />
                        <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
                        <asp:BoundField DataField="Code" HeaderText="Code" SortExpression="Code" />
                    <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:ObjectDataSource ID="ObjectDataSourceAddress" runat="server" TypeName="StaticInfoAddress"
    DeleteMethod="DeleteAddress"
    InsertMethod="InsertAddress"
    SelectMethod="SearchAddress"
    UpdateMethod="UpdateAddress">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AddressId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="DropDownListExternalCompanyId" DefaultValue="-1" Name="ExternalCompanyId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AddressId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AddressId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Street" Type="String" />
        <asp:Parameter Name="Suburb" Type="String" />
        <asp:Parameter Name="Town" Type="String" />
        <asp:Parameter Name="Country" Type="String" />
        <asp:Parameter Name="Code" Type="String" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="AddressId" Type="Int32" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Street" Type="String" />
        <asp:Parameter Name="Suburb" Type="String" />
        <asp:Parameter Name="Town" Type="String" />
        <asp:Parameter Name="Country" Type="String" />
        <asp:Parameter Name="Code" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
