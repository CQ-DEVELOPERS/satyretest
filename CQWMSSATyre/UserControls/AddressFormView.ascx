<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddressFormView.ascx.cs" Inherits="UserControls_AddressFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Address</h3>
<asp:FormView ID="FormViewAddress" runat="server" DataKeyNames="AddressId" DataSourceID="ObjectDataSourceAddress" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompanyId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyId"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyId" runat="server" TypeName="StaticInfoExternalCompany"
                        SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListExternalCompanyId"
                        ErrorMessage="* Please enter ExternalCompanyId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Street:
                </td>
                <td>
                    <asp:TextBox ID="StreetTextBox" runat="server" Text='<%# Bind("Street") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Suburb:
                </td>
                <td>
                    <asp:TextBox ID="SuburbTextBox" runat="server" Text='<%# Bind("Suburb") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Town:
                </td>
                <td>
                    <asp:TextBox ID="TownTextBox" runat="server" Text='<%# Bind("Town") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Country:
                </td>
                <td>
                    <asp:TextBox ID="CountryTextBox" runat="server" Text='<%# Bind("Country") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Code:
                </td>
                <td>
                    <asp:TextBox ID="CodeTextBox" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompanyId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyId"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyId" runat="server" TypeName="StaticInfoExternalCompany"
                        SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListExternalCompanyId"
                        ErrorMessage="* Please enter ExternalCompanyId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Street:
                </td>
                <td>
                    <asp:TextBox ID="StreetTextBox" runat="server" Text='<%# Bind("Street") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Suburb:
                </td>
                <td>
                    <asp:TextBox ID="SuburbTextBox" runat="server" Text='<%# Bind("Suburb") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Town:
                </td>
                <td>
                    <asp:TextBox ID="TownTextBox" runat="server" Text='<%# Bind("Town") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Country:
                </td>
                <td>
                    <asp:TextBox ID="CountryTextBox" runat="server" Text='<%# Bind("Country") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Code:
                </td>
                <td>
                    <asp:TextBox ID="CodeTextBox" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyId"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyId" runat="server" TypeName="StaticInfoExternalCompany"
                        SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Street:
                </td>
                <td>
                    <asp:Label ID="StreetLabel" runat="server" Text='<%# Bind("Street") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Suburb:
                </td>
                <td>
                    <asp:Label ID="SuburbLabel" runat="server" Text='<%# Bind("Suburb") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Town:
                </td>
                <td>
                    <asp:Label ID="TownLabel" runat="server" Text='<%# Bind("Town") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Country:
                </td>
                <td>
                    <asp:Label ID="CountryLabel" runat="server" Text='<%# Bind("Country") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Code:
                </td>
                <td>
                    <asp:Label ID="CodeLabel" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/AddressGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceAddress" runat="server" TypeName="StaticInfoAddress"
    SelectMethod="GetAddress"
    DeleteMethod="DeleteAddress"
    UpdateMethod="UpdateAddress"
    InsertMethod="InsertAddress">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AddressId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AddressId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Street" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Suburb" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Town" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Country" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Code" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="AddressId" QueryStringField="AddressId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="AddressId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Street" Type="String" />
        <asp:Parameter Name="Suburb" Type="String" />
        <asp:Parameter Name="Town" Type="String" />
        <asp:Parameter Name="Country" Type="String" />
        <asp:Parameter Name="Code" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
