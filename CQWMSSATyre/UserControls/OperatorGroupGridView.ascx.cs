using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_OperatorGroupGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridOperatorGroup.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridOperatorGroup_SelectedIndexChanged
    protected void RadGridOperatorGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridOperatorGroup.SelectedItems)
            {
                Session["OperatorGroupId"] = item.GetDataKeyValue("OperatorGroupId");
            }
        }
        catch { }
    }
    #endregion "RadGridOperatorGroup_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
    
        RadGridOperatorGroup.DataBind();
    }
    #endregion BindData
    
}
