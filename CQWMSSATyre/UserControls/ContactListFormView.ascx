<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactListFormView.ascx.cs" Inherits="UserControls_ContactListFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>ContactList</h3>
<asp:FormView ID="FormViewContactList" runat="server" DataKeyNames="ContactListId" DataSourceID="ObjectDataSourceContactList" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompanyId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyId"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyId" runat="server" TypeName="StaticInfoExternalCompany"
                        SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorId"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorId" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ContactPerson:
                </td>
                <td>
                    <asp:TextBox ID="ContactPersonTextBox" runat="server" Text='<%# Bind("ContactPerson") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Telephone:
                </td>
                <td>
                    <asp:TextBox ID="TelephoneTextBox" runat="server" Text='<%# Bind("Telephone") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Fax:
                </td>
                <td>
                    <asp:TextBox ID="FaxTextBox" runat="server" Text='<%# Bind("Fax") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    EMail:
                </td>
                <td>
                    <asp:TextBox ID="EMailTextBox" runat="server" Text='<%# Bind("EMail") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Alias:
                </td>
                <td>
                    <asp:TextBox ID="AliasTextBox" runat="server" Text='<%# Bind("Alias") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Type:
                </td>
                <td>
                    <asp:TextBox ID="TypeTextBox" runat="server" Text='<%# Bind("Type") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompanyId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyId"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyId" runat="server" TypeName="StaticInfoExternalCompany"
                        SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    OperatorId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorId"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorId" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ContactPerson:
                </td>
                <td>
                    <asp:TextBox ID="ContactPersonTextBox" runat="server" Text='<%# Bind("ContactPerson") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Telephone:
                </td>
                <td>
                    <asp:TextBox ID="TelephoneTextBox" runat="server" Text='<%# Bind("Telephone") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Fax:
                </td>
                <td>
                    <asp:TextBox ID="FaxTextBox" runat="server" Text='<%# Bind("Fax") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    EMail:
                </td>
                <td>
                    <asp:TextBox ID="EMailTextBox" runat="server" Text='<%# Bind("EMail") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Alias:
                </td>
                <td>
                    <asp:TextBox ID="AliasTextBox" runat="server" Text='<%# Bind("Alias") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Type:
                </td>
                <td>
                    <asp:TextBox ID="TypeTextBox" runat="server" Text='<%# Bind("Type") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    ExternalCompany:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListExternalCompanyId" runat="server" DataSourceID="ObjectDataSourceDDLExternalCompanyId"
                        DataTextField="ExternalCompany" DataValueField="ExternalCompanyId" SelectedValue='<%# Bind("ExternalCompanyId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLExternalCompanyId" runat="server" TypeName="StaticInfoExternalCompany"
                        SelectMethod="ParameterExternalCompany">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperatorId" runat="server" DataSourceID="ObjectDataSourceDDLOperatorId"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperatorId" runat="server" TypeName="StaticInfoOperator"
                        SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    ContactPerson:
                </td>
                <td>
                    <asp:Label ID="ContactPersonLabel" runat="server" Text='<%# Bind("ContactPerson") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Telephone:
                </td>
                <td>
                    <asp:Label ID="TelephoneLabel" runat="server" Text='<%# Bind("Telephone") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Fax:
                </td>
                <td>
                    <asp:Label ID="FaxLabel" runat="server" Text='<%# Bind("Fax") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    EMail:
                </td>
                <td>
                    <asp:Label ID="EMailLabel" runat="server" Text='<%# Bind("EMail") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Alias:
                </td>
                <td>
                    <asp:Label ID="AliasLabel" runat="server" Text='<%# Bind("Alias") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Type:
                </td>
                <td>
                    <asp:Label ID="TypeLabel" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ContactListGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceContactList" runat="server" TypeName="StaticInfoContactList"
    SelectMethod="GetContactList"
    DeleteMethod="DeleteContactList"
    UpdateMethod="UpdateContactList"
    InsertMethod="InsertContactList">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ContactListId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ContactListId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OperatorId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ContactPerson" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Telephone" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Fax" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="EMail" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Alias" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Type" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ContactListId" QueryStringField="ContactListId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ContactListId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ExternalCompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="OperatorId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ContactPerson" Type="String" />
        <asp:Parameter Name="Telephone" Type="String" />
        <asp:Parameter Name="Fax" Type="String" />
        <asp:Parameter Name="EMail" Type="String" />
        <asp:Parameter Name="Alias" Type="String" />
        <asp:Parameter Name="Type" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
