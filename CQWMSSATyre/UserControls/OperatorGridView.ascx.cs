using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_OperatorGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridOperator.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridOperator_SelectedIndexChanged
    protected void RadGridOperator_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridOperator.SelectedItems)
            {
                Session["OperatorId"] = item.GetDataKeyValue("OperatorId");
            }
        }
        catch { }
    }
    #endregion "RadGridOperator_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["OperatorGroupId"] != null)
          DropDownListOperatorGroupId.SelectedValue = Session["OperatorGroupId"].ToString();
 
        if (Session["CultureId"] != null)
          DropDownListCultureId.SelectedValue = Session["CultureId"].ToString();
        if (Session["PrincipalId"] != null)
          DropDownListPrincipalId.SelectedValue = Session["PrincipalId"].ToString();
        if (Session["ExternalCompanyId"] != null)
          DropDownListExternalCompanyId.SelectedValue = Session["ExternalCompanyId"].ToString();
        if (Session["MenuId"] != null)
          DropDownListMenuId.SelectedValue = Session["MenuId"].ToString();
    
        RadGridOperator.DataBind();
    }
    #endregion BindData
    
}
