<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocationGridView.ascx.cs" Inherits="UserControls_LocationGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowLocation()
{
   window.open("LocationFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelLocationTypeId" runat="server" Text="LocationType:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListLocationType" runat="server" DataSourceID="SqlDataSourceLocationTypeDDL"
                DataTextField="LocationType" DataValueField="LocationTypeId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceLocationTypeDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_LocationType_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelLocation" runat="server" Text="Location:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxLocation" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="<%$ Resources:Default, ButtonNew %>" OnClientClick="javascript:openNewWindowLocation();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, ButtonEdit %>" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, ButtonSave %>" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, ButtonDelete %>" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="<%$ Resources:Default, ButtonBack %>" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="<%$ Resources:Default, ButtonPrevious %>" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="<%$ Resources:Default, ButtonNext %>" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="<%$ Resources:Default, ButtonHelp %>"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewLocation" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
LocationId
"
                DataSourceID="SqlDataSourceLocation" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewLocation_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="LocationId" HeaderText="LocationId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="LocationId" />
            <asp:TemplateField HeaderText="LocationType" meta:resourcekey="LocationType" SortExpression="LocationTypeId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListLocationType" runat="server" DataSourceID="SqlDataSourceDDLLocationType"
                        DataTextField="LocationType" DataValueField="LocationTypeId" SelectedValue='<%# Bind("LocationTypeId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLLocationType" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_LocationType_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkLocationTypeId" runat="server" Text='<%# Bind("LocationType") %>' NavigateUrl="~/Administration/LocationTypeGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="LocationType" HeaderText="LocationType" ReadOnly="True" SortExpression="LocationType" Visible="False" />
                        <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                        <asp:BoundField DataField="Ailse" HeaderText="Ailse" SortExpression="Ailse" />
                        <asp:BoundField DataField="Column" HeaderText="Column" SortExpression="Column" />
                        <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
                        <asp:BoundField DataField="PalletQuantity" HeaderText="PalletQuantity" SortExpression="PalletQuantity" />
                        <asp:BoundField DataField="SecurityCode" HeaderText="SecurityCode" SortExpression="SecurityCode" />
                        <asp:BoundField DataField="RelativeValue" HeaderText="RelativeValue" SortExpression="RelativeValue" />
                        <asp:BoundField DataField="NumberOfSUB" HeaderText="NumberOfSUB" SortExpression="NumberOfSUB" />
                        <asp:TemplateField HeaderText="StocktakeInd" SortExpression="StocktakeInd">
                            <EditItemTemplate>
                                <asp:CheckBox ID="StocktakeIndCheckBox" runat="server" Checked='<%# Bind("StocktakeInd") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="StocktakeIndCheckBox" runat="server" Checked='<%# Bind("StocktakeInd") %>' Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ActiveBinning" SortExpression="ActiveBinning">
                            <EditItemTemplate>
                                <asp:CheckBox ID="ActiveBinningCheckBox" runat="server" Checked='<%# Bind("ActiveBinning") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="ActiveBinningCheckBox" runat="server" Checked='<%# Bind("ActiveBinning") %>' Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ActivePicking" SortExpression="ActivePicking">
                            <EditItemTemplate>
                                <asp:CheckBox ID="ActivePickingCheckBox" runat="server" Checked='<%# Bind("ActivePicking") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="ActivePickingCheckBox" runat="server" Checked='<%# Bind("ActivePicking") %>' Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Used" SortExpression="Used">
                            <EditItemTemplate>
                                <asp:CheckBox ID="UsedCheckBox" runat="server" Checked='<%# Bind("Used") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="UsedCheckBox" runat="server" Checked='<%# Bind("Used") %>' Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceLocation" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_Location_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_Location_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_Location_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_Location_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListLocationType" DefaultValue="" Name="LocationTypeId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="TextBoxLocation" DefaultValue="%" Name="Location" PropertyName="Text" Type="String" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="LocationId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="LocationTypeId" Type="Int32" />
        <asp:Parameter Name="Location" Type="String" />
        <asp:Parameter Name="Ailse" Type="String" />
        <asp:Parameter Name="Column" Type="String" />
        <asp:Parameter Name="Level" Type="String" />
        <asp:Parameter Name="PalletQuantity" Type="Int32" />
        <asp:Parameter Name="SecurityCode" Type="Int32" />
        <asp:Parameter Name="RelativeValue" Type="Decimal" />
        <asp:Parameter Name="NumberOfSUB" Type="Int32" />
        <asp:Parameter Name="StocktakeInd" Type="Boolean" />
        <asp:Parameter Name="ActiveBinning" Type="Boolean" />
        <asp:Parameter Name="ActivePicking" Type="Boolean" />
        <asp:Parameter Name="Used" Type="Boolean" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="LocationId" Type="Int32" />
        <asp:Parameter Name="LocationTypeId" Type="Int32" />
        <asp:Parameter Name="Location" Type="String" />
        <asp:Parameter Name="Ailse" Type="String" />
        <asp:Parameter Name="Column" Type="String" />
        <asp:Parameter Name="Level" Type="String" />
        <asp:Parameter Name="PalletQuantity" Type="Int32" />
        <asp:Parameter Name="SecurityCode" Type="Int32" />
        <asp:Parameter Name="RelativeValue" Type="Decimal" />
        <asp:Parameter Name="NumberOfSUB" Type="Int32" />
        <asp:Parameter Name="StocktakeInd" Type="Boolean" />
        <asp:Parameter Name="ActiveBinning" Type="Boolean" />
        <asp:Parameter Name="ActivePicking" Type="Boolean" />
        <asp:Parameter Name="Used" Type="Boolean" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
