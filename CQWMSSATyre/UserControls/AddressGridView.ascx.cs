using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControls_AddressGridView : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ExternalCompanyId"] != null && DropDownListExternalCompanyId.SelectedValue == "")
            DropDownListExternalCompanyId.SelectedValue = Request.QueryString["ExternalCompanyId"];

        if (DropDownListShow.SelectedValue == "")
            DropDownListShow.SelectedValue = (string)this.Profile.GetPropertyValue("ShowRecords");

        GridViewAddress.PageSize = int.Parse(DropDownListShow.SelectedValue);

        this.lbShowError.Text = "";
    }
    // Shows success or error message on delete
    protected void GridViewDeleted(Object s, GridViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            lbShowError.ForeColor = System.Drawing.Color.Red;
            lbShowError.Text = "There was an error deleting the row: " + e.Exception.ToString();
            Session["ErrorMessage"] = e.Exception.ToString();
            e.ExceptionHandled = true;
        }
        else
        {
            lbShowError.ForeColor = System.Drawing.Color.Green;
            lbShowError.Text = "The row was successfully deleted";
        }
    }
    // Shows success or error message on update
    protected void GridViewUpdated(Object s, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            lbShowError.ForeColor = System.Drawing.Color.Red;
            lbShowError.Text = "There was an error updating the row: " + e.Exception.ToString();
            Session["ErrorMessage"] = e.Exception.ToString();
            e.ExceptionHandled = true;
        }
        else
        {
            lbShowError.ForeColor = System.Drawing.Color.Green;
            lbShowError.Text = "The row was successfully updated";
        }
    }
    // Sets up session object for Previous and Next buttons
    protected void ButtonPrevious_Click(object sender, EventArgs e)
    {
        Session["PageNumber"] = Convert.ToInt32(Session["PageNumber"]) - 1;
    }
    // Sets up session object for Previous and Next buttons
    protected void ButtonNext_Click(object sender, EventArgs e)
    {
        Session["PageNumber"] = Convert.ToInt32(Session["PageNumber"]) + 1;
    }
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        ProfileCommon pc = this.Profile.GetProfile(Profile.UserName);

        if (pc != null)
        {
            pc.ShowRecords = DropDownListShow.SelectedValue.ToString();

            pc.Save();
        }
        GridViewAddress.PageSize = int.Parse(pc.ShowRecords);
    }
    // Sets the default value from the UsersProfile
    protected void DropDownListShow_OnInit(object sender, EventArgs e)
    {
        DropDownListShow.SelectedValue = (string)this.Profile.GetPropertyValue("ShowRecords");
    }
    // Sets the GridView into edit mode for selected row	
    protected void ButtonEdit_Click(object sender, EventArgs e)
    {
        GetEditIndex();
        SetEditIndex();
    }
    // Deletes selected row in GridView	
    protected void ButtonDelete_Click(object sender, EventArgs e)
    {
        int index = 0;
        CheckBox cb = new CheckBox();

        while (index < GridViewAddress.Rows.Count)
        {
            GridViewRow checkedRow = GridViewAddress.Rows[index];
            cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

            if (cb.Checked == true)
                GridViewAddress.DeleteRow(index);

            index++;
        }
    }
    protected void ButtonSave_Click(object sender, EventArgs e)
    {
        if (GridViewAddress.EditIndex != -1)
            GridViewAddress.UpdateRow(GridViewAddress.EditIndex, true);

        SetEditIndex();
    }
    protected void GetEditIndex()
    {
        if (Session["checkedList"] == null)
        {
            int index = 0;
            CheckBox cb = new CheckBox();
            ArrayList rowList = new ArrayList();

            while (index < GridViewAddress.Rows.Count)
            {
                GridViewRow checkedRow = GridViewAddress.Rows[index];
                cb = (CheckBox)checkedRow.FindControl("CheckBoxEdit");

                if (cb.Checked == true)
                    rowList.Add(index);

                index++;
            }
            if (rowList.Count > 0)
                Session["checkedList"] = rowList;
        }
    }

    protected void SetEditIndex()
    {
        ArrayList rowList = (ArrayList)Session["checkedList"];
        if (rowList == null)
            Session.Remove("checkedList");
        else
        {
            if (GridViewAddress.EditIndex != -1)
                GridViewAddress.UpdateRow(GridViewAddress.EditIndex, true);

            if (rowList.Count < 1)
            {
                Session.Remove("checkedList");
                GridViewAddress.EditIndex = -1;
            }
            else
            {
                GridViewAddress.EditIndex = (int)rowList[0];
                rowList.Remove(rowList[0]);
            }
        }
    }
    // Opens related window
    protected void GridViewAddress_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
}
