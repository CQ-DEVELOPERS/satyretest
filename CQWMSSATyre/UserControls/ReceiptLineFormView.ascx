<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReceiptLineFormView.ascx.cs" Inherits="UserControls_ReceiptLineFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>ReceiptLine</h3>
<asp:FormView ID="FormViewReceiptLine" runat="server" DataKeyNames="ReceiptLineId" DataSourceID="ObjectDataSourceReceiptLine" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Receipt:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceipt" runat="server" DataSourceID="ObjectDataSourceDDLReceipt"
                        DataTextField="Receipt" DataValueField="ReceiptId" SelectedValue='<%# Bind("ReceiptId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceipt" runat="server"
                        TypeName="StaticInfoReceipt" SelectMethod="ParameterReceipt">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListReceipt" ErrorMessage="* Please enter ReceiptId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    InboundLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundLine" runat="server" DataSourceID="ObjectDataSourceDDLInboundLine"
                        DataTextField="InboundLine" DataValueField="InboundLineId" SelectedValue='<%# Bind("InboundLineId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundLine" runat="server"
                        TypeName="StaticInfoInboundLine" SelectMethod="ParameterInboundLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListInboundLine" ErrorMessage="* Please enter InboundLineId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnitBatch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatch"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatch" runat="server"
                        TypeName="StaticInfoStorageUnitBatch" SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListStorageUnitBatch" ErrorMessage="* Please enter StorageUnitBatchId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    RequiredQuantity:
                </td>
                <td>
                    <asp:TextBox ID="RequiredQuantityTextBox" runat="server" Text='<%# Bind("RequiredQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="RequiredQuantityTextBox" ErrorMessage="* Please enter RequiredQuantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="RequiredQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReceivedQuantity:
                </td>
                <td>
                    <asp:TextBox ID="ReceivedQuantityTextBox" runat="server" Text='<%# Bind("ReceivedQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="ReceivedQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AcceptedQuantity:
                </td>
                <td>
                    <asp:TextBox ID="AcceptedQuantityTextBox" runat="server" Text='<%# Bind("AcceptedQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="AcceptedQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    RejectQuantity:
                </td>
                <td>
                    <asp:TextBox ID="RejectQuantityTextBox" runat="server" Text='<%# Bind("RejectQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="RejectQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryNoteQuantity:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryNoteQuantityTextBox" runat="server" Text='<%# Bind("DeliveryNoteQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="DeliveryNoteQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Receipt:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceipt" runat="server" DataSourceID="ObjectDataSourceDDLReceipt"
                        DataTextField="Receipt" DataValueField="ReceiptId" SelectedValue='<%# Bind("ReceiptId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceipt" runat="server"
                        TypeName="StaticInfoReceipt" SelectMethod="ParameterReceipt">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListReceipt" ErrorMessage="* Please enter ReceiptId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    InboundLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundLine" runat="server" DataSourceID="ObjectDataSourceDDLInboundLine"
                        DataTextField="InboundLine" DataValueField="InboundLineId" SelectedValue='<%# Bind("InboundLineId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundLine" runat="server"
                        TypeName="StaticInfoInboundLine" SelectMethod="ParameterInboundLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListInboundLine" ErrorMessage="* Please enter InboundLineId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnitBatch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatch"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatch" runat="server"
                        TypeName="StaticInfoStorageUnitBatch" SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListStorageUnitBatch" ErrorMessage="* Please enter StorageUnitBatchId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="DropDownListStatus" ErrorMessage="* Please enter StatusId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    RequiredQuantity:
                </td>
                <td>
                    <asp:TextBox ID="RequiredQuantityTextBox" runat="server" Text='<%# Bind("RequiredQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="RequiredQuantityTextBox" ErrorMessage="* Please enter RequiredQuantity"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="RequiredQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReceivedQuantity:
                </td>
                <td>
                    <asp:TextBox ID="ReceivedQuantityTextBox" runat="server" Text='<%# Bind("ReceivedQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="ReceivedQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AcceptedQuantity:
                </td>
                <td>
                    <asp:TextBox ID="AcceptedQuantityTextBox" runat="server" Text='<%# Bind("AcceptedQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="AcceptedQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    RejectQuantity:
                </td>
                <td>
                    <asp:TextBox ID="RejectQuantityTextBox" runat="server" Text='<%# Bind("RejectQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="RejectQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryNoteQuantity:
                </td>
                <td>
                    <asp:TextBox ID="DeliveryNoteQuantityTextBox" runat="server" Text='<%# Bind("DeliveryNoteQuantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="DeliveryNoteQuantityTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Receipt:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceipt" runat="server" DataSourceID="ObjectDataSourceDDLReceipt"
                        DataTextField="Receipt" DataValueField="ReceiptId" SelectedValue='<%# Bind("ReceiptId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceipt" runat="server"
                        TypeName="StaticInfoReceipt" SelectMethod="ParameterReceipt">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    InboundLine:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListInboundLine" runat="server" DataSourceID="ObjectDataSourceDDLInboundLine"
                        DataTextField="InboundLine" DataValueField="InboundLineId" SelectedValue='<%# Bind("InboundLineId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLInboundLine" runat="server"
                        TypeName="StaticInfoInboundLine" SelectMethod="ParameterInboundLine">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    StorageUnitBatch:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnitBatch" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnitBatch"
                        DataTextField="StorageUnitBatch" DataValueField="StorageUnitBatchId" SelectedValue='<%# Bind("StorageUnitBatchId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnitBatch" runat="server"
                        TypeName="StaticInfoStorageUnitBatch" SelectMethod="ParameterStorageUnitBatch">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Status:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="ObjectDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStatus" runat="server"
                        TypeName="StaticInfoStatus" SelectMethod="ParameterStatus">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Operator:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="ObjectDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLOperator" runat="server"
                        TypeName="StaticInfoOperator" SelectMethod="ParameterOperator">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    RequiredQuantity:
                </td>
                <td>
                    <asp:Label ID="RequiredQuantityLabel" runat="server" Text='<%# Bind("RequiredQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ReceivedQuantity:
                </td>
                <td>
                    <asp:Label ID="ReceivedQuantityLabel" runat="server" Text='<%# Bind("ReceivedQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AcceptedQuantity:
                </td>
                <td>
                    <asp:Label ID="AcceptedQuantityLabel" runat="server" Text='<%# Bind("AcceptedQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    RejectQuantity:
                </td>
                <td>
                    <asp:Label ID="RejectQuantityLabel" runat="server" Text='<%# Bind("RejectQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DeliveryNoteQuantity:
                </td>
                <td>
                    <asp:Label ID="DeliveryNoteQuantityLabel" runat="server" Text='<%# Bind("DeliveryNoteQuantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/ReceiptLineGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceReceiptLine" runat="server" TypeName="StaticInfoReceiptLine"
    SelectMethod="GetReceiptLine"
    DeleteMethod="DeleteReceiptLine"
    UpdateMethod="UpdateReceiptLine"
    InsertMethod="InsertReceiptLine">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
        <asp:Parameter Name="InboundLineId" Type="Int32" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="RequiredQuantity" Type="Int32" />
        <asp:Parameter Name="ReceivedQuantity" Type="Int32" />
        <asp:Parameter Name="AcceptedQuantity" Type="Int32" />
        <asp:Parameter Name="RejectQuantity" Type="Int32" />
        <asp:Parameter Name="DeliveryNoteQuantity" Type="Int32" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="ReceiptLineId" QueryStringField="ReceiptLineId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
        <asp:Parameter Name="InboundLineId" Type="Int32" />
        <asp:Parameter Name="StorageUnitBatchId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="RequiredQuantity" Type="Int32" />
        <asp:Parameter Name="ReceivedQuantity" Type="Int32" />
        <asp:Parameter Name="AcceptedQuantity" Type="Int32" />
        <asp:Parameter Name="RejectQuantity" Type="Int32" />
        <asp:Parameter Name="DeliveryNoteQuantity" Type="Int32" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
