<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReceiptGridView.ascx.cs" Inherits="UserControls_ReceiptGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowReceipt()
{
   window.open("ReceiptFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelInboundDocumentId" runat="server" Text="InboundDocument:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListInboundDocument" runat="server" DataSourceID="SqlDataSourceInboundDocumentDDL"
                DataTextField="InboundDocument" DataValueField="InboundDocumentId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceInboundDocumentDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_InboundDocument_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelPriorityId" runat="server" Text="Priority:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="SqlDataSourcePriorityDDL"
                DataTextField="Priority" DataValueField="PriorityId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourcePriorityDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Priority_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelLocationId" runat="server" Text="Location:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="SqlDataSourceLocationDDL"
                DataTextField="Location" DataValueField="LocationId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceLocationDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Location_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelStatusId" runat="server" Text="Status:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceStatusDDL"
                DataTextField="Status" DataValueField="StatusId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStatusDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Status_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelOperatorId" runat="server" Text="Operator:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="SqlDataSourceOperatorDDL"
                DataTextField="Operator" DataValueField="OperatorId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceOperatorDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Operator_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowReceipt();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="Search" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="Previous" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="Next" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="Help"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewReceipt" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
ReceiptId
"
                DataSourceID="SqlDataSourceReceipt" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewReceipt_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="ReceiptId" HeaderText="ReceiptId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="ReceiptId" />
            <asp:TemplateField HeaderText="InboundDocument" meta:resourcekey="InboundDocument" SortExpression="InboundDocumentId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListInboundDocument" runat="server" DataSourceID="SqlDataSourceDDLInboundDocument"
                        DataTextField="InboundDocument" DataValueField="InboundDocumentId" SelectedValue='<%# Bind("InboundDocumentId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLInboundDocument" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_InboundDocument_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkInboundDocumentId" runat="server" Text='<%# Bind("InboundDocument") %>' NavigateUrl="~/Administration/InboundDocumentGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="InboundDocument" HeaderText="InboundDocument" ReadOnly="True" SortExpression="InboundDocument" Visible="False" />
            <asp:TemplateField HeaderText="Priority" meta:resourcekey="Priority" SortExpression="PriorityId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="SqlDataSourceDDLPriority"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLPriority" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Priority_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkPriorityId" runat="server" Text='<%# Bind("Priority") %>' NavigateUrl="~/Administration/PriorityGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Priority" HeaderText="Priority" ReadOnly="True" SortExpression="Priority" Visible="False" />
                        <asp:BoundField DataField="Warehouse" HeaderText="Warehouse" ReadOnly="True" SortExpression="Warehouse" Visible="False" />
            <asp:TemplateField HeaderText="Location" meta:resourcekey="Location" SortExpression="LocationId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="SqlDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLLocation" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Location_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkLocationId" runat="server" Text='<%# Bind("Location") %>' NavigateUrl="~/Administration/LocationGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Location" HeaderText="Location" ReadOnly="True" SortExpression="Location" Visible="False" />
            <asp:TemplateField HeaderText="Status" meta:resourcekey="Status" SortExpression="StatusId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStatus" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Status_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStatusId" runat="server" Text='<%# Bind("Status") %>' NavigateUrl="~/Administration/StatusGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" Visible="False" />
            <asp:TemplateField HeaderText="Operator" meta:resourcekey="Operator" SortExpression="OperatorId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="SqlDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLOperator" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Operator_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkOperatorId" runat="server" Text='<%# Bind("Operator") %>' NavigateUrl="~/Administration/OperatorGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Operator" HeaderText="Operator" ReadOnly="True" SortExpression="Operator" Visible="False" />
                        <asp:BoundField DataField="DeliveryNoteNumber" HeaderText="DeliveryNoteNumber" SortExpression="DeliveryNoteNumber" />
                        <asp:TemplateField HeaderText="DeliveryDate" SortExpression="DeliveryDate">
                            <EditItemTemplate>
                                <asp:Button ID="ButtonDeliveryDate" runat="server" Text="..." OnClientClick="javascript:openNewWindowCalendar('DeliveryDate');" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelDeliveryDate" runat="server" Text='<%# Bind("DeliveryDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="SealNumber" HeaderText="SealNumber" SortExpression="SealNumber" />
                        <asp:BoundField DataField="VehicleRegistration" HeaderText="VehicleRegistration" SortExpression="VehicleRegistration" />
                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks" />
                        <asp:TemplateField HeaderText="CreditAdviceIndicator" SortExpression="CreditAdviceIndicator">
                            <EditItemTemplate>
                                <asp:CheckBox ID="CreditAdviceIndicatorCheckBox" runat="server" Checked='<%# Bind("CreditAdviceIndicator") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CreditAdviceIndicatorCheckBox" runat="server" Checked='<%# Bind("CreditAdviceIndicator") %>' Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Delivery" HeaderText="Delivery" SortExpression="Delivery" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceReceipt" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_Receipt_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_Receipt_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_Receipt_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_Receipt_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListInboundDocument" DefaultValue="" Name="InboundDocumentId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListPriority" DefaultValue="" Name="PriorityId" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListLocation" DefaultValue="" Name="LocationId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStatus" DefaultValue="" Name="StatusId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListOperator" DefaultValue="" Name="OperatorId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="ReceiptId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="ReceiptId" Type="Int32" />
        <asp:Parameter Name="InboundDocumentId" Type="Int32" />
        <asp:Parameter Name="PriorityId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="DeliveryNoteNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="SealNumber" Type="String" />
        <asp:Parameter Name="VehicleRegistration" Type="String" />
        <asp:Parameter Name="Remarks" Type="String" />
        <asp:Parameter Name="CreditAdviceIndicator" Type="Boolean" />
        <asp:Parameter Name="Delivery" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="ReceiptId" Type="Int32" />
        <asp:Parameter Name="InboundDocumentId" Type="Int32" />
        <asp:Parameter Name="PriorityId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="DeliveryNoteNumber" Type="String" />
        <asp:Parameter Name="DeliveryDate" Type="DateTime" />
        <asp:Parameter Name="SealNumber" Type="String" />
        <asp:Parameter Name="VehicleRegistration" Type="String" />
        <asp:Parameter Name="Remarks" Type="String" />
        <asp:Parameter Name="CreditAdviceIndicator" Type="Boolean" />
        <asp:Parameter Name="Delivery" Type="Int32" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
