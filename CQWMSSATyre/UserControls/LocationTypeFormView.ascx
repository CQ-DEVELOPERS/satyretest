<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocationTypeFormView.ascx.cs" Inherits="UserControls_LocationTypeFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>LocationType</h3>
<asp:FormView ID="FormViewLocationType" runat="server" DataKeyNames="LocationTypeId" DataSourceID="ObjectDataSourceLocationType" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    LocationType:
                </td>
                <td>
                    <asp:TextBox ID="LocationTypeTextBox" runat="server" Text='<%# Bind("LocationType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="LocationTypeTextBox"
                        ErrorMessage="* Please enter LocationType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    LocationTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="LocationTypeCodeTextBox" runat="server" Text='<%# Bind("LocationTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="LocationTypeCodeTextBox"
                        ErrorMessage="* Please enter LocationTypeCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DeleteIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="DeleteIndicatorCheckBox" runat="server" Checked='<%# Bind("DeleteIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter DeleteIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReplenishmentIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="ReplenishmentIndicatorCheckBox" runat="server" Checked='<%# Bind("ReplenishmentIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter ReplenishmentIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MultipleProductsIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="MultipleProductsIndicatorCheckBox" runat="server" Checked='<%# Bind("MultipleProductsIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter MultipleProductsIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    BatchTrackingIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="BatchTrackingIndicatorCheckBox" runat="server" Checked='<%# Bind("BatchTrackingIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter BatchTrackingIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    LocationType:
                </td>
                <td>
                    <asp:TextBox ID="LocationTypeTextBox" runat="server" Text='<%# Bind("LocationType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="LocationTypeTextBox"
                        ErrorMessage="* Please enter LocationType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    LocationTypeCode:
                </td>
                <td>
                    <asp:TextBox ID="LocationTypeCodeTextBox" runat="server" Text='<%# Bind("LocationTypeCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="LocationTypeCodeTextBox"
                        ErrorMessage="* Please enter LocationTypeCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DeleteIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="DeleteIndicatorCheckBox" runat="server" Checked='<%# Bind("DeleteIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter DeleteIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReplenishmentIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="ReplenishmentIndicatorCheckBox" runat="server" Checked='<%# Bind("ReplenishmentIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter ReplenishmentIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    MultipleProductsIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="MultipleProductsIndicatorCheckBox" runat="server" Checked='<%# Bind("MultipleProductsIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter MultipleProductsIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    BatchTrackingIndicator:
                </td>
                <td>
                    <asp:CheckBox ID="BatchTrackingIndicatorCheckBox" runat="server" Checked='<%# Bind("BatchTrackingIndicator") %>'></asp:CheckBox>
                </td>
                <td>
                        ErrorMessage="* Please enter BatchTrackingIndicator"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    LocationType:
                </td>
                <td>
                    <asp:Label ID="LocationTypeLabel" runat="server" Text='<%# Bind("LocationType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    LocationTypeCode:
                </td>
                <td>
                    <asp:Label ID="LocationTypeCodeLabel" runat="server" Text='<%# Bind("LocationTypeCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DeleteIndicator:
                </td>
                <td>
                    <asp:Label ID="DeleteIndicatorLabel" runat="server" Text='<%# Bind("DeleteIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ReplenishmentIndicator:
                </td>
                <td>
                    <asp:Label ID="ReplenishmentIndicatorLabel" runat="server" Text='<%# Bind("ReplenishmentIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    MultipleProductsIndicator:
                </td>
                <td>
                    <asp:Label ID="MultipleProductsIndicatorLabel" runat="server" Text='<%# Bind("MultipleProductsIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    BatchTrackingIndicator:
                </td>
                <td>
                    <asp:Label ID="BatchTrackingIndicatorLabel" runat="server" Text='<%# Bind("BatchTrackingIndicator") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/LocationTypeGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceLocationType" runat="server" TypeName="StaticInfoLocationType"
    SelectMethod="GetLocationType"
    DeleteMethod="DeleteLocationType"
    UpdateMethod="UpdateLocationType"
    InsertMethod="InsertLocationType">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="LocationTypeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="LocationTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="LocationType" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="LocationTypeCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="DeleteIndicator" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="ReplenishmentIndicator" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="MultipleProductsIndicator" Type="Boolean" DefaultValue="-1" />
        <asp:Parameter Name="BatchTrackingIndicator" Type="Boolean" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="LocationTypeId" QueryStringField="LocationTypeId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="LocationTypeId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="LocationType" Type="String" />
        <asp:Parameter Name="LocationTypeCode" Type="String" />
        <asp:Parameter Name="DeleteIndicator" Type="Boolean" />
        <asp:Parameter Name="ReplenishmentIndicator" Type="Boolean" />
        <asp:Parameter Name="MultipleProductsIndicator" Type="Boolean" />
        <asp:Parameter Name="BatchTrackingIndicator" Type="Boolean" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
