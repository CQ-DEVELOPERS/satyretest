<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SKUFormView.ascx.cs" Inherits="UserControls_SKUFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>SKU</h3>
<asp:FormView ID="FormViewSKU" runat="server" DataKeyNames="SKUId" DataSourceID="ObjectDataSourceSKU" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    UOMId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListUOMId" runat="server" DataSourceID="ObjectDataSourceDDLUOMId"
                        DataTextField="UOM" DataValueField="UOMId" SelectedValue='<%# Bind("UOMId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLUOMId" runat="server" TypeName="StaticInfoUOM"
                        SelectMethod="ParameterUOM">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListUOMId"
                        ErrorMessage="* Please enter UOMId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    SKU:
                </td>
                <td>
                    <asp:TextBox ID="SKUTextBox" runat="server" Text='<%# Bind("SKU") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="SKUTextBox"
                        ErrorMessage="* Please enter SKU"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    SKUCode:
                </td>
                <td>
                    <asp:TextBox ID="SKUCodeTextBox" runat="server" Text='<%# Bind("SKUCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Quantity:
                </td>
                <td>
                    <asp:TextBox ID="QuantityTextBox" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="QuantityTextBox"
                        ErrorMessage="* Please enter Quantity"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AlternatePallet:
                </td>
                <td>
                    <asp:TextBox ID="AlternatePalletTextBox" runat="server" Text='<%# Bind("AlternatePallet") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="AlternatePalletTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    SubstitutePallet:
                </td>
                <td>
                    <asp:TextBox ID="SubstitutePalletTextBox" runat="server" Text='<%# Bind("SubstitutePallet") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="SubstitutePalletTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Litres:
                </td>
                <td>
                    <asp:TextBox ID="LitresTextBox" runat="server" Text='<%# Bind("Litres") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ParentSKUCode:
                </td>
                <td>
                    <asp:TextBox ID="ParentSKUCodeTextBox" runat="server" Text='<%# Bind("ParentSKUCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    UOMId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListUOMId" runat="server" DataSourceID="ObjectDataSourceDDLUOMId"
                        DataTextField="UOM" DataValueField="UOMId" SelectedValue='<%# Bind("UOMId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLUOMId" runat="server" TypeName="StaticInfoUOM"
                        SelectMethod="ParameterUOM">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListUOMId"
                        ErrorMessage="* Please enter UOMId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    SKU:
                </td>
                <td>
                    <asp:TextBox ID="SKUTextBox" runat="server" Text='<%# Bind("SKU") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="SKUTextBox"
                        ErrorMessage="* Please enter SKU"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    SKUCode:
                </td>
                <td>
                    <asp:TextBox ID="SKUCodeTextBox" runat="server" Text='<%# Bind("SKUCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Quantity:
                </td>
                <td>
                    <asp:TextBox ID="QuantityTextBox" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="QuantityTextBox"
                        ErrorMessage="* Please enter Quantity"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    AlternatePallet:
                </td>
                <td>
                    <asp:TextBox ID="AlternatePalletTextBox" runat="server" Text='<%# Bind("AlternatePallet") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="AlternatePalletTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    SubstitutePallet:
                </td>
                <td>
                    <asp:TextBox ID="SubstitutePalletTextBox" runat="server" Text='<%# Bind("SubstitutePallet") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="SubstitutePalletTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Litres:
                </td>
                <td>
                    <asp:TextBox ID="LitresTextBox" runat="server" Text='<%# Bind("Litres") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    ParentSKUCode:
                </td>
                <td>
                    <asp:TextBox ID="ParentSKUCodeTextBox" runat="server" Text='<%# Bind("ParentSKUCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    UOM:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListUOMId" runat="server" DataSourceID="ObjectDataSourceDDLUOMId"
                        DataTextField="UOM" DataValueField="UOMId" SelectedValue='<%# Bind("UOMId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLUOMId" runat="server" TypeName="StaticInfoUOM"
                        SelectMethod="ParameterUOM">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    SKU:
                </td>
                <td>
                    <asp:Label ID="SKULabel" runat="server" Text='<%# Bind("SKU") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    SKUCode:
                </td>
                <td>
                    <asp:Label ID="SKUCodeLabel" runat="server" Text='<%# Bind("SKUCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Quantity:
                </td>
                <td>
                    <asp:Label ID="QuantityLabel" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    AlternatePallet:
                </td>
                <td>
                    <asp:Label ID="AlternatePalletLabel" runat="server" Text='<%# Bind("AlternatePallet") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    SubstitutePallet:
                </td>
                <td>
                    <asp:Label ID="SubstitutePalletLabel" runat="server" Text='<%# Bind("SubstitutePallet") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Litres:
                </td>
                <td>
                    <asp:Label ID="LitresLabel" runat="server" Text='<%# Bind("Litres") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ParentSKUCode:
                </td>
                <td>
                    <asp:Label ID="ParentSKUCodeLabel" runat="server" Text='<%# Bind("ParentSKUCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/SKUGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceSKU" runat="server" TypeName="StaticInfoSKU"
    SelectMethod="GetSKU"
    DeleteMethod="DeleteSKU"
    UpdateMethod="UpdateSKU"
    InsertMethod="InsertSKU">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="SKUId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="SKUId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="UOMId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="SKU" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="SKUCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Quantity" Type="Double" DefaultValue="-1" />
        <asp:Parameter Name="AlternatePallet" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="SubstitutePallet" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Litres" Type="Decimal" DefaultValue="-1" />
        <asp:Parameter Name="ParentSKUCode" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="SKUId" QueryStringField="SKUId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="SKUId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="UOMId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="SKU" Type="String" />
        <asp:Parameter Name="SKUCode" Type="String" />
        <asp:Parameter Name="Quantity" Type="Double" />
        <asp:Parameter Name="AlternatePallet" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="SubstitutePallet" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Litres" Type="Decimal" />
        <asp:Parameter Name="ParentSKUCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
