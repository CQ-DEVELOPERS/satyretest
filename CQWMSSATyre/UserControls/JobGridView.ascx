<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JobGridView.ascx.cs" Inherits="UserControls_JobGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowJob()
{
   window.open("JobFormView.aspx", "_blank",
      "height=440px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelPriorityId" runat="server" Text="Priority:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="SqlDataSourcePriorityDDL"
                DataTextField="Priority" DataValueField="PriorityId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourcePriorityDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Priority_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelOperatorId" runat="server" Text="Operator:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="SqlDataSourceOperatorDDL"
                DataTextField="Operator" DataValueField="OperatorId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceOperatorDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Operator_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelStatusId" runat="server" Text="Status:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceStatusDDL"
                DataTextField="Status" DataValueField="StatusId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceStatusDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_Status_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelReceiptLineId" runat="server" Text="ReceiptLine:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="SqlDataSourceReceiptLineDDL"
                DataTextField="ReceiptLine" DataValueField="ReceiptLineId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceReceiptLineDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_ReceiptLine_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelIssueLineId" runat="server" Text="IssueLine:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="SqlDataSourceIssueLineDDL"
                DataTextField="IssueLine" DataValueField="IssueLineId">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceIssueLineDDL" runat="server"
                ConnectionString="<%$ ConnectionStrings:Connection String %>" SelectCommand="p_IssueLine_List"
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/Images/Buttons/search.png" OnClick="ButtonSearch_Click"/>
        </td>
    </tr>
</table>
</asp:View>
    <asp:View ID="View2" runat="server">
<table>
    <tr>
        <td><asp:Button ID="ButtonNew" runat="server" Text="<%$ Resources:Default, ButtonNew %>" OnClientClick="javascript:openNewWindowJob();"/></td>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="<%$ Resources:Default, ButtonEdit %>" OnClick="ButtonEdit_Click"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="<%$ Resources:Default, ButtonSave %>" OnClick="ButtonSave_Click"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="<%$ Resources:Default, ButtonDelete %>" OnClick="ButtonDelete_Click"/></td>
        <td><asp:Button ID="ButtonBackToSearch" runat="server" Text="<%$ Resources:Default, ButtonBack %>" OnClick="ButtonBackToSearch_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonPrevious" runat="server" Text="<%$ Resources:Default, ButtonPrevious %>" OnClick="ButtonPrevious_Click"/></td>
        <td><asp:Button ID="ButtonNext" runat="server" Text="<%$ Resources:Default, ButtonNext %>" OnClick="ButtonNext_Click"/></td>
        <td></td>
        <td></td>
        <td><asp:Button ID="ButtonHelp" runat="server" Text="<%$ Resources:Default, ButtonHelp %>"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewJob" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
JobId
"
                DataSourceID="SqlDataSourceJob" PageSize="50"  OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewJob_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
        <asp:BoundField DataField="JobId" HeaderText="JobId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="JobId" />
            <asp:TemplateField HeaderText="Priority" meta:resourcekey="Priority" SortExpression="PriorityId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListPriority" runat="server" DataSourceID="SqlDataSourceDDLPriority"
                        DataTextField="Priority" DataValueField="PriorityId" SelectedValue='<%# Bind("PriorityId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLPriority" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Priority_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkPriorityId" runat="server" Text='<%# Bind("Priority") %>' NavigateUrl="~/Administration/PriorityGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Priority" HeaderText="Priority" ReadOnly="True" SortExpression="Priority" Visible="False" />
            <asp:TemplateField HeaderText="Operator" meta:resourcekey="Operator" SortExpression="OperatorId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListOperator" runat="server" DataSourceID="SqlDataSourceDDLOperator"
                        DataTextField="Operator" DataValueField="OperatorId" SelectedValue='<%# Bind("OperatorId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLOperator" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Operator_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkOperatorId" runat="server" Text='<%# Bind("Operator") %>' NavigateUrl="~/Administration/OperatorGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Operator" HeaderText="Operator" ReadOnly="True" SortExpression="Operator" Visible="False" />
            <asp:TemplateField HeaderText="Status" meta:resourcekey="Status" SortExpression="StatusId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListStatus" runat="server" DataSourceID="SqlDataSourceDDLStatus"
                        DataTextField="Status" DataValueField="StatusId" SelectedValue='<%# Bind("StatusId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLStatus" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_Status_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkStatusId" runat="server" Text='<%# Bind("Status") %>' NavigateUrl="~/Administration/StatusGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" SortExpression="Status" Visible="False" />
                        <asp:BoundField DataField="Warehouse" HeaderText="Warehouse" ReadOnly="True" SortExpression="Warehouse" Visible="False" />
            <asp:TemplateField HeaderText="ReceiptLine" meta:resourcekey="ReceiptLine" SortExpression="ReceiptLineId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListReceiptLine" runat="server" DataSourceID="SqlDataSourceDDLReceiptLine"
                        DataTextField="ReceiptLine" DataValueField="ReceiptLineId" SelectedValue='<%# Bind("ReceiptLineId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLReceiptLine" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_ReceiptLine_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkReceiptLineId" runat="server" Text='<%# Bind("ReceiptLine") %>' NavigateUrl="~/Administration/ReceiptLineGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ReceiptLine" HeaderText="ReceiptLine" ReadOnly="True" SortExpression="ReceiptLine" Visible="False" />
            <asp:TemplateField HeaderText="IssueLine" meta:resourcekey="IssueLine" SortExpression="IssueLineId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListIssueLine" runat="server" DataSourceID="SqlDataSourceDDLIssueLine"
                        DataTextField="IssueLine" DataValueField="IssueLineId" SelectedValue='<%# Bind("IssueLineId") %>' >
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceDDLIssueLine" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
                        SelectCommand="p_IssueLine_Parameter" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkIssueLineId" runat="server" Text='<%# Bind("IssueLine") %>' NavigateUrl="~/Administration/IssueLineGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="IssueLine" HeaderText="IssueLine" ReadOnly="True" SortExpression="IssueLine" Visible="False" />
                        <asp:BoundField DataField="ReferenceNumber" HeaderText="ReferenceNumber" SortExpression="ReferenceNumber" />
                        <asp:BoundField DataField="TareWeight" HeaderText="TareWeight" SortExpression="TareWeight" />
                        <asp:BoundField DataField="Weight" HeaderText="Weight" SortExpression="Weight" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonPrevious" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNext" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonHelp" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:SqlDataSource ID="SqlDataSourceJob" runat="server" ConnectionString="<%$ ConnectionStrings:Connection String %>"
    DeleteCommand="p_Job_Delete" DeleteCommandType="StoredProcedure" InsertCommand="p_Job_Insert"
    InsertCommandType="StoredProcedure" SelectCommand="p_Job_Search" SelectCommandType="StoredProcedure"
    UpdateCommand="p_Job_Update" UpdateCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="DropDownListShow" DefaultValue="" Name="PageSize" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="PageNumber" SessionField="PageNumber" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListPriority" DefaultValue="" Name="PriorityId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListOperator" DefaultValue="" Name="OperatorId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListStatus" DefaultValue="" Name="StatusId" PropertyName="SelectedValue" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListReceiptLine" DefaultValue="" Name="ReceiptLineId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListIssueLine" DefaultValue="" Name="IssueLineId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="JobId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="JobId" Type="Int32" />
        <asp:Parameter Name="PriorityId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="ReferenceNumber" Type="String" />
        <asp:Parameter Name="TareWeight" Type="Decimal" />
        <asp:Parameter Name="Weight" Type="Decimal" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Direction="InputOutput" Name="JobId" Type="Int32" />
        <asp:Parameter Name="PriorityId" Type="Int32" />
        <asp:Parameter Name="OperatorId" Type="Int32" />
        <asp:Parameter Name="StatusId" Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        <asp:Parameter Name="ReceiptLineId" Type="Int32" />
        <asp:Parameter Name="IssueLineId" Type="Int32" />
        <asp:Parameter Name="ReferenceNumber" Type="String" />
        <asp:Parameter Name="TareWeight" Type="Decimal" />
        <asp:Parameter Name="Weight" Type="Decimal" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
    </asp:View>
</asp:MultiView>
