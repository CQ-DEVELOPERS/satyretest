using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
 
public partial class UserControls_InterfaceMappingFileGridView : System.Web.UI.UserControl
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    #endregion Page_Load
    
    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        RadGridInterfaceMappingFile.DataBind();
    }
    #endregion ButtonSearch_Click
    
    #region RadGridInterfaceMappingFile_SelectedIndexChanged
    protected void RadGridInterfaceMappingFile_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in RadGridInterfaceMappingFile.SelectedItems)
            {
                Session["InterfaceMappingFileId"] = item.GetDataKeyValue("InterfaceMappingFileId");
            }
        }
        catch { }
    }
    #endregion "RadGridInterfaceMappingFile_SelectedIndexChanged"
    
    #region BindData
    public void BindData()
    {
        if (Session["PrincipalId"] != null)
          DropDownListPrincipalId.SelectedValue = Session["PrincipalId"].ToString();
        if (Session["InterfaceFileTypeId"] != null)
          DropDownListInterfaceFileTypeId.SelectedValue = Session["InterfaceFileTypeId"].ToString();
        if (Session["InterfaceDocumentTypeId"] != null)
          DropDownListInterfaceDocumentTypeId.SelectedValue = Session["InterfaceDocumentTypeId"].ToString();
    
        RadGridInterfaceMappingFile.DataBind();
    }
    #endregion BindData
    
}
