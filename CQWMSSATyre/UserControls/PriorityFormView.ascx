<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PriorityFormView.ascx.cs" Inherits="UserControls_PriorityFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>Priority</h3>
<asp:FormView ID="FormViewPriority" runat="server" DataKeyNames="PriorityId" DataSourceID="ObjectDataSourcePriority" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:TextBox ID="PriorityTextBox" runat="server" Text='<%# Bind("Priority") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PriorityTextBox"
                        ErrorMessage="* Please enter Priority"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PriorityCode:
                </td>
                <td>
                    <asp:TextBox ID="PriorityCodeTextBox" runat="server" Text='<%# Bind("PriorityCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="PriorityCodeTextBox"
                        ErrorMessage="* Please enter PriorityCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:TextBox ID="OrderByTextBox" runat="server" Text='<%# Bind("OrderBy") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="OrderByTextBox"
                        ErrorMessage="* Please enter OrderBy"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="OrderByTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:TextBox ID="PriorityTextBox" runat="server" Text='<%# Bind("Priority") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PriorityTextBox"
                        ErrorMessage="* Please enter Priority"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PriorityCode:
                </td>
                <td>
                    <asp:TextBox ID="PriorityCodeTextBox" runat="server" Text='<%# Bind("PriorityCode") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="PriorityCodeTextBox"
                        ErrorMessage="* Please enter PriorityCode"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:TextBox ID="OrderByTextBox" runat="server" Text='<%# Bind("OrderBy") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="OrderByTextBox"
                        ErrorMessage="* Please enter OrderBy"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="OrderByTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:Label ID="PriorityLabel" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    PriorityCode:
                </td>
                <td>
                    <asp:Label ID="PriorityCodeLabel" runat="server" Text='<%# Bind("PriorityCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    OrderBy:
                </td>
                <td>
                    <asp:Label ID="OrderByLabel" runat="server" Text='<%# Bind("OrderBy") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/PriorityGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourcePriority" runat="server" TypeName="StaticInfoPriority"
    SelectMethod="GetPriority"
    DeleteMethod="DeletePriority"
    UpdateMethod="UpdatePriority"
    InsertMethod="InsertPriority">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="PriorityId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Priority" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="PriorityCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="OrderBy" Type="Int32" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="PriorityId" QueryStringField="PriorityId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="PriorityId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Priority" Type="String" />
        <asp:Parameter Name="PriorityCode" Type="String" />
        <asp:Parameter Name="OrderBy" Type="Int32" DefaultValue="-1" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
