<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResultGridView.ascx.cs" Inherits="UserControls_ResultGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowResult()
{
   window.open("ResultFormView.aspx", "_blank",
      "height=640px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelResultCode" runat="server" Text="ResultCode:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxResultCode" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server" Text="Result:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxResult" runat="server"></asp:TextBox>
        </td>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"/>
        <td>
            <asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowResult();"/>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Visible="False"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewResult" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
ResultId
"
                DataSourceID="ObjectDataSourceResult" OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewResult_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit" Visible="False">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="False" ShowEditButton="True" />
        <asp:BoundField DataField="ResultId" HeaderText="ResultId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="ResultId" />
                        <asp:BoundField DataField="ResultCode" HeaderText="ResultCode" SortExpression="ResultCode" />
                        <asp:BoundField DataField="Result" HeaderText="Result" SortExpression="Result" />
                        <asp:TemplateField HeaderText="Pass" SortExpression="Pass">
                            <EditItemTemplate>
                                <asp:CheckBox ID="PassCheckBox" runat="server" Checked='<%# Bind("Pass") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="PassCheckBox" runat="server" Checked='<%# Bind("Pass") %>' Enabled="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StartRange" HeaderText="StartRange" SortExpression="StartRange" />
                        <asp:BoundField DataField="EndRange" HeaderText="EndRange" SortExpression="EndRange" />
                    <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:ObjectDataSource ID="ObjectDataSourceResult" runat="server" TypeName="StaticInfoResult"
    DeleteMethod="DeleteResult"
    InsertMethod="InsertResult"
    SelectMethod="SearchResult"
    UpdateMethod="UpdateResult">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ResultId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="TextBoxResultCode" DefaultValue="%" Name="ResultCode" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="TextBoxResult" DefaultValue="%" Name="Result" PropertyName="Text" Type="String" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ResultId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="ResultId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="ResultCode" Type="String" />
        <asp:Parameter Name="Result" Type="String" />
        <asp:Parameter Name="Pass" Type="Boolean" />
        <asp:Parameter Name="StartRange" Type="Double" />
        <asp:Parameter Name="EndRange" Type="Double" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="ResultId" Type="Int32" />
        <asp:Parameter Name="ResultCode" Type="String" />
        <asp:Parameter Name="Result" Type="String" />
        <asp:Parameter Name="Pass" Type="Boolean" />
        <asp:Parameter Name="StartRange" Type="Double" />
        <asp:Parameter Name="EndRange" Type="Double" />
    </InsertParameters>
</asp:ObjectDataSource>
