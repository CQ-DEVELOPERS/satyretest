<%@ Control Language="C#" AutoEventWireup="true" CodeFile="COADetailGridView.ascx.cs" Inherits="UserControls_COADetailGridView" %>
 
<script type="text/javascript" language="JavaScript">
 
function openNewWindowCOADetail()
{
   window.open("COADetailFormView.aspx", "_blank",
      "height=640px width=540px top=100 left=100 resizable=no scrollbars=no ");
}
 
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
 
function openNewWindowCalendar(string)
{
    window.open("../Common/Calendar.aspx?Date=" + string, "_blank",
        "height=250px width=280px top=200 left=200 resizable=no scrollbars=no ");
}
</script>
 
<table style="background-color: #F8F7F4; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <asp:Label ID="LabelCOAId" runat="server" Text="COA:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCOAId" runat="server" DataSourceID="ObjectDataSourceDDLCOAId"
                DataTextField="COA" DataValueField="COAId" >
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLCOAId" runat="server" TypeName="StaticInfoCOA"
                SelectMethod="ParameterCOA">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
        <td>
            <asp:Label ID="LabelNoteId" runat="server" Text="Note:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListNoteId" runat="server" DataSourceID="ObjectDataSourceDDLNoteId"
                DataTextField="Note" DataValueField="NoteId" >
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceDDLNoteId" runat="server" TypeName="StaticInfoNote"
                SelectMethod="ParameterNote">
                <SelectParameters>
                    <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    <tr>
        <td>
            <asp:Label ID="LabelShow" runat="server" Text="Show:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListShow" runat="server" OnInit="DropDownListShow_OnInit">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"/>
        <td>
            <asp:Button ID="ButtonNew" runat="server" Text="New" OnClientClick="javascript:openNewWindowCOADetail();"/>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" Visible="False"/></td>
        <td><asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Visible="False"/></td>
    </tr>
</table>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridViewCOADetail" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="
COADetailId
"
                DataSourceID="ObjectDataSourceCOADetail" OnRowUpdated="GridViewUpdated" OnRowDeleted="GridViewDeleted"
                OnSelectedIndexChanged="GridViewCOADetail_SelectedIndexChanged" EnableViewState="false">
                <Columns>
                <asp:TemplateField HeaderText="Edit" Visible="False">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxEdit" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="False" ShowEditButton="True" />
        <asp:BoundField DataField="COADetailId" HeaderText="COADetailId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="COADetailId" />
            <asp:TemplateField HeaderText="COAId" meta:resourcekey="COAId" SortExpression="COAId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListCOAId" runat="server" DataSourceID="ObjectDataSourceDDLCOAId"
                        DataTextField="COA" DataValueField="COAId" SelectedValue='<%# Bind("COAId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCOAId" runat="server" TypeName="StaticInfoCOA"
                        SelectMethod="ParameterCOA">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkCOAId" runat="server" Text='<%# Bind("COA") %>' NavigateUrl="~/Administration/COAGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
            <asp:TemplateField HeaderText="NoteId" meta:resourcekey="NoteId" SortExpression="NoteId">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownListNoteId" runat="server" DataSourceID="ObjectDataSourceDDLNoteId"
                        DataTextField="Note" DataValueField="NoteId" SelectedValue='<%# Bind("NoteId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLNoteId" runat="server" TypeName="StaticInfoNote"
                        SelectMethod="ParameterNote">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLinkNoteId" runat="server" Text='<%# Bind("Note") %>' NavigateUrl="~/Administration/NoteGridView.aspx"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NoteCode" HeaderText="NoteCode" SortExpression="NoteCode" />
        <asp:TemplateField HeaderText="Note" SortExpression="Note">
            <EditItemTemplate>
                <asp:TextBox ID="TextBoxNote" runat="server" Text='<%# Bind("Note") %>' TextMode="MultiLine" Rows="10" Width="300px" AutoCompleteType="Notes"></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="LabelNote" runat="server" Text='<%# Bind("Note") %>' ></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        No rows to display
                    </EmptyDataTemplate>
                </asp:GridView>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonNew" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEdit" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonDelete" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
 
<asp:ObjectDataSource ID="ObjectDataSourceCOADetail" runat="server" TypeName="StaticInfoCOADetail"
    DeleteMethod="DeleteCOADetail"
    InsertMethod="InsertCOADetail"
    SelectMethod="SearchCOADetail"
    UpdateMethod="UpdateCOADetail">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COADetailId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="DropDownListCOAId" DefaultValue="-1" Name="COAId" PropertyName="SelectedValue" Type="Int32" />
        <asp:ControlParameter ControlID="DropDownListNoteId" DefaultValue="-1" Name="NoteId" PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COADetailId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COADetailId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteCode" Type="String" />
        <asp:Parameter Name="Note" Type="String" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="COADetailId" Type="Int32" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteCode" Type="String" />
        <asp:Parameter Name="Note" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
