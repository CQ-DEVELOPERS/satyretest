<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SerialNumberFormView.ascx.cs" Inherits="UserControls_SerialNumberFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>SerialNumber</h3>
<asp:FormView ID="FormViewSerialNumber" runat="server" DataKeyNames="SerialNumberId" DataSourceID="ObjectDataSourceSerialNumber" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListStorageUnit" ErrorMessage="* Please enter StorageUnitId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Receipt:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceipt" runat="server" DataSourceID="ObjectDataSourceDDLReceipt"
                        DataTextField="Receipt" DataValueField="ReceiptId" SelectedValue='<%# Bind("ReceiptId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceipt" runat="server"
                        TypeName="StaticInfoReceipt" SelectMethod="ParameterReceipt">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    StoreInstructionId:
                </td>
                <td>
                    <asp:TextBox ID="StoreInstructionIdTextBox" runat="server" Text='<%# Bind("StoreInstructionId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="StoreInstructionIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PickInstructionId:
                </td>
                <td>
                    <asp:TextBox ID="PickInstructionIdTextBox" runat="server" Text='<%# Bind("PickInstructionId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="PickInstructionIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    SerialNumber:
                </td>
                <td>
                    <asp:TextBox ID="SerialNumberTextBox" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="SerialNumberTextBox" ErrorMessage="* Please enter SerialNumber"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReceivedDate:
                </td>
                <td>
                    <asp:TextBox ID="ReceivedDateTextBox" runat="server" Text='<%# Bind("ReceivedDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ReceivedDateTextBox" ErrorMessage="* Please enter ReceivedDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DespatchDate:
                </td>
                <td>
                    <asp:TextBox ID="DespatchDateTextBox" runat="server" Text='<%# Bind("DespatchDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListStorageUnit" ErrorMessage="* Please enter StorageUnitId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Receipt:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceipt" runat="server" DataSourceID="ObjectDataSourceDDLReceipt"
                        DataTextField="Receipt" DataValueField="ReceiptId" SelectedValue='<%# Bind("ReceiptId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceipt" runat="server"
                        TypeName="StaticInfoReceipt" SelectMethod="ParameterReceipt">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    StoreInstructionId:
                </td>
                <td>
                    <asp:TextBox ID="StoreInstructionIdTextBox" runat="server" Text='<%# Bind("StoreInstructionId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="StoreInstructionIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PickInstructionId:
                </td>
                <td>
                    <asp:TextBox ID="PickInstructionIdTextBox" runat="server" Text='<%# Bind("PickInstructionId") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="PickInstructionIdTextBox"  ErrorMessage="Numbers only" MaximumValue="1000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    SerialNumber:
                </td>
                <td>
                    <asp:TextBox ID="SerialNumberTextBox" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="SerialNumberTextBox" ErrorMessage="* Please enter SerialNumber"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ReceivedDate:
                </td>
                <td>
                    <asp:TextBox ID="ReceivedDateTextBox" runat="server" Text='<%# Bind("ReceivedDate") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ReceivedDateTextBox" ErrorMessage="* Please enter ReceivedDate"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DespatchDate:
                </td>
                <td>
                    <asp:TextBox ID="DespatchDateTextBox" runat="server" Text='<%# Bind("DespatchDate") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    StorageUnit:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListStorageUnit" runat="server" DataSourceID="ObjectDataSourceDDLStorageUnit"
                        DataTextField="StorageUnit" DataValueField="StorageUnitId" SelectedValue='<%# Bind("StorageUnitId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLStorageUnit" runat="server"
                        TypeName="StaticInfoStorageUnit" SelectMethod="ParameterStorageUnit">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListLocation" runat="server" DataSourceID="ObjectDataSourceDDLLocation"
                        DataTextField="Location" DataValueField="LocationId" SelectedValue='<%# Bind("LocationId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLLocation" runat="server"
                        TypeName="StaticInfoLocation" SelectMethod="ParameterLocation">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Receipt:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListReceipt" runat="server" DataSourceID="ObjectDataSourceDDLReceipt"
                        DataTextField="Receipt" DataValueField="ReceiptId" SelectedValue='<%# Bind("ReceiptId") %>'  Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLReceipt" runat="server"
                        TypeName="StaticInfoReceipt" SelectMethod="ParameterReceipt">
                        <SelectParameters>
                            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    StoreInstructionId:
                </td>
                <td>
                    <asp:Label ID="StoreInstructionIdLabel" runat="server" Text='<%# Bind("StoreInstructionId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    PickInstructionId:
                </td>
                <td>
                    <asp:Label ID="PickInstructionIdLabel" runat="server" Text='<%# Bind("PickInstructionId") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    SerialNumber:
                </td>
                <td>
                    <asp:Label ID="SerialNumberLabel" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    ReceivedDate:
                </td>
                <td>
                    <asp:Label ID="ReceivedDateLabel" runat="server" Text='<%# Bind("ReceivedDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    DespatchDate:
                </td>
                <td>
                    <asp:Label ID="DespatchDateLabel" runat="server" Text='<%# Bind("DespatchDate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/SerialNumberGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceSerialNumber" runat="server" TypeName="StaticInfoSerialNumber"
    SelectMethod="GetSerialNumber"
    DeleteMethod="DeleteSerialNumber"
    UpdateMethod="UpdateSerialNumber"
    InsertMethod="InsertSerialNumber">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="SerialNumberId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="SerialNumberId" Type="Int32" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
        <asp:Parameter Name="StoreInstructionId" Type="Int32" />
        <asp:Parameter Name="PickInstructionId" Type="Int32" />
        <asp:Parameter Name="SerialNumber" Type="String" />
        <asp:Parameter Name="ReceivedDate" Type="DateTime" />
        <asp:Parameter Name="DespatchDate" Type="DateTime" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="SerialNumberId" QueryStringField="SerialNumberId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="SerialNumberId" Type="Int32" />
        <asp:Parameter Name="StorageUnitId" Type="Int32" />
        <asp:Parameter Name="LocationId" Type="Int32" />
        <asp:Parameter Name="ReceiptId" Type="Int32" />
        <asp:Parameter Name="StoreInstructionId" Type="Int32" />
        <asp:Parameter Name="PickInstructionId" Type="Int32" />
        <asp:Parameter Name="SerialNumber" Type="String" />
        <asp:Parameter Name="ReceivedDate" Type="DateTime" />
        <asp:Parameter Name="DespatchDate" Type="DateTime" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
