using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
 
public partial class UserControls_COATestFormView : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void FormViewInserted(Object s, FormViewInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            lbShowError.ForeColor = System.Drawing.Color.Red;
            lbShowError.Text = "There was an error inserting the row: " + e.Exception.ToString();
            Session["ErrorMessage"] = e.Exception.ToString();
            e.ExceptionHandled = true;
        }
        else
        {
            lbShowError.ForeColor = System.Drawing.Color.Green;
            lbShowError.Text = "The row was successfully inserted";
        }
    }
    protected void FormViewUpdated(Object s, FormViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            lbShowError.ForeColor = System.Drawing.Color.Red;
            lbShowError.Text = "There was an error updating the row: " + e.Exception.ToString();
            Session["ErrorMessage"] = e.Exception.ToString();
            e.ExceptionHandled = true;
        }
        else
        {
            lbShowError.ForeColor = System.Drawing.Color.Green;
            lbShowError.Text = "The row was successfully updated";
        }
    }
}
