<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RouteGridView.ascx.cs" Inherits="UserControls_RouteGridView" %>
 
<table style="background-color:lightgray; border-right: #e7e5db 2pt double; border-top: #e7e5db 2pt double; border-left: #e7e5db 2pt double; border-bottom: #e7e5db 2pt double;">
    <tr>
        <td>
            <telerik:RadTextBox ID="TextBoxRoute" runat="server" Label="Route" Width="300px" Skin="Metro"></telerik:RadTextBox>
        </td>
        <td>
            <telerik:RadTextBox ID="TextBoxRouteCode" runat="server" Label="RouteCode" Width="300px" Skin="Metro"></telerik:RadTextBox>
        </td>
        <td>
            <telerik:RadButton ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click"></telerik:RadButton>
        </td>
    </tr>
</table>
 
<telerik:RadGrid ID="RadGridRoute" runat="server" AllowAutomaticInserts="true" AllowAutomaticUpdates="true" AllowAutomaticDeletes="true" AutoGenerateDeleteColumn="true"
    AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="30" DataSourceID="ObjectDataSourceRoute" OnSelectedIndexChanged="RadGridRoute_SelectedIndexChanged" Skin="Metro">
<PagerStyle Mode="NumericPages"></PagerStyle>
<MasterTableView DataKeyNames="
RouteId
" DataSourceID="ObjectDataSourceRoute"
    CommandItemDisplay="Top" AutoGenerateColumns="false" InsertItemDisplay="Top"
    InsertItemPageIndexAction="ShowItemOnFirstPage">
        <Columns>
            <telerik:GridEditCommandColumn ButtonType="ImageButton"></telerik:GridEditCommandColumn>
            <telerik:GridBoundColumn ReadOnly="True" DataField="RouteId" HeaderText="RouteId" SortExpression="RouteId" UniqueName="RouteId"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Route" HeaderText="Route" SortExpression="Route" UniqueName="Route"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="RouteCode" HeaderText="RouteCode" SortExpression="RouteCode" UniqueName="RouteCode"></telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowDragToGroup="True" AllowColumnsReorder="True" EnablePostBackOnRowClick="True">
        <Resizing AllowColumnResize="true"></Resizing>
        <Selecting AllowRowSelect="true" />
       </ClientSettings>
    <GroupingSettings ShowUnGroupButton="True" />
</telerik:RadGrid>
 
<asp:ObjectDataSource ID="ObjectDataSourceRoute" runat="server" TypeName="StaticInfoRoute"
    DeleteMethod="DeleteRoute"
    InsertMethod="InsertRoute"
    SelectMethod="SearchRoute"
    UpdateMethod="UpdateRoute">
    <SelectParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RouteId" Type="Int32" DefaultValue="-1" />
        <asp:ControlParameter ControlID="TextBoxRoute" DefaultValue="%" Name="Route" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="TextBoxRouteCode" DefaultValue="%" Name="RouteCode" PropertyName="Text" Type="String" />
    </SelectParameters>
    <DeleteParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RouteId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="RouteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="Route" Type="String" />
        <asp:Parameter Name="RouteCode" Type="String" />
    </UpdateParameters>
    <InsertParameters>
        <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Direction="InputOutput" Name="RouteId" Type="Int32" />
        <asp:Parameter Name="Route" Type="String" />
        <asp:Parameter Name="RouteCode" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
