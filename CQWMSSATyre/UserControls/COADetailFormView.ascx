<%@ Control Language="C#" AutoEventWireup="true" CodeFile="COADetailFormView.ascx.cs" Inherits="UserControls_COADetailFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>COADetail</h3>
<asp:FormView ID="FormViewCOADetail" runat="server" DataKeyNames="COADetailId" DataSourceID="ObjectDataSourceCOADetail" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    COAId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCOAId" runat="server" DataSourceID="ObjectDataSourceDDLCOAId"
                        DataTextField="COA" DataValueField="COAId" SelectedValue='<%# Bind("COAId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCOAId" runat="server" TypeName="StaticInfoCOA"
                        SelectMethod="ParameterCOA">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    NoteId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListNoteId" runat="server" DataSourceID="ObjectDataSourceDDLNoteId"
                        DataTextField="Note" DataValueField="NoteId" SelectedValue='<%# Bind("NoteId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLNoteId" runat="server" TypeName="StaticInfoNote"
                        SelectMethod="ParameterNote">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    NoteCode:
                </td>
                <td>
                    <asp:TextBox ID="NoteCodeTextBox" runat="server" Text='<%# Bind("NoteCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Note:
                </td>
                <td>
                    <asp:TextBox ID="NoteTextBox" runat="server" Text='<%# Bind("Note") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    COAId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCOAId" runat="server" DataSourceID="ObjectDataSourceDDLCOAId"
                        DataTextField="COA" DataValueField="COAId" SelectedValue='<%# Bind("COAId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCOAId" runat="server" TypeName="StaticInfoCOA"
                        SelectMethod="ParameterCOA">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    NoteId:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListNoteId" runat="server" DataSourceID="ObjectDataSourceDDLNoteId"
                        DataTextField="Note" DataValueField="NoteId" SelectedValue='<%# Bind("NoteId") %>' >
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLNoteId" runat="server" TypeName="StaticInfoNote"
                        SelectMethod="ParameterNote">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    NoteCode:
                </td>
                <td>
                    <asp:TextBox ID="NoteCodeTextBox" runat="server" Text='<%# Bind("NoteCode") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Note:
                </td>
                <td>
                    <asp:TextBox ID="NoteTextBox" runat="server" Text='<%# Bind("Note") %>'></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    COA:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCOAId" runat="server" DataSourceID="ObjectDataSourceDDLCOAId"
                        DataTextField="COA" DataValueField="COAId" SelectedValue='<%# Bind("COAId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLCOAId" runat="server" TypeName="StaticInfoCOA"
                        SelectMethod="ParameterCOA">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    Note:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListNoteId" runat="server" DataSourceID="ObjectDataSourceDDLNoteId"
                        DataTextField="Note" DataValueField="NoteId" SelectedValue='<%# Bind("NoteId") %>' Enabled="false">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ObjectDataSourceDDLNoteId" runat="server" TypeName="StaticInfoNote"
                        SelectMethod="ParameterNote">
                        <SelectParameters>
                            <asp:SessionParameter Name="ConnectionStringName" SessionField="ConnectionStringName" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    NoteCode:
                </td>
                <td>
                    <asp:Label ID="NoteCodeLabel" runat="server" Text='<%# Bind("NoteCode") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Note:
                </td>
                <td>
                    <asp:Label ID="NoteLabel" runat="server" Text='<%# Bind("Note") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/COADetailGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourceCOADetail" runat="server" TypeName="StaticInfoCOADetail"
    SelectMethod="GetCOADetail"
    DeleteMethod="DeleteCOADetail"
    UpdateMethod="UpdateCOADetail"
    InsertMethod="InsertCOADetail">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COADetailId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COADetailId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteCode" Type="String" DefaultValue="-1" />
        <asp:Parameter Name="Note" Type="String" DefaultValue="-1" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="COADetailId" QueryStringField="COADetailId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="COADetailId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="COAId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="NoteCode" Type="String" />
        <asp:Parameter Name="Note" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:Label  ID="lbShowError" runat="server"></asp:Label>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
