<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PackTypeFormView.ascx.cs" Inherits="UserControls_PackTypeFormView" %>
 
<script type="text/javascript" language="JavaScript">
function openNewWindow()
{
    window.open("Error.aspx", "_blank",
        "height=440px width=540px top=150 left=150 resizable=no scrollbars=no ");
}
</script>
 
<h3>PackType</h3>
<asp:FormView ID="FormViewPackType" runat="server" DataKeyNames="PackTypeId" DataSourceID="ObjectDataSourcePackType" OnItemInserted="FormViewInserted" OnItemUpdated="FormViewUpdated" DefaultMode="Insert">
    <EditItemTemplate>
        <table>
            <tr>
                <td>
                    PackType:
                </td>
                <td>
                    <asp:TextBox ID="PackTypeTextBox" runat="server" Text='<%# Bind("PackType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PackTypeTextBox" ErrorMessage="* Please enter PackType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    InboundSequence:
                </td>
                <td>
                    <asp:TextBox ID="InboundSequenceTextBox" runat="server" Text='<%# Bind("InboundSequence") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="InboundSequenceTextBox" ErrorMessage="* Please enter InboundSequence"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OutboundSequence:
                </td>
                <td>
                    <asp:TextBox ID="OutboundSequenceTextBox" runat="server" Text='<%# Bind("OutboundSequence") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="OutboundSequenceTextBox" ErrorMessage="* Please enter OutboundSequence"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </EditItemTemplate>
    <InsertItemTemplate>
        <table>
            <tr>
                <td>
                    PackType:
                </td>
                <td>
                    <asp:TextBox ID="PackTypeTextBox" runat="server" Text='<%# Bind("PackType") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PackTypeTextBox" ErrorMessage="* Please enter PackType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    InboundSequence:
                </td>
                <td>
                    <asp:TextBox ID="InboundSequenceTextBox" runat="server" Text='<%# Bind("InboundSequence") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="InboundSequenceTextBox" ErrorMessage="* Please enter InboundSequence"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    OutboundSequence:
                </td>
                <td>
                    <asp:TextBox ID="OutboundSequenceTextBox" runat="server" Text='<%# Bind("OutboundSequence") %>'></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="OutboundSequenceTextBox" ErrorMessage="* Please enter OutboundSequence"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"></asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </InsertItemTemplate>
    <ItemTemplate>
        <table>
            <tr>
                <td>
                    PackType:
                </td>
                <td>
                    <asp:Label ID="PackTypeLabel" runat="server" Text='<%# Bind("PackType") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    InboundSequence:
                </td>
                <td>
                    <asp:Label ID="InboundSequenceLabel" runat="server" Text='<%# Bind("InboundSequence") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    OutboundSequence:
                </td>
                <td>
                    <asp:Label ID="OutboundSequenceLabel" runat="server" Text='<%# Bind("OutboundSequence") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButtonBack" runat="server" Text="Back" PostBackUrl="~/Administration/PackTypeGridView.aspx"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
 
<asp:ObjectDataSource ID="ObjectDataSourcePackType" runat="server" TypeName="StaticInfoPackType"
    SelectMethod="GetPackType"
    DeleteMethod="DeletePackType"
    UpdateMethod="UpdatePackType"
    InsertMethod="InsertPackType">
    <DeleteParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="PackTypeId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="PackTypeId" Type="Int32" />
        <asp:Parameter Name="PackType" Type="String" />
        <asp:Parameter Name="InboundSequence" Type="Int16" />
        <asp:Parameter Name="OutboundSequence" Type="Int16" />
    </UpdateParameters>
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
      <asp:QueryStringParameter Name="PackTypeId" QueryStringField="PackTypeId" DefaultValue="-1" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:Parameter Name="PackTypeId" Type="Int32" />
        <asp:Parameter Name="PackType" Type="String" />
        <asp:Parameter Name="InboundSequence" Type="Int16" />
        <asp:Parameter Name="OutboundSequence" Type="Int16" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:LinkButton ID="LinkButtonShowError" runat="server" CommandName="ShowError" OnClientClick="javascript:openNewWindow();"></asp:LinkButton>
<br />
<br />
<asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="window.opener.document.forms[0].submit();window.close()"/>
