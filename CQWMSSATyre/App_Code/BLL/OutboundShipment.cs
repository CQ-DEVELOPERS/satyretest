using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Author: Grant Schultz
/// Date:   20 July 2007
/// Summary description for OutboundShipment
/// </summary>
public class OutboundShipment
{
    #region Constructor Logic
    public int outboundShipmentId;

    public OutboundShipment()
    {
        outboundShipmentId = -1;
    }
    #endregion "Constructor Logic"

    #region SearchOutboundShipmentCreate
    /// <summary>
    /// Searches for an OutboundShipment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    public DataSet SearchOutboundShipmentCreate(string connectionStringName,
                                                int WarehouseId,
                                                int OutboundDocumentTypeId,
                                                int OutboundShipmentId,
                                                DateTime FromDate,
                                                DateTime ToDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Shipment_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOutboundShipmentCreate"

    #region UpdateOutboundShipment
    public bool UpdateOutboundShipment(string connectionStringName, int outboundShipmentId, int locationId, int routeId, DateTime shipmentDate, string sealNumber, string vehicleRegistration, string remarks, int despatchBay, int contactListId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Shipment_Edit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "routeId", DbType.Int32, routeId);
        db.AddInParameter(dbCommand, "shipmentDate", DbType.DateTime, shipmentDate);
        db.AddInParameter(dbCommand, "sealNumber", DbType.String, sealNumber);
        db.AddInParameter(dbCommand, "vehicleRegistration", DbType.String, vehicleRegistration);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);
        db.AddInParameter(dbCommand, "despatchBay", DbType.Int32, despatchBay);
        db.AddInParameter(dbCommand, "contactListId", DbType.Int32, contactListId);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOutboundShipment"

    #region GetOutboundShipmentCreate
    /// <summary>
    /// Get a single OutboundShipment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetOutboundShipmentCreate(string connectionStringName, int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Shipment_Get";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOutboundShipmentCreate"

    #region GetLinkedIssues
    /// <summary>
    /// Retreives Issues linked to an Outbound Shipment.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetLinkedIssues(string connectionStringName, int OutboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Shipment_Linked_Issue";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetLinkedIssues"

    #region GetUnlinkedIssues
    /// <summary>
    /// Retreives Issues not linked to an Outbound Shipment.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    public DataSet GetUnlinkedIssues(string connectionStringName,
                                        int WarehouseId,
                                        int OutboundDocumentTypeId,
                                        string OrderNumber,
                                        string ExternalCompanyCode,
                                        string ExternalCompany,
                                        DateTime FromDate,
                                        DateTime ToDate,
                                        int principalId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Shipment_Unlinked_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetUnlinkedIssues"

    #region GetUnlinkedIssuesPicked
    /// <summary>
    /// Retreives Issues not linked to an Outbound Shipment.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    public DataSet GetUnlinkedIssuesPicked(string connectionStringName,
                                            int WarehouseId,
                                            int OutboundDocumentTypeId,
                                            string OrderNumber,
                                            string ExternalCompanyCode,
                                            string ExternalCompany,
                                            DateTime FromDate,
                                            DateTime ToDate,
                                            int principalId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Shipment_Unlinked_Picked_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetUnlinkedIssuesPicked"

    #region GetLoadDetails
    /// <summary>
    /// Retreives Issues not linked to an Outbound Shipment.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetLoadDetails(string connectionStringName, int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Load_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetLoadDetails"

    #region Transfer
    /// <summary>
    /// Adds a Issue to an OutboundShipment.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <returns></returns>
    public bool Transfer(string connectionStringName, int OutboundShipmentId, int IssueId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundShipmentIssue_Link";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Transfer"

    #region Remove
    /// <summary>
    /// Removes a Issue from an OutboundShipment.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundShipmentId"></param>
    /// <param name="IssueId"></param>
    /// <returns></returns>
    public bool Remove(string connectionStringName, int OutboundShipmentId, int IssueId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundShipmentIssue_Unlink";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, OutboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Remove"

    #region InsertOutboundShipment
    /// <summary>
    /// Insert an Outbound Shipment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="shipmentDate"></param>
    /// <param name="remarks"></param>
    /// <returns></returns>
    public bool InsertOutboundShipment(string connectionStringName, int warehouseId, DateTime shipmentDate, string remarks)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_OutboundShipment_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "ShipmentDate", DbType.DateTime, shipmentDate);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, remarks);

        // Enter a default outboundShipmentId, and catch the returned outboundShipmentId for the new record
        db.AddOutParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            
            try
            {
                db.ExecuteNonQuery(dbCommand);
                
                //OutboundShipmentId
                outboundShipmentId = (int)db.GetParameterValue(dbCommand, "outboundShipmentId");
                
                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion "InsertOutboundShipment"

    #region GetDropSequenceDetails
    /// <summary>
    /// Retreives Issues linked to an Outbound Shipment, for updating the Drop Sequence.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetDropSequenceDetails(string connectionStringName, int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Drop_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetDropSequenceDetails"

    #region UpdateDropSequenceDetails
    /// <summary>
    /// Updates Drop Sequence Details for an Issue to an OutboundShipment.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="IssueId"></param>
    /// <param name="DropSequence"></param>
    /// <returns></returns>
    public bool UpdateDropSequenceDetails(string connectionStringName, int IssueId, int DropSequence)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Drop_Details_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, IssueId);
        db.AddInParameter(dbCommand, "DropSequence", DbType.Int32, DropSequence);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion "UpdateDropSequenceDetails"

    #region GetShipmentDetails
    /// <summary>
    /// Retreives Issues linked to an Outbound Shipment, for updating the Drop Sequence.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetShipmentDetails(string connectionStringName, int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Shipment_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetShipmentDetails"

    #region GetDropSequence
    /// <summary>
    /// Gets avaliable Drop Sequences
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetDropSequence(string connectionStringName, int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_DropSequence";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetDropSequence"

    #region GetOrderDetails
    /// <summary>
    /// Retreives Issues linked to an Outbound Shipment, for updating the Drop Sequence.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetOrderDetails(string connectionStringName, int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_Order_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderLineDetails"

    #region GetOrderLineDetails
    /// <summary>
    /// Retreives Issues linked to an Outbound Shipment, for updating the Drop Sequence.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetOrderLineDetails(string connectionStringName, int outboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Outbound_OrderLine_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderLineDetails"
}
