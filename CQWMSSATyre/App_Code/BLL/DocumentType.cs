using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

// --------------------------------------------
// CLASS : DocumentType
// --------------------------------------------
/// <summary>
/// Executes the query. Returning objects.
/// </summary>        
public class DocumentType : BaseBLL
{

  public DocumentType()
  {
  }

  /// <summary>
  /// Gets the document types.
  /// </summary>
  /// <returns></returns>
  public DataSet GetDocumentTypes()
  {
    return ExecuteDataSet( "p_DocumentType_Select" );
  }

  /// <summary>
  /// Updates the specified id.
  /// </summary>
  /// <param name="id">The id.</param>
  /// <param name="vehicleTypeId">The vehicle type id.</param>
  /// <param name="name">The name.</param>
  /// <returns></returns>
  public int Update( int id, string documentTypeId, string name )
  {
    string sql = string.Format( "p_DocumentType_{0}", id > 0 ? "Update" : "Insert" );

    var idParam = new ParamItem( "Id", DbType.Int32, id, ParameterDirection.InputOutput );

    var idValue = ExecuteNonQuery( sql, idParam
                                      , new ParamItem( "DocumentTypeId", documentTypeId )
                                      , new ParamItem( "Name", name ) );
    
    return (int)idParam.Value;
  }

  /// <summary>
  /// Deletes the specified id.
  /// </summary>
  /// <param name="id">The id.</param>
  /// <param name="original_id">The original_id.</param>
  /// <returns></returns>
  public bool Delete( int id, int original_id )
  {
    try
    {
      ExecuteNonQuery( "p_DocumentType_Delete", new ParamItem( "Id", original_id ) );
    }
    catch ( Exception ex )
    {
      return false;
    }

    return true;
  }


}
