using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Operator
/// </summary>
public class Operator
{
    public string strAccess = "~/App_Themes/Default/0.png";


    #region "Constructor Logic"
    public Operator()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Construct"

    #region GetOperators
    public DataSet GetOperators(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Parameter_By_Warehouse";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetOperators

    #region GetOperatorPrint
    public DataSet GetOperatorPrint(string connectionStringName, int warehouseId, string StartOperator, String EndOperator)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_User_Print";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "StartOperator", DbType.String, StartOperator);
        db.AddInParameter(dbCommand, "EndOperator", DbType.String, EndOperator);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetOperatorPrint

    #region GetCultures
    public DataSet GetCultures(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Cultures";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetCultures

    #region GetOperatorsDetails
    public DataSet GetOperatorsDetails(string connectionStringName, int warehouseId, bool rblActiveOnly)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Details_By_Warehouse";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "rblActiveOnly", DbType.Boolean, rblActiveOnly);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetOperatorsDetails

    #region UpdateOperatorDetail
    public bool UpdateOperatorDetail(string connectionStringName, int warehouseId, int OperatorId, int OperatorGroupId, bool activeIndicator, int CultureId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "ActiveIndicator", DbType.Boolean, activeIndicator);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);

        try
        {
            db.ExecuteNonQuery(dbCommand);
            result = true;
        }
        catch
        { }

        return result;
    }
    #endregion UpdateOperatorDetail
 
    #region UpdateOperatorCulture
    public bool UpdateOperatorCulture(string connectionStringName, int warehouseId, int OperatorId, int CultureId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Update_Details_By_Warehouse";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, CultureId);

        try
        {
            db.ExecuteNonQuery(dbCommand);
            result = true;
        }
        catch
        { }

        return result;
    }
    #endregion UpdateOperatorCulture

    #region GetOperatorGroups
    public DataSet GetOperatorGroups(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Groups";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        //db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetOperatorGroups

    #region GetMenuItems

    public DataSet GetMenuItems(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Menu_Items";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        //db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetMenuItems

    #region GetMenus

    public DataSet GetMenus(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_Menu_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        //db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetMenus

    #region GetOperatorList
    public DataSet GetOperatorList(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_List_By_Warehouse";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetOperatorList

    #region InsertOperator
    public int InsertOperator(string connectionStringName,
                                    int operatorGroupId,
                                    int warehouseId,
                                    string userName,
                                    string operatorCode,
                                    string password,
                                    bool activeIndicator,
                                    DateTime expiryDate)
    {
        int operatorId = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operator", DbType.String, userName);
        db.AddInParameter(dbCommand, "operatorCode", DbType.String, operatorCode);
        db.AddInParameter(dbCommand, "password", DbType.String, password);
        db.AddInParameter(dbCommand, "activeIndicator", DbType.Boolean, activeIndicator);
        db.AddInParameter(dbCommand, "expiryDate", DbType.DateTime, expiryDate);
        db.AddInParameter(dbCommand, "CultureID", DbType.Int16, 1);

        //Add out parameter to return the OperatorId
        db.AddParameter(dbCommand, "operatorId", DbType.Int32, ParameterDirection.Output, "operatorId", DataRowVersion.Current, null);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                // Get output parameter
                operatorId = (int)db.GetParameterValue(dbCommand, "operatorId");
            }
            catch { }

            connection.Close();

            return operatorId;
        }
    }
    #endregion InsertOperator

    #region UpdateOperatorGroupId
    public bool UpdateOperatorGroupId(string connectionStringName,
                                        int operatorId,
                                        int operatorGroupId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "operatorGroupId", DbType.Int32, operatorGroupId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateOperatorGroupId

    #region UpdateOperatorCultureId
    public bool UpdateOperatorCultureId(string connectionStringName,
                                        int operatorId,
                                        int operatorCultureId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "operatorCultureId", DbType.Int32, operatorCultureId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateOperatorCultureId

    #region UpdateOperator
    public bool UpdateOperator(string connectionStringName,
                                        int operatorId,
                                        int operatorGroupId,
                                        bool activeIndicator,
                                        int cultureId,      
                                        string printer,
                                        string port,
                                        string iPAddress,
                                        int warehouseId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        if (printer == null)
            printer = "";

        if (port == null)
            port = "";

        if (iPAddress == null)
            iPAddress = "";

        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "ActiveIndicator", DbType.Boolean, activeIndicator);
        db.AddInParameter(dbCommand, "CultureId", DbType.Int32, cultureId);
        db.AddInParameter(dbCommand, "Printer", DbType.String, printer);
        db.AddInParameter(dbCommand, "Port", DbType.String, port);
        db.AddInParameter(dbCommand, "IPAddress", DbType.String, iPAddress);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateOperator

    #region InsertOperatorGroup
    public bool InsertOperatorGroup(string connectionStringName,
                                        string OperatorGroupDesc,
                                        int SourceOperatorGroupId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Group_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "OperatorGroupDesc", DbType.String, OperatorGroupDesc);
        db.AddInParameter(dbCommand, "SourceOperatorGroupId", DbType.Int32, SourceOperatorGroupId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion InsertOperatorGroup

    #region GetOperatorId
    public int GetOperatorId(string connectionStringName, string userName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Get_OperatorId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "operator", DbType.String, userName);

        int operatorId = (int)db.ExecuteScalar(dbCommand);

        return operatorId;
    }
    #endregion GetOperatorId

    #region GetOperatorMenuGroup


    public string GetOperatorMenuGroup(string connectionStringName, int MenuItemId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        string str = "";
        int cnt = 1;
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Menu_Group";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int16, MenuItemId);
        DataSet dataset = db.ExecuteDataSet(dbCommand);


        //dataset = db.ExecuteDataSet(dbCommand);

        //return dataset;
        while (cnt < dataset.Tables[0].Rows.Count + 1)
        {
            str = str + dataset.Tables[0].Rows[cnt - 1][0].ToString();
            cnt++;
        }


        return str;
    }
    #endregion GetOperatorMenuGroup

    #region GetOperatorGroupMenu


    public string GetOperatorGroupMenu(string connectionStringName, int OperatorGroupID)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        string str = "";
        int cnt = 1;
        string sqlCommand = connectionStringName + ".dbo.p_Operator_Group_Menu";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int16, OperatorGroupID);
        DataSet dataset = db.ExecuteDataSet(dbCommand);


        //dataset = db.ExecuteDataSet(dbCommand);

        //return dataset;
        while (cnt < dataset.Tables[0].Rows.Count + 1)
        {
            str = str + dataset.Tables[0].Rows[cnt - 1][0].ToString();
            cnt++;
        }


        return str;
    }
    #endregion GetOperatorGroupMenu

    #region OperatorGroupSwitchAccess
    public bool OperatorGroupSwitchAccess(string connectionStringName,
                                        int OperatorGroupId,
                                        int MenuItemId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Group_Switch_Access";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddOutParameter(dbCommand, "Access", DbType.String, 40);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
                strAccess = (string)db.GetParameterValue(dbCommand, "Access");
                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion OperatorGroupSwitchAccess

    #region UpdateOperatorPrinter
    public bool UpdateOperatorPrinter(string connectionStringName,
                                        int operatorId,
                                        string printer,
                                        string port,
                                        string iPAddress)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Printer_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "printer", DbType.String, printer);
        db.AddInParameter(dbCommand, "port", DbType.String, port);
        db.AddInParameter(dbCommand, "iPAddress", DbType.String, iPAddress);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateOperatorPrinter

    #region UpdateOperatorPassword
    public bool UpdateOperatorPassword(string connectionStringName,
                                        int operatorId,
                                        string password)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Password_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "password", DbType.String, password);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateOperatorPassword

    #region GetNextPickingJob
    public int GetNextPickingJob(String connectionStringName, Int32 operatorId, String instructionTypeCode, String referenceNumber)
    {
        Int32 jobId = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Next_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        // Set JobId as an output parameter
        db.AddParameter(dbCommand, "jobId", DbType.Int32, ParameterDirection.InputOutput, "jobId", DataRowVersion.Proposed, jobId);
        db.AddInParameter(dbCommand, "instructionTypeCode", DbType.String, instructionTypeCode);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get JobId output parameter
                jobId = (int)db.GetParameterValue(dbCommand, "jobId");
            }
            catch { }

            connection.Close();

            return jobId;
        }
    }
    #endregion GetNextPickingJob

    #region DeleteOperator
    public bool DeleteOperator(string connectionStringName,
                                        int operatorId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DeleteOperator

    #region GetOperatorId
    public static bool GetOperatorUploadAccess(string connectionStringName, int operatorId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Operator_Get_Upload_Access";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        bool uploadAccess = (bool)db.ExecuteScalar(dbCommand);

        return uploadAccess;
    }
    #endregion GetOperatorId
}
