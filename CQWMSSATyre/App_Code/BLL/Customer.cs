using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for OutboundDocumentType
/// </summary>
public class Customer
{
    #region Constructor Logic
    public Customer()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region SearchCustomers
    /// <summary>
     /// Searches for customers on input parameters
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="customer"></param>
    /// <param name="customerCode"></param>
    /// <returns></returns>
    public DataSet SearchCustomers(string connectionStringName, string customer, string customerCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Search_Customer";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, customer);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, customerCode);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchCustomers
}
