using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Area
/// </summary>
public class StockTake
{
    #region Constructor Logic
    public StockTake()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region CreateStockTakeLocation
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool CreateStockTakeLocation(string connectionStringName, int warehouseId, int operatorId, int jobId, int locationId, int StockTakeReferenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Create_Location";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, StockTakeReferenceId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateStockTakeLocation

    #region CreateStockTakeGroup
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool CreateStockTakeGroup(string connectionStringName, int warehouseId, int operatorId, int jobId, int StockTakeGroupId, int StockTakeReferenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Create_Group";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "StockTakeGroupId", DbType.Int32, StockTakeGroupId);
        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, StockTakeReferenceId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateStockTakeGroup

    #region CreateStockTakeProduct
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool CreateStockTakeProduct(string connectionStringName, int warehouseId, int operatorId, int jobId, int locationId, int storageUnitId, int StockTakeReferenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Create_Product";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, StockTakeReferenceId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateStockTakeProduct

    #region CreateStockTakeBatch
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool CreateStockTakeBatch(string connectionStringName, int warehouseId, int operatorId, int jobId, int locationId, int storageUnitBatchId, int StockTakeReferenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Create_Product";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, StockTakeReferenceId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateStockTakeBatch

    #region CreateStockTakeArea
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool CreateStockTakeArea(string connectionStringName, int warehouseId, int operatorId, int jobId, int areaId, bool areaOrAisle, int split, bool columnOrLevel, int StockTakeReferenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Create_Area";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "areaId", DbType.Int32, areaId);
        db.AddInParameter(dbCommand, "areaOrAisle", DbType.Boolean, areaOrAisle);
        db.AddInParameter(dbCommand, "split", DbType.Int32, split);
        db.AddInParameter(dbCommand, "columnOrLevel", DbType.Boolean, columnOrLevel);
        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, StockTakeReferenceId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateStockTakeArea

    #region CreateStockTakeAisle
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool CreateStockTakeAisle(string connectionStringName, int warehouseId, int operatorId, int jobId, bool byAisle, bool byLevel, int split, bool columnOrLevel, string fromAisle, string toAisle, string fromLevel, string toLevel, int StockTakeReferenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Create_Aisle_Level";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "byAisle", DbType.Boolean, byAisle);
        db.AddInParameter(dbCommand, "byLevel", DbType.Boolean, byLevel);
        db.AddInParameter(dbCommand, "split", DbType.Int32, split);
        db.AddInParameter(dbCommand, "columnOrLevel", DbType.Boolean, columnOrLevel);
        db.AddInParameter(dbCommand, "fromAisle", DbType.String, fromAisle);
        db.AddInParameter(dbCommand, "toAisle", DbType.String, toAisle);
        db.AddInParameter(dbCommand, "fromLevel", DbType.String, fromLevel);
        db.AddInParameter(dbCommand, "toLevel", DbType.String, toLevel);
        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, StockTakeReferenceId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateStockTakeAisle

    #region CreateStockTakeJob
    /// <summary>
    /// Creates a Stock Take Job
    /// </summary>
    public int CreateStockTakeJob(string connectionStringName, int warehouseId, int operatorId, string instructionTypeCode, string referenceNumber, int StockTakeReferenceId)
    {
        int jobId = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Create_Job";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "instructionTypeCode", DbType.String, instructionTypeCode);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, StockTakeReferenceId);

        //Return the correct id for the new record
        db.AddOutParameter(dbCommand, "jobId", DbType.Int32, jobId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                jobId = (int)db.GetParameterValue(dbCommand, "jobId");
            }
            catch { }

            connection.Close();

            return jobId;
        }
    }
    #endregion CreateStockTakeJob

    #region CreateStockTakeCategory
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool CreateStockTakeCategory(string connectionStringName, int warehouseId, int operatorId, int jobId, string item, int StockTakeReferenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Create_Product_Category";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "Category", DbType.String, item);
        //db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, StockTakeReferenceId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateStockTakeCategory

    #region UpdateStockTakeJob
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool UpdateStockTakeJob(string connectionStringName, int operatorId, int jobId, int priorityId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Update_Job";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "priorityId", DbType.Int32, priorityId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateStockTakeJob

    #region UpdateStockTakeCount
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool UpdateStockTakeCount(string connectionStringName, int instructionId, int operatorId, int latestCount)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Update_Count";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "latestCount", DbType.Int32, latestCount);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateStockTakeCount

    #region Release
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool Release(string connectionStringName, int jobId, int operatorId, string statusCode)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Release";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "statusCode", DbType.String, statusCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Release

    #region Cancel
    /// <summary>
    /// Cancels a Stock Take Job
    /// </summary>
    public bool Cancel(string connectionStringName, int jobId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTake_Cancel_Job";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 1800;

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Cancel

    #region ViewDetails
    public DataSet ViewDetails(string connectionStringName, string referenceNumber)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ViewDetails

    #region SearchDetails
    public DataSet SearchDetails(string connectionStringName, int warehouseId, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchDetails

    #region ViewAuthoriseDetails
    public DataSet ViewAuthoriseDetails(string connectionStringName, int warehouseId, int areaId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Authorise";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "areaId", DbType.Int32, areaId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet ViewAuthoriseDetails(string connectionStringName, int warehouseId, int areaId, string jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Authorise";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "areaId", DbType.Int32, areaId);
        db.AddInParameter(dbCommand, "jobId ", DbType.String, jobId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ViewAuthoriseDetails

    #region ViewLineDetails
    public DataSet ViewLineDetails(string connectionStringName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_View";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ViewLineDetails

    #region ViewUpdateDetails
    public DataSet ViewUpdateDetails(string connectionStringName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Count";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ViewUpdateDetails

    #region Authorise
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool Authorise(string connectionStringName, int instructionId, int operatorId, string statusCode)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Update";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "statusCode", DbType.String, statusCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Authorise

    #region ProductInsertSelect
    public DataSet ProductInsertSelect(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Product_Insert_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ProductInsertSelect

    #region ProductInsert
    public int ProductInsert(string connectionStringName, int operatorId, int instructionId, int storageUnitBatchId, int locationId, Decimal quantity)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Product_Insert";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        //Add out parameter to return the InstructionId
        db.AddParameter(dbCommand, "instructionId", DbType.Int32, ParameterDirection.InputOutput, "instructionId", DataRowVersion.Current, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch
            {
                instructionId = -1;
            }

            connection.Close();

            return instructionId;
        }
    }
    #endregion ProductInsert

    #region ViewUploadDetails
    public DataSet ViewUploadDetails(string connectionStringName, int warehouseId, string warehouseCode, string productCode, string product, string batch, string skuCode, bool showZeroVariance, bool showSent, int principalId, string category, bool ExactMatch)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Upload";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "category", DbType.String, category);
        db.AddInParameter(dbCommand, "skuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "warehouseCode", DbType.String, warehouseCode);
        db.AddInParameter(dbCommand, "showZeroVariance", DbType.Boolean, showZeroVariance);
        db.AddInParameter(dbCommand, "showSent", DbType.Boolean, showSent);
        db.AddInParameter(dbCommand, "principalId", DbType.Int32, principalId);
        db.AddInParameter(dbCommand, "CategoryExactMatch", DbType.Boolean, ExactMatch);

		dbCommand.CommandTimeout = 0;
        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ViewUploadDetails

    #region UploadDetailsAccept
    /// <summary>
    /// Sends adjustment to host
    /// </summary>
    public bool UploadDetailsAccept(String connectionStringName,
                                    Int32 warehouseId,
                                    DateTime comparisonDate,
                                    Int32 storageUnitId,
                                    Int32 batchId,
                                    String warehouseCode)
    {
        Boolean result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Housekeeping_Stock_Take_Upload_Accept";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "comparisonDate", DbType.DateTime, comparisonDate);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "batchId", DbType.Int32, batchId);
        db.AddInParameter(dbCommand, "warehouseCode", DbType.String, warehouseCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {

            }

            connection.Close();

            return result;
        }
    }
    #endregion UploadDetailsAccept

    #region UploadDetailsReject
    /// <summary>
    /// Sends adjustment to host
    /// </summary>
    public bool UploadDetailsReject(String connectionStringName, Int32 warehouseId, DateTime comparisonDate, Int32 storageUnitId, Int32 batchId, String warehouseCode)
    {
        Boolean result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Housekeeping_Stock_Take_Upload_Reject";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "comparisonDate", DbType.DateTime, comparisonDate);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "batchId", DbType.Int32, batchId);
        db.AddInParameter(dbCommand, "warehouseCode", DbType.String, warehouseCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UploadDetailsReject

    #region UploadDetailsReload
    /// <summary>
    /// Sends adjustment to host
    /// </summary>
    public bool UploadDetailsReload(String connectionStringName, Int32 warehouseId)
    {
        Boolean result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Housekeeping_Stock_Take_Reload";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 0;

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UploadDetailsReload

    #region ViewSOHDetails
    public DataSet ViewSOHDetails(string connectionStringName, int warehouseId, string productCode, string product, string batch)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_On_Hand";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ViewSOHDetails

    #region UploadStockOnHand
    /// <summary>
    /// Sends adjustment to host
    /// </summary>
    public bool UploadStockOnHand(String connectionStringName, Int32 warehouseId)
    {
        Boolean result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Housekeeping_Stock_On_Hand_Upload";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UploadStockOnHand

    #region SOHReload
    /// <summary>
    /// Sends adjustment to host
    /// </summary>
    public bool SOHReload(String connectionStringName, Int32 warehouseId)
    {
        Boolean result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Housekeeping_Stock_On_Hand_Reload";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SOHReload

    #region SOHDetailsAccept
    /// <summary>
    /// Sends adjustment to host
    /// </summary>
    public bool SOHDetailsAccept(String connectionStringName, Int32 warehouseId, DateTime comparisonDate, Int32 storageUnitId, Int32 batchId)
    {
        Boolean result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Housekeeping_Stock_On_Hand_Accept";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "comparisonDate", DbType.DateTime, comparisonDate);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "batchId", DbType.Int32, batchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SOHDetailsAccept

    #region SOHDetailsReject
    /// <summary>
    /// Sends adjustment to host
    /// </summary>
    public bool SOHDetailsReject(String connectionStringName, Int32 warehouseId, DateTime comparisonDate, Int32 storageUnitId, Int32 batchId)
    {
        Boolean result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Housekeeping_Stock_On_Hand_Reject";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "comparisonDate", DbType.DateTime, comparisonDate);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "batchId", DbType.Int32, batchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SOHDetailsReject

    #region AislesList
    public DataSet AislesList(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Aisle_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion AislesList

    #region LevelsList
    public DataSet LevelsList(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Level_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion LevelsList

    #region GroupList
    public DataSet GroupList(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Group_Search_Warehouse";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GroupList

    #region CreateStockTakeReference

    public DataSet StockTake_Search(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Static_StockTake_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        //db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, stockTakeReferenceId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public bool StockTake_Update(string connectionStringName,
        int warehouseId,
        string stockTakeType,
        DateTime startDate,
        DateTime endDate,
        int operatorId,
        int stockTakeReferenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTake_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "StockTakeType", DbType.String, stockTakeType);
        db.AddInParameter(dbCommand, "StartDate", DbType.DateTime, startDate);
        db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, DBNull.Value);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, stockTakeReferenceId);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = true;
            }
            catch (Exception ex) { }
        }
        //connection.Close();

        return result;

    }

    public static string StockTake_Add(
        string connectionStringName,
        int stockTakeReferenceNumber,
        int warehouseId,
        string stockTakeType,
        DateTime startDate,
        //DateTime endDate,
        int operatorId,
        out int stockTakeReferenceId,
        string Reference)
    {
        string result = "";

        stockTakeReferenceId = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTake_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "StockTakeType", DbType.String, stockTakeType);
        db.AddInParameter(dbCommand, "StartDate", DbType.DateTime, startDate);
        //db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, endDate);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddOutParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, stockTakeReferenceId);
        db.AddInParameter(dbCommand, "Reference", DbType.String, Reference);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                string Error = db.ExecuteScalar(dbCommand).ToString();
                result = (Error == "" ? "Stocktake was added successfully." : Error);//int.Parse(db.GetParameterValue(dbCommand, "StockTakeReferenceId").ToString());
            }
            catch (Exception ex) {
                result = ex.Message;
            }
        }
        //connection.Close();

        return result;

    }

    public static bool StockTake_Delete(string connectionStringName, int StockTakeReferenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTake_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, StockTakeReferenceId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }


    #endregion

    #region CreateStockTakeGroup

    public DataSet StockTakeGroup_Search(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTakeGroup_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion CreateStockTakeGroup

    #region StockTakeGroup_Update
    public bool StockTakeGroup_Update(string connectionStringName,
        string stockTakeGroupCode,
        string stockTakeGroup,
        int    stockTakeGroupId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTakeGroup_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StockTakeGroupCode", DbType.String, stockTakeGroupCode);
        db.AddInParameter(dbCommand, "StockTakeGroup", DbType.String, stockTakeGroup);
        db.AddInParameter(dbCommand, "StockTakeGroupId", DbType.Int32, stockTakeGroupId);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = true;
            }
            catch (Exception ex) { }
        }
        //connection.Close();

        return result;

    }
    #endregion StockTakeGroup_Update

    #region StockTakeGroup_Add
    public static string StockTakeGroup_Add(
        string connectionStringName,
        string stockTakeGroupCode,
        string stockTakeGroup)
    {
        string result = "";

        //stockTakeReferenceId = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTakeGroup_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
        db.AddInParameter(dbCommand, "StockTakeGroupCode", DbType.String, stockTakeGroupCode);
        db.AddInParameter(dbCommand, "stockTakeGroup", DbType.String, stockTakeGroup);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                string Error = db.ExecuteScalar(dbCommand).ToString();
                result = (Error == "" ? "Stocktake Group was added successfully." : Error);//int.Parse(db.GetParameterValue(dbCommand, "StockTakeReferenceId").ToString());
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
        }
        //connection.Close();

        return result;

    }
    #endregion StockTakeGroup_Add

    #region StockTakeGroup_Delete
    public static bool StockTakeGroup_Delete(string connectionStringName, int stockTakeGroupId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTakeGroup_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StockTakeGroupId", DbType.Int32, stockTakeGroupId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion StockTakeGroup_Delete

    #region SearchStockTakeGroupLoc

    public DataSet StockTakeGroupLoc_Search(string connectionStringName, int stockTakeGroupId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTakeGroupLocation_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "StockTakeGroupId", DbType.Int32, stockTakeGroupId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchStockTakeGroupLoc

    #region StockTakeGroupLoc_Add
    public static string StockTakeGroupLoc_Add(
        string connectionStringName,
        int stockTakeGroupId,
        int locationId)
    {
        string result = "";

        //stockTakeReferenceId = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTakeGroupLocation_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
        db.AddInParameter(dbCommand, "StockTakeGroupId", DbType.Int32, stockTakeGroupId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                string Error = db.ExecuteScalar(dbCommand).ToString();
                result = (Error == "" ? "Stocktake Group Location was added successfully." : Error);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
        }
        //connection.Close();

        return result;

    }
    #endregion StockTakeGroupLoc_Add

    #region StockTakeGroupLoc_Delete
    public static bool StockTakeGroupLoc_Delete(string connectionStringName, int stockTakeGroupLocId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTakeGroupLocation_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StockTakeGroupLocId", DbType.Int32, stockTakeGroupLocId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion StockTakeGroupLoc_Delete

    #region StockTake_Close
    public bool StockTake_Close(string connectionStringName,
        int warehouseId,
        string stockTakeType,
        DateTime startDate,
        DateTime endDate,
        int operatorId,
        int stockTakeReferenceId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockTake_Close";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        //db.AddInParameter(dbCommand, "StockTakeType", DbType.String, stockTakeType);
        //db.AddInParameter(dbCommand, "StartDate", DbType.DateTime, startDate);
        db.AddInParameter(dbCommand, "EndDate", DbType.DateTime, endDate);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "StockTakeReferenceId", DbType.Int32, stockTakeReferenceId);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = true;
            }
            catch (Exception ex) { }
        }
        //connection.Close();

        return result;

    }
    #endregion StockTake_Close

    #region GetAuthoriseAreas
    public DataSet GetAuthoriseAreas(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Area_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAuthoriseAreas

    #region ListStockTakes
    public DataSet ListStockTakes(string connectionStringName,
                                  DateTime FromDate,
                                  DateTime ToDate,
                                  int Location)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_List_Stock_Takes";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);
        db.AddInParameter(dbCommand, "Location", DbType.Int32, Location);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ListStockTakes

    #region CategoryList
    public DataSet CategoryList(string connectionStringName, int PrincipalId, string Category, bool ExactMatch)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Category_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "Category", DbType.String, Category);
        db.AddInParameter(dbCommand, "CategoryExactMatch", DbType.Boolean, ExactMatch);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion CategoryList
}
