using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Collections;

/// <summary>
/// Summary description for Area
/// </summary>
public class GridViewCommon
{
    #region Constructor Logic
    public GridViewCommon()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion Constructor Logic

    #region SelectAllCheckBox
    public GridView SelectAllCheckBox(GridView gridView, String checkBox)
    {
        try
        {
            CheckBox cb = new CheckBox();

            foreach (GridViewRow row in gridView.Rows)
            {
                cb = (CheckBox)row.FindControl(checkBox);

                cb.Checked = true;
            }

            return gridView;
        }
        catch
        {
            return null;
        }
    }
    #endregion SelectAllCheckBox

    //#region EditIndexesGet
    //protected void EditIndexesGet(GridView g, String rowList, String checkBox)
    //{
    //    try
    //    {
    //        CheckBox cb = new CheckBox();
    //        ArrayList arrayList = new ArrayList();

    //        foreach (GridViewRow row in g.Rows)
    //        {
    //            cb = (CheckBox)row.FindControl(checkBox);

    //            if (cb.Checked)
    //                arrayList.Add(row.RowIndex);
    //        }

    //        if (arrayList.Count > 0)
    //            Session[rowList] = arrayList;
    //    }
    //    catch { }
    //}
    //#endregion EditIndexesGet

    //#region EditIndexesSet
    //protected void EditIndexesSet(GridView gridView, ArrayList arrayList)
    //{
    //    try
    //    {
    //        if (arrayList.Count > 0)
    //        {
    //            gridView.SelectedIndex = (int)arrayList[0];
    //            gridView.EditIndex = (int)arrayList[0];

    //            arrayList.Remove(arrayList[0]);
    //        }
    //    }
    //    catch { }
    //}
    //#endregion EditIndexesSet
}
