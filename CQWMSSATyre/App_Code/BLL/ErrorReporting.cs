using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for ErrorReporting
/// </summary>
public class ErrorReporting
{
    public ErrorReporting()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    private string result = "";

    /// <summary>
    /// 
    /// </summary>
    /// <param name="priorityLevel"></param>
    /// <param name="module"></param>
    /// <param name="errpage"></param>
    /// <param name="errMethod"></param>
    /// <param name="errMsg"></param>
    /// <param name="createDate"></param>
    /// <returns></returns>
    public string SetupErrorReporting(string connectionStringName,
                                         int priorityLevel,
                                         string module,
                                         string errpage,
                                         string errMethod,
                                         string errMsg,
                                         DateTime createDate)
    {

        /*
         @ExceptionId int = null output,
         @ReceiptLineId int = null,
         @InstructionId int = null,
         @IssueLineId int = null,
         @ReasonId int = null,
         @Exception varchar(255) = null,
         @ExceptionCode varchar(10) = null,
         @CreateDate datetime = null 
         */
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        try
        {


            string sqlCommand = connectionStringName + ".dbo.p_ApplicationException_Insert";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "PriorityLevel", DbType.Int32, priorityLevel);
            db.AddInParameter(dbCommand, "Module", DbType.String, module);
            db.AddInParameter(dbCommand, "Page", DbType.String, errpage);
            db.AddInParameter(dbCommand, "Method", DbType.String, errMethod);
            db.AddInParameter(dbCommand, "ErrorMsg", DbType.String, errMsg);
            db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, createDate);

            // DataSet that will hold the returned results		
            //DataSet dataSet = null;

            //Execute command
            db.ExecuteNonQuery(dbCommand);


            //result = "Your File is updated successfully";
            //throw new System.Exception();  

            dbCommand.Dispose();
        }
        catch
        {
            result = "Error Reporting";
        }

        return result;
    }
}


