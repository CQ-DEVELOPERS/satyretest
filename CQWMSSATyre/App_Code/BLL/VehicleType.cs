﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Vehical Type
/// </summary>
public class VehicleType : BaseBLL
{
  public VehicleType()
  {
  }

  public DataSet GetVehicleTypes()
  {
      var data = ExecuteDataSet("p_VehicleType_Select");

    return data;
  }

  public DataSet GetVehicleTypes(string connectionStringName)
  {
      // Create the Database object, passing it the required database service.
      Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

      string sqlCommand = connectionStringName + ".dbo.p_VehicleType_Parameter";

      DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

      // DataSet that will hold the returned results
      DataSet dataSet = null;

      dataSet = db.ExecuteDataSet(dbCommand);

      return dataSet;
  }

  public int Update( int id, string vehicleTypeId, string name )
  {
    string sql = string.Format( "p_VehicleType_{0}", id > 0 ? "Update" : "Insert" );

    var idParam = new ParamItem( "Id", DbType.Int32, id, ParameterDirection.InputOutput );

    var idValue = ExecuteNonQuery( sql, idParam
                                      , new ParamItem( "VehicleTypeId", vehicleTypeId )
                                      , new ParamItem( "Name", name ) );

    return (int)idParam.Value;
  }

  /// <summary>
  /// Deletes the specified id.
  /// </summary>
  /// <param name="id">The id.</param>
  /// <param name="original_id">The original_id.</param>
  /// <returns></returns>
  public bool Delete( int id, int original_id )
  {
    try
    {
      ExecuteNonQuery( "p_VehicleType_Delete", new ParamItem( "Id", original_id ) );
    }
    catch ( Exception ex )
    {
      return false;
    }
    return true;
  }
}