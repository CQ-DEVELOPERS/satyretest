using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoAreaOperator
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:14:53
/// </summary>
/// <remarks>
///   Inserts a row into the AreaOperator table.
///   Selects rows from the AreaOperator table.
///   Searches for rows from the AreaOperator table.
///   Updates a rows in the AreaOperator table.
///   Deletes a row from the AreaOperator table.
/// </remarks>
/// <param>
public class StaticInfoAreaOperator
{
    #region "Constructor Logic"
    public StaticInfoAreaOperator()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetAreaOperator
    /// <summary>
    ///   Selects rows from the AreaOperator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="OperatorId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetAreaOperator
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 OperatorId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperator_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetAreaOperator
 
    #region DeleteAreaOperator
    /// <summary>
    ///   Deletes a row from the AreaOperator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="OperatorId"></param>
    /// <returns>bool</returns>
    public bool DeleteAreaOperator
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 OperatorId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperator_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteAreaOperator
 
    #region UpdateAreaOperator
    /// <summary>
    ///   Updates a row in the AreaOperator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="OperatorId"></param>
    /// <returns>bool</returns>
    public bool UpdateAreaOperator
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 OperatorId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperator_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateAreaOperator
 
    #region InsertAreaOperator
    /// <summary>
    ///   Inserts a row into the AreaOperator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="OperatorId"></param>
    /// <returns>bool</returns>
    public bool InsertAreaOperator
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 OperatorId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperator_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertAreaOperator
 
    #region SearchAreaOperator
    /// <summary>
    ///   Selects rows from the AreaOperator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="AreaId"></param>
    /// <param name="OperatorId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchAreaOperator
    (
        string connectionStringName,
        Int32 AreaId,
        Int32 OperatorId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperator_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, AreaId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchAreaOperator
 
    #region ListAreaOperator
    /// <summary>
    ///   Lists rows from the AreaOperator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListAreaOperator
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperator_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListAreaOperator
 
    #region ParameterAreaOperator
    /// <summary>
    ///   Lists rows from the AreaOperator table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterAreaOperator
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_AreaOperator_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterAreaOperator
}
