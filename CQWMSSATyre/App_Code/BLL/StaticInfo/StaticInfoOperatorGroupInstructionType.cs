using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoOperatorGroupInstructionType
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:23
/// </summary>
/// <remarks>
///   Inserts a row into the OperatorGroupInstructionType table.
///   Selects rows from the OperatorGroupInstructionType table.
///   Searches for rows from the OperatorGroupInstructionType table.
///   Updates a rows in the OperatorGroupInstructionType table.
///   Deletes a row from the OperatorGroupInstructionType table.
/// </remarks>
/// <param>
public class StaticInfoOperatorGroupInstructionType
{
    #region "Constructor Logic"
    public StaticInfoOperatorGroupInstructionType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetOperatorGroupInstructionType
    /// <summary>
    ///   Selects rows from the OperatorGroupInstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetOperatorGroupInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId,
        Int32 OperatorGroupId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupInstructionType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetOperatorGroupInstructionType
 
    #region DeleteOperatorGroupInstructionType
    /// <summary>
    ///   Deletes a row from the OperatorGroupInstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <returns>bool</returns>
    public bool DeleteOperatorGroupInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId,
        Int32 OperatorGroupId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupInstructionType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteOperatorGroupInstructionType
 
    #region UpdateOperatorGroupInstructionType
    /// <summary>
    ///   Updates a row in the OperatorGroupInstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="OrderBy"></param>
    /// <returns>bool</returns>
    public bool UpdateOperatorGroupInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId,
        Int32 OperatorGroupId,
        Int32 OrderBy 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupInstructionType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "OrderBy", DbType.Int32, OrderBy);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateOperatorGroupInstructionType
 
    #region InsertOperatorGroupInstructionType
    /// <summary>
    ///   Inserts a row into the OperatorGroupInstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="OrderBy"></param>
    /// <returns>bool</returns>
    public bool InsertOperatorGroupInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId,
        Int32 OperatorGroupId,
        Int32 OrderBy 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupInstructionType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "OrderBy", DbType.Int32, OrderBy);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertOperatorGroupInstructionType
 
    #region SearchOperatorGroupInstructionType
    /// <summary>
    ///   Selects rows from the OperatorGroupInstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionTypeId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchOperatorGroupInstructionType
    (
        string connectionStringName,
        Int32 InstructionTypeId,
        Int32 OperatorGroupId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupInstructionType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, InstructionTypeId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchOperatorGroupInstructionType
 
    #region ListOperatorGroupInstructionType
    /// <summary>
    ///   Lists rows from the OperatorGroupInstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListOperatorGroupInstructionType
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupInstructionType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListOperatorGroupInstructionType
 
    #region ParameterOperatorGroupInstructionType
    /// <summary>
    ///   Lists rows from the OperatorGroupInstructionType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterOperatorGroupInstructionType
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroupInstructionType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterOperatorGroupInstructionType
}
