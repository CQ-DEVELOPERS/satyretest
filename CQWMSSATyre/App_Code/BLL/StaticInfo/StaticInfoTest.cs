using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoTest
///   Create By      : Grant Schultz
///   Date Created   : 04 Oct 2012 15:03:31
/// </summary>
/// <remarks>
///   Inserts a row into the Test table.
///   Selects rows from the Test table.
///   Searches for rows from the Test table.
///   Updates a rows in the Test table.
///   Deletes a row from the Test table.
/// </remarks>
/// <param>
public class StaticInfoTest
{
    #region "Constructor Logic"
    public StaticInfoTest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetTest
    /// <summary>
    ///   Selects rows from the Test table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TestId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetTest
    (
        string connectionStringName,
        Int32 TestId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Test_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TestId", DbType.Int32, TestId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetTest
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Test_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetTest
 
    #region DeleteTest
    /// <summary>
    ///   Deletes a row from the Test table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TestId"></param>
    /// <returns>bool</returns>
    public bool DeleteTest
    (
        string connectionStringName,
        Int32 TestId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Test_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TestId", DbType.Int32, TestId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteTest
 
    #region UpdateTest
    /// <summary>
    ///   Updates a row in the Test table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TestId"></param>
    /// <param name="TestCode"></param>
    /// <param name="Test"></param>
    /// <returns>bool</returns>
    public bool UpdateTest
    (
        string connectionStringName,
        Int32 TestId,
        String TestCode,
        String Test 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Test_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TestId", DbType.Int32, TestId);
        db.AddInParameter(dbCommand, "TestCode", DbType.String, TestCode);
        db.AddInParameter(dbCommand, "Test", DbType.String, Test);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateTest
 
    #region InsertTest
    /// <summary>
    ///   Inserts a row into the Test table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TestId"></param>
    /// <param name="TestCode"></param>
    /// <param name="Test"></param>
    /// <returns>bool</returns>
    public bool InsertTest
    (
        string connectionStringName,
        Int32 TestId,
        String TestCode,
        String Test 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Test_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TestId", DbType.Int32, TestId);
        db.AddInParameter(dbCommand, "TestCode", DbType.String, TestCode);
        db.AddInParameter(dbCommand, "Test", DbType.String, Test);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertTest
 
    #region SearchTest
    /// <summary>
    ///   Selects rows from the Test table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TestId"></param>
    /// <param name="TestCode"></param>
    /// <param name="Test"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchTest
    (
        string connectionStringName,
        Int32 TestId,
        String TestCode,
        String Test 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Test_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TestId", DbType.Int32, TestId);
        db.AddInParameter(dbCommand, "TestCode", DbType.String, TestCode);
        db.AddInParameter(dbCommand, "Test", DbType.String, Test);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchTest
 
    #region PageSearchTest
    /// <summary>
    ///   Selects rows from the Test table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TestId"></param>
    /// <param name="TestCode"></param>
    /// <param name="Test"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchTest
    (
        string connectionStringName,
        Int32 TestId,
        String TestCode,
        String Test,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Test_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TestId", DbType.Int32, TestId);
        db.AddInParameter(dbCommand, "TestCode", DbType.String, TestCode);
        db.AddInParameter(dbCommand, "Test", DbType.String, Test);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchTest
 
    #region ListTest
    /// <summary>
    ///   Lists rows from the Test table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListTest
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Test_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListTest
 
    #region ParameterTest
    /// <summary>
    ///   Lists rows from the Test table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterTest
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Test_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterTest
}
