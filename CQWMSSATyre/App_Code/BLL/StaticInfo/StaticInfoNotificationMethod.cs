using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoNotificationMethod
///   Create By      : Grant Schultz
///   Date Created   : 16 Feb 2013 22:20:55
/// </summary>
/// <remarks>
///   Inserts a row into the NotificationMethod table.
///   Selects rows from the NotificationMethod table.
///   Searches for rows from the NotificationMethod table.
///   Updates a rows in the NotificationMethod table.
///   Deletes a row from the NotificationMethod table.
/// </remarks>
/// <param>
public class StaticInfoNotificationMethod
{
    #region "Constructor Logic"
    public StaticInfoNotificationMethod()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetNotificationMethod
    /// <summary>
    ///   Selects rows from the NotificationMethod table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NotificationMethodId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetNotificationMethod
    (
        string connectionStringName,
        Int32 NotificationMethodId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_NotificationMethod_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NotificationMethodId", DbType.Int32, NotificationMethodId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetNotificationMethod
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_NotificationMethod_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetNotificationMethod
 
    #region DeleteNotificationMethod
    /// <summary>
    ///   Deletes a row from the NotificationMethod table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NotificationMethodId"></param>
    /// <returns>bool</returns>
    public bool DeleteNotificationMethod
    (
        string connectionStringName,
        Int32 NotificationMethodId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_NotificationMethod_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NotificationMethodId", DbType.Int32, NotificationMethodId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteNotificationMethod
 
    #region UpdateNotificationMethod
    /// <summary>
    ///   Updates a row in the NotificationMethod table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NotificationMethodId"></param>
    /// <param name="NotificationMethodCode"></param>
    /// <param name="NotificationMethod"></param>
    /// <returns>bool</returns>
    public bool UpdateNotificationMethod
    (
        string connectionStringName,
        Int32 NotificationMethodId,
        String NotificationMethodCode,
        String NotificationMethod 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_NotificationMethod_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NotificationMethodId", DbType.Int32, NotificationMethodId);
        db.AddInParameter(dbCommand, "NotificationMethodCode", DbType.String, NotificationMethodCode);
        db.AddInParameter(dbCommand, "NotificationMethod", DbType.String, NotificationMethod);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateNotificationMethod
 
    #region InsertNotificationMethod
    /// <summary>
    ///   Inserts a row into the NotificationMethod table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NotificationMethodId"></param>
    /// <param name="NotificationMethodCode"></param>
    /// <param name="NotificationMethod"></param>
    /// <returns>bool</returns>
    public bool InsertNotificationMethod
    (
        string connectionStringName,
        Int32 NotificationMethodId,
        String NotificationMethodCode,
        String NotificationMethod 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_NotificationMethod_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NotificationMethodId", DbType.Int32, NotificationMethodId);
        db.AddInParameter(dbCommand, "NotificationMethodCode", DbType.String, NotificationMethodCode);
        db.AddInParameter(dbCommand, "NotificationMethod", DbType.String, NotificationMethod);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertNotificationMethod
 
    #region SearchNotificationMethod
    /// <summary>
    ///   Selects rows from the NotificationMethod table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NotificationMethodId"></param>
    /// <param name="NotificationMethodCode"></param>
    /// <param name="NotificationMethod"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchNotificationMethod
    (
        string connectionStringName,
        Int32 NotificationMethodId,
        String NotificationMethodCode,
        String NotificationMethod 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_NotificationMethod_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NotificationMethodId", DbType.Int32, NotificationMethodId);
        db.AddInParameter(dbCommand, "NotificationMethodCode", DbType.String, NotificationMethodCode);
        db.AddInParameter(dbCommand, "NotificationMethod", DbType.String, NotificationMethod);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchNotificationMethod
 
    #region PageSearchNotificationMethod
    /// <summary>
    ///   Selects rows from the NotificationMethod table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NotificationMethodId"></param>
    /// <param name="NotificationMethodCode"></param>
    /// <param name="NotificationMethod"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchNotificationMethod
    (
        string connectionStringName,
        Int32 NotificationMethodId,
        String NotificationMethodCode,
        String NotificationMethod,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_NotificationMethod_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NotificationMethodId", DbType.Int32, NotificationMethodId);
        db.AddInParameter(dbCommand, "NotificationMethodCode", DbType.String, NotificationMethodCode);
        db.AddInParameter(dbCommand, "NotificationMethod", DbType.String, NotificationMethod);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchNotificationMethod
 
    #region ListNotificationMethod
    /// <summary>
    ///   Lists rows from the NotificationMethod table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListNotificationMethod
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_NotificationMethod_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListNotificationMethod
 
    #region ParameterNotificationMethod
    /// <summary>
    ///   Lists rows from the NotificationMethod table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterNotificationMethod
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_NotificationMethod_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterNotificationMethod
}
