using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoReceiptLine
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:39
/// </summary>
/// <remarks>
///   Inserts a row into the ReceiptLine table.
///   Selects rows from the ReceiptLine table.
///   Searches for rows from the ReceiptLine table.
///   Updates a rows in the ReceiptLine table.
///   Deletes a row from the ReceiptLine table.
/// </remarks>
/// <param>
public class StaticInfoReceiptLine
{
    #region "Constructor Logic"
    public StaticInfoReceiptLine()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetReceiptLine
    /// <summary>
    ///   Selects rows from the ReceiptLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReceiptLineId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetReceiptLine
    (
        string connectionStringName,
        Int32 ReceiptLineId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReceiptLine_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetReceiptLine
 
    #region DeleteReceiptLine
    /// <summary>
    ///   Deletes a row from the ReceiptLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReceiptLineId"></param>
    /// <returns>bool</returns>
    public bool DeleteReceiptLine
    (
        string connectionStringName,
        Int32 ReceiptLineId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReceiptLine_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteReceiptLine
 
    #region UpdateReceiptLine
    /// <summary>
    ///   Updates a row in the ReceiptLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="ReceiptId"></param>
    /// <param name="InboundLineId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="RequiredQuantity"></param>
    /// <param name="ReceivedQuantity"></param>
    /// <param name="AcceptedQuantity"></param>
    /// <param name="RejectQuantity"></param>
    /// <param name="DeliveryNoteQuantity"></param>
    /// <returns>bool</returns>
    public bool UpdateReceiptLine
    (
        string connectionStringName,
        Int32 ReceiptLineId,
        Int32 ReceiptId,
        Int32 InboundLineId,
        Int32 StorageUnitBatchId,
        Int32 StatusId,
        Int32 OperatorId,
        Decimal RequiredQuantity,
        Decimal ReceivedQuantity,
        Decimal AcceptedQuantity,
        Decimal RejectQuantity,
        Decimal DeliveryNoteQuantity 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReceiptLine_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, InboundLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "RequiredQuantity", DbType.Decimal, RequiredQuantity);
        db.AddInParameter(dbCommand, "ReceivedQuantity", DbType.Decimal, ReceivedQuantity);
        db.AddInParameter(dbCommand, "AcceptedQuantity", DbType.Decimal, AcceptedQuantity);
        db.AddInParameter(dbCommand, "RejectQuantity", DbType.Decimal, RejectQuantity);
        db.AddInParameter(dbCommand, "DeliveryNoteQuantity", DbType.Decimal, DeliveryNoteQuantity);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateReceiptLine
 
    #region InsertReceiptLine
    /// <summary>
    ///   Inserts a row into the ReceiptLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="ReceiptId"></param>
    /// <param name="InboundLineId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="RequiredQuantity"></param>
    /// <param name="ReceivedQuantity"></param>
    /// <param name="AcceptedQuantity"></param>
    /// <param name="RejectQuantity"></param>
    /// <param name="DeliveryNoteQuantity"></param>
    /// <returns>bool</returns>
    public bool InsertReceiptLine
    (
        string connectionStringName,
        Int32 ReceiptLineId,
        Int32 ReceiptId,
        Int32 InboundLineId,
        Int32 StorageUnitBatchId,
        Int32 StatusId,
        Int32 OperatorId,
        Decimal RequiredQuantity,
        Decimal ReceivedQuantity,
        Decimal AcceptedQuantity,
        Decimal RejectQuantity,
        Decimal DeliveryNoteQuantity 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReceiptLine_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, InboundLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "RequiredQuantity", DbType.Decimal, RequiredQuantity);
        db.AddInParameter(dbCommand, "ReceivedQuantity", DbType.Decimal, ReceivedQuantity);
        db.AddInParameter(dbCommand, "AcceptedQuantity", DbType.Decimal, AcceptedQuantity);
        db.AddInParameter(dbCommand, "RejectQuantity", DbType.Decimal, RejectQuantity);
        db.AddInParameter(dbCommand, "DeliveryNoteQuantity", DbType.Decimal, DeliveryNoteQuantity);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertReceiptLine
 
    #region SearchReceiptLine
    /// <summary>
    ///   Selects rows from the ReceiptLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="ReceiptId"></param>
    /// <param name="InboundLineId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchReceiptLine
    (
        string connectionStringName,
        Int32 ReceiptLineId,
        Int32 ReceiptId,
        Int32 InboundLineId,
        Int32 StorageUnitBatchId,
        Int32 StatusId,
        Int32 OperatorId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReceiptLine_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, InboundLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchReceiptLine
 
    #region ListReceiptLine
    /// <summary>
    ///   Lists rows from the ReceiptLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListReceiptLine
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReceiptLine_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListReceiptLine
 
    #region ParameterReceiptLine
    /// <summary>
    ///   Lists rows from the ReceiptLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterReceiptLine
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ReceiptLine_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterReceiptLine
}
