using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInboundDocumentType
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:11:56
/// </summary>
/// <remarks>
///   Inserts a row into the InboundDocumentType table.
///   Selects rows from the InboundDocumentType table.
///   Searches for rows from the InboundDocumentType table.
///   Updates a rows in the InboundDocumentType table.
///   Deletes a row from the InboundDocumentType table.
/// </remarks>
/// <param>
public class StaticInfoInboundDocumentType
{
    #region "Constructor Logic"
    public StaticInfoInboundDocumentType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInboundDocumentType
    /// <summary>
    ///   Selects rows from the InboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInboundDocumentType
    (
        string connectionStringName,
        Int32 InboundDocumentTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocumentType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetInboundDocumentType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocumentType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInboundDocumentType
 
    #region DeleteInboundDocumentType
    /// <summary>
    ///   Deletes a row from the InboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteInboundDocumentType
    (
        string connectionStringName,
        Int32 InboundDocumentTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocumentType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInboundDocumentType
 
    #region UpdateInboundDocumentType
    /// <summary>
    ///   Updates a row in the InboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="InboundDocumentTypeCode"></param>
    /// <param name="InboundDocumentType"></param>
    /// <param name="OverReceiveIndicator"></param>
    /// <param name="AllowManualCreate"></param>
    /// <param name="QuantityToFollowIndicator"></param>
    /// <param name="PriorityId"></param>
    /// <param name="AutoSendup"></param>
    /// <param name="BatchButton"></param>
    /// <param name="BatchSelect"></param>
    /// <param name="DeliveryNoteNumber"></param>
    /// <param name="VehicleRegistration"></param>
    /// <param name="AreaType"></param>
    /// <returns>bool</returns>
    public bool UpdateInboundDocumentType
    (
        string connectionStringName,
        Int32 InboundDocumentTypeId,
        String InboundDocumentTypeCode,
        String InboundDocumentType,
        Boolean OverReceiveIndicator,
        Boolean AllowManualCreate,
        Boolean QuantityToFollowIndicator,
        Int32 PriorityId,
        Boolean AutoSendup,
        Boolean BatchButton,
        Boolean BatchSelect,
        Boolean DeliveryNoteNumber,
        Boolean VehicleRegistration,
        String AreaType 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocumentType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeCode", DbType.String, InboundDocumentTypeCode);
        db.AddInParameter(dbCommand, "InboundDocumentType", DbType.String, InboundDocumentType);
        db.AddInParameter(dbCommand, "OverReceiveIndicator", DbType.Boolean, OverReceiveIndicator);
        db.AddInParameter(dbCommand, "AllowManualCreate", DbType.Boolean, AllowManualCreate);
        db.AddInParameter(dbCommand, "QuantityToFollowIndicator", DbType.Boolean, QuantityToFollowIndicator);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "AutoSendup", DbType.Boolean, AutoSendup);
        db.AddInParameter(dbCommand, "BatchButton", DbType.Boolean, BatchButton);
        db.AddInParameter(dbCommand, "BatchSelect", DbType.Boolean, BatchSelect);
        db.AddInParameter(dbCommand, "DeliveryNoteNumber", DbType.Boolean, DeliveryNoteNumber);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.Boolean, VehicleRegistration);
        db.AddInParameter(dbCommand, "AreaType", DbType.String, AreaType);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInboundDocumentType
 
    #region InsertInboundDocumentType
    /// <summary>
    ///   Inserts a row into the InboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="InboundDocumentTypeCode"></param>
    /// <param name="InboundDocumentType"></param>
    /// <param name="OverReceiveIndicator"></param>
    /// <param name="AllowManualCreate"></param>
    /// <param name="QuantityToFollowIndicator"></param>
    /// <param name="PriorityId"></param>
    /// <param name="AutoSendup"></param>
    /// <param name="BatchButton"></param>
    /// <param name="BatchSelect"></param>
    /// <param name="DeliveryNoteNumber"></param>
    /// <param name="VehicleRegistration"></param>
    /// <param name="AreaType"></param>
    /// <returns>bool</returns>
    public bool InsertInboundDocumentType
    (
        string connectionStringName,
        Int32 InboundDocumentTypeId,
        String InboundDocumentTypeCode,
        String InboundDocumentType,
        Boolean OverReceiveIndicator,
        Boolean AllowManualCreate,
        Boolean QuantityToFollowIndicator,
        Int32 PriorityId,
        Boolean AutoSendup,
        Boolean BatchButton,
        Boolean BatchSelect,
        Boolean DeliveryNoteNumber,
        Boolean VehicleRegistration,
        String AreaType 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocumentType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeCode", DbType.String, InboundDocumentTypeCode);
        db.AddInParameter(dbCommand, "InboundDocumentType", DbType.String, InboundDocumentType);
        db.AddInParameter(dbCommand, "OverReceiveIndicator", DbType.Boolean, OverReceiveIndicator);
        db.AddInParameter(dbCommand, "AllowManualCreate", DbType.Boolean, AllowManualCreate);
        db.AddInParameter(dbCommand, "QuantityToFollowIndicator", DbType.Boolean, QuantityToFollowIndicator);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "AutoSendup", DbType.Boolean, AutoSendup);
        db.AddInParameter(dbCommand, "BatchButton", DbType.Boolean, BatchButton);
        db.AddInParameter(dbCommand, "BatchSelect", DbType.Boolean, BatchSelect);
        db.AddInParameter(dbCommand, "DeliveryNoteNumber", DbType.Boolean, DeliveryNoteNumber);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.Boolean, VehicleRegistration);
        db.AddInParameter(dbCommand, "AreaType", DbType.String, AreaType);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInboundDocumentType
 
    #region SearchInboundDocumentType
    /// <summary>
    ///   Selects rows from the InboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="InboundDocumentTypeCode"></param>
    /// <param name="InboundDocumentType"></param>
    /// <param name="PriorityId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInboundDocumentType
    (
        string connectionStringName,
        Int32 InboundDocumentTypeId,
        String InboundDocumentTypeCode,
        String InboundDocumentType,
        Int32 PriorityId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocumentType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeCode", DbType.String, InboundDocumentTypeCode);
        db.AddInParameter(dbCommand, "InboundDocumentType", DbType.String, InboundDocumentType);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInboundDocumentType
 
    #region PageSearchInboundDocumentType
    /// <summary>
    ///   Selects rows from the InboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="InboundDocumentTypeCode"></param>
    /// <param name="InboundDocumentType"></param>
    /// <param name="PriorityId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchInboundDocumentType
    (
        string connectionStringName,
        Int32 InboundDocumentTypeId,
        String InboundDocumentTypeCode,
        String InboundDocumentType,
        Int32 PriorityId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocumentType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeCode", DbType.String, InboundDocumentTypeCode);
        db.AddInParameter(dbCommand, "InboundDocumentType", DbType.String, InboundDocumentType);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchInboundDocumentType
 
    #region ListInboundDocumentType
    /// <summary>
    ///   Lists rows from the InboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInboundDocumentType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocumentType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInboundDocumentType
 
    #region ParameterInboundDocumentType
    /// <summary>
    ///   Lists rows from the InboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInboundDocumentType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundDocumentType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInboundDocumentType
}
