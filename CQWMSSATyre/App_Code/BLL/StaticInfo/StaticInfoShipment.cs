using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoShipment
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:42
/// </summary>
/// <remarks>
///   Inserts a row into the Shipment table.
///   Selects rows from the Shipment table.
///   Searches for rows from the Shipment table.
///   Updates a rows in the Shipment table.
///   Deletes a row from the Shipment table.
/// </remarks>
/// <param>
public class StaticInfoShipment
{
    #region "Constructor Logic"
    public StaticInfoShipment()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetShipment
    /// <summary>
    ///   Selects rows from the Shipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShipmentId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetShipment
    (
        string connectionStringName,
        Int32 ShipmentId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shipment_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, ShipmentId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetShipment
 
    #region DeleteShipment
    /// <summary>
    ///   Deletes a row from the Shipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShipmentId"></param>
    /// <returns>bool</returns>
    public bool DeleteShipment
    (
        string connectionStringName,
        Int32 ShipmentId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shipment_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, ShipmentId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteShipment
 
    #region UpdateShipment
    /// <summary>
    ///   Updates a row in the Shipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShipmentId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <param name="ShipmentDate"></param>
    /// <param name="Remarks"></param>
    /// <param name="SealNumber"></param>
    /// <param name="VehicleRegistration"></param>
    /// <param name="Route"></param>
    /// <returns>bool</returns>
    public bool UpdateShipment
    (
        string connectionStringName,
        Int32 ShipmentId,
        Int32 StatusId,
        Int32 WarehouseId,
        Int32 LocationId,
        DateTime ShipmentDate,
        String Remarks,
        String SealNumber,
        String VehicleRegistration,
        String Route 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shipment_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, ShipmentId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "ShipmentDate", DbType.DateTime, ShipmentDate);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
        db.AddInParameter(dbCommand, "SealNumber", DbType.String, SealNumber);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.String, VehicleRegistration);
        db.AddInParameter(dbCommand, "Route", DbType.String, Route);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateShipment
 
    #region InsertShipment
    /// <summary>
    ///   Inserts a row into the Shipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShipmentId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <param name="ShipmentDate"></param>
    /// <param name="Remarks"></param>
    /// <param name="SealNumber"></param>
    /// <param name="VehicleRegistration"></param>
    /// <param name="Route"></param>
    /// <returns>bool</returns>
    public bool InsertShipment
    (
        string connectionStringName,
        Int32 ShipmentId,
        Int32 StatusId,
        Int32 WarehouseId,
        Int32 LocationId,
        DateTime ShipmentDate,
        String Remarks,
        String SealNumber,
        String VehicleRegistration,
        String Route 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shipment_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, ShipmentId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "ShipmentDate", DbType.DateTime, ShipmentDate);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
        db.AddInParameter(dbCommand, "SealNumber", DbType.String, SealNumber);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.String, VehicleRegistration);
        db.AddInParameter(dbCommand, "Route", DbType.String, Route);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertShipment
 
    #region SearchShipment
    /// <summary>
    ///   Selects rows from the Shipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShipmentId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchShipment
    (
        string connectionStringName,
        Int32 ShipmentId,
        Int32 StatusId,
        Int32 WarehouseId,
        Int32 LocationId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shipment_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, ShipmentId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchShipment
 
    #region ListShipment
    /// <summary>
    ///   Lists rows from the Shipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListShipment
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shipment_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListShipment
 
    #region ParameterShipment
    /// <summary>
    ///   Lists rows from the Shipment table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterShipment
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shipment_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterShipment
}
