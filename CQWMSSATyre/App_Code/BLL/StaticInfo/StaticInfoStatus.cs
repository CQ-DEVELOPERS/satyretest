using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoStatus
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:25
/// </summary>
/// <remarks>
///   Inserts a row into the Status table.
///   Selects rows from the Status table.
///   Searches for rows from the Status table.
///   Updates a rows in the Status table.
///   Deletes a row from the Status table.
/// </remarks>
/// <param>
public class StaticInfoStatus
{
    #region "Constructor Logic"
    public StaticInfoStatus()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetStatus
    /// <summary>
    ///   Selects rows from the Status table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StatusId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetStatus
    (
        string connectionStringName,
        Int32 StatusId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Status_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetStatus
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Status_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetStatus
 
    #region DeleteStatus
    /// <summary>
    ///   Deletes a row from the Status table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StatusId"></param>
    /// <returns>bool</returns>
    public bool DeleteStatus
    (
        string connectionStringName,
        Int32 StatusId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Status_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteStatus
 
    #region UpdateStatus
    /// <summary>
    ///   Updates a row in the Status table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StatusId"></param>
    /// <param name="Status"></param>
    /// <param name="StatusCode"></param>
    /// <param name="Type"></param>
    /// <param name="OrderBy"></param>
    /// <returns>bool</returns>
    public bool UpdateStatus
    (
        string connectionStringName,
        Int32 StatusId,
        String Status,
        String StatusCode,
        String Type,
        Int32 OrderBy 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Status_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "Status", DbType.String, Status);
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, StatusCode);
        db.AddInParameter(dbCommand, "Type", DbType.String, Type);
        db.AddInParameter(dbCommand, "OrderBy", DbType.Int32, OrderBy);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateStatus
 
    #region InsertStatus
    /// <summary>
    ///   Inserts a row into the Status table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StatusId"></param>
    /// <param name="Status"></param>
    /// <param name="StatusCode"></param>
    /// <param name="Type"></param>
    /// <param name="OrderBy"></param>
    /// <returns>bool</returns>
    public bool InsertStatus
    (
        string connectionStringName,
        Int32 StatusId,
        String Status,
        String StatusCode,
        String Type,
        Int32 OrderBy 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Status_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "Status", DbType.String, Status);
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, StatusCode);
        db.AddInParameter(dbCommand, "Type", DbType.String, Type);
        db.AddInParameter(dbCommand, "OrderBy", DbType.Int32, OrderBy);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertStatus
 
    #region SearchStatus
    /// <summary>
    ///   Selects rows from the Status table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StatusId"></param>
    /// <param name="Status"></param>
    /// <param name="StatusCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchStatus
    (
        string connectionStringName,
        Int32 StatusId,
        String Status,
        String StatusCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Status_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "Status", DbType.String, Status);
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, StatusCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchStatus
 
    #region PageSearchStatus
    /// <summary>
    ///   Selects rows from the Status table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StatusId"></param>
    /// <param name="Status"></param>
    /// <param name="StatusCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchStatus
    (
        string connectionStringName,
        Int32 StatusId,
        String Status,
        String StatusCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Status_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "Status", DbType.String, Status);
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, StatusCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchStatus
 
    #region ListStatus
    /// <summary>
    ///   Lists rows from the Status table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListStatus
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Status_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListStatus
 
    #region ParameterStatus
    /// <summary>
    ///   Lists rows from the Status table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterStatus
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Status_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterStatus
}
