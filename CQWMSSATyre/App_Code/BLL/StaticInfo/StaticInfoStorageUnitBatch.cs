using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoStorageUnitBatch
///   Create By      : Grant Schultz
///   Date Created   : 05 Jul 2012 16:59:19
/// </summary>
/// <remarks>
///   Inserts a row into the StorageUnitBatch table.
///   Selects rows from the StorageUnitBatch table.
///   Searches for rows from the StorageUnitBatch table.
///   Updates a rows in the StorageUnitBatch table.
///   Deletes a row from the StorageUnitBatch table.
/// </remarks>
/// <param>
public class StaticInfoStorageUnitBatch
{
    #region "Constructor Logic"
    public StaticInfoStorageUnitBatch()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetStorageUnitBatch
    /// <summary>
    ///   Selects rows from the StorageUnitBatch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetStorageUnitBatch
    (
        string connectionStringName,
        Int32 StorageUnitBatchId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetStorageUnitBatch
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetStorageUnitBatch
 
    #region DeleteStorageUnitBatch
    /// <summary>
    ///   Deletes a row from the StorageUnitBatch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <returns>bool</returns>
    public bool DeleteStorageUnitBatch
    (
        string connectionStringName,
        Int32 StorageUnitBatchId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteStorageUnitBatch
 
    #region UpdateStorageUnitBatch
    /// <summary>
    ///   Updates a row in the StorageUnitBatch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="BatchId"></param>
    /// <param name="StorageUnitId"></param>
    /// <returns>bool</returns>
    public bool UpdateStorageUnitBatch
    (
        string connectionStringName,
        Int32 StorageUnitBatchId,
        Int32 BatchId,
        Int32 StorageUnitId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateStorageUnitBatch
 
    #region InsertStorageUnitBatch
    /// <summary>
    ///   Inserts a row into the StorageUnitBatch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="BatchId"></param>
    /// <param name="StorageUnitId"></param>
    /// <returns>bool</returns>
    public bool InsertStorageUnitBatch
    (
        string connectionStringName,
        Int32 StorageUnitBatchId,
        Int32 BatchId,
        Int32 StorageUnitId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertStorageUnitBatch
 
    #region SearchStorageUnitBatch
    /// <summary>
    ///   Selects rows from the StorageUnitBatch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="BatchId"></param>
    /// <param name="StorageUnitId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchStorageUnitBatch
    (
        string connectionStringName,
        Int32 StorageUnitBatchId,
        Int32 BatchId,
        Int32 StorageUnitId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchStorageUnitBatch
 
    #region PageSearchStorageUnitBatch
    /// <summary>
    ///   Selects rows from the StorageUnitBatch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="BatchId"></param>
    /// <param name="StorageUnitId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchStorageUnitBatch
    (
        string connectionStringName,
        Int32 StorageUnitBatchId,
        Int32 BatchId,
        Int32 StorageUnitId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchStorageUnitBatch
 
    #region ListStorageUnitBatch
    /// <summary>
    ///   Lists rows from the StorageUnitBatch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListStorageUnitBatch
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListStorageUnitBatch
 
    #region ParameterStorageUnitBatch
    /// <summary>
    ///   Lists rows from the StorageUnitBatch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterStorageUnitBatch
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterStorageUnitBatch
}
