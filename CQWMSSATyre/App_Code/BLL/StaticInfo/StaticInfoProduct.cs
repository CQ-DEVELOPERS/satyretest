using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoProduct
///   Create By      : Grant Schultz
///   Date Created   : 14 May 2014 16:58:11
/// </summary>
/// <remarks>
///   Inserts a row into the Product table.
///   Selects rows from the Product table.
///   Searches for rows from the Product table.
///   Updates a rows in the Product table.
///   Deletes a row from the Product table.
/// </remarks>
/// <param>
public class StaticInfoProduct
{
    #region "Constructor Logic"
    public StaticInfoProduct()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetProduct
    /// <summary>
    ///   Selects rows from the Product table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProductId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetProduct
    (
        string connectionStringName,
        Int32 ProductId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Product_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, ProductId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetProduct
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Product_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetProduct
 
    #region DeleteProduct
    /// <summary>
    ///   Deletes a row from the Product table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProductId"></param>
    /// <returns>bool</returns>
    public bool DeleteProduct
    (
        string connectionStringName,
        Int32 ProductId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Product_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, ProductId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteProduct
 
    #region UpdateProduct
    /// <summary>
    ///   Updates a row in the Product table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProductId"></param>
    /// <param name="StatusId"></param>
    /// <param name="ProductCode"></param>
    /// <param name="Product"></param>
    /// <param name="Barcode"></param>
    /// <param name="MinimumQuantity"></param>
    /// <param name="ReorderQuantity"></param>
    /// <param name="MaximumQuantity"></param>
    /// <param name="CuringPeriodDays"></param>
    /// <param name="ShelfLifeDays"></param>
    /// <param name="QualityAssuranceIndicator"></param>
    /// <param name="ProductType"></param>
    /// <param name="OverReceipt"></param>
    /// <param name="HostId"></param>
    /// <param name="RetentionSamples"></param>
    /// <param name="Samples"></param>
    /// <param name="ParentProductCode"></param>
    /// <param name="DangerousGoodsId"></param>
    /// <param name="Description2"></param>
    /// <param name="Description3"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="AssaySamples"></param>
    /// <param name="Category"></param>
    /// <param name="ProductCategoryId"></param>
    /// <param name="ProductAlias"></param>
    /// <returns>bool</returns>
    public bool UpdateProduct
    (
        string connectionStringName,
        Int32 ProductId,
        Int32 StatusId,
        String ProductCode,
        String Product,
        String Barcode,
        Double MinimumQuantity,
        Double ReorderQuantity,
        Double MaximumQuantity,
        Int32 CuringPeriodDays,
        Int32 ShelfLifeDays,
        Boolean QualityAssuranceIndicator,
        String ProductType,
        Decimal OverReceipt,
        String HostId,
        Int32 RetentionSamples,
        Int32 Samples,
        String ParentProductCode,
        Int32 DangerousGoodsId,
        String Description2,
        String Description3,
        Int32 PrincipalId,
        Double AssaySamples,
        String Category,
        Int32 ProductCategoryId,
        String ProductAlias 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Product_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, ProductId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, ProductCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, Product);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, Barcode);
        db.AddInParameter(dbCommand, "MinimumQuantity", DbType.Double, MinimumQuantity);
        db.AddInParameter(dbCommand, "ReorderQuantity", DbType.Double, ReorderQuantity);
        db.AddInParameter(dbCommand, "MaximumQuantity", DbType.Double, MaximumQuantity);
        db.AddInParameter(dbCommand, "CuringPeriodDays", DbType.Int32, CuringPeriodDays);
        db.AddInParameter(dbCommand, "ShelfLifeDays", DbType.Int32, ShelfLifeDays);
        db.AddInParameter(dbCommand, "QualityAssuranceIndicator", DbType.Boolean, QualityAssuranceIndicator);
        db.AddInParameter(dbCommand, "ProductType", DbType.String, ProductType);
        db.AddInParameter(dbCommand, "OverReceipt", DbType.Decimal, OverReceipt);
        db.AddInParameter(dbCommand, "HostId", DbType.String, HostId);
        db.AddInParameter(dbCommand, "RetentionSamples", DbType.Int32, RetentionSamples);
        db.AddInParameter(dbCommand, "Samples", DbType.Int32, Samples);
        db.AddInParameter(dbCommand, "ParentProductCode", DbType.String, ParentProductCode);
        db.AddInParameter(dbCommand, "DangerousGoodsId", DbType.Int32, DangerousGoodsId);
        db.AddInParameter(dbCommand, "Description2", DbType.String, Description2);
        db.AddInParameter(dbCommand, "Description3", DbType.String, Description3);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "AssaySamples", DbType.Double, AssaySamples);
        db.AddInParameter(dbCommand, "Category", DbType.String, Category);
        db.AddInParameter(dbCommand, "ProductCategoryId", DbType.Int32, ProductCategoryId);
        db.AddInParameter(dbCommand, "ProductAlias", DbType.String, ProductAlias);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateProduct
 
    #region InsertProduct
    /// <summary>
    ///   Inserts a row into the Product table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProductId"></param>
    /// <param name="StatusId"></param>
    /// <param name="ProductCode"></param>
    /// <param name="Product"></param>
    /// <param name="Barcode"></param>
    /// <param name="MinimumQuantity"></param>
    /// <param name="ReorderQuantity"></param>
    /// <param name="MaximumQuantity"></param>
    /// <param name="CuringPeriodDays"></param>
    /// <param name="ShelfLifeDays"></param>
    /// <param name="QualityAssuranceIndicator"></param>
    /// <param name="ProductType"></param>
    /// <param name="OverReceipt"></param>
    /// <param name="HostId"></param>
    /// <param name="RetentionSamples"></param>
    /// <param name="Samples"></param>
    /// <param name="ParentProductCode"></param>
    /// <param name="DangerousGoodsId"></param>
    /// <param name="Description2"></param>
    /// <param name="Description3"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="AssaySamples"></param>
    /// <param name="Category"></param>
    /// <param name="ProductCategoryId"></param>
    /// <param name="ProductAlias"></param>
    /// <returns>bool</returns>
    public bool InsertProduct
    (
        string connectionStringName,
        Int32 ProductId,
        Int32 StatusId,
        String ProductCode,
        String Product,
        String Barcode,
        Double MinimumQuantity,
        Double ReorderQuantity,
        Double MaximumQuantity,
        Int32 CuringPeriodDays,
        Int32 ShelfLifeDays,
        Boolean QualityAssuranceIndicator,
        String ProductType,
        Decimal OverReceipt,
        String HostId,
        Int32 RetentionSamples,
        Int32 Samples,
        String ParentProductCode,
        Int32 DangerousGoodsId,
        String Description2,
        String Description3,
        Int32 PrincipalId,
        Double AssaySamples,
        String Category,
        Int32 ProductCategoryId,
        String ProductAlias 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Product_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, ProductId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, ProductCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, Product);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, Barcode);
        db.AddInParameter(dbCommand, "MinimumQuantity", DbType.Double, MinimumQuantity);
        db.AddInParameter(dbCommand, "ReorderQuantity", DbType.Double, ReorderQuantity);
        db.AddInParameter(dbCommand, "MaximumQuantity", DbType.Double, MaximumQuantity);
        db.AddInParameter(dbCommand, "CuringPeriodDays", DbType.Int32, CuringPeriodDays);
        db.AddInParameter(dbCommand, "ShelfLifeDays", DbType.Int32, ShelfLifeDays);
        db.AddInParameter(dbCommand, "QualityAssuranceIndicator", DbType.Boolean, QualityAssuranceIndicator);
        db.AddInParameter(dbCommand, "ProductType", DbType.String, ProductType);
        db.AddInParameter(dbCommand, "OverReceipt", DbType.Decimal, OverReceipt);
        db.AddInParameter(dbCommand, "HostId", DbType.String, HostId);
        db.AddInParameter(dbCommand, "RetentionSamples", DbType.Int32, RetentionSamples);
        db.AddInParameter(dbCommand, "Samples", DbType.Int32, Samples);
        db.AddInParameter(dbCommand, "ParentProductCode", DbType.String, ParentProductCode);
        db.AddInParameter(dbCommand, "DangerousGoodsId", DbType.Int32, DangerousGoodsId);
        db.AddInParameter(dbCommand, "Description2", DbType.String, Description2);
        db.AddInParameter(dbCommand, "Description3", DbType.String, Description3);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "AssaySamples", DbType.Double, AssaySamples);
        db.AddInParameter(dbCommand, "Category", DbType.String, Category);
        db.AddInParameter(dbCommand, "ProductCategoryId", DbType.Int32, ProductCategoryId);
        db.AddInParameter(dbCommand, "ProductAlias", DbType.String, ProductAlias);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertProduct
 
    #region SearchProduct
    /// <summary>
    ///   Selects rows from the Product table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProductId"></param>
    /// <param name="StatusId"></param>
    /// <param name="ProductCode"></param>
    /// <param name="Product"></param>
    /// <param name="DangerousGoodsId"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="ProductCategoryId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchProduct
    (
        string connectionStringName,
        Int32 ProductId,
        Int32 StatusId,
        String ProductCode,
        String Product,
        Int32 DangerousGoodsId,
        Int32 PrincipalId,
        Int32 ProductCategoryId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Product_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, ProductId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, ProductCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, Product);
        db.AddInParameter(dbCommand, "DangerousGoodsId", DbType.Int32, DangerousGoodsId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "ProductCategoryId", DbType.Int32, ProductCategoryId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchProduct
 
    #region PageSearchProduct
    /// <summary>
    ///   Selects rows from the Product table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProductId"></param>
    /// <param name="StatusId"></param>
    /// <param name="ProductCode"></param>
    /// <param name="Product"></param>
    /// <param name="DangerousGoodsId"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="ProductCategoryId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchProduct
    (
        string connectionStringName,
        Int32 ProductId,
        Int32 StatusId,
        String ProductCode,
        String Product,
        Int32 DangerousGoodsId,
        Int32 PrincipalId,
        Int32 ProductCategoryId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Product_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, ProductId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, ProductCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, Product);
        db.AddInParameter(dbCommand, "DangerousGoodsId", DbType.Int32, DangerousGoodsId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "ProductCategoryId", DbType.Int32, ProductCategoryId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchProduct
 
    #region ListProduct
    /// <summary>
    ///   Lists rows from the Product table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListProduct
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Product_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListProduct
 
    #region ParameterProduct
    /// <summary>
    ///   Lists rows from the Product table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterProduct
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Product_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterProduct
}
