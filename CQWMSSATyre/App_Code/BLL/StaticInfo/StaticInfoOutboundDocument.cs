using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoOutboundDocument
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:25
/// </summary>
/// <remarks>
///   Inserts a row into the OutboundDocument table.
///   Selects rows from the OutboundDocument table.
///   Searches for rows from the OutboundDocument table.
///   Updates a rows in the OutboundDocument table.
///   Deletes a row from the OutboundDocument table.
/// </remarks>
/// <param>
public class StaticInfoOutboundDocument
{
    #region "Constructor Logic"
    public StaticInfoOutboundDocument()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetOutboundDocument
    /// <summary>
    ///   Selects rows from the OutboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetOutboundDocument
    (
        string connectionStringName,
        Int32 OutboundDocumentId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocument_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetOutboundDocument
 
    #region DeleteOutboundDocument
    /// <summary>
    ///   Deletes a row from the OutboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <returns>bool</returns>
    public bool DeleteOutboundDocument
    (
        string connectionStringName,
        Int32 OutboundDocumentId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocument_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteOutboundDocument
 
    #region UpdateOutboundDocument
    /// <summary>
    ///   Updates a row in the OutboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="DeliveryDate"></param>
    /// <param name="CreateDate"></param>
    /// <param name="ModifiedDate"></param>
    /// <returns>bool</returns>
    public bool UpdateOutboundDocument
    (
        string connectionStringName,
        Int32 OutboundDocumentId,
        Int32 OutboundDocumentTypeId,
        Int32 ExternalCompanyId,
        Int32 StatusId,
        Int32 WarehouseId,
        String OrderNumber,
        DateTime DeliveryDate,
        DateTime CreateDate,
        DateTime ModifiedDate 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocument_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, DeliveryDate);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "ModifiedDate", DbType.DateTime, ModifiedDate);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateOutboundDocument
 
    #region InsertOutboundDocument
    /// <summary>
    ///   Inserts a row into the OutboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="DeliveryDate"></param>
    /// <param name="CreateDate"></param>
    /// <param name="ModifiedDate"></param>
    /// <returns>bool</returns>
    public bool InsertOutboundDocument
    (
        string connectionStringName,
        Int32 OutboundDocumentId,
        Int32 OutboundDocumentTypeId,
        Int32 ExternalCompanyId,
        Int32 StatusId,
        Int32 WarehouseId,
        String OrderNumber,
        DateTime DeliveryDate,
        DateTime CreateDate,
        DateTime ModifiedDate 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocument_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, DeliveryDate);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "ModifiedDate", DbType.DateTime, ModifiedDate);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertOutboundDocument
 
    #region SearchOutboundDocument
    /// <summary>
    ///   Selects rows from the OutboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchOutboundDocument
    (
        string connectionStringName,
        Int32 OutboundDocumentId,
        Int32 OutboundDocumentTypeId,
        Int32 ExternalCompanyId,
        Int32 StatusId,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocument_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchOutboundDocument
 
    #region ListOutboundDocument
    /// <summary>
    ///   Lists rows from the OutboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListOutboundDocument
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocument_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListOutboundDocument
 
    #region ParameterOutboundDocument
    /// <summary>
    ///   Lists rows from the OutboundDocument table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterOutboundDocument
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocument_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterOutboundDocument
}
