using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInterfaceTables
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:03
/// </summary>
/// <remarks>
///   Inserts a row into the InterfaceTables table.
///   Selects rows from the InterfaceTables table.
///   Searches for rows from the InterfaceTables table.
///   Updates a rows in the InterfaceTables table.
///   Deletes a row from the InterfaceTables table.
/// </remarks>
/// <param>
public class StaticInfoInterfaceTables
{
    #region "Constructor Logic"
    public StaticInfoInterfaceTables()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInterfaceTables
    /// <summary>
    ///   Selects rows from the InterfaceTables table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceTableId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInterfaceTables
    (
        string connectionStringName,
        Int32 InterfaceTableId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceTables_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceTableId", DbType.Int32, InterfaceTableId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetInterfaceTables
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceTables_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInterfaceTables
 
    #region DeleteInterfaceTables
    /// <summary>
    ///   Deletes a row from the InterfaceTables table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceTableId"></param>
    /// <returns>bool</returns>
    public bool DeleteInterfaceTables
    (
        string connectionStringName,
        Int32 InterfaceTableId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceTables_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceTableId", DbType.Int32, InterfaceTableId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInterfaceTables
 
    #region UpdateInterfaceTables
    /// <summary>
    ///   Updates a row in the InterfaceTables table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceTableId"></param>
    /// <param name="HeaderTable"></param>
    /// <param name="DetailTable"></param>
    /// <param name="HeaderTableKey"></param>
    /// <param name="Description1"></param>
    /// <param name="Description2"></param>
    /// <param name="HeaderField1"></param>
    /// <param name="HeaderField2"></param>
    /// <param name="HeaderField3"></param>
    /// <param name="HeaderField4"></param>
    /// <param name="HeaderField5"></param>
    /// <param name="HeaderField6"></param>
    /// <param name="HeaderField7"></param>
    /// <param name="HeaderField8"></param>
    /// <param name="DetailField1"></param>
    /// <param name="DetailField2"></param>
    /// <param name="DetailField3"></param>
    /// <param name="DetailField4"></param>
    /// <param name="DetailField5"></param>
    /// <param name="DetailField6"></param>
    /// <param name="DetailField7"></param>
    /// <param name="DetailField8"></param>
    /// <param name="StartId"></param>
    /// <param name="InsertDetail"></param>
    /// <param name="GroupByKey"></param>
    /// <param name="AltGroupByKey"></param>
    /// <returns>bool</returns>
    public bool UpdateInterfaceTables
    (
        string connectionStringName,
        Int32 InterfaceTableId,
        String HeaderTable,
        String DetailTable,
        String HeaderTableKey,
        String Description1,
        String Description2,
        String HeaderField1,
        String HeaderField2,
        String HeaderField3,
        String HeaderField4,
        String HeaderField5,
        String HeaderField6,
        String HeaderField7,
        String HeaderField8,
        String DetailField1,
        String DetailField2,
        String DetailField3,
        String DetailField4,
        String DetailField5,
        String DetailField6,
        String DetailField7,
        String DetailField8,
        Int32 StartId,
        Int32 InsertDetail,
        String GroupByKey,
        String AltGroupByKey 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceTables_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceTableId", DbType.Int32, InterfaceTableId);
        db.AddInParameter(dbCommand, "HeaderTable", DbType.String, HeaderTable);
        db.AddInParameter(dbCommand, "DetailTable", DbType.String, DetailTable);
        db.AddInParameter(dbCommand, "HeaderTableKey", DbType.String, HeaderTableKey);
        db.AddInParameter(dbCommand, "Description1", DbType.String, Description1);
        db.AddInParameter(dbCommand, "Description2", DbType.String, Description2);
        db.AddInParameter(dbCommand, "HeaderField1", DbType.String, HeaderField1);
        db.AddInParameter(dbCommand, "HeaderField2", DbType.String, HeaderField2);
        db.AddInParameter(dbCommand, "HeaderField3", DbType.String, HeaderField3);
        db.AddInParameter(dbCommand, "HeaderField4", DbType.String, HeaderField4);
        db.AddInParameter(dbCommand, "HeaderField5", DbType.String, HeaderField5);
        db.AddInParameter(dbCommand, "HeaderField6", DbType.String, HeaderField6);
        db.AddInParameter(dbCommand, "HeaderField7", DbType.String, HeaderField7);
        db.AddInParameter(dbCommand, "HeaderField8", DbType.String, HeaderField8);
        db.AddInParameter(dbCommand, "DetailField1", DbType.String, DetailField1);
        db.AddInParameter(dbCommand, "DetailField2", DbType.String, DetailField2);
        db.AddInParameter(dbCommand, "DetailField3", DbType.String, DetailField3);
        db.AddInParameter(dbCommand, "DetailField4", DbType.String, DetailField4);
        db.AddInParameter(dbCommand, "DetailField5", DbType.String, DetailField5);
        db.AddInParameter(dbCommand, "DetailField6", DbType.String, DetailField6);
        db.AddInParameter(dbCommand, "DetailField7", DbType.String, DetailField7);
        db.AddInParameter(dbCommand, "DetailField8", DbType.String, DetailField8);
        db.AddInParameter(dbCommand, "StartId", DbType.Int32, StartId);
        db.AddInParameter(dbCommand, "InsertDetail", DbType.Int32, InsertDetail);
        db.AddInParameter(dbCommand, "GroupByKey", DbType.String, GroupByKey);
        db.AddInParameter(dbCommand, "AltGroupByKey", DbType.String, AltGroupByKey);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInterfaceTables
 
    #region InsertInterfaceTables
    /// <summary>
    ///   Inserts a row into the InterfaceTables table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceTableId"></param>
    /// <param name="HeaderTable"></param>
    /// <param name="DetailTable"></param>
    /// <param name="HeaderTableKey"></param>
    /// <param name="Description1"></param>
    /// <param name="Description2"></param>
    /// <param name="HeaderField1"></param>
    /// <param name="HeaderField2"></param>
    /// <param name="HeaderField3"></param>
    /// <param name="HeaderField4"></param>
    /// <param name="HeaderField5"></param>
    /// <param name="HeaderField6"></param>
    /// <param name="HeaderField7"></param>
    /// <param name="HeaderField8"></param>
    /// <param name="DetailField1"></param>
    /// <param name="DetailField2"></param>
    /// <param name="DetailField3"></param>
    /// <param name="DetailField4"></param>
    /// <param name="DetailField5"></param>
    /// <param name="DetailField6"></param>
    /// <param name="DetailField7"></param>
    /// <param name="DetailField8"></param>
    /// <param name="StartId"></param>
    /// <param name="InsertDetail"></param>
    /// <param name="GroupByKey"></param>
    /// <param name="AltGroupByKey"></param>
    /// <returns>bool</returns>
    public bool InsertInterfaceTables
    (
        string connectionStringName,
        Int32 InterfaceTableId,
        String HeaderTable,
        String DetailTable,
        String HeaderTableKey,
        String Description1,
        String Description2,
        String HeaderField1,
        String HeaderField2,
        String HeaderField3,
        String HeaderField4,
        String HeaderField5,
        String HeaderField6,
        String HeaderField7,
        String HeaderField8,
        String DetailField1,
        String DetailField2,
        String DetailField3,
        String DetailField4,
        String DetailField5,
        String DetailField6,
        String DetailField7,
        String DetailField8,
        Int32 StartId,
        Int32 InsertDetail,
        String GroupByKey,
        String AltGroupByKey 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceTables_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceTableId", DbType.Int32, InterfaceTableId);
        db.AddInParameter(dbCommand, "HeaderTable", DbType.String, HeaderTable);
        db.AddInParameter(dbCommand, "DetailTable", DbType.String, DetailTable);
        db.AddInParameter(dbCommand, "HeaderTableKey", DbType.String, HeaderTableKey);
        db.AddInParameter(dbCommand, "Description1", DbType.String, Description1);
        db.AddInParameter(dbCommand, "Description2", DbType.String, Description2);
        db.AddInParameter(dbCommand, "HeaderField1", DbType.String, HeaderField1);
        db.AddInParameter(dbCommand, "HeaderField2", DbType.String, HeaderField2);
        db.AddInParameter(dbCommand, "HeaderField3", DbType.String, HeaderField3);
        db.AddInParameter(dbCommand, "HeaderField4", DbType.String, HeaderField4);
        db.AddInParameter(dbCommand, "HeaderField5", DbType.String, HeaderField5);
        db.AddInParameter(dbCommand, "HeaderField6", DbType.String, HeaderField6);
        db.AddInParameter(dbCommand, "HeaderField7", DbType.String, HeaderField7);
        db.AddInParameter(dbCommand, "HeaderField8", DbType.String, HeaderField8);
        db.AddInParameter(dbCommand, "DetailField1", DbType.String, DetailField1);
        db.AddInParameter(dbCommand, "DetailField2", DbType.String, DetailField2);
        db.AddInParameter(dbCommand, "DetailField3", DbType.String, DetailField3);
        db.AddInParameter(dbCommand, "DetailField4", DbType.String, DetailField4);
        db.AddInParameter(dbCommand, "DetailField5", DbType.String, DetailField5);
        db.AddInParameter(dbCommand, "DetailField6", DbType.String, DetailField6);
        db.AddInParameter(dbCommand, "DetailField7", DbType.String, DetailField7);
        db.AddInParameter(dbCommand, "DetailField8", DbType.String, DetailField8);
        db.AddInParameter(dbCommand, "StartId", DbType.Int32, StartId);
        db.AddInParameter(dbCommand, "InsertDetail", DbType.Int32, InsertDetail);
        db.AddInParameter(dbCommand, "GroupByKey", DbType.String, GroupByKey);
        db.AddInParameter(dbCommand, "AltGroupByKey", DbType.String, AltGroupByKey);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInterfaceTables
 
    #region SearchInterfaceTables
    /// <summary>
    ///   Selects rows from the InterfaceTables table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceTableId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInterfaceTables
    (
        string connectionStringName,
        Int32 InterfaceTableId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceTables_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceTableId", DbType.Int32, InterfaceTableId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInterfaceTables
 
    #region PageSearchInterfaceTables
    /// <summary>
    ///   Selects rows from the InterfaceTables table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceTableId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchInterfaceTables
    (
        string connectionStringName,
        Int32 InterfaceTableId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceTables_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceTableId", DbType.Int32, InterfaceTableId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchInterfaceTables
 
    #region ListInterfaceTables
    /// <summary>
    ///   Lists rows from the InterfaceTables table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInterfaceTables
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceTables_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInterfaceTables
 
    #region ParameterInterfaceTables
    /// <summary>
    ///   Lists rows from the InterfaceTables table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInterfaceTables
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceTables_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInterfaceTables
}
