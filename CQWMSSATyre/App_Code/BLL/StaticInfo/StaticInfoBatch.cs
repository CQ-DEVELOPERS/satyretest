using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoBatch
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:14:55
/// </summary>
/// <remarks>
///   Inserts a row into the Batch table.
///   Selects rows from the Batch table.
///   Searches for rows from the Batch table.
///   Updates a rows in the Batch table.
///   Deletes a row from the Batch table.
/// </remarks>
/// <param>
public class StaticInfoBatch
{
    #region "Constructor Logic"
    public StaticInfoBatch()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetBatch
    /// <summary>
    ///   Selects rows from the Batch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BatchId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetBatch
    (
        string connectionStringName,
        Int32 BatchId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Batch_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetBatch
 
    #region DeleteBatch
    /// <summary>
    ///   Deletes a row from the Batch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BatchId"></param>
    /// <returns>bool</returns>
    public bool DeleteBatch
    (
        string connectionStringName,
        Int32 BatchId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Batch_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteBatch
 
    #region UpdateBatch
    /// <summary>
    ///   Updates a row in the Batch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BatchId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="Batch"></param>
    /// <param name="CreateDate"></param>
    /// <param name="ExpiryDate"></param>
    /// <param name="IncubationDays"></param>
    /// <param name="ShelfLifeDays"></param>
    /// <param name="ExpectedYield"></param>
    /// <param name="ActualYield"></param>
    /// <param name="ECLNumber"></param>
    /// <returns>bool</returns>
    public bool UpdateBatch
    (
        string connectionStringName,
        Int32 BatchId,
        Int32 StatusId,
        Int32 WarehouseId,
        String Batch,
        DateTime CreateDate,
        DateTime ExpiryDate,
        Int32 IncubationDays,
        Int32 ShelfLifeDays,
        Int32 ExpectedYield,
        Int32 ActualYield,
        String ECLNumber 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Batch_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, Batch);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "ExpiryDate", DbType.DateTime, ExpiryDate);
        db.AddInParameter(dbCommand, "IncubationDays", DbType.Int32, IncubationDays);
        db.AddInParameter(dbCommand, "ShelfLifeDays", DbType.Int32, ShelfLifeDays);
        db.AddInParameter(dbCommand, "ExpectedYield", DbType.Int32, ExpectedYield);
        db.AddInParameter(dbCommand, "ActualYield", DbType.Int32, ActualYield);
        db.AddInParameter(dbCommand, "ECLNumber", DbType.String, ECLNumber);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateBatch
 
    #region InsertBatch
    /// <summary>
    ///   Inserts a row into the Batch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BatchId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="Batch"></param>
    /// <param name="CreateDate"></param>
    /// <param name="ExpiryDate"></param>
    /// <param name="IncubationDays"></param>
    /// <param name="ShelfLifeDays"></param>
    /// <param name="ExpectedYield"></param>
    /// <param name="ActualYield"></param>
    /// <param name="ECLNumber"></param>
    /// <returns>bool</returns>
    public bool InsertBatch
    (
        string connectionStringName,
        Int32 BatchId,
        Int32 StatusId,
        Int32 WarehouseId,
        String Batch,
        DateTime CreateDate,
        DateTime ExpiryDate,
        Int32 IncubationDays,
        Int32 ShelfLifeDays,
        Int32 ExpectedYield,
        Int32 ActualYield,
        String ECLNumber 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Batch_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, Batch);
        db.AddInParameter(dbCommand, "CreateDate", DbType.DateTime, CreateDate);
        db.AddInParameter(dbCommand, "ExpiryDate", DbType.DateTime, ExpiryDate);
        db.AddInParameter(dbCommand, "IncubationDays", DbType.Int32, IncubationDays);
        db.AddInParameter(dbCommand, "ShelfLifeDays", DbType.Int32, ShelfLifeDays);
        db.AddInParameter(dbCommand, "ExpectedYield", DbType.Int32, ExpectedYield);
        db.AddInParameter(dbCommand, "ActualYield", DbType.Int32, ActualYield);
        db.AddInParameter(dbCommand, "ECLNumber", DbType.String, ECLNumber);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertBatch
 
    #region SearchBatch
    /// <summary>
    ///   Selects rows from the Batch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BatchId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="Batch"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchBatch
    (
        string connectionStringName,
        Int32 BatchId,
        Int32 StatusId,
        Int32 WarehouseId,
        String Batch 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Batch_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, Batch);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchBatch
 
    #region ListBatch
    /// <summary>
    ///   Lists rows from the Batch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListBatch
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Batch_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListBatch
 
    #region ParameterBatch
    /// <summary>
    ///   Lists rows from the Batch table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterBatch
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Batch_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterBatch
}
