using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoBillable
///   Create By      : Grant Schultz
///   Date Created   : 03 Mar 2014 12:45:49
/// </summary>
/// <remarks>
///   Inserts a row into the Billable table.
///   Selects rows from the Billable table.
///   Searches for rows from the Billable table.
///   Updates a rows in the Billable table.
///   Deletes a row from the Billable table.
/// </remarks>
/// <param>
public class StaticInfoBillable
{
    #region "Constructor Logic"
    public StaticInfoBillable()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetBillable
    /// <summary>
    ///   Selects rows from the Billable table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BillableId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetBillable
    (
        string connectionStringName,
        Int32 BillableId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Billable_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BillableId", DbType.Int32, BillableId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetBillable
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Billable_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetBillable
 
    #region DeleteBillable
    /// <summary>
    ///   Deletes a row from the Billable table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BillableId"></param>
    /// <returns>bool</returns>
    public bool DeleteBillable
    (
        string connectionStringName,
        Int32 BillableId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Billable_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BillableId", DbType.Int32, BillableId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteBillable
 
    #region UpdateBillable
    /// <summary>
    ///   Updates a row in the Billable table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BillableId"></param>
    /// <param name="BillableCode"></param>
    /// <param name="Billable"></param>
    /// <returns>bool</returns>
    public bool UpdateBillable
    (
        string connectionStringName,
        Int32 BillableId,
        String BillableCode,
        String Billable 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Billable_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BillableId", DbType.Int32, BillableId);
        db.AddInParameter(dbCommand, "BillableCode", DbType.String, BillableCode);
        db.AddInParameter(dbCommand, "Billable", DbType.String, Billable);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateBillable
 
    #region InsertBillable
    /// <summary>
    ///   Inserts a row into the Billable table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BillableId"></param>
    /// <param name="BillableCode"></param>
    /// <param name="Billable"></param>
    /// <returns>bool</returns>
    public bool InsertBillable
    (
        string connectionStringName,
        Int32 BillableId,
        String BillableCode,
        String Billable 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Billable_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BillableId", DbType.Int32, BillableId);
        db.AddInParameter(dbCommand, "BillableCode", DbType.String, BillableCode);
        db.AddInParameter(dbCommand, "Billable", DbType.String, Billable);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertBillable
 
    #region SearchBillable
    /// <summary>
    ///   Selects rows from the Billable table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BillableId"></param>
    /// <param name="BillableCode"></param>
    /// <param name="Billable"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchBillable
    (
        string connectionStringName,
        Int32 BillableId,
        String BillableCode,
        String Billable 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Billable_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BillableId", DbType.Int32, BillableId);
        db.AddInParameter(dbCommand, "BillableCode", DbType.String, BillableCode);
        db.AddInParameter(dbCommand, "Billable", DbType.String, Billable);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchBillable
 
    #region PageSearchBillable
    /// <summary>
    ///   Selects rows from the Billable table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="BillableId"></param>
    /// <param name="BillableCode"></param>
    /// <param name="Billable"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchBillable
    (
        string connectionStringName,
        Int32 BillableId,
        String BillableCode,
        String Billable,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Billable_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "BillableId", DbType.Int32, BillableId);
        db.AddInParameter(dbCommand, "BillableCode", DbType.String, BillableCode);
        db.AddInParameter(dbCommand, "Billable", DbType.String, Billable);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchBillable
 
    #region ListBillable
    /// <summary>
    ///   Lists rows from the Billable table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListBillable
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Billable_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListBillable
 
    #region ParameterBillable
    /// <summary>
    ///   Lists rows from the Billable table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterBillable
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Billable_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterBillable
}
