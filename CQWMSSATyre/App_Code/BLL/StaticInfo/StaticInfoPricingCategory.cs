using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoPricingCategory
///   Create By      : Grant Schultz
///   Date Created   : 30 Apr 2014 11:52:01
/// </summary>
/// <remarks>
///   Inserts a row into the PricingCategory table.
///   Selects rows from the PricingCategory table.
///   Searches for rows from the PricingCategory table.
///   Updates a rows in the PricingCategory table.
///   Deletes a row from the PricingCategory table.
/// </remarks>
/// <param>
public class StaticInfoPricingCategory
{
    #region "Constructor Logic"
    public StaticInfoPricingCategory()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetPricingCategory
    /// <summary>
    ///   Selects rows from the PricingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PricingCategoryId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetPricingCategory
    (
        string connectionStringName,
        Int32 PricingCategoryId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PricingCategory_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetPricingCategory
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PricingCategory_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetPricingCategory
 
    #region DeletePricingCategory
    /// <summary>
    ///   Deletes a row from the PricingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PricingCategoryId"></param>
    /// <returns>bool</returns>
    public bool DeletePricingCategory
    (
        string connectionStringName,
        Int32 PricingCategoryId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PricingCategory_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeletePricingCategory
 
    #region UpdatePricingCategory
    /// <summary>
    ///   Updates a row in the PricingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="PricingCategory"></param>
    /// <param name="PricingCategoryCode"></param>
    /// <returns>bool</returns>
    public bool UpdatePricingCategory
    (
        string connectionStringName,
        Int32 PricingCategoryId,
        String PricingCategory,
        String PricingCategoryCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PricingCategory_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "PricingCategory", DbType.String, PricingCategory);
        db.AddInParameter(dbCommand, "PricingCategoryCode", DbType.String, PricingCategoryCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdatePricingCategory
 
    #region InsertPricingCategory
    /// <summary>
    ///   Inserts a row into the PricingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="PricingCategory"></param>
    /// <param name="PricingCategoryCode"></param>
    /// <returns>bool</returns>
    public bool InsertPricingCategory
    (
        string connectionStringName,
        Int32 PricingCategoryId,
        String PricingCategory,
        String PricingCategoryCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PricingCategory_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "PricingCategory", DbType.String, PricingCategory);
        db.AddInParameter(dbCommand, "PricingCategoryCode", DbType.String, PricingCategoryCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertPricingCategory
 
    #region SearchPricingCategory
    /// <summary>
    ///   Selects rows from the PricingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="PricingCategory"></param>
    /// <param name="PricingCategoryCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchPricingCategory
    (
        string connectionStringName,
        Int32 PricingCategoryId,
        String PricingCategory,
        String PricingCategoryCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PricingCategory_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "PricingCategory", DbType.String, PricingCategory);
        db.AddInParameter(dbCommand, "PricingCategoryCode", DbType.String, PricingCategoryCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchPricingCategory
 
    #region PageSearchPricingCategory
    /// <summary>
    ///   Selects rows from the PricingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="PricingCategory"></param>
    /// <param name="PricingCategoryCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchPricingCategory
    (
        string connectionStringName,
        Int32 PricingCategoryId,
        String PricingCategory,
        String PricingCategoryCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PricingCategory_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "PricingCategory", DbType.String, PricingCategory);
        db.AddInParameter(dbCommand, "PricingCategoryCode", DbType.String, PricingCategoryCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchPricingCategory
 
    #region ListPricingCategory
    /// <summary>
    ///   Lists rows from the PricingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListPricingCategory
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PricingCategory_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListPricingCategory
 
    #region ParameterPricingCategory
    /// <summary>
    ///   Lists rows from the PricingCategory table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterPricingCategory
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_PricingCategory_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterPricingCategory
}
