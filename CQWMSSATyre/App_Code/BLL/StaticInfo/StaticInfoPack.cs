using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoPack
///   Create By      : Grant Schultz
///   Date Created   : 14 May 2014 16:58:08
/// </summary>
/// <remarks>
///   Inserts a row into the Pack table.
///   Selects rows from the Pack table.
///   Searches for rows from the Pack table.
///   Updates a rows in the Pack table.
///   Deletes a row from the Pack table.
/// </remarks>
/// <param>
public class StaticInfoPack
{
    #region "Constructor Logic"
    public StaticInfoPack()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetPack
    /// <summary>
    ///   Selects rows from the Pack table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetPack
    (
        string connectionStringName,
        Int32 PackId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackId", DbType.Int32, PackId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetPack
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetPack
 
    #region DeletePack
    /// <summary>
    ///   Deletes a row from the Pack table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackId"></param>
    /// <returns>bool</returns>
    public bool DeletePack
    (
        string connectionStringName,
        Int32 PackId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackId", DbType.Int32, PackId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeletePack
 
    #region UpdatePack
    /// <summary>
    ///   Updates a row in the Pack table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="PackTypeId"></param>
    /// <param name="Quantity"></param>
    /// <param name="Barcode"></param>
    /// <param name="Length"></param>
    /// <param name="Width"></param>
    /// <param name="Height"></param>
    /// <param name="Volume"></param>
    /// <param name="Weight"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="NettWeight"></param>
    /// <param name="TareWeight"></param>
    /// <param name="ProductionQuantity"></param>
    /// <param name="StatusId"></param>
    /// <returns>bool</returns>
    public bool UpdatePack
    (
        string connectionStringName,
        Int32 PackId,
        Int32 StorageUnitId,
        Int32 PackTypeId,
        Double Quantity,
        String Barcode,
        Int32 Length,
        Int32 Width,
        Int32 Height,
        Decimal Volume,
        Double Weight,
        Int32 WarehouseId,
        Double NettWeight,
        Double TareWeight,
        Double ProductionQuantity,
        Int32 StatusId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackId", DbType.Int32, PackId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "PackTypeId", DbType.Int32, PackTypeId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Double, Quantity);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, Barcode);
        db.AddInParameter(dbCommand, "Length", DbType.Int32, Length);
        db.AddInParameter(dbCommand, "Width", DbType.Int32, Width);
        db.AddInParameter(dbCommand, "Height", DbType.Int32, Height);
        db.AddInParameter(dbCommand, "Volume", DbType.Decimal, Volume);
        db.AddInParameter(dbCommand, "Weight", DbType.Double, Weight);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "NettWeight", DbType.Double, NettWeight);
        db.AddInParameter(dbCommand, "TareWeight", DbType.Double, TareWeight);
        db.AddInParameter(dbCommand, "ProductionQuantity", DbType.Double, ProductionQuantity);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdatePack
 
    #region InsertPack
    /// <summary>
    ///   Inserts a row into the Pack table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="PackTypeId"></param>
    /// <param name="Quantity"></param>
    /// <param name="Barcode"></param>
    /// <param name="Length"></param>
    /// <param name="Width"></param>
    /// <param name="Height"></param>
    /// <param name="Volume"></param>
    /// <param name="Weight"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="NettWeight"></param>
    /// <param name="TareWeight"></param>
    /// <param name="ProductionQuantity"></param>
    /// <param name="StatusId"></param>
    /// <returns>bool</returns>
    public bool InsertPack
    (
        string connectionStringName,
        Int32 PackId,
        Int32 StorageUnitId,
        Int32 PackTypeId,
        Double Quantity,
        String Barcode,
        Int32 Length,
        Int32 Width,
        Int32 Height,
        Decimal Volume,
        Double Weight,
        Int32 WarehouseId,
        Double NettWeight,
        Double TareWeight,
        Double ProductionQuantity,
        Int32 StatusId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackId", DbType.Int32, PackId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "PackTypeId", DbType.Int32, PackTypeId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Double, Quantity);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, Barcode);
        db.AddInParameter(dbCommand, "Length", DbType.Int32, Length);
        db.AddInParameter(dbCommand, "Width", DbType.Int32, Width);
        db.AddInParameter(dbCommand, "Height", DbType.Int32, Height);
        db.AddInParameter(dbCommand, "Volume", DbType.Decimal, Volume);
        db.AddInParameter(dbCommand, "Weight", DbType.Double, Weight);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "NettWeight", DbType.Double, NettWeight);
        db.AddInParameter(dbCommand, "TareWeight", DbType.Double, TareWeight);
        db.AddInParameter(dbCommand, "ProductionQuantity", DbType.Double, ProductionQuantity);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertPack
 
    #region SearchPack
    /// <summary>
    ///   Selects rows from the Pack table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="PackTypeId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="StatusId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchPack
    (
        string connectionStringName,
        Int32 PackId,
        Int32 StorageUnitId,
        Int32 PackTypeId,
        Int32 WarehouseId,
        Int32 StatusId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackId", DbType.Int32, PackId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "PackTypeId", DbType.Int32, PackTypeId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchPack
 
    #region PageSearchPack
    /// <summary>
    ///   Selects rows from the Pack table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PackId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="PackTypeId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="StatusId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchPack
    (
        string connectionStringName,
        Int32 PackId,
        Int32 StorageUnitId,
        Int32 PackTypeId,
        Int32 WarehouseId,
        Int32 StatusId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PackId", DbType.Int32, PackId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "PackTypeId", DbType.Int32, PackTypeId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchPack
 
    #region ListPack
    /// <summary>
    ///   Lists rows from the Pack table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListPack
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ListPack
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListPack
 
    #region ParameterPack
    /// <summary>
    ///   Lists rows from the Pack table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterPack
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet ParameterPack
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pack_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterPack
}
