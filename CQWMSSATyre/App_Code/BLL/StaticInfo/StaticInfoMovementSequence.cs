using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoMovementSequence
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:20
/// </summary>
/// <remarks>
///   Inserts a row into the MovementSequence table.
///   Selects rows from the MovementSequence table.
///   Searches for rows from the MovementSequence table.
///   Updates a rows in the MovementSequence table.
///   Deletes a row from the MovementSequence table.
/// </remarks>
/// <param>
public class StaticInfoMovementSequence
{
    #region "Constructor Logic"
    public StaticInfoMovementSequence()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetMovementSequence
    /// <summary>
    ///   Selects rows from the MovementSequence table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MovementSequenceId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetMovementSequence
    (
        string connectionStringName,
        Int32 MovementSequenceId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MovementSequence_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MovementSequenceId", DbType.Int32, MovementSequenceId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetMovementSequence
 
    #region DeleteMovementSequence
    /// <summary>
    ///   Deletes a row from the MovementSequence table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MovementSequenceId"></param>
    /// <returns>bool</returns>
    public bool DeleteMovementSequence
    (
        string connectionStringName,
        Int32 MovementSequenceId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MovementSequence_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MovementSequenceId", DbType.Int32, MovementSequenceId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteMovementSequence
 
    #region UpdateMovementSequence
    /// <summary>
    ///   Updates a row in the MovementSequence table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MovementSequenceId"></param>
    /// <param name="PickAreaId"></param>
    /// <param name="StoreAreaId"></param>
    /// <param name="StageAreaId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="StatusCode"></param>
    /// <returns>bool</returns>
    public bool UpdateMovementSequence
    (
        string connectionStringName,
        Int32 MovementSequenceId,
        Int32 PickAreaId,
        Int32 StoreAreaId,
        Int32 StageAreaId,
        Int32 OperatorGroupId,
        String StatusCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MovementSequence_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MovementSequenceId", DbType.Int32, MovementSequenceId);
        db.AddInParameter(dbCommand, "PickAreaId", DbType.Int32, PickAreaId);
        db.AddInParameter(dbCommand, "StoreAreaId", DbType.Int32, StoreAreaId);
        db.AddInParameter(dbCommand, "StageAreaId", DbType.Int32, StageAreaId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, StatusCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateMovementSequence
 
    #region InsertMovementSequence
    /// <summary>
    ///   Inserts a row into the MovementSequence table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MovementSequenceId"></param>
    /// <param name="PickAreaId"></param>
    /// <param name="StoreAreaId"></param>
    /// <param name="StageAreaId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="StatusCode"></param>
    /// <returns>bool</returns>
    public bool InsertMovementSequence
    (
        string connectionStringName,
        Int32 MovementSequenceId,
        Int32 PickAreaId,
        Int32 StoreAreaId,
        Int32 StageAreaId,
        Int32 OperatorGroupId,
        String StatusCode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MovementSequence_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MovementSequenceId", DbType.Int32, MovementSequenceId);
        db.AddInParameter(dbCommand, "PickAreaId", DbType.Int32, PickAreaId);
        db.AddInParameter(dbCommand, "StoreAreaId", DbType.Int32, StoreAreaId);
        db.AddInParameter(dbCommand, "StageAreaId", DbType.Int32, StageAreaId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, StatusCode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertMovementSequence
 
    #region SearchMovementSequence
    /// <summary>
    ///   Selects rows from the MovementSequence table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MovementSequenceId"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="StatusCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchMovementSequence
    (
        string connectionStringName,
        Int32 MovementSequenceId,
        Int32 OperatorGroupId,
        String StatusCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MovementSequence_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MovementSequenceId", DbType.Int32, MovementSequenceId);
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "StatusCode", DbType.String, StatusCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchMovementSequence
 
    #region ListMovementSequence
    /// <summary>
    ///   Lists rows from the MovementSequence table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListMovementSequence
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MovementSequence_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListMovementSequence
 
    #region ParameterMovementSequence
    /// <summary>
    ///   Lists rows from the MovementSequence table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterMovementSequence
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MovementSequence_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterMovementSequence
}
