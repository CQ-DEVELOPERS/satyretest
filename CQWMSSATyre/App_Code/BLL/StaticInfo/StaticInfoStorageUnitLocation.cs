using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoStorageUnitLocation
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:50
/// </summary>
/// <remarks>
///   Inserts a row into the StorageUnitLocation table.
///   Selects rows from the StorageUnitLocation table.
///   Searches for rows from the StorageUnitLocation table.
///   Updates a rows in the StorageUnitLocation table.
///   Deletes a row from the StorageUnitLocation table.
/// </remarks>
/// <param>
public class StaticInfoStorageUnitLocation
{
    #region "Constructor Logic"
    public StaticInfoStorageUnitLocation()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetStorageUnitLocation
    /// <summary>
    ///   Selects rows from the StorageUnitLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="LocationId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetStorageUnitLocation
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 LocationId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLocation_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetStorageUnitLocation
 
    #region DeleteStorageUnitLocation
    /// <summary>
    ///   Deletes a row from the StorageUnitLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="LocationId"></param>
    /// <returns>bool</returns>
    public bool DeleteStorageUnitLocation
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 LocationId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLocation_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteStorageUnitLocation
 
    #region UpdateStorageUnitLocation
    /// <summary>
    ///   Updates a row in the StorageUnitLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="LocationId"></param>
    /// <returns>bool</returns>
    public bool UpdateStorageUnitLocation
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 LocationId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLocation_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateStorageUnitLocation
 
    #region InsertStorageUnitLocation
    /// <summary>
    ///   Inserts a row into the StorageUnitLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="LocationId"></param>
    /// <returns>bool</returns>
    public bool InsertStorageUnitLocation
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 LocationId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLocation_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertStorageUnitLocation
 
    #region SearchStorageUnitLocation
    /// <summary>
    ///   Selects rows from the StorageUnitLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="LocationId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchStorageUnitLocation
    (
        string connectionStringName,
        Int32 StorageUnitId,
        Int32 LocationId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLocation_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchStorageUnitLocation
 
    #region ListStorageUnitLocation
    /// <summary>
    ///   Lists rows from the StorageUnitLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListStorageUnitLocation
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLocation_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListStorageUnitLocation
 
    #region ParameterStorageUnitLocation
    /// <summary>
    ///   Lists rows from the StorageUnitLocation table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterStorageUnitLocation
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLocation_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterStorageUnitLocation
}
