using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInboundLine
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:05
/// </summary>
/// <remarks>
///   Inserts a row into the InboundLine table.
///   Selects rows from the InboundLine table.
///   Searches for rows from the InboundLine table.
///   Updates a rows in the InboundLine table.
///   Deletes a row from the InboundLine table.
/// </remarks>
/// <param>
public class StaticInfoInboundLine
{
    #region "Constructor Logic"
    public StaticInfoInboundLine()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInboundLine
    /// <summary>
    ///   Selects rows from the InboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundLineId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInboundLine
    (
        string connectionStringName,
        Int32 InboundLineId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundLine_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, InboundLineId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInboundLine
 
    #region DeleteInboundLine
    /// <summary>
    ///   Deletes a row from the InboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundLineId"></param>
    /// <returns>bool</returns>
    public bool DeleteInboundLine
    (
        string connectionStringName,
        Int32 InboundLineId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundLine_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, InboundLineId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInboundLine
 
    #region UpdateInboundLine
    /// <summary>
    ///   Updates a row in the InboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundLineId"></param>
    /// <param name="InboundDocumentId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="StatusId"></param>
    /// <param name="LineNumber"></param>
    /// <param name="Quantity"></param>
    /// <param name="BatchId"></param>
    /// <returns>bool</returns>
    public bool UpdateInboundLine
    (
        string connectionStringName,
        Int32 InboundLineId,
        Int32 InboundDocumentId,
        Int32 StorageUnitId,
        Int32 StatusId,
        Int32 LineNumber,
        Decimal Quantity,
        Int32 BatchId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundLine_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, InboundLineId);
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "LineNumber", DbType.Int32, LineNumber);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInboundLine
 
    #region InsertInboundLine
    /// <summary>
    ///   Inserts a row into the InboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundLineId"></param>
    /// <param name="InboundDocumentId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="StatusId"></param>
    /// <param name="LineNumber"></param>
    /// <param name="Quantity"></param>
    /// <param name="BatchId"></param>
    /// <returns>bool</returns>
    public bool InsertInboundLine
    (
        string connectionStringName,
        Int32 InboundLineId,
        Int32 InboundDocumentId,
        Int32 StorageUnitId,
        Int32 StatusId,
        Int32 LineNumber,
        Decimal Quantity,
        Int32 BatchId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundLine_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, InboundLineId);
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "LineNumber", DbType.Int32, LineNumber);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInboundLine
 
    #region SearchInboundLine
    /// <summary>
    ///   Selects rows from the InboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundLineId"></param>
    /// <param name="InboundDocumentId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="StatusId"></param>
    /// <param name="BatchId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInboundLine
    (
        string connectionStringName,
        Int32 InboundLineId,
        Int32 InboundDocumentId,
        Int32 StorageUnitId,
        Int32 StatusId,
        Int32 BatchId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundLine_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, InboundLineId);
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInboundLine
 
    #region ListInboundLine
    /// <summary>
    ///   Lists rows from the InboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInboundLine
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundLine_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInboundLine
 
    #region ParameterInboundLine
    /// <summary>
    ///   Lists rows from the InboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInboundLine
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundLine_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInboundLine
}
