using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoJob
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:13
/// </summary>
/// <remarks>
///   Inserts a row into the Job table.
///   Selects rows from the Job table.
///   Searches for rows from the Job table.
///   Updates a rows in the Job table.
///   Deletes a row from the Job table.
/// </remarks>
/// <param>
public class StaticInfoJob
{
    #region "Constructor Logic"
    public StaticInfoJob()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetJob
    /// <summary>
    ///   Selects rows from the Job table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetJob
    (
        string connectionStringName,
        Int32 JobId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Job_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetJob
 
    #region DeleteJob
    /// <summary>
    ///   Deletes a row from the Job table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <returns>bool</returns>
    public bool DeleteJob
    (
        string connectionStringName,
        Int32 JobId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Job_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteJob
 
    #region UpdateJob
    /// <summary>
    ///   Updates a row in the Job table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="ReferenceNumber"></param>
    /// <param name="TareWeight"></param>
    /// <param name="Weight"></param>
    /// <returns>bool</returns>
    public bool UpdateJob
    (
        string connectionStringName,
        Int32 JobId,
        Int32 PriorityId,
        Int32 OperatorId,
        Int32 StatusId,
        Int32 WarehouseId,
        Int32 ReceiptLineId,
        Int32 IssueLineId,
        String ReferenceNumber,
        Decimal TareWeight,
        Decimal Weight 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Job_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "ReferenceNumber", DbType.String, ReferenceNumber);
        db.AddInParameter(dbCommand, "TareWeight", DbType.Decimal, TareWeight);
        db.AddInParameter(dbCommand, "Weight", DbType.Decimal, Weight);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateJob
 
    #region InsertJob
    /// <summary>
    ///   Inserts a row into the Job table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="IssueLineId"></param>
    /// <param name="ReferenceNumber"></param>
    /// <param name="TareWeight"></param>
    /// <param name="Weight"></param>
    /// <returns>bool</returns>
    public bool InsertJob
    (
        string connectionStringName,
        Int32 JobId,
        Int32 PriorityId,
        Int32 OperatorId,
        Int32 StatusId,
        Int32 WarehouseId,
        Int32 ReceiptLineId,
        Int32 IssueLineId,
        String ReferenceNumber,
        Decimal TareWeight,
        Decimal Weight 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Job_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
        db.AddInParameter(dbCommand, "ReferenceNumber", DbType.String, ReferenceNumber);
        db.AddInParameter(dbCommand, "TareWeight", DbType.Decimal, TareWeight);
        db.AddInParameter(dbCommand, "Weight", DbType.Decimal, Weight);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertJob
 
    #region SearchJob
    /// <summary>
    ///   Selects rows from the Job table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="StatusId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="ReceiptLineId"></param>
    /// <param name="IssueLineId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchJob
    (
        string connectionStringName,
        Int32 JobId,
        Int32 PriorityId,
        Int32 OperatorId,
        Int32 StatusId,
        Int32 WarehouseId,
        Int32 ReceiptLineId,
        Int32 IssueLineId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Job_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, IssueLineId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchJob
 
    #region ListJob
    /// <summary>
    ///   Lists rows from the Job table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListJob
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Job_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListJob
 
    #region ParameterJob
    /// <summary>
    ///   Lists rows from the Job table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterJob
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Job_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterJob
}
