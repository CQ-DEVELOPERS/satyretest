using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoReceipt
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:37
/// </summary>
/// <remarks>
///   Inserts a row into the Receipt table.
///   Selects rows from the Receipt table.
///   Searches for rows from the Receipt table.
///   Updates a rows in the Receipt table.
///   Deletes a row from the Receipt table.
/// </remarks>
/// <param>
public class StaticInfoReceipt
{
    #region "Constructor Logic"
    public StaticInfoReceipt()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetReceipt
    /// <summary>
    ///   Selects rows from the Receipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReceiptId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetReceipt
    (
        string connectionStringName,
        Int32 ReceiptId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Receipt_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetReceipt
 
    #region DeleteReceipt
    /// <summary>
    ///   Deletes a row from the Receipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReceiptId"></param>
    /// <returns>bool</returns>
    public bool DeleteReceipt
    (
        string connectionStringName,
        Int32 ReceiptId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Receipt_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteReceipt
 
    #region UpdateReceipt
    /// <summary>
    ///   Updates a row in the Receipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReceiptId"></param>
    /// <param name="InboundDocumentId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="DeliveryNoteNumber"></param>
    /// <param name="DeliveryDate"></param>
    /// <param name="SealNumber"></param>
    /// <param name="VehicleRegistration"></param>
    /// <param name="Remarks"></param>
    /// <param name="CreditAdviceIndicator"></param>
    /// <param name="Delivery"></param>
    /// <returns>bool</returns>
    public bool UpdateReceipt
    (
        string connectionStringName,
        Int32 ReceiptId,
        Int32 InboundDocumentId,
        Int32 PriorityId,
        Int32 WarehouseId,
        Int32 LocationId,
        Int32 StatusId,
        Int32 OperatorId,
        String DeliveryNoteNumber,
        DateTime DeliveryDate,
        String SealNumber,
        String VehicleRegistration,
        String Remarks,
        Boolean CreditAdviceIndicator,
        Int32 Delivery 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Receipt_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "DeliveryNoteNumber", DbType.String, DeliveryNoteNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, DeliveryDate);
        db.AddInParameter(dbCommand, "SealNumber", DbType.String, SealNumber);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.String, VehicleRegistration);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
        db.AddInParameter(dbCommand, "CreditAdviceIndicator", DbType.Boolean, CreditAdviceIndicator);
        db.AddInParameter(dbCommand, "Delivery", DbType.Int32, Delivery);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateReceipt
 
    #region InsertReceipt
    /// <summary>
    ///   Inserts a row into the Receipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReceiptId"></param>
    /// <param name="InboundDocumentId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="DeliveryNoteNumber"></param>
    /// <param name="DeliveryDate"></param>
    /// <param name="SealNumber"></param>
    /// <param name="VehicleRegistration"></param>
    /// <param name="Remarks"></param>
    /// <param name="CreditAdviceIndicator"></param>
    /// <param name="Delivery"></param>
    /// <returns>bool</returns>
    public bool InsertReceipt
    (
        string connectionStringName,
        Int32 ReceiptId,
        Int32 InboundDocumentId,
        Int32 PriorityId,
        Int32 WarehouseId,
        Int32 LocationId,
        Int32 StatusId,
        Int32 OperatorId,
        String DeliveryNoteNumber,
        DateTime DeliveryDate,
        String SealNumber,
        String VehicleRegistration,
        String Remarks,
        Boolean CreditAdviceIndicator,
        Int32 Delivery 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Receipt_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "DeliveryNoteNumber", DbType.String, DeliveryNoteNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, DeliveryDate);
        db.AddInParameter(dbCommand, "SealNumber", DbType.String, SealNumber);
        db.AddInParameter(dbCommand, "VehicleRegistration", DbType.String, VehicleRegistration);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
        db.AddInParameter(dbCommand, "CreditAdviceIndicator", DbType.Boolean, CreditAdviceIndicator);
        db.AddInParameter(dbCommand, "Delivery", DbType.Int32, Delivery);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertReceipt
 
    #region SearchReceipt
    /// <summary>
    ///   Selects rows from the Receipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ReceiptId"></param>
    /// <param name="InboundDocumentId"></param>
    /// <param name="PriorityId"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="LocationId"></param>
    /// <param name="StatusId"></param>
    /// <param name="OperatorId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchReceipt
    (
        string connectionStringName,
        Int32 ReceiptId,
        Int32 InboundDocumentId,
        Int32 PriorityId,
        Int32 WarehouseId,
        Int32 LocationId,
        Int32 StatusId,
        Int32 OperatorId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Receipt_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, InboundDocumentId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchReceipt
 
    #region ListReceipt
    /// <summary>
    ///   Lists rows from the Receipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ListReceipt
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Receipt_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListReceipt
 
    #region ParameterReceipt
    /// <summary>
    ///   Lists rows from the Receipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterReceipt
    (
        string connectionStringName,
        Int32 WarehouseId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Receipt_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterReceipt
}
