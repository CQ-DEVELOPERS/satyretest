using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoDivision
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:11:52
/// </summary>
/// <remarks>
///   Inserts a row into the Division table.
///   Selects rows from the Division table.
///   Searches for rows from the Division table.
///   Updates a rows in the Division table.
///   Deletes a row from the Division table.
/// </remarks>
/// <param>
public class StaticInfoDivision
{
    #region "Constructor Logic"
    public StaticInfoDivision()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetDivision
    /// <summary>
    ///   Selects rows from the Division table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DivisionId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetDivision
    (
        string connectionStringName,
        Int32 DivisionId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Division_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DivisionId", DbType.Int32, DivisionId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetDivision
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Division_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetDivision
 
    #region DeleteDivision
    /// <summary>
    ///   Deletes a row from the Division table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DivisionId"></param>
    /// <returns>bool</returns>
    public bool DeleteDivision
    (
        string connectionStringName,
        Int32 DivisionId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Division_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DivisionId", DbType.Int32, DivisionId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteDivision
 
    #region UpdateDivision
    /// <summary>
    ///   Updates a row in the Division table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DivisionId"></param>
    /// <param name="DivisionCode"></param>
    /// <param name="Division"></param>
    /// <returns>bool</returns>
    public bool UpdateDivision
    (
        string connectionStringName,
        Int32 DivisionId,
        String DivisionCode,
        String Division 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Division_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DivisionId", DbType.Int32, DivisionId);
        db.AddInParameter(dbCommand, "DivisionCode", DbType.String, DivisionCode);
        db.AddInParameter(dbCommand, "Division", DbType.String, Division);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateDivision
 
    #region InsertDivision
    /// <summary>
    ///   Inserts a row into the Division table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DivisionId"></param>
    /// <param name="DivisionCode"></param>
    /// <param name="Division"></param>
    /// <returns>bool</returns>
    public bool InsertDivision
    (
        string connectionStringName,
        Int32 DivisionId,
        String DivisionCode,
        String Division 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Division_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DivisionId", DbType.Int32, DivisionId);
        db.AddInParameter(dbCommand, "DivisionCode", DbType.String, DivisionCode);
        db.AddInParameter(dbCommand, "Division", DbType.String, Division);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertDivision
 
    #region SearchDivision
    /// <summary>
    ///   Selects rows from the Division table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DivisionId"></param>
    /// <param name="DivisionCode"></param>
    /// <param name="Division"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchDivision
    (
        string connectionStringName,
        Int32 DivisionId,
        String DivisionCode,
        String Division 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Division_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DivisionId", DbType.Int32, DivisionId);
        db.AddInParameter(dbCommand, "DivisionCode", DbType.String, DivisionCode);
        db.AddInParameter(dbCommand, "Division", DbType.String, Division);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchDivision
 
    #region PageSearchDivision
    /// <summary>
    ///   Selects rows from the Division table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="DivisionId"></param>
    /// <param name="DivisionCode"></param>
    /// <param name="Division"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchDivision
    (
        string connectionStringName,
        Int32 DivisionId,
        String DivisionCode,
        String Division,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Division_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "DivisionId", DbType.Int32, DivisionId);
        db.AddInParameter(dbCommand, "DivisionCode", DbType.String, DivisionCode);
        db.AddInParameter(dbCommand, "Division", DbType.String, Division);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchDivision
 
    #region ListDivision
    /// <summary>
    ///   Lists rows from the Division table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListDivision
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Division_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListDivision
 
    #region ParameterDivision
    /// <summary>
    ///   Lists rows from the Division table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterDivision
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Division_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterDivision
}
