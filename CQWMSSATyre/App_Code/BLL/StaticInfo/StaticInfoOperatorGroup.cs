using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoOperatorGroup
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:14
/// </summary>
/// <remarks>
///   Inserts a row into the OperatorGroup table.
///   Selects rows from the OperatorGroup table.
///   Searches for rows from the OperatorGroup table.
///   Updates a rows in the OperatorGroup table.
///   Deletes a row from the OperatorGroup table.
/// </remarks>
/// <param>
public class StaticInfoOperatorGroup
{
    #region "Constructor Logic"
    public StaticInfoOperatorGroup()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetOperatorGroup
    /// <summary>
    ///   Selects rows from the OperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetOperatorGroup
    (
        string connectionStringName,
        Int32 OperatorGroupId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroup_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetOperatorGroup
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroup_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetOperatorGroup
 
    #region DeleteOperatorGroup
    /// <summary>
    ///   Deletes a row from the OperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <returns>bool</returns>
    public bool DeleteOperatorGroup
    (
        string connectionStringName,
        Int32 OperatorGroupId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroup_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteOperatorGroup
 
    #region UpdateOperatorGroup
    /// <summary>
    ///   Updates a row in the OperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="OperatorGroup"></param>
    /// <param name="RestrictedAreaIndicator"></param>
    /// <param name="OperatorGroupCode"></param>
    /// <param name="NarrowAisle"></param>
    /// <param name="Interleaving"></param>
    /// <returns>bool</returns>
    public bool UpdateOperatorGroup
    (
        string connectionStringName,
        Int32 OperatorGroupId,
        String OperatorGroup,
        Boolean RestrictedAreaIndicator,
        String OperatorGroupCode,
        Boolean NarrowAisle,
        Boolean Interleaving 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroup_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "OperatorGroup", DbType.String, OperatorGroup);
        db.AddInParameter(dbCommand, "RestrictedAreaIndicator", DbType.Boolean, RestrictedAreaIndicator);
        db.AddInParameter(dbCommand, "OperatorGroupCode", DbType.String, OperatorGroupCode);
        db.AddInParameter(dbCommand, "NarrowAisle", DbType.Boolean, NarrowAisle);
        db.AddInParameter(dbCommand, "Interleaving", DbType.Boolean, Interleaving);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateOperatorGroup
 
    #region InsertOperatorGroup
    /// <summary>
    ///   Inserts a row into the OperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="OperatorGroup"></param>
    /// <param name="RestrictedAreaIndicator"></param>
    /// <param name="OperatorGroupCode"></param>
    /// <param name="NarrowAisle"></param>
    /// <param name="Interleaving"></param>
    /// <returns>bool</returns>
    public bool InsertOperatorGroup
    (
        string connectionStringName,
        Int32 OperatorGroupId,
        String OperatorGroup,
        Boolean RestrictedAreaIndicator,
        String OperatorGroupCode,
        Boolean NarrowAisle,
        Boolean Interleaving 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroup_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "OperatorGroup", DbType.String, OperatorGroup);
        db.AddInParameter(dbCommand, "RestrictedAreaIndicator", DbType.Boolean, RestrictedAreaIndicator);
        db.AddInParameter(dbCommand, "OperatorGroupCode", DbType.String, OperatorGroupCode);
        db.AddInParameter(dbCommand, "NarrowAisle", DbType.Boolean, NarrowAisle);
        db.AddInParameter(dbCommand, "Interleaving", DbType.Boolean, Interleaving);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertOperatorGroup
 
    #region SearchOperatorGroup
    /// <summary>
    ///   Selects rows from the OperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="OperatorGroup"></param>
    /// <param name="OperatorGroupCode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchOperatorGroup
    (
        string connectionStringName,
        Int32 OperatorGroupId,
        String OperatorGroup,
        String OperatorGroupCode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroup_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "OperatorGroup", DbType.String, OperatorGroup);
        db.AddInParameter(dbCommand, "OperatorGroupCode", DbType.String, OperatorGroupCode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchOperatorGroup
 
    #region PageSearchOperatorGroup
    /// <summary>
    ///   Selects rows from the OperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OperatorGroupId"></param>
    /// <param name="OperatorGroup"></param>
    /// <param name="OperatorGroupCode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchOperatorGroup
    (
        string connectionStringName,
        Int32 OperatorGroupId,
        String OperatorGroup,
        String OperatorGroupCode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroup_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, OperatorGroupId);
        db.AddInParameter(dbCommand, "OperatorGroup", DbType.String, OperatorGroup);
        db.AddInParameter(dbCommand, "OperatorGroupCode", DbType.String, OperatorGroupCode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchOperatorGroup
 
    #region ListOperatorGroup
    /// <summary>
    ///   Lists rows from the OperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListOperatorGroup
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroup_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListOperatorGroup
 
    #region ParameterOperatorGroup
    /// <summary>
    ///   Lists rows from the OperatorGroup table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterOperatorGroup
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OperatorGroup_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterOperatorGroup
}
