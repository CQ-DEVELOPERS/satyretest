using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoCOA
///   Create By      : Grant Schultz
///   Date Created   : 04 Oct 2012 15:03:11
/// </summary>
/// <remarks>
///   Inserts a row into the COA table.
///   Selects rows from the COA table.
///   Searches for rows from the COA table.
///   Updates a rows in the COA table.
///   Deletes a row from the COA table.
/// </remarks>
/// <param>
public class StaticInfoCOA
{
    #region "Constructor Logic"
    public StaticInfoCOA()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetCOA
    /// <summary>
    ///   Selects rows from the COA table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COAId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetCOA
    (
        string connectionStringName,
        Int32 COAId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COA_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetCOA
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COA_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetCOA
 
    #region DeleteCOA
    /// <summary>
    ///   Deletes a row from the COA table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COAId"></param>
    /// <returns>bool</returns>
    public bool DeleteCOA
    (
        string connectionStringName,
        Int32 COAId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COA_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteCOA
 
    #region UpdateCOA
    /// <summary>
    ///   Updates a row in the COA table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COAId"></param>
    /// <param name="COACode"></param>
    /// <param name="COA"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <returns>bool</returns>
    public bool UpdateCOA
    (
        string connectionStringName,
        Int32 COAId,
        String COACode,
        String COA,
        Int32 StorageUnitBatchId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COA_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "COACode", DbType.String, COACode);
        db.AddInParameter(dbCommand, "COA", DbType.String, COA);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateCOA
 
    #region InsertCOA
    /// <summary>
    ///   Inserts a row into the COA table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COAId"></param>
    /// <param name="COACode"></param>
    /// <param name="COA"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <returns>bool</returns>
    public bool InsertCOA
    (
        string connectionStringName,
        Int32 COAId,
        String COACode,
        String COA,
        Int32 StorageUnitBatchId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COA_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "COACode", DbType.String, COACode);
        db.AddInParameter(dbCommand, "COA", DbType.String, COA);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertCOA
 
    #region SearchCOA
    /// <summary>
    ///   Selects rows from the COA table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COAId"></param>
    /// <param name="COACode"></param>
    /// <param name="COA"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchCOA
    (
        string connectionStringName,
        Int32 COAId,
        String COACode,
        String COA,
        Int32 StorageUnitBatchId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COA_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "COACode", DbType.String, COACode);
        db.AddInParameter(dbCommand, "COA", DbType.String, COA);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchCOA
 
    #region PageSearchCOA
    /// <summary>
    ///   Selects rows from the COA table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="COAId"></param>
    /// <param name="COACode"></param>
    /// <param name="COA"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchCOA
    (
        string connectionStringName,
        Int32 COAId,
        String COACode,
        String COA,
        Int32 StorageUnitBatchId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COA_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "COAId", DbType.Int32, COAId);
        db.AddInParameter(dbCommand, "COACode", DbType.String, COACode);
        db.AddInParameter(dbCommand, "COA", DbType.String, COA);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchCOA
 
    #region ListCOA
    /// <summary>
    ///   Lists rows from the COA table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListCOA
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COA_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListCOA
 
    #region ParameterCOA
    /// <summary>
    ///   Lists rows from the COA table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterCOA
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_COA_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterCOA
}
