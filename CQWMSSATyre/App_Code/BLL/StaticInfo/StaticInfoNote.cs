using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoNote
///   Create By      : Grant Schultz
///   Date Created   : 04 Oct 2012 15:03:25
/// </summary>
/// <remarks>
///   Inserts a row into the Note table.
///   Selects rows from the Note table.
///   Searches for rows from the Note table.
///   Updates a rows in the Note table.
///   Deletes a row from the Note table.
/// </remarks>
/// <param>
public class StaticInfoNote
{
    #region "Constructor Logic"
    public StaticInfoNote()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetNote
    /// <summary>
    ///   Selects rows from the Note table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NoteId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetNote
    (
        string connectionStringName,
        Int32 NoteId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Note_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NoteId", DbType.Int32, NoteId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetNote
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Note_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetNote
 
    #region DeleteNote
    /// <summary>
    ///   Deletes a row from the Note table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NoteId"></param>
    /// <returns>bool</returns>
    public bool DeleteNote
    (
        string connectionStringName,
        Int32 NoteId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Note_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NoteId", DbType.Int32, NoteId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteNote
 
    #region UpdateNote
    /// <summary>
    ///   Updates a row in the Note table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NoteId"></param>
    /// <param name="NoteCode"></param>
    /// <param name="Note"></param>
    /// <returns>bool</returns>
    public bool UpdateNote
    (
        string connectionStringName,
        Int32 NoteId,
        String NoteCode,
        String Note 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Note_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NoteId", DbType.Int32, NoteId);
        db.AddInParameter(dbCommand, "NoteCode", DbType.String, NoteCode);
        db.AddInParameter(dbCommand, "Note", DbType.String, Note);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateNote
 
    #region InsertNote
    /// <summary>
    ///   Inserts a row into the Note table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NoteId"></param>
    /// <param name="NoteCode"></param>
    /// <param name="Note"></param>
    /// <returns>bool</returns>
    public bool InsertNote
    (
        string connectionStringName,
        Int32 NoteId,
        String NoteCode,
        String Note 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Note_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NoteId", DbType.Int32, NoteId);
        db.AddInParameter(dbCommand, "NoteCode", DbType.String, NoteCode);
        db.AddInParameter(dbCommand, "Note", DbType.String, Note);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertNote
 
    #region SearchNote
    /// <summary>
    ///   Selects rows from the Note table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NoteId"></param>
    /// <param name="NoteCode"></param>
    /// <param name="Note"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchNote
    (
        string connectionStringName,
        Int32 NoteId,
        String NoteCode,
        String Note 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Note_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NoteId", DbType.Int32, NoteId);
        db.AddInParameter(dbCommand, "NoteCode", DbType.String, NoteCode);
        db.AddInParameter(dbCommand, "Note", DbType.String, Note);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchNote
 
    #region PageSearchNote
    /// <summary>
    ///   Selects rows from the Note table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="NoteId"></param>
    /// <param name="NoteCode"></param>
    /// <param name="Note"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchNote
    (
        string connectionStringName,
        Int32 NoteId,
        String NoteCode,
        String Note,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Note_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "NoteId", DbType.Int32, NoteId);
        db.AddInParameter(dbCommand, "NoteCode", DbType.String, NoteCode);
        db.AddInParameter(dbCommand, "Note", DbType.String, Note);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchNote
 
    #region ListNote
    /// <summary>
    ///   Lists rows from the Note table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListNote
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Note_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListNote
 
    #region ParameterNote
    /// <summary>
    ///   Lists rows from the Note table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterNote
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Note_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterNote
}
