using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoQuestions
///   Create By      : Grant Schultz
///   Date Created   : 14 May 2014 20:30:25
/// </summary>
/// <remarks>
///   Inserts a row into the Questions table.
///   Selects rows from the Questions table.
///   Searches for rows from the Questions table.
///   Updates a rows in the Questions table.
///   Deletes a row from the Questions table.
/// </remarks>
/// <param>
public class StaticInfoQuestions
{
    #region "Constructor Logic"
    public StaticInfoQuestions()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetQuestions
    /// <summary>
    ///   Selects rows from the Questions table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetQuestions
    (
        string connectionStringName,
        Int32 QuestionId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Questions_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionId", DbType.Int32, QuestionId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetQuestions
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Questions_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetQuestions
 
    #region DeleteQuestions
    /// <summary>
    ///   Deletes a row from the Questions table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionId"></param>
    /// <returns>bool</returns>
    public bool DeleteQuestions
    (
        string connectionStringName,
        Int32 QuestionId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Questions_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionId", DbType.Int32, QuestionId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteQuestions
 
    #region UpdateQuestions
    /// <summary>
    ///   Updates a row in the Questions table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionId"></param>
    /// <param name="QuestionaireId"></param>
    /// <param name="Category"></param>
    /// <param name="Code"></param>
    /// <param name="Sequence"></param>
    /// <param name="Active"></param>
    /// <param name="QuestionText"></param>
    /// <param name="QuestionType"></param>
    /// <param name="DateUpdated"></param>
    /// <returns>bool</returns>
    public bool UpdateQuestions
    (
        string connectionStringName,
        Int32 QuestionId,
        Int32 QuestionaireId,
        String Category,
        String Code,
        Int32 Sequence,
        Boolean Active,
        String QuestionText,
        String QuestionType,
        DateTime DateUpdated 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Questions_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionId", DbType.Int32, QuestionId);
        db.AddInParameter(dbCommand, "QuestionaireId", DbType.Int32, QuestionaireId);
        db.AddInParameter(dbCommand, "Category", DbType.String, Category);
        db.AddInParameter(dbCommand, "Code", DbType.String, Code);
        db.AddInParameter(dbCommand, "Sequence", DbType.Int32, Sequence);
        db.AddInParameter(dbCommand, "Active", DbType.Boolean, Active);
        db.AddInParameter(dbCommand, "QuestionText", DbType.String, QuestionText);
        db.AddInParameter(dbCommand, "QuestionType", DbType.String, QuestionType);
        db.AddInParameter(dbCommand, "DateUpdated", DbType.DateTime, DateUpdated);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateQuestions
 
    #region InsertQuestions
    /// <summary>
    ///   Inserts a row into the Questions table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionId"></param>
    /// <param name="QuestionaireId"></param>
    /// <param name="Category"></param>
    /// <param name="Code"></param>
    /// <param name="Sequence"></param>
    /// <param name="Active"></param>
    /// <param name="QuestionText"></param>
    /// <param name="QuestionType"></param>
    /// <param name="DateUpdated"></param>
    /// <returns>bool</returns>
    public bool InsertQuestions
    (
        string connectionStringName,
        Int32 QuestionId,
        Int32 QuestionaireId,
        String Category,
        String Code,
        Int32 Sequence,
        Boolean Active,
        String QuestionText,
        String QuestionType,
        DateTime DateUpdated 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Questions_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionId", DbType.Int32, QuestionId);
        db.AddInParameter(dbCommand, "QuestionaireId", DbType.Int32, QuestionaireId);
        db.AddInParameter(dbCommand, "Category", DbType.String, Category);
        db.AddInParameter(dbCommand, "Code", DbType.String, Code);
        db.AddInParameter(dbCommand, "Sequence", DbType.Int32, Sequence);
        db.AddInParameter(dbCommand, "Active", DbType.Boolean, Active);
        db.AddInParameter(dbCommand, "QuestionText", DbType.String, QuestionText);
        db.AddInParameter(dbCommand, "QuestionType", DbType.String, QuestionType);
        db.AddInParameter(dbCommand, "DateUpdated", DbType.DateTime, DateUpdated);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertQuestions
 
    #region SearchQuestions
    /// <summary>
    ///   Selects rows from the Questions table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchQuestions
    (
        string connectionStringName,
        Int32 QuestionId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Questions_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionId", DbType.Int32, QuestionId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchQuestions
 
    #region PageSearchQuestions
    /// <summary>
    ///   Selects rows from the Questions table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="QuestionId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchQuestions
    (
        string connectionStringName,
        Int32 QuestionId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Questions_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "QuestionId", DbType.Int32, QuestionId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchQuestions
 
    #region ListQuestions
    /// <summary>
    ///   Lists rows from the Questions table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListQuestions
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Questions_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListQuestions
 
    #region ParameterQuestions
    /// <summary>
    ///   Lists rows from the Questions table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterQuestions
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Questions_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterQuestions
}
