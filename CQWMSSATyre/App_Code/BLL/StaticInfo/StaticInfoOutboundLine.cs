using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoOutboundLine
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:28
/// </summary>
/// <remarks>
///   Inserts a row into the OutboundLine table.
///   Selects rows from the OutboundLine table.
///   Searches for rows from the OutboundLine table.
///   Updates a rows in the OutboundLine table.
///   Deletes a row from the OutboundLine table.
/// </remarks>
/// <param>
public class StaticInfoOutboundLine
{
    #region "Constructor Logic"
    public StaticInfoOutboundLine()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetOutboundLine
    /// <summary>
    ///   Selects rows from the OutboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundLineId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetOutboundLine
    (
        string connectionStringName,
        Int32 OutboundLineId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundLine_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundLineId", DbType.Int32, OutboundLineId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetOutboundLine
 
    #region DeleteOutboundLine
    /// <summary>
    ///   Deletes a row from the OutboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundLineId"></param>
    /// <returns>bool</returns>
    public bool DeleteOutboundLine
    (
        string connectionStringName,
        Int32 OutboundLineId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundLine_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundLineId", DbType.Int32, OutboundLineId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteOutboundLine
 
    #region UpdateOutboundLine
    /// <summary>
    ///   Updates a row in the OutboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundLineId"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="StatusId"></param>
    /// <param name="LineNumber"></param>
    /// <param name="Quantity"></param>
    /// <param name="BatchId"></param>
    /// <returns>bool</returns>
    public bool UpdateOutboundLine
    (
        string connectionStringName,
        Int32 OutboundLineId,
        Int32 OutboundDocumentId,
        Int32 StorageUnitId,
        Int32 StatusId,
        Int32 LineNumber,
        Decimal Quantity,
        Int32 BatchId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundLine_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundLineId", DbType.Int32, OutboundLineId);
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "LineNumber", DbType.Int32, LineNumber);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateOutboundLine
 
    #region InsertOutboundLine
    /// <summary>
    ///   Inserts a row into the OutboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundLineId"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="StatusId"></param>
    /// <param name="LineNumber"></param>
    /// <param name="Quantity"></param>
    /// <param name="BatchId"></param>
    /// <returns>bool</returns>
    public bool InsertOutboundLine
    (
        string connectionStringName,
        Int32 OutboundLineId,
        Int32 OutboundDocumentId,
        Int32 StorageUnitId,
        Int32 StatusId,
        Int32 LineNumber,
        Decimal Quantity,
        Int32 BatchId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundLine_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundLineId", DbType.Int32, OutboundLineId);
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "LineNumber", DbType.Int32, LineNumber);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertOutboundLine
 
    #region SearchOutboundLine
    /// <summary>
    ///   Selects rows from the OutboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundLineId"></param>
    /// <param name="OutboundDocumentId"></param>
    /// <param name="StorageUnitId"></param>
    /// <param name="StatusId"></param>
    /// <param name="BatchId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchOutboundLine
    (
        string connectionStringName,
        Int32 OutboundLineId,
        Int32 OutboundDocumentId,
        Int32 StorageUnitId,
        Int32 StatusId,
        Int32 BatchId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundLine_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundLineId", DbType.Int32, OutboundLineId);
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, OutboundDocumentId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, StorageUnitId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, StatusId);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, BatchId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchOutboundLine
 
    #region ListOutboundLine
    /// <summary>
    ///   Lists rows from the OutboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListOutboundLine
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundLine_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListOutboundLine
 
    #region ParameterOutboundLine
    /// <summary>
    ///   Lists rows from the OutboundLine table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterOutboundLine
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundLine_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterOutboundLine
}
