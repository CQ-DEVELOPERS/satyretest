using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInterfaceMappingColumn
///   Create By      : Grant Schultz
///   Date Created   : 19 Mar 2014 08:42:53
/// </summary>
/// <remarks>
///   Inserts a row into the InterfaceMappingColumn table.
///   Selects rows from the InterfaceMappingColumn table.
///   Searches for rows from the InterfaceMappingColumn table.
///   Updates a rows in the InterfaceMappingColumn table.
///   Deletes a row from the InterfaceMappingColumn table.
/// </remarks>
/// <param>
public class StaticInfoInterfaceMappingColumn
{
    #region "Constructor Logic"
    public StaticInfoInterfaceMappingColumn()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInterfaceMappingColumn
    /// <summary>
    ///   Selects rows from the InterfaceMappingColumn table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingColumnId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInterfaceMappingColumn
    (
        string connectionStringName,
        Int32 InterfaceMappingColumnId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingColumn_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingColumnId", DbType.Int32, InterfaceMappingColumnId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetInterfaceMappingColumn
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingColumn_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInterfaceMappingColumn
 
    #region DeleteInterfaceMappingColumn
    /// <summary>
    ///   Deletes a row from the InterfaceMappingColumn table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingColumnId"></param>
    /// <returns>bool</returns>
    public bool DeleteInterfaceMappingColumn
    (
        string connectionStringName,
        Int32 InterfaceMappingColumnId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingColumn_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingColumnId", DbType.Int32, InterfaceMappingColumnId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInterfaceMappingColumn
 
    #region UpdateInterfaceMappingColumn
    /// <summary>
    ///   Updates a row in the InterfaceMappingColumn table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingColumnId"></param>
    /// <param name="InterfaceMappingColumnCode"></param>
    /// <param name="InterfaceMappingColumn"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="InterfaceFieldId"></param>
    /// <param name="ColumnNumber"></param>
    /// <param name="StartPosition"></param>
    /// <param name="EndPostion"></param>
    /// <param name="Format"></param>
    /// <param name="InterfaceMappingFileId"></param>
    /// <returns>bool</returns>
    public bool UpdateInterfaceMappingColumn
    (
        string connectionStringName,
        Int32 InterfaceMappingColumnId,
        String InterfaceMappingColumnCode,
        String InterfaceMappingColumn,
        Int32 InterfaceDocumentTypeId,
        Int32 InterfaceFieldId,
        Int32 ColumnNumber,
        Int32 StartPosition,
        Int32 EndPostion,
        String Format,
        Int32 InterfaceMappingFileId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingColumn_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingColumnId", DbType.Int32, InterfaceMappingColumnId);
        db.AddInParameter(dbCommand, "InterfaceMappingColumnCode", DbType.String, InterfaceMappingColumnCode);
        db.AddInParameter(dbCommand, "InterfaceMappingColumn", DbType.String, InterfaceMappingColumn);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "InterfaceFieldId", DbType.Int32, InterfaceFieldId);
        db.AddInParameter(dbCommand, "ColumnNumber", DbType.Int32, ColumnNumber);
        db.AddInParameter(dbCommand, "StartPosition", DbType.Int32, StartPosition);
        db.AddInParameter(dbCommand, "EndPostion", DbType.Int32, EndPostion);
        db.AddInParameter(dbCommand, "Format", DbType.String, Format);
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInterfaceMappingColumn
 
    #region InsertInterfaceMappingColumn
    /// <summary>
    ///   Inserts a row into the InterfaceMappingColumn table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingColumnId"></param>
    /// <param name="InterfaceMappingColumnCode"></param>
    /// <param name="InterfaceMappingColumn"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="InterfaceFieldId"></param>
    /// <param name="ColumnNumber"></param>
    /// <param name="StartPosition"></param>
    /// <param name="EndPostion"></param>
    /// <param name="Format"></param>
    /// <param name="InterfaceMappingFileId"></param>
    /// <returns>bool</returns>
    public bool InsertInterfaceMappingColumn
    (
        string connectionStringName,
        Int32 InterfaceMappingColumnId,
        String InterfaceMappingColumnCode,
        String InterfaceMappingColumn,
        Int32 InterfaceDocumentTypeId,
        Int32 InterfaceFieldId,
        Int32 ColumnNumber,
        Int32 StartPosition,
        Int32 EndPostion,
        String Format,
        Int32 InterfaceMappingFileId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingColumn_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingColumnId", DbType.Int32, InterfaceMappingColumnId);
        db.AddInParameter(dbCommand, "InterfaceMappingColumnCode", DbType.String, InterfaceMappingColumnCode);
        db.AddInParameter(dbCommand, "InterfaceMappingColumn", DbType.String, InterfaceMappingColumn);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "InterfaceFieldId", DbType.Int32, InterfaceFieldId);
        db.AddInParameter(dbCommand, "ColumnNumber", DbType.Int32, ColumnNumber);
        db.AddInParameter(dbCommand, "StartPosition", DbType.Int32, StartPosition);
        db.AddInParameter(dbCommand, "EndPostion", DbType.Int32, EndPostion);
        db.AddInParameter(dbCommand, "Format", DbType.String, Format);
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInterfaceMappingColumn
 
    #region SearchInterfaceMappingColumn
    /// <summary>
    ///   Selects rows from the InterfaceMappingColumn table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingColumnId"></param>
    /// <param name="InterfaceMappingColumnCode"></param>
    /// <param name="InterfaceMappingColumn"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="InterfaceFieldId"></param>
    /// <param name="InterfaceMappingFileId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInterfaceMappingColumn
    (
        string connectionStringName,
        Int32 InterfaceMappingColumnId,
        String InterfaceMappingColumnCode,
        String InterfaceMappingColumn,
        Int32 InterfaceDocumentTypeId,
        Int32 InterfaceFieldId,
        Int32 InterfaceMappingFileId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingColumn_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingColumnId", DbType.Int32, InterfaceMappingColumnId);
        db.AddInParameter(dbCommand, "InterfaceMappingColumnCode", DbType.String, InterfaceMappingColumnCode);
        db.AddInParameter(dbCommand, "InterfaceMappingColumn", DbType.String, InterfaceMappingColumn);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "InterfaceFieldId", DbType.Int32, InterfaceFieldId);
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInterfaceMappingColumn
 
    #region PageSearchInterfaceMappingColumn
    /// <summary>
    ///   Selects rows from the InterfaceMappingColumn table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceMappingColumnId"></param>
    /// <param name="InterfaceMappingColumnCode"></param>
    /// <param name="InterfaceMappingColumn"></param>
    /// <param name="InterfaceDocumentTypeId"></param>
    /// <param name="InterfaceFieldId"></param>
    /// <param name="InterfaceMappingFileId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchInterfaceMappingColumn
    (
        string connectionStringName,
        Int32 InterfaceMappingColumnId,
        String InterfaceMappingColumnCode,
        String InterfaceMappingColumn,
        Int32 InterfaceDocumentTypeId,
        Int32 InterfaceFieldId,
        Int32 InterfaceMappingFileId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingColumn_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InterfaceMappingColumnId", DbType.Int32, InterfaceMappingColumnId);
        db.AddInParameter(dbCommand, "InterfaceMappingColumnCode", DbType.String, InterfaceMappingColumnCode);
        db.AddInParameter(dbCommand, "InterfaceMappingColumn", DbType.String, InterfaceMappingColumn);
        db.AddInParameter(dbCommand, "InterfaceDocumentTypeId", DbType.Int32, InterfaceDocumentTypeId);
        db.AddInParameter(dbCommand, "InterfaceFieldId", DbType.Int32, InterfaceFieldId);
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchInterfaceMappingColumn
 
    #region ListInterfaceMappingColumn
    /// <summary>
    ///   Lists rows from the InterfaceMappingColumn table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInterfaceMappingColumn
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingColumn_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInterfaceMappingColumn
 
    #region ParameterInterfaceMappingColumn
    /// <summary>
    ///   Lists rows from the InterfaceMappingColumn table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInterfaceMappingColumn
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InterfaceMappingColumn_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInterfaceMappingColumn
}
