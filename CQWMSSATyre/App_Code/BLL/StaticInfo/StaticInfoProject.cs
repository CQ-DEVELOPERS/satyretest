using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoProject
///   Create By      : Grant Schultz
///   Date Created   : 03 Mar 2014 12:45:56
/// </summary>
/// <remarks>
///   Inserts a row into the Project table.
///   Selects rows from the Project table.
///   Searches for rows from the Project table.
///   Updates a rows in the Project table.
///   Deletes a row from the Project table.
/// </remarks>
/// <param>
public class StaticInfoProject
{
    #region "Constructor Logic"
    public StaticInfoProject()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetProject
    /// <summary>
    ///   Selects rows from the Project table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProjectId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetProject
    (
        string connectionStringName,
        Int32 ProjectId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Project_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProjectId", DbType.Int32, ProjectId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetProject
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Project_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetProject
 
    #region DeleteProject
    /// <summary>
    ///   Deletes a row from the Project table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProjectId"></param>
    /// <returns>bool</returns>
    public bool DeleteProject
    (
        string connectionStringName,
        Int32 ProjectId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Project_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProjectId", DbType.Int32, ProjectId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteProject
 
    #region UpdateProject
    /// <summary>
    ///   Updates a row in the Project table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProjectId"></param>
    /// <param name="ProjectCode"></param>
    /// <param name="Project"></param>
    /// <returns>bool</returns>
    public bool UpdateProject
    (
        string connectionStringName,
        Int32 ProjectId,
        String ProjectCode,
        String Project 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Project_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProjectId", DbType.Int32, ProjectId);
        db.AddInParameter(dbCommand, "ProjectCode", DbType.String, ProjectCode);
        db.AddInParameter(dbCommand, "Project", DbType.String, Project);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateProject
 
    #region InsertProject
    /// <summary>
    ///   Inserts a row into the Project table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProjectId"></param>
    /// <param name="ProjectCode"></param>
    /// <param name="Project"></param>
    /// <returns>bool</returns>
    public bool InsertProject
    (
        string connectionStringName,
        Int32 ProjectId,
        String ProjectCode,
        String Project 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Project_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProjectId", DbType.Int32, ProjectId);
        db.AddInParameter(dbCommand, "ProjectCode", DbType.String, ProjectCode);
        db.AddInParameter(dbCommand, "Project", DbType.String, Project);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertProject
 
    #region SearchProject
    /// <summary>
    ///   Selects rows from the Project table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProjectId"></param>
    /// <param name="ProjectCode"></param>
    /// <param name="Project"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchProject
    (
        string connectionStringName,
        Int32 ProjectId,
        String ProjectCode,
        String Project 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Project_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProjectId", DbType.Int32, ProjectId);
        db.AddInParameter(dbCommand, "ProjectCode", DbType.String, ProjectCode);
        db.AddInParameter(dbCommand, "Project", DbType.String, Project);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchProject
 
    #region PageSearchProject
    /// <summary>
    ///   Selects rows from the Project table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ProjectId"></param>
    /// <param name="ProjectCode"></param>
    /// <param name="Project"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchProject
    (
        string connectionStringName,
        Int32 ProjectId,
        String ProjectCode,
        String Project,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Project_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ProjectId", DbType.Int32, ProjectId);
        db.AddInParameter(dbCommand, "ProjectCode", DbType.String, ProjectCode);
        db.AddInParameter(dbCommand, "Project", DbType.String, Project);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchProject
 
    #region ListProject
    /// <summary>
    ///   Lists rows from the Project table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListProject
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Project_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListProject
 
    #region ParameterProject
    /// <summary>
    ///   Lists rows from the Project table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterProject
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Project_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterProject
}
