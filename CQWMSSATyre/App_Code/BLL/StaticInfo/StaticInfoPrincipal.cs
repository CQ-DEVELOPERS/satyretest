using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoPrincipal
///   Create By      : Grant Schultz
///   Date Created   : 20 Mar 2014 08:16:57
/// </summary>
/// <remarks>
///   Inserts a row into the Principal table.
///   Selects rows from the Principal table.
///   Searches for rows from the Principal table.
///   Updates a rows in the Principal table.
///   Deletes a row from the Principal table.
/// </remarks>
/// <param>
public class StaticInfoPrincipal
{
    #region "Constructor Logic"
    public StaticInfoPrincipal()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetPrincipal
    /// <summary>
    ///   Selects rows from the Principal table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PrincipalId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetPrincipal
    (
        string connectionStringName,
        Int32 PrincipalId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Principal_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetPrincipal
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Principal_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetPrincipal
 
    #region DeletePrincipal
    /// <summary>
    ///   Deletes a row from the Principal table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PrincipalId"></param>
    /// <returns>bool</returns>
    public bool DeletePrincipal
    (
        string connectionStringName,
        Int32 PrincipalId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Principal_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeletePrincipal
 
    #region UpdatePrincipal
    /// <summary>
    ///   Updates a row in the Principal table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="PrincipalCode"></param>
    /// <param name="Principal"></param>
    /// <param name="Email"></param>
    /// <param name="FirstName"></param>
    /// <param name="LastName"></param>
    /// <param name="Address1"></param>
    /// <param name="Address2"></param>
    /// <param name="Address3"></param>
    /// <param name="Address4"></param>
    /// <param name="Address5"></param>
    /// <param name="Address6"></param>
    /// <returns>bool</returns>
    public bool UpdatePrincipal
    (
        string connectionStringName,
        Int32 PrincipalId,
        String PrincipalCode,
        String Principal,
        String Email,
        String FirstName,
        String LastName,
        String Address1,
        String Address2,
        String Address3,
        String Address4,
        String Address5,
        String Address6 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Principal_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "PrincipalCode", DbType.String, PrincipalCode);
        db.AddInParameter(dbCommand, "Principal", DbType.String, Principal);
        db.AddInParameter(dbCommand, "Email", DbType.String, Email);
        db.AddInParameter(dbCommand, "FirstName", DbType.String, FirstName);
        db.AddInParameter(dbCommand, "LastName", DbType.String, LastName);
        db.AddInParameter(dbCommand, "Address1", DbType.String, Address1);
        db.AddInParameter(dbCommand, "Address2", DbType.String, Address2);
        db.AddInParameter(dbCommand, "Address3", DbType.String, Address3);
        db.AddInParameter(dbCommand, "Address4", DbType.String, Address4);
        db.AddInParameter(dbCommand, "Address5", DbType.String, Address5);
        db.AddInParameter(dbCommand, "Address6", DbType.String, Address6);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdatePrincipal
 
    #region InsertPrincipal
    /// <summary>
    ///   Inserts a row into the Principal table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="PrincipalCode"></param>
    /// <param name="Principal"></param>
    /// <param name="Email"></param>
    /// <param name="FirstName"></param>
    /// <param name="LastName"></param>
    /// <param name="Address1"></param>
    /// <param name="Address2"></param>
    /// <param name="Address3"></param>
    /// <param name="Address4"></param>
    /// <param name="Address5"></param>
    /// <param name="Address6"></param>
    /// <returns>bool</returns>
    public bool InsertPrincipal
    (
        string connectionStringName,
        Int32 PrincipalId,
        String PrincipalCode,
        String Principal,
        String Email,
        String FirstName,
        String LastName,
        String Address1,
        String Address2,
        String Address3,
        String Address4,
        String Address5,
        String Address6 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Principal_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "PrincipalCode", DbType.String, PrincipalCode);
        db.AddInParameter(dbCommand, "Principal", DbType.String, Principal);
        db.AddInParameter(dbCommand, "Email", DbType.String, Email);
        db.AddInParameter(dbCommand, "FirstName", DbType.String, FirstName);
        db.AddInParameter(dbCommand, "LastName", DbType.String, LastName);
        db.AddInParameter(dbCommand, "Address1", DbType.String, Address1);
        db.AddInParameter(dbCommand, "Address2", DbType.String, Address2);
        db.AddInParameter(dbCommand, "Address3", DbType.String, Address3);
        db.AddInParameter(dbCommand, "Address4", DbType.String, Address4);
        db.AddInParameter(dbCommand, "Address5", DbType.String, Address5);
        db.AddInParameter(dbCommand, "Address6", DbType.String, Address6);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertPrincipal
 
    #region SearchPrincipal
    /// <summary>
    ///   Selects rows from the Principal table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="PrincipalCode"></param>
    /// <param name="Principal"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchPrincipal
    (
        string connectionStringName,
        Int32 PrincipalId,
        String PrincipalCode,
        String Principal 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Principal_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "PrincipalCode", DbType.String, PrincipalCode);
        db.AddInParameter(dbCommand, "Principal", DbType.String, Principal);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchPrincipal
 
    #region PageSearchPrincipal
    /// <summary>
    ///   Selects rows from the Principal table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="PrincipalCode"></param>
    /// <param name="Principal"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchPrincipal
    (
        string connectionStringName,
        Int32 PrincipalId,
        String PrincipalCode,
        String Principal,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Principal_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "PrincipalCode", DbType.String, PrincipalCode);
        db.AddInParameter(dbCommand, "Principal", DbType.String, Principal);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchPrincipal
 
    #region ListPrincipal
    /// <summary>
    ///   Lists rows from the Principal table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListPrincipal
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Principal_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListPrincipal
 
    #region ParameterPrincipal
    /// <summary>
    ///   Lists rows from the Principal table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterPrincipal
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Principal_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterPrincipal
}
