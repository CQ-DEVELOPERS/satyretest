using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoContactList
///   Create By      : Grant Schultz
///   Date Created   : 03 Sep 2012 12:18:52
/// </summary>
/// <remarks>
///   Inserts a row into the ContactList table.
///   Selects rows from the ContactList table.
///   Searches for rows from the ContactList table.
///   Updates a rows in the ContactList table.
///   Deletes a row from the ContactList table.
/// </remarks>
/// <param>
public class StaticInfoContactList
{
    #region "Constructor Logic"
    public StaticInfoContactList()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetContactList
    /// <summary>
    ///   Selects rows from the ContactList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContactListId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetContactList
    (
        string connectionStringName,
        Int32 ContactListId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
 
        string sqlCommand = "p_ContactList_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetContactList
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
 
        string sqlCommand = "p_ContactList_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetContactList
 
    #region DeleteContactList
    /// <summary>
    ///   Deletes a row from the ContactList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContactListId"></param>
    /// <returns>bool</returns>
    public bool DeleteContactList
    (
        string connectionStringName,
        Int32 ContactListId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
 
        string sqlCommand = "p_ContactList_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteContactList
 
    #region UpdateContactList
    /// <summary>
    ///   Updates a row in the ContactList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContactListId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="ContactPerson"></param>
    /// <param name="Telephone"></param>
    /// <param name="Fax"></param>
    /// <param name="EMail"></param>
    /// <param name="Alias"></param>
    /// <param name="Type"></param>
    /// <returns>bool</returns>
    public bool UpdateContactList
    (
        string connectionStringName,
        Int32 ContactListId,
        Int32 ExternalCompanyId,
        Int32 OperatorId,
        String ContactPerson,
        String Telephone,
        String Fax,
        String EMail,
        String Alias,
        String Type 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
 
        string sqlCommand = "p_ContactList_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "ContactPerson", DbType.String, ContactPerson);
        db.AddInParameter(dbCommand, "Telephone", DbType.String, Telephone);
        db.AddInParameter(dbCommand, "Fax", DbType.String, Fax);
        db.AddInParameter(dbCommand, "EMail", DbType.String, EMail);
        db.AddInParameter(dbCommand, "Alias", DbType.String, Alias);
        db.AddInParameter(dbCommand, "Type", DbType.String, Type);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateContactList
 
    #region InsertContactList
    /// <summary>
    ///   Inserts a row into the ContactList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContactListId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="OperatorId"></param>
    /// <param name="ContactPerson"></param>
    /// <param name="Telephone"></param>
    /// <param name="Fax"></param>
    /// <param name="EMail"></param>
    /// <param name="Alias"></param>
    /// <param name="Type"></param>
    /// <returns>bool</returns>
    public bool InsertContactList
    (
        string connectionStringName,
        Int32 ContactListId,
        Int32 ExternalCompanyId,
        Int32 OperatorId,
        String ContactPerson,
        String Telephone,
        String Fax,
        String EMail,
        String Alias,
        String Type 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
 
        string sqlCommand = "p_ContactList_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "ContactPerson", DbType.String, ContactPerson);
        db.AddInParameter(dbCommand, "Telephone", DbType.String, Telephone);
        db.AddInParameter(dbCommand, "Fax", DbType.String, Fax);
        db.AddInParameter(dbCommand, "EMail", DbType.String, EMail);
        db.AddInParameter(dbCommand, "Alias", DbType.String, Alias);
        db.AddInParameter(dbCommand, "Type", DbType.String, Type);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertContactList
 
    #region SearchContactList
    /// <summary>
    ///   Selects rows from the ContactList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContactListId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="OperatorId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchContactList
    (
        string connectionStringName,
        Int32 ContactListId,
        Int32 ExternalCompanyId,
        Int32 OperatorId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
 
        string sqlCommand = "p_ContactList_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchContactList
 
    #region PageSearchContactList
    /// <summary>
    ///   Selects rows from the ContactList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ContactListId"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="OperatorId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchContactList
    (
        string connectionStringName,
        Int32 ContactListId,
        Int32 ExternalCompanyId,
        Int32 OperatorId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
 
        string sqlCommand = "p_ContactList_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ContactListId", DbType.Int32, ContactListId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchContactList
 
    #region ListContactList
    /// <summary>
    ///   Lists rows from the ContactList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListContactList
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
 
        string sqlCommand = "p_ContactList_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListContactList
 
    #region ParameterContactList
    /// <summary>
    ///   Lists rows from the ContactList table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterContactList
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
 
        string sqlCommand = "p_ContactList_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterContactList
}
