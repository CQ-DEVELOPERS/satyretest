using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoModule
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:10
/// </summary>
/// <remarks>
///   Inserts a row into the Module table.
///   Selects rows from the Module table.
///   Searches for rows from the Module table.
///   Updates a rows in the Module table.
///   Deletes a row from the Module table.
/// </remarks>
/// <param>
public class StaticInfoModule
{
    #region "Constructor Logic"
    public StaticInfoModule()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetModule
    /// <summary>
    ///   Selects rows from the Module table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ModuleId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetModule
    (
        string connectionStringName,
        Int32 ModuleId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Module_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, ModuleId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetModule
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Module_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetModule
 
    #region DeleteModule
    /// <summary>
    ///   Deletes a row from the Module table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ModuleId"></param>
    /// <returns>bool</returns>
    public bool DeleteModule
    (
        string connectionStringName,
        Int32 ModuleId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Module_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, ModuleId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteModule
 
    #region UpdateModule
    /// <summary>
    ///   Updates a row in the Module table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ModuleId"></param>
    /// <param name="Module"></param>
    /// <returns>bool</returns>
    public bool UpdateModule
    (
        string connectionStringName,
        Int32 ModuleId,
        String Module 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Module_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, ModuleId);
        db.AddInParameter(dbCommand, "Module", DbType.String, Module);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateModule
 
    #region InsertModule
    /// <summary>
    ///   Inserts a row into the Module table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ModuleId"></param>
    /// <param name="Module"></param>
    /// <returns>bool</returns>
    public bool InsertModule
    (
        string connectionStringName,
        Int32 ModuleId,
        String Module 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Module_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, ModuleId);
        db.AddInParameter(dbCommand, "Module", DbType.String, Module);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertModule
 
    #region SearchModule
    /// <summary>
    ///   Selects rows from the Module table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ModuleId"></param>
    /// <param name="Module"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchModule
    (
        string connectionStringName,
        Int32 ModuleId,
        String Module 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Module_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, ModuleId);
        db.AddInParameter(dbCommand, "Module", DbType.String, Module);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchModule
 
    #region PageSearchModule
    /// <summary>
    ///   Selects rows from the Module table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ModuleId"></param>
    /// <param name="Module"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchModule
    (
        string connectionStringName,
        Int32 ModuleId,
        String Module,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Module_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ModuleId", DbType.Int32, ModuleId);
        db.AddInParameter(dbCommand, "Module", DbType.String, Module);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchModule
 
    #region ListModule
    /// <summary>
    ///   Lists rows from the Module table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListModule
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Module_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListModule
 
    #region ParameterModule
    /// <summary>
    ///   Lists rows from the Module table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterModule
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Module_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterModule
}
