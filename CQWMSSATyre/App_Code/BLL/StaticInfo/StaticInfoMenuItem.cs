using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoMenuItem
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:07
/// </summary>
/// <remarks>
///   Inserts a row into the MenuItem table.
///   Selects rows from the MenuItem table.
///   Searches for rows from the MenuItem table.
///   Updates a rows in the MenuItem table.
///   Deletes a row from the MenuItem table.
/// </remarks>
/// <param>
public class StaticInfoMenuItem
{
    #region "Constructor Logic"
    public StaticInfoMenuItem()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetMenuItem
    /// <summary>
    ///   Selects rows from the MenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetMenuItem
    (
        string connectionStringName,
        Int32 MenuItemId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetMenuItem
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetMenuItem
 
    #region DeleteMenuItem
    /// <summary>
    ///   Deletes a row from the MenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemId"></param>
    /// <returns>bool</returns>
    public bool DeleteMenuItem
    (
        string connectionStringName,
        Int32 MenuItemId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteMenuItem
 
    #region UpdateMenuItem
    /// <summary>
    ///   Updates a row in the MenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemId"></param>
    /// <param name="MenuId"></param>
    /// <param name="MenuItem"></param>
    /// <param name="ToolTip"></param>
    /// <param name="NavigateTo"></param>
    /// <param name="ParentMenuItemId"></param>
    /// <param name="OrderBy"></param>
    /// <returns>bool</returns>
    public bool UpdateMenuItem
    (
        string connectionStringName,
        Int32 MenuItemId,
        Int32 MenuId,
        String MenuItem,
        String ToolTip,
        String NavigateTo,
        Int32 ParentMenuItemId,
        Int16 OrderBy 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "MenuItem", DbType.String, MenuItem);
        db.AddInParameter(dbCommand, "ToolTip", DbType.String, ToolTip);
        db.AddInParameter(dbCommand, "NavigateTo", DbType.String, NavigateTo);
        db.AddInParameter(dbCommand, "ParentMenuItemId", DbType.Int32, ParentMenuItemId);
        db.AddInParameter(dbCommand, "OrderBy", DbType.Int16, OrderBy);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateMenuItem
 
    #region InsertMenuItem
    /// <summary>
    ///   Inserts a row into the MenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemId"></param>
    /// <param name="MenuId"></param>
    /// <param name="MenuItem"></param>
    /// <param name="ToolTip"></param>
    /// <param name="NavigateTo"></param>
    /// <param name="ParentMenuItemId"></param>
    /// <param name="OrderBy"></param>
    /// <returns>bool</returns>
    public bool InsertMenuItem
    (
        string connectionStringName,
        Int32 MenuItemId,
        Int32 MenuId,
        String MenuItem,
        String ToolTip,
        String NavigateTo,
        Int32 ParentMenuItemId,
        Int16 OrderBy 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "MenuItem", DbType.String, MenuItem);
        db.AddInParameter(dbCommand, "ToolTip", DbType.String, ToolTip);
        db.AddInParameter(dbCommand, "NavigateTo", DbType.String, NavigateTo);
        db.AddInParameter(dbCommand, "ParentMenuItemId", DbType.Int32, ParentMenuItemId);
        db.AddInParameter(dbCommand, "OrderBy", DbType.Int16, OrderBy);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertMenuItem
 
    #region SearchMenuItem
    /// <summary>
    ///   Selects rows from the MenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemId"></param>
    /// <param name="MenuId"></param>
    /// <param name="MenuItem"></param>
    /// <param name="ParentMenuItemId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchMenuItem
    (
        string connectionStringName,
        Int32 MenuItemId,
        Int32 MenuId,
        String MenuItem,
        Int32 ParentMenuItemId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "MenuItem", DbType.String, MenuItem);
        db.AddInParameter(dbCommand, "ParentMenuItemId", DbType.Int32, ParentMenuItemId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchMenuItem
 
    #region PageSearchMenuItem
    /// <summary>
    ///   Selects rows from the MenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="MenuItemId"></param>
    /// <param name="MenuId"></param>
    /// <param name="MenuItem"></param>
    /// <param name="ParentMenuItemId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchMenuItem
    (
        string connectionStringName,
        Int32 MenuItemId,
        Int32 MenuId,
        String MenuItem,
        Int32 ParentMenuItemId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "MenuItemId", DbType.Int32, MenuItemId);
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, MenuId);
        db.AddInParameter(dbCommand, "MenuItem", DbType.String, MenuItem);
        db.AddInParameter(dbCommand, "ParentMenuItemId", DbType.Int32, ParentMenuItemId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchMenuItem
 
    #region ListMenuItem
    /// <summary>
    ///   Lists rows from the MenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListMenuItem
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListMenuItem
 
    #region ParameterMenuItem
    /// <summary>
    ///   Lists rows from the MenuItem table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterMenuItem
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterMenuItem
}
