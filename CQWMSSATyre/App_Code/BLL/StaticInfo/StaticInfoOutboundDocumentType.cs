using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoOutboundDocumentType
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:17
/// </summary>
/// <remarks>
///   Inserts a row into the OutboundDocumentType table.
///   Selects rows from the OutboundDocumentType table.
///   Searches for rows from the OutboundDocumentType table.
///   Updates a rows in the OutboundDocumentType table.
///   Deletes a row from the OutboundDocumentType table.
/// </remarks>
/// <param>
public class StaticInfoOutboundDocumentType
{
    #region "Constructor Logic"
    public StaticInfoOutboundDocumentType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetOutboundDocumentType
    /// <summary>
    ///   Selects rows from the OutboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetOutboundDocumentType
    (
        string connectionStringName,
        Int32 OutboundDocumentTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocumentType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetOutboundDocumentType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocumentType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetOutboundDocumentType
 
    #region DeleteOutboundDocumentType
    /// <summary>
    ///   Deletes a row from the OutboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteOutboundDocumentType
    (
        string connectionStringName,
        Int32 OutboundDocumentTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocumentType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteOutboundDocumentType
 
    #region UpdateOutboundDocumentType
    /// <summary>
    ///   Updates a row in the OutboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="OutboundDocumentTypeCode"></param>
    /// <param name="OutboundDocumentType"></param>
    /// <param name="PriorityId"></param>
    /// <param name="AlternatePallet"></param>
    /// <param name="SubstitutePallet"></param>
    /// <param name="AutoRelease"></param>
    /// <param name="AddToShipment"></param>
    /// <param name="MultipleOnShipment"></param>
    /// <param name="LIFO"></param>
    /// <param name="MinimumShelfLife"></param>
    /// <param name="AreaType"></param>
    /// <param name="AutoSendup"></param>
    /// <param name="CheckingLane"></param>
    /// <param name="DespatchBay"></param>
    /// <param name="PlanningComplete"></param>
    /// <param name="AutoInvoice"></param>
    /// <param name="Backorder"></param>
    /// <param name="AutoCheck"></param>
    /// <param name="ReserveBatch"></param>
    /// <returns>bool</returns>
    public bool UpdateOutboundDocumentType
    (
        string connectionStringName,
        Int32 OutboundDocumentTypeId,
        String OutboundDocumentTypeCode,
        String OutboundDocumentType,
        Int32 PriorityId,
        Boolean AlternatePallet,
        Boolean SubstitutePallet,
        Boolean AutoRelease,
        Boolean AddToShipment,
        Boolean MultipleOnShipment,
        Boolean LIFO,
        Decimal MinimumShelfLife,
        String AreaType,
        Boolean AutoSendup,
        Int32 CheckingLane,
        Int32 DespatchBay,
        Boolean PlanningComplete,
        Boolean AutoInvoice,
        Boolean Backorder,
        Boolean AutoCheck,
        Boolean ReserveBatch 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocumentType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeCode", DbType.String, OutboundDocumentTypeCode);
        db.AddInParameter(dbCommand, "OutboundDocumentType", DbType.String, OutboundDocumentType);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "AlternatePallet", DbType.Boolean, AlternatePallet);
        db.AddInParameter(dbCommand, "SubstitutePallet", DbType.Boolean, SubstitutePallet);
        db.AddInParameter(dbCommand, "AutoRelease", DbType.Boolean, AutoRelease);
        db.AddInParameter(dbCommand, "AddToShipment", DbType.Boolean, AddToShipment);
        db.AddInParameter(dbCommand, "MultipleOnShipment", DbType.Boolean, MultipleOnShipment);
        db.AddInParameter(dbCommand, "LIFO", DbType.Boolean, LIFO);
        db.AddInParameter(dbCommand, "MinimumShelfLife", DbType.Decimal, MinimumShelfLife);
        db.AddInParameter(dbCommand, "AreaType", DbType.String, AreaType);
        db.AddInParameter(dbCommand, "AutoSendup", DbType.Boolean, AutoSendup);
        db.AddInParameter(dbCommand, "CheckingLane", DbType.Int32, CheckingLane);
        db.AddInParameter(dbCommand, "DespatchBay", DbType.Int32, DespatchBay);
        db.AddInParameter(dbCommand, "PlanningComplete", DbType.Boolean, PlanningComplete);
        db.AddInParameter(dbCommand, "AutoInvoice", DbType.Boolean, AutoInvoice);
        db.AddInParameter(dbCommand, "Backorder", DbType.Boolean, Backorder);
        db.AddInParameter(dbCommand, "AutoCheck", DbType.Boolean, AutoCheck);
        db.AddInParameter(dbCommand, "ReserveBatch", DbType.Boolean, ReserveBatch);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateOutboundDocumentType
 
    #region InsertOutboundDocumentType
    /// <summary>
    ///   Inserts a row into the OutboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="OutboundDocumentTypeCode"></param>
    /// <param name="OutboundDocumentType"></param>
    /// <param name="PriorityId"></param>
    /// <param name="AlternatePallet"></param>
    /// <param name="SubstitutePallet"></param>
    /// <param name="AutoRelease"></param>
    /// <param name="AddToShipment"></param>
    /// <param name="MultipleOnShipment"></param>
    /// <param name="LIFO"></param>
    /// <param name="MinimumShelfLife"></param>
    /// <param name="AreaType"></param>
    /// <param name="AutoSendup"></param>
    /// <param name="CheckingLane"></param>
    /// <param name="DespatchBay"></param>
    /// <param name="PlanningComplete"></param>
    /// <param name="AutoInvoice"></param>
    /// <param name="Backorder"></param>
    /// <param name="AutoCheck"></param>
    /// <param name="ReserveBatch"></param>
    /// <returns>bool</returns>
    public bool InsertOutboundDocumentType
    (
        string connectionStringName,
        Int32 OutboundDocumentTypeId,
        String OutboundDocumentTypeCode,
        String OutboundDocumentType,
        Int32 PriorityId,
        Boolean AlternatePallet,
        Boolean SubstitutePallet,
        Boolean AutoRelease,
        Boolean AddToShipment,
        Boolean MultipleOnShipment,
        Boolean LIFO,
        Decimal MinimumShelfLife,
        String AreaType,
        Boolean AutoSendup,
        Int32 CheckingLane,
        Int32 DespatchBay,
        Boolean PlanningComplete,
        Boolean AutoInvoice,
        Boolean Backorder,
        Boolean AutoCheck,
        Boolean ReserveBatch 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocumentType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeCode", DbType.String, OutboundDocumentTypeCode);
        db.AddInParameter(dbCommand, "OutboundDocumentType", DbType.String, OutboundDocumentType);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "AlternatePallet", DbType.Boolean, AlternatePallet);
        db.AddInParameter(dbCommand, "SubstitutePallet", DbType.Boolean, SubstitutePallet);
        db.AddInParameter(dbCommand, "AutoRelease", DbType.Boolean, AutoRelease);
        db.AddInParameter(dbCommand, "AddToShipment", DbType.Boolean, AddToShipment);
        db.AddInParameter(dbCommand, "MultipleOnShipment", DbType.Boolean, MultipleOnShipment);
        db.AddInParameter(dbCommand, "LIFO", DbType.Boolean, LIFO);
        db.AddInParameter(dbCommand, "MinimumShelfLife", DbType.Decimal, MinimumShelfLife);
        db.AddInParameter(dbCommand, "AreaType", DbType.String, AreaType);
        db.AddInParameter(dbCommand, "AutoSendup", DbType.Boolean, AutoSendup);
        db.AddInParameter(dbCommand, "CheckingLane", DbType.Int32, CheckingLane);
        db.AddInParameter(dbCommand, "DespatchBay", DbType.Int32, DespatchBay);
        db.AddInParameter(dbCommand, "PlanningComplete", DbType.Boolean, PlanningComplete);
        db.AddInParameter(dbCommand, "AutoInvoice", DbType.Boolean, AutoInvoice);
        db.AddInParameter(dbCommand, "Backorder", DbType.Boolean, Backorder);
        db.AddInParameter(dbCommand, "AutoCheck", DbType.Boolean, AutoCheck);
        db.AddInParameter(dbCommand, "ReserveBatch", DbType.Boolean, ReserveBatch);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertOutboundDocumentType
 
    #region SearchOutboundDocumentType
    /// <summary>
    ///   Selects rows from the OutboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="OutboundDocumentTypeCode"></param>
    /// <param name="OutboundDocumentType"></param>
    /// <param name="PriorityId"></param>
    /// <param name="CheckingLane"></param>
    /// <param name="DespatchBay"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchOutboundDocumentType
    (
        string connectionStringName,
        Int32 OutboundDocumentTypeId,
        String OutboundDocumentTypeCode,
        String OutboundDocumentType,
        Int32 PriorityId,
        Int32 CheckingLane,
        Int32 DespatchBay 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocumentType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeCode", DbType.String, OutboundDocumentTypeCode);
        db.AddInParameter(dbCommand, "OutboundDocumentType", DbType.String, OutboundDocumentType);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "CheckingLane", DbType.Int32, CheckingLane);
        db.AddInParameter(dbCommand, "DespatchBay", DbType.Int32, DespatchBay);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchOutboundDocumentType
 
    #region PageSearchOutboundDocumentType
    /// <summary>
    ///   Selects rows from the OutboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="OutboundDocumentTypeId"></param>
    /// <param name="OutboundDocumentTypeCode"></param>
    /// <param name="OutboundDocumentType"></param>
    /// <param name="PriorityId"></param>
    /// <param name="CheckingLane"></param>
    /// <param name="DespatchBay"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchOutboundDocumentType
    (
        string connectionStringName,
        Int32 OutboundDocumentTypeId,
        String OutboundDocumentTypeCode,
        String OutboundDocumentType,
        Int32 PriorityId,
        Int32 CheckingLane,
        Int32 DespatchBay,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocumentType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, OutboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeCode", DbType.String, OutboundDocumentTypeCode);
        db.AddInParameter(dbCommand, "OutboundDocumentType", DbType.String, OutboundDocumentType);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, PriorityId);
        db.AddInParameter(dbCommand, "CheckingLane", DbType.Int32, CheckingLane);
        db.AddInParameter(dbCommand, "DespatchBay", DbType.Int32, DespatchBay);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchOutboundDocumentType
 
    #region ListOutboundDocumentType
    /// <summary>
    ///   Lists rows from the OutboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListOutboundDocumentType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocumentType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListOutboundDocumentType
 
    #region ParameterOutboundDocumentType
    /// <summary>
    ///   Lists rows from the OutboundDocumentType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterOutboundDocumentType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocumentType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterOutboundDocumentType
}
