using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoExternalCompany
///   Create By      : Grant Schultz
///   Date Created   : 30 Apr 2014 11:51:59
/// </summary>
/// <remarks>
///   Inserts a row into the ExternalCompany table.
///   Selects rows from the ExternalCompany table.
///   Searches for rows from the ExternalCompany table.
///   Updates a rows in the ExternalCompany table.
///   Deletes a row from the ExternalCompany table.
/// </remarks>
/// <param>
public class StaticInfoExternalCompany
{
    #region "Constructor Logic"
    public StaticInfoExternalCompany()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetExternalCompany
    /// <summary>
    ///   Selects rows from the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetExternalCompany
    (
        string connectionStringName,
        Int32 ExternalCompanyId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetExternalCompany
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetExternalCompany
 
    #region DeleteExternalCompany
    /// <summary>
    ///   Deletes a row from the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <returns>bool</returns>
    public bool DeleteExternalCompany
    (
        string connectionStringName,
        Int32 ExternalCompanyId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteExternalCompany
 
    #region UpdateExternalCompany
    /// <summary>
    ///   Updates a row in the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <param name="RouteId"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="Rating"></param>
    /// <param name="RedeliveryIndicator"></param>
    /// <param name="QualityAssuranceIndicator"></param>
    /// <param name="ContainerTypeId"></param>
    /// <param name="Backorder"></param>
    /// <param name="HostId"></param>
    /// <param name="OrderLineSequence"></param>
    /// <param name="DropSequence"></param>
    /// <param name="AutoInvoice"></param>
    /// <param name="OneProductPerPallet"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="ProcessId"></param>
    /// <param name="TrustedDelivery"></param>
    /// <param name="OutboundSLAHours"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="LabelingCategoryId"></param>
    /// <returns>bool</returns>
    public bool UpdateExternalCompany
    (
        string connectionStringName,
        Int32 ExternalCompanyId,
        Int32 ExternalCompanyTypeId,
        Int32 RouteId,
        String ExternalCompany,
        String ExternalCompanyCode,
        Int32 Rating,
        Boolean RedeliveryIndicator,
        Boolean QualityAssuranceIndicator,
        Int32 ContainerTypeId,
        Boolean Backorder,
        String HostId,
        Boolean OrderLineSequence,
        Int32 DropSequence,
        Boolean AutoInvoice,
        Boolean OneProductPerPallet,
        Int32 PrincipalId,
        Int32 ProcessId,
        Boolean TrustedDelivery,
        Int32 OutboundSLAHours,
        Int32 PricingCategoryId,
        Int32 LabelingCategoryId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "Rating", DbType.Int32, Rating);
        db.AddInParameter(dbCommand, "RedeliveryIndicator", DbType.Boolean, RedeliveryIndicator);
        db.AddInParameter(dbCommand, "QualityAssuranceIndicator", DbType.Boolean, QualityAssuranceIndicator);
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);
        db.AddInParameter(dbCommand, "Backorder", DbType.Boolean, Backorder);
        db.AddInParameter(dbCommand, "HostId", DbType.String, HostId);
        db.AddInParameter(dbCommand, "OrderLineSequence", DbType.Boolean, OrderLineSequence);
        db.AddInParameter(dbCommand, "DropSequence", DbType.Int32, DropSequence);
        db.AddInParameter(dbCommand, "AutoInvoice", DbType.Boolean, AutoInvoice);
        db.AddInParameter(dbCommand, "OneProductPerPallet", DbType.Boolean, OneProductPerPallet);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "ProcessId", DbType.Int32, ProcessId);
        db.AddInParameter(dbCommand, "TrustedDelivery", DbType.Boolean, TrustedDelivery);
        db.AddInParameter(dbCommand, "OutboundSLAHours", DbType.Int32, OutboundSLAHours);
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "LabelingCategoryId", DbType.Int32, LabelingCategoryId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateExternalCompany
 
    #region InsertExternalCompany
    /// <summary>
    ///   Inserts a row into the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <param name="RouteId"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="Rating"></param>
    /// <param name="RedeliveryIndicator"></param>
    /// <param name="QualityAssuranceIndicator"></param>
    /// <param name="ContainerTypeId"></param>
    /// <param name="Backorder"></param>
    /// <param name="HostId"></param>
    /// <param name="OrderLineSequence"></param>
    /// <param name="DropSequence"></param>
    /// <param name="AutoInvoice"></param>
    /// <param name="OneProductPerPallet"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="ProcessId"></param>
    /// <param name="TrustedDelivery"></param>
    /// <param name="OutboundSLAHours"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="LabelingCategoryId"></param>
    /// <returns>bool</returns>
    public bool InsertExternalCompany
    (
        string connectionStringName,
        Int32 ExternalCompanyId,
        Int32 ExternalCompanyTypeId,
        Int32 RouteId,
        String ExternalCompany,
        String ExternalCompanyCode,
        Int32 Rating,
        Boolean RedeliveryIndicator,
        Boolean QualityAssuranceIndicator,
        Int32 ContainerTypeId,
        Boolean Backorder,
        String HostId,
        Boolean OrderLineSequence,
        Int32 DropSequence,
        Boolean AutoInvoice,
        Boolean OneProductPerPallet,
        Int32 PrincipalId,
        Int32 ProcessId,
        Boolean TrustedDelivery,
        Int32 OutboundSLAHours,
        Int32 PricingCategoryId,
        Int32 LabelingCategoryId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "Rating", DbType.Int32, Rating);
        db.AddInParameter(dbCommand, "RedeliveryIndicator", DbType.Boolean, RedeliveryIndicator);
        db.AddInParameter(dbCommand, "QualityAssuranceIndicator", DbType.Boolean, QualityAssuranceIndicator);
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);
        db.AddInParameter(dbCommand, "Backorder", DbType.Boolean, Backorder);
        db.AddInParameter(dbCommand, "HostId", DbType.String, HostId);
        db.AddInParameter(dbCommand, "OrderLineSequence", DbType.Boolean, OrderLineSequence);
        db.AddInParameter(dbCommand, "DropSequence", DbType.Int32, DropSequence);
        db.AddInParameter(dbCommand, "AutoInvoice", DbType.Boolean, AutoInvoice);
        db.AddInParameter(dbCommand, "OneProductPerPallet", DbType.Boolean, OneProductPerPallet);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "ProcessId", DbType.Int32, ProcessId);
        db.AddInParameter(dbCommand, "TrustedDelivery", DbType.Boolean, TrustedDelivery);
        db.AddInParameter(dbCommand, "OutboundSLAHours", DbType.Int32, OutboundSLAHours);
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "LabelingCategoryId", DbType.Int32, LabelingCategoryId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertExternalCompany
 
    #region SearchExternalCompany
    /// <summary>
    ///   Selects rows from the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <param name="RouteId"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ContainerTypeId"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="ProcessId"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="LabelingCategoryId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchExternalCompany
    (
        string connectionStringName,
        Int32 ExternalCompanyId,
        Int32 ExternalCompanyTypeId,
        Int32 RouteId,
        String ExternalCompany,
        String ExternalCompanyCode,
        Int32 ContainerTypeId,
        Int32 PrincipalId,
        Int32 ProcessId,
        Int32 PricingCategoryId,
        Int32 LabelingCategoryId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "ProcessId", DbType.Int32, ProcessId);
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "LabelingCategoryId", DbType.Int32, LabelingCategoryId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchExternalCompany
 
    #region PageSearchExternalCompany
    /// <summary>
    ///   Selects rows from the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <param name="RouteId"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ContainerTypeId"></param>
    /// <param name="PrincipalId"></param>
    /// <param name="ProcessId"></param>
    /// <param name="PricingCategoryId"></param>
    /// <param name="LabelingCategoryId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchExternalCompany
    (
        string connectionStringName,
        Int32 ExternalCompanyId,
        Int32 ExternalCompanyTypeId,
        Int32 RouteId,
        String ExternalCompany,
        String ExternalCompanyCode,
        Int32 ContainerTypeId,
        Int32 PrincipalId,
        Int32 ProcessId,
        Int32 PricingCategoryId,
        Int32 LabelingCategoryId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, PrincipalId);
        db.AddInParameter(dbCommand, "ProcessId", DbType.Int32, ProcessId);
        db.AddInParameter(dbCommand, "PricingCategoryId", DbType.Int32, PricingCategoryId);
        db.AddInParameter(dbCommand, "LabelingCategoryId", DbType.Int32, LabelingCategoryId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchExternalCompany
 
    #region ListExternalCompany
    /// <summary>
    ///   Lists rows from the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListExternalCompany
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListExternalCompany
 
    #region ParameterExternalCompany
    /// <summary>
    ///   Lists rows from the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterExternalCompany
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterExternalCompany
}
