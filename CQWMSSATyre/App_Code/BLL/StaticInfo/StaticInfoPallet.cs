using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoPallet
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:33
/// </summary>
/// <remarks>
///   Inserts a row into the Pallet table.
///   Selects rows from the Pallet table.
///   Searches for rows from the Pallet table.
///   Updates a rows in the Pallet table.
///   Deletes a row from the Pallet table.
/// </remarks>
/// <param>
public class StaticInfoPallet
{
    #region "Constructor Logic"
    public StaticInfoPallet()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetPallet
    /// <summary>
    ///   Selects rows from the Pallet table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PalletId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetPallet
    (
        string connectionStringName,
        Int32 PalletId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PalletId", DbType.Int32, PalletId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetPallet
 
    #region DeletePallet
    /// <summary>
    ///   Deletes a row from the Pallet table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PalletId"></param>
    /// <returns>bool</returns>
    public bool DeletePallet
    (
        string connectionStringName,
        Int32 PalletId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PalletId", DbType.Int32, PalletId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeletePallet
 
    #region UpdatePallet
    /// <summary>
    ///   Updates a row in the Pallet table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PalletId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="LocationId"></param>
    /// <param name="Weight"></param>
    /// <param name="Tare"></param>
    /// <param name="Quantity"></param>
    /// <param name="Prints"></param>
    /// <returns>bool</returns>
    public bool UpdatePallet
    (
        string connectionStringName,
        Int32 PalletId,
        Int32 StorageUnitBatchId,
        Int32 LocationId,
        Decimal Weight,
        Decimal Tare,
        Decimal Quantity,
        Int32 Prints 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PalletId", DbType.Int32, PalletId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "Weight", DbType.Decimal, Weight);
        db.AddInParameter(dbCommand, "Tare", DbType.Decimal, Tare);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "Prints", DbType.Int32, Prints);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdatePallet
 
    #region InsertPallet
    /// <summary>
    ///   Inserts a row into the Pallet table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PalletId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="LocationId"></param>
    /// <param name="Weight"></param>
    /// <param name="Tare"></param>
    /// <param name="Quantity"></param>
    /// <param name="Prints"></param>
    /// <returns>bool</returns>
    public bool InsertPallet
    (
        string connectionStringName,
        Int32 PalletId,
        Int32 StorageUnitBatchId,
        Int32 LocationId,
        Decimal Weight,
        Decimal Tare,
        Decimal Quantity,
        Int32 Prints 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PalletId", DbType.Int32, PalletId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
        db.AddInParameter(dbCommand, "Weight", DbType.Decimal, Weight);
        db.AddInParameter(dbCommand, "Tare", DbType.Decimal, Tare);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);
        db.AddInParameter(dbCommand, "Prints", DbType.Int32, Prints);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertPallet
 
    #region SearchPallet
    /// <summary>
    ///   Selects rows from the Pallet table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="PalletId"></param>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="LocationId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchPallet
    (
        string connectionStringName,
        Int32 PalletId,
        Int32 StorageUnitBatchId,
        Int32 LocationId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "PalletId", DbType.Int32, PalletId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, LocationId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchPallet
 
    #region ListPallet
    /// <summary>
    ///   Lists rows from the Pallet table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListPallet
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pallet_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListPallet
 
    #region ParameterPallet
    /// <summary>
    ///   Lists rows from the Pallet table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterPallet
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterPallet
}
