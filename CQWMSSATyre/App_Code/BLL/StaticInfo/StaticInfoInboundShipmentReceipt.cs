using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoInboundShipmentReceipt
///   Create By      : Grant Schultz
///   Date Created   : 10 Dec 2007 15:15:08
/// </summary>
/// <remarks>
///   Inserts a row into the InboundShipmentReceipt table.
///   Selects rows from the InboundShipmentReceipt table.
///   Searches for rows from the InboundShipmentReceipt table.
///   Updates a rows in the InboundShipmentReceipt table.
///   Deletes a row from the InboundShipmentReceipt table.
/// </remarks>
/// <param>
public class StaticInfoInboundShipmentReceipt
{
    #region "Constructor Logic"
    public StaticInfoInboundShipmentReceipt()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetInboundShipmentReceipt
    /// <summary>
    ///   Selects rows from the InboundShipmentReceipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ReceiptId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetInboundShipmentReceipt
    (
        string connectionStringName,
        Int32 InboundShipmentId,
        Int32 ReceiptId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipmentReceipt_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetInboundShipmentReceipt
 
    #region DeleteInboundShipmentReceipt
    /// <summary>
    ///   Deletes a row from the InboundShipmentReceipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ReceiptId"></param>
    /// <returns>bool</returns>
    public bool DeleteInboundShipmentReceipt
    (
        string connectionStringName,
        Int32 InboundShipmentId,
        Int32 ReceiptId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipmentReceipt_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteInboundShipmentReceipt
 
    #region UpdateInboundShipmentReceipt
    /// <summary>
    ///   Updates a row in the InboundShipmentReceipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ReceiptId"></param>
    /// <returns>bool</returns>
    public bool UpdateInboundShipmentReceipt
    (
        string connectionStringName,
        Int32 InboundShipmentId,
        Int32 ReceiptId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipmentReceipt_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateInboundShipmentReceipt
 
    #region InsertInboundShipmentReceipt
    /// <summary>
    ///   Inserts a row into the InboundShipmentReceipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ReceiptId"></param>
    /// <returns>bool</returns>
    public bool InsertInboundShipmentReceipt
    (
        string connectionStringName,
        Int32 InboundShipmentId,
        Int32 ReceiptId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipmentReceipt_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertInboundShipmentReceipt
 
    #region SearchInboundShipmentReceipt
    /// <summary>
    ///   Selects rows from the InboundShipmentReceipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ReceiptId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchInboundShipmentReceipt
    (
        string connectionStringName,
        Int32 InboundShipmentId,
        Int32 ReceiptId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipmentReceipt_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchInboundShipmentReceipt
 
    #region ListInboundShipmentReceipt
    /// <summary>
    ///   Lists rows from the InboundShipmentReceipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListInboundShipmentReceipt
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipmentReceipt_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListInboundShipmentReceipt
 
    #region ParameterInboundShipmentReceipt
    /// <summary>
    ///   Lists rows from the InboundShipmentReceipt table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterInboundShipmentReceipt
    (
        string connectionStringName 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_InboundShipmentReceipt_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterInboundShipmentReceipt
}
