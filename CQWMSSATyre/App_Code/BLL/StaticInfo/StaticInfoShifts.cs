using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoShifts
///   Create By      : Grant Schultz
///   Date Created   : 29 Apr 2014 10:12:23
/// </summary>
/// <remarks>
///   Inserts a row into the Shifts table.
///   Selects rows from the Shifts table.
///   Searches for rows from the Shifts table.
///   Updates a rows in the Shifts table.
///   Deletes a row from the Shifts table.
/// </remarks>
/// <param>
public class StaticInfoShifts
{
    #region "Constructor Logic"
    public StaticInfoShifts()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetShifts
    /// <summary>
    ///   Selects rows from the Shifts table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShiftId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetShifts
    (
        string connectionStringName,
        Int32 ShiftId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shifts_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShiftId", DbType.Int32, ShiftId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetShifts
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shifts_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetShifts
 
    #region DeleteShifts
    /// <summary>
    ///   Deletes a row from the Shifts table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShiftId"></param>
    /// <returns>bool</returns>
    public bool DeleteShifts
    (
        string connectionStringName,
        Int32 ShiftId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shifts_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShiftId", DbType.Int32, ShiftId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteShifts
 
    #region UpdateShifts
    /// <summary>
    ///   Updates a row in the Shifts table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShiftId"></param>
    /// <param name="Starttime"></param>
    /// <param name="EndTime"></param>
    /// <returns>bool</returns>
    public bool UpdateShifts
    (
        string connectionStringName,
        Int32 ShiftId,
        Int32 Starttime,
        Int32 EndTime 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shifts_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShiftId", DbType.Int32, ShiftId);
        db.AddInParameter(dbCommand, "Starttime", DbType.Int32, Starttime);
        db.AddInParameter(dbCommand, "EndTime", DbType.Int32, EndTime);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateShifts
 
    #region InsertShifts
    /// <summary>
    ///   Inserts a row into the Shifts table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShiftId"></param>
    /// <param name="Starttime"></param>
    /// <param name="EndTime"></param>
    /// <returns>bool</returns>
    public bool InsertShifts
    (
        string connectionStringName,
        Int32 ShiftId,
        Int32 Starttime,
        Int32 EndTime 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shifts_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShiftId", DbType.Int32, ShiftId);
        db.AddInParameter(dbCommand, "Starttime", DbType.Int32, Starttime);
        db.AddInParameter(dbCommand, "EndTime", DbType.Int32, EndTime);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertShifts
 
    #region SearchShifts
    /// <summary>
    ///   Selects rows from the Shifts table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShiftId"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchShifts
    (
        string connectionStringName,
        Int32 ShiftId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shifts_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShiftId", DbType.Int32, ShiftId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchShifts
 
    #region PageSearchShifts
    /// <summary>
    ///   Selects rows from the Shifts table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ShiftId"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchShifts
    (
        string connectionStringName,
        Int32 ShiftId,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shifts_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "ShiftId", DbType.Int32, ShiftId);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchShifts
 
    #region ListShifts
    /// <summary>
    ///   Lists rows from the Shifts table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListShifts
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shifts_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListShifts
 
    #region ParameterShifts
    /// <summary>
    ///   Lists rows from the Shifts table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterShifts
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_Shifts_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterShifts
}
