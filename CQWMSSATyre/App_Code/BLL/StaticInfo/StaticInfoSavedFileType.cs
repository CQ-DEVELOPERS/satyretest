using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoSavedFileType
///   Create By      : Grant Schultz
///   Date Created   : 16 Feb 2013 22:21:00
/// </summary>
/// <remarks>
///   Inserts a row into the SavedFileType table.
///   Selects rows from the SavedFileType table.
///   Searches for rows from the SavedFileType table.
///   Updates a rows in the SavedFileType table.
///   Deletes a row from the SavedFileType table.
/// </remarks>
/// <param>
public class StaticInfoSavedFileType
{
    #region "Constructor Logic"
    public StaticInfoSavedFileType()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetSavedFileType
    /// <summary>
    ///   Selects rows from the SavedFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SavedFileTypeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetSavedFileType
    (
        string connectionStringName,
        Int32 SavedFileTypeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SavedFileType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SavedFileTypeId", DbType.Int32, SavedFileTypeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetSavedFileType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SavedFileType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetSavedFileType
 
    #region DeleteSavedFileType
    /// <summary>
    ///   Deletes a row from the SavedFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SavedFileTypeId"></param>
    /// <returns>bool</returns>
    public bool DeleteSavedFileType
    (
        string connectionStringName,
        Int32 SavedFileTypeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SavedFileType_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SavedFileTypeId", DbType.Int32, SavedFileTypeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteSavedFileType
 
    #region UpdateSavedFileType
    /// <summary>
    ///   Updates a row in the SavedFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SavedFileTypeId"></param>
    /// <param name="SavedFileTypeCode"></param>
    /// <param name="SavedFileType"></param>
    /// <returns>bool</returns>
    public bool UpdateSavedFileType
    (
        string connectionStringName,
        Int32 SavedFileTypeId,
        String SavedFileTypeCode,
        String SavedFileType 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SavedFileType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SavedFileTypeId", DbType.Int32, SavedFileTypeId);
        db.AddInParameter(dbCommand, "SavedFileTypeCode", DbType.String, SavedFileTypeCode);
        db.AddInParameter(dbCommand, "SavedFileType", DbType.String, SavedFileType);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateSavedFileType
 
    #region InsertSavedFileType
    /// <summary>
    ///   Inserts a row into the SavedFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SavedFileTypeId"></param>
    /// <param name="SavedFileTypeCode"></param>
    /// <param name="SavedFileType"></param>
    /// <returns>bool</returns>
    public bool InsertSavedFileType
    (
        string connectionStringName,
        Int32 SavedFileTypeId,
        String SavedFileTypeCode,
        String SavedFileType 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SavedFileType_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SavedFileTypeId", DbType.Int32, SavedFileTypeId);
        db.AddInParameter(dbCommand, "SavedFileTypeCode", DbType.String, SavedFileTypeCode);
        db.AddInParameter(dbCommand, "SavedFileType", DbType.String, SavedFileType);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertSavedFileType
 
    #region SearchSavedFileType
    /// <summary>
    ///   Selects rows from the SavedFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SavedFileTypeId"></param>
    /// <param name="SavedFileTypeCode"></param>
    /// <param name="SavedFileType"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchSavedFileType
    (
        string connectionStringName,
        Int32 SavedFileTypeId,
        String SavedFileTypeCode,
        String SavedFileType 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SavedFileType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SavedFileTypeId", DbType.Int32, SavedFileTypeId);
        db.AddInParameter(dbCommand, "SavedFileTypeCode", DbType.String, SavedFileTypeCode);
        db.AddInParameter(dbCommand, "SavedFileType", DbType.String, SavedFileType);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchSavedFileType
 
    #region PageSearchSavedFileType
    /// <summary>
    ///   Selects rows from the SavedFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="SavedFileTypeId"></param>
    /// <param name="SavedFileTypeCode"></param>
    /// <param name="SavedFileType"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchSavedFileType
    (
        string connectionStringName,
        Int32 SavedFileTypeId,
        String SavedFileTypeCode,
        String SavedFileType,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SavedFileType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "SavedFileTypeId", DbType.Int32, SavedFileTypeId);
        db.AddInParameter(dbCommand, "SavedFileTypeCode", DbType.String, SavedFileTypeCode);
        db.AddInParameter(dbCommand, "SavedFileType", DbType.String, SavedFileType);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchSavedFileType
 
    #region ListSavedFileType
    /// <summary>
    ///   Lists rows from the SavedFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListSavedFileType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SavedFileType_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListSavedFileType
 
    #region ParameterSavedFileType
    /// <summary>
    ///   Lists rows from the SavedFileType table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterSavedFileType
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_SavedFileType_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterSavedFileType
}
