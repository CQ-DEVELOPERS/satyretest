using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
 
/// <summary>
///   Summary description for StaticInfoTransportMode
///   Create By      : Grant Schultz
///   Date Created   : 16 Feb 2013 22:21:01
/// </summary>
/// <remarks>
///   Inserts a row into the TransportMode table.
///   Selects rows from the TransportMode table.
///   Searches for rows from the TransportMode table.
///   Updates a rows in the TransportMode table.
///   Deletes a row from the TransportMode table.
/// </remarks>
/// <param>
public class StaticInfoTransportMode
{
    #region "Constructor Logic"
    public StaticInfoTransportMode()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"
 
    #region GetTransportMode
    /// <summary>
    ///   Selects rows from the TransportMode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TransportModeId"></param>
    /// <returns>DataSet</returns>
    public DataSet GetTransportMode
    (
        string connectionStringName,
        Int32 TransportModeId 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TransportMode_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TransportModeId", DbType.Int32, TransportModeId);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    public DataSet GetTransportMode
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TransportMode_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion GetTransportMode
 
    #region DeleteTransportMode
    /// <summary>
    ///   Deletes a row from the TransportMode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TransportModeId"></param>
    /// <returns>bool</returns>
    public bool DeleteTransportMode
    (
        string connectionStringName,
        Int32 TransportModeId 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TransportMode_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TransportModeId", DbType.Int32, TransportModeId);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion DeleteTransportMode
 
    #region UpdateTransportMode
    /// <summary>
    ///   Updates a row in the TransportMode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TransportModeId"></param>
    /// <param name="TransportModeCode"></param>
    /// <param name="TransportMode"></param>
    /// <returns>bool</returns>
    public bool UpdateTransportMode
    (
        string connectionStringName,
        Int32 TransportModeId,
        String TransportModeCode,
        String TransportMode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TransportMode_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TransportModeId", DbType.Int32, TransportModeId);
        db.AddInParameter(dbCommand, "TransportModeCode", DbType.String, TransportModeCode);
        db.AddInParameter(dbCommand, "TransportMode", DbType.String, TransportMode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion UpdateTransportMode
 
    #region InsertTransportMode
    /// <summary>
    ///   Inserts a row into the TransportMode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TransportModeId"></param>
    /// <param name="TransportModeCode"></param>
    /// <param name="TransportMode"></param>
    /// <returns>bool</returns>
    public bool InsertTransportMode
    (
        string connectionStringName,
        Int32 TransportModeId,
        String TransportModeCode,
        String TransportMode 
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TransportMode_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TransportModeId", DbType.Int32, TransportModeId);
        db.AddInParameter(dbCommand, "TransportModeCode", DbType.String, TransportModeCode);
        db.AddInParameter(dbCommand, "TransportMode", DbType.String, TransportMode);
 
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
 
            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);
 
                result = true;
            }
            catch { }
 
            connection.Close();
 
            return result;
        }
    }
    #endregion InsertTransportMode
 
    #region SearchTransportMode
    /// <summary>
    ///   Selects rows from the TransportMode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TransportModeId"></param>
    /// <param name="TransportModeCode"></param>
    /// <param name="TransportMode"></param>
    /// <returns>DataSet</returns>
    public DataSet SearchTransportMode
    (
        string connectionStringName,
        Int32 TransportModeId,
        String TransportModeCode,
        String TransportMode 
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TransportMode_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TransportModeId", DbType.Int32, TransportModeId);
        db.AddInParameter(dbCommand, "TransportModeCode", DbType.String, TransportModeCode);
        db.AddInParameter(dbCommand, "TransportMode", DbType.String, TransportMode);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion SearchTransportMode
 
    #region PageSearchTransportMode
    /// <summary>
    ///   Selects rows from the TransportMode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="TransportModeId"></param>
    /// <param name="TransportModeCode"></param>
    /// <param name="TransportMode"></param>
    /// <returns>DataSet</returns>
    public DataSet PageSearchTransportMode
    (
        string connectionStringName,
        Int32 TransportModeId,
        String TransportModeCode,
        String TransportMode,
        int pageSize,
        int pageNumber
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TransportMode_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // Add Parameters
        db.AddInParameter(dbCommand, "TransportModeId", DbType.Int32, TransportModeId);
        db.AddInParameter(dbCommand, "TransportModeCode", DbType.String, TransportModeCode);
        db.AddInParameter(dbCommand, "TransportMode", DbType.String, TransportMode);
        db.AddInParameter(dbCommand, "PageSize", DbType.Int32, pageSize);
        db.AddInParameter(dbCommand, "PageNumber", DbType.Int32, pageNumber);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion PageSearchTransportMode
 
    #region ListTransportMode
    /// <summary>
    ///   Lists rows from the TransportMode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ListTransportMode
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TransportMode_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ListTransportMode
 
    #region ParameterTransportMode
    /// <summary>
    ///   Lists rows from the TransportMode table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns>DataSet</returns>
    public DataSet ParameterTransportMode
    (
        string connectionStringName
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
 
        string sqlCommand = connectionStringName + ".dbo.p_TransportMode_Parameter";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
 
        // DataSet that will hold the returned results
        DataSet dataSet = null;
 
        dataSet = db.ExecuteDataSet(dbCommand);
 
        return dataSet;
    }
    #endregion ParameterTransportMode
}
