using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for OutboundDocumentType
/// </summary>
public class ExternalCompany
{
    #region Constructor Logic
    public ExternalCompany()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region SearchExternalCompanies
    public DataSet SearchExternalCompanies(string connectionStringName, string externalCompany, string externalCompanyCode, string externalCompanyTypeCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        
        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Search_Input";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "externalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "externalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "externalCompanyTypeCode", DbType.String, externalCompanyTypeCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchExternalCompanies

    #region ListAllExternalCompanies
    public DataSet SelectExternalCompanies(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ListAllExternalCompanies

    #region ListOperatorExternalCompanies
    public DataSet SelectOperatorExternalCompanies(string connectionStringName, int OperatorID)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Parameter_OperatorExternalCompany";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int16, OperatorID);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ListOperatorExternalCompanies

    #region GetExternalCompanyType
    public DataSet GetExternalCompanyType(string connectionStringName, int externalCompanyTypeId, string externalCompanyTypeCode, string externalCompanyType)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Get_ExternalCompanyType";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.String, externalCompanyTypeId);
        db.AddInParameter(dbCommand, "externalCompanyTypeCode", DbType.String, externalCompanyTypeCode);
        db.AddInParameter(dbCommand, "ExternalCompanyType", DbType.String, externalCompanyType);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetExternalCompanyType

    #region UpdateExternalCompany
    /// <summary>
    ///   Updates a row in the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <param name="RouteId"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="Rating"></param>
    /// <param name="RedeliveryIndicator"></param>
    /// <param name="QualityAssuranceIndicator"></param>
    /// <returns>bool</returns>
    public bool UpdateExternalCompany
    (
        string connectionStringName,
        Int32 ExternalCompanyId,
        Int32 ExternalCompanyTypeId,
        Int32 RouteId,
        String ExternalCompany,
        String ExternalCompanyCode,
        Int32 Rating,
        Boolean RedeliveryIndicator,
        Boolean QualityAssuranceIndicator
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "Rating", DbType.Int32, Rating);
        db.AddInParameter(dbCommand, "RedeliveryIndicator", DbType.Boolean, RedeliveryIndicator);
        db.AddInParameter(dbCommand, "QualityAssuranceIndicator", DbType.Boolean, QualityAssuranceIndicator);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateExternalCompany

    #region DeleteExternalCompany
    /// <summary>
    ///   Deletes a row from the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <returns>bool</returns>
    public bool DeleteExternalCompany
    (
        string connectionStringName,
        Int32 ExternalCompanyId
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DeleteExternalCompany

    #region InsertExternalCompany
    /// <summary>
    ///   Inserts a row into the ExternalCompany table.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="ExternalCompanyId"></param>
    /// <param name="ExternalCompanyTypeId"></param>
    /// <param name="RouteId"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="Rating"></param>
    /// <param name="RedeliveryIndicator"></param>
    /// <param name="QualityAssuranceIndicator"></param>
    /// <returns>bool</returns>
    public bool InsertExternalCompany
    (
        string connectionStringName,
        Int32 ExternalCompanyId,
        Int32 ExternalCompanyTypeId,
        Int32 RouteId,
        String ExternalCompany,
        String ExternalCompanyCode,
        Int32 Rating,
        Boolean RedeliveryIndicator,
        Boolean QualityAssuranceIndicator
    )
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, ExternalCompanyId);
        db.AddInParameter(dbCommand, "ExternalCompanyTypeId", DbType.Int32, ExternalCompanyTypeId);
        db.AddInParameter(dbCommand, "RouteId", DbType.Int32, RouteId);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "Rating", DbType.Int32, Rating);
        db.AddInParameter(dbCommand, "RedeliveryIndicator", DbType.Boolean, RedeliveryIndicator);
        db.AddInParameter(dbCommand, "QualityAssuranceIndicator", DbType.Boolean, QualityAssuranceIndicator);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion InsertExternalCompany

    #region ExternalCompaniesByType
    public DataSet ExternalCompaniesByType(string connectionStringName, string externalCompanyTypeCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Search_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "externalCompanyTypeCode", DbType.String, externalCompanyTypeCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ExternalCompaniesByType


}
