using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class Principal : BaseBLL
{
  public Principal()
  {
    //
    // TODO: Add constructor logic here
    //
  }

  /// <summary>
  /// Retreives a list of Principals
  /// </summary>
  /// <param name="connectionStringName"></param>
  /// <returns></returns>
  public DataSet GetPrincipalList( string connectionStringName )
  {
    // Create the Database object, passing it the required database service.
    Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

    string sqlCommand = connectionStringName + ".dbo.p_Principal_List";
    DbCommand dbCommand = db.GetStoredProcCommand( sqlCommand );

    // DataSet that will hold the returned results
    DataSet dataSet = null;

    dataSet = db.ExecuteDataSet( dbCommand );

    return dataSet;
  }

  /// <summary>
  /// Retreives a list of Principals as Parameters
  /// </summary>
  /// <param name="connectionStringName"></param>
  /// <returns></returns>
  public DataSet GetPrincipalParameter( string connectionStringName )
  {
    // Create the Database object, passing it the required database service.
    Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

    string sqlCommand = connectionStringName + ".dbo.p_Principal_Parameter";
    DbCommand dbCommand = db.GetStoredProcCommand( sqlCommand );

    // DataSet that will hold the returned results
    DataSet dataSet = null;

    dataSet = db.ExecuteDataSet( dbCommand );

    return dataSet;
  }

  //public DataSet GetAllPrincipals()
  //{
  //  var data = ExecuteDataSet( "p_Principal_Select" );

  //  return data;
  //}
  #region ListPrincipals
  public DataSet ListPrincipals(string connectionStringName)
  {
      // Create the Database object, passing it the required database service.
      Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

      string sqlCommand = connectionStringName + ".dbo.p_List_Principals";
      DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

      // DataSet that will hold the returned results
      DataSet dataSet = null;

      dataSet = db.ExecuteDataSet(dbCommand);

      return dataSet;
  }
  #endregion ListPrincipals

  #region PrincipalSelect
  public DataSet PrincipalSelect(string connectionStringName, string userName)
  {
      // Create the Database object, passing it the required database service.
      Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

      string sqlCommand = connectionStringName + ".dbo.p_Principal_Select_by_Operator";
      DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

      db.AddInParameter(dbCommand, "UserName", DbType.String, userName);

      // DataSet that will hold the returned results
      DataSet dataSet = null;

      dataSet = db.ExecuteDataSet(dbCommand);

      return dataSet;
  }
  public DataSet GetAllPrincipals()
  {
    var data = ExecuteDataSet( "p_Principal_Select" );

    return data;
  }
  #endregion PrincipalSelect

}
