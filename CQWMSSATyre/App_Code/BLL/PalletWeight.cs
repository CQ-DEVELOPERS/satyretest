using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class PalletWeight
{
    #region Constructor Logic
    public PalletWeight()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetWeights
    public DataSet GetWeights(string connectionStringName, int jobId)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Weight_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetWeights

    #region UpdateWeights
    public bool UpdateWeights(string connectionStringName, int jobId, decimal tareWeight, decimal nettWeight, decimal grossWeight)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Weight_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "tareWeight", DbType.Decimal, tareWeight);
        db.AddInParameter(dbCommand, "nettWeight", DbType.Decimal, nettWeight);
        db.AddInParameter(dbCommand, "grossWeight", DbType.Decimal, grossWeight);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion UpdateWeights

    #region Reject
    public bool Reject(string connectionStringName, int jobId, int reasonId, int operatorId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Reject";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "reasonId", DbType.Int32, reasonId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion Reject

    #region GetPackaging
    public DataSet GetPackaging(string connectionStringName, int jobId)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Packaging_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetPackagingNull(string connectionStringName, int jobId, int storageUnitId)
    {
        bool showNull = true;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Packaging_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "null", DbType.Boolean, showNull);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetPackaging

    #region SearchPackaging
    public DataSet SearchPackaging(string connectionStringName, int jobId)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Packaging_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchPackaging

    #region InsertPackaging
    public bool InsertPackaging(string connectionStringName, int jobId, int storageUnitBatchId, Decimal quantity, int operatorId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Packaging_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, storageUnitBatchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion InsertPackaging

    #region GetInstructions
    /// <summary>
    /// Gets outstanding Store instructions within a date range.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="statusId"></param>
    /// <returns></returns>
    public DataSet GetInstructions(string connectionStringName, string barcode)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Weight_Job_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructions

    #region CheckWeights
    public int CheckWeights(string connectionStringName, int jobId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");


        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Weight_Check";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = int.Parse(db.ExecuteScalar(dbCommand).ToString());
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CheckWeights"

    #region GetPallet
    /// <summary>
    /// Gets outstanding Store instructions within a date range.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <param name="statusId"></param>
    /// <returns></returns>
    public DataSet GetPallet(string connectionStringName, string barcode)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Weight_Pallet_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetPallet

    #region UpdatePallet
    public bool UpdatePallet(string connectionStringName, int palletId, Decimal tareWeight, Decimal nettWeight, Decimal grossWeight)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Weight_Pallet_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "tareWeight", DbType.Decimal, tareWeight);
        db.AddInParameter(dbCommand, "nettWeight", DbType.Decimal, nettWeight);
        db.AddInParameter(dbCommand, "grossWeight", DbType.Decimal, grossWeight);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion UpdatePallet
}
