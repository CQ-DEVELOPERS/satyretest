using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Product
/// </summary>
public class Production
{
    #region Constructor Logic

    public Production()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region ProductionLines
    /// <summary>
    /// Searches for Production Lines.
    /// </summary>
    public DataSet ProductionLines(string connectionStringName, int warehouseId, int palletId, int jobId, DateTime fromDate, DateTime toDate, int statusId, string productCode, string product, string skuCode, string batch)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Lines";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "statusId", DbType.Int32, statusId);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "skuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ProductionLines

    #region UpdateProductionLines
    /// <summary>
    /// Updates ConfirmedQuantity and OperatorId of Instruction
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="confirmedQuantity"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool UpdateProductionLines(string connectionStringName, int instructionId, int jobId, Decimal confirmedQuantity, int operatorId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Putaway_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion UpdateProductionLines

    #region DeleteProductionLines
    /// <summary>
    /// Deletes Instructions for Movements and Replenishments
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="jobId"></param>
    /// <returns></returns>
    public bool DeleteProductionLines(string connectionStringName, int instructionId, int jobId, int operatorId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Line_Delete";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(createCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(createCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(createCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteProductionLines"

    #region SearchProductionLines
    /// <summary>
    /// Retrieves and InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundDocumentId"></param>
    /// <returns></returns>
    public DataSet SearchProductionLines(string connectionStringName, int storageUnitBatchId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Line_Create_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.String, storageUnitBatchId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchProductionLines

    #region CreateInboundLine
    /// <summary>
    /// Inserts an InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="operatorId"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="quantity"></param>
    /// <param name="batchId"></param>
    /// <returns></returns>
    public bool CreateInboundLine(string connectionStringName,
                                    int operatorId,
                                    int inboundDocumentId,
                                    int storageUnitId,
                                    Decimal quantity,
                                    int batchId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Line_Create_InboundLine";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorId", DbType.String, operatorId);
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateInboundLine

    #region UpdateInboundLine
    /// <summary>
    /// Updates an InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundLineId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool UpdateInboundLine(string connectionStringName, int inboundLineId, Decimal quantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Line_Edit_InboundLine";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, inboundLineId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateInboundLine"

    #region DeleteInboundLine
    /// <summary>
    /// Deletes an InboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundLineId"></param>
    /// <returns></returns>
    public bool DeleteInboundLine(string connectionStringName, int inboundLineId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Line_Delete_InboundLine";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundLineId", DbType.Int32, inboundLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteInboundLine"

    #region RegisterProductionPallets
    /// <summary>
    /// Inserts an InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="operatorId"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="quantity"></param>
    /// <param name="batchId"></param>
    /// <returns></returns>
    public bool RegisterProductionPallets(string connectionStringName,
                                             int warehouseId,
                                             int operatorId,
                                             int storageUnitBatchId,
                                             Decimal quantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Line_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.String, warehouseId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.String, operatorId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion RegisterProductionPallets

    #region SplitInstruction
    /// <summary>
    /// Inserts an InboundLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="operatorId"></param>
    /// <param name="inboundDocumentId"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="quantity"></param>
    /// <param name="batchId"></param>
    /// <returns></returns>
    public bool SplitInstruction(string connectionStringName,
                                             int instructionId,
                                             Decimal quantity,
                                             int reasonId,
                                             int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Instruction_Split";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "instructionId", DbType.String, instructionId);
        db.AddInParameter(dbCommand, "splitQuantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "reasonId", DbType.Int32, reasonId);
        db.AddInParameter(dbCommand, "operatorId", DbType.String, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SplitInstruction

    #region GetReceivingDocuments
    /// <summary>
    /// Retreives Receipt Documents available for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    public DataSet GetReceivingDocuments(string connectionStringName,
                                        int warehouseId,
                                        string orderNumber,
                                        string productCode,
                                        string batch,
                                        //int statusId,
                                        DateTime fromDate,
                                        DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Authorisation_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        //db.AddInParameter(dbCommand, "StatusId", DbType.Int32, statusId);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetReceivingDocuments"
}
