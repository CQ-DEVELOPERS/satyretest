using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

/// <summary>
/// Summary description for Transact
/// </summary>
public class ProductCheck
{
    public string skuCode = "1234567890";
    public Decimal quantity = -1;

    #region Constructor Logic
    public ProductCheck()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region ConfirmPallet
    public DataSet ConfirmPallet(string connectionStringName, int warehouseId, string referenceNumber)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ConfirmPallet

    #region ConfirmProduct
    public DataSet ConfirmProduct(string connectionStringName, int warehouseId, int jobId, string barcode)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Barcode";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ConfirmProduct

    #region ConfirmBatch
    public DataSet ConfirmBatch(string connectionStringName, int warehouseId, int jobId, int storageUnitId, string batch)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Batch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ConfirmBatch

    #region ConfirmQuantity
    public DataSet ConfirmQuantity(string connectionStringName, int warehouseId, int jobId, int storageUnitBatchId, Decimal quantity, bool scanMode, string barcode)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "scanMode", DbType.Boolean, scanMode);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ConfirmQuantity

    #region ConfirmFinished
    public int ConfirmFinished(string connectionStringName, int jobId, int operatorId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Finished";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmFinished

    #region NewBox
    public int NewBox(string connectionStringName, int jobId, int operatorId, string referenceNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_New_Box";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion NewBox

    #region ProductDetails
    public DataSet ProductDetails(string connectionStringName, int jobId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Product_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ProductDetails

    #region GetLineStatus
    public int GetLineStatus(string connectionStringName, int warehouseId, int jobId, int storageUnitId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Get_Status";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetLineStatus

    #region DefaultQuantity
    public int DefaultQuantity(string connectionStringName, int warehouseId, string barCode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Get_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "barCode", DbType.String, barCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DefaultQuantity

    #region GetContainerType
    /// <summary>
    /// Get the jobs container type
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <returns></returns>
    public int GetContainerType(string connectionStringName
                                    , int JobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        int ContainerTypeId = 0;

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_ContainerType";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddOutParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);

        db.ExecuteNonQuery(dbCommand);

        ContainerTypeId = (int)db.GetParameterValue(dbCommand, "@ContainerTypeId");

        return ContainerTypeId;
    }
    #endregion

    #region SetContainerType
    /// <summary>
    /// Get the jobs container type
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <returns></returns>
    public int SetContainerType(string connectionStringName
                                    , int JobId
                                    , int ContainerTypeId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_ContainerType_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, JobId);
        db.AddInParameter(dbCommand, "ContainerTypeId", DbType.Int32, ContainerTypeId);

        db.ExecuteNonQuery(dbCommand);

        return ContainerTypeId;
    }
    #endregion

    #region SearchContainerTypes
    /// <summary>
    /// Searches for a Container Type
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="JobId"></param>
    /// <returns></returns>
    public DataSet SearchContainerTypes(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchContainerTypes"

    #region ConfirmBOMPallet
    public DataSet ConfirmBOMPallet(string connectionStringName, int warehouseId, string referenceNumber)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_BOM_Product_Check_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion

    #region ConfirmBOMProductDetails
    public DataSet ConfirmBOMProductDetails(string connectionStringName, int BOMInstructionId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_BOM_Product_Check_Product_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, BOMInstructionId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ConfirmBOMProductDetails

    #region ConfirmBOMProduct
    public DataSet ConfirmBOMProduct(string connectionStringName, int warehouseId, int jobId, string barcode)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        //db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        //db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        //db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion

    #region ConfirmBOMBatch
    public DataSet ConfirmBOMBatch(string connectionStringName, int warehouseId, int BOMInstructionId, int storageUnitId, string batch)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_BOM_Product_Check_Batch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, BOMInstructionId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion

    #region ConfirmBOMQuantity
    public DataSet ConfirmBOMQuantity(string connectionStringName, int warehouseId, int BOMInstructionId, int storageUnitBatchId, Decimal quantity)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_BOM_Product_Check_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, BOMInstructionId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion

    #region ConfirmBOMFinished
    public string ConfirmBOMFinished(string connectionStringName, int BOMInstructionId, int OperatorId, string ReferenceNumber)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_BOM_Product_Check_Finished";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, BOMInstructionId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);
        //db.AddInParameter(dbCommand, "ReferenceNumber", DbType.String, ReferenceNumber);

        return db.ExecuteScalar(dbCommand).ToString();
    }
    #endregion

    #region StoreLocation
    public bool StoreLocation(string connectionStringName, int jobId, string storeLocation)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_StoreLocation";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storeLocation", DbType.String, storeLocation);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion StoreLocation

    #region StoreLocationGet
    public string StoreLocationGet(string connectionStringName, int jobId)
    {
        string storeLocation = "";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Get_StoreLocation";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        // Set storeLocation as an output parameter
        db.AddOutParameter(dbCommand, "storeLocation", DbType.String, 255);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get storeLocation output parameter
                storeLocation = db.GetParameterValue(dbCommand, "storeLocation").ToString();
            }
            catch (Exception ex)
            {
                string str = ex.Message.ToString();
            }

            connection.Close();

            return storeLocation;
        }
    }
    #endregion StoreLocationGet

    #region DespatchAdvice
    public int DespatchAdvice(string connectionStringName, int jobId)
    {
        int issueId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Despatch_Advice";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        // Set issueId as an output parameter
        db.AddOutParameter(dbCommand, "issueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get issueId output parameter
                issueId = (int)db.GetParameterValue(dbCommand, "issueId");
            }
            catch (Exception ex)
            {
                string str = ex.Message.ToString();
            }

            connection.Close();

            return issueId;
        }
    }
    #endregion DespatchAdvice

    #region CheckingStatusSearch
    public DataSet CheckingStatusSearch(string connectionStringName, string orderNumber, int jobId, string referenceNumber, int storeLocationId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Checking_Status_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "storeLocationId", DbType.Int32, storeLocationId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion CheckingStatusSearch

    #region ConfirmWeight
    public bool ConfirmWeight(string connectionStringName, int jobId, decimal weight)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Weight";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "weight", DbType.Decimal, weight);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmWeight

    #region LinkedJobs
    public DataSet LinkedJobs(string connectionStringName, int jobId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Linked_Jobs";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion LinkedJobs

    #region JobsList
    public DataSet JobsList(string connectionStringName, int jobId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Jobs_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion JobsList

    #region CreateJobs
    public bool CreateJobs(string connectionStringName, int jobId, int jobs)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Create_Jobs";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "jobs", DbType.Int32, jobs);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion CreateJobs

    #region SearchJob
    public DataSet SearchJob(string connectionStringName, int warehouseId, int operatorId, string referenceNumber, int newJobId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Job_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
		
		dbCommand.CommandTimeout = 0;
		
        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "newJobId", DbType.Int32, newJobId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion SearchJob

    #region UpdateJob
    public int UpdateJob(string connectionStringName, int operatorId, int jobId, int instructionId, decimal packQuantity, int newJobId, int boxes, decimal weight, int issueLineId, string orderNumber)
    {
        int result = -1;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Job_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "packQuantity", DbType.Decimal, packQuantity);
        db.AddParameter(dbCommand, "newJobId", DbType.Int32, ParameterDirection.InputOutput, "newJobId", DataRowVersion.Current, newJobId);
        db.AddInParameter(dbCommand, "boxes", DbType.Int32, boxes);
        db.AddInParameter(dbCommand, "weight", DbType.Decimal, weight);
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);

        db.ExecuteNonQuery(dbCommand);

        result = (int)db.GetParameterValue(dbCommand, "@newJobId");

        return result;
    }
    #endregion UpdateJob

    #region DeleteJob
    public bool DeleteJob(string connectionStringName, int operatorId, int jobId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_Job_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion DeleteJob

    #region GetJobId
    public int GetJobId(string connectionStringName, int operatorId, string referenceNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_JobId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetJobId

    #region GetNewJobId
    public int GetNewJobId(string connectionStringName, int warehouseId, int operatorId, int jobId, decimal weight)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Check_JobId_New";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "weight", DbType.Decimal, weight);
        db.AddParameter(dbCommand, "newJobId", DbType.Int32, ParameterDirection.InputOutput, "newJobId", DataRowVersion.Current, result);

        db.ExecuteNonQuery(dbCommand);

        result = (int)db.GetParameterValue(dbCommand, "@newJobId");

        return result;
    }
    #endregion GetNewJobId
}