using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Summary description for StaticInfo
/// </summary>
public class StaticInfo
{
    public StaticInfo()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region Product

    public DataSet product_Search(string connectionStringName, string productCode, string product, string SKUCode, string SKU, string barcode, int principalId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Product_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, SKUCode);
        db.AddInParameter(dbCommand, "SKU", DbType.String, SKU);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "principalId", DbType.Int32, principalId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        //if (dataSet.Tables.Count > 0)
        //{
        //    //create an empty dataset row
        //    if (dataSet.Tables[0].Rows.Count == 0)
        //    {
        //        foreach (DataColumn dc in dataSet.Tables[0].Columns)
        //        {
        //            dc.AllowDBNull = true; 
        //        }
        //        dataSet.Tables[0].Columns[0].DefaultValue = -1;
        //        dataSet.Tables[0].Rows.Add(dataSet.Tables[0].NewRow());
        //    }
        //}

        return dataSet;
    }

    public bool product_Update(string connectionStringName,
        int statusId,
        string product,
        string productCode,
        string barcode,
        Decimal minimumQuantity,
        Decimal reorderQuantity,
        Decimal maximumQuantity,
        int curingPeriodDays,
        int shelfLifeDays,
        string qualityAssuranceIndicator,
        string productType,
        decimal overReceipt,
        string hostId,
        Decimal retentionSamples,
        Decimal samples,
        Decimal assaySamples,
        string category,
        string parentProductCode,
        Nullable<int> dangerousGoodsId,
        int principalId,
        int productId)
    {
        bool result = false;

        if (dangerousGoodsId == -1)
        {
            dangerousGoodsId = null;
        }

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Product_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ProductID", DbType.Int32, productId);
        db.AddInParameter(dbCommand, "StatusID", DbType.Int32, statusId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);

        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "MinimumQuantity", DbType.Decimal, minimumQuantity);

        db.AddInParameter(dbCommand, "ReorderQuantity", DbType.Decimal, reorderQuantity);
        db.AddInParameter(dbCommand, "MaximumQuantity", DbType.Decimal, maximumQuantity);
        db.AddInParameter(dbCommand, "CuringPeriodDays", DbType.Int32, curingPeriodDays);

        db.AddInParameter(dbCommand, "ShelfLifeDays", DbType.Int32, shelfLifeDays);
        db.AddInParameter(dbCommand, "QualityAssuranceIndicator", DbType.Boolean, bool.Parse(qualityAssuranceIndicator));

        db.AddInParameter(dbCommand, "ProductType", DbType.String, productType);
        db.AddInParameter(dbCommand, "OverReceipt", DbType.Decimal, overReceipt);
        db.AddInParameter(dbCommand, "HostId", DbType.String, hostId);
        db.AddInParameter(dbCommand, "RetentionSamples", DbType.Decimal, retentionSamples);
        db.AddInParameter(dbCommand, "Samples", DbType.Decimal, samples);
        db.AddInParameter(dbCommand, "AssaySamples", DbType.Decimal, assaySamples);
        db.AddInParameter(dbCommand, "Category", DbType.String, category);
        db.AddInParameter(dbCommand, "ParentProductCode", DbType.String, parentProductCode);
        db.AddInParameter(dbCommand, "DangerousGoodsId", DbType.Int32, dangerousGoodsId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static int product_Add(string connectionStringName,
        int statusId,
        string productCode,
        string product,
        string barcode,                     //null
        Nullable<Decimal> minimumQuantity,     //null 
        Nullable<Decimal> reorderQuantity,     //null 
        Nullable<Decimal> maximumQuantity,     //null 
        Nullable<int> curingPeriodDays,    //null 
        Nullable<int> shelfLifeDays,       //null 
        string qualityAssuranceIndicator,
        string productType,
        Nullable<decimal> overReceipt,         //null
        string hostId,
        Nullable<Decimal> retentionSamples,     //null
        Nullable<Decimal> samples,     //null
        Nullable<Decimal> assaySamples,     //null
        string category,
        string parentProductCode,
        Nullable<int> dangerousGoodsId,
        int principalId
        )
    {
        int result = 0;

        if (dangerousGoodsId == -1)
        {
            dangerousGoodsId = null;
        }

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Product_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddOutParameter(dbCommand, "ProductId", DbType.Int32, 5);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, statusId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "MinimumQuantity", DbType.Decimal, minimumQuantity);
        db.AddInParameter(dbCommand, "ReorderQuantity", DbType.Decimal, reorderQuantity);
        db.AddInParameter(dbCommand, "MaximumQuantity", DbType.Decimal, maximumQuantity);
        db.AddInParameter(dbCommand, "CuringPeriodDays", DbType.Int32, curingPeriodDays);
        db.AddInParameter(dbCommand, "ShelfLifeDays", DbType.Int32, shelfLifeDays);
        db.AddInParameter(dbCommand, "QualityAssuranceIndicator", DbType.Boolean, Boolean.Parse(qualityAssuranceIndicator));
        db.AddInParameter(dbCommand, "ProductType", DbType.String, productType);
        db.AddInParameter(dbCommand, "OverReceipt", DbType.Decimal, overReceipt);
        db.AddInParameter(dbCommand, "HostId", DbType.String, hostId);
        db.AddInParameter(dbCommand, "RetentionSamples", DbType.Decimal, retentionSamples);
        db.AddInParameter(dbCommand, "Samples", DbType.Decimal, samples);
        db.AddInParameter(dbCommand, "AssaySamples", DbType.Decimal, assaySamples);
        db.AddInParameter(dbCommand, "Category", DbType.String, category);
        db.AddInParameter(dbCommand, "ParentProductCode", DbType.String, parentProductCode);
        db.AddInParameter(dbCommand, "DangerousGoodsId", DbType.Int32, dangerousGoodsId);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = int.Parse(db.GetParameterValue(dbCommand, "ProductId").ToString());
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    #endregion

    #region SKU

    public DataSet sku_Search(string connectionStringName, int productId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_StorageUnit_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, productId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public static int sku_Add(string connectionStringName,
        int skuId,
        int productId
        )
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddOutParameter(dbCommand, "StorageUnitId", DbType.Int32, 5);
        db.AddInParameter(dbCommand, "SKUId", DbType.Int32, skuId);
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, productId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = int.Parse(db.GetParameterValue(dbCommand, "PackId").ToString());
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static bool sku_Delete(string connectionStringName, int storageUnitId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static bool sku_Update(string connectionStringName,
        int storageUnitId,
        int skuId,
        string packingCategory,
        string productCategory,
        string pickEmpty,
        Nullable<int> stackingCategory,
        Nullable<int> movementCategory,
        Nullable<int> valueCategory,
        Nullable<int> storingCategory,
        Nullable<int> pickPartPallet,
        Nullable<Decimal> minimumQuantity,
        Nullable<Decimal> reorderQuantity,
        Nullable<Decimal> maximumQuantity,
        string defaultQC,
        Nullable<Decimal> unitPrice,
        string size,
        string colour,
        string style,
        string lotAttributeRule,
        string SerialTracked
        )
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "SkuID", DbType.Int32, skuId);
        db.AddInParameter(dbCommand, "PackingCategory", DbType.String, packingCategory);
        db.AddInParameter(dbCommand, "ProductCategory", DbType.String, productCategory);
        db.AddInParameter(dbCommand, "PickEmpty", DbType.Boolean, Boolean.Parse(pickEmpty));
        db.AddInParameter(dbCommand, "StackingCategory", DbType.Int32, stackingCategory);
        db.AddInParameter(dbCommand, "MovementCategory", DbType.Int32, movementCategory);
        db.AddInParameter(dbCommand, "ValueCategory", DbType.Int32, valueCategory);
        db.AddInParameter(dbCommand, "StoringCategory", DbType.Int32, storingCategory);
        db.AddInParameter(dbCommand, "PickPartPallet", DbType.Int32, pickPartPallet);
        db.AddInParameter(dbCommand, "defaultQC", DbType.Boolean, defaultQC);
        db.AddInParameter(dbCommand, "unitPrice", DbType.Decimal, unitPrice);
        db.AddInParameter(dbCommand, "size", DbType.String, size);
        db.AddInParameter(dbCommand, "colour", DbType.String, colour);
        db.AddInParameter(dbCommand, "style", DbType.String, style);
        db.AddInParameter(dbCommand, "LotAttributeRule", DbType.String, lotAttributeRule);
        db.AddInParameter(dbCommand, "SerialTracked", DbType.Boolean, SerialTracked);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    #endregion

    #region Pack

    public DataSet pack_Search(string connectionStringName, int storageUnitId, int wareHouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Pack_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "WareHouseId", DbType.Int32, wareHouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public bool pack_Update(string connectionStringName,
       int storageUnitId,
       int packTypeId,
       Decimal quantity,
       string barcode,
       Decimal length,
       Decimal width,
       Decimal height,
       Decimal volume,
       Decimal weight,
       Decimal nettWeight,
       Decimal tareWeight,
       int packId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pack_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "PackId", DbType.Int32, packId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "PackTypeId", DbType.String, packTypeId);

        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "Length", DbType.Decimal, length);

        db.AddInParameter(dbCommand, "Width", DbType.Decimal, width);
        db.AddInParameter(dbCommand, "Height", DbType.Decimal, height);
        db.AddInParameter(dbCommand, "Volume", DbType.Decimal, volume);

        db.AddInParameter(dbCommand, "Weight", DbType.Decimal, weight);
        db.AddInParameter(dbCommand, "NettWeight", DbType.Decimal, nettWeight);
        db.AddInParameter(dbCommand, "TareWeight", DbType.Decimal, tareWeight);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static int pack_Add(string connectionStringName, int storageUnitId, int warehouseId)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pack_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddOutParameter(dbCommand, "PackId", DbType.Int32, 5);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "PackTypeId", DbType.Int32, 1);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, 1);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = int.Parse(db.GetParameterValue(dbCommand, "PackId").ToString());
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static bool pack_Delete(string connectionStringName, int packId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pack_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "PackId", DbType.Int32, packId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;

    }
    #endregion

    #region StoragUnitBatch
    public DataSet storageUnitBatch_Search(string connectionStringName, int storageUnitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_StorageUnitBatch_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public static bool storageUnitBatch_Add(string connectionStringName, int storageUnitId, int batchId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddOutParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, 5);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

    public DataSet SearchBatchesByStorageUnit(string connectionStringName, string batch, string eCLNumber, int StartRow, int PageSize)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Batch_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, null);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "ECLNumber", DbType.String, eCLNumber);
        db.AddInParameter(dbCommand, "StartRowIndex", DbType.Int32, StartRow);
        db.AddInParameter(dbCommand, "MaximumRows", DbType.String, PageSize);

        // DataSet that will hold the returned results		
        DataSet genDataSet = null;

        genDataSet = db.ExecuteDataSet(dbCommand);

        return genDataSet;
    }

    public int CountBatchesByStorageUnit(string connectionStringName, string batch, string eCLNumber)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Batch_Search_Count";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, null);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "ECLNumber", DbType.String, eCLNumber);

        // DataSet that will hold the returned results		
        DataSet genDataSet = null;

        genDataSet = db.ExecuteDataSet(dbCommand);

        return int.Parse(genDataSet.Tables[0].Rows[0][0].ToString());
    }

    public bool storageUnitBatch_Delete(string connectionStringName, int storageUnitBatchId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitBatch_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { result = false; }
        }
        //connection.Close();

        return result;
    }

    #endregion

    #region StorageUnitArea

    public DataSet storageUnitArea_Search(string connectionStringName, int storageUnitId, int warehouseID)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_StorageUnitArea_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseID);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public bool storageUnitArea_Update(string connectionStringName,
        int storeOrder,
        int pickOrder,
        int storageUnitId,
        int areaId,
        Decimal minimumQuantity,
        Decimal reorderQuantity,
        Decimal maximumQuantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitArea_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);
        db.AddInParameter(dbCommand, "StoreOrder", DbType.Int32, storeOrder);
        db.AddInParameter(dbCommand, "PickOrder", DbType.Int32, pickOrder);
        db.AddInParameter(dbCommand, "MinimumQuantity", DbType.Decimal, minimumQuantity);
        db.AddInParameter(dbCommand, "ReorderQuantity", DbType.Decimal, reorderQuantity);
        db.AddInParameter(dbCommand, "MaximumQuantity", DbType.Decimal, maximumQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static bool storageUnitArea_Add(string connectionStringName, 
        int storageUnitId, 
        int areaId,
        Decimal minimumQuantity,
        Decimal reorderQuantity,
        Decimal maximumQuantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitArea_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);
        db.AddInParameter(dbCommand, "StoreOrder", DbType.Int32, 1);
        db.AddInParameter(dbCommand, "PickOrder", DbType.Int32, 1);
        db.AddInParameter(dbCommand, "MinimumQuantity", DbType.Decimal, minimumQuantity);
        db.AddInParameter(dbCommand, "ReorderQuantity", DbType.Decimal, reorderQuantity);
        db.AddInParameter(dbCommand, "MaximumQuantity", DbType.Decimal, maximumQuantity);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

    public static bool storageUnitArea_Delete(string connectionStringName, int storageUnitId, int areaId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitArea_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }



    #endregion

    #region storageUnitLocation

    public DataSet storageUnitLocation_Search(string connectionStringName, int storageUnitId, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_StorageUnitLocation_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public bool storageUnitLocation_Update(string connectionStringName,
        int storageUnitId,
                    Decimal minimumQuantity,
                    Decimal handlingQuantity,
                    Decimal maximumQuantity,
                    int locationId
        )
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLocation_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "MinimumQuantity", DbType.Decimal, minimumQuantity);
        db.AddInParameter(dbCommand, "HandlingQuantity", DbType.Decimal, handlingQuantity);
        db.AddInParameter(dbCommand, "MaximumQuantity", DbType.Decimal, maximumQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static bool storageUnitLocation_Add(string connectionStringName, int storageUnitId, int locationId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLocation_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

    public static bool storageUnitLocation_Delete(string connectionStringName, int storageUnitId, int locationId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLocation_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

    #endregion

    #region ListStorageUnitExternalCompanies
    public DataSet SelectStorageUnitExternalCompanies(string connectionStringName,
                                                        int StorageUnitId,
                                                        int ExternalCompanyId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.P_StorageUnit_ExternalCompany_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int16, StorageUnitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ListOperatorExternalCompanies

    #region Lookups

    public DataSet GetPackType(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_PackType";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        //db.AddInParameter(dbCommand, "Type", DbType.String, type);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    //     
    public DataSet GetSku(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SKUCode_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        //db.AddInParameter(dbCommand, "Type", DbType.String, type);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public IEnumerable GetLotAttributeRules()
    {
        DataSet ds = new DataSet();
        List<string> rules = new List<string>() { "None", "AutoGenerate", "CreateOnReceipt" ,"SelectExisting"};
        
        return rules;
    }
    #endregion

    #region Area

    public DataSet Area_Search(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Area_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public bool Area_Update(string connectionStringName,
                            int warehouseId,
                            string area,
                            string areaCode,
                            bool stockOnHand,
                            int areaId,
                            bool replenish,
                            int areaSequence,
                            string areaType,
                            string warehouseCode,
                            decimal minimumShelfLife)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "Area", DbType.String, area);
        db.AddInParameter(dbCommand, "AreaCode", DbType.String, areaCode);
        db.AddInParameter(dbCommand, "StockOnHand", DbType.Boolean, stockOnHand);
        db.AddInParameter(dbCommand, "replenish", DbType.Boolean, replenish);
        db.AddInParameter(dbCommand, "areaSequence", DbType.Int32, areaSequence);
        db.AddInParameter(dbCommand, "areaType", DbType.String, areaType);
        db.AddInParameter(dbCommand, "warehouseCode", DbType.String, warehouseCode);
        db.AddInParameter(dbCommand, "minimumShelfLife", DbType.Decimal, minimumShelfLife);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static int Area_Add(string connectionStringName,
                                int warehouseId,
                                string area,
                                string areaCode,
                                string stockOnHand,
                                int areaId,
                                bool replenish,
                                int areaSequence,
                                string areaType,
                                string warehouseCode,
                                decimal minimumShelfLife)
    {
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddOutParameter(dbCommand, "AreaId", DbType.Int32, areaId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "Area", DbType.String, area);
        db.AddInParameter(dbCommand, "AreaCode", DbType.String, areaCode);
        db.AddInParameter(dbCommand, "StockOnHand", DbType.Boolean, stockOnHand);
        db.AddInParameter(dbCommand, "replenish", DbType.Boolean, replenish);
        db.AddInParameter(dbCommand, "areaSequence", DbType.Int32, areaSequence);
        db.AddInParameter(dbCommand, "areaType", DbType.String, areaType);
        db.AddInParameter(dbCommand, "warehouseCode", DbType.String, warehouseCode);
        db.AddInParameter(dbCommand, "minimumShelfLife", DbType.Decimal, minimumShelfLife);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = int.Parse(db.GetParameterValue(dbCommand, "AreaId").ToString());
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static bool Area_Delete(string connectionStringName, int areaId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }


    #endregion

    #region Location

    public DataSet Location_Search(string connectionStringName, int areaId, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Location_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public bool Location_Update(string connectionStringName,
        int locationId,
        int locationTypeId,
        string location,
        string ailse,
        string column,
        string level,
        int palletQuantity,
        int securityCode,
        Decimal relativeValue,
        int numberofSub,
        string stockTakeInd,
        string activeBinning,
        string activePicking,
        string used,
        int areaId)
    {
        //string location = ailse + column + level;
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_location_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, locationTypeId);
        db.AddInParameter(dbCommand, "Location", DbType.String, location);
        db.AddInParameter(dbCommand, "Ailse", DbType.String, ailse);
        db.AddInParameter(dbCommand, "Column", DbType.String, column);
        db.AddInParameter(dbCommand, "Level", DbType.String, level);
        db.AddInParameter(dbCommand, "PalletQuantity", DbType.Decimal, palletQuantity);
        db.AddInParameter(dbCommand, "SecurityCode", DbType.Int32, securityCode);
        db.AddInParameter(dbCommand, "RelativeValue", DbType.Decimal, relativeValue);
        db.AddInParameter(dbCommand, "NumberOfSUB", DbType.Int32, numberofSub);
        db.AddInParameter(dbCommand, "StockTakeInd", DbType.Boolean, Boolean.Parse(stockTakeInd));
        db.AddInParameter(dbCommand, "ActiveBinning", DbType.Boolean, Boolean.Parse(activeBinning));
        db.AddInParameter(dbCommand, "ActivePicking", DbType.Boolean, Boolean.Parse(activePicking));
        db.AddInParameter(dbCommand, "Used", DbType.Boolean, Boolean.Parse(used));
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        sqlCommand = connectionStringName + ".dbo.p_AreaLocation_Update";
        dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, result);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

            }
            catch { }
            connection.Close();
        }

        return result;

    }

    public int Location_Add(string connectionStringName,
                            int locationId,
                            Nullable<int> locationTypeId,
                            string location,
                            string ailse,
                            string column,
                            string level,
                            Nullable<int> palletQuantity,
                            Nullable<int> securityCode,
                            Nullable<Decimal> relativeValue,
                            Nullable<int> numberofSub,
                            string stockTakeInd,
                            string activeBinning,
                            string activePicking,
                            string used,
                            Nullable<int> areaId)
    {
        //string location = ailse + column + level;
        int result = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_location_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddOutParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, locationTypeId);
        db.AddInParameter(dbCommand, "Location", DbType.String, location);
        db.AddInParameter(dbCommand, "Ailse", DbType.String, ailse);
        db.AddInParameter(dbCommand, "Column", DbType.String, column);
        db.AddInParameter(dbCommand, "Level", DbType.String, level);
        db.AddInParameter(dbCommand, "PalletQuantity", DbType.Decimal, palletQuantity);
        db.AddInParameter(dbCommand, "SecurityCode", DbType.Int32, securityCode);
        db.AddInParameter(dbCommand, "RelativeValue", DbType.Decimal, relativeValue);
        db.AddInParameter(dbCommand, "NumberOfSUB", DbType.Int32, numberofSub);
        db.AddInParameter(dbCommand, "StockTakeInd", DbType.Boolean, Boolean.Parse(stockTakeInd));
        db.AddInParameter(dbCommand, "ActiveBinning", DbType.Boolean, Boolean.Parse(activeBinning));
        db.AddInParameter(dbCommand, "ActivePicking", DbType.Boolean, Boolean.Parse(activePicking));
        db.AddInParameter(dbCommand, "Used", DbType.Boolean, Boolean.Parse(used));
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);
                result = int.Parse(db.GetParameterValue(dbCommand, "LocationId").ToString());
            }
            catch { }
            connection.Close();
        }


        sqlCommand = connectionStringName + ".dbo.p_AreaLocation_Insert";
        dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, result);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

            }
            catch { }
            connection.Close();
        }


        return result;

    }

    public static bool Location_Delete(string connectionStringName, int locationId, int areaId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_AreaLocation_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                return result;
            }
        }
        //connection.Close();

        return result;
    }

    public bool Location_Edit(string connectionStringName,
                              Nullable<int> locationId,
                              Nullable<int> locationTypeId,
                              string location,
                              string ailse,
                              string column,
                              string level,
                              Nullable<int> palletQuantity,
                              Nullable<int> securityCode,
                              Nullable<Decimal> relativeValue,
                              Nullable<int> numberofSub,
                              string stockTakeInd,
                              string activeBinning,
                              string activePicking,
                              string used,
                              Nullable<int> areaId)
    {
        //string location = ailse + column + level;
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_location_Edit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "LocationTypeId", DbType.Int32, locationTypeId);
        db.AddInParameter(dbCommand, "Location", DbType.String, location);
        db.AddInParameter(dbCommand, "Ailse", DbType.String, ailse);
        db.AddInParameter(dbCommand, "Column", DbType.String, column);
        db.AddInParameter(dbCommand, "Level", DbType.String, level);
        db.AddInParameter(dbCommand, "PalletQuantity", DbType.Decimal, palletQuantity);
        db.AddInParameter(dbCommand, "SecurityCode", DbType.Int32, securityCode);
        db.AddInParameter(dbCommand, "RelativeValue", DbType.Decimal, relativeValue);
        db.AddInParameter(dbCommand, "NumberOfSUB", DbType.Int32, numberofSub);
        db.AddInParameter(dbCommand, "StockTakeInd", DbType.Boolean, Boolean.Parse(stockTakeInd));
        db.AddInParameter(dbCommand, "ActiveBinning", DbType.Boolean, Boolean.Parse(activeBinning));
        db.AddInParameter(dbCommand, "ActivePicking", DbType.Boolean, Boolean.Parse(activePicking));
        db.AddInParameter(dbCommand, "Used", DbType.Boolean, Boolean.Parse(used));
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        sqlCommand = connectionStringName + ".dbo.p_AreaLocation_Update";
        dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

            }
            catch { }
            connection.Close();
        }

        return result;

        return result;
    }

    #endregion

    #region BOM
    public DataSet BOMHeader_Search(string connectionStringName, string productCode, string product, string barcode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_BOMHeader_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public static int BOMHeader_Add(string connectionStringName, int storageUnitId, string description, string remarks, string instruction, string BOMType)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOMHeader_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "description", DbType.String, description);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);
        db.AddInParameter(dbCommand, "instruction", DbType.String, instruction);
        db.AddInParameter(dbCommand, "BOMType", DbType.String, BOMType);

        // DataSet that will hold the returned results

        result = db.ExecuteNonQuery(dbCommand);

        return result;
    }

    public static int BOMHeader_Update(string connectionStringName, int storageUnitId, string description, string remarks, string instruction, string BOMType, int original_BOMHeaderId)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOMHeader_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters        
        db.AddInParameter(dbCommand, "BOMHeaderId", DbType.Int32, original_BOMHeaderId);

        db.AddInParameter(dbCommand, "@StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "@Description", DbType.String, description);
        db.AddInParameter(dbCommand, "@Remarks", DbType.String, remarks);
        db.AddInParameter(dbCommand, "@Instruction", DbType.String, instruction);
        db.AddInParameter(dbCommand, "@BOMType", DbType.String, BOMType);


        // DataSet that will hold the returned results

        result = db.ExecuteNonQuery(dbCommand);

        return result;
    }

    public static int BOMHeader_Delete(string connectionStringName, int original_BOMHeaderId)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOMHeader_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters        
        db.AddInParameter(dbCommand, "BOMHeaderId", DbType.Int32, original_BOMHeaderId);

        // DataSet that will hold the returned results

        result = db.ExecuteNonQuery(dbCommand);

        return result;
    }

    public DataSet BOMLine_Search(string connectionStringName, int BOMHeaderId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_BOMLine_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "BOMHeaderId", DbType.Int32, BOMHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public static int BOMLine_Add(string connectionStringName, int BOMHeaderId, int storageUnitId, Decimal quantity)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOMLine_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters        
        db.AddInParameter(dbCommand, "BOMHeaderId", DbType.Int32, BOMHeaderId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        // DataSet that will hold the returned results

        result = db.ExecuteNonQuery(dbCommand);

        return result;
    }

    //public static int BOMLine_Update(string connectionStringName, int BOMLineId)
    //{
    //    int result = 0;
    //    // Create the Database object, passing it the required database service.
    //    Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

    //    string sqlCommand = connectionStringName + ".dbo.p_BOMLine_Update";
    //    DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

    //    // Add Parameters        
    //    db.AddInParameter(dbCommand, "BOMHeaderId", DbType.Int32, BOMHeaderId);


    //    // DataSet that will hold the returned results

    //    result = db.ExecuteNonQuery(dbCommand);

    //    return result;
    //}

    public static int BOMLine_Delete(string connectionStringName, int original_BOMLineId)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOMLine_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters        
        db.AddInParameter(dbCommand, "BOMLineId", DbType.Int32, original_BOMLineId);

        // DataSet that will hold the returned results

        result = db.ExecuteNonQuery(dbCommand);

        return result;
    }

    public static DataSet BOMProduct_Search(string connectionStringName, int BOMLineId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_BOMProduct_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "BOMLineId", DbType.Int32, BOMLineId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public static int BOMProduct_Add(string connectionStringName, int BOMLineId, int storageUnitId, int lineNumber, Decimal quantity)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        try
        {
            string sqlCommand = connectionStringName + ".dbo.p_BOMProduct_Insert";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add Parameters        
            db.AddInParameter(dbCommand, "BOMLineId", DbType.Int32, BOMLineId);
            db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
            db.AddInParameter(dbCommand, "lineNumber", DbType.Int32, lineNumber);
            db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

            // DataSet that will hold the returned results

            result = db.ExecuteNonQuery(dbCommand);
        }
        catch (Exception ex)
        {
        }
        return result;
    }

    public static int BOMProduct_Update(string connectionStringName, int LineNumber, Decimal Quantity, int original_BOMLineId, int original_StorageUnitId, int original_LineNumber
        )
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        try
        {
            string sqlCommand = connectionStringName + ".dbo.p_BOMProduct_Update";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add Parameters        
            db.AddInParameter(dbCommand, "BOMLineId", DbType.Int32, original_BOMLineId);
            db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, original_StorageUnitId);
            db.AddInParameter(dbCommand, "oldLineNumber", DbType.Int32, original_LineNumber);
            db.AddInParameter(dbCommand, "newLineNumber", DbType.Int32, LineNumber);
            db.AddInParameter(dbCommand, "newQuantity", DbType.Decimal, Quantity);

            // DataSet that will hold the returned results

            result = db.ExecuteNonQuery(dbCommand);
        }
        catch (Exception ex)
        {
        }
        return result;
    }

    public static int BOMProduct_Delete(string connectionStringName, int original_BOMLineId, int original_StorageUnitId, int original_LineNumber)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        try
        {
            string sqlCommand = connectionStringName + ".dbo.p_BOMProduct_Delete";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add Parameters        
            db.AddInParameter(dbCommand, "BOMLineId", DbType.Int32, original_BOMLineId);
            db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, original_StorageUnitId);
            db.AddInParameter(dbCommand, "lineNumber", DbType.Int32, original_LineNumber);

            // DataSet that will hold the returned results

            result = db.ExecuteNonQuery(dbCommand);
        }
        catch (Exception ex)
        {
        }
        return result;
    }

    public static DataSet BOMInstruction_Search(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_BOMInstruction_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters


        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public DataSet BOMInstructionLine_Search(string connectionStringName, int BOMInstructionId)
    {
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Static_BOMInstructionLine_Search";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add Parameters
            db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, BOMInstructionId);

            // DataSet that will hold the returned results
            DataSet dataSet = null;

            dataSet = db.ExecuteDataSet(dbCommand);

            return dataSet;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static int BOMInstruction_Add(string connectionStringName, int BOMHeaderId, Decimal quantity, string orderNumber)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOMInstruction_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "BOMHeaderId", DbType.Int32, BOMHeaderId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);

        // DataSet that will hold the returned results

        result = db.ExecuteNonQuery(dbCommand);

        return result;
    }

    public static int BOMInstruction_Update(string connectionStringName, int original_BOMInstructionId, int original_BOMHeaderId, string OrderNumber, Decimal Quantity)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        try
        {
            string sqlCommand = connectionStringName + ".dbo.p_BOMInstruction_Update";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add Parameters        
            db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, original_BOMInstructionId);
            db.AddInParameter(dbCommand, "BOMHeaderId", DbType.Int32, original_BOMHeaderId);
            db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
            db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);

            // DataSet that will hold the returned results

            result = db.ExecuteNonQuery(dbCommand);
        }
        catch (Exception ex)
        {
        }
        return result;
    }

    public static int BOMInstruction_Update(string connectionStringName, int original_BOMInstructionId, int original_BOMHeaderId, string OrderNumber, string Remarks, Decimal Quantity)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        try
        {
            string sqlCommand = connectionStringName + ".dbo.p_BOMInstruction_Update";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add Parameters        
            db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, original_BOMInstructionId);
            db.AddInParameter(dbCommand, "BOMHeaderId", DbType.Int32, original_BOMHeaderId);
            db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
            db.AddInParameter(dbCommand, "Remarks", DbType.String, Remarks);
            db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, Quantity);

            // DataSet that will hold the returned results

            result = db.ExecuteNonQuery(dbCommand);
        }
        catch (Exception ex)
        {
        }
        return result;
    }

    public static int BOMInstruction_Delete(string connectionStringName, int original_BOMInstructionId, int original_BOMHeaderId)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOMInstruction_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters        
        db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, original_BOMInstructionId);

        // DataSet that will hold the returned results

        result = db.ExecuteNonQuery(dbCommand);

        return result;
    }

    public static int BOMInstructionLine_Update(string connectionStringName, int BOMInstructionId, int BOMLineId, int LineNumber, int CurrentSets)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        try
        {
            string sqlCommand = connectionStringName + ".dbo.p_BOMInstructionLine_Update";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add Parameters        
            db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, BOMInstructionId);
            db.AddInParameter(dbCommand, "BOMLineId", DbType.Int32, BOMLineId);
            db.AddInParameter(dbCommand, "LineNumber", DbType.Int32, LineNumber);
            db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, CurrentSets);

            // DataSet that will hold the returned results

            result = db.ExecuteNonQuery(dbCommand);
        }
        catch (Exception ex)
        {
        }
        return result;
    }

    public DataSet BOMInstructionSubstitutions_Search(string connectionStringName, int BOMInstructionId, int BOMLineId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_BOMInstructionSubstitutions_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, BOMInstructionId);
        db.AddInParameter(dbCommand, "BOMLineId", DbType.Int32, BOMLineId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public static DataSet BOMInstructionSubstitutionProduct_Search(string connectionStringName, int BOMLineId, int LineNumber)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.select p.ProductCode, p.Product, bp.Quantity " +
            "from BOMProduct bp inner join StorageUnit su on bp.StorageUnitId = su.StorageUnitId " +
            "inner join Product p on su.ProductId = p.ProductId " +
            "where bp.BOMLineId = @BOMLineId and bp.LineNumber = @LineNumber"; ;
        DbCommand dbCommand = db.GetSqlStringCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "BOMLineId", DbType.Int32, BOMLineId);
        db.AddInParameter(dbCommand, "LineNumber", DbType.Int32, LineNumber);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public static int BOMInstruction_Release(string connectionStringName, int BOMInstructionId)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        try
        {
            string sqlCommand = connectionStringName + ".dbo.p_static_BOMReleaseInstruction";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add Parameters        
            db.AddInParameter(dbCommand, "BOMInstructionId", DbType.Int32, BOMInstructionId);

            // DataSet that will hold the returned results

            result = db.ExecuteNonQuery(dbCommand);
        }
        catch (Exception ex)
        {
        }
        return result;        
    }
    #endregion


    #region GetReportType
    /// <summary>
    /// Searches for Contact by code
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="contactListId"></param>
    /// <returns></returns>
    public DataSet GetReportType(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_List_ReportType";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetReportType


}
