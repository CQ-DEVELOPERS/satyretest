using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Product
/// </summary>
public class Product
{
    #region Constructor Logic
    public Product()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion
	
  #region SearchProducts
  /// <summary>
  /// Searches Products by Product Description and Product Code.
  /// </summary>
  public DataSet SearchProducts(string connectionStringName, string productCode, string product, string skuCode, string sku)
  {
    // Create the Database object, passing it the required database service.
    Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

    string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Search_Product_SKU";
    DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

    // Add paramters
    db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
    db.AddInParameter(dbCommand, "Product", DbType.String, product);
    db.AddInParameter(dbCommand, "SKUCode", DbType.String, skuCode);
    db.AddInParameter(dbCommand, "SKU", DbType.String, sku);

    // DataSet that will hold the returned results
    DataSet dataSet = null;

    dataSet = db.ExecuteDataSet(dbCommand);

    return dataSet;
  }
  /// <summary>
  /// Searches Products by Product Description and Product Code and Principal
  /// </summary>
  public DataSet SearchProducts(string connectionStringName, string productCode, string product, int principalId)
  {
    // Create the Database object, passing it the required database service.
    Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

    string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Search_Product_SKU";
    DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

    // Add paramters
    db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
    db.AddInParameter(dbCommand, "Product", DbType.String, product);
    db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);

    // DataSet that will hold the returned results
    DataSet dataSet = null;

    dataSet = db.ExecuteDataSet(dbCommand);

    return dataSet;
  }
  /// <summary>
  /// Searches Products by Product Description and Product Code.
  /// </summary>
  public DataSet SearchProducts(string connectionStringName, string productCode, string product, string skuCode, string sku, int principalId)
  {
    // Create the Database object, passing it the required database service.
    Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

    string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Search_Product_SKU";
    DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

    // Add paramters
    db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
    db.AddInParameter(dbCommand, "Product", DbType.String, product);
    db.AddInParameter(dbCommand, "SKUCode", DbType.String, skuCode);
    db.AddInParameter(dbCommand, "SKU", DbType.String, sku);

    // DataSet that will hold the returned results
    DataSet dataSet = null;

    dataSet = db.ExecuteDataSet(dbCommand);

    return dataSet;
  }
  #endregion SearchProducts

    #region SearchPackageProducts
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchPackageProducts(string connectionStringName, string productCode, string product)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StorageUnit_Search_Product_Packaging";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchPackageProducts

    #region SearchProductId
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchProductId(string connectionStringName, string productCode, string product)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Product_Search_ProductId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchProductId

    #region SearchProductsStorageUnitBatch
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchProductsStorageUnitBatch(string connectionStringName, string productCode, string product, string skuCode, string sku, string batch, string eclNumber)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StorageUnitBatch_Search_All";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "skuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "sku", DbType.String, sku);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "eclNumber", DbType.String, eclNumber);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchProductsStorageUnitBatch

    #region SearchProductsBatchId
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchProductsBatchId(string connectionStringName, string productCode, string product, string skuCode, string sku, string batch)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StorageUnitBatch_Search_BatchId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "skuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "sku", DbType.String, sku);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchProductsBatchId

    #region SearchProductsStorageUnitBatch
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchProductsStorageUnitBatch(string connectionStringName, string productCode, string product)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StorageUnitBatch_Search_Product_SKU";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion

    #region SearchProductsStorageUnitBatch
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchProductsStorageUnitBatchLocation(string connectionStringName, int warehouseId, string productCode, string product, string batch)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StorageUnitBatch_Search_SUBL";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.String, warehouseId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion

    #region SearchStockOnHand
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchStockOnHand(string connectionStringName, Int32 warehouseId, String productCode, String product, String skuCode, String sku, String batch, String fromLocation, String toLocation)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StockOnHand_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "SkuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "Sku", DbType.String, sku);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "FromLocation", DbType.String, fromLocation);
        db.AddInParameter(dbCommand, "ToLocation", DbType.String, toLocation);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchStockOnHand

    #region SearchViewSUL
    /// <summary>
    /// Searches viewSUL.
    /// </summary>
    public DataSet SearchViewSUL(string connectionStringName, Int32 warehouseId, String productCode, String product, String skuCode, String sku, String fromLocation, String toLocation)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_viewSUL_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "SkuCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "Sku", DbType.String, sku);
        db.AddInParameter(dbCommand, "FromLocation", DbType.String, fromLocation);
        db.AddInParameter(dbCommand, "ToLocation", DbType.String, toLocation);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchViewSUL

    #region GetProductsByStorageUnit
    /// <summary>
    /// Searches Batch by StorageUnitId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitId"></param>
    /// <returns></returns>
    public DataSet GetProductsByStorageUnit(string connectionStringName, int storageUnitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Product_Search_StorageUnit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetProductsByStorageUnit

    #region SearchProductsCustomerReturn
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchProductsCustomerReturn(string connectionStringName, int inboundDocumentId, string productCode, string product, string barcode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Customer_Return_Search_Product";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundDocumentId", DbType.Int32, inboundDocumentId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchProducts

    #region ProductProductCategory_Add
    public static bool ProductProductCategory_Add(string connectionStringName, int operatorGroupId, int areaId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_AreaOperatorGroup_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OperatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion ProductProductCategory_Add

    #region ProductProductCategory_Delete
    public static bool ProductProductCategory_Delete(string connectionStringName, int productCategoryId, int productId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_ProductProductCategory_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ProductCategoryId", DbType.Int32, productCategoryId);
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, productId);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion ProductProductCategory_Delete

    #region GetProductsSel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetProductsSel(string connectionStringName, int productCategoryId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Product_LinkedToProductCategory";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCategoryId", DbType.Int32, productCategoryId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetProductsSel

    #region GetProductsUnsel
    /// <summary>
    /// Gets a list of Areas
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetProductsUnsel(string connectionStringName, int productCategoryId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Product_NotLinkedToProductCategory";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCategoryId", DbType.Int32, productCategoryId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetProductsUnsel

    #region ListProductCategories
    public DataSet ListProductCategories(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_List_ProductCategories";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ListProductCategories
}
