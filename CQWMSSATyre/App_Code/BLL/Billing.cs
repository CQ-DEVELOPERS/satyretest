﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Billing
/// </summary>
[DataObject]
public class Billing : BaseBLL
{
  public Billing()
  {

  }

  public DataTable GetBills( int principalId, int billingCategoryId )
  {
    string cn = HttpContext.Current.Session["ConnectionStringName"].ToString();

    // Create the Database object, passing it the required database service.
    Database db = DatabaseFactory.CreateDatabase( cn );

    string sqlCommand = "p_Bill_Search";

    DbCommand dbCommand = db.GetStoredProcCommand( sqlCommand );

    // Add paramters
    db.AddInParameter( dbCommand, "PrincipalId", DbType.Int32, principalId );
    db.AddInParameter( dbCommand, "BillingCategoryId", DbType.Int32, billingCategoryId );

    // DataSet that will hold the returned results
    var data = new DataTable( "Bill" );

    // Execute the command
    data.Load( db.ExecuteReader( dbCommand ) );

    return data;
  }

  public DataTable GetBillableTransactions( int principalId, DateTime fromDate, DateTime toDate, BillTransactionStatuses status )
  {
    var data = ExecuteDataSet( "p_Bill_BillingSearch", new ParamItem( "PrincipalId", principalId )
                                                     , new ParamItem( "From", fromDate )
                                                     , new ParamItem( "To", toDate )
                                                     , new ParamItem( "Status", status ) );

    return data.Tables[0];
  }

  public enum BillTransactionStatuses
  {
    All = 0,
    InProgress = 1,
    Billed = 2
  }

  public static int CreateBilledTransaction( List<object> ids, DateTime fromDate, DateTime toDate )
  {
    var service = CQ.Billing.Core.Services.BillService.GetInstance( HttpContext.Current.Session["ConnectionStringName"].ToString() );
    var warehouseId = int.Parse( HttpContext.Current.Session["WarehouseId"].ToString() );
    var id = service.CreateBilledTransaction( fromDate, toDate, ids, warehouseId );

    return id;
  }
}