using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Planning
/// </summary>
public class Routing
{
    #region Constructor Logic
    public Routing()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SearchOrders
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchOrders(string connectionStringName,
                                int warehouseId,
                                int outboundShipmentId,
                                int outboundDocumentTypeId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Routing_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrders"

    #region GetPendingOrders
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetPendingOrders(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Routing_Order_Pending";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetPendingOrders

    #region UpdateOrder
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateOrder(string connectionStringName,
                            int outboundShipmentId,
                            int issueId,
                            string orderNumber,
                            int locationId,
                            int priorityId,
                            int routeId,
                            DateTime deliveryDate,
                            string remarks)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Routing_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "priorityId", DbType.Int32, priorityId);
        db.AddInParameter(dbCommand, "routeId", DbType.Int32, routeId);
        db.AddInParameter(dbCommand, "deliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrder"

    #region SearchLoads
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchLoads(string connectionStringName,
                                int warehouseId,
                                int outboundShipmentId,
                                int outboundDocumentTypeId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Routing_Load_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchLoads"

    #region GetOrderLines
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetOrderLines(string connectionStringName, int outboundShipmentId, int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Routing_Line_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderLines"

    #region GetOrderDetails
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetOrderDetails(string connectionStringName,
                                    int outboundShipmentId,
                                    int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Routing_Detail_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderDetails"

    #region AddToRoute
    /// <summary>
    /// Adds Issue from Routing System send
    /// </summary>
    public bool AddToRoute(string connectionStringName, int issueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Routing_Add";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion AddToRoute

    #region RemoveFromRoute
    /// <summary>
    /// Removes Issue from Routing System send
    /// </summary>
    public bool RemoveFromRoute(string connectionStringName, int issueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Routing_Remove";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion RemoveFromRoute

    #region SendToRoutingSystem
    /// <summary>
    /// Sends to the Routing System
    /// </summary>
    public bool SendToRoutingSystem(string connectionStringName)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_FamousBrands_Export_Flo_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SendToRoutingSystem
}
