using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Batch
/// </summary>
public class BusinessLayerValidation
{
    #region Constructor Logic
    public BusinessLayerValidation()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Validate
    /// <summary>
    /// Validates required Session Objects and returns boolean
    /// </summary>
    public static bool Validate()
    {
        try
        {
            return true;
            //if (Session["WarehouseId"].ToString() == null)
            //    return false;

            //if (Session["OperatorId"].ToString() == null)
            //    return false;

            //if (Session["OperatorGroupId"].ToString() == null)
            //    return false;
        }
        catch
        {
            return false;
        }
    }
    #endregion "Validate"
}
