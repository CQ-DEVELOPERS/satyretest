using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

/// <summary>
/// Summary description for Transact
/// </summary>
public class BulkImport
{
    #region Constructor Logic
    public BulkImport()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region BulkImportTransfers
    public bool BulkImportTransfers(string connectionStringName, string filename)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Bulk_Import_Tranfers";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "filename", DbType.String, filename);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
                catch             
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion BulkImportTransfers

    #region ImportTransfers
    public bool ImportTransfers(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Import_Tranfers";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion ImportTransfers

    #region ProductMasterImport
    public bool ProductMasterImport(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Product_Master_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);
		dbCommand.CommandTimeout = 0;
		
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion ProductMasterImport

    #region ProductMasterExecute
    public bool ProductMasterExecute(string connectionStringName)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Product_Master_Import_Product";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion ProductMasterExecute

    #region LocationMasterImport
    public bool LocationMasterImport(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Master_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion LocationMasterImport

    #region PriceListImport
    public bool PriceListImport(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Price_List_Master_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion PriceListImport

    #region StorageUnitLabelImport
    public bool StorageUnitLabelImport(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnitLabel_Master_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion StorageUnitLabelImport

    #region ExternalCompanyImport
    public bool ExternalCompanyImport(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Master_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion ExternalCompanyImport

    #region ProductAreaMasterImport
    public bool ProductAreaMasterImport(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Product_Area_Master_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion ProductAreaMasterImport

    #region InboundDocumentImport
    public bool InboundDocumentImport(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Inbound_Document_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion InboundDocumentImport

    #region OutboundDocumentImport
    public bool OutboundDocumentImport(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Outbound_Document_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion OutboundDocumentImport

    #region OrdersImport
    public bool OrdersImport(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OrderNumbers_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion OrdersImport

    #region OrdersClear
    public bool OrdersClear(string connectionStringName, int warehouseId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OrderNumbers_Clear";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion OrdersClear
	
	#region TakeOnStockImport
    public bool TakeOnStockImport(string connectionStringName, string xmlstring)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Take_On_Stock_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "xmlstring", DbType.String, xmlstring);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion TakeOnStockImport
	
	 #region TakeOnStockInsert
    public bool TakeOnStockInsert(string connectionStringName)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Import_TakeOnStock_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion TakeOnStockInsert
}