using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class InboundShipment
{
    #region Constructor Logic

    public int inboundShipmentId;

    public InboundShipment()
    {
        inboundShipmentId = -1;
    }
    #endregion Constructor Logic

    /// <summary>
    /// Searches for an InboundShipment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    public DataSet SearchInboundShipmentCreate(string connectionStringName,
                                                int WarehouseId,
                                                int InboundDocumentTypeId,
                                                int InboundShipmentId,
                                                string ExternalCompanyCode,
                                                string ExternalCompany,
                                                string OrderNumber,
                                                DateTime FromDate,
                                                DateTime ToDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Inbound_Shipment_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    /// <summary>
    /// Get a single InboundShipment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetInboundShipmentCreate(string connectionStringName, int inboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Inbound_Shipment_Get";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);

        // DataSet that will hold the returned results		
        DataSet inboundDocumentTypeDataSet = null;

        inboundDocumentTypeDataSet = db.ExecuteDataSet(dbCommand);

        return inboundDocumentTypeDataSet;
    }

    /// <summary>
    /// Retreives Receipts linked to an Inbound Shipment.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <returns></returns>
    public DataSet GetLinkedReceipts(string connectionStringName, int InboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Inbound_Shipment_Linked_Receipt";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    /// <summary>
    /// Retreives Receipts not linked to an Inbound Shipment.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    public DataSet GetUnlinkedReceipts(string connectionStringName, 
                                        int WarehouseId,
                                        int InboundDocumentTypeId,
                                        string OrderNumber,
                                        string ExternalCompanyCode,
                                        string ExternalCompany,
                                        DateTime FromDate,
                                        DateTime ToDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Inbound_Shipment_Unlinked_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    /// <summary>
    /// Adds a Receipt to an InboundShipment.
    /// </summary>
    /// <param name="InboundShipmentId">InboundShipment affected.</param>
    /// <param name="ReceiptId">Receipt to be added.</param>
    /// <returns>true if sucessful; otherwise false.</returns>
    /// <remarks>None.</remarks>
    public bool Transfer(string connectionStringName, int InboundShipmentId, int ReceiptId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InboundShipmentReceipt_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    /// <summary>
    /// Removes a Receipt from an InboundShipment.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ReceiptId"></param>
    /// <returns></returns>
    public bool Remove(string connectionStringName, int InboundShipmentId, int ReceiptId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InboundShipmentReceipt_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }

    /// <summary>
    /// Insert an Inbound Shipment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="shipmentDate"></param>
    /// <param name="remarks"></param>
    /// <returns></returns>
    public bool InsertInboundShipment(string connectionStringName, int warehouseId, DateTime shipmentDate, string remarks)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "ShipmentDate", DbType.DateTime, shipmentDate);
        db.AddInParameter(dbCommand, "Remarks", DbType.String, remarks);

        // Enter a default inboundShipmentId, and catch the returned inboundShipmentId for the new record
        db.AddOutParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                //InboundShipmentId
                inboundShipmentId = (int)db.GetParameterValue(dbCommand, "inboundShipmentId");

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }

    public bool DeleteInboundShipment(string connectionStringName, int inboundShipmentId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_InboundShipment_Remove";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }

    #region CreateConsolidatedLoad
    /// <summary>
    /// Inserts a Redelivery for a Receipt (A new Receipt Document)
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public bool CreateConsolidatedLoad(string connectionStringName, int operatorId, int inboundShipmentId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Consolidated_Load_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateConsolidatedLoad"

    #region GetOrderNumnber
    public String GetOrderNumnber(string connectionStringName, int inboundShipmentId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Inbound_Shipment_Get_OrderNumber";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);

        // DataSet that will hold the returned results		
        String result = null;

        try
        {
            result = (string)db.ExecuteScalar(dbCommand);
        }
        catch { }

        return result;
    }
    #endregion GetOrderNumnber
}
