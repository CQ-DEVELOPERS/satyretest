using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   16 Oct 2007
/// Summary description for Planning
/// </summary>
public class OutboundWIP
{
    #region Constructor Logic
    public OutboundWIP()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SearchOrders
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchOrders(string connectionStringName,
                                int outboundShipmentId,
                                int warehouseId,
                                int outboundDocumentTypeId,
                                int waveId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate,
                                int principalId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_WIP_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "WaveId", DbType.Int32, waveId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "PrincipalId", DbType.Int32, principalId);


        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet SearchOrders(string connectionStringName,
                                int outboundShipmentId,
                                int warehouseId,
                                int outboundDocumentTypeId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate,
                                int principalId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_WIP_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "principalId", DbType.Int32, principalId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrders"

    #region UpdateOrder
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateOrder(string connectionStringName,
                            int outboundShipmentId,
                            int issueId,
                            int priorityId,
                            int locationId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_WIP_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "PriorityId", DbType.Int32, priorityId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrder"

    #region GetOrderLines
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetOrderLines(string connectionStringName, int outboundShipmentId, int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_WIP_Line_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderLines"

    #region SearchOrderLines
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchOrderLines(string connectionStringName,
                                    int warehouseId,
                                    int outboundDocumentTypeId,
                                    string orderNumber,
                                    string externalCompany,
                                    string externalCompanyCode,
                                    DateTime fromDate,
                                    DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Line_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrderLines"

    #region GetOrderDetails
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetOrderDetails(string connectionStringName,
                                    int outboundShipmentId,
                                    int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Detail_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderDetails"

    #region UpdateOrderLine
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateOrderLine(string connectionStringName,
                                int issueLineId,
                                int storageUnitBatchId,
                                int storageUnitId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_WIP_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrderLine"

    #region Palletise
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool Palletise(string connectionStringName,
                            int issueId,
                            int issueLineId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Palletise";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Palletise"

    #region SearchReleaseDocuments
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchReleaseDocuments(string connectionStringName,
                                            int warehouseId,
                                            int outboundDocumentTypeId,
                                            string orderNumber,
                                            string externalCompany,
                                            string externalCompanyCode,
                                            DateTime fromDate,
                                            DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Release_Document_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchReleaseDocuments"

    #region SearchReleaseLines
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchReleaseLines(string connectionStringName, int outboundShipmentId, int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Planning_Release_Lines_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchReleaseLines"

    #region AutoLocationAllocate
    /// <summary>
    /// Allocates a Location to an IssueLine (Automatically).
    /// </summary>
    public bool AutoLocationAllocate(string connectionStringName, int issueLineId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "issueLineId", DbType.Int32, issueLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion "AutoLocationAllocate"

    #region AutoLocationAllocateIssue
    /// <summary>
    /// Allocates a Location to an Issue (Automatically).
    /// </summary>
    public bool AutoLocationAllocateIssue(string connectionStringName, int issueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion "AutoLocationAllocateIssue"

    #region AutoLocationAllocateInstruction
    /// <summary>
    /// Allocates a Location to an Instruction (Automatically).
    /// </summary>
    public bool AutoLocationAllocateInstruction(string connectionStringName, int instructionId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        dbCommand.CommandTimeout = 300;

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion "AutoLocationAllocateInstruction"

    #region SearchPickingJobs
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchPickingJobs(string connectionStringName, int outboundShipmentId, int issueId, string instructionTypeCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_WIP_Job_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchPickingJobs"

    #region SearchLinesByJob
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchLinesByJob(string connectionStringName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_WIP_Lines_By_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchLinesByJob"

    #region PalletiseDeallocate
    /// <summary>
    /// Deallocates Stock and Deletes Instructions - if not instructions are Started or Finished
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <param name="issueId"></param>
    /// <returns></returns>
    public bool PalletiseDeallocate(string connectionStringName, int outboundShipmentId, int issueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Palletise_Deallocate";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion PalletiseDeallocate

    #region ReleaseNoStock
    /// <summary>
    /// Releases short picked items again.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <param name="issueId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool ReleaseNoStock(string connectionStringName, int outboundShipmentId, int issueId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Palletise_Release_NoStock";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    /// <summary>
    /// Releases short picked items again. This overload does it by StorageUnit.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <param name="issueId"></param>
    /// <param name="operatorId"></param>
    /// <param name="storageUnitId"></param>
    /// <returns></returns>
    public bool ReleaseNoStock(string connectionStringName, int outboundShipmentId, int issueId, int operatorId, int storageUnitId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Palletise_Release_NoStock";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion ReleaseNoStock

    #region ReleaseWave
    /// <summary>
    /// Releases short picked items again.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <param name="issueId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool ReleaseWave(string connectionStringName, int outboundShipmentId, int issueId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Outbound_Release_Wave";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion ReleaseWave

    #region CreateJobs
    /// <summary>
    /// Creates Jobs for Label print - wizard change
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <param name="issueId"></param>
    /// <returns></returns>
    public bool CreateJobs(string connectionStringName, int operatorId, string referenceNumber, int numberOfLabels)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Linked_Labels_Create_Jobs";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "numberOfLabels", DbType.Int32, numberOfLabels);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();
            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion CreateJobs

    #region CancelOrder
    /// <summary>
    /// Cancel Order
    /// </summary>
    public bool CancelOrder(string connectionStringName,
                                int operatorId,
                                int outboundShipmentId,
                                int issueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Palletise_Cancel_Order";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CancelOrder"
}
