﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for RateDetailLine
/// </summary>
public class RateDetailLine : BaseBLL
{
  public RateDetailLine()
  {
  }

  /// <summary>
  /// Gets the by rate detail id.
  /// </summary>
  /// <param name="rateDetailId">The rate id.</param>
  /// <returns></returns>
  public DataSet GetByRateDetailId( int rateDetailId )
  {
    var data = ExecuteDataSet( "p_RateDetailLine_Search", new ParamItem( "RateDetailId", rateDetailId ) );
    return data;
  }

  public int Update( int original_Id, int id, double weightInKg, double numberOfUnits, double cubeInMeters
                   , double numberOfPallets, double numberOfCartons, double flatRateAmount, double minimumWeightInKg
                   , int rateDetailId, int billingEventId, int areaId )
  {
    string sql = "p_RateDetailLine_" + (original_Id > 0 ? "Update" : "Insert");

    var idParam = new ParamItem( "Id", DbType.Int32, original_Id, original_Id > 0 ? ParameterDirection.Input : ParameterDirection.InputOutput );

    ExecuteNonQuery( sql, idParam
                        , new ParamItem( "WeightInKg", weightInKg)
                        , new ParamItem( "NumberOfUnits", numberOfUnits )
                        , new ParamItem( "CubeInMeters", cubeInMeters )
                        , new ParamItem( "NumberOfPallets", numberOfPallets )
                        , new ParamItem( "NumberOfCartons", numberOfCartons )
                        , new ParamItem( "FlatRateAmount", flatRateAmount )
                        , new ParamItem( "MinimumWeightInKg", minimumWeightInKg )
                        , new ParamItem( "RateDetailId", rateDetailId )
                        , new ParamItem( "BillingEventId", billingEventId )
                        , new ParamItem( "AreaId", areaId ) );

    return (int)idParam.Value;

  }

}