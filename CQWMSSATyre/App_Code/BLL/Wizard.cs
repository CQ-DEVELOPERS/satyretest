using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Author: Grant Schultz
/// Date:   20 July 2007
/// Summary description for OutboundShipment
/// </summary>
public class Wizard
{
    #region Constructor Logic

    public Wizard()
    {
    }
    #endregion "Constructor Logic"

    #region GetVersions
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetVersions(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "p_Instance_Version_List";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        //db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetVersions

    #region SaveInstanceDetails
    /// <summary>
    /// Saves the users details entered into the database
    /// </summary>
    /// <param name="email"></param>
    /// <param name="fristName"></param>
    /// <param name="lastName"></param>
    /// <param name="company"></param>
    /// <param name="telephone"></param>
    /// <param name="url"></param>
    /// <param name="versionCode"></param>
    /// <returns></returns>
    public bool SaveInstanceDetails(String email, String fristName, String lastName, String company, String telephone, String url, String versionCode)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "p_Instance_Save_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "email", DbType.String, email);
        db.AddInParameter(dbCommand, "fristName", DbType.String, fristName);
        db.AddInParameter(dbCommand, "lastName", DbType.String, lastName);
        db.AddInParameter(dbCommand, "company", DbType.String, company);
        db.AddInParameter(dbCommand, "telephone", DbType.String, telephone);
        db.AddInParameter(dbCommand, "url", DbType.String, url);
        db.AddInParameter(dbCommand, "versionCode", DbType.String, versionCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion SaveInstanceDetails

    #region CheckInstanceAvailability
    /// <summary>
    /// Saves the users details entered into the database
    /// </summary>
    /// <param name="email"></param>
    /// <param name="fristName"></param>
    /// <param name="lastName"></param>
    /// <param name="company"></param>
    /// <param name="telephone"></param>
    /// <param name="url"></param>
    /// <param name="versionCode"></param>
    /// <returns></returns>
    public bool CheckInstanceAvailability(String instanceName)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "p_Instance_Check_Availability";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instanceName", DbType.String, instanceName);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion CheckInstanceAvailability
}
