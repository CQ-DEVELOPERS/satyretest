﻿using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class StationSheetReceiving
{
    #region Constructor logic
    public StationSheetReceiving()
    {
        //
        // TODO: Add Constructor logic here
        //
    }
    #endregion "Constructor logic"

    #region GetReceivingDocuments
    /// <summary>
    /// Retrieves Receipt Documents available for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <param name="ProductCode"></param>
    /// <param name="Batch"></param>
    /// <returns></returns>
    public DataSet GetReceivingDocuments(string connectionStringName,
                                        int WarehouseId,
                                        int InboundDocumentTypeId,
                                        int InboundShipmentId,
                                        string ExternalCompanyCode,
                                        string ExternalCompany,
                                        string OrderNumber,
                                        DateTime FromDate,
                                        DateTime ToDate,
                                        string ProductCode,
                                        string Batch)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Station_Sheet_Receiving_Document_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, ProductCode);
        db.AddInParameter(dbCommand, "Batch", DbType.String, Batch);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetReceivingDocuments"

    #region GetReceivingDocumentsReports
    /// <summary>
    /// Retreives Receipt Documents available for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="WarehouseId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    public DataSet GetReceivingDocumentsReports(string connectionStringName,
                                        int WarehouseId,
                                        int InboundDocumentTypeId,
                                        int InboundShipmentId,
                                        string ExternalCompanyCode,
                                        string ExternalCompany,
                                        string OrderNumber,
                                        DateTime FromDate,
                                        DateTime ToDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Document_Search_Reports";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetReceivingDocumentsReports"

    #region GetReceivedReceivingDocuments
    /// <summary>
    /// Retreives Receipt Documents available for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="InboundDocumentTypeId"></param>
    /// <param name="InboundShipmentId"></param>
    /// <param name="ExternalCompanyCode"></param>
    /// <param name="ExternalCompany"></param>
    /// <param name="OrderNumber"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    public DataSet GetReceivedReceivingDocuments(string connectionStringName,
                                                    int warehouseId, //Not implemented
                                                    int InboundDocumentTypeId,
                                                    int InboundShipmentId,
                                                    string ExternalCompanyCode,
                                                    string ExternalCompany,
                                                    string OrderNumber,
                                                    DateTime FromDate,
                                                    DateTime ToDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Palletisation_Document_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundDocumentTypeId", DbType.Int32, InboundDocumentTypeId);
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, InboundShipmentId);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, ExternalCompanyCode);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, ExternalCompany);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, FromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, ToDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetReceivedReceivingDocuments"

    #region GetQuestion
    public DataSet GetQuestion(string connectionStringName,
                                Int32 questionaireId,
                                string questionaireType,
                                Int32 sequence,
                                int warehouseId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Question_Get";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "questionaireId", DbType.Int32, questionaireId);
        db.AddInParameter(dbCommand, "questionaireType", DbType.String, questionaireType);
        db.AddInParameter(dbCommand, "sequence", DbType.Int32, sequence);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;


    }


    #endregion GetQuestion

    #region InsertQuestion

    public bool InsertQuestion(String connectionStringName,
                                String OrderNumber,
                                Int32 jobId,
                                Int32 palletId,
                                Int32 receiptId,
                                Int32 questionId,
                                String answer)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        bool result = false;
        string sqlCommand = connectionStringName + ".dbo.p_Question_Answer_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "questionId", DbType.Int32, questionId);
        db.AddInParameter(dbCommand, "answer", DbType.String, answer);
        //db.AddInParameter(dbCommand, "dateAsked", DbType.DateTime, dateAsked);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteScalar(dbCommand);
                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }

    }



    #endregion InsertQuestion

    #region GetPackageLinesComplete
    /// <summary>
    /// Retreives ReceiptLines available for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public DataSet GetPackageLinesComplete(string connectionStringName, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Package_Line_Search_Complete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters

        //db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;
        try
        {
            dataSet = db.ExecuteDataSet(dbCommand);
        }
        catch { }

        return dataSet;
    }
    #endregion "GetPackageLinesComplete"

    #region GetPackageLines
    /// <summary>
    /// Retreives ReceiptLines available for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public DataSet GetPackageLines(string connectionStringName, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Package_Line_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters

        //db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;
        try
        {
            dataSet = db.ExecuteDataSet(dbCommand);
        }
        catch { }

        return dataSet;
    }
    #endregion "GetPackageLines"

    #region GetReceiptLines
    /// <summary>
    /// Retreives ReceiptLines available for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public DataSet GetReceiptLines(string connectionStringName, int inboundShipmentId, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Station_Sheet_Receiving_Line_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetReceiptLines"

    #region AutoLocationAllocate
    /// <summary>
    /// Allocates a Location to an Instruction (Automatically).
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public bool AutoLocationAllocate(string connectionStringName, int receiptId, int receiptLineId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "AutoLocationAllocate"

    #region ManualLocationAllocate
    /// <summary>
    /// Allocates a Location to an Instruction (Manually selected).
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionId"></param>
    /// <param name="storeLocationId"></param>
    /// <returns></returns>
    public bool ManualLocationAllocate(string connectionStringName, int InstructionId, int storeLocationId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Palletisation_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);
        db.AddInParameter(dbCommand, "StoreLocationId", DbType.Int32, storeLocationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "ManualLocationAllocate"

    #region Palletise
    /// <summary>
    /// Palletises a ReceiptDocument or a Receipt Line depending on the input parameters.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptId"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public bool Palletise(string connectionStringName, int receiptId, int receiptLineId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Palletise";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Palletise"

    #region SetDefaultDeliveryNoteQty
    /// <summary>
    /// Sets the Delivery Not Quantity
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="p_receiptId"></param>
    /// <returns></returns>

    public int SetDefaultDeliveryNoteQty(string connectionStringName, int inboundShipmentId, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_ReceiptLine_Default_Delivery";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        return db.ExecuteNonQuery(dbCommand);
    }
    #endregion "SetDefaultDeliveryNoteQty"

    #region SetDefaultActualQty
    /// <summary>
    /// Sets the Actual Quantity
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="p_receiptId"></param>
    /// <returns></returns>
    public int SetDefaultActualQty(string connectionStringName, int inboundShipmentId, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_ReceiptLine_Default_Accepted";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        return db.ExecuteNonQuery(dbCommand);
    }
    #endregion "SetDefaultActualQty"

    #region SetDefaultZero
    /// <summary>
    /// Sets the Actual Quantity
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="p_receiptId"></param>
    /// <returns></returns>
    public int SetDefaultZero(string connectionStringName, int inboundShipmentId, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_ReceiptLine_Default_Zero";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        return db.ExecuteNonQuery(dbCommand);
    }
    #endregion "SetDefaultZero"

    #region CompletePackageLines
    /// <summary>
    /// Completes packaging lines that are Status Received and changes them to Receiving Complete
    /// </summary>
    /// <returns></returns>
    public int CompletePackageLines(string connectionStringName,
                                     int WarehouseId,
                                     int ReceiptId)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_PackageLine_Complete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
                result = 1;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CompletePackageLines"

    #region SetPackageLines
    /// <summary>
    /// Palletises a ReceiptDocument or a Receipt Line depending on the input parameters.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <param name="operatorId"></param>
    /// <param name="acceptedQuantity"></param>
    /// <param name="deliveryNoteQuantity"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="alternateBatch"></param>
    /// <returns></returns>
    public int SetPackageLines(string connectionStringName,
                                int PackageLineId,
                                int storageUnitBatchId,
                                int operatorId,
                                decimal acceptedQuantity,
                                decimal deliveryNoteQuantity,
                                decimal rejectQuantity)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_PackageLine_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "PackageLineId", DbType.Int32, PackageLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "AcceptedQuantity", DbType.Decimal, acceptedQuantity);
        db.AddInParameter(dbCommand, "DeliveryNoteQuantity", DbType.Decimal, deliveryNoteQuantity);
        db.AddInParameter(dbCommand, "RejectQuantity", DbType.Decimal, rejectQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "SetPackageLines"

    #region InsertPackageLines
    /// <summary>
    /// Palletises a ReceiptDocument or a Receipt Line depending on the input parameters.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <param name="operatorId"></param>
    /// <param name="acceptedQuantity"></param>
    /// <param name="deliveryNoteQuantity"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="alternateBatch"></param>
    /// <returns></returns>
    public int InsertPackageLines(string connectionStringName,
                                int WarehouseId,
                                int ReceiptId,
                                int storageUnitBatchId,
                                int operatorId)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_PackageLine_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "InsertPackageLines"

    #region SetReceiptLines
    /// <summary>
    /// Palletises a ReceiptDocument or a Receipt Line depending on the input parameters.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="acceptedQuantity"></param>
    /// <returns></returns>
    public int SetReceiptLines(string connectionStringName,
                                int ReceiptLineId,
                                decimal AcceptedQuantity)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Station_Sheet_Receiving_ReceiptLine_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, ReceiptLineId);
        db.AddInParameter(dbCommand, "acceptedQuantity", DbType.Decimal, AcceptedQuantity);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "SetReceiptLines"

    #region Receiving Check Sheet
    /// <summary>
    /// Retreives the Receiving Check Sheet.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public DataSet ReceivingCheckSheet(string connectionStringName, int inboundShipmentId, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Check_Sheet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion

    #region RedeliveryInsert
    /// <summary>
    /// Inserts a Redelivery for a Receipt (A new Receipt Document)
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public bool RedeliveryInsert(string connectionStringName, int inboundShipmentId, int receiptId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Redelivery_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "RedeliveryInsert"

    #region UploadToHost
    /// <summary>
    /// Sents receipt to ERP
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public bool UploadToHost(string connectionStringName, int receiptId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_interface_xml_PO_Export_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion "UploadToHost"

    #region UploadToHostAdjustment
    /// <summary>
    /// Sents receipt to ERP
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public bool UploadToHostAdjustment(string connectionStringName, int receiptId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Stock_Adjustment";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion "UploadToHostAdjustment"

    #region LinesReceived
    /// <summary>
    /// Retreives Number ReceiptLines left for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public DataSet LinesReceived(string connectionStringName, int inboundShipmentId, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Lines_Received";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "LinesReceived"

    #region PackageLinesReceived
    /// <summary>
    /// Retreives Number ReceiptLines left for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public DataSet PackageLinesReceived(string connectionStringName, int receiptId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Package_Lines_Received";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "PackageLinesReceived"

    #region DocumentReceived
    /// <summary>
    /// Updates a Receipt to Received
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    public bool DocumentReceived(string connectionStringName, int inboundShipmentId, int receiptId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receipt_Update_Status_Received";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DocumentReceived"

    #region UpdateReceipt
    /// <summary>
    /// Retreives ReceiptLines available for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="locationId"></param>
    /// <param name="receiptId"></param>
    /// <param name="allowPalletise"></param>
    /// <param name="orderNumber"></param>
    /// <returns></returns>
    /// 


    public DataSet UpdateReceipt(string connectionStringName
                                , int locationId
                                , int receiptId
                                , bool allowPalletise
                                , string orderNumber
        //,int inboundShipmentId

        )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Station_Sheet_Receipt_Receive_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "AllowPalletise", DbType.Boolean, allowPalletise);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "UpdateReceipt"

    #region SearchPickingJobs
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchPickingJobs(string connectionStringName, int inboundShipmentId, int receiptId, string instructionTypeCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Job_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InboundShipmentId", DbType.Int32, inboundShipmentId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchPickingJobs"

    #region SearchLinesByJob
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchLinesByJob(string connectionStringName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Lines_By_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchLinesByJob"

    #region UpdateLine
    /// <summary>
    /// Retreives ReceiptLines available for processing.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="inboundShipmentId"></param>
    /// <param name="receiptId"></param>
    /// <returns></returns>
    /// 


    public DataSet UpdateLine(string connectionStringName, Int32 instructionId, Decimal confirmedQuantity)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receipt_Instruction_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "UpdateLine"
}

