using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Batch
/// </summary>
public class Intergration
{
    #region Constructor Logic
    public Intergration()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetFileFields
    public DataSet GetFileFields(string connectionStringName, int InterfaceMappingFileId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Intergration_Get_File_Fields";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InterfaceMappingFileId", DbType.Int32, InterfaceMappingFileId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;

    }
    #endregion GetFileFields

    #region GetMappingColumn
    public DataSet GetMappingColumn(string connectionStringName, int interfaceMappingFileId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Intergration_Get_Column_Mapping";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "interfaceMappingFileId", DbType.Int32, interfaceMappingFileId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;

    }
    #endregion GetMappingColumn

    #region SaveMapping
    public static bool SaveMapping(string connectionStringName, int interfaceMappingFileId, int interfaceMappingColumnId, int interfaceFieldId, int orderBy, int startPosition, int endPosition, string format)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Intergration_Mapping_Save";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "interfaceMappingFileId", DbType.Int32, interfaceMappingFileId);
        db.AddInParameter(dbCommand, "interfaceMappingColumnId", DbType.Int32, interfaceMappingColumnId);
        db.AddInParameter(dbCommand, "interfaceFieldId", DbType.Int32, interfaceFieldId);
        db.AddInParameter(dbCommand, "orderBy", DbType.Int32, orderBy);
        db.AddInParameter(dbCommand, "startPosition", DbType.Int32, startPosition);
        db.AddInParameter(dbCommand, "endPosition", DbType.Int32, endPosition);
        db.AddInParameter(dbCommand, "format", DbType.String, format);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion SaveMapping

    #region DeleteMapping
    public static bool DeleteMapping(string connectionStringName, int interfaceMappingColumnId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Intergration_Mapping_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "interfaceMappingColumnId", DbType.Int32, interfaceMappingColumnId);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //connection.Close();

        return result;
    }
    #endregion DeleteMapping

    #region SampleDownload
    public DataSet SampleDownload(string connectionStringName, int interfaceMappingFileId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Report_Interface_Sample";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "interfaceMappingFileId", DbType.Int32, interfaceMappingFileId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;

    }
    #endregion SampleDownload

    #region GetInterfaceFilePrefix
    public string GetInterfaceFilePrefix(string connectionStringName, int interfaceMappingFileId)
    {
        string interfaceFilePrefix = "ERR";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Intergration_Get_InterfaceFilePrefix";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "interfaceMappingFileId", DbType.Int32, interfaceMappingFileId);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "interfaceFilePrefix", DbType.String, ParameterDirection.InputOutput, "interfaceFilePrefix", DataRowVersion.Proposed, interfaceFilePrefix);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                interfaceFilePrefix = (string)db.GetParameterValue(dbCommand, "interfaceFilePrefix");
            }
            catch (Exception ex)
            {
                string str = ex.Message.ToString();
            }

            connection.Close();

            return interfaceFilePrefix;
        }
    }
    #endregion GetInterfaceFilePrefix

    #region GetInterfaceFileExtension
    public string GetInterfaceFileExtension(string connectionStringName, int interfaceMappingFileId)
    {
        string fileExtension = "ERR";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Intergration_Get_FileExtension";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "interfaceMappingFileId", DbType.Int32, interfaceMappingFileId);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "fileExtension", DbType.String, ParameterDirection.InputOutput, "fileExtension", DataRowVersion.Proposed, fileExtension);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                fileExtension = (string)db.GetParameterValue(dbCommand, "fileExtension");
            }
            catch (Exception ex)
            {
                string str = ex.Message.ToString();
            }

            connection.Close();

            return fileExtension;
        }
    }
    #endregion GetInterfaceFileExtension
}

