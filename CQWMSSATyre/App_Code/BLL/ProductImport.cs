using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

/// <summary>
/// Summary description for ProductImport
/// </summary>
public class ProductImport
{
	public ProductImport()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    #region ProductImport

    public DataSet ProductImport_Search(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Wizard_Product_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    public static bool ProductImport_Truncate(string connectionStringName)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Wizard_Product_Truncate";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

    public bool ProductImport_Update(string connectionStringName,
    string productCode,
    string skuCode,
    string product,
    string productBarcode,
    decimal? minimumQuantity,
    decimal? reorderQuantity,
    decimal? maximumQuantity,
    int? curingPeriodDays,
    int? shelfLifeDays,
    string qualityAssuranceIndicator,
    string productType,
    string packType,
    decimal? packQuantity,
    string packBarcode,
    decimal? packVolume,    
    decimal? packWeight,
    int productImportId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ProductImport_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ProductImportId", DbType.Int32, productImportId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "ProductBarcode", DbType.String, productBarcode);
        db.AddInParameter(dbCommand, "MinimumQuantity", DbType.Decimal, minimumQuantity);
        db.AddInParameter(dbCommand, "ReorderQuantity", DbType.Decimal, reorderQuantity);
        db.AddInParameter(dbCommand, "MaximumQuantity", DbType.Decimal, maximumQuantity);
        db.AddInParameter(dbCommand, "CuringPeriodDays", DbType.Int32, curingPeriodDays);
        db.AddInParameter(dbCommand, "ShelfLifeDays", DbType.Int32, shelfLifeDays);
        db.AddInParameter(dbCommand, "QualityAssuranceIndicator", DbType.Boolean, bool.Parse(qualityAssuranceIndicator));
        db.AddInParameter(dbCommand, "ProductType", DbType.String, productType);
        db.AddInParameter(dbCommand, "PackType", DbType.String, packType);
        db.AddInParameter(dbCommand, "PackQuantity", DbType.Decimal, packQuantity);
        db.AddInParameter(dbCommand, "PackBarcode", DbType.String, packBarcode);
        db.AddInParameter(dbCommand, "PackVolume", DbType.Decimal, packVolume);
        db.AddInParameter(dbCommand, "PackWeight", DbType.Decimal, packWeight);
        db.AddInParameter(dbCommand, "HasError", DbType.Boolean, false);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;

    }

    public static bool ProductImport_Delete(string connectionStringName, int productImportId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ProductImport_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "ProductImportId", DbType.Int32, productImportId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

    public static bool ProductImport_Add(string connectionStringName)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Wizard_Product_Import";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

    public static bool ProductImport_CheckError(string connectionStringName)
    {

        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Wizard_Product_CheckError";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                DataSet dataSet = null;
                dataSet = db.ExecuteDataSet(dbCommand);
                db.ExecuteNonQuery(dbCommand);
                result = bool.Parse(dataSet.Tables[0].Rows[0]["Error"].ToString());
            }
            catch { }
        }
        //connection.Close();

        return result;
    }

    #endregion
}
