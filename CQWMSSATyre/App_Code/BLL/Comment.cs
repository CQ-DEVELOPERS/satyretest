using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   16 Oct 2007
/// Summary description for Planning
/// </summary>
public class Comment
{
    #region Constructor Logic
    public Comment()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SelectComment
    /// <summary>
    /// Retrieves comments linked to a specific TableId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <returns></returns>
    public DataSet SelectComment(string connectionStringName,
                                 int tableId,
                                 string tableName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Comment_Link_Select";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "tableId", DbType.Int32, tableId);
        db.AddInParameter(dbCommand, "tableName", DbType.String, tableName);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SelectComment"

    #region InsertComment
    /// <summary>
    /// Insert a comment for a specific CallOffHeaderId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="callOffHeaderId"></param>
    /// <param name="comment"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool InsertComment(string connectionStringName,
                              int tableId,
                              string tableName,
                              string comment,
                              int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Comment_Link_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "tableId", DbType.Int32, tableId);
        db.AddInParameter(dbCommand, "tableName", DbType.String, tableName);
        db.AddInParameter(dbCommand, "comment", DbType.String, comment);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "InsertComment"

    #region UpdateComment
    /// <summary>
    /// Update a comment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="commentId"></param>
    /// <param name="comment"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public bool UpdateComment(string connectionStringName,
                              int commentId,
                              string comment,
                              int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Comment_Link_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "commentId", DbType.Int32, commentId);
        db.AddInParameter(dbCommand, "comment", DbType.String, comment);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateComment"

    #region DeleteComment
    /// <summary>
    /// Delete a comment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="commentId"></param>
    /// <returns></returns>
    public bool DeleteComment(string connectionStringName,
                              int commentId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Comment_Link_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "commentId", DbType.Int32, commentId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteComment"
}
