﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillingCategory
/// </summary>
[DataObject]
public class BillingCategory
{
  public BillingCategory()
  {

  }

  public DataTable GetBillingCategoryByPrincipal( int principalId )
  {
    string cn = HttpContext.Current.Session["ConnectionStringName"].ToString();

    // Create the Database object, passing it the required database service.
    Database db = DatabaseFactory.CreateDatabase( cn );

    string sqlCommand = "p_BillingCategory_Search";

    DbCommand dbCommand = db.GetStoredProcCommand( sqlCommand );

    // Add paramters
    db.AddInParameter( dbCommand, "PrincipalId", DbType.Int32, principalId );

    // DataSet that will hold the returned results
    var data = new DataTable( "BillingCategories" );

    // Execute the command
    data.Load( db.ExecuteReader( dbCommand ) );

    return data;
  }

}