using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for MixedPutAway
/// </summary>
public class MixedJobCreate
{
    #region Constructor Logic
    public MixedJobCreate()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region GetAvailable
    /// <summary>
    /// Get available Jobs
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionTypeCode"></param>
    /// <param name="jobId"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet GetAvailable(string connectionStringName, string instructionTypeCode, int jobId, DateTime fromDate, DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mixed_Job_Available";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetAvailable(string connectionStringName, string instructionTypeCode, int jobId, int shipmentId, string orderNumber, DateTime fromDate, DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mixed_Job_Available";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, shipmentId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetAvailable"

    #region GetDetails
    /// <summary>
    /// Gets instructions linked to Job
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <returns></returns>
    public DataSet GetDetails(string connectionStringName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mixed_Job_Details";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetDetails"

    #region GetLinked
    /// <summary>
    /// Gets instructions linked to Job
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <returns></returns>
    public DataSet GetLinked(string connectionStringName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mixed_Job_Linked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetLinked"

    #region GetUnlinked
    /// <summary>
    /// Search for availbale unlinked instructions
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionTypeCode"></param>
    /// <param name="jobId"></param>
    /// <param name="productCode"></param>
    /// <param name="product"></param>
    /// <param name="location"></param>
    /// <returns></returns>
    public DataSet GetUnlinked(string connectionStringName, string instructionTypeCode, int jobId, string productCode, string product, string location)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mixed_Job_Unlinked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "Location", DbType.String, location);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetUnlinked(string connectionStringName, string instructionTypeCode, int jobId, int shipmentId, string orderNumber, string productCode, string product, string location)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mixed_Job_Unlinked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "ShipmentId", DbType.Int32, shipmentId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "Location", DbType.String, location);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetUnlinked"

    #region Transfer
    /// <summary>
    /// Adds a Instruction to a Mixed Pallet Job.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public bool Transfer(string connectionStringName, int jobId, int instructionId)
    {
        bool result = false;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mixed_Job_Link_Instruction";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Transfer"

    #region Remove
    /// <summary>
    /// Removes an Instruction from a Mixed Pallet Job.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public bool Remove(string connectionStringName, int jobId, int instructionId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mixed_Job_Unlink_Instruction";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Remove"
}
