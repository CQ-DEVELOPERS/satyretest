// DateRange.cs

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Creates date ranges for use by the GUI or for queries. The caller must handle any exceptions.
/// </summary>
public class DateRange
{
    #region Constructor Logic
    public DateRange()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region FromDate
    /// <summary>
    /// Sets the From DateTime.
    /// </summary>
    /// <returns></returns>
    public static DateTime GetFromDate()
    {
        DateTime today = DateTime.Today;

        return new DateTime(today.Year,
                            today.Month,
                            today.Day,
                            00,
                            00,
                            00);
    }
    #endregion "FromDate"

    #region ToDate
    /// <summary>
    /// Sets the To DateTime.
    /// </summary>
    /// <returns></returns>
    public static DateTime GetToDate()
    {
        DateTime today = DateTime.Today;

        return new DateTime(today.Year,
                            today.Month,
                            today.Day,
                            23,
                            59,
                            59);
    }
    #endregion "ToDate"
}
