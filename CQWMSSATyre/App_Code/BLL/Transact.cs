using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;


/// <summary>
/// Summary description for Transact
/// </summary>
public class Transact
{
    #region Constructor Logic
    public Transact()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region DwellTime
    public int DwellTime(string connectionStringName, int type, int operatorId)
    {

        //Type 2 and OperatorId to check if Dwell Time is on or off
        //Type 0 for the menu
        //Type 1 for Insert

        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Dwell_Time";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "type", DbType.Int32, type);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DwellTime


    #region DwellTimeReasons
    public DataSet DwellTimeReasons(string connectionStringName)
    {
        int type = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Dwell_Time";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "type", DbType.Int32, type);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion DwellTimeReasons


    #region DwellTimeUpdate
    public DataSet DwellTimeUpdate(string connectionStringName, int reasonId, int operatorId)
    {
        int type = 1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Dwell_Time";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "type", DbType.Int32, type);
        db.AddInParameter(dbCommand, "reasonId", DbType.Int32, reasonId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion DwellTimeUpdate

    #region ConfirmProduct
    public int ConfirmProduct(string connectionStringName, int instructionId, string barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Product";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public int ConfirmProduct(string connectionStringName, int instructionId, string barcode, string type)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Product";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "type", DbType.String, type);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmProduct

    #region ConfirmBatch
    public int ConfirmBatch(string connectionStringName, int instructionId, string barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Batch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmBatch

    #region ConfirmStoreLocation
    public int ConfirmStoreLocation(string connectionStringName, int instructionId, string storeBarcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "storeBarcode", DbType.String, storeBarcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmStoreLocation

    #region ConfirmPickLocation
    public int ConfirmPickLocation(string connectionStringName, int instructionId, string pickBarcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "pickBarcode", DbType.String, pickBarcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmPickLocation

    #region ConfirmQuantity
    public int ConfirmQuantity(string connectionStringName, int instructionId, Decimal confirmedQuantity)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmQuantity

    #region ConfirmPalletWeight
    public int ConfirmPalletWeight(string connectionStringName, int instructionId, decimal confirmedWeight)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Pallet_Weight";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "confirmedWeight", DbType.Int32, confirmedWeight);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch
            {
                result = -1;
            }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmPalletWeight


    #region TareWeight
    public int TareWeight(string connectionStringName, int instructionId, int tareWeight)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Tare_Weight";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "tareWeight", DbType.Decimal, tareWeight);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion TareWeight

    #region CancelTransaction
    public int CancelTransaction(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Cancel_Transaction";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CancelTransaction

    #region Pick
    public int Pick(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Pick_Transaction";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Pick

    #region Store
    public int Store(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Store_Transaction";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Store

    #region GetPickingFull
    public int GetPickingFull(string connectionStringName, int operatorId)
    {
        int instructionId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Picking_Full";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch (Exception ex)
            {
                string str = ex.Message.ToString();
            }

            connection.Close();

            return instructionId;
        }
    }
    #endregion GetPickingFull

    #region GetPickingMixed
    public int GetPickingMixed(string connectionStringName, int operatorId, string referenceNumber)
    {
        int instructionId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Picking_Mixed";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return instructionId;
        }
    }
    #endregion GetPickingMixed

    #region GetPutawayFull
    public int GetPutawayFull(string connectionStringName, int operatorId, int palletId)
    {
        int instructionId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Store_Full";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return instructionId;
        }
    }
    #endregion GetPutawayFull

    #region GetPutawayMixed
    public int GetPutawayMixed(string connectionStringName, int operatorId, string barcode)
    {
        int instructionId = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Store_Mixed";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                instructionId = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return instructionId;
        }
    }
    #endregion GetPutawayMixed

    #region GetPutawayMixedNextLine
    public int GetPutawayMixedNextLine(string connectionStringName, int operatorId, int instructionId, string barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Store_Mixed_Next";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "instructionId", DbType.Int32, ParameterDirection.InputOutput, "instructionId", DataRowVersion.Proposed, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                result = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetPutawayMixedNextLine

    #region PalletCreate
    public int PalletCreate(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Movement_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "instructionId", DbType.Int32, ParameterDirection.InputOutput, "instructionId", DataRowVersion.Proposed, instructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                result = (int)db.GetParameterValue(dbCommand, "instructionId");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion PalletCreate

    #region PalletUpdate
    public bool PalletUpdate(string connectionStringName, int instructionId, int palletId, Decimal quantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Movement_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion PalletUpdate

    #region GetInstructionDetails
    public DataSet GetInstructionDetails(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Instruction_Detail";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetMixedDetails

    #region GetMixedDetails
    public DataSet GetMixedDetails(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Mixed_Detail";
        ///string sqlCommand = connectionStringName + ".dbo.p_Mobile_Putaway_Mixed";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetMixedDetails

    #region LocationErrorStore
    public int LocationErrorStore(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Location_Error";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "storeError", DbType.Boolean, 1);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion LocationErrorStore

    #region LocationErrorPick
    public int LocationErrorPick(string connectionStringName, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Location_Error";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "pickError", DbType.Boolean, 1);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion LocationErrorPick

    #region SearchProducts
    /// <summary>
    /// Searchs Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchProducts(string productCode, string barcode)
    {

        Database db = DatabaseFactory.CreateDatabase("Connection String");
        DataSet genDataSet = new DataSet();

        string sqlCommand = "dbo.p_Mobile_Product_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);

        // DataSet that will hold the returned results       

        genDataSet = db.ExecuteDataSet(dbCommand);

        return genDataSet;
    }
    #endregion SearchProducts

    #region NewJob
    public int NewJob(string connectionStringName, int operatorId, string referenceNumber, int containerId, int jobId, int instructionId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_New_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "containerId", DbType.Int32, containerId);
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

        // Set InstructionId as an output parameter
        db.AddParameter(dbCommand, "jobId", DbType.Int32, ParameterDirection.InputOutput, "jobId", DataRowVersion.Proposed, jobId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                result = (int)db.GetParameterValue(dbCommand, "jobId");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion NewJob

    #region GetContainerTypes
    public DataSet GetContainerTypes(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ContainerType_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetContainerTypes
}