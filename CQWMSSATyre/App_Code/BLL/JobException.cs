using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Planning
/// </summary>
public class JobException
{
    #region Constructor Logic
    public JobException()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SearchPickingJobs
    /// <summary>
    /// Retrieves Lines by Job
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet SearchPickingJobs(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_JobException_Job_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchPickingJobs"

    #region SearchLinesByJob
    /// <summary>
    /// Retrieves Lines by Job
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <param name="instructionTypeCode"></param>
    /// <returns></returns>
    public DataSet SearchLinesByJob(string connectionStringName, int jobId, string instructionTypeCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_JobException_Lines_By_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchLinesByJob"

    #region UpdateLine
    /// <summary>
    /// Updates an instructions Check Quantity
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <param name="instructionId"></param>
    /// <param name="checkQuantity"></param>
    /// <returns></returns>
    public bool UpdateLine(string connectionStringName, int jobId, int instructionId, Decimal checkQuantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_PickException_Update_Line";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "checkQuantity", DbType.Decimal, checkQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateLine"
}
