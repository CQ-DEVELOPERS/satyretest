using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for OutboundDocument
/// </summary>
public class Housekeeping
{
    #region Constructor Logic
    public Housekeeping()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region MovementSearch
    /// <summary>
    /// Searches for Product to Move
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="instructionTypeCode"></param>
    /// <param name="productCode"></param>
    /// <param name="product"></param>
    /// <param name="skuCode"></param>
    /// <param name="batch"></param>
    /// <param name="pickLocation"></param>
    /// <returns></returns>
    public DataSet MovementSearch(string connectionStringName,
                                    int warehouseId,
                                    string instructionTypeCode,
                                    string productCode,
                                    string product,
                                    string skuCode,
                                    string batch,
                                    string pickLocation)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Housekeeping_SUBL_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "PickLocation", DbType.String, pickLocation);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion MovementSearch

    #region UpdateOutboundDocument
    /// <summary>
    /// Updates an OutboundDocument
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundDocumentId"></param>
    /// <param name="outboundDocumentTypeId"></param>
    /// <param name="customerId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="deliveryDate"></param>
    /// <returns></returns>
    public bool UpdateOutboundDocument(string connectionStringName,
                                       int outboundDocumentId,
                                       int outboundDocumentTypeId,
                                       int customerId,
                                       string orderNumber,
                                       DateTime deliveryDate)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_OutboundDocument_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundDocumentId", DbType.Int32, outboundDocumentId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "ExternalCompanyId", DbType.Int32, customerId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "DeliveryDate", DbType.DateTime, deliveryDate);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateOutboundDocument

    #region PalletReprint
    /// <summary>
    /// Searches for Product to Move
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="instructionTypeCode"></param>
    /// <param name="productCode"></param>
    /// <param name="product"></param>
    /// <param name="skuCode"></param>
    /// <param name="batch"></param>
    /// <param name="pickLocation"></param>
    /// <returns></returns>
    public DataSet PalletReprint(string connectionStringName,
                                    int warehouseId,
                                    string instructionTypeCode,
                                    string productCode,
                                    string product,
                                    string skuCode,
                                    string batch,
                                    string pickLocation)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Report_Pallet_Print";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, skuCode);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "PickLocation", DbType.String, pickLocation);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion PalletReprint

    #region PalletInsert
    /// <summary>
    /// Inserts Product into table for Pallet reprint
    /// </summary>
    /// <param name="StorageUnitBatchId"></param>
    /// <param name="PickLocationId"></param>
    /// <returns>PalletId</returns>
    public int PalletInsert(string connectionStringName, int StorageUnitBatchId, int PickLocationId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Report_Pallet_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "@StorageUnitBatchId", DbType.Int32, StorageUnitBatchId);
        db.AddInParameter(dbCommand, "@LocationId", DbType.Int32, PickLocationId);
        db.AddOutParameter(dbCommand, "@PalletID", DbType.Int32, 10);

        db.ExecuteNonQuery(dbCommand);

        // DataSet that will hold the returned results
        //DataSet dataSet = null;

        return int.Parse(db.GetParameterValue(dbCommand, "@PalletID").ToString());

    }
    #endregion PalletInsert

    #region ManualFileTypeSearch
    /// <summary>
    /// Searches for manual file types
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet ManualFileTypeSearch(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ManualFileRequestType_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ManualFileTypeSearch

    #region ManualFileColumnSearch
    /// <summary>
    /// Searches for manual file types
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet ManualFileColumnSearch(string connectionStringName, int ManualFileRequestTypeId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ManualFileRequestColumn_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ManualFileRequestTypeId", DbType.String, ManualFileRequestTypeId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    #endregion ManualFileColumnSearch

    #region ManualFileRequest_Send
    public static int ManualFileRequest_Send(string connectionStringName, string xmlBody)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ManualFileRequest_Extract";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "@XMLBody", DbType.String, xmlBody);

        return db.ExecuteNonQuery(dbCommand);
    }
    #endregion ManualFileRequest_Send

    #region ForceUploadSearch
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="orderNumber"></param>
    /// <returns></returns>
    public DataSet ForceUploadSearch(string connectionStringName, int warehouseId, string orderNumber)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Force_Upload_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    #endregion ForceUploadSearch

    #region ForceUploadUpdate
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InterfaceExportHeaderId"></param>
    /// <returns></returns>
    public bool ForceUploadUpdate(string connectionStringName,
                                       int InterfaceExportHeaderId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Force_Upload_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InterfaceExportHeaderId", DbType.Int32, InterfaceExportHeaderId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ForceUploadUpdate
}
