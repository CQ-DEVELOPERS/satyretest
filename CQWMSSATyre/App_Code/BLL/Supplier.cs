using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   11 July 2007
/// Summary description for OutboundDocumentType
/// </summary>
public class Supplier
{
    public Supplier()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region SearchSuppliers
    /// <summary>
    /// Retreives Suppliers.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="supplier"></param>
    /// <param name="supplierCode"></param>
    /// <returns></returns>
    public DataSet SearchSuppliers(string connectionStringName, string supplier, string supplierCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_ExternalCompany_Search_Supplier";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, supplier);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, supplierCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchSuppliers"
}
