using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Planning
/// </summary>
public class InterfaceConsole
{
    #region Constructor Logic
    public InterfaceConsole()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SearchStockAdjustments
    /// <summary>
    /// Retrieves Space Utilisation
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    public DataSet SearchStockAdjustments(string connectionStringName, int warehouseId,
            string productCode,
            string product,
            string batch,
            string additional1,
            string additional2,
            string additional3,
            string status,
            string message,
            string recordType,
            DateTime fromDate,
            DateTime toDate
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Interface_Console_SA";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "additional1", DbType.String, additional1);
        db.AddInParameter(dbCommand, "additional2", DbType.String, additional2);
        db.AddInParameter(dbCommand, "additional3", DbType.String, additional3);
        db.AddInParameter(dbCommand, "status", DbType.String, status);
        db.AddInParameter(dbCommand, "message", DbType.String, message);
        db.AddInParameter(dbCommand, "recordType", DbType.String, recordType);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchStockAdjustments

    #region Reprocess
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="interfaceId"></param>
    /// <param name="interfaceTableId"></param>
    /// <returns></returns>
    public bool Reprocess(string connectionStringName, int interfaceId, int interfaceTableId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Interface_Reprocess";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "interfaceTableId", DbType.Int32, interfaceTableId);
        db.AddInParameter(dbCommand, "interfaceId", DbType.Int32, interfaceId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Reprocess"

    #region SearchOrders
    /// <summary>
    /// Retrieves Space Utilisation
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    public DataSet SearchOrders(string connectionStringName, int warehouseId,
            string orderNumber,
            string company,
            string companyCode,
            string productCode,
            string product,
            string batch,
            string status,
            string message,
            string recordType,
            DateTime fromDate,
            DateTime toDate
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Interface_Console_Orders";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "company", DbType.String, company);
        db.AddInParameter(dbCommand, "companyCode", DbType.String, companyCode);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "status", DbType.String, status);
        db.AddInParameter(dbCommand, "message", DbType.String, message);
        db.AddInParameter(dbCommand, "recordType", DbType.String, recordType);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchOrders

    #region SearchStockAdjustments
    /// <summary>
    /// Search Stock Adjustments
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    public DataSet SearchStockAdjustments(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Interface_Console_PM";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchStockAdjustments

    #region SearchInterfaceImportOrders
    /// <summary>
    /// Retrieves Imported Interface Orders
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    public DataSet SearchInterfaceImportOrders(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Interface_Console_ImportOrders";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchInterfaceImportOrders
	
	#region SearchTakeOnStockUpload
    /// <summary>
    /// Retrieves Space Utilisation
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    public DataSet SearchTakeOnStockUpload(string connectionStringName, int warehouseId,
            string productCode,
            string location,
            string batch,
            string status,
            string message,
            string recordType,
            DateTime fromDate,
            DateTime toDate
    )
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Interface_Console_TakeOnStock";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "location", DbType.String, location);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "status", DbType.String, status);
        db.AddInParameter(dbCommand, "message", DbType.String, message);
        db.AddInParameter(dbCommand, "recordType", DbType.String, recordType);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchTakeOnStockUpload

    #region SearchTakeOnStockUpload
    /// <summary>
    /// Search Stock Adjustments
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <returns></returns>
    public DataSet SearchTakeOnStockUpload(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Interface_Console_TOS";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchTakeOnStockUpload
}
