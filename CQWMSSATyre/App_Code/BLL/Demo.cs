using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   16 Oct 2007
/// Summary description for Planning
/// </summary>
public class Demo
{
    #region Constructor Logic
    public Demo()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region ResetDatabase
    /// <summary>
    /// Resets the entire database to it's original settings
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public bool ResetDatabase(string connectionStringName)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Demo_Reset";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "ResetDatabase"

    #region GetOperators
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <returns></returns>
    public DataSet GetOperators(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Demo_Get_Operators";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchCustomers
}
