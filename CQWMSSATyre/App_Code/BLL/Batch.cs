using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Batch
/// </summary>
public class Batch
{
    #region Constructor Logic
    public Batch()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region SearchBatches
    /// <summary>
    /// Searches Batches by Batch and ECLNumber
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="batch"></param>
    /// <param name="eCLNumber"></param>
    /// <returns></returns>
    public DataSet SearchBatches(string connectionStringName, string batch, string eCLNumber)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Batch_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "ECLNumber", DbType.String, eCLNumber);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchBatches"

    #region GetBatchesByStorageUnit
    /// <summary>
    /// Searches Batch by StorageUnitId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitId"></param>
    /// <returns></returns>
    public DataSet GetBatchesByStorageUnit(string connectionStringName, int storageUnitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Batch_Search_StorageUnit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetBatchesByStorageUnit

    #region GetBatchesNotExpired
    /// <summary>
    /// Searches Batch by StorageUnitId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitId"></param>
    /// <returns></returns>
    public DataSet GetBatchesNotExpired(string connectionStringName, int storageUnitId)
    {

        DataSet dataSet = null;
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Batch_Search_ExpiryDate";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);

            // DataSet that will hold the returned results		
            dataSet = db.ExecuteDataSet(dbCommand);
            
        }
        catch (Exception exp)
        {

            string str = exp.Message.ToString();
        }
        return dataSet;

    }
    #endregion GetBatchesNotExpired

    #region GetBatchesByBatchId
    /// <summary>
    /// Searches Batch by StorageUnitId
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitId"></param>
    /// <returns></returns>
    public DataSet GetBatchesByBatchId(string connectionStringName, int batchId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Batch_Search_BatchId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "batchId", DbType.Int32, batchId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetBatchesByStorageUnit

    #region SearchBatchesByStorageUnit
    /// <summary>
    /// Searches Batches by StorageUnitId Batch and ECLNumber
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="batch"></param>
    /// <param name="eCLNumber"></param>
    /// <returns></returns>
    public DataSet SearchBatchesByStorageUnit(string connectionStringName, int storageUnitId, string batch, string eCLNumber)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Batch_Search_StorageUnit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "ECLNumber", DbType.String, eCLNumber);

        // DataSet that will hold the returned results		
        DataSet genDataSet = null;

        genDataSet = db.ExecuteDataSet(dbCommand);

        return genDataSet;
    }
    #endregion SearchBatchesByStorageUnit

    #region SearchBatchesByProduct
    /// <summary>
    /// Searches Batches by StorageUnitId Batch and ECLNumber
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="batch"></param>
    /// <param name="eCLNumber"></param>
    /// <returns></returns>
    public DataSet SearchBatchesByProduct(string connectionStringName, int productId, string batch, string eCLNumber)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Batch_Search_Product";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductId", DbType.Int32, productId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "ECLNumber", DbType.String, eCLNumber);

        // DataSet that will hold the returned results		
        DataSet genDataSet = null;

        genDataSet = db.ExecuteDataSet(dbCommand);

        return genDataSet;
    }
    #endregion SearchBatchesByProduct

    #region SearchBatchesDate
    /// <summary>
    /// Retrieves Outbound Orders
    /// @WarehouseId            int,
    //  @BatchNumber	         varchar(30),
    //  @BatchStatusId			 int,
    //  @DaysoldLt				 int,
    //  @DaysOldGt				 int,
    //  @FromDate	             datetime,
    //  @ToDate	                datetime
    /// </summary>
    public DataSet SearchBatchesDate(string connectionStringName,
                                int warehouseId,
                                string BatchNumber,
                                string ProductCode,
                                string Product,
                                int BatchStatusId,
                                int DaysoldLt,
                                int DaysOldGt,
                                DateTime fromDate,
                                DateTime toDate,
                                string SOHOnly)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        string sqlCommand = connectionStringName + ".dbo.p_Batch_Search_Full";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "BatchNumber", DbType.String, BatchNumber);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, ProductCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, Product);
        db.AddInParameter(dbCommand, "BatchStatusId", DbType.Int32, BatchStatusId);
        db.AddInParameter(dbCommand, "DaysoldLt", DbType.Int32, DaysoldLt);
        db.AddInParameter(dbCommand, "DaysOldGt", DbType.Int32, DaysOldGt);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "SOHOnly", DbType.String, SOHOnly);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchBatchesDate"

    #region SearchBatchesByBatch
    /// <summary>
    /// Searches Batches by StorageUnitId Batch and ECLNumber
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="batch"></param>
    /// <param name="eCLNumber"></param>
    /// <returns></returns>
    public DataSet SearchBatchesByBatch(string connectionStringName, int warehouseId, string batch, string batchReferenceNumber)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_BatchNumber_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "BatchReferenceNumber", DbType.String, batchReferenceNumber);

        // DataSet that will hold the returned results		
        DataSet genDataSet = null;

        genDataSet = db.ExecuteDataSet(dbCommand);

        return genDataSet;
    }
    #endregion SearchBatchesByBatch

    #region InsertBatch
    public int InsertBatch(string connectionStringName,
                                string ProductCode,
                                string Product,
                                int batchId,
                                int warehouseId,
                                int storageUnitId,
                                int operatorId,
                                int statusId,
                                string batch,
                                DateTime expiryDate,
                                int incubationDays,
                                int shelfLifeDays,
                                int expectedYield,
                                int actualYield,
                                string eCLNumber,
                                string batchReferenceNumber,
                                DateTime productionDateTime,
                                int filledYield,
                                bool cOACertificate,
                                int receiptId,
                                Decimal RI,
                                Decimal density
                                )
    {
        int storageUnitBatchId = 0;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Batch_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, statusId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "ExpiryDate", DbType.DateTime, expiryDate);
        db.AddInParameter(dbCommand, "IncubationDays", DbType.Int32, incubationDays);
        db.AddInParameter(dbCommand, "ShelfLifeDays", DbType.Int32, shelfLifeDays);
        db.AddInParameter(dbCommand, "ExpectedYield", DbType.Int32, expectedYield);
        db.AddInParameter(dbCommand, "ActualYield", DbType.Int32, actualYield);
        db.AddInParameter(dbCommand, "ECLNumber", DbType.String, eCLNumber);
        db.AddInParameter(dbCommand, "BatchReferenceNumber", DbType.String, batchReferenceNumber);
        db.AddInParameter(dbCommand, "ProductionDateTime", DbType.DateTime, productionDateTime);
        db.AddInParameter(dbCommand, "FilledYield", DbType.Int32, filledYield);
        db.AddInParameter(dbCommand, "COACertificate", DbType.Boolean, cOACertificate);
        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "RI", DbType.Decimal, RI);
        db.AddInParameter(dbCommand, "Density", DbType.Decimal, density);

        // Set InstructionId as an output parameter
        db.AddOutParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                db.ExecuteNonQuery(dbCommand);

                // Get InstructionId output parameter
                storageUnitBatchId = (int)db.GetParameterValue(dbCommand, "StorageUnitBatchId");
            }
            catch (Exception ex)
            {
                string str = ex.Message.ToString();
            }

            connection.Close();
            
            return storageUnitBatchId;
        }

    }

    #endregion

    #region UpdateBatch

    public DataSet UpdateBatch(string connectionStringName,
                                string ProductCode,
                                string Product,
                                int batchId,
                                int warehouseId,
                                int storageUnitId,
                                int operatorId,
                                int statusId,
                                string batch,
                                DateTime expiryDate,
                                int incubationDays,
                                int shelfLifeDays,
                                int expectedYield,
                                int actualYield,
                                string eCLNumber,
                                string batchReferenceNumber,
                                DateTime productionDateTime,
                                int filledYield,
                                bool cOACertificate,
                                int receiptId,
                                Decimal RI,
                                Decimal density
                                )
    {



        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Batch_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "StatusId", DbType.Int32, statusId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "ExpiryDate", DbType.DateTime, expiryDate);
        db.AddInParameter(dbCommand, "IncubationDays", DbType.Int32, incubationDays);
        db.AddInParameter(dbCommand, "ShelfLifeDays", DbType.Int32, shelfLifeDays);
        db.AddInParameter(dbCommand, "ExpectedYield", DbType.Int32, expectedYield);
        db.AddInParameter(dbCommand, "ActualYield", DbType.Int32, actualYield);
        db.AddInParameter(dbCommand, "ECLNumber", DbType.String, eCLNumber);
        db.AddInParameter(dbCommand, "BatchReferenceNumber", DbType.String, batchReferenceNumber);
        db.AddInParameter(dbCommand, "ProductionDateTime", DbType.DateTime, productionDateTime);
        db.AddInParameter(dbCommand, "FilledYield", DbType.Int32, filledYield);
        db.AddInParameter(dbCommand, "COACertificate", DbType.Boolean, cOACertificate);
        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "RI", DbType.Decimal, RI);
        db.AddInParameter(dbCommand, "Density", DbType.Decimal, density);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    #endregion

    #region GetBatchFields

    public DataSet GetBatchFields(string connectionStringName)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_BatchFields";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    #endregion

    #region GetBatchStatus
    public DataSet GetBatchStatus(string connectionStringName)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Batch_Status";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetBatchStatus"

    #region GetBatchWarnings
    /// <summary>
    /// Gets any warinings definded in the configurtaion table and displays to the user
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="batchId"></param>
    /// <returns></returns>
    public DataSet GetBatchWarnings(string connectionStringName, int storageUnitBatchId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Batch_Show_Warning";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetBatchesByStorageUnit

    #region LinkBatch
    public bool LinkBatch(string connectionStringName, int warehouseId, int storageUnitId, int batchId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Batch_Link";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "StorageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion LinkBatch

    #region UnlinkBatch
    public bool UnlinkBatch(string connectionStringName, int warehouseId, int storageUnitBatchId, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Batch_Unlink";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UnlinkBatch

    #region LinkedProducts
    /// <summary>
    /// Searches Batches by StorageUnitId Batch and ECLNumber
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="batch"></param>
    /// <param name="eCLNumber"></param>
    /// <returns></returns>
    public DataSet LinkedProducts(string connectionStringName, int warehouseId, int batchId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Linked_Products";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "BatchId", DbType.Int32, batchId);
        
        // DataSet that will hold the returned results		
        DataSet genDataSet = null;

        genDataSet = db.ExecuteDataSet(dbCommand);

        return genDataSet;
    }
    #endregion LinkedProducts

    #region SearchUnlinkedProducts
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchUnlinkedProducts(string connectionStringName, string productCode, string product, int batchId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Static_Unlinked_Products";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "batchId", DbType.Int32, batchId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchUnlinkedProducts

    #region BatchReceive
    public bool BatchReceive(string connectionStringName, int warehouseId, string batch, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Log_Batch_Receive";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion BatchReceive

    #region BatchComplete
    public bool BatchComplete(string connectionStringName, int warehouseId, string batch, int operatorId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Log_Batch_Complete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "Batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion BatchComplete
}
