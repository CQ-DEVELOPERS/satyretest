using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for Planning
/// </summary>
public class BOM
{
    #region Constructor Logic
    public BOM()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SearchOrders
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchOrders(string connectionStringName, string productCode, string product, string barcode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_SO_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet SearchOrders(string connectionStringName, string orderNumber, string productCode, string product, string barcode, DateTime fromDate, DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_SO_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrders"

    #region UpdateOrder
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateOrder(string connectionStringName,
                            int outboundShipmentId,
                            int issueId,
                            string orderNumber,
                            int locationId,
                            int priorityId,
                            int routeId,
                            DateTime deliveryDate,
                            string remarks,
                            string areaType)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_SO_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "priorityId", DbType.Int32, priorityId);
        db.AddInParameter(dbCommand, "routeId", DbType.Int32, routeId);
        db.AddInParameter(dbCommand, "deliveryDate", DbType.DateTime, deliveryDate);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);
        db.AddInParameter(dbCommand, "areaType", DbType.String, areaType);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrder"

    #region Combine
    /// <summary>
    /// Combines issue onto shipment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueId"></param>
    /// <param name="firstIssueId"></param>
    /// <returns></returns>
    public bool Combine(String connectionStringName, int outboundShipmentId, int issueId, int firstIssueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_SO_Combine";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "firstIssueId", DbType.Int32, firstIssueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Combine"

    #region Split
    /// <summary>
    /// Spits issues from shipment
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueId"></param>
    /// <returns></returns>
    public bool Split(String connectionStringName, int issueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_SO_Split";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Split"

    #region DeleteOrder
    /// <summary>
    /// Delete issue
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueId"></param>
    /// <param name="firstIssueId"></param>
    /// <returns></returns>
    public bool DeleteOrder(String connectionStringName, int outboundShipmentId, int issueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_SO_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    /// <summary>
    /// Delete issue
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueId"></param>
    /// <param name="firstIssueId"></param>
    /// <returns></returns>
    public bool DeleteOrder(String connectionStringName, string productCode, int bomHeaderId, int outboundShipmentId, int issueId, string bomOrderNumber, Decimal shipmentTotal)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_SO_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteOrder"

    #region BOMInstructionLine_Search
    public DataSet BOMInstructionLine_Search(string connectionStringName, int warehouseId, int bOMInstructionId, int outboundShipmentId, int issueId)
    {
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_BOM_SO_InstructionLine";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add Parameters
            db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
            db.AddInParameter(dbCommand, "bOMInstructionId", DbType.Int32, bOMInstructionId);
            db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
            db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

            // DataSet that will hold the returned results
            DataSet dataSet = null;

            dataSet = db.ExecuteDataSet(dbCommand);

            return dataSet;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    #endregion BOMInstructionLine_Search

    public static int BOMInstruction_Add(string connectionStringName, int BOMHeaderId, Decimal quantity, string orderNumber, int outboundShipmentId, int issueId)
    {
        int result = 0;
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOMInstruction_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add Parameters
        db.AddInParameter(dbCommand, "BOMHeaderId", DbType.Int32, BOMHeaderId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "issueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results

        result = db.ExecuteNonQuery(dbCommand);

        return result;
    }

    #region InsertPackaging
    public bool InsertPackaging(string connectionStringName,
                                 int operatorId,
                                 int bomInstructionId,
                                 int storageUnitId,
                                 Decimal quantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_Packaging_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "operatorId", DbType.String, operatorId);
        db.AddInParameter(dbCommand, "bomInstructionId", DbType.String, bomInstructionId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion InsertPackaging

    #region SearchPackaging
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchPackaging(string connectionStringName, int bomInstructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_Packaging_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "bomInstructionId", DbType.Int32, bomInstructionId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchPackaging"

    #region DeletePackaging
    public bool DeletePackaging(string connectionStringName,
                                 int bomPackagingId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_Packaging_Delete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "bomPackagingId", DbType.String, bomPackagingId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion DeletePackaging

    #region UpdatePackaging
    public bool UpdatePackaging(string connectionStringName,
                                 int bomPackagingId,
                                 decimal quantity)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_BOM_Packaging_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "bomPackagingId", DbType.String, bomPackagingId);
        db.AddInParameter(dbCommand, "quantity", DbType.String, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion UpdatePackaging
}
