using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class ManualPalletise
{
    #region Constructor Logic
    public ManualPalletise()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetInstructions
    /// <summary>
    /// Gets outstanding Store instructions within a date range.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet GetInstructions(string connectionStringName, DateTime fromDate, DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Store_Instruction_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructions

    #region GetReceiptLines
    /// <summary>
    /// Retruns ReceiptLine in a DataSet
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public DataSet GetReceiptLines(string connectionStringName, int receiptLineId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Palletisation_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetReceiptLines

    #region GetMovementDetails
    /// <summary>
    /// Retruns ReceiptLine in a DataSet
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <param name="pickLocationId"></param>
    /// <returns></returns>
    public DataSet GetMovementDetails(string connectionStringName, int storageUnitBatchId, int pickLocationId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Housekeeping_Movement_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "PickLocationId", DbType.Int32, pickLocationId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetMovementDetails

    #region GetInstructions
    /// <summary>
    /// Gets Instructions linked to a ReceiptLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <returns></returns>
    public DataSet GetInstructions(string connectionStringName, int receiptLineId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Palletisation_Create_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ReceiptLineId", DbType.Int32, receiptLineId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetInstructions

    #region UpdateInstructions
    /// <summary>
    /// Updates Instruction's Quantity
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool UpdateInstructions(string connectionStringName, int instructionId, Decimal quantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Palletisation_Create_Update";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(createCommand, "InstructionId", DbType.Int32, instructionId);
        db.AddInParameter(createCommand, "Quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateInstructions"

    #region AutoLocationAllocate
    /// <summary>
    /// Allocates a Location to an Instruction (Automatically).
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="InstructionId"></param>
    /// <returns></returns>
    public bool AutoLocationAllocate(string connectionStringName, int InstructionId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Palletisation_Auto_Locations";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, InstructionId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "AutoLocationAllocate"

    #region CreateLines
    /// <summary>
    /// Inserts Instructions from ReceiptLine
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="numberOfLines"></param>
    /// <returns></returns>
    public bool CreateLines(string connectionStringName, int receiptLineId, int numberOfLines)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Palletisation_Create";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(createCommand, "ReceiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(createCommand, "NumberOfLines", DbType.Int32, numberOfLines);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateLines"

    #region CreateMovements
    /// <summary>
    /// Inserts Instructions for Movements and Replenishments
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionTypeCode"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <param name="pickLocationId"></param>
    /// <param name="numberOfLines"></param>
    /// <param name="quantity"></param>
    /// <param name="reasonId"></param>
    /// <returns></returns>
    public bool CreateMovements(string connectionStringName, string instructionTypeCode, int storageUnitBatchId, int pickLocationId, int numberOfLines, Decimal quantity, int reasonId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Housekeeping_Manual_Palletisation_Create";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(createCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);
        db.AddInParameter(createCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(createCommand, "PickLocationId", DbType.Int32, pickLocationId);
        db.AddInParameter(createCommand, "NumberOfLines", DbType.Int32, numberOfLines);
        db.AddInParameter(createCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(createCommand, "ReasonId", DbType.Int32, reasonId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateMovements"

    #region CreateMovementLocation
    /// <summary>
    /// Inserts Instructions for Movements and Replenishments
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionTypeCode"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <param name="pickLocationId"></param>
    /// <param name="numberOfLines"></param>
    /// <param name="quantity"></param>
    /// <param name="reasonId"></param>
    /// <returns></returns>
    public bool CreateMovementLocation(string connectionStringName, string instructionTypeCode, int storageUnitBatchId, 
                                        int pickLocationId, int storeLocationId, int reasonId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Housekeeping_Manual_Palletisation_Create_Location";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(createCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);
        db.AddInParameter(createCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(createCommand, "PickLocationId", DbType.Int32, pickLocationId);
        db.AddInParameter(createCommand, "StoreLocationId", DbType.Int32, storeLocationId);
        db.AddInParameter(createCommand, "ReasonId", DbType.Int32, reasonId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "CreateMovementLocation"

    #region UpdateMovements
    /// <summary>
    /// Updates Instructions for Movements and Replenishments
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="jobId"></param>
    /// <param name="priorityId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool UpdateMovements(string connectionStringName, int instructionId, int jobId, int priorityId, Decimal quantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Housekeeping_Instruction_Update";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(createCommand, "InstructionId", DbType.Int32, instructionId);
        db.AddInParameter(createCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(createCommand, "PriorityId", DbType.Int32, priorityId);
        db.AddInParameter(createCommand, "Quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateMovements"

    #region DeleteMovements
    /// <summary>
    /// Deletes Instructions for Movements and Replenishments
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="jobId"></param>
    /// <returns></returns>
    public bool DeleteMovements(string connectionStringName, int instructionId, int jobId, int operatorId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Housekeeping_Instruction_Delete";
        DbCommand createCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(createCommand, "InstructionId", DbType.Int32, instructionId);
        db.AddInParameter(createCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(createCommand, "OperatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute Query
                db.ExecuteNonQuery(createCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "DeleteMovements"

    #region GetMovements
    /// <summary>
    /// Gets Instructions linked to a Movement
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <param name="jobId"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <param name="pickLocationId"></param>
    /// <returns></returns>
    public DataSet GetMovements(string connectionStringName, int instructionId, int jobId, int storageUnitBatchId, int pickLocationId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Housekeeping_Manual_Palletisation_Create_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "PickLocationId", DbType.Int32, pickLocationId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetMovements"

    #region GetMovementByInstructionType
    /// <summary>
    /// Gets Movements By InstructionType's
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionTypeId"></param>
    /// <returns></returns>
    public DataSet GetMovementByInstructionType(string connectionStringName, int warehouseId, int instructionTypeId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Housekeeping_Instruction_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "InstructionTypeId", DbType.Int32, instructionTypeId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetMovementByInstructionType"

    #region GetManualInstruction
    /// <summary>
    /// Retreives a single instructions details.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="instructionId"></param>
    /// <returns></returns>
    public DataSet GetManualInstruction(string connectionStringName, int instructionId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_Manual_Instruction_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetManualInstruction"
}
