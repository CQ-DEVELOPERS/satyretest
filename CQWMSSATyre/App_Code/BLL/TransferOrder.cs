using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   16 Oct 2007
/// Summary description for Planning
/// </summary>
public class TransferOrder
{
    #region Constructor Logic
    public TransferOrder()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion "Constructor Logic"

    #region SearchOrders
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchOrders(string connectionStringName,
                                int warehouseId,
                                int locationId,
                                string productCode,
                                string product,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Transfer_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "LocationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "Product", DbType.String, product);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrders"

    #region UpdateOrder
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateOrder(String connectionStringName,
                            int outboundShipmentId,
                            int issueId,
                            String batch,
                            int manufacturingId,
                            int rawStagingId,
                            int productionStagingId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Transfer_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "manufacturingId", DbType.Int32, manufacturingId);
        db.AddInParameter(dbCommand, "rawStagingId", DbType.Int32, rawStagingId);
        db.AddInParameter(dbCommand, "productionStagingId", DbType.Int32, productionStagingId);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrder"

    #region GetOrderLines
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet GetOrderLines(string connectionStringName, int outboundShipmentId, int issueId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Transfer_Line_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetOrderLines"

    #region UpdateOrderLine
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool UpdateOrderLine(string connectionStringName,
                                int issueLineId,
                                int storageUnitBatchId,
                                int storageUnitId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Transfer_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "UpdateOrderLine"

    #region SearchPickingJobs
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchPickingJobs(string connectionStringName, int outboundShipmentId, int issueId, string instructionTypeCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Transfer_Job_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "OutboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "InstructionTypeCode", DbType.String, instructionTypeCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchPickingJobs"

    #region SearchLinesByJob
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    public DataSet SearchLinesByJob(string connectionStringName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Transfer_Lines_By_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchLinesByJob"

    #region SearchOrders
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="outboundShipmentId"></param>
    /// <param name="warehouseId"></param>
    /// <param name="locationId"></param>
    /// <param name="orderNumber"></param>
    /// <param name="batch"></param>
    /// <param name="fromDate"></param>
    /// <param name="toDate"></param>
    /// <returns></returns>
    public DataSet SearchOrders(string connectionStringName,
                                int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Production_Plan_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        
        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "SearchOrders"

    #region Combine
    /// <summary>
    /// Updates an Issue
    /// </summary>
    public bool Combine(String connectionStringName,
                            int issueId,
                            int firstIssueId)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Transfer_Order_Combine";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IssueId", DbType.Int32, issueId);
        db.AddInParameter(dbCommand, "firstIssueId", DbType.Int32, firstIssueId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Combine"
}
