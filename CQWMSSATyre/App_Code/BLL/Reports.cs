using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;

/// <summary>
/// Summary description for Reports
/// </summary>
public class Reports
{
	public Reports()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    #region ReceivingCheckSheet
    public DataSet ReceivingCheckSheet(string connectionStringName, int inboundShipmentId, int receiptId)
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Receiving_Check_Sheet";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "inboundShipmentId", DbType.Int32, inboundShipmentId);
            db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

            dataSet = db.ExecuteDataSet(dbCommand);
        }
        catch { }

        return dataSet;
    }
    #endregion ReceivingCheckSheet

    #region SearchDespatchDocument
    public DataSet SearchDespatchDocument(string connectionStringName,
                                int outboundShipmentId,
                                int warehouseId,
                                int outboundDocumentTypeId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Report_Despatch_Document_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchDespatchDocument

    #region SearchDespatchAdvice
    public DataSet SearchDespatchAdvice(string connectionStringName,
                                int outboundShipmentId,
                                int warehouseId,
                                int outboundDocumentTypeId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Report_Despatch_Advice_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchDespatchAdvice

    #region SearchDespatchAdviceShipment
    public DataSet SearchDespatchAdviceShipment(string connectionStringName,
                                int outboundShipmentId,
                                int warehouseId,
                                int outboundDocumentTypeId,                                
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Report_Despatch_Advice_Shipment_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchDespatchAdviceShipment

    #region SearchInvoice
    public DataSet SearchInvoice(string connectionStringName,
                                int outboundShipmentId,
                                int warehouseId,
                                int outboundDocumentTypeId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Report_Invoice_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchInvoice

    #region SearchManufacturingOrder
    public DataSet SearchManufacturingOrder(string connectionStringName,
                                int outboundShipmentId,
                                int warehouseId,
                                int outboundDocumentTypeId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate,
                                string productCode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Manufacturing_Order_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchManufacturingOrder

    #region SearchWave
    public DataSet SearchWave(string connectionStringName,
                                int outboundShipmentId,
                                int warehouseId,
                                int outboundDocumentTypeId,
                                int waveId,
                                string orderNumber,
                                string externalCompany,
                                string externalCompanyCode,
                                DateTime fromDate,
                                DateTime toDate,
                                int principalId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Report_Wave_Search";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "outboundShipmentId", DbType.Int32, outboundShipmentId);
        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "waveId", DbType.Int32, waveId);
        db.AddInParameter(dbCommand, "OutboundDocumentTypeId", DbType.Int32, outboundDocumentTypeId);
        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "ExternalCompany", DbType.String, externalCompany);
        db.AddInParameter(dbCommand, "ExternalCompanyCode", DbType.String, externalCompanyCode);
        db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "principalId", DbType.Int32, principalId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchWave

    #region DayendOutbound
    public DataSet DayendOutbound(string connectionStringName)
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_DayEnd_SO_Extract";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            dataSet = db.ExecuteDataSet(dbCommand);
        }
        catch { }

        return dataSet;
    }
    #endregion DayendOutbound

    #region DayendHousekeeping
    public DataSet DayendHousekeeping(string connectionStringName)
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_DayEnd_Movement_Extract";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            dataSet = db.ExecuteDataSet(dbCommand);
        }
        catch { }

        return dataSet;
    }
    #endregion DayendHousekeeping

    #region DayendInbound
    public DataSet DayendInbound(string connectionStringName)
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_DayEnd_PO_Extract";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

              dataSet = db.ExecuteDataSet(dbCommand);
        }
        catch { }

        return dataSet;
    }
    #endregion DayendInbound

    #region ClearDayend
    public DataSet ClearDayend(string connectionStringName)
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_DayEnd_Clear";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            dataSet = db.ExecuteDataSet(dbCommand);
        }
        catch { }

        return dataSet;
    }
    #endregion ClearDayend
}
