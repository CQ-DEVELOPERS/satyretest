using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   9 July 2007
/// Summary description for ManualPalletiseOutbound
/// </summary>
public class ManualPalletiseOutbound
{
    #region Constructor Logic
    public bool allocated = false;

    public ManualPalletiseOutbound()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetIssueLines
    /// <summary>
    /// Retrieves Outbound Orders
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueLineId"></param>
    /// <returns></returns>
    public DataSet GetIssueLines(string connectionStringName, int issueLineId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Manual_Palletisation_Select";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetIssueLines"

    #region CreateLines
    /// <summary>
    /// Creates Instruction Records
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueLineId"></param>
    /// <param name="numberOfLines"></param>
    /// <returns></returns>
    public bool CreateLines(string connectionStringName, int issueLineId, int numberOfLines)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Manual_Palletisation_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);
        db.AddInParameter(dbCommand, "NumberOfLines", DbType.Int32, numberOfLines);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();
            
            return result;
        }
    }
    #endregion "CreateLines"

    #region GetAreas
    /// <summary>
    /// Get a list of available areas for StorageUnit linked to Instruction
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueLineId"></param>
    /// <returns></returns>
    public DataSet GetAreas(string connectionStringName, int issueLineId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Area_Search_IssueLineId";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);

        // DataSet that will hold the returned results		
        DataSet resultDataSet = null;

        resultDataSet = db.ExecuteDataSet(dbCommand);

        return resultDataSet;
    }
    #endregion "GetAreas"

    #region GetLocations
    /// <summary>
    /// Get locations and products linked to the selected Area
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueLineId"></param>
    /// <param name="areaId"></param>
    /// <returns></returns>
    public DataSet GetLocations(string connectionStringName, int issueLineId, int areaId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Location_Search_IssueLine";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);
        db.AddInParameter(dbCommand, "AreaId", DbType.Int32, areaId);

        // DataSet that will hold the returned results		
        DataSet resultDataSet = null;

        resultDataSet = db.ExecuteDataSet(dbCommand);

        return resultDataSet;
    }
    #endregion "GetLocations"

    #region ManualLocationAllocate
    public bool ManualLocationAllocate(string connectionStringName, int issueLineId, Decimal quantity, int pickLocationId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Manual_Palletisation_IssueLine";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IssueLineId", DbType.Int32, issueLineId);
        db.AddInParameter(dbCommand, "Quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "PickLocationId", DbType.Int32, pickLocationId);
        
        //Add out parameter to validate if IssueLine has been fully allocated
        db.AddParameter(dbCommand, "Allocated", DbType.Boolean, ParameterDirection.Output, "Allocated", DataRowVersion.Current, allocated);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;

                // Get output parameter
                allocated = bool.Parse(db.GetParameterValue(dbCommand, "Allocated").ToString());
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "ManualLocationAllocate"
}
