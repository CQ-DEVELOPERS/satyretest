using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Author: Grant Schultz
/// Date:   20 July 2007
/// Summary description for OutboundShipment
/// </summary>
public class OrderQuery
{
    #region Constructor Logic

    public OrderQuery()
    {
    }
    #endregion "Constructor Logic"

    #region SearchOrder
    /// <summary>
    /// Container Search
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="locationId"></param>
    /// <returns></returns>
    public DataSet SearchOrder(string connectionStringName,
                                Int32 warehouseId,
                                Int32 operatorId,
                                String orderNumber,
                                String invoiceNumber,
                                DateTime fromDate,
                                DateTime toDate,
                                String status)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Order_Query_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);
        db.AddInParameter(dbCommand, "invoiceNumber", DbType.String, invoiceNumber);
        db.AddInParameter(dbCommand, "fromDate", DbType.DateTime, fromDate);
        db.AddInParameter(dbCommand, "toDate", DbType.DateTime, toDate);
        db.AddInParameter(dbCommand, "Status", DbType.String, status);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchOrder
}
