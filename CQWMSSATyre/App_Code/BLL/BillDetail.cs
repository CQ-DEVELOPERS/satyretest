﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillDetail
/// </summary>
public class BillDetail : BaseBLL
{
  public BillDetail()
  {
  }

  /// <summary>
  /// Gets the by bill id.
  /// </summary>
  /// <param name="billId">The bill id.</param>
  /// <returns></returns>
  public DataSet GetByBillId( int billId )
  {
    var data = ExecuteDataSet( "p_BillDetail_Search", new ParamItem( "BillID", billId ) );
    return data;
  }


  /// <summary>
  /// Updates the specified id.
  /// </summary>
  /// <param name="id">The id.</param>
  /// <param name="billDetailId">The bill detail id.</param>
  /// <param name="name">The name.</param>
  /// <param name="areaId">The area id.</param>
  /// <param name="weightInKg">The weight in kg.</param>
  /// <param name="numberOfUnits">The number of units.</param>
  /// <param name="cubeInMeters">The cube in meters.</param>
  /// <param name="numberOfPallets">The number of pallets.</param>
  /// <param name="numberOfCartons">The number of cartons.</param>
  /// <param name="amount">The amount.</param>
  /// <param name="billingEventId">The billing event id.</param>
  /// <param name="productId">The product id.</param>
  /// <param name="billId">The bill id.</param>
  /// <returns></returns>
  public int Update( int id, string billDetailId, string name, int areaId, double weightInKg,
                    double numberOfUnits, double cubeInMeters, double numberOfPallets, double numberOfCartons,
                    double amount, int billingEventId, int? productId, int billId )
  {

    // Create a list of Parameters
    List<ParamItem> parameters = new List<ParamItem>();
    parameters.Add( new ParamItem( "Id", System.Data.DbType.Int32, id, System.Data.ParameterDirection.InputOutput ) );
    parameters.Add( new ParamItem( "BillDetailId", billDetailId ) );
    parameters.Add( new ParamItem( "Name", name ) );
    parameters.Add( new ParamItem( "AreaId", areaId ) );
    parameters.Add( new ParamItem( "WeightInKg", weightInKg ) );
    parameters.Add( new ParamItem( "NumberOfUnits", numberOfUnits ) );
    parameters.Add( new ParamItem( "CubeInMeters", cubeInMeters ) );
    parameters.Add( new ParamItem( "NumberOfPallets", numberOfPallets ) );
    parameters.Add( new ParamItem( "NumberOfCartons", numberOfCartons ) );
    parameters.Add( new ParamItem( "Amount", amount ) );
    parameters.Add( new ParamItem( "BillingEventId", billingEventId ) );
    parameters.Add( new ParamItem( "ProductId", System.Data.DbType.Int32, productId ) );
    parameters.Add( new ParamItem( "BillId", billId ) );

    // Execute the Query (Stored Proc) against the database
    var data = ExecuteNonQuery( "p_BillDetail_Insert", parameters.ToArray() );

    // Get the Id parameters value
    var idValue = (int)parameters[0].Value;

    // Return the New/Updated Id value
    return idValue;
  }

  public bool Delete( int id )
  {
    bool result = false;

    try
    {
      var data = ExecuteNonQuery( "p_BillDetail_Insert", new ParamItem( "Id", id ) );
      result = true;
    }
    catch ( Exception ex )
    {
      throw;
    }

    return result;
  }

}