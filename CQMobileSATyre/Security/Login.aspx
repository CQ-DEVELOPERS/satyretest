<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Security_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta name="MobileOptimized" content="width" />
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
        <asp:Image ID="ImageLogo" runat="server" ImageUrl="~/Security/CQ Logo.png" />
        <asp:Login ID="Login1" runat="server" TabIndex="1" DisplayRememberMe="False" PasswordRecoveryUrl="~/Security/ForgotPassword.aspx" FailureText="Your login attempt was not <br />successful. Please try again." OnLoggedIn="Login1_OnLoggedIn">
        </asp:Login>
        <asp:Label ID="LabelUserNotSetup" runat="server" SkinID="Error" Text="<%$ Resources:Default, LabelUserNotSetup %>" Visible="false"></asp:Label>
    </div>
    </form>
</body>
</html>
