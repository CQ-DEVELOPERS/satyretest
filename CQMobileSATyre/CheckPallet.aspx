<%@ Page Language="C#"
    MasterPageFile="~/Master.master"
    AutoEventWireup="true" 
    CodeFile="CheckPallet.aspx.cs"
    Inherits="Screens_CheckPallet" 
    Title="<%$ Resources:Default, CheckPalletTitle %>"
    StylesheetTheme="Default"
    Theme="Default"
%>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="Form1" runat="server">
            <table>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="TextView" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelPalletId" runat="server" Font-Bold="True">PalletId:</asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBoxPalletId" runat="server" Width="115px" Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelBarcode" runat="server" Font-Bold="True">Product barcode:</asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBoxBarcode" runat="server" Width="115px" TabIndex="1"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelBatch" runat="server" Width="52px" Font-Bold="True">Batch:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxBatch" runat="server" Width="115px" TabIndex="2"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelQuantity" runat="server" Font-Bold="True">Quantity:</asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBoxQuantity" runat="server" Width="115px" TabIndex="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:LinkButton ID="CommandNext" runat="server" OnClick="CommandNext_Click" Text="<%$ Resources:Default,Next %>"
                            TabIndex="4" Font-Bold="True" ForeColor="#3D4782"></asp:LinkButton>
                        <asp:LinkButton ID="CommandCancel" runat="server" OnClick="CommandCancel_Click" Text="<%$ Resources:Default,Cancel %>"
                            TabIndex="5" Font-Bold="True" ForeColor="#3D4782"></asp:LinkButton></td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="Form2" runat="server">
            <asp:Label ID="LabelErrorMsg" runat="server" ForeColor="#ff0000"></asp:Label>
            <asp:LinkButton ID="CommandTryAgain" runat="server" OnClick="CommandTryAgain_Click"
                Text="<%$ Resources:Default,TryAgain %>" Font-Bold="True" ForeColor="#3D4782"></asp:LinkButton>
        </asp:View>
    </asp:MultiView>
</asp:Content>
