using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

/// <summary>
/// Summary description for CQExceptionLayer
/// </summary>
/// 

public class MCQExceptionLayer
{
 
    #region private variables
    //private StringBuilder strBuilder = new StringBuilder();
    private string result = "Exception";
    private int countErrors = -1;

    #endregion private variables

    #region Construct
    public MCQExceptionLayer()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #endregion Construct

    #region Outbound
    
    #region Generic Outbound Error Handling
    /// <summary>
    /// 
    /// </summary>
    /// <param name="priorityLevel">1</param>
    /// <param name="errpage">Default.aspx</param>
    /// <param name="errMethod">Page_Load</param>
    /// <param name="errMsg">Session Variables</param>
    /// <returns></returns>
    public string GenericOutboundErrorHandling(int intId, string ex, string someMessage)
      
    {

        return result;

    }

    public string GenericMobileErrorHandling( int priorityLevel,
                                                string errpage,
                                                string errMethod,
                                                string errMsg)
    {

        return result;

    }

  
  

    #endregion Generic Outbound Error Handling  

    #endregion Outbound

    #region Inbound
    
    #region SendError

    public string GenericMobileInErrorHandling(int priorityLevel,
                                               string errpage,
                                               string errMethod,
                                               string errMsg)
    {
        return result;
    
    }


    public string SendInboundErrorNow(int intId, string ex, string something)
    {
        return result;
    }

    #endregion SendError

    #endregion Inbound

    #region SendError
    /// <summary>
    /// 
    /// </summary>
    /// <param name="ex">ex</param>
    /// <param name="inOrout">Inbound or Outbound</param>
    /// <param name="errMethod">SendInboundErrorNow</param>
    /// <returns></returns>
    private string SendErrorNow(string ex, string inOrout, string errMethod)
    {

        return result;
    }
    #endregion "Send Error Now"
}
