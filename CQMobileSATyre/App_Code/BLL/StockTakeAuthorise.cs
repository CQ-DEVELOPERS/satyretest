using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for Area
/// </summary>
public class StockTakeAuthorise
{
    #region Constructor Logic
    public StockTakeAuthorise()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region ViewAuthoriseDetails
    public DataSet ViewAuthoriseDetails(string connectionStringName, int warehouseId, int areaId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Authorise";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "areaId", DbType.Int32, areaId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion ViewAuthoriseDetails

    #region Authorise
    /// <summary>
    /// Creates a Stock Take on a Location
    /// </summary>
    public bool Authorise(string connectionStringName, int instructionId, int operatorId, string statusCode)
    {
        bool result = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Update";

        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "statusCode", DbType.String, statusCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Authorise

    #region GetAuthoriseAreas
    public DataSet GetAuthoriseAreas(string connectionStringName, int warehouseId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Housekeeping_Stock_Take_Area_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAuthoriseAreas
}
