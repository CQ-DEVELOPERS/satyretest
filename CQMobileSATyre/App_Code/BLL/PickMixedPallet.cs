using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for PickMixedPallet
/// </summary>
public class PickMixedPallet
{
	public PickMixedPallet()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    #region 
    ///           0 -- Get Next Tranaction
    ///           1 -- Cancel Pick
    ///           2 -- Pick Transaction
    ///           3 -- Request Replenishment
    ///           4 -- Store Transaction
    ///           5 -- Loation Error
    ///           6 -- Confirm Quantity
    ///           7 -- Tare Pallet
    ///           8 -- Confirm Product
    #endregion

    #region GetPickInformation
    public DataSet GetPickInformation(int type, int operatorId, int palletId, int newPalletId, string pickLocation,
                                      Decimal confirmedQuantity, string storeLocation, int instructionId) 
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = null;
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("Connection String");

            string sqlCommand = "p_Mobile_Movement";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "type", DbType.Int32, type);
            db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
            db.AddInParameter(dbCommand, "newPalletId", DbType.Int32, newPalletId);
            db.AddInParameter(dbCommand, "pickLocation", DbType.String, pickLocation);
            db.AddInParameter(dbCommand, "confirmedQuantity", DbType.Decimal, confirmedQuantity);
            db.AddInParameter(dbCommand, "storeLocation", DbType.String, storeLocation);
            db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);

            dataSet = db.ExecuteDataSet(dbCommand);
           
        }
        catch
        { }

        return dataSet;
    }
    #endregion GetPickInformation

    #region SetWeightInfo
    //Grant -- if I send through a null for weight could you return the default weight
    public DataSet SetWeightInfo(int type, int orderPalletID, decimal dcWeight)
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = new DataSet();

        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("Connection String");

            string sqlCommand = "#";

            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "Weight", DbType.Decimal, dcWeight);


            dataSet = db.ExecuteDataSet(dbCommand);

            db = null;
            dbCommand.Dispose();
        }
        catch (DbException dbex)
        {
            string xx = dbex.Message;

        }


        return dataSet;
    }
    #endregion SetWeightInfo    
    
    #region UpdatePickMix
    /*public DataSet UpdatePickMixPallet(type, operatorId, palletID, newPalletId, pickLocation, confirmedQuantity, storeLocation, 0)
              
    {
        // DataSet that will hold the returned results		
        DataSet dataSet = new DataSet();
        try
        {
            // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("Connection String");

            string sqlCommand = "p_Mobile_Auto_Move";

            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "orderId", DbType.Decimal, dcWeight);


            dataSet = db.ExecuteDataSet(dbCommand);

            db = null;
            dbCommand.Dispose();
        }
        catch (DbException dbex)
        {
            string xx = dbex.Message;

        }


        return dataSet;
    }
   
    */

    #endregion UpdatePickMix

    #region GetPickInformation
    /// <summary>
    /// </summary>
    /// <param name="type">@Type, int</param>
    /// <param name="operatorId">@OperatorId,  int</param>
    /// <param name="referenceNumber">@ReferenceNumber,  varchar(30)</param>
    /// <param name="instructionId">@InstructionId,   int output</param>
    /// <param name="barCode">@Barcode,      varchar(30)</param>
    /// <param name="confirmedQuantity">@ConfirmedQuantity,   int</param>
    /// <param name="tareWeight">@TareWeight,  int</param>
    /// <returns></returns>
    
    public DataSet PickMixed(   int type, 
                                int operatorId, 
                                string referenceNumber, 
                                int instructionId,                               
                                string barCode,
                                Decimal confirmedQuantity, 
                                string tareWeight)
    {
         
        // DataSet that will hold the returned results		
        DataSet dataSet = null;
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("Connection String");

            string sqlCommand = "p_Mobile_Pick_Mixed";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "Type", DbType.Int32, type);
            db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "ReferenceNumber", DbType.String, referenceNumber);
            db.AddInParameter(dbCommand, "InstructionId", DbType.Int32, instructionId);
            db.AddInParameter(dbCommand, "Barcode", DbType.String, barCode);
            db.AddInParameter(dbCommand, "ConfirmedQuantity", DbType.Decimal, confirmedQuantity);
            db.AddInParameter(dbCommand, "TareWeight", DbType.String, tareWeight);
         
            dataSet = db.ExecuteDataSet(dbCommand);
           
        }
        catch
        { 
        
        }

        return dataSet;
    }
    #endregion GetPickInformation
}
