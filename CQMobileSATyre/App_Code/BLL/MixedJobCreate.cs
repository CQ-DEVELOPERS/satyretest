using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// PutawayFull allows an operator to scan a pallet and put it away
/// </summary>
public class MixedJobCreate
{
    public MixedJobCreate()
    {
    }

    #region CheckPallet
    /// <summary>
    /// Check if a Pallet is correct against user input
    /// </summary>
    /// <param name="type"></param>
    /// <param name="operatorId"></param>
    /// <param name="jobId"></param>
    /// <param name="barcode"></param>
    /// <param name="storeLocation"></param>
    /// <param name="confirmedQuantity"></param>
    /// <returns></returns>
    public DataSet Verify(int type, int operatorId, int palletId, string barcode, string batch, Decimal quantity)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("Connection String");

        string sqlCommand = "p_Mobile_Mixed_Job_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "type", DbType.Int32, type);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion CheckPallet
}
