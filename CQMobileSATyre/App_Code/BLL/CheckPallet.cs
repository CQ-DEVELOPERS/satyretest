using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// PutawayFull allows an operator to scan a pallet and put it away
/// </summary>
public class CheckPallet
{
    public CheckPallet()
    {
    }

    #region CheckPallet
    /// <summary>
    /// Check if a Pallet is correct against user input
    /// </summary>
    /// <param name="type"></param>
    /// <param name="operatorId"></param>
    /// <param name="jobId"></param>
    /// <param name="barcode"></param>
    /// <param name="storeLocation"></param>
    /// <param name="confirmedQuantity"></param>
    /// <returns></returns>
    public DataSet Verify(int type, int operatorId, int palletId, string barcode, string batch, Decimal quantity)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("Connection String");

        string sqlCommand = "p_Mobile_Check_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "type", DbType.Int32, type);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion CheckPallet

    #region AutoMovePallet
    /// <summary>
    /// Check if a Pallet is correct against user input
    /// </summary>
    /// <param name="type"></param>
    /// <param name="operatorId"></param>
    /// <param name="jobId"></param>
    /// <param name="barcode"></param>
    /// <param name="storeLocation"></param>
    /// <param name="confirmedQuantity"></param>
    /// <returns></returns>
    ///public DataSet AutoMovePallet(int type, int operatorId, int palletId, string barcode, string batch, int quantity)
    public DataSet AutoMovePallet(int type)
    {
            // DataSet that will hold the returned results		
            DataSet dataSet = new DataSet();
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("Connection String");

            string sqlCommand = "p_Mobile_Movment";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "orderId", DbType.Int32, type);


            dataSet = db.ExecuteDataSet(dbCommand);
        }
        catch(DbException dbex)
        {
            string xx = dbex.Message;
        
        }
        return dataSet;
    }
    #endregion CheckPallet

    #region GetCheckList
    public DataSet GetCheckList(string connectionStringName, int warehouseId, int operatorId, string referenceNumber)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "p_Mobile_Check_List_Get";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetCheckList

    #region UpdateCheckList
    public bool UpdateCheckList(string connectionStringName, int operatorId, int jobId, int storageUnitBatchId, Decimal quantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "p_Mobile_Check_List_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion UpdateCheckList

    #region StatusRollup
    public bool StatusRollup(string connectionStringName, int operatorId, int jobId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = "p_Mobile_Check_List_Rollup";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
                result = false;
            }

            connection.Close();

            return result;
        }
    }
    #endregion StatusRollup
}
