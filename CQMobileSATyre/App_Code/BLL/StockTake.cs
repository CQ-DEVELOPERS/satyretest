using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Net;
using System.Net.Sockets;
using System.IO;

/// <summary>
/// Summary description for Transact
/// </summary>
public class StockTake
{
    #region GetAreas
    public DataSet GetAreas(string connectionStringName, int operatorId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Stock_Take_Area";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetAreas

    #region GetJobs
    public DataSet GetJobs(string connectionStringName, int operatorId, int areaId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Stock_Take_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "areaId", DbType.Int32, areaId);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetJobs

    #region GetNextLocation
    public DataSet GetNextLocation(string connectionStringName, int operatorId, int jobId)
    {
        try
        {
            int type = 0;
            string paramValues = String.Format("{0},{1},{2}", type, operatorId, jobId);
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), "", "", operatorId, paramValues);

            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Mobile_Stock_Take_Count";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            db.AddInParameter(dbCommand, "type", DbType.Int32, type);
            db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);

            // DataSet that will hold the returned results		
            DataSet dataSet = null;

            dataSet = db.ExecuteDataSet(dbCommand);

            StringWriter stringWriter = new StringWriter();
            dataSet.WriteXml(stringWriter);
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), "", stringWriter.ToString(), operatorId, "");

            return dataSet;
        }
        catch (Exception e)
        {
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), "", "", operatorId, String.Format("Procedure Exception: {0}", e.Message));
            throw;
        }
    }
    #endregion GetNextLocation

    #region ConfirmCount
    public int ConfirmCount(string connectionStringName, int operatorId, int jobId, string barcode,
                                string batch, int locationId, int storageUnitBatchId, Decimal quantity)
    {
        try
        {
            int type = 1;
            string paramValues = String.Format("{0},{1},{2},{3},{4},{5},{6},{7}", type, operatorId, jobId, barcode, batch, locationId, storageUnitBatchId, quantity);
            string storageUnitBatchLocationXmlOld = GetStorageUnitBatchLocationValues(connectionStringName, storageUnitBatchId, locationId);
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), storageUnitBatchLocationXmlOld, "", operatorId, paramValues);

            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Mobile_Stock_Take_Count";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            db.AddInParameter(dbCommand, "type", DbType.Int32, type);
            db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
            db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
            db.AddInParameter(dbCommand, "batch", DbType.String, batch);
            db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
            db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
            db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

            int result = (int)db.ExecuteScalar(dbCommand);
            string storageUnitBatchLocationXmlNew = GetStorageUnitBatchLocationValues(connectionStringName, storageUnitBatchId, locationId);
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), storageUnitBatchLocationXmlOld, storageUnitBatchLocationXmlNew, operatorId, String.Format("Procedure Result: {0}", result));


            return result;
        }
        catch(Exception ex)
        {
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), "", "", operatorId, String.Format("Procedure Exception: {0}", ex.Message));
            return -2;
        }
    }

    public int ConfirmCount(string connectionStringName, int operatorId, int jobId, string barcode,
                           string batch, int locationId, int storageUnitBatchId, Decimal quantity, decimal weight)
    {
        try
        {
            int type = 1;
            string paramValues = String.Format("{0},{1},{2},{3},{4},{5},{6},{7}", type, operatorId, jobId, barcode, batch, locationId, storageUnitBatchId, quantity);
            string storageUnitBatchLocationXmlOld = GetStorageUnitBatchLocationValues(connectionStringName, storageUnitBatchId, locationId);
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), storageUnitBatchLocationXmlOld, "", operatorId, paramValues);

            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Mobile_Stock_Take_Count";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            db.AddInParameter(dbCommand, "type", DbType.Int32, type);
            db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
            db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
            db.AddInParameter(dbCommand, "batch", DbType.String, batch);
            db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
            db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
            db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);
            db.AddInParameter(dbCommand, "weight", DbType.Decimal, weight);

            int result = (int)db.ExecuteScalar(dbCommand);
            string storageUnitBatchLocationXmlNew = GetStorageUnitBatchLocationValues(connectionStringName, storageUnitBatchId, locationId);
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), storageUnitBatchLocationXmlOld, storageUnitBatchLocationXmlNew, operatorId, String.Format("Procedure Result: {0}", result));

            return result;
        }
        catch(Exception ex)
        {
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), "", "", operatorId, String.Format("Procedure Exception: {0}", ex.Message));
            return -2;
        }
    }
    #endregion ConfirmCount

    public static string GetLocalIPAddress()
    {
        try
        {

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
        catch
        {
            throw;
        }
    }

    #region CheckCount
    public int CheckCount(string connectionStringName, int operatorId, int jobId, string barcode,
                                string batch, int locationId, int storageUnitBatchId, Decimal quantity)
    {
        try
        {
            int type = 3;
            
            string paramValues = String.Format("{0},{1},{2},{3},{4},{5},{6},{7}", type, operatorId, jobId, barcode, batch, locationId, storageUnitBatchId, quantity);
            string storageUnitBatchLocationXmlOld = GetStorageUnitBatchLocationValues(connectionStringName, storageUnitBatchId, locationId);
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), storageUnitBatchLocationXmlOld, "",  operatorId, paramValues);
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Mobile_Stock_Take_Count";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            db.AddInParameter(dbCommand, "type", DbType.Int32, type);
            db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
            db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
            db.AddInParameter(dbCommand, "batch", DbType.String, batch);
            db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
            db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
            db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

            int result = (int)db.ExecuteScalar(dbCommand);

            string storageUnitBatchLocationXmlNew = GetStorageUnitBatchLocationValues(connectionStringName, storageUnitBatchId, locationId);
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), storageUnitBatchLocationXmlOld, storageUnitBatchLocationXmlNew, operatorId, String.Format("Procedure Result: {0}", result));

            return result;
        }
        catch (Exception ex)
        {
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), "", "", operatorId, String.Format("Procedure Exception: {0}", ex.Message));
            return -2;
        }
    }
    #endregion CheckCount

    #region SearchProduct
    public DataSet SearchProduct(string connectionStringName, string product, string productCode, string batch)
    {
        int type = 1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Product_Search";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "product", DbType.String, product);
        db.AddInParameter(dbCommand, "productCode", DbType.String, productCode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchProduct

    #region Finished
    public int Finished(string connectionStringName, int operatorId, int jobId, int locationId)
    {
        try
        {
            int type = 2;
            string paramValues = String.Format("{0},{1},{2},{3}", type, operatorId, jobId, locationId);
            string storageUnitBatchLocationXmlOld = GetStorageUnitBatchLocationValues(connectionStringName, 0, locationId);
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), storageUnitBatchLocationXmlOld, "", operatorId, paramValues);

            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

            string sqlCommand = connectionStringName + ".dbo.p_Mobile_Stock_Take_Count";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            db.AddInParameter(dbCommand, "type", DbType.Int32, type);
            db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
            db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

            int result = (int)db.ExecuteScalar(dbCommand);
            string storageUnitBatchLocationXmlNew = GetStorageUnitBatchLocationValues(connectionStringName, 0, locationId);
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), storageUnitBatchLocationXmlOld, storageUnitBatchLocationXmlNew, operatorId, String.Format("Procedure Result: {0}", result));

            return result;
        }
        catch (Exception ex)
        {
            MobileLogStockTakeAudit(connectionStringName, "p_Mobile_Stock_Take_Count", GetLocalIPAddress(), "", "", operatorId, String.Format("Procedure Exception: {0}", ex.Message));
            return -1;
        }
    }
    #endregion Finished

    #region ValidLocation
    public bool ValidLocation(string connectionStringName, string barcode)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Check_Valid_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public bool ValidLocation(string connectionStringName, string barcode, int locationId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Check_Valid_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (bool)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ValidLocation

    #region EmptyLocationSelect
    public DataSet EmptyLocationSelect(string connectionStringName, int warehouseId, string location)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Stock_Take_Empty";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "location", DbType.String, location);

        // DataSet that will hold the returned results		
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion EmptyLocationSelect

    #region EmptyLocationUpdate
    public int EmptyLocationUpdate(string connectionStringName, int warehouseId, int operatorId, int locationId)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Stock_Take_Empty_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public int EmptyLocationUpdate(string connectionStringName, int warehouseId, int operatorId, int locationId, bool createOnly)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Stock_Take_Empty_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "locationId", DbType.Int32, locationId);
        db.AddInParameter(dbCommand, "createOnly", DbType.Boolean, createOnly);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteScalar(dbCommand);

                result = 0;
            }
            catch {}

            connection.Close();

            return result;
        }
    }
    #endregion EmptyLocationUpdate

    public void MobileLogStockTakeAudit(string connectionStringName, string procName, string ipAdd, string old, string newValue, int operatorId, string paramsValue)
    {
        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
            string sqlCommand = String.Format("insert into {0}..MobileLogStockTakeAudit (ProcName, IPAdd, OperatorId, Old, New, Params) values(@ProcName, @IPAdd, @OperatorId, @Old, @New, @Params)", connectionStringName);
            DbCommand dbCommand = db.GetSqlStringCommand(sqlCommand);

            db.AddInParameter(dbCommand, "@ProcName", DbType.String, procName);
            db.AddInParameter(dbCommand, "@OperatorId", DbType.Int32, operatorId);
            db.AddInParameter(dbCommand, "@IPAdd", DbType.String, ipAdd);
            db.AddInParameter(dbCommand, "@Old", DbType.String, old);
            db.AddInParameter(dbCommand, "@New", DbType.String, newValue);
            db.AddInParameter(dbCommand, "@Params", DbType.String, paramsValue);

            db.ExecuteNonQuery(dbCommand);
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public string GetStorageUnitBatchLocationValues(string connectionStringName, int storageUnitBatchId, int locationId)
    {
        try
        {
            string storageUnitBatchParam = ", StorageUnitBatchId = @StorageUnitBatchId";
            if (storageUnitBatchId <= 0)
                storageUnitBatchParam = "";

            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
            string sqlCommand = String.Format("select * FROM {0}..StorageUnitBatchLocation WHERE LocationId = @LocationId {1}", connectionStringName, storageUnitBatchParam);
            DbCommand dbCommand = db.GetSqlStringCommand(sqlCommand);

            if(storageUnitBatchId > 0)
                db.AddInParameter(dbCommand, "@StorageUnitBatchId", DbType.String, storageUnitBatchId);

            db.AddInParameter(dbCommand, "@LocationId", DbType.Int32, locationId);
            DataSet set = db.ExecuteDataSet(dbCommand);

            StringWriter stringWriter = new StringWriter();
            set.WriteXml(stringWriter);

            return stringWriter.ToString();
        }
        catch (Exception ex)
        {
            throw;
        }
    }
}