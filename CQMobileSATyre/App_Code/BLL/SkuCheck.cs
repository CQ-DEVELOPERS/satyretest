using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

/// <summary>
/// Summary description for Transact
/// </summary>
public class SkuCheck
{
    public string skuCode = "1234567890";
    public Decimal quantity = -1;

    #region Constructor Logic
    public SkuCheck()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetSkuCheck
    public int GetSkuCheck(string connectionStringName, int warehouseId, string barcode)
    {
        int result = 0;
        
        // Create the Database object
            Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_SKU_Get";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // Set Pallets as an output parameter
        //db.AddParameter(dbCommand, "skuCode", DbType.String, ParameterDirection.Output, "skuCode", DataRowVersion.Current, "          ");
        db.AddOutParameter(dbCommand, "skuCode", DbType.String, 20);
        // Set Total as an output parameter
        db.AddParameter(dbCommand, "quantity", DbType.Decimal, ParameterDirection.Output, "quantity", DataRowVersion.Current, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);
                
                // Get SkuCode output parameter
                skuCode = (string)db.GetParameterValue(dbCommand, "skuCode");

                // Get Quantity output parameter
                quantity = (Decimal)db.GetParameterValue(dbCommand, "quantity");
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetSkuCheck
    #region CheckSkuQuantity

    public int CheckSkuQuantity(string connectionStringName, int warehouseId, string barcode, string SkuCode, Decimal ConfirmedQuantity, int OperatorId)
    {
        int result = 0;
        
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_SKU_Check";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "SKUCode", DbType.String, SkuCode);
        db.AddInParameter(dbCommand, "ConfirmedQuantity", DbType.Decimal, ConfirmedQuantity);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, OperatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);
                
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CheckSkuQuantity


}