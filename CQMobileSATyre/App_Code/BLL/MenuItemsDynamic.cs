using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Joe Black
/// Date:   23 June 2007
/// Summary description for Menu
/// </summary>
public class MenuItemsDynamic
{

    #region Constructor Logic
    public MenuItemsDynamic()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    /// <summary>
    /// Returns MenuItems By OperatorGroup
    /// </summary>
    /// <param name="menuId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public DataSet GetMenuItems(string connectionStringName, int menuId, int operatorId, string siteType)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_Select_ByMenu";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Retrieve MenuItems from the specified OperatorGroup.
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, menuId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "SiteType", DbType.String, siteType);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        // Setup DataSet for Parent / Child Relationship
        dataSet.DataSetName = "MenuItems";
        dataSet.Tables[0].TableName = "MenuItem";

        DataRelation relation = new DataRelation("ParentChild",
                                                    dataSet.Tables["MenuItem"].Columns["MenuItemId"],
                                                    dataSet.Tables["MenuItem"].Columns["ParentMenuItemId"],
                                                    true);

        relation.Nested = true;

        dataSet.Relations.Add(relation);

        return dataSet;
    }

    /// <summary>
    /// Returns MenuItems By OperatorGroup and Menu
    /// </summary>
    /// <param name="menuId"></param>
    /// <param name="operatorId"></param>
    /// <returns></returns>
    public DataSet SearchMenuItemAccess(string connectionStringName, int operatorGroupId, int menuId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_OperatorGroup_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
                
        // Retrieve MenuItems from the specified OperatorGroup and Menu id.
        db.AddInParameter(dbCommand, "operatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "MenuId", DbType.Int32, menuId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }

    /// <summary>
    /// Updates the Access
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="menuItemId"></param>
    /// <param name="access"></param>
    /// <returns></returns>
    public bool UpdateMenuItemAccess(string connectionStringName,
                                            int operatorGroupId,
                                            int menuItemId,
                                            Boolean access)
    {
        bool resultVal = false;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_MenuItem_OperatorGroup_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "operatorGroupId", DbType.Int32, operatorGroupId);
        db.AddInParameter(dbCommand, "menuItemId", DbType.Int32, menuItemId);
        db.AddInParameter(dbCommand, "access", DbType.Boolean, access);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                resultVal = true;
            }
            catch { }

            connection.Close();

            return resultVal;
        }
    }
}
