using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

/// <summary>
/// Summary description for Despatch
/// </summary>
public class Despatch
{
    public int pallets = -1;
    public int total = -1;
    public string despatchLocation = "123456789012345";

    #region Constructor Logic
    public Despatch()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region RecordWeight
    public int RecordWeight(string connectionStringName, string barcode, decimal tareWeight, decimal weight)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Despatch_Check_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "tareWeight", DbType.Decimal, tareWeight);
        db.AddInParameter(dbCommand, "weight", DbType.Decimal, weight);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion RecordWeight

    #region ConfirmVehicle
    public ArrayList ConfirmVehicle(string connectionStringName, string documentNumber, string vehicle, string barcode)
    {
        int result = 0;
        ArrayList arrlist = new ArrayList();
        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Despatch_Confirm_Vehicle";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "documentNumber", DbType.String, documentNumber);
        db.AddInParameter(dbCommand, "vehicle", DbType.String, vehicle);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // Set Pallets as an output parameter
        db.AddOutParameter(dbCommand, "pallets", DbType.Int32, pallets);
        // Set Total as an output parameter
        db.AddOutParameter(dbCommand, "total", DbType.Int32, total);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);
                arrlist.Add(result);

                // Get Pallets output parameter
                pallets = (int)db.GetParameterValue(dbCommand, "pallets");
                arrlist.Add(pallets);

                // Get Total output parameter
                total = (int)db.GetParameterValue(dbCommand, "total");
                arrlist.Add(total);

            }
            catch { }

            connection.Close();

            return arrlist;
        }
    }
    #endregion ConfirmVehicle

    #region ConfirmLocation
    public ArrayList ConfirmLocation(string connectionStringName, string location, string barcode)
    {
        int result = 0;
        ArrayList arrlist = new ArrayList();

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Despatch_Confirm_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "location", DbType.String, location);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // Set Pallets as an output parameter
        db.AddOutParameter(dbCommand, "pallets", DbType.Int32, pallets);
        // Set Total as an output parameter
        db.AddOutParameter(dbCommand, "total", DbType.Int32, total);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);
                arrlist.Add(result.ToString());

                // Get Pallets output parameter
                pallets = (int)db.GetParameterValue(dbCommand, "pallets");
                arrlist.Add(pallets.ToString());

                // Get Total output parameter
                total = (int)db.GetParameterValue(dbCommand, "total");
                arrlist.Add(total.ToString());
            }
            catch { }

            connection.Close();

            return arrlist;
        }
    }
    #endregion ConfirmLocation

    #region ConfirmLocation
    public ArrayList ConfirmLocation(string connectionStringName, string documentNumber, string location, string barcode)
    {
        int result = 0;
        ArrayList arrlist = new ArrayList();

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Despatch_Confirm_Location";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "documentNumber", DbType.String, documentNumber);
        db.AddInParameter(dbCommand, "location", DbType.String, location);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // Set Pallets as an output parameter
        db.AddOutParameter(dbCommand, "pallets", DbType.Int32, pallets);
        // Set Total as an output parameter
        db.AddOutParameter(dbCommand, "total", DbType.Int32, total);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);
                arrlist.Add(result.ToString());

                // Get Pallets output parameter
                pallets = (int)db.GetParameterValue(dbCommand, "pallets");
                arrlist.Add(pallets.ToString());

                // Get Total output parameter
                total = (int)db.GetParameterValue(dbCommand, "total");
                arrlist.Add(total.ToString());
            }
            catch { }

            connection.Close();

            return arrlist;
        }
    }
    #endregion ConfirmLocation

    #region MoveToDespatch
    public ArrayList MoveToDespatch(string connectionStringName, string barcode, string location)
    {
        int result = 0;
        ArrayList arrlist = new ArrayList();

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Despatch_Move";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);


        db.AddParameter(dbCommand, "location", DbType.String, ParameterDirection.InputOutput, "location", DataRowVersion.Current, location);
        db.AddParameter(dbCommand, "pallets", DbType.Int32, ParameterDirection.InputOutput, "pallets", DataRowVersion.Current, pallets);
        db.AddParameter(dbCommand, "total", DbType.Int32, ParameterDirection.InputOutput, "total", DataRowVersion.Current, total);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);
                arrlist.Add(result.ToString());

                // Get Location output parameter
                despatchLocation = db.GetParameterValue(dbCommand, "location").ToString();
                arrlist.Add(despatchLocation.ToString());

                // Get Pallets output parameter
                pallets = (int)db.GetParameterValue(dbCommand, "pallets");
                arrlist.Add(pallets.ToString());

                // Get Total output parameter
                total = (int)db.GetParameterValue(dbCommand, "total");
                arrlist.Add(total.ToString());
            }
            catch { }

            connection.Close();

            return arrlist;
        }
    }
    #endregion MoveToDespatch
}