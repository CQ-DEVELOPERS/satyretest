using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

    /// <summary>
    /// PutawayFull allows an operator to scan a pallet and put it away
    /// </summary>
public class Receiving
{
    public Receiving()
    {
    }

    #region ReceivingDocumentSelect
    public DataSet ReceivingDocumentSelect(string connectionStringName,
                                           Int32 receiptId)
    {
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        DataSet ds = null;
        String sqlCommand = "p_Mobile_Inbound_Order_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ReceivingDocumentSelect

    #region ReceivingLineSelect
    public DataSet ReceivingLineSelect(string connectionStringName,
                                           Int32 receiptLineId)
    {
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        DataSet ds = null;
        String sqlCommand = "p_Mobile_Inbound_Line_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion ReceivingLineSelect

    #region ReceivingDocumentUpdate
    public Int32 ReceivingDocumentUpdateDeliveryNoteNumber(String connectionStringName, Int32 receiptId, String deliveryNoteNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "deliveryNoteNumber", DbType.String, deliveryNoteNumber);
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingDocumentUpdateDeliveryDate(String connectionStringName, Int32 receiptId, String deliveryDate)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "deliveryDate", DbType.String, deliveryDate);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingDocumentUpdateSealNumber(String connectionStringName, Int32 receiptId, String sealNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "sealNumber", DbType.String, sealNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingDocumentUpdateVehicleRegistration(String connectionStringName, Int32 receiptId, String vehicleRegistration)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Order_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "vehicleRegistration", DbType.String, vehicleRegistration);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ReceivingDocumentUpdate

    #region ReceivingGetExpiryDate
    public String ReceivingGetExpiryDate(String connectionStringName, Int32 receiptLineId)
    {
        String result = "";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Get_Expiry";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = db.ExecuteScalar(dbCommand).ToString();
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ReceivingGetExpiryDate

    #region ReceivingLineUpdate
    public Int32 ReceivingLineUpdateBatch(String connectionStringName, Int32 receiptLineId, String batch)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingLineUpdateBOEL(String connectionStringName, Int32 receiptLineId, String batch, String boe)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "boe", DbType.String, boe);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingLineUpdateExpiry(String connectionStringName, Int32 receiptLineId, String expiryDate)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "expiryDate", DbType.String, expiryDate);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingLineUpdateQuantity(String connectionStringName, Int32 receiptLineId, Decimal quantity, Int32 operatorId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingLineUpdateSample(String connectionStringName, Int32 receiptLineId, Decimal sampleQuantity)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "sampleQuantity", DbType.Decimal, sampleQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingLineConfirmStoreLocation(String connectionStringName, Int32 receiptLineId, String storeLocation)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "storeLocation", DbType.String, storeLocation);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingLineUpdateNumberOfPallets(String connectionStringName, Int32 receiptLineId, Decimal numberOfPallets)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "numberOfPallets", DbType.String, numberOfPallets);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingLineUpdateReject(String connectionStringName, Int32 receiptLineId, Decimal rejectQuantity, Int32 reasonId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "rejectQuantity", DbType.Decimal, rejectQuantity);
        db.AddInParameter(dbCommand, "reasonId", DbType.Int32, reasonId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 ReceivingLinePalletId(String connectionStringName, Int32 receiptLineId, String palletId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Line_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "palletId", DbType.String, palletId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ReceivingLineUpdate

    #region ConfirmOrder
    public Int32 ConfirmOrder(String connectionStringName, Int32 warehouseId, String orderNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Confirm_Inbound_Order";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmOrder

    #region ConfirmProduct
    public Int32 ConfirmProduct(String connectionStringName, Int32 receiptId, String barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Confirm_Inbound_Product";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmProduct

    #region ConfirmStorageUnit
    public Int32 ConfirmStorageUnit(String connectionStringName, Int32 receiptId, String barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Confirm_Inbound_StorageUnit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmStorageUnit

    #region GetQuestion
    public DataSet GetQuestion(string connectionStringName,
                                Int32 questionaireId,
                                String questionaireType,
                                Int32 sequence,
                                Int32 WarehouseId)
    {
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        DataSet ds = null;
        String sqlCommand = "p_Question_Get";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "questionaireId", DbType.Int32, questionaireId);
        db.AddInParameter(dbCommand, "questionaireType", DbType.String, questionaireType);
        db.AddInParameter(dbCommand, "sequence", DbType.Int32, sequence);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, WarehouseId);


        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion GetQuestion

    #region CreateBatch
    public Int32 CreateBatch(String connectionStringName, Int32 receiptId, Int32 storageUnitId, String batch, String batchReferenceNumber, String expiryDate)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Batch_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "batchReferenceNumber", DbType.String, batchReferenceNumber);
        db.AddInParameter(dbCommand, "expiryDate", DbType.String, expiryDate);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateBatch

    #region ConfirmBatchReferenceNumber
    public Int32 ConfirmBatchReferenceNumber(String connectionStringName, Int32 storageUnitId, String batchReferenceNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_BatchReferenceNumber";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "batchReferenceNumber", DbType.String, batchReferenceNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmBatchReferenceNumber

    #region ConfirmSupplierBatch
    public Int32 ConfirmSupplierBatch(String connectionStringName, Int32 receiptId, String batch)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Confirm_Inbound_Supplier_Batch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmSupplierBatch

    #region CreatePallet
    public Int32 CreatePallet(String connectionStringName, Int32 receiptId, Int32 jobId, Int32 storageUnitId, Int32 storageUnitBatchId, Decimal quantity, String instructionTypeCode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Create_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "instructionTypeCode", DbType.String, instructionTypeCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreatePallet

    #region InsertQuestion
    public bool InsertQuestion(String connectionStringName,
                                string OrderNumber,
                                Int32 jobId,
                                Int32 palletId,
                                Int32 receiptId,
                                Int32 questionId,
                                String answer)
    {
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        bool result = false;
        String sqlCommand = "p_Question_Answer_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "questionId", DbType.Int32, questionId);
        db.AddInParameter(dbCommand, "answer", DbType.String, answer);
        //db.AddInParameter(dbCommand, "dateAsked", DbType.DateTime, dateAsked);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteScalar(dbCommand);
                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion InsertQuestion

    #region SearchPackageProducts
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchPackageProducts(string connectionStringName) //, string productCode, string product)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_StorageUnit_Search_Product_Packaging";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, -1);
        db.AddInParameter(dbCommand, "Product", DbType.String, -1);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchPackageProducts

    #region InsertPackageLines
    /// <summary>
    /// Palletises a ReceiptDocument or a Receipt Line depending on the input parameters.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <param name="operatorId"></param>
    /// <param name="acceptedQuantity"></param>
    /// <param name="deliveryNoteQuantity"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="alternateBatch"></param>
    /// <returns></returns>
    public int InsertPackageLines(string connectionStringName,
                                int WarehouseId,
                                int ReceiptId,
                                int storageUnitBatchId,
                                int operatorId,
                                decimal ActualQuantity)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Receiving_PackageLine_Insert_Actual";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "ActualQuantity", DbType.Decimal, ActualQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }

            catch
            {

            }

            connection.Close();

            return result;
        }
    }
    #endregion "InsertPackageLines"

    #region SetPackageLines
    /// <summary>
    /// Palletises a ReceiptDocument or a Receipt Line depending on the input parameters.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <param name="operatorId"></param>
    /// <param name="acceptedQuantity"></param>
    /// <param name="deliveryNoteQuantity"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="alternateBatch"></param>
    /// <returns></returns>
    public int SetPackageLines(string connectionStringName,
                                int PackageLineId,
                                int storageUnitBatchId,
                                int operatorId,
                                decimal acceptedQuantity,
                                decimal deliveryNoteQuantity,
                                decimal rejectQuantity)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Receiving_PackageLine_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "PackageLineId", DbType.Int32, PackageLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "AcceptedQuantity", DbType.Decimal, acceptedQuantity);
        db.AddInParameter(dbCommand, "DeliveryNoteQuantity", DbType.Decimal, deliveryNoteQuantity);
        db.AddInParameter(dbCommand, "RejectQuantity", DbType.Decimal, rejectQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "SetPackageLines"

    #region CompletePackageLines
    /// <summary>
    /// Completes packaging lines that are Status Received and changes them to Receiving Complete
    /// </summary>
    /// <returns></returns>
    public int CompletePackageLines(string connectionStringName,
                                     int WarehouseId,
                                     int ReceiptId)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Receiving_PackageLine_Complete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
                result = 1;
            }
            catch (Exception ex)
            {

            }

            connection.Close();

            return result;
        }
    }
    #endregion "CompletePackageLines"

    #region Redelivery
    public Int32 Redelivery(String connectionStringName, Int32 receiptId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Redelivery";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Redelivery

    #region ReceivingLineSampleQuantity
    public String ReceivingLineSampleQuantity(String connectionStringName, Int32 receiptLineId)
    {
        String result = "0";

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Inbound_Line_Sample_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = db.ExecuteScalar(dbCommand).ToString();
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ReceivingLineSampleQuantity

    #region GetSampleTypes
    public DataSet GetSampleTypes(string connectionStringName,
                                Int32 receiptLineId)
    {
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        DataSet ds = null;
        String sqlCommand = "p_Receiving_Get_Samples";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion GetSampleTypes

    #region GetDims
    public DataSet GetDims(string connectionStringName,
                                Int32 receiptLineId)
    {
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        DataSet ds = null;
        String sqlCommand = "p_Receiving_Get_Dims";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion GetDims

    #region SetDims
    public bool SetDims(string connectionStringName,
                                Int32 receiptLineId,
                                Decimal length,
                                Decimal width,
                                Decimal height,
                                Decimal weight,
                                Decimal quantity,
                                Decimal palletQuantity)
    {
        bool result = false;
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);
        String sqlCommand = "p_Receiving_Set_Dims";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);
        db.AddInParameter(dbCommand, "length", DbType.Decimal, length);
        db.AddInParameter(dbCommand, "width", DbType.Decimal, width);
        db.AddInParameter(dbCommand, "height", DbType.Decimal, height);
        db.AddInParameter(dbCommand, "weight", DbType.Decimal, weight);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "palletQuantity", DbType.Decimal, palletQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteScalar(dbCommand);
                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SetDims

    #region IsBOEL
    public static bool IsBOEL(string connectionStringName, int receiptLineId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Mobile_Inbound_BOE_Line";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (bool)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion IsBOEL

    #region GetPackingOrder

    public Int32 GetPackingOrder(String connectionStringName, Int32 receiptId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        String sqlCommand = "p_Mobile_Get_Packaing_Order";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {

                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetPackingOrder


    #region GetBatchOfOrder
    public string GetBatchOfOrder(string connectionStringName, int receiptLineId)
    {
        string result = string.Empty;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        String sqlCommand = "p_Mobile_Inbound_Get_Batch_of_Order";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (string)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }

    #endregion GetBatchOfOrder

}