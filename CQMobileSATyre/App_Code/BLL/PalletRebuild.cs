using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

    /// <summary>
    /// Author: Grant Schultz
    /// Date:   20 July 2007
    /// Summary description for OutboundShipment
    /// </summary>
public class PalletRebuild
{
    #region Constructor Logic
    public int outboundShipmentId;

    public PalletRebuild()
    {
        outboundShipmentId = -1;
    }
    #endregion "Constructor Logic"

    #region GetJob
    /// <summary>
    /// Returns the JobId for the Barcode Scanned
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="barcode"></param>
    /// <returns></returns>
    public int GetJob(string connectionStringName,
                                string barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Rebuild_Job_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "GetJob"

    #region GetInstructions
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <param name="barcode"></param>
    /// <returns></returns>
    public DataSet GetInstructions(string connectionStringName, int jobId, string barcode)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Rebuild_Instructions";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "Barcode", DbType.String, barcode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetInstructions"

    #region GetComments
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="jobId"></param>
    /// <returns></returns>
    public DataSet GetComments(string connectionStringName, int jobId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Rebuild_Instruction_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "JobId", DbType.Int32, jobId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion "GetComments"

    #region Transfer
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueLineId"></param>
    /// <param name="instructionId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool Transfer(string connectionStringName, int jobId, int issueLineId, int instructionId, Decimal quantity)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Rebuild_Link";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "issueLineId", DbType.Int32, issueLineId);
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Transfer"

    #region Remove
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueLineId"></param>
    /// <param name="instructionId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool Remove(string connectionStringName, int jobId, int issueLineId, int instructionId, Decimal quantity)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Rebuild_Unlink";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "issueLineId", DbType.Int32, issueLineId);
        db.AddInParameter(dbCommand, "instructionId", DbType.Int32, instructionId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Remove"

    #region Move
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="issueLineId"></param>
    /// <param name="instructionId"></param>
    /// <param name="quantity"></param>
    /// <returns></returns>
    public bool Move(string connectionStringName, int oldJobId, int newJobId)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Pallet_Rebuild_Move";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "oldJobId", DbType.Int32, oldJobId);
        db.AddInParameter(dbCommand, "newJobId", DbType.Int32, newJobId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Move"
}