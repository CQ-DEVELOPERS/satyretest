using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


/// <summary>
/// Author: Grant Schultz
/// Date:   20 June 2007
/// Summary description for InstructionMaintenance
/// </summary>
public class Container
{
    #region Constructor Logic
    public Container()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetItems
    public DataSet GetItems(string connectionStringName, int warehouseId, int operatorId, string barcode)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Get_Items";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetItems(string connectionStringName, int warehouseId, int operatorId, string barcode, string locationBarcode)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Get_Items";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "locationBarcode", DbType.String, locationBarcode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetItems(string connectionStringName, int warehouseId, int operatorId, string barcode, string productBarcode, string batch)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Get_Items";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "productBarcode", DbType.String, productBarcode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetItems(string connectionStringName, int warehouseId, int operatorId, string barcode, string productBarcode, string batch, string locationBarcode)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Get_Items";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "productBarcode", DbType.String, productBarcode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "locationBarcode", DbType.String, locationBarcode);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetItems

    #region SetItems
    public bool SetItems(string connectionStringName, int containerHeaderId, int containerDetailId, int operatorId, decimal quantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Set_Items";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "containerHeaderId", DbType.Int32, containerHeaderId);
        db.AddInParameter(dbCommand, "containerDetailId", DbType.Int32, containerDetailId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion SetItems

    #region Move
    public bool Move(string connectionStringName, int warehouseId, int operatorId, int containerHeaderId, string storeLocation)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Move_Items";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "containerHeaderId", DbType.Int32, containerHeaderId);
        db.AddInParameter(dbCommand, "storeLocation", DbType.String, storeLocation);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    public bool Move(string connectionStringName, int warehouseId, int operatorId, int containerHeaderId, string storeLocation, string barcode, string batch, Decimal quantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Move_Items";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "containerHeaderId", DbType.Int32, containerHeaderId);
        db.AddInParameter(dbCommand, "storeLocation", DbType.String, storeLocation);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion Move

    #region Pack
    public bool Pack(string connectionStringName, int warehouseId, int operatorId, int containerHeaderId, string pickLocation, string barcode, string batch, Decimal quantity)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Pack_Items";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "containerHeaderId", DbType.Int32, containerHeaderId);
        db.AddInParameter(dbCommand, "pickLocation", DbType.String, pickLocation);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute command
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch
            {
            }
            connection.Close();

            return result;
        }
    }
    #endregion Pack

    #region GetSOH
    public DataSet GetSOH(string connectionStringName, int warehouseId, int operatorId, int containerHeaderId)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Get_SOH";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "containerHeaderId", DbType.Int32, containerHeaderId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetSOH

    #region GetPickLocation
    public DataSet GetPickLocation(string connectionStringName, int warehouseId, int operatorId, int containerHeaderId, int containerDetailId)
    {

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Container_Get_PickLocation";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "containerHeaderId", DbType.Int32, containerHeaderId);
        db.AddInParameter(dbCommand, "containerDetailId", DbType.Int32, containerDetailId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetPickLocation
}
