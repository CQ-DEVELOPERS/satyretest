using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// PutawayFull allows an operator to scan a pallet and put it away
/// </summary>
public class IntransitLoad
{
    public IntransitLoad()
    {
    }

    #region IntransitLoadSelect
    public DataSet IntransitLoadSelect(string connectionStringName,
                                           Int32 IntransitLoadId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Load_Select";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "IntransitLoadId", DbType.Int32, IntransitLoadId);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion IntransitLoadSelect

    #region CreateOrder
    public Int32 CreateOrder(String connectionStringName, Int32 warehouseId, Int32 operatorId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Create_Load";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateOrder

    #region AddJob
    public Int32 AddJob(String connectionStringName, Int32 warehouseId, Int32 operatorId, Int32 intransitLoadId, String barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Load_Add_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion AddJob

    #region RemoveJob
    public Int32 RemoveJob(String connectionStringName, Int32 warehouseId, Int32 operatorId, Int32 intransitLoadId, String barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Load_Remove_Job";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion RemoveJob

    #region GetOrder
    public Int32 GetOrder(String connectionStringName, Int32 warehouseId, String orderNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Get_Load";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion GetOrder

    #region GetDrivers
    public DataSet GetDrivers(string connectionStringName)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Contactist_Get_Drivers";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        ds = db.ExecuteDataSet(dbCommand);

        return ds;
    }
    #endregion GetDrivers

    #region Update
    public Int32 UpdateDeliveryNoteNumber(String connectionStringName, Int32 intransitLoadId, String deliveryNoteNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_IntransitLoad_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "deliveryNoteNumber", DbType.String, deliveryNoteNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 UpdateDeliveryDate(String connectionStringName, Int32 intransitLoadId, String deliveryDate)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_IntransitLoad_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "deliveryDate", DbType.String, deliveryDate);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 UpdateLoadClosed(String connectionStringName, Int32 intransitLoadId, String loadClosed)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_IntransitLoad_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "loadClosed", DbType.String, loadClosed);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 UpdateReceivedDate(String connectionStringName, Int32 intransitLoadId, String receivedDate)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_IntransitLoad_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "receivedDate", DbType.String, receivedDate);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 UpdateOffloaded(String connectionStringName, Int32 intransitLoadId, String offloaded)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_IntransitLoad_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "offloaded", DbType.String, offloaded);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 UpdateSealNumber(String connectionStringName, Int32 intransitLoadId, String sealNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_IntransitLoad_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "sealNumber", DbType.String, sealNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 UpdateVehicleRegistration(String connectionStringName, Int32 intransitLoadId, String vehicleRegistration)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_IntransitLoad_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "vehicleRegistration", DbType.String, vehicleRegistration);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 UpdateRemarks(String connectionStringName, Int32 intransitLoadId, String remarks)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_IntransitLoad_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "remarks", DbType.String, remarks);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 UpdateRoute(String connectionStringName, Int32 intransitLoadId, String route)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_IntransitLoad_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "route", DbType.String, route);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public Int32 UpdateDriverId(String connectionStringName, Int32 intransitLoadId, Int32 driverId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_IntransitLoad_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        db.AddInParameter(dbCommand, "driverId", DbType.Int32, driverId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Update

    #region ConfirmOrder
    public Int32 ConfirmOrder(String connectionStringName, Int32 warehouseId, String orderNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Inbound_Order";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "orderNumber", DbType.String, orderNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmOrder

    #region ConfirmProduct
    public Int32 ConfirmProduct(String connectionStringName, Int32 receiptId, String barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Inbound_Product";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmProduct

    #region ConfirmStorageUnit
    public Int32 ConfirmStorageUnit(String connectionStringName, Int32 receiptId, String barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Inbound_StorageUnit";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmStorageUnit

    #region GetQuestion
    public DataSet GetQuestion( string connectionStringName, 
                                Int32 questionaireId, 
                                String questionaireType,
                                Int32 sequence,
                                Int32 WarehouseId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        DataSet ds = null;
        string sqlCommand = connectionStringName + ".dbo.p_Question_Get";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "questionaireId", DbType.Int32, questionaireId);
        db.AddInParameter(dbCommand, "questionaireType", DbType.String, questionaireType);
        db.AddInParameter(dbCommand, "sequence", DbType.Int32, sequence);
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, WarehouseId);


        ds = db.ExecuteDataSet(dbCommand);
        
        return ds;
    }
    #endregion GetQuestion

    #region CreateBatch
    public Int32 CreateBatch(String connectionStringName, Int32 receiptId, Int32 storageUnitId, String batch, String batchReferenceNumber, String expiryDate)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Inbound_Batch_Create";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);
        db.AddInParameter(dbCommand, "batchReferenceNumber", DbType.String, batchReferenceNumber);
        db.AddInParameter(dbCommand, "expiryDate", DbType.String, expiryDate);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreateBatch

    #region ConfirmBatchReferenceNumber
    public Int32 ConfirmBatchReferenceNumber(String connectionStringName, Int32 storageUnitId, String batchReferenceNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_BatchReferenceNumber";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "batchReferenceNumber", DbType.String, batchReferenceNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmBatchReferenceNumber

    #region ConfirmSupplierBatch
    public Int32 ConfirmSupplierBatch(String connectionStringName, Int32 receiptId, String batch)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Confirm_Inbound_Supplier_Batch";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "batch", DbType.String, batch);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ConfirmSupplierBatch

    #region CreatePallet
    public Int32 CreatePallet(String connectionStringName, Int32 receiptId, Int32 jobId, Int32 storageUnitId, Int32 storageUnitBatchId, Decimal quantity, String instructionTypeCode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Inbound_Create_Pallet";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);
        db.AddInParameter(dbCommand, "storageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "quantity", DbType.Decimal, quantity);
        db.AddInParameter(dbCommand, "instructionTypeCode", DbType.String, instructionTypeCode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion CreatePallet

    #region InsertQuestion
    public bool InsertQuestion(String connectionStringName,
                                string OrderNumber,
                                Int32 jobId,
                                Int32 palletId,
                                Int32 receiptId,
                                Int32 questionId,
                                String answer)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        bool result = false;
        string sqlCommand = connectionStringName + ".dbo.p_Question_Answer_Insert";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "OrderNumber", DbType.String, OrderNumber);
        db.AddInParameter(dbCommand, "jobId", DbType.Int32, jobId);
        db.AddInParameter(dbCommand, "palletId", DbType.Int32, palletId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, receiptId);
        db.AddInParameter(dbCommand, "questionId", DbType.Int32, questionId);
        db.AddInParameter(dbCommand, "answer", DbType.String, answer);
        //db.AddInParameter(dbCommand, "dateAsked", DbType.DateTime, dateAsked);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                 db.ExecuteScalar(dbCommand);
                 result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion InsertQuestion

    #region SearchPackageProducts
    /// <summary>
    /// Searches Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchPackageProducts(string connectionStringName) //, string productCode, string product)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_StorageUnit_Search_Product_Packaging";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add paramters
        db.AddInParameter(dbCommand, "ProductCode", DbType.String, -1);
        db.AddInParameter(dbCommand, "Product", DbType.String, -1);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion SearchPackageProducts

    #region InsertPackageLines
    /// <summary>
    /// Palletises a ReceiptDocument or a Receipt Line depending on the input parameters.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <param name="operatorId"></param>
    /// <param name="acceptedQuantity"></param>
    /// <param name="deliveryNoteQuantity"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="alternateBatch"></param>
    /// <returns></returns>
    public int InsertPackageLines(string connectionStringName,
                                int WarehouseId,
                                int ReceiptId,
                                int storageUnitBatchId,
                                int operatorId,
                                decimal ActualQuantity)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_PackageLine_Insert_Actual";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "ActualQuantity", DbType.Decimal, ActualQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }

            catch 
            
            { 
            
            }

            connection.Close();

            return result;
        }
    }
    #endregion "InsertPackageLines"

    #region SetPackageLines
    /// <summary>
    /// Palletises a ReceiptDocument or a Receipt Line depending on the input parameters.
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="receiptLineId"></param>
    /// <param name="storageUnitBatchId"></param>
    /// <param name="operatorId"></param>
    /// <param name="acceptedQuantity"></param>
    /// <param name="deliveryNoteQuantity"></param>
    /// <param name="storageUnitId"></param>
    /// <param name="alternateBatch"></param>
    /// <returns></returns>
    public int SetPackageLines(string connectionStringName,
                                int PackageLineId,
                                int storageUnitBatchId,
                                int operatorId,
                                decimal acceptedQuantity,
                                decimal deliveryNoteQuantity,
                                decimal rejectQuantity)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_PackageLine_Update";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "PackageLineId", DbType.Int32, PackageLineId);
        db.AddInParameter(dbCommand, "StorageUnitBatchId", DbType.Int32, storageUnitBatchId);
        db.AddInParameter(dbCommand, "OperatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "AcceptedQuantity", DbType.Decimal, acceptedQuantity);
        db.AddInParameter(dbCommand, "DeliveryNoteQuantity", DbType.Decimal, deliveryNoteQuantity);
        db.AddInParameter(dbCommand, "RejectQuantity", DbType.Decimal, rejectQuantity);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "SetPackageLines"


    #region CompletePackageLines
    /// <summary>
    /// Completes packaging lines that are Status Received and changes them to Receiving Complete
    /// </summary>
    /// <returns></returns>
    public int CompletePackageLines(string connectionStringName,
                                     int WarehouseId,
                                     int ReceiptId)
    {
        int result = -1;

        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Receiving_PackageLine_Complete";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "WarehouseId", DbType.Int32, WarehouseId);
        db.AddInParameter(dbCommand, "ReceiptId", DbType.Int32, ReceiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
                result = 1;
            }
            catch (Exception ex)
            {

            }

            connection.Close();

            return result;
        }
    }
    #endregion "CompletePackageLines"

    #region Redelivery
    public Int32 Redelivery(String connectionStringName, Int32 receiptId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Inbound_Redelivery";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptId", DbType.Int32, receiptId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion Redelivery

    #region ReceivingLineSampleQuantity
    public Decimal ReceivingLineSampleQuantity(String connectionStringName, Int32 receiptLineId)
    {
        Decimal result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_Mobile_Inbound_Line_Sample_Quantity";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (Decimal)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion ReceivingLineSampleQuantity

    #region FinishLoad
    public bool FinishLoad(String connectionStringName,
                                Int32 intransitLoadId)
    {
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");
        bool result = false;
        string sqlCommand = connectionStringName + ".dbo.p_IntransitLoad_Finished";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);


        db.AddInParameter(dbCommand, "intransitLoadId", DbType.Int32, intransitLoadId);
        
        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                 db.ExecuteScalar(dbCommand);
                 result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion FinishLoad
}
