using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/// <summary>
/// Summary description for MobileProduct
/// </summary>
public class MobileProduct
{
    public MobileProduct()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region SearchProducts
    /// <summary>
    /// Searchs Products by Product Description and Product Code.
    /// </summary>
    public DataSet SearchProducts(string productCode, string product, int searchNum)
    {

        // DataSet that will hold the returned results		
        DataSet genDataSet = new DataSet();

        try
        {
            // Create the Database object, passing it the required database service.
            Database db = DatabaseFactory.CreateDatabase("Connection String");

            string sqlCommand = "p_StorageUnit_Search_Product_SKU";
            DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

            // Add paramters
            db.AddInParameter(dbCommand, "ProductCode", DbType.String, productCode);
            db.AddInParameter(dbCommand, "Product", DbType.String, product);
            db.AddInParameter(dbCommand, "SearchNum", DbType.Int32, searchNum);       


            genDataSet = db.ExecuteDataSet(dbCommand);
        }
        catch (Exception ex)
        {
            string strEx = ex.Message.ToString();
        }

        return genDataSet;
    }
    #endregion
}
