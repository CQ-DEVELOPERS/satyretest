using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

/// <summary>
/// Summary description for Transact
/// </summary>
public class SerialNumber
{
    public string skuCode = "1234567890";
    public Decimal quantity = -1;

    #region Constructor Logic
    public SerialNumber()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructor Logic

    #region GetSerialNumbers
    public DataSet GetSerialNumbers(string connectionStringName, int keyId, string keyType, int storageUnitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Get_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "keyId", DbType.Int32, keyId);
        db.AddInParameter(dbCommand, "keyType", DbType.String, keyType);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetSerialNumbers

    #region GetTotal
    public DataSet GetTotal(string connectionStringName, int keyId, string keyType)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Get_Total";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "keyId", DbType.Int32, keyId);
        db.AddInParameter(dbCommand, "keyType", DbType.String, keyType);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    public DataSet GetTotal(string connectionStringName, int keyId, string keyType, int storageUnitId)
    {
        // Create the Database object, passing it the required database service.
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Get_Total";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "keyId", DbType.Int32, keyId);
        db.AddInParameter(dbCommand, "keyType", DbType.String, keyType);
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        // DataSet that will hold the returned results
        DataSet dataSet = null;

        dataSet = db.ExecuteDataSet(dbCommand);

        return dataSet;
    }
    #endregion GetTotal

    #region SetSerialNumber
    public int SetSerialNumbers(string connectionStringName, int warehouseId, int operatorId, int keyId, string keyType, string serialNumber)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Set_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "keyId", DbType.Int32, keyId);
        db.AddInParameter(dbCommand, "keyType", DbType.String, keyType);
        db.AddInParameter(dbCommand, "serialNumber", DbType.String, serialNumber);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SetSerialNumber

    #region DeleteSerialNumber
    public int DeleteSerialNumber(string connectionStringName, int operatorId, int serialNumberId)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Delete_By_Type";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "serialNumberId", DbType.Int32, serialNumberId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (int)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion SetSerialNumbers

    #region IsTracked
    public static bool IsTrackedReceiptLineId(string connectionStringName, int receiptLineId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Tracked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "receiptLineId", DbType.Int32, receiptLineId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (bool)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    public static bool IsTrackedStorageUnitId(string connectionStringName, int storageUnitId)
    {
        bool result = false;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase("CQuentialCommon");

        string sqlCommand = connectionStringName + ".dbo.p_SerialNumber_Tracked";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        // Add parameters
        db.AddInParameter(dbCommand, "storageUnitId", DbType.Int32, storageUnitId);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                // Execute the query
                result = (bool)db.ExecuteScalar(dbCommand);

            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion IsTracked
}