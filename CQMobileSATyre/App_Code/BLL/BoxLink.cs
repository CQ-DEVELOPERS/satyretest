using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

    /// <summary>
    /// Author: Grant Schultz
    /// Date:   20 July 2007
    /// Summary description for OutboundShipment
    /// </summary>
public class BoxLink
{
    #region Constructor Logic
    public BoxLink()
    {
    }
    #endregion "Constructor Logic"

    #region ConfirmPalletId
    /// <summary>
    /// Returns the 0 if barcode aviable or -1 if failed
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="barcode"></param>
    /// <returns></returns>
    public int ConfirmPalletId(string connectionStringName, int warehouseId, int operatorId, string barcode)
    {
        int result = 0;

        // Create the Database object
        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Box_Confirm_Barcode";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                result = (int)db.ExecuteScalar(dbCommand);
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "ConfirmPalletId"

    #region Add
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="operatorId"></param>
    /// <param name="barcode"></param>
    /// <param name="referenceNumber"></param>
    /// <returns></returns>
    public bool Add(string connectionStringName, int warehouseId, int operatorId, string referenceNumber, string barcode)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Box_Link";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Add"

    #region Remove
    /// <summary>
    /// 
    /// </summary>
    /// <param name="connectionStringName"></param>
    /// <param name="warehouseId"></param>
    /// <param name="operatorId"></param>
    /// <param name="barcode"></param>
    /// <param name="referenceNumber"></param>
    /// <returns></returns>
    public bool Remove(string connectionStringName, int warehouseId, int operatorId, string referenceNumber, string barcode)
    {
        bool result = false;

        Database db = DatabaseFactory.CreateDatabase(connectionStringName);

        string sqlCommand = "p_Box_Unlink";
        DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);

        db.AddInParameter(dbCommand, "warehouseId", DbType.Int32, warehouseId);
        db.AddInParameter(dbCommand, "operatorId", DbType.Int32, operatorId);
        db.AddInParameter(dbCommand, "referenceNumber", DbType.String, referenceNumber);
        db.AddInParameter(dbCommand, "barcode", DbType.String, barcode);

        using (DbConnection connection = db.CreateConnection())
        {
            connection.Open();

            try
            {
                db.ExecuteNonQuery(dbCommand);

                result = true;
            }
            catch { }

            connection.Close();

            return result;
        }
    }
    #endregion "Remove"
}