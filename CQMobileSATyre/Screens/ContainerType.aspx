<%@ Page Language="C#"
    MasterPageFile="~/Master.master"
    AutoEventWireup="true" 
    CodeFile="ContainerType.aspx.cs"
    Inherits="Screens_ContainerType" 
    Title="<%$ Resources:Default, ContainerTypeTitle %>"
    StylesheetTheme="Default"
    Theme="Default"
%>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table style="font-size:xx-small" width="100%" >
    <tr>
        <td width="40%">
            <asp:Label ID="lblProductCode" runat="server" Text="<%$ Resources:Default, ContainerTypeTitle %>" Font-Size="XX-Small" Font-Bold="True" ForeColor="#3D4782"></asp:Label>
        </td>
        <td width="60%">
            <asp:TextBox ID="TextBoxProductCode" runat="server" Font-Size="XX-Small" TabIndex="1" Width="90%"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td width="40%">
            <asp:Label ID="lblContainer" runat="server" Text="<%$ Resources:Default, ContainerType %>" Font-Size="XX-Small" Font-Bold="True" ForeColor="#3D4782"></asp:Label>
        </td>
        <td width="60%">
            <asp:TextBox ID="TextBoxProduct" runat="server" Font-Size="XX-Small" TabIndex="2" Width="90%"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 18px" width="40%"></td>
        <td align="right" style="height: 18px" width="60%">
            <asp:Button ID="ButtonSearch" runat="server" Text="<%$ Resources:Default, Select %>" OnClick="ButtonSearch_Click" Font-Size="XX-Small" TabIndex="3" Font-Bold="True" ForeColor="#3D4782"/>&nbsp;
        </td>
    </tr>
</table>

<asp:GridView ID="GridViewProductSearch"
    runat="server"
    Font-Size="XX-Small"
    AllowPaging="True"
    AutoGenerateColumns="False"
    AutoGenerateSelectButton="True" 
    DataKeyNames="StorageUnitBatchId,Product"
    OnSelectedIndexChanged="GridViewProductSearch_OnSelectedIndexChanged"
    OnPageIndexChanging="GridViewProductSearch_PageIndexChanging" CellPadding="4" ForeColor="#333333" GridLines="None">
    <Columns>
        <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product2 %>" />
        <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, Code %>" />
        <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
    </Columns>
    <EmptyDataTemplate>
        <h5>No rows</h5>
    </EmptyDataTemplate>
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <EditRowStyle BackColor="#999999" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>

<%--<asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="Product" SelectMethod="SearchProducts">
    <SelectParameters>
        <asp:ControlParameter Name="Product" ControlID="TextBoxProduct" Type="String" />
        <asp:ControlParameter Name="ProductCode" ControlID="TextBoxProductCode" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>--%>
</asp:Content>

