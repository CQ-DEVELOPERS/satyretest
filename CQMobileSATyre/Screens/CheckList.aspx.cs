using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_CheckList : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/CheckList.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Session["FromURL"] = null;
                
                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["JobId"] = GridView1.SelectedDataKey["JobId"];
            Session["StorageUnitBatchId"] = GridView1.SelectedDataKey["StorageUnitBatchId"];
            Session["ConfirmedQuantity"] = GridView1.SelectedDataKey["ConfirmedQuantity"];
            Session["PreviousQuantity"] = null;

            ButtonFinished.Visible = true;
            ButtonAcceptQty.Visible = true;
            LabelQuantity.Visible = true;
            TextBoxQuantity.Visible = true;
        }
        catch { }
    }

    #region GridView1_RowDataBound
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex != -1)
        {
            if (e.Row.Cells[2].Text != "0")
            {
                e.Row.Cells[0].Text = "Checked";
                e.Row.Cells[0].Enabled = false;
            }
        }
    }
    #endregion GridView1_RowDataBound

    #region ButtonAcceptRef_Click
    protected void ButtonAcceptRef_Click(object sender, EventArgs e)
    {
        try
        {
            GridView1.DataBind();
            TextBoxReferenceNumber.Enabled = false;
            ButtonAcceptRef.Visible = false;
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }
    #endregion ButtonAcceptRef_Click

    #region ButtonFinished_Click
    protected void ButtonFinished_Click(object sender, EventArgs e)
    {
        try
        {
            CheckPallet cp = new CheckPallet();

            if(cp.StatusRollup(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], (int)Session["JobId"]))
                Master.MsgText = Resources.ResMessages.Successful;
            else
                Master.MsgText = Resources.ResMessages.SkuCheckQuantityQA;

            Reset();
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }
    #endregion ButtonFinished_Click

    #region ButtonAcceptQty
    protected void ButtonAcceptQty_Click(object sender, EventArgs e)
    {
        try
        {
            Decimal quantity = 0;
            Decimal confirmedQuantity = 0;
            Decimal previousQuantity = 0;

            CheckPallet cp = new CheckPallet();
            
            if (!Decimal.TryParse(TextBoxQuantity.Text, out quantity))
            {
                TextBoxQuantity.Text = "";
                Master.MsgText = Resources.ResMessages.Failed;
                return;
            }

            if(!Decimal.TryParse(Session["ConfirmedQuantity"].ToString(), out confirmedQuantity))
            {
                Master.MsgText = Resources.ResMessages.QuantityMismatch;
                return;
            }

            if (quantity != confirmedQuantity)
            {
                if (Session["PreviousQuantity"] == null)
                {
                    Session["PreviousQuantity"] = quantity;
                    TextBoxQuantity.Text = "";
                    Master.MsgText = Resources.ResMessages.QuantityReEnter;
                    return;
                }
                else
                {
                    if (!Decimal.TryParse(Session["PreviousQuantity"].ToString(), out previousQuantity))
                    {
                        Master.MsgText = Resources.ResMessages.QuantityMismatch;
                        return;
                    }

                    if (cp.StatusRollup(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], (int)Session["JobId"]))
                    {
                        Reset();

                        Master.MsgText = Resources.ResMessages.SkuCheckQuantityQA;
                        return;
                    }
                }
            }

            if (cp.UpdateCheckList(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], (int)Session["JobId"], (int)Session["StorageUnitBatchId"], quantity))
            {
                //ButtonFinished.Visible = false;
                ButtonAcceptQty.Visible = false;
                LabelQuantity.Visible = false;
                TextBoxQuantity.Visible = false;

                Master.MsgText = Resources.ResMessages.Successful;
                GridView1.SelectedIndex = -1;
                GridView1.DataBind();
            }
            else
                Master.MsgText = Resources.ResMessages.Failed;

            Session["PreviousQuantity"] = null;

            TextBoxQuantity.Text = "";
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }
    #endregion ButtonAcceptQty

    #region Reset
    protected void Reset()
    {
        TextBoxReferenceNumber.Text = "";
        TextBoxQuantity.Text = "";
        GridView1.DataBind();
        TextBoxReferenceNumber.Enabled = true;
        ButtonAcceptRef.Visible = true;
        ButtonFinished.Visible = false;
        ButtonAcceptQty.Visible = false;
        LabelQuantity.Visible = false;
        TextBoxQuantity.Visible = false;
    }
    #endregion Reset
}
