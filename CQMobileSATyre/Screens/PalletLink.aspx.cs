using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_PalletLink : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/PalletLink.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                if (Session["StorageUnitBatchId"].ToString() != null && Request.QueryString["Type"] != null)
                {
                    MultiView1.ActiveViewIndex = 3;
                }

            }
        }
        catch { }
    }
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        Transact tran = new Transact();
        
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                if (tran.ValidLocation(Session["ConnectionStringName"].ToString(), TextBoxStoreLocation.Text))
                {
                    Session["Location"] = TextBoxStoreLocation.Text;
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                }
                else
                {
                    TextBoxStoreLocation.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidLocation;
                }

                break;
            case 1:
                MultiView1.ActiveViewIndex++;
                try
                {
                    Session["PalletId"] = TextBoxPalletId.Text;
                    Session["StorageUnitBatchId"] = null;
                    Session["URL"] = "~/Screens/Palletlink.aspx?Type=ProductSearch";
                    Response.Redirect("~/Screens/ProductSearch.aspx");

                }
                catch {
                    Master.MsgText = "Error";
                    }

                break;
            case 2:
                MultiView1.ActiveViewIndex++;
                break;
            case 3:
                Decimal result;

                if (Decimal.TryParse(TextBoxQuantity.Text, out result))
                {
                    Transact t2 = new Transact();

                    LinkButtonAccept.Text = Resources.Default.Accept;

                    MultiView1.ActiveViewIndex = 0;
                    if (t2.PalletLink(Session["ConnectionStringName"].ToString(),
                                  (int)Session["OperatorId"],
                                  (int)Session["StorageUnitBAtchId"],
                                  Session["PalletId"].ToString(),
                                  Session["Location"].ToString(),
                                  Decimal.Parse(TextBoxQuantity.Text),
                                  "STL"))
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidPalletId;
                    }
                }
                else
                {
                    Master.MsgText = Resources.ResMessages.InvalidField;
                }


                break;
        }
    }
    protected void TextBoxQuantity_TextChanged(object sender, EventArgs e)
    {

    }
}
