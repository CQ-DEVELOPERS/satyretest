using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_GetMixedPickDetails : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //lbAccept.Attributes.Add("onclick", "this.disabled= 'disabled' ");
        }

        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 19))
        {
            LabelTareWeight.Visible = true;
            TextBoxTareWeight.Visible = true;
        }
    }

    protected void lbAccept_Click(object sender, EventArgs e)
    {
        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 19))
        {
            ConfirmTareWeight();
        }
        else
        {
            Session["Message"] = Resources.ResMessages.Successful;
            Session["Error"] = false;
            Session["CurrentView"] = "GetMixedPickDetails";
        }
    }

    protected bool ConfirmTareWeight()
    {
        try
        {
            Transact tran = new Transact();
            decimal tareWeight = 0;

            if (!decimal.TryParse(TextBoxTareWeight.Text, out tareWeight))
            {
                TextBoxTareWeight.Text = "";
                Session["Error"] = true;
                Session["Message"] = Resources.ResMessages.InvalidTareWeight;
                return false;
            }

            int result = tran.TareWeight(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], tareWeight);

            if (result == 0)
            {
                Session["Message"] = Resources.ResMessages.Successful;
                Session["Error"] = false;
                Session["CurrentView"] = "GetMixedPickDetails";
                return true;
            }
            else
            {
                Session["Message"] = Resources.ResMessages.InvalidTareWeight;
                Session["Error"] = true;
                return false;
            }
        }

        catch (Exception ex)
        {
            Session["Error"] = true;
            Session["Message"] = ex.Message;
            return false;
        }
    }
}
