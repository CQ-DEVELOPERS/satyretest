<%@ Page Language="C#" 
    MasterPageFile="~/Master.master" 
    AutoEventWireup="true" 
    CodeFile="YardEntry.aspx.cs" 
    Inherits="Screens_YardEntry" 
    Title="Container On Dock" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelContainer" runat="server" Text="Container No:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxBarcode" runat="server" Text="" TabIndex="1"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelLocation" runat="server" Text="Location:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxLocation" runat="server" Text="" TabIndex="2"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <br />
                <br />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonIn" runat="server" Text="CONTAINER IN" OnClick="ButtonIn_Click" TabIndex="3" />
            </td>
            <td>
                <asp:Button ID="btnQuit" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
</asp:Content>
