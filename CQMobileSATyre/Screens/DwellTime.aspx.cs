using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_DwellTime : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.MsgText = "Dwell Time";
    }
    protected void lbAccept_Click(object sender, EventArgs e)
    {
        try
        {
            DwellTime Dwell = new DwellTime();
            Dwell.DwellTimeInsert(Session["ConnectionStringName"].ToString(), int.Parse(DD_DwellReason.SelectedValue), int.Parse(Session["OperatorId"].ToString()));
            Response.Redirect(Session["ReturnURL"].ToString());    
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
}
