<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" EnableViewState="false" CodeFile="PickMixed.aspx.cs" Inherits="Screens_PickMixed" Title="Untitled Page" %>

<%@ Register Src="AlternatePickLocation.ascx" TagName="AlternatePickLocation" TagPrefix="uc7" %>
<%@ Register Src="GetMixedPickDetails.ascx" TagName="GetMixedPickDetails" TagPrefix="uc6" %>
<%@ Register Src="GetMixedPick.ascx" TagName="GetMixedPick" TagPrefix="uc5" %>
<%@ Register Src="ConfirmStoreLocation.ascx" TagName="ConfirmStoreLocation" TagPrefix="uc4" %>
<%@ Register Src="ConfirmPickLocation.ascx" TagName="ConfirmPickLocation" TagPrefix="uc1" %>
<%@ Register Src="ConfirmProduct.ascx" TagName="ConfirmProduct" TagPrefix="uc2" %>
<%@ Register Src="ConfirmQuantity.ascx" TagName="ConfirmQuantity" TagPrefix="uc3" %>
<%@ Register Src="ConfirmBatch.ascx" TagName="ConfirmBatch" TagPrefix="uc8" %>
<%@ Register Src="AlternatePickLocationPickface.ascx" TagName="AlternatePickLocationPickface" TagPrefix="uc9" %>
<%@ Register Src="StockTake.ascx" TagName="StockTake" TagPrefix="uc10" %>
<%@ Register Src="NewBox.ascx" TagName="NewBox" TagPrefix="uc11" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
<asp:DetailsView ID="DetailsViewInstruction" runat="server" DataKeyNames="ProductCode,Quantity,PickLocation,StoreLocation" DataSourceID="ObjectDataSourceInstruction" AutoGenerateRows="False">
        <Fields>
            <asp:TemplateField HeaderText="<%$ Resources:Default, NumberOfLines %>">
                <ItemTemplate>
                    <asp:Label ID="labelCurrentLine" runat="server" Text='<%# Bind("CurrentLine") %>'></asp:Label>
                    <asp:Label ID="labelOf" runat="server" Text="<%$ Resources:Default, Of %>"></asp:Label>
                    <asp:Label ID="labelTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="<%$ Resources:Default, ProductCode %>">
                <ItemTemplate>
                    <asp:Label ID="LabelProductCode" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                    <input id="ipProductCode" runat="server" type="hidden" name="ProductCode" value='<%# Bind("ProductValue") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
            <asp:TemplateField HeaderText="<%$ Resources:Default, SKU %>">
                <ItemTemplate>
                    <asp:Label ID="labelSKUCode" runat="server" Text='<%# Bind("SKUCode") %>'></asp:Label> -
                    <asp:Label ID="labelSKU" runat="server" Text='<%# Bind("SKU") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="<%$ Resources:Default, Batch %>">
                <ItemTemplate>
                    <asp:Label ID="LabelBatch" runat="server" Text='<%# Bind("Batch") %>'></asp:Label>
                    <input id="ipBatch" runat="server" disabled="disabled" type="hidden" name="Batch" value='<%# Bind("Batch") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="SOH" HeaderText="<%$ Resources:Default, SOH %>" />
            <asp:TemplateField HeaderText="<%$ Resources:Default, Quantity %>">
                <ItemTemplate>
                    <asp:Label ID="LabelQuantity" runat="server" Text='<%# Bind("DisplayQuantity") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="<%$ Resources:Default, PickLocation %>">
                <ItemTemplate>
                    <asp:Label ID="LabelLocation" runat="server" Text='<%# Bind("PickLocation") %>'></asp:Label>
                    <input id="ipPickSecurityCode" runat="server" disabled="disabled" type="hidden" name="PickSecurityCode" value='<%# Bind("PickSecurityCode") %>'/>
                    <input id="ipPickAisle" runat="server" disabled="disabled" type="hidden" name="PickAisle" value='<%# Bind("PickAisle") %>'/>
                    <input id="ipPickColumn" runat="server" disabled="disabled" type="hidden" name="PickColumn" value='<%# Bind("PickColumn") %>'/>
                    <input id="ipPickLevel" runat="server" disabled="disabled" type="hidden" name="PickLevel" value='<%# Bind("PickLevel") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="<%$ Resources:Default, StoreLocation %>">
                <ItemTemplate>
                    <asp:Label ID="LabelStoreLocation" runat="server" Text='<%# Bind("StoreLocation") %>'></asp:Label>
                    <input id="ipStoreSecurityCode" runat="server" disabled="disabled" type="hidden" name="StoreLocation" value='<%# Bind("StoreSecurityCode") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="<%$ Resources:Default, TargetFace %>">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Green Face.jpg" />
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="Transact"
        SelectMethod="GetInstructionDetails">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="instructionId" SessionField="InstructionId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <uc5:GetMixedPick ID="GetMixedPick1" runat="server" />
        </asp:View>
        <asp:View ID="View2" runat="server">
            <uc6:GetMixedPickDetails ID="GetMixedPickDetails1" runat="server" />
        </asp:View>
        <asp:View ID="View3" runat="server">
            <uc1:ConfirmPickLocation ID="ConfirmPickLocation1" runat="server" />
            <br />
            <uc11:NewBox ID="NewBox1" runat="server" />
        </asp:View>
        <asp:View ID="View4" runat="server">
            <uc7:AlternatePickLocation ID="AlternatePickLocation1" runat="server" />
        </asp:View>
        <asp:View ID="View5" runat="server">
            <uc2:ConfirmProduct ID="ConfirmProduct1" runat="server" />
            <uc9:AlternatePickLocationPickface ID="AlternatePickLocationPickface" runat="server" />
        </asp:View>
        <asp:View ID="View6" runat="server">
            <uc8:ConfirmBatch ID="ConfirmBatch1" runat="server" />
        </asp:View>
        <asp:View ID="View7" runat="server">
            <uc3:ConfirmQuantity ID="ConfirmQuantity1" runat="server" />
        </asp:View>
        <asp:View ID="View8" runat="server">
            <uc10:StockTake ID="StockTake1" runat="server" />
        </asp:View>
        <asp:View ID="View9" runat="server">
            <uc4:ConfirmStoreLocation ID="ConfirmStoreLocation1" runat="server" />
        </asp:View>
    </asp:MultiView>
</asp:Content>

