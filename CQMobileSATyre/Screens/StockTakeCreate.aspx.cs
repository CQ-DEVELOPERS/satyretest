using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_StockTakeCreate : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                if (Session["StorageUnitBatchId"] != null)
                    if (Session["StorageUnitBatchId"].ToString() == "-1")
                        Session["StorageUnitBatchId"] = null;

                if (Session["StorageUnitBatchId"] != null)
                {
                    if (Session["StorageUnitBatchId"].ToString() != "-1")
                        MultiView1.ActiveViewIndex = 2;
                }
                else
                {
                    if (Session["FromURL"] != null)
                    {
                        DetailsView1.DataBind();
                        Session["StorageUnitBatchId"] = DetailsView1.DataKey["StorageUnitBatchId"];

                        if (Session["StorageUnitBatchId"].ToString() != "-1")
                            MultiView1.ActiveViewIndex = 2;
                    }
                }

                if(DetailsView1.Rows.Count > 0)
                    if(DetailsView1.DataKey["LocationId"].ToString() == "-1")
                        Response.Redirect("~/Screens/StockTakeJob.aspx");
            }
        }
        catch { }
    }
    #endregion Page_Load

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
      Page.SetFocus(TextBoxConfirmLocation);
    }

    #region LinkButtonAccept_Click
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            int locationId = -1;
            
            if(DetailsView1.DataKey["LocationId"] != null)
                locationId = int.Parse(DetailsView1.DataKey["LocationId"].ToString());

            StockTake st = new StockTake();

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    if (st.ValidLocation(Session["ConnectionStringName"].ToString(), TextBoxConfirmLocation.Text))
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsView1.DataBind();
                        MultiView1.ActiveViewIndex++;
                    }
                    else
                    {
                        TextBoxConfirmLocation.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidLocation;
                    }
                    break;
                case 1:
                    if (st.EmptyLocationUpdate(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], locationId) == 0)
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.LocationError;
                    }

                    TextBoxConfirmLocation.Text = "";
                    MultiView1.ActiveViewIndex--;
                    break;
            }
        }
        catch { }
    }
    #endregion LinkButtonAccept_Click

    #region LinkButtonReject_Click
    protected void LinkButtonReject_Click(object sender, EventArgs e)
    {
        try
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    Response.Redirect("..");
                    break;
                case 1:
                    Reset();
                    Master.MsgText = Resources.Default.LocationScan;
                    break;
            }
        }
        catch { }
    }
    #endregion LinkButtonReject_Click

    #region Reset
    protected void Reset()
    {
        try
        {
            TextBoxConfirmLocation.Text = "";
            MultiView1.ActiveViewIndex = 0;
        }
        catch { }
    }
    #endregion Reset
}
