using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_PickfaceCheck : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                if (Session["StorageUnitBatchId"] != null || Session["Location"] != null || Session["URL"] != null)
                {
                    if (Session["StorageUnitBatchId"].ToString() != "-1")
                        MultiView1.ActiveViewIndex = 1;

                    if (Session["Location"] != null)
                        TextBoxConfirmLocation.Text = Session["Location"].ToString();

                    Session["URL"] = null;
                    Session["Location"] = null;

                    DetailsView1.DataBind();
                }
                else
                    Session["StorageUnitBatchId"] = "-1";
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region LinkButtonNext_Click
    protected void LinkButtonNext_Click(object sender, EventArgs e)
    {
        try
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    Response.Redirect("..");
                    break;
                case 1:
                    Reset();
                    Master.MsgText = Resources.Default.LocationScan;
                    break;
            }
        }
        catch { }
    }
    #endregion LinkButtonNext_Click

    #region LinkButtonAccept_Click
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            int locationId = -1;
            int storageUnitId = -1;

            if (DetailsView1.DataKey["LocationId"] != null)
                locationId = int.Parse(DetailsView1.DataKey["LocationId"].ToString());

            if (DetailsView1.DataKey["StorageUnitId"] != null)
                storageUnitId = int.Parse(DetailsView1.DataKey["StorageUnitId"].ToString());

            Housekeeping hk = new Housekeeping();
            Transact tr = new Transact();

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    if (tr.ValidLocation(Session["ConnectionStringName"].ToString(), TextBoxConfirmLocation.Text))
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsView1.DataBind();
                        MultiView1.ActiveViewIndex++;
                    }
                    else
                    {
                        TextBoxConfirmLocation.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidLocation;
                    }
                    break;
                case 1:
                    if (hk.PickfaceCheckAccept(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], storageUnitId, locationId) == 0)
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        Session["StorageUnitBatchId"] = -1;
                        DetailsView1.DataBind();
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.LocationError;
                    }

                    break;
            }
        }
        catch { }
    }
    #endregion LinkButtonAccept_Click

    #region LinkButtonReject_Click
    protected void LinkButtonReject_Click(object sender, EventArgs e)
    {
        try
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    Response.Redirect("..");
                    break;
                case 1:
                    int locationId = -1;
                    int storageUnitId = -1;

                    if (DetailsView1.DataKey["LocationId"] != null)
                        locationId = int.Parse(DetailsView1.DataKey["LocationId"].ToString());

                    if (DetailsView1.DataKey["StorageUnitId"] != null)
                        storageUnitId = int.Parse(DetailsView1.DataKey["StorageUnitId"].ToString());

                    Housekeeping hk = new Housekeeping();
                    Transact tr = new Transact();

                    if (hk.PickfaceCheckReject(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], storageUnitId, locationId) == 0)
                    {
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsView1.DataBind();
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.LocationError;
                    }
                    
                    Master.MsgText = Resources.Default.LocationScan;
                    break;
            }
        }
        catch { }
    }
    #endregion LinkButtonReject_Click

    #region LinkButtonSearch_Click
    protected void LinkButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Session["Location"] = TextBoxConfirmLocation.Text;
            Session["URL"] = "~/Screens/PickfaceCheck.aspx";
            Response.Redirect("~/Screens/ProductSearch.aspx");
        }
        catch { }
    }
    #endregion LinkButtonSearch_Click

    #region Reset
    protected void Reset()
    {
        try
        {
            TextBoxConfirmLocation.Text = "";
            MultiView1.ActiveViewIndex = 0;
        }
        catch { }
    }
    #endregion Reset
}
