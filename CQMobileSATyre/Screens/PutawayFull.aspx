<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PutawayFull.aspx.cs" Inherits="Screens_PutawayFull" Title="Untitled Page" %>

<%@ Register Src="AlternateStoreLocation.ascx" TagName="AlternateStoreLocation" TagPrefix="uc7" %>
<%@ Register Src="GetFullPutaway.ascx" TagName="GetFullPutaway" TagPrefix="uc5" %>
<%@ Register Src="ConfirmProduct.ascx" TagName="ConfirmProduct" TagPrefix="uc2" %>
<%@ Register Src="ConfirmStoreLocation.ascx" TagName="ConfirmStoreLocation" TagPrefix="uc4" %>
<%@ Register Src="ConfirmQuantity.ascx" TagName="ConfirmQuantity" TagPrefix="uc3" %>
<%@ Register Src="ConfirmBatch.ascx" TagName="ConfirmBatch" TagPrefix="uc8" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:DetailsView ID="DetailsViewInstruction" runat="server" DataKeyNames="ProductCode,Quantity,PickLocation,StoreLocation" DataSourceID="ObjectDataSourceInstruction" AutoGenerateRows="False">
        <Fields>
            <asp:TemplateField HeaderText="<%$ Resources:Default, NumberOfLines %>">
                <ItemTemplate>
                    <asp:Label ID="labelCurrentLine" runat="server" Text='<%# Bind("CurrentLine") %>'></asp:Label>
                    <asp:Label ID="labelOf" runat="server" Text="<%$ Resources:Default, Of %>"></asp:Label>
                    <asp:Label ID="labelTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>" />
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" />
            <asp:BoundField DataField="StoreArea" HeaderText="<%$ Resources:Default, Area %>" />
            <asp:BoundField DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>" />
            <asp:BoundField DataField="StoreLocation" HeaderText="<%$ Resources:Default, StoreLocation %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="Transact"
        SelectMethod="GetInstructionDetails">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="instructionId" SessionField="InstructionId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
        <asp:View ID="View0" runat="server">
            <uc5:GetFullPutaway ID="GetFullPutaway1" runat="server" />
        </asp:View>
        <asp:View ID="View1" runat="server">
            <uc2:ConfirmProduct ID="ConfirmProduct1" runat="server" />
        </asp:View>
        <asp:View ID="View2" runat="server">
            <uc8:ConfirmBatch ID="ConfirmBatch1" runat="server" />
        </asp:View>
        <asp:View ID="View3" runat="server">
            <uc4:ConfirmStoreLocation ID="ConfirmStoreLocation1" runat="server" />
        </asp:View>
        <asp:View ID="View4" runat="server">
            <uc7:AlternateStoreLocation ID="AlternateStoreLocation1" runat="server" />
        </asp:View>
        <asp:View ID="View5" runat="server">
            <uc3:ConfirmQuantity ID="ConfirmQuantity1" runat="server" />
        </asp:View>
    </asp:MultiView>
</asp:Content>

