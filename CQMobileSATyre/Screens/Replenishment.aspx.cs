using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_Replenishment : System.Web.UI.Page
{



    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion "InitializeCulture"

    #region PrivateVariables
        private int result = 0;
        private string strresult = "";
        private string theErrMethod = "";
    #endregion PrivateVariables

    #region Page_Load 
        public override String StyleSheetTheme
        {
            get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
        }


    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/Replenishment.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
                ClearAllForNewProduct();
            }

        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion Page_Load

    #region CommandNext_Click 
    protected void CommandNext_Click(object sender, EventArgs e)
    {
        try
        {
            ReplenishmentRequest_DataBind();

        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion CommandNext_Click

    #region ReplenishmentRequest_DataBind 
    protected void ReplenishmentRequest_DataBind()
    {
        string location = TextBoxLocation.Text;
        string barcode = TextBoxBarcode.Text;
        try
        {
            if (location == "")
                Master.MsgText = "Please scan a Pick Bin";

            ReplenishmentRequest request = new ReplenishmentRequest();
            DataSet ds = request.GetReplenishmentRequest(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], location, barcode);

            int result = int.Parse(ds.Tables[0].Rows[0].ItemArray.GetValue(ds.Tables[0].Columns["Result"].Ordinal).ToString());

            string message = ds.Tables[0].Rows[0]["Message"].ToString();

            Master.MsgText = message;

            if (result == 0)
            {
                LabelBarcode.Visible = false;
                TextBoxBarcode.Visible = false;
            }
            if (result == 4)
            {
                LabelBarcode.Visible = true;
                TextBoxBarcode.Visible = true;
            }
            else
            {
                if (Session["FromURL"] != null)
                    Response.Redirect(Session["FromURL"].ToString());
            }

            TextBoxLocation.Text = "";
            TextBoxBarcode.Text = "";

            Master.MsgText = message;
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion ReplenishmentRequest_DataBind

    #region ClearAllForNewProduct
    private void ClearAllForNewProduct()
    {
        theErrMethod = "ClearAllForNewProduct";
        try
        {
            Session["FromURL"] = null;
            //Clear Session Variables
            Session["StoreLocation"] = "";
            Session["PickSecurityCode"] = "";
            Session["ConfirmedQuantity"] = "";
            Session["Quantity"] = "";
            Session["Confirmed"] = "";
            Session["PickLocation"] = "";
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion ClearAllForNewProduct

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        int loopPrevention = -1;

        try
        {
            try
            {
                loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());
            }
            catch
            {
                Session["countLoopsToPreventInfinLoop"] = 0;
                loopPrevention = 0;
            }



            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                  Resources.Default.ReplenishmentTitle.ToString(),
                                                                  theErrMethod,
                                                                  ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {

                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
            }

        }
        catch (Exception exMsg)
        {

            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                 Resources.Default.ReplenishmentTitle.ToString(),
                                                                 theErrMethod,
                                                                 ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return strresult;
    }
    #endregion ErrorHandling
}
