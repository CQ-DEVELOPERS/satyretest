<%@ Page Language="C#"
    AutoEventWireup="true"
    CodeFile="MainMenuWT.aspx.cs"
    Inherits="Screens_MainMenuWT"
    StylesheetTheme="Default"
    Theme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Main Menu</title>
</head>
<script type="text/javascript">
    
    function fnClose()
        {
            window.close();
        }
        
    function fnOpen(page_name) {
        var sFeatures = "dialogHeight: 240px; dialogWidth: 320px; dialogTop: 0px; dialogLeft: 0px; edge:Raised; center:Yes; resizable:No; status:No;";

        window.showModalDialog(page_name, "", sFeatures);

        //window.open(page_name, "_blank", "height=420px width=350px top=0 left=0 resizable=no scrollbars=no ");

        //alert("Window is open in a new way, need to Test on the Hand helds");

        window.close();
    }

    function fnOpenMenu()
    {
        fnOpen('MainMenuWT.aspx');
    }
    
    function myKeyHandler() 
        {
          //alert(event.keyCode);
          
          if (event.keyCode != null)        
            {                                 
                if(event.keyCode == 48 || event.keyCode == 27) //0 or ESC
                {
                    window.close();
                } 
                
                if(event.keyCode == 49 || event.keyCode == 112) //1 or p
                {
                     fnOpen('PutawayFull.aspx');
                }
                
                if(event.keyCode == 50 || event.keyCode == 120) //2 or x
                {
                    fnOpen('PutawayMixed.aspx');
                }
                   
                if(event.keyCode == 51 || event.keyCode == 115) //3 or a
                {
                      fnOpen('StockTakeArea.aspx');
                }
                   
                if(event.keyCode == 52 || event.keyCode == 102) //4 or f
                {
                   fnOpen('PickFull.aspx');
                }
                      
                if(event.keyCode == 53 || event.keyCode == 109) //5 or m
                {
                   fnOpen('PickMixed.aspx');
                }
                      
                if(event.keyCode == 54 || event.keyCode == 97) //6 or a
                {
                   fnOpen('AutoMove.aspx');
                }    
                
                if(event.keyCode == 55 || event.keyCode == 114) //7 or r
                {
                   fnOpen('Replenishment.aspx');
                }
                 
                if(event.keyCode == 56 || event.keyCode == 99) //8 or c
                {
                   fnOpen('DespatchConfirm.aspx');
                }
                 
                if(event.keyCode == 56 || event.keyCode == 100) //8 or d
                {
                   fnOpen('DespatchMove.aspx');
                }
                
                if(event.keyCode == 57 || event.keyCode == 118) //9 or v
                {
                    fnOpen('VehicleConfirm.aspx');
                }
                
                if(event.keyCode == 119) //w
                {
                    fnOpen('StockWriteOff.aspx');
                }
                
                if(event.keyCode == 107) //k
                {
                    fnOpen('SkuCheck.aspx');
                }
                
                if(event.keyCode == 116) //t
                {
                    fnOpen('ChooseTheme.aspx');
                }
            }
        }
        
</script>
<body onkeypress="myKeyHandler()">
    <form id="form1" runat="server">
        <div>
            <div style="text-align:center; background-color:#485889;">
                <asp:Label ID="Msg" runat="server" SkinID="Header" Text="Main Menu" Width="100%"></asp:Label>
            </div>
            <asp:Menu ID="Menu1" runat="server" DataSourceID="xmlDataSource" DisappearAfter="500">
                <DataBindings>
                    <asp:MenuItemBinding DataMember="MenuItem" NavigateUrlField="NavigateUrl" TextField="MenuItemText"
                        ToolTipField="ToolTip" />
                </DataBindings>
            </asp:Menu>
            <asp:XmlDataSource ID="xmlDataSource" runat="server" TransformFile="MenuItem.xsl"
                XPath="MenuItems/MenuItem" />
            <div style="text-align:center;background-color:#848484; layout-flow:horizontal;">
                <%--<asp:Label ID="LabelCopyRight" runat="server" SkinID="Footer" Text="See Warehouse Run"></asp:Label>--%>
                <%--<asp:LoginName ID="LoginName1" runat="server" />&nbsp; &nbsp;<asp:LoginStatus ID="LoginStatus1" runat="server" />--%>
                <asp:Button ID="btnLogout" runat="server" Text="Logout" OnClick="btnLogout_Click"/>
            </div>
        </div>
    </form>
</body>
</html>
