<%@ Page Language="C#"
    MasterPageFile="~/Master.master" 
    AutoEventWireup="true" 
    CodeFile="ProductSearch.aspx.cs" 
    Inherits="Screens_ProductSearch" 
    Title="<%$ Resources:Default, ProductSearchTitle %>"
    StylesheetTheme="Default"
    Theme="Default"
%>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table style="font-size:xx-small" >
    <tr>
        <td>
            <asp:Label ID="LabelBarcode" SkinID="Small" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxBarcode" SkinID="Small" runat="server" TabIndex="1"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelProductCode" SkinID="Small" runat="server" Text="<%$ Resources:Default, ProductCode %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxProductCode" SkinID="Small" runat="server" TabIndex="1"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelProduct" SkinID="Small" runat="server" Text="<%$ Resources:Default, Product %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxProduct" SkinID="Small" runat="server" TabIndex="2"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelSKUCode" SkinID="Small" runat="server" Text="<%$ Resources:Default, SKUCode %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxSKUCode" SkinID="Small" runat="server" TabIndex="3"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelBatch" SkinID="Small" runat="server" Text="<%$ Resources:Default, Batch %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxBatch" SkinID="Small" runat="server" TabIndex="4" Text="Default"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:Button ID="ButtonSearch" runat="server" TabIndex="5" Text="<%$ Resources:Default, Search %>" OnClick="ButtonSearch_Click" />
            <asp:Button ID="LinkButtonCancel" runat="server" TabIndex="6" Text="<%$ Resources:Default, Cancel %>" OnClick="ButtonCancel_Click" />
        </td>
    </tr>
</table>

<asp:GridView ID="GridViewProductSearch"
    runat="server"
    AllowPaging="True"
    PageSize="5"
    AutoGenerateColumns="False"
    DataSourceID="ObjectDataSourceProduct"
    DataKeyNames="StorageUnitBatchId,Product"
    OnSelectedIndexChanged="GridViewProductSearch_OnSelectedIndexChanged">
    <Columns>
        <asp:CommandField ButtonType="Link" SelectText="<%$ Resources:Default, OK2 %>" ShowSelectButton="true" />
        <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, Code %>" />
        <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKU %>" />
        <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>">
            <ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
    </Columns>
    <EmptyDataTemplate>
        <h5>No rows</h5>
    </EmptyDataTemplate>
</asp:GridView>

<asp:ObjectDataSource ID="ObjectDataSourceProduct" runat="server" TypeName="Transact" SelectMethod="SearchProducts">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:ControlParameter Name="productCode" ControlID="TextBoxProductCode" Type="String" />
        <asp:ControlParameter Name="product" ControlID="TextBoxProduct" Type="String" />
        <asp:ControlParameter Name="skuCode" ControlID="TextBoxSKUCode" Type="String" />
        <asp:ControlParameter Name="batch" ControlID="TextBoxBatch" Type="String" />
        <asp:ControlParameter Name="barcode" ControlID="TextBoxBarcode" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
</asp:Content>

