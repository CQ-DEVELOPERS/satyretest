using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_GetFullPutaway : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //lbAccept.Attributes.Add("onclick", "this.disabled= 'disabled' ");

            TextBoxReferenceNumber.Focus();
        }
    }
    protected void lbAccept_Click(object sender, EventArgs e)
    {
        try
        {
            int palletId = -1;

            int.TryParse(TextBoxReferenceNumber.Text, out palletId);

            if (palletId <= 0)
                if (TextBoxReferenceNumber.Text.StartsWith("P:") || TextBoxReferenceNumber.Text.StartsWith("J:"))
                    int.TryParse(TextBoxReferenceNumber.Text.Substring(2), out palletId);

            if (palletId > 0)
            {
                Transact tran = new Transact();
                int instructionId = tran.GetPutawayFull(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], palletId);

                switch (instructionId)
                {
                    case -1:
                        Session["Message"] = Resources.ResMessages.InvalidPalletId;
                        Session["Error"] = true;
                        TextBoxReferenceNumber.Text = "";
                        break;
                    case -2:
                        Session["Message"] = Resources.ResMessages.StoreLocationError;
                        Session["Error"] = true;
                        TextBoxReferenceNumber.Text = "";
                        break;
                    case -3:
                        Session["Message"] = Resources.ResMessages.AnotherPutawayAlreadyStarted;
                        Session["Error"] = true;
                        TextBoxReferenceNumber.Text = "";
                        break;
                    case -4:
                        Session["Message"] = Resources.ResMessages.PalletNotApproved;
                        Session["Error"] = true;
                        TextBoxReferenceNumber.Text = "";
                        break;
                    default:
                        Session["ReferenceNumber"] = TextBoxReferenceNumber.Text;
                        Session["InstructionId"] = instructionId;
                        Session["Message"] = Resources.ResMessages.Successful;
                        Session["Error"] = false;
                        TextBoxReferenceNumber.Text = "";
                        Session["CurrentView"] = "GetFullPutaway";
                        break;
                }
            }
            else
            {
                Session["Message"] = Resources.ResMessages.InvalidPalletId;
                Session["Error"] = true;
                TextBoxReferenceNumber.Text = "";
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }

        //Response.Redirect("~/Screens/PickMixed.aspx");
    }
}
