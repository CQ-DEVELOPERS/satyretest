<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GetMixedPickDetails.ascx.cs" Inherits="Screens_GetMixedPickDetails" %>

<asp:DetailsView ID="DetailsViewMixedDetails" runat="server" DataSourceID="ObjectDataSourceMixedDetails" DataKeyNames="JobId" AutoGenerateRows="False">
    <Fields>
        <asp:TemplateField HeaderText="<%$ Resources:Default, ContainerType %>">
            <ItemTemplate>
                <asp:Label ID="LabelContainerType" runat="server" Text='<%# Bind("ContainerType") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="JobId" HeaderText="<%$ Resources:Default, JobId %>" />
        <asp:BoundField DataField="Route" HeaderText="<%$ Resources:Default, Route %>" />
        <asp:BoundField DataField="Load" HeaderText="<%$ Resources:Default, Load %>" />
        <asp:BoundField DataField="DespatchDate" HeaderText="<%$ Resources:Default, DespatchDate %>" />
        <asp:BoundField DataField="OutboundDocumentType" HeaderText="<%$ Resources:Default, OutboundDocumentType %>" />
        <asp:BoundField DataField="Area" HeaderText="<%$ Resources:Default, Area %>" />
        <asp:BoundField DataField="Weight" HeaderText="<%$ Resources:Default, Weight %>" />
        <asp:BoundField DataField="Items" HeaderText="<%$ Resources:Default, Items %>" />
    </Fields>
</asp:DetailsView>
<asp:ObjectDataSource ID="ObjectDataSourceMixedDetails" runat="server" TypeName="Transact"
    SelectMethod="GetMixedDetails">
    <SelectParameters>
        <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
        <asp:SessionParameter Name="instructionId" SessionField="InstructionId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<br />
<asp:Label ID="LabelTareWeight" runat="server" Text="<%$ Resources:Default, TareWeight2 %>" Visible="false"></asp:Label>
<br />
<asp:TextBox ID="TextBoxTareWeight" runat="server" TabIndex="1" Visible="false"></asp:TextBox>
<br />
<asp:Button ID="lbAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
