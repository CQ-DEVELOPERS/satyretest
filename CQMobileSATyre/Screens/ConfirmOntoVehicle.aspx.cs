using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ConfirmOntoVehicle : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }

           

        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region PrivateVariables
    private int result = 0;
    private string strresult = "";
    private string theErrMethod = "";
    private int? ses = null;
 
    #endregion PrivateVariables

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["Url"] = "ConfirmOntoVehicle.aspx";
        Master.MsgText = Resources.Default.ConfirmOntoVehicleTitlte.ToString();       

        try
        {
            if (!Page.IsPostBack)
            {
                //int dwelltime = 0;
                //string constring = Session["ConnectionStringName"].ToString();
                //int operatorID = int.Parse(Session["OperatorId"].ToString());

                //Transact tran = new Transact();
                //dwelltime = tran.DwellTime(constring, 0, operatorID);

                //if (Session["DwellTime"] == null)
                //{
                //    if (dwelltime == 0) // This should be 1
                //    {
                //        Session["DwellTime"] = 0;
                //        Session["Url"] = "ConfirmOntoVehicle.aspx";
                //        Server.Transfer("CopyDwellTime.aspx?DwellTime");
                //    }
                //}
                //else
                //{
                //    if (dwelltime == 1) // This should be 1
                //    {
                //        Session["DwellTime"] = 0;
                //    }
                //}
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion Page Load

    #region CommandAccept_Click
    protected void CommandAccept_Click(object sender, EventArgs e)
    {
        try
        {

            Button btn = new Button();
            btn = (Button)sender;
           
            int loadOrder = int.Parse(TextBoxLoadOrder.Text);
            string palletCheckId = "", totalLines = "";
            string vehicleID = TextBoxVehicleId.Text;
            
            ArrayList arrlist = new ArrayList();

            /*************************************************/

            string Constr = Session["ConnectionStringName"].ToString();// = "Connection String"; 

            // string connectionStringName = Session["ConnectionStringName"].ToString();
            int operatorId = -1;

            if (!int.TryParse(Session["OperatorId"].ToString(), out operatorId))
            {
                Master.MsgText = Resources.ResMessages.OperatorInvalid.ToString();
                return;
            }

            //loadOrder = CleanloadOrder("Load");
            if (!int.TryParse(TextBoxLoadOrder.Text, out loadOrder))
            {
                Master.MsgText = Resources.ResMessages.LoadOrderInvalidField.ToString();
                return;
            }
                     

            if (TextBoxPalletId.Text == "")
            {

                Master.MsgText = Resources.ResMessages.NotValidID.ToString();
                lbReset_Click(sender, e);
                return;
            }

                DBErrorMessage dberr = new DBErrorMessage();

                /**************************************************/

                Despatch desConfirmOntoVehicle = new Despatch();

                arrlist = desConfirmOntoVehicle.ConfirmVehicle(Constr, TextBoxLoadOrder.Text, TextBoxVehicleId.Text, TextBoxPalletId.Text);

                if (arrlist != null)
                {
                    result = int.Parse(arrlist[0].ToString());

                    palletCheckId = arrlist[1].ToString();

                    totalLines = arrlist[2].ToString();
                }

                if (result != 0)
                {
                    Master.MsgText = dberr.ReturnErrorString(result);

                    lbNext.Visible = false;

                    btn.ID = "lbResetPallet";

                    lbReset_Click(sender, e);

                    return;

                }
                else
                {
                    if (palletCheckId.ToString() == "1")
                    {
                        //Next = Accept    
                        CommandAccept.Text = "Accept";
                        CommandAccept.Visible = false;  

                        lbCompleteAll.Visible = true;

                    }
                    else
                    {
                        //Accept = Next 
                        CommandAccept.Text = "Next";

                        Master.MsgText = "Line: " + palletCheckId + " of " + totalLines;

                        btn.ID = "lbResetPallet";

                        lbReset_Click(sender, e);
                    }

                }
                             

            //}
            //else
            //{

                
                //bool lastLine = GetLastLine(); //int number of lines left
                //if it is the last line finish up else make visible next and get next info
                //bool lastLine = GetLastLine(); //int number of lines left


                //Session["TotalLines"] = "10";
                //Session["CurrentLine"] = "3";

                //for (int i = 0; i <= lastLine; i++)
                //{ 
                // Clean Pallet TextBox
                //TextBoxPalletId.Text = "";
                //TextBoxPalletId.TabIndex = 0;
                //TextBoxPalletId.Focus();

                // Rename the label
                // lbLineCount.Text = "Line " + "3 " + " 10";
               
                //i--;
                //}l

                //GetlastLine
                //if (TextBox1.Text == "1")
                //    lastLine = true;

                //if (lastLine)
                //    lbCompleteAll.Visible = true;
                //else
                //    lbCompleteAll.Visible = false;


                /**************************************************/

                //Get the information from the DB

                /**************************************************/
                //Master.MsgText = "Lines " + palletCheckId + " Of" + totalLines;                              

                //if (loadOrder != 0)
                //{
                //    Master.MsgText = Resources.ResMessages.LoadOrderInvalidField.ToString();
                   
                //    lb.ID = "lbResetloadOrder";
                //    lbReset_Click(sender, e);
                //    return;
                  
                //}
                //else
                //{
                //    if (instructId != 0)
                //    {
                //        Master.MsgText = Resources.ResMessages.LoadOrderInvalidField.ToString();

                //        TextBoxLoadOrder.Text = "";
                //        TextBoxLoadOrder.TabIndex = 1;
                //        TextBoxLoadOrder.Focus();

                //    }
                //    else if (instructId == 0)
                //    {
                //        TextBoxPalletId.TabIndex = 1;
                //        TextBoxPalletId.Focus();
                //    }
                //    else
                //    {
                //        MultiView1.ActiveViewIndex = MultiView1.ActiveViewIndex + 1;
                //    }
                //}

                dberr = null;
              
            //}
        }
        catch (Exception ex)
        { 
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }


    }
    #endregion CommandAccept_Click

    #region Reset
    protected void lbReset_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lbButton = (LinkButton)sender;

            switch (lbButton.ID)
            {
               
                case "lbResetPallet":

                    TextBoxPalletId.TabIndex = 1;
                    CommandAccept.TabIndex = 2;
                    lbResetloadOrder.TabIndex = 3;
                    //hlQuit.TabIndex = 4;
                    lbCompleteAll.TabIndex = 5;

                    TextBoxPalletId.Text = "";
                    //TextBoxVehicleId.Text = "";
                    //TextBoxPalletId.Text = "";

                    TextBoxPalletId.Focus();
                    break;



                default:
                    break;


            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

       
    }
    #endregion Reset

    #region NextPallet
    protected void lbNextLoad_Click(object sender, EventArgs e)
    {
        try
        {
            CleanPage();
            MultiView1.ActiveViewIndex = 0;
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion NextPallet   

    #region Clean Page
    protected void CleanPage()
    {
        try
        {
            TextBoxLoadOrder.Text = "";
            TextBoxVehicleId.Text = "";
            TextBoxPalletId.Text = "";
           // TextBox1.Text = "";
            CommandAccept.Visible = true;  
            //Panel1.Visible = false;
            lbCompleteAll.Visible = false;
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
       

    }
    #endregion Clean Page

    #region Clean Pallet ID
    protected int CleanloadOrder(string LoadORPallet)
    {   
        
        int iloadOrder = 0;
        string strloadOrder = TextBoxLoadOrder.Text;
        
        try
        {
         

            switch (LoadORPallet)
            {
                case "Load":

                    if (TextBoxLoadOrder.Text.StartsWith("L:"))
                    {

                        // Remove first 2 char.
                        StringReader strReader = new StringReader(strloadOrder.Remove(0, 2));
                        strloadOrder = strReader.ReadLine();
                        Session["loadOrder"] = TextBoxLoadOrder.Text;
                        iloadOrder = int.Parse(strloadOrder);


                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.LoadOrderInvalidField.ToString();

                        Session["loadOrder"] = iloadOrder.ToString();

                        TextBoxLoadOrder.Text = "";
                        TextBoxLoadOrder.TabIndex = 1;
                        TextBoxLoadOrder.Focus();

                        iloadOrder = 0;
                    }
                    break;
                case "Pallet":


                    if (TextBoxPalletId.Text.StartsWith("P:"))
                    {
                        // Remove first 2 char.
                        StringReader strReader = new StringReader(strloadOrder.Remove(0, 2));
                        strloadOrder = strReader.ReadLine();
                        Session["loadOrder"] = TextBoxPalletId.Text;
                        iloadOrder = int.Parse(strloadOrder);

                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidPalletId.ToString();

                        Session["loadOrder"] = iloadOrder.ToString();

                        TextBoxPalletId.Text = "";
                        TextBoxPalletId.TabIndex = 1;
                        TextBoxPalletId.Focus();
                        iloadOrder = 0;

                    }
                    break;
                default:
                    break;



            }

          
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

        return iloadOrder;
    }     
    #endregion Clean Pallet ID

    #region Get Last Line
    protected bool GetLastLine()
    {


        bool blastLine = false;
        //Stan s� dit is blind so ons hoef nie hierdie in te sit nie.
        //How many lines are left
        //Despatch desConfirmOntoVehicle = new Despatch();
        //int result = desConfirmOntoVehicle.ConfirmVehicle(Constr, TextBoxLoadOrder.Text, TextBoxVehicleId.Text, TextBoxPalletId.Text);


        try
        {
            //if (TextBox1.Text == "1")
            //    blastLine = true;         
         
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

        return blastLine;
    }
    #endregion Get Last Line

    #region Complete All
    protected void lbCompleteAll_Click(object sender, EventArgs e)
    {
        try
        {
            //Pick and Bin
            MultiView1.ActiveViewIndex = MultiView1.ActiveViewIndex + 1;

            
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
       
    }
    #endregion Complete All

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        int loopPrevention = -1;

        try
        {
            try
            {
                loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());
            }
            catch 
            {
                Session["countLoopsToPreventInfinLoop"] = 0;
                loopPrevention = 0;
            }



            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                  Resources.Default.ConfirmOntoVehicleTitlte.ToString(),
                                                                  theErrMethod,
                                                                  ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {
             
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
            }

        }
        catch (Exception exMsg)
        {          

             if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                 Resources.Default.ConfirmOntoVehicleTitlte.ToString(),
                                                                 theErrMethod,
                                                                 ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }           
            else
            {                
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return strresult;
    }
    #endregion ErrorHandling

    protected void lbNext_Click(object sender, EventArgs e)
    {

        LinkButton lb = (LinkButton)sender;
      
        //Get Last Line if lastline then Text = 1
        
        if (TextBoxPalletId.Text == "1")
        {
            if (lbCompleteAll.Visible == true)
            {
                MultiView1.ActiveViewIndex = MultiView1.ActiveViewIndex + 1;
            }
            else
            {
                lbCompleteAll.Visible = true;
                lbNext.Visible = false;
                //CommandAccept.Visible = true;                
            }
        }          
        else
        {
            //Get last info
            //bool lastLine = GetLastLine();
            lb.ID = "lbResetPallet";
            lbReset_Click(sender, e);


            lbCompleteAll.Visible = false;
            lbNext.Visible = true;
            CommandAccept.Visible = false;


        }
        
    }
}
