using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_BlindReceiving : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/BlindReceiving.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
                    
                ResetSessionValues();
                if (int.Parse(Session["ActiveViewIndex"].ToString()) > 0)
                {

                    MultiViewConfirm.ActiveViewIndex = int.Parse(Session["ActiveViewIndex"].ToString());
                    Session["ActiveViewIndex"] = 0;
                }
            }
            else
            {
                
            }

        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void ResetSessionValues()
    {
        // Reset all the Session Values
    }

    protected void LinkButtonView1_Click(object sender, EventArgs e)
    {
        Int32 receiptId = -1;
        Receiving r = new Receiving();

        Session["OrderNumber"] = TextBoxOrderNumber.Text;
        Session["ParameterOrderNumber"] = TextBoxOrderNumber.Text;
        

        receiptId = r.ConfirmOrder(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], Session["OrderNumber"].ToString());
        Session["receiptId"] = receiptId;
        if (Session["OrderNumber"].ToString() == "" || receiptId < 0)
        {
            Master.MsgText = Resources.ResMessages.InvalidOrderNumber.ToString();
            TextBoxOrderNumber.TabIndex = 1;
            TextBoxOrderNumber.Focus();
            return;
        }

        Session["ReceiptId"] = receiptId;
        

        Master.MsgText = Resources.ResMessages.Successful.ToString();
        LabelOrderNumber2.Text = Session["OrderNumber"].ToString();
        MultiViewConfirm.ActiveViewIndex++;
        Session["Sequence"] = 0;
        Session["QuestionaireId"] = 5;
        Session["QuestionaireType"] = "Supplier Load Conformance";
        Session["ActiveViewIndex"] = MultiViewConfirm.ActiveViewIndex;
        Session["ReturnURL"] = "~/Screens/BlindReceiving.aspx";
        //Response.Redirect("~/Screens/Question.aspx");
    }

    protected void LinkButtonView2_Click(object sender, EventArgs e)
    {
        if (Session["TotalPallets"] == null)
            Session["TotalPallets"] = 1;
        else
            Session["TotalPallets"] = (int)Session["TotalPallets"] + 1;

        LabelTotalPallets1.Text = Session["TotalPallets"].ToString();
        LabelTotalPallets2.Text = Session["TotalPallets"].ToString();

        Session["InstructionTypeCode"] = RadioButtonListPalletType.SelectedValue.ToString();
        Session["JobId"] = null;

        if (Session["InstructionTypeCode"].ToString() == "SM")
        {
            Session["CurrentProduct"] = 0;
            MultiViewConfirm.ActiveViewIndex++;
        }
        else
        {
            Session["CurrentProduct"] = 1;
            Session["TotalProducts"] = 1;
            LabelProductNumber.Text = "1 of 1";
            MultiViewConfirm.ActiveViewIndex++;
            MultiViewConfirm.ActiveViewIndex++;
        }
    }

    protected void LinkButtonView3_Click(object sender, EventArgs e)
    {
        if (Session["CurrentProduct"] == null)
            Session["CurrentProduct"] = 0;

        Session["CurrentProduct"] = (int)Session["CurrentProduct"] + 1;
        Session["TotalProducts"] = int.Parse(TextBoxNumberOfProducts.Text);

        TextBoxNumberOfProducts.Text = "";

        LabelProductNumber.Text = Session["CurrentProduct"].ToString() + " of " + Session["TotalProducts"].ToString();
        MultiViewConfirm.ActiveViewIndex++;
    }

    protected void LinkButtonView4_Click(object sender, EventArgs e)
    {
        Int32 storageUnitId = -1;
        Int32 storageUnitBatchId = -1;
        Int32 jobId = -1;
        Decimal quantity = -1;

        Receiving r = new Receiving();

        if (Session["JobId"] != null)
            jobId = (int)Session["JobId"];

        storageUnitId = r.ConfirmStorageUnit(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], TextBoxBarcode.Text);

       

        if (Session["OrderNumber"].ToString() == "" || storageUnitId < 0)
        {
            Master.MsgText = Resources.ResMessages.InvalidProduct.ToString();
            TextBoxBarcode.TabIndex = 1;
            TextBoxBarcode.Focus();
            return;
        }

        Session["StorageUnitId"] = storageUnitId;

        storageUnitBatchId = r.CreateBatch(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], storageUnitId, TextBoxBatch.Text, TextBoxReferenceNumber.Text, TextBoxExpiryDate.Text);

        if (storageUnitBatchId < 0)
        {
            Master.MsgText = Resources.ResMessages.InvalidOrderNumber.ToString();
            TextBoxOrderNumber.TabIndex = 1;
            TextBoxOrderNumber.Focus();
            return;
        }

        if (Decimal.TryParse(TextBoxQuantity.Text, out quantity) == false)
        {
            Master.MsgText = Resources.ResMessages.QuantityReEnter.ToString();
            TextBoxQuantity.TabIndex = 1;
            TextBoxQuantity.Focus();
            return;
        }

        jobId = r.CreatePallet(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], jobId, storageUnitId, storageUnitBatchId, quantity, Session["InstructionTypeCode"].ToString());
        Session["jobId"] = jobId;
        
        if ((int)Session["CurrentProduct"] == (int)Session["TotalProducts"])
        {
            MultiViewConfirm.ActiveViewIndex--;
            MultiViewConfirm.ActiveViewIndex--;

            Session["Sequence"] = 0;
            Session["QuestionaireId"] = 6;
            Session["QuestionaireType"] = "Supplier Pallet";
            Session["ActiveViewIndex"] = MultiViewConfirm.ActiveViewIndex;
            Session["ReturnURL"] = "~/Screens/BlindReceiving.aspx";
            //Response.Redirect("~/Screens/Question.aspx");

        }

        TextBoxBarcode.Text = "";
        TextBoxBatch.Text = "";
        TextBoxReferenceNumber.Text = "";
        TextBoxExpiryDate.Text = "";
        TextBoxQuantity.Text = "";
        
        Session["CurrentProduct"] = (int)Session["CurrentProduct"] + 1;
        LabelProductNumber.Text = Session["CurrentProduct"].ToString() + " of " + Session["TotalProducts"].ToString();
    }
    

    protected void LinkButtonComplete_Click(object sender, EventArgs e)
    {
        TextBoxOrderNumber.Text = "";
        Session["ParameterOrderNumber"] = null;
        TextBoxVehicleRegistration.Text = "";
        TextBoxDeliveryNoteNumber.Text = "";
        TextBoxDeliveryDate.Text = "";
        TextBoxSealNumber.Text = "";
        Session["TotalPallets"] = null;

        LabelTotalPallets1.Text = "0";
        LabelTotalPallets2.Text = "0";

        MultiViewConfirm.ActiveViewIndex--;
        MultiViewConfirm.ActiveViewIndex--;
        MultiViewConfirm.ActiveViewIndex--;
    }
}
