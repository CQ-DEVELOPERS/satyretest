<%@ Page Language="C#"
    MasterPageFile="~/Master.master"  
    AutoEventWireup="true" 
    CodeFile="CreatePallet.aspx.cs" 
    Inherits="Screens_CreatePallet" 
    Title="<%$ Resources:Default, CopyDwellTimeTitle %>"
    StylesheetTheme="Default"
    Theme="Default"
%>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="Form1" runat="server">
            <table width="100%">
                <tr>
                    <td align="left">
                        <asp:Label ID="LabelPalletId" runat="server" Font-Bold="True" Font-Size="XX-Small">PalletId:</asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBoxPalletId" runat="server" Font-Size="X-Small" TabIndex="1"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="LabelBarcode" runat="server" Font-Bold="True" Font-Size="XX-Small">Barcode:</asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBoxBarcode" runat="server" Font-Size="X-Small"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="LabelBatch" runat="server" Font-Bold="True" Font-Size="XX-Small">Batch:</asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBoxBatch" runat="server" Font-Size="X-Small"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="LabelQuantity" runat="server" Font-Bold="True" Font-Size="XX-Small">Quantity:</asp:Label></td>
                    <td>
                        <asp:TextBox ID="TextBoxQuantity" runat="server" Font-Size="X-Small" TabIndex="2"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left">
                    </td>
                    <td>
                        <asp:Button ID="CommandNext" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            TabIndex="3" Text="<%$ Resources:Default, Next %>" OnClick="CommandNext_Click"></asp:Button>
                        <asp:Button ID="CommandCancel" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            TabIndex="4" Text="<%$ Resources:Default, Cancel %>" ></asp:Button>
                        <asp:HyperLink ID="lbQuitLoc" runat="server" Font-Bold="True" ForeColor="#3D4782"
                            NavigateUrl="~/Screens/MainMenu.aspx" TabIndex="4">| Quit|</asp:HyperLink></td>
                </tr>
            </table>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        </asp:View>
        <asp:View ID="Form2" runat="server">
            <asp:Label ID="LabelErrorMsg" runat="server" ForeColor="#ff0000"></asp:Label>&nbsp;
            <asp:Button ID="CommandTryAgain" runat="server" Font-Bold="True" ForeColor="#3D4782"
                TabIndex="1" Text="<%$ Resources:Default, TryAgain %>" ></asp:Button></asp:View>
    </asp:MultiView>
</asp:Content>