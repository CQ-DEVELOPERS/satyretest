using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_StockWriteOff : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/StockWriteOff.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        Transact tran = new Transact();

        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                if (tran.ValidLocation(Session["ConnectionStringName"].ToString(), TextBoxPickLocation.Text))
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                }
                else
                {
                    TextBoxPickLocation.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidLocation;
                }

                break;
            case 1:
                if (TextBoxPalletId.Text.StartsWith("P:"))
                    if (tran.ValidProduct(Session["ConnectionStringName"].ToString(), TextBoxPalletId.Text) == false)
                    {
                        TextBoxPalletId.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidPalletId;
                        return;
                    }

                DetailsViewStock.DataBind();

                if (DetailsViewStock.Rows.Count < 1)
                {
                    TextBoxPalletId.Text = "";
                    Master.MsgText = Resources.ResMessages.InvalidProduct;
                }
                else
                {
                    MultiView1.ActiveViewIndex++;
                    Master.MsgText = Resources.ResMessages.Successful;
                }

                break;
            case 2:
                Decimal quantity = -1;

                if (Decimal.TryParse(TextBoxQuantity.Text, out quantity))
                {
                    int storageUnitBatchId = (int)DetailsViewStock.DataKey["StorageUnitBatchId"];
                    int instructionId = tran.AutoMove(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], storageUnitBatchId, TextBoxPickLocation.Text, "-1", quantity, "O");

                    if (instructionId < 0)
                    {
                        Master.MsgText = Resources.ResMessages.Failed;
                        return;
                    }
                    else
                    {
                        Session["InstructionId"] = instructionId;

                        DetailsViewInstruction.DataBind();

                        TextBoxPalletId.Text = "";
                        TextBoxPickLocation.Text = "";
                        TextBoxQuantity.Text = "";
                        MultiView1.ActiveViewIndex++;
                    }
                }
                else
                {
                    TextBoxQuantity.Text = "";
                    Master.MsgText = Resources.ResMessages.QuantityReEnter;
                }
                break;
            case 3:
                if (Session["ReasonId"] == null)
                {
                    Master.MsgText = Resources.ResMessages.ReasonSelect;
                    return;
                }

                if (tran.ReasonUpdate(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], (int)Session["ReasonId"]))
                {
                    if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 332))
                    {
                        int jobId = tran.AdhocStockTake(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]);

                        if (jobId > 0)
                        {
                            Session["JobId"] = jobId;
                            Session["FromURL"] = "~/Screens/StockWriteOff.aspx";
                            Response.Redirect("~/Screens/StockTakeCount.aspx", false);
                        }

                        MultiView1.ActiveViewIndex = 0;
                    }
                    MultiView1.ActiveViewIndex = 0;
                }
                else
                {
                    Master.MsgText = Resources.ResMessages.ReasonSelect;
                }
                break;
        }
    }

    #region GridViewReason_OnSelectedIndexChanged
    protected void GridViewReason_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["ReasonId"] = GridViewReason.SelectedDataKey["ReasonId"];
        }
        catch { }
    }
    #endregion GridViewReason_OnSelectedIndexChanged
}
