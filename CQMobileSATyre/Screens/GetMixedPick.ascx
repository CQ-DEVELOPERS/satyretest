<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GetMixedPick.ascx.cs" Inherits="Screens_GetMixedPick" %>

<asp:Label ID="lblNewPallet" runat="server" Text="<%$ Resources:Default, NewPallet %>"></asp:Label>
<br />
<asp:TextBox ID="TextBoxReferenceNumber" runat="server" TabIndex="1" CssClass="txtbx"></asp:TextBox>
<br />
<asp:Button ID="lbAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>