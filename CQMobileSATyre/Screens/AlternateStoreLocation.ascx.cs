using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_AlternateStoreLocation : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Page.IsPostBack)
        //{
        //    lbStoreLocation.Attributes.Add("onclick", "this.disabled= 'disabled' ");
        //    lbAlternate.Attributes.Add("onclick", "this.disabled= 'disabled' ");
        //}

        if (Session["StoreLocation"] != null)
            TextBoxConfirmStoreLocation.Text = Session["StoreLocation"].ToString();

    }

    protected void lbStoreLocation_Click(object sender, EventArgs e)
    {
        Session["Error"] = false;
        Session["CurrentView"] = "Alternate";
    }
    protected void lbAlternate_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();

            if (tran.LocationErrorStore(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
            {
                Session["Type"] = "LocationError";
                Session["Error"] = false;
                Session["CurrentView"] = "LocationError";
            }
            else
            {
                Session["Message"] = Resources.ResMessages.InvalidLocation;
                Session["Error"] = true;
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
    protected void lbManual_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();

            if (tran.ManualErrorStore(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
            {
                Session["Type"] = "LocationError";
                Session["Error"] = false;
                Session["CurrentView"] = "LocationError";
            }
            else
            {
                Session["Message"] = Resources.ResMessages.InvalidLocation;
                Session["Error"] = true;
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
}
