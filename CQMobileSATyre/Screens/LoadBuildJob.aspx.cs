using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_LoadBuildJob : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                //Session["IntransitLoadId"] = null;

                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/LoadBuildJob.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        Page.SetFocus(TextBoxBarcode);
    }

    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            IntransitLoad trans = new IntransitLoad();
            int intransitLoadJobId = 0;

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    intransitLoadJobId = trans.AddJob(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], (int)Session["IntransitLoadId"], TextBoxBarcode.Text);

                    if (intransitLoadJobId > 0)
                    {
                        Session["IntransitLoadJobId"] = intransitLoadJobId;
                        //MultiView1.ActiveViewIndex++;
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsViewDocument.DataBind();
                    }
                    else
                    {
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidBarcode;
                    }

                    break;
            }
        }
        catch { }
    }

    protected void LinkButtonRemove_Click(object sender, EventArgs e)
    {
        try
        {
            IntransitLoad trans = new IntransitLoad();
            int intransitLoadJobId = 0;

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    intransitLoadJobId = trans.RemoveJob(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], (int)Session["IntransitLoadId"], TextBoxBarcode.Text);

                    if (intransitLoadJobId > 0)
                    {
                        Session["IntransitLoadJobId"] = intransitLoadJobId;
                        //MultiView1.ActiveViewIndex++;
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsViewDocument.DataBind();
                    }
                    else
                    {
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidBarcode;
                    }

                    break;
            }
        }
        catch { }
    }

    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            default:
                break;
        }
    }

    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                Response.Redirect("~/Screens/LoadBuild.aspx");
                break;
            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }

    protected void LinkButtonSkip_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 5:

                Reset();
                break;

            default:
                MultiView1.ActiveViewIndex++;
                break;
        }
    }

    protected void Reset()
    {
        MultiView1.ActiveViewIndex = 0;
        Session["intransitLoadJobIdPass"] = Session["IntransitLoadJobId"];
        Session["IntransitLoadId"] = null;
        Master.MsgText = Resources.ResMessages.Successful;
        DetailsViewProduct.DataBind();
        TextBoxBarcode.Text = "";
    }
    protected void ButtonFinished_Click(object sender, EventArgs e)
    {
        IntransitLoad trans = new IntransitLoad();
        int intransitLoadJobId = 0;



        if (trans.FinishLoad(Session["ConnectionStringName"].ToString(), (int)Session["IntransitLoadId"]))
        {
            Session["IntransitLoadJobId"] = intransitLoadJobId;
            //MultiView1.ActiveViewIndex++;
            TextBoxBarcode.Text = "";
            Master.MsgText = Resources.ResMessages.Successful;
            DetailsViewDocument.DataBind();
        }
        else
        {
            TextBoxBarcode.Text = "";
            Master.MsgText = Resources.ResMessages.InvalidBarcode;
        }
    }
}
