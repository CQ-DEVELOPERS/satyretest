using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_Question : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            DataSet ds = null;
            Receiving r = new Receiving();
            if ((Session["QuestionaireId"] == null)||(Session["QuestionaireType"] == null))
            {
                Master.MsgText = "No Questionaire set";
                Session["QuestionaireId"] = 1;
                Session["QuestionaireType"] = "Delivery";


            }
            if (Session["Sequence"] == null)
            {
                Session["Sequence"] = 0;
            }
            
            ds = r.GetQuestion(Session["ConnectionStringName"].ToString(), 
                                int.Parse(Session["QuestionaireId"].ToString()),
                                Session["QuestionaireType"].ToString(),
                                int.Parse(Session["Sequence"].ToString()),
                                int.Parse(Session["WarehouseId"].ToString()));
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                Session["Sequence"] = dr.ItemArray.GetValue(3).ToString();
                Session["questionId"] = dr.ItemArray.GetValue(0).ToString();

                if (dr.ItemArray.GetValue(5).ToString() == "YesNo")
                {
                    QuestionText.Text = dr.ItemArray.GetValue(4).ToString();
                    MultiViewConfirm.ActiveViewIndex = 0;
                }
                else
                {
                    QuestionTextBox.Text = dr.ItemArray.GetValue(4).ToString();
                    MultiViewConfirm.ActiveViewIndex = 1;
                }
            }
            else
            {
                if (Session["ReturnURL"] == null)
                    Session["ReturnURL"] = "~/Screens/MainMenu.aspx";
                
                Response.Redirect(Session["ReturnURL"].ToString());   
            }
            
        }
    }

    protected void LinkButtonView1_Click(object sender, EventArgs e)
    {
        string QuestionType = "";
        DataSet ds = null;
        Receiving r = new Receiving();

        int jobId = -1;
        int palletId = -1;
        int questionId = -1;
        int receiptId = -1;
        string OrderNumber = " ";

        if (Session["ParameterOrderNumber"] != null)
        {
            OrderNumber = Session["ParameterOrderNumber"].ToString();
        }

        if (Session["jobId"] != null)
        {
            jobId = int.Parse(Session["jobId"].ToString());
        }

        if (Session["palletId"] != null)
        {
            palletId = int.Parse(Session["palletId"].ToString());
        }

        if (Session["ReceiptId"] != null)
        {
            receiptId = int.Parse(Session["ReceiptId"].ToString());
        }
        if (Session["questionId"] != null)
        {
            questionId = int.Parse(Session["QuestionId"].ToString());
        }

        r.InsertQuestion(Session["ConnectionStringName"].ToString(),                              
                            OrderNumber,
                            jobId,
                            palletId, receiptId, questionId,
                            QuestionValueRadio.Text.ToString()); 


        //ds = r.GetQuestion(Session["ConnectionStringName"].ToString(),int.Parse(Session["QuestionaireId"].ToString()), int.Parse(Session["Sequence"].ToString()));
        ds = r.GetQuestion(Session["ConnectionStringName"].ToString(),
                             int.Parse(Session["QuestionaireId"].ToString()),
                             Session["QuestionaireType"].ToString(),
                             int.Parse(Session["Sequence"].ToString()),
                             int.Parse(Session["WarehouseId"].ToString()));

        if (ds.Tables[0].Rows.Count == 0)
        {
            Response.Redirect(Session["ReturnURL"].ToString());   
        }
        else
        {
            DataRow dr = ds.Tables[0].Rows[0];
            Session["Sequence"] = dr.ItemArray.GetValue(3).ToString();
            QuestionType = dr.ItemArray.GetValue(5).ToString();
            Session["questionId"] = dr.ItemArray.GetValue(0).ToString();
            if (QuestionType == "YesNo")
            {
                QuestionText.Text = dr.ItemArray.GetValue(4).ToString();
                MultiViewConfirm.ActiveViewIndex=0;

            }

            if (QuestionType == "TextBox")
            {
                QuestionTextBox.Text = dr.ItemArray.GetValue(4).ToString();
                MultiViewConfirm.ActiveViewIndex=1;
            }
        }

    }

    protected void LinkButtonView2_Click(object sender, EventArgs e)
    {

        string QuestionType = "";
        DataSet ds = null;
        Receiving r = new Receiving();
        int jobId = -1;
        int palletId = -1;
        int questionId = -1;
        int receiptId = -1;
        string OrderNumber = "";

        if (Session["ParameterOrderNumber"] != null)
        {
            OrderNumber = Session["ParameterOrderNumber"].ToString();
        }

        if (Session["jobId"] != null)
        {          
            jobId = int.Parse(Session["jobId"].ToString());
        }
        
        if (Session["palletId"] != null)
        {         
            palletId = int.Parse(Session["palletId"].ToString());
        }
        
        if (Session["ReceiptId"] != null)
        {
            receiptId = int.Parse(Session["ReceiptId"].ToString());
        }
        if (Session["QuestionId"] != null)
        {
            questionId = int.Parse(Session["QuestionId"].ToString());
        }

        r.InsertQuestion(Session["ConnectionStringName"].ToString(),
                            OrderNumber,
                            jobId,
                           palletId, receiptId,questionId,
                           QuestionValueTextBox.Text.ToString()); 


        //ds = r.GetQuestion(Session["ConnectionStringName"].ToString(), int.Parse(Session["QuestionaireId"].ToString()), int.Parse(Session["Sequence"].ToString()));
        ds = r.GetQuestion(Session["ConnectionStringName"].ToString(),
                              int.Parse(Session["QuestionaireId"].ToString()),
                              Session["QuestionaireType"].ToString(),
                              int.Parse(Session["Sequence"].ToString()),
                              int.Parse(Session["WarehouseId"].ToString()));
        if (ds.Tables[0].Rows.Count == 0)
        {
            if (Session["ReturnURL"] == null)
            {

                Session["ReturnURL"] = "~/Screens/MainMenu.aspx";
            };

            Response.Redirect(Session["ReturnURL"].ToString());   
        }
        else
        {
            DataRow dr = ds.Tables[0].Rows[0];
            Session["Sequence"] = dr.ItemArray.GetValue(3).ToString();

            QuestionType = dr.ItemArray.GetValue(5).ToString();
            Session["questionId"] = dr.ItemArray.GetValue(0).ToString();
            if (QuestionType == "YesNo")
            {
                QuestionText.Text = dr.ItemArray.GetValue(4).ToString();
                MultiViewConfirm.ActiveViewIndex=0;
            }

            if (QuestionType == "TextBox")
            {
                QuestionTextBox.Text = dr.ItemArray.GetValue(4).ToString();
                MultiViewConfirm.ActiveViewIndex=1;
            }
        }

    }
}
