<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="BoxLink.aspx.cs" Inherits="Screens_BoxLink" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelReferenceNumber" runat="server" Text="<%$ Resources:Default, ReferenceNumber %>"></asp:Label>
            <asp:TextBox ID="TextBoxReferenceNumber" runat="server" TabIndex="1"></asp:TextBox>
            <asp:Button ID="ButtonAccept1" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="ButtonAccept_Click"></asp:Button>
            <asp:Button ID="ButtonQuit1" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="4"></asp:TextBox>
            <asp:Button ID="ButtonAdd2" runat="server" TabIndex="5" Text="<%$ Resources:Default, Add %>" OnClick="ButtonAdd_Click"></asp:Button>
            <asp:Button ID="ButtonRemove1" runat="server" TabIndex="6" Text="<%$ Resources:Default, Remove %>" OnClick="ButtonRemove_Click"></asp:Button>
            <asp:Button ID="ButtonFinish2" runat="server" TabIndex="7" Text="<%$ Resources:Default, Finish %>" OnClick="ButtonFinish_Click"></asp:Button>
            <asp:Button ID="ButtonQuit2" runat="server" TabIndex="8" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
    </asp:MultiView>
    <br />
</asp:Content>

