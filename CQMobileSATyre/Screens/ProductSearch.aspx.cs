using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ProductSearch : System.Web.UI.Page
{

    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion "InitializeCulture"

    #region PrivateVariables
    private int result = 0;
    private string strresult = "";
    private string theErrMethod = "";
    private int? ses = null;
    #endregion PrivateVariables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Master.MsgText = "PRODUCT SEARCH";

        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

    }
    #endregion Page_Load

    #region GridViewProductSearch_OnSelectedIndexChanged
    protected void GridViewProductSearch_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["StorageUnitBatchId"] = GridViewProductSearch.SelectedDataKey["StorageUnitBatchId"];
            Session["Batch"] = GridViewProductSearch.SelectedRow.Cells[4].Text;

            if(Session["URL"] != null)
                Response.Redirect(Session["URL"].ToString());
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

    }
    #endregion GridViewProductSearch_OnSelectedIndexChanged

    #region ButtonSearch_Click
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewProductSearch.PageIndex = 0;

            GridViewProductSearch.DataBind();
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion "ButtonSearch_Click"

    #region ButtonCancel_Click
    protected void ButtonCancel_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["URL"] != null)
                Response.Redirect(Session["URL"].ToString());
            else
                Response.Redirect("~/Screens/MainMenu.aspx".ToString());
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion "ButtonCancel_Click"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {
        int loopPrevention = -1;

        try
        {
            try
            {
                loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());
            }
            catch
            {
                Session["countLoopsToPreventInfinLoop"] = 0;
                loopPrevention = 0;
            }



            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                  Resources.Default.ProductSearchTitle.ToString(),
                                                                  theErrMethod,
                                                                  ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {

                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
            }

        }
        catch (Exception exMsg)
        {

            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                 Resources.Default.ProductSearchTitle.ToString(),
                                                                 theErrMethod,
                                                                 ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return strresult;
    }
    #endregion ErrorHandling
}
