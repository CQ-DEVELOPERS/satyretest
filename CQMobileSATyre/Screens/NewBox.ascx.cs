using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_AlternatePickLocationPickface : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //lbAlternate.Attributes.Add("onclick", "this.disabled= 'disabled' ");
        }
    }

    protected void ButtonNewBox_Click(object sender, EventArgs e)
    {
        try
        {
            ProductCheck chk = new ProductCheck();
            Transact tran = new Transact();

            if (TextBoxNewReferenceNumber.Text.ToUpper().StartsWith("R:"))
            {
                int jobId = 0;

                jobId = chk.NewBoxInstruction(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], (int)Session["OperatorId"], TextBoxNewReferenceNumber.Text);

                if (jobId > 0)
                {
                    int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], TextBoxNewReferenceNumber.Text, -1);

                    if (instructionId != -1)
                    {
                        Session["ReferenceNumber"] = TextBoxNewReferenceNumber.Text;
                        Session["InstructionId"] = instructionId;
                        Session["Message"] = Resources.ResMessages.Successful;
                        Session["Error"] = false;
                        Session["CurrentView"] = "GetMixedPick";
                        TextBoxNewReferenceNumber.Text = "";
                    }
                    else
                    {
                        Session["Message"] = Resources.ResMessages.InvalidPalletId;
                        Session["Error"] = true;
                        TextBoxNewReferenceNumber.Text = "";
                    }
                }
                else
                {
                    Session["Message"] = Resources.ResMessages.InvalidBarcode;
                    Session["Error"] = true;
                    TextBoxNewReferenceNumber.Text = "";
                }
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
}
