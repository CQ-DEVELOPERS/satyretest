<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PickfaceCheck.aspx.cs" Inherits="Screens_PickfaceCheck" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="LabelConfirmLocation" runat="server" Text="<%$ Resources:Default, Location %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxConfirmLocation" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
            <asp:Button ID="btnQuit" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="LocationId,StorageUnitId,StorageUnitBatchId" DataSourceID="ObjectDataSourceStock" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="Status" HeaderText="<%$ Resources:Default, Status %>" />
                    <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
                    <asp:BoundField DataField="ActualQuantity" HeaderText="<%$ Resources:Default, ActualQuantity %>" />
                    <asp:BoundField DataField="AllocatedQuantity" HeaderText="<%$ Resources:Default, AllocatedQuantity %>" />
                    <asp:BoundField DataField="ReservedQuantity" HeaderText="<%$ Resources:Default, ReservedQuantity %>" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="ObjectDataSourceStock" runat="server" TypeName="Housekeeping"
                SelectMethod="PickfaceCheckSelect">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:ControlParameter Name="location" Type="String" ControlID="TextBoxConfirmLocation" />
                    <asp:SessionParameter Name="storageUnitBatchId" SessionField="StorageUnitBatchId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Button ID="LinkButtonAccept2" runat="server" TabIndex="1" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
            <asp:Button ID="LinkButtonReject2" runat="server" TabIndex="2" Text="<%$ Resources:Default, Reject %>" OnClick="LinkButtonReject_Click"></asp:Button>
            <asp:Button ID="LinkButtonSearch2" runat="server" TabIndex="3" Text="<%$ Resources:Default, Search %>" OnClick="LinkButtonSearch_Click"></asp:Button>
            <br />
            <asp:Button ID="LinkButtonNext2" runat="server" TabIndex="4" Text="<%$ Resources:Default, NextLocation %>" OnClick="LinkButtonNext_Click"></asp:Button>
            <asp:Button ID="Button1" runat="server" TabIndex="5" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
    </asp:MultiView>
</asp:Content>

