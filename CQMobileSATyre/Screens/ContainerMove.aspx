<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ContainerMove.aspx.cs" Inherits="Screens_ContainerMove" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsViewDocument" runat="server" DataKeyNames="ContainerHeaderId,ContainerDetailId" DataSourceID="ObjectDataSourceDocument" OnPageIndexChanged="DetailsViewDocument_PageIndexChanged"
        AutoGenerateRows="False" PageIndex="0" AllowPaging="true" PagerStyle-Font-Size="Large" PagerStyle-HorizontalAlign="Center" PagerStyle-Font-Bold="true" AutoGenerateEditButton="true">
        <Fields>
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" ReadOnly="true" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" ReadOnly="true" />
            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" ReadOnly="true" />
            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" ReadOnly="true" />
            <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" ReadOnly="true" />
            <%--<asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" ReadOnly="true" DataFormatString="{0:G0}" ApplyFormatInEditMode="true" />--%>
            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Default, Quantity %>" DataFormatString="{0:G0}" ApplyFormatInEditMode="true" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceDocument" runat="server" TypeName="Container"
        SelectMethod="GetItems" UpdateMethod="SetItems">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="String" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
            <asp:ControlParameter Name="barcode" ControlID="TextBoxBarcode" DbType="String" />
            <asp:ControlParameter Name="locationBarcode" ControlID="TextBoxPickLocation" DbType="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="String" />
            <asp:Parameter Name="ContainerHeaderId" DbType="Int32" />
            <asp:Parameter Name="ContainerDetailId" DbType="Int32" />
            <asp:Parameter Name="Quantity" DbType="Decimal" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View0" runat="server">
            <br />
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, Barcode %>"></asp:Label>
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelPickLocation" runat="server" Text="<%$ Resources:Default, PickLocation %>"></asp:Label>
            <asp:TextBox ID="TextBoxPickLocation" runat="server" TabIndex="2"></asp:TextBox>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <asp:Label ID="LabelMessage" runat="server" Text="Confirm All Quantities to move"></asp:Label>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <br />
            <asp:Label ID="LabelStoreLocation" runat="server" Text="<%$ Resources:Default, StoreLocation %>"></asp:Label>
            <asp:TextBox ID="TextBoxStoreLocation" runat="server" TabIndex="3"></asp:TextBox>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <br />
            <asp:GridView ID="GridViewSOH" runat="server" DataKeyNames="ContainerHeaderId,ContainerDetailId" DataSourceID="ObjectDataSourceSOH"
                AutoGenerateColumns="False" PageIndex="0" AllowPaging="true" PagerStyle-Font-Size="Large" PagerStyle-HorizontalAlign="Center" PagerStyle-Font-Bold="true">
                <Columns>
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" ReadOnly="true" />
                    <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" ReadOnly="true" />
                    <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" ReadOnly="true" />
                    <asp:BoundField DataField="SKU" HeaderText="<%$ Resources:Default, SKU %>" ReadOnly="true" />
                    <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" ReadOnly="true" />
                    <asp:BoundField DataField="Location" HeaderText="<%$ Resources:Default, Location %>" ReadOnly="true" />
                    <asp:BoundField DataField="ActualQuantity" HeaderText="Update Quantity" DataFormatString="{0:G0}" ApplyFormatInEditMode="true" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceSOH" runat="server" TypeName="Container"
                SelectMethod="GetSOH">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
                    <asp:SessionParameter Name="OperatorId" SessionField="OperatorId" Type="Int32" />
                    <asp:SessionParameter Name="ContainerHeaderId" SessionField="ContainerHeaderId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
    </asp:MultiView>
    <br />
    <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="9" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
    <asp:Button ID="LinkButtonBack" runat="server" TabIndex="10" Text="<%$ Resources:Default, Back %>" OnClick="LinkButtonBack_Click" Visible="false"></asp:Button>
    <asp:Button ID="btnQuit" runat="server" TabIndex="11" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

