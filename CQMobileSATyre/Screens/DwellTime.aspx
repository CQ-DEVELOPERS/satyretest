<%@ Page MasterPageFile="~/Master.master" Language="C#" AutoEventWireup="true" CodeFile="DwellTime.aspx.cs" Inherits="Screens_DwellTime" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />
<asp:Label ID="lbl_DwellMessage" runat="server" Text="<%$ Resources:Default, DwellTimeMessage %>"></asp:Label>
<br />

<asp:Label ID="lblNewPallet" runat="server" Text="<%$ Resources:Default, DwellTime %>" Visible="true"></asp:Label>

<asp:Table ID="TableDwell" runat="server">
    <asp:TableRow><asp:TableCell HorizontalAlign="Left"> 
        <asp:RadioButtonList ID="DD_DwellReason" runat="server" DataSourceID="DwellReason" DataTextField="Reason" DataValueField="ReasonId" TabIndex="1"></asp:RadioButtonList>
    </asp:TableCell></asp:TableRow>
</asp:Table>

<br />
<asp:Button ID="lbAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>


<!-- //Object DataSource for Dropdown List -->
<asp:ObjectDataSource runat="server" SelectMethod="DwellTimeReasons" ID="DwellReason" TypeName="DwellTime">
    <SelectParameters>
    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

</asp:Content>