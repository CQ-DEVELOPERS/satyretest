using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_PickFull : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected bool changeactiveindex(int configurationId)
    {
        bool checkme = false;

        if (configurationId == 0)
            checkme = true;

        if (configurationId != 0)
        {
            checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], configurationId);        

            string[,] active = Session["ActiveTabIndexValue"] as string[,];

            for (int i = 0; i < active.GetLength(0); i++)
            {
                if (active[i, 0] == configurationId.ToString())
                {
                    checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], int.Parse(Session["ConfiguarationId"].ToString()));

                    if (!checkme)
                    {
                            Session["ConfiguarationId"] = active[i + 1, 0].ToString();
                            Session["Type"] = active[i + 1, 1].ToString();
                            Session["CurrentView"] = active[i + 1, 1].ToString();
                            Session["ActiveViewIndex"] = active[i + 1, 2].ToString();
                            Master.MsgText = active[i + 1, 3].ToString();

                            checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], int.Parse(Session["ConfiguarationId"].ToString()));
                            configurationId = int.Parse(Session["ConfiguarationId"].ToString());
                        break;
                    }
                    else
                    {
                        Session["ConfiguarationId"] = active[i, 0].ToString();
                        Session["Type"] = active[i, 1].ToString();
                        Session["CurrentView"] = active[i, 1].ToString();
                        Session["ActiveViewIndex"] = active[i, 2].ToString();
                        Master.MsgText = active[i, 3].ToString();

                        checkme = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], int.Parse(Session["ConfiguarationId"].ToString()));
                    }
                    break;
                }
            }
        }
        return checkme;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (Session["Error"] != null)
            {
                if ((bool)Session["Error"])
                {
                    if (Session["Message"] != null)
                        Master.MsgText = Session["Message"].ToString();
                }
                else
                {
                    if (Session["Type"] != null)
                    {
                        Session["Error"] = null;
                        Master.MsgText = Session["Message"].ToString();
 
                        if (Session["CurrentView"] == Session["Type"])
                        {
                            switch (Session["Type"].ToString())
                            {
                                case "GetFullPick":     //Index 0
                                    DetailsViewInstruction.Visible = true;
                                    Session["ConfiguarationId"] = 86;
                                    break;
                                case "PickLocation":   //Index 1
                                    Session["ConfiguarationId"] = 87;
                                    break;
                                case "Alternate":     //Index 2
                                    Session["ConfiguarationId"] = 88;
                                    break;
                                case "LocationError":     //No Index 
                                    //Master.MsgText = Resources.ResMessages.ConfirmAfterLocationError;
                                    //int instructionId = (int)Session["InstructionId"];
                                    //ResetSessionValues();
                                    //Session["InstructionId"] = instructionId;
                                    //DetailsViewInstruction.DataBind();
                                    //MultiViewConfirm.ActiveViewIndex = 0;
                                    //Session["Type"] = "GetFullPick";
                                    //Session["ActiveViewIndex"] = 0;
                                    //Session["Message"] = Resources.ResMessages.Successful;
                                    //Session["ConfiguarationId"] = 86;
                                    ResetSessionValues();
                                    MultiViewConfirm.ActiveViewIndex = 0;
                                    Session["ActiveViewIndex"] = 0;
                                    Session["Message"] = Resources.ResMessages.Successful;
                                    break;
                                case "Product":   //Index 3
                                    Session["ConfiguarationId"] = 89;
									if (Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), -1, (int)Session["StorageUnitId"]) == LotAttributeRule.None)
                                        goto case "ConfirmBatch";
                                    break;
                                case "ConfirmBatch":   //Index 3
                                    Session["ConfiguarationId"] = 90;
                                    break;
                                case "Quantity":     //Index 4
                                    Session["ConfiguarationId"] = 92;
                                    DetailsViewInstruction.DataBind();
                                    break;
                                case "StoreLocation":   //Index 5
                                    Transact tran = new Transact();

                                    if (tran.Pick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0 && tran.Store(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
                                    {
                                        ResetSessionValues();
                                        MultiViewConfirm.ActiveViewIndex = 0;
                                        Session["ActiveViewIndex"] = 0;
                                        Session["Message"] = Resources.ResMessages.Successful;
                                    }
                                    else
                                    {
                                        Session["Message"] = Resources.ResMessages.Failed;
                                        Session["Error"] = true;
                                    }
                                    Session["ConfiguarationId"] = 0;
                                    break;
                                default:
                                    Session["Message"] = Resources.ResMessages.Failed;
                                    Session["Error"] = true;
                                    break;
                            }

                            bool retval = false;
                            
                            while (!retval)
                            {
                                retval = changeactiveindex(int.Parse(Session["ConfiguarationId"].ToString()));
                            }
                        }
                    }
                }
            }

            if (Session["ActiveViewIndex"] != null)
                MultiViewConfirm.ActiveViewIndex = int.Parse(Session["ActiveViewIndex"].ToString());
               
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    #region DetailsViewInstruction_DataBound
    protected void DetailsViewInstruction_DataBound(object sender, EventArgs e)
    {
        if (DetailsViewInstruction.Rows.Count > 0)
        {
            if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 312))
                DetailsViewInstruction.Rows[3].Visible = false;
            if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 313))
                DetailsViewInstruction.Rows[4].Visible = false;
            if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 314))
                DetailsViewInstruction.Rows[8].Visible = false;
        }

        if (DetailsViewInstruction.Rows.Count > 0 && Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 315))
        {
            DetailsViewInstruction.Rows[8].Visible = true;

            DataSet GetmyFace = new DataSet();

            GetmyFace = Face.GetFace(Session["ConnectionStringName"].ToString(), DateTime.Now.ToString(), (int)Session["OperatorId"], (int)Session["OperatorGroupId"]);
            //GetmyFace = Face.GetFace(Session["ConnectionStringName"].ToString(), "11/06/2008 10:00:00.000", 30, 6);

            if (GetmyFace.Tables.Count > 0)
            {
                if (GetmyFace.Tables[0].Rows.Count > 0)
                {
                    if (GetmyFace.Tables[0].Rows[0]["FaceOption"].ToString() == "G")
                        ((Image)DetailsViewInstruction.FindControl("Image1")).ImageUrl = "~/Images/Green Face.jpg";
                    
                    if (GetmyFace.Tables[0].Rows[0]["FaceOption"].ToString() == "Y")
                        ((Image)DetailsViewInstruction.FindControl("Image1")).ImageUrl = "~/Images/Yellow Face.jpg";
                    
                    if (GetmyFace.Tables[0].Rows[0]["FaceOption"].ToString() == "R")
                        ((Image)DetailsViewInstruction.FindControl("Image1")).ImageUrl = "~/Images/Red Face.jpg";
                }
                
                if (GetmyFace.Tables[0].Rows.Count == 0)
                {
                    ((Image)DetailsViewInstruction.FindControl("Image1")).ImageUrl = "~/Images/Red Face.jpg";
                }
            }
        }
    }
    #endregion DetailsViewInstruction_DataBound

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                string[,] activeTabIndexValue = new string[,] {   { "0", "GetFullPick", "0", Resources.ResMessages.Successful },
                                                                { "86", "PickLocation", "1", Resources.ResMessages.ConfirmPickLocation },
                                                                { "87", "Alternate", "2", Resources.ResMessages.ConfirmAlternatePickLocation },
                                                                { "88", "Product", "3", Resources.ResMessages.ConfirmProduct },
                                                                { "89", "ConfirmBatch", "4", Resources.ResMessages.ConfirmBatch },
                                                                { "90", "Quantity", "5", Resources.ResMessages.ConfirmQuantity },
                                                                { "92", "StoreLocation", "6", Resources.ResMessages.ConfirmStoreLocation } };

                Session["ActiveTabIndexValue"] = activeTabIndexValue;

                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/PickFull.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                ResetSessionValues();
            }

            if (Session["FromURL"] == null)
                Session["FromURL"] = "~/Screens/PickFull.aspx";

            if (Session["Type"] == null)
            {
                Session["Type"] = "GetFullPick";
                DetailsViewInstruction.Visible = false;
            }
            else
            {
                if (DetailsViewInstruction.Rows.Count > 0)
                {
                    Session["ProductCode"] = DetailsViewInstruction.DataKey["ProductCode"].ToString();
                    Session["StoreLocation"] = DetailsViewInstruction.DataKey["StoreLocation"].ToString();
                    Session["PickLocation"] = DetailsViewInstruction.DataKey["PickLocation"].ToString();
                    Session["Quantity"] = Decimal.Parse(DetailsViewInstruction.DataKey["Quantity"].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void ResetSessionValues()
    {
        // Reset all the Session Values
        Session["Type"] = null;
        Session["ActiveViewIndex"] = null;
        Session["InstructionId"] = null;
        Session["ProductCode"] = null;
        Session["StoreLocation"] = null;
        Session["PickLocation"] = null;
        Session["Quantity"] = null;
        Session["Error"] = null;
        Session["ConfiguarationId"] = 0;
        //Session["Message"] = null;
    }
}
