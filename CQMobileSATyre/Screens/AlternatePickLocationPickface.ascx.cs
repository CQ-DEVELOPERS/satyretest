using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_AlternatePickLocationPickface : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //lbAlternate.Attributes.Add("onclick", "this.disabled= 'disabled' ");
        }
    }
    protected void lbPickLocation_Click(object sender, EventArgs e)
    {
        Session["Error"] = false;
        Session["CurrentView"] = "Alternate";
    }
    protected void lbAlternate_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();

            if (tran.LocationErrorPick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
            {
                int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), -1);

                if (instructionId != -1)
                {
                    Session["InstructionId"] = instructionId;
                    Session["Message"] = Resources.ResMessages.Successful;
                    Session["Error"] = false;
                    Session["Type"] = "PickLocation";
                    Session["ActiveViewIndex"] = 2;
                }
                else
                {
                    Session["Message"] = Resources.ResMessages.ConfirmStoreLocation;
                    Session["Type"] = "StoreLocation";
                    Session["ActiveViewIndex"] = 7;
                    Session["ConfiguarationId"] = 100;
                }

                //Session["Type"] = "LocationError";
                //Session["Error"] = false;
                //Session["CurrentView"] = "LocationError";
            }
            else
            {
                Session["Message"] = Resources.ResMessages.InvalidLocation;
                Session["Error"] = true;
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
}
