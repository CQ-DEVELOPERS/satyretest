<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="StockTakeAuthorise.aspx.cs" Inherits="Housekeeping_StockTakeAuthorise" Title="Stock Take Authorise" StylesheetTheme="Default" Theme="Default" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DropDownList ID="DropDownListArea" runat="server" DataSourceID="ObjectDataSourceArea"
        DataTextField="Area" DataValueField="AreaId" AutoPostBack="true">
    </asp:DropDownList>
    <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" TypeName="StockTakeAuthorise"
        SelectMethod="GetAuthoriseAreas">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName"
                Type="String" />
            <asp:SessionParameter Name="WarehouseId" SessionField="WarehouseId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Button ID="ButtomRefresh" runat="server" Text="Refresh" OnClick="ButtomRefresh_Click" />
    <asp:DetailsView ID="GridViewAuthorise" runat="server" DataSourceID="ObjectDataSourceAuthorise" DataKeyNames="InstructionId" AutoGenerateRows="false" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="JobId" HeaderText='JobId' SortExpression="JobId"><ItemStyle Wrap="False"></ItemStyle></asp:BoundField>
            <asp:BoundField DataField="LineNumber" HeaderText='Line Number' SortExpression="LineNumber"><ItemStyle Wrap="false"></ItemStyle></asp:BoundField>
            <asp:BoundField DataField="Location" HeaderText='<%$ Resources:Default,Location %>' SortExpression="Location"><ItemStyle Wrap="False"></ItemStyle></asp:BoundField>
            <asp:BoundField DataField="ProductCode" HeaderText='<%$ Resources:Default,ProductCode %>' SortExpression="ProductCode"><ItemStyle Wrap="False"></ItemStyle></asp:BoundField>
            <asp:BoundField DataField="Product" HeaderText='<%$ Resources:Default,Product %>' SortExpression="Product"><ItemStyle Wrap="False"></ItemStyle></asp:BoundField>
            <asp:BoundField DataField="SKUCode" HeaderText='<%$ Resources:Default,SKUCode %>' SortExpression="SKUCode"><ItemStyle Wrap="False"></ItemStyle></asp:BoundField>
            <asp:BoundField DataField="Batch" HeaderText='<%$ Resources:Default,Batch %>' SortExpression="Batch"><ItemStyle Wrap="False"></ItemStyle></asp:BoundField>
            <asp:BoundField DataField="ExpectedQuantity" HeaderText='Expected Quantity' SortExpression="ExpectedQuantity"><ItemStyle Wrap="False"></ItemStyle></asp:BoundField>
            <asp:BoundField DataField="PreviousCount2" HeaderText='Previous Count 2' SortExpression="PreviousCount2"><ItemStyle Wrap="False"></ItemStyle></asp:BoundField>
            <asp:BoundField DataField="PreviousCount1" HeaderText='Previous Count 1' SortExpression="PreviousCount1"><ItemStyle Wrap="False"></ItemStyle></asp:BoundField>
            <asp:BoundField DataField="LatestCount" HeaderText='Latest Count' SortExpression="LatestCount"><ItemStyle Wrap="False"></ItemStyle></asp:BoundField>
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceAuthorise" runat="server" TypeName="StockTakeAuthorise" SelectMethod="ViewAuthoriseDetails">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="warehouseId" SessionField="WarehouseId" Type="String" />
            <asp:ControlParameter ControlID="DropDownListArea" DefaultValue="" Name="AreaId" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:Parameter Name="InstructionId" Type="int32" />
            <asp:Parameter Name="Status" Type="string" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:Button ID="ButtonAccept" runat="server" Text="Accept" OnClick="ButtomAccept_Click" />
    <asp:Button ID="ButtonRecount" runat="server" Text="Recount" OnClick="ButtonRecount_Click" />
    <asp:Button ID="ButtonQuit" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

