<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PutawayMixed.aspx.cs" Inherits="Screens_PutawayMixed" Title="Untitled Page" %>

<%@ Register Src="GetMixedPickDetails.ascx" TagName="GetMixedPickDetails" TagPrefix="uc6" %>
<%@ Register Src="AlternateStoreLocation.ascx" TagName="AlternateStoreLocation" TagPrefix="uc7" %>
<%@ Register Src="GetMixedPutaway.ascx" TagName="GetMixedPutaway" TagPrefix="uc5" %>
<%@ Register Src="ConfirmStoreLocation.ascx" TagName="ConfirmStoreLocation" TagPrefix="uc4" %>
<%@ Register Src="ConfirmProduct.ascx" TagName="ConfirmProduct" TagPrefix="uc2" %>
<%@ Register Src="ConfirmQuantity.ascx" TagName="ConfirmQuantity" TagPrefix="uc3" %>
<%@ Register Src="ConfirmBatch.ascx" TagName="ConfirmBatch" TagPrefix="uc8" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsViewInstruction" runat="server" DataKeyNames="ProductCode,Quantity,PickLocation,StoreLocation" DataSourceID="ObjectDataSourceInstruction" AutoGenerateRows="False">
        <Fields>
            <asp:TemplateField HeaderText="<%$ Resources:Default, NumberOfLines %>">
                <ItemTemplate>
                    <asp:Label ID="labelCurrentLine" runat="server" Text='<%# Bind("CurrentLine") %>'></asp:Label>
                    <asp:Label ID="labelOf" runat="server" Text="<%$ Resources:Default, Of %>"></asp:Label>
                    <asp:Label ID="labelTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="PalletId" HeaderText="<%$ Resources:Default, PalletId %>" />--%>
            <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" />
            <asp:BoundField DataField="Product" HeaderText="<%$ Resources:Default, Product %>" />
            <asp:BoundField DataField="SKUCode" HeaderText="<%$ Resources:Default, SKUCode %>" />
            <asp:BoundField DataField="Batch" HeaderText="<%$ Resources:Default, Batch %>" />
            <asp:TemplateField HeaderText="<%$ Resources:Default, Quantity %>">
                <ItemTemplate>
                    <asp:Label ID="LabelQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="PickLocation" HeaderText="<%$ Resources:Default, PickLocation %>" />--%>
            <asp:BoundField DataField="StoreArea" HeaderText="<%$ Resources:Default, Area %>" />
            <asp:TemplateField HeaderText="<%$ Resources:Default, StoreLocation %>">
                <ItemTemplate>
                    <asp:Label ID="LabelStoreLocation" runat="server" Text='<%# Bind("StoreLocation") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
    <br />
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <uc5:GetMixedPutaway ID="GetMixedPutaway1" runat="server" />
        </asp:View>
        <asp:View ID="View2" runat="server">
            <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="ProductCode,Quantity,PickLocation,StoreLocation" DataSourceID="ObjectDataSourceInstruction" AutoGenerateRows="False">
                <Fields>
                    <asp:TemplateField HeaderText="<%$ Resources:Default, NumberOfLines %>">
                        <ItemTemplate>
                            <asp:Label ID="labelCurrentLine" runat="server" Text='<%# Bind("CurrentLine") %>'></asp:Label>
                            <asp:Label ID="labelOf" runat="server" Text="<%$ Resources:Default, Of %>"></asp:Label>
                            <asp:Label ID="labelTotalLines" runat="server" Text='<%# Bind("TotalLines") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>
            </asp:DetailsView>
            <uc2:ConfirmProduct ID="ConfirmProduct1" runat="server" />
            <asp:Button ID="lbComplete" runat="server" TabIndex="2" Text="<%$ Resources:Default, Finish %>" OnClick="lbComplete_Click"></asp:Button>
        </asp:View>
         <asp:View ID="View3" runat="server">
            <uc8:ConfirmBatch ID="ConfirmBatch1" runat="server" />
        </asp:View>
        <asp:View ID="View4" runat="server">
            <uc4:ConfirmStoreLocation ID="ConfirmStoreLocation1" runat="server" />
        </asp:View>
        <asp:View ID="View5" runat="server">
            <uc7:AlternateStoreLocation ID="AlternateStoreLocation1" runat="server" />
        </asp:View>
        <asp:View ID="View6" runat="server">
            <uc3:ConfirmQuantity ID="ConfirmQuantity1" runat="server" />
        </asp:View>
        <asp:View ID="View7" runat="server">
            <asp:Button ID="LinkButtonAcceptNext" runat="server" TabIndex="1" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAcceptNext_Click"></asp:Button>
            <asp:Button ID="LinkButtonRejectNext" runat="server" TabIndex="2" Text="<%$ Resources:Default, Reset %>" OnClick="LinkButtonRejectNext_Click"></asp:Button>
        </asp:View>
    </asp:MultiView>
    
    <asp:ObjectDataSource ID="ObjectDataSourceInstruction" runat="server" TypeName="Transact"
        SelectMethod="GetInstructionDetails">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="instructionId" SessionField="InstructionId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

