<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GetMixedPutaway.ascx.cs" Inherits="Screens_GetMixedPutaway" %>

<asp:Label ID="lblNewPallet" runat="server" Text="<%$ Resources:Default, NewPallet %>"></asp:Label>
<br />
<asp:TextBox ID="TextBoxReferenceNumber" runat="server" Font-Size="X-Small" TabIndex="1"></asp:TextBox>
<br />
<asp:Button ID="lbAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
