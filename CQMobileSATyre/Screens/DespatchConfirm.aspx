<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="DespatchConfirm.aspx.cs" Inherits="Screens_DespatchConfirm" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiViewConfirm" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="LabelConfirmLocation" runat="server" Text="<%$ Resources:Default, StoreLocation %>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxConfirmLocation" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonConfirmLocation" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonConfirmLocation_Click"></asp:Button>
            <asp:Button ID="btnQuit" runat="server" TabIndex="3" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
        <asp:View ID="View2" runat="server">5
            <asp:Label ID="LabelDespatchLocation" runat="server" Text="<%$ Resources:Default, DespatchLocation %>"></asp:Label>
            <asp:Label ID="LabelBay" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Default, PalletstoScan %>"></asp:Label>
            <asp:Label ID="LabelPallets" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Default, TotalPallets%> "></asp:Label>
            <asp:Label ID="LabelTotal" runat="server"></asp:Label>
            <br />
            <br />
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, ScanBarcode%>"></asp:Label>
            <br />
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
            <br />
            <asp:Button ID="LinkButtonBarcode" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonBarcode_Click"></asp:Button>
            <asp:Button ID="Button2" runat="server" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
        </asp:View>
    </asp:MultiView>
</asp:Content>

