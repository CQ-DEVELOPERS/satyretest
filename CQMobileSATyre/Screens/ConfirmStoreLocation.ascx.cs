using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ConfirmStoreLocation : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //lbStoreLocation.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            //lbResetLoc.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            if (Session["Type"].ToString() == "StoreLocation")
                TextBoxConfirmStoreLocation.Focus();
        }

        if (Session["InstructionId"] != null)
        {
            Transact tran = new Transact();

            string instructionTypeCode = tran.InstructionType(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]);

            Session["InstructionTypeCode"] = instructionTypeCode;

            //if (instructionTypeCode != "S" && instructionTypeCode != "SM" && instructionTypeCode != "PR" && instructionTypeCode != "R" && instructionTypeCode != "M")
                //lbManual.Visible = false;
        }
        
        Session["StoreLocation"] = TextBoxConfirmStoreLocation.Text;
        
        if (TextBoxConfirmStoreLocation.Text == "")
            return;
    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
      Page.SetFocus(TextBoxConfirmStoreLocation);
    }
    protected void lbStoreLocation_Click(object sender, EventArgs e)
    {
        try
        {
            if (TextBoxConfirmStoreLocation.Text == "")
                return;

            Transact tran = new Transact();

            if (tran.ConfirmStoreLocation(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], TextBoxConfirmStoreLocation.Text, (int)Session["OperatorId"]) == 0)
            {
                    Session["Message"] = Resources.ResMessages.Successful;
                    Session["Error"] = false;
                    Session["CurrentView"] = "StoreLocation";
                    Session["Type"] = "StoreLocation";
                // Set the store location to display
                //if (Session["InstructionId"] != null)
                //{
                //    String location = tran.GetStoreLocation(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]);

                //    if (location != null)
                //        Session["StoreLocation"] = location;
                //}
            }
            else
            {
                Session["Message"] = Resources.ResMessages.InvalidLocation;
                Session["Error"] = true;
            }

            TextBoxConfirmStoreLocation.Text = "";
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }

        //if (Session["FromURL"] != null)
        //    Response.Redirect(Session["FromURL"].ToString());
    }
    protected void lbResetLoc_Click(object sender, EventArgs e)
    {
        TextBoxConfirmStoreLocation.Text = "";
        TextBoxConfirmStoreLocation.Focus();
    }
    protected void lbManual_Click(object sender, EventArgs e)
    {
        try
        {
            Transact tran = new Transact();

            if (tran.ManualErrorStore(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
            {
                Session["Type"] = "LocationError";
                Session["Error"] = true;
                Session["CurrentView"] = "LocationError";
            }
            else
            {
                Session["Message"] = Resources.ResMessages.InvalidLocation;
                Session["Error"] = true;
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
}
