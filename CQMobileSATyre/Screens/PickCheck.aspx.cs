using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_PickCheck : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        try
        {
            if (Session["Error"] != null)
            {
                if ((bool)Session["Error"])
                {
                    if (Session["Message"] != null)
                        Master.MsgText = Session["Message"].ToString();
                }
                else
                {
                    if (Session["Type"] != null)
                    {
                        Session["Error"] = null;

                        switch (Session["Type"].ToString())
                        {
                            case "GetFullPick":
                                Master.MsgText = Resources.ResMessages.ConfirmPickLocation;
                                DetailsViewInstruction.Visible = true;
                                Session["Type"] = "PickLocation";
                                Session["ActiveViewIndex"] = 1;
                                break;
                            case "PickLocation":
                                Master.MsgText = Resources.ResMessages.ConfirmAlternatePickLocation;
                                Session["Type"] = "Alternate";
                                Session["ActiveViewIndex"] = 2;
                                break;
                            case "Alternate":
                                Master.MsgText = Resources.ResMessages.ConfirmProduct;
                                Session["Type"] = "Product";
                                Session["ActiveViewIndex"] = 3;
                                break;
                            case "LocationError":
                                Master.MsgText = Resources.ResMessages.ConfirmAfterLocationError;
                                Session["Type"] = "PickLocation";
                                Session["ActiveViewIndex"] = 1;

                                ResetSessionValues();
                                MultiViewConfirm.ActiveViewIndex = 0;
                                Session["ActiveViewIndex"] = 0;
                                Session["Message"] = Resources.ResMessages.Successful;

                                break;
                            case "Product":
                                Master.MsgText = Resources.ResMessages.ConfirmQuantity;
                                Session["Type"] = "Quantity";
                                Session["ActiveViewIndex"] = 4;
                                break;
                            case "Quantity":
                                Master.MsgText = Resources.ResMessages.ConfirmStoreLocation;
                                Session["Type"] = "StoreLocation";
                                Session["ActiveViewIndex"] = 5;
                                break;
                            case "StoreLocation":
                                Transact tran = new Transact();

                                if (tran.Pick(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0 && tran.Store(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"]) == 0)
                                {
                                    ResetSessionValues();
                                    MultiViewConfirm.ActiveViewIndex = 0;
                                    Session["ActiveViewIndex"] = 0;
                                    Session["Message"] = Resources.ResMessages.Successful;
                                }
                                else
                                {
                                    Session["Message"] = Resources.ResMessages.Failed;
                                    Session["Error"] = true;
                                }

                                break;
                            default:
                                Session["Message"] = Resources.ResMessages.Failed;
                                Session["Error"] = true;
                                break;
                        }
                    }
                }
            }

            if (Session["ActiveViewIndex"] != null)
                MultiViewConfirm.ActiveViewIndex = (int)Session["ActiveViewIndex"];
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                ResetSessionValues();
            }

            if (Session["FromURL"] == null)
                Session["FromURL"] = "~/Screens/PickFull.aspx";

            if (Session["Type"] == null)
            {
                Session["Type"] = "GetFullPick";
                DetailsViewInstruction.Visible = false;
            }
            else
            {
                if (DetailsViewInstruction.Rows.Count > 0)
                {
                    Session["ProductCode"] = DetailsViewInstruction.DataKey["ProductCode"].ToString();
                    Session["StoreLocation"] = DetailsViewInstruction.DataKey["StoreLocation"].ToString();
                    Session["PickLocation"] = DetailsViewInstruction.DataKey["PickLocation"].ToString();
                    Session["Quantity"] = Decimal.Parse(DetailsViewInstruction.DataKey["Quantity"].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void ResetSessionValues()
    {
        // Reset all the Session Values
        Session["Type"] = null;
        Session["ActiveViewIndex"] = null;
        Session["InstructionId"] = null;
        Session["ProductCode"] = null;
        Session["StoreLocation"] = null;
        Session["PickLocation"] = null;
        Session["Quantity"] = null;
        Session["Error"] = null;
        Session["Message"] = null;
    }
}
