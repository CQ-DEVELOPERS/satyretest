using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_SkuCheck : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }

    #endregion "InitializeCulture"
    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/SkuCheck.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                ResetSessionValues();
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }

    protected void ResetSessionValues()
    {
        // Reset all the Session Values
        Session["PrevQuantity"] = "";
        Session["Quantity"] = "";
        Session["SkuCode"] = "";
        Session["barcode"] = "";
        Session["Retry"] = 0;
        Session["ShowError"] = true;
    }

    protected void SkuCheck(string barcode, string SkuCode, Decimal ConfirmedQuantity)
    {
        SkuCheck sku = new SkuCheck();
        int result;
        result = sku.CheckSkuQuantity(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], barcode, SkuCode, ConfirmedQuantity, (int)Session["OperatorId"]);
    }

    protected void GetNextSku(string barcode)
    {
        if (barcode == "")
        {
            ReturnToScreen0(Resources.ResMessages.EmptyField);
            return;
        }
        SkuCheck sku = new SkuCheck();
        int result;
        string message;
        result = sku.GetSkuCheck(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], barcode);

        if (result == 0)
        {
            Session["Quantity"] = sku.quantity;
            Session["SkuCode"] = sku.skuCode;
            Session["barcode"] = barcode;
            Session["Retry"] = 0;
            ReturnToScreen1(Resources.ResMessages.ConfirmQuantity, sku.skuCode, "");
        }
        else
        {
            switch (result)
            {
                case 900003:
                    if ((Boolean)Session["ShowError"] == true)
                        message = Resources.ResMessages.PalletStatusInvalid;
                    else
                        message = Resources.ResMessages.ScanPallet;
                    break;
                case 900005:
                    message = Resources.ResMessages.db_900005;
                    break;
                case 900006:
                    message = Resources.ResMessages.InvalidPalletId;
                    break;
                case 900008:
                    message = Resources.ResMessages.SkuCheckPalletComplete;
                    break;
                case 900009:
                    message = Resources.ResMessages.SkuCheckQuantityQA;
                    break;
                default:
                    message = result.ToString();
                    break;
            }
            ReturnToScreen0(message);
        }
    }

    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
         GetNextSku(TextBoxBarcode.Text);
    }
    
    protected void LinkButtonBarcode_Click(object sender, EventArgs e)
    {
        int retry = 0;
        try
        {
            if (TextBoxQuantity.Text == "")
            {
                ReturnToScreen1(Resources.ResMessages.EmptyField, "", "");
                return;
            }
            if (Session["Quantity"].ToString() == TextBoxQuantity.Text)
            // Quantity Matches
            {
                SkuCheck(Session["barcode"].ToString(), Session["SkuCode"].ToString(), Decimal.Parse(TextBoxQuantity.Text));
                Session["ShowError"] = false;
                GetNextSku(Session["barcode"].ToString());
                Session["ShowError"] = true;
            }
            else
            // Must enter same incorrect quantity twice to 
            // send to QA.
            {
                retry = (int)Session["Retry"];
                retry++;
                Session["Retry"] = retry;
                if (retry > 1)
                {
                    if (Session["PrevQuantity"].ToString() == TextBoxQuantity.Text.ToString())
                    // Entered Same incorrect quantity twice
                    // Send Pallet to QA
                    {
                        SkuCheck(Session["barcode"].ToString(), Session["SkuCode"].ToString(), Decimal.Parse(TextBoxQuantity.Text));
                        ReturnToScreen0(Resources.ResMessages.SkuCheckQuantityQA);
                    }
                    else
                    // Entered a Different incorrect quantity
                    // Must enter it again
                    {
                        Session["Retry"] = 1;
                        ReturnToScreen1(Resources.ResMessages.SkuCheckQuantityMismatch, "", TextBoxQuantity.Text.ToString());
                    }   
                }
                else
                // Store the incorrect entered quantity in Session variable
                {
                    ReturnToScreen1(Resources.ResMessages.SkuCheckQuantityInvalid, "", TextBoxQuantity.Text.ToString());
                }
                   
            }
                        
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }
    protected void ReturnToScreen1(string message, string SkuCode, string prevQuantity)
    {
        Master.MsgText = message;
        Session["PrevQuantity"] = prevQuantity;
        if (SkuCode != "")
        {
            LabelSkuCode.Text = SkuCode;
        }
        TextBoxQuantity.Text = "";
        MultiViewConfirm.ActiveViewIndex = 1;
    }
    protected void ReturnToScreen0(string message)
    {
        Master.MsgText = message;
        TextBoxBarcode.Text = "";
        TextBoxQuantity.Text = "";
        ResetSessionValues();
        MultiViewConfirm.ActiveViewIndex = 0;
    }

}
