<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfirmBatch.ascx.cs" Inherits="Screens_ConfirmBatch" %>
<asp:Label ID="LabelConfirmBatch" runat="server" Text="<%$ Resources:Default, Batch %> "></asp:Label>
<br />
<asp:TextBox ID="TextBoxConfirmBatch" runat="server" TabIndex="1"></asp:TextBox>
<br />
<asp:Button ID="lbConfirmBatch" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbConfirmBatch_Click"></asp:Button>
<asp:Button ID="lbResetBatch" runat="server" TabIndex="3" Text="<%$ Resources:Default, Reset %>" OnClick="lbResetBatch_Click"></asp:Button>
<asp:Button ID="Button3" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>