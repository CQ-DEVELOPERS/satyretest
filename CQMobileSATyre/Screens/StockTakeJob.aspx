<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="StockTakeJob.aspx.cs" Inherits="Screens_StockTakeJob" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsView1" runat="server" DataSourceID="ObjectDataSource1" DataKeyNames="JobId" AutoGenerateRows="false" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="ReferenceNumber" HeaderText="<%$ Resources:Default, ReferenceNumber %>" />
            <asp:BoundField DataField="InstructionType" HeaderText="<%$ Resources:Default, InstructionType %>" />
            <asp:BoundField DataField="Count" HeaderText="<%$ Resources:Default, Count %>" />
            <asp:BoundField DataField="FirstLocation" HeaderText="<%$ Resources:Default, FirstLocation %>" />
            <asp:BoundField DataField="LastLocation" HeaderText="<%$ Resources:Default, LastLocation %>" />
        </Fields>
    </asp:DetailsView>
    <asp:Button ID="lbAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
    <asp:Button ID="btnQuit" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="StockTake" SelectMethod="GetJobs">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="operatorId" SessionField="OperatorId" Type="Int32" DefaultValue="-1" />
            <asp:SessionParameter Name="areaId" SessionField="AreaId" Type="Int32" DefaultValue="-1" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

