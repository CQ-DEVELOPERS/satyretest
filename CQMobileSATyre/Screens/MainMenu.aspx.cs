using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_MainMenu : System.Web.UI.Page
{
    #region StyleSheetTheme
    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }
    #endregion "StyleSheetTheme"

    #region Page_PreInit
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }
    #endregion "Page_PreInit"

    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }

        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;

        try
        {
            if (!Page.IsPostBack)
            {
                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                ProfileCommon pc = this.Profile.GetProfile(Profile.UserName);

                if (pc == null)
                {
                    Response.Redirect("~/Screens/ChooseTheme.aspx");
                    return;
                }

                if (this.Profile.GetPropertyValue("Theme").ToString() == "VehicleMount")
                    Response.Redirect("~/Screens/MainMenuVM.aspx");

                if (Session["ConnectionStringName"] == null)
                    return;

                MenuItemsDynamic ds = new MenuItemsDynamic();

                int operatorId;
                int menuId = 6;

                if (Session["OperatorId"] == null)
                    operatorId = -1;
                else
                    operatorId = (int)Session["OperatorId"];

                xmlDataSource.Data = ds.GetMenuItems(Session["ConnectionStringName"].ToString(), menuId, operatorId, "Mobile").GetXml();
                xmlDataSource.EnableCaching = false;
                xmlDataSource.DataBind();
            }
        }
        catch { }
    }
    #endregion

    #region btnLogout_Click
    protected void btnLogout_Click(object sender, EventArgs e)
    {
        // Perform any post-logout processing, such as setting the
        // user's last logout time or clearing a per-user cache of 
        // objects here.
        try
        {
            LogonCredentials lc = new LogonCredentials();

            lc.UserLoggedOut(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], "Mobile");
        }
        catch (Exception ex)
        {
            Session["LogonErrorMessage"] = ex.Message;
        }

        try
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            FormsAuthentication.RedirectToLoginPage();
        }
        catch { }
    }
    #endregion btnLogout_Click
}

