<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ReceiveDocument.aspx.cs" Inherits="Screens_ReceiveDocument" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:DetailsView ID="DetailsViewDocument" runat="server" DataKeyNames="ReceiptId" DataSourceID="ObjectDataSourceDocument" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
        <Fields>
            <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" />
            <asp:BoundField DataField="VehicleRegistration" HeaderText="<%$ Resources:Default, VehicleRegistration %>" />
            <asp:BoundField DataField="DeliveryNoteNumber" HeaderText="<%$ Resources:Default, DeliveryNoteNumber %>" />
            <asp:BoundField DataField="DeliveryDate" HeaderText="<%$ Resources:Default, DeliveryDate %>" />
            <asp:BoundField DataField="SealNumber" HeaderText="<%$ Resources:Default, SealNumber %>" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSourceDocument" runat="server" TypeName="Receiving"
        SelectMethod="ReceivingDocumentSelect">
        <SelectParameters>
            <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
            <asp:SessionParameter Name="receiptId" SessionField="ReceiptId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0" OnActiveViewChanged="MultiView1_ActiveViewChanged">
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelOrderNumber" runat="server" Text="<%$ Resources:Default, OrderNumber %>"></asp:Label>
            <asp:TextBox ID="TextBoxOrderNumber" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <asp:Label ID="LabelVehicleRegistration" runat="server" Text="<%$ Resources:Default, VehicleRegistration2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxVehicleRegistration" runat="server" TabIndex="2"></asp:TextBox>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <br />
            <asp:Label ID="LabelDeliveryNoteNumber" runat="server" Text="<%$ Resources:Default, DeliveryNoteNumber2 %>"></asp:Label>
            <asp:TextBox ID="TextBoxDeliveryNoteNumber" runat="server" TabIndex="3"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxDeliveryNoteNumber"  Text="* Delivery Note Number Required"></asp:RequiredFieldValidator>
            <%--<asp:RequiredFieldValidator ID="rfvDeliveryNoteNumber" runat="server" ControlToValidate="TextBoxDeliveryNoteNumber" SetFocusOnError="true" ValidationGroup="First" Text="* Delivery Note Number Required" Display="Dynamic"></asp:RequiredFieldValidator>--%>

        </asp:View>
        <asp:View ID="View4" runat="server">
            <br />
            <asp:Label ID="LabelDeliveryDate" runat="server" Text="<%$ Resources:Default, DeliveryDate %>"></asp:Label>
            <br />
            <table>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxYear" runat="server" TabIndex="4" MaxLength="4" EnableTheming="false"
                            Width="40"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxMonth" runat="server" TabIndex="5" MaxLength="2" EnableTheming="false"
                            Width="20"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDay" runat="server" TabIndex="6" MaxLength="2" EnableTheming="false"
                            Width="20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelYear" runat="server" Enabled="false" Text="YYYY"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelMonth" runat="server" Enabled="false" Text="MM"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelDay" runat="server" Enabled="false" Text="DD"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="TextBoxHours" runat="server" TabIndex="7" MaxLength="2" EnableTheming="false"
                Width="15" Visible="false" Text="12"></asp:TextBox>
            <asp:TextBox ID="TextBoxMinutes" runat="server" TabIndex="8" MaxLength="2" EnableTheming="false"
                Width="15" Visible="false" Text="00"></asp:TextBox>
        </asp:View>
        <asp:View ID="View5" runat="server">
            <br />
            <asp:Label ID="LabelSealNumber" runat="server" Text="<%$ Resources:Default, SealNumber %>"></asp:Label>
            <asp:TextBox ID="TextBoxSealNumber" runat="server" TabIndex="9"></asp:TextBox>
        </asp:View>
    </asp:MultiView>
    <br />
    <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="10" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
    <asp:Button ID="LinkButtonBack" runat="server" TabIndex="11" Text="<%$ Resources:Default, Back %>" OnClick="LinkButtonBack_Click"></asp:Button>
    <asp:Button ID="LinkButtonSkip" runat="server" TabIndex="12" Text="<%$ Resources:Default, Skip %>" OnClick="LinkButtonSkip_Click" Visible="false"></asp:Button>
    <asp:Button ID="btnQuit" runat="server" TabIndex="13" CausesValidation="False" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

