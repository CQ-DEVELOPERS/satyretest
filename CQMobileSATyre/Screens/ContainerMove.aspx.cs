using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ContainerMove : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/ContainerMove.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");
            }
        }
        catch { }
    }
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            Container cont = new Container();
            
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    DetailsViewDocument.DataBind();
                    
                    LinkButtonBack.Visible = true;

                    if (DetailsViewDocument.Rows.Count > 0)
                        Session["ContainerHeaderId"] = int.Parse(DetailsViewDocument.DataKey["ContainerHeaderId"].ToString());

                    if (Session["ContainerHeaderId"] != null)
                    {
                        if(!TextBoxBarcode.Text.StartsWith("P:"))
                            MultiView1.ActiveViewIndex++;

                        DataSet ds = null;

                        if (DetailsViewDocument.Rows.Count > 0)
                        {
                            Session["ContainerHeaderId"] = int.Parse(DetailsViewDocument.DataKey["ContainerHeaderId"].ToString());
                            Session["ContainerDetailId"] = int.Parse(DetailsViewDocument.DataKey["ContainerDetailId"].ToString());

                            ds = cont.GetPickLocation(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], (int)Session["ContainerHeaderId"], (int)Session["ContainerHeaderId"]);

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow dr = ds.Tables[0].Rows[0];
                                TextBoxPickLocation.Text = dr.ItemArray.GetValue(0).ToString();
                            }
                        }

                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidBarcode;
                    }

                    break;
                case 1:
                    DetailsViewDocument.DataBind();
                    
                    if (DetailsViewDocument.Rows.Count > 0)
                        Session["ContainerHeaderId"] = int.Parse(DetailsViewDocument.DataKey["ContainerHeaderId"].ToString());

                    if (Session["ContainerHeaderId"] != null)
                    {
                        DetailsViewDocument.ChangeMode(DetailsViewMode.Edit);
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidBarcode;
                    }

                    break;
                case 2:
                    if (DetailsViewDocument.CurrentMode == DetailsViewMode.Edit)
                        DetailsViewDocument.UpdateItem(true);
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;

                    break;
                case 3:
                    if (cont.Move(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], (int)Session["OperatorId"], (int)Session["ContainerHeaderId"], TextBoxStoreLocation.Text))
                    {
                        MultiView1.ActiveViewIndex++;
                        DetailsViewDocument.Visible = false;
                        LinkButtonBack.Visible = false;
                        GridViewSOH.DataBind();
                        Master.MsgText = Resources.ResMessages.Successful;
                    }
                    else
                    {
                        TextBoxStoreLocation.Text = "";
                        Master.MsgText = Resources.ResMessages.StoreLocationError;
                    }

                    break;
                case 4:
                    Reset();
                    DetailsViewDocument.Visible = true;
                    break;
            }
        }
        catch { }
    }

    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
    }

    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                Response.Redirect("~/Screens/ContainerMove.aspx");
                break;
            //case 3:
            //    if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 124))
            //        MultiView1.ActiveViewIndex = 1;
            //    else
            //        MultiView1.ActiveViewIndex--;
            //    break;
            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }

    #region DetailsViewDocument_PageIndexChanged
    protected void DetailsViewDocument_PageIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (DetailsViewDocument.CurrentMode == DetailsViewMode.ReadOnly)
                DetailsViewDocument.ChangeMode(DetailsViewMode.Edit);
        }
        catch { }
    }
    #endregion DetailsViewDocument_PageIndexChanged

    protected void Reset()
    {
        try
        {
            Session["ContainerHeaderId"] = null;
            MultiView1.ActiveViewIndex = 0;
            Master.MsgText = Resources.ResMessages.Successful;
            TextBoxBarcode.Text = "";
            TextBoxStoreLocation.Text = "";
        }
        catch { }
    }
}
