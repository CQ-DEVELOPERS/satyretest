<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="ChooseTheme.aspx.cs" Inherits="Screens_ChooseTheme" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
        <asp:ListItem Selected="True" Text="<%$ Resources:Default, DefaultTheme %>" Value="Default"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Default, VehicleMountTheme %>" Value="VehicleMount"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Default, WearableTheme %>" Value="Wearable"></asp:ListItem>
        <asp:ListItem Text="Large" Value="Large"></asp:ListItem>
    </asp:RadioButtonList>
    <asp:Button ID="Button1" runat="server" Text="<%$ Resources:Default, Accept2 %>" OnClick="Button1_Click" />
    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="<%$ Resources:Default, Menu %>" />
</asp:Content>