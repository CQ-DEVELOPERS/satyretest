using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_StockTake : System.Web.UI.UserControl
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lbConfirmQuantity.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            lbResetQuantity.Attributes.Add("onclick", "this.disabled= 'disabled' ");
            if (Session["Type"].ToString() == "StockTake")
                TextBoxConfirmQuantity.Focus();
        }
    }

    protected void lbConfirmQuantity_Click(object sender, EventArgs e)
    {
        try
        {
            if (ConfirmQuantity())
            {
                Session["Message"] = Resources.ResMessages.Successful;
                Session["CurrentView"] = "StockTake";
            }

            TextBoxConfirmQuantity.Text = "";
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }

        //if (Session["FromURL"] != null)
        //    Response.Redirect(Session["FromURL"].ToString());
    }

    protected bool ConfirmQuantity()
    {
        try
        {
            Transact tran = new Transact();

            Decimal confirmQuantity = -1;
            Decimal quantity = 1;
            Decimal previousQuantity = -1;

            if (!Decimal.TryParse(Session["Quantity"].ToString(), out quantity))
            {
                Session["Error"] = true;
                Session["Message"] = Resources.ResMessages.InvalidField;
                return false;
            }

            if (!Decimal.TryParse(TextBoxConfirmQuantity.Text, out confirmQuantity))
            {
                Session["Error"] = true;
                Session["Message"] = Resources.ResMessages.QuantityReEnter;
                return false;
            }

            if (quantity == confirmQuantity)
                Session["QuantityCheck"] = true;
            else
            {
                if (Session["QuantityCheck"] == null)
                {
                    Session["QuantityCheck"] = false;
                    Session["PreviousQuantity"] = confirmQuantity;
                }
                else
                {
                    Session["QuantityCheck"] = false;
                    if (Session["PreviousQuantity"] == null)
                        Session["PreviousQuantity"] = confirmQuantity;
                    else
                        if (!Decimal.TryParse(Session["PreviousQuantity"].ToString(), out previousQuantity))
                        {
                            Session["Error"] = true;
                            Session["Message"] = Resources.ResMessages.InvalidField;
                            return false;
                        }
                }
            }

            if (previousQuantity == confirmQuantity)
            {
                Session["QuantityCheck"] = true;
                Session["PreviousQuantity"] = null;
            }

            if ((bool)Session["QuantityCheck"])
            {
                //int result = tran.ConfirmQuantity(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], confirmQuantity);
                //if (result == 0)
                //{
                //    Session["Error"] = false;
                //    return true;
                //}
                //else if (result == 5)
                //{
                //    int instructionId = tran.GetPickingMixed(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], Session["ReferenceNumber"].ToString(), -1);

                //    if (instructionId != -1)
                //    {
                //        Session["InstructionId"] = instructionId;
                //        Session["Message"] = Resources.ResMessages.Successful;
                //        Session["Error"] = false;
                //        Session["Type"] = "GetMixedPickDetails";
                //        Session["ActiveViewIndex"] = 1;
                //    }
                //    return true;
                //}
                //else if (result == 8)
                //{
                    int jobId = tran.AdhocStockTake(Session["ConnectionStringName"].ToString(), (int)Session["InstructionId"], confirmQuantity);

                    if (jobId >= 0)
                    {
                        Session["Message"] = Resources.ResMessages.Successful;
                        Session["Error"] = false;
                        Session["Type"] = "StockTake";
                        Session["CurrentView"] = "StockTake";
                        Session["ActiveViewIndex"] = 7;

                        return true;
                    }
                    else
                    {
                        Session["Message"] = Resources.ResMessages.QuantityReEnter;
                        Session["Error"] = true;
                        return false;
                    }
            }
            else
            {
                TextBoxConfirmQuantity.Text = "";
                Session["PreviousQuantity"] = confirmQuantity;
                Session["Message"] = Resources.ResMessages.QuantityReEnter;
                Session["Error"] = true;
                return false;
            }
        }

        catch (Exception ex)
        {
            Session["Error"] = true;
            Session["Message"] = ex.Message;
            return false;
        }
    }

    protected void lbResetQuantity_Click(object sender, EventArgs e)
    {
        TextBoxConfirmQuantity.Text = "";
        Session["QuantityCheck"] = null;
        Session["PreviousQuantity"] = null;
        TextBoxConfirmQuantity.Focus();
    }
}
