using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_DespatchMove : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }



        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region StyleSheetTheme
    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }
    #endregion StyleSheetTheme

    #region Page_PreInit
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }
    #endregion Page_PreInit

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = "~/Screens/DespatchMove.aspx";
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                ResetSessionValues();
            }
        }
        catch (Exception ex)
        {
            Session["Message"] = ex.Message;
        }
    }
    #endregion Page_Load

    #region ResetSessionValues
    protected void ResetSessionValues()
    {
        // Reset all the Session Values
    }
    #endregion ResetSessionValues

    #region LinkButtonBarcode_Click
    protected void LinkButtonBarcode_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList arrList = new ArrayList();

            int result = 0, total = 0, pallets = 0, operatorId = -1;
            String despatchBay;

            if (!int.TryParse(Session["OperatorId"].ToString(), out operatorId))
            {
                Master.MsgText = Resources.ResMessages.InvalidOperator.ToString();
                return;
            }

            if (TextBoxBarcode.Text == "")
            {
                Master.MsgText = Resources.ResMessages.InvalidPalletId.ToString();
                TextBoxBarcode.TabIndex = 1;
                TextBoxBarcode.Focus();
                return;
            }

            Despatch despatch = new Despatch();
            arrList = despatch.MoveToDespatch(Session["ConnectionStringName"].ToString(), TextBoxBarcode.Text, "-1        ");

            if (arrList != null)
            {
                result = int.Parse(arrList[0].ToString());

                despatchBay = arrList[1].ToString();

                pallets = int.Parse(arrList[2].ToString());

                total = int.Parse(arrList[3].ToString());
            }
            else
                return;

            if (result != 0)
            {
                TextBoxBarcode.Text = "";

                DBErrorMessage dberr = new DBErrorMessage();

                Master.MsgText = dberr.ReturnErrorString(result);
                
                ResetSessionValues();
            }
            else
            {
                if(despatchBay.StartsWith("-1"))
                    LabelBay.Text = Resources.Default.ScanEmptyBay;
                else
                    LabelBay.Text = despatchBay;

                LabelPallets.Text = pallets.ToString();
                LabelTotal.Text = total.ToString();

                MultiViewConfirm.ActiveViewIndex++;

                Master.MsgText = Resources.ResMessages.Successful.ToString();

                ResetSessionValues();
            }
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }
    #endregion LinkButtonBarcode_Click

    #region LinkButtonConfirmLocation_Click
    protected void LinkButtonConfirmLocation_Click(object sender, EventArgs e)
    {
        try
        {
            ArrayList arrList = new ArrayList();

            int result = 0, total = 0, pallets = 0, operatorId = -1;
            string despatchBay = "123456789012345";

            if (!int.TryParse(Session["OperatorId"].ToString(), out operatorId))
            {
                Master.MsgText = Resources.ResMessages.InvalidOperator.ToString();
                return;
            }

            if (LabelBay.Text == "")
            {
                Master.MsgText = Resources.ResMessages.InvalidLocation.ToString();
                LabelBay.TabIndex = 1;
                LabelBay.Focus();
                return;
            }

            Despatch despatch = new Despatch();
            arrList = despatch.MoveToDespatch(Session["ConnectionStringName"].ToString(), TextBoxBarcode.Text, TextBoxConfirmLocation.Text);

            if (arrList != null)
            {
                result = int.Parse(arrList[0].ToString());

                despatchBay = arrList[1].ToString();

                pallets = int.Parse(arrList[2].ToString());

                total = int.Parse(arrList[3].ToString());
            }
            else
                return;

            TextBoxConfirmLocation.Text = "";

            if (result != 0)
            {
                DBErrorMessage dberr = new DBErrorMessage();

                Master.MsgText = dberr.ReturnErrorString(result);

                ResetSessionValues();
            }
            else
            {
                LabelBay.Text = despatchBay;
                LabelPallets.Text = pallets.ToString();
                LabelTotal.Text = total.ToString();

                TextBoxBarcode.Text = "";

                MultiViewConfirm.ActiveViewIndex--;

                Master.MsgText = Resources.ResMessages.Successful.ToString();

                ResetSessionValues();
            }
        }
        catch (Exception ex)
        {
            Master.MsgText = ex.Message;
        }
    }
    #endregion LinkButtonConfirmLocation_Click

    #region LinkButtonQuit_Click
    protected void LinkButtonQuit_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Default.aspx");
    }
    #endregion LinkButtonQuit_Click
}
