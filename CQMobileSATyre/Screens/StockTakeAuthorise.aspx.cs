using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Housekeeping_StockTakeAuthorise : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }
        }
        catch { }
    }
    #endregion "InitializeCulture"

    #region Private Variables
        private string result = "";
        private string theErrMethod = "";
    #endregion Private Variables

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["countLoopsToPreventInfinLoop"] = 0;
        theErrMethod = "Page Load";

        try
        {

            Master.MsgText = "Page loaded successfully";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.MsgText = result;
        }

    }
    #endregion Page_Load

    #region ButtomRefresh_Click
    protected void ButtomRefresh_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtomRefresh_Click";

        try
        {
            GridViewAuthorise.DataBind();

            Master.MsgText = "";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.MsgText = result;
        }
    }
    #endregion ButtomRefresh_Click

    #region ButtomAccept_Click
    protected void ButtomAccept_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtomAccept_Click";
        try
        {
            StockTakeAuthorise stockTake = new StockTakeAuthorise();

            if (!stockTake.Authorise(Session["ConnectionStringName"].ToString(),
                                        int.Parse(GridViewAuthorise.DataKey["InstructionId"].ToString()),
                                         int.Parse(Session["OperatorId"].ToString()),
                                         "A"))
                Master.MsgText = "There was an error";
            else
                Master.MsgText = "Stock Take Accepted";

            GridViewAuthorise.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.MsgText = result;
        }
    }
    #endregion "ButtomAccept_Click"

    #region ButtonRecount_Click
    protected void ButtonRecount_Click(object sender, EventArgs e)
    {
        theErrMethod = "ButtonRecount_Click";
        try
        {
            StockTakeAuthorise stockTake = new StockTakeAuthorise();

            if (!stockTake.Authorise(Session["ConnectionStringName"].ToString(),
                                        int.Parse(GridViewAuthorise.DataKey["InstructionId"].ToString()),
                                         int.Parse(Session["OperatorId"].ToString()),
                                         "RC"))
                Master.MsgText = "There was an error";
            else
                Master.MsgText = "Stock Take sent for recount";

            GridViewAuthorise.DataBind();
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.MsgText = result;
        }
    }
    #endregion "ButtonRecount_Click"

    #region StatusChange
    protected void StatusChange(string statusCode)
    {
        theErrMethod = "StatusChange";

        try
        {
            StockTakeAuthorise stockTake = new StockTakeAuthorise();

            if (GridViewAuthorise.Rows.Count > 0)
                if (!stockTake.Authorise(Session["ConnectionStringName"].ToString(),
                                            int.Parse(GridViewAuthorise.DataKey["InstructionId"].ToString()),
                                             int.Parse(Session["OperatorId"].ToString()),
                                             statusCode))

            Master.MsgText = "Page loaded successfully";
        }
        catch (Exception ex)
        {
            result = SendErrorNow("StockTakeAuthorise" + "_" + ex.Message.ToString());
            Master.MsgText = result;
        }

    }
    #endregion "StatusChange"

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        try
        {
            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                result = cqexception.GenericInboundErrorHandling(5, "Housekeeping_StockTakeAuthorise", theErrMethod, ex);
            }

            Session["countLoopsToPreventInfinLoop"] = "0";

            // throw new System.Exception();  
            loopPrevention++;

            Session["countLoopsToPreventInfinLoop"] = loopPrevention;

        }
        catch (Exception exMsg)
        {

            int loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());

            if (loopPrevention == 0)
            {
                CQExceptionLayer cqexception = new CQExceptionLayer();

                Session["countLoopsToPreventInfinLoop"] = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString()) + 1;

                result = cqexception.GenericInboundErrorHandling(3, "Housekeeping_StockTakeAuthorise", theErrMethod, exMsg.Message.ToString());

                Master.MsgText = result;

                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return result;
            }
            else
            {
                loopPrevention++;

                Session["countLoopsToPreventInfinLoop"] = loopPrevention;

                return "Please refresh this page.";
            }

        }

        return result;
    }
    #endregion ErrorHandling

}
