using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_CapturePickWeight : System.Web.UI.Page
{
    #region InitializeCulture
    protected virtual void InitializeCulture()
    {
        try
        {
            if (Session["CultureName"] != null)
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Session["CultureName"].ToString());
            }

        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
 

      
    }
    #endregion "InitializeCulture"

    #region PrivateVariables
    private int result = 0;
    private string strresult = "";
    private string theErrMethod = "";
    private int? ses = null;
    #endregion PrivateVariables

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["Url"] = "CapturePickWeight";
            Master.MsgText = Resources.Default.CapturePickWeightTitlte.ToString();

            if (!Page.IsPostBack)
            {
                //int dwelltime = 0;
                //string constring = Session["ConnectionStringName"].ToString();
                //int operatorID = int.Parse(Session["OperatorId"].ToString());

                //Transact tran = new Transact();
                //dwelltime = tran.DwellTime(constring, 0, operatorID);

                //if (Session["DwellTime"] == null)
                //{
                //    if (dwelltime == 1) // This should be 1
                //    {
                //        Session["DwellTime"] = 0;
                //        Session["Url"] = "CapturePickWeight.aspx";
                //        Server.Transfer("CopyDwellTime.aspx?CapturePickWeight");
                //    }
                //}
                //else
                //{
                //    if (dwelltime == 1) // This should be 1
                //    {
                //        Session["DwellTime"] = 0;
                //    }
                //}
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

    }
    #endregion Page Load

    #region CommandAccept_Click
    protected void CommandAccept_Click(object sender, EventArgs e)
    {
        try
        {
            Button btn = new Button();
            btn = (Button)sender;

            int instructId = 0, palletID = 0;

            //TEST DATA: exec p_Despatch_Check_Pallet @barcode=N'J:107',@tareWeight=0,@weight=1
            //Grant will change this to first get confirmation on the pallet to see if it exist then
            //send through the 2 weights
            //TEST DATA VALUES:
            // int weight = 100, weight2 = 90;


            /*************************************************/

            string Constr = Session["ConnectionStringName"].ToString();// = "Connection String"; 


            // string connectionStringName = Session["ConnectionStringName"].ToString();
            int operatorId = -1;

            if (!int.TryParse(Session["OperatorId"].ToString(), out operatorId))
            {
                Master.MsgText = "OperatorId invalid";
                return;
            }

            // Don't clean the menu, Grant expect the following: 
            // 'J:16'
            // palletID = CleanPalletID();

            /**************************************************/
            if (TextBoxPalletId.Text == "")
            {
                Master.MsgText = Resources.ResMessages.EmptyField.ToString();

                btn.ID = "lbResetPalletId";
                lbReset_Click(sender, e);
                return;
            }
            else if (TextBoxPalletId.Text == "0" || TextBoxPalletId.Text == "1")
            {
                Master.MsgText = Resources.ResMessages.InvalidField.ToString();

                btn.ID = "lbResetPalletId";
                lbReset_Click(sender, e);
                return;
            }
            else
            {

                Despatch dp = new Despatch();
                int iresult = dp.RecordWeight(Constr, TextBoxPalletId.Text, 0, 0);

                if (iresult != 0)
                {
                    Master.MsgText = Resources.ResMessages.InvalidPalletId.ToString();

                    btn.ID = "lbResetPalletId";
                    lbReset_Click(sender, e);
                    return;
                }
                else
                {                   
                    MultiView1.ActiveViewIndex = MultiView1.ActiveViewIndex + 1;
                   
                }
            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

    }
    #endregion CommandAccept_Click

    #region Reset
    protected void lbReset_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lbButton = (LinkButton)sender;

            switch (lbButton.ID)
            {

                case "lbResetPalletId":

                    TextBoxPalletId.TabIndex = 1;
                    CommandAccept.TabIndex = 2;
                    lbResetPalletId.TabIndex = 3;
                    //hlQuit.TabIndex = 4;
                    lbNewPallet.TabIndex = 5;

                    TextBoxPalletId.Text = "";
                    TextBoxPalletId.Focus();
                    break;

                case "lbResetWeight":

                    TextBoxTareWeight.TabIndex = 1;
                    TextBoxTotalWeight.TabIndex = 2;
                    CommandTareContainer.TabIndex = 3;
                    lbResetWeight.TabIndex = 4;
                    //LbQuitWeight.TabIndex = 5;

                    TextBoxTotalWeight.Text = "";
                    TextBoxTareWeight.Text = "";

                    TextBoxTareWeight.Focus();
                    break;

                default:
                    break;

            }

            //  string str = lb.ID.ToString();
            //  the following needs reset
            //  PalletId 
            //  PickLocation 

        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion Reset

    #region NextPallet
    protected void lbNextPallet_Click(object sender, EventArgs e)
    {
        try
        {
            CleanPage();
            MultiView1.ActiveViewIndex = 0;
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion NextPallet

    #region TareContainer
    protected void CommandTareContainer_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lb = new LinkButton();
            lb = (LinkButton)sender;

            int itareWeight = 0, palletID = 0, itareWeightTotal = 0;

            /*************************************************/

            string Constr = Session["ConnectionStringName"].ToString();// = "Connection String"; 


            // string connectionStringName = Session["ConnectionStringName"].ToString();
            int operatorId = -1;

            if (!int.TryParse(Session["OperatorId"].ToString(), out operatorId))
            {
                Master.MsgText = Resources.ResMessages.OperatorInvalid.ToString();
                return;
            }

            // Don't clean the menu, Grant expect the following: 
            // 'J:16'
            // palletID = CleanPalletID();

            if(!int.TryParse(TextBoxTareWeight.Text, out itareWeight))
            {
                Master.MsgText = Resources.ResMessages.InvalidTareWeight.ToString();
               return;
            }

            if (!int.TryParse(TextBoxTotalWeight.Text, out itareWeightTotal))
            {
                Master.MsgText = Resources.ResMessages.InvalidTareWeightTotal.ToString();
                return;
            }


            /**************************************************/

            Despatch dp = new Despatch();
            int iresult = dp.RecordWeight(Constr, TextBoxPalletId.Text, itareWeight, itareWeightTotal);
        

            //bool allValid = true;

            if (iresult != 0)//allValid)
            {

                Master.MsgText = Resources.ResMessages.ErrorTareWeightPage.ToString();
                lb.ID = "lbResetWeight";
                lbReset_Click(sender, e);
                return;
               
            }
            else
            {
                MultiView1.ActiveViewIndex = MultiView1.ActiveViewIndex + 1;
            }

        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }
    }
    #endregion TareContainer

    #region Clean Page
    protected void CleanPage()
    {
        try
        {
            TextBoxPalletId.Text = "";
            TextBoxTareWeight.Text = "";
            TextBoxTotalWeight.Text = "";

        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

    }
    #endregion Clean Page

    #region Clean Pallet ID
    protected int CleanPalletID()
    {

        int iPalletID = 0;

        try
        {
            string strPalletId = TextBoxPalletId.Text;

            if (TextBoxPalletId.Text != "")
            {

                if (TextBoxPalletId.Text.StartsWith("P:"))
                {
                    // Remove first 2 char.
                    StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                    strPalletId = strReader.ReadLine();
                    Session["PalletId"] = TextBoxPalletId.Text;
                    iPalletID = int.Parse(strPalletId);


                }
                else if (TextBoxPalletId.Text.StartsWith("J:"))
                {

                    // Remove first 2 char.
                    StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                    strPalletId = strReader.ReadLine();
                    Session["PalletId"] = TextBoxPalletId.Text;
                    iPalletID = int.Parse(strPalletId);


                }
                else if (TextBoxPalletId.Text.StartsWith("R:"))
                {

                    // Remove first 2 char.
                    StringReader strReader = new StringReader(strPalletId.Remove(0, 2));
                    strPalletId = strReader.ReadLine();
                    Session["PalletId"] = TextBoxPalletId.Text;
                    iPalletID = int.Parse(strPalletId);


                }
                else if (int.TryParse(TextBoxPalletId.Text, out iPalletID))
                {

                    Master.MsgText = Resources.ResMessages.InvalidPalletId.ToString();

                    Session["PalletId"] = iPalletID.ToString();

                    TextBoxPalletId.Text = "";
                    TextBoxPalletId.TabIndex = 1;
                    TextBoxPalletId.Focus();

                }
                else
                {
                    //Master.MsgText = Resources.ResMessages.EmptyField.ToString();
                    //TextBoxPalletId.Text = "";
                    //TextBoxPalletId.TabIndex = 1;
                    //TextBoxPalletId.Focus();
                }

            }
        }
        catch (Exception ex)
        {
            string errMsgReturn = SendErrorNow(ex.Message);
            Master.MsgText = errMsgReturn;
        }

        return iPalletID;
    }     
    #endregion Clean Pallet ID

    #region ErrorHandling
    private string SendErrorNow(string ex)
    {

        int loopPrevention = -1;

        try
        {
            try
            {
                loopPrevention = int.Parse(Session["countLoopsToPreventInfinLoop"].ToString());
            }
            catch
            {
                Session["countLoopsToPreventInfinLoop"] = 0;
                loopPrevention = 0;
            }



            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                  Resources.Default.CapturePickWeightTitlte.ToString(),
                                                                  theErrMethod,
                                                                  ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {

                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
            }


        }
        catch (Exception exMsg)
        {

            if (loopPrevention == 0)
            {
                MCQExceptionLayer cqexception = new MCQExceptionLayer();

                strresult = cqexception.GenericMobileErrorHandling(int.Parse(Resources.ResMessages.ErrorPriority3.ToString()),
                                                                 Resources.Default.CapturePickWeightTitlte.ToString(),
                                                                 theErrMethod,
                                                                 ex);
                Session["countLoopsToPreventInfinLoop"] = 1;
                loopPrevention = 1;
            }
            else
            {
                loopPrevention++;
                Session["countLoopsToPreventInfinLoop"] = loopPrevention;
                return "Please refresh this page.";
            }

        }

        return strresult;
    }
    #endregion ErrorHandling
}
