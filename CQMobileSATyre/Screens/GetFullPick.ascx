<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GetFullPick.ascx.cs" Inherits="Screens_GetFullPick" %>

<asp:Label ID="lblNewPallet" runat="server" Text="<%$ Resources:Default, NewPallet %>" Visible="false"></asp:Label>
<asp:Button ID="lbAccept" runat="server" TabIndex="2" Text="<%$ Resources:Default, Accept %>" OnClick="lbAccept_Click"></asp:Button>
<asp:Button ID="btnQuit" runat="server" TabIndex="4" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
