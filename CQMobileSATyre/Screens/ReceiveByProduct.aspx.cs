using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Screens_ReceiveByProduct : System.Web.UI.Page
{
    public void ButtonPackaging_Click(object sender, EventArgs e)
    {
        try
        {
            Receiving r = new Receiving();

            int packagingId = r.GetPackingOrder(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"]);

            if (packagingId > 0)
                Session["ReceiptId"] = packagingId;
            else
                Master.MsgText = Resources.ResMessages.PackagingOrder;
            LinkButtonRedelivery.Visible = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 17);
            DetailsViewDocument.DataBind();
        }
        catch { }
    }

    public override String StyleSheetTheme
    {
        get { return this.Profile.GetPropertyValue("StyleSheetTheme").ToString(); }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = this.Profile.GetPropertyValue("Theme").ToString();
    }

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ReceiptLineId"] = null;

                // Check if the Operator has exceeded the Dwell Time Value
                DwellTime dwell = new DwellTime();

                if (dwell.DwellTimeCheck(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"]))
                {
                    Session["ReturnURL"] = HttpContext.Current.Request.Url; ;
                    Response.Redirect("~/Screens/DwellTime.aspx");
                }

                Response.AddHeader("Cache-control", "no-store, must-revalidate,private,no-cache");
                Response.AddHeader("Pragma", "no-cache");
                Response.AddHeader("Expires", "0");

                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 154))
                {
                    LinkButtonPackaging.Visible = false;
                    ButtonPackaging.Visible = false;
                    }
            }
        }
        catch { }
    }
    #endregion Page_Load

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
      switch (MultiView1.ActiveViewIndex)
      {
        case 0:
          Page.SetFocus(TextBoxBarcode);
          break;
        case 1:
          Page.SetFocus(TextBoxLength);
          break;
        case 2:
          Page.SetFocus(TextBoxBatch);
          break;
        case 3:
          Page.SetFocus(TextBoxYear);
          break;
        case 4:
          Page.SetFocus(TextBoxQuantity);
          break;
        case 5:
          Page.SetFocus(TextBoxSample);
          break;
        case 6:
          Page.SetFocus(TextBoxReject);
          break;
        case 7:
          Page.SetFocus(TextBoxStoreLocation);
          break;
        case 8:
          Page.SetFocus(TextBoxNumberOfPallets);
          break;
        case 9:
          Page.SetFocus(TextBoxPallet);
          break;
      };

    }

    #region LinkButtonAccept_Click
    protected void LinkButtonAccept_Click(object sender, EventArgs e)
    {
        try
        {
            Receiving rcv = new Receiving();
            int receiptLineId = 0;

            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    receiptLineId = rcv.ConfirmProduct(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"], TextBoxBarcode.Text);

                    if (receiptLineId > 0)
                    {
                        Session["ReceiptLineId"] = receiptLineId;
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;

                        if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 120) || 
                                Configuration.GetSwitchReceiptLineId(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"]) ||
                                Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], -1) == LotAttributeRule.AutoGenerate) 
                        {
                            // Generate Number
                            if (TextBoxBatch.Text == "")
                                TextBoxBatch.Text = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

                            // Disable Editing
                            TextBoxBatch.Enabled = false;
                        }
                        else
                        {
                            // Generate Number
                            TextBoxBatch.Text = "";

                            // Disable Editing
                            TextBoxBatch.Enabled = true;
                        }
                    }
                    else
                    {
                        TextBoxBarcode.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidProduct;
                    }

                    break;
                case 1:
                    try
                    {
                        Decimal length = 0;
                        Decimal width = 0;
                        Decimal height = 0;
                        Decimal weight = 0;
                        Decimal packQuantity = 0;
                        Decimal palletQuantity = 0;

                        if (!Decimal.TryParse(TextBoxLength.Text, out length) && TextBoxLength.Text != "")
                            throw new Exception("Length Error");

                        if (!Decimal.TryParse(TextBoxWidth.Text, out width) && TextBoxWidth.Text != "")
                            throw new Exception("Width Error");

                        if (!Decimal.TryParse(TextBoxHeight.Text, out height) && TextBoxHeight.Text != "")
                            throw new Exception("Height Error");

                        if (!Decimal.TryParse(TextBoxWeight.Text, out weight) && TextBoxWeight.Text != "")
                            throw new Exception("Weight Error");

                        if (!Decimal.TryParse(TextBoxPackQuantity.Text, out packQuantity) && TextBoxPackQuantity.Text != "")
                            throw new Exception("Pack Quantity Error");

                        if (!Decimal.TryParse(TextBoxPalletQuantity.Text, out palletQuantity) && TextBoxPalletQuantity.Text != "")
                            throw new Exception("Pallet Quantity Error");

                        if (!rcv.SetDims(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], length, width, height, weight, packQuantity, palletQuantity))
                        {
                            Master.MsgText = Resources.ResMessages.Failed;
                        }

                        MultiView1.ActiveViewIndex++;
                    }
                    catch { }

                    break;
                case 2:
                    receiptLineId = rcv.ReceivingLineUpdateBatch(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], TextBoxBatch.Text);

                    if (receiptLineId > 0)
                    {
                        Session["ReceiptLineId"] = receiptLineId;

                        if(Receiving.IsBOEL(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"]))
                        {
                            if (TextBoxBatch.Enabled)
                            {
                                TextBox1.Visible = true;
                                TextBoxBatch.Enabled = false;
                            }
                            else
                            {
                                TextBox1.Visible = false;
                                TextBoxBatch.Enabled = true;
                                receiptLineId = rcv.ReceivingLineUpdateBOEL(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], TextBox1.Text, TextBoxBatch.Text);

                                if (receiptLineId <= 0)
                                {
                                    Master.MsgText = Resources.ResMessages.InvalidBatch;
                                    break;
                                }
                        MultiView1.ActiveViewIndex++;
                            }
                        }
                        else
                        {
                            MultiView1.ActiveViewIndex++;
                        }

                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsViewProduct.DataBind();
                    }
                    else
                    {
                        if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 120)
                            && Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], -1) != LotAttributeRule.AutoGenerate)
                            TextBoxBatch.Text = "";

                        Master.MsgText = Resources.ResMessages.InvalidBatch;
                    }
                    break;
                case 3:
                    String expiryDate = TextBoxYear.Text + "/" + TextBoxMonth.Text + "/" + TextBoxDay.Text + " " + TextBoxHours.Text + ":" + TextBoxMinutes.Text;
                    DateTime result;

                    DateTime.TryParse(expiryDate, out result);

                    receiptLineId = rcv.ReceivingLineUpdateExpiry(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], expiryDate);

                    if (receiptLineId > 0)
                    {
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsViewProduct.DataBind();
                    }
                    else if (receiptLineId == -3)
                    {
                        Master.MsgText = Resources.ResMessages.InvalidBatch;
                    }
                    else
                    {
                        Master.MsgText = Resources.ResMessages.InvalidField;
                    }
                    break;
                case 4:
                    Decimal quantity = 0;

                    if (!Decimal.TryParse(TextBoxQuantity.Text, out quantity))
                    {
                        TextBoxQuantity.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidField;
                        break;
                    }
                    receiptLineId = rcv.ReceivingLineUpdateQuantity(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], quantity, (int)Session["OperatorId"]);

                    if (receiptLineId > 0)
                    {
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsViewProduct.DataBind();
                        GridViewSamples.DataBind();
                    }
                    else if (receiptLineId == -2)
                    {
                        TextBoxQuantity.Text = "";
                        Master.MsgText = Resources.ResMessages.NoOverReceipt;
                    }
                    else
                    {
                        TextBoxQuantity.Text = "";
                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                    }
                    break;
                case 5:
                    Decimal sample = 0;

                    if (!Decimal.TryParse(TextBoxSample.Text, out sample))
                    {
                        TextBoxSample.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidField;
                        break;
                    }

                    receiptLineId = rcv.ReceivingLineUpdateSample(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], sample);

                    if (receiptLineId > 0)
                    {
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsViewProduct.DataBind();
                    }
                    else
                    {
                        TextBoxSample.Text = "";
                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                    }
                    break;
                case 6:
                    int reasonId = -1;
                    Decimal reject = 0;

                    if (!Decimal.TryParse(TextBoxReject.Text, out reject))
                    {
                        TextBoxReject.Text = "0";
                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                        break;
                    }

                    if (RadioButtonListReason.SelectedValue.ToString() == "")
                    {
                        Master.MsgText = Resources.ResMessages.InvalidField;
                        break;
                    }
                    reasonId = int.Parse(RadioButtonListReason.SelectedValue.ToString());

                    receiptLineId = rcv.ReceivingLineUpdateReject(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], reject, reasonId);

                    if (receiptLineId > 0)
                    {
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsViewProduct.DataBind();
                    }
                    else
                    {
                        TextBoxReject.Text = "0";
                        Master.MsgText = Resources.ResMessages.QuantityReEnter;
                    }
                    break;
                case 7:
                    receiptLineId = rcv.ReceivingLineConfirmStoreLocation(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], TextBoxStoreLocation.Text);

                    if (receiptLineId > 0)
                    {
                        Reset();
                        DoPackaging();
                    }
                    else
                    {
                        TextBoxStoreLocation.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidLocation;
                    }
                    break;
                case 8:
                    Decimal numberOfPallets = 0;

                    if (!Decimal.TryParse(TextBoxNumberOfPallets.Text, out numberOfPallets))
                    {
                        TextBoxNumberOfPallets.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidField;
                        break;
                    }

                    receiptLineId = rcv.ReceivingLineUpdateNumberOfPallets(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], numberOfPallets);

                    if (receiptLineId > 0)
                    {
                        MultiView1.ActiveViewIndex++;
                        Master.MsgText = Resources.ResMessages.Successful;
                        DetailsViewProduct.DataBind();
                        //Reset();
                        //DoPackaging();
                    }
                    else
                    {
                        TextBoxNumberOfPallets.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidQuantity;
                    }
                    break;
                case 9:
                    receiptLineId = rcv.ReceivingLinePalletId(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], TextBoxPallet.Text);

                    if (receiptLineId > 0)
                    {
                        MultiView1.ActiveViewIndex++;
                    }
                    else
                    {
                        TextBoxStoreLocation.Text = "";
                        Master.MsgText = Resources.ResMessages.InvalidPalletId;
                    }
                    break;
                case 10:
                    
                    try
                    {
                        if (Session["SerialNumber"] != null)
                        {
                            Session["SerialNumber"] = null;
                            Reset();
                            DoPackaging();
                        }
                        else
                            Master.MsgText = Resources.ResMessages.SerialNumberCountError;
                    }
                    catch { }

                    break;

                //case 6:

                //    if (TextBoxActualQuantity.Text.ToString() == "")
                //    {
                //        TextBoxQuantity.Focus();
                //        Master.MsgText = "Enter Quantity";
                //        break;
                //    }
                //    else
                //    {

                //        Receiving r = new Receiving();
                //        string StorageUnitBatchId = "";

                //        StorageUnitBatchId = RadioButtonPackaging.SelectedItem.Value.ToString();
                //        decimal ActualQuantity = int.Parse(TextBoxActualQuantity.ToString());

                //        r.InsertPackageLines(Session["ConnectionStringName"].ToString(),
                //                            int.Parse(Session["WarehouseId"].ToString()),
                //                            int.Parse(Session["ReceiptId"].ToString()),
                //                            int.Parse(StorageUnitBatchId),
                //                            int.Parse(Session["OperatorID"].ToString()),
                //                            ActualQuantity);
                //        r.CompletePackageLines(Session["ConnectionStringName"].ToString(),
                //                            int.Parse(Session["WarehouseId"].ToString()),
                //                            int.Parse(Session["ReceiptId"].ToString()));
                //    }
                //    break;
            }
        }
        catch { }
    }
    #endregion LinkButtonAccept_Click

    #region MultiView1_ActiveViewChanged
    protected void MultiView1_ActiveViewChanged(object sender, EventArgs e)
    {
        LinkButtonSkip.Visible = false;
        LinkButtonRedelivery.Visible = false;

        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                TextBoxPallet.Text = "";
                TextBoxExpiryDate.Text = "";
                
                LinkButtonRedelivery.Visible = Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 17);
                break;
            case 1:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 385))
                    MultiView1.ActiveViewIndex++;
                else
                {
                    Receiving rcv = new Receiving();
                    DataSet ds = null;

                    ds = rcv.GetDims(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"]);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        TextBoxLength.Text = dr.ItemArray.GetValue(0).ToString();
                        TextBoxWidth.Text = dr.ItemArray.GetValue(1).ToString();
                        TextBoxHeight.Text = dr.ItemArray.GetValue(2).ToString();
                        TextBoxWeight.Text = dr.ItemArray.GetValue(3).ToString();
                        TextBoxPackQuantity.Text = dr.ItemArray.GetValue(4).ToString();
                        TextBoxPalletQuantity.Text = dr.ItemArray.GetValue(5).ToString();
                    }
                }

                LinkButtonSkip.Visible = true;

                break;
            case 2:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 204)||
                        Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], -1) == LotAttributeRule.None)
                    MultiView1.ActiveViewIndex++;

                LinkButtonSkip.Visible = true;

                break;
            case 3:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 124) 
                    || Configuration.GetSwitchReceiptLineId(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"])
                    || Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], -1) == LotAttributeRule.None)
                    MultiView1.ActiveViewIndex++;
                else
                {
                    if (!Configuration.GetSwitchReceiptLineId(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"]))
                    {
                        Receiving rcv = new Receiving();

                        TextBoxExpiryDate.Text = rcv.ReceivingGetExpiryDate(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"]);

                        DateTime ExpiryDate = Convert.ToDateTime(TextBoxExpiryDate.Text);
                        TextBoxYear.Text = ExpiryDate.Year.ToString();
                        TextBoxMonth.Text = ExpiryDate.Month.ToString();
                        TextBoxDay.Text = ExpiryDate.Day.ToString();
                        TextBoxHours.Text = ExpiryDate.Hour.ToString();
                        TextBoxMinutes.Text = ExpiryDate.Minute.ToString();
                    }
                    else
                    {
                        TextBoxYear.Text = DateTime.Now.Year.ToString();
                        TextBoxMonth.Text = DateTime.Now.Month.ToString();
                        TextBoxDay.Text = DateTime.Now.Day.ToString();
                        TextBoxHours.Text = DateTime.Now.Hour.ToString();
                        TextBoxMinutes.Text = DateTime.Now.Minute.ToString();
                    }
                }
                break;
            case 5:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 151))
                    MultiView1.ActiveViewIndex++;
                else if (Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 223))
                {

                    Receiving rcv = new Receiving();
                    String sampleQuantity = "0";

                    sampleQuantity = rcv.ReceivingLineSampleQuantity(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"]);

                    if (sampleQuantity != "")
                    {
                        if (Session["OperatorGroupCode"] != null)
                            if (Session["OperatorGroupCode"].ToString() != "A" && Session["OperatorGroupCode"].ToString() != "S")
                                TextBoxSample.Enabled = false;

                        TextBoxSample.Text = sampleQuantity.ToString();
                    }
                    else
                        TextBoxSample.Enabled = true;
                }
                GridViewSamples.DataBind();
                LinkButtonSkip.Visible = true;

                break;
            case 6:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 152))
                    MultiView1.ActiveViewIndex++;

                LinkButtonSkip.Visible = true;

                break;
            case 7:
                Transact tran = new Transact();
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 174) && tran.ScanStoreLocationByArea(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"]))
                    MultiView1.ActiveViewIndex++;

                LinkButtonSkip.Visible = false;
                break;
            case 8:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 288))
                    MultiView1.ActiveViewIndex++;

                LinkButtonSkip.Visible = false;
                break;
            case 9:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 289))
                    MultiView1.ActiveViewIndex++;

                LinkButtonSkip.Visible = false;
                break;
            case 10:
                if (!SerialNumber.IsTrackedReceiptLineId(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"]))
                    Reset();
                else
                {
                    Session["KeyId"] = (int)Session["ReceiptLineId"];
                    Session["KeyType"] = "ReceiptLineId";
                    Session["KeyStorageUnitId"] = null;
                }

                LinkButtonSkip.Visible = false;
                break;
        }
    }
    #endregion MultiView1_ActiveViewChanged

    #region LinkButtonBack_Click
    protected void LinkButtonBack_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 0:
                Response.Redirect("~/Screens/ReceiveDocument.aspx");
                break;
            case 4:
                if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 124) || Configuration.GetSwitchReceiptLineId(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"]))
                    if (!Configuration.GetSwitch(Session["ConnectionStringName"].ToString(), (int)Session["WarehouseId"], 204) ||
                            Configuration.GetLotAttricuteRule(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptLineId"], -1) == LotAttributeRule.None)
                        MultiView1.ActiveViewIndex = 0;
                    else
                        MultiView1.ActiveViewIndex = 1;
                else
                    MultiView1.ActiveViewIndex--;
                break;
            case 6:

                // Go back to Quantity - not sure which configs may be off
                MultiView1.ActiveViewIndex = 4;
                break;
            case 7:

                // Go back to Quantity - not sure which configs may be off
                MultiView1.ActiveViewIndex = 4;
                break;
            case 8:

                // Go back to Quantity - not sure which configs may be off
                MultiView1.ActiveViewIndex = 4;
                break;
            case 9:

                // Go back to Quantity - not sure which configs may be off
                MultiView1.ActiveViewIndex = 4;
                break;
            case 10:

                // Go back to Quantity - not sure which configs may be off
                MultiView1.ActiveViewIndex = 4;
                break;

            default:
                MultiView1.ActiveViewIndex--;
                break;
        }
    }
    #endregion LinkButtonBack_Click

    #region LinkButtonSkip_Click
    protected void LinkButtonSkip_Click(object sender, EventArgs e)
    {
        switch (MultiView1.ActiveViewIndex)
        {
            case 2:
                MultiView1.ActiveViewIndex = 4;
                break;
            case 8:
                Reset();
                DoPackaging();
                break;
            default:
                MultiView1.ActiveViewIndex++;
                break;
        }
    }
    #endregion LinkButtonSkip_Click

    #region Reset
    protected void Reset()
    {
        MultiView1.ActiveViewIndex = 0;
        Session["ReceiptLineIdPass"] = Session["ReceiptLineId"];
        Session["ReceiptLineId"] = null;
        Master.MsgText = Resources.ResMessages.Successful;
        DetailsViewProduct.DataBind();
        TextBoxBarcode.Text = "";
        //TextBoxBatch.Text = "";
        TextBoxQuantity.Text = "";
        TextBoxSample.Text = "";
        TextBoxReject.Text = "0";
        TextBoxStoreLocation.Text = "";
        TextBoxNumberOfPallets.Text = "";
        RadioButtonListReason.SelectedIndex = 0;
    }
    #endregion Reset

    #region LinkButtonPackaging_Click
    protected void LinkButtonPackaging_Click(object sender, EventArgs e)
    {
        Session["ActiveViewIndex"] = MultiView1.ActiveViewIndex;
        DoPackaging();
    }
    #endregion LinkButtonPackaging_Click

    #region LinkButtonRedelivery_Click
    protected void LinkButtonRedelivery_Click(object sender, EventArgs e)
    {
        try
        {
            Receiving rcv = new Receiving();
            int receiptId = 0;

            receiptId = rcv.Redelivery(Session["ConnectionStringName"].ToString(), (int)Session["ReceiptId"]);

            if (receiptId > 0)
            {
                Session["ReceiptId"] = receiptId;
                Response.Redirect("~/Screens/ReceiveDocument.aspx");
            }
            else
            {
                TextBoxBarcode.Text = "";
                Master.MsgText = Resources.ResMessages.InvalidRedelivery;
            }
        }
        catch { }
    }
    #endregion LinkButtonRedelivery_Click

    #region DoPackaging
    protected void DoPackaging()
    {

        //Session["ReturnURL"] = HttpContext.Current.Request.Url;;
        //Response.Redirect("~/Screens/Packaging.aspx");

    }
    #endregion DoPackaging
}
