<%@ Page Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="PalletRebuild.aspx.cs" Inherits="Screens_PalletRebuild" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <br />
            <asp:Label ID="LabelBarcode" runat="server" Text="<%$ Resources:Default, FromBarcode %>"></asp:Label>
            <asp:TextBox ID="TextBoxBarcode" runat="server" TabIndex="1"></asp:TextBox>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <asp:DetailsView ID="DetailsViewComments" runat="server" DataKeyNames="ExceptionId" DataSourceID="ObjectDataSourceComments" AutoGenerateRows="False" PageIndex="0" AllowPaging="true">
                <Fields>
                    <asp:BoundField DataField="Comments" HeaderText="<%$ Resources:Default, Comments %>" />
                    <asp:BoundField DataField="Detail" HeaderText="<%$ Resources:Default, Detail %>" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="ObjectDataSourceComments" runat="server" TypeName="PalletRebuild"
                SelectMethod="GetComments">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="jobId" SessionField="JobId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <br />
            <asp:Label ID="LabelProduct" runat="server" Text="Product :"></asp:Label>
            <asp:TextBox ID="TextBoxProduct" runat="server" TabIndex="2"></asp:TextBox>
            <asp:Label ID="LabelOr" runat="server" Text=" or "></asp:Label>
            <asp:Button ID="ButtonAll" runat="server" TabIndex="3" Text="<%$ Resources:Default, All %>" OnClick="ButtonAll_Click"></asp:Button>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <br />
            <asp:GridView ID="GridViewInstructions" runat="server" DataSourceID="ObjectDataSourceInstructions" DataKeyNames="IssueLineId,InstructionId,ConfirmedQuantity" AllowSorting="true"
                AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewUnlinked_SelectedIndexChanged">
                <Columns>
                    <asp:CommandField SelectText="<%$ Resources:Default, Select %>" ShowSelectButton="True" HeaderText="<%$ Resources:Default, Transfer %>" />
                    <asp:BoundField DataField="OrderNumber" HeaderText="<%$ Resources:Default, OrderNumber %>" SortExpression="OrderNumber" />
                    <asp:BoundField DataField="ExternalCompany" HeaderText="<%$ Resources:Default, Customer %>" SortExpression="ExternalCompany" />
                    <asp:BoundField DataField="ProductCode" HeaderText="<%$ Resources:Default, ProductCode %>" SortExpression="ProductCode" />
                    <asp:BoundField DataField="ConfirmedQuantity" HeaderText="<%$ Resources:Default, Quantity %>" SortExpression="ConfirmedQuantity" />
                </Columns>
            </asp:GridView>

            <asp:ObjectDataSource ID="ObjectDataSourceInstructions" runat="server" TypeName="PalletRebuild" SelectMethod="GetInstructions">
                <SelectParameters>
                    <asp:SessionParameter Name="connectionStringName" SessionField="ConnectionStringName" Type="String" />
                    <asp:SessionParameter Name="jobId" SessionField="JobId" Type="String" />
                    <asp:ControlParameter Name="barcode" ControlID="TextBoxProduct" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <br />
            <asp:Label ID="LabelQuantity" runat="server" Text="<%$ Resources:Default, Quantity %>"></asp:Label>
            <asp:TextBox ID="TextBoxQuantity" runat="server" TabIndex="4"></asp:TextBox>
        </asp:View>
        <asp:View ID="View7" runat="server">
            <br />
            <asp:Label ID="LabelBarcode2" runat="server" Text="<%$ Resources:Default, ToBarcode %>"></asp:Label>
            <asp:TextBox ID="TextBoxBarcode2" runat="server" TabIndex="5"></asp:TextBox>
        </asp:View>
    </asp:MultiView>
    <br />
    <asp:Button ID="LinkButtonAccept" runat="server" TabIndex="9" Text="<%$ Resources:Default, Accept %>" OnClick="LinkButtonAccept_Click"></asp:Button>
    <asp:Button ID="LinkButtonBack" runat="server" TabIndex="10" Text="<%$ Resources:Default, Back %>" OnClick="LinkButtonBack_Click"></asp:Button>
    <asp:Button ID="btnQuit" runat="server" TabIndex="14" Text="<%$ Resources:Default, Quit %>" PostBackUrl="<%$ Resources:Reslabels, MainMenuNavigationURL %>"></asp:Button>
</asp:Content>

