using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Master : System.Web.UI.MasterPage
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Context.Session.IsNewSession)
            {
                FormsAuthentication.SignOut();
                Response.Redirect("~/Security/Login.aspx");
            }
            else
            {
                // Acquire Auth Ticket from the FormsIdentity object
                FormsAuthenticationTicket ticket = ((FormsIdentity)Context.User.Identity).Ticket;

                // Manually slide the expiration
                FormsAuthentication.RenewTicketIfOld(ticket);
            }
        }
        catch { }
    }
    #endregion Page_Load

    #region btnLogout_Click
    protected void btnLogout_Click(object sender, EventArgs e)
    {
        // Perform any post-logout processing, such as setting the
        // user's last logout time or clearing a per-user cache of 
        // objects here.
        try
        {
            LogonCredentials lc = new LogonCredentials();

            lc.UserLoggedOut(Session["ConnectionStringName"].ToString(), (int)Session["OperatorId"], "Mobile");
        }
        catch (Exception ex)
        {
            Session["LogonErrorMessage"] = ex.Message;
        }

        try
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            FormsAuthentication.RedirectToLoginPage();
        }
        catch { }
    }
    #endregion btnLogout_Click
}
